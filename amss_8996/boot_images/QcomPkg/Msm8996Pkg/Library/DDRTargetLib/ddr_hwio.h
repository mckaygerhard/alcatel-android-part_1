#ifndef __DDR_HWIO__
#define __DDR_HWIO__
/*
===========================================================================
*/
/**
  @file ddr_hwio
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v2 [istari_v2.1_p3q2r16.11]
 
  This file contains HWIO register definitions for the following modules:
    MPM2_PSHOLD

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DDRTargetLib/ddr_hwio.h#3 $
  $DateTime: 2015/08/17 12:45:03 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_REG_BASE                                                     (MPM2_MPM_BASE      + 0x00000000)

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000000)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_1_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000004)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_2_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000008)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_3_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_ADDR                                  (MPM2_MPM_REG_BASE      + 0x0000000c)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_4_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000010)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_5_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000014)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_6_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000018)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_7_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_ADDR                                  (MPM2_MPM_REG_BASE      + 0x0000001c)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_8_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000020)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_RMSK                                  0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_DELAY_BMSK                            0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_DELAY_SHFT                                  0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_ID_BMSK                          0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_ID_SHFT                               0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_ADDR_BMSK                         0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_ADDR_SHFT                              0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_DATA_BMSK                             0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_9_SLAVE_DATA_SHFT                              0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000024)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_10_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000028)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_11_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_ADDR                                 (MPM2_MPM_REG_BASE      + 0x0000002c)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_ADDR, HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_ON_CFG_12_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000030)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_1_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000034)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_2_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000038)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_3_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_ADDR                                 (MPM2_MPM_REG_BASE      + 0x0000003c)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_4_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000040)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_5_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000044)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_6_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000048)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_7_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_ADDR                                 (MPM2_MPM_REG_BASE      + 0x0000004c)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_8_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000050)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_DELAY_BMSK                           0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_DELAY_SHFT                                 0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_ID_BMSK                         0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_ID_SHFT                              0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_ADDR_BMSK                        0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_ADDR_SHFT                             0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_DATA_BMSK                            0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_9_SLAVE_DATA_SHFT                             0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_ADDR                                (MPM2_MPM_REG_BASE      + 0x00000054)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_RMSK                                0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_DELAY_BMSK                          0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_DELAY_SHFT                                0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_ID_BMSK                        0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_ID_SHFT                             0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_ADDR_BMSK                       0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_ADDR_SHFT                            0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_DATA_BMSK                           0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_10_SLAVE_DATA_SHFT                            0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_ADDR                                (MPM2_MPM_REG_BASE      + 0x00000058)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_RMSK                                0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_DELAY_BMSK                          0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_DELAY_SHFT                                0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_ID_BMSK                        0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_ID_SHFT                             0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_ADDR_BMSK                       0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_ADDR_SHFT                            0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_DATA_BMSK                           0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_11_SLAVE_DATA_SHFT                            0x0

#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_ADDR                                (MPM2_MPM_REG_BASE      + 0x0000005c)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_RMSK                                0xffffffff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_ADDR, HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_RMSK)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_ADDR, m)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_ADDR,v)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_ADDR,m,v,HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_IN)
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_DELAY_BMSK                          0xf0000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_DELAY_SHFT                                0x1c
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_ID_BMSK                        0xf000000
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_ID_SHFT                             0x18
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_ADDR_BMSK                       0xffff00
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_ADDR_SHFT                            0x8
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_DATA_BMSK                           0xff
#define HWIO_MPM2_MPM_PMIC_VDD_OFF_CFG_12_SLAVE_DATA_SHFT                            0x0

#define HWIO_MPM2_MPM_SPMI_CMD_CFG_ADDR                                       (MPM2_MPM_REG_BASE      + 0x00000060)
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_RMSK                                             0xff
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SPMI_CMD_CFG_ADDR, HWIO_MPM2_MPM_SPMI_CMD_CFG_RMSK)
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SPMI_CMD_CFG_ADDR, m)
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_SPMI_CMD_CFG_ADDR,v)
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_SPMI_CMD_CFG_ADDR,m,v,HWIO_MPM2_MPM_SPMI_CMD_CFG_IN)
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_NUM_CMD_OFF_BMSK                                 0xf0
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_NUM_CMD_OFF_SHFT                                  0x4
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_NUM_CMD_ON_BMSK                                   0xf
#define HWIO_MPM2_MPM_SPMI_CMD_CFG_NUM_CMD_ON_SHFT                                   0x0

#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_ADDR                                    (MPM2_MPM_REG_BASE      + 0x00000064)
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_RMSK                                         0x7ff
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_ADDR, HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_RMSK)
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_ADDR, m)
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_ADDR,v)
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_ADDR,m,v,HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_IN)
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_XO_DIV_BMSK                                  0x600
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_XO_DIV_SHFT                                    0x9
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_RO_DIV_BMSK                                  0x1ff
#define HWIO_MPM2_MPM_SPMI_CDIV_CNTRL_RO_DIV_SHFT                                    0x0

#define HWIO_MPM2_MPM_CXO_CTRL_ADDR                                           (MPM2_MPM_REG_BASE      + 0x00000068)
#define HWIO_MPM2_MPM_CXO_CTRL_RMSK                                                  0x3
#define HWIO_MPM2_MPM_CXO_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_MPM_CXO_CTRL_ADDR, HWIO_MPM2_MPM_CXO_CTRL_RMSK)
#define HWIO_MPM2_MPM_CXO_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_CXO_CTRL_ADDR, m)
#define HWIO_MPM2_MPM_CXO_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_CXO_CTRL_ADDR,v)
#define HWIO_MPM2_MPM_CXO_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_CXO_CTRL_ADDR,m,v,HWIO_MPM2_MPM_CXO_CTRL_IN)
#define HWIO_MPM2_MPM_CXO_CTRL_CXOPAD_DIS_BMSK                                       0x2
#define HWIO_MPM2_MPM_CXO_CTRL_CXOPAD_DIS_SHFT                                       0x1
#define HWIO_MPM2_MPM_CXO_CTRL_CXO_DIS_BMSK                                          0x1
#define HWIO_MPM2_MPM_CXO_CTRL_CXO_DIS_SHFT                                          0x0

#define HWIO_MPM2_MPM_PXO_OSC_CTRL_ADDR                                       (MPM2_MPM_REG_BASE      + 0x0000006c)
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_RMSK                                             0x7f
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PXO_OSC_CTRL_ADDR, HWIO_MPM2_MPM_PXO_OSC_CTRL_RMSK)
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PXO_OSC_CTRL_ADDR, m)
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PXO_OSC_CTRL_ADDR,v)
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PXO_OSC_CTRL_ADDR,m,v,HWIO_MPM2_MPM_PXO_OSC_CTRL_IN)
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_GAIN_BMSK                                0x70
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_GAIN_SHFT                                 0x4
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXOPAD_CORE_DIS_BMSK                              0x8
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXOPAD_CORE_DIS_SHFT                              0x3
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_SLEEP_BMSK                                0x4
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_SLEEP_SHFT                                0x2
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXOPAD_HV_DIS_BMSK                                0x2
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXOPAD_HV_DIS_SHFT                                0x1
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_RF_BYPASS_BMSK                            0x1
#define HWIO_MPM2_MPM_PXO_OSC_CTRL_PXO_OSC_RF_BYPASS_SHFT                            0x0

#define HWIO_MPM2_MPM_LOW_POWER_CFG_ADDR                                      (MPM2_MPM_REG_BASE      + 0x00000070)
#define HWIO_MPM2_MPM_LOW_POWER_CFG_RMSK                                        0x1f5fff
#define HWIO_MPM2_MPM_LOW_POWER_CFG_IN          \
        in_dword_masked(HWIO_MPM2_MPM_LOW_POWER_CFG_ADDR, HWIO_MPM2_MPM_LOW_POWER_CFG_RMSK)
#define HWIO_MPM2_MPM_LOW_POWER_CFG_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_LOW_POWER_CFG_ADDR, m)
#define HWIO_MPM2_MPM_LOW_POWER_CFG_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_LOW_POWER_CFG_ADDR,v)
#define HWIO_MPM2_MPM_LOW_POWER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_LOW_POWER_CFG_ADDR,m,v,HWIO_MPM2_MPM_LOW_POWER_CFG_IN)
#define HWIO_MPM2_MPM_LOW_POWER_CFG_CXOPAD_EN_SEL_BMSK                          0x100000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_CXOPAD_EN_SEL_SHFT                              0x14
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SMEM_EN_BMSK                                 0x80000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SMEM_EN_SHFT                                    0x13
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_FREEZE_IO_M_BMSK                     0x40000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_FREEZE_IO_M_SHFT                        0x12
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CLAMP_MEM_BMSK                       0x20000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CLAMP_MEM_SHFT                          0x11
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_HW_RESTORED_BMSK                     0x10000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_HW_RESTORED_SHFT                        0x10
#define HWIO_MPM2_MPM_LOW_POWER_CFG_ENABLE_CXOPAD_GATING_BMSK                     0x4000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_ENABLE_CXOPAD_GATING_SHFT                        0xe
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_TRST_N_BMSK                           0x1000
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_TRST_N_SHFT                              0xc
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CORE_RESET_BMSK                        0x800
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CORE_RESET_SHFT                          0xb
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CLAMPS_BMSK                            0x400
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_CLAMPS_SHFT                              0xa
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_FREEZE_BMSK                            0x200
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DISABLE_FREEZE_SHFT                              0x9
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DEBUG_ON_BMSK                                  0x100
#define HWIO_MPM2_MPM_LOW_POWER_CFG_DEBUG_ON_SHFT                                    0x8
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SW_EBI1_CTL_ENABLE_BMSK                         0x80
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SW_EBI1_CTL_ENABLE_SHFT                          0x7
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SW_EBI1_CTL_VALUE_BMSK                          0x40
#define HWIO_MPM2_MPM_LOW_POWER_CFG_SW_EBI1_CTL_VALUE_SHFT                           0x6
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDMX_MIN_EN_BMSK                               0x20
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDMX_MIN_EN_SHFT                                0x5
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDMX_PC_EN_BMSK                                0x10
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDMX_PC_EN_SHFT                                 0x4
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDCX_MIN_EN_BMSK                                0x8
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDCX_MIN_EN_SHFT                                0x3
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDCX_PC_EN_BMSK                                 0x4
#define HWIO_MPM2_MPM_LOW_POWER_CFG_VDDCX_PC_EN_SHFT                                 0x2
#define HWIO_MPM2_MPM_LOW_POWER_CFG_PXO_SD_EN_BMSK                                   0x2
#define HWIO_MPM2_MPM_LOW_POWER_CFG_PXO_SD_EN_SHFT                                   0x1
#define HWIO_MPM2_MPM_LOW_POWER_CFG_CXO_SD_EN_BMSK                                   0x1
#define HWIO_MPM2_MPM_LOW_POWER_CFG_CXO_SD_EN_SHFT                                   0x0

#define HWIO_MPM2_MPM_LOW_POWER_STATUS_ADDR                                   (MPM2_MPM_REG_BASE      + 0x00000074)
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_RMSK                                       0x3fff
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_MPM_LOW_POWER_STATUS_ADDR, HWIO_MPM2_MPM_LOW_POWER_STATUS_RMSK)
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_LOW_POWER_STATUS_ADDR, m)
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_SPMI_SEQ_FSM_BMSK                          0x3000
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_SPMI_SEQ_FSM_SHFT                             0xc
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_SMEM_FSM_STATE_BMSK                         0xe00
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_SMEM_FSM_STATE_SHFT                           0x9
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_VDDMX_DOWN_BMSK                             0x100
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_VDDMX_DOWN_SHFT                               0x8
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_VDDCX_DOWN_BMSK                              0x80
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_VDDCX_DOWN_SHFT                               0x7
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_PXO_DOWN_BMSK                                0x40
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_PXO_DOWN_SHFT                                 0x6
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_CXO_DOWN_BMSK                                0x20
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_CXO_DOWN_SHFT                                 0x5
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_POWER_DOWN_BMSK                              0x10
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_POWER_DOWN_SHFT                               0x4
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_FSM_STATE_BMSK                                0xf
#define HWIO_MPM2_MPM_LOW_POWER_STATUS_FSM_STATE_SHFT                                0x0

#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_ADDR                              (MPM2_MPM_REG_BASE      + 0x00000078)
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_RMSK                              0xffffffff
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_IN          \
        in_dword_masked(HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_ADDR, HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_RMSK)
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_ADDR, m)
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_ADDR,v)
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_ADDR,m,v,HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_IN)
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_CXO_WARMUP_TIME_BMSK              0xffff0000
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_CXO_WARMUP_TIME_SHFT                    0x10
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_POWER_RAMPUP_TIME_BMSK                0xffff
#define HWIO_MPM2_MPM_CXO_POWER_RAMPUP_TIME_POWER_RAMPUP_TIME_SHFT                   0x0

#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_ADDR                                    (MPM2_MPM_REG_BASE      + 0x0000007c)
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_RMSK                                        0xffff
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PXO_WARMUP_TIME_ADDR, HWIO_MPM2_MPM_PXO_WARMUP_TIME_RMSK)
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PXO_WARMUP_TIME_ADDR, m)
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PXO_WARMUP_TIME_ADDR,v)
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PXO_WARMUP_TIME_ADDR,m,v,HWIO_MPM2_MPM_PXO_WARMUP_TIME_IN)
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_PXO_WARMUP_TIME_BMSK                        0xffff
#define HWIO_MPM2_MPM_PXO_WARMUP_TIME_PXO_WARMUP_TIME_SHFT                           0x0

#define HWIO_MPM2_MPM_HARDWARE_RESTORED_ADDR                                  (MPM2_MPM_REG_BASE      + 0x00000080)
#define HWIO_MPM2_MPM_HARDWARE_RESTORED_RMSK                                         0x1
#define HWIO_MPM2_MPM_HARDWARE_RESTORED_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_HARDWARE_RESTORED_ADDR,v)
#define HWIO_MPM2_MPM_HARDWARE_RESTORED_DATA_BMSK                                    0x1
#define HWIO_MPM2_MPM_HARDWARE_RESTORED_DATA_SHFT                                    0x0

#define HWIO_MPM2_MPM_INT_CLEAR_1_ADDR                                        (MPM2_MPM_REG_BASE      + 0x00000084)
#define HWIO_MPM2_MPM_INT_CLEAR_1_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_CLEAR_1_ADDR,v)
#define HWIO_MPM2_MPM_INT_CLEAR_1_INT_CLEAR_1_BMSK                            0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_1_INT_CLEAR_1_SHFT                                   0x0

#define HWIO_MPM2_MPM_INT_CLEAR_2_ADDR                                        (MPM2_MPM_REG_BASE      + 0x00000088)
#define HWIO_MPM2_MPM_INT_CLEAR_2_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_CLEAR_2_ADDR,v)
#define HWIO_MPM2_MPM_INT_CLEAR_2_INT_CLEAR_2_BMSK                            0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_2_INT_CLEAR_2_SHFT                                   0x0

#define HWIO_MPM2_MPM_INT_EN_1_ADDR                                           (MPM2_MPM_REG_BASE      + 0x0000008c)
#define HWIO_MPM2_MPM_INT_EN_1_RMSK                                           0xffffffff
#define HWIO_MPM2_MPM_INT_EN_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_1_ADDR, HWIO_MPM2_MPM_INT_EN_1_RMSK)
#define HWIO_MPM2_MPM_INT_EN_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_1_ADDR, m)
#define HWIO_MPM2_MPM_INT_EN_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_EN_1_ADDR,v)
#define HWIO_MPM2_MPM_INT_EN_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_EN_1_ADDR,m,v,HWIO_MPM2_MPM_INT_EN_1_IN)
#define HWIO_MPM2_MPM_INT_EN_1_INT_EN_1_BMSK                                  0xffffffff
#define HWIO_MPM2_MPM_INT_EN_1_INT_EN_1_SHFT                                         0x0

#define HWIO_MPM2_MPM_INT_EN_2_ADDR                                           (MPM2_MPM_REG_BASE      + 0x00000090)
#define HWIO_MPM2_MPM_INT_EN_2_RMSK                                           0xffffffff
#define HWIO_MPM2_MPM_INT_EN_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_2_ADDR, HWIO_MPM2_MPM_INT_EN_2_RMSK)
#define HWIO_MPM2_MPM_INT_EN_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_2_ADDR, m)
#define HWIO_MPM2_MPM_INT_EN_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_EN_2_ADDR,v)
#define HWIO_MPM2_MPM_INT_EN_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_EN_2_ADDR,m,v,HWIO_MPM2_MPM_INT_EN_2_IN)
#define HWIO_MPM2_MPM_INT_EN_2_INT_EN_2_BMSK                                  0xffffffff
#define HWIO_MPM2_MPM_INT_EN_2_INT_EN_2_SHFT                                         0x0

#define HWIO_MPM2_MPM_INT_POLARITY_1_ADDR                                     (MPM2_MPM_REG_BASE      + 0x00000094)
#define HWIO_MPM2_MPM_INT_POLARITY_1_RMSK                                     0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_1_ADDR, HWIO_MPM2_MPM_INT_POLARITY_1_RMSK)
#define HWIO_MPM2_MPM_INT_POLARITY_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_1_ADDR, m)
#define HWIO_MPM2_MPM_INT_POLARITY_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_POLARITY_1_ADDR,v)
#define HWIO_MPM2_MPM_INT_POLARITY_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_POLARITY_1_ADDR,m,v,HWIO_MPM2_MPM_INT_POLARITY_1_IN)
#define HWIO_MPM2_MPM_INT_POLARITY_1_INT_POLARITY_1_BMSK                      0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_1_INT_POLARITY_1_SHFT                             0x0

#define HWIO_MPM2_MPM_INT_POLARITY_2_ADDR                                     (MPM2_MPM_REG_BASE      + 0x00000098)
#define HWIO_MPM2_MPM_INT_POLARITY_2_RMSK                                     0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_2_ADDR, HWIO_MPM2_MPM_INT_POLARITY_2_RMSK)
#define HWIO_MPM2_MPM_INT_POLARITY_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_2_ADDR, m)
#define HWIO_MPM2_MPM_INT_POLARITY_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_POLARITY_2_ADDR,v)
#define HWIO_MPM2_MPM_INT_POLARITY_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_POLARITY_2_ADDR,m,v,HWIO_MPM2_MPM_INT_POLARITY_2_IN)
#define HWIO_MPM2_MPM_INT_POLARITY_2_INT_POLARITY_2_BMSK                      0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_2_INT_POLARITY_2_SHFT                             0x0

#define HWIO_MPM2_MPM_DETECT_CTL_1_ADDR                                       (MPM2_MPM_REG_BASE      + 0x0000009c)
#define HWIO_MPM2_MPM_DETECT_CTL_1_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_1_ADDR, HWIO_MPM2_MPM_DETECT_CTL_1_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_1_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_1_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_1_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_1_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_1_DETECT_CTL_1_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_1_DETECT_CTL_1_SHFT                                 0x0

#define HWIO_MPM2_MPM_DETECT_CTL_2_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000a0)
#define HWIO_MPM2_MPM_DETECT_CTL_2_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_2_ADDR, HWIO_MPM2_MPM_DETECT_CTL_2_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_2_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_2_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_2_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_2_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_2_DETECT_CTL_2_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_2_DETECT_CTL_2_SHFT                                 0x0

#define HWIO_MPM2_MPM_DETECT_CTL_3_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000a4)
#define HWIO_MPM2_MPM_DETECT_CTL_3_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_3_ADDR, HWIO_MPM2_MPM_DETECT_CTL_3_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_3_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_3_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_3_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_3_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_3_DETECT_CTL_3_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_3_DETECT_CTL_3_SHFT                                 0x0

#define HWIO_MPM2_MPM_DETECT_CTL_4_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000a8)
#define HWIO_MPM2_MPM_DETECT_CTL_4_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_4_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_4_ADDR, HWIO_MPM2_MPM_DETECT_CTL_4_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_4_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_4_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_4_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_4_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_4_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_4_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_4_DETECT_CTL_4_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_4_DETECT_CTL_4_SHFT                                 0x0

#define HWIO_MPM2_MPM_INT_STATUS_1_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000ac)
#define HWIO_MPM2_MPM_INT_STATUS_1_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_1_ADDR, HWIO_MPM2_MPM_INT_STATUS_1_RMSK)
#define HWIO_MPM2_MPM_INT_STATUS_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_1_ADDR, m)
#define HWIO_MPM2_MPM_INT_STATUS_1_INT_STATUS_1_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_1_INT_STATUS_1_SHFT                                 0x0

#define HWIO_MPM2_MPM_INT_STATUS_2_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000b0)
#define HWIO_MPM2_MPM_INT_STATUS_2_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_2_ADDR, HWIO_MPM2_MPM_INT_STATUS_2_RMSK)
#define HWIO_MPM2_MPM_INT_STATUS_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_2_ADDR, m)
#define HWIO_MPM2_MPM_INT_STATUS_2_INT_STATUS_2_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_2_INT_STATUS_2_SHFT                                 0x0

#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_ADDR                                 (MPM2_MPM_REG_BASE      + 0x000000b4)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_ADDR, HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_RMSK)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_ADDR, m)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_ADDR,v)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_ADDR,m,v,HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_IN)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_MPM_INT_WKUP_CLK_SEL_1_BMSK          0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_1_MPM_INT_WKUP_CLK_SEL_1_SHFT                 0x0

#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_ADDR                                 (MPM2_MPM_REG_BASE      + 0x000000b8)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_ADDR, HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_RMSK)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_ADDR, m)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_ADDR,v)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_ADDR,m,v,HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_IN)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_MPM_INT_WKUP_CLK_SEL_2_BMSK          0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_2_MPM_INT_WKUP_CLK_SEL_2_SHFT                 0x0

#define HWIO_MPM2_MPM_DEBUG_BUS_EN_ADDR                                       (MPM2_MPM_REG_BASE      + 0x000000bc)
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_RMSK                                             0x1f
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DEBUG_BUS_EN_ADDR, HWIO_MPM2_MPM_DEBUG_BUS_EN_RMSK)
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DEBUG_BUS_EN_ADDR, m)
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DEBUG_BUS_EN_ADDR,v)
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DEBUG_BUS_EN_ADDR,m,v,HWIO_MPM2_MPM_DEBUG_BUS_EN_IN)
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_QTMR_TEST_BUS_EN_BMSK                            0x10
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_QTMR_TEST_BUS_EN_SHFT                             0x4
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_TEST_BUS_S_EN_BMSK                                0x8
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_TEST_BUS_S_EN_SHFT                                0x3
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_TEST_BUS_M_EN_BMSK                                0x4
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_TEST_BUS_M_EN_SHFT                                0x2
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_DEBUG_EN_1_BMSK                                   0x2
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_DEBUG_EN_1_SHFT                                   0x1
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_DEBUG_EN_BMSK                                     0x1
#define HWIO_MPM2_MPM_DEBUG_BUS_EN_DEBUG_EN_SHFT                                     0x0

#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ADDR                                   (MPM2_MPM_REG_BASE      + 0x000000c0)
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_RMSK                                          0xf
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ADDR, HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_RMSK)
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ADDR, m)
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ADDR,v)
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ADDR,m,v,HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_IN)
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ENABLE_BMSK                                   0x8
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_ENABLE_SHFT                                   0x3
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_TIME_BMSK                                     0x7
#define HWIO_MPM2_MPM_PEN_DEBOUNCE_CTL_TIME_SHFT                                     0x0

#define HWIO_MPM2_MPM_WARM_BOOT_CFG_ADDR                                      (MPM2_MPM_REG_BASE      + 0x000000c4)
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_RMSK                                             0x3
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_IN          \
        in_dword_masked(HWIO_MPM2_MPM_WARM_BOOT_CFG_ADDR, HWIO_MPM2_MPM_WARM_BOOT_CFG_RMSK)
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_WARM_BOOT_CFG_ADDR, m)
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_WARM_BOOT_CFG_ADDR,v)
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_WARM_BOOT_CFG_ADDR,m,v,HWIO_MPM2_MPM_WARM_BOOT_CFG_IN)
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_ENABLE_EBI2_BMSK                                 0x2
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_ENABLE_EBI2_SHFT                                 0x1
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_ENABLE_EBI1_BMSK                                 0x1
#define HWIO_MPM2_MPM_WARM_BOOT_CFG_ENABLE_EBI1_SHFT                                 0x0

#define HWIO_MPM2_MPM_REGn_FILE_ADDR(n)                                       (MPM2_MPM_REG_BASE      + 0x000000c8 + 0x4 * (n))
#define HWIO_MPM2_MPM_REGn_FILE_RMSK                                          0xffffffff
#define HWIO_MPM2_MPM_REGn_FILE_MAXn                                                   8
#define HWIO_MPM2_MPM_REGn_FILE_INI(n)        \
        in_dword_masked(HWIO_MPM2_MPM_REGn_FILE_ADDR(n), HWIO_MPM2_MPM_REGn_FILE_RMSK)
#define HWIO_MPM2_MPM_REGn_FILE_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_MPM_REGn_FILE_ADDR(n), mask)
#define HWIO_MPM2_MPM_REGn_FILE_OUTI(n,val)    \
        out_dword(HWIO_MPM2_MPM_REGn_FILE_ADDR(n),val)
#define HWIO_MPM2_MPM_REGn_FILE_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_MPM_REGn_FILE_ADDR(n),mask,val,HWIO_MPM2_MPM_REGn_FILE_INI(n))
#define HWIO_MPM2_MPM_REGn_FILE_DATA_BMSK                                     0xffffffff
#define HWIO_MPM2_MPM_REGn_FILE_DATA_SHFT                                            0x0

#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_ADDR                                   (MPM2_MPM_REG_BASE      + 0x000000ec)
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_RMSK                                          0x7
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_IN          \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_CLK_CONTROL_ADDR, HWIO_MPM2_MPM_CNTR_CLK_CONTROL_RMSK)
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_CLK_CONTROL_ADDR, m)
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_CNTR_CLK_CONTROL_ADDR,v)
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_CNTR_CLK_CONTROL_ADDR,m,v,HWIO_MPM2_MPM_CNTR_CLK_CONTROL_IN)
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_CLK_STATUS_BMSK                               0x4
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_CLK_STATUS_SHFT                               0x2
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_SW_OVERWRITE_SWITCH_BMSK                      0x2
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_SW_OVERWRITE_SWITCH_SHFT                      0x1
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_SW_CLK_SWITCH_BMSK                            0x1
#define HWIO_MPM2_MPM_CNTR_CLK_CONTROL_SW_CLK_SWITCH_SHFT                            0x0

#define HWIO_MPM2_MPM_CNTR_FRAC_ADDR                                          (MPM2_MPM_REG_BASE      + 0x000000f0)
#define HWIO_MPM2_MPM_CNTR_FRAC_RMSK                                              0xffff
#define HWIO_MPM2_MPM_CNTR_FRAC_IN          \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_FRAC_ADDR, HWIO_MPM2_MPM_CNTR_FRAC_RMSK)
#define HWIO_MPM2_MPM_CNTR_FRAC_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_FRAC_ADDR, m)
#define HWIO_MPM2_MPM_CNTR_FRAC_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_CNTR_FRAC_ADDR,v)
#define HWIO_MPM2_MPM_CNTR_FRAC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_CNTR_FRAC_ADDR,m,v,HWIO_MPM2_MPM_CNTR_FRAC_IN)
#define HWIO_MPM2_MPM_CNTR_FRAC_LOAD_VAL_BMSK                                     0xffff
#define HWIO_MPM2_MPM_CNTR_FRAC_LOAD_VAL_SHFT                                        0x0

#define HWIO_MPM2_MPM_CNTR_INCR_DATA_ADDR                                     (MPM2_MPM_REG_BASE      + 0x000000f4)
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_RMSK                                     0xffffffff
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_IN          \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_INCR_DATA_ADDR, HWIO_MPM2_MPM_CNTR_INCR_DATA_RMSK)
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_CNTR_INCR_DATA_ADDR, m)
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_CNTR_INCR_DATA_ADDR,v)
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_CNTR_INCR_DATA_ADDR,m,v,HWIO_MPM2_MPM_CNTR_INCR_DATA_IN)
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_MULT_BMSK                                0xffffffff
#define HWIO_MPM2_MPM_CNTR_INCR_DATA_MULT_SHFT                                       0x0

#define HWIO_MPM2_MPM_HW_ID_ADDR                                              (MPM2_MPM_REG_BASE      + 0x000000f8)
#define HWIO_MPM2_MPM_HW_ID_RMSK                                              0xffffffff
#define HWIO_MPM2_MPM_HW_ID_IN          \
        in_dword_masked(HWIO_MPM2_MPM_HW_ID_ADDR, HWIO_MPM2_MPM_HW_ID_RMSK)
#define HWIO_MPM2_MPM_HW_ID_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_HW_ID_ADDR, m)
#define HWIO_MPM2_MPM_HW_ID_MAJOR_BMSK                                        0xf0000000
#define HWIO_MPM2_MPM_HW_ID_MAJOR_SHFT                                              0x1c
#define HWIO_MPM2_MPM_HW_ID_MINOR_BMSK                                         0xfff0000
#define HWIO_MPM2_MPM_HW_ID_MINOR_SHFT                                              0x10
#define HWIO_MPM2_MPM_HW_ID_STEP_BMSK                                             0xffff
#define HWIO_MPM2_MPM_HW_ID_STEP_SHFT                                                0x0

#define HWIO_MPM2_MPM_INT_CLEAR_3_ADDR                                        (MPM2_MPM_REG_BASE      + 0x000000fc)
#define HWIO_MPM2_MPM_INT_CLEAR_3_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_CLEAR_3_ADDR,v)
#define HWIO_MPM2_MPM_INT_CLEAR_3_INT_CLEAR_3_BMSK                            0xffffffff
#define HWIO_MPM2_MPM_INT_CLEAR_3_INT_CLEAR_3_SHFT                                   0x0

#define HWIO_MPM2_MPM_INT_EN_3_ADDR                                           (MPM2_MPM_REG_BASE      + 0x00000100)
#define HWIO_MPM2_MPM_INT_EN_3_RMSK                                           0xffffffff
#define HWIO_MPM2_MPM_INT_EN_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_3_ADDR, HWIO_MPM2_MPM_INT_EN_3_RMSK)
#define HWIO_MPM2_MPM_INT_EN_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_EN_3_ADDR, m)
#define HWIO_MPM2_MPM_INT_EN_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_EN_3_ADDR,v)
#define HWIO_MPM2_MPM_INT_EN_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_EN_3_ADDR,m,v,HWIO_MPM2_MPM_INT_EN_3_IN)
#define HWIO_MPM2_MPM_INT_EN_3_INT_EN_3_BMSK                                  0xffffffff
#define HWIO_MPM2_MPM_INT_EN_3_INT_EN_3_SHFT                                         0x0

#define HWIO_MPM2_MPM_INT_POLARITY_3_ADDR                                     (MPM2_MPM_REG_BASE      + 0x00000104)
#define HWIO_MPM2_MPM_INT_POLARITY_3_RMSK                                     0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_3_ADDR, HWIO_MPM2_MPM_INT_POLARITY_3_RMSK)
#define HWIO_MPM2_MPM_INT_POLARITY_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_POLARITY_3_ADDR, m)
#define HWIO_MPM2_MPM_INT_POLARITY_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_POLARITY_3_ADDR,v)
#define HWIO_MPM2_MPM_INT_POLARITY_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_POLARITY_3_ADDR,m,v,HWIO_MPM2_MPM_INT_POLARITY_3_IN)
#define HWIO_MPM2_MPM_INT_POLARITY_3_INT_POLARITY_3_BMSK                      0xffffffff
#define HWIO_MPM2_MPM_INT_POLARITY_3_INT_POLARITY_3_SHFT                             0x0

#define HWIO_MPM2_MPM_DETECT_CTL_5_ADDR                                       (MPM2_MPM_REG_BASE      + 0x00000108)
#define HWIO_MPM2_MPM_DETECT_CTL_5_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_5_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_5_ADDR, HWIO_MPM2_MPM_DETECT_CTL_5_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_5_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_5_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_5_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_5_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_5_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_5_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_5_DETECT_CTL_5_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_5_DETECT_CTL_5_SHFT                                 0x0

#define HWIO_MPM2_MPM_DETECT_CTL_6_ADDR                                       (MPM2_MPM_REG_BASE      + 0x0000010c)
#define HWIO_MPM2_MPM_DETECT_CTL_6_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_6_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_6_ADDR, HWIO_MPM2_MPM_DETECT_CTL_6_RMSK)
#define HWIO_MPM2_MPM_DETECT_CTL_6_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DETECT_CTL_6_ADDR, m)
#define HWIO_MPM2_MPM_DETECT_CTL_6_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DETECT_CTL_6_ADDR,v)
#define HWIO_MPM2_MPM_DETECT_CTL_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DETECT_CTL_6_ADDR,m,v,HWIO_MPM2_MPM_DETECT_CTL_6_IN)
#define HWIO_MPM2_MPM_DETECT_CTL_6_DETECT_CTL_6_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_DETECT_CTL_6_DETECT_CTL_6_SHFT                                 0x0

#define HWIO_MPM2_MPM_INT_STATUS_3_ADDR                                       (MPM2_MPM_REG_BASE      + 0x00000110)
#define HWIO_MPM2_MPM_INT_STATUS_3_RMSK                                       0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_3_ADDR, HWIO_MPM2_MPM_INT_STATUS_3_RMSK)
#define HWIO_MPM2_MPM_INT_STATUS_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_STATUS_3_ADDR, m)
#define HWIO_MPM2_MPM_INT_STATUS_3_INT_STATUS_3_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_INT_STATUS_3_INT_STATUS_3_SHFT                                 0x0

#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_ADDR                                 (MPM2_MPM_REG_BASE      + 0x00000114)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_RMSK                                 0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_IN          \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_ADDR, HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_RMSK)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_ADDR, m)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_ADDR,v)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_ADDR,m,v,HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_IN)
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_MPM_INT_WKUP_CLK_SEL_3_BMSK          0xffffffff
#define HWIO_MPM2_MPM_INT_WKUP_CLK_SEL_3_MPM_INT_WKUP_CLK_SEL_3_SHFT                 0x0

#define HWIO_MPM2_MPM_TS_CNTCV_LO_ADDR                                        (MPM2_MPM_REG_BASE      + 0x00000118)
#define HWIO_MPM2_MPM_TS_CNTCV_LO_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_TS_CNTCV_LO_IN          \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTCV_LO_ADDR, HWIO_MPM2_MPM_TS_CNTCV_LO_RMSK)
#define HWIO_MPM2_MPM_TS_CNTCV_LO_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTCV_LO_ADDR, m)
#define HWIO_MPM2_MPM_TS_CNTCV_LO_VALUE_BMSK                                  0xffffffff
#define HWIO_MPM2_MPM_TS_CNTCV_LO_VALUE_SHFT                                         0x0

#define HWIO_MPM2_MPM_TS_CNTCV_HI_ADDR                                        (MPM2_MPM_REG_BASE      + 0x0000011c)
#define HWIO_MPM2_MPM_TS_CNTCV_HI_RMSK                                          0xffffff
#define HWIO_MPM2_MPM_TS_CNTCV_HI_IN          \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTCV_HI_ADDR, HWIO_MPM2_MPM_TS_CNTCV_HI_RMSK)
#define HWIO_MPM2_MPM_TS_CNTCV_HI_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTCV_HI_ADDR, m)
#define HWIO_MPM2_MPM_TS_CNTCV_HI_VALUE_BMSK                                    0xffffff
#define HWIO_MPM2_MPM_TS_CNTCV_HI_VALUE_SHFT                                         0x0

#define HWIO_MPM2_MPM_TS_CNTR_EN_ADDR                                         (MPM2_MPM_REG_BASE      + 0x00000120)
#define HWIO_MPM2_MPM_TS_CNTR_EN_RMSK                                         0x80000000
#define HWIO_MPM2_MPM_TS_CNTR_EN_IN          \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTR_EN_ADDR, HWIO_MPM2_MPM_TS_CNTR_EN_RMSK)
#define HWIO_MPM2_MPM_TS_CNTR_EN_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_TS_CNTR_EN_ADDR, m)
#define HWIO_MPM2_MPM_TS_CNTR_EN_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_TS_CNTR_EN_ADDR,v)
#define HWIO_MPM2_MPM_TS_CNTR_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_TS_CNTR_EN_ADDR,m,v,HWIO_MPM2_MPM_TS_CNTR_EN_IN)
#define HWIO_MPM2_MPM_TS_CNTR_EN_ENABLE_BMSK                                  0x80000000
#define HWIO_MPM2_MPM_TS_CNTR_EN_ENABLE_SHFT                                        0x1f

#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ADDR                                    (MPM2_MPM_REG_BASE      + 0x00000124)
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_RMSK                                           0x1
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_IN          \
        in_dword_masked(HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ADDR, HWIO_MPM2_MPM_VREF_PWRSAVE_EN_RMSK)
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ADDR, m)
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ADDR,v)
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ADDR,m,v,HWIO_MPM2_MPM_VREF_PWRSAVE_EN_IN)
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ENABLE_BMSK                                    0x1
#define HWIO_MPM2_MPM_VREF_PWRSAVE_EN_ENABLE_SHFT                                    0x0

/*----------------------------------------------------------------------------
 * MODULE: MPM2_PSHOLD
 *--------------------------------------------------------------------------*/
#ifndef MPM2_MPM_BASE
#define MPM2_MPM_BASE                                                           0x4A0000
#endif

#define MPM2_PSHOLD_REG_BASE                                                    (MPM2_MPM_BASE      + 0x0000b000)

#define HWIO_MPM2_MPM_PS_HOLD_ADDR                                              (MPM2_PSHOLD_REG_BASE      + 0x00000000)
#define HWIO_MPM2_MPM_PS_HOLD_RMSK                                                     0x1
#define HWIO_MPM2_MPM_PS_HOLD_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR, HWIO_MPM2_MPM_PS_HOLD_RMSK)
#define HWIO_MPM2_MPM_PS_HOLD_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR, m)
#define HWIO_MPM2_MPM_PS_HOLD_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PS_HOLD_ADDR,v)
#define HWIO_MPM2_MPM_PS_HOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PS_HOLD_ADDR,m,v,HWIO_MPM2_MPM_PS_HOLD_IN)
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_BMSK                                              0x1
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_SHFT                                              0x0

#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR                                (MPM2_PSHOLD_REG_BASE      + 0x00000004)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK                                       0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR, HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR, m)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR,v)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR,m,v,HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_BMSK                 0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_SHFT                 0x0

#define HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR                                        (MPM2_PSHOLD_REG_BASE      + 0x00000008)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR, HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR, m)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR,v)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR,m,v,HWIO_MPM2_MPM_SSCAON_CONFIG_IN)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_SHFT                                 0x0

#define HWIO_MPM2_MPM_SSCAON_STATUS_ADDR                                        (MPM2_PSHOLD_REG_BASE      + 0x0000000c)
#define HWIO_MPM2_MPM_SSCAON_STATUS_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR, HWIO_MPM2_MPM_SSCAON_STATUS_RMSK)
#define HWIO_MPM2_MPM_SSCAON_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR, m)
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_SHFT                                 0x0

/*----------------------------------------------------------------------------
 * MODULE: BIMC_GLOBAL2
 *--------------------------------------------------------------------------*/

#define BIMC_GLOBAL2_REG_BASE                                              (BIMC_BASE      + 0x00002000)

#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_ADDR                         (BIMC_GLOBAL2_REG_BASE      + 0x00000000)
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_RMSK                             0xffff
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_IN          \
        in_dword_masked(HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_ADDR, HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_RMSK)
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_INM(m)      \
        in_dword_masked(HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_ADDR, m)
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_SUB_TYPE_BMSK                    0xff00
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_SUB_TYPE_SHFT                       0x8
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_TYPE_BMSK                          0xff
#define HWIO_BIMC_BRIC_GLOBAL2_COMPONENT_INFO_TYPE_SHFT                           0x0

#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_ADDR                                (BIMC_GLOBAL2_REG_BASE      + 0x000001f0)
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_RMSK                                0x800f000f
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_IN          \
        in_dword_masked(HWIO_BIMC_BRIC_DEFAULT_SEGMENT_ADDR, HWIO_BIMC_BRIC_DEFAULT_SEGMENT_RMSK)
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_INM(m)      \
        in_dword_masked(HWIO_BIMC_BRIC_DEFAULT_SEGMENT_ADDR, m)
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_OUT(v)      \
        out_dword(HWIO_BIMC_BRIC_DEFAULT_SEGMENT_ADDR,v)
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_DEFAULT_SEGMENT_ADDR,m,v,HWIO_BIMC_BRIC_DEFAULT_SEGMENT_IN)
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_REDIRECT_EN_BMSK                    0x80000000
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_REDIRECT_EN_SHFT                          0x1f
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_REDIRECT_BMSK                          0xf0000
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_REDIRECT_SHFT                             0x10
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_DEFAULT_BMSK                               0xf
#define HWIO_BIMC_BRIC_DEFAULT_SEGMENT_DEFAULT_SHFT                               0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000200 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_RMSK                     0xfff0040f
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_BASE_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_BASE_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_BASE_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_BASE_10_SHFT                    0xa
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_RFU_BMSK                        0xc
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_RFU_SHFT                        0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_TYPE_BMSK                       0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_TYPE_SHFT                       0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ENABLE_BMSK                     0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER_ENABLE_SHFT                     0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000204 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_BASE_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_A_UPPER_BASE_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000208 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_RMSK                     0xfff00400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_MASK_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_MASK_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_MASK_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER_MASK_10_SHFT                    0xa

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x0000020c + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_MASK_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_A_UPPER_MASK_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000210 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_RMSK                     0xfff0040f
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_BASE_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_BASE_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_BASE_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_BASE_10_SHFT                    0xa
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_RFU_BMSK                        0xc
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_RFU_SHFT                        0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_TYPE_BMSK                       0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_TYPE_SHFT                       0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ENABLE_BMSK                     0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER_ENABLE_SHFT                     0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000214 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_BASE_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_B_UPPER_BASE_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000218 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_RMSK                     0xfff00400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_MASK_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_MASK_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_MASK_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER_MASK_10_SHFT                    0xa

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x0000021c + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_MASK_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_B_UPPER_MASK_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000220 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_RMSK                     0xfff0040f
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_BASE_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_BASE_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_BASE_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_BASE_10_SHFT                    0xa
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_RFU_BMSK                        0xc
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_RFU_SHFT                        0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_TYPE_BMSK                       0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_TYPE_SHFT                       0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ENABLE_BMSK                     0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER_ENABLE_SHFT                     0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000224 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_BASE_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_C_UPPER_BASE_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000228 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_RMSK                     0xfff00400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_MASK_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_MASK_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_MASK_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER_MASK_10_SHFT                    0xa

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x0000022c + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_MASK_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_C_UPPER_MASK_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000230 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_RMSK                     0xfff0040f
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_BASE_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_BASE_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_BASE_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_BASE_10_SHFT                    0xa
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_RFU_BMSK                        0xc
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_RFU_SHFT                        0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_TYPE_BMSK                       0x2
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_TYPE_SHFT                       0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ENABLE_BMSK                     0x1
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_LOWER_ENABLE_SHFT                     0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000234 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_BASE_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_BASE_D_UPPER_BASE_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x00000238 + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_RMSK                     0xfff00400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_MASK_31_20_BMSK          0xfff00000
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_MASK_31_20_SHFT                0x14
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_MASK_10_BMSK                  0x400
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_LOWER_MASK_10_SHFT                    0xa

#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_ADDR(n)                  (BIMC_GLOBAL2_REG_BASE      + 0x0000023c + 0x80 * (n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_RMSK                            0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_MAXn                              5
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_ADDR(n), HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_RMSK)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_INI(n))
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_MASK_35_32_BMSK                 0xf
#define HWIO_BIMC_BRIC_SEGMENTn_ADDR_MASK_D_UPPER_MASK_35_32_SHFT                 0x0

#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ADDR(n)                      (BIMC_GLOBAL2_REG_BASE      + 0x00000a00 + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_RMSK                         0xf000040f
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_MAXn                                  3
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ADDR(n), HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_BASE_31_28_BMSK              0xf0000000
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_BASE_31_28_SHFT                    0x1c
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_BASE_10_BMSK                      0x400
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_BASE_10_SHFT                        0xa
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_RFU_BMSK                            0xe
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_RFU_SHFT                            0x1
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ENABLE_BMSK                         0x1
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_LOWER_ENABLE_SHFT                         0x0

#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_ADDR(n)                      (BIMC_GLOBAL2_REG_BASE      + 0x00000a04 + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_RMSK                                0xf
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_MAXn                                  3
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_ADDR(n), HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_BASE_35_32_BMSK                     0xf
#define HWIO_BIMC_BRIC_REMAPn_ADDR_BASE_UPPER_BASE_35_32_SHFT                     0x0

#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_ADDR(n)                      (BIMC_GLOBAL2_REG_BASE      + 0x00000a08 + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_RMSK                         0xf0000400
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_MAXn                                  3
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_ADDR(n), HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_MASK_31_28_BMSK              0xf0000000
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_MASK_31_28_SHFT                    0x1c
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_MASK_10_BMSK                      0x400
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_LOWER_MASK_10_SHFT                        0xa

#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_ADDR(n)                      (BIMC_GLOBAL2_REG_BASE      + 0x00000a0c + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_RMSK                                0xf
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_MAXn                                  3
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_ADDR(n), HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_MASK_35_32_BMSK                     0xf
#define HWIO_BIMC_BRIC_REMAPn_ADDR_MASK_UPPER_MASK_35_32_SHFT                     0x0

#define HWIO_BIMC_BRIC_REMAPn_OP0_ADDR(n)                                  (BIMC_GLOBAL2_REG_BASE      + 0x00000a20 + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_OP0_RMSK                                       0xff0011
#define HWIO_BIMC_BRIC_REMAPn_OP0_MAXn                                              3
#define HWIO_BIMC_BRIC_REMAPn_OP0_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_OP0_ADDR(n), HWIO_BIMC_BRIC_REMAPn_OP0_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_OP0_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_OP0_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_OP0_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_OP0_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_OP0_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_OP0_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_OP0_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPERAND_BMSK                               0xff0000
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPERAND_SHFT                                   0x10
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPCODE_BMSK                                    0x10
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPCODE_SHFT                                     0x4
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPCODE_ENABLE_BMSK                              0x1
#define HWIO_BIMC_BRIC_REMAPn_OP0_OPCODE_ENABLE_SHFT                              0x0

#define HWIO_BIMC_BRIC_REMAPn_OP1_ADDR(n)                                  (BIMC_GLOBAL2_REG_BASE      + 0x00000a24 + 0x40 * (n))
#define HWIO_BIMC_BRIC_REMAPn_OP1_RMSK                                       0xff0011
#define HWIO_BIMC_BRIC_REMAPn_OP1_MAXn                                              3
#define HWIO_BIMC_BRIC_REMAPn_OP1_INI(n)        \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_OP1_ADDR(n), HWIO_BIMC_BRIC_REMAPn_OP1_RMSK)
#define HWIO_BIMC_BRIC_REMAPn_OP1_INMI(n,mask)    \
        in_dword_masked(HWIO_BIMC_BRIC_REMAPn_OP1_ADDR(n), mask)
#define HWIO_BIMC_BRIC_REMAPn_OP1_OUTI(n,val)    \
        out_dword(HWIO_BIMC_BRIC_REMAPn_OP1_ADDR(n),val)
#define HWIO_BIMC_BRIC_REMAPn_OP1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_BIMC_BRIC_REMAPn_OP1_ADDR(n),mask,val,HWIO_BIMC_BRIC_REMAPn_OP1_INI(n))
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPERAND_BMSK                               0xff0000
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPERAND_SHFT                                   0x10
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPCODE_BMSK                                    0x10
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPCODE_SHFT                                     0x4
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPCODE_ENABLE_BMSK                              0x1
#define HWIO_BIMC_BRIC_REMAPn_OP1_OPCODE_ENABLE_SHFT                              0x0

/*----------------------------------------------------------------------------
 * MODULE: BIMC_S_DDR0_SHKE
 *--------------------------------------------------------------------------*/

#define BIMC_S_DDR0_SHKE_REG_BASE                                                              (BIMC_BASE      + 0x00035000)

#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_ADDR                                                     (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000000)
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_RMSK                                                     0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_HW_INFO_ADDR, HWIO_BIMC_S_DDR0_SHKE_HW_INFO_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_HW_INFO_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_MAJOR_REVISION_BMSK                                      0xff000000
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_MAJOR_REVISION_SHFT                                            0x18
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_BRANCH_REVISION_BMSK                                       0xff0000
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_BRANCH_REVISION_SHFT                                           0x10
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_MINOR_REVISION_BMSK                                          0xff00
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_MINOR_REVISION_SHFT                                             0x8
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_ECO_REVISION_BMSK                                              0xff
#define HWIO_BIMC_S_DDR0_SHKE_HW_INFO_ECO_REVISION_SHFT                                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_ADDR                                                      (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000004)
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RMSK                                                      0x333fe001
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_CONFIG_ADDR, HWIO_BIMC_S_DDR0_SHKE_CONFIG_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_CONFIG_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_CONFIG_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_CONFIG_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_CONFIG_IN)
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK1_EN_BMSK                                             0x20000000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK1_EN_SHFT                                                   0x1d
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK0_EN_BMSK                                             0x10000000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK0_EN_SHFT                                                   0x1c
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK1_INITCOMPLETE_BMSK                                    0x2000000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK1_INITCOMPLETE_SHFT                                         0x19
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK0_INITCOMPLETE_BMSK                                    0x1000000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_RANK0_INITCOMPLETE_SHFT                                         0x18
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_NUM_BANKS_BMSK                                              0x300000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_NUM_BANKS_SHFT                                                  0x14
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_BMSK                                0xf0000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_SHFT                                   0x10
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_MULT_BMSK                            0xe000
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_MULT_SHFT                               0xd
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_CLK_ON_EN_BMSK                                                   0x1
#define HWIO_BIMC_S_DDR0_SHKE_CONFIG_CLK_ON_EN_SHFT                                                   0x0

#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_ADDR                                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000010)
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_RMSK                                                           0x1
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_CSR_ACCESS_BMSK                                                0x1
#define HWIO_BIMC_S_DDR0_SHKE_CGC_CNTL_CSR_ACCESS_SHFT                                                0x0

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ADDR                                               (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000020)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RMSK                                               0x3ffffbfe
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RANK_SEL_BMSK                                      0x30000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RANK_SEL_SHFT                                            0x1c
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CK_ON_BMSK                                          0x8000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CK_ON_SHFT                                               0x1b
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CKE_ON_BMSK                                         0x4000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CKE_ON_SHFT                                              0x1a
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESETN_ON_BMSK                                      0x2000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESETN_ON_SHFT                                           0x19
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CK_OFF_BMSK                                         0x1000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CK_OFF_SHFT                                              0x18
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CKE_OFF_BMSK                                         0x800000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CKE_OFF_SHFT                                             0x17
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESETN_OFF_BMSK                                      0x400000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESETN_OFF_SHFT                                          0x16
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_DEEP_PD_BMSK                                   0x200000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_DEEP_PD_SHFT                                       0x15
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXIT_DEEP_PD_BMSK                                    0x100000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXIT_DEEP_PD_SHFT                                        0x14
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_SHORT_BMSK                                      0x80000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_SHORT_SHFT                                         0x13
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_LONG_BMSK                                       0x40000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_LONG_SHFT                                          0x12
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_AUTO_REFRESH_BMSK                                     0x20000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_AUTO_REFRESH_SHFT                                        0x11
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_PRECHARGE_ALL_BMSK                                    0x10000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_PRECHARGE_ALL_SHFT                                       0x10
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_IDLE_BMSK                           0x8000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_IDLE_SHFT                              0xf
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_ASAP_BMSK                           0x4000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_ASAP_SHFT                              0xe
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXIT_SELF_REFRESH_BMSK                                 0x2000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXIT_SELF_REFRESH_SHFT                                    0xd
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_MODE_REGISTER_READ_BMSK                                0x1000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_MODE_REGISTER_READ_SHFT                                   0xc
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_MODE_REGISTER_WRITE_BMSK                                0x800
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_MODE_REGISTER_WRITE_SHFT                                  0xb
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_DRAM_DEBUG_CMD_BMSK                                     0x200
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_DRAM_DEBUG_CMD_SHFT                                       0x9
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_STALL_CMDS_BMSK                                         0x100
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_STALL_CMDS_SHFT                                           0x8
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESUME_CMDS_BMSK                                         0x80
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RESUME_CMDS_SHFT                                          0x7
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXTND_MODE_REGISTER_WRITE_BMSK                           0x40
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_EXTND_MODE_REGISTER_WRITE_SHFT                            0x6
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CA_TRAIN_PATTERN_CMD_BMSK                                0x20
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_CA_TRAIN_PATTERN_CMD_SHFT                                 0x5
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RD_DQCAL_BMSK                                            0x10
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_RD_DQCAL_SHFT                                             0x4
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_START_BMSK                                          0x8
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_START_SHFT                                          0x3
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_LATCH_BMSK                                          0x4
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_ZQCAL_LATCH_SHFT                                          0x2
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_VREF_SETUP_BMSK                                           0x2
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_0_VREF_SETUP_SHFT                                           0x1

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_ADDR                                               (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000024)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_RMSK                                                  0x1ffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_WAIT_TIMER_DOMAIN_BMSK                                0x10000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_WAIT_TIMER_DOMAIN_SHFT                                   0x10
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_WAIT_TIMER_BEFORE_HW_CLEAR_BMSK                        0xffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_MANUAL_1_WAIT_TIMER_BEFORE_HW_CLEAR_SHFT                           0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_ADDR                                        (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000030)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_RMSK                                         0xff0ffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_MREG_ADDR_BMSK                               0xff00000
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_MREG_ADDR_SHFT                                    0x14
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_MREG_WDATA_BMSK                                 0xffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_ADDR_WDATA_CNTL_MREG_WDATA_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_ADDR                                          (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000050)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_U_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_ADDR                                          (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000054)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK1_L_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_ADDR                                          (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000060)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_U_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_ADDR                                          (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000064)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_RANK0_L_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_ADDR                                           (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000070)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_RMSK                                                  0x1
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_ADDR, HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_MREG_TIMEOUT_BMSK                                     0x1
#define HWIO_BIMC_S_DDR0_SHKE_MREG_RDATA_STATUS_MREG_TIMEOUT_SHFT                                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_ADDR                                                 (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000074)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_RMSK                                                        0x7
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_WDOG_SELF_RFSH_BMSK                                         0x4
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_WDOG_SELF_RFSH_SHFT                                         0x2
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_SW_SELF_RFSH_BMSK                                           0x2
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_SW_SELF_RFSH_SHFT                                           0x1
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_STALL_BMSK                                                  0x1
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_STATUS_STALL_SHFT                                                  0x0

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000080)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_RMSK                                                0xffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_DDR_CA_EVEN_BMSK                                    0xffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_0_DDR_CA_EVEN_SHFT                                       0x0

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000084)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_RMSK                                                0xffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_DDR_CA_ODD_BMSK                                     0xffff
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_1_DDR_CA_ODD_SHFT                                        0x0

#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000088)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_RMSK                                            0x3ff00000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_ADDR, HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CS_N_BMSK                                   0x30000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CS_N_SHFT                                         0x1c
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_RAS_N_BMSK                                   0x8000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_RAS_N_SHFT                                        0x1b
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CAS_N_BMSK                                   0x4000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CAS_N_SHFT                                        0x1a
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_WE_N_BMSK                                    0x2000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_WE_N_SHFT                                         0x19
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CKE_BMSK                                     0x1000000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_CKE_SHFT                                          0x18
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_BA_BMSK                                       0xf00000
#define HWIO_BIMC_S_DDR0_SHKE_DRAM_DEBUG_CMD_2_DDR_BA_SHFT                                           0x14

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_ADDR                                                (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000a0)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RMSK                                                 0xff3ffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_IN)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_ADDR_BMSK                                        0xff00000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_ADDR_SHFT                                             0x14
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_RANK_SEL_BMSK                                      0x30000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_RANK_SEL_SHFT                                         0x10
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_INTERVAL_BMSK                                       0xffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_MRR_INTERVAL_SHFT                                          0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR                                  (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000b0)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_U_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR                                  (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000b4)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK1_L_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR                                  (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000b8)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_U_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR                                  (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000bc)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_MRR_RDATA_RANK0_L_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000c0)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_RMSK                                                   0x7
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_ADDR, HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_DERATE_REF_VALUE_BMSK                                  0x7
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK0_DERATE_REF_VALUE_SHFT                                  0x0

#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000c4)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_RMSK                                                   0x7
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_ADDR, HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_DERATE_REF_VALUE_BMSK                                  0x7
#define HWIO_BIMC_S_DDR0_SHKE_DERATE_REF_RANK1_DERATE_REF_VALUE_SHFT                                  0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ADDR                                              (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000d0)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_RMSK                                              0x3000ffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_RANK_SEL_BMSK                                     0x30000000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_RANK_SEL_SHFT                                           0x1c
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ZQCAL_INTERVAL_BMSK                                   0xffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_ZQCAL_INTERVAL_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ADDR                                            (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000d4)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_RMSK                                             0x703ffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ADDR, HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_LATCH_STAGGER_BMSK                            0x4000000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_LATCH_STAGGER_SHFT                                 0x1a
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_START_STAGGER_BMSK                            0x2000000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_START_STAGGER_SHFT                                 0x19
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_TIME_SLICE_MODE_BMSK                          0x1000000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQ_TIME_SLICE_MODE_SHFT                               0x18
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQCAL_MODE_BMSK                                    0x30000
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQCAL_MODE_SHFT                                       0x10
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQCAL_INTERVAL_BMSK                                 0xffff
#define HWIO_BIMC_S_DDR0_SHKE_PERIODIC_ZQCAL_1_ZQCAL_INTERVAL_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_ADDR                                           (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000f0)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_RMSK                                           0x33c103ff
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK1_BMSK                    0x20000000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK1_SHFT                          0x1d
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK0_BMSK                    0x10000000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK0_SHFT                          0x1c
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_REFRESH_MODE_BMSK                          0x3000000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_REFRESH_MODE_SHFT                               0x18
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_SWITCH_ALL_BANK_MODE_BMSK                   0xc00000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_AUTO_SWITCH_ALL_BANK_MODE_SHFT                       0x16
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_DISABLE_AUTO_REFRESH_TEMP_UPDATE_BMSK             0x10000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_DISABLE_AUTO_REFRESH_TEMP_UPDATE_SHFT                0x10
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_TREFI_BMSK                                          0x3ff
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_TREFI_SHFT                                            0x0

#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_ADDR                                           (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000f4)
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_RMSK                                           0x30fff100
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK1_BMSK                 0x20000000
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK1_SHFT                       0x1d
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK0_BMSK                 0x10000000
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK0_SHFT                       0x1c
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_IDLE_TIMER_BMSK                     0xfff000
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_IDLE_TIMER_SHFT                          0xc
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_CONCURRENT_SELF_RFSH_EN_BMSK                        0x100
#define HWIO_BIMC_S_DDR0_SHKE_SELF_REFRESH_CNTL_CONCURRENT_SELF_RFSH_EN_SHFT                          0x8

#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_ADDR                                         (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000100)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_RMSK                                              0x3ff
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_ADDR, HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_TREFI_BMSK                                        0x3ff
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_1_TREFI_SHFT                                          0x0

#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_ADDR                                         (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000104)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_RMSK                                         0xffff0001
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_ADDR, HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_IN)
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_RFSH_UPDATE_TIMER_BMSK                       0xffff0000
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_RFSH_UPDATE_TIMER_SHFT                             0x10
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_DLY_RFSH_CONDN_BMSK                                 0x1
#define HWIO_BIMC_S_DDR0_SHKE_AUTO_REFRESH_CNTL_2_DLY_RFSH_CONDN_SHFT                                 0x0

#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_ADDR                                         (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000d8)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_RMSK                                         0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_TRIGGER_BMSK                  0xffff0000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_TRIGGER_SHFT                        0x10
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_READ_BMSK                         0xffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_READ_SHFT                            0x0

#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_ADDR                                       (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000dc)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_RMSK                                       0x1111ff03
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_ADDR, HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_IN)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_PERIODIC_MPC_DTTS_BYPASS_EN_BMSK           0x10000000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_PERIODIC_MPC_DTTS_BYPASS_EN_SHFT                 0x1c
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_INTERVAL_TIMER_BMSK                         0x1000000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_INTERVAL_TIMER_SHFT                              0x18
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_THRESHOLD_COMPARE_BMSK                       0x100000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_THRESHOLD_COMPARE_SHFT                           0x14
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_BYPASS_MPC_STOP_BMSK                          0x10000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_BYPASS_MPC_STOP_SHFT                             0x10
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_THERSHOLD_BMSK                                 0xff00
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_THERSHOLD_SHFT                                    0x8
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_RANK_EN_BMSK                                      0x3
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_1_RANK_EN_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_ADDR                                       (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000001d0)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_ADDR, HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_IN)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK1_BMSK                   0xffff0000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK1_SHFT                         0x10
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK0_BMSK                       0xffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK0_SHFT                          0x0

#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_ADDR                                       (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000001d4)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_ADDR, HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_IN)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHB_BMSK              0xffff0000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHB_SHFT                    0x10
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHA_BMSK                  0xffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHA_SHFT                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_ADDR                                       (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000001d8)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_ADDR, HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_IN)
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHB_BMSK              0xffff0000
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHB_SHFT                    0x10
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHA_BMSK                  0xffff
#define HWIO_BIMC_S_DDR0_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHA_SHFT                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_ADDR                                          (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000fc)
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_RMSK                                            0x1fffff
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_PULSE_WIDTH_BMSK                           0x1f0000
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_PULSE_WIDTH_SHFT                               0x10
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_B_BMSK                               0xff00
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_B_SHFT                                  0x8
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_A_BMSK                                 0xff
#define HWIO_BIMC_S_DDR0_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_A_SHFT                                  0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ADDR                                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000e0)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_RMSK                                                         0x371
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ISSUE_TO_RANK_BMSK                                           0x300
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_ISSUE_TO_RANK_SHFT                                             0x8
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_COMMAND_OPCODE_BMSK                                           0x70
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_COMMAND_OPCODE_SHFT                                            0x4
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_MPC_START_BMSK                                                 0x1
#define HWIO_BIMC_S_DDR0_SHKE_MPC_CNTL_MPC_START_SHFT                                                 0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_ADDR                                              (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000e4)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_MPC_DATA_PE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_PE_MPC_DATA_PE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_ADDR                                              (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000000e8)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_MPC_DATA_NE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_WR_DATA_NE_MPC_DATA_NE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_ADDR                                              (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000001f4)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_MPC_DATA_PE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_PE_MPC_DATA_PE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_ADDR                                              (BIMC_S_DDR0_SHKE_REG_BASE      + 0x000001f8)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_MPC_DATA_NE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_RD_DATA_NE_MPC_DATA_NE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_ADDR                                                (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000114)
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_RMSK                                                      0xf1
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_ADDR, HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_TESTBUS_SEL_BMSK                                          0xf0
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_TESTBUS_SEL_SHFT                                           0x4
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_TESTBUS_EN_BMSK                                            0x1
#define HWIO_BIMC_S_DDR0_SHKE_TESTBUS_CNTL_TESTBUS_EN_SHFT                                            0x0

#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_ADDR                                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000130)
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_RMSK                                                   0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_ADDR, HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_IN)
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_SPARE_BITS_BMSK                                        0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_SPARE_REG_SPARE_BITS_SHFT                                               0x0

#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_ADDR                                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000134)
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_RMSK                                                          0x1
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_ADDR, HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_SAFE_EN_BMSK                                                  0x1
#define HWIO_BIMC_S_DDR0_SHKE_SAFE_CTRL_SAFE_EN_SHFT                                                  0x0

#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_ADDR                                                  (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000140)
#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_RMSK                                                  0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_ADDR, HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_RDATA_BMSK                                            0xffffffff
#define HWIO_BIMC_S_DDR0_SHKE_DEBUG_DATA_RDATA_SHFT                                                   0x0

#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR                                       (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000144)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_RMSK                                             0xff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR, HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_IN)
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_MPC_STOP_TO_MRR_BMSK                             0xff
#define HWIO_BIMC_S_DDR0_SHKE_MPC_STOP_TO_MRR_TIMER_MPC_STOP_TO_MRR_SHFT                              0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR                                         (BIMC_S_DDR0_SHKE_REG_BASE      + 0x0000014c)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_RMSK                                                0x3
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_SYNC_MODE_EN_BMSK                                   0x3
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_MODE_CTRL_SYNC_MODE_EN_SHFT                                   0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000150)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR                                     (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000154)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000158)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000160)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR                                     (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000164)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000168)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000170)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR                                     (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000174)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000178)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR                                   (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000180)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR                                     (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000184)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR                                    (BIMC_S_DDR0_SHKE_REG_BASE      + 0x00000188)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR, HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RMSK)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR,m,v,HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_IN)
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR0_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RANGE_SHFT                                     0x0

/*----------------------------------------------------------------------------
 * MODULE: BIMC_S_DDR1_SHKE
 *--------------------------------------------------------------------------*/

#define BIMC_S_DDR1_SHKE_REG_BASE                                                              (BIMC_BASE      + 0x00041000)

#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_ADDR                                                     (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000000)
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_RMSK                                                     0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_HW_INFO_ADDR, HWIO_BIMC_S_DDR1_SHKE_HW_INFO_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_HW_INFO_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_MAJOR_REVISION_BMSK                                      0xff000000
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_MAJOR_REVISION_SHFT                                            0x18
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_BRANCH_REVISION_BMSK                                       0xff0000
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_BRANCH_REVISION_SHFT                                           0x10
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_MINOR_REVISION_BMSK                                          0xff00
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_MINOR_REVISION_SHFT                                             0x8
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_ECO_REVISION_BMSK                                              0xff
#define HWIO_BIMC_S_DDR1_SHKE_HW_INFO_ECO_REVISION_SHFT                                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_ADDR                                                      (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000004)
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RMSK                                                      0x333fe001
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_CONFIG_ADDR, HWIO_BIMC_S_DDR1_SHKE_CONFIG_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_CONFIG_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_CONFIG_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_CONFIG_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_CONFIG_IN)
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK1_EN_BMSK                                             0x20000000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK1_EN_SHFT                                                   0x1d
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK0_EN_BMSK                                             0x10000000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK0_EN_SHFT                                                   0x1c
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK1_INITCOMPLETE_BMSK                                    0x2000000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK1_INITCOMPLETE_SHFT                                         0x19
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK0_INITCOMPLETE_BMSK                                    0x1000000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_RANK0_INITCOMPLETE_SHFT                                         0x18
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_NUM_BANKS_BMSK                                              0x300000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_NUM_BANKS_SHFT                                                  0x14
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_BMSK                                0xf0000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_SHFT                                   0x10
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_MULT_BMSK                            0xe000
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_IOCAL_UPDATE_PULSE_WIDTH_MULT_SHFT                               0xd
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_CLK_ON_EN_BMSK                                                   0x1
#define HWIO_BIMC_S_DDR1_SHKE_CONFIG_CLK_ON_EN_SHFT                                                   0x0

#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_ADDR                                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000010)
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_RMSK                                                           0x1
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_CSR_ACCESS_BMSK                                                0x1
#define HWIO_BIMC_S_DDR1_SHKE_CGC_CNTL_CSR_ACCESS_SHFT                                                0x0

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ADDR                                               (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000020)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RMSK                                               0x3ffffbfe
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RANK_SEL_BMSK                                      0x30000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RANK_SEL_SHFT                                            0x1c
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CK_ON_BMSK                                          0x8000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CK_ON_SHFT                                               0x1b
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CKE_ON_BMSK                                         0x4000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CKE_ON_SHFT                                              0x1a
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESETN_ON_BMSK                                      0x2000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESETN_ON_SHFT                                           0x19
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CK_OFF_BMSK                                         0x1000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CK_OFF_SHFT                                              0x18
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CKE_OFF_BMSK                                         0x800000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CKE_OFF_SHFT                                             0x17
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESETN_OFF_BMSK                                      0x400000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESETN_OFF_SHFT                                          0x16
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_DEEP_PD_BMSK                                   0x200000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_DEEP_PD_SHFT                                       0x15
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXIT_DEEP_PD_BMSK                                    0x100000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXIT_DEEP_PD_SHFT                                        0x14
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_SHORT_BMSK                                      0x80000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_SHORT_SHFT                                         0x13
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_LONG_BMSK                                       0x40000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_LONG_SHFT                                          0x12
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_AUTO_REFRESH_BMSK                                     0x20000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_AUTO_REFRESH_SHFT                                        0x11
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_PRECHARGE_ALL_BMSK                                    0x10000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_PRECHARGE_ALL_SHFT                                       0x10
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_IDLE_BMSK                           0x8000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_IDLE_SHFT                              0xf
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_ASAP_BMSK                           0x4000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ENTER_SELF_REFRESH_ASAP_SHFT                              0xe
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXIT_SELF_REFRESH_BMSK                                 0x2000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXIT_SELF_REFRESH_SHFT                                    0xd
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_MODE_REGISTER_READ_BMSK                                0x1000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_MODE_REGISTER_READ_SHFT                                   0xc
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_MODE_REGISTER_WRITE_BMSK                                0x800
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_MODE_REGISTER_WRITE_SHFT                                  0xb
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_DRAM_DEBUG_CMD_BMSK                                     0x200
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_DRAM_DEBUG_CMD_SHFT                                       0x9
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_STALL_CMDS_BMSK                                         0x100
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_STALL_CMDS_SHFT                                           0x8
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESUME_CMDS_BMSK                                         0x80
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RESUME_CMDS_SHFT                                          0x7
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXTND_MODE_REGISTER_WRITE_BMSK                           0x40
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_EXTND_MODE_REGISTER_WRITE_SHFT                            0x6
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CA_TRAIN_PATTERN_CMD_BMSK                                0x20
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_CA_TRAIN_PATTERN_CMD_SHFT                                 0x5
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RD_DQCAL_BMSK                                            0x10
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_RD_DQCAL_SHFT                                             0x4
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_START_BMSK                                          0x8
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_START_SHFT                                          0x3
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_LATCH_BMSK                                          0x4
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_ZQCAL_LATCH_SHFT                                          0x2
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_VREF_SETUP_BMSK                                           0x2
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_0_VREF_SETUP_SHFT                                           0x1

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_ADDR                                               (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000024)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_RMSK                                                  0x1ffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_WAIT_TIMER_DOMAIN_BMSK                                0x10000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_WAIT_TIMER_DOMAIN_SHFT                                   0x10
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_WAIT_TIMER_BEFORE_HW_CLEAR_BMSK                        0xffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_MANUAL_1_WAIT_TIMER_BEFORE_HW_CLEAR_SHFT                           0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_ADDR                                        (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000030)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_RMSK                                         0xff0ffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_MREG_ADDR_BMSK                               0xff00000
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_MREG_ADDR_SHFT                                    0x14
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_MREG_WDATA_BMSK                                 0xffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_ADDR_WDATA_CNTL_MREG_WDATA_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_ADDR                                          (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000050)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_U_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_ADDR                                          (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000054)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK1_L_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_ADDR                                          (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000060)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_U_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_ADDR                                          (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000064)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_RMSK                                          0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_MREG_RDATA_BMSK                               0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_RANK0_L_MREG_RDATA_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_ADDR                                           (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000070)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_RMSK                                                  0x1
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_ADDR, HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_MREG_TIMEOUT_BMSK                                     0x1
#define HWIO_BIMC_S_DDR1_SHKE_MREG_RDATA_STATUS_MREG_TIMEOUT_SHFT                                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_ADDR                                                 (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000074)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_RMSK                                                        0x7
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_WDOG_SELF_RFSH_BMSK                                         0x4
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_WDOG_SELF_RFSH_SHFT                                         0x2
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_SW_SELF_RFSH_BMSK                                           0x2
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_SW_SELF_RFSH_SHFT                                           0x1
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_STALL_BMSK                                                  0x1
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_STATUS_STALL_SHFT                                                  0x0

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000080)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_RMSK                                                0xffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_DDR_CA_EVEN_BMSK                                    0xffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_0_DDR_CA_EVEN_SHFT                                       0x0

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000084)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_RMSK                                                0xffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_DDR_CA_ODD_BMSK                                     0xffff
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_1_DDR_CA_ODD_SHFT                                        0x0

#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000088)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_RMSK                                            0x3ff00000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_ADDR, HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CS_N_BMSK                                   0x30000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CS_N_SHFT                                         0x1c
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_RAS_N_BMSK                                   0x8000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_RAS_N_SHFT                                        0x1b
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CAS_N_BMSK                                   0x4000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CAS_N_SHFT                                        0x1a
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_WE_N_BMSK                                    0x2000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_WE_N_SHFT                                         0x19
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CKE_BMSK                                     0x1000000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_CKE_SHFT                                          0x18
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_BA_BMSK                                       0xf00000
#define HWIO_BIMC_S_DDR1_SHKE_DRAM_DEBUG_CMD_2_DDR_BA_SHFT                                           0x14

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_ADDR                                                (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000a0)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RMSK                                                 0xff3ffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_IN)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_ADDR_BMSK                                        0xff00000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_ADDR_SHFT                                             0x14
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_RANK_SEL_BMSK                                      0x30000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_RANK_SEL_SHFT                                         0x10
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_INTERVAL_BMSK                                       0xffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_MRR_INTERVAL_SHFT                                          0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR                                  (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000b0)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_U_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR                                  (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000b4)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK1_L_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR                                  (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000b8)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_U_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR                                  (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000bc)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_RMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_MREG_DATA_BMSK                        0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_MRR_RDATA_RANK0_L_MREG_DATA_SHFT                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000c0)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_RMSK                                                   0x7
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_ADDR, HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_DERATE_REF_VALUE_BMSK                                  0x7
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK0_DERATE_REF_VALUE_SHFT                                  0x0

#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000c4)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_RMSK                                                   0x7
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_ADDR, HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_DERATE_REF_VALUE_BMSK                                  0x7
#define HWIO_BIMC_S_DDR1_SHKE_DERATE_REF_RANK1_DERATE_REF_VALUE_SHFT                                  0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ADDR                                              (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000d0)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_RMSK                                              0x3000ffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_RANK_SEL_BMSK                                     0x30000000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_RANK_SEL_SHFT                                           0x1c
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ZQCAL_INTERVAL_BMSK                                   0xffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_ZQCAL_INTERVAL_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ADDR                                            (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000d4)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_RMSK                                             0x703ffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ADDR, HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_LATCH_STAGGER_BMSK                            0x4000000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_LATCH_STAGGER_SHFT                                 0x1a
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_START_STAGGER_BMSK                            0x2000000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_START_STAGGER_SHFT                                 0x19
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_TIME_SLICE_MODE_BMSK                          0x1000000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQ_TIME_SLICE_MODE_SHFT                               0x18
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQCAL_MODE_BMSK                                    0x30000
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQCAL_MODE_SHFT                                       0x10
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQCAL_INTERVAL_BMSK                                 0xffff
#define HWIO_BIMC_S_DDR1_SHKE_PERIODIC_ZQCAL_1_ZQCAL_INTERVAL_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_ADDR                                           (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000f0)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_RMSK                                           0x33c103ff
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK1_BMSK                    0x20000000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK1_SHFT                          0x1d
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK0_BMSK                    0x10000000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_RFSH_ENABLE_RANK0_SHFT                          0x1c
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_REFRESH_MODE_BMSK                          0x3000000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_REFRESH_MODE_SHFT                               0x18
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_SWITCH_ALL_BANK_MODE_BMSK                   0xc00000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_AUTO_SWITCH_ALL_BANK_MODE_SHFT                       0x16
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_DISABLE_AUTO_REFRESH_TEMP_UPDATE_BMSK             0x10000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_DISABLE_AUTO_REFRESH_TEMP_UPDATE_SHFT                0x10
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_TREFI_BMSK                                          0x3ff
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_TREFI_SHFT                                            0x0

#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_ADDR                                           (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000f4)
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_RMSK                                           0x30fff100
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK1_BMSK                 0x20000000
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK1_SHFT                       0x1d
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK0_BMSK                 0x10000000
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_ENABLE_RANK0_SHFT                       0x1c
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_IDLE_TIMER_BMSK                     0xfff000
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_HW_SELF_RFSH_IDLE_TIMER_SHFT                          0xc
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_CONCURRENT_SELF_RFSH_EN_BMSK                        0x100
#define HWIO_BIMC_S_DDR1_SHKE_SELF_REFRESH_CNTL_CONCURRENT_SELF_RFSH_EN_SHFT                          0x8

#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_ADDR                                         (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000100)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_RMSK                                              0x3ff
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_ADDR, HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_TREFI_BMSK                                        0x3ff
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_1_TREFI_SHFT                                          0x0

#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_ADDR                                         (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000104)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_RMSK                                         0xffff0001
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_ADDR, HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_IN)
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_RFSH_UPDATE_TIMER_BMSK                       0xffff0000
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_RFSH_UPDATE_TIMER_SHFT                             0x10
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_DLY_RFSH_CONDN_BMSK                                 0x1
#define HWIO_BIMC_S_DDR1_SHKE_AUTO_REFRESH_CNTL_2_DLY_RFSH_CONDN_SHFT                                 0x0

#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_ADDR                                         (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000d8)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_RMSK                                         0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_TRIGGER_BMSK                  0xffff0000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_TRIGGER_SHFT                        0x10
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_READ_BMSK                         0xffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_INTERVAL_TIMER_READ_SHFT                            0x0

#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_ADDR                                       (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000dc)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_RMSK                                       0x1111ff03
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_ADDR, HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_IN)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_PERIODIC_MPC_DTTS_BYPASS_EN_BMSK           0x10000000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_PERIODIC_MPC_DTTS_BYPASS_EN_SHFT                 0x1c
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_INTERVAL_TIMER_BMSK                         0x1000000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_INTERVAL_TIMER_SHFT                              0x18
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_THRESHOLD_COMPARE_BMSK                       0x100000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_THRESHOLD_COMPARE_SHFT                           0x14
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_BYPASS_MPC_STOP_BMSK                          0x10000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_BYPASS_MPC_STOP_SHFT                             0x10
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_THERSHOLD_BMSK                                 0xff00
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_THERSHOLD_SHFT                                    0x8
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_RANK_EN_BMSK                                      0x3
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_1_RANK_EN_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_ADDR                                       (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000001d0)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_ADDR, HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_IN)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK1_BMSK                   0xffff0000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK1_SHFT                         0x10
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK0_BMSK                       0xffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_2_COMPARE_VALUE_RANK0_SHFT                          0x0

#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_ADDR                                       (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000001d4)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_ADDR, HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_IN)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHB_BMSK              0xffff0000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHB_SHFT                    0x10
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHA_BMSK                  0xffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_3_RECEIVED_VALUE_RANK0_CHA_SHFT                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_ADDR                                       (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000001d8)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RMSK                                       0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_ADDR, HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_IN)
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHB_BMSK              0xffff0000
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHB_SHFT                    0x10
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHA_BMSK                  0xffff
#define HWIO_BIMC_S_DDR1_SHKE_INTERVAL_TIMER_CNTL_4_RECEIVED_VALUE_RANK1_CHA_SHFT                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_ADDR                                          (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000fc)
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_RMSK                                            0x1fffff
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_PULSE_WIDTH_BMSK                           0x1f0000
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_PULSE_WIDTH_SHFT                               0x10
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_B_BMSK                               0xff00
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_B_SHFT                                  0x8
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_A_BMSK                                 0xff
#define HWIO_BIMC_S_DDR1_SHKE_VREF_TRAINING_CNTL_VREF_DATA_CH_A_SHFT                                  0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ADDR                                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000e0)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_RMSK                                                         0x371
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ISSUE_TO_RANK_BMSK                                           0x300
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_ISSUE_TO_RANK_SHFT                                             0x8
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_COMMAND_OPCODE_BMSK                                           0x70
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_COMMAND_OPCODE_SHFT                                            0x4
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_MPC_START_BMSK                                                 0x1
#define HWIO_BIMC_S_DDR1_SHKE_MPC_CNTL_MPC_START_SHFT                                                 0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_ADDR                                              (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000e4)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_MPC_DATA_PE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_PE_MPC_DATA_PE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_ADDR                                              (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000000e8)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_MPC_DATA_NE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_WR_DATA_NE_MPC_DATA_NE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_ADDR                                              (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000001f4)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_MPC_DATA_PE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_PE_MPC_DATA_PE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_ADDR                                              (BIMC_S_DDR1_SHKE_REG_BASE      + 0x000001f8)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_RMSK                                              0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_MPC_DATA_NE_BMSK                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_RD_DATA_NE_MPC_DATA_NE_SHFT                                         0x0

#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_ADDR                                                (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000114)
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_RMSK                                                      0xf1
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_ADDR, HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_TESTBUS_SEL_BMSK                                          0xf0
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_TESTBUS_SEL_SHFT                                           0x4
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_TESTBUS_EN_BMSK                                            0x1
#define HWIO_BIMC_S_DDR1_SHKE_TESTBUS_CNTL_TESTBUS_EN_SHFT                                            0x0

#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_ADDR                                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000130)
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_RMSK                                                   0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_ADDR, HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_IN)
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_SPARE_BITS_BMSK                                        0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_SPARE_REG_SPARE_BITS_SHFT                                               0x0

#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_ADDR                                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000134)
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_RMSK                                                          0x1
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_ADDR, HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_SAFE_EN_BMSK                                                  0x1
#define HWIO_BIMC_S_DDR1_SHKE_SAFE_CTRL_SAFE_EN_SHFT                                                  0x0

#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_ADDR                                                  (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000140)
#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_RMSK                                                  0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_ADDR, HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_RDATA_BMSK                                            0xffffffff
#define HWIO_BIMC_S_DDR1_SHKE_DEBUG_DATA_RDATA_SHFT                                                   0x0

#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR                                       (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000144)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_RMSK                                             0xff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR, HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_IN)
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_MPC_STOP_TO_MRR_BMSK                             0xff
#define HWIO_BIMC_S_DDR1_SHKE_MPC_STOP_TO_MRR_TIMER_MPC_STOP_TO_MRR_SHFT                              0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR                                         (BIMC_S_DDR1_SHKE_REG_BASE      + 0x0000014c)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_RMSK                                                0x3
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_SYNC_MODE_EN_BMSK                                   0x3
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_MODE_CTRL_SYNC_MODE_EN_SHFT                                   0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000150)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR                                     (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000154)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000158)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQS_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000160)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR                                     (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000164)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000168)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQS_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000170)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR                                     (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000174)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000178)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK0_ZQL_MASK_RANGE_SHFT                                     0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR                                   (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000180)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RMSK                                     0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RANGE_BMSK                               0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_START_RANGE_SHFT                                    0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR                                     (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000184)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RMSK                                       0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RANGE_BMSK                                 0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_END_RANGE_SHFT                                      0x0

#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR                                    (BIMC_S_DDR1_SHKE_REG_BASE      + 0x00000188)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RMSK                                      0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_IN          \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR, HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RMSK)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_INM(m)      \
        in_dword_masked(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR, m)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_OUT(v)      \
        out_dword(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR,v)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_ADDR,m,v,HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_IN)
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RANGE_BMSK                                0x3fffff
#define HWIO_BIMC_S_DDR1_SHKE_ZQ_SYNCED_RANK1_ZQL_MASK_RANGE_SHFT                                     0x0
#endif /* __DDR_HWIO__ */