/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             Boot Dload Debug Target File

GENERAL DESCRIPTION
  This module defines the target specific memory debug information to be sent
  to host.
  Please make sure that dump region file name follows the 8.3 file name format!

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright 2011 - 2016 by QUALCOMM Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*===========================================================================

                           EDIT HISTORY FOR FILE
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/27/16   kpa     Support taking 6GB DDR ramdumps
07/09/15   kpa     Added check to get device number to decide ipa region dump addition
07/09/15   kpa     Add IPA memory region dumps for chip version v2 onwards.
06/25/15   kpa     Allow DDR CS1 address range to be lesser than CS0 for ramdumps.
06/25/15   as      Updated dload_mem_debug_supported
05/19/15   kpa     Add pImem region dumps runtime if pimem is enabled.
02/27/15   kpa     Remove marking rpm message ram as restricted memory.
02/07/15   kpa     Update dload_mem_debug_target_init to include DDR CS1 when its non-contiguous
10/29/15   kpa     set mem_dump_supported to true
10/29/14   kpa     Support 64 bit memory map
09/23/14   ck      Moved target specific functions dload_mem_debug_is_device_retail_unlocked
                   and dload_mem_debug_supported from boot_dload_debug.
08/08/14   ck      Refactored Data ZI region attributes for GCC compiler
07/21/14   ck      Moved from Loader to RamDump
05/07/14   kedara  Fix compilation warnings.
04/29/14   ck      Adding zero_init attribute on small variables that must be in ZI region
04/02/14   ck      Added RPM MSG RAM as a restricted region
03/17/14   ck      Added call to dload_mem_debug_zero_peripheral_memory in retail unlocked scenario
01/10/14   ck      Added support for retail unlock dumps
06/13/13   dh      Add dload_add_ddr_training_data to dump ddr training data
06/06/13   dh      Call sbl1_hw_get_reset_status to read the backed up copy of
                   gcc reset status

02/11/13   dh      Move dload_mem_debug_supported to common code
01/28/13   dh      Add DDR as the last region in memory debug table
09/20/12   dh      Add Pmic power on reason and reset status to ram dump
07/17/12   dh      Remove reset status region
05/23/11   dh      call dload_mem_debug_len_init in dload_mem_debug_target_init to
                   set memory region table to be fixed length, required by sahara ram dump
04/05/11   dh      do not include customer.h
09/29/11   dh      Initial revision

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "boot_dload_debug.h"
#include "boot_ddr_info.h"
#include "boot_target.h"
#include "boot_shared_imem_cookie.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_seccfg_interface.h"
#include "boot_sbl_qsee_interface.h"
#include "boot_msm.h"
#include "boot_extern_pimem_interface.h"
#include "boot_logger.h"
#include "sbl1_hw.h"
#include "sbl1_mc.h"
#include "LoaderUtils.h"


#ifdef FEATURE_DLOAD_MEM_DEBUG


/*===========================================================================

**  Function :  dload_add_ddr_training_data_info

** ==========================================================================
*/
/*!
* 
* @brief
*   Add the ddr training data region, ddr training data is loaded from ddr 
*   training data partition
*   
* @par Dependencies
*   None
*   
* @retval
*   None                                                               
* 
* @par Side Effects
*   None
* 
*/
static void dload_add_ddr_training_data(void)
{
  /* Buffer to store ddr training data */
  static uint8 ddr_training_data_dump[SCL_DDR_TRAINING_DATA_BUF_SIZE];
  
  /* Load SCL_DDR_TRAINING_DATA_BUF_SIZE of data from the training partition*/
  sbl1_load_ddr_training_data_from_partition(ddr_training_data_dump,
                                             SCL_DDR_TRAINING_DATA_BUF_SIZE);
  
  dload_debug_add_region(OPTIONAL_DEF_SAVE,
                          (uint32)ddr_training_data_dump,
                          SCL_DDR_TRAINING_DATA_BUF_SIZE,
                          "DDR Training Data", "DDR_DATA.BIN");  


}



/*===========================================================================

**  Function :  dload_add_pmic_info

** ==========================================================================
*/
/*!
* 
* @brief
*   Add the Reset Status register region
*   
* @par Dependencies
*   This api Must be called after clk_regime_apps_init_boot 
*   
* @retval
*   None                                                               
* 
* @par Side Effects
*   None
* 
*/
static void dload_add_pmic_info(void)
{
  // Dump variable must be declared as static and have section attribute to
  // be added to RamDump Data ZI region.  USB controller can not access locked 
  // TCM region  
  static uint64 pmic_power_on __attribute__((section ("BOOT_INTERNAL_HEAP")));
  
  /* Get power on reason for pmic device 0 */
  boot_pm_dev_get_power_on_reason(0, &pmic_power_on);

  dload_debug_add_region(OPTIONAL_DEF_SAVE,
                          (uint32)(&pmic_power_on),
                          (uint32) sizeof(pmic_power_on),
                          "Pmic PON stat", "PMIC_PON.BIN");
}


/*===========================================================================

**  Function :  dload_add_reset_status

** ==========================================================================
*/
/*!
* 
* @brief
*   Add the Reset Status register
*   
* @par Dependencies
*   None 
*   
* @retval
*   None                                                               
* 
* @par Side Effects
*   None
* 
*/
static void dload_add_reset_status(void)
{
  // Dump variable must be declared as static and have section attribute to
  // be added to RamDump Data ZI region.  USB controller can not access locked 
  // TCM region  
  static uint32 reset_status_register
    __attribute__((section ("BOOT_INTERNAL_HEAP")));
  
  reset_status_register = sbl1_hw_get_reset_status();
  dload_debug_add_region(OPTIONAL_DEF_SAVE,
                          (uint32)(&reset_status_register),
                          (uint32) sizeof(reset_status_register),
                          "Reset Status Region", "RST_STAT.BIN");
}


/*===========================================================================

**  Function :  dload_add_pimem_region

** ==========================================================================
*/
/*!
* 
* @brief
*   Add Pimem region dumps
*   
* @par Dependencies
*   None
*   
* @retval
*   None                                                               
* 
* @par Side Effects
*   None
* 
*/
static void dload_add_pimem_region(void)
{
  uint32 size_in_mb = 0;
  PIMEM_STATUS pimem_enable_status;
  
  pimem_enable_status = boot_pimem_get_status(&size_in_mb);
  if( PIMEM_OK == pimem_enable_status)
  {
    /*Add pimem region */
    dload_debug_add_region(OPTIONAL_DEF_SAVE,
                          (uintnt)SCL_pIMEM_BASE,
                          (uintnt)(size_in_mb << CONVERT_TO_MB_SHIFT),
                          "PIMEM region", "PIMEM.BIN");
  }
  else
  {
    boot_log_message("pImem Disabled. Hence Not added to Dumps.");
  }

}

/*===========================================================================

**  Function :  dload_add_ipa_memory_regions

** ==========================================================================
*/
/*!
* 
* @brief
*   Add IPA region dumps for chip versions v2 onwards
*   
* @par Dependencies
*   None
*   
* @retval
*   None                                                               
* 
* @par Side Effects
*   None
* 
*/
static void dload_add_ipa_memory_regions(void)
{
  struct memory_region 
  { 
      uint32 region_base;
      uint32 region_size;
      char* desc;
      char* filename;
  } dump_regions[] = {IPA_MEMORY_REGION_TABLE}; 
  uint32 index = 0;  

  /* Check Chip version */
  if((GET_CHIP_VERSION >= 0x2) || (GET_DEVICE_NUMBER >= 0x4))
  {  
    /* RAM-DUMP table defined in .builds file */
    while ( dump_regions[index].region_base != 0x0 ) 
    {
        dload_debug_add_region(OPTIONAL_DEF_SAVE, 
                               dump_regions[index].region_base,
                               dump_regions[index].region_size,
                               dump_regions[index].desc,
                               dump_regions[index].filename
                               );
        index++;
    }
  }
  else
  {
    boot_log_message("IPA dumps not added for chip v1");
  }
}


/*===========================================================================

FUNCTION DLOAD_MEM_DEBUG_TARGET_INIT

DESCRIPTION
  This function initializes clocks as needed for this target, as well as sets
  up the memory regions to be used.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/                                                                          
void dload_mem_debug_target_init(void)
{
  uintnt start_addr =0;
  uint32 size =0, index =0 ;
  sbl_if_shared_ddr_device_info_type *available_ddr;
  char ddr_string_info[18],ddr_filename[14];

  struct memory_region 
  { 
      uint32 region_base;
      uint32 region_size;
      char* desc;
      char* filename;
  };
  
  struct memory_region dump_regions[] = {MEMORY_REGION_TABLE}; 
  

  /* Set memory region table to be fixed length, required by sahara*/
  dload_mem_debug_len_init();


  /* Check if device is retail unlocked. Do not dump internal memories 
     in retail unlock scenario.  Also zero peripheral memory so it does not
     appear in DDR dump. */
  if (dload_mem_debug_is_device_retail_unlocked()) 
  {
    dump_regions[0].region_base = 0x0;
    dump_regions[0].region_size = 0x0;
    dump_regions[0].desc = NULL;
    dump_regions[0].filename = NULL;

    dload_mem_debug_zero_peripheral_memory();
  }
  

/*
 ********************************WARNING**************************************
 
 Please make sure all dump region file names follow the 8.3 file name format!
 
 *****************************************************************************
*/

  index = 0;

  /* RAM-DUMP table defined in .builds file */
  while ( dump_regions[index].region_base != 0x0 ) 
  {
      dload_debug_add_region(OPTIONAL_DEF_SAVE, 
                             dump_regions[index].region_base,
                             dump_regions[index].region_size,
                             dump_regions[index].desc,
                             dump_regions[index].filename
                             );
      index++;
  }

  /* Add IPA memory dumps */
  dload_add_ipa_memory_regions();  
  
  /* Add Pmic power on reason*/
  dload_add_pmic_info();  

  /* Add reset status */ 
  dload_add_reset_status();  
  
  /* Add ddr training data */
  dload_add_ddr_training_data();
  
  /* Add pImem region */
  dload_add_pimem_region();  
  
  index = 0;
  available_ddr = boot_get_ddr_info();
  while(index < available_ddr->noofddr)
  {
    start_addr = available_ddr->ddr_info[index].cs_addr;
    size = available_ddr->ddr_info[index].ramsize << CONVERT_TO_MB_SHIFT;
      
    /* Define DDR Memory Region  EBI1 CS0/CS1 etc */
    qsnprintf (ddr_string_info, 16, " DDR CS%u Memory", index);
    qsnprintf (ddr_filename, 11, "DDRCS%u.BIN", index);

    dload_debug_add_region(OPTIONAL_DEF_SAVE, start_addr, size, 
                     ddr_string_info, ddr_filename);
    index++;
  } 

}


/*===========================================================================

FUNCTION DLOAD_MEM_DEBUG_SUPPORT

DESCRIPTION
  This function returns if memory debug is supported.

DEPENDENCIES
  None.

RETURN VALUE
  TRUE or FALSE

SIDE EFFECTS
  None.

===========================================================================*/
boolean dload_mem_debug_supported()
{
  boolean mem_dump_supported = TRUE;
  bl_shared_data_type * bl_shared_data_ptr;
  int32 sbl1_entry = 0;

  bl_shared_data_ptr = sbl1_get_shared_data();

  sbl1_entry = boot_sbl_qsee_interface_get_image_entry(&(bl_shared_data_ptr->sbl_qsee_interface), SECBOOT_SBL1_SW_TYPE);

  BL_VERIFY(sbl1_entry != -1,
                BL_ERR_IMAGE_NOT_FOUND);

  mem_dump_supported = (boolean)boot_qsee_is_memory_dump_allowed(&(bl_shared_data_ptr->sbl_qsee_interface.boot_image_entry[sbl1_entry].image_verified_info));

  return mem_dump_supported;
}


/*===========================================================================

FUNCTION DLOAD_MEM_DEBUG_IS_DEVICE_RETAIL_UNLOCKED

DESCRIPTION
  This function returns if device is retail unlocked
  
DEPENDENCIES
  None.

RETURN VALUE
  TRUE or FALSE

SIDE EFFECTS
  None.

===========================================================================*/
boolean dload_mem_debug_is_device_retail_unlocked(void)
{
  return FALSE;
}


#endif /*FEATURE_DLOAD_MEM_DEBUG*/
