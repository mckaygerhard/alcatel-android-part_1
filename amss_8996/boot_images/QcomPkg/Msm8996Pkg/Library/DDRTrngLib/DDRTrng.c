/** @file DDRTraining.c
  Top-Level Logic for DDRTraining.c
  
  Copyright (c) 2014, Qualcomm Technologies, Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 07/06/15   rp      Initial revision

=============================================================================*/

#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "HAL_SNS_DDR.h"
#include "ddr_config.h"
#include "ClockBoot.h"
#include "ddr_external.h"
#include "railway.h"
#include <string.h>
#include "ddr_target.h"
#include "busywait.h"
#include "pm_ldo.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HAL_SNS_DDR.h"
#include "ddr_config.h"
#include "ClockBoot.h"
#include "icbcfg.h"
#include <stddef.h>
#include "ddr_external.h"
#include "pm_ldo.h"
#include "CoreVerify.h"
#include "cpr_api.h"
#include "crc.h"
#include "HALbootHWIO.h"
#include "boot_shared_functions_consumer.h"


void DDRTrngMain( void )
{
 
  boot_shared_functions_init();
  HAL_DDR_Boot_Training_Init(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH);	

}

