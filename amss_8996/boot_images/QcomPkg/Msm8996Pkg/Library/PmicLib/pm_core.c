/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                  PMIC Startup Services

GENERAL DESCRIPTION
  This file contains initialization functions and corresponding variable
  declarations to support interaction with the Qualcomm Pmic chips.

  Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/01/15   al      Adding Haptics support
07/21/14   va      Adding Fuel Gauge Protocol
07/03/14   al      Adding MIPI-BIF, RGB. Removing SW workaround since it doesn't solve issue
06/06/14   al      Adding workaround for HW issue 
04/28/14   va      Expose Npa Test protocol
05/09/14   al      Adding IBB and LAB 
04/18/14   al      Added SMBCHG 
11/22/13   va      PmicLib Dec Addition
10/02/13   al      New File
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "com_dtypes.h"
#include "pm_uefi.h"

/**
  PMIC PROTOCOL interface
 */
#include <Library/UefiBootServicesTableLib.h>
#include "Protocol/EFIPmicClkBuff.h"
#include "Protocol/EFIPmicVib.h"
#include "Protocol/EFIPmicWled.h"
#include "Protocol/EFIPmicGpio.h"
#include "Protocol/EFIPmicMpp.h"
#include "Protocol/EFIPmicVreg.h"
#include "Protocol/EFIPmicLpg.h"
#include "Protocol/EFIPmicRTC.h"
#include "Protocol/EFIPmicPwrOn.h"
#include "Protocol/EFIPmicMipiBif.h"
#include "Protocol/EFIPmicRgbLed.h"
#include "Protocol/EFIPmicVersion.h"
#include "Protocol/EFIPmicSmbchg.h"
#include "Protocol/EFIPmicNpaTest.h"
#include "Protocol/EFIPmicIbb.h"
#include "Protocol/EFIPmicLab.h"
#include "Protocol/EFIPmicFg.h"
#include "Protocol/EFIPmicHaptics.h"

/*===========================================================================

                 LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/


/*===========================================================================

                VARIABLES DEFINITIONS

===========================================================================*/
extern EFI_QCOM_PMIC_CLKBUFF_PROTOCOL   PmicClkBuffProtocolImplementation;
extern EFI_QCOM_PMIC_VIB_PROTOCOL       PmicVibProtocolImplementation;
extern EFI_QCOM_PMIC_WLED_PROTOCOL      PmicWledProtocolImplementation;
extern EFI_QCOM_PMIC_GPIO_PROTOCOL      PmicGpioProtocolImplementation;
extern EFI_QCOM_PMIC_MPP_PROTOCOL       PmicMppProtocolImplementation;
extern EFI_QCOM_PMIC_VREG_PROTOCOL      PmicVregProtocolImplementation;
extern EFI_QCOM_PMIC_LPG_PROTOCOL       PmicLpgProtocolImplementation;
extern EFI_QCOM_PMIC_RTC_PROTOCOL       PmicRtcProtocolImplementation;
extern EFI_QCOM_PMIC_PWRON_PROTOCOL     PmicPwronProtocolImplementation;
extern EFI_QCOM_PMIC_MIPIBIF_PROTOCOL   PmicMipiBifProtocolImplementation;
extern EFI_QCOM_PMIC_RGB_LED_PROTOCOL   PmicRgbLedProtocolImplementation;
extern EFI_QCOM_PMIC_VERSION_PROTOCOL   PmicVersionProtocolImplementation;
extern EFI_QCOM_PMIC_SMBCHG_PROTOCOL    PmicSmbchgProtocolImplementation;
extern EFI_QCOM_PMIC_NPATEST_PROTOCOL   PmicNpaTestProtocolImplementation;
extern EFI_QCOM_PMIC_IBB_PROTOCOL       PmicIbbProtocolImplementation;
extern EFI_QCOM_PMIC_LAB_PROTOCOL       PmicLabProtocolImplementation;
extern EFI_QCOM_PMIC_FG_PROTOCOL        PmicFgProtocolImplementation;
extern EFI_QCOM_PMIC_HAPTICS_PROTOCOL   PmicHapticsProtocolImplementation;

/*===========================================================================

                LOCAL FUNCTION PROTOTYPES

===========================================================================*/


/*===========================================================================

                EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================
FUNCTION pm_install_uefi_protocol                                EXTERNAL FUNCTION

DESCRIPTION
    This function installs pmic uefi protocols.

    It does the following:
    1)  It initializes the target specific pmic uefi protocol 

INPUT PARAMETERS
  Imagehandle and SystemTable pointer

RETURN VALUE
  pm_err_flag_type

DEPENDENCIES
  pm_init must have been called

SIDE EFFECTS
  None.
===========================================================================*/
pm_err_flag_type pm_install_uefi_protocol(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
   EFI_STATUS Status = EFI_SUCCESS;

   pm_err_flag_type    err_flag = PM_ERR_FLAG__SUCCESS;

   Status  = gBS->InstallMultipleProtocolInterfaces(
      &ImageHandle,
      &gQcomPmicGpioProtocolGuid,
      &PmicGpioProtocolImplementation,
      &gQcomPmicMppProtocolGuid,
      &PmicMppProtocolImplementation,
      &gQcomPmicVregProtocolGuid,
      &PmicVregProtocolImplementation,
      &gQcomPmicLpgProtocolGuid,
      &PmicLpgProtocolImplementation,
      &gQcomPmicClkBuffProtocolGuid,
      &PmicClkBuffProtocolImplementation,
      &gQcomPmicRtcProtocolGuid,
      &PmicRtcProtocolImplementation,
      &gQcomPmicPwrOnProtocolGuid,
      &PmicPwronProtocolImplementation,
      &gQcomPmicVersionProtocolGuid,
      &PmicVersionProtocolImplementation,
      &gQcomPmicSmbchgProtocolGuid,
      &PmicSmbchgProtocolImplementation,
      &gQcomPmicMipiBifProtocolGuid,
      &PmicMipiBifProtocolImplementation,
      &gQcomPmicNpaTestProtocolGuid,
      &PmicNpaTestProtocolImplementation,
      &gQcomPmicWledProtocolGuid,
      &PmicWledProtocolImplementation,
      &gQcomPmicIbbProtocolGuid,
      &PmicIbbProtocolImplementation,
      &gQcomPmicLabProtocolGuid,
      &PmicLabProtocolImplementation,
      &gQcomPmicRgbLedProtocolGuid,
      &PmicRgbLedProtocolImplementation,
      &gQcomPmicFgProtocolGuid,
      &PmicFgProtocolImplementation,
      &gQcomPmicHapticsProtocolGuid,
      &PmicHapticsProtocolImplementation,
      NULL
      );

   return (pm_err_flag_type)(Status | err_flag);
}


