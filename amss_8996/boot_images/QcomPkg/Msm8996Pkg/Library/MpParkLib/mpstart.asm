// @file
//  This code sets up another core to start executing mailbox code.
//
//  Copyright (c) 2011-2014, Qualcomm Technologies Inc. All rights reserved.
//
//=============================================================================
//                              EDIT HISTORY
//
//
// when       who     what, where, why
// --------   ---     -----------------------------------------------------------
// 06/30/14   rli     Updated to support A57 cluster
// 06/26/14   rli     Removed SCTLR, ACTLR, DACR from MPSTART_ARGS
// 02/12/14   rli     Branched from 8x26
//
//=============================================================================

#include <Library/PcdLib.h>

        INCLUDE scorpion_common_macros.inc

        EXPORT           StartAuxiliaryCPU
        EXPORT           StartAuxiliaryCPU_END

Mode_SVC                EQU         0x13
NoIntsMask              EQU         0x000000C0

; startup parameter block
OFST_CALLBACK           EQU         0x0FF8
OFST_STACK              EQU         0x0FFC

        PRESERVE8
        AREA             MpStart, CODE, READONLY

StartAuxiliaryCPU

        dcd              0x00000000  ; Reset
        dcd              0x00000000  ; Undefined instructions
        dcd              0x00000000  ; Software interrupt (SWI/SVC)
        dcd              0x00000000  ; Prefetch abort
        dcd              0x00000000  ; Data abort
        dcd              0x00000000  ; Reserved
        dcd              0x00000000  ; IRQ
        dcd              0x00000000  ; FIQ

        ldr              r0, =(Mode_SVC :OR: NoIntsMask)
        msr              cpsr_c, r0
        mov              lr, pc                       ; used to get page boundary

        ; save page boundary to r4
        mov              r0, pc                       ; get current value of PC
        mov              r0, r0, LSR #20              ; strip off lower 20 bits
        mov              r0, r0, LSL #20
        mov              r4, r0

        WCP15_VBAR       r4                           ; set vector base
        isb

        ; Fill vector table with branchs to current pc (jmp $)
        ldr              r1, VectorSpin
        movs             r2, #0
FillVectors
        str              r1, [r0, r2]
        adds             r2, r2, #4
        cmp              r2, #32
        bne              FillVectors

        ; Get the CPU number from MPIDR[AFFL0] and hold in R10
        RCP15_MPIDR      r10                          ; get cpu id
        and              r9,  r10, #0x100             ; get cluster id 
        lsr              r9,  r9,  #6                 ; 4 cpus per cluster
        and              r10, r10, #0xff              ; get lower byte

        add              r0, r10, r9                  ; absolute cpu id
        add              r0, r0, #1                   ; increment index
        add              r4, r0, LSL #12              ; calculate
        ldr              lr, [r4, #OFST_CALLBACK]     ; this is where we exit to

        ; Stack required to call C functions
        ldr              r1, [r4, #OFST_STACK]
        mov              sp, r1

        mov              r0, r4
        bx               lr


VectorSpin
        b                VectorSpin

StartAuxiliaryCPU_END

ShouldNeverGetHere
        b                ShouldNeverGetHere

        END

