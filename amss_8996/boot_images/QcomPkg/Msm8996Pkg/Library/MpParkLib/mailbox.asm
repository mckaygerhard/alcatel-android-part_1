// @file
//  This implements Page Parking Algorithm.
//  MpParkMailbox is relocated at runtime, so everything must remain relative.
//
//  Copyright (c) 2014, Qualcomm TechnologiesInc. All rights reserved.
//
//=============================================================================
//                              EDIT HISTORY
//
//
// when       who     what, where, why
// --------   ---     -----------------------------------------------------------
// 06/30/14   rli     Updated to support A57 cluster
// 02/12/14   rli     Branched from 8x26.
//
//=============================================================================

        ARM
        REQUIRE8
        PRESERVE8

        AREA    MpParkDxe, CODE, READONLY

; startup parameter block
OFST_PROCID_LO_DWORD    EQU         0x0000
OFST_PROCID_HI_DWORD    EQU         0x0004
OFST_JUMP_LO_DWORD      EQU         0x0008
OFST_JUMP_HI_DWORD      EQU         0x000c

MpParkMailbox

        ; save entry point address to r10
        mov     r10, pc                      ; get current value of PC
        mov     r10, r10, LSR #8             ; strip off lower 12 bits
        mov     r10, r10, LSL #8             ; entry point

        ; save page boundary to r4
        mov     r4, pc                       ; get current value of PC
        mov     r4, r4, LSR #12              ; strip off lower 12 bits
        mov     r4, r4, LSL #12              ; page boundary

        ; isolate processor id
        ldr     r0, [r4, #OFST_PROCID_LO_DWORD]
        and     r0, r0, #0xFF                ; isolate proc ID

        ; get current cpu id
        mrc     p15, 0, r2, c0, c0, 5        ; get cpu id
        and     r3,  r2, #0x100              ; get cluster id 
        lsr     r3,  r3, #6                  ; 4 cpus per cluster
        and     r2, r2, #0xFF                ; get lower byte
        add     r2, r2, r3                   ; absolute cpu id
        cmp     r0, r2
        bne     _wait                        ; if not our CPU id, goto WFI

        ; check for Zero Jump
        ldr     r0, [r4, #OFST_JUMP_LO_DWORD]
        cmp     r0, #0
        beq     _wait                        ; if Jump address == 0, goto WFI

        ; copy jump address to r8-r9
        mov     r8, r0
        ldr     r9, [r4, #OFST_JUMP_HI_DWORD]
        
        ; store mailbox address in R0
        mov     r0, r4

        ; acknowledge
        mov     r1, #0
        str     r1, [r4, #OFST_JUMP_LO_DWORD]
        str     r1, [r4, #OFST_JUMP_HI_DWORD]

        ; jump to specified address
        bx      r8

_wait
        dsb                                  ; data syncronization barrier
        wfi                                  ; wait for interrupt

        bx      r10

MpParkMailbox_END
        EXPORT  MpParkMailbox
        EXPORT  MpParkMailbox_END
        END
