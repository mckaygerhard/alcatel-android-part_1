/** @file
ButtonsLib.c

Button driver is responsible for detecting key press/release through PMIC HW
and service. It implements the interfaces of simple text input protocol.

This file is platform dependent, it supports ButtonsDxe based on the platform.

Copyright (c) 2012-2014 Qualcomm Technologies, Inc. All rights reserved.
Qualcomm Technologies Proprietary and Confidential.

**/
/*=============================================================================
EDIT HISTORY

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who      what, where, why
--------   ---      ----------------------------------------------------------
11/13/14   ns       Updated for 8996
06/18/14   ma       Add pwr key as combo button
03/28/14   ma       Change DVDD hard reset to normal hard reset for WP
03/01/14   ma       Updated for 8994
11/18/13   ma       Add pwr key to button array
08/26/13   ma       Revert DVDD shutdown configuration for WA to SHUTDOWN
08/22/13   ma       Enable DVDD shudown for WA on 10s press of PWR key
08/14/13   ma       Configure S3 reset timer, enable DVDD button reset
05/15/13   ma       Modify reset configurations. Check for OfflineMemoryDump
04/09/13   ma       Added IsNotificationSupported() API
03/22/13   ma       Corrected comments, reset support for PM8941 3.0
03/21/13   ma       Added support to configure debounce
03/18/13   ma       Added macros to denote registers
03/12/2013 ma       Check revision before RESIN_BARK init
03/07/2013 ma       Support for PM8941 3.0
02/15/13   vk       Move to UefiCfgLib
02/08/2013 ma       Added hard reset PCD
12/20/2012 ma       Modifying reset behavior
12/06/2012 ma       Remove KW warning
11/07/2012 ma       Adding IsHomeKeyDetected() API
10/22/2012 ma       Updating copyrights
10/01/2012 ma       Modifying reset behavior
09/27/2012 ma       Configure resets based on OS platform,
                    move hard-coded addresses to ButtonsLibPrivate.h
09/13/2012 ma       Disable reset via Pwr Key extended press
08/16/2012 ma       Enabling support for Vol Dwn button using RESIN_BARK
08/11/2012 ma       Used spmi protocol to configure GPIOs
08/01/2012 ma       Added workaround for Vol Dwn key
07/30/2012 ma       Initial version for 8974
=============================================================================*/

#include <Base.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/ButtonsLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/OfflineCrashDump.h>

#include "ButtonsLibPrivate.h"


EFI_PLATFORMINFO_PLATFORM_TYPE  PlatformType ;
EFI_QCOM_SPMI_PROTOCOL  *SPMIProtocol;


BOOLEAN isEfiKeyDetected = FALSE;
BOOLEAN isHomeKeyDetected = FALSE;
extern EFI_GUID gQcomTokenSpaceGuid;

#define NUMBER_OF_KEYS 5

/*** Define the Key Map for all Platforms ***/

//KeyMap for CDP
KEY_TYPE KeyMap_8996_CDP[ NUMBER_OF_KEYS] =
{
   PWR, VOL_UP, VOL_DOWN, HOME, CAMERA
};

//KeyMap for MTP and FLUID
KEY_TYPE KeyMap_8996_MTP_FLUID[ NUMBER_OF_KEYS] =
{
   PWR, VOL_UP, VOL_DOWN, NONE, CAMERA
};

//KeyMap for LIQUID
KEY_TYPE KeyMap_8996_LIQUID[ NUMBER_OF_KEYS] =
{
   PWR, VOL_UP, VOL_DOWN, HOME, NONE
};

/**
Find a local key to an array.

@param  pArray         An array pointer to which the key will be found from.
@param  sizeOfArray    The array size.
@param  key            The key will be found.

@retval TRUE           Found successfully.
@retval FALSE          Found failed.

**/

BOOLEAN FoundAKey(
   KEY_TYPE *pArray,
   UINT8     sizeOfArray,
   KEY_TYPE  key
   )
{
   UINT8 i;
   for( i=0; i<sizeOfArray; i++ )
   {
      if( pArray[i] == key )
      {
         return TRUE;
      }
   }
   return FALSE;
}


/**
Translate local keys into EFI keys.

The press session starts with any key is pressed and ends with no key is
pressed.

Only generate one EFI key per press session and this EFI key is generated
only based on the above table.

For the case of multiple keys pressed, check the possible combo keys first,
first detected combo keys will be used to generate the EFI key. If there is
no valid combo keys detected, then check the single key case, first detected
single key will be used to generate the EFI key.

Once an EFI key is generated in a session, even though there is still other
key currently pressed, no more EFI key will be generated in this session and
just wait for user to release those pressed keys.

@param  pKeysPressed                Pointer to currently pressed keys array.
@param  numOfKeysPressed            Number of keys pressed in pKeysPressed
@param  pKeysReleased               Pointer to currently released keys array.
@param  numOfKeysReleased           Number of keys released in pKeysReleased
@param  sizeOfPressedReleasedArray  Size of pKeysPresed/pKeysPressed arrays.
@param  pEfiKeys                    Pointer to Uefi keys array.
@param  pNumOfEfiKeys               Number of Uefi keys detected.

@retval EFI_SUCCESS                 UEFI key detected
@retval EFI_NOT_READY               No UEFI key detected

**/
EFI_STATUS ConvertEfiKeyCode (
   KEY_TYPE       *pKeysPressed,
   UINT8           numOfKeysPressed,
   KEY_TYPE       *pKeysReleased,
   UINT8           numOfKeysReleased,
   UINT8           sizeOfPressedReleasedArray,
   EFI_INPUT_KEY  *pEfiKeys,
   UINT8          *pNumOfEfiKeys
   )
{
   EFI_STATUS retVal = EFI_NOT_FOUND;
   EFI_INPUT_KEY EfiKey;

   EfiKey.ScanCode = SCAN_NULL;
   EfiKey.UnicodeChar = 0;
   *pNumOfEfiKeys = 0;
   isHomeKeyDetected = FALSE;

   BOOLEAN bCameraKeyIsPressed;
   BOOLEAN bVolUpKeyIsPressed;
   BOOLEAN bVolDownKeyIsPressed;
   BOOLEAN bCameraKeyIsReleased;
   BOOLEAN bHomeKeyIsPressed;
   BOOLEAN bPwrKeyPressed;

   if( isEfiKeyDetected == FALSE )
   {
      bCameraKeyIsPressed      = FoundAKey(pKeysPressed, sizeOfPressedReleasedArray, CAMERA);
      bVolUpKeyIsPressed       = FoundAKey(pKeysPressed, sizeOfPressedReleasedArray, VOL_UP);
      bVolDownKeyIsPressed     = FoundAKey(pKeysPressed, sizeOfPressedReleasedArray, VOL_DOWN);
      bCameraKeyIsReleased     = FoundAKey(pKeysReleased,sizeOfPressedReleasedArray, CAMERA);
      bHomeKeyIsPressed        = FoundAKey(pKeysPressed, sizeOfPressedReleasedArray, HOME);
      bPwrKeyPressed           = FoundAKey(pKeysPressed, sizeOfPressedReleasedArray, PWR);

      // assume EFI key found
      isEfiKeyDetected = TRUE;

      if( bCameraKeyIsPressed && bVolUpKeyIsPressed )
      {
         // combo key found
         EfiKey.ScanCode = SCAN_HOME;
      }
      else if( bCameraKeyIsPressed && bVolDownKeyIsPressed )
      {
         // combo key found
         EfiKey.ScanCode = SCAN_DELETE;
      }
      else if( bVolUpKeyIsPressed )
      {
         if( bPwrKeyPressed )
         {
            // combo key found
            EfiKey.ScanCode = SCAN_HOME;
         }
         else
         {
            // single key found
            EfiKey.ScanCode = SCAN_UP;
         }
      }
      else if( bVolDownKeyIsPressed )
      {
         if( bPwrKeyPressed )
         {
            // combo key found
            EfiKey.ScanCode = SCAN_DELETE;
         }
         else
         {
            // single key found
            EfiKey.ScanCode = SCAN_DOWN;
         }
      }
      else if( bCameraKeyIsReleased )
      {
         // single key found
         EfiKey.UnicodeChar = CHAR_CARRIAGE_RETURN;
      }
      else if( bHomeKeyIsPressed )
      {
         EfiKey.ScanCode = SCAN_NULL;
         isHomeKeyDetected = TRUE;
      }
      else if( bPwrKeyPressed )
      {
        EfiKey.ScanCode = SCAN_SUSPEND;
      }
      else
      {
         // no EFI key found, set the flag to FALSE.
         isEfiKeyDetected = FALSE;
      }


      if( isEfiKeyDetected == TRUE )
      {
         // an EFI key is detected, return it.
         CopyMem( &pEfiKeys[*pNumOfEfiKeys], &EfiKey, sizeof(EFI_INPUT_KEY));
         (*pNumOfEfiKeys)++;

         retVal = EFI_SUCCESS;
      }
   }

   if( numOfKeysPressed == 0)
   {
      // no more key pressed, reset flag for next press session.
      isEfiKeyDetected = FALSE;
   }

   return retVal;
}



/**
Initialize KeyMap based on the platform.
Also initialze Power Key.

@param  pNumberOfKeys            Pointer to number of keys
@param  pKeyMap                  Pointer to key map.
@retval EFI_SUCCESS              Initialization successful
@retval non EFI_SUCCESS          Initialization failed

**/

EFI_STATUS InitializeKeyMap (
   UINT8         *pNumberofKeys,
   KEY_TYPE      *pKeyMap
   )
{
   EFI_PLATFORMINFO_PROTOCOL *pPlatformInfoProtocol;
   EFI_PLATFORMINFO_PLATFORM_INFO_TYPE  platformInfo;
   EFI_STATUS Status;

   Status = gBS->LocateProtocol ( &gEfiPlatformInfoProtocolGuid,
      NULL,
      (VOID **)&pPlatformInfoProtocol
      );

   if ( Status != EFI_SUCCESS )
   {
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Failed to locate PlatformInfo Protocol, Status =  (0x%x)\r\n", Status ));
      goto ErrorExit;
   }

   if( !pPlatformInfoProtocol )
   {
      Status = EFI_INVALID_PARAMETER;
      goto ErrorExit;
   }

   pPlatformInfoProtocol->GetPlatformInfo( pPlatformInfoProtocol, &platformInfo );
   PlatformType = platformInfo.platform;

   switch ( PlatformType )
   {
   case EFI_PLATFORMINFO_TYPE_CDP:
      CopyMem( pKeyMap, KeyMap_8996_CDP, NUMBER_OF_KEYS*sizeof(KEY_TYPE) );
      break;
   case EFI_PLATFORMINFO_TYPE_MTP_MSM:
   case EFI_PLATFORMINFO_TYPE_FLUID:
      CopyMem( pKeyMap, KeyMap_8996_MTP_FLUID, NUMBER_OF_KEYS*sizeof(KEY_TYPE) );
      break;
   case EFI_PLATFORMINFO_TYPE_LIQUID:
      CopyMem( pKeyMap, KeyMap_8996_LIQUID, NUMBER_OF_KEYS*sizeof(KEY_TYPE) );
      break;

   default:
      Status = EFI_INVALID_PARAMETER;
      goto ErrorExit;
   }

   *pNumberofKeys = NUMBER_OF_KEYS;

ErrorExit:
   return Status;

}

/**
Enable the GPIO for Input

@param  UINT32                   SPMI register on PMIC
@retval EFI_SUCCESS              Configuration successful
@retval non EFI_SUCCESS          Configuration failed

**/
EFI_STATUS EnableInput ( UINT32 WriteReg )
{
  EFI_STATUS Status = EFI_SUCCESS;
  Spmi_Result result = SPMI_SUCCESS;
  UINT8 Data = 0;

   if(SPMIProtocol)
   {
     //MODE_CTL
     Data = GPIO_MODE_CTL_INPUT;
     result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg|GPIO_MODE_CTL_REG_OFFSET , &Data, 1);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result,&Status);
       goto ErrorExit;
     }

     //DIG_VIN_CTL
     Data = GPIO_DIG_VIN_CTL_VIN2;
     result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg|GPIO_DIG_VIN_CTL_REG_OFFSET , &Data, 1);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result,&Status);
       goto ErrorExit;
     }

     //DIG_PULL_CTL
     Data = GPIO_DIG_PULL_CTL_30microAmp;
     result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg|GPIO_DIG_PULL_CTL_REG_OFFSET , &Data, 1);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result,&Status);
       goto ErrorExit;
     }

     //EN_CTL
     Data = GPIO_EN_CTL_ENABLE;
     result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg|GPIO_EN_CTL_REG_OFFSET , &Data, 1);
     if (SPMI_SUCCESS != result)
     {
      SPMIError(result,&Status);
       goto ErrorExit;
     }
}

ErrorExit:
   return Status;
}



/**
Initialize all button GPIOs on PMIC for input based
on platform

@param  None
@retval EFI_SUCCESS              Configuration successful
@retval non EFI_SUCCESS          Configuration failed

**/
EFI_STATUS ConfigureButtonGPIOs ( VOID )
{

   EFI_STATUS Status;

   // volume up
   Status = EnableInput(VOLUME_UP_BUTTON_PERIPHERAL);
   if ( EFI_ERROR (Status) )
   {
      DEBUG(( EFI_D_ERROR, "ConfigureButtonGPIOs: EnableInput failed for VOL+ button, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
   }


   if( (PlatformType == EFI_PLATFORMINFO_TYPE_MTP_MSM)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_FLUID)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_CDP))
   {
     // Camera Snapshot
     Status = EnableInput(CAMERA_SNAPSHOT_BUTTON_PERIPHERAL);
     if ( EFI_ERROR (Status) )
     {
        DEBUG(( EFI_D_ERROR, "ConfigureButtonGPIOs: EnableInput failed for Snapshot button, Status = (0x%x)\r\n", Status));
        goto ErrorExit;
     }
   }

   if( (PlatformType == EFI_PLATFORMINFO_TYPE_LIQUID)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_CDP) )
   {
     //home
     Status = EnableInput(HOME_BUTTON_PERIPHERAL);
     if ( EFI_ERROR (Status) )
     {
        DEBUG(( EFI_D_ERROR, "ConfigureButtonGPIOs: EnableInput failed for home button, Status = (0x%x)\r\n", Status));
        goto ErrorExit;
     }
  }

ErrorExit:
   return Status;

}

/**
Read PON S3 reset timer value

@param  pData                    Pointer to revision data

@retval EFI_SUCCESS              Retrieve status successfully
@retval non EFI_SUCCESS          Retrieve status failed

**/
EFI_STATUS ReadPONTimerS3(UINT8 *pData)
{
   EFI_STATUS Status = EFI_SUCCESS;
   Spmi_Result result = SPMI_SUCCESS;
   UINT32 ReadNum = 0;

   if(SPMIProtocol)
   {
     result = SPMIProtocol->ReadLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_S3_RESET_TIMER, pData, 1, &ReadNum);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result, &Status);
       goto ErrorExit;
     }
   }
ErrorExit:
   return Status;
}


/**
Configure reset based on OS platform (WA vs WP)

WA: Forced shutdown via extended press of POWER button for 10.2s
Warm Reset via extended press of PWR + VOL- for 6.7s

WP: Hard reset via extended press of VOL- + PWR button for 10.2s.
Warm Reset via extended press PWR for 10.2s

@param  None
@retval EFI_SUCCESS              Configuration successful
@retval non EFI_SUCCESS          Configuration failed

**/
EFI_STATUS ConfigureReset (VOID)
{
   EFI_STATUS Status = EFI_SUCCESS;
   Spmi_Result result = SPMI_SUCCESS;
   UINT8 Data = 0;
   UINT32 WriteReg = 0;
   UINT32 PwrBtnShutdown = 0;
   UINT8 PONTimerS3Value = 0;

   if(SPMIProtocol)
   {

      Status = GetConfigValue(PcdGetPtr(PcdPwrBtnShutdownFlag), &PwrBtnShutdown);

      if ( EFI_ERROR(Status) )
      {
        PwrBtnShutdown = 0;
        DEBUG(( EFI_D_ERROR, "ConfigureReset: failed to read PcdPwrBtnShutdownFlag, Status = (0x%x)\r\n", Status));
        DEBUG(( EFI_D_WARN, "ConfigureReset: configuring default settings \r\n"));
      }

      //Configure Hard Reset
      if(PwrBtnShutdown && FeaturePcdGet(PcdUefiHardResetConfigure) )
      {
         /************************************************************************
         * Enable forced shutdown via extended power button press for 10.2s      *
         * This is for Windows on ARM platform                                   *
         *************************************************************************/

        //Disable forced shutdown via power button extended press first


        WriteReg = PON_KPDPWR_N_RESET_S2_CTL2_REG;
        Data = RESET_S2_CTL_DISABLE;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }

        gBS->Stall(600);

        //Init S1 (Bark) timer to 10.2 seconds and S2 (Bite) timer to 100ms

        //S1
        WriteReg = PON_KPDPWR_N_RESET_S1_TIMER_REG;
        Data = S1_TIMER_10256ms;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }

        //S2
        WriteReg = PON_KPDPWR_N_RESET_S2_TIMER_REG;
        Data = S2_TIMER_100ms;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }


        if ( FeaturePcdGet(PcdUefiHardResetEnabled) )
        {
          gBS->Stall(600);
          //Forced forced shutdown via power button extended press
          WriteReg = PON_KPDPWR_N_RESET_S2_CTL_REG;
          Data = RESET_S2_CTL_SHUTDOWN;
          result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
          if (SPMI_SUCCESS != result)
          {
            SPMIError(result, &Status);
            goto ErrorExit;
          }

          gBS->Stall(600);
          WriteReg = PON_KPDPWR_N_RESET_S2_CTL2_REG;
          Data = RESET_S2_CTL2_ENABLE;
          result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
          if (SPMI_SUCCESS != result)
          {
             SPMIError(result, &Status);
             goto ErrorExit;
          }
        }
      }
      else if( FeaturePcdGet(PcdUefiHardResetConfigure) )
      {
        /***********************************************
        * Enable  Hard Reset via extended press of     *
        * 2 button combo for 10.2s                     *
        * This is for Windows Phone platform           *
        ***********************************************/

        //Disable hard reset via extended press 2 button combo first

        WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL2_REG;
        Data = RESET_S2_CTL_DISABLE;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }

        gBS->Stall(600);

        //Init S1 (Bark) timer to 10.2 seconds and S2 (Bite) timer to 100 milli seconds

        // S1
        WriteReg = PON_RESIN_AND_KPDPWR_RESET_S1_TIMER_REG;
        Data = S1_TIMER_10256ms;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }

        // S2
        WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_TIMER_REG;
        Data = S2_TIMER_100ms;
        result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
        if (SPMI_SUCCESS != result)
        {
          SPMIError(result, &Status);
          goto ErrorExit;
        }

        if( FeaturePcdGet(PcdUefiHardResetEnabled) )
        {
          gBS->Stall(600);
          //Enable hard reset via extended press of 2 button combo
          WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL_REG;
          Data = RESET_S2_CTL_HARD_RESET;
          result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
          if (SPMI_SUCCESS != result)
          {
            SPMIError(result, &Status);
            goto ErrorExit;
          }

          gBS->Stall(600);
          WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL2_REG;
          Data = RESET_S2_CTL2_ENABLE;
          result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
          if (SPMI_SUCCESS != result)
          {
             SPMIError(result, &Status);
             goto ErrorExit;
          }
        }

      }// end of Hard reset configuration


      // Configure Warm Reset
      if( PwrBtnShutdown && FeaturePcdGet(PcdUefiWarmResetConfigure) )
      {
        /***************************************************************************
         * Enable Warm Reset via extended press of power + vol- button for 6.7s    *
         * This is for Windows on ARM Platform                                     *
         ***************************************************************************/

         //Disable warm reset via extended press of power button + vol- combo first
         WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL2_REG;
         Data = RESET_S2_CTL_DISABLE;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }

         gBS->Stall(600);

         //Init S1 (Bark) timer to 6.7 seconds and S2 (Bite) timer to 100 milli seconds

         //S1
         WriteReg = PON_RESIN_AND_KPDPWR_RESET_S1_TIMER_REG;
         Data = S1_TIMER_6720ms;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }

         //S2
         WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_TIMER_REG;
         Data = S2_TIMER_100ms;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }


         if( FeaturePcdGet(PcdUefiWarmResetEnabled) && IsMemoryDumpEnabled() )
         {
           gBS->Stall(600);
           //Enable warm reset via extended press of power button + vol- combo
           WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL_REG;
           Data = RESET_S2_CTL_WARM_RESET;
           result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
           if (SPMI_SUCCESS != result)
           {
             SPMIError(result, &Status);
             goto ErrorExit;
           }

           gBS->Stall(600);
           WriteReg = PON_RESIN_AND_KPDPWR_RESET_S2_CTL2_REG;
           Data = RESET_S2_CTL2_ENABLE;
           result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
           if (SPMI_SUCCESS != result)
           {
              SPMIError(result, &Status);
              goto ErrorExit;
           }

         }
      }
      else if ( FeaturePcdGet(PcdUefiWarmResetConfigure) )
      {
        /***************************************************************************
         * Enable Warm Reset via extended press of power button for 10.2s          *
         * This is for Windows Phone Platform                                      *
         ***************************************************************************/

         //Disable warm reset via extended press of power button first
         WriteReg = PON_KPDPWR_N_RESET_S2_CTL2_REG;
         Data = RESET_S2_CTL_DISABLE;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }

         gBS->Stall(600);

         //Init S1 (Bark) timer to 10.2 seconds and S2 (Bite) timer to 100 milli seconds

         //S1
         WriteReg = PON_KPDPWR_N_RESET_S1_TIMER_REG;
         Data = S1_TIMER_10256ms;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }

         //S2
         WriteReg = PON_KPDPWR_N_RESET_S2_TIMER_REG;
         Data = S2_TIMER_100ms;
         result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
         if (SPMI_SUCCESS != result)
         {
           SPMIError(result, &Status);
           goto ErrorExit;
         }


         if( FeaturePcdGet(PcdUefiWarmResetEnabled) && IsMemoryDumpEnabled() )
         {
           gBS->Stall(600);
           //Enable warm reset via extended press of power button + vol- combo
           WriteReg = PON_KPDPWR_N_RESET_S2_CTL_REG;
           Data = RESET_S2_CTL_WARM_RESET;
           result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
           if (SPMI_SUCCESS != result)
           {
             SPMIError(result, &Status);
             goto ErrorExit;
           }

           gBS->Stall(600);
           WriteReg = PON_KPDPWR_N_RESET_S2_CTL2_REG;
           Data = RESET_S2_CTL2_ENABLE;
           result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &Data, 1);
           if (SPMI_SUCCESS != result)
           {
              SPMIError(result, &Status);
              goto ErrorExit;
           }
         }
      }// end of warm reset configuration


      //configure S3 reset timer
      Status = ReadPONTimerS3(&PONTimerS3Value);
      if( PONTimerS3Value < S3_TIMER_16s )
      {
		  //configure the S3 reset timer to 32s
		  WriteReg = PON_S3_RESET_TIMER;
		  PONTimerS3Value = S3_TIMER_32s;
          result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, WriteReg, &PONTimerS3Value, 1);
          if (SPMI_SUCCESS != result)
          {
            SPMIError(result, &Status);
            goto ErrorExit;
          }
          //PON_S3_RESET_TIMER is a shadowed register. Min time between write access is 5 sleep clock cycles ~ 300 micro seconds
          gBS->Stall(300);
      }

   } // end of SPMIProtocol check

ErrorExit:
   return Status;
}



/**
Configure PON peripheral Debounce


@param  None
@retval EFI_SUCCESS              Configuration successful
@retval non EFI_SUCCESS          Configuration failed

**/
EFI_STATUS ConfigurePONDebounce (VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  Spmi_Result result = SPMI_SUCCESS;
  UINT8 Data = 0;

   if(SPMIProtocol)
   {
     //PON_DEBOUNCE_CTL
     Data = PcdGet32(PcdPONDebounce);
     result = SPMIProtocol->WriteLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_DEBOUNCE_CTL , &Data, 1);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result, &Status);
       goto ErrorExit;
     }
   }

ErrorExit:
   return Status;

}


/**
Initialize all PMIC GPIOs as input based on platform.
Also configure the power key

@param  pNumberOfKeys            Pointer to number of keys
@param  pKeyMap                  Pointer to key map.
@retval EFI_SUCCESS              Initialization successful
@retval non EFI_SUCCESS          Initialization failed

**/
EFI_STATUS ButtonsInit (
   UINT8         *pNumberofKeys,
   KEY_TYPE      *pKeyMap
   )
{
   EFI_STATUS Status;

   // get SPMIprotocol
   Status = gBS->LocateProtocol(&gQcomSPMIProtocolGuid, NULL, (VOID **) &SPMIProtocol);
   if ( EFI_ERROR (Status) )
   {
      DEBUG(( EFI_D_ERROR, "ButtonsInit: failed to locate SPMIProtocol, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
   }

   ASSERT(SPMIProtocol != NULL);


   // Intialize KeyMap
   Status = InitializeKeyMap( pNumberofKeys, pKeyMap );
   if ( EFI_ERROR(Status) )
   {
      DEBUG(( EFI_D_ERROR, "ButtonsInit: InitializeKeyMap() failed, Status =  (0x%x)\r\n", Status ));
      goto ErrorExit;
   }

   // Configure button GPIOs
   Status = ConfigureButtonGPIOs();
   if ( EFI_ERROR(Status) )
   {
      DEBUG(( EFI_D_ERROR, "ButtonsInit: ConfigureButtonGPIOs() failed, Status =  (0x%x)\r\n", Status ));
      goto ErrorExit;
   }


   // Configure Reset
   Status = ConfigureReset();
   if ( EFI_ERROR(Status) )
   {
      DEBUG(( EFI_D_ERROR, "ButtonsInit: ConfigureReset() failed, Status =  (0x%x)\r\n", Status ));
      goto ErrorExit;
   }


   // Configure PON Debounce
   Status = ConfigurePONDebounce();
   if ( EFI_ERROR(Status) )
   {
       DEBUG(( EFI_D_ERROR, "ButtonsInit: ConfigurePONDebounce() failed, Status =  (0x%x)\r\n", Status ));
      goto ErrorExit;
   }

ErrorExit:
   return Status;
}


/**
Read real time interrupt status on PMIC.

@param  ReadReg                  SPMI register on PMIC.
@param  pGpioButtonPressed       Pointer if GPIO activity (press) happened.

@retval EFI_SUCCESS              Retrieve status successfully
@retval non EFI_SUCCESS          Retrieve status failed

**/
EFI_STATUS ReadRealTimeIRQStatus(UINT32 ReadReg, UINT8 Mask, BOOLEAN *pGpioButtonPressed )
{
   EFI_STATUS Status = EFI_SUCCESS;
   Spmi_Result result = SPMI_SUCCESS;
   UINT32 ReadNum = 0;
   UINT8 ReadData = 0;

   if(SPMIProtocol)
   {
     result = SPMIProtocol->ReadLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, ReadReg, &ReadData, 1, &ReadNum);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result, &Status);
       goto ErrorExit;
     }

     if(ReadData & Mask)
     {
       *pGpioButtonPressed = TRUE;
     }
     else
     {
       *pGpioButtonPressed = FALSE;
     }
   }
ErrorExit:
   return Status;
}




/**
Read gpio status on PMIC.

@param  ReadReg                  SPMI register on PMIC.
@param  pGpioButtonPressed       Pointer if GPIO activity (press) happened..

@retval EFI_SUCCESS              Retrieve status successfully
@retval non EFI_SUCCESS          Retrieve status failed

**/
EFI_STATUS ReadGpioStatus(UINT32 ReadReg, UINT8 Mask, BOOLEAN *pGpioButtonPressed )
{
   EFI_STATUS Status = EFI_SUCCESS;
   Spmi_Result result = SPMI_SUCCESS;
   UINT32 ReadNum = 0;
   UINT8 ReadData = 0;

   if(SPMIProtocol)
   {
     result = SPMIProtocol->ReadLong(SPMIProtocol, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, ReadReg, &ReadData, 1, &ReadNum);
     if (SPMI_SUCCESS != result)
     {
       SPMIError(result, &Status);
       goto ErrorExit;
     }

     //GPIOs initially Pulled-Up
     if(ReadData & Mask)
     {
       *pGpioButtonPressed = FALSE;
     }
     else
     {
       *pGpioButtonPressed = TRUE;
     }
   }
ErrorExit:
   return Status;
}


/**
Poll button array.

@param  pButtonArray             Pointer to buttons array.

@retval EFI_SUCCESS              Retrieve matrix successfully
@retval non EFI_SUCCESS          Retrieve matrix failed

**/
EFI_STATUS PollButtonArray( UINT8 *pButtonArray )
{
   EFI_STATUS Status;
   BOOLEAN ButtonPressed = FALSE;

   //for Power Key
   Status = PollPowerKey(&ButtonPressed );
   if ( EFI_ERROR (Status) )
   {
     DEBUG(( EFI_D_ERROR, "PollButtonArray: PollPowerKey failed Status = (0x%x)\r\n", Status));
     goto ErrorExit;
   }
   *(pButtonArray + 0) = ButtonPressed;

   //for volume up
   Status = ReadGpioStatus( VOLUME_UP_BUTTON_GPIO_STATUS_REG, 0x01, &ButtonPressed );
   if ( EFI_ERROR (Status) )
   {
      DEBUG(( EFI_D_ERROR, "PollButtonArray: ReadGpioStatus failed for VOL+, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
   }
   *(pButtonArray + 1) = ButtonPressed;

   //for volume down
   Status = ReadRealTimeIRQStatus( PON_INT_RT_STS_REG, RESIN_ON_MASK, &ButtonPressed );
   if ( EFI_ERROR (Status) )
   {
      DEBUG(( EFI_D_ERROR, "PollButtonArray: ReadRealTimeIRQStatus/ReadResinBarkStatus failed for VOL-, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
   }
   *(pButtonArray + 2) = ButtonPressed;

   if( (PlatformType == EFI_PLATFORMINFO_TYPE_LIQUID)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_CDP) )
   {
      //for home
      Status = ReadGpioStatus( HOME_BUTTON_GPIO_STATUS_REG, 0x01, &ButtonPressed );
      if ( EFI_ERROR (Status) )
      {
         DEBUG(( EFI_D_ERROR, "PollButtonArray: ReadGpioStatus failed for HOME, Status = (0x%x)\r\n", Status));
         goto ErrorExit;
      }
      *(pButtonArray + 3) = ButtonPressed;
   }

   if( (PlatformType == EFI_PLATFORMINFO_TYPE_MTP_MSM)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_FLUID)||
       (PlatformType == EFI_PLATFORMINFO_TYPE_CDP))
   {
      //for camera snapshot
      Status = ReadGpioStatus( CAMERA_SNAPSHOT_BUTTON_GPIO_STATUS_REG, 0x01, &ButtonPressed );
      if ( EFI_ERROR (Status) )
      {
         DEBUG(( EFI_D_ERROR, "PollButtonArray: ReadGpioStatus failed for CAMERA SNAPSHOT, Status = (0x%x)\r\n", Status));
         goto ErrorExit;
      }
      *(pButtonArray + 4) = ButtonPressed;
   }

ErrorExit:
   return Status;
}

/**
Poll power key state from PMIC.

@param  pPowerKey                Pointer to power key state.

@retval EFI_SUCCESS              Retrieve power key status successfully
@retval non EFI_SUCCESS          Retrieve power key status failed

**/
EFI_STATUS PollPowerKey(BOOLEAN *pPowerKey)
{
  BOOLEAN PwrButtonPressed = FALSE;
  EFI_STATUS Status = EFI_INVALID_PARAMETER;

   Status = ReadRealTimeIRQStatus( PON_INT_RT_STS_REG, KPDPWR_ON_MASK, &PwrButtonPressed );
   if ( EFI_ERROR (Status) )
   {
      DEBUG(( EFI_D_ERROR, "PollPowerKey: ReadRealTimeIRQStatus failed for Power Button, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
   }
   *pPowerKey = PwrButtonPressed;

ErrorExit:
  return Status;
}

/**
  Set the value of isEfiKeyDetected .

  @param  flag  Boolean value .

  @retval none

**/
VOID SetEfiKeyDetection(BOOLEAN flag)
{
   isEfiKeyDetected = flag;
}

/**
  return the value of isHomeKeyDetected .

  @param  none .

  @retval Boolean value.

**/
BOOLEAN IsHomeKeyDetected(VOID)
{
    return isHomeKeyDetected;
}

/**
  return TRUE if Key press notification is supported .

  @param  none .

  @retval Boolean value.

**/
BOOLEAN IsNotificationSupported(VOID)
{
  return TRUE;
}


/**
  Prints the error strings on console

  @param[in] error    Error code defined in SpmiBus_ResultType

  @retval none

 */


VOID SPMIError (
	Spmi_Result error,
     EFI_STATUS *Status
     )
{
  switch (error)
  {
    case SPMI_FAILURE_INIT_FAILED:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:INIT_FAILED Status = (0x%x)\r\n", error));
      *Status = EFI_LOAD_ERROR;
      break;

    case SPMI_FAILURE_INVALID_PARAMETER:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:INVALID_PARAMETER Status = (0x%x)\r\n", error));
      *Status = EFI_INVALID_PARAMETER;
      break;

#if 0 // nsivadas - Remove after review
    case SPMI_BUS_FAILURE_GENERAL_FAILURE:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:GENERAL_FAILURE Status = (0x%x)\r\n", Status));
      break;
#endif
    case SPMI_FAILURE_TRANSACTION_FAILED:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:TRANSACTION_FAILED Status = (0x%x)\r\n", error));
      *Status = EFI_PROTOCOL_ERROR;
      break;

    case SPMI_FAILURE_TRANSACTION_DENIED:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:TRANSACTION_DENIED Status = (0x%x)\r\n", error));
      *Status = EFI_PROTOCOL_ERROR;
      break;

    case SPMI_FAILURE_TRANSACTION_DROPPED:
      DEBUG(( EFI_D_ERROR, "ButtonsInit: Error:TRANSACTION_DROPPED Status = (0x%x)\r\n", error));
      *Status = EFI_PROTOCOL_ERROR;
      break;

    default:
      break;
  }
}
