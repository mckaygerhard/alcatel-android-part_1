//============================================================================
/**
 * @file        qusb_dci_8996.c
 * @author      shreyasr
 * @date        19-June-2013
 *
 * @brief       QUSB (Qualcomm High-Speed USB) DCI (Device-Controller-Interface) PHY specific handling.
 *
 * @details      There might be different PHY types for different targets.
 *               When using 3-wired Full-Speed PHY the USB Core (Link) cannot conntrol the D+/D-.
 * 
 * @note 
 * 
 * @ref         Design Ware Controller spec "DWC_usb3_databook.pdf".
 *
 *              Copyright (c) 2013 QUALCOMM Technologies Incorporated.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 * 
 */
//============================================================================

// ===========================================================================
// 
//                            EDIT HISTORY FOR FILE
//   This section contains comments describing changes made to the module.
//   Notice that changes are listed in reverse chronological order.
// 
// 
// when          who     what, where, why
// ----------   -----    ----------------------------------------------------------
// 2015-02-26   ck       Removed include of TARGET_DEF_H
// 2013-06-19   shreyasr Added changes for MDM9x35
// 2012-04-14   tnk      Added flat file changes 
// 2010-04-14   yli      MSM8960 branch
// 2008-09-03   amirs    First Draft
// 
// ===========================================================================

//----------------------------------------------------------------------------
// Include Files
//----------------------------------------------------------------------------
#include "qusb_hwio_dependency_8996.h"
#include "ClockBoot.h"
#include "com_dtypes.h"           // common defines - basic types as uint32 etc
#include "qusb_dci_api.h"         // DCI API Prototypes
#include "qusb_dci_private.h"     // dQH_t
#include "qusb_log.h" 
#include "HALhwio.h"              // For OUTF and INF definitions

//----------------------------------------------------------------------------
// Preprocessor Definitions and Constants
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// Type Declarations
//----------------------------------------------------------------------------
typedef struct
{
  uint16 base_addr;
  uint16 depth;
} qusb_dci_tx_fifo_t;

//----------------------------------------------------------------------------
// Global Data Definitions
//----------------------------------------------------------------------------
core_info_t dci_core_info[DCI_MAX_CORES];

#define QUSB_DLOAD_INFO_ADDR_IN_IMEM              (SHARED_IMEM_USB_BASE)
//defines for enabling the 1.5k pull up 
#define QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_EN     (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN   (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS   (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_SHFT)
#define QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE          (0x0 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE_SHFT)

#define QUSB_LINESTATE_CHECK_DELAY                (0x5)
#define QUSB_LINESTATE_CHECK_RETRY_CNT            (10000)  //50msec is the total wait time to deal with a bad cable

// Assume a 300 ohm SMB charger resistor on D+/D- which corresponds to HSTX_TRIM offset of value 2
// Subtract QUSB_HSTX_TRIM_OFFSET to make amplitude overshoot by 400mV to account for longer cables and hubs.

#define QUSB_HSTX_TRIM_OFFSET                     (2)
#define QUSB_HS_PHY_PLL_BMSK                      (0x1)
#define QUSB_HS_PHY_PLL_MAX_CNT                   (20000)

#define PMI_8994_INDEX                            (1)
#define USB3_PHY_PLL_FREQ                         (10.0e9)
#define USB3_PHY_PLL_TOLERANCE_LARGE              (40.0e6)
#define USB3_PHY_PLL_TOLERANCE_SMALL              (10.0e6)
#define USB3_PHY_VCO_TUNE_MIN_VAL                 (0x1)
#define USB3_PHY_VCO_TUNE_REC_VAL                 (0x13F)
#define USB3_PHY_VCO_TUNE_MAX_VAL                 (0x3FF)
#define USB3_PHY_VCO_TUNE_STEP_LARGE              (5)
#define USB3_PHY_VCO_TUNE_STEP_SMALL              (1)
#define USB3_PHY_REF_CLOCK_FREQ_HZ                (19200000)
#define USB3_PHY_PLL_CNT_DEC                      (1024)


#define QUSB_DCI_MAX_TX_FIFO_SIZE  (0xA65)
#define QUSB_DCI_MAX_TX_FIFO_NUM   (16)


//----------------------------------------------------------------------------
// Static Variable Definitions
//----------------------------------------------------------------------------

//============================================================================
// QUSB Super-Speed PHY Configuration Array
//============================================================================
#define QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT           (64)
#define QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_COMMON    (56)
#define QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_V1        (60)
#define QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_V2        (64)

// Values according to SS QMP PHY spec. given out by PHY DV Team Apr 2015 v9
static const uint32 qusb_dci_ss_phy_cfg_address_common[QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT] =
{
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYSCLK_EN_SEL_ADDR,                    // 00: (0x14)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,              // 01: (0x08)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CLK_SELECT_ADDR,                       // 02: (0x30)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_CONFIG_ADDR,                       // 03: (0x06)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                 // 04: (0x01)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                        // 05: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TRIM_ADDR,                          // 06: (0x0F)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_IVCO_ADDR,                         // 07: (0x0F)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYS_CLK_CTRL_ADDR,                     // 08: (0x04)
    QUSB_HWIO_ADDR_EMPTY,                                                     // 09: (0x00)
    QUSB_HWIO_ADDR_EMPTY,                                                     // 10: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEC_START_MODE0_ADDR,                  // 11: (0x82)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,            // 12: (0x55)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,            // 13: (0x55)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,            // 14: (0x03)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CP_CTRL_MODE0_ADDR,                    // 15: (0x0B)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                  // 16: (0x16)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                  // 17: (0x28)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,            // 18: (0x80)
    QUSB_HWIO_ADDR_EMPTY,                                                     // 19: (0x00)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                  // 20: (0x15)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                  // 21: (0x34)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                  // 22: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_ADDR,                      // 23: (0x00)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_ADDR,                     // 24: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_ADDR,                     // 25: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TIMER_ADDR,                         // 26: (0x0A)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_EN_CENTER_ADDR,                    // 27: (0x01)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER1_ADDR,                         // 28: (0x31)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER2_ADDR,                         // 29: (0x01)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER1_ADDR,                     // 30: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER2_ADDR,                     // 31: (0x00)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE1_ADDR,                   // 32: (0xDE)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE2_ADDR,                   // 33: (0x07)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,             // 34: (0x0B)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_SO_GAIN_ADDR,                      // 35: (0x04)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,             // 36: (0x02)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_ADDR,             // 37: (0x4C)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_ADDR,             // 38: (0xBB)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQ_OFFSET_ADAPTOR_CNTRL1_ADDR,       // 39: (0x77)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_OFFSET_ADAPTOR_CNTRL2_ADDR,          // 40: (0x80)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_CNTRL_ADDR,                      // 41: (0x03)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_LVL_ADDR,                        // 42: (0x1B)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,             // 43: (0x16)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,  // 44: (0x45)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_RCV_DETECT_LVL_2_ADDR,                  // 45: (0x12)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_LANE_MODE_ADDR,                         // 46: (0x06)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL2_ADDR,                          // 47: (0x03)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL1_ADDR,                          // 48: (0x02)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_L_ADDR,                       // 49: (0x09)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_H_TOL_ADDR,                   // 50: (0x42)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_MAN_CODE_ADDR,                        // 51: (0x85)
    //-------------------------------------------------------------------------------------
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG1_ADDR,                 // 52: (0xD1)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG2_ADDR,                 // 53: (0x1F)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG3_ADDR,                 // 54: (0x47)
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_STATE_CONFIG2_ADDR,                 // 55: (0x08)
    //----------------------- END OF COMMON BLOCK - INCLUDES V2.X -------------------------
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                        // 56: (0x01)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_ADDR,                    // 57: (0x1C)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE0_ADDR,                  // 58: (0x3F)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE0_ADDR,                  // 59: (0x01)
    //------------------------------ END OF V1 BLOCK --------------------------------------
    QUSB_HWIO_ADDR_EMPTY,                                                     // 60: (0x00)
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                        // 61: (0x01)
    QUSB_HWIO_ADDR_EMPTY,                                                     // 62: (0x00)
    QUSB_HWIO_ADDR_EMPTY,                                                     // 63: (0x00)
    //------------------------------ END OF V2 BLOCK --------------------------------------
};

ALIGN(4)
static const uint8 qusb_dci_ss_phy_cfg_value_common[QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT] =
{
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYSCLK_EN_SEL_ADDR,                     00 */ (0x14),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,               01 */ (0x08),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CLK_SELECT_ADDR,                        02 */ (0x30),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_CONFIG_ADDR,                        03 */ (0x06),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                  04 */ (0x01),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                         05 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TRIM_ADDR,                           06 */ (0x0F),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_IVCO_ADDR,                          07 */ (0x0F),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYS_CLK_CTRL_ADDR,                      08 */ (0x04),
    /* QUSB_HWIO_ADDR_EMPTY,                                                      09 */ (0x00),
    /* QUSB_HWIO_ADDR_EMPTY,                                                      10 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEC_START_MODE0_ADDR,                   11 */ (0x82),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,             12 */ (0x55),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,             13 */ (0x55),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,             14 */ (0x03),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CP_CTRL_MODE0_ADDR,                     15 */ (0x0B),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                   16 */ (0x16),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                   17 */ (0x28),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,             18 */ (0x80),
    /* QUSB_HWIO_ADDR_EMPTY,                                                      19 */ (0x00),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                   20 */ (0x15),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                   21 */ (0x34),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                   22 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_ADDR,                       23 */ (0x00),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_ADDR,                      24 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      25 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TIMER_ADDR,                          26 */ (0x0A),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_EN_CENTER_ADDR,                     27 */ (0x01),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER1_ADDR,                          28 */ (0x31),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER2_ADDR,                          29 */ (0x01),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER1_ADDR,                      30 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER2_ADDR,                      31 */ (0x00),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE1_ADDR,                    32 */ (0xDE),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE2_ADDR,                    33 */ (0x07),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,              34 */ (0x0B),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_SO_GAIN_ADDR,                       35 */ (0x04),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,              36 */ (0x02),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_ADDR,              37 */ (0x4C),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_ADDR,              38 */ (0xBB),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQ_OFFSET_ADAPTOR_CNTRL1_ADDR,        39 */ (0x77),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_OFFSET_ADAPTOR_CNTRL2_ADDR,           40 */ (0x80),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_CNTRL_ADDR,                       41 */ (0x03),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_LVL_ADDR,                         42 */ (0x18),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,              43 */ (0x16),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,   44 */ (0x45),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_RCV_DETECT_LVL_2_ADDR,                   45 */ (0x12),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_LANE_MODE_ADDR,                          46 */ (0x06),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL2_ADDR,                           47 */ (0x03),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL1_ADDR,                           48 */ (0x02),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_L_ADDR,                        49 */ (0x09),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_H_TOL_ADDR,                    50 */ (0x42),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_MAN_CODE_ADDR,                         51 */ (0x85),
    //-----------------------------------------------------------------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG1_ADDR,                  52 */ (0xD1),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG2_ADDR,                  53 */ (0x1F),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG3_ADDR,                  54 */ (0x47),
    /* HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_STATE_CONFIG2_ADDR,                  55 */ (0x08),
    //---------------------------------- END OF COMMON BLOCK ----------------------------------
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                         56 */ (0x01),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_ADDR,                     57 */ (0x1C),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE0_ADDR,                   58 */ (0x3F),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE0_ADDR,                   59 */ (0x01),
    //---------------------------------- END OF V1 BLOCK --------------------------------------
    /* QUSB_HWIO_ADDR_EMPTY,                                                      60 */ (0x00),
    /* HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                         61 */ (0x01),
    /* QUSB_HWIO_ADDR_EMPTY,                                                      62 */ (0x00),
    /* QUSB_HWIO_ADDR_EMPTY,                                                      63 */ (0x00),
    //---------------------------------- END OF V2 BLOCK --------------------------------------
};

#define QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT          (128)
#define QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX                       (8)

// initialization without reading the foundry information
static uint32 qusb_dci_ss_phy_foundry_id = 0;

// Array keeps tracking of starting index of override programming sequence
// per foundry
static const uint32 qusb_dci_ss_phy_cfg_override_start_index[QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX] =
{ 
    0,    // 0
    16,   // 1
    32,   // 2
    48,   // 3
    64,   // 4
    80,   // 5
    96,   // 6
    112,  // 7
};

// Between 2 foundries, there must be QUSB_HWIO_ADDR_END as sentinel value to
// prevent going over the boundry.
static const uint32 qusb_dci_ss_phy_cfg_address_override[QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT] = 
{
    //  0
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 16
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 32
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 48
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 64
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 80
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 96
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 112
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 128
};
ALIGN(4)
static const uint8 qusb_dci_ss_phy_cfg_value_override[QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT]    = 
{
  //  0
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 16
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 32
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 48
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 64
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 80
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 96
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 112
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 128
};

//============================================================================
// QUSB High-Speed PHY Configuration Array
//============================================================================

#define QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT      (12)

// It is recommended to set the CSR bit "r_power_down = 1'b1", program any 
// hardware tuning (such as mentioned in #1 above)/config bits/etc., then 
// release "r_power_down = 1'b0" once again, in the QUSB2PHY_POWER_DOWN 
// register.

static const uint32 qusb_dci_hs_phy_cfg_address[QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE1_ADDR,          // 0  : (0xF8)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_ADDR,          // 1  : (0xB3)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE3_ADDR,          // 2  : (0x83)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE4_ADDR,          // 3  : (0xC0)
  //------------------------------------------------------------------------------------------
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TUNE_ADDR,            // 4  : (0x30)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL1_ADDR,       // 5  : (0x79)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL2_ADDR,       // 6  : (0x21)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TEST2_ADDR,          // 7  : (0x14)
  //------------------------------------------------------------------------------------------  
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_PWR_CTL_ADDR,         // 8  : (0x00) 
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,    // 9  : (0x9F)
  QUSB_HWIO_ADDR_EMPTY,                                           // 10 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                           // 11 : (0x00)
};


static const uint8 qusb_dci_hs_phy_cfg_value[QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE1_ADDR,          0  */ (0xF8),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_ADDR,          1  */ (0xB3),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE3_ADDR,          2  */ (0x83),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE4_ADDR,          3  */ (0xC0),
  //------------------------------------------------------------------------------------------
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TUNE_ADDR,            4  */ (0x30),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL1_ADDR,       5  */ (0x79),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL2_ADDR,       6  */ (0x21),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TEST2_ADDR,          7  */ (0x14),
  //------------------------------------------------------------------------------------------  
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_PWR_CTL_ADDR,         8  */ (0x00),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,    9  */ (0x9F),
  /* QUSB_HWIO_ADDR_EMPTY,                                           10 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                           11 */ (0x00),
};

/*

Formula to calculate optimal TxFIFO size: TxFIFOSize (in mdwidth) = (BURST_SIZE*(1024+mdwidth))+mdwidth
Mdwidth = 64bit bus / 8bits = 8byte.

BULK IN EP:
* Total TxFIFO size in MSM8996 in (mdwidth) = 0xA65 or 21288 bytes i.e. ~20Kbytes
* In SBL, TxFIFO size is set to HW default i.e. 0x184(mdwidth) or ~3Kbytes. If device advertises NumP=4, this might cause flow control (throughput loss) by setting EOB=Y on every third packet.
* For IN EP, NumP=4/8 and FIFOSize = 0x205/0x40A i.e. ~4/8Kbytes, seems optimal. 
* Device has to honor host�s(receiver�s) NumP. Since Win7 xHCI advertises NumP=4, NumP > 4 does not seem to make much difference.
* Usage of TxFIFO:
  * Since each TxFIFO maps to GTxFIFOSiz(n) register, I have selected linear mapping for TxFIFO.
  * Thus Ctrl endpoint TX/RX uses same FIFONum = 0.
 
BULK OUT EP:
* Total shared RxFIFO size in MSM8996 (mdwidth) = 0x305 or 6Kbytes.
* For OUT EP, FIFOSize is shared and fixed to 0x305 i.e. ~6Kbytes hence use NumP=6.
* In SBL, DCFG.NumP = 4 by default. Setting NumP=6 optimizes use of RxFIFO further.
* NumP used for RX endpoint needs to be programmed in DCFG.NumP

*/

static qusb_dci_tx_fifo_t qusb_dci_tx_fifo[QUSB_DCI_MAX_TX_FIFO_NUM] = 
{
  {0x0000, 0x0042}, // 0
  {0x0042, 0x040A}, // 1
  {0x044C, 0x0000}, // 2
  {0x044C, 0x0000}, // 3
  {0x044C, 0x0000}, // 4
  {0x044C, 0x0000}, // 5
  {0x044C, 0x0000}, // 6
  {0x044C, 0x0000}, // 7
  {0x044C, 0x0000}, // 8
  {0x044C, 0x0000}, // 9
  {0x044C, 0x0000}, // 10
  {0x044C, 0x0000}, // 11
  {0x044C, 0x0000}, // 12
  {0x044C, 0x0000}, // 13
  {0x044C, 0x0000}, // 14
  {0x044C, 0x0000}, // 15
};

//----------------------------------------------------------------------------
// Static Function Declarations and Definitions
//----------------------------------------------------------------------------

static boolean qusb_dci_allocate_tx_fifo(void)
{
  uint32 base_addr;
  uint32 depth;
  uint32* addr;
  int i;

  addr = (uint32*)HWIO_GTXFIFOSIZ_REGS_p_GTXFIFOSIZ0_ADDR(0);
  
  for(i=0; i<QUSB_DCI_MAX_TX_FIFO_NUM; i++)
  {
    base_addr = (qusb_dci_tx_fifo[i].base_addr << HWIO_GTXFIFOSIZ_REGS_p_GTXFIFOSIZ0_TXFSTADDR_N_SHFT) 
                 & HWIO_GTXFIFOSIZ_REGS_p_GTXFIFOSIZ0_TXFSTADDR_N_BMSK;
    depth = (qusb_dci_tx_fifo[i].depth << HWIO_GTXFIFOSIZ_REGS_p_GTXFIFOSIZ0_TXFDEP_N_SHFT) 
            & HWIO_GTXFIFOSIZ_REGS_p_GTXFIFOSIZ0_TXFDEP_N_BMSK;
    addr[i] = (base_addr | depth);
    if((qusb_dci_tx_fifo[i].base_addr+qusb_dci_tx_fifo[i].depth)>QUSB_DCI_MAX_TX_FIFO_SIZE)
    {
      return FALSE;
    }
  }
  return TRUE;
}

static void qusb_dci_phy_reg_array_process
(
  const uint32 *address_array, 
  const uint8  *value_array, 
  uint32        start_index, 
  uint32        array_entry_cnt
);

//----------------------------------------------------------------------------
// Externalized Function Definitions
//----------------------------------------------------------------------------


//============================================================================
/**
 * @function    qusb_get_shared_imem_address
 * 
 * @brief   	Gets the shared imem address based on the hardware version
 * 
 * @note    	None          
 *  
 * @param   	None
 *
 * @return 		uint32 A 32-Bit Address
 *
 * @ref 
 *
 */
// ===========================================================================
uint64 qusb_get_shared_imem_address()
{
  return QUSB_DLOAD_INFO_ADDR_IN_IMEM;
}

// ===========================================================================
/**
 * @function    qusb_hs_phy_refclk_enable
 * 
 * @brief   This function will be used to enable / disable HS PHY reference clock.
 * 
 * @param  TRUE or FALSE depending on enable or disable.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_hs_phy_refclk_enable(boolean enable)
{
  HWIO_GCC_RX1_USB2_CLKREF_EN_OUTM(HWIO_GCC_RX1_USB2_CLKREF_EN_RX1_USB2_ENABLE_BMSK,
    enable << HWIO_GCC_RX1_USB2_CLKREF_EN_RX1_USB2_ENABLE_SHFT);

  HWIO_GCC_RX1_USB2_CLKREF_EN_OUTM(HWIO_GCC_RX1_USB2_CLKREF_EN_SW_RXTAP1_EN_BMSK,
    enable << HWIO_GCC_RX1_USB2_CLKREF_EN_SW_RXTAP1_EN_SHFT);

}

// ===========================================================================
/**
 * @function    qusb_dci_enable_usb30_clocks
 * 
 * @brief   This function will be used to turn ON the USB3.0 clocks
 * 
 * @param   None
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
boolean qusb_dci_enable_usb30_clocks(void)
{
  boolean var = TRUE;
#if !defined(BUILD_HOSTDL) && !defined(BUILD_EHOSTDL) && !defined(FEATURE_EMMCBLD)
  var = Clock_InitUSB();
#endif
  return (var);
}

// ===========================================================================
/**
 * @function    qusb_dci_disable_usb30_clocks
 * 
 * @brief   This function will be used to turn OFF the USB3.0 clocks
 * 
 * @param   None
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
boolean qusb_dci_disable_usb30_clocks(void)
{
  boolean var = TRUE; 
#if !defined(BUILD_HOSTDL) && !defined(BUILD_EHOSTDL) && !defined(FEATURE_EMMCBLD)
  var = Clock_DisableUSB();
#endif

  /** Disable HS PHY reference clock **/
  qusb_hs_phy_refclk_enable(FALSE);

  return (var);
}

// ===========================================================================
/**
 * @function    qusb_dci_enable_usb30_power
 * 
 * @brief   This function enables the power domain for SNPS controller
 *
 * @param   TRUE: It enables the s/w Power Collapse 
 *          FALSE:  IT disables the s/w Power Collapse
 * 
 * @details  Used for enabling the power domain for SNPS controller
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
void qusb_dci_enable_usb30_power_collapse(boolean enable)
{
  Clock_Usb30EnableSWCollapse(enable);
}

// ===========================================================================
/**
 * @function    qusb_dci_is_usb30_sw_collapsed
 * 
 * @brief   This function reads back if  the power domain for SNPS 
            controller is power collapsed.
 * 
 * @details Used fr cheking if USB core was initialized in PBL.
 *          If not then single enumeration cannot continue, 
 *          USB core needs to be re-enumerated.
 * 
 * @return  TRUE or FALSE depending on status of SW collapse bit.
 * 
 */
// ===========================================================================
boolean qusb_dci_is_usb30_sw_collapsed(void)
{
  return Clock_Usb30GetSWCollapse();
}

// ===========================================================================
/**
 * @function    qusb_dci_get_hardware_id
 * 
 * @brief   This function will be used for getting the SNPS h/w id.
 * 
 * @param   None
 * 
 * @return  uint32 - hardware id. 
 * 
 */
// ===========================================================================
uint32 qusb_dci_get_hardware_id(void)
{
  uint32 hw_id =0x0;
  hw_id = HWIO_GSNPSID_INM(HWIO_GSNPSID_SYNOPSYSID_BMSK);
  return hw_id;
}

// ===========================================================================
/**
 * @function    qusb_dci_hs_phy_update_hstx_trim
 * 
 * @brief   This function will update TUNE2 HSTX_TRIM register bits if feature is enabled.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_dci_hs_phy_update_hstx_trim(void)
{
  uint8 hstx_trim_val = 
    (HWIO_QFPROM_CORR_CALIB_ROW12_MSB_INM(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_BMSK) 
    >> HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_SHFT);

  if(hstx_trim_val)
  {
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_BMSK,
        hstx_trim_val << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_SHFT);
  }
}

// ------------------------------------------------------------------------------------------------
// Core Initialization APIs
// ------------------------------------------------------------------------------------------------
// ===========================================================================
/**
 * @function    qusb_hs_phy_init
 * 
 * @brief   API used to initialize the High Speed PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_hs_phy_init(void)
{
    uint8 pll_status = 0;
    uint32 pll_status_cnt = 0;

    /* Keep the PHY in power down mode */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

    qusb_dci_phy_reg_array_process(
          qusb_dci_hs_phy_cfg_address,
          qusb_dci_hs_phy_cfg_value,
          0,
          QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT);

    // Overwrite HSTX TRIM calibration value
    qusb_dci_hs_phy_update_hstx_trim();

    /* Bring Out  the PHY from power down mode */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x0 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

    /* The delay is required for PHY PLL capacitor to get charged / ramp up */
    qusb_dci_delay_us(150);

    /* Enable/Select sourcing single ended reference clock to PHY */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TEST_OUT(0x80);

    /* Wait maximum of ~1ms for PLL to lock */
    for(pll_status_cnt = 0; (FALSE == pll_status) && (pll_status_cnt < QUSB_HS_PHY_PLL_MAX_CNT); pll_status_cnt++)
    {
      qusb_dci_delay_us(10);
      
      pll_status = (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_INM
                    (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_PLL_STATUS_BMSK)
                    >> HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_PLL_STATUS_SHFT);

      pll_status &= QUSB_HS_PHY_PLL_BMSK;
    
    }
  
    if(FALSE == pll_status)
    {
      qusb_error_log(DCI_HS_PHY_PLL_FAILURE_LOG, 0, pll_status_cnt);
      qusb_uart_log("PLL lock failed", pll_status_cnt);
    }
    else
    {
      qusb_uart_log("PLL lock success", pll_status_cnt);
    }
}

// ===========================================================================
/**
 * @function    qusb_dci_configure_device_mode
 * 
 * @brief   This is used for configuring the Device config Register post reset.
 * 
 * @param   qusb_max_speed_required_t Maximum speed at which the device can operate
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_configure_device_mode(qusb_max_speed_required_t speed_required)
{

  qusb_device_connect_speed_t connect_speed;

  switch(speed_required)
  {
    case QUSB_MAX_SPEED_SUPER:
    {
      connect_speed = QUSB_SUPER_SPEED;
    }
    break;
    
    case QUSB_MAX_SPEED_HIGH:
    {
      connect_speed = QUSB_HIGH_SPEED;
    }
    break;

    default:
    {
      connect_speed = QUSB_FULL_SPEED;
    }
  }

  // Configure the Minimum negotiatiated Speed to  HS: 0x0 , SS: 0x4
  HWIO_DCFG_OUTM(HWIO_DCFG_DEVSPD_BMSK, (connect_speed << HWIO_DCFG_DEVSPD_SHFT));

  /*set the device address to 0x0 after reset*/ 
  HWIO_DCFG_OUTM(HWIO_DCFG_DEVADDR_BMSK,(0x0 << HWIO_DCFG_DEVADDR_SHFT)); 

  /*LPM Capability EN: 0x1, DIS:0x0*/
  HWIO_DCFG_OUTM(HWIO_DCFG_LPMCAP_BMSK,(0x0 << HWIO_DCFG_LPMCAP_SHFT));

  // Update NumP based on default RxFIFO
  HWIO_DCFG_OUTM(HWIO_DCFG_NUMP_BMSK,(QUSB_BULK_OUT_BURST_SIZE << HWIO_DCFG_NUMP_SHFT));

  // Allocate TxFIFO based on table.
  if(FALSE == qusb_dci_allocate_tx_fifo())
  {
    qusb_error_log(DCI_INSUFFCIENT_HW_RSRC_LOG, 0,__LINE__);
  }

  /* Enable Device mode operation */
  HWIO_GCTL_OUTM(HWIO_GCTL_PRTCAPDIR_BMSK, (0x2 << HWIO_GCTL_PRTCAPDIR_SHFT )); 

  return;
}


// ===========================================================================
/**
 * @function    qusb_dci_configure_usb30
 * 
 * @brief   API used for PHY configuration in device Mode for USB3.0 
 *  
 * @details This API is used for configuring based on the HPG specifications 
 * 			Please follow the same steps for configuration 
 * 
 * @param   qusb_max_speed_required_t Maximum speed at which the device should
 *          operate. 
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_configure_usb30(qusb_max_speed_required_t speed_required)
{

  // Set DELAYP1TRANS to 0 
  HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_OUTMI(0, HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_DELAYP1TRANS_BMSK,
                               ((uint32) 0x0 << HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_DELAYP1TRANS_SHFT));
  qusb_dci_delay_us(100);

  // Controller Configuration to Peripheral mode 
  qusb_dci_configure_device_mode(speed_required);

  return;
}

// ===========================================================================
/**
 * @function    qusb_dci_usb30_gcc_reset
 * 
 * @brief   API used for resetting the Link and PHYs using GCC control
 *  
 * @details This API is used for resetting the Link and PHYs using clock control 
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_usb30_gcc_reset(void)
{
  // Reset SNPS Link controller 
  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  // Reset QUSB (USB 2.0) and QMP (USB 3.0) PHYs 
  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));

  return; 
}

// ===========================================================================
/**
 * @function    qusb_dci_select_utmi_clk
 * 
 * @brief   This is used for configuring the core to UTMI clock instead of pipe
 *          clock.  This needs to be called when there is no SS USB PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_select_utmi_clk(void)
{
  qusb_error_log(QUSB_DCI_SELECT_UTMI_CLK_LOG, 0, 0);
  
  // If operating without SS PHY, follow this sequence to disable 
  // pipe clock requirement
  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
                      0x1 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);

  qusb_dci_delay_us(100);

  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_SEL_BMSK,
                      0x1 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_SEL_SHFT);

  qusb_dci_delay_us(100);
  
  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE3_PHYSTATUS_SW_BMSK,
    0x1 << HWIO_GENERAL_CFG_PIPE3_PHYSTATUS_SW_SHFT);

  qusb_dci_delay_us(100);

  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
                      0x0 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);
}

// ===========================================================================
/**
 * @function    qusb_dci_write_vco_tune_val
 * 
 * @brief   This is used for configuring the VCO TUNE value for SS QMP PHY
 * 
 * @param   mode_select - SS PHY VCO frequency mode.
 *          vco_tune_val -  VCO tune value to write.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_dci_write_vco_tune_val(uint8 mode_select, uint16 vco_tune_val)
{
  // qusb_error_log(SS_PHY_VCO_TUNE_VAL_LOG, 0, vco_tune_val);
  switch(mode_select)
  {
    case 0:
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE0_OUT(vco_tune_val & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE0_PLL_VCOTUNE_MODE0_7_0_BMSK);
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE0_OUT((vco_tune_val >> 0x8) & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE0_PLL_VCOTUNE_MODE0_9_8_BMSK);
    break;

    case 1:
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE1_OUT(vco_tune_val & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE1_PLL_VCOTUNE_MODE1_7_0_BMSK);
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE1_OUT((vco_tune_val >> 0x8) & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE1_PLL_VCOTUNE_MODE1_9_8_BMSK);
    break;

    case 2:
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE2_OUT(vco_tune_val & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE2_PLL_VCOTUNE_MODE2_7_0_BMSK);
      HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE2_OUT((vco_tune_val >> 0x8) & HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE2_PLL_VCOTUNE_MODE2_9_8_BMSK);
    break;

    default:
      qusb_error_log(DCI_DEFAULT_CASE_LOG, 0, __LINE__);
    break;
  }
}

// ===========================================================================
/**
 * @function    qusb_read_pll_freq_hz
 * 
 * @brief   This API is used read to  SS PHY PLL frequency from SS PHY debug bus.
 * 
 * @param   mode_select - SS PHY VCO frequency mode.
 * 
 * @return  Frequency in GHz.
 * 
 */
// ===========================================================================
static double qusb_read_pll_freq_hz(uint8 mode_select)
{
  uint8  debug_bus3; 
  uint8  debug_bus2;
  uint8  debug_bus1;
  uint8  debug_bus0;
  uint8  debug_temp3;
  uint8  debug_temp1;
  uint8  pll_cnt; 
  uint8  core_clk_div;
  uint8  hs_clk_div;
  uint8  pll_div_sel;
  double freq_code;
  double freq;

  // Enable the COM LOCK STOP COUNT for the PLL, so that the COUNTER gets incremented exactly for 1ms duration.  
  // The rate at which PLL count gets incremented will be proportionate to the PLL's clock frequency.
  HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_OUTM(
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_PLLLOCK_STOP_COUNT_BMSK, 
    0x0 << HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_PLLLOCK_STOP_COUNT_SHFT);

  qusb_dci_delay_ms(1);

  // Disable the COM LOCK STOP COUNT for the PLL
  HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_OUTM(
   HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_PLLLOCK_STOP_COUNT_BMSK, 
   0x1 << HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_PLLLOCK_STOP_COUNT_SHFT);

  // Allow PLL COUNT to settle down before reading.
  qusb_dci_delay_ms(1);

  debug_bus3 = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS3_IN;
  debug_bus2 = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS2_IN;
  debug_bus1 = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS1_IN;
  debug_bus0 = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS0_IN;
  debug_temp3 = debug_bus3 & 0x7;
  debug_temp1 = debug_bus1 & 0xFE;

  // Read debug bus and calculate frequency code which needs to be converted to Hz further.
  freq_code = (debug_temp3*32768) + (debug_bus2*128) + (debug_temp1>>1);
  // qusb_log(,0, freq_code);

  pll_cnt = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_EN_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_EN_PLLLOCK_CNT_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_EN_PLLLOCK_CNT_SHFT;

  switch(mode_select)
  {
    case 0:
      core_clk_div = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_CORE_CLK_DIV_BMSK);
      hs_clk_div   = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE0_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE0_SHFT;
      pll_div_sel  = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE0_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE0_SHFT;
    break;

    case 1:
      core_clk_div = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_CORE_CLK_DIV_BMSK);
      hs_clk_div   = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE1_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE1_SHFT;
      pll_div_sel  = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE1_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE1_SHFT;
    break;

    case 2:
      core_clk_div = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORECLK_DIV_CORE_CLK_DIV_BMSK);
      hs_clk_div   = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE2_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_HSCLK_DIVSEL_MODE2_SHFT;
      pll_div_sel  = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_INM(HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE2_BMSK) >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_CLKS_PLL_DIVSEL_MODE2_SHFT;
    break;

    default:
      qusb_error_log(DCI_DEFAULT_CASE_LOG, 0, __LINE__);
      return 0;
    break;
  }

  hs_clk_div  += 1;
  pll_div_sel  = 2 << pll_div_sel;
  freq=((freq_code/USB3_PHY_PLL_CNT_DEC)*USB3_PHY_REF_CLOCK_FREQ_HZ)*core_clk_div*pll_div_sel*hs_clk_div;
  // qusb_error_log(SS_PHY_FREQ_MHZ_LOG, 0, freq/10.0e6);
  return freq;
}

// ===========================================================================
/**
 * @function    qusb_dci_find_optimal_vco_tune
 * 
 * @brief   This API is finds optimal VCO tune value.
 * 
 * @param   mode_select - SS PHY VCO frequency mode.
 * 
 * @return  Optimal VCO tune value.
 * 
 */
// ===========================================================================
static uint16 qusb_dci_find_optimal_vco_tune(uint8 mode_select)
{
  double current_freq;
  double freq_delta;
  double min_freq;
  double min_delta = USB3_PHY_PLL_FREQ;
  uint16 vco_tune_val = USB3_PHY_VCO_TUNE_MIN_VAL;
  uint16 vco_tune_optimal = USB3_PHY_VCO_TUNE_REC_VAL;
  uint16 vco_tune_val_min_delta = USB3_PHY_VCO_TUNE_REC_VAL;

    /*Put the PHY and CSR blocks into RESET*/
    HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,
      (0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
    
    /*Put the PHY block into RESET*/
    HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, 
      (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));
    
    qusb_dci_delay_us(100);
    
    /*Release the CSR block from RESET*/
    HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,
      (0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
    
    qusb_dci_delay_us(100);
    
    // Write 0x11 to AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG so that 
    // writes and reads to/from the PHY use one wait state.
    HWIO_PERIPH_SS_AHB2PHY_TOP_CFG_OUT(0x11);
    
    /* Release the PHY from Power Down mode */
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_OUTM(
      HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, 
      (0x1 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));
    
    // Configure the QMP PHY as per Table 4-1 of the QMP PHY HPG with default entry
    qusb_dci_phy_reg_array_process(
            qusb_dci_ss_phy_cfg_address_common, 
            qusb_dci_ss_phy_cfg_value_common,
             0,
            QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT);
    
    // Read foundry specific information and program addition overrides
    // qusb_dci_ss_phy_foundry_id = pbl_get_foundry_id();
    if (qusb_dci_ss_phy_foundry_id >= QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX)
    {
      qusb_error_log(INVALID_QUSB_DCI_SS_PHY_FOUNDRY_ID_LOG, 0, qusb_dci_ss_phy_foundry_id);
    }
    else
    {
      qusb_dci_phy_reg_array_process(
            qusb_dci_ss_phy_cfg_address_override, 
            qusb_dci_ss_phy_cfg_value_override, 
            qusb_dci_ss_phy_cfg_override_start_index[qusb_dci_ss_phy_foundry_id],
            QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT);
    }
    
    /* Release the PHY block from Reset */
    HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));
    
    qusb_dci_delay_us(100);
    
    /* Release the PHY from SW Reset state */
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_OUTM(HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_SW_RESET_BMSK, (0x0 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_SW_RESET_SHFT));
    
    /* Start PHY operation (begin with internal power-up & calibration) */
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_START_CONTROL_OUT(0x03);

    // Enable debug bus
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS_SEL_OUT(0x4);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_MISC1_OUT(0x19);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_INTEGLOOP_EN_OUT(0x4);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OUT(0x4);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_IVCO_OUT(0xF);

    // Find optimal VCO Tune.
    switch(mode_select)
    {
      case 0:
        HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OUT(0x4);
      break;

      case 1:
        HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OUT(0x8);
      break;

      case 2:
        HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OUT(0x10);
      break;
      default:
        qusb_error_log(DCI_DEFAULT_CASE_LOG, 0, __LINE__);
      break;
    }

    do
    {
      qusb_dci_write_vco_tune_val(mode_select, vco_tune_val);
      current_freq = qusb_read_pll_freq_hz(mode_select);
      freq_delta = (current_freq > USB3_PHY_PLL_FREQ) ? (current_freq-USB3_PHY_PLL_FREQ) : (USB3_PHY_PLL_FREQ-current_freq);

      // Discontinue when PLL hits dead zone.
      if(0x0 == current_freq )
      {
        break;
      }
      
      // Exit if delta is within lower tolerance  limit.
      if(freq_delta < USB3_PHY_PLL_TOLERANCE_SMALL)
      {
        min_freq = current_freq;
        vco_tune_optimal = vco_tune_val;
        break;
      }

      if(freq_delta < min_delta) 
      { 
        min_freq = current_freq;
        min_delta = freq_delta;
        vco_tune_val_min_delta = vco_tune_val;
      }

      // Fine step VCO tune if delta is within upper tolerance limit.
      vco_tune_val += (freq_delta < USB3_PHY_PLL_TOLERANCE_LARGE) ? USB3_PHY_VCO_TUNE_STEP_SMALL : USB3_PHY_VCO_TUNE_STEP_LARGE;

    } while(vco_tune_val < USB3_PHY_VCO_TUNE_MAX_VAL);

    // Update vco_tune_val to one that is optimal or has minimum delta.
    if(vco_tune_val >= USB3_PHY_VCO_TUNE_MAX_VAL)
    {
      vco_tune_val = vco_tune_val_min_delta;
      qusb_error_log(SS_PHY_MIN_DELTA_VCO_TUNE_VAL_LOG, 0, vco_tune_val_min_delta);
    }
    else
    {
      vco_tune_val = vco_tune_optimal;
      qusb_error_log(SS_PHY_OPTIMAL_VCO_TUNE_VAL_LOG, 0, vco_tune_optimal);
    }

    qusb_error_log(SS_PHY_FREQ_MHZ_LOG, 0, min_freq/1.0e6);

    // Disable debug bus
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEBUG_BUS_SEL_OUT(0x0);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_MISC1_OUT(0x0);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_INTEGLOOP_EN_OUT(0x0);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OUT(0x1C);
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_START_CONTROL_OUT(0x0);  

    return vco_tune_val;
}

// ===========================================================================
/**
 * @function    qusb_ss_phy_init
 * 
 * @brief   API used for initializing the SS PHY 
 *  
 * @details This is used for initializing the SNPS controller PHY and QMP PHY 
 * 			Configure the QMP PHY as per Table 4-1 of the QMP PHY HPG 
 * 
 * @param   None.
 * 
 * @return  TRUE if successful else FALSE.
 * 
 */
// ===========================================================================
boolean qusb_ss_phy_init(void)
{
  uint32 qusb_timeout=0x0;
  uint16 vco_tune_val;
  uint8  mode_select;
  qusb_dci_chip_info hw_info = qusb_dci_get_chip_info();

  qusb_error_log(QUSB_SS_PHY_INIT_LOG, 0, 0);

  if( (hw_info.dev_num == CHIP_DEV_NUM_ISTARI_8996) && (hw_info.rev_maj == CHIP_REV_MAJ_1) )
  {
    mode_select = HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_INM(
                    HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_VCO_DEFAULT_FREQ_BMSK) 
                    >> HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_VCO_DEFAULT_FREQ_SHFT;

    vco_tune_val = qusb_dci_find_optimal_vco_tune(mode_select);
  }

  /*Put the PHY and CSR blocks into RESET*/
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,
    (0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  /*Put the PHY block into RESET*/
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, 
    (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  /*Release the CSR block from RESET*/
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,
    (0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);
  
  /* Release the PHY from Power Down mode */
  HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_OUTM(
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, 
    (0x1 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));

  // Configure the QMP PHY as per Table 4-1 of the QMP PHY HPG with default entry
  qusb_dci_phy_reg_array_process(
          qusb_dci_ss_phy_cfg_address_common, 
          qusb_dci_ss_phy_cfg_value_common,
           0,
          QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_COMMON);

  // Perform hardware revision specific writes - only for 8996 V1 and V2.
  if(hw_info.dev_num == CHIP_DEV_NUM_ISTARI_8996)
  {
    switch(hw_info.rev_maj)
    {
      case CHIP_REV_MAJ_1:
      {
        qusb_dci_phy_reg_array_process(
          qusb_dci_ss_phy_cfg_address_common, 
          qusb_dci_ss_phy_cfg_value_common,
          QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_COMMON,
          QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_V1);

        qusb_dci_write_vco_tune_val(mode_select, vco_tune_val);
      }
      break;

      case CHIP_REV_MAJ_2:
      {
        qusb_dci_phy_reg_array_process(
          qusb_dci_ss_phy_cfg_address_common,
          qusb_dci_ss_phy_cfg_value_common,
          QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_V1,
          QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT_V2);
      }
      break;
    }
  }

  // Read foundry specific information and program addition overrides
  // qusb_dci_ss_phy_foundry_id = pbl_get_foundry_id();
  if (qusb_dci_ss_phy_foundry_id >= QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX)
  {
    qusb_error_log(INVALID_QUSB_DCI_SS_PHY_FOUNDRY_ID_LOG, 0, qusb_dci_ss_phy_foundry_id);
  }
  else
  {
    qusb_dci_phy_reg_array_process(
          qusb_dci_ss_phy_cfg_address_override, 
          qusb_dci_ss_phy_cfg_value_override, 
          qusb_dci_ss_phy_cfg_override_start_index[qusb_dci_ss_phy_foundry_id],
          QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT);
  }

  /* Release the PHY block from Reset */
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(
    HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, 
    (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  /* Start PHY operation (begin with internal power-up & calibration) */
  HWIO_PERIPH_SS_USB3PHY_USB3_PHY_START_CONTROL_OUT(0x03);

  /* Release the PHY from SW Reset state */
  HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_OUTM(
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_SW_RESET_BMSK, 
    (0x0 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_SW_RESET_SHFT));

  /* Poll PHYSTATUS field of Register USB3_PHY_PCS_STATUS to Go LOW after reset*/
  while(HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_INM(
          HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_PHYSTATUS_BMSK) 
          >> HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_PHYSTATUS_SHFT)
  {
    qusb_dci_delay_us(100);
    if (qusb_timeout++ > 100)
    {
      DCI_ASSERT(DCI_SS_PHY_RESET_ERROR_LOG);
      return FALSE;
    }
  } 

  return TRUE;
}


// ===========================================================================
/**
 * @function    qusb_dci_deinit_hs_phy
 * 
 * @brief   API used to de-initialize the High Speed PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_deinit_hs_phy(void)
{
  //Clear the D+ Pull Up Assertion
  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x0 << HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x0 << HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));
}
// ===========================================================================
/**
 * @function    qusb_dci_enable_vbus_valid
 * 
 * @brief       API used to enable VBUS using s/w control
 * 
 * @param   qusb_max_speed_required_t - Maximum Speed required to be configured
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_enable_vbus_valid(qusb_max_speed_required_t speed_required)
{

  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x1 << HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x1 << HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));

  /* If we want to enable SUPER SPEED then also set LANE PWR PRESENT bit */
  if(speed_required == QUSB_MAX_SPEED_SUPER)
  {
    HWIO_SS_PHY_CTRL_OUTM(HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_BMSK, (0x1 << HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_SHFT));
  }

  return;
}
// ===========================================================================
/**
 * @function    qusb_dci_phy_reg_array_process
 * 
 * @brief   This function reads from array which define list of hwio writes for
 *          USB PHY
 * 
 * @param   address_array   - array holding address of HW register
 *          value_array     - array holding values to be written to HW register
 *          start_index     - starting index for array processing
 *          array_entry_cnt - number of entries in the array
 * 
 * @return  None
 * 
 */
// ===========================================================================
static void qusb_dci_phy_reg_array_process
(
  const uint32 *address_array, 
  const uint8  *value_array, 
  uint32        start_index, 
  uint32        array_entry_cnt
)
{
  uint32 index = start_index;

  if ( (NULL == address_array)
      || (NULL == value_array)
      || (0 == array_entry_cnt) )
  {
    qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS__FAIL_LOG, 0, (uint32)address_array);
  }
  else
  {
    for (; index < array_entry_cnt; index++)
    {
      if (QUSB_HWIO_ADDR_END == address_array[index])
      {
        break;
      }

      if (QUSB_HWIO_ADDR_EMPTY == address_array[index])
      {
        continue;
      }

      out_dword(address_array[index], value_array[index]);
    }
  }
  qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS__START_LOG, 0, start_index);
  qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS____END_LOG, 0, index);
}

// ===========================================================================
/**
 * @function    qusb_dci_check_ss_phy_presence
 * 
 * @brief   This function will check if 0.9V analog supply to SS PHY is on.
 * 
 * @param   None
 * 
 * @return  TRUE:  If analog supply to SS PHY on.
 *          FALSE: If analog supply to SS PHY off.
 * 
 */
// ===========================================================================
boolean qusb_dci_ss_phy_present(void)
{
  uint32 ac_jtag_outp, ac_jtag_outn;
  boolean ss_phy_present = FALSE;
  
  //1.  Release pcieusb3phy_reset (pcieusb3phy_phy_reset can remain asserted).
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,
    (0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  //2.  Write 0x01 to PCIE_USB3_PHY_POWER_DOWN_CONTROL to bring the PHY out of powerdown mode.
  HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_OUTM(
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, 
    (0x1 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));

  //3.  Write 0x01 to QSERDES_RX_AC_JTAG_RESET to enable set/reset of the ac_jtag receiver circuit.
  HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_RESET_OUTM(
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_RESET_AC_JTAG_RESET_BMSK, 
    (0x1 << HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_RESET_AC_JTAG_RESET_SHFT));
  
  //4.  Write 0x02 to QSERDES_RX_AC_JTAG_INITP and 0x03 to QSERDES_RX_AC_JTAG_INITN to set/reset ac_jtag_outp/n.
  HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_INITP_OUT(0x02);
  HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_INITN_OUT(0x03);
  
  //5.  Read QSERDES_RX_AC_JTAG_OUTP (expect value is 0x01) and  QSERDES_RX_AC_JTAG_OUTN (expect value is 0x00).
  ac_jtag_outp = HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_INM(
                    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_BMSK) 
                    >> HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_SHFT;
  ac_jtag_outn = HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_INM(
                    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_BMSK) 
                    >> HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_SHFT;
  if ((ac_jtag_outp == 0x01) && (ac_jtag_outn == 0x00))
  {
    //6.  Write 0x03 to QSERDES_RX_AC_JTAG_INITP and 0x02 to QSERDES_RX_AC_JTAG_INITN to reset/set ac_jtag_outp/n.
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_INITP_OUT(0x03);
    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_INITN_OUT(0x02);
   
    //7.  Read QSERDES_RX_AC_JTAG_OUTP (expect value is 0x00) and  QSERDES_RX_AC_JTAG_OUTN (expect value is 0x01).
    ac_jtag_outp = HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_INM(
                    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_BMSK) 
                    >> HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_SHFT;
    ac_jtag_outn = HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_INM(
                    HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_BMSK) 
                    >> HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_SHFT;
    if ((ac_jtag_outp == 0x00) && (ac_jtag_outn == 0x01))
    {
      ss_phy_present = TRUE;
    }
  }

  //If any of the reads at step 5 or step 7 do not match the expected values, then it can be assumed that the 0.9V analog supply is off.
  //If this is the case, then the PHY should be placed back in powerdown mode by writing 0x00 to PCIE_USB3_PHY_POWER_DOWN_CONTROL.
  if(FALSE == ss_phy_present)
  {
    HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_OUTM(
        HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, 
        (0x0 << HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));
  }

  qusb_error_log(QUSB_SS_PHY_PRESENT, 0, ss_phy_present);
  return ss_phy_present;
}

