
/*
===========================================================================
*/
/**
  @file HWIOBaseMap.c
  @brief Auto-generated HWIO Device Configuration base file.

  DESCRIPTION:
    This file contains Device Configuration data structures for mapping
    physical and virtual memory for HWIO blocks.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================


  ===========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "Drivers/HWIODxe/DalHWIO.h"
#include "msmhwiobase.h"


/*=========================================================================
      Data Definitions
==========================================================================*/

static HWIOModuleType HWIOModules_CNOC_0_BUS_TIMEOUT[] =
{
  { "CNOC_0_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_1_BUS_TIMEOUT[] =
{
  { "CNOC_1_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_2_BUS_TIMEOUT[] =
{
  { "CNOC_2_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_3_BUS_TIMEOUT[] =
{
  { "CNOC_3_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_4_BUS_TIMEOUT[] =
{
  { "CNOC_4_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_5_BUS_TIMEOUT[] =
{
  { "CNOC_5_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_6_BUS_TIMEOUT[] =
{
  { "CNOC_6_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_7_BUS_TIMEOUT[] =
{
  { "CNOC_7_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_8_BUS_TIMEOUT[] =
{
  { "CNOC_8_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CNOC_9_BUS_TIMEOUT[] =
{
  { "CNOC_9_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_CPR_APU1132_2[] =
{
  { "XPU_CFG_CPR_APU1132_2",                       0x00000000, 0x00000300 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB[] =
{
  { "XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB",       0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB[] =
{
  { "XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB",      0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB[] =
{
  { "XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB",      0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB[] =
{
  { "XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB",      0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_QDSS_MPU1132_3_M23L12_AHB[] =
{
  { "XPU_CFG_QDSS_MPU1132_3_M23L12_AHB",           0x00000000, 0x00000380 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_SSC_MPU1032_10_M22L11_AHB[] =
{
  { "XPU_CFG_SSC_MPU1032_10_M22L11_AHB",           0x00000000, 0x00000700 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE20_AHB2PHY[] =
{
  { "CM_PCIE",                                     0x00000000, 0x00004000 },
  { "QSERDES_COM_PCIE_QMP_COM",                    0x00000000, 0x000001c4 },
  { "PCIE_COM_PCIE_COM",                           0x00000400, 0x000000a4 },
  { "QSERDES_TX_A_PCIE_QMP_TX",                    0x00001000, 0x00000130 },
  { "QSERDES_RX_A_PCIE_QMP_RX",                    0x00001200, 0x00000200 },
  { "PCIE_A_PCIE_PORT",                            0x00001400, 0x000001dc },
  { "QSERDES_TX_B_PCIE_QMP_TX",                    0x00002000, 0x00000130 },
  { "QSERDES_RX_B_PCIE_QMP_RX",                    0x00002200, 0x00000200 },
  { "PCIE_B_PCIE_PORT",                            0x00002400, 0x000001dc },
  { "QSERDES_TX_C_PCIE_QMP_TX",                    0x00003000, 0x00000130 },
  { "QSERDES_RX_C_PCIE_QMP_RX",                    0x00003200, 0x00000200 },
  { "PCIE_C_PCIE_PORT",                            0x00003400, 0x000001dc },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RAMBLUR_PIMEM[] =
{
  { "RAMBLUR_PIMEM_REGS",                          0x00000000, 0x00007000 },
  { "RAMBLUR_PIMEM_APU_APU1132_8",                 0x00007000, 0x00000600 },
  { "RAMBLUR_PIMEM_MPU_MPU1132A_8_M23L10_AXI_36",  0x00007800, 0x00000600 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_OCIMEM_WRAPPER_CSR[] =
{
  { "OCIMEM_CSR",                                  0x00000000, 0x00000800 },
  { "OCIMEM_APU",                                  0x00002000, 0x00000300 },
  { "OCIMEM_MPU",                                  0x00003000, 0x00000600 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SPDM_WRAPPER_TOP[] =
{
  { "SPDM_SPDM_CREG",                              0x00000000, 0x00000120 },
  { "SPDM_SPDM_OLEM",                              0x00001000, 0x0000015c },
  { "SPDM_SPDM_RTEM",                              0x00002000, 0x00000318 },
  { "SPDM_APU0132_1",                              0x00003000, 0x00000280 },
  { "SPDM_SPDM_SREG",                              0x00004000, 0x00000120 },
  { "SPDM_VMIDMT_IDX_1_SSD0",                      0x00005000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CPR3[] =
{
  { "CPR3",                                        0x00000000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MX_CPR3[] =
{
  { "MX_CPR3",                                     0x00000000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ISTARI_SKL_WRAPPER[] =
{
  { "SKL",                                         0x00000000, 0x00002000 },
  { "SKL_XPU2",                                    0x00002000, 0x00000300 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MSS_MPU_MPU1032A_16_M35L12_AXI_36[] =
{
  { "MSS_MPU_MPU1032A_16_M35L12_AXI_36",           0x00000000, 0x00000a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36[] =
{
  { "MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36",        0x00000000, 0x00000a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ANOC1_MPU_MPU1032A_16_M35L12_AXI_36[] =
{
  { "ANOC1_MPU_MPU1032A_16_M35L12_AXI_36",         0x00000000, 0x00000a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ANOC2_MPU_MPU1032A_16_M35L12_AXI_36[] =
{
  { "ANOC2_MPU_MPU1032A_16_M35L12_AXI_36",         0x00000000, 0x00000a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_MSG_RAM[] =
{
  { "RPM_MSG_RAM",                                 0x00000000, 0x00008000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SECURITY_CONTROL[] =
{
  { "SECURITY_CONTROL_CORE",                       0x00000000, 0x00007000 },
  { "SECURE_CHANNEL",                              0x00008000, 0x00004200 },
  { "KEY_CTRL",                                    0x00008000, 0x00004000 },
  { "CRI_CM",                                      0x0000c000, 0x00000100 },
  { "CRI_CM_EXT",                                  0x0000c100, 0x00000100 },
  { "SEC_CTRL_APU_APU1132_37",                     0x0000e000, 0x00001480 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PRNG_CFG_PRNG_TOP[] =
{
  { "PRNG_CFG_CM_CM_PRNG_CM",                      0x00000000, 0x00001000 },
  { "PRNG_CFG_TZ_TZ_PRNG_TZ",                      0x00001000, 0x00001000 },
  { "PRNG_CFG_MSA_MSA_PRNG_SUB",                   0x00002000, 0x00001000 },
  { "PRNG_CFG_EE2_EE2_PRNG_SUB",                   0x00003000, 0x00001000 },
  { "PRNG_CFG_EE3_EE3_PRNG_SUB",                   0x00004000, 0x00001000 },
  { "PRNG_CFG_EE4_EE4_PRNG_SUB",                   0x00005000, 0x00001000 },
  { "PRNG_CFG_EE5_EE5_PRNG_SUB",                   0x00006000, 0x00001000 },
  { "PRNG_CFG_EE6_EE6_PRNG_SUB",                   0x00007000, 0x00001000 },
  { "PRNG_CFG_EE7_EE7_PRNG_SUB",                   0x00008000, 0x00001000 },
  { "PRNG_CFG_EE8_EE8_PRNG_SUB",                   0x00009000, 0x00001000 },
  { "PRNG_CFG_EE9_EE9_PRNG_SUB",                   0x0000a000, 0x00001000 },
  { "PRNG_CFG_EE10_EE10_PRNG_SUB",                 0x0000b000, 0x00001000 },
  { "PRNG_CFG_EE11_EE11_PRNG_SUB",                 0x0000c000, 0x00001000 },
  { "PRNG_CFG_EE12_EE12_PRNG_SUB",                 0x0000d000, 0x00001000 },
  { "PRNG_CFG_EE13_EE13_PRNG_SUB",                 0x0000e000, 0x00001000 },
  { "PRNG_CFG_EE14_EE14_PRNG_SUB",                 0x0000f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BOOT_ROM[] =
{
  { "BOOT_ROM_MPU1032_4_M17L10_AHB",               0x000ff000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM[] =
{
  { "RPM_DEC",                                     0x00080000, 0x00002000 },
  { "RPM_QTMR_AC",                                 0x00082000, 0x00001000 },
  { "RPM_F0_QTMR_V1_F0",                           0x00083000, 0x00001000 },
  { "RPM_F1_QTMR_V1_F1",                           0x00084000, 0x00001000 },
  { "RPM_MSTR_MPU",                                0x00089000, 0x00000c00 },
  { "RPM_VMIDMT",                                  0x00088000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_DATA_RAM_START_ADDRESS[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CLK_CTL[] =
{
  { "GCC_CLK_CTL_REG",                             0x00000000, 0x00090000 },
  { "GCC_RPU_RPU0032_144_L12",                     0x00090000, 0x00004a00 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BIMC[] =
{
  { "BIMC_GLOBAL0",                                0x00000000, 0x00001000 },
  { "BIMC_GLOBAL1",                                0x00001000, 0x00001000 },
  { "BIMC_GLOBAL2",                                0x00002000, 0x00001000 },
  { "BIMC_CONFIG_APU",                             0x00004000, 0x00000a00 },
  { "BIMC_PERFMON",                                0x00003000, 0x00001000 },
  { "BIMC_DTE",                                    0x00006000, 0x0000005c },
  { "BIMC_M_APP_MPORT",                            0x00008000, 0x00001000 },
  { "BIMC_M_GPU_MPORT",                            0x0000c000, 0x00001000 },
  { "BIMC_M_MMSS_MPORT",                           0x00010000, 0x00001000 },
  { "BIMC_M_SYS_MPORT",                            0x00014000, 0x00001000 },
  { "BIMC_M_PIMEM_MPORT",                          0x00018000, 0x00001000 },
  { "BIMC_M_MDSP_MPORT",                           0x0001c000, 0x00001000 },
  { "BIMC_M_APP_PROF",                             0x00009000, 0x00001000 },
  { "BIMC_M_GPU_PROF",                             0x0000d000, 0x00001000 },
  { "BIMC_M_MMSS_PROF",                            0x00011000, 0x00001000 },
  { "BIMC_M_SYS_PROF",                             0x00015000, 0x00001000 },
  { "BIMC_M_PIMEM_PROF",                           0x00019000, 0x00001000 },
  { "BIMC_M_MDSP_PROF",                            0x0001d000, 0x00001000 },
  { "BIMC_S_DDR0_SCMO",                            0x00030000, 0x00001000 },
  { "BIMC_S_DDR1_SCMO",                            0x0003c000, 0x00001000 },
  { "BIMC_S_APP_SWAY",                             0x00048000, 0x00001000 },
  { "BIMC_S_SYS0_SWAY",                            0x00050000, 0x00001000 },
  { "BIMC_S_SYS1_SWAY",                            0x00058000, 0x00001000 },
  { "BIMC_S_DEFAULT_SWAY",                         0x00060000, 0x00001000 },
  { "BIMC_S_DDR0_ARB",                             0x00031000, 0x00001000 },
  { "BIMC_S_DDR1_ARB",                             0x0003d000, 0x00001000 },
  { "BIMC_S_APP_ARB",                              0x00049000, 0x00001000 },
  { "BIMC_S_SYS0_ARB",                             0x00051000, 0x00001000 },
  { "BIMC_S_SYS1_ARB",                             0x00059000, 0x00001000 },
  { "BIMC_S_DEFAULT_ARB",                          0x00061000, 0x00001000 },
  { "BIMC_S_DDR0",                                 0x00032000, 0x00000e80 },
  { "BIMC_S_DDR0_DPE",                             0x00034000, 0x00001000 },
  { "BIMC_S_DDR0_SHKE",                            0x00035000, 0x00001000 },
  { "BIMC_S_DDR0_DTTS_CFG",                        0x00036000, 0x00001000 },
  { "BIMC_S_DDR0_DTTS_SRAM",                       0x00037000, 0x00004000 },
  { "BIMC_S_DDR1",                                 0x0003e000, 0x00000e80 },
  { "BIMC_S_DDR1_DPE",                             0x00040000, 0x00001000 },
  { "BIMC_S_DDR1_SHKE",                            0x00041000, 0x00001000 },
  { "BIMC_S_DDR1_DTTS_CFG",                        0x00042000, 0x00001000 },
  { "BIMC_S_DDR1_DTTS_SRAM",                       0x00043000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_DDR_SS[] =
{
  { "CH0_CA0_DDR_PHY",                             0x00000000, 0x00000800 },
  { "CH0_CA1_DDR_PHY",                             0x00000800, 0x00000800 },
  { "CH0_DQ0_DDR_PHY",                             0x00001000, 0x00000800 },
  { "CH0_DQ1_DDR_PHY",                             0x00001800, 0x00000800 },
  { "CH0_DQ2_DDR_PHY",                             0x00002000, 0x00000800 },
  { "CH0_DQ3_DDR_PHY",                             0x00002800, 0x00000800 },
  { "CH0_DDR_CC",                                  0x00003000, 0x00000800 },
  { "CH1_CA0_DDR_PHY",                             0x00003800, 0x00000800 },
  { "CH1_CA1_DDR_PHY",                             0x00004000, 0x00000800 },
  { "CH1_DQ0_DDR_PHY",                             0x00004800, 0x00000800 },
  { "CH1_DQ1_DDR_PHY",                             0x00005000, 0x00000800 },
  { "CH1_DQ2_DDR_PHY",                             0x00005800, 0x00000800 },
  { "CH1_DQ3_DDR_PHY",                             0x00006000, 0x00000800 },
  { "CH1_DDR_CC",                                  0x00006800, 0x00000800 },
  { "DDR_REG_DDR_SS_REGS",                         0x00007000, 0x00000400 },
  { "DDRSS_AHB2PHY_SWMAN",                         0x0001f000, 0x00000400 },
  { "DDRSS_AHB2PHY_BROADCAST_SWMAN1",              0x0001f800, 0x00000800 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MPM2_MPM[] =
{
  { "MPM2_MPM",                                    0x00000000, 0x00001000 },
  { "MPM2_G_CTRL_CNTR",                            0x00001000, 0x00001000 },
  { "MPM2_G_RD_CNTR",                              0x00002000, 0x00001000 },
  { "MPM2_SLP_CNTR",                               0x00003000, 0x00001000 },
  { "MPM2_QTIMR_AC",                               0x00004000, 0x00001000 },
  { "MPM2_QTIMR_V1",                               0x00005000, 0x00001000 },
  { "MPM2_TSYNC",                                  0x00006000, 0x00001000 },
  { "MPM2_APU",                                    0x00007000, 0x00000880 },
  { "MPM2_TSENS0",                                 0x00008000, 0x00001000 },
  { "MPM2_TSENS0_TSENS0_TM",                       0x00009000, 0x00001000 },
  { "MPM2_WDOG",                                   0x0000a000, 0x00000020 },
  { "MPM2_PSHOLD",                                 0x0000b000, 0x00001000 },
  { "MPM2_TSENS1",                                 0x0000c000, 0x00001000 },
  { "MPM2_TSENS1_TSENS1_TM",                       0x0000d000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_DCC_WRAPPER[] =
{
  { "DCC",                                         0x00003000, 0x00001000 },
  { "DCC_SEC",                                     0x00002000, 0x00001000 },
  { "DCC_XPU",                                     0x00000000, 0x00000280 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CONFIG_NOC[] =
{
  { "CONFIG_NOC",                                  0x00000000, 0x00000080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SYSTEM_NOC[] =
{
  { "SYSTEM_NOC",                                  0x00000000, 0x0000a100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_A0_NOC_AGGRE0_NOC[] =
{
  { "A0_NOC_AGGRE0_NOC",                           0x00000000, 0x00005100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_A1_NOC_AGGRE1_NOC[] =
{
  { "A1_NOC_AGGRE1_NOC",                           0x00000000, 0x00003100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_A2_NOC_AGGRE2_NOC[] =
{
  { "A2_NOC_AGGRE2_NOC",                           0x00000000, 0x00008100 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MMSS_NOC[] =
{
  { "MMSS_NOC",                                    0x00000000, 0x0000b080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PERIPH_NOC[] =
{
  { "PERIPH_NOC",                                  0x00000000, 0x00002480 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_0_PCIE20_WRAPPER_AHB[] =
{
  { "PCIE_0_PCIE20_PARF",                          0x00000000, 0x00002000 },
  { "PCIE_0_PCIE20_MHI",                           0x00007000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_1_PCIE20_WRAPPER_AHB[] =
{
  { "PCIE_1_PCIE20_PARF",                          0x00000000, 0x00002000 },
  { "PCIE_1_PCIE20_MHI",                           0x00007000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_2_PCIE20_WRAPPER_AHB[] =
{
  { "PCIE_2_PCIE20_PARF",                          0x00000000, 0x00002000 },
  { "PCIE_2_PCIE20_MHI",                           0x00007000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_UFS_UFS_REGS[] =
{
  { "UFS_APU",                                     0x00000000, 0x00000300 },
  { "UFS",                                         0x00004000, 0x00003000 },
  { "UFS_MPHY_UFS",                                0x00007000, 0x00000e00 },
  { "UFS_QSERDES_COM_QSERDES_COM_UFS_QSRV_COM",    0x00007000, 0x000001c4 },
  { "UFS_QSERDES_TX_QSERDES_TX_UFS_QMP_TX",        0x00007400, 0x0000012c },
  { "UFS_QSERDES_RX_QSERDES_RX_UFS_QMP_RX",        0x00007600, 0x00000200 },
  { "UFS_UFS_PHY_UFS_PHY_UFS_PCS",                 0x00007c00, 0x000001b8 },
  { "UFS_TZ",                                      0x00009000, 0x00001000 },
  { "UFS_SCM",                                     0x0000b000, 0x00000800 },
  { "UFS_ICE",                                     0x00010000, 0x00008000 },
  { "UFS_ICE_REGS",                                0x00010000, 0x00002000 },
  { "UFS_ICE_LUT_KEYS",                            0x00012000, 0x00001000 },
  { "UFS_ICE_XPU2",                                0x00013000, 0x00000380 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CRYPTO0_CRYPTO_TOP[] =
{
  { "CRYPTO0_CRYPTO",                              0x0003a000, 0x00006000 },
  { "CRYPTO0_CRYPTO_BAM",                          0x00004000, 0x00024000 },
  { "CRYPTO0_CRYPTO_BAM_XPU2_BAM",                 0x00002000, 0x00002000 },
  { "CRYPTO0_CRYPTO_BAM_VMIDMT_BAM",               0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_IPA_0_IPA_WRAPPER[] =
{
  { "IPA_0_BAM_NDP",                               0x00000000, 0x0002b000 },
  { "IPA_0_BAM",                                   0x00004000, 0x00027000 },
  { "IPA_0_XPU2",                                  0x00002000, 0x00002000 },
  { "IPA_0_IPA_VMIDMT",                            0x00030000, 0x00001000 },
  { "IPA_0_IPA",                                   0x00040000, 0x00010000 },
  { "IPA_0_IPA_UC",                                0x00050000, 0x00014000 },
  { "IPA_0_IPA_UC_IPA_UC_RAM",                     0x00050000, 0x00008000 },
  { "IPA_0_IPA_UC_IPA_UC_PER",                     0x00060000, 0x00002000 },
  { "IPA_0_IPA_UC_IPA_UC_MBOX",                    0x00062000, 0x00002000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CORE_TOP_CSR[] =
{
  { "TCSR_MUTEX_RPU1132_64_L12",                   0x00000000, 0x00002200 },
  { "TCSR_TCSR_MUTEX",                             0x00040000, 0x00040000 },
  { "TCSR_REGS_RPU1132_32_L12",                    0x00080000, 0x00001200 },
  { "TCSR_TCSR_REGS",                              0x000a0000, 0x00020000 },
  { "TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW",     0x000b9000, 0x00000080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MMSS[] =
{
  { "MMSS_MISC",                                   0x00028000, 0x00004000 },
  { "MMSS_CPR_WRAPPER",                            0x00038000, 0x00004000 },
  { "MMSS_CC",                                     0x000c0000, 0x00040000 },
  { "MMSS_MDSS",                                   0x00100000, 0x00100000 },
  { "MMSS_MDSS_HW",                                0x00100000, 0x00001000 },
  { "MMSS_MDP",                                    0x00101000, 0x0008f000 },
  { "MMSS_MDP_SSPP_TOP0",                          0x00101000, 0x00000030 },
  { "MMSS_MDP_SMP_TOP0",                           0x00101030, 0x000001b8 },
  { "MMSS_MDP_SSPP_TOP1",                          0x001011e8, 0x00000114 },
  { "MMSS_MDP_SMP_TOP1",                           0x001012fc, 0x00000004 },
  { "MMSS_MDP_DSPP_TOP0",                          0x00101300, 0x00000080 },
  { "MMSS_MDP_PERIPH_TOP0",                        0x00101380, 0x00000028 },
  { "MMSS_MDP_SSPP_TOP2",                          0x001013a8, 0x00000048 },
  { "MMSS_MDP_PERIPH_TOP1",                        0x001013f0, 0x00000c10 },
  { "MMSS_MDP_CTL_0",                              0x00102000, 0x00000100 },
  { "MMSS_MDP_CTL_1",                              0x00102200, 0x00000100 },
  { "MMSS_MDP_CTL_2",                              0x00102400, 0x00000100 },
  { "MMSS_MDP_CTL_3",                              0x00102600, 0x00000100 },
  { "MMSS_MDP_CTL_4",                              0x00102800, 0x00000100 },
  { "MMSS_MDP_SSPP_IGC_LUT",                       0x00103000, 0x00000101 },
  { "MMSS_MDP_VP_0_VIG_0_SSPP",                    0x00105000, 0x00000200 },
  { "MMSS_MDP_VP_0_VIG_0",                         0x00105200, 0x00001600 },
  { "MMSS_MDP_VP_0_VIG_0_PCC",                     0x00106780, 0x00000080 },
  { "MMSS_MDP_VP_0_VIG_1_SSPP",                    0x00107000, 0x00000200 },
  { "MMSS_MDP_VP_0_VIG_1",                         0x00107200, 0x00001600 },
  { "MMSS_MDP_VP_0_VIG_1_PCC",                     0x00108780, 0x00000080 },
  { "MMSS_MDP_VP_0_VIG_2_SSPP",                    0x00109000, 0x00000200 },
  { "MMSS_MDP_VP_0_VIG_2",                         0x00109200, 0x00001600 },
  { "MMSS_MDP_VP_0_VIG_2_PCC",                     0x0010a780, 0x00000080 },
  { "MMSS_MDP_VP_0_VIG_3_SSPP",                    0x0010b000, 0x00000200 },
  { "MMSS_MDP_VP_0_VIG_3",                         0x0010b200, 0x00001600 },
  { "MMSS_MDP_VP_0_VIG_3_PCC",                     0x0010c780, 0x00000080 },
  { "MMSS_MDP_VP_0_RGB_0_SSPP",                    0x00115000, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_0",                         0x00115200, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_0_PCC",                     0x00115380, 0x00000080 },
  { "MMSS_MDP_VP_0_RGB_1_SSPP",                    0x00117000, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_1",                         0x00117200, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_1_PCC",                     0x00117380, 0x00000080 },
  { "MMSS_MDP_VP_0_RGB_2_SSPP",                    0x00119000, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_2",                         0x00119200, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_2_PCC",                     0x00119380, 0x00000080 },
  { "MMSS_MDP_VP_0_RGB_3_SSPP",                    0x0011b000, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_3",                         0x0011b200, 0x00000200 },
  { "MMSS_MDP_VP_0_RGB_3_PCC",                     0x0011b380, 0x00000080 },
  { "MMSS_MDP_VP_0_DMA_0_SSPP",                    0x00125000, 0x00000200 },
  { "MMSS_MDP_VP_0_DMA_0",                         0x00125200, 0x00000200 },
  { "MMSS_MDP_VP_0_DMA_0_PCC",                     0x00125380, 0x00000080 },
  { "MMSS_MDP_VP_0_DMA_1_SSPP",                    0x00127000, 0x00000200 },
  { "MMSS_MDP_VP_0_DMA_1",                         0x00127200, 0x00000200 },
  { "MMSS_MDP_VP_0_DMA_1_PCC",                     0x00127380, 0x00000080 },
  { "MMSS_MDP_VP_0_CURSOR_0",                      0x00135000, 0x00000400 },
  { "MMSS_MDP_VP_0_CURSOR_0_SSPP",                 0x00135000, 0x00000200 },
  { "MMSS_MDP_VP_0_CURSOR_1",                      0x00137000, 0x00000400 },
  { "MMSS_MDP_VP_0_CURSOR_1_SSPP",                 0x00137000, 0x00000200 },
  { "MMSS_MDP_VP_0_LAYER_0",                       0x00145000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_0_PGC",                   0x001453c0, 0x00000040 },
  { "MMSS_MDP_VP_0_LAYER_1",                       0x00146000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_1_PGC",                   0x001463c0, 0x00000040 },
  { "MMSS_MDP_VP_0_LAYER_2",                       0x00147000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_2_PGC",                   0x001473c0, 0x00000040 },
  { "MMSS_MDP_VP_0_LAYER_3",                       0x00148000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_3_PGC",                   0x001483c0, 0x00000040 },
  { "MMSS_MDP_VP_0_LAYER_4",                       0x00149000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_4_PGC",                   0x001493c0, 0x00000040 },
  { "MMSS_MDP_VP_0_LAYER_5",                       0x0014a000, 0x00000400 },
  { "MMSS_MDP_VP_0_LAYER_5_PGC",                   0x0014a3c0, 0x00000040 },
  { "MMSS_MDP_VP_0_DSPP_0",                        0x00155000, 0x00001800 },
  { "MMSS_MDP_VP_0_DSPP_0_GAMUT",                  0x00156600, 0x00000100 },
  { "MMSS_MDP_VP_0_DSPP_0_PCC",                    0x00156700, 0x00000080 },
  { "MMSS_MDP_VP_0_DSPP_0_PGC",                    0x001567c0, 0x00000040 },
  { "MMSS_MDP_VP_0_DSPP_1",                        0x00157000, 0x00001800 },
  { "MMSS_MDP_VP_0_DSPP_1_GAMUT",                  0x00158600, 0x00000100 },
  { "MMSS_MDP_VP_0_DSPP_1_PCC",                    0x00158700, 0x00000080 },
  { "MMSS_MDP_VP_0_DSPP_1_PGC",                    0x001587c0, 0x00000040 },
  { "MMSS_MDP_VP_0_DSPP_2",                        0x00159000, 0x00001800 },
  { "MMSS_MDP_VP_0_DSPP_2_GAMUT",                  0x0015a600, 0x00000100 },
  { "MMSS_MDP_VP_0_DSPP_2_PCC",                    0x0015a700, 0x00000080 },
  { "MMSS_MDP_VP_0_DSPP_2_PGC",                    0x0015a7c0, 0x00000040 },
  { "MMSS_MDP_VP_0_DSPP_3",                        0x0015b000, 0x00001800 },
  { "MMSS_MDP_VP_0_DSPP_3_GAMUT",                  0x0015c600, 0x00000100 },
  { "MMSS_MDP_VP_0_DSPP_3_PCC",                    0x0015c700, 0x00000080 },
  { "MMSS_MDP_VP_0_DSPP_3_PGC",                    0x0015c7c0, 0x00000040 },
  { "MMSS_MDP_WB_0",                               0x00165000, 0x00000400 },
  { "MMSS_MDP_WB_1",                               0x00165800, 0x00000400 },
  { "MMSS_MDP_WB_2",                               0x00166000, 0x00000400 },
  { "MMSS_MDP_INTF_0",                             0x0016b000, 0x00000300 },
  { "MMSS_MDP_INTF_1",                             0x0016b800, 0x00000300 },
  { "MMSS_MDP_INTF_2",                             0x0016c000, 0x00000300 },
  { "MMSS_MDP_INTF_3",                             0x0016c800, 0x00000300 },
  { "MMSS_MDP_INTF_4",                             0x0016d000, 0x00000300 },
  { "MMSS_MDP_PP_0",                               0x00171000, 0x00000100 },
  { "MMSS_MDP_PP_1",                               0x00171800, 0x00000100 },
  { "MMSS_MDP_PP_2",                               0x00172000, 0x00000100 },
  { "MMSS_MDP_PP_3",                               0x00172800, 0x00000100 },
  { "MMSS_MDP_PP_4",                               0x00173000, 0x00000100 },
  { "MMSS_MDP_ENCR_NS_0",                          0x00177000, 0x00000010 },
  { "MMSS_MDP_ENCR_S_0",                           0x00177400, 0x00000030 },
  { "MMSS_MDP_AADC_0",                             0x00179000, 0x00000200 },
  { "MMSS_MDP_AADC_1",                             0x00179800, 0x00000200 },
  { "MMSS_MDP_AADC_2",                             0x0017a000, 0x00000200 },
  { "MMSS_MDP_CDM_0",                              0x0017a200, 0x00000240 },
  { "MMSS_MDP_CDM_0_CSC_10",                       0x0017a200, 0x00000100 },
  { "MMSS_MDP_CDM_0_CDWN2",                        0x0017a300, 0x00000100 },
  { "MMSS_MDP_CDM_0_HDMI_PACK",                    0x0017a400, 0x00000010 },
  { "MMSS_MDP_DSC_0",                              0x00181000, 0x00000200 },
  { "MMSS_MDP_DSC_1",                              0x00181400, 0x00000200 },
  { "MMSS_MDP_SCM3_0",                             0x00181600, 0x00000020 },
  { "MMSS_MDP_QDSS",                               0x00181a00, 0x00000200 },
  { "MMSS_EDP",                                    0x00190000, 0x00001600 },
  { "MMSS_EDP_PHY_QSERDES_COM_QSERDES_COM_EDP_COM", 0x00190800, 0x000001c4 },
  { "MMSS_EDP_PHY_QSERDES_TX_L0_QSERDES_TX_L0_EDP_TX", 0x00190c00, 0x00000124 },
  { "MMSS_EDP_PHY_QSERDES_TX_L1_QSERDES_TX_L1_EDP_TX", 0x00190e00, 0x00000124 },
  { "MMSS_EDP_PHY_QSERDES_TX_L2_QSERDES_TX_L2_EDP_TX", 0x00191000, 0x00000124 },
  { "MMSS_EDP_PHY_QSERDES_TX_L3_QSERDES_TX_L3_EDP_TX", 0x00191200, 0x00000124 },
  { "MMSS_EDP_PHY",                                0x00191400, 0x000000f0 },
  { "MMSS_DSI_0",                                  0x00194000, 0x00000c00 },
  { "MMSS_DSI_0_DSI",                              0x00194000, 0x00000400 },
  { "MMSS_DSI_0_PHY",                              0x00194400, 0x00000588 },
  { "MMSS_DSI_0_PHY_DSIPHY_CMN_DSIPHY_CMN_DSI_COMMON", 0x00194400, 0x0000007c },
  { "MMSS_DSI_0_PHY_DSIPHY_DLN0_DSIPHY_DLN0_DSI_DATALN", 0x00194500, 0x00000080 },
  { "MMSS_DSI_0_PHY_DSIPHY_DLN1_DSIPHY_DLN1_DSI_DATALN", 0x00194580, 0x00000080 },
  { "MMSS_DSI_0_PHY_DSIPHY_DLN2_DSIPHY_DLN2_DSI_DATALN", 0x00194600, 0x00000080 },
  { "MMSS_DSI_0_PHY_DSIPHY_DLN3_DSIPHY_DLN3_DSI_DATALN", 0x00194680, 0x00000080 },
  { "MMSS_DSI_0_PHY_DSIPHY_CKLN_DSIPHY_CKLN_DSI_DATALN", 0x00194700, 0x00000080 },
  { "MMSS_DSI_0_PHY_DSIPHY_PLL_DSIPHY_PLL_DSI_COM_LITE", 0x00194800, 0x00000124 },
  { "MMSS_DSI_1",                                  0x00196000, 0x00000c00 },
  { "MMSS_DSI_1_DSI",                              0x00196000, 0x00000400 },
  { "MMSS_DSI_1_PHY",                              0x00196400, 0x00000588 },
  { "MMSS_DSI_1_PHY_DSIPHY_CMN_DSIPHY_CMN_DSI_COMMON", 0x00196400, 0x0000007c },
  { "MMSS_DSI_1_PHY_DSIPHY_DLN0_DSIPHY_DLN0_DSI_DATALN", 0x00196500, 0x00000080 },
  { "MMSS_DSI_1_PHY_DSIPHY_DLN1_DSIPHY_DLN1_DSI_DATALN", 0x00196580, 0x00000080 },
  { "MMSS_DSI_1_PHY_DSIPHY_DLN2_DSIPHY_DLN2_DSI_DATALN", 0x00196600, 0x00000080 },
  { "MMSS_DSI_1_PHY_DSIPHY_DLN3_DSIPHY_DLN3_DSI_DATALN", 0x00196680, 0x00000080 },
  { "MMSS_DSI_1_PHY_DSIPHY_CKLN_DSIPHY_CKLN_DSI_DATALN", 0x00196700, 0x00000080 },
  { "MMSS_DSI_1_PHY_DSIPHY_PLL_DSIPHY_PLL_DSI_COM_LITE", 0x00196800, 0x00000124 },
  { "MMSS_HDMI",                                   0x001a0000, 0x00001300 },
  { "MMSS_HDMI_PHY_QSERDES_COM_QSERDES_COM_HDMI_COM", 0x001a0600, 0x000001c4 },
  { "MMSS_HDMI_PHY_QSERDES_TX_L0_QSERDES_TX_L0_HDMI_TX", 0x001a0a00, 0x00000124 },
  { "MMSS_HDMI_PHY_QSERDES_TX_L1_QSERDES_TX_L1_HDMI_TX", 0x001a0c00, 0x00000124 },
  { "MMSS_HDMI_PHY_QSERDES_TX_L2_QSERDES_TX_L2_HDMI_TX", 0x001a0e00, 0x00000124 },
  { "MMSS_HDMI_PHY_QSERDES_TX_L3_QSERDES_TX_L3_HDMI_TX", 0x001a1000, 0x00000124 },
  { "MMSS_HDMI_PHY",                               0x001a1200, 0x000000c8 },
  { "MMSS_MHL",                                    0x001a8000, 0x00000800 },
  { "MMSS_MHL_CBUS",                               0x001a8400, 0x00000100 },
  { "MMSS_MHL_PHY",                                0x001a8500, 0x00000124 },
  { "MMSS_MHL_PHY_PLL",                            0x001a8700, 0x000000f4 },
  { "MMSS_VBIF",                                   0x001b0000, 0x00003000 },
  { "MMSS_VBIF_VBIF_MDP",                          0x001b0000, 0x00003000 },
  { "MMSS_VBIF_NRT",                               0x001b8000, 0x00003000 },
  { "MMSS_VBIF_NRT_VBIF_ROT",                      0x001b8000, 0x00003000 },
  { "MMSS_XPU",                                    0x001c8000, 0x00000480 },
  { "MMSS_MDSS_SEC",                               0x001d0000, 0x00030000 },
  { "MMSS_MDSS_SEC_HDCP_SEC_TZ_ONLY",              0x001d0000, 0x00001000 },
  { "MMSS_MDSS_SEC_HDCP_SEC_TZ_HV",                0x001d8000, 0x00001000 },
  { "MMSS_MDSS_SEC_DSI_0_SEC",                     0x001d9000, 0x00001000 },
  { "MMSS_MDSS_SEC_DSI_1_SEC",                     0x001da000, 0x00001000 },
  { "MMSS_MDSS_SEC_HDCP_SEC_TZ_HV_HLOS",           0x001e0000, 0x00001000 },
  { "MMSS_MDSS_SEC_HDCP_SEC_DBG",                  0x001f0000, 0x00001000 },
  { "MMSS_CAMSS",                                  0x00200000, 0x00100000 },
  { "MMSS_A_CAMSS",                                0x00200000, 0x00004000 },
  { "MMSS_A_CAMSS_MICRO",                          0x00204000, 0x00004000 },
  { "MMSS_A_CCI",                                  0x0020c000, 0x00004000 },
  { "MMSS_A_VFE_0",                                0x00210000, 0x00004000 },
  { "MMSS_A_VFE_1",                                0x00214000, 0x00004000 },
  { "MMSS_A_CPP",                                  0x00218000, 0x00004000 },
  { "MMSS_A_JPEG_0",                               0x0021c000, 0x00004000 },
  { "MMSS_A_JPEG_2",                               0x00224000, 0x00004000 },
  { "MMSS_A_CSID_0",                               0x00230000, 0x00000400 },
  { "MMSS_A_CSID_1",                               0x00230400, 0x00000400 },
  { "MMSS_A_CSID_2",                               0x00230800, 0x00000400 },
  { "MMSS_A_CSID_3",                               0x00230c00, 0x00000400 },
  { "MMSS_A_ISPIF",                                0x00231000, 0x00000c00 },
  { "MMSS_A_CSI_PHY_0",                            0x00234000, 0x00001000 },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN0_CSIPHY_LN0_CSI_2PHASE_DATALN", 0x00234000, 0x000000dc },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN1_CSIPHY_LN1_CSI_3PHASE_DATALN", 0x00234100, 0x00000100 },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN2_CSIPHY_LN2_CSI_2PHASE_DATALN", 0x00234200, 0x000000dc },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN3_CSIPHY_LN3_CSI_3PHASE_DATALN", 0x00234300, 0x00000100 },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN4_CSIPHY_LN4_CSI_2PHASE_DATALN", 0x00234400, 0x000000dc },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN5_CSIPHY_LN5_CSI_3PHASE_DATALN", 0x00234500, 0x00000100 },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LN6_CSIPHY_LN6_CSI_2PHASE_DATALN", 0x00234600, 0x000000dc },
  { "MMSS_A_CSI_PHY_0_CSIPHY_LNCK_CSIPHY_LNCK_CSI_2PHASE_DATALN", 0x00234700, 0x000000dc },
  { "MMSS_A_CSI_PHY_0_CSIPHY_CMN_CSIPHY_CMN_CSI_3PHASE_COMMON", 0x00234800, 0x00000100 },
  { "MMSS_A_CSI_PHY_1",                            0x00235000, 0x00001000 },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN0_CSIPHY_LN0_CSI_2PHASE_DATALN", 0x00235000, 0x000000dc },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN1_CSIPHY_LN1_CSI_3PHASE_DATALN", 0x00235100, 0x00000100 },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN2_CSIPHY_LN2_CSI_2PHASE_DATALN", 0x00235200, 0x000000dc },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN3_CSIPHY_LN3_CSI_3PHASE_DATALN", 0x00235300, 0x00000100 },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN4_CSIPHY_LN4_CSI_2PHASE_DATALN", 0x00235400, 0x000000dc },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN5_CSIPHY_LN5_CSI_3PHASE_DATALN", 0x00235500, 0x00000100 },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LN6_CSIPHY_LN6_CSI_2PHASE_DATALN", 0x00235600, 0x000000dc },
  { "MMSS_A_CSI_PHY_1_CSIPHY_LNCK_CSIPHY_LNCK_CSI_2PHASE_DATALN", 0x00235700, 0x000000dc },
  { "MMSS_A_CSI_PHY_1_CSIPHY_CMN_CSIPHY_CMN_CSI_3PHASE_COMMON", 0x00235800, 0x00000100 },
  { "MMSS_A_CSI_PHY_2",                            0x00236000, 0x00001000 },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN0_CSIPHY_LN0_CSI_2PHASE_DATALN", 0x00236000, 0x000000dc },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN1_CSIPHY_LN1_CSI_3PHASE_DATALN", 0x00236100, 0x00000100 },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN2_CSIPHY_LN2_CSI_2PHASE_DATALN", 0x00236200, 0x000000dc },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN3_CSIPHY_LN3_CSI_3PHASE_DATALN", 0x00236300, 0x00000100 },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN4_CSIPHY_LN4_CSI_2PHASE_DATALN", 0x00236400, 0x000000dc },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN5_CSIPHY_LN5_CSI_3PHASE_DATALN", 0x00236500, 0x00000100 },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LN6_CSIPHY_LN6_CSI_2PHASE_DATALN", 0x00236600, 0x000000dc },
  { "MMSS_A_CSI_PHY_2_CSIPHY_LNCK_CSIPHY_LNCK_CSI_2PHASE_DATALN", 0x00236700, 0x000000dc },
  { "MMSS_A_CSI_PHY_2_CSIPHY_CMN_CSIPHY_CMN_CSI_3PHASE_COMMON", 0x00236800, 0x00000100 },
  { "MMSS_A_VBIF_VFE",                             0x00240000, 0x00003000 },
  { "MMSS_A_VBIF_VFE_VBIF_VFE_DENOISE",            0x00240000, 0x00003000 },
  { "MMSS_A_VBIF_JPEG",                            0x00260000, 0x00003000 },
  { "MMSS_A_VBIF_CPP",                             0x00280000, 0x00003000 },
  { "MMSS_A_JPEG_3",                               0x002a0000, 0x00004000 },
  { "MMSS_A_FD_FD_CORE",                           0x002a4000, 0x00004000 },
  { "MMSS_A_FD_A_FD",                              0x002a4000, 0x00000800 },
  { "MMSS_A_FD_A_FD_MISC",                         0x002a5000, 0x00000400 },
  { "MMSS_OXILI_A5X",                              0x00300000, 0x0006000d },
  { "MMSS_OXILI_RBBM_BLOCK0",                      0x00300000, 0x00002000 },
  { "MMSS_OXILI_RBBM_BLOCK7",                      0x0033c000, 0x00004000 },
  { "MMSS_OXILI_CP_BLOCK0",                        0x00302000, 0x00000f00 },
  { "MMSS_OXILI_VSC_BLOCK0",                       0x00302f00, 0x00000300 },
  { "MMSS_OXILI_UCHE_BLOCK0",                      0x00303a00, 0x00000100 },
  { "MMSS_OXILI_GRAS_BLOCK0",                      0x00303200, 0x00000100 },
  { "MMSS_OXILI_GRAS_BLOCK1",                      0x00308600, 0x00000100 },
  { "MMSS_OXILI_GRAS_BLOCK2",                      0x00309600, 0x00000100 },
  { "MMSS_OXILI_GRAS_BLOCK3",                      0x00338000, 0x00000500 },
  { "MMSS_OXILI_GRAS_BLOCK4",                      0x0033a000, 0x00000500 },
  { "MMSS_OXILI_RB_BLOCK0",                        0x00303300, 0x00000100 },
  { "MMSS_OXILI_RB_BLOCK1",                        0x00308400, 0x00000200 },
  { "MMSS_OXILI_RB_BLOCK2",                        0x00309400, 0x00000200 },
  { "MMSS_OXILI_RB_BLOCK3",                        0x00338500, 0x00000500 },
  { "MMSS_OXILI_RB_BLOCK4",                        0x0033a500, 0x00000500 },
  { "MMSS_OXILI_VPC_BLOCK0",                       0x00303980, 0x00000080 },
  { "MMSS_OXILI_VPC_BLOCK3",                       0x00338a00, 0x00000400 },
  { "MMSS_OXILI_VPC_BLOCK4",                       0x0033aa00, 0x00000400 },
  { "MMSS_OXILI_VPC_BLOCK5",                       0x00334000, 0x00000800 },
  { "MMSS_OXILI_PC_BLOCK0",                        0x00303400, 0x00000100 },
  { "MMSS_OXILI_PC_BLOCK1",                        0x00308300, 0x00000100 },
  { "MMSS_OXILI_PC_BLOCK2",                        0x00309300, 0x00000100 },
  { "MMSS_OXILI_PC_BLOCK3",                        0x00338e00, 0x00000200 },
  { "MMSS_OXILI_PC_BLOCK4",                        0x0033ae00, 0x00000200 },
  { "MMSS_OXILI_VFD_BLOCK0",                       0x00303900, 0x00000080 },
  { "MMSS_OXILI_VFD_BLOCK3",                       0x00339000, 0x00000600 },
  { "MMSS_OXILI_VFD_BLOCK4",                       0x0033b000, 0x00000600 },
  { "MMSS_OXILI_VFD_BLOCK5",                       0x00334800, 0x00000100 },
  { "MMSS_OXILI_SP_BLOCK0",                        0x00303b00, 0x00000100 },
  { "MMSS_OXILI_SP_BLOCK1",                        0x00308100, 0x00000100 },
  { "MMSS_OXILI_SP_BLOCK2",                        0x00309100, 0x00000100 },
  { "MMSS_OXILI_SP_BLOCK3",                        0x00339600, 0x00000600 },
  { "MMSS_OXILI_SP_BLOCK4",                        0x0033b600, 0x00000600 },
  { "MMSS_OXILI_TPL1_BLOCK0",                      0x00303c00, 0x00000100 },
  { "MMSS_OXILI_TPL1_BLOCK1",                      0x00308000, 0x00000100 },
  { "MMSS_OXILI_TPL1_BLOCK2",                      0x00309000, 0x00000100 },
  { "MMSS_OXILI_TPL1_BLOCK3",                      0x00339c00, 0x00000200 },
  { "MMSS_OXILI_TPL1_BLOCK4",                      0x0033bc00, 0x00000200 },
  { "MMSS_OXILI_HLSQ_BLOCK0",                      0x00303800, 0x00000100 },
  { "MMSS_OXILI_HLSQ_BLOCK1",                      0x00308200, 0x00000100 },
  { "MMSS_OXILI_HLSQ_BLOCK2",                      0x00309200, 0x00000100 },
  { "MMSS_OXILI_HLSQ_BLOCK3",                      0x00339e00, 0x00000200 },
  { "MMSS_OXILI_HLSQ_BLOCK4",                      0x0033be00, 0x00000200 },
  { "MMSS_OXILI_HLSQ_BLOCK5",                      0x00320000, 0x00002000 },
  { "MMSS_OXILI_HLSQ_BLOCK6",                      0x00328000, 0x00002000 },
  { "MMSS_OXILI_HLSQ_BLOCK7",                      0x0032f000, 0x00001000 },
  { "MMSS_OXILI_GPMU_BLOCK0",                      0x0032a000, 0x00005000 },
  { "MMSS_OXILI_GPMU_BLOCK5",                      0x00322000, 0x00006000 },
  { "MMSS_OXILI_CX_BLOCK0",                        0x00360000, 0x0000000d },
  { "MMSS_OXILI_VBIF",                             0x0030c000, 0x00003000 },
  { "MMSS_OXILI_VBIF_3D",                          0x0030c000, 0x00003000 },
  { "MMSS_OXILI_SMMU",                             0x00340000, 0x00020000 },
  { "MMSS_OXILI_SMMU_GLOBAL0",                     0x00340000, 0x00001000 },
  { "MMSS_OXILI_SMMU_GLOBAL1",                     0x00341000, 0x00001000 },
  { "MMSS_OXILI_SMMU_IMPL_DEF0",                   0x00342000, 0x00001000 },
  { "MMSS_OXILI_SMMU_PERF_MON",                    0x00343000, 0x00001000 },
  { "MMSS_OXILI_SMMU_SSD",                         0x00344000, 0x00001000 },
  { "MMSS_OXILI_SMMU_IMPL_DEF1",                   0x00346000, 0x00001000 },
  { "MMSS_OXILI_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00348000, 0x00001000 },
  { "MMSS_OXILI_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00349000, 0x00001000 },
  { "MMSS_OXILI_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0034a000, 0x00001000 },
  { "MMSS_OXILI_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0034b000, 0x00001000 },
  { "MMSS_OXILI_GDPM_DPM",                         0x0032e000, 0x00000200 },
  { "MMSS_OXILI_SP_LDPM",                          0x0032e400, 0x00000080 },
  { "MMSS_OXILI_TP_LDPM",                          0x0032e480, 0x00000080 },
  { "MMSS_OXILI_RBCCU_LDPM",                       0x0032e500, 0x00000080 },
  { "MMSS_OXILI_UCHE_LDPM",                        0x0032e580, 0x00000080 },
  { "MMSS_OXILI_MISC_LDPM",                        0x0032e600, 0x00000080 },
  { "MMSS_VENUS0",                                 0x00400000, 0x00100000 },
  { "MMSS_VENUS0_VENC_TOP_BASE",                   0x00400000, 0x00001000 },
  { "MMSS_VENUS0_VENC_SS",                         0x00400000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VSP_BASE",                   0x00401000, 0x00003000 },
  { "MMSS_VENUS0_VENC_VSP_TOP",                    0x00401000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VSP_CCE",                    0x00402000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VSP_SDE",                    0x00403000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_BASE",                   0x00404000, 0x0000a000 },
  { "MMSS_VENUS0_VENC_VPP_TOP",                    0x00404000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_SE",                     0x00405000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_PE",                     0x00406000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_TE",                     0x00407000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_FE",                     0x00408000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_SSE",                    0x00409000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_ISE",                    0x0040a000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_FSE",                    0x0040b000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_VPE",                    0x0040c000, 0x00001000 },
  { "MMSS_VENUS0_VENC_VPP_MVP",                    0x0040d000, 0x00001000 },
  { "MMSS_VENUS0_VENC_DMA_BASE",                   0x0040f000, 0x00001000 },
  { "MMSS_VENUS0_VENC_DMA",                        0x0040f000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_TOP_BASE",                   0x00430000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_SS",                         0x00430000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VSP_BASE",                   0x00431000, 0x00003000 },
  { "MMSS_VENUS0_VDEC_VSP_TOP",                    0x00431000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VSP_CCE",                    0x00432000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VSP_SDE",                    0x00433000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_BASE",                   0x00434000, 0x00006000 },
  { "MMSS_VENUS0_VDEC_VPP_TOP",                    0x00434000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_SE",                     0x00435000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_PE",                     0x00436000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_TE",                     0x00437000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_FE",                     0x00438000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_VPP_PPE",                    0x00439000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_DMA_BASE",                   0x0043f000, 0x00001000 },
  { "MMSS_VENUS0_VDEC_DMA",                        0x0043f000, 0x00001000 },
  { "MMSS_VENUS0_VENUS_CPU_BASE",                  0x004c0000, 0x00020000 },
  { "MMSS_VENUS0_CPU_WD",                          0x004c1000, 0x00001000 },
  { "MMSS_VENUS0_CPU_TM",                          0x004c2000, 0x00001000 },
  { "MMSS_VENUS0_CPU_CS",                          0x004d2000, 0x00001000 },
  { "MMSS_VENUS0_CPU_IQ",                          0x004df000, 0x00001000 },
  { "MMSS_VENUS0_VENUS_VENUS_VBIF_TOP_VENUS_ISTARI", 0x00480000, 0x00003000 },
  { "MMSS_VENUS0_VENUS_VBIF_VENUS",                0x00480000, 0x00003000 },
  { "MMSS_VENUS0_VENUS_WRAPPER_BASE",              0x004e0000, 0x00020000 },
  { "MMSS_VENUS0_VENUS_WRAPPER_TOP",               0x004e0000, 0x00010000 },
  { "MMSS_VENUS0_VENUS_VENUS_APU1132_3_QR",        0x004f0000, 0x00000380 },
  { "MMSS_OCMEM",                                  0x00080000, 0x00000800 },
  { "MMAGIC_QHS_MMSS_U_AHB_BUS_TIMEOUT0",          0x00000000, 0x00001000 },
  { "MMAGIC_QHS_MMSS1_U_AHB_BUS_TIMEOUT1",         0x00001000, 0x00001000 },
  { "MMAGIC_QHS_MMSS2_U_AHB_BUS_TIMEOUT2",         0x00002000, 0x00001000 },
  { "MMAGIC_QHS_MMSS3_U_AHB_BUS_TIMEOUT3",         0x00003000, 0x00001000 },
  { "MMAGIC_QHS_MMSS4_U_AHB_BUS_TIMEOUT4",         0x00004000, 0x00001000 },
  { "MMAGIC_MDP_U_QSMMUV2_WRAP_MDP_ISTARI",        0x00500000, 0x00020000 },
  { "MMAGIC_MDP_SMMU",                             0x00500000, 0x00020000 },
  { "MMAGIC_MDP_SMMU_GLOBAL0",                     0x00500000, 0x00001000 },
  { "MMAGIC_MDP_SMMU_GLOBAL1",                     0x00501000, 0x00001000 },
  { "MMAGIC_MDP_SMMU_IMPL_DEF0",                   0x00502000, 0x00001000 },
  { "MMAGIC_MDP_SMMU_PERF_MON",                    0x00503000, 0x00001000 },
  { "MMAGIC_MDP_SMMU_SSD",                         0x00504000, 0x00001000 },
  { "MMAGIC_MDP_SMMU_IMPL_DEF1",                   0x00506000, 0x00001000 },
  { "MMAGIC_MDP_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00508000, 0x00001000 },
  { "MMAGIC_MDP_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00509000, 0x00001000 },
  { "MMAGIC_MDP_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0050a000, 0x00001000 },
  { "MMAGIC_MDP_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0050b000, 0x00001000 },
  { "MMAGIC_ROT_U_QSMMUV2_WRAP_ROT_ISTARI",        0x00520000, 0x00020000 },
  { "MMAGIC_ROT_SMMU",                             0x00520000, 0x00020000 },
  { "MMAGIC_ROT_SMMU_GLOBAL0",                     0x00520000, 0x00001000 },
  { "MMAGIC_ROT_SMMU_GLOBAL1",                     0x00521000, 0x00001000 },
  { "MMAGIC_ROT_SMMU_IMPL_DEF0",                   0x00522000, 0x00001000 },
  { "MMAGIC_ROT_SMMU_PERF_MON",                    0x00523000, 0x00001000 },
  { "MMAGIC_ROT_SMMU_SSD",                         0x00524000, 0x00001000 },
  { "MMAGIC_ROT_SMMU_IMPL_DEF1",                   0x00526000, 0x00001000 },
  { "MMAGIC_ROT_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00528000, 0x00001000 },
  { "MMAGIC_ROT_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00529000, 0x00001000 },
  { "MMAGIC_ROT_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0052a000, 0x00001000 },
  { "MMAGIC_ROT_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0052b000, 0x00001000 },
  { "MMAGIC_VENUS_U_QSMMUV2_WRAP_VENUS_ISTARI",    0x00540000, 0x00020000 },
  { "MMAGIC_VENUS_SMMU",                           0x00540000, 0x00020000 },
  { "MMAGIC_VENUS_SMMU_GLOBAL0",                   0x00540000, 0x00001000 },
  { "MMAGIC_VENUS_SMMU_GLOBAL1",                   0x00541000, 0x00001000 },
  { "MMAGIC_VENUS_SMMU_IMPL_DEF0",                 0x00542000, 0x00001000 },
  { "MMAGIC_VENUS_SMMU_PERF_MON",                  0x00543000, 0x00001000 },
  { "MMAGIC_VENUS_SMMU_SSD",                       0x00544000, 0x00001000 },
  { "MMAGIC_VENUS_SMMU_IMPL_DEF1",                 0x00546000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB0_CB0_MM_MMU_CONTEXT_BANK",  0x00550000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB1_CB1_MM_MMU_CONTEXT_BANK",  0x00551000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB2_CB2_MM_MMU_CONTEXT_BANK",  0x00552000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB3_CB3_MM_MMU_CONTEXT_BANK",  0x00553000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB4_CB4_MM_MMU_CONTEXT_BANK",  0x00554000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB5_CB5_MM_MMU_CONTEXT_BANK",  0x00555000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB6_CB6_MM_MMU_CONTEXT_BANK",  0x00556000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB7_CB7_MM_MMU_CONTEXT_BANK",  0x00557000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB8_CB8_MM_MMU_CONTEXT_BANK",  0x00558000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB9_CB9_MM_MMU_CONTEXT_BANK",  0x00559000, 0x00001000 },
  { "MMAGIC_VENUS_A_CB10_CB10_MM_MMU_CONTEXT_BANK", 0x0055a000, 0x00001000 },
  { "MMAGIC_CPP_U_QSMMUV2_WRAP_CPP_ISTARI",        0x00560000, 0x00020000 },
  { "MMAGIC_CPP_SMMU",                             0x00560000, 0x00020000 },
  { "MMAGIC_CPP_SMMU_GLOBAL0",                     0x00560000, 0x00001000 },
  { "MMAGIC_CPP_SMMU_GLOBAL1",                     0x00561000, 0x00001000 },
  { "MMAGIC_CPP_SMMU_IMPL_DEF0",                   0x00562000, 0x00001000 },
  { "MMAGIC_CPP_SMMU_PERF_MON",                    0x00563000, 0x00001000 },
  { "MMAGIC_CPP_SMMU_SSD",                         0x00564000, 0x00001000 },
  { "MMAGIC_CPP_SMMU_IMPL_DEF1",                   0x00566000, 0x00001000 },
  { "MMAGIC_CPP_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00568000, 0x00001000 },
  { "MMAGIC_CPP_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00569000, 0x00001000 },
  { "MMAGIC_CPP_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0056a000, 0x00001000 },
  { "MMAGIC_CPP_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0056b000, 0x00001000 },
  { "MMAGIC_JPEG_U_QSMMUV2_WRAP_JPEG_ISTARI",      0x00580000, 0x00020000 },
  { "MMAGIC_JPEG_SMMU",                            0x00580000, 0x00020000 },
  { "MMAGIC_JPEG_SMMU_GLOBAL0",                    0x00580000, 0x00001000 },
  { "MMAGIC_JPEG_SMMU_GLOBAL1",                    0x00581000, 0x00001000 },
  { "MMAGIC_JPEG_SMMU_IMPL_DEF0",                  0x00582000, 0x00001000 },
  { "MMAGIC_JPEG_SMMU_PERF_MON",                   0x00583000, 0x00001000 },
  { "MMAGIC_JPEG_SMMU_SSD",                        0x00584000, 0x00001000 },
  { "MMAGIC_JPEG_SMMU_IMPL_DEF1",                  0x00586000, 0x00001000 },
  { "MMAGIC_JPEG_A_CB0_CB0_MM_MMU_CONTEXT_BANK",   0x00588000, 0x00001000 },
  { "MMAGIC_JPEG_A_CB1_CB1_MM_MMU_CONTEXT_BANK",   0x00589000, 0x00001000 },
  { "MMAGIC_JPEG_A_CB2_CB2_MM_MMU_CONTEXT_BANK",   0x0058a000, 0x00001000 },
  { "MMAGIC_JPEG_A_CB3_CB3_MM_MMU_CONTEXT_BANK",   0x0058b000, 0x00001000 },
  { "MMAGIC_VFE_U_QSMMUV2_WRAP_VFE_ISTARI",        0x005a0000, 0x00020000 },
  { "MMAGIC_VFE_SMMU",                             0x005a0000, 0x00020000 },
  { "MMAGIC_VFE_SMMU_GLOBAL0",                     0x005a0000, 0x00001000 },
  { "MMAGIC_VFE_SMMU_GLOBAL1",                     0x005a1000, 0x00001000 },
  { "MMAGIC_VFE_SMMU_IMPL_DEF0",                   0x005a2000, 0x00001000 },
  { "MMAGIC_VFE_SMMU_PERF_MON",                    0x005a3000, 0x00001000 },
  { "MMAGIC_VFE_SMMU_SSD",                         0x005a4000, 0x00001000 },
  { "MMAGIC_VFE_SMMU_IMPL_DEF1",                   0x005a6000, 0x00001000 },
  { "MMAGIC_VFE_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x005a8000, 0x00001000 },
  { "MMAGIC_VFE_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x005a9000, 0x00001000 },
  { "MMAGIC_VFE_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x005aa000, 0x00001000 },
  { "MMAGIC_VFE_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x005ab000, 0x00001000 },
  { "MMAGIC_DISPLAY_U_THROTTLE_WRAPPER_DISPLAY",   0x000b1000, 0x00001000 },
  { "MMAGIC_CAMSS_U_THROTTLE_WRAPPER_CAMSS",       0x000b2000, 0x00001000 },
  { "MMAGIC_VENUS_U_THROTTLE_WRAPPER_VENUS",       0x000b3000, 0x00001000 },
  { "MMAGIC_U_DSA",                                0x000b4000, 0x00004000 },
  { "MMAGIC_U_MMAGIC_CORE_RIF_LEGACY_WRAPPER",     0x000a0000, 0x00010000 },
  { "MMAGIC_XPU2_MNOC_U_XPU2_AHB_WRAPPER_MNOC",    0x00010000, 0x00000780 },
  { "MMAGIC_DSA_MPU_U_XPU2_AHB_WRAPPER",           0x00011000, 0x00000380 },
  { "MMSS_XPU2_MMSS_CC_U_XPU2_MMCC_WRAPPER",       0x00012000, 0x00000800 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_TLMM[] =
{
  { "TLMM_APU1032_175",                            0x00000000, 0x00005980 },
  { "TLMM_CSR",                                    0x00010000, 0x00300000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS_Q6_SMMU_QSMMUV2_WRAP_LPASS_Q6_ISTARI[] =
{
  { "LPASS_Q6_SMMU",                               0x00000000, 0x00020000 },
  { "LPASS_Q6_SMMU_SMMU_GLOBAL0",                  0x00000000, 0x00001000 },
  { "LPASS_Q6_SMMU_SMMU_GLOBAL1",                  0x00001000, 0x00001000 },
  { "LPASS_Q6_SMMU_SMMU_IMPL_DEF0",                0x00002000, 0x00001000 },
  { "LPASS_Q6_SMMU_SMMU_PERF_MON",                 0x00003000, 0x00001000 },
  { "LPASS_Q6_SMMU_SMMU_SSD",                      0x00004000, 0x00001000 },
  { "LPASS_Q6_SMMU_SMMU_IMPL_DEF1",                0x00006000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK", 0x00010000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK", 0x00011000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK", 0x00012000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK", 0x00013000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB4_CB4_MM_MMU_CONTEXT_BANK", 0x00014000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB5_CB5_MM_MMU_CONTEXT_BANK", 0x00015000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB6_CB6_MM_MMU_CONTEXT_BANK", 0x00016000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB7_CB7_MM_MMU_CONTEXT_BANK", 0x00017000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB8_CB8_MM_MMU_CONTEXT_BANK", 0x00018000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB9_CB9_MM_MMU_CONTEXT_BANK", 0x00019000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB10_CB10_MM_MMU_CONTEXT_BANK", 0x0001a000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB11_CB11_MM_MMU_CONTEXT_BANK", 0x0001b000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB12_CB12_MM_MMU_CONTEXT_BANK", 0x0001c000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB13_CB13_MM_MMU_CONTEXT_BANK", 0x0001d000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB14_CB14_MM_MMU_CONTEXT_BANK", 0x0001e000, 0x00001000 },
  { "LPASS_Q6_SMMU_A_CB15_CB15_MM_MMU_CONTEXT_BANK", 0x0001f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS_CORE_SMMU_QSMMUV2_WRAP_LPASS_CORE_ISTARI[] =
{
  { "LPASS_CORE_SMMU",                             0x00000000, 0x00020000 },
  { "LPASS_CORE_SMMU_SMMU_GLOBAL0",                0x00000000, 0x00001000 },
  { "LPASS_CORE_SMMU_SMMU_GLOBAL1",                0x00001000, 0x00001000 },
  { "LPASS_CORE_SMMU_SMMU_IMPL_DEF0",              0x00002000, 0x00001000 },
  { "LPASS_CORE_SMMU_SMMU_PERF_MON",               0x00003000, 0x00001000 },
  { "LPASS_CORE_SMMU_SMMU_SSD",                    0x00004000, 0x00001000 },
  { "LPASS_CORE_SMMU_SMMU_IMPL_DEF1",              0x00006000, 0x00001000 },
  { "LPASS_CORE_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK", 0x00008000, 0x00001000 },
  { "LPASS_CORE_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK", 0x00009000, 0x00001000 },
  { "LPASS_CORE_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK", 0x0000a000, 0x00001000 },
  { "LPASS_CORE_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK", 0x0000b000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ANOC0_SMMU_QSMMUV2_WRAP_PCIE_ISTARI[] =
{
  { "ANOC0_SMMU",                                  0x00000000, 0x00020000 },
  { "ANOC0_SMMU_SMMU_GLOBAL0",                     0x00000000, 0x00001000 },
  { "ANOC0_SMMU_SMMU_GLOBAL1",                     0x00001000, 0x00001000 },
  { "ANOC0_SMMU_SMMU_IMPL_DEF0",                   0x00002000, 0x00001000 },
  { "ANOC0_SMMU_SMMU_PERF_MON",                    0x00003000, 0x00001000 },
  { "ANOC0_SMMU_SMMU_SSD",                         0x00004000, 0x00001000 },
  { "ANOC0_SMMU_SMMU_IMPL_DEF1",                   0x00006000, 0x00001000 },
  { "ANOC0_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00008000, 0x00001000 },
  { "ANOC0_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00009000, 0x00001000 },
  { "ANOC0_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0000a000, 0x00001000 },
  { "ANOC0_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0000b000, 0x00001000 },
  { "ANOC0_SMMU_A_CB4_CB4_MM_MMU_CONTEXT_BANK",    0x0000c000, 0x00001000 },
  { "ANOC0_SMMU_A_CB5_CB5_MM_MMU_CONTEXT_BANK",    0x0000d000, 0x00001000 },
  { "ANOC0_SMMU_A_CB6_CB6_MM_MMU_CONTEXT_BANK",    0x0000e000, 0x00001000 },
  { "ANOC0_SMMU_A_CB7_CB7_MM_MMU_CONTEXT_BANK",    0x0000f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ANOC1_SMMU_QSMMUV2_WRAP_ANOC1_ISTARI[] =
{
  { "ANOC1_SMMU",                                  0x00000000, 0x00020000 },
  { "ANOC1_SMMU_SMMU_GLOBAL0",                     0x00000000, 0x00001000 },
  { "ANOC1_SMMU_SMMU_GLOBAL1",                     0x00001000, 0x00001000 },
  { "ANOC1_SMMU_SMMU_IMPL_DEF0",                   0x00002000, 0x00001000 },
  { "ANOC1_SMMU_SMMU_PERF_MON",                    0x00003000, 0x00001000 },
  { "ANOC1_SMMU_SMMU_SSD",                         0x00004000, 0x00001000 },
  { "ANOC1_SMMU_SMMU_IMPL_DEF1",                   0x00006000, 0x00001000 },
  { "ANOC1_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00008000, 0x00001000 },
  { "ANOC1_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00009000, 0x00001000 },
  { "ANOC1_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x0000a000, 0x00001000 },
  { "ANOC1_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x0000b000, 0x00001000 },
  { "ANOC1_SMMU_A_CB4_CB4_MM_MMU_CONTEXT_BANK",    0x0000c000, 0x00001000 },
  { "ANOC1_SMMU_A_CB5_CB5_MM_MMU_CONTEXT_BANK",    0x0000d000, 0x00001000 },
  { "ANOC1_SMMU_A_CB6_CB6_MM_MMU_CONTEXT_BANK",    0x0000e000, 0x00001000 },
  { "ANOC1_SMMU_A_CB7_CB7_MM_MMU_CONTEXT_BANK",    0x0000f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_ANOC2_SMMU_QSMMUV2_WRAP_ANOC2_ISTARI[] =
{
  { "ANOC2_SMMU",                                  0x00000000, 0x00020000 },
  { "ANOC2_SMMU_SMMU_GLOBAL0",                     0x00000000, 0x00001000 },
  { "ANOC2_SMMU_SMMU_GLOBAL1",                     0x00001000, 0x00001000 },
  { "ANOC2_SMMU_SMMU_IMPL_DEF0",                   0x00002000, 0x00001000 },
  { "ANOC2_SMMU_SMMU_PERF_MON",                    0x00003000, 0x00001000 },
  { "ANOC2_SMMU_SMMU_SSD",                         0x00004000, 0x00001000 },
  { "ANOC2_SMMU_SMMU_IMPL_DEF1",                   0x00006000, 0x00001000 },
  { "ANOC2_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK",    0x00010000, 0x00001000 },
  { "ANOC2_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK",    0x00011000, 0x00001000 },
  { "ANOC2_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK",    0x00012000, 0x00001000 },
  { "ANOC2_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK",    0x00013000, 0x00001000 },
  { "ANOC2_SMMU_A_CB4_CB4_MM_MMU_CONTEXT_BANK",    0x00014000, 0x00001000 },
  { "ANOC2_SMMU_A_CB5_CB5_MM_MMU_CONTEXT_BANK",    0x00015000, 0x00001000 },
  { "ANOC2_SMMU_A_CB6_CB6_MM_MMU_CONTEXT_BANK",    0x00016000, 0x00001000 },
  { "ANOC2_SMMU_A_CB7_CB7_MM_MMU_CONTEXT_BANK",    0x00017000, 0x00001000 },
  { "ANOC2_SMMU_A_CB8_CB8_MM_MMU_CONTEXT_BANK",    0x00018000, 0x00001000 },
  { "ANOC2_SMMU_A_CB9_CB9_MM_MMU_CONTEXT_BANK",    0x00019000, 0x00001000 },
  { "ANOC2_SMMU_A_CB10_CB10_MM_MMU_CONTEXT_BANK",  0x0001a000, 0x00001000 },
  { "ANOC2_SMMU_A_CB11_CB11_MM_MMU_CONTEXT_BANK",  0x0001b000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MDM_Q6_SMMU_QSMMUV2_WRAP_MDM_Q6_ISTARI[] =
{
  { "MDM_Q6_SMMU",                                 0x00000000, 0x00020000 },
  { "MDM_Q6_SMMU_SMMU_GLOBAL0",                    0x00000000, 0x00001000 },
  { "MDM_Q6_SMMU_SMMU_GLOBAL1",                    0x00001000, 0x00001000 },
  { "MDM_Q6_SMMU_SMMU_IMPL_DEF0",                  0x00002000, 0x00001000 },
  { "MDM_Q6_SMMU_SMMU_PERF_MON",                   0x00003000, 0x00001000 },
  { "MDM_Q6_SMMU_SMMU_SSD",                        0x00004000, 0x00001000 },
  { "MDM_Q6_SMMU_SMMU_IMPL_DEF1",                  0x00006000, 0x00001000 },
  { "MDM_Q6_SMMU_A_CB0_CB0_MM_MMU_CONTEXT_BANK",   0x00008000, 0x00001000 },
  { "MDM_Q6_SMMU_A_CB1_CB1_MM_MMU_CONTEXT_BANK",   0x00009000, 0x00001000 },
  { "MDM_Q6_SMMU_A_CB2_CB2_MM_MMU_CONTEXT_BANK",   0x0000a000, 0x00001000 },
  { "MDM_Q6_SMMU_A_CB3_CB3_MM_MMU_CONTEXT_BANK",   0x0000b000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SSC[] =
{
  { "SSC_BLSP_BLSP",                               0x00680000, 0x00040000 },
  { "SSC_BLSP_BLSP_BAM",                           0x00684000, 0x0001f000 },
  { "SSC_BLSP_BLSP_BAM_XPU2",                      0x00682000, 0x00002000 },
  { "SSC_BLSP_BLSP_BAM_VMIDMT",                    0x00680000, 0x00001000 },
  { "SSC_BLSP_BLSP_UART0_UART0_DM",                0x006af000, 0x00000200 },
  { "SSC_BLSP_BLSP_UART1_UART1_DM",                0x006b0000, 0x00000200 },
  { "SSC_BLSP_BLSP_UART2_UART2_DM",                0x006b1000, 0x00000200 },
  { "SSC_BLSP_BLSP_QUP0",                          0x006b5000, 0x00000600 },
  { "SSC_BLSP_BLSP_QUP1",                          0x006b6000, 0x00000600 },
  { "SSC_BLSP_BLSP_QUP2",                          0x006b7000, 0x00000600 },
  { "SSC_QDSP6V60SS_PUBLIC",                       0x00400000, 0x00080000 },
  { "SSC_QDSP6V60SS_PUB",                          0x00400000, 0x00004040 },
  { "SSC_QDSP6V60SS_PRIVATE",                      0x00480000, 0x00080000 },
  { "SSC_QDSP6V60SS_CSR",                          0x00480000, 0x00008028 },
  { "SSC_QDSP6V60SS_L2VIC",                        0x00490000, 0x00001000 },
  { "SSC_QDSP6SS_QDSP6SS_QTMR_AC",                 0x004a0000, 0x00001000 },
  { "SSC_QDSP6SS_QTMR_F0_0",                       0x004a1000, 0x00001000 },
  { "SSC_QDSP6SS_QTMR_F1_1",                       0x004a2000, 0x00001000 },
  { "SSC_QDSP6SS_QTMR_F2_2",                       0x004a3000, 0x00001000 },
  { "SSC_QDSP6SS_QDSP6SS_SAW3",                    0x004b0000, 0x00001000 },
  { "SSC_SSC_MCC",                                 0x00600000, 0x00080000 },
  { "SSC_MCC_REGS",                                0x00600000, 0x00010000 },
  { "SSC_TLMM_GPIO",                               0x00610000, 0x00010000 },
  { "SSC_SCC",                                     0x00620000, 0x00010000 },
  { "SSC_SCC_SCC_SCC_REG",                         0x00620000, 0x00010000 },
  { "SSC_Q6_MPU_MPU1032_12_M22L11_AHB",            0x006c4000, 0x00000800 },
  { "SSC_CFG_AHBE_BUS_TIMEOUT",                    0x006c5000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MODEM_TOP[] =
{
  { "MPSS_TOP",                                    0x00180000, 0x00080000 },
  { "MPSS_RMB",                                    0x00180000, 0x00010000 },
  { "MSS_UIM0_UART_DM",                            0x00190000, 0x00000200 },
  { "MSS_UIM1_UART_DM",                            0x00194000, 0x00000200 },
  { "MSS_UIM2_UART_DM",                            0x00198000, 0x00000200 },
  { "MSS_UIM3_UART_DM",                            0x0019c000, 0x00000200 },
  { "MSS_CXM_UART_DM",                             0x001a0000, 0x00000204 },
  { "MSS_CPR3",                                    0x001a4000, 0x00004000 },
  { "MPSS_PERPH",                                  0x001a8000, 0x00002120 },
  { "MSS_CONF_BUS_TIMEOUT",                        0x001b0000, 0x00001000 },
  { "MSS_MGPI",                                    0x001b2000, 0x00000128 },
  { "MSS_MVC",                                     0x001b3000, 0x00000048 },
  { "MSS_STMR",                                    0x001b4000, 0x000000fd },
  { "MSS_RFC",                                     0x001b8000, 0x00008000 },
  { "MSS_PDMEM",                                   0x001b8000, 0x00006000 },
  { "MSS_RFC_SWI",                                 0x001bfe00, 0x00000200 },
  { "MSS_CRYPTO_TOP",                              0x001c0000, 0x00040000 },
  { "MSS_CRYPTO",                                  0x001fa000, 0x00006000 },
  { "MSS_CRYPTO_BAM",                              0x001c4000, 0x00024000 },
  { "MSS_NAV",                                     0x00200000, 0x000f888d },
  { "MSS_DANGER",                                  0x00300000, 0x00001000 },
  { "MSS_DGR_CRP_CSR",                             0x00300000, 0x00000800 },
  { "MSS_DGR_CSR",                                 0x00300800, 0x00000800 },
  { "MSS_QDSP6SS_PUB",                             0x00080000, 0x00004040 },
  { "MSS_QDSP6SS_PRIVATE",                         0x00100000, 0x00080000 },
  { "MSS_QDSP6SS_CSR",                             0x00100000, 0x00008028 },
  { "MSS_QDSP6SS_L2VIC",                           0x00110000, 0x00001000 },
  { "MSS_QDSP6SS_QDSP6SS_QTMR_AC",                 0x00120000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F0_0",                       0x00121000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F1_1",                       0x00122000, 0x00001000 },
  { "MSS_QDSP6SS_QTMR_F2_2",                       0x00123000, 0x00001000 },
  { "MSS_QDSP6SS_QDSP6SS_SAW3_QDSP6V55SS_WRAPPER", 0x00130000, 0x00001000 },
  { "MSS_QDSP6SS_SAW3",                            0x00130000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_QDSS_APB[] =
{
  { "QDSS_DAPROM",                                 0x00000000, 0x00001000 },
  { "QDSS_QDSSCSR",                                0x00001000, 0x00001000 },
  { "QDSS_CXSTM_8_32_32_TRUE",                     0x00002000, 0x00001000 },
  { "QDSS_TPDA_TPDA_TPDA_S8_W64_D8_M64_CSF9D1CB23", 0x00003000, 0x00001000 },
  { "QDSS_TPDM_TPDM_TPDM_ATB64_ATCLK_DSB256_CS319DD72A", 0x00004000, 0x00001000 },
  { "QDSS_CTI0_CTI0_CSCTI",                        0x00010000, 0x00001000 },
  { "QDSS_CTI1_CTI1_CSCTI",                        0x00011000, 0x00001000 },
  { "QDSS_CTI2_CTI2_CSCTI",                        0x00012000, 0x00001000 },
  { "QDSS_CTI3_CTI3_CSCTI",                        0x00013000, 0x00001000 },
  { "QDSS_CTI4_CTI4_CSCTI",                        0x00014000, 0x00001000 },
  { "QDSS_CTI5_CTI5_CSCTI",                        0x00015000, 0x00001000 },
  { "QDSS_CTI6_CTI6_CSCTI",                        0x00016000, 0x00001000 },
  { "QDSS_CTI7_CTI7_CSCTI",                        0x00017000, 0x00001000 },
  { "QDSS_CTI8_CTI8_CSCTI",                        0x00018000, 0x00001000 },
  { "QDSS_CTI9_CTI9_CSCTI",                        0x00019000, 0x00001000 },
  { "QDSS_CTI10_CTI10_CSCTI",                      0x0001a000, 0x00001000 },
  { "QDSS_CTI11_CTI11_CSCTI",                      0x0001b000, 0x00001000 },
  { "QDSS_CTI12_CTI12_CSCTI",                      0x0001c000, 0x00001000 },
  { "QDSS_CTI13_CTI13_CSCTI",                      0x0001d000, 0x00001000 },
  { "QDSS_CTI14_CTI14_CSCTI",                      0x0001e000, 0x00001000 },
  { "QDSS_CSTPIU_CSTPIU_CSTPIU",                   0x00020000, 0x00001000 },
  { "QDSS_IN_FUN0_IN_FUN0_CXATBFUNNEL_128W8SP",    0x00021000, 0x00001000 },
  { "QDSS_IN_FUN1_IN_FUN1_CXATBFUNNEL_128W8SP",    0x00022000, 0x00001000 },
  { "QDSS_IN_FUN2_IN_FUN2_CXATBFUNNEL_128W8SP",    0x00023000, 0x00001000 },
  { "QDSS_IN_FUN3_IN_FUN3_CXATBFUNNEL_128W8SP",    0x00024000, 0x00001000 },
  { "QDSS_MERG_FUN_MERG_FUN_CXATBFUNNEL_128W4SP",  0x00025000, 0x00001000 },
  { "QDSS_REPL64_REPL64_CXATBREPLICATOR_64WP",     0x00026000, 0x00001000 },
  { "QDSS_ETFETB_ETFETB_CXTMC_F128W64K",           0x00027000, 0x00001000 },
  { "QDSS_ETR_ETR_CXTMC_R64W32D",                  0x00028000, 0x00001000 },
  { "QDSS_SSC_CTI_0_SSC_CTI_0_CSCTI",              0x00030000, 0x00001000 },
  { "QDSS_SSC_CTI_1_SSC_CTI_1_CSCTI",              0x00031000, 0x00001000 },
  { "QDSS_SSC_STM_SSC_STM_CXSTM_2_4_4_TRUE",       0x00033000, 0x00001000 },
  { "QDSS_SSC_FUN0_SSC_FUN0_CXATBFUNNEL_32W2SP",   0x00034000, 0x00001000 },
  { "QDSS_SSC_ETFETB_SSC_ETFETB_CXTMC_F32W8K",     0x00035000, 0x00001000 },
  { "QDSS_VSENSE_CONTROLLER",                      0x00038000, 0x00001000 },
  { "QDSS_MSS_QDSP6_CTI_MSS_QDSP6_CTI_CSCTI",      0x00040000, 0x00001000 },
  { "QDSS_QDSP6_CTI_QDSP6_CTI_CSCTI",              0x00044000, 0x00001000 },
  { "QDSS_RPM_M3_CTI_RPM_M3_CTI_CSCTI",            0x00048000, 0x00001000 },
  { "QDSS_PRNG_TPDM_PRNG_TPDM_TPDM_ATB32_APCLK_CMB32_CS5CFE4153", 0x0004c000, 0x00001000 },
  { "QDSS_PIMEM_TPDM_PIMEM_TPDM_TPDM_ATB64_APCLK_GPRCLK_BC8_TC2_CMB64_DSB64_CS4528A7E6", 0x00050000, 0x00001000 },
  { "QDSS_DCC_TPDM_DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1", 0x00054000, 0x00001000 },
  { "QDSS_ISTARI_APB2JTAG",                        0x00058000, 0x00004000 },
  { "QDSS_VMIDDAP_VMIDDAP_VMIDMT_IDX_2_SSD1",      0x00076000, 0x00001000 },
  { "QDSS_VMIDETR_VMIDETR_VMIDMT_IDX_2_SSD1",      0x00077000, 0x00001000 },
  { "QDSS_NDPBAM_NDPBAM_BAM_NDP_TOP_AUTO_SCALE_V2_0", 0x00080000, 0x00019000 },
  { "QDSS_NDPBAM_BAM",                             0x00084000, 0x00015000 },
  { "QDSS_ISDB_CTI_ISDB_CTI_CSCTI",                0x00141000, 0x00001000 },
  { "QDSS_GPMU_CTI_GPMU_CTI_CSCTI",                0x00142000, 0x00001000 },
  { "QDSS_ARM9_CTI_ARM9_CTI_CSCTI",                0x00180000, 0x00001000 },
  { "QDSS_VENUS_ARM9_ETM_VENUS_ARM9_ETM_A9ETM_V2", 0x00181000, 0x00001000 },
  { "QDSS_MMSS_FUNNEL_MMSS_FUNNEL_CXATBFUNNEL_64W8SP", 0x00184000, 0x00001000 },
  { "QDSS_MMSS_DSAT_TPDM_MMSS_DSAT_TPDM_TPDM_ATB32_APCLK_GPRCLK_BC32_TC3_DSB128_CS7D2CD278", 0x00185000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_HMSS_DBG_HMSS_DEBUG[] =
{
  { "HMSS_DBG_APC0_CPU0_DBG_APC0_CPU0_DBG_HYDRASR_APB_PSELDBG", 0x00010000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU0_CTI_APC0_CPU0_CTI_CSCTI",  0x00020000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU0_PMU_APC0_CPU0_PMU_HYDRASR_APB_PSELPM", 0x00030000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU0_TRC_APC0_CPU0_TRC_HYDRASR_APB_PSELETM", 0x00040000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU0_PMU_CTI_APC0_CPU0_PMU_CTI_CSCTI", 0x000a0000, 0x00001000 },
  { "HMSS_DBG_APC0_L2_PMU_CTI_APC0_L2_PMU_CTI_CSCTI", 0x000b0000, 0x00001000 },
  { "HMSS_DBG_M4M_DBG_M4M_DBG_DBGCSR",             0x000c0000, 0x00010000 },
  { "HMSS_DBG_M4M_CTI_M4M_CTI_CSCTI",              0x000d0000, 0x00001000 },
  { "HMSS_DBG_M4M_PMU_M4M_PMU_PMONCSR",            0x000e0000, 0x00010000 },
  { "HMSS_DBG_APC0_CPU1_DBG_APC0_CPU1_DBG_HYDRASR_APB_PSELDBG", 0x00110000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU1_CTI_APC0_CPU1_CTI_CSCTI",  0x00120000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU1_PMU_APC0_CPU1_PMU_HYDRASR_APB_PSELPM", 0x00130000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU1_TRC_APC0_CPU1_TRC_HYDRASR_APB_PSELETM", 0x00140000, 0x00001000 },
  { "HMSS_DBG_APC0_CPU1_PMU_CTI_APC0_CPU1_PMU_CTI_CSCTI", 0x001a0000, 0x00001000 },
  { "HMSS_DBG_APC0_TRC_FNL_APC0_TRC_FNL_CXATBFUNNEL_128W2SP", 0x001b0000, 0x00001000 },
  { "HMSS_DBG_LM_0_CTI_LM_0_CTI_CSCTI",            0x001c0000, 0x00001000 },
  { "HMSS_DBG_LM_1_CTI_LM_1_CTI_CSCTI",            0x001d0000, 0x00001000 },
  { "HMSS_DBG_TPDA_CFG_TPDA_CFG_TPDA_S2_W32_D2_M32_CS6B2DD87D", 0x001e0000, 0x00001000 },
  { "HMSS_DBG_TPDA_CTI_TPDA_CTI_CSCTI",            0x001f0000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU0_DBG_APC1_CPU0_DBG_HYDRASR_APB_PSELDBG", 0x00210000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU0_CTI_APC1_CPU0_CTI_CSCTI",  0x00220000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU0_PMU_APC1_CPU0_PMU_HYDRASR_APB_PSELPM", 0x00230000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU0_TRC_APC1_CPU0_TRC_HYDRASR_APB_PSELETM", 0x00240000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU0_PMU_CTI_APC1_CPU0_PMU_CTI_CSCTI", 0x002a0000, 0x00001000 },
  { "HMSS_DBG_APC1_L2_PMU_CTI_APC1_L2_PMU_CTI_CSCTI", 0x002b0000, 0x00001000 },
  { "HMSS_DBG_L3_DBG_L3_DBG_HML3_APB_DBG",         0x002c0000, 0x00010000 },
  { "HMSS_DBG_L3_DBG_HML3_APB_REG",                0x002cc000, 0x00004000 },
  { "HMSS_DBG_L3_CTI_L3_CTI_CSCTI",                0x002d0000, 0x00001000 },
  { "HMSS_DBG_L3_PMU_L3_PMU_HML3_APB_PMU",         0x002e0000, 0x00010000 },
  { "HMSS_DBG_L3_PMU_HML3_APB_TPDMCS",             0x002ece00, 0x00000200 },
  { "HMSS_DBG_L3_PMU_HML3_APB_PERFMON",            0x002ec000, 0x00000e00 },
  { "HMSS_DBG_L3_PFT_L3_PFT_HML3_APB_PFT",         0x002f0000, 0x00010000 },
  { "HMSS_DBG_APC1_CPU1_DBG_APC1_CPU1_DBG_HYDRASR_APB_PSELDBG", 0x00310000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU1_CTI_APC1_CPU1_CTI_CSCTI",  0x00320000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU1_PMU_APC1_CPU1_PMU_HYDRASR_APB_PSELPM", 0x00330000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU1_TRC_APC1_CPU1_TRC_HYDRASR_APB_PSELETM", 0x00340000, 0x00001000 },
  { "HMSS_DBG_APC1_CPU1_PMU_CTI_APC1_CPU1_PMU_CTI_CSCTI", 0x003a0000, 0x00001000 },
  { "HMSS_DBG_APC1_TRC_FNL_APC1_TRC_FNL_CXATBFUNNEL_128W2SP", 0x003b0000, 0x00001000 },
  { "HMSS_DBG_APCS_FNL_APCS_FNL_CXATBFUNNEL_128W4SP", 0x003c0000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PMIC_ARB[] =
{
  { "SPMI_CFG_TOP",                                0x00000000, 0x0000d000 },
  { "SPMI_GENI_CFG",                               0x0000a000, 0x00000700 },
  { "SPMI_CFG",                                    0x0000a700, 0x00001a00 },
  { "SPMI_PIC",                                    0x01800000, 0x00200000 },
  { "PMIC_ARB_MPU1132_18_M25L12_AHB",              0x0000e000, 0x00000b00 },
  { "PMIC_ARB_CORE",                               0x0000f000, 0x00001000 },
  { "PMIC_ARB_CORE_REGISTERS",                     0x00400000, 0x00800000 },
  { "PMIC_ARB_CORE_REGISTERS_OBS",                 0x00c00000, 0x00800000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_L3_TCM[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_HMSS_QLL[] =
{
  { "APC0_QLL_HY_TOP_QLL",                         0x00000000, 0x00040000 },
  { "APC0_QLL_CPU0_PUB_CPU0_PUB_LMH_PUBLIC",       0x00020000, 0x00001000 },
  { "APC0_QLL_CPU0_PRV_CPU0_PRV_LMH_PRIVATE",      0x00010000, 0x00001000 },
  { "APC0_QLL_CPU1_PUB_CPU1_PUB_LMH_PUBLIC",       0x00021000, 0x00001000 },
  { "APC0_QLL_CPU1_PRV_CPU1_PRV_LMH_PRIVATE",      0x00011000, 0x00001000 },
  { "APC0_QLL_L2_PUB_L2_PUB_LMH_PUBLIC",           0x00022000, 0x00001000 },
  { "APC0_QLL_L2_PRV_L2_PRV_LMH_PRIVATE",          0x00012000, 0x00001000 },
  { "APC0_QLL_HYDRASR_QLL",                        0x00000000, 0x00010000 },
  { "APC1_QLL_HY_TOP_QLL",                         0x00080000, 0x00040000 },
  { "APC1_QLL_CPU0_PUB_CPU0_PUB_LMH_PUBLIC",       0x000a0000, 0x00001000 },
  { "APC1_QLL_CPU0_PRV_CPU0_PRV_LMH_PRIVATE",      0x00090000, 0x00001000 },
  { "APC1_QLL_CPU1_PUB_CPU1_PUB_LMH_PUBLIC",       0x000a1000, 0x00001000 },
  { "APC1_QLL_CPU1_PRV_CPU1_PRV_LMH_PRIVATE",      0x00091000, 0x00001000 },
  { "APC1_QLL_L2_PUB_L2_PUB_LMH_PUBLIC",           0x000a2000, 0x00001000 },
  { "APC1_QLL_L2_PRV_L2_PRV_LMH_PRIVATE",          0x00092000, 0x00001000 },
  { "APC1_QLL_HYDRASR_QLL",                        0x00080000, 0x00010000 },
  { "L3_QLL_HML3_QLL",                             0x00100000, 0x00040000 },
  { "L3_QLL_LLM_PRIVATE",                          0x00110000, 0x00001000 },
  { "L3_QLL_LLM_PUBLIC",                           0x00120000, 0x00001000 },
  { "L3_QLL_HML3_CFG",                             0x00100000, 0x00010000 },
  { "L3_QLL_HML3_QLL_PERFMON",                     0x00108000, 0x00000e00 },
  { "L3_QLL_HML3_CSR",                             0x00100000, 0x00004000 },
  { "HYDRA_GIC_QLL_HYDRASR_GICCHVBASE_EL3",        0x001c0000, 0x00040000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SNOC_1_BUS_TIMEOUT[] =
{
  { "SNOC_1_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SNOC_2_BUS_TIMEOUT[] =
{
  { "SNOC_2_BUS_TIMEOUT",                          0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_USB30_PRIM[] =
{
  { "DWC_USB3",                                    0x00000000, 0x0000cd00 },
  { "USB30_QSRAM_REGS",                            0x000fc000, 0x00000100 },
  { "USB30_QSCRATCH",                              0x000f8800, 0x00000400 },
  { "USB30_DBM_REGFILE",                           0x000f8000, 0x00000400 },
  { "USB30_BAM",                                   0x00104000, 0x0001b000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PERIPH_SS[] =
{
  { "PERIPH_SS_PDM_PERPH_WEB",                     0x00000000, 0x00004000 },
  { "PERIPH_SS_PDM_WEB_TCXO4",                     0x00000000, 0x00004000 },
  { "PERIPH_SS_BUS_TIMEOUT_0_BUS_TIMEOUT",         0x00004000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_1_BUS_TIMEOUT",         0x00005000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_2_BUS_TIMEOUT",         0x00006000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_3_BUS_TIMEOUT",         0x00007000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_4_BUS_TIMEOUT",         0x00008000, 0x00001000 },
  { "PERIPH_SS_MPU_MPU1132A_4_M15L7_AHB",          0x0000f000, 0x00000400 },
  { "PERIPH_SS_USB_PHYS_AHB2PHY",                  0x00010000, 0x00008000 },
  { "PERIPH_SS_AHB2PHY_BROADCAST_SWMAN",           0x00017000, 0x00000400 },
  { "PERIPH_SS_AHB2PHY_SWMAN",                     0x00016000, 0x00000400 },
  { "PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_SEC_CM_QUSB2_LQ_1EX", 0x00012000, 0x00000180 },
  { "PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PRIM_CM_QUSB2_LQ_1EX", 0x00011000, 0x00000180 },
  { "PERIPH_SS_USB3PHY_USB3PHY_CM_USB3_SW",        0x00010000, 0x00000800 },
  { "PERIPH_SS_USB3PHY_QSERDES_COM_QSERDES_COM_USB3_QSRV_COM", 0x00010000, 0x000001c4 },
  { "PERIPH_SS_USB3PHY_QSERDES_TX_QSERDES_TX_USB3_TX_MPHY", 0x00010200, 0x00000130 },
  { "PERIPH_SS_USB3PHY_QSERDES_RX_QSERDES_RX_USB3_RX_MPHY", 0x00010400, 0x00000200 },
  { "PERIPH_SS_USB3PHY_USB3_PHY_USB3_PHY_USB3_PCS", 0x00010600, 0x000001a8 },
  { "PERIPH_SS_QSPI_QSPI",                         0x00018000, 0x00000100 },
  { "PERIPH_SS_SDC1_SDCC5_TOP",                    0x00040000, 0x00026800 },
  { "PERIPH_SS_SDC1_SDCC_XPU2",                    0x00042000, 0x00000300 },
  { "PERIPH_SS_SDC1_SDCC_ICE",                     0x00043000, 0x00008000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_REGS",                0x00043000, 0x00002000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_LUT_KEYS",            0x00045000, 0x00001000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_XPU2",                0x00046000, 0x00000380 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5",                   0x00064000, 0x00000800 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC",                0x00064900, 0x00000500 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_CMDQ",           0x00064e00, 0x00000200 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_SCM",            0x00065000, 0x00000100 },
  { "PERIPH_SS_SDC1_SDCC_SCM",                     0x00066000, 0x00000800 },
  { "PERIPH_SS_SDC2_SDCC5_TOP",                    0x00080000, 0x00026800 },
  { "PERIPH_SS_SDC2_SDCC_XPU2",                    0x00082000, 0x00000300 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5",                   0x000a4000, 0x00000800 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC",                0x000a4900, 0x00000500 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC_SCM",            0x000a5000, 0x00000100 },
  { "PERIPH_SS_SDC2_SDCC_SCM",                     0x000a6000, 0x00000800 },
  { "PERIPH_SS_SDC4_SDCC5_TOP",                    0x00100000, 0x00026800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5",                   0x00124000, 0x00000800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC",                0x00124900, 0x00000500 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC_SCM",            0x00125000, 0x00000100 },
  { "PERIPH_SS_BLSP1_BLSP",                        0x00140000, 0x00040000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM",                    0x00144000, 0x0002b000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_XPU2",               0x00142000, 0x00002000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_VMIDMT",             0x00140000, 0x00001000 },
  { "PERIPH_SS_BLSP1_BLSP_UART0_UART0_DM",         0x0016f000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART1_UART1_DM",         0x00170000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART2_UART2_DM",         0x00171000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART3_UART3_DM",         0x00172000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART4_UART4_DM",         0x00173000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART5_UART5_DM",         0x00174000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_QUP0",                   0x00175000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP1",                   0x00176000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP2",                   0x00177000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP3",                   0x00178000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP4",                   0x00179000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP5",                   0x0017a000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP",                        0x00180000, 0x00040000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM",                    0x00184000, 0x0002b000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_XPU2",               0x00182000, 0x00002000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_VMIDMT",             0x00180000, 0x00001000 },
  { "PERIPH_SS_BLSP2_BLSP_UART0_UART0_DM",         0x001af000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART1_UART1_DM",         0x001b0000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART2_UART2_DM",         0x001b1000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART3_UART3_DM",         0x001b2000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART4_UART4_DM",         0x001b3000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART5_UART5_DM",         0x001b4000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_QUP0",                   0x001b5000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP1",                   0x001b6000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP2",                   0x001b7000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP3",                   0x001b8000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP4",                   0x001b9000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP5",                   0x001ba000, 0x00000600 },
  { "PERIPH_SS_TSIF_TSIF_12SEG_WRAPPER",           0x001c0000, 0x00040000 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_BAM_LITE_TOP_AUTO_SCALE_V2_0", 0x001c0000, 0x00027000 },
  { "PERIPH_SS_TSIF_TSIF_BAM",                     0x001c4000, 0x00023000 },
  { "PERIPH_SS_TSIF_TSIF_0_TSIF_0_TSIF",           0x001e7000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_1_TSIF_1_TSIF",           0x001e8000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_TSPP",               0x001e9000, 0x00001000 },
  { "PERIPH_SS_USB1_HS_USB20S",                    0x00200000, 0x00200000 },
  { "PERIPH_SS_USB1_HS_DWC_USB3",                  0x00200000, 0x0000cd00 },
  { "PERIPH_SS_USB1_HS_USB30_QSCRATCH",            0x002f8800, 0x00000400 },
  { "PERIPH_SS_USB1_HS_USB30_QSRAM_REGS",          0x002fc000, 0x00000100 },
  { "PERIPH_SS_USB1_HS_USB30_DBM_REGFILE",         0x002f8000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_STM[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS[] =
{
  { "LPASS_LPASS_CC_REG",                          0x00000000, 0x0003f000 },
  { "LPASS_LPASS_TCSR",                            0x00080000, 0x0001f000 },
  { "LPASS_LPASS_QOS",                             0x000a0000, 0x00004000 },
  { "LPASS_QOS_QOS_GENERIC",                       0x000a0000, 0x00000100 },
  { "LPASS_QOS_QOS_DEBUG",                         0x000a0100, 0x00000200 },
  { "LPASS_QOS_QOS_DANGER",                        0x000a0300, 0x00000200 },
  { "LPASS_QOS_QOS_INTERRUPTS",                    0x000a1000, 0x00002000 },
  { "LPASS_QOS_QOS_THROTTLE_WRAPPER_1",            0x000a3000, 0x00001000 },
  { "LPASS_AHBE_TIME",                             0x000b7000, 0x00001000 },
  { "LPASS_Q6SS_MPU",                              0x000b8000, 0x00001600 },
  { "LPASS_LPASS_CSR",                             0x000c0000, 0x0001d000 },
  { "LPASS_AHBI_TIME",                             0x000f6000, 0x00001000 },
  { "LPASS_LPASS_SYNC_WRAPPER",                    0x000f7000, 0x00000ffd },
  { "LPASS_AVTIMER",                               0x000f7000, 0x00000100 },
  { "LPASS_LPASS_HDMITX",                          0x000f8000, 0x00007000 },
  { "LPASS_ATIMER",                                0x000ff000, 0x00000100 },
  { "LPASS_LPA_IF",                                0x00100000, 0x0001fffd },
  { "LPASS_LPASS_LPM",                             0x00120000, 0x00010000 },
  { "LPASS_TLB_PL",                                0x00130000, 0x00005000 },
  { "LPASS_RESAMPLER",                             0x00138000, 0x00008000 },
  { "LPASS_CORE_LPASS_CORE_QOS",                   0x00140000, 0x00003000 },
  { "LPASS_CORE_QOS_QOS_GENERIC",                  0x00140000, 0x00000100 },
  { "LPASS_CORE_QOS_QOS_CORE_DEBUG",               0x00140100, 0x00000200 },
  { "LPASS_CORE_QOS_QOS_INTERRUPTS",               0x00141000, 0x00002000 },
  { "LPASS_AUD_SBMASTER0_BASE",                    0x00180000, 0x00080000 },
  { "LPASS_AUD_SB_SLIMBUS_BAM_LITE",               0x00180000, 0x00036000 },
  { "LPASS_AUD_SB_BAM",                            0x00184000, 0x00032000 },
  { "LPASS_AUD_SLIMBUS",                           0x001c0000, 0x0002c000 },
  { "LPASS_QCA_SBMASTER0_BASE",                    0x00200000, 0x00080000 },
  { "LPASS_QCA_SB_SLIMBUS_BAM_LITE",               0x00200000, 0x0002a000 },
  { "LPASS_QCA_SB_BAM",                            0x00204000, 0x00026000 },
  { "LPASS_QCA_SLIMBUS",                           0x00240000, 0x0002c000 },
  { "LPASS_QDSP6V60SS_PUBLIC",                     0x00300000, 0x00080000 },
  { "LPASS_QDSP6V60SS_PUB",                        0x00300000, 0x00004040 },
  { "LPASS_QDSP6V60SS_PRIVATE",                    0x00380000, 0x00080000 },
  { "LPASS_QDSP6V60SS_CSR",                        0x00380000, 0x00008028 },
  { "LPASS_QDSP6V60SS_L2VIC",                      0x00390000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_QTMR_AC",               0x003a0000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F0_0",                     0x003a1000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F1_1",                     0x003a2000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F2_2",                     0x003a3000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_SAW3",                  0x003b0000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS_TCM[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_HMSS[] =
{
  { "APCS_HMSS_CFG",                               0x00010000, 0x00010000 },
  { "APCS_HMSS_CSR",                               0x00020000, 0x00010000 },
  { "APCS_HMSS_WDT",                               0x00030000, 0x00010000 },
  { "APCS_QTMR_AC",                                0x00040000, 0x00001000 },
  { "APCS_F0_QTMR_V1",                             0x00050000, 0x00001000 },
  { "APCS_F0_QTMR_V2",                             0x00060000, 0x00001000 },
  { "APCS_F1_QTMR_V1",                             0x00070000, 0x00001000 },
  { "APCS_F2_QTMR_V1",                             0x00080000, 0x00001000 },
  { "APCS_F3_QTMR_V1",                             0x00090000, 0x00001000 },
  { "APCS_F4_QTMR_V1",                             0x000a0000, 0x00001000 },
  { "APCS_F5_QTMR_V1",                             0x000b0000, 0x00001000 },
  { "APCS_F6_QTMR_V1",                             0x000c0000, 0x00001000 },
  { "APCS_PCTR",                                   0x00100000, 0x00010000 },
  { "APCS_NCTR_APC0_CPU0_NCTR_APC0_CPU0_HMSS_PCTR_CNTR", 0x00100000, 0x00000010 },
  { "APCS_NCTR_APC0_CPU1_NCTR_APC0_CPU1_HMSS_PCTR_CNTR", 0x00100010, 0x00000010 },
  { "APCS_NCTR_APC0_L2_NCTR_APC0_L2_HMSS_PCTR_CNTR", 0x00100020, 0x00000010 },
  { "APCS_NCTR_APC1_CPU0_NCTR_APC1_CPU0_HMSS_PCTR_CNTR", 0x00100030, 0x00000010 },
  { "APCS_NCTR_APC1_CPU1_NCTR_APC1_CPU1_HMSS_PCTR_CNTR", 0x00100040, 0x00000010 },
  { "APCS_NCTR_APC1_L2_NCTR_APC1_L2_HMSS_PCTR_CNTR", 0x00100050, 0x00000010 },
  { "APCS_DCTR_APC0_CPU0_DCTR_APC0_CPU0_HMSS_PCTR_CNTR", 0x00101000, 0x00000010 },
  { "APCS_DCTR_APC0_CPU1_DCTR_APC0_CPU1_HMSS_PCTR_CNTR", 0x00101010, 0x00000010 },
  { "APCS_DCTR_APC0_L2_DCTR_APC0_L2_HMSS_PCTR_CNTR", 0x00101020, 0x00000010 },
  { "APCS_DCTR_APC1_CPU0_DCTR_APC1_CPU0_HMSS_PCTR_CNTR", 0x00101030, 0x00000010 },
  { "APCS_DCTR_APC1_CPU1_DCTR_APC1_CPU1_HMSS_PCTR_CNTR", 0x00101040, 0x00000010 },
  { "APCS_DCTR_APC1_L2_DCTR_APC1_L2_HMSS_PCTR_CNTR", 0x00101050, 0x00000010 },
  { "APCS_HMSS_PCTR_AC",                           0x0010fc00, 0x00000100 },
  { "APCS_SMC",                                    0x00110000, 0x00010000 },
  { "APCS_SMC_CBF_M1_SMC_CBF_M1_HMSS_SMC_CNTR",    0x00110000, 0x00000010 },
  { "APCS_SMC_CBF_M2_SMC_CBF_M2_HMSS_SMC_CNTR",    0x00110010, 0x00000010 },
  { "APCS_SMC_CBF_M3_SMC_CBF_M3_HMSS_SMC_CNTR",    0x00110020, 0x00000010 },
  { "APCS_SMC_CBF_M4_SMC_CBF_M4_HMSS_SMC_CNTR",    0x00110030, 0x00000010 },
  { "APCS_SMC_L3_E1_SMC_L3_E1_HMSS_SMC_CNTR",      0x00110100, 0x00000010 },
  { "APCS_SMC_L3_E2_SMC_L3_E2_HMSS_SMC_CNTR",      0x00110110, 0x00000010 },
  { "APCS_SMC_L3_E3_SMC_L3_E3_HMSS_SMC_CNTR",      0x00110120, 0x00000010 },
  { "APCS_SMC_L3_E4_SMC_L3_E4_HMSS_SMC_CNTR",      0x00110130, 0x00000010 },
  { "APCS_SMC_L3_E5_SMC_L3_E5_HMSS_SMC_CNTR",      0x00110140, 0x00000010 },
  { "APCS_SMC_APC0_L2_D1_SMC_APC0_L2_D1_HMSS_SMC_CNTR", 0x00110200, 0x00000010 },
  { "APCS_SMC_APC0_L2_D2_SMC_APC0_L2_D2_HMSS_SMC_CNTR", 0x00110210, 0x00000010 },
  { "APCS_SMC_APC0_L2_D3_SMC_APC0_L2_D3_HMSS_SMC_CNTR", 0x00110220, 0x00000010 },
  { "APCS_SMC_APC0_L2_D4_SMC_APC0_L2_D4_HMSS_SMC_CNTR", 0x00110230, 0x00000010 },
  { "APCS_SMC_APC0_L2_D5_SMC_APC0_L2_D5_HMSS_SMC_CNTR", 0x00110240, 0x00000010 },
  { "APCS_SMC_APC0_CPU1_C1_SMC_APC0_CPU1_C1_HMSS_SMC_CNTR", 0x00110300, 0x00000010 },
  { "APCS_SMC_APC0_CPU1_C2_SMC_APC0_CPU1_C2_HMSS_SMC_CNTR", 0x00110310, 0x00000010 },
  { "APCS_SMC_APC0_CPU1_C3_SMC_APC0_CPU1_C3_HMSS_SMC_CNTR", 0x00110320, 0x00000010 },
  { "APCS_SMC_APC0_CPU1_C4_SMC_APC0_CPU1_C4_HMSS_SMC_CNTR", 0x00110330, 0x00000010 },
  { "APCS_SMC_APC0_CPU1_C5_SMC_APC0_CPU1_C5_HMSS_SMC_CNTR", 0x00110340, 0x00000010 },
  { "APCS_SMC_APC0_CPU0_C1_SMC_APC0_CPU0_C1_HMSS_SMC_CNTR", 0x00110400, 0x00000010 },
  { "APCS_SMC_APC0_CPU0_C2_SMC_APC0_CPU0_C2_HMSS_SMC_CNTR", 0x00110410, 0x00000010 },
  { "APCS_SMC_APC0_CPU0_C3_SMC_APC0_CPU0_C3_HMSS_SMC_CNTR", 0x00110420, 0x00000010 },
  { "APCS_SMC_APC0_CPU0_C4_SMC_APC0_CPU0_C4_HMSS_SMC_CNTR", 0x00110430, 0x00000010 },
  { "APCS_SMC_APC0_CPU0_C5_SMC_APC0_CPU0_C5_HMSS_SMC_CNTR", 0x00110440, 0x00000010 },
  { "APCS_SMC_APC1_L2_D1_SMC_APC1_L2_D1_HMSS_SMC_CNTR", 0x00110500, 0x00000010 },
  { "APCS_SMC_APC1_L2_D2_SMC_APC1_L2_D2_HMSS_SMC_CNTR", 0x00110510, 0x00000010 },
  { "APCS_SMC_APC1_L2_D3_SMC_APC1_L2_D3_HMSS_SMC_CNTR", 0x00110520, 0x00000010 },
  { "APCS_SMC_APC1_L2_D4_SMC_APC1_L2_D4_HMSS_SMC_CNTR", 0x00110530, 0x00000010 },
  { "APCS_SMC_APC1_L2_D5_SMC_APC1_L2_D5_HMSS_SMC_CNTR", 0x00110540, 0x00000010 },
  { "APCS_SMC_APC1_CPU1_C1_SMC_APC1_CPU1_C1_HMSS_SMC_CNTR", 0x00110600, 0x00000010 },
  { "APCS_SMC_APC1_CPU1_C2_SMC_APC1_CPU1_C2_HMSS_SMC_CNTR", 0x00110610, 0x00000010 },
  { "APCS_SMC_APC1_CPU1_C3_SMC_APC1_CPU1_C3_HMSS_SMC_CNTR", 0x00110620, 0x00000010 },
  { "APCS_SMC_APC1_CPU1_C4_SMC_APC1_CPU1_C4_HMSS_SMC_CNTR", 0x00110630, 0x00000010 },
  { "APCS_SMC_APC1_CPU1_C5_SMC_APC1_CPU1_C5_HMSS_SMC_CNTR", 0x00110640, 0x00000010 },
  { "APCS_SMC_APC1_CPU0_C1_SMC_APC1_CPU0_C1_HMSS_SMC_CNTR", 0x00110700, 0x00000010 },
  { "APCS_SMC_APC1_CPU0_C2_SMC_APC1_CPU0_C2_HMSS_SMC_CNTR", 0x00110710, 0x00000010 },
  { "APCS_SMC_APC1_CPU0_C3_SMC_APC1_CPU0_C3_HMSS_SMC_CNTR", 0x00110720, 0x00000010 },
  { "APCS_SMC_APC1_CPU0_C4_SMC_APC1_CPU0_C4_HMSS_SMC_CNTR", 0x00110730, 0x00000010 },
  { "APCS_SMC_APC1_CPU0_C5_SMC_APC1_CPU0_C5_HMSS_SMC_CNTR", 0x00110740, 0x00000010 },
  { "APCS_HMSS_SMC_CTL",                           0x0011fc00, 0x00000100 },
  { "APCS_M4M_M4M_DLM_LLM_REGS",                   0x00140000, 0x00040000 },
  { "APCS_M4M_LLM_PRIVATE",                        0x00140000, 0x00001000 },
  { "APCS_M4M_LLM_PUBLIC",                         0x00150000, 0x00001000 },
  { "APCS_M4M_DLM_PRIVATE",                        0x00160000, 0x00001000 },
  { "APCS_M4M_DLM_PUBLIC",                         0x00170000, 0x00001000 },
  { "APCS_HMSS_PM_APCC_TOP",                       0x00180000, 0x00080000 },
  { "APCS_APC0_CPU0_APC0_CPU0_SAW3_HMSS_CPU_WRAPPER", 0x00180000, 0x00001000 },
  { "APCS_APC0_CPU0_SAW3",                         0x00180000, 0x00001000 },
  { "APCS_APC0_CPU0_APC0_CPU0_HMSS_PM_APCC_CPU",   0x00181000, 0x00001000 },
  { "APCS_APC0_CPU1_APC0_CPU1_SAW3_HMSS_CPU_WRAPPER", 0x00190000, 0x00001000 },
  { "APCS_APC0_CPU1_SAW3",                         0x00190000, 0x00001000 },
  { "APCS_APC0_CPU1_APC0_CPU1_HMSS_PM_APCC_CPU",   0x00191000, 0x00001000 },
  { "APCS_APC0_L2_APC0_L2_SAW3_HMSS_L2_WRAPPER",   0x001a0000, 0x00001000 },
  { "APCS_APC0_L2_SAW3",                           0x001a0000, 0x00001000 },
  { "APCS_APC0_L2_APC0_L2_HMSS_PM_APCC_L2",        0x001a1000, 0x00001000 },
  { "APCS_APC0_APC0_HMSS_PM_APCC_APC",             0x001a2000, 0x00001000 },
  { "APCS_APC1_CPU0_APC1_CPU0_SAW3_HMSS_CPU_WRAPPER", 0x001b0000, 0x00001000 },
  { "APCS_APC1_CPU0_SAW3",                         0x001b0000, 0x00001000 },
  { "APCS_APC1_CPU0_APC1_CPU0_HMSS_PM_APCC_CPU",   0x001b1000, 0x00001000 },
  { "APCS_APC1_CPU1_APC1_CPU1_SAW3_HMSS_CPU_WRAPPER", 0x001c0000, 0x00001000 },
  { "APCS_APC1_CPU1_SAW3",                         0x001c0000, 0x00001000 },
  { "APCS_APC1_CPU1_APC1_CPU1_HMSS_PM_APCC_CPU",   0x001c1000, 0x00001000 },
  { "APCS_APC1_L2_APC1_L2_SAW3_HMSS_L2_WRAPPER",   0x001d0000, 0x00001000 },
  { "APCS_APC1_L2_SAW3",                           0x001d0000, 0x00001000 },
  { "APCS_APC1_L2_APC1_L2_HMSS_PM_APCC_L2",        0x001d1000, 0x00001000 },
  { "APCS_APC1_APC1_HMSS_PM_APCC_APC",             0x001d2000, 0x00001000 },
  { "APCS_APCC_APCC_HMSS_PM_APCC_GLB",             0x001e0000, 0x00001000 },
  { "APCS_APCC_APCC_HMSS_CPR3",                    0x001e8000, 0x00004000 },
  { "APCS_HMSS_PM_L3_TOP",                         0x00200000, 0x00010000 },
  { "APCS_L3_L3_SAW3_HMSS_L3_WRAPPER",             0x00200000, 0x00001000 },
  { "APCS_L3_SAW3",                                0x00200000, 0x00001000 },
  { "APCS_L3_L3_HMSS_L3_CSR",                      0x00201000, 0x00001000 },
  { "APCS_HMSS_PM_CBF_TOP",                        0x00210000, 0x00070000 },
  { "APCS_CBF_CBF_SAW3_HMSS_CBF_WRAPPER",          0x00210000, 0x00001000 },
  { "APCS_CBF_SAW3",                               0x00210000, 0x00001000 },
  { "APCS_CBF_CBF_HMSS_CBF_CSR",                   0x00211000, 0x00001000 },
  { "APCS_CBF_CBF_HMSS_CBF_PLL",                   0x00220000, 0x00010000 },
  { "APCS_CBF_CBF_M4M_REGS",                       0x00240000, 0x00040000 },
  { "APCS_QGICDR_QGICDR_GICD_GITS",                0x003c0000, 0x00040000 },
  { "APCS_QGICDR_QGICDR_GICD",                     0x003c0000, 0x00010000 },
  { "APCS_QGICDR_QGICDR_GICM",                     0x003d0000, 0x00010000 },
  { "APCS_QGICDR_QGICDR_GITS",                     0x003e0000, 0x00020000 },
  { "APCS_QGICDR_QGICDR_GICR_PAGE",                0x00400000, 0x00100000 },
  { "APCS_QGICDR_R0_R0_QGICDR_GICR",               0x00400000, 0x00040000 },
  { "APCS_QGICDR_R1_R1_QGICDR_GICR",               0x00440000, 0x00040000 },
  { "APCS_QGICDR_R2_R2_QGICDR_GICR",               0x00480000, 0x00040000 },
  { "APCS_QGICDR_R3_R3_QGICDR_GICR",               0x004c0000, 0x00040000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_0_PCIE20_WRAPPER_AXI[] =
{
  { "PCIE_0_PCIE20_DBI",                           0x00000000, 0x00000f1d },
  { "PCIE_0_PCIE20_ELBI",                          0x00000f20, 0x000000a8 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_1_PCIE20_WRAPPER_AXI[] =
{
  { "PCIE_1_PCIE20_DBI",                           0x00000000, 0x00000f1d },
  { "PCIE_1_PCIE20_ELBI",                          0x00000f20, 0x000000a8 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PCIE_2_PCIE20_WRAPPER_AXI[] =
{
  { "PCIE_2_PCIE20_DBI",                           0x00000000, 0x00000f1d },
  { "PCIE_2_PCIE20_ELBI",                          0x00000f20, 0x000000a8 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MODEM[] =
{
  { "MODEM_VUIC",                                  0x00000000, 0x00200000 },
  { "VUSS1_VUSS1_TOP",                             0x00020000, 0x00020000 },
  { "VUSS1_VUSS_CSR",                              0x00020000, 0x00000800 },
  { "VUSS1_PMEMSS_CSR",                            0x00020800, 0x00000800 },
  { "VUSS1_VUSS_DMEM_X4X8",                        0x00021000, 0x00004000 },
  { "VUSS2_VUSS2_TOP",                             0x00040000, 0x00020000 },
  { "VUSS2_VUSS_CSR",                              0x00040000, 0x00000800 },
  { "VUSS2_VUSS_DMEM_X4X8",                        0x00041000, 0x00004000 },
  { "VUSS3_VUSS3_TOP",                             0x00060000, 0x00020000 },
  { "VUSS3_VUSS_CSR",                              0x00060000, 0x00000800 },
  { "VUSS3_VUSS_DMEM_X4X8",                        0x00061000, 0x00004000 },
  { "VUSS4_VUSS4_TOP",                             0x00080000, 0x00020000 },
  { "VUSS4_VUSS_CSR",                              0x00080000, 0x00000800 },
  { "VUSS4_PMEMSS_CSR",                            0x00080800, 0x00000800 },
  { "VUSS4_VUSS_DMEM_X4X8",                        0x00081000, 0x00004000 },
  { "VUSS5_VUSS5_TOP",                             0x000a0000, 0x00020000 },
  { "VUSS5_VUSS_CSR",                              0x000a0000, 0x00000800 },
  { "VUSS5_VUSS_DMEM_X4X8",                        0x000a1000, 0x00004000 },
  { "VUSS6_VUSS6_TOP",                             0x000c0000, 0x00020000 },
  { "VUSS6_VUSS_CSR",                              0x000c0000, 0x00000800 },
  { "VUSS6_PMEMSS_CSR",                            0x000c0800, 0x00000800 },
  { "VUSS6_VUSS_DMEM_X4X8",                        0x000c1000, 0x00004000 },
  { "DEMAP_DEMAP_TOP",                             0x000e0000, 0x00020000 },
  { "DEMAP_VUSS_CSR",                              0x000e0000, 0x00000800 },
  { "DEMAP_VUSS_DMEM_X4X8",                        0x000e1000, 0x00004000 },
  { "CCHP_CCHP_TOP",                               0x00100000, 0x00020000 },
  { "CCHP_VUSS_CSR",                               0x00100000, 0x00000800 },
  { "CCHP_PMEMSS_CSR",                             0x00100800, 0x00000800 },
  { "CCHP_VUSS_DMEM_CCHP",                         0x00101000, 0x00008000 },
  { "ENC_ENC_TOP",                                 0x00120000, 0x00020000 },
  { "ENC_VUSS_CSR",                                0x00120000, 0x00000800 },
  { "ENC_VUSS_DMEM_ENC",                           0x00121000, 0x0000d800 },
  { "MPMU_TOP",                                    0x00300000, 0x00100000 },
  { "MODEM_MPMU_CSR",                              0x00300400, 0x00000400 },
  { "MNOC_AXI_BR_CSR",                             0x00400000, 0x00100000 },
  { "DBSS_TOP",                                    0x00600000, 0x00100000 },
  { "CCS",                                         0x00600000, 0x00020000 },
  { "CIPHER_TOP",                                  0x0061fe00, 0x00000200 },
  { "CIPHER",                                      0x0061fe00, 0x00000100 },
  { "ENCRYPT",                                     0x0061ff00, 0x000000fd },
  { "CONTROL",                                     0x0061fc00, 0x00000200 },
  { "CPMEM",                                       0x0061f800, 0x00000400 },
  { "CCS_PDMEM",                                   0x00600000, 0x00010000 },
  { "MTC_TOP",                                     0x00620000, 0x00010000 },
  { "MCDMA",                                       0x00620400, 0x00000400 },
  { "A2",                                          0x00620800, 0x00000400 },
  { "DBG",                                         0x00620c00, 0x00000400 },
  { "MTC_BRDG",                                    0x00621000, 0x00000400 },
  { "A2_MEM",                                      0x00622000, 0x00002000 },
  { "MCDMA_TS_TRIF",                               0x00624000, 0x00000c00 },
  { "TDEC",                                        0x00640000, 0x000000a8 },
  { "TD_CFG_TRIF",                                 0x00641000, 0x00000a00 },
  { "TD_TRIF",                                     0x00642000, 0x00000900 },
  { "TDECIB_MEM",                                  0x00643000, 0x00001000 },
  { "DEMBACK_COMMON",                              0x00660000, 0x00000010 },
  { "DEMBACK_BRDG",                                0x00661000, 0x00000100 },
  { "LTE_DEMBACK",                                 0x00662000, 0x00000050 },
  { "LTE_REENC",                                   0x00663000, 0x00000008 },
  { "UMTS_DEMBACK",                                0x00664000, 0x00000300 },
  { "TDS_DEMBACK",                                 0x00665000, 0x00000100 },
  { "CDMA_DEINT",                                  0x00666000, 0x00000100 },
  { "CDMA_WIDGET",                                 0x00666100, 0x00000100 },
  { "HDR_DEINT",                                   0x00667000, 0x00000100 },
  { "SVD_TBVD",                                    0x00668000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF",                        0x00669000, 0x00000300 },
  { "LTE_DEMBACK_CCH_TRIF",                        0x0066a000, 0x00000c00 },
  { "LTE_REENC_TS_TRIF",                           0x0066b000, 0x00000400 },
  { "W_DBACK_HS_TRIF",                             0x0066c000, 0x00000400 },
  { "W_DBACK_NONHS_TRIF",                          0x0066d000, 0x00000400 },
  { "T_DBACK_TRIF",                                0x0066e000, 0x00000500 },
  { "HDR_DEINT_TS_TRIF",                           0x0066f000, 0x00000300 },
  { "TBVD_CCH_TRIF",                               0x00670000, 0x00000400 },
  { "DB_BUF",                                      0x00674000, 0x00004000 },
  { "TDEC_DB1",                                    0x00680000, 0x000000a8 },
  { "TD_CFG_TRIF_DB1",                             0x00681000, 0x00000a00 },
  { "TD_TRIF_DB1",                                 0x00682000, 0x00000900 },
  { "TDECIB_MEM_DB1",                              0x00683000, 0x00001000 },
  { "DEMBACK_COMMON_DB1",                          0x006a0000, 0x00000010 },
  { "DEMBACK_BRDG_DB1",                            0x006a1000, 0x00000100 },
  { "LTE_DEMBACK_DB1",                             0x006a2000, 0x00000050 },
  { "LTE_REENC_DB1",                               0x006a3000, 0x00000008 },
  { "UMTS_DEMBACK_DB1",                            0x006a4000, 0x00000300 },
  { "TDS_DEMBACK_DB1",                             0x006a5000, 0x00000100 },
  { "CDMA_DEINT_DB1",                              0x006a6000, 0x00000100 },
  { "CDMA_WIDGET_DB1",                             0x006a6100, 0x00000100 },
  { "HDR_DEINT_DB1",                               0x006a7000, 0x00000100 },
  { "SVD_TBVD_DB1",                                0x006a8000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF_DB1",                    0x006a9000, 0x00000300 },
  { "LTE_DEMBACK_CCH_TRIF_DB1",                    0x006aa000, 0x00000c00 },
  { "LTE_REENC_TS_TRIF_DB1",                       0x006ab000, 0x00000400 },
  { "W_DBACK_HS_TRIF_DB1",                         0x006ac000, 0x00000400 },
  { "W_DBACK_NONHS_TRIF_DB1",                      0x006ad000, 0x00000400 },
  { "T_DBACK_TRIF_DB1",                            0x006ae000, 0x00000500 },
  { "HDR_DEINT_TS_TRIF_DB1",                       0x006af000, 0x00000300 },
  { "TBVD_CCH_TRIF_DB1",                           0x006b0000, 0x00000400 },
  { "DB_BUF_DB1",                                  0x006b4000, 0x00004000 },
  { "DTR",                                         0x00700000, 0x00100000 },
  { "MODEM_DTR_ANALOG",                            0x00700000, 0x00008000 },
  { "RXFE_TOP_CFG",                                0x00708000, 0x00008000 },
  { "RXFE_WB_WB0",                                 0x00710000, 0x00008000 },
  { "RXFE_WB_WB1",                                 0x00718000, 0x00008000 },
  { "RXFE_WB_WB2",                                 0x00720000, 0x00008000 },
  { "RXFE_WB_WB3",                                 0x00728000, 0x00008000 },
  { "RXFE_WB_WB4",                                 0x00730000, 0x00008000 },
  { "RXFE_WB_WB5",                                 0x00738000, 0x00008000 },
  { "RXFE_WB_WB6",                                 0x00740000, 0x00008000 },
  { "RXFE_NB_NB0",                                 0x00748000, 0x00008000 },
  { "RXFE_NB_NB1",                                 0x00750000, 0x00008000 },
  { "RXFE_NB_NB2",                                 0x00758000, 0x00008000 },
  { "RXFE_NB_NB3",                                 0x00760000, 0x00008000 },
  { "RXFE_NB_NB4",                                 0x00768000, 0x00008000 },
  { "RXFE_NB_NB5",                                 0x00770000, 0x00008000 },
  { "RXFE_NB_NB6",                                 0x00778000, 0x00008000 },
  { "RXFE_NB_NB7",                                 0x00780000, 0x00008000 },
  { "RXFE_NB_NB8",                                 0x00788000, 0x00008000 },
  { "RXFE_NB_NB9",                                 0x00790000, 0x00008000 },
  { "RXFE_NB_NB10",                                0x00798000, 0x00008000 },
  { "DTR_EVT",                                     0x007a0000, 0x00008000 },
  { "TXFE_SRIF_CHAIN0",                            0x007b8000, 0x00008000 },
  { "TXFE_SRIF_CHAIN1",                            0x007c0000, 0x00008000 },
  { "DTR_BRDG",                                    0x007d0000, 0x00008000 },
  { "MODEM_DTR_DAC_CALIB_0",                       0x007d8000, 0x00001000 },
  { "DAC_REGARRAY_0",                              0x007d9000, 0x00001000 },
  { "MODEM_DTR_DAC_CALIB_1",                       0x007da000, 0x00001000 },
  { "DAC_REGARRAY_1",                              0x007db000, 0x00001000 },
  { "MNOC_AXI_BR_MEM",                             0x00800000, 0x00800000 },
  { NULL, 0, 0 }
};

HWIOPhysRegionType HWIOBaseMap[] =
{
  {
    "CNOC_0_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00020000,
    0x00001000,
    (DALSYSMemAddr)CNOC_0_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_0_BUS_TIMEOUT
  },
  {
    "CNOC_1_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00021000,
    0x00001000,
    (DALSYSMemAddr)CNOC_1_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_1_BUS_TIMEOUT
  },
  {
    "CNOC_2_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00022000,
    0x00001000,
    (DALSYSMemAddr)CNOC_2_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_2_BUS_TIMEOUT
  },
  {
    "CNOC_3_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00023000,
    0x00001000,
    (DALSYSMemAddr)CNOC_3_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_3_BUS_TIMEOUT
  },
  {
    "CNOC_4_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00024000,
    0x00001000,
    (DALSYSMemAddr)CNOC_4_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_4_BUS_TIMEOUT
  },
  {
    "CNOC_5_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00025000,
    0x00001000,
    (DALSYSMemAddr)CNOC_5_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_5_BUS_TIMEOUT
  },
  {
    "CNOC_6_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00026000,
    0x00001000,
    (DALSYSMemAddr)CNOC_6_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_6_BUS_TIMEOUT
  },
  {
    "CNOC_7_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00027000,
    0x00001000,
    (DALSYSMemAddr)CNOC_7_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_7_BUS_TIMEOUT
  },
  {
    "CNOC_8_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00028000,
    0x00001000,
    (DALSYSMemAddr)CNOC_8_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_8_BUS_TIMEOUT
  },
  {
    "CNOC_9_BUS_TIMEOUT",
    (DALSYSMemAddr)0x00029000,
    0x00001000,
    (DALSYSMemAddr)CNOC_9_BUS_TIMEOUT_BASE,
    HWIOModules_CNOC_9_BUS_TIMEOUT
  },
  {
    "XPU_CFG_CPR_APU1132_2",
    (DALSYSMemAddr)0x0002c000,
    0x00000300,
    (DALSYSMemAddr)XPU_CFG_CPR_APU1132_2_BASE,
    HWIOModules_XPU_CFG_CPR_APU1132_2
  },
  {
    "XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB",
    (DALSYSMemAddr)0x0002d000,
    0x00000400,
    (DALSYSMemAddr)XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB_BASE,
    HWIOModules_XPU_CFG_SNOC_CFG_MPU1032_4_M16L12_AHB
  },
  {
    "XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB",
    (DALSYSMemAddr)0x0002e000,
    0x00000400,
    (DALSYSMemAddr)XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB_BASE,
    HWIOModules_XPU_CFG_ANOC0_CFG_MPU1032_4_M16L12_AHB
  },
  {
    "XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB",
    (DALSYSMemAddr)0x0002f000,
    0x00000400,
    (DALSYSMemAddr)XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB_BASE,
    HWIOModules_XPU_CFG_ANOC1_CFG_MPU1032_4_M16L12_AHB
  },
  {
    "XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB",
    (DALSYSMemAddr)0x00030000,
    0x00000400,
    (DALSYSMemAddr)XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB_BASE,
    HWIOModules_XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB
  },
  {
    "XPU_CFG_QDSS_MPU1132_3_M23L12_AHB",
    (DALSYSMemAddr)0x00031000,
    0x00000380,
    (DALSYSMemAddr)XPU_CFG_QDSS_MPU1132_3_M23L12_AHB_BASE,
    HWIOModules_XPU_CFG_QDSS_MPU1132_3_M23L12_AHB
  },
  {
    "XPU_CFG_SSC_MPU1032_10_M22L11_AHB",
    (DALSYSMemAddr)0x00032000,
    0x00000700,
    (DALSYSMemAddr)XPU_CFG_SSC_MPU1032_10_M22L11_AHB_BASE,
    HWIOModules_XPU_CFG_SSC_MPU1032_10_M22L11_AHB
  },
  {
    "PCIE20_AHB2PHY",
    (DALSYSMemAddr)0x00034000,
    0x00004000,
    (DALSYSMemAddr)PCIE20_AHB2PHY_BASE,
    HWIOModules_PCIE20_AHB2PHY
  },
  {
    "RAMBLUR_PIMEM",
    (DALSYSMemAddr)0x00038000,
    0x00008000,
    (DALSYSMemAddr)RAMBLUR_PIMEM_BASE,
    HWIOModules_RAMBLUR_PIMEM
  },
  {
    "OCIMEM_WRAPPER_CSR",
    (DALSYSMemAddr)0x00040000,
    0x00004000,
    (DALSYSMemAddr)OCIMEM_WRAPPER_CSR_BASE,
    HWIOModules_OCIMEM_WRAPPER_CSR
  },
  {
    "SPDM_WRAPPER_TOP",
    (DALSYSMemAddr)0x00048000,
    0x00008000,
    (DALSYSMemAddr)SPDM_WRAPPER_TOP_BASE,
    HWIOModules_SPDM_WRAPPER_TOP
  },
  {
    "CPR3",
    (DALSYSMemAddr)0x00050000,
    0x00004000,
    (DALSYSMemAddr)CPR3_BASE,
    HWIOModules_CPR3
  },
  {
    "MX_CPR3",
    (DALSYSMemAddr)0x00054000,
    0x00004000,
    (DALSYSMemAddr)MX_CPR3_BASE,
    HWIOModules_MX_CPR3
  },
  {
    "ISTARI_SKL_WRAPPER",
    (DALSYSMemAddr)0x00058000,
    0x00004000,
    (DALSYSMemAddr)ISTARI_SKL_WRAPPER_BASE,
    HWIOModules_ISTARI_SKL_WRAPPER
  },
  {
    "MSS_MPU_MPU1032A_16_M35L12_AXI_36",
    (DALSYSMemAddr)0x0005c000,
    0x00000a00,
    (DALSYSMemAddr)MSS_MPU_MPU1032A_16_M35L12_AXI_36_BASE,
    HWIOModules_MSS_MPU_MPU1032A_16_M35L12_AXI_36
  },
  {
    "MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36",
    (DALSYSMemAddr)0x0005d000,
    0x00000a00,
    (DALSYSMemAddr)MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36_BASE,
    HWIOModules_MSS_Q6_MPU_MPU1032A_16_M35L12_AXI_36
  },
  {
    "ANOC1_MPU_MPU1032A_16_M35L12_AXI_36",
    (DALSYSMemAddr)0x0005e000,
    0x00000a00,
    (DALSYSMemAddr)ANOC1_MPU_MPU1032A_16_M35L12_AXI_36_BASE,
    HWIOModules_ANOC1_MPU_MPU1032A_16_M35L12_AXI_36
  },
  {
    "ANOC2_MPU_MPU1032A_16_M35L12_AXI_36",
    (DALSYSMemAddr)0x0005f000,
    0x00000a00,
    (DALSYSMemAddr)ANOC2_MPU_MPU1032A_16_M35L12_AXI_36_BASE,
    HWIOModules_ANOC2_MPU_MPU1032A_16_M35L12_AXI_36
  },
  {
    "RPM_MSG_RAM",
    (DALSYSMemAddr)0x00068000,
    0x00008000,
    (DALSYSMemAddr)RPM_MSG_RAM_BASE,
    HWIOModules_RPM_MSG_RAM
  },
  {
    "SECURITY_CONTROL",
    (DALSYSMemAddr)0x00070000,
    0x00010000,
    (DALSYSMemAddr)SECURITY_CONTROL_BASE,
    HWIOModules_SECURITY_CONTROL
  },
  {
    "PRNG_CFG_PRNG_TOP",
    (DALSYSMemAddr)0x00080000,
    0x00010000,
    (DALSYSMemAddr)PRNG_CFG_PRNG_TOP_BASE,
    HWIOModules_PRNG_CFG_PRNG_TOP
  },
  {
    "BOOT_ROM",
    (DALSYSMemAddr)0x00100000,
    0x00100000,
    (DALSYSMemAddr)BOOT_ROM_BASE,
    HWIOModules_BOOT_ROM
  },
  {
    "RPM",
    (DALSYSMemAddr)0x00200000,
    0x00100000,
    (DALSYSMemAddr)RPM_BASE,
    HWIOModules_RPM
  },
  {
    "RPM_DATA_RAM_START_ADDRESS",
    (DALSYSMemAddr)0x00290000,
    0x100000000,
    (DALSYSMemAddr)RPM_DATA_RAM_START_ADDRESS_BASE,
    HWIOModules_RPM_DATA_RAM_START_ADDRESS
  },
  {
    "CLK_CTL",
    (DALSYSMemAddr)0x00300000,
    0x000a0000,
    (DALSYSMemAddr)CLK_CTL_BASE,
    HWIOModules_CLK_CTL
  },
  {
    "BIMC",
    (DALSYSMemAddr)0x00400000,
    0x00080000,
    (DALSYSMemAddr)BIMC_BASE,
    HWIOModules_BIMC
  },
  {
    "DDR_SS",
    (DALSYSMemAddr)0x00480000,
    0x00020000,
    (DALSYSMemAddr)DDR_SS_BASE,
    HWIOModules_DDR_SS
  },
  {
    "MPM2_MPM",
    (DALSYSMemAddr)0x004a0000,
    0x00010000,
    (DALSYSMemAddr)MPM2_MPM_BASE,
    HWIOModules_MPM2_MPM
  },
  {
    "DCC_WRAPPER",
    (DALSYSMemAddr)0x004b0000,
    0x00004000,
    (DALSYSMemAddr)DCC_WRAPPER_BASE,
    HWIOModules_DCC_WRAPPER
  },
  {
    "CONFIG_NOC",
    (DALSYSMemAddr)0x00500000,
    0x00000080,
    (DALSYSMemAddr)CONFIG_NOC_BASE,
    HWIOModules_CONFIG_NOC
  },
  {
    "SYSTEM_NOC",
    (DALSYSMemAddr)0x00520000,
    0x0000a100,
    (DALSYSMemAddr)SYSTEM_NOC_BASE,
    HWIOModules_SYSTEM_NOC
  },
  {
    "A0_NOC_AGGRE0_NOC",
    (DALSYSMemAddr)0x00540000,
    0x00005100,
    (DALSYSMemAddr)A0_NOC_AGGRE0_NOC_BASE,
    HWIOModules_A0_NOC_AGGRE0_NOC
  },
  {
    "A1_NOC_AGGRE1_NOC",
    (DALSYSMemAddr)0x00560000,
    0x00003100,
    (DALSYSMemAddr)A1_NOC_AGGRE1_NOC_BASE,
    HWIOModules_A1_NOC_AGGRE1_NOC
  },
  {
    "A2_NOC_AGGRE2_NOC",
    (DALSYSMemAddr)0x00580000,
    0x00008100,
    (DALSYSMemAddr)A2_NOC_AGGRE2_NOC_BASE,
    HWIOModules_A2_NOC_AGGRE2_NOC
  },
  {
    "MMSS_NOC",
    (DALSYSMemAddr)0x005a0000,
    0x0000b080,
    (DALSYSMemAddr)MMSS_NOC_BASE,
    HWIOModules_MMSS_NOC
  },
  {
    "PERIPH_NOC",
    (DALSYSMemAddr)0x005c0000,
    0x00002480,
    (DALSYSMemAddr)PERIPH_NOC_BASE,
    HWIOModules_PERIPH_NOC
  },
  {
    "PCIE_0_PCIE20_WRAPPER_AHB",
    (DALSYSMemAddr)0x00600000,
    0x00008000,
    (DALSYSMemAddr)PCIE_0_PCIE20_WRAPPER_AHB_BASE,
    HWIOModules_PCIE_0_PCIE20_WRAPPER_AHB
  },
  {
    "PCIE_1_PCIE20_WRAPPER_AHB",
    (DALSYSMemAddr)0x00608000,
    0x00008000,
    (DALSYSMemAddr)PCIE_1_PCIE20_WRAPPER_AHB_BASE,
    HWIOModules_PCIE_1_PCIE20_WRAPPER_AHB
  },
  {
    "PCIE_2_PCIE20_WRAPPER_AHB",
    (DALSYSMemAddr)0x00610000,
    0x00008000,
    (DALSYSMemAddr)PCIE_2_PCIE20_WRAPPER_AHB_BASE,
    HWIOModules_PCIE_2_PCIE20_WRAPPER_AHB
  },
  {
    "UFS_UFS_REGS",
    (DALSYSMemAddr)0x00620000,
    0x00020000,
    (DALSYSMemAddr)UFS_UFS_REGS_BASE,
    HWIOModules_UFS_UFS_REGS
  },
  {
    "CRYPTO0_CRYPTO_TOP",
    (DALSYSMemAddr)0x00640000,
    0x00040000,
    (DALSYSMemAddr)CRYPTO0_CRYPTO_TOP_BASE,
    HWIOModules_CRYPTO0_CRYPTO_TOP
  },
  {
    "IPA_0_IPA_WRAPPER",
    (DALSYSMemAddr)0x00680000,
    0x00080000,
    (DALSYSMemAddr)IPA_0_IPA_WRAPPER_BASE,
    HWIOModules_IPA_0_IPA_WRAPPER
  },
  {
    "CORE_TOP_CSR",
    (DALSYSMemAddr)0x00700000,
    0x00100000,
    (DALSYSMemAddr)CORE_TOP_CSR_BASE,
    HWIOModules_CORE_TOP_CSR
  },
  {
    "MMSS",
    (DALSYSMemAddr)0x00800000,
    0x00800000,
    (DALSYSMemAddr)MMSS_BASE,
    HWIOModules_MMSS
  },
  {
    "TLMM",
    (DALSYSMemAddr)0x01000000,
    0x00310000,
    (DALSYSMemAddr)TLMM_BASE,
    HWIOModules_TLMM
  },
  {
    "LPASS_Q6_SMMU_QSMMUV2_WRAP_LPASS_Q6_ISTARI",
    (DALSYSMemAddr)0x01600000,
    0x00020000,
    (DALSYSMemAddr)LPASS_Q6_SMMU_QSMMUV2_WRAP_LPASS_Q6_ISTARI_BASE,
    HWIOModules_LPASS_Q6_SMMU_QSMMUV2_WRAP_LPASS_Q6_ISTARI
  },
  {
    "LPASS_CORE_SMMU_QSMMUV2_WRAP_LPASS_CORE_ISTARI",
    (DALSYSMemAddr)0x01620000,
    0x00020000,
    (DALSYSMemAddr)LPASS_CORE_SMMU_QSMMUV2_WRAP_LPASS_CORE_ISTARI_BASE,
    HWIOModules_LPASS_CORE_SMMU_QSMMUV2_WRAP_LPASS_CORE_ISTARI
  },
  {
    "ANOC0_SMMU_QSMMUV2_WRAP_PCIE_ISTARI",
    (DALSYSMemAddr)0x01640000,
    0x00020000,
    (DALSYSMemAddr)ANOC0_SMMU_QSMMUV2_WRAP_PCIE_ISTARI_BASE,
    HWIOModules_ANOC0_SMMU_QSMMUV2_WRAP_PCIE_ISTARI
  },
  {
    "ANOC1_SMMU_QSMMUV2_WRAP_ANOC1_ISTARI",
    (DALSYSMemAddr)0x01660000,
    0x00020000,
    (DALSYSMemAddr)ANOC1_SMMU_QSMMUV2_WRAP_ANOC1_ISTARI_BASE,
    HWIOModules_ANOC1_SMMU_QSMMUV2_WRAP_ANOC1_ISTARI
  },
  {
    "ANOC2_SMMU_QSMMUV2_WRAP_ANOC2_ISTARI",
    (DALSYSMemAddr)0x01680000,
    0x00020000,
    (DALSYSMemAddr)ANOC2_SMMU_QSMMUV2_WRAP_ANOC2_ISTARI_BASE,
    HWIOModules_ANOC2_SMMU_QSMMUV2_WRAP_ANOC2_ISTARI
  },
  {
    "MDM_Q6_SMMU_QSMMUV2_WRAP_MDM_Q6_ISTARI",
    (DALSYSMemAddr)0x016a0000,
    0x00020000,
    (DALSYSMemAddr)MDM_Q6_SMMU_QSMMUV2_WRAP_MDM_Q6_ISTARI_BASE,
    HWIOModules_MDM_Q6_SMMU_QSMMUV2_WRAP_MDM_Q6_ISTARI
  },
  {
    "SSC",
    (DALSYSMemAddr)0x01800000,
    0x00800000,
    (DALSYSMemAddr)SSC_BASE,
    HWIOModules_SSC
  },
  {
    "MODEM_TOP",
    (DALSYSMemAddr)0x02000000,
    0x01000000,
    (DALSYSMemAddr)MODEM_TOP_BASE,
    HWIOModules_MODEM_TOP
  },
  {
    "QDSS_QDSS_APB",
    (DALSYSMemAddr)0x03000000,
    0x00800000,
    (DALSYSMemAddr)QDSS_QDSS_APB_BASE,
    HWIOModules_QDSS_QDSS_APB
  },
  {
    "HMSS_DBG_HMSS_DEBUG",
    (DALSYSMemAddr)0x03800000,
    0x00400000,
    (DALSYSMemAddr)HMSS_DBG_HMSS_DEBUG_BASE,
    HWIOModules_HMSS_DBG_HMSS_DEBUG
  },
  {
    "PMIC_ARB",
    (DALSYSMemAddr)0x04000000,
    0x01c00000,
    (DALSYSMemAddr)PMIC_ARB_BASE,
    HWIOModules_PMIC_ARB
  },
  {
    "L3_TCM",
    (DALSYSMemAddr)0x06000000,
    0x100000000,
    (DALSYSMemAddr)L3_TCM_BASE,
    HWIOModules_L3_TCM
  },
  {
    "HMSS_QLL",
    (DALSYSMemAddr)0x06400000,
    0x00200000,
    (DALSYSMemAddr)HMSS_QLL_BASE,
    HWIOModules_HMSS_QLL
  },
  {
    "SNOC_1_BUS_TIMEOUT",
    (DALSYSMemAddr)0x06601000,
    0x00001000,
    (DALSYSMemAddr)SNOC_1_BUS_TIMEOUT_BASE,
    HWIOModules_SNOC_1_BUS_TIMEOUT
  },
  {
    "SNOC_2_BUS_TIMEOUT",
    (DALSYSMemAddr)0x06602000,
    0x00001000,
    (DALSYSMemAddr)SNOC_2_BUS_TIMEOUT_BASE,
    HWIOModules_SNOC_2_BUS_TIMEOUT
  },
  {
    "USB30_PRIM",
    (DALSYSMemAddr)0x06a00000,
    0x00200000,
    (DALSYSMemAddr)USB30_PRIM_BASE,
    HWIOModules_USB30_PRIM
  },
  {
    "PERIPH_SS",
    (DALSYSMemAddr)0x07400000,
    0x00400000,
    (DALSYSMemAddr)PERIPH_SS_BASE,
    HWIOModules_PERIPH_SS
  },
  {
    "QDSS_STM",
    (DALSYSMemAddr)0x08000000,
    0x100000000,
    (DALSYSMemAddr)QDSS_STM_BASE,
    HWIOModules_QDSS_STM
  },
  {
    "LPASS",
    (DALSYSMemAddr)0x09000000,
    0x00800000,
    (DALSYSMemAddr)LPASS_BASE,
    HWIOModules_LPASS
  },
  {
    "LPASS_TCM",
    (DALSYSMemAddr)0x09400000,
    0x100000000,
    (DALSYSMemAddr)LPASS_TCM_BASE,
    HWIOModules_LPASS_TCM
  },
  {
    "HMSS",
    (DALSYSMemAddr)0x09800000,
    0x00800000,
    (DALSYSMemAddr)HMSS_BASE,
    HWIOModules_HMSS
  },
  {
    "PCIE_0_PCIE20_WRAPPER_AXI",
    (DALSYSMemAddr)0x0c000000,
    0x01000000,
    (DALSYSMemAddr)PCIE_0_PCIE20_WRAPPER_AXI_BASE,
    HWIOModules_PCIE_0_PCIE20_WRAPPER_AXI
  },
  {
    "PCIE_1_PCIE20_WRAPPER_AXI",
    (DALSYSMemAddr)0x0d000000,
    0x01000000,
    (DALSYSMemAddr)PCIE_1_PCIE20_WRAPPER_AXI_BASE,
    HWIOModules_PCIE_1_PCIE20_WRAPPER_AXI
  },
  {
    "PCIE_2_PCIE20_WRAPPER_AXI",
    (DALSYSMemAddr)0x0e000000,
    0x02000000,
    (DALSYSMemAddr)PCIE_2_PCIE20_WRAPPER_AXI_BASE,
    HWIOModules_PCIE_2_PCIE20_WRAPPER_AXI
  },
  {
    "MODEM",
    (DALSYSMemAddr)0x10000000,
    0x02000000,
    (DALSYSMemAddr)MODEM_BASE,
    HWIOModules_MODEM
  },
  { NULL, 0, 0, 0, NULL }
};

