/*============================================================================
  FILE:         TsensBsp.c

  OVERVIEW:     BSP for Tsens.

  DEPENDENCIES: None

                Copyright (c) 2014-2016 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2016-02-23  jjo  Disable MIN/MAX thresholds for channel 0.
  2015-09-15  jjo  Auto adjust period settings changes.
  2015-06-18  jjo  Add PWM enable.
  2015-04-30  jjo  Update controller 1 period.
  2014-07-21  jjo  Initial revision.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBsp.h"
#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

/* Thresholds */
#define TSENS_THRESHOLD_MIN       -350
#define TSENS_THRESHOLD_LOWER     TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_UPPER     TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_CRITICAL  TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_MAX       1200

/* Calibration points */
#define POINT_1_DECI_DEG_C 300
#define POINT_2_DECI_DEG_C 1200

/* Default calibration values */
#define INTERPOLATE_FOR_Y(m,x,x1,y1) (((m) * ((x) - (x1))) + (y1))
#define CODE_PER_DECI_DEG_C 0.312
#define POINT_1_CODE 456
#define POINT_2_CODE (INTERPOLATE_FOR_Y(CODE_PER_DECI_DEG_C, POINT_2_DECI_DEG_C, POINT_1_DECI_DEG_C, POINT_1_CODE))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
const TsensControllerCfgType gaTsensControllerCfgs[] =
{
   /* Controller 0 */
   {
      .bPSHoldResetEn     = TRUE,
      .uPeriodActive      = 0,
      .uPeriodSleep       = 64,
      .bAutoAdjustPeriod  = TRUE,
      .uTSControl         = 0x01803020,
      .uTSConfig          = 0x4B,
      .bStandAlone        = FALSE,
      .uSidebandEnMask    = 0x0,
      .bPWMEn             = FALSE,
      .pucTsensSROTPhys   = (uint8 *)MPM2_TSENS0_REG_BASE_PHYS,
      .uTsensSROTSize     = MPM2_TSENS0_REG_BASE_SIZE,
      .pucTsensTMPhys     = (uint8 *)MPM2_TSENS0_TSENS0_TM_REG_BASE_PHYS,
      .uTsensTMSize       = MPM2_TSENS0_TSENS0_TM_REG_BASE_SIZE
   },

   /* Controller 1 */
   {
      .bPSHoldResetEn     = TRUE,
      .uPeriodActive      = 32,
      .uPeriodSleep       = 64,
      .bAutoAdjustPeriod  = FALSE,
      .uTSControl         = 0x01803020,
      .uTSConfig          = 0x4B,
      .bStandAlone        = FALSE,
      .uSidebandEnMask    = 0xC0,
      .bPWMEn             = FALSE,
      .pucTsensSROTPhys   = (uint8 *)MPM2_TSENS1_REG_BASE_PHYS,
      .uTsensSROTSize     = MPM2_TSENS1_REG_BASE_SIZE,
      .pucTsensTMPhys     = (uint8 *)MPM2_TSENS1_TSENS1_TM_REG_BASE_PHYS,
      .uTsensTMSize       = MPM2_TSENS1_TSENS1_TM_REG_BASE_SIZE
   },
};

const TsensControllerCfgType gaTsensControllerCfgs_v1[] =
{
   /* Controller 0 */
   {
      .bPSHoldResetEn     = TRUE,
      .uPeriodActive      = 1,
      .uPeriodSleep       = 64,
      .bAutoAdjustPeriod  = TRUE,
      .uTSControl         = 0x01803020,
      .uTSConfig          = 0x4B,
      .bStandAlone        = FALSE,
      .uSidebandEnMask    = 0xC000,
      .bPWMEn             = FALSE,
      .pucTsensSROTPhys   = (uint8 *)MPM2_TSENS0_REG_BASE_PHYS,
      .uTsensSROTSize     = MPM2_TSENS0_REG_BASE_SIZE,
      .pucTsensTMPhys     = (uint8 *)MPM2_TSENS0_TSENS0_TM_REG_BASE_PHYS,
      .uTsensTMSize       = MPM2_TSENS0_TSENS0_TM_REG_BASE_SIZE
   },
};

const TsensSensorCfgType gaTsensSensorCfgs[] =
{
   /* Sensor 0 */
   {
      .uController                 = 0,
      .uChannel                    = 0,
      .uSensorID                   = 0,
      .uFuseIdx                    = 0,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_DISABLED,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_DISABLED
   },

   /* Sensor 1 */
   {
      .uController                 = 0,
      .uChannel                    = 1,
      .uSensorID                   = 1,
      .uFuseIdx                    = 1,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 2 */
   {
      .uController                 = 0,
      .uChannel                    = 2,
      .uSensorID                   = 2,
      .uFuseIdx                    = 2,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 3 */
   {
      .uController                 = 0,
      .uChannel                    = 3,
      .uSensorID                   = 3,
      .uFuseIdx                    = 3,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 4 */
   {
      .uController                 = 0,
      .uChannel                    = 4,
      .uSensorID                   = 4,
      .uFuseIdx                    = 4,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 5 */
   {
      .uController                 = 0,
      .uChannel                    = 5,
      .uSensorID                   = 5,
      .uFuseIdx                    = 5,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 6 */
   {
      .uController                 = 0,
      .uChannel                    = 6,
      .uSensorID                   = 6,
      .uFuseIdx                    = 6,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 7 */
   {
      .uController                 = 0,
      .uChannel                    = 7,
      .uSensorID                   = 7,
      .uFuseIdx                    = 7,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 8 */
   {
      .uController                 = 0,
      .uChannel                    = 8,
      .uSensorID                   = 8,
      .uFuseIdx                    = 8,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 9 */
   {
      .uController                 = 0,
      .uChannel                    = 9,
      .uSensorID                   = 9,
      .uFuseIdx                    = 9,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 10 */
   {
      .uController                 = 0,
      .uChannel                    = 10,
      .uSensorID                   = 10,
      .uFuseIdx                    = 10,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 11 */
   {
      .uController                 = 0,
      .uChannel                    = 11,
      .uSensorID                   = 11,
      .uFuseIdx                    = 11,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 12 */
   {
      .uController                 = 0,
      .uChannel                    = 12,
      .uSensorID                   = 12,
      .uFuseIdx                    = 12,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 13 */
   {
      .uController                 = 1,
      .uChannel                    = 1,
      .uSensorID                   = 1,
      .uFuseIdx                    = 14,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 14 */
   {
      .uController                 = 1,
      .uChannel                    = 6,
      .uSensorID                   = 6,
      .uFuseIdx                    = 19,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 15 */
   {
      .uController                 = 1,
      .uChannel                    = 7,
      .uSensorID                   = 7,
      .uFuseIdx                    = 20,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 16 */
   {
      .uController                 = 1,
      .uChannel                    = 0,
      .uSensorID                   = 0,
      .uFuseIdx                    = 13,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_DISABLED,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_DISABLED
   },

   /* Sensor 17 */
   {
      .uController                 = 1,
      .uChannel                    = 2,
      .uSensorID                   = 2,
      .uFuseIdx                    = 15,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 18 */
   {
      .uController                 = 1,
      .uChannel                    = 3,
      .uSensorID                   = 3,
      .uFuseIdx                    = 16,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 19 */
   {
      .uController                 = 1,
      .uChannel                    = 4,
      .uSensorID                   = 4,
      .uFuseIdx                    = 17,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 20 */
   {
      .uController                 = 1,
      .uChannel                    = 5,
      .uSensorID                   = 5,
      .uFuseIdx                    = 18,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },
};

const TsensSensorCfgType gaTsensSensorCfgs_v1[] =
{
   /* Sensor 0 */
   {
      .uController                 = 0,
      .uChannel                    = 0,
      .uSensorID                   = 0,
      .uFuseIdx                    = 0,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 1 */
   {
      .uController                 = 0,
      .uChannel                    = 1,
      .uSensorID                   = 1,
      .uFuseIdx                    = 1,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 2 */
   {
      .uController                 = 0,
      .uChannel                    = 13,
      .uSensorID                   = 13,
      .uFuseIdx                    = 13,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 3 */
   {
      .uController                 = 0,
      .uChannel                    = 3,
      .uSensorID                   = 3,
      .uFuseIdx                    = 3,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 4 */
   {
      .uController                 = 0,
      .uChannel                    = 4,
      .uSensorID                   = 4,
      .uFuseIdx                    = 4,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 5 */
   {
      .uController                 = 0,
      .uChannel                    = 5,
      .uSensorID                   = 5,
      .uFuseIdx                    = 5,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 6 */
   {
      .uController                 = 0,
      .uChannel                    = 6,
      .uSensorID                   = 6,
      .uFuseIdx                    = 6,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 7 */
   {
      .uController                 = 0,
      .uChannel                    = 7,
      .uSensorID                   = 7,
      .uFuseIdx                    = 7,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 8 */
   {
      .uController                 = 0,
      .uChannel                    = 8,
      .uSensorID                   = 8,
      .uFuseIdx                    = 8,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 9 */
   {
      .uController                 = 0,
      .uChannel                    = 9,
      .uSensorID                   = 9,
      .uFuseIdx                    = 9,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 10 */
   {
      .uController                 = 0,
      .uChannel                    = 10,
      .uSensorID                   = 10,
      .uFuseIdx                    = 10,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 11 */
   {
      .uController                 = 0,
      .uChannel                    = 11,
      .uSensorID                   = 11,
      .uFuseIdx                    = 11,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 12 */
   {
      .uController                 = 0,
      .uChannel                    = 12,
      .uSensorID                   = 12,
      .uFuseIdx                    = 12,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 13 */
   {
      .uController                 = 0,
      .uChannel                    = 2,
      .uSensorID                   = 2,
      .uFuseIdx                    = 2,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 14 */
   {
      .uController                 = 0,
      .uChannel                    = 14,
      .uSensorID                   = 14,
      .uFuseIdx                    = 14,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },

   /* Sensor 15 */
   {
      .uController                 = 0,
      .uChannel                    = 15,
      .uSensorID                   = 15,
      .uFuseIdx                    = 15,
      .eCal                        = TSENS_BSP_SENSOR_CAL_NORMAL,
      .nCalPoint1CodeDefault       = POINT_1_CODE,
      .nCalPoint2CodeDefault       = (int32)POINT_2_CODE,
      .nThresholdMinDeciDegC       = TSENS_THRESHOLD_MIN,
      .nThresholdLowerDeciDegC     = TSENS_THRESHOLD_LOWER,
      .nThresholdUpperDeciDegC     = TSENS_THRESHOLD_UPPER,
      .nThresholdCriticalDeciDegC  = TSENS_THRESHOLD_CRITICAL,
      .nThresholdMaxDeciDegC       = TSENS_THRESHOLD_MAX
   },
};

const TsensBspType TsensBsp[] =
{
   {
      .paControllerCfgs    = gaTsensControllerCfgs,
      .uNumControllers     = ARRAY_LENGTH(gaTsensControllerCfgs),
      .paSensorCfgs        = gaTsensSensorCfgs,
      .uNumSensors         = ARRAY_LENGTH(gaTsensSensorCfgs),
      .uSensorConvTime_us  = 170,
      .nCalPoint1DeciDegC  = POINT_1_DECI_DEG_C,
      .nCalPoint2DeciDegC  = POINT_2_DECI_DEG_C,
      .uShift              = 10
   }
};

const TsensBspType TsensBsp_v1[] =
{
   {
      .paControllerCfgs    = gaTsensControllerCfgs_v1,
      .uNumControllers     = ARRAY_LENGTH(gaTsensControllerCfgs_v1),
      .paSensorCfgs        = gaTsensSensorCfgs_v1,
      .uNumSensors         = ARRAY_LENGTH(gaTsensSensorCfgs_v1),
      .uSensorConvTime_us  = 170,
      .nCalPoint1DeciDegC  = POINT_1_DECI_DEG_C,
      .nCalPoint2DeciDegC  = POINT_2_DECI_DEG_C,
      .uShift              = 10
   }
};

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/

