/*===========================================================================

FILE:  SecDbgHw.c

DESCRIPTION:
  Debug Policy HW dependent functionality

EXTERNALIZED FUNCTIONS


Copyright (c) 2014-2015 by Qualcomm Technologies, Inc. All Rights Reserved.
===========================================================================*/

/*=========================================================================

                          EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when         who                what, where, why
--------   ----               ------------------------------------------- 
11/06/13   hw                  Init version.
===========================================================================*/


/*==========================================================================

           Include Files for Module

==========================================================================*/
#include <comdef.h>
#include "HALhwio.h"
#include "secboot_hwio.h"


/* Check if the debug policy fuse bit is set */
boolean sec_dbg_is_disable_fuse_bit_set()
{
  return 0 != HWIO_INM(QFPROM_RAW_OEM_CONFIG_ROW0_LSB, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_BMSK);
}

/* * 
 * @brief 
 *        Check if debug_disabled fuse is blown
 *
 * @retval    TRUE if debug_disabled fuse is blown, FALSE otherwise
 *
 */
boolean sec_dbg_is_debug_disabled_fuse_blown()
{
  boolean debug_disabled = FALSE;
  uint32 val1,val2;
  
  val1 = HWIO_INF(QFPROM_CORR_OEM_CONFIG_ROW1_LSB, DAP_SPIDEN_DISABLE);
  val2 = HWIO_INF(QFPROM_CORR_OEM_CONFIG_ROW1_LSB, DAP_SPNIDEN_DISABLE);
  if(val1 && val2 )
  {
    debug_disabled = TRUE;
  }
  return debug_disabled;
}
/* Set the override bits */
void sec_dbg_override_jtag()
{
  HWIO_OUT(OVERRIDE_1, HWIO_OVERRIDE_1_RMSK);
  HWIO_OUT(OVERRIDE_2, HWIO_OVERRIDE_2_RMSK);
  HWIO_OUT(OVERRIDE_3, HWIO_OVERRIDE_3_RMSK);
}

