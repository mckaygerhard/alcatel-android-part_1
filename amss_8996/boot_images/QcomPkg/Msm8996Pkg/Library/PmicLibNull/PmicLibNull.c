/** @file PmicLibNull.c
  
  Stub functions for PmicLib

  Copyright (c) 2014-2015, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 05/06/15   ck      Added pm_smbchg_usb_chgpth_irq_status
 01/21/15   ck      Added pmapp_ps_hold_cfg
 10/03/14   al      Updated for compilation
 05/30/14   kedara  Initial revision

=============================================================================*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/

#include "pm.h"
#include "pm_smbchg_usb_chgpth.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/*===========================================================================

**  Function :  pm_device_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type pm_device_init ( void )
{
  return PM_ERR_FLAG__SUCCESS;
}

/*===========================================================================

**  Function :  pm_pon_get_all_pon_reasons

** ==========================================================================
*/
/*!
 * @brief  This function returns the phone power-on reason. 
 *
 * INPUT PARAMETERS
 * @param pmic_device_index When the target has more than
 *          one pmic, this is the index of the PMIC in which
 *          the power on module is physically located. The device
 *          index starts at zero.
 *@param pwr_on_reason
 *    - pointer to 64-bit unsigned integer that stores the all PON reasons
 *   including PON power on, Warm Reset Reason and POFF_REASON,
 *   SOFT_RESET_REASON.
 *    PON_REASON
 *    PON_WARM_RESET_REASON
 *    PON_POFF_REASON
 *    PON_SOFT_RESET_REASON
 *
 * @return pm_err_flag_type.
 *         PM_ERR_FLAG__PAR1_OUT_OF_RANGE     = Input Parameter one is out of range.
 *         PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this 
 *                                       version of the PMIC.
 *         PM_ERR_FLAG__SBI_OPT_ERR           = The SBI driver failed to communicate
 *                                       with the PMIC.
 *         PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 *@par Dependencies
 *      and pm_init() must have been called.
===========================================================================*/
pm_err_flag_type pm_pon_get_all_pon_reasons(uint32 pmic_device_index, uint64* pon_reasons)
{
  return PM_ERR_FLAG__SUCCESS;
}


/*===========================================================================

**  Function :  pm_driver_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type pm_driver_init(void)
{
  return PM_ERR_FLAG__SUCCESS;
}


/*===========================================================================

**  Function :  pm_oem_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type pm_oem_init(void)
{
  return PM_ERR_FLAG__SUCCESS;
}


/*===========================================================================

**  Function :  pm_smem_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic smem initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type pm_smem_init(void)
{
  return PM_ERR_FLAG__SUCCESS;
}


/*===========================================================================

**  Function :  pm_rgb_led_config

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic rgb led functions to turn on blue LED
 * @return Error flag.
 *
 */ 
pm_err_flag_type pm_rgb_led_config
(
   uint32                     device_index,
   pm_rgb_which_type          rgb_peripheral,
   uint32                     rgb_mask,
   pm_rgb_voltage_source_type source_type,
   uint32                     dim_level,
   boolean                    enable_rgb )
{
  return PM_ERR_FLAG__SUCCESS;
}


/*===========================================================================

**  Function :  pm_pon_ps_hold_cfg

** ==========================================================================
*/
/**
 * @brief Configure MSM PS_HOLD behavior.
 *  
 * @details Configure PMIC to act on MSM PS_HOLD state
 * 
 * @param pmic_device_index When the target has more than
 *          one pmic, this is the index of the PMIC in which
 *          the power on module is physically located. The device
 *          index starts at zero.
 * @param ps_hold_cfg 
 *          PM_PON_RESET_CFG_WARM_RESET,
 *          PM_PON_RESET_CFG_NORMAL_SHUTDOWN,
 *          PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_SHUTDOWN, Shutdown + dVdd_rb remove main battery
 *          PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_SHUTDOWN, Shutdown + xVdd_rb remove main and coin cell battery
 *          PM_PON_RESET_CFG_HARD_RESET,     Shutdown + Auto pwr up
 *   
 *                   
 * @return pm_err_flag_type
 */
pm_err_flag_type pm_pon_ps_hold_cfg
(
  uint32 pmic_device_index, 
  pm_pon_reset_cfg_type ps_hold_cfg
)
{
  return PM_ERR_FLAG__SUCCESS;
}

pm_err_flag_type pm_vib_set_volt
(
  uint32              pmic_device_index,
  pm_vib_which_type vib,
  uint16                volt
)
{
  return PM_ERR_FLAG__SUCCESS;
}

pm_err_flag_type pm_vib_enable
(
  uint32            pmic_device_index,
  pm_vib_which_type vib,
  pm_vib_mode_type  mode,
  boolean           enable
)
{
  return PM_ERR_FLAG__SUCCESS;
}


pm_err_flag_type 
pmapp_ps_hold_cfg(pmapp_ps_hold_cfg_type ps_hold_cfg)
{
    return PM_ERR_FLAG__SUCCESS;
}


pm_err_flag_type
pm_smbchg_usb_chgpth_irq_status(uint32 device_index, 
                                pm_smbchg_usb_chgpth_irq_type irq, 
                                pm_irq_status_type type, 
                                boolean *status )
{
  return PM_ERR_FLAG__SUCCESS;
}
