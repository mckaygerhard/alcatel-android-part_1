#ifndef PMIC_SHARED_FUNCTIONS_H
#define PMIC_SHARED_FUNCTIONS_H

/*===========================================================================

                                APT PMIC Shared Functions
                                Header File

GENERAL DESCRIPTION
  This header file contains declarations and definitions for apt's shared 
  functions. 

Copyright  2015 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ----     ----------------------------------------------------------

10/29/15   pbarot   Initial creation
============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_extern_pmic_interface.h"
#include "pm_smbchg_usb_chgpth.h"
#include "pm_ibb.h"
#include "pm_lab.h"

/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
#define PMTEST shared_functions_table->pmic_shared_apis
    
/* Single structure that houses all of the PMIC functions to be shared between
   XBLLoader and any other image running at the same time as XBLLoader.
   I.E. Loader_test */

   
typedef struct
{
  pm_err_flag_type(*pm_gpio_status_get)(uint32, pm_gpio_perph_index, pm_gpio_status_type*);
  pm_err_flag_type(*pm_gpio_config_bias_voltage)(uint32, pm_gpio_perph_index, pm_gpio_volt_src_type);
  pm_err_flag_type(*pm_gpio_config_digital_input)(uint32, pm_gpio_perph_index, pm_gpio_current_src_pulls_type, pm_gpio_volt_src_type, pm_gpio_out_buffer_drv_strength_type, pm_gpio_src_config_type);
  pm_err_flag_type(*pm_gpio_config_digital_output)(uint32, pm_gpio_perph_index, pm_gpio_out_buffer_config_type, pm_gpio_volt_src_type, pm_gpio_src_config_type, pm_gpio_out_buffer_drv_strength_type, boolean);
  pm_err_flag_type(*pm_gpio_config_digital_input_output)(uint32, pm_gpio_perph_index, pm_gpio_src_config_type, pm_gpio_current_src_pulls_type, pm_gpio_volt_src_type, pm_gpio_out_buffer_config_type, pm_gpio_out_buffer_drv_strength_type);
  pm_err_flag_type(*pm_gpio_set_volt_source)(uint32, pm_gpio_perph_index, pm_gpio_volt_src_type);
  // pm_err_flag_type(*pm_gpio_config_mode_selection)(uint32, pm_gpio_perph_index, pm_on_off_type);
  pm_err_flag_type(*pm_gpio_set_output_buffer_configuration)(uint32, pm_gpio_perph_index, pm_gpio_out_buffer_config_type);
  pm_err_flag_type(*pm_gpio_set_inversion_configuration)(uint32, pm_gpio_perph_index, boolean);
  pm_err_flag_type(*pm_gpio_set_current_source_pulls)(uint32, pm_gpio_perph_index, pm_gpio_current_src_pulls_type);
  pm_err_flag_type(*pm_gpio_set_ext_pin_config)(uint32, pm_gpio_perph_index, pm_gpio_ext_pin_config_type);
  pm_err_flag_type(*pm_gpio_set_output_buffer_drive_strength)(uint32, pm_gpio_perph_index, pm_gpio_out_buffer_drv_strength_type);
  pm_err_flag_type(*pm_gpio_set_source_configuration)(uint32, pm_gpio_perph_index, pm_gpio_src_config_type);
  pm_err_flag_type(*pm_gpio_set_mux_ctrl)(uint32, pm_gpio_uart_path_type);
  pm_err_flag_type(*pm_gpio_irq_enable)(uint32, pm_gpio_perph_index, boolean);
  pm_err_flag_type(*pm_gpio_irq_clear)(uint32, pm_gpio_perph_index);
  pm_err_flag_type(*pm_gpio_irq_set_trigger)(uint32, pm_gpio_perph_index, pm_irq_trigger_type);
  pm_err_flag_type(*pm_gpio_irq_status)(uint32, pm_gpio_perph_index, pm_irq_status_type, boolean*);
  pm_err_flag_type(*pm_gpio_set_dig_in_ctl)(uint32, pm_gpio_perph_index, pm_gpio_dig_in_type, boolean);
 #if 1
  pm_err_flag_type(*pm_mpp_status_get)(uint32, pm_mpp_perph_index, pm_mpp_status_type*);
  pm_err_flag_type(*pm_mpp_config_digital_input)(uint32, pm_mpp_perph_index, pm_mpp_dlogic_lvl_type, pm_mpp_dlogic_in_dbus_type);
  pm_err_flag_type(*pm_mpp_config_digital_output)(uint32, pm_mpp_perph_index, pm_mpp_dlogic_lvl_type, pm_mpp_dlogic_out_ctrl_type);
  pm_err_flag_type(*pm_mpp_config_digital_inout)(uint32, pm_mpp_perph_index, pm_mpp_dlogic_lvl_type, pm_mpp_dlogic_inout_pup_type);
  pm_err_flag_type(*pm_mpp_config_dtest_output)(uint32, pm_mpp_perph_index, pm_mpp_dlogic_lvl_type, pm_mpp_dlogic_out_dbus_type);
  pm_err_flag_type(*pm_mpp_config_analog_input)(uint32,pm_mpp_perph_index, pm_mpp_ain_ch_type);
  pm_err_flag_type(*pm_mpp_config_analog_output)(uint32, pm_mpp_perph_index, pm_mpp_aout_level_type, pm_mpp_aout_switch_type);
  pm_err_flag_type(*pm_mpp_config_i_sink)(uint32, pm_mpp_perph_index, pm_mpp_i_sink_level_type, pm_mpp_i_sink_switch_type);
  pm_err_flag_type(*pm_mpp_config_atest)(uint32, pm_mpp_perph_index, pm_mpp_ch_atest_type);
  pm_err_flag_type(*pm_mpp_enable)(uint32, pm_mpp_perph_index, boolean);
  pm_err_flag_type(*pm_mpp_irq_enable)(uint32, pm_mpp_perph_index, boolean);
  pm_err_flag_type(*pm_mpp_irq_clear)(uint32, pm_mpp_perph_index);
  pm_err_flag_type(*pm_mpp_irq_set_trigger)(uint32, pm_mpp_perph_index, pm_irq_trigger_type);
  pm_err_flag_type(*pm_mpp_irq_status)(uint32, pm_mpp_perph_index, pm_irq_status_type, boolean*);
  pm_err_flag_type(*pm_mpp_set_list_mpp_with_shunt_cap)(uint32, pm_mpp_perph_index);
  // pm_err_flag_type(*pm_dev_get_mpp_with_shunt_cap_list_status_for_device)(uint32, pm_mpp_perph_index, uint32*);
  pm_err_flag_type(*pm_ibb_lcd_amoled_sel)(uint32, boolean);
  pm_err_flag_type(*pm_ibb_ibb_module_rdy)(uint32, boolean);
  pm_err_flag_type(*pm_ibb_config_ibb_ctrl)(uint32, boolean, boolean);
  pm_err_flag_type(*pm_ibb_set_soft_strt_chgr_resistor)(uint32, uint32);
  pm_err_flag_type(*pm_ibb_set_swire_output_pulse)(uint32, uint32);
  pm_err_flag_type(*pm_ibb_config_output_volt)(uint32, boolean, uint32);
  // pm_err_flag_type(*pm_ibb_config_pwrup_pwrdn_dly)(uint32, pm_ibb_pwrup_pwrdn_dly_type*);
  pm_err_flag_type(*pm_ibb_get_ibb_status)(uint32, pm_ibb_status_type*);
  pm_err_flag_type(*pm_lab_lcd_amoled_sel)(uint32, boolean);
  pm_err_flag_type(*pm_lab_lab_module_rdy)(uint32, boolean);
  pm_err_flag_type(*pm_lab_en_lab_module)(uint32, boolean);
  pm_err_flag_type(*pm_lab_ibb_rdy_en)(uint32, boolean);
  pm_err_flag_type(*pm_lab_config_precharge_ctrl)(uint32, boolean, uint32);
  pm_err_flag_type(*pm_lab_get_lab_status)(uint32, pm_lab_status_type*);
  pm_err_flag_type(*pm_lab_config_output_volt)(uint32, boolean, uint32);
  pm_err_flag_type(*pm_lpg_pwm_enable)(uint32, pm_lpg_chan_type, boolean);
  pm_err_flag_type(*pm_lpg_pwm_output_enable)(uint32, pm_lpg_chan_type,boolean);
  pm_err_flag_type(*pm_lpg_pwm_set_pwm_value)(uint32, pm_lpg_chan_type,uint16);
  pm_err_flag_type(*pm_lpg_pwm_set_pre_divide)(uint32, pm_lpg_chan_type, pm_lpg_pwm_pre_divide_type, pm_lpg_pwm_freq_expo_type);
  pm_err_flag_type(*pm_lpg_pwm_clock_sel)(uint32,pm_lpg_chan_type, pm_lpg_pwm_clock_type);
  pm_err_flag_type(*pm_lpg_set_pwm_bit_size)(uint32, pm_lpg_chan_type,pm_lpg_pwm_size_type);
  // pm_err_flag_type(*pm_lpg_bank_enable)(uint32, uint8, boolean);
  // pm_err_flag_type(*pm_lpg_bank_select)(uint32, uint8);
  // pm_err_flag_type(*pm_lpg_pwm_value_bypass_enable)(uint32, pm_lpg_chan_type,boolean);
  pm_err_flag_type(*pm_lpg_pwm_src_select)(uint32, pm_lpg_chan_type, pm_lpg_src_type);
  pm_err_flag_type(*pm_lpg_config_pwm_type)(uint32, pm_lpg_chan_type, boolean);
  pm_err_flag_type(*pm_lpg_pattern_config)(uint32, pm_lpg_chan_type, boolean, boolean, boolean, boolean, boolean);
  pm_err_flag_type(*pm_lpg_lut_config_set)(uint32, uint32, uint32);
  pm_err_flag_type(*pm_lpg_lut_config_get)(uint32, uint32, uint32*);
  pm_err_flag_type(*pm_lpg_lut_config_set_array)(uint32, uint32, uint32*, uint32);
  pm_err_flag_type(*pm_lpg_lut_config_get_array)(uint32, uint32, uint32*, uint32);
  pm_err_flag_type(*pm_lpg_pwm_ramp_generator_start)(uint32, pm_lpg_chan_type);
  pm_err_flag_type(*pm_lpg_pwm_lut_index_set)(uint32, pm_lpg_chan_type, uint32, uint32);
  pm_err_flag_type(*pm_lpg_config_pause_time)(uint32, pm_lpg_chan_type, uint32, uint32, uint32);
  pm_err_flag_type(*pm_lpg_pwm_ramp_generator_enable)(uint32, pm_lpg_chan_type, boolean);
  pm_err_flag_type(*pm_lpg_get_status)(uint32, pm_lpg_chan_type, pm_lpg_status_type*);
  pm_err_flag_type(*pm_lpg_set_lpg_dtest)(uint32, pm_lpg_chan_type, pm_lpg_chan_dtest_type, pm_lpg_chan_lpg_out_type);
  pm_err_flag_type(*pm_vib_enable)(unsigned, pm_vib_which_type, pm_vib_mode_type, boolean);
  pm_err_flag_type(*pm_vib_set_volt)(unsigned, pm_vib_which_type, uint16);
#endif

} pmtest_shared_function_type;

#endif /* PMIC_SHARED_FUNCTIONS */


