/**********************************************************************
* ufs_bsp_8996.c
*
* Board support file for MSM8996
*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
* 
*
**********************************************************************/

/*=======================================================================
                        Edit History

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/UfsTargetLib/ufs_bsp_uefi_8996.c#3 $
$DateTime: 2015/07/08 15:42:29 $

YYYY-MM-DD   who     what, where, why
---------------------------------------------------------------------- 
2015-06-15   rh      Disable the PHY from decoding the LCC sequence
2014-09-17   rh      Adapted for 8996 
2013-10-30   rh      Clock regime function cleanup
2013-10-30   rh      More accurate delay added
2013-06-19   rh      Initial creation
===========================================================================*/

#include <Uefi.h>
#include <HALhwio.h>
#include <Library/PcdLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/CacheMaintenanceLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
#include <Library/SynchronizationLib.h>
#include <Library/TimerLib.h>
#include <Protocol/EFIHWIO.h>
#include <Protocol/EFIClock.h>
#include <Protocol/LoadedImage.h> 

#include <Library/PcdLib.h>
#include <Library/ArmLib.h>

#include "ufs_osal.h"
#include "ufs_bsp.h"
#include "ufs_phy_hwio.h"

#define UFS_PHY_PCS_READY_TIMEOUT      0xffffff


EFI_CLOCK_PROTOCOL *hUFS_Clock = NULL;

void ufs_bsp_clk_set (uint8_t hostid, uint32_t mode)
{
   (void) hostid;
   /*    Disable for Virtio for now
   EFI_STATUS    eResult;
   uint32_t  nClockID, nFreqHz;   
   ptrdiff_t i;

   char* const szUFSClocks[] = {
      "gcc_ufs_tx_symbol_0_clk", "gcc_ufs_tx_symbol_1_clk", "gcc_ufs_rx_symbol_0_clk",
      "gcc_ufs_rx_symbol_1_clk", "gcc_ufs_tx_cfg_clk",      "gcc_ufs_rx_cfg_clk", 
      "gcc_sys_noc_ufs_axi_clk", "gcc_ufs_axi_clk",         "gcc_ufs_ahb_clk"};
   int32_t const nUFSClockFreq[] = {
             -1,         -1,         -1, 
             -1,  100000000,  100000000,
      200000000,  200000000,         -1};
      
   // Grab the clock protocol
   if (NULL == hUFS_Clock) {
      eResult = gBS->LocateProtocol(&gEfiClockProtocolGuid,
                                            NULL,
                                            (VOID **)&hUFS_Clock);
      if (EFI_SUCCESS != eResult)
         ASSERT_EFI_ERROR(eResult);

   }
  
   if (mode == UFS_CLOCK_MODE_OFF)
   {
      for (i=0; i < sizeof(szUFSClocks)/sizeof(szUFSClocks[0]); i++) {
         eResult = hUFS_Clock->GetClockID(hUFS_Clock, szUFSClocks[i], &nClockID);
         if (EFI_SUCCESS != eResult)
            ASSERT_EFI_ERROR(eResult);

         eResult = hUFS_Clock->DisableClock(hUFS_Clock, nClockID);         
         if (EFI_SUCCESS != eResult)
            ASSERT_EFI_ERROR(eResult);
      }
   }
   else if (mode == UFS_CLOCK_MODE_ON)
   {

      // SYS NOC AXI CLK - 200 MHz
      eResult = hUFS_Clock->GetClockID(hUFS_Clock, "gcc_sys_noc_axi_clk", &nClockID);
      if (EFI_SUCCESS != eResult)
         ASSERT_EFI_ERROR(eResult);

      eResult = hUFS_Clock->EnableClock(hUFS_Clock, nClockID);
      if (EFI_SUCCESS != eResult)
         ASSERT_EFI_ERROR(eResult);

      // CONFIG NOC AHB CLK - 75 MHz
      eResult = hUFS_Clock->GetClockID(hUFS_Clock, "gcc_cfg_noc_ahb_clk", &nClockID);
      if (EFI_SUCCESS != eResult)
         ASSERT_EFI_ERROR(eResult);

      eResult = hUFS_Clock->EnableClock(hUFS_Clock, nClockID);
      if (EFI_SUCCESS != eResult)
         ASSERT_EFI_ERROR(eResult);

      // TODO: MSM8994 supports NPA - Use NPA for set frequency on /clk/cnoc & /clk/snoc

      ufs_bsp_busy_wait(1000);

      // Enable rest of the clocks

      for (i=sizeof(szUFSClocks)/sizeof(szUFSClocks[0]) - 1; i>=0; i--) {
         eResult = hUFS_Clock->GetClockID(hUFS_Clock, szUFSClocks[i], &nClockID);
         if (EFI_SUCCESS != eResult)
            ASSERT_EFI_ERROR(eResult);

         if (nUFSClockFreq[i] >= 0) {
            eResult = hUFS_Clock->SetClockFreqHz(hUFS_Clock, nClockID, nUFSClockFreq[i],
                                              EFI_CLOCK_FREQUENCY_HZ_AT_MOST, &nFreqHz);
            if (EFI_SUCCESS != eResult)
               ASSERT_EFI_ERROR(eResult);
         }

         eResult = hUFS_Clock->EnableClock(hUFS_Clock, nClockID);
         if (EFI_SUCCESS != eResult)
            ASSERT_EFI_ERROR(eResult);
      }    

      ufs_bsp_busy_wait(1000);

   }
   */
}   

void ufs_bsp_cache_op (void *addr, uint32_t len, uint32_t op) 
{
   if (op & FLUSH) {
      WriteBackInvalidateDataCacheRange (addr, len);
   }

   if (op & INVAL) {
      InvalidateDataCacheRange (addr, len);
   }

   if (op & CLEAN) {
      WriteBackDataCacheRange (addr, len);
   }
}

void ufs_bsp_busy_wait(uint32_t us)
{
   gBS->Stall(us);
}

void ufs_bsp_memory_barrier (void)
{
   ArmDataMemoryBarrier();
}

// Constant table for UFS-PHY initialization
static struct ufs_mphy_init_item ufs_bsp_mphy_init_table[] = {
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      0x14},       // Adjust for series A
     {0,0}};

static struct ufs_mphy_init_item ufs_bsp_mphy_rate_b_init_table[] = {
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      0x54},
     {0,0}};

uint32_t ufs_bsp_get_mphy_init_item_table (struct ufs_mphy_init_item **itm)
{
   *itm = ufs_bsp_mphy_init_table;
   return sizeof(ufs_bsp_mphy_init_table)/sizeof(struct ufs_mphy_init_item);
}

uint32_t ufs_bsp_get_mphy_init_rate_b_item_table (struct ufs_mphy_init_item **itm)
{
   *itm = ufs_bsp_mphy_rate_b_init_table;
   return sizeof(ufs_bsp_mphy_rate_b_init_table)/sizeof(struct ufs_mphy_init_item);
}

// Start the PHY and wait for the PHY to be ready
uint32_t ufs_bsp_mphy_start (uint8_t hostid)
{
   uintptr_t base_addr;
   int32_t tout = UFS_PHY_PCS_READY_TIMEOUT;

   base_addr = ufs_bsp_get_reg_baseaddress (hostid);

   HWIO_REG_OUTM (base_addr, UFS_UFS_PHY_PHY_START_ADDR, 1, 1);
   ufs_bsp_busy_wait (10);
   
   while (!(HWIO_REG_IN(base_addr, UFS_UFS_PHY_PCS_READY_STATUS_ADDR) & 0x01)) {
      if (tout-- == 0) {
         return FAIL;
      }
      ufs_bsp_busy_wait (1);
   }
   return SUCCESS;
}

void ufs_bsp_post_link_init (uint8_t hostid)
{
   uintptr_t base_addr;

   base_addr = ufs_bsp_get_reg_baseaddress (hostid);
   out_dword(HWIO_UFS_UFS_PHY_LINECFG_DISABLE_ADDR + base_addr, 0x02);   
}

void *ufs_bsp_allocate_norm_buf (size_t size)
{
    VOID *mem;
    EFI_STATUS            status;
    status=gBS->AllocatePool(EfiBootServicesData,size,(VOID**)&mem);

    if (EFI_SUCCESS != status)
    {
        DEBUG((EFI_D_ERROR, "UFS: Allocate memory of size 0x%x     [FAILED]\n",size));
        return NULL;
    }
    return mem;
}

void  ufs_bsp_free_norm_buf (void *pmem)
{
    if ( pmem == NULL )
        return;

    gBS->FreePool(pmem);
}

void *ufs_bsp_allocate_xfer_buf (size_t size)
{
    return UncachedAllocatePool(size);
}

void  ufs_bsp_free_xfer_buf (void *pmem)
{
    if ( pmem == NULL )
        return;

    UncachedFreePool(pmem);
}

uintptr_t ufs_bsp_get_reg_baseaddress (uint8_t hostid)
{
   (void) hostid;
   return (uintptr_t) 0x620000;
}
