/**
  @file bamtgtcfgdata_apss.h
  @brief
  This file contains configuration data for the BAM driver for the 
  8996 APSS system.

*/
/*
===============================================================================

                             Edit History


when       who     what, where, why
--------   ---     ------------------------------------------------------------
27/8/14    rl      initial

===============================================================================
                   Copyright (c) 2013-2014 QUALCOMM Technologies Inc.
                          All Rights Reserved.
                        Qualcomm Confidential and Proprietary.
===============================================================================
*/

/** Configs supported for
    CE0
 */

#include "bamtgtcfg.h"

#define BAM_CNFG_BITS_VAL 0xFFFFF004
/* clear BAM_NO_EXT_P_RST,i.e bit 12, due to HW limitation*/
#define USB_BAM_CNFG_BITS (BAM_CNFG_BITS_VAL & (~(1 << 12)))


const bam_target_config_type  bam_tgt_config[] = {
    { 
                        // CE0_BAM 
     /* .bam_pa     */   0x644000,
     /* .options    */   BAM_TGT_CFG_FORCEINIT,
     /* .cfg_bits   */   BAM_CNFG_BITS_VAL,     
     /* .ee         */   0,
     /* .sec_config */   NULL,
     /* .size       */   BAM_MAX_MMAP
    },
    { //dummy config
     /* .bam_pa     */   BAM_TGT_CFG_LAST,
     /* .options    */   0,
     /* .cfg_bits   */   0,
     /* .ee         */   0,
     /* .sec_config */   NULL,
     /* .size       */   0
    },
};




