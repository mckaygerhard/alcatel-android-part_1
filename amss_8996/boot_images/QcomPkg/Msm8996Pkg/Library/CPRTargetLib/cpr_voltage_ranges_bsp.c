/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_voltage_ranges.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"

// Mx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t SS_8996_mx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil,  ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS,                850000, 850000,   850000,                 0,                   0,                 0,   CPR_FUSE_NO_FUSE,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,            950000, 850000,   950000,                 0,                   0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 855000,  1015000,                 0,                   0,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 3,
    .pmic_step_size = 5000,
};

static const cpr_config_rail_voltage_ranges_t mx_8996_voltage_ranges =
{
    .rail_id = CPR_RAIL_MX,
    .disable_fused_floor = true,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
        &SS_8996_mx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// Cx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t cx_voltage_ranges_v1_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                  CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(3, 0),   0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 520000,   670000,             70000,                  0,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SVS,                745000, 520000,   745000,             75000,                  0,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,            935000, 520000,   905000,             90000,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 520000,  1015000,            100000,                  0,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_versioned_voltage_ranges_t cx_voltage_ranges_v3_0 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           1},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 520000,   670000,             70000,                  0,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_SVS,                745000, 520000,   745000,             75000,                  0,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_NOMINAL,            935000, 520000,   905000,             90000,              30000,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 520000,  1015000,            100000,             -10000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_TURBO_OFFSET},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_versioned_voltage_ranges_t cx_voltage_ranges_v3_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 1,           2},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 520000,   670000,             70000,             -30000,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_SVS,                745000, 520000,   745000,             75000,             -30000,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_NOMINAL,            935000, 520000,   905000,             90000,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 520000,  1015000,            100000,             -10000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_TURBO_OFFSET},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_versioned_voltage_ranges_t cx_voltage_ranges_v3_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 2,           3},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 520000,   670000,             70000,             -30000,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_SVS,                745000, 520000,   745000,             75000,             -30000,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_NOMINAL,            935000, 520000,   905000,             90000,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 520000,  1015000,            100000,             -10000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_TURBO_OFFSET},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_versioned_voltage_ranges_t cx_voltage_ranges_v3_3 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 3,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 520000,   670000,             70000,                  0,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_SVS,                745000, 520000,   745000,             75000,                  0,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NOMINAL_OFFSET},
        {CPR_VOLTAGE_MODE_NOMINAL,            935000, 520000,   905000,             90000,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 520000,  1015000,            100000,                  0,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_TURBO_OFFSET},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_rail_voltage_ranges_t cx_8996_voltage_ranges =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
        &cx_voltage_ranges_v1_2,
		&cx_voltage_ranges_v3_0,
        &cx_voltage_ranges_v3_1,
        &cx_voltage_ranges_v3_2,
        &cx_voltage_ranges_v3_3,
    },
    .versioned_voltage_range_count = 5,
};

// VddaEbi Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t SS_8996_vdda_ebi_voltage_ranges_2_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                  CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(3, 0),   0,           3},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling,       Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,        725000,       675000,   725000,                 0,             125000,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SVS,         850000,       725000,   850000,                 0,                  0,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,     900000,       750000,   900000,                 0,              50000,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 962500+50000, 875000,   962500,                 0,              50000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 12500,
};

static const cpr_config_versioned_voltage_ranges_t SS_8996_vdda_ebi_voltage_ranges_2_3 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                  CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(3, 0),   3,           255},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling,       Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,        725000,       675000,   725000,                 0,                  0,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SVS,         850000,       725000,   850000,                 0,                  0,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,     900000,       750000,   900000,                 0,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 962500+50000, 875000,   962500,                 0,                  0,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 12500,
};

static const cpr_config_versioned_voltage_ranges_t SS_8996_vdda_ebi_voltage_ranges_3_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                       CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),  0,           3},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,         725000, 675000,   725000,                 0,             -25000,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SVS,          850000, 725000,   850000,                 0,             -25000,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,      900000, 750000,   900000,                 0,             -25000,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 1015000, 875000,  1015000,                 0,             -55000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_versioned_voltage_ranges_t SS_8996_vdda_ebi_voltage_ranges_3_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                       CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(3,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),  3,           255},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling,  Floor, Fuse-Ref, max_floor_to_ceil, ceiling_correction,  floor_correction,   volt_fuse_type,    offset_fuse_type
        {CPR_VOLTAGE_MODE_SVS2,         725000, 675000,   725000,                 0,             -25000,                 0,   CPR_FUSE_SVS2,     CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SVS,          850000, 725000,   850000,                 0,             -25000,                 0,   CPR_FUSE_SVS,      CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_NOMINAL,      900000, 750000,   900000,                 0,                  0,                 0,   CPR_FUSE_NOMINAL,  CPR_FUSE_NO_FUSE},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 1015000, 875000,  1015000,                 0,             -25000,                 0,   CPR_FUSE_TURBO,    CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 4,
    .pmic_step_size = 5000,
};

static const cpr_config_rail_voltage_ranges_t vdda_ebi_8996_voltage_ranges =
{
    .rail_id = CPR_RAIL_VDDA_EBI,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
        &SS_8996_vdda_ebi_voltage_ranges_2_1,
		&SS_8996_vdda_ebi_voltage_ranges_2_3,
        &SS_8996_vdda_ebi_voltage_ranges_3_1,
        &SS_8996_vdda_ebi_voltage_ranges_3_2,
    },
    .versioned_voltage_range_count = 4,
};

const cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config =
{
    .rail_voltage_ranges = (const cpr_config_rail_voltage_ranges_t*[])
    {
        &mx_8996_voltage_ranges,
        &cx_8996_voltage_ranges,
        &vdda_ebi_8996_voltage_ranges,
    },
    .rail_voltage_ranges_count = 3,
};

// Rail PMIC step-sizes
const cpr_config_misc_info_t cpr_bsp_misc_info_config =
{
    .cpr_rev_fuse = {HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR, 16, (1<<18) | (1<<17) | (1<<16)}, //0x7013C 18:16 CPR_GLOBAL_RC
};
