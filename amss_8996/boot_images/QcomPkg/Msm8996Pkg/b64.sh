#!/bin/bash
#/** @file b64.sh
# Shell script to initiate build using linaro gcc 64 bit
#
# Copyright (c) 2015, Qualcomm Technologies, Inc. All rights reserved.<BR>
# Portions Copyright (c) 2009 - 2010, Apple Inc. All rights reserved.<BR>
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#  
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#  
#**/

#==============================================================================
# EDIT HISTORY
#
#
# when       who     what, where, why
# --------   ---     ----------------------------------------------------------
#:11/02/15   fz      Update for STI pack out.
# 08/13/15   ck      Added extra Image Version error string
# 08/04/15   ck      Added Image Version build logic
# 08/01/15   sng     DDRDebugImage Framework Added
# 06/24/15   bh      Initial revision
#
#==============================================================================

shopt -s nocaseglob

#set up default paths
build_dir=`dirname $0`
cd $build_dir
export BUILD_ROOT=../..  

#
# Setup workspace if it is not set
#
if [ -z "$WORKSPACE" ]
then
  echo Initializing workspace
  cd $BUILD_ROOT
  export EDK_TOOLS_PATH=`pwd`/BaseTools
  export ARMLMD_LICENSE_FILE=7117@license-wan-arm1
  source edksetup.sh BaseTools
else
  echo Building from: $WORKSPACE
fi

#
# Pick a default tool type for a given OS
#
case `uname` in
  CYGWIN*)
      TARGET_TOOLS=RVCT501CYGWIN 
      ;;
  Linux*)
      TARGET_TOOLS=LLVMLINUX
      ;;
esac

#
# Set and parse compiling flags
#
if [ -z "$TARGET" ]
then
TARGET=DEBUG
fi
PACK=DEBUG
PRODMODE=DEBUGMODE
unset CLEAN   #pass in the clean command     
for arg in "$@"
do
  if [[ $arg == PRERELEASE ]];
  then
    PACK=PRERELEASE
  fi
  if [[ $arg == SkipBuild ]];
  then
    SkipBuild=1
  fi
  if [[ $arg == PRODMODE ]];
  then
    PRODMODE=PRODMODE
  fi
  if [[ $arg == clean ]]; then
    CLEAN=clean
    shift 
  fi 
  if [[ $arg == cleanall ]]; then
    CLEAN=cleanall
    shift
  fi
done
  

#
# Update TARGETMSM for the given package
#
QCOM_DIR=QcomPkg
export TARGETMSM=Msm8996
export VARIANT=LA
export TARGETMSM_PKG="$TARGETMSM"Pkg
export TARGETROOT=$QCOM_DIR/$TARGETMSM_PKG
export TARGETID=8996
TARGETMSM_PKG_PRE=Msm8996Pkg_pre
TARGETMSM_EFI=MSM8996_EFI.fd
BUILD_ROOT=$WORKSPACE/Build/${TARGETMSM}${VARIANT}_Core/"$TARGET"_"$TARGET_TOOLS"
ARCH=AARCH64
IMAGE_HEADER="python $WORKSPACE/QcomPkg/Tools/image_header.py"
PARTITION_TYPE=GPT
USES_BIN_FORMAT=1


#
# Enable sectools and set sectools_policy
#
export USE_SECTOOLS=1
export SECTOOLS_POLICY=USES_SEC_POLICY_MULTIPLE_DEFAULT_SIGN
export $SECTOOLS_POLICY=1
SECTOOLS_CFG_PATH=$WORKSPACE/QcomPkg/Tools/sectools/config/integration
SECTOOLS_SCRIPTS_PATH=$WORKSPACE/QcomPkg/Tools/sectools


IMAGE_VERSION_DIR=$WORKSPACE/QcomPkg/Library/ImageVersionLib
IMAGE_VERSION_BUILDER_PY=$WORKSPACE/QcomPkg/Tools/scripts/version_builder.py


TARGETMSM_ELF=MSM8996_EFI.elf

XBLLOADER_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/XBLLoader/DEBUG/XBLLoader.dll
XBLRAMDUMP_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$TARGETROOT/Library/XBLRamDumpLib/XBLRamDumpLib/DEBUG/XBLRamDump.dll
DDRTRAINING_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$TARGETROOT/Library/DDRTrngLib/DDRTrngLib/DEBUG/DDRTrng.dll

XBLCORE_IMG=$WORKSPACE/Build/${TARGETMSM}${VARIANT}_Core/$BUILD_DIR/${TARGET}_LLVMLINUX/FV/$TARGETMSM_ELF

XBL_IMG=$WORKSPACE/Build/${TARGETMSM}${VARIANT}_Core/$BUILD_DIR/${TARGET}_LLVMLINUX/FV/xbl.elf
PMIC_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$TARGETROOT/Library/PmicLib/Pmic/DEBUG/Pmic.dll
PMIC_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/Pmic.elf

DEVICEPROG_DDR_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/DevPrgD/DEBUG/DevPrgD.dll
DEVICEPROG_DDR_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/DevPrgD.elf

DEVICEPROG_LITE_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/DevPrgL/DEBUG/DevPrgL.dll
DEVICEPROG_LITE_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/DevPrgL.elf

DEVICEPROG_DDR_VIP_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/DevPrgDV/DEBUG/DevPrgDV.dll
DEVICEPROG_DDR_VIP_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/DevPrgDV.elf

DEVICEPROG_LITE_VIP_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/DevPrgLV/DEBUG/DevPrgLV.dll
DEVICEPROG_LITE_VIP_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/DevPrgLV.elf
DDR_DEBUG_IMAGE_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/XBLLoader/DDI/DEBUG/DDI.dll
DDR_DEBUG_IMAGE_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/DDI.elf

LOADERTEST_DLL=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/${TARGETMSM_PKG}/Library/LoaderTestLib/LoaderTestLib/DEBUG/LoaderTest.dll
LOADERTEST_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/$BUILD_DIR/${TARGET}_LLVMLINUX/AARCH64/loaderTest.elf
JTAG_PROGRAMMER_IMG=$WORKSPACE/Build/${TARGETMSM}_Loader/${TARGET}_LLVMLINUX/AARCH64/$QCOM_DIR/Library/JtagProgrammerLib/JtagProgrammer/DEBUG/JtagProgrammer.dll


echo ""
echo -n "Generating Image Version Files..."

rm -f $IMAGE_VERSION_DIR/oem_version.c > /dev/null
rm -f $IMAGE_VERSION_DIR/oem_uuid.c > /dev/null
rm -f $IMAGE_VERSION_DIR/qc_version.c > /dev/null

python $IMAGE_VERSION_BUILDER_PY -t $IMAGE_VERSION_DIR > /dev/null
result=$?
if [[ $result -ne 0 ]];
then
  if [[ $result -eq 2 ]];
  then
    echo "ERROR  manifest.xml file could not be found"
  else
    echo "ERROR  Image Version File generation error"
  fi
  exit -1
fi 

if [[ (! -e $IMAGE_VERSION_DIR/oem_version.c) ||
      (! -e $IMAGE_VERSION_DIR/oem_uuid.c) ||
      (! -e $IMAGE_VERSION_DIR/qc_version.c) ]];
then
  echo "ERROR  Image Version Files not generated"
  exit -1
fi
echo Done
echo ""


# Set -e option so failed commands exit this script.
# Do not set this before image version builder
set -e


if [[ $TARGET == DEBUG ]]; then
  BIN_DIR=Bin64
else
  BIN_DIR=Bin64
fi

if  [[ ! -e $EDK_TOOLS_PATH/Source/C/bin ]];
then
  #
  # Build the tools if they don't yet exist
  #
  echo Building tools: $EDK_TOOLS_PATH
  make -C $EDK_TOOLS_PATH
else
  echo using prebuilt tools
fi


#
#  For now create BuildVersion file with const version string. Enhance later to take cmdline param
#
if [ ! $CLEAN ]; then
 BUILD_VER_HDR_FILE=$WORKSPACE/QcomPkg/Drivers/SmBiosTableDxe/BuildVersion.h
 echo Creating QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h with version: 0
 echo \/\* DO NOT EDIT: This file is autogenerated \*\/> $BUILD_VER_HDR_FILE
 echo \#ifndef __BUILDVERSION_H__>> $BUILD_VER_HDR_FILE
 echo \#define __BUILDVERSION_H__>> $BUILD_VER_HDR_FILE
 echo >> $BUILD_VER_HDR_FILE
 echo \#ifndef UEFI_BUILD_VERSION>> $BUILD_VER_HDR_FILE
 echo \#define UEFI_BUILD_VERSION        \"0\">> $BUILD_VER_HDR_FILE
 echo \#endif \/\* UEFI_BUILD_VERSION \*\/>> $BUILD_VER_HDR_FILE
 echo >> $BUILD_VER_HDR_FILE
 echo \#endif \/\* __BUILDVERSION_H__ \*\/>> $BUILD_VER_HDR_FILE

 echo "Use default platcfg file uefiplatLA.cfg"
 rm -f $WORKSPACE/QcomPkg/$TARGETMSM_PKG/uefiplat.cfg 
 cp -f $WORKSPACE/QcomPkg/$TARGETMSM_PKG/uefiplatLA.cfg $WORKSPACE/QcomPkg/$TARGETMSM_PKG/uefiplat.cfg 
 touch $WORKSPACE/QcomPkg/$TARGETMSM_PKG/uefiplat.cfg 
fi

#Handle command arguments
if [[ $PACK == PRERELEASE ]]; then
  echo Please re-run build.sh to generate final image.
fi

# Check the clean flag
if [[ $CLEAN ]];then
   if [[ "$CLEAN" == "cleanall" ]]; then #clean the tools
    make -C $EDK_TOOLS_PATH clean
   fi
  #
  # Remove the binary files generated in this build script
  #
  rm -f $BUILD_ROOT/FV/uefi.mbn
  rm -f $BUILD_ROOT/FV/uefi_sec.mbn
  rm -f $BUILD_ROOT/FV/uefi_nonsec.mbn
  rm -f $BUILD_ROOT/FV/uefi.hd
  rm -f $BUILD_ROOT/FV/uefi_sec.hd
  rm -f $WORKSPACE/$TARGETROOT/$BIN_DIR/uefi.mbn
  #
  # Remove BuildVersion.h and BuildProducts.txt
  rm -f $WORKSPACE/QcomPkg/Drivers/SmBiosTableDxe/BuildVersion.h
  rm -f $WORKSPACE/../BuildProducts.txt
  #make clean
  build -p $WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}_Loader.dsc $CLEAN -a AARCH64 -t $TARGET_TOOLS -b $TARGET -D PRODMODE=$PRODMODE -j QcomPkg/$TARGETMSM_PKG/build_loader.log $1 $2 $3 $4 $5 $6 $7 $8
  build -p $WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}LA_Core.dsc   $CLEAN -a AARCH64 -t $TARGET_TOOLS -b $TARGET -D PRODMODE=$PRODMODE -j QcomPkg/$TARGETMSM_PKG/build_core.log   $1 $2 $3 $4 $5 $6 $7 $8
  
  if [ -e "$WORKSPACE/$TARGETROOT/$TARGETMSM_PKG_PRE.dsc" ]
  then
     build -p $WORKSPACE/$TARGETROOT/$TARGETMSM_PKG_PRE.dsc -a AARCH64 -t $TARGET_TOOLS -b $TARGET $CLEAN
  fi 
  exit
fi 
 
#
# Remove the binary files generated in this build script
#
rm -f $BUILD_ROOT/FV/uefi.mbn
rm -f $BUILD_ROOT/FV/uefi_sec.mbn
rm -f $BUILD_ROOT/FV/uefi_nonsec.mbn
rm -f $BUILD_ROOT/FV/uefi.hd
rm -f $BUILD_ROOT/FV/uefi_sec.hd
rm -f $WORKSPACE/$TARGETROOT/$BIN_DIR/uefi.mbn

if [[ $PACK != PRERELEASE ]]; then
  if [ -e "$WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}_LoaderTest.dsc" ]
  then
  build -p $WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}_LoaderTest.dsc -a AARCH64 -t $TARGET_TOOLS -b ${TARGET} -D PRODMODE=$PRODMODE -j QcomPkg/$TARGETMSM_PKG/build_loader.log $1 $2 $3 $4 $5 $6 $7 $8
  fi
  build -p $WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}_Loader.dsc -a AARCH64 -t $TARGET_TOOLS -b ${TARGET} -D PRODMODE=$PRODMODE -j QcomPkg/$TARGETMSM_PKG/build_loader.log $1 $2 $3 $4 $5 $6 $7 $8
  build -p $WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}LA_Core.dsc   -a AARCH64 -t $TARGET_TOOLS -b ${TARGET} -D PRODMODE=$PRODMODE -j QcomPkg/$TARGETMSM_PKG/build_core.log   $1 $2 $3 $4 $5 $6 $7 $8
fi

#
#  Add header and generate loadable binary image
#
if [ -e "$BUILD_ROOT/FV/$TARGETMSM_EFI" ]
then
    if [[ $PACK != PRERELEASE ]]; then
	if [[ $USES_BIN_FORMAT == 1 ]]; then
	    echo -n Creating image ELF header...
	    $IMAGE_HEADER $BUILD_ROOT/FV/$TARGETMSM_EFI $BUILD_ROOT/FV/uefi.mbn 0x80200000 elf 64
	else
	    echo -n Creating image MBN header...
	    $IMAGE_HEADER $BUILD_ROOT/FV/$TARGETMSM_EFI $BUILD_ROOT/FV/uefi.mbn 0x80200000 bin
	fi
	echo Done

	
    if  [[ ! -e $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/ ]];
	then
	mkdir -p $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR
	fi

    if  [[ ! -e $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/unsigned ]];
	then
	mkdir -p $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/unsigned
	fi
	
	echo ""
        cp -f $BUILD_ROOT/FV/uefi.mbn $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/uefi.mbn
	echo -n "Generating default signed UEFI image..."
        python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR -i $XBLCORE_IMG -g uefi --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
	echo Done
	
	# Call createxbl the first time with -n option as hashing should not be done till last merge
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $XBLLOADER_IMG -s $XBLRAMDUMP_IMG -o $XBL_IMG -a 64 -b 64 -n
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $XBL_IMG -s $XBLCORE_IMG -o $XBL_IMG -a 64 -b 64 -n
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $XBL_IMG -s $DDRTRAINING_IMG -o $XBL_IMG -a 64 -b 64
        cp $XBL_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/unsigned/xbl.elf
	echo -n "Generating default signed XBL image..."
        python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR -i $XBL_IMG -g sbl1 --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
        cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/sign/default/sbl1/xbl.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/xbl.elf
	echo Done
	
	# Call createxbl to get final Pmic and DeviceProgrammer Elf Image
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $PMIC_DLL -o $PMIC_IMG -a 64
        cp $PMIC_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/unsigned/Pmic.elf
	echo -n "Generating default signed PMIC image..."
        python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR -i $PMIC_IMG -g pmic --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
        cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/sign/default/pmic/Pmic.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/$BIN_DIR/pmic.elf
	echo Done

	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $DEVICEPROG_DDR_DLL -o $DEVICEPROG_DDR_IMG -a 64
	cp $DEVICEPROG_DDR_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/DeviceProgrammerDDR.elf
	cp $DEVICEPROG_DDR_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/prog_emmc_firehose_${TARGETID}_ddr.elf
	cp $DEVICEPROG_DDR_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/prog_ufs_firehose_${TARGETID}_ddr.elf

	echo -n "Generating default signed DEVICE PROGRAMMER DDR image..."
	python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $DEVICEPROG_DDR_IMG -g ENPRG --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/ENPRG/DevPrgD.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDR.elf
	echo Done
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDR.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/prog_emmc_firehose_${TARGETID}_ddr.elf >nul
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDR.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/prog_ufs_firehose_${TARGETID}_ddr.elf >nul

	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $DEVICEPROG_LITE_DLL -o $DEVICEPROG_LITE_IMG -a 64
	cp $DEVICEPROG_LITE_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/DeviceProgrammerLite.elf
	cp $DEVICEPROG_LITE_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/prog_emmc_firehose_${TARGETID}_lite.elf
	cp $DEVICEPROG_LITE_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/prog_ufs_firehose_${TARGETID}_lite.elf

        echo -n "Generating default signed DEVICE PROGRAMMER LITE image..."
	python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $DEVICEPROG_LITE_IMG -g ENPRG --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
        cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/ENPRG/DevPrgL.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLite.elf
	echo Done

	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLite.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/prog_emmc_firehose_${TARGETID}_lite.elf >nul
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLite.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/prog_ufs_firehose_${TARGETID}_lite.elf >nul

	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $DEVICEPROG_DDR_VIP_DLL -o $DEVICEPROG_DDR_VIP_IMG -a 64
	cp $DEVICEPROG_DDR_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/DeviceProgrammerDDRVIP.elf
	cp $DEVICEPROG_DDR_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/validated_emmc_firehose_${TARGETID}_ddr.elf
	cp $DEVICEPROG_DDR_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/validated_ufs_firehose_${TARGETID}_ddr.elf
	echo -n "Generating default signed DEVICE PROGRAMMER DDR VIP image..."
	python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $DEVICEPROG_DDR_VIP_IMG -g ENPRG --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/ENPRG/DevPrgDV.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDRVIP.elf
	echo Done
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDRVIP.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/validated_emmc_firehose_${TARGETID}_ddr.elf >nul
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerDDRVIP.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/validated_ufs_firehose_${TARGETID}_ddr.elf >nul

	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $DEVICEPROG_LITE_VIP_DLL -o $DEVICEPROG_LITE_VIP_IMG -a 64
	cp $DEVICEPROG_LITE_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/DeviceProgrammerLiteVIP.elf
	cp $DEVICEPROG_LITE_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/validated_emmc_firehose_${TARGETID}_lite.elf
	cp $DEVICEPROG_LITE_VIP_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/validated_ufs_firehose_${TARGETID}_lite.elf
	echo -n "Generating default signed DEVICE PROGRAMMER Lite VIP image..."
	python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $DEVICEPROG_LITE_VIP_IMG -g ENPRG --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/ENPRG/DevPrgLV.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLiteVIP.elf
	echo Done
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLiteVIP.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/validated_emmc_firehose_${TARGETID}_lite.elf >nul
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DeviceProgrammerLiteVIP.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/validated_ufs_firehose_${TARGETID}_lite.elf >nul
  
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $DDR_DEBUG_IMAGE_DLL  -o $DDR_DEBUG_IMAGE_IMG -a 64 

	cp $DDR_DEBUG_IMAGE_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/DDRDebugImage_${TARGETID}.elf		
	echo -n "Generating default signed DDR DEBUG image..."
	python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $DDR_DEBUG_IMAGE_IMG -g ENPRG --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >> QcomPkg/$TARGETMSM_PKG/build_sectools.log
	cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/ENPRG/DDI.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/DDRDebugImage_${TARGETID}.elf	
	echo Done
	if [ -e "$WORKSPACE/$TARGETROOT/${TARGETMSM_PKG}_LoaderTest.dsc" ]
    then
	# Call createxbl to get final Loader Test Elf Image
	python $WORKSPACE/QcomPkg/Tools/createxbl.py -f $LOADERTEST_DLL  -o $LOADERTEST_IMG -a 64  
	cp $LOADERTEST_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/loaderTest.elf >/dev/null
    echo -n "Generating default signed LOADERTEST image..."
    python $SECTOOLS_SCRIPTS_PATH/sectools_builder.py -t $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64 -i $LOADERTEST_IMG -g sdi --msmid=$TARGETID --config=$SECTOOLS_CFG_PATH/secimage.xml >>build_sectools.log
    cp $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/sign/default/sdi/loaderTest.elf $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/loaderTest.elf >/dev/null
	echo Done
	fi
	#Copy JTAG Programmer as is to keep debug data
	#JTAG Programmer is not signed so copy to BIN_DIR and BIN_DIR\unsigned
	cp $JTAG_PROGRAMMER_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/JtagProgrammer.elf
	cp $JTAG_PROGRAMMER_IMG $WORKSPACE/QcomPkg/$TARGETMSM_PKG/Bin64/unsigned/JtagProgrammer.elf

    echo ""

    python $WORKSPACE/QcomPkg/Tools/scripts/buildproducts_builder.py -t $WORKSPACE/.. -s $WORKSPACE/$TARGETROOT/$BIN_DIR -i xbl.elf
    python $WORKSPACE/QcomPkg/Tools/scripts/buildproducts_builder.py -t $WORKSPACE/.. -s $WORKSPACE/$TARGETROOT/$BIN_DIR -i pmic.elf

  fi
fi
