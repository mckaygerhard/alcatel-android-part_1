@ECHO OFF
:: @file b_full.bat
:: Batch file to initiate build
:: Copyright (c) 2014-2015, Qualcomm Technologies, Inc. All rights reserved.
:: Portions copyright (c) 2008 - 2010, Apple Inc. All rights reserved.
:: This program and the accompanying materials
:: are licensed and made available under the terms and conditions of the BSD License
:: which accompanies this distribution.  The full text of the license may be found at
:: http://opensource.org/licenses/bsd-license.php
:: 
:: THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
:: WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

::===========================================================================
:: when       who     what, where, why
:: --------   ---     -------------------------------------------------------
:: 11/02/15   fz      Update for STI pack out.
:: 09/02/15   kpa     Update Loadertest image to use sdi image id.
:: 09/02/15   kpa     Update Loadertest image name to loadertest
:: 08/13/15   ck      Added extra Image Version error string
:: 08/05/15   ck      Removed Msm8998
:: 08/04/15   ck      Added Image Version build logic
:: 08/01/15   sng     DDRDebugImage Framework Added
:: 07/28/15   rp      Add DDR training image to XBL image
:: 06/24/15   bh      Add variant variable to Build folder output
:: 06/09/15   plc     Build PMIC image for 8998 and BaseTarget
:: 06/04/15   vk      Build Loader using LLVM only
:: 06/02/15   ck      Updated for Sectools 3.14
:: 06/01/15   plc     Update for Msm8998
:: 05/19/15   vk      Support STI 32 bit build
:: 05/15/15   vk      Support seperate dsc for 8996 loader and core
:: 05/10/15   vk      Fix BuildProduct PROD mode image name
:: 05/10/15   vk      Fix BuildProduct forward slash
:: 05/10/15   vk      Add debug logs
:: 05/08/15   ck      Fixed JtagProgrammer copy issue
:: 04/30/15   vk      Pass arguments directly to build.exe
:: 04/28/15   ck      Added image default signing
:: 04/23/15   jb      Update BuildProduct base directory
:: 03/19/15   ck      Removed static binary copy list
:: 03/13/15   vk      Trim comment history in file - DOS limitation
:: 03/12/15   ck      Added logic to copy EFI files needed for packed build
::===========================================================================

echo [b_full.bat] **************************
echo [b_full.bat] **************************
echo [b_full.bat] **************************
echo [b_full.bat] Initializing workspace

:: Example usage of this script. default is a DEBUG build
:: This script needs to be called only from the target level b.bat script

:: 2432 has a "split" build.  Loader and Core have seperate DSC files and
:: output is in two different locations.  Use BUILD_SUFFIX variables
:: to enable this.
::
echo Choosing DSC files ...

if "%TARGETMSM%"=="Msm8996" (
  SET BUILD_SUFFIX_LOADER="_Loader"
  echo ARCH is %ARCH%
  if "%ARCH%"=="ARM" (
    SET BUILD_SUFFIX_CORE="_Core_32"
  ) else (
    SET BUILD_SUFFIX_CORE="_Core"
  )
) else if "%TARGETMSM%"=="BaseTarget" (
  SET BUILD_SUFFIX_LOADER="_Loader"
  echo ARCH is %ARCH%
  if "%ARCH%"=="ARM" (
    SET BUILD_SUFFIX_CORE="_Core_32"
  ) else (
    SET BUILD_SUFFIX_CORE="_Core"
  )
) else (
  SET BUILD_SUFFIX_LOADER=
  SET BUILD_SUFFIX_CORE=
) 


:: Setup Build environment. Sets WORKSPACE and puts build in path
CALL ..\edksetup.bat --nt32

@ECHO OFF

if ERRORLEVEL 1 (
  echo [b_full.bat]  ERROR: Script needs to be called by the target specific b.bat
  goto Failure
)

:: Check whether env variables are set
SET MSM=%TARGETMSM%
if not defined MSM ( 
  echo [b_full.bat]  ERROR: Script needs to be called by the target specific b.bat
  goto Failure
)

set RVCT501_ASMOPT=--sitelicense
set RVCT501_CCOPT=--sitelicense
set RVCT501_LINKOPT=--sitelicense
set TOOLS_PATH=C:\Apps\ARMCT5.01\113\bin

:: Setup the ARM license location, setup closest server 
set ARMLMD_LICENSE_FILE=7117@license-wan-arm1

:: Set for tools chain. Currently RVCT501
SET TARGET_TOOLS=RVCT501

setlocal enabledelayedexpansion
if not defined TARGET ( 
  SET TARGET=DEBUG
)
setlocal disabledelayedexpansion

if "%TARGETMSM%"=="BaseTarget" (
  SET BIN_DIR=Bin64
)
if "%TARGETMSM%"=="Msm8996" (
  SET BIN_DIR=Bin64
) else (
  if "%TARGET%"=="DEBUG" (
    SET BIN_DIR=Bin64
  ) else (
    SET BIN_DIR=Bin64_Release
  )
)

SET ARCH=ARM
SET QCOM_DIR=QcomPkg
SET SKIPBUILD=0
SET BUILDVERSION=0
SET CLEAN_SWITCH= 
SET PRODMODE=DEBUGMODE
SET UEFI_PLAT_CONFIG_FILE=
SET IS_PRE_BUILD=0
SET UEFI_OUTPUT_FILE=uefi.mbn
SET BUILD_LOG_FILE=%WORKSPACE%\%TARGETROOT%\build%BUILD_SUFFIX_CORE%.log
SET BUILD_PRODUCTS_FILE=%WORKSPACE%\..\BuildProducts.txt

:: ----------------------------------------
:: enable sectools and set sectools_policy
SET USE_SECTOOLS=%USE_SECTOOLS%
SET %SECTOOLS_POLICY%=1
SET SECTOOLS_CFG_PATH=%WORKSPACE%\QcomPkg\Tools\sectools\config\integration
SET SECTOOLS_SCRIPTS_PATH=%WORKSPACE%\QcomPkg\Tools\sectools
:: ----------------------------------------


goto argloop
:shift2args
shift /1
:shift1args
shift /1

:argloop
if /I "%1"=="RELEASE" (  
  SET PRODMODE=PRODMODE
  SET UEFI_OUTPUT_FILE=uefiprod.mbn  
  goto shift1args
)

if /I "%1"=="PRERELEASE" (
  SET IS_PRE_BUILD=1  
  goto shift1args
)



if /I "%1"=="-v" (  
  ::Set the build version: example "-v 1234"
  SET BUILDVERSION=%2
  @echo [b_full.bat]  Build Version String: %2
  goto shift2args
)

if /I "%1"=="CLEAN" (  
  SET CLEAN_SWITCH=cleanall
  goto shift1args
)

if /I "%1"=="CLEANALL" (  
  SET CLEAN_SWITCH=cleanall
  goto shift1args
)

if /I "%1"=="DEBUGFLAG" (  
  echo [b_full.bat]  ERROR: DEBUGFLAG no longer supported ...
  goto Failure
)

if /I "%1"=="SKIPBUILD" (  
  SET SKIPBUILD=1
  goto shift1args
)

if /I "%1"=="PRODMODE" (  
  SET PRODMODE=PRODMODE
  goto shift1args
)

if /I "%1"=="-cfg" (  
  :: Set the config file
  SET UEFI_PLAT_CONFIG_FILE=%2
  goto shift2args
)

if /I "%1"=="-o" (  
  :: Set the output file name
  SET UEFI_OUTPUT_FILE=%2
  goto shift2args
)

if /I "%1"=="-compiler" (  
  SET TARGET_TOOLS=%2
  goto shift2args
)

if /I "%1"=="-arch" (  
  SET ARCH=%2
  if /I "%2"=="AARCH64" ( 
    echo [b_full.bat]  Reset tools for AARCH64 LLVM ...
    SET TOOLS_PATH="C:\Apps\LLVM\3.5.2.1\bin"
    echo [b_full.bat]  Set tools for AARCH64 GCC...
    SET TOOLS_PATH_GCC="C:\Apps\Linaro\gcc-linaro-aarch64-none-elf-4.9-2014.07"
  )

  if /I "%2"=="ARM" ( 
    echo [b_full.bat]  Reset tools for RVCT 5.01
    set TOOLS_PATH=C:\Apps\ARMCT5.01\113\bin
    set TOOLS_PATH_GCC=C:\Apps\ARMCT5.01\113\bin
  )
  goto shift2args
)

if /I "%1"=="" (  
  goto doneloop
)
echo [b_full.bat]  ----------------------------------------------------------------------------------
echo [b_full.bat]  Usage warning: passing command line arguments directly to edk build.exe
SET EXTRA_ARGS=%EXTRA_ARGS% %1
echo [b_full.bat]  %EXTRA_ARGS%
echo [b_full.bat]  ----------------------------------------------------------------------------------
goto shift1args

:doneloop

if NOT EXIST %TOOLS_PATH% (
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo [b_full.bat]   ERROR...!!  Compiler tool chain ERROR...!!
  echo.
  echo [b_full.bat]   Compiler tools are not installed, or installed in an unexpected location.
  echo [b_full.bat]   If NOT installed, please install and try building again.
  echo.
  echo [b_full.bat]   If installed in unexpected location, then update the configuration files accordingly
  echo.
  echo [b_full.bat]   Expected location %TOOLS_PATH%
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo.
  echo.
  goto Failure
)


if NOT EXIST %TOOLS_PATH_GCC% (
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo [b_full.bat]   ERROR...!!  Compiler tool chain ERROR...!!
  echo.
  echo [b_full.bat]   Compiler tools are not installed, or installed in an unexpected location.
  echo [b_full.bat]   If NOT installed, please install and try building again.
  echo.
  echo [b_full.bat]   If installed in unexpected location, then update the configuration files accordingly
  echo.
  echo [b_full.bat]   Expected location %TOOLS_PATH_GCC%
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo.
  echo [b_full.bat] .
  goto Failure
)

echo [b_full.bat]  Saving nmake environment log ...
call nmake /p>nmake_env.log
if ERRORLEVEL 1 (
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo [b_full.bat]  ERROR...!!  Missing nmake.exe ERROR...!!
  echo.
  echo [b_full.bat]  ERROR: nmake.exe not found in path, please install nmake from Visual Studio 2013 
  echo.
  echo [b_full.bat]  **************************************************************************************
  echo.
  echo.
 
  goto Failure
)


if exist %BUILD_PRODUCTS_FILE% (
  del /F /Q %BUILD_PRODUCTS_FILE%

  if /I NOT "%CLEAN_SWITCH%"=="cleanall" (

    if "%ARCH%"=="AARCH64" (
      echo .\boot_images\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\%UEFI_OUTPUT_FILE% > %BUILD_PRODUCTS_FILE%
      echo .\boot_images\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\xbl.elf >> %BUILD_PRODUCTS_FILE%
      
      if "%PRODMODE%"=="PRODMODE" (
        ::echo .\HY11_1\boot_images\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\%UEFI_OUTPUT_FILE% >> %BUILD_PRODUCTS_FILE%
	::echo .\HY11_1\boot_images\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\xbl.elf >> %BUILD_PRODUCTS_FILE%
      )
    )
  
    if "%ARCH%"=="ARM" (
      echo .\boot_images\QcomPkg\%TARGETMSM%Pkg\Bin32\%UEFI_OUTPUT_FILE% > %BUILD_PRODUCTS_FILE%
      echo .\boot_images\QcomPkg\%TARGETMSM%Pkg\Bin32\xbl.elf >> %BUILD_PRODUCTS_FILE%
    
      if "%PRODMODE%"=="PRODMODE" (
        ::echo .\HY11_1\boot_images\QcomPkg\%TARGETMSM%Pkg\Bin32\%UEFI_OUTPUT_FILE% >> %BUILD_PRODUCTS_FILE%
        ::echo .\HY11_1\boot_images\QcomPkg\%TARGETMSM%Pkg\Bin32\xbl.elf >> %BUILD_PRODUCTS_FILE%
      )
    )
  )
)


if exist %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h (
  if /I NOT "%BUILDVERSION%"=="0" (  
    :: Recreate build version only if new build version is provided
    goto createbuildversion
  )

  echo [b_full.bat]  QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h already exists. 
  echo [b_full.bat]  No build version specified. Will use existing version.
  goto endbuildversion
) else (
  goto createbuildversion
)

:createbuildversion
echo [b_full.bat]  Creating QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h with version: %BUILDVERSION%
echo /* DO NOT EDIT: This file is autogenerated */> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo #ifndef __BUILDVERSION_H__>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo #define __BUILDVERSION_H__>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo.>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo #ifndef UEFI_BUILD_VERSION>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo   #define UEFI_BUILD_VERSION        "%BUILDVERSION%">> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo #endif>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo.>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo #endif /* __BUILDVERSION_H__ */>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h
echo.>> %WORKSPACE%\QcomPkg\Drivers\SmBiosTableDxe\BuildVersion.h

:endbuildversion

SET BUILD_DIR=%TARGET%_%TARGET_TOOLS%
SET IMAGE_VERSION_DIR=%WORKSPACE%\QcomPkg\Library\ImageVersionLib
SET IMAGE_VERSION_BUILDER_PY=%WORKSPACE%\QcomPkg\Tools\scripts\version_builder.py
SET XBLLOADER_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\XBLLoader\DEBUG\XBLLoader.dll
SET XBLRAMDUMP_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\%TARGETMSM%Pkg\Library\XBLRamDumpLib\XBLRamDumpLib\DEBUG\XBLRamDump.dll
SET DDRTRAINING_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\%TARGETMSM%Pkg\Library\DDRTrngLib\DDRTrngLib\DEBUG\DDRTrng.dll
SET XBLCORE_IMG=%WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\%TARGETMSM_ELF%
SET XBL_IMG=%WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\xbl.elf
SET XBL_VERSION_IMG=%WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\xbl_version.elf
SET VERSION_MANIFEST=%WORKSPACE%\QcomPkg\Library\AdcLib\build\manifest.xml
SET XBLLOADER_IMG_OUT=xbl_loader.elf
SET XBLRAMDUMP_IMG_OUT=xbl_ramdump.elf
SET DDRTRAINING_IMG_OUT=ddr_training.elf
SET XBLCORE_IMG_OUT=xbl_core.elf

SET PMIC_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\%TARGETMSM%Pkg\Library\PmicLib\Pmic\DEBUG\Pmic.dll
SET PMIC_IMG=%WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\Pmic.elf

SET DEVICEPROG_DDR_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\DevPrgD\DEBUG\DevPrgD.dll
SET DEVICEPROG_DDR_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\DevPrgD.elf

SET DEVICEPROG_LITE_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\DevPrgL\DEBUG\DevPrgL.dll
SET DEVICEPROG_LITE_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\DevPrgL.elf

SET DEVICEPROG_DDR_VIP_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\DevPrgDV\DEBUG\DevPrgDV.dll
SET DEVICEPROG_DDR_VIP_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\DevPrgDV.elf

SET DEVICEPROG_LITE_VIP_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\DevPrgLV\DEBUG\DevPrgLV.dll
SET DEVICEPROG_LITE_VIP_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\DevPrgLV.elf

SET DDR_DEBUG_IMAGE_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\XBLLoader\DDI\DEBUG\DDI.dll
SET DDR_DEBUG_IMAGE_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\DDI.elf

SET JTAG_PROGRAMMER_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\Library\JtagProgrammerLib\JtagProgrammer\DEBUG\JtagProgrammer.dll
SET LOADERTEST_DLL=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\%QCOM_DIR%\%TARGETMSM%Pkg\Library\LoaderTestLib\LoaderTestLib\DEBUG\loaderTest.dll
SET LOADERTEST_IMG=%WORKSPACE%\Build\%TARGETMSM%%BUILD_SUFFIX_LOADER%\%BUILD_DIR%\AARCH64\loaderTest.elf


echo. 
echo|set /p=[b_full.bat]  Generating Image Version Files...

del %IMAGE_VERSION_DIR%\oem_version.c 2>nul
del %IMAGE_VERSION_DIR%\oem_uuid.c 2>nul
del %IMAGE_VERSION_DIR%\qc_version.c 2>nul

python %IMAGE_VERSION_BUILDER_PY% -t %IMAGE_VERSION_DIR% >nul
if not %ERRORLEVEL%==0 (
  if %ERRORLEVEL%==2 (
    echo ERROR  manifest.xml file could not be found
  ) else (
    echo ERROR  Image Version File generation error
  )
  EXIT /B 1
)

set created=true
if not exist %IMAGE_VERSION_DIR%\oem_version.c set created=false
if not exist %IMAGE_VERSION_DIR%\oem_uuid.c set created=false
if not exist %IMAGE_VERSION_DIR%\qc_version.c set created=false
if %created%=="false" (
  echo ERROR  Image Version Files not generated
  EXIT /B 1
)
echo Done
echo.


echo [b_full.bat]  Building from: %WORKSPACE%
:: echo using prebuilt tools

:: Delete the files generated in this batch file
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.hd del /F /Q %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.hd
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_sec.hd del /F /Q %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_sec.hd
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn del /F /Q %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_nonsec.mbn del /F /Q %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_nonsec.mbn
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_sec.mbn del /F /Q %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi_sec.mbn
if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\signed rmdir /q /s %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\signed

if "%ARCH%"=="AARCH64" (
  if exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi.mbn
  if exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi_nondebug.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi_nondebug.mbn
  if exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi_sign_ready.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi_sign_ready.mbn
  if exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\signed rmdir /q /s %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\signed
  if exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\xbl.elf del /F /Q %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\xbl.elf

) else (
  if exist %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn
  if exist %WORKSPACE%\%TARGETROOT%\Bin32\uefi_nondebug.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\Bin32\uefi_nondebug.mbn
  if exist %WORKSPACE%\%TARGETROOT%\Bin32\uefi_sign_ready.mbn del /F /Q %WORKSPACE%\%TARGETROOT%\Bin32\uefi_sign_ready.mbn
  if exist %WORKSPACE%\%TARGETROOT%\Bin32\signed rmdir /q /s %WORKSPACE%\%TARGETROOT%\Bin32\signed
  if exist %WORKSPACE%\%TARGETROOT%\Bin32\xbl.elf del /F /Q %WORKSPACE%\%TARGETROOT%\Bin32\xbl.elf
)

cd %WORKSPACE%\%TARGETROOT%

if %SKIPBUILD%==1 goto SkipBuild

if "%CLEAN_SWITCH%"=="cleanall" ( 
  if "%BUILD_SUFFIX_CORE%"=="" (
    CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%VARIANT%.dsc -a %ARCH% -t %TARGET_TOOLS% -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build.log %CLEAN_SWITCH% %EXTRA_ARGS%
  ) else (
    CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%VARIANT%%BUILD_SUFFIX_CORE%.dsc -a %ARCH% -t %TARGET_TOOLS% -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build%BUILD_SUFFIX_CORE%.log %CLEAN_SWITCH% %EXTRA_ARGS%
    CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%VARIANT%%BUILD_SUFFIX_CORE%.dsc -a %ARCH% -t %TARGET_TOOLS% -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build%BUILD_SUFFIX_CORE%.log %CLEAN_SWITCH% %EXTRA_ARGS%
  )
  echo [b_full.bat]  Exiting ...
  goto Exit
)


if exist build.log (
  del /Q /F old_build%BUILD_SUFFIX_CORE%.log
  ren build%BUILD_SUFFIX_CORE%.log old_build%BUILD_SUFFIX_CORE%.log

  del /Q /F old_build%BUILD_SUFFIX_LOADER%.log
  ren build%BUILD_SUFFIX_LOADER%.log old_build%BUILD_SUFFIX_LOADER%.log
)

if "%UEFI_PLAT_CONFIG_FILE%"=="" (
  echo [b_full.bat]  WARNING: Config file not specified, using uefiplatWP.cfg !
  SET UEFI_PLAT_CONFIG_FILE="uefiplatWP.cfg"
)

echo [b_full.bat]  Platform config file is %UEFI_PLAT_CONFIG_FILE%
if not exist %WORKSPACE%\%TARGETROOT%\%UEFI_PLAT_CONFIG_FILE% (
  echo [b_full.bat]  ERROR: Missing Platform config file ...
  goto Failure
)

:: Delete existing, copy specified config file as uefiplat.cfg and touch,  this will be packaged in mbn file
del /F   %WORKSPACE%\%TARGETROOT%\uefiplat.cfg

copy /Y/B  %WORKSPACE%\%TARGETROOT%\%UEFI_PLAT_CONFIG_FILE% %WORKSPACE%\%TARGETROOT%\uefiplat.cfg
if ERRORLEVEL 1 goto Failure

::  unix touch
copy /Y/B  %WORKSPACE%\%TARGETROOT%\uefiplat.cfg+,,
if ERRORLEVEL 1 goto Failure

:: Confirm copy did not fail
if not exist %WORKSPACE%\%TARGETROOT%\%UEFI_PLAT_CONFIG_FILE% (
  echo [b_full.bat]  ERROR: Unable to find copied config file ...
  goto Failure
)

:: Build the %TARGETMSM% SURF firmware and create an FD (FLASH Device) Image.
if "%BUILD_SUFFIX_CORE%"=="" (
  CALL build.exe -p %WORKSPACE%\%TARGETROOT%\%TARGETMSM%Pkg%VARIANT%.dsc -a %ARCH% -t %TARGET_TOOLS% -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build.log %EXTRA_ARGS%
) else (
   if exist %WORKSPACE%\%TARGETROOT%\%TARGETMSM%Pkg%BUILD_SUFFIX_LOADER%Test.dsc (
     CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%BUILD_SUFFIX_LOADER%Test.dsc -a AARCH64 -t LLVMWIN       -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build%BUILD_SUFFIX_LOADER%.log %EXTRA_ARGS%
   )
   CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%BUILD_SUFFIX_LOADER%.dsc -a AARCH64 -t LLVMWIN       -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build%BUILD_SUFFIX_LOADER%.log %EXTRA_ARGS%
   
  if ERRORLEVEL 1 goto Failure
   CALL build.exe -p %TARGETROOT%\%TARGETMSM%Pkg%VARIANT%%BUILD_SUFFIX_CORE%.dsc   -a %ARCH% -t %TARGET_TOOLS% -b %TARGET% -D "PRODMODE=%PRODMODE%" -j build%BUILD_SUFFIX_CORE%.log %EXTRA_ARGS%
)


:SkipBuild

if ERRORLEVEL 1 goto Failure

if exist %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\%TARGETMSM_EFI% (
  if "%ARCH%"=="AARCH64" (
    echo|set /p=Creating UEFI ELF64 image header...
    python %WORKSPACE%\QcomPkg\Tools\image_header.py %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\%TARGETMSM_EFI% %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn %TARGET_ADDR% elf 64
    echo Done
  ) else (
    echo|set /p=Creating ELF image header...
    python %WORKSPACE%\QcomPkg\Tools\image_header.py %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\%TARGETMSM_EFI% %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn %TARGET_ADDR% elf
    echo Done
  )

  if "%ARCH%"=="AARCH64" (
    REM echo Copying image file to Bin folder...
    if not exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR% (
      mkdir %WORKSPACE%\%TARGETROOT%\%BIN_DIR%
    )
      
    if not exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned (
      mkdir %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned
    )

    echo.  
    copy %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\uefi.mbn >nul
    echo|set /p=Generating default signed UEFI image...
    python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn -g uefi --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >>%BUILD_LOG_FILE%
    copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\uefi\uefi.mbn %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi.mbn >nul
    echo Done
  ) 

  REM Generate XBL image
  REM Create single or individual ELF output files based on target.
  if "%ARCH%"=="AARCH64" (
    if not exist %WORKSPACE%\%TARGETROOT%\%BIN_DIR% (
      mkdir %WORKSPACE%\%TARGETROOT%\%BIN_DIR%
    )

    if "%TARGETMSM%"=="Msm8996" (
      REM Call createxbl to get final DDR DEBUG IMAGE 
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DDR_DEBUG_IMAGE_DLL% -o %DDR_DEBUG_IMAGE_IMG% -a 64
	  
      copy %DDR_DEBUG_IMAGE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\DDRDebugImage_%TARGETID%.elf >nul   
      echo|set /p=Generating default signed DDR DEBUG image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %DDR_DEBUG_IMAGE_IMG% -g ENPRG --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log	  
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\ENPRG\DDI.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DDRDebugImage_%TARGETID%.elf >nul
      echo Done
 
      REM Call createxbl the first time with -n option as hashing should not be done till last merge
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %XBLLOADER_IMG% -s %XBLRAMDUMP_IMG% -o %XBL_IMG% -a 64 -b 64 -n
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %XBL_IMG% -s %XBLCORE_IMG% -o %XBL_IMG% -a 64 -b 64 -n
	  python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %XBL_IMG% -s %DDRTRAINING_IMG% -o %XBL_IMG% -a 64 -b 64
      copy %XBL_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\xbl.elf >nul
      echo|set /p=Generating default signed XBL image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %XBL_IMG% -g sbl1 --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml > build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\sbl1\xbl.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\xbl.elf >nul
      echo Done

      REM Call createxbl to get final Pmic Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %PMIC_DLL% -o %PMIC_IMG% -a 64
      copy %PMIC_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\pmic.elf >nul
      echo|set /p=Generating default signed PMIC image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %PMIC_IMG% -g pmic --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\pmic\Pmic.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\Pmic.elf >nul
      echo Done

      REM Call createxbl to get final Device Programmer DDR Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_DDR_DLL% -o %DEVICEPROG_DDR_IMG% -a 64
      copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\DeviceProgrammerDDR.elf >nul
      copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\prog_emmc_firehose_%TARGETID%_ddr.elf >nul
      copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\prog_ufs_firehose_%TARGETID%_ddr.elf >nul
      echo|set /p=Generating default signed DEVICE PROGRAMMER DDR image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %DEVICEPROG_DDR_IMG% -g ENPRG --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\ENPRG\DevPrgD.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDR.elf >nul
      echo Done
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDR.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_emmc_firehose_%TARGETID%_ddr.elf >nul
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDR.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_ufs_firehose_%TARGETID%_ddr.elf >nul
      
      REM Call createxbl to get final Device Programmer Lite Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_LITE_DLL% -o %DEVICEPROG_LITE_IMG% -a 64
      copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\DeviceProgrammerLite.elf >nul
      copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\prog_emmc_firehose_%TARGETID%_lite.elf >nul
      copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\prog_ufs_firehose_%TARGETID%_lite.elf >nul
      echo|set /p=Generating default signed DEVICE PROGRAMMER LITE image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %DEVICEPROG_LITE_IMG% -g ENPRG --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\ENPRG\DevPrgL.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLite.elf >nul
      echo Done
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLite.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_emmc_firehose_%TARGETID%_lite.elf >nul
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLite.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_ufs_firehose_%TARGETID%_lite.elf >nul

      REM Call createxbl to get final Device Programmer DDR VIP Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_DDR_VIP_DLL% -o %DEVICEPROG_DDR_VIP_IMG% -a 64
      copy %DEVICEPROG_DDR_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\DeviceProgrammerDDRVIP.elf >nul
      copy %DEVICEPROG_DDR_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\validated_emmc_firehose_%TARGETID%_ddr.elf >nul
      copy %DEVICEPROG_DDR_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\validated_ufs_firehose_%TARGETID%_ddr.elf >nul
      echo|set /p=Generating default signed DEVICE PROGRAMMER DDR VIP image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %DEVICEPROG_DDR_VIP_IMG% -g ENPRG --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\ENPRG\DevPrgDV.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDRVIP.elf >nul
      echo Done
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDRVIP.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\validated_emmc_firehose_%TARGETID%_ddr.elf >nul
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDRVIP.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\validated_ufs_firehose_%TARGETID%_ddr.elf >nul


      REM Call createxbl to get final Device Programmer Lite VIP Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_LITE_VIP_DLL% -o %DEVICEPROG_LITE_VIP_IMG% -a 64
      copy %DEVICEPROG_LITE_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\DeviceProgrammerLiteVIP.elf >nul
      copy %DEVICEPROG_LITE_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\validated_emmc_firehose_%TARGETID%_lite.elf >nul
      copy %DEVICEPROG_LITE_VIP_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\validated_ufs_firehose_%TARGETID%_lite.elf >nul
      echo|set /p=Generating default signed DEVICE PROGRAMMER LITE VIP image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %DEVICEPROG_LITE_VIP_IMG% -g ENPRG --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\ENPRG\DevPrgLV.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLiteVIP.elf >nul
      echo Done
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLiteVIP.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\validated_emmc_firehose_%TARGETID%_lite.elf >nul
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLiteVIP.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\validated_ufs_firehose_%TARGETID%_lite.elf >nul

      REM Copy JTAG Programmer as is to keep debug data.
      REM JTAG Programmer is not signed so copy to BIN_DIR and BIN_DIR\unsigned
      copy %JTAG_PROGRAMMER_IMG% %WORKSPACE%\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\JtagProgrammer.elf >nul
      copy %JTAG_PROGRAMMER_IMG% %WORKSPACE%\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\unsigned\JtagProgrammer.elf >nul
	  
      IF EXIST %WORKSPACE%\%TARGETROOT%\%TARGETMSM%Pkg%BUILD_SUFFIX_LOADER%Test.dsc (
	  
	   REM Call createxbl to get final Loader Test Elf Image
       python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %LOADERTEST_DLL%  -o %LOADERTEST_IMG% -a 64  
       copy %LOADERTEST_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\loaderTest.elf >nul
       echo|set /p=Generating default signed LOADERTEST image...
       python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %LOADERTEST_IMG% -g sdi --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >>build_sectools.log
       copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\sdi\loaderTest.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\loaderTest.elf >nul
	   echo Done
	 )
	 
    )
    
	
    if "%TARGETMSM%"=="BaseTarget" (
 
      REM Call createxbl the first time with -n option as hashing should not be done till last merge
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %XBLLOADER_IMG% -s %XBLRAMDUMP_IMG% -o %XBL_IMG% -a 64 -b 64 -n
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %XBL_IMG% -s %XBLCORE_IMG% -o %XBL_IMG% -a 64 -b 64
      copy %XBL_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\xbl.elf >nul
      echo|set /p=Generating default signed XBL image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %XBL_IMG% -g sbl1 --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml > build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\sbl1\xbl.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\xbl.elf >nul
      echo Done

      REM Call createxbl to get final Pmic Elf Image
      python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %PMIC_DLL% -o %PMIC_IMG% -a 64
      copy %PMIC_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\unsigned\pmic.elf >nul
      echo|set /p=Generating default signed PMIC image...
      python %SECTOOLS_SCRIPTS_PATH%\sectools_builder.py -t %WORKSPACE%\%TARGETROOT%\%BIN_DIR% -i %PMIC_IMG% -g pmic --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml >> build_sectools.log
      copy %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\sign\default\pmic\Pmic.elf %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\Pmic.elf >nul
      echo Done

      REM Call createxbl to get final Device Programmer DDR Elf Image
      REM python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_DDR_DLL% -o %DEVICEPROG_DDR_IMG% -a 64
      REM copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerDDR.elf
      REM copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_emmc_firehose_%TARGETID%_ddr.elf
      REM copy %DEVICEPROG_DDR_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_ufs_firehose_%TARGETID%_ddr.elf

      REM Call createxbl to get final Device Programmer Lite Elf Image
      REM python %WORKSPACE%\QcomPkg\Tools\createxbl.py -f %DEVICEPROG_LITE_DLL% -o %DEVICEPROG_LITE_IMG% -a 64
      REM copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\DeviceProgrammerLite.elf
      REM copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_emmc_firehose_%TARGETID%_lite.elf
      REM copy %DEVICEPROG_LITE_IMG% %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\prog_ufs_firehose_%TARGETID%_lite.elf

      REM Copy JTAG Programmer as is to keep debug data
      REM copy %JTAG_PROGRAMMER_IMG% %WORKSPACE%\QcomPkg\%TARGETMSM%Pkg\%BIN_DIR%\JtagProgrammer.elf
    )		
	
  ) else (
     if not exist %WORKSPACE%\%TARGETROOT%\Bin32 (
     mkdir %WORKSPACE%\%TARGETROOT%\Bin32
     )
  )

  echo.

  if "%ARCH%"=="AARCH32" ( 
    echo.
    echo [b_full.bat]  To sign the image, re-issue command: 
    echo [b_full.bat]  'b.bat SIGN'
    echo.
    
    if not exist %WORKSPACE%\%TARGETROOT%\Bin32\cert (
      mkdir %WORKSPACE%\%TARGETROOT%\Bin32\cert
    )
    
    echo [b_full.bat]  UEFI image built successfully! Copying unsecure image to %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn ... 
    copy %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn
    
    echo [b_full.bat]  Creating sign ready UEFI image
    copy %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn %WORKSPACE%\%TARGETROOT%\Bin32\uefi_sign_ready.mbn
    
    REM --------------------------------------------------------------
    REM run sectools for image signing
    if "%USE_SECTOOLS%"=="1" (
        echo [b_full.bat]  Copying unsigned image files to sub folder...
        if not exist %WORKSPACE%\%TARGETROOT%\Bin32\unsigned (
          mkdir %WORKSPACE%\%TARGETROOT%\Bin32\unsigned
        )
        copy %WORKSPACE%\%TARGETROOT%\Bin32\uefi_sign_ready.mbn %WORKSPACE%\%TARGETROOT%\Bin32\unsigned\uefi_sign_ready.mbn
        echo [b_full.bat]  Generate and install signed images as default
        python %WORKSPACE%\QcomPkg\Tools\sectools\sectools_builder.py -t %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV -i %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\uefi.mbn -g uefi --msmid=%TARGETID% --config=%SECTOOLS_CFG_PATH%\secimage.xml --install_base_dir=%WORKSPACE%\%TARGETROOT%\Bin32 --install_file_name=uefi_sign_ready.mbn>>%BUILD_LOG_FILE%
        copy %WORKSPACE%\%TARGETROOT%\Bin32\uefi_sign_ready.mbn %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn
    )
    REM --------------------------------------------------------------
  )
)
REM unset the sec_policy for next run
SET %SECTOOLS_POLICY%=0

if not %PRODMODE%==PRODMODE (
  echo [b_full.bat]  %PRODMODE% Appending debug FV..
  if "%VARIANT%"=="" (
    if "%ARCH%"=="AARCH64" (
      python %WORKSPACE%\QcomPkg\Tools\fvinfo_creator.py %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi.mbn %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\TOOLS_COMPACT.Fv %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\TESTS_COMPACT.Fv %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\DALTFTESTS_COMPACT.Fv 
    ) else (
      python %WORKSPACE%\QcomPkg\Tools\fvinfo_creator.py %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\TOOLS_COMPACT.Fv %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\TESTS_COMPACT.Fv %WORKSPACE%\Build\%TARGETMSM%%VARIANT%%BUILD_SUFFIX_CORE%\%BUILD_DIR%\FV\DALTFTESTS_COMPACT.Fv
    )
  )
)

if not %UEFI_OUTPUT_FILE%==uefi.mbn (
  echo [b_full.bat]  Renaming output file to %UEFI_OUTPUT_FILE%
  if "%ARCH%"=="AARCH64" (
    move %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\uefi.mbn %WORKSPACE%\%TARGETROOT%\%BIN_DIR%\%UEFI_OUTPUT_FILE%
  ) else (
    move %WORKSPACE%\%TARGETROOT%\Bin32\uefi.mbn %WORKSPACE%\%TARGETROOT%\Bin32\%UEFI_OUTPUT_FILE%
  )
)


if ERRORLEVEL 1 goto Failure
:: TODO: Fix pythin script and move to this
:: python %WORKSPACE%\QcomPkg\Tools\scripts\buildproducts_builder.py -t %WORKSPACE%\.. -s %WORKSPACE%\QcomPkg\%TARGETMSM%Pkg\Bin64

echo [b_full.bat] ++++++++++++++++++++++++++
echo [b_full.bat] ++++++++++++++++++++++++++
echo [b_full.bat] ++++++++++++++++++++++++++
echo [b_full.bat] Done building %TARGETMSM%%VARIANT%, Exiting ... 
goto Exit

:Failure
echo [b_full.bat] ERROR: Failed to build package: %TARGETMSM%%VARIANT%
EXIT /B %ERRORLEVEL%

:Exit
EXIT /B 0
