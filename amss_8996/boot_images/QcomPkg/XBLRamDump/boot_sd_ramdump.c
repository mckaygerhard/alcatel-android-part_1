/*===========================================================================

                         Boot SD RAM Dump Feature File

GENERAL DESCRIPTION
  This feature file contains definitions  for  functions for taking memory
  dumps to SD card.

Copyright 2010-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who             what, where, why
--------   ---         --------------------------------------------------
04/14/15   ao          adjust baseaddr for 64bit in function boot_process_sd_dumps()
01/01/15   bh          Added stdlib header files
10/01/14   ck          Added boot_extern_hotplug_interface header
07/18/14   ck          Removed bl_shared_data from parameter list
06/20/14   kedara      Remove std lib header inlcudes for compilation using llvm.
05/30/14   kedara      Replace snprintf by qsnprintf.
04/02/14   ck          Fixed KW issues
03/18/14   ck          Updated boot_hw_reset calls as they now take a reset type parameter
10/18/13   jz          Updated to support the new HotPlug API to poll the device
10/02/13   wg          Updated MAX_SIZE_FOLDER_STRING size for up to 999 folders
					   for RamDump 
07/11/13   dh          Rewrite ram dump code. Add logic to generate the ram dump
                       header file(ramdump.bin) on the same directory.
07/04/13   hh          Move ram dump to directory /ram_dump
05/15/13   dh          Reset device at the end of sd card ram dump if emmc raw ramdump
                        cookie is set
05/07/13   wg          fix klocwork warnings
04/22/13   dh          switch to use boot_efs_opendir, boot_efs_closedir and boot_befs_mkdir
04/17/13   plc         Make Cumulative Ram Dumps feature replace legacy
04/02/13   dh          Call dload_mem_debug_supported to determine if dump is allowed
03/29/13   plc         Add logic to dump multiple times, and toggle LED
05/02/12   dh          change all local char array to global static variable
04/09/12   dh          Use boot_efs_errno instead of efs_errno
11/09/11   kedar       Skip DLOAD mode after copying system dumps to Sdcard if 
                       reset cookie file present. 
06/09/11   kedar       Replace unsafe string functions and use strlcpy, strlcat
02/01/11   dh          add feature flag to protect dload_mem_debug_init
                       and only execute it if auth is disabled.
01/21/11   dh          fix klocwok warning
7/22/10   Kedar        Initial version  

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_sd_ramdump.h"
#include "boot_dload_debug.h"
#include "boot_sdcc.h"
#include "boot_util.h"
#include "boot_extern_hotplug_interface.h"
#include "boot_extern_efs_interface.h"
#include "boothw_target.h"
#include "boot_cache_mmu.h"
#include "boot_visual_indication.h"
#include "boot_shared_imem_cookie.h"
#include "boot_raw_partition_ramdump.h"
#include "boot_extern_hotplug_interface.h"
#include <string.h>
#include <stdio.h>
#include "LoaderUtils.h"
#include "boot_target.h"
#include "boot_extern_pmic_interface.h"
#include "boot_logger.h"
#include "tct.h"
#ifdef FEATURE_TCTNB_DUMPSDCARD
#include "boot_logger.h"
#include "boot_logger_timer.h"
#include "boot_extern_busywait_interface.h"
#include "boot_flash_trans_if.h"
#include "boot_flash_dev_if.h"
#endif /* FEATURE_TCTNB_DUMPSDCARD */

/* Buffer to store filenames */
static char cookiefilepath[MAX_FILE_NAME];
#ifndef FEATURE_TCTNB_DUMPSDCARD
static char cookiefilename[MAX_FILE_NAME];
#endif
static char filename[MAX_FILE_NAME];
static char errmax_filename[MAX_FILE_NAME];
static char err_filename[MAX_FILE_NAME];
static char foldername[MAX_FILE_NAME];
static char foldernumber[MAX_FILE_NAME];

static boot_ramdump_errlog_data_type boot_sd_rd_errdata; 

static uint32 max_region_entries;
/* 512 Bytes buffer if the base address is 0*/
static uint8 sector_buf[SECTOR_SIZE]; 

#ifdef FEATURE_TCTNB_DUMPSDCARD

extern uint8 traceability_partition_id[];

#define TRACEABILITY_PARTITION_OFFSET 0xE7000
#define FULL_RAMDUMP_SIZE 4328521728UL    /* (4 * 1024 + 32) MB, 4136+MB in fact*/

#define WAIT_SDCARD_TIMEOUT_DEFAULT (5)     /* 5 mins */

enum dump_device_location_type
{
  LOCATION_INTERNAL_ONLY    = 0,           /* try ramdump partition -> QPST */
  LOCATION_EXTERNAL_ONLY,                  /* try SD card -> QPST */
  LOCATION_INTERNAL_FORCE,                 /* force ramdump partition -> QPST */
  LOCATION_EXTERNAL_FORCE,                 /* force SD card -> QPST */
  LOCATION_INTERNAL_PREFER,                /* try ramdump partition -> try SD card -> QPST */
  LOCATION_EXTERNAL_PREFER,                /* try SD card -> try ramdump partition -> QPST */
  LOCATION_DEVICE_AUTO,                    /* SD card & ramdump partition both failed -> QPST */
  LOCATION_DEVICE_DEFAULT   = LOCATION_EXTERNAL_ONLY
};

enum restart_reason_type
{ /* reference to kernel/drivers/power/reset/msm-poweroff.c */
  REASON_IGNORE             = -1,         /* ignore this input */
  REASON_BOOTLOADER         = 0x77665500,
  REASON_RECOVERY           = 0x77665502,
  REASON_RTC                = 0x77665503,
  REASON_DMVERITY_CORRUPTED = 0x77665508,
  REASON_DMVERITY_ENFORCE   = 0x77665509,
  REASON_KEYS_CLEAR         = 0x7766550a,
  REASON_OEM_PREFIX         = 0x6f656d00,
};

struct PACK (boot_ramdump_header)
{
    uint16_t magic;     /* 0xAAAA or 0x5555 */
    uint8_t  timeout;   /* time out minutes waiting for SD card */
    uint8_t  location;  /* dump device location type */
};

#endif /* FEATURE_TCTNB_DUMPSDCARD */

/* Max number of cumulative dumps supported */
#define MAXDUMPS 100
#define MAX_SIZE_FOLDER_STRING 5 /* foldernames range from 1-100, needed for same qsnprintf */

#define SD_DUMP_HEADER_NAME "rawdump.bin"
/*=========================================================================
                            
                       FUNCTION DEFINITIONS

=========================================================================*/


#ifdef FEATURE_TCTNB_DUMPSDCARD
/**
 * Save restart reason to offset 0x1200 at traceability partition.
 *
 * @param restart_reason
 *    The restart reason, refer to the enum restart_reason_type for the
 *    list of restart reasons.
 *
 * @return
 *    0 on success, or negative error value on failure.
 */
static void set_restart_reason
(
  enum restart_reason_type restart_reason                 /* for testing */
)
{
  boot_flash_trans_if_type *trans_if = NULL;
  boot_boolean success = FALSE;
  uint32 *boot_restart_reason_ptr = (uint32*)(SHARED_IMEM_BOOT_BASE + 0x65C);
  struct trace_flag {
    uint32 reason;
    uint32 dload;
  } flag;

  flag.dload = 0x504d5544; //"DUMP"
  if (restart_reason != REASON_IGNORE)
  {
    flag.reason = (enum restart_reason_type) restart_reason;
  }
  else
  {
    flag.reason = *boot_restart_reason_ptr;
  }

  /* Write DDR training data to storage device */
  boot_flash_configure_target_image(traceability_partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  BL_VERIFY( trans_if != NULL, BL_ERR_RAM_DUMP_FAIL );

  success = dev_sdcc_write_bytes(&flag,
                                 0x1200,
                                 sizeof(flag),
                                 GEN_IMG);

  //BL_VERIFY(success, BL_ERR_RAM_DUMP_FAIL);
  if (!success)
  {
    boot_log_message("warning: set_restart_reason failed");
  }

  /* close parition */
  boot_flash_dev_close_image(&trans_if);
}

void set_restart_reason_wrapper(
  int restart_reason                 /* for testing */
)
{
  set_restart_reason((enum restart_reason_type) restart_reason);
}

/**
 * Get ramdump header from partition.
 *
 * @param ramdump_header
 *    Pointer to ramdump_header.
 *
 * @param partition_id
 *    Partition id for reading.
 *
 * @param offset
 *    Offset of the partition.
 *
 * @return
 *    Returns nothing.
 */
static void boot_get_ramdump_header
(
  uint8 partition_id[],                          /* partition id for reading */
  uint32 offset,                                 /* offset of the partition */
  struct boot_ramdump_header *ramdump_header    /* pointer to ramdump_header */
)
{
  boot_read_flash_partition(partition_id,
                            (void*)ramdump_header,
                            offset,
                            sizeof(struct boot_ramdump_header));
  if (ramdump_header->magic != 0xAAAA && ramdump_header->magic != 0x5555)
  {
    /* Use default values */
    ramdump_header->timeout = WAIT_SDCARD_TIMEOUT_DEFAULT;
    ramdump_header->location = LOCATION_DEVICE_DEFAULT;
  }
  else if (ramdump_header->magic == 0x5555)
  {
    ramdump_header->timeout = 0;
  }
}

/**
 * Wait for SD card insert, until timeout.
 *
 * @param timeout_seconds
 *    Timeout seconds.
 *
 * @return
 *    Returns nothing.
 */
static void boot_wait_for_sd_card
(
  uint32 timeout_seconds          /* time out seconds waiting for SD card */
)
{
  if (timeout_seconds == 0)
  {
    return;
  }

  boot_log_message("boot_wait_for_sd_card, Start");
  boot_log_start_timer();
  boot_hotplug_poll_wait_device(HOTPLUG_TYPE_MMC,
          HOTPLUG_ITER_EXTERNAL_DEVICES_ONLY, timeout_seconds * 1000 * 1000);
  boot_log_stop_timer("boot_wait_for_sd_card, Delta");
  boot_busywait(1 * 1000 * 1000); //wait 1s to detect sdcard
}

static int boot_delete_tree(char *path)
{
  int status = 0;
  struct fs_stat stat_buf;
  struct fs_dirent *dirent;
  EFSDIR *dirp;
  char fs_path_buff[FS_PATH_MAX + 1];

  //char log_message[100];

  if (efs_stat (path, &stat_buf) != 0)
  {
    /* Unable to stat the path. Return an error */
    return -1;
  }

  if (!S_ISDIR (stat_buf.st_mode))
  {
    /* This is not a directory */
    if (efs_unlink (path) != 0)
    {
      /* Failed to delete the entity return fail */
      status = -1;
    }
    /* We are done here so return 0 */
    status = 0;
  }
  else
  {
    /* This is a directory */
    dirp = efs_opendir (path);
    if (!dirp)
    {
      /* Unable to open the directory */
      status = -1;
    }

    while ((dirent = efs_readdir (dirp)) != 0)
    {
      if ((dirent->d_name[0] == '.' && dirent->d_name[1] == 0)
         || (dirent->d_name[0] == '.' && dirent->d_name[1] == '.' && dirent->d_name[2] == 0))
      {
        continue;
      }

      /* Build child path */
      strlcpy (fs_path_buff, path, sizeof (fs_path_buff));
      strlcat (fs_path_buff, "/", sizeof (fs_path_buff));
      strlcat (fs_path_buff, dirent->d_name, sizeof (fs_path_buff));

      /* Recursion */
      boot_log_message(fs_path_buff);
      status = boot_delete_tree(fs_path_buff);
    }

    /* Empty. Delete this directory and return */
    if (efs_closedir (dirp) < 0)
    {
      status = -1;
    }
    if (efs_rmdir (path) < 0)
    {
      //qsnprintf(log_message, 100, "efs_rmdir %s, efs_errno=%d", path, efs_errno);
      //boot_log_message(log_message);
      status = -1;
    }
  }

  return status;
}

/**
 * Try to mount SD card for saving ramdump.
 *
 * @param delete_ramdump_if_no_space
 *    FALSE if delete nothing
 *    TRUE if delete any ramdumps
 *
 * @return
 *    0 on success, or negative error value on failure.
 */
static int boot_try_sd_card
(
  boot_boolean delete_ramdump_if_no_space     /* delete ramdump if no space left */
)
{
  struct fs_statvfs vfs_stat;
  fs_fsblkcnt_t full_ramdump_blocks;

  hotplug_poll_device(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EXTERNAL_DEVICES_ONLY);

  if (hotplug_has_device_discovered(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EXTERNAL_DEVICES_ONLY))
  {
    /* unmount /mmc1/ */
    efs_umount(SD_PATH);

    if (boot_hotplug_poll_and_mount_first_fat(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EXTERNAL_DEVICES_ONLY) == 0)
    {
      if (efs_statvfs(SD_PATH, &vfs_stat) == 0)
      {
        full_ramdump_blocks = FULL_RAMDUMP_SIZE / vfs_stat.f_bsize;
        if (vfs_stat.f_bfree < full_ramdump_blocks)
        {
          boot_log_message("No enough space in SD card");
          if (delete_ramdump_if_no_space)
          {
            //TODO free space & change return value
            boot_log_message("try to free space");
            //boot_delete_tree(/* SD_RAM_DUMP_PATH */ "/mmc1/ram_dump");
            return -1;
          }
          else
          {
            return -3;
          }
        }
      }
      else
      {
        boot_log_message("efs_statvfs error, why?");
        return -1;
      }
    }
    else
    {
      boot_log_message("Mount SD card failed");
      return -1;
    }
  }
  else
  {
    boot_log_message("No SD card insert");
    return -2;
  }

  boot_log_message("OK, SD card ready");

  return 0;
}

/**
 * Try to mount ramdump partition for saving ramdump.
 *
 * @param delete_ramdump_if_no_space
 *    FALSE if delete nothing
 *    TRUE if delete any ramdumps
 *
 * @return
 *    0 on success, or negative error value on failure.
 */
static int boot_try_ramdump_partition
(
  boot_boolean delete_ramdump_if_no_space     /* delete ramdump if no space left */
)
{
  static boot_boolean checked = FALSE;
  static boot_boolean partition_valid = FALSE;
  static boot_boolean no_space_left = FALSE;

  struct fs_statvfs vfs_stat;
  fs_fsblkcnt_t full_ramdump_blocks;

  /* check ramdump partition and ramdump firstly */
  if (!checked)
  {
    checked = TRUE;

    /* unmount /mmc1/ */
    efs_umount(SD_PATH);

    /* a mount-able ramdump partition */
    if (boot_hotplug_poll_and_mount_first_fat(HOTPLUG_TYPE_MMC,
                                              HOTPLUG_ITER_EMBEDDED_DEVICES_ONLY) == 0)
    {
      /* check free space */
      if (efs_statvfs(SD_PATH, &vfs_stat) == 0)
      {
        partition_valid = TRUE;

        full_ramdump_blocks = FULL_RAMDUMP_SIZE / vfs_stat.f_bsize;
        if (vfs_stat.f_bfree < full_ramdump_blocks)
        {
          no_space_left = TRUE;
          boot_log_message("No enough space in ramdump partition");
        }
      }
      else
      {
        /* why error? return -1 */
        return -1;
      }
    }
    else
    {
      boot_log_message("Ramdump partition invalid");
    }
  }

  /* ramdump partition invalid, return -2 */
  if (!partition_valid)
  {
    return -2;
  }

  /* would not delete ramdump & no space left, return -3 */
  if (!delete_ramdump_if_no_space && no_space_left)
  {
    return -3;
  }

  /* unmount /mmc1/ */
  efs_umount(SD_PATH);

  /* mount ramdump partition finally */
  boot_hotplug_poll_and_mount_first_fat(HOTPLUG_TYPE_MMC,
                                        HOTPLUG_ITER_EMBEDDED_DEVICES_ONLY);
  if (delete_ramdump_if_no_space && no_space_left)
  {
    boot_log_message("Delete ram_dump folder in ramdump partition");
    //TODO sync?
    boot_delete_tree(/* SD_RAM_DUMP_PATH */ "/mmc1/ram_dump");
  }

  boot_log_message("OK, ramdump partition ready");

  return 0;
}

/**
 * Prepare device for saving ramdump.
 *
 * @param ramdump_header
 *    Pointer to ramdump_header.
 *
 * @return
 *    0 on success, or negative error value on failure.
 */
static int boot_prepare_device
(
  struct boot_ramdump_header *ramdump_header    /* pointer to ramdump_header */
)
{
  int rc = -1, tmp;

  boot_log_message("boot_prepare_device, Start");
  boot_log_start_timer();

  switch (ramdump_header->location)
  {
  case LOCATION_INTERNAL_ONLY:
    rc = boot_try_ramdump_partition(FALSE);
    break;

  case LOCATION_EXTERNAL_ONLY:
    /* wait for some minutes */
    boot_wait_for_sd_card(ramdump_header->timeout * 60);
    rc = boot_try_sd_card(FALSE);
    break;

  case LOCATION_INTERNAL_FORCE:
    rc = boot_try_ramdump_partition(TRUE);
    break;

  case LOCATION_EXTERNAL_FORCE:
    /* wait for some minutes */
    boot_wait_for_sd_card(ramdump_header->timeout * 60);
    rc = boot_try_sd_card(TRUE);
    break;

  case LOCATION_INTERNAL_PREFER:
    rc = boot_try_ramdump_partition(FALSE);
    if (rc == -2 || rc == -1) /* ramdump partition invalid */
    {
      rc = boot_try_sd_card(FALSE);
    }
    else if (rc == -3) /* no space left */
    {
      rc = boot_try_sd_card(FALSE);
      if (rc != 0)
      {
        /* delete ramdump and try again */
        rc = boot_try_ramdump_partition(TRUE);
      }
    }
    break;

  case LOCATION_EXTERNAL_PREFER:
    rc = boot_try_sd_card(FALSE);
    if (rc == -2) /* no sd card */
    {
      rc = boot_try_ramdump_partition(FALSE);
    }
    else if (rc == -3) /* no space left */
    {
      rc = boot_try_ramdump_partition(FALSE);
      if (rc != 0)
      {
        /* delete ramdump and try again */
        rc = boot_try_sd_card(TRUE);
      }
    }
    break;

  case LOCATION_DEVICE_AUTO:
    rc = boot_try_sd_card(FALSE);
    if (rc == -1) /* sd card error */
    {
      /* force ramdump partition */
      rc = boot_try_ramdump_partition(TRUE);
    }
    else if (rc == -2) /* no sd card */
    {

      /**
       * This table shows return values and final device:
       *   R: Ramdump partition
       *   S: Sd card
       *   x: no device ready
       *
       * first column is 'tmp', return value of boot_try_ramdump_partition
       * first row is 'rc', return value of boot_try_sd_card after 5 mins
       *
       *      *  0 -1 -2 -3
       *   ****************
       *   -1 *  S  x  x  S
       *   -2 *  S  x  x  S
       *   -3 *  S  R  R  R
       */

      /* try ramdump partition firstly, save in 'tmp' */
      rc = boot_try_ramdump_partition(FALSE);
      tmp = rc;
      if (tmp != 0) /* invalid (-1, -2) or no space (-3) */
      {
        /* wait for some minutes */
        boot_wait_for_sd_card(ramdump_header->timeout * 60);

        /* try sd card */
        rc = boot_try_sd_card(FALSE);
        if (tmp == -3 && rc != 0) /* sd card failed but partition ok */
        {
          /* force ramdump partition */
          rc = boot_try_ramdump_partition(TRUE);
        }
        else if ((tmp == -2 || tmp == -1) && rc == -3) /* partition invalid & sd card no space */
        {
          /* force sd card*/
          rc = boot_try_sd_card(TRUE);
        }
      }
    }
    else if (rc == -3) /* no space left */
    {
      /* try ramdump partition */
      rc = boot_try_ramdump_partition(FALSE);
      if (rc == -2 || rc == -1) /* ramdump partition invalid */
      {
        /* force sd card */
        rc = boot_try_sd_card(TRUE);
      }
      else if (rc == -3) /* no space left */
      {
        /* so both no space left, force ramdump partition */
        rc = boot_try_ramdump_partition(TRUE);
      }
    }
    break;
  }

  boot_log_stop_timer("boot_prepare_device, Delta");

  return rc;
}

#endif /* FEATURE_TCTNB_DUMPSDCARD */

/*===========================================================================

**  Function :  boot_ramdump_reset

** ==========================================================================
*/
/*!
* 
* @brief :  This routine checks for the reset cookie file on sd card and resets
*  the target if its found. This is done to prevent device from continuing to
*  bootup in dload mode after saving ramdumps to SD card
*
* @param[in]
*  None
* 
* @par Dependencies:
*  Must be called after efs_boot_initialize.
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

#ifndef FEATURE_TCTNB_DUMPSDCARD
static void boot_ramdump_reset( void )
{
  int32 fs_handle = INVALID_FILE_HANDLE;
  
  uint32 str_size=0;

  str_size = strlcpy(cookiefilename, cookiefilepath, MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL);

  str_size = strlcat(cookiefilename, "rtcookie.txt", MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL );

  fs_handle = boot_efs_open(cookiefilename,O_RDONLY);

  /* Reset target if Cookie file exists.*/
  if(!(fs_handle < 0))
  {
    /*File exists. Close reset cookie file handle */
    boot_efs_close(fs_handle);

    /* If cache is on, some data might still be in the cache and not 
    written to memory. So flush the cache before causing a watchdog reset */
    mmu_flush_cache();

    /* reset device */
    boot_hw_reset(BOOT_HARD_RESET_TYPE);
  }
}
#endif


/*===========================================================================

**  Function :  boot_efs_write_file

** ==========================================================================
*/
/*!
* 
* @brief : Helper function to write certain size of data to a given file
*          boot_sd_rd_errdata.err_no will be set if there's an error
*
* @param[in]
*  None
* 
* @par Dependencies:
*  Must be called after efs_boot_initialize.
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static void boot_efs_write_file(int32 fs_handle, uint8 *buf_addr, uint32 size)
{
  int32 tbytes_written;
  while(size > 0)
  {
    boot_toggle_led();
    tbytes_written = boot_efs_write(fs_handle, buf_addr, size);
    if(tbytes_written < 0)
    {
      /* Break on any EFS write error */
      boot_sd_rd_errdata.err_no = boot_efs_errno;
      break;
    }
    size -= tbytes_written;
    buf_addr += tbytes_written;
  }
}


/*===========================================================================

**  Function :  boot_write_ram_dump_headers

** ==========================================================================
*/
/*!
* 
* @brief : Helper function to write a dump header and section header
*          to the beginning of a file 
*
* @param[in]
*  None
* 
* @par Dependencies:
*  Must be called after efs_boot_initialize.
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static void boot_write_ram_dump_headers(int32 dump_header_handle)
{
  /*seek to beginning of the file */
  boot_efs_lseek(dump_header_handle, 0, SEEK_SET);
  
  boot_efs_write_file(dump_header_handle,
                 (void *)&raw_dump_header, 
                 DUMP_HEADER_SIZE);
                 
  boot_efs_write_file(dump_header_handle, 
                 (void *)raw_dump_section_header_table, 
                 max_region_entries * SECTION_HEADER_SIZE);
}


/*===========================================================================

**  Function :  find_next_available_index

** ==========================================================================
*/
/*!
* 
* @brief  
*    This routine analyzes the integer incremented folder names in the root 
*   directory of the SD device, and returns the value of the next available
*   folder.
*
*   When no available indexes are left available, "0" is returned as an error
*
* @par Dependencies
*     SDCC Hw and EFS needs to be initialised 
*
* @retval
*   int32 next_avail_index
* 
* @par Side Effects
*   None
* 
*/
static uint32 find_next_available_index( void )
{
  uint32 next_avail_index = 0;
  uint32 i,str_size = 0;
  EFSDIR *dir_ptr = NULL; 
 
  for (i = 1; i <= MAXDUMPS; i++)
  {
    qsnprintf(foldernumber, MAX_SIZE_FOLDER_STRING, "%lu/", (uintnt)i);
    
    str_size = strlcpy(foldername, cookiefilepath, MAX_FILE_NAME);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 
  
    str_size = strlcat (foldername, foldernumber, MAX_FILE_NAME);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 
  
    /* Attempt to open integer digit named folder to determine 
       current presence.  Break's are placed such that if efs 
       error occurs, we stop attempting to create a folder, and 
       error value is returned. */
    dir_ptr = boot_efs_opendir (foldername);
        
    /* If the folder doesn't exist, create it */
    if (dir_ptr == NULL)
    {
      /* 0666:Provide read/write permissions for all users */
      if (boot_efs_mkdir(foldername, 0666) == 0)
      {
        next_avail_index = i;
      }
      break;
    }
    /* Else, if it already exists, close it and check the next */
    else 
    {
      if (boot_efs_closedir (dir_ptr) != 0)
      {
        break;
      }
    }
  }
  return next_avail_index;
}

/*===========================================================================

**  Function :  boot_process_sd_dumps

** ==========================================================================
*/
/*!
* 
* @brief  
*    This routine reads the memory regions and writes to the respective files
*   in the SD card.  It checks the SD card for space availability first, and 
*   without deleting any previous dumps, saves the new dumps under a new, 
*   numerically indexed and named folder.  
*
* @par Dependencies
*     SDCC Hw and EFS needs to be initialised 
*
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/

#ifdef FEATURE_TCTNB_DUMPSDCARD
static int boot_process_sd_dumps( void )
#else
static void boot_process_sd_dumps( void )
#endif

{
  int32  fs_handle =  INVALID_FILE_HANDLE;
  int32  errmax_fs_handle = INVALID_FILE_HANDLE;
  int32  err_fs_handle = INVALID_FILE_HANDLE;
  int32  sd_dump_header_handle = INVALID_FILE_HANDLE;
  
  uint32 memregion_index, error_info_size, length, str_size;
  uint64 baseaddr;
  uint32 cur_dump_index = 0;

  char * debug_filename = NULL;
  
  /*ensure string arrays delimited correctly */
  filename[(MAX_FILE_NAME-1)]='\0';
  errmax_filename[(MAX_FILE_NAME-1)]='\0';
  err_filename[(MAX_FILE_NAME-1)]='\0';
  foldername[(MAX_FILE_NAME-1)]='\0';
  cookiefilepath[(MAX_FILE_NAME-1)]='\0';
  
  /*Get number of entries in mem_debug_info table */
  max_region_entries = dload_mem_debug_num_ent();
  
  str_size = strlcpy (errmax_filename, cookiefilepath, MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 

  str_size = strlcat (errmax_filename, "errmax.txt", MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 
  
  /* Create Errmax log file ,if no errors occured this file would be deleted */
  /* 0666:Provide write/execute permissions for User,group and others */
  errmax_fs_handle = boot_efs_open(errmax_filename, O_WRONLY|O_CREAT|O_TRUNC,0666);
  
  cur_dump_index = find_next_available_index();

  /* Initialize the LED handling logic */
  boot_toggle_led_init();
  /* if folder could not be created, or maximum number of dumps has been met, just stop */
  if (cur_dump_index == 0)
  {
    boot_efs_close(errmax_fs_handle);

#ifdef FEATURE_TCTNB_DUMPSDCARD
    return -1;
#else
    return;
#endif

  }
  else
  {
    boot_efs_close(errmax_fs_handle);
    boot_efs_unlink(errmax_filename);
  }

  str_size = strlcpy(err_filename, foldername, MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 

  str_size = strlcat(err_filename, "errfile.txt", MAX_FILE_NAME);
  BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL ); 

  /* Create Error log file ,if no errors occured during ramdumps this file would be deleted */
  /* 0666:Provide write/execute permissions for User,group and others */
  err_fs_handle = boot_efs_open(err_filename, O_WRONLY | O_CREAT | O_TRUNC, 0666);
  if(err_fs_handle == INVALID_FILE_HANDLE)
  {

#ifdef FEATURE_TCTNB_DUMPSDCARD
    return -1;
#else
    return;
#endif

  }
  
  do
  {
    /*Open ram dump header file */
    qsnprintf(filename, MAX_FILE_NAME, "%s%s", foldername, SD_DUMP_HEADER_NAME);
    
    sd_dump_header_handle = boot_efs_open (filename, O_WRONLY|O_CREAT|O_TRUNC,0666);
    
    if(sd_dump_header_handle == INVALID_FILE_HANDLE)
    {
      /* log error file name */
      BL_VERIFY((qsnprintf(filename, MAX_FILE_NAME, "%s",SD_DUMP_HEADER_NAME) 
                < MAX_FILE_NAME), 
              BL_ERR_SBL);
      break;         
    }
    
    /* Initialize dump header */
    boot_ram_dump_header_init();
    
    /* write a fresh copy of the headers */
    boot_write_ram_dump_headers(sd_dump_header_handle);  
    if(boot_sd_rd_errdata.err_no != EFS_NO_ERR)
    {
      /* log error file name */
      BL_VERIFY((qsnprintf(filename, MAX_FILE_NAME, "%s",SD_DUMP_HEADER_NAME) 
                < MAX_FILE_NAME), 
              BL_ERR_SBL);
      break;
    }  
    /* Iterate over the mem_debug_info table and dump all the memory regions to sd card. */
    for(memregion_index = 0; memregion_index < max_region_entries; memregion_index++)
    {
      baseaddr = dload_mem_debug_mem_base( memregion_index );
      length = dload_mem_debug_mem_length( memregion_index );
      
      boot_sd_rd_errdata.err_no = EFS_NO_ERR;
      
      /* update section header info for this dump section */
      boot_update_section_header_table_by_index(memregion_index);
      /* write a fresh copy of the headers */
      boot_write_ram_dump_headers(sd_dump_header_handle);
      if(boot_sd_rd_errdata.err_no != EFS_NO_ERR)
      {
        /* log error file name */
         BL_VERIFY((qsnprintf(filename, MAX_FILE_NAME, "%s",SD_DUMP_HEADER_NAME) 
                  < MAX_FILE_NAME), 
                  BL_ERR_SBL);
        break;
      }
      
      str_size = strlcpy(filename, foldername, MAX_FILE_NAME);
      BL_VERIFY((str_size < MAX_FILE_NAME),
                BL_ERR_SBL ); 


      debug_filename = dload_mem_debug_filename(memregion_index);
      BL_VERIFY(debug_filename,
                BL_ERR_NULL_PTR);

      str_size = strlcat(filename, debug_filename, MAX_FILE_NAME);
      BL_VERIFY((str_size < MAX_FILE_NAME),
                BL_ERR_SBL ); 

      /* 0666:Provide write/execute permissions for User,group and others */
      fs_handle = boot_efs_open (filename, O_WRONLY|O_CREAT|O_TRUNC,0666);
      boot_sd_rd_errdata.err_no = EFS_NO_ERR;
      
      if(INVALID_FILE_HANDLE != fs_handle)
      {
        /* If the Base address of the Ram address is '0'  copy  512Bytes into local buffer 
          and pass the  local buffer to the EFS to write into SD card. EFS can't handle data 
          writes for Null Pointer */
        if(baseaddr == 0)
        {
          /* Copy the initial 512Bytes (SDCC Sector size)  in temporary buffer */
          qmemcpy(sector_buf, (void *)(uintnt)baseaddr, SECTOR_SIZE);
          
          /* Flush temp buffer to file  */
          boot_efs_write_file(fs_handle, sector_buf, SECTOR_SIZE);
          
          baseaddr += SECTOR_SIZE;
          length -=SECTOR_SIZE;
        }
        
        /* Dump the rest of this memory region data to same file */
        if(boot_sd_rd_errdata.err_no == EFS_NO_ERR)
        {
          boot_efs_write_file(fs_handle, (void *) (uintnt)(baseaddr), length);
        }
        
        if(boot_sd_rd_errdata.err_no != EFS_NO_ERR)
        {
          /*if there is a writte error, delete the partial file */
          boot_efs_close(fs_handle);
          boot_efs_unlink(filename);
          break;
        }
  
        /* Now current dump region has been written to card */
        /* update the validity flag of the section header */ 
        /* Update the actual size and section count we have dumped in overall header */
        boot_efs_close(fs_handle);  
        raw_dump_section_header_table[memregion_index].validity_flag |= RAM_DUMP_VALID_MASK;       
        raw_dump_header.dump_size += length;
        raw_dump_header.sections_count++;
        
        /* write a fresh copy of the headers */
        boot_write_ram_dump_headers(sd_dump_header_handle);
        if(boot_sd_rd_errdata.err_no != EFS_NO_ERR)
        {
          /* log error file name */
          BL_VERIFY((qsnprintf(filename, MAX_FILE_NAME, "%s",SD_DUMP_HEADER_NAME) 
                    < MAX_FILE_NAME), 
                    BL_ERR_SBL);  
          break;
        }
      } /* file handle valid */
      else
      {
        /*we can not open the dump file,terminate the dump */
        break;
      }  
    }
  }while(0);
  /*At the end of dump loop we check for error */
  if(boot_sd_rd_errdata.err_no != EFS_NO_ERR)
  {
    boot_enable_led(RAM_DUMP_LED, TRUE);
    /*generate error file name + newline */
    str_size = qsnprintf(boot_sd_rd_errdata.filename, MAX_FILE_NAME, "%s\n", filename);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL );
  
    error_info_size = sizeof(boot_sd_rd_errdata.err_no) + str_size;
    
    /* write the error number and error file name to error log */
    if(err_fs_handle != INVALID_FILE_HANDLE)
    {
      boot_efs_write_file(err_fs_handle, (void *)&boot_sd_rd_errdata, error_info_size);
      boot_efs_close(err_fs_handle);
    }
  }
  else
  {
    /* If no error occured during the Ramdump delete the error file */
    boot_enable_led(RAM_DUMP_LED, FALSE);
    boot_efs_close(err_fs_handle);
    boot_efs_unlink(err_filename);
    
    /* set dump header to valid */
    raw_dump_header.validity_flag |= RAM_DUMP_VALID_MASK;
    /* write a fresh copy of the headers */
    boot_write_ram_dump_headers(sd_dump_header_handle);        
  }
  
  /*close the header handle in the end */
  boot_efs_close(sd_dump_header_handle);

#ifdef FEATURE_TCTNB_DUMPSDCARD
  return 0;
#endif


} /* boot_process_sd_dumps */

/*===========================================================================

**  Function :  boot_ram_dump_check_rdcookie

** ==========================================================================
*/
/*!
*
* @brief
*   This routine checks whether file rdcookie.txt exists and records
* the directory path to cookiefilepath.
*
* @param[in]
*   path Pointer to the searching directory.
*
* @par Dependencies:
*
* @retval
*   int Handler for the rdcookie.txt file.
*
* @par Side Effects
*   Set static variable cookiefilepath.
*
*/

#ifndef FEATURE_TCTNB_DUMPSDCARD
static int boot_ram_dump_check_rdcookie(const char *path)
{
    uint32 str_size = 0;
    int fd;

    str_size = strlcpy(cookiefilepath, path, MAX_FILE_NAME);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL);

    str_size = strlcpy(cookiefilename, path, MAX_FILE_NAME);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL);

    str_size = strlcat(cookiefilename, "rdcookie.txt", MAX_FILE_NAME);
    BL_VERIFY((str_size < MAX_FILE_NAME), BL_ERR_SBL);

    fd = boot_efs_open(cookiefilename, O_RDONLY);

    if (fd >= 0)
    {
        boot_efs_close(fd);
    }

    return fd;
}
#endif

/*===========================================================================

**  Function :  boot_ram_dumps_to_sd_card

** ==========================================================================
*/
/*!
* 
* @brief :  This routine initiates the Ramdumps to SD card. Checks for the Ramdump 
* cookie file and go ahead with Ramdumps to SD card if cookie file is present.
* Setup the Ram regions to be taken as dumps.
*
* @param[in]
*   None
* 
* @par Dependencies:
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void boot_ram_dumps_to_sd_card(void)
{
#ifdef FEATURE_TCTNB_DUMPSDCARD
  uint32 str_size = 0;
  EFSDIR *dir_ptr = NULL;
  int efs_return_status=0;
#endif /* FEATURE_TCTNB_DUMPSDCARD */
//Add-Begin by TCTNB.lijiang:2016/03/01,defect-1692658
#ifdef FEATURE_TCTNB_DUMPSDCARD
  set_restart_reason(REASON_IGNORE);
#endif /* FEATURE_TCTNB_DUMPSDCARD */
//Add-end by TCTNB.lijiang:2016/03/01,defect-1692658
  /* dummy while loop which executes only once and used to break on error */
  do
  {
#ifndef FEATURE_TCTNB_DUMPSDCARD
    /* Poll the hotplug device */
    boot_hotplug_poll_and_mount_first_fat(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EXTERNAL_DEVICES_ONLY);
#else
    struct boot_ramdump_header ramdump_header;
    boot_get_ramdump_header(traceability_partition_id, TRACEABILITY_PARTITION_OFFSET, &ramdump_header);
    if (boot_prepare_device(&ramdump_header) != 0)
    {
      break;
    }
#endif /* FEATURE_TCTNB_DUMPSDCARD */

#ifdef FEATURE_TCTNB_DUMPSDCARD
      str_size = strlcpy(cookiefilepath, SD_RAM_DUMP_PATH, MAX_FILE_NAME);
      BL_VERIFY((str_size < MAX_FILE_NAME),BL_ERR_SBL );
      dir_ptr = efs_opendir (cookiefilepath);

      if (dir_ptr == NULL)
      {
        /* 0666:Provide-read/write permissions for User,group and others */
        efs_return_status=efs_mkdir (cookiefilepath, 0666);

        if(efs_return_status!=EFS_NO_ERR)
           break;
      }
      efs_closedir(dir_ptr);
#else /* !FEATURE_TCTNB_DUMPSDCARD */
    /* Return if cookie file doesn't exit.*/
    if((boot_ram_dump_check_rdcookie(SD_RAM_DUMP_PATH) < 0)  &&
       (boot_ram_dump_check_rdcookie(SD_PATH) < 0))
        break;
#endif /* FEATURE_TCTNB_DUMPSDCARD */

#ifdef FEATURE_DLOAD_MEM_DEBUG
    /*only perform memory debug operations when it's supported*/
    if(dload_mem_debug_supported())
    {
    /* Initialize the debug memory regions array */
      dload_mem_debug_init();
    }
	else
	{
		break;
	}
#endif


#ifdef FEATURE_TCTNB_DUMPSDCARD
    boot_log_message("begin to dump process!");
    if(boot_process_sd_dumps()!=0)
    {
      break;
    }
    mmu_flush_cache();
    /* reset device */
    boot_hw_reset(BOOT_HARD_RESET_TYPE);
#else
    /*Store the ramdumps to sd card without deletion*/
    boot_process_sd_dumps();

    /* reset target if reset cookie present */
    boot_ramdump_reset();
#endif

  } while(0);

    
  /* At the end of sd card ram dump, if we are in raw ram dump mode, reset the device
     so we don't enter sahara */
  if((boot_shared_imem_cookie_ptr != NULL) &&
     (boot_shared_imem_cookie_ptr->uefi_ram_dump_magic == BOOT_RAW_RAM_DUMP_MAGIC_NUM))
  {
      mmu_flush_cache();
      boot_hw_reset(BOOT_WARM_RESET_TYPE);
  }
  
}/* boot_ram_dumps_to_sd_card*/
