#/** @file Sec.inf
#  SEC - Reset vector code that jumps to and loads DXE core
#
#  
#  Copyright (c) 2010-2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
#  Qualcomm Technologies Proprietary and Confidential.
#
#  Portions copyright (c) 2008, Apple Inc. All rights reserved.
#
#  This program and the accompanying materials                          
#  are licensed and made available under the terms and conditions of the BSD License         
#  which accompanies this distribution.  The full text of the license may be found at        
#  http://opensource.org/licenses/bsd-license.php                                            
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,                     
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED. 
#**/

#==============================================================================
#                              EDIT HISTORY
#
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 04/23/15   vk      Print boot device
# 04/17/15   vk      Remove unused PCD
# 03/10/15   jb      Add PCD to control which EL to start in
# 01/22/15   jb      Add IMEM cookie PCDs
# 11/13/14   jb      Support skipping early cache init
# 10/30/14   vk      Support RAMPArtitionTableLib
# 10/10/14   vk      Fix EmbeddedFdBaseAddress
# 10/18/14   jb      Single region resource memory support
# 04/22/14   aus     Added PCD for ImemCookiesBase
# 04/10/14   niting  Enable warning as error
# 04/04/14   vk      Disable warning as error
# 04/01/14   cpaulo  Removed dependency on QcomTimerLibB
# 10/23/13   niting  Updated location of UefiInfoBlock
# 10/22/13   vk      Add ARM BuildOptions
# 10/22/13   vk      Add AARCH64
# 04/03/13   niting  Added offline crash dump support
# 03/19/13   vk      Add gEfiInfoBlkHobGuid
# 03/12/13   vk      Add Pccd for virtio support, if SMEM not ready
# 01/29/13   vk      Branch from 8974 for target independent sec
#
#==============================================================================

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = Sec
  FILE_GUID                      = 8AF09F13-44C5-96EC-1437-DD899CB5EE5D
  MODULE_TYPE                    = SEC
  VERSION_STRING                 = 1.0

[Sources.common]
  Sec.c
  Cache.c
  UefiPlatCfg.c
  UefiInfoBlk.c

[Sources.Arm]
  ReleaseInfo.asm | RVCT
  ModuleEntryPoint.asm | RVCT

[Sources.AARCH64]
  ReleaseInfo.S | GCC
  ModuleEntryPoint.S | GCC

  ReleaseInfo.S | RVCT
  ModuleEntryPoint.S | RVCT

[BuildOptions.AARCH64]
#  GCC:*_*_*_CC_FLAGS = -O0

[BuildOptions.ARM]
  RVCT:*_*_*_CC_FLAGS = --diag_error=warning
  RVCT:*_*_*_DLINK_FLAGS = "--first=Sec.lib(ReleaseInfo)"
  
[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  EmbeddedPkg/EmbeddedPkg.dec
  ArmPkg/ArmPkg.dec
  IntelFrameworkModulePkg/IntelFrameworkModulePkg.dec
  QcomPkg/QcomPkg.dec

[LibraryClasses]
  BaseLib
  DebugLib
  ArmLib
  IoLib
  ExtractGuidedSectionLib
  LzmaDecompressLib
  PeCoffGetEntryPointLib
  DebugAgentLib
  SerialPortLib
  QcomBaseLib
  FBPTLib
  ProcLib
  PrePiMemoryAllocationLib
  ParserLib
  ShLib
  UefiCfgLib
  PerformanceLib
  TargetInitLib
  OfflineCrashDumpLib
  RamPartitionTableLib
  BootConfigLib

[FeaturePcd]
  gEmbeddedTokenSpaceGuid.PcdCacheEnable
  gEmbeddedTokenSpaceGuid.PcdPrePiProduceMemoryTypeInformationHob
  gQcomTokenSpaceGuid.PcdSingleDDRSystemResource

[FixedPcd]
  gArmTokenSpaceGuid.PcdCpuVectorBaseAddress
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultBaudRate
  gEmbeddedTokenSpaceGuid.PcdPrePiStackBase
  gEmbeddedTokenSpaceGuid.PcdPrePiStackSize
  gEmbeddedTokenSpaceGuid.PcdFlashFvMainBase
  gEmbeddedTokenSpaceGuid.PcdFlashFvMainSize
  gQcomTokenSpaceGuid.PcdPreAllocatedMemorySize
  gQcomTokenSpaceGuid.PcdUefiMemPoolBaseOffset
  gQcomTokenSpaceGuid.PcdUefiMemPoolSize
  gQcomTokenSpaceGuid.PcdMaxMemRegions
  gQcomTokenSpaceGuid.PcdHLOSCrashCookieAddr
  gQcomTokenSpaceGuid.PcdKraitFrequencyMhz
  gQcomTokenSpaceGuid.PcdHLOSMemoryBaseOffset
  gQcomTokenSpaceGuid.PcdHLOSMemorySize
  gQcomTokenSpaceGuid.PcdEmbeddedFdBaseAddress
  gQcomTokenSpaceGuid.PcdUefiInfoBlockOffset
  gQcomTokenSpaceGuid.PcdIMemCookiesBase
  gQcomTokenSpaceGuid.PcdSkipEarlyCacheMaint

  gEmbeddedTokenSpaceGuid.PcdPrePiCpuMemorySize
  gEmbeddedTokenSpaceGuid.PcdPrePiCpuIoSize
  gQcomTokenSpaceGuid.PcdUefiDebugCookieOffset
  gQcomTokenSpaceGuid.PcdUefiDebugCookie
  gQcomTokenSpaceGuid.PcdSwitchToEL1
  gArmTokenSpaceGuid.PcdArmArchTimerFreqInHz

[Guids]
  gQcomMemoryCaptureGuid
  gEfiInfoBlkHobGuid
  gQcomMemoryCaptureValueGuid
  gQcomAbnormalResetOccurredValueGuid

