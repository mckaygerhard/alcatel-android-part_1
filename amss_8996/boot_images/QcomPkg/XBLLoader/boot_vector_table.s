//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                              boot_vector_table.s
//
// GENERAL DESCRIPTION
//   This file captures 64-bit vector table for ARMV8
//
// Copyright 2013 by Qualcomm Technologies, Incorporated.All Rights Reserved.
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                           EDIT HISTORY FOR FILE
//
// This section contains comments describing changes made to the module.
// Notice that changes are listed in reverse chronological order.
//
//
// when       who     what, where, why
// --------   ---     --------------------------------------------------------
// 12/08/13   dhaval  vector table creation for ARMV8 64-bit mode
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


//============================================================================
//
//                            MODULE INCLUDES
//
//============================================================================
#include "boot_target.h"

//============================================================================
//
//                             MODULE IMPORTS
//
//============================================================================

//////////// For now, symbols are branched to itself in first version, those will 
//////////// be implemented and imported in these files eventually.
//    Import the external symbols that are referenced in this module.
//    IMPORT sbl1_csp0_sync
//    IMPORT sbl1_csp0_irq
//    IMPORT sbl1_csp0_fiq
//    IMPORT sbl1_csp0_serror

//    IMPORT sbl1_cspx_sync
//    IMPORT sbl1_cspx_irq
//    IMPORT sbl1_cspx_fiq
//    IMPORT sbl1_cspx_serror

//    IMPORT sbl1_lel64_sync
//    IMPORT sbl1_lel64_irq 
//    IMPORT sbl1_lel64_fiq 
//    IMPORT sbl1_lel64_serror 

//    IMPORT sbl1_lel32_sync
//    IMPORT sbl1_lel32_irq 
//    IMPORT sbl1_lel32_fiq 
//    IMPORT sbl1_lel32_serror 
	
//============================================================================
//
//                             MODULE EXPORTS
//
//============================================================================

    // Export the sbl1_vector_table for VBAR configuration in initial boot 
	// assembly init routines.
    .global sbl1_vector_table
	
//============================================================================
//
//                             MODULE DATA AREA
//
//============================================================================

    .section SBL1_VECTOR_TABLE, "ax", %progbits
    .p2align 11

//for SBL, vector table is same for all ELs(EL3 and EL1)
sbl1_vector_table:
  //--------------------------------------
  // +0x180 SError/vSError	 	
  // +0x100 FIQ/vFIQ
  // +0x080 IRQ/vIRQ	
  // VBAR_ELn+0x000 Synchronous (at base)
  //--------Current EL with SP0-----------
  //--------------------------------------
sbl1_csp0_sync:
  b sbl1_csp0_sync

  .align 7
//sbl1_csp0_irq  b sbl1_csp0_irq
sbl1_csp0_irq:
  b sbl1_csp0_irq 

  .align 7
sbl1_csp0_fiq:
  b sbl1_csp0_fiq 

  .align 7
sbl1_csp0_serror:
  b sbl1_csp0_serror 

  //--------------------------------------
  // +0x380 SError/vSError	 	
  // +0x300 FIQ/vFIQ
  // +0x280 IRQ/vIRQ	
  // +0x200 Synchronous 
  //--------Current EL with SPx-----------
  //--------------------------------------
  .align 7
sbl1_cspx_sync:
  b sbl1_cspx_sync

  .align 7
sbl1_cspx_irq:
  b sbl1_cspx_irq

  .align 7
sbl1_cspx_fiq:
  b sbl1_cspx_fiq

  .align 7
sbl1_cspx_serror:
  b sbl1_cspx_serror

  //--------------------------------------
  // +0x580 SError/vSError	 	
  // +0x500 FIQ/vFIQ
  // +0x480 IRQ/vIRQ	
  // +0x400 Synchronous 
  //--------Lower EL using AArch64-----------
  //--------------------------------------
  .align 7
sbl1_lel64_sync:
  b sbl1_lel64_sync

  .align 7
sbl1_lel64_irq:
  b sbl1_lel64_irq

  .align 7
sbl1_lel64_fiq:
  b sbl1_lel64_fiq

  .align 7
sbl1_lel64_serror:
 b sbl1_lel64_serror

  //--------------------------------------
  // +0x780 SError/vSError	 	
  // +0x700 FIQ/vFIQ
  // +0x680 IRQ/vIRQ	
  // +0x600 Synchronous 
  //--------Lower EL using AArch32-----------
  //--------------------------------------
  .align 7
sbl1_lel32_sync:
  b sbl1_lel32_sync

  .align 7
sbl1_lel32_irq:
  b sbl1_lel32_irq

  .align 7
sbl1_lel32_fiq:
  b sbl1_lel32_fiq

  .align 7
sbl1_lel32_serror:
  b sbl1_lel32_serror

  .end
