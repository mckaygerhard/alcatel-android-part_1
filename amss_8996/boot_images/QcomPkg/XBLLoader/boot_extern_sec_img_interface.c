/*===========================================================================

                    BOOT EXTERN SECURE IMAGE AUTH DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external image authentication drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/15/15   kpa     Added boot_sec_img_auth_re_enable_debug.
09/12/14   plc     Add support for engineering certificates
05/05/14   wg      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/

#include "boot_extern_sec_img_interface.h"

/* TODO : security team to provide appropriate header */
extern void sec_img_auth_re_enable_debug(secboot_verified_info_type v_info);

/*===========================================================================
                           DEFINITIONS
===========================================================================*/


/*===========================================================================
                      FUNCTION DEFINITIONS
===========================================================================*/ 

/**
 * @brief This function authenticates image Metadata
 *      
 *
 * @param[in,out] sec_img_id           	SW Image ID
 *                sec_img_data          Pointer to ELF header         
 *                                     
 * 				  
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_verify_metadata(uint32 sec_img_id, 
							 const sec_img_auth_elf_info_type *sec_img_data,
							 sec_img_auth_whitelist_area_param_t *whitelist_area,
							 sec_img_auth_verified_info_s *v_info)
{
  return sec_img_auth_verify_metadata(sec_img_id, 
					 sec_img_data,
					 whitelist_area,
					 v_info);
}

/**
 * @brief This function authenticates an ELF images hash segments
 *      
 *
 * @param[in,out] sec_img_id              SW Image ID
 *                v_info                  Pointer to location for verifed info
 *                                   
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_hash_elf_segments(uint32 sec_img_id,
								  sec_img_auth_verified_info_s *v_info)
{
  return sec_img_auth_hash_elf_segments(sec_img_id, v_info);
}								  
								
								  
/**
 * @brief This function validates the ELF image
 *      
 *
 * @param[in,out] elf_hdr               Pointer to ELF header
 *                                    
 *                        
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_validate_elf(const void *elf_hdr)
{
  return sec_img_auth_validate_elf(elf_hdr);
}


/**
 * @brief This function checks if the image segment is valid
 *
 *
 * @param[in,out] format              File format
 *                entry               Pointer to hash segment to be checked
 *                     
 *
 * @return \c TRUE if the segment is valid, \c FALSE otherwise.
 *
 * @sideeffects  None
 *
 *
 */
boolean boot_sec_img_auth_is_valid_segment(uint32 format, const void *entry)
{
  return sec_img_auth_is_valid_segment(format, entry);
}


/**
 * @brief This function checks whether authentication is enabled
 *      
 *
 * @param[in,out] code_seg            Type of code segment descriptor
 *                *is_auth_enabled    Pointer to store auth_en flag. 
 *                                                           
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_is_auth_enabled(uint32 code_seg, 
																									uint32 *is_auth_enabled)
{
  return sec_img_auth_is_auth_enabled(code_seg, is_auth_enabled);
}

/**
 * @brief send pbl debug information to sec img auth library
 *
 * @param[in] bl_shared_data - pbl shared info
 */
void boot_sec_img_auth_init(bl_shared_data_type * bl_shared_data)
{
  sec_img_auth_error_type status = SEC_IMG_AUTH_INVALID_ARG;

  /* get the enable_debug flag of sbl cert */
  if( (NULL != bl_shared_data)
     && (NULL != bl_shared_data->sbl_shared_data) 
     && (NULL != bl_shared_data->sbl_shared_data->pbl_shared_data)
     && (NULL != bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data))
  {
    status = sec_img_auth_init( 
      (secboot_verified_info_type*)&(bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info));
    BL_VERIFY((SEC_IMG_AUTH_SUCCESS == status),BL_ERR_IMG_META_DATA_AUTH_FAIL);
    
  }
}

/**
 * @brief sec img auth library api to re-enable debug
 *
 * @param[in] secboot_verified_info_type - secboot info
 */
 void boot_sec_img_auth_re_enable_debug(secboot_verified_info_type v_info)
{
  sec_img_auth_re_enable_debug(v_info);
}
 