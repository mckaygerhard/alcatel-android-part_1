/*=============================================================================

                       Boot Shared Functions Producer

GENERAL DESCRIPTION
  This file contains definitions of functions for boot shared functions.

Copyright 2014 by QUALCOMM Technologies Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ------------------------------------------------------------
09/16/15   kpa     Added dcache_inval_region
08/24/15   ck      Added boot_dload_transition_pbl_forced_dload
08/01/15   kpa     Added dcache_flush_region
07/28/15   rp      Added Clock_SetBIMCSpeed
06/25/15   as      Added sbl1_get_shared_data and boot_sbl_qsee_interface_get_image_entry
04/23/15   kpa     Added pmic and hotplug apis
10/01/14   ck      Removed Hotplug functions
09/30/14   ck      Added boot_err_fatal
09/30/14   ck      Removing efs functions as EFS driver properly split
09/23/14   ck      Added boot extern crypto, efs, and seccfg interface functions
07/15/14   ck      Intial creation
=============================================================================*/


/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_shared_functions_producer.h"
#include "tct.h"

void
boot_shared_functions_register(void)
{
  boot_shared_functions_type * function_table = 
    (boot_shared_functions_type *)SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE;


  /* Assign each function pointer here.  When adding new functions simply
     add to this list. */

  function_table->boot_CeMLDeInit = &boot_CeMLDeInit;
  function_table->boot_CeMLHashDeInit = &boot_CeMLHashDeInit;
  function_table->boot_CeMLHashFinal = &boot_CeMLHashFinal;
  function_table->boot_CeMLHashInit = &boot_CeMLHashInit;
  function_table->boot_CeMLHashSetParam = &boot_CeMLHashSetParam;
  function_table->boot_CeMLHashUpdate = &boot_CeMLHashUpdate;
  function_table->boot_CeMLInit = &boot_CeMLInit;
  function_table->boot_clobber_clear_whitelist_table = &boot_clobber_clear_whitelist_table;
  function_table->boot_clobber_check_global_whitelist_range = &boot_clobber_check_global_whitelist_range;
  function_table->boot_dload_transition_pbl_forced_dload = &boot_dload_transition_pbl_forced_dload;
  function_table->boot_dload_set_cookie = &boot_dload_set_cookie;
  function_table->boot_enable_led = &boot_enable_led;
  function_table->boot_err_fatal = &boot_err_fatal;
  function_table->boot_error_handler = bl_err_if.error_handler;
  function_table->boot_flash_dev_if_get_singleton = &boot_flash_dev_if_get_singleton;
  function_table->boot_get_ddr_info = &boot_get_ddr_info;
  function_table->boot_hw_reset = &boot_hw_reset;
  function_table->boot_install_error_callback = bl_err_if.install_callback;
  function_table->boot_log_message = &boot_log_message;

/*[FEATURE]-Add-by TCTNB.(LWS), 2016/01/20, FR-1195511  [system]Dump method improvement . */
#ifdef FEATURE_TCTNB_DUMPSDCARD
  function_table->boot_log_get_meta_info = &boot_log_get_meta_info;
  function_table->boot_read_flash_partition = &boot_read_flash_partition;
  function_table->boot_log_start_timer = &boot_log_start_timer;
  function_table->boot_log_stop_timer = &boot_log_stop_timer;
#endif
/*[FEATURE]-Add-End-by TCTNB.(LWS), 2016/01/20, FR-1195511  */

  function_table->boot_pbl_is_auth_enabled = &boot_pbl_is_auth_enabled;
  function_table->boot_pm_dev_get_power_on_reason = &boot_pm_dev_get_power_on_reason;
  function_table->boot_qsee_is_memory_dump_allowed = &boot_qsee_is_memory_dump_allowed;
  function_table->boot_qsee_is_retail_unlocked = &boot_qsee_is_retail_unlocked;
  function_table->boot_qsee_zero_peripheral_memory = &boot_qsee_zero_peripheral_memory;
  function_table->boot_toggle_led = &boot_toggle_led;
  function_table->boot_toggle_led_init = &boot_toggle_led_init;
  function_table->boot_sbl_qsee_interface_get_image_entry = &boot_sbl_qsee_interface_get_image_entry;

  function_table->mmu_flush_cache = &mmu_flush_cache;

  function_table->sbl1_get_shared_data = &sbl1_get_shared_data;
  function_table->sbl_error_handler = &sbl_error_handler;
  function_table->sbl1_hw_get_reset_status = &sbl1_hw_get_reset_status;
  function_table->sbl1_load_ddr_training_data_from_partition = &sbl1_load_ddr_training_data_from_partition;
  function_table->sbl1_save_ddr_training_data_to_partition = &sbl1_save_ddr_training_data_to_partition;
  
  function_table->hotplug_get_partition_size_by_image_id = &hotplug_get_partition_size_by_image_id;
  function_table->dev_sdcc_write_bytes = &dev_sdcc_write_bytes;

  /*API's used by USB driver */
  function_table->pm_smbchg_usb_chgpth_irq_status = &pm_smbchg_usb_chgpth_irq_status;
  function_table->qusb_pbl_dload_check = &qusb_pbl_dload_check;
  
  function_table->Clock_SetBIMCSpeed = &Clock_SetBIMCSpeed;
  function_table->dcache_flush_region = &dcache_flush_region;
  function_table->dcache_inval_region = &dcache_inval_region;

  /* Fill in version number and magic cookie values so consumer knows table has
     been populated. */
  function_table->version = SHARED_FUNCTIONS_VERSION;
  function_table->magic_cookie_1 = SHARED_FUNCTIONS_MAGIC_COOKIE_1;
  function_table->magic_cookie_2 = SHARED_FUNCTIONS_MAGIC_COOKIE_2;
}

