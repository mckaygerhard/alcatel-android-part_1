/*=============================================================================

                       Boot Shared Functions Consumer

GENERAL DESCRIPTION
  This file contains definitions of functions for boot shared functions

Copyright 2014 by QUALCOMM Technologies Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ------------------------------------------------------------
09/16/15   kpa     Added dcache_inval_region
08/24/15   ck      Added boot_dload_transition_pbl_forced_dload
08/01/15   kpa     Added dcache_flush_region
07/28/15   rp      Added Clock_SetBIMCSpeed
07/11/15   rp	   Changed boot_enable_led function to add an additional parameter
06/25/15   as      Added sbl1_get_shared_data and boot_sbl_qsee_interface_get_image_entry
04/23/15   kpa     Added pmic and hotplug apis
10/01/14   ck      Removed Hotplug functions
09/30/14   ck      Added boot_err_fatal
09/30/14   ck      Removing efs functions as EFS driver properly split
09/23/14   ck      Added boot_extern_crypto_interface functions
09/23/14   ck      Added boot_extern_efs_interface functions
09/23/14   ck      Added boot_extern_seccfg_interface functions
08/05/14   ck      Updated boot_clobber_check_global_whitelist_range prototype
07/14/14   ck      Intial creation
=============================================================================*/


/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_shared_functions.h"
#include "tct.h"

/* Following prototypes were copied from boot_error_handler.c */
void boot_error_handler
(
  const char* filename_ptr,     /* File this function was called from -
                                   defined by __FILE__ */
  uint32      line,             /* Line number this function was called
                                   from - defined by  __LINE__ */
  uint32      err_code          /* Enum that describes error type */
);
static void boot_install_error_callback
(
  bl_error_callback_func_type,
  void *,
  bl_error_callback_node_type *node_ptr
);


/* Assign boot shared functions table to memory carved out in scatter load */
static boot_shared_functions_type * shared_functions_table =
  (boot_shared_functions_type *) SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE;


/* Boot error handler uses a global bl_err_if structure.  Create a new one
   here and assign with functions from the producer. */
boot_error_if_type bl_err_if = 
{
  boot_error_handler,
  boot_install_error_callback
};


boolean boot_shared_functions_init(void)
{
  boolean init_valid = FALSE;


  /* Verify that the shared functions table is the proper version and it was
     filled out by a producer. */
  if ((shared_functions_table->version == SHARED_FUNCTIONS_VERSION) &&
      (shared_functions_table->magic_cookie_1 == SHARED_FUNCTIONS_MAGIC_COOKIE_1) &&
      (shared_functions_table->magic_cookie_2 == SHARED_FUNCTIONS_MAGIC_COOKIE_2))
  {
    init_valid = TRUE;
  }
  
  return init_valid;
}


/* The following functions are the "shim" layer between the original function
   call and execution via function pointer. For each entry in the boot shared
   functions type structure there must be a shim. */

boot_flash_dev_if_type * boot_flash_dev_if_get_singleton(void)
{
  return shared_functions_table->boot_flash_dev_if_get_singleton();
}

void boot_error_handler(const char * filename_ptr,
                        uint32 line,
                        uint32 error)
{
  shared_functions_table->boot_error_handler(filename_ptr,
                                             line,
                                             error);
}


void boot_install_error_callback(bl_error_callback_func_type cb_func,
                                 void * data_ptr,
                                 bl_error_callback_node_type * node_ptr)
{
  shared_functions_table->boot_install_error_callback(cb_func,
                                                      data_ptr,
                                                      node_ptr);
}


boot_boolean boot_enable_led(uint32 led_color, boot_boolean led_on)
{
  return shared_functions_table->boot_enable_led(led_color, led_on);
}


void boot_hw_reset(boot_hw_reset_type reset_type)
{
  shared_functions_table->boot_hw_reset(reset_type);
}


boot_boolean boot_toggle_led(void)
{
  return shared_functions_table->boot_toggle_led();
}


void boot_toggle_led_init(void)
{
  shared_functions_table->boot_toggle_led_init();
}


void mmu_flush_cache(void)
{
  shared_functions_table->mmu_flush_cache();
}


void boot_log_message(char * message)
{
  shared_functions_table->boot_log_message(message);
}

/*[FEATURE]-Add-by TCTNB.(LWS), 2016/01/20, FR-1195511  [system]Dump method improvement . */
#ifdef FEATURE_TCTNB_DUMPSDCARD
void boot_log_start_timer(void)
{
  shared_functions_table->boot_log_start_timer();
}

uint32 boot_log_stop_timer(char * message)
{
  return shared_functions_table->boot_log_stop_timer(message);
}

struct boot_log_meta_info* boot_log_get_meta_info(void)
{
  return shared_functions_table->boot_log_get_meta_info();
}

void boot_read_flash_partition
(
  uint8 * partition_id,           /*partition id define in boot_gpt_partition_id.c*/
  void* buf,                      /*data buffer*/
  uint32 byte_offset,             /*offset in partition*/
  uint32 size                     /*size to read*/
)
{
  shared_functions_table->boot_read_flash_partition( partition_id , buf , byte_offset , size );
}
#endif
/*[FEATURE]-Add-End-by TCTNB.(LWS), 2016/01/20, FR-1195511 */

void boot_clobber_clear_whitelist_table(void)
{
  shared_functions_table->boot_clobber_clear_whitelist_table();
}


boolean boot_clobber_check_global_whitelist_range(const void * start_addr,
                                                  uintnt size)
{
  return shared_functions_table->
    boot_clobber_check_global_whitelist_range(start_addr,
                                              size);
}


void boot_dload_transition_pbl_forced_dload(void)
{
  shared_functions_table->boot_dload_transition_pbl_forced_dload();
}


void boot_dload_set_cookie(void)
{
  shared_functions_table->boot_dload_set_cookie();
}


boot_boolean boot_pbl_is_auth_enabled(void)
{
  return shared_functions_table->boot_pbl_is_auth_enabled();
}


bl_shared_data_type * sbl1_get_shared_data(void)
{
  return shared_functions_table->sbl1_get_shared_data();
}


void sbl_error_handler(void)
{
  shared_functions_table->sbl_error_handler();
}


sbl_if_shared_ddr_device_info_type * boot_get_ddr_info(void)
{
  return shared_functions_table->boot_get_ddr_info();
}


uint32 sbl1_hw_get_reset_status(void)
{
  return shared_functions_table->sbl1_hw_get_reset_status();
}


void sbl1_load_ddr_training_data_from_partition(uint8 * ddr_training_data_buf,
                                                uint32 ddr_training_data_size)
{
  shared_functions_table->
    sbl1_load_ddr_training_data_from_partition(ddr_training_data_buf,
                                               ddr_training_data_size);
}

void sbl1_save_ddr_training_data_to_partition
(
  uint8* ddr_training_data_ptr,
  uint32 ddr_training_data_size,
  uint32 ddr_training_data_offset
)
{
  shared_functions_table->sbl1_save_ddr_training_data_to_partition(ddr_training_data_ptr,
                                                                   ddr_training_data_size,
                                                                   ddr_training_data_offset);
}

pm_err_flag_type boot_pm_dev_get_power_on_reason(unsigned pmic_device_index,
                                                 uint64 * pwr_on_reason)
{
  return shared_functions_table->
    boot_pm_dev_get_power_on_reason(pmic_device_index,
                                    pwr_on_reason);
}


boot_boolean
boot_qsee_is_memory_dump_allowed(secboot_verified_info_type * secboot_info)
{
  return shared_functions_table->boot_qsee_is_memory_dump_allowed(secboot_info);
}


boot_boolean
boot_qsee_is_retail_unlocked(secboot_verified_info_type * secboot_info)
{
  return shared_functions_table->boot_qsee_is_retail_unlocked(secboot_info);
}


void boot_qsee_zero_peripheral_memory(void)
{
  shared_functions_table->boot_qsee_zero_peripheral_memory();
}


CeMLErrorType boot_CeMLDeInit(void)
{
  return shared_functions_table->boot_CeMLDeInit();
}


CeMLErrorType boot_CeMLHashDeInit(CeMLCntxHandle ** _h)
{
  return shared_functions_table->boot_CeMLHashDeInit(_h);
}


CeMLErrorType boot_CeMLHashFinal(CeMLCntxHandle * _h,
                                 CEMLIovecListType * ioVecOut)
{
  return shared_functions_table->boot_CeMLHashFinal(_h,
                                                    ioVecOut);
} 


CeMLErrorType boot_CeMLHashInit(CeMLCntxHandle ** _h,
                                CeMLHashAlgoType pAlgo)
{
  return shared_functions_table->boot_CeMLHashInit(_h,
                                                   pAlgo);
}


CeMLErrorType boot_CeMLHashSetParam(CeMLCntxHandle * _h,
                                    CeMLHashParamType nParamID, 
                                    const void * pParam, 
                                    uint32 cParam,
                                    CeMLHashAlgoType pAlgo)
{
  return shared_functions_table->boot_CeMLHashSetParam(_h,
                                                       nParamID,
                                                       pParam,
                                                       cParam,
                                                       pAlgo);
}


CeMLErrorType boot_CeMLHashUpdate(CeMLCntxHandle * _h,
                                  CEMLIovecListType ioVecIn)
{
  return shared_functions_table->boot_CeMLHashUpdate(_h,
                                                     ioVecIn);
}


CeMLErrorType boot_CeMLInit(void)
{
  return shared_functions_table->boot_CeMLInit();
}


void boot_err_fatal(void)
{
  shared_functions_table->boot_err_fatal();
}

uint64 hotplug_get_partition_size_by_image_id(image_type image_id)
{
  return shared_functions_table->hotplug_get_partition_size_by_image_id(image_id);
}

boolean dev_sdcc_write_bytes(void * ram_addr,
                             uint64 dst_offset,
                             uint32 bytes_to_write,
                             image_type image_id)
{
  return shared_functions_table->dev_sdcc_write_bytes(ram_addr,
                                                      dst_offset,
                                                      bytes_to_write,
                                                      image_id);
}

  /*API's used by USB driver */

pm_err_flag_type pm_smbchg_usb_chgpth_irq_status(uint32 device_index, 
                                                  pm_smbchg_usb_chgpth_irq_type irq, 
                                                  pm_irq_status_type type, 
                                                  boolean *status )
{
  return shared_functions_table->pm_smbchg_usb_chgpth_irq_status(device_index,
                                                                 irq,
                                                                 type,
                                                                 status);
}

boolean qusb_pbl_dload_check(void)
{
  return shared_functions_table->qusb_pbl_dload_check();
}

boolean Clock_SetBIMCSpeed(uint32 nFreqKHz )
{
  return shared_functions_table->Clock_SetBIMCSpeed(nFreqKHz);
}

int32 boot_sbl_qsee_interface_get_image_entry(boot_sbl_qsee_interface * sbl_qsee_interface,
                                              secboot_sw_type image_id)
{
  return shared_functions_table->boot_sbl_qsee_interface_get_image_entry(sbl_qsee_interface,
                                                                         image_id);
}


void dcache_flush_region(void *addr, unsigned int length)
{
  shared_functions_table->dcache_flush_region(addr, length);
}

void dcache_inval_region(void *addr, unsigned int length)
{
  shared_functions_table->dcache_inval_region(addr, length);
}
