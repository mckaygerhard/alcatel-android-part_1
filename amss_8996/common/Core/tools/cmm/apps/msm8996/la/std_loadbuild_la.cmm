//============================================================================
//  Name:                                                                     
//    std_loadbuild_la.cmm 
//
//  Description:                                                              
//    LA MSM HLOS Specific Build loading script. Target specific. Meant to be run under std_loadbuild
//                                                                            
//  Copyright (c) 2013 - 2016 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 03/09/2016 JBILLING      Minor typo fix in load image section
// 02/02/2016 JBILLING      Automation/Logging updates
// 03/10/2015 AJCheriyan    Added workaround for UFS erase
// 01/26/2015 JBILLING      Changed image names for 8996. removed SDI
// 06/24/2014 AJCheriyan    Added pmic.mbn loading support
// 05/26/2014 AJCheriyan    Added single image loading option
// 12/05/2013 AJCheriyan    Added erase option back in for UFS
// 11/05/2013 AJCheriyan    Added changes to pass storage type to loading script
// 10/02/2013 AJCheriyan    New set of changes for UFS loading
// 10/01/2012 AJCheriyan    Removed SDI image loading for good (needs boot support)
// 09/13/2012 AJCheriyan    Added SDI image loading (add partition.xml changes)
// 09/07/2012 AJCheriyan    Removed SDI image loading (revert partition.xml changes )
// 09/02/2012 AJCheriyan    Added SDI image loading
// 08/08/2012 AJCheriyan    Fixed issue with paths for mjsdload
// 07/19/2012 AJCheriyan    Created for B-family 


LOCAL &ArgumentLine &rvalue &SUBROUTINE
ENTRY %LINE &ArgumentLine
    
    &SUBROUTINE="MAIN" 
    AREA.SELECT
    
    GOSUB &SUBROUTINE &ArgumentLine
    ENTRY %LINE &rvalue 

    GOSUB EXIT &rvalue


////////////////////////////////////////////////////////////////
//
//            MAIN
//            Main std_loadbuild_le logic
//            Expected input: 
//                loadoption - Load option - Supported : ERASEONLY, LOADCOMMON, LOADFULL, LOADIMG
//                loadimages - Valid image name. Can be used only with LOADIMG option.
//                extraoptions - additional settings
//                  
//
////////////////////////////////////////////////////////////////
MAIN:
    LOCAL &loadoption &loadimages &extraoptions
    ENTRY &loadoption &loadimages &extraoptions

    LOCAL &CWD &SearchPaths &StorageOption &StorageType &Programmer &Partition
    LOCAL &XML &MaxPartitions &XML_Location &Binary &BinaryPath &Programmer_NAND &Programmer_ERASE

    // We have checked for all the intercom sessions at this point and we don't need any error checking
    

    // Switch to the tools directory
    &CWD=OS.PWD()

    // Check for the boot option
    do hwio 
    
    
    do std_utils HWIO_INF BOOT_CONFIG FAST_BOOT
    ENTRY &StorageOption
    
    
    IF (&StorageOption==0x4) //0x4 UFS
    (
    
        IF (OS.DIR("&BOOT_BUILDROOT/QcomPkg"))
        (
            &Programmer="QcomPkg/Tools/storage/UFS/jtagprogrammer"
        )
        ELSE
        (
            &Programmer="boot_images/QcomPkg/Tools/storage/UFS/jtagprogrammer"
        )
        &XML_Location="&METASCRIPTSDIR/../../../build/ufs"
        &StorageType="ufs"
        &MaxPartitions=6

    )
    ELSE //else EMMC 
    (
    
        IF (OS.DIR("&BOOT_BUILDROOT/QcomPkg"))
        (
            &Programmer="QcomPkg/Tools/storage/eMMC/jtagprogrammer"
        )
        ELSE
        (
            &Programmer="boot_images/QcomPkg/Tools/storage/eMMC/jtagprogrammer"
        )
        
        &XML_Location="&METASCRIPTSDIR/../../../build/emmc"
        &StorageType="emmc"
        &MaxPartitions=1
    )

    // Erase only
    IF (("&loadoption"=="ERASEONLY")||("&loadoption"=="LOADCOMMON")||("&loadoption"=="LOADFULL"))
    (
        //only erase UFS if specifically asked for
        IF ("&StorageType"=="emmc")||("&StorageType"=="ufs")
        (  
            // Only erase the chip and exit
            CD.DO &BOOT_BUILDROOT/&Programmer ERASE
        )
    )

    // Load common images
    IF (("&loadoption"=="LOADCOMMON")||("&loadoption"=="LOADFULL"))
    (
        // Check for all the common images 

        // Check for the presence of all the binaries
        // Not needed because meta-build should have populated all this information
        // SBL, TZ, HYP, RPM, APPSBL
        do std_utils FILEXIST DOFATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
        do std_utils FILEXIST DOFATALEXIT &BOOT_BUILDROOT/&PMIC_BINARY
        do std_utils FILEXIST DOFATALEXIT &RPM_BUILDROOT/&RPM_BINARY
        do std_utils FILEXIST DOFATALEXIT &APPS_BUILDROOT/&APPSBOOT_BINARY
        do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&TZ_BINARY
        do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&HYP_BINARY
        do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&TZDEVCFG_BINARY


        // Now flash them all one by one 
        // Flash the partition table
        &SearchPaths="&XML_Location"
        &Partition=0
        WHILE (&Partition<&MaxPartitions)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &Partition)+".xml"
            &FILES="gpt_main"+FORMAT.DECIMAL(1, &Partition)+".bin,"+"gpt_backup"+FORMAT.DECIMAL(1,&Partition)+".bin"
            CD.DO &BOOT_BUILDROOT/&Programmer LOAD searchpaths=&SearchPaths xml=&XML files=&FILES
            &Partition=&Partition+1
        )

    
        // Flash xbl.elf, pmic.elf, tz/hyp/mon, rpm, emmc_appsboot
        &SearchPaths="&XML_Location,"+OS.FILE.PATH(&BOOT_BUILDROOT/&BOOT_BINARY)+","+OS.FILE.PATH(&TZ_BUILDROOT/&TZ_BINARY)+","+OS.FILE.PATH(&RPM_BUILDROOT/&RPM_BINARY)+","+OS.FILE.PATH(&BOOT_BUILDROOT/&PMIC_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&APPSBOOT_BINARY)

        &METASEARCHPATH="&METASCRIPTSDIR/../../../config"+","+"&XML_Location"

        &Partition=0
        WHILE (&Partition<&MaxPartitions)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &Partition)+".xml"
            
            CD.DO &BOOT_BUILDROOT/&Programmer LOAD searchpaths=&SearchPaths xml=&XML files=xbl.elf,tz.mbn,devcfg.mbn,emmc_appsboot.mbn,hyp.mbn,rpm.mbn,pmic.elf,keymaster.mbn,cmnlib.mbn,cmnlib64.mbn
            // Loading Boot partition with zeroed binary to stop after JTAG. This is workaround for UFS flashing issue. To be removed later.
            CD.DO &BOOT_BUILDROOT/&Programmer LOAD searchpaths=&METASEARCHPATH xml=&XML files=boot.img
            &Partition=&Partition+1
        )

        // Apply the disk patches
        &SearchPaths="&XML_Location"
        &Partition=0
        WHILE (&Partition<&MaxPartitions)
        (
            &XML="patch"+FORMAT.DECIMAL(1, &Partition)+".xml"
            CD.DO &BOOT_BUILDROOT/&Programmer PATCH searchpaths=&SearchPaths xml=&XML
            &Partition=&Partition+1
        )

    )

        // Load common images
    IF ("&loadoption"=="LOADIMG")
    (
        // Check for the binary first 
        IF ("&loadimages"=="xbl")
        (
            do std_utils FILEXIST DOFATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
            do std_utils FILEXIST DOFATALEXIT &BOOT_BUILDROOT/&PMIC_BINARY
            &Binary="xbl.elf,pmic.elf"
            &BinaryPath=OS.FILE.PATH("&BOOT_BUILDROOT/&BOOT_BINARY")+","+OS.FILE.PATH(&BOOT_BUILDROOT/&PMIC_BINARY)
        )
        IF ("&loadimages"=="tz")
        (
            do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&TZ_BINARY
            do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&HYP_BINARY
            do std_utils FILEXIST DOFATALEXIT &TZ_BUILDROOT/&TZDEVCFG_BINARY
            &Binary="tz.mbn,hyp.mbn,devcfg.mbn,keymaster.mbn,cmnlib.mbn,cmnlib64.mbn"
            &BinaryPath=OS.FILE.PATH("&TZ_BUILDROOT/&TZ_BINARY")+","+OS.FILE.PATH("&TZ_BUILDROOT/&HYP_BINARY")+","+OS.FILE.PATH("&TZ_BUILDROOT/&TZDEVCFG_BINARY")
        )
        IF ("&loadimages"=="rpm")
        (
            do std_utils FILEXIST DOFATALEXIT &RPM_BUILDROOT/&RPM_BINARY
            &Binary="rpm.mbn"
            &BinaryPath=OS.FILE.PATH("&RPM_BUILDROOT/&RPM_BINARY")
        )
        
        IF ("&loadimages"=="appsboot")
        (
            do std_utils FILEXIST DOFATALEXIT &APPS_BUILDROOT/&APPSBOOT_BINARY
            &Binary="emmc_appsboot.mbn"
            &BinaryPath=OS.FILE.PATH("&APPS_BUILDROOT/&APPSBOOT_BINARY")
        )

        // Flash the image now
        &SearchPaths="&XML_Location,"+"&BinaryPath"
        &Partition=0
        WHILE (&Partition<&MaxPartitions)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &Partition)+".xml"
            CD.DO &BOOT_BUILDROOT/&Programmer LOAD searchpaths=&SearchPaths xml=&XML files=&Binary
            &Partition=&Partition+1
        )
    )

    // Load HLOS images
    IF ("&loadoption"=="LOADFULL")
    (

        // Change the active partition. This is needed only if the user flashes an HLOS that needs
        // a different partition
        // Set the active boot partition based on the type of the device
        IF (&StorageOption!=0x4)
        (
            CD.DO &BOOT_BUILDROOT/&Programmer 9 activeboot=0
        )
        ELSE
        (
            CD.DO &BOOT_BUILDROOT/&Programmer 9 activeboot=1
        )
        

        // Call the script to fastboot the remaining images
        OS.COMMAND cmd /k python &METASCRIPTSDIR/../../../build/fastboot_all.py --ap=&APPS_BUILDROOT --pf=&PRODUCT_FLAVOR --st=&StorageType
    )
    
    
    // Return to the old directory
    CD &CWD
    
    
    //FIXME - get programmer scripts to return success or failure arguments
    &result="SUCCESS"
    RETURN &result


EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue
    

//Should never get here. 
FATALEXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    IF STRING.SCAN("&FAILUREKEYWORD","FAILUREKEYWORD",0)==-1
    (
        GOSUB EXIT &FAILUREKEYWORD - &rvalue
    )
    ELSE
    (
        GOSUB EXIT &rvalue
    )
    
//Should never get here
    END



    
    
    

        

