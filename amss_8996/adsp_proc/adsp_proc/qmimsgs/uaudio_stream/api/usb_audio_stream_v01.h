#ifndef UAUDIO_STREAM_SERVICE_01_H
#define UAUDIO_STREAM_SERVICE_01_H
/**
  @file usb_audio_stream_v01.h

  @brief This is the public header file which defines the uaudio_stream service Data structures.

  This header file defines the types and structures that were defined in
  uaudio_stream. It contains the constant values defined, enums, structures,
  messages, and service message IDs (in that order) Structures that were
  defined in the IDL as messages contain mandatory elements, optional
  elements, a combination of mandatory and optional elements (mandatory
  always come before optionals in the structure), or nothing (null message)

  An optional element in a message is preceded by a uint8_t value that must be
  set to true if the element is going to be included. When decoding a received
  message, the uint8_t values will be set to true or false by the decode
  routine, and should be checked before accessing the values that they
  correspond to.

  Variable sized arrays are defined as static sized arrays with an unsigned
  integer (32 bit) preceding it that must be set to the number of elements
  in the array that are valid. For Example:

  uint32_t test_opaque_len;
  uint8_t test_opaque[16];

  If only 4 elements are added to test_opaque[] then test_opaque_len must be
  set to 4 before sending the message.  When decoding, the _len value is set
  by the decode routine and should be checked so that the correct number of
  elements in the array will be accessed.

*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2016 Qualcomm Technologies, Inc.
 All rights reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.



  $Header: //components/rel/qmimsgs.adsp/2.6/uaudio_stream/api/usb_audio_stream_v01.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.14.7 
   It was generated on: Mon Jul 18 2016 (Spin 0)
   From IDL File: usb_audio_stream_v01.idl */

/** @defgroup uaudio_stream_qmi_consts Constant values defined in the IDL */
/** @defgroup uaudio_stream_qmi_msg_ids Constant values for QMI message IDs */
/** @defgroup uaudio_stream_qmi_enums Enumerated types used in QMI messages */
/** @defgroup uaudio_stream_qmi_messages Structures sent as QMI messages */
/** @defgroup uaudio_stream_qmi_aggregates Aggregate types used in QMI messages */
/** @defgroup uaudio_stream_qmi_accessor Accessor for QMI service object */
/** @defgroup uaudio_stream_qmi_version Constant values for versioning information */

#include <stdint.h>
#include "qmi_idl_lib.h"
#include "common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup uaudio_stream_qmi_version
    @{
  */
/** Major Version Number of the IDL used to generate this file */
#define UAUDIO_STREAM_V01_IDL_MAJOR_VERS 0x01
/** Revision Number of the IDL used to generate this file */
#define UAUDIO_STREAM_V01_IDL_MINOR_VERS 0x01
/** Major Version Number of the qmi_idl_compiler used to generate this file */
#define UAUDIO_STREAM_V01_IDL_TOOL_VERS 0x06
/** Maximum Defined Message ID */
#define UAUDIO_STREAM_V01_MAX_MESSAGE_ID 0x0001
/**
    @}
  */


/** @addtogroup uaudio_stream_qmi_consts
    @{
  */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_aggregates
    @{
  */
typedef struct {

  /*  IOMMU virtual address assigned by AP USB driver. */
  uint64_t va;

  /*  IOMMU physical address */
  uint64_t pa;

  /*  Size of the memory */
  uint32_t size;
}mem_info_v01;  /* Type */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_aggregates
    @{
  */
typedef struct {

  /*  Memory information of:
 Secondary event ring */
  mem_info_v01 evt_ring;

  /*  Audio stream data endpoint transfer ring */
  mem_info_v01 tr_data;

  /*  Audio stream sync endpoint transfer ring */
  mem_info_v01 tr_sync;

  /*  Audio stream data and sync transfer request buffer */
  mem_info_v01 xfer_buff;

  /*  USB device context base address */
  mem_info_v01 dcba;
}apps_mem_info_v01;  /* Type */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_aggregates
    @{
  */
/**  USB endpoint descriptor structure
 Defined in Universal Serial Bus Specification Revision 2.0
 Table 9-13. Standard Endpoint Descriptor
 Implementation here as per USB Audio Class Specification 1.0
 Table 4-20: Standard AS Isochronous Audio Data Endpoint Descriptor
 */
typedef struct {

  /*  Size of the descriptor in bytes */
  uint8_t bLength;

  /*  Endpoint descriptor type */
  uint8_t bDescriptorType;

  /*  The address of the endpoint on the USB device */
  uint8_t bEndpointAddress;

  /*  This field describes the endpointís attributes */
  uint8_t bmAttributes;

  /*  Maximum packet size this endpoint is capable of 
 sending or receiving */
  uint16_t wMaxPacketSize;

  /*  Interval for polling endpoint for data transfers
 Expressed in frames or microframes based on speed of operation */
  uint8_t bInterval;

  /*  This field indicates the rate at which an isochronous pipe
 provides new synchronization feedback data */
  uint8_t bRefresh;

  /*  The address of the endpoint used to communicate 
 synchronization information */
  uint8_t bSynchAddress;
}usb_endpoint_descriptor_v01;  /* Type */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_aggregates
    @{
  */
/**  USB interface descriptor structure
 As defined in Universal Serial Bus Specification Revision 2.0
 Table 9-12. Standard Interface Descriptor
 */
typedef struct {

  /*  Size of the descriptor in bytes */
  uint8_t bLength;

  /*  Interface descriptor type */
  uint8_t bDescriptorType;

  /*  Number of this interface. */
  uint8_t bInterfaceNumber;

  /*  Value used to select this alternate setting 
 for the interface identified. */
  uint8_t bAlternateSetting;

  /*  Number of endpoints used by this interface */
  uint8_t bNumEndpoints;

  /*  Class code (assigned by the USB-IF) */
  uint8_t bInterfaceClass;

  /*  Subclass code (assigned by the USB-IF) */
  uint8_t bInterfaceSubClass;

  /*  Protocol code (assigned by the USB) */
  uint8_t bInterfaceProtocol;

  /*  Index of string descriptor describing this interface */
  uint8_t iInterface;
}usb_interface_descriptor_v01;  /* Type */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_enums
    @{
  */
typedef enum {
  USB_AUDIO_STREAM_STATUS_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  USB_AUDIO_STREAM_REQ_SUCCESS_V01 = 0, /**<  Audio stream enable or disable request successful  */
  USB_AUDIO_STREAM_REQ_FAILURE_V01 = 1, /**<  Audio stream enable or disable request failed for unknown reason  */
  USB_AUDIO_STREAM_REQ_FAILURE_NOT_FOUND_V01 = 2, /**<  APPS side could not find audio stream requested  */
  USB_AUDIO_STREAM_REQ_FAILURE_INVALID_PARAM_V01 = 3, /**<  Invalid  audio stream request parameter  */
  USB_AUDIO_STREAM_REQ_FAILURE_MEMALLOC_V01 = 4, /**<  Failed to allocate memory for audio stream  */
  USB_AUDIO_STREAM_STATUS_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}usb_audio_stream_status_enum_v01;
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_enums
    @{
  */
typedef enum {
  USB_AUDIO_DEVICE_INDICATION_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  USB_AUDIO_DEV_CONNECT_V01 = 0, /**<  USB audio device connect indication  */
  USB_AUDIO_DEV_DISCONNECT_V01 = 1, /**<  USB audio device disconnect indication  */
  USB_AUDIO_DEV_SUSPEND_V01 = 2, /**<  USB audio device suspend indication  */
  USB_AUDIO_DEV_RESUME_V01 = 3, /**<  USB audio device resume indication  */
  USB_AUDIO_DEVICE_INDICATION_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}usb_audio_device_indication_enum_v01;
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_messages
    @{
  */
/** Request Message; This command returns information of enumerated USB audio device. */
typedef struct {

  /* Mandatory */
  /*  Indication to enable selected audio stream */
  uint8_t enable;

  /* Mandatory */
  /*  Platform agnostic unique USB stream identifier */
  uint32_t usb_token;

  /* Optional */
  /*  Audio configuration parameters */
  uint8_t audio_format_valid;  /**< Must be set to true if audio_format is being passed */
  uint32_t audio_format;

  /* Optional */
  uint8_t number_of_ch_valid;  /**< Must be set to true if number_of_ch is being passed */
  uint32_t number_of_ch;

  /* Optional */
  uint8_t bit_rate_valid;  /**< Must be set to true if bit_rate is being passed */
  uint32_t bit_rate;

  /* Optional */
  /*  Size of transfer buffer requested */
  uint8_t xfer_buff_size_valid;  /**< Must be set to true if xfer_buff_size is being passed */
  uint32_t xfer_buff_size;
}qmi_uaudio_stream_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_messages
    @{
  */
/** Response Message; This command returns information of enumerated USB audio device. */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  qmi_response_type_v01 resp;
  /**<   Standard response type.*/

  /* Optional */
  /*  Status in response to audio stream request */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  usb_audio_stream_status_enum_v01 status;

  /* Optional */
  /*  Internal status reported by apps side. */
  uint8_t internal_status_valid;  /**< Must be set to true if internal_status is being passed */
  uint32_t internal_status;

  /* Optional */
  /*  Platform agnostic unique USB stream identifier */
  uint8_t slot_id_valid;  /**< Must be set to true if slot_id is being passed */
  uint32_t slot_id;

  /* Optional */
  /*  USB Interface Descriptor  */
  uint8_t usb_token_valid;  /**< Must be set to true if usb_token is being passed */
  uint32_t usb_token;

  /* Optional */
  /*  USB host controller allocated index of device slot
 Defined in extensible host controller interface (XHCI) specification v1.0. */
  uint8_t std_as_opr_intf_desc_valid;  /**< Must be set to true if std_as_opr_intf_desc is being passed */
  usb_interface_descriptor_v01 std_as_opr_intf_desc;

  /* Optional */
  /*  USB Endpoint Descriptor - Data */
  uint8_t std_as_data_ep_desc_valid;  /**< Must be set to true if std_as_data_ep_desc is being passed */
  usb_endpoint_descriptor_v01 std_as_data_ep_desc;

  /* Optional */
  /*  USB Endpoint Descriptor - Sync */
  uint8_t std_as_sync_ep_desc_valid;  /**< Must be set to true if std_as_sync_ep_desc is being passed */
  usb_endpoint_descriptor_v01 std_as_sync_ep_desc;

  /* Optional */
  /*  Revision of USB audio class specification in binary coded decimal
 Represents bcdADC field from USB Class-Specific AC Interface Descriptor */
  uint8_t usb_audio_spec_revision_valid;  /**< Must be set to true if usb_audio_spec_revision is being passed */
  uint16_t usb_audio_spec_revision;

  /* Optional */
  /*  Delay introduced in audio data stream due to internal processing of signal
 within the audio function expressed in number of frames or micro-frames
 Represents bDelay field from USB Class-Specific AS Interface Descriptor */
  uint8_t data_path_delay_valid;  /**< Must be set to true if data_path_delay is being passed */
  uint8_t data_path_delay;

  /* Optional */
  /*  The number of bytes occupied by one audio subslot or subframe
 This field is same as bSubSlotSize or bSubFrameSize defined in 
 USB Audio Class Formats Specification 1.0 or 2.0 - Type I Format Type Descriptor */
  uint8_t usb_audio_subslot_size_valid;  /**< Must be set to true if usb_audio_subslot_size is being passed */
  uint8_t usb_audio_subslot_size;

  /* Optional */
  /*  APPS allocated memory info */
  uint8_t xhci_mem_info_valid;  /**< Must be set to true if xhci_mem_info is being passed */
  apps_mem_info_v01 xhci_mem_info;

  /* Optional */
  /*  ADSP interrupter num */
  uint8_t interrupter_num_valid;  /**< Must be set to true if interrupter_num is being passed */
  uint8_t interrupter_num;
}qmi_uaudio_stream_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup uaudio_stream_qmi_messages
    @{
  */
/** Indication Message; Indication sent by the APPS USB driver to inform the UAC about usb_token and descriptor information. */
typedef struct {

  /* Mandatory */
  /*  USB audio device indication type */
  usb_audio_device_indication_enum_v01 dev_event;

  /* Mandatory */
  /*  Platform agnostic unique USB stream identifier */
  uint32_t slot_id;

  /* Optional */
  /*  USB Interface Descriptor  */
  uint8_t usb_token_valid;  /**< Must be set to true if usb_token is being passed */
  uint32_t usb_token;

  /* Optional */
  /*  USB host controller allocated index of device slot
 Defined in extensible host controller interface (XHCI) specification v1.0. */
  uint8_t std_as_opr_intf_desc_valid;  /**< Must be set to true if std_as_opr_intf_desc is being passed */
  usb_interface_descriptor_v01 std_as_opr_intf_desc;

  /* Optional */
  /*  USB Endpoint Descriptor - Data */
  uint8_t std_as_data_ep_desc_valid;  /**< Must be set to true if std_as_data_ep_desc is being passed */
  usb_endpoint_descriptor_v01 std_as_data_ep_desc;

  /* Optional */
  /*  USB Endpoint Descriptor - Sync */
  uint8_t std_as_sync_ep_desc_valid;  /**< Must be set to true if std_as_sync_ep_desc is being passed */
  usb_endpoint_descriptor_v01 std_as_sync_ep_desc;

  /* Optional */
  /*  Revision of USB audio class specification in binary coded decimal
 Represents bcdADC field from USB Class-Specific AC Interface Descriptor */
  uint8_t usb_audio_spec_revision_valid;  /**< Must be set to true if usb_audio_spec_revision is being passed */
  uint16_t usb_audio_spec_revision;

  /* Optional */
  /*  Delay introduced in audio data stream due to internal processing of signal
 within the audio function expressed in number of frames or micro-frames
 Represents bDelay field from USB Class-Specific AS Interface Descriptor */
  uint8_t data_path_delay_valid;  /**< Must be set to true if data_path_delay is being passed */
  uint8_t data_path_delay;

  /* Optional */
  /*  The number of bytes occupied by one audio subslot or subframe
 This field is same as bSubSlotSize or bSubFrameSize defined in 
 USB Audio Class Formats Specification 1.0 or 2.0 - Type I Format Type Descriptor */
  uint8_t usb_audio_subslot_size_valid;  /**< Must be set to true if usb_audio_subslot_size is being passed */
  uint8_t usb_audio_subslot_size;

  /* Optional */
  /*  APPS allocated memory info */
  uint8_t xhci_mem_info_valid;  /**< Must be set to true if xhci_mem_info is being passed */
  apps_mem_info_v01 xhci_mem_info;

  /* Optional */
  /*  ADSP interrupter num */
  uint8_t interrupter_num_valid;  /**< Must be set to true if interrupter_num is being passed */
  uint8_t interrupter_num;
}qmi_uaudio_stream_ind_msg_v01;  /* Message */
/**
    @}
  */

/* Conditional compilation tags for message removal */ 
//#define REMOVE_QMI_UAUDIO_IND_V01 
//#define REMOVE_QMI_UAUDIO_STREAM_V01 

/*Service Message Definition*/
/** @addtogroup uaudio_stream_qmi_msg_ids
    @{
  */
#define QMI_UAUDIO_STREAM_REQ_V01 0x0001
#define QMI_UAUDIO_STREAM_RESP_V01 0x0001
#define QMI_UADUIO_STREAM_IND_V01 0x0001
/**
    @}
  */

/* Service Object Accessor */
/** @addtogroup wms_qmi_accessor
    @{
  */
/** This function is used internally by the autogenerated code.  Clients should use the
   macro uaudio_stream_get_service_object_v01( ) that takes in no arguments. */
qmi_idl_service_object_type uaudio_stream_get_service_object_internal_v01
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version );

/** This macro should be used to get the service object */
#define uaudio_stream_get_service_object_v01( ) \
          uaudio_stream_get_service_object_internal_v01( \
            UAUDIO_STREAM_V01_IDL_MAJOR_VERS, UAUDIO_STREAM_V01_IDL_MINOR_VERS, \
            UAUDIO_STREAM_V01_IDL_TOOL_VERS )
/**
    @}
  */


#ifdef __cplusplus
}
#endif
#endif

