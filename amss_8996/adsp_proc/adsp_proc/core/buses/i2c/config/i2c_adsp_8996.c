/*=============================================================================

  FILE:     i2c_adsp_8996.c

  OVERVIEW: This file has the devices for 8994 platform for adsp. 
 
            Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary 

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.adsp/2.7/buses/i2c/config/i2c_adsp_8996.c#3 $
  $DateTime: 2015/03/13 19:00:11 $$Author: pwbldsvc $

  When     Who    What, where, why
  -------- ---    -----------------------------------------------------------
  10/03/14 NP     Added support for 8996
  07/01/14 SK     Removed all devices except 5 & 7 as 1 & 8 were causing conflicts.
  25/03/11 LK     Removed gsbi11,gsbi12 since they are dedicated to sps.
  12/03/09 LK     Created.

  ===========================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cDriverTypes.h"
#include "I2cPlatSvc.h"
#include "I2cDevice.h"

//#include "DALStdDef.h"
#include "DALDeviceId.h"
//#include "DALSys.h"

#define I2C_NUM_PLATFORM_DEVICES         (0)


/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

const uint32              i2cDeviceNum = I2C_NUM_PLATFORM_DEVICES;

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/


#ifdef BUILD_FOR_ISLAND
#define ATTRIBUTE_ISLAND_CODE __attribute__((section("RX.island")))
#define ATTRIBUTE_ISLAND_CONST __attribute__((section("RO.island")))
#define ATTRIBUTE_ISLAND_DATA __attribute__((section("RW.island")))
#else
#define ATTRIBUTE_ISLAND_CODE /* empty */
#define ATTRIBUTE_ISLAND_CONST /* empty */
#define ATTRIBUTE_ISLAND_DATA /* empty */
#endif


/*-------------------------------------------------------------------------
 * Global Data Definitions
 * ----------------------------------------------------------------------*/
I2cPlat_PropertyType i2cPlatPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   // I2CPLAT_PROPERTY_INIT(0x2001c023,0x2001c033,2,"PERIPH_SS",0x175000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP1_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup1_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c2b3,0x2001c2c3,2,"PERIPH_SS",0x176000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP2_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup2_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c2f3,0x2001c303,2,"PERIPH_SS",0x177000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP3_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup3_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c434,0x2001c444,2,"PERIPH_SS",0x178000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP4_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup4_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c534,0x2001c543,2,"PERIPH_SS",0x179000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP5_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup5_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c1b3,0x2001c1c3,2,"PERIPH_SS",0x17A000,CLOCK_GCC_BLSP1_AHB_CLK,CLOCK_GCC_BLSP1_QUP6_APPS_CLK,"gcc_blsp1_ahb_clk","gcc_blsp1_qup6_i2c_apps_clk"),
   I2CPLAT_PROPERTY_INIT(0x2001c373,0x2001c383,2,"PERIPH_SS",0x1B5000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP1_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup1_i2c_apps_clk"), //eeprom?
   // I2CPLAT_PROPERTY_INIT(0x2001c063,0x2001c073,2,"PERIPH_SS",0x1B6000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP2_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup2_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c334,0x2001c344,2,"PERIPH_SS",0x1B7000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP3_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup3_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c0a4,0x2001c0b4,2,"PERIPH_SS",0x1B8000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP4_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup4_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c3c5,0x2001c3d5,2,"PERIPH_SS",0x1B9000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP5_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup5_i2c_apps_clk"),
   // I2CPLAT_PROPERTY_INIT(0x2001c573,0x2001c583,2,"PERIPH_SS",0x1BA000,CLOCK_GCC_BLSP2_AHB_CLK,CLOCK_GCC_BLSP2_QUP6_APPS_CLK,"gcc_blsp2_ahb_clk","gcc_blsp2_qup6_i2c_apps_clk")
};

I2cDev_PropertyType i2cDevPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
   // I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ)
};

I2cDrv_DriverProperty i2cDrvPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_1, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_2, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_3, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_4, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_5, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_6, FALSE, 0),
    I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_7, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_8, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_9, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_10, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_11, FALSE, 0),
   // I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_12, FALSE, 0)
};

I2cDrv_DescType i2cDrvDescArray[I2C_NUM_PLATFORM_DEVICES] ATTRIBUTE_ISLAND_DATA ;
