#include "../config/devcfg_i2c_adsp_8996.h"
typedef unsigned char boolean;
typedef unsigned long long uint64;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long long int64;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned char byte;
typedef struct I2cClientConfig
{
   uint32 uBusFreqKhz;
   uint32 uByteTransferTimeoutUs;
   uint32 uSlaveAddr;
} I2cClientConfig;
typedef struct I2cTransferConfig
{
   uint32 uSlaveAddr;
   uint16 aWordVal[4];
} I2cTransferConfig;
typedef struct I2cIoVec
{
   uint8 *pBuff;
   uint32 size;
} I2cIoVec;
typedef struct I2cBuffDesc
{
   uint8 *pBuff;
   uint32 uBuffSize;
} I2cBuffDesc;
typedef enum _eTranDirection{
   I2cTranDirIn,
   I2cTranDirOut
} I2cTranDirection;
typedef enum _eTranCtxt{
   I2cTrCtxNotASequence,
   I2cTrCtxSeqStart ,
   I2cTrCtxSeqContinue ,
   I2cTrCtxSeqEnd
} I2cTranCtxt ;
typedef struct I2cTransfer
{
   union {
      I2cBuffDesc *pI2cBuffDesc;
   I2cIoVec *pIoVec;
   };
   uint32 uTrSize;
   I2cTransferConfig tranCfg;
   I2cTranDirection eTranDirection;
   I2cTranCtxt eTranCtxt ;
} I2cTransfer;
typedef struct I2cIoResult
{
  int32 nOperationResult;
  uint32 uOutByteCnt;
  uint32 uInByteCnt;
} I2cIoResult;
typedef struct I2cSequence
{
   I2cTransfer *pTransfer;
   uint32 uNumTransfers;
} I2cSequence;
typedef struct I2cRegSeqObj
{
   I2cTransfer aTr[2];
   I2cIoVec io_vec[2];
   uint8 offset[2];
   I2cSequence seq;
} I2cRegSeqObj ;
typedef enum I2cRegOffsetType
{
   I2cRegOffset_8Bit,
   I2cRegOffset_16Bit
} I2cRegOffsetType;
typedef struct I2cSimpleTrObj
{
   I2cTransfer aTr[1];
   I2cIoVec io_vec[1];
} I2cSimpleTrObj ;
static __inline void I2c_TrObj_Init
(
   I2cTransfer *pTr,
   I2cIoVec *pVec,
   I2cTranDirection eTranDirection,
   I2cTranCtxt eTranCtxt
)
{
   pTr->eTranDirection = eTranDirection;
   pTr->eTranCtxt = eTranCtxt;
   pTr->pIoVec = pVec;
   pTr->uTrSize = 0;
}
static __inline void I2c_TrObj_AddBuffer
(
   I2cTransfer *pTr ,
   uint32 uBuffIndex,
   uint8 *pBuff ,
   uint32 size
)
{
   pTr->pIoVec[uBuffIndex].pBuff = pBuff;
   pTr->pIoVec[uBuffIndex].size = size;
   pTr->uTrSize += size;
}
static __inline void I2c_SeqObj_Init
(
   I2cSequence *pSeq,
   I2cTransfer *pTr ,
   uint32 uNumTr
)
{
   pSeq->pTransfer = pTr;
   pSeq->uNumTransfers = uNumTr;
}
static __inline void I2c_RegObj_InitRead(I2cRegSeqObj *pRegObj)
{
   I2c_SeqObj_Init(&pRegObj->seq, pRegObj->aTr, 2);
   I2c_TrObj_Init(&(pRegObj->aTr[0]),&(pRegObj->io_vec[0]), I2cTranDirOut, I2cTrCtxSeqStart);
   I2c_TrObj_Init(&(pRegObj->aTr[1]),&(pRegObj->io_vec[1]), I2cTranDirIn, I2cTrCtxSeqEnd);
}
static __inline void I2c_RegObj_InitWrite(I2cRegSeqObj *pRegObj)
{
   I2c_SeqObj_Init(&(pRegObj->seq), &(pRegObj->aTr[0]), 1);
   I2c_TrObj_Init(&(pRegObj->aTr[0]),&(pRegObj->io_vec[0]), I2cTranDirOut, I2cTrCtxNotASequence);
}
static __inline void I2c_RegObj_SetOffset
(
   I2cRegSeqObj *pRegObj,
   uint32 offset,
   I2cRegOffsetType offsetType
)
{
   uint32 uSize;
   pRegObj->offset[0] = (uint8)(offset &0xFF);
   if (offsetType == I2cRegOffset_16Bit) {
     uSize = 2;
     pRegObj->offset[1] = (uint8)((offset >> 8) &0xFF);
   }
   else {
     uSize = 1;
   }
   I2c_TrObj_AddBuffer(&(pRegObj->aTr[0]), 0,pRegObj->offset, uSize);
}
static __inline void I2c_RegObj_SetReadIoVector
(
   I2cRegSeqObj *pRegObj,
   uint8 *pBuff,
   uint32 size
)
{
   I2c_TrObj_AddBuffer(&(pRegObj->aTr[1]), 0,pBuff, size);
}
static __inline void I2c_RegObj_SetWriteIoVector
(
   I2cRegSeqObj *pRegObj,
   uint8 *pBuff,
   uint32 size
)
{
   I2c_TrObj_AddBuffer(&(pRegObj->aTr[0]), 1,pBuff, size);
}
typedef enum I2cResult
{
   I2C_RES_SUCCESS =0,
   I2C_RES_ERROR_CLS_I2C_CORE = (int32)0x10000000,
   I2C_RES_ERROR_CLS_QUP_DEV_TIMEOUT = (int32)0x20000000,
   I2C_RES_ERROR_CLS_QUP_DEV = (int32)0x30000000,
   I2C_RES_ERROR_CLS_I2C_DRV = (int32)0x40000000,
   I2C_RES_ERROR_CLS_DEV_PLATFORM = (int32)0xE0000000,
   I2C_RES_ERROR_CLS_DEV_OS = (int32)0xF0000000,
} I2cResult;
typedef void* I2CDRV_HANDLE;
typedef enum I2cDrv_I2cBusId
{
   I2CDRV_I2C_MIN = 0,
   I2CDRV_I2C_1 = 0,
   I2CDRV_I2C_2 = 1,
   I2CDRV_I2C_3 = 2,
   I2CDRV_I2C_4 = 3,
   I2CDRV_I2C_5 = 4,
   I2CDRV_I2C_6 = 5,
   I2CDRV_I2C_7 = 6,
   I2CDRV_I2C_8 = 7,
   I2CDRV_I2C_9 = 8,
   I2CDRV_I2C_10 = 9,
   I2CDRV_I2C_11 = 10,
   I2CDRV_I2C_12 = 11,
   I2CDRV_I2C_MAX = I2CDRV_I2C_12,
   I2CDRV_I2C_NUM = 12,
   I2CDRV_I2C_DEV_INVALID = -1,
} I2cDrv_I2cBusId;
typedef struct I2cDrv_I2cBus
{
   I2cDrv_I2cBusId i2cId;
   I2cClientConfig clntCfg;
   uint32 cookie;
} I2cDrv_I2cBus;
typedef enum I2cDrvCmdType
{
   I2cDrvCmd_Vote_PnocOn,
   I2cDrvCmd_Vote_PnocOff,
   I2cDrvCmd_Vote_I2cClocsOn,
   I2cDrvCmd_Vote_I2cClocsOff,
} I2cDrvCmdType;
typedef struct I2cDrvPnocVoteData
{
   uint64 uPnocIb;
   uint64 uPnocAb;
} I2cDrvPnocVoteData;
int32
I2cDrv_Open
(
   I2cDrv_I2cBusId eI2cBusId ,
   I2cDrv_I2cBus *pI2cBus ,
   uint32 dwaccessMode
);
int32
I2cDrv_Close
(
   I2cDrv_I2cBus *pI2cBus
);
int32
I2cDrv_Read
(
   I2cDrv_I2cBus *pI2cBus,
   I2cTransfer *pTransfer,
   uint32 *puNumCompleted
);
int32
I2cDrv_Write
(
   I2cDrv_I2cBus *pI2cBus,
   I2cTransfer *pTransfer,
   uint32 *puNumCompleted
);
int32
I2cDrv_BatchTransfer
(
   I2cDrv_I2cBus *pI2cBus,
   I2cSequence *pSequence,
   I2cIoResult *pIoRes
);
int32
I2cDrv_Cmd
(
   I2cDrv_I2cBus *pI2cBus,
   I2cDrvCmdType cmd,
   void *pData
);
typedef enum I2C_MASTER_STATUS_CANCEL_FSM_STATE_Value {
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_IDLE_STATE = 0,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_CANCEL_PENDING_STATE = 1,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_PAUSE_STATE = 2,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_PAUSE_WAIT_STATE = 3,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_SEND_STOP_STATE = 4,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_FLUSH_STATE = 5,
   I2C_MASTER_STATUS_CANCEL_FSM_STATE_SEND_IRQ_STATE = 6,
} I2C_MASTER_STATUS_CANCEL_FSM_STATE_Value;
typedef enum I2C_MASTER_STATUS_INPUT_FSM_STATE_Value {
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_RESET_STATE = 0,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_IDLE_STATE = 1,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_READ_LAST_BYTE_STATE = 2,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_MI_REC_STATE = 3,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_DEC_STATE = 4,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V1_V2_STORE_STATE = 5,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V2_WR_TAG_STATE = 6,
   I2C_MASTER_STATUS_INPUT_FSM_STATE_V2_WAIT_TAG_STATE = 7,
} I2C_MASTER_STATUS_INPUT_FSM_STATE_Value;
typedef enum I2C_MASTER_STATUS_OUTPUT_FSM_STATE_Value {
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_RESET_STATE = 0,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_IDLE_STATE = 1,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_DECODE_STATE = 2,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_SEND_STATE = 3,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_MI_REC_STATE = 4,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_NOP_STATE = 5,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_V2_INVALID_STATE = 6,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_PEEK_STATE = 7,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V1_SEND_R_STATE = 8,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V2_GET_BYTE_STATE = 11,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V2_GET_WRITE_BYTE_STATE = 12,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V2_SPECIALITY_STATE = 13,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V2_WAIT_FLUSH_STOP_STATE = 14,
   I2C_MASTER_STATUS_OUTPUT_FSM_STATE_V2_STOP_STATE = 15,
} I2C_MASTER_STATUS_OUTPUT_FSM_STATE_Value;
typedef enum I2C_MASTER_STATUS_CLK_STATE_Value {
   I2C_MASTER_STATUS_CLK_STATE_RESET_BUSIDLE_STATE = 0,
   I2C_MASTER_STATUS_CLK_STATE_NOT_MASTER_STATE = 1,
   I2C_MASTER_STATUS_CLK_STATE_HIGH_STATE = 2,
   I2C_MASTER_STATUS_CLK_STATE_LOW_STATE = 3,
   I2C_MASTER_STATUS_CLK_STATE_HIGH_WAIT_STATE = 4,
   I2C_MASTER_STATUS_CLK_STATE_FORCED_LOW_STATE = 5,
   I2C_MASTER_STATUS_CLK_STATE_HS_ADDR_LOW_STATE = 6,
   I2C_MASTER_STATUS_CLK_STATE_DOUBLE_BUFFER_WAIT_STATE = 7,
} I2C_MASTER_STATUS_CLK_STATE_Value;
typedef enum I2C_MASTER_STATUS_DATA_STATE_Value {
   I2C_MASTER_STATUS_DATA_STATE_RESET_WAIT_STATE = 0,
   I2C_MASTER_STATUS_DATA_STATE_TX_ADDR_STATE = 1,
   I2C_MASTER_STATUS_DATA_STATE_TX_DATA_STATE = 2,
   I2C_MASTER_STATUS_DATA_STATE_TX_HS_ADDR_STATE = 3,
   I2C_MASTER_STATUS_DATA_STATE_TX_10_BIT_ADDR_STATE = 4,
   I2C_MASTER_STATUS_DATA_STATE_TX_2ND_BYTE_STATE = 5,
   I2C_MASTER_STATUS_DATA_STATE_RX_DATA_STATE = 6,
   I2C_MASTER_STATUS_DATA_STATE_RX_NACK_HOLD_STATE = 7,
} I2C_MASTER_STATUS_DATA_STATE_Value;
typedef volatile struct QupRegs {
   uint32 QUP_CONFIG;
   uint32 QUP_STATE;
   uint32 QUP_IO_MODES;
   uint32 QUP_SW_RESET;
   const uint32 _FILL0[1];
   uint32 QUP_TRANSFER_CANCEL;
   uint32 QUP_OPERATIONAL;
   uint32 QUP_ERROR_FLAGS;
   uint32 QUP_ERROR_FLAGS_EN;
   uint32 QUP_TEST_CTRL;
   uint32 QUP_OPERATIONAL_MASK;
   const uint32 _FILL1[1];
   uint32 QUP_HW_VERSION;
   const uint32 _FILL2[51];
   uint32 QUP_MX_OUTPUT_COUNT;
   uint32 QUP_MX_OUTPUT_CNT_CURRENT;
   uint32 QUP_OUTPUT_DEBUG;
   uint32 QUP_OUTPUT_FIFO_WORD_CNT;
   uint32 QUP_OUTPUT_FIFO0;
   const uint32 _FILL3[15];
   uint32 QUP_MX_WRITE_COUNT;
   uint32 QUP_MX_WRITE_CNT_CURRENT;
   const uint32 _FILL4[42];
   uint32 QUP_MX_INPUT_COUNT;
   uint32 QUP_MX_INPUT_CNT_CURRENT;
   uint32 QUP_MX_READ_COUNT;
   uint32 QUP_MX_READ_CNT_CURRENT;
   uint32 QUP_INPUT_DEBUG;
   uint32 QUP_INPUT_FIFO_WORD_CNT;
   uint32 QUP_INPUT_FIFO0;
   const uint32 _FILL5[57];
   uint32 SPI_CONFIG;
   uint32 SPI_IO_CONTROL;
   uint32 SPI_ERROR_FLAGS;
   uint32 SPI_ERROR_FLAGS_EN;
   uint32 SPI_DEASSERT_WAIT;
   uint32 SPI_MASTER_LOCAL_ID;
   uint32 SPI_MASTER_COMMAND;
   uint32 SPI_MASTER_STATUS;
   uint32 SPI_CHAR_CFG;
   uint32 SPI_CHAR_DATA_SPI_CS;
   uint32 SPI_CHAR_DATA_SPI_MOSI;
   uint32 SPI_CHAR_DATA_SPI_MISO;
   const uint32 _FILL6[52];
   uint32 I2C_MASTER_CLK_CTL;
   uint32 I2C_MASTER_STATUS;
   uint32 I2C_MASTER_CONFIG;
   uint32 I2C_MASTER_BUS_CLEAR;
   uint32 I2C_MASTER_LOCAL_ID;
   uint32 I2C_MASTER_COMMAND;
} QupRegsType;
typedef enum QUP_RunStateType
{
   QUP_RUNSTATE_RESET = 0 ,
   QUP_RUNSTATE_RUN = 1 ,
   QUP_RUNSTATE_PAUSE = 3 ,
   QUP_RUNSTATE_INVALID = 0xFF ,
   QUP_RUNSTATE_EXTEND = 0x7FFFFFFF
} QUP_RunStateType;
typedef enum QUP_RunStateCmdType
{
   QUP_RUNSTATE_CMD_RESET = 0x2 ,
   QUP_RUNSTATE_CMD_RUN = 0x1 ,
   QUP_RUNSTATE_CMD_PAUSE = 0x3 ,
   QUP_RUNSTATE_CMD_EXTEND = 0x7FFFFFFF
} QUP_RunStateCmdType;
typedef enum QUP_ErrStateType
{
   QUP_ERRSTATE_BAM_TRANS_ERROR = 0x80,
   QUP_ERRSTATE_OUTPUT_OVER_RUN = 0x20,
   QUP_ERRSTATE_INPUT_UNDER_RUN = 0x10,
   QUP_ERRSTATE_OUTPUT_UNDER_RUN = 0x8,
   QUP_ERRSTATE_INPUT_OVER_RUN = 0x4,
   QUP_ERRSTATE_FIELD = QUP_ERRSTATE_BAM_TRANS_ERROR |
                         QUP_ERRSTATE_OUTPUT_OVER_RUN |
                         QUP_ERRSTATE_INPUT_UNDER_RUN |
                         QUP_ERRSTATE_OUTPUT_UNDER_RUN |
                         QUP_ERRSTATE_INPUT_OVER_RUN,
   QUP_ERRSTATE_EXTEND = 0x7FFFFFFF
} QUP_ErrStateType;
typedef enum I2C_OpStatusType
{
   I2C_OP_STATUS_INVALID_READ_SEQ = 0x2000000,
   I2C_OP_STATUS_INVALID_READ_ADDR = 0x1000000,
   I2C_OP_STATUS_INVALID_TAG = 0x800000,
   I2C_OP_STATUS_FAILED = 0xC0,
   I2C_OP_STATUS_INVALID_WRITE = 0x20,
   I2C_OP_STATUS_ARB_LOST = 0x10,
   I2C_OP_STATUS_PACKET_NACKED = 0x08,
   I2C_OP_STATUS_BUS_ERROR = 0x04,
   I2C_OP_STATUS_ERROR_FIELD =
                  I2C_OP_STATUS_INVALID_READ_SEQ |
                  I2C_OP_STATUS_INVALID_READ_ADDR|
                  I2C_OP_STATUS_INVALID_TAG |
                  I2C_OP_STATUS_FAILED |
                  I2C_OP_STATUS_INVALID_WRITE |
                  I2C_OP_STATUS_ARB_LOST |
                  I2C_OP_STATUS_PACKET_NACKED |
                  I2C_OP_STATUS_BUS_ERROR ,
   I2C_OP_STATUS_FLAG_EXTEND = 0x7FFFFFFF
} I2C_OpStatusType;
typedef enum I2C_ClkCtlType
{
   I2C_CLK_CTL_FS_DIVIDER_VALUE_MIN = 0x7,
   I2C_CLK_CTL_FLAG_EXTEND = 0x7FFFFFFF
} I2C_ClkCtlType;
typedef enum QUP_MiniCoreType
{
   QUP_MINI_CORE_FLD_MIN = 0 ,
   QUP_MINI_CORE_NULL = 0 ,
   QUP_MINI_CORE_SPI = 1 ,
   QUP_MINI_CORE_I2C_MASTER = 0x2,
   QUP_MINI_CORE_FLD_MAX = 0x2,
   QUP_MINI_CORE_EXTEND = 0x7FFFFFFF
} QUP_MiniCoreType;
typedef enum QUP_IoModeType{
   QUP_IO_MODE_FLD_MIN = 0,
   QUP_IO_MODE_FIFO = 0,
   QUP_IO_MODE_BLOCK = 1,
   QUP_IO_MODE_BAM = 3,
   QUP_IO_MODE_FLD_MAX = 3,
   QUP_IO_MODE_EXTEND = 0x7FFFFFFF
} QUP_IoModeType;
typedef enum QUP_I2cTagType
{
   QUP_I2C_TAGS_V1 = 0,
   QUP_I2C_TAGS_V2 = 0x1,
   QUP_I2C_TAGS_FLD_MAX = 0x1,
   QUP_I2C_TAGS_EXTEND = 0x7FFFFFFF
} QUP_I2cTagType;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef enum
{
  CLOCK_GCC_BLSP1_AHB_CLK,
  CLOCK_GCC_BLSP1_QUP1_APPS_CLK,
  CLOCK_GCC_BLSP1_QUP2_APPS_CLK,
  CLOCK_GCC_BLSP1_QUP3_APPS_CLK,
  CLOCK_GCC_BLSP1_QUP4_APPS_CLK,
  CLOCK_GCC_BLSP1_QUP5_APPS_CLK,
  CLOCK_GCC_BLSP1_QUP6_APPS_CLK,
  CLOCK_GCC_BLSP2_AHB_CLK,
  CLOCK_GCC_BLSP2_QUP1_APPS_CLK,
  CLOCK_GCC_BLSP2_QUP2_APPS_CLK,
  CLOCK_GCC_BLSP2_QUP3_APPS_CLK,
  CLOCK_GCC_BLSP2_QUP4_APPS_CLK,
  CLOCK_GCC_BLSP2_QUP5_APPS_CLK,
  CLOCK_GCC_BLSP2_QUP6_APPS_CLK,
  CLOCK_TOTAL_CLOCK_ENUMS,
}uClockIdType;
boolean uClock_EnableClock(uClockIdType eClockId);
boolean uClock_DisableClock(uClockIdType eClockId);
boolean uClock_IsClockEnabled(uClockIdType eClockId);
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef uint32 ClockIdType;
typedef uint32 SourceIdType;
typedef enum
{
  CLOCK_FREQUENCY_HZ_AT_LEAST = 0,
  CLOCK_FREQUENCY_HZ_AT_MOST = 1,
  CLOCK_FREQUENCY_HZ_CLOSEST = 2,
  CLOCK_FREQUENCY_HZ_EXACT = 3,
  CLOCK_FREQUENCY_KHZ_AT_LEAST = 0x10,
  CLOCK_FREQUENCY_KHZ_AT_MOST = 0x11,
  CLOCK_FREQUENCY_KHZ_CLOSEST = 0x12,
  CLOCK_FREQUENCY_KHZ_EXACT = 0x13,
  CLOCK_FREQUENCY_MHZ_AT_LEAST = 0x20,
  CLOCK_FREQUENCY_MHZ_AT_MOST = 0x21,
  CLOCK_FREQUENCY_MHZ_CLOSEST = 0x22,
  CLOCK_FREQUENCY_MHZ_EXACT = 0x23,
} ClockFrequencyType;
typedef enum
{
  CLOCK_RESET_DEASSERT = 0,
  CLOCK_RESET_ASSERT = 1,
  CLOCK_RESET_PULSE = 2
} ClockResetType;
typedef enum
{
  CLOCK_QDSS_LEVEL_OFF,
  CLOCK_QDSS_LEVEL_DEBUG,
  CLOCK_QDSS_LEVEL_HSDEBUG,
} ClockQDSSLevelType;
typedef enum
{
  CLOCK_CONFIG_MMSS_SOURCE_CSI0 = 1,
  CLOCK_CONFIG_MMSS_SOURCE_CSI1 = 2,
  CLOCK_CONFIG_MMSS_SOURCE_CSI2 = 3,
  CLOCK_CONFIG_LPASS_CORE_MEM_ON = 4,
  CLOCK_CONFIG_LPASS_CORE_MEM_OFF = 5,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_ON = 6,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_OFF = 7,
  CLOCK_CONFIG_LPASS__HW_CTL_ON = 8,
  CLOCK_CONFIG_LPASS__HW_CTL_OFF = 9,
  CLOCK_CONFIG_AUTOGATE_ENABLE = 10,
  CLOCK_CONFIG_AUTOGATE_DISABLE = 11,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_DIG_CODEC = 12,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_PRI_MI2S = 13,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_SEC_MI2S = 14,
  CLOCK_CONFIG_TOTAL = 15
} ClockConfigType;
typedef enum
{
  CLOCK_SOURCE_NONE,
  CLOCK_SOURCE_PRIMARY,
  CLOCK_SOURCE_SECONDARY,
  CLOCK_SOURCE_TERNERY,
  CLOCK_SOURCE_TOTAL
}ClockSourceType;
typedef enum
{
  CLOCK_LOG_STATE_CLOCK_FREQUENCY = (1 << 0),
} ClockLogStateFlags;
typedef uint32 ClockPowerDomainIdType;
typedef enum
{
  CLOCK_SLEEP_MODE_HALT,
  CLOCK_SLEEP_MODE_POWER_COLLAPSE
} ClockSleepModeType;
typedef enum
{
  CLOCK_CPU_MSS_Q6,
  CLOCK_CPU_LPASS_Q6,
  CLOCK_CPU_APPS_C0,
  CLOCK_CPU_APPS_C1,
  CLOCK_CPU_APPS_C2,
  CLOCK_CPU_APPS_C3,
  CLOCK_CPU_NUM_OF_CPUS
} ClockCPUType;
typedef enum I2cPlat_Error
{
   I2CPLAT_ERROR_BASE = I2C_RES_ERROR_CLS_DEV_PLATFORM,
   I2CPLAT_ERROR_INVALID_DEVICE_INDEX,
   I2CPLAT_ERROR_DAL_GET_PROPERTY_HANDLE,
   I2CPLAT_ERROR_DAL_GET_PROPERTY_VALUE,
   I2CPLAT_ERROR_ATTACH_TO_DALHWIO,
   I2CPLAT_ERROR_FAILED_TO_MAP_BLOCK_HWIO,
   I2CPLAT_ERROR_GETTING_CLK_AHB,
   I2CPLAT_ERROR_GETTING_CLK_APPS,
   I2CPLAT_ERROR_DETACH_FROM_DALHWIO,
   I2CPLAT_ERROR_INVALID_POWER_STATE,
   I2CPLAT_ERROR_FAILED_TO_ATTACH_TO_TLMM,
   I2CPLAT_ERROR_FAILED_TO_DETTACH_FROM_TLMM,
   I2CPLAT_ERROR_FAILED_TO_ATTACH_TO_CLOCKS,
   I2CPLAT_ERROR_FAILED_TO_DETACH_FROM_CLOCKS,
   I2CPLAT_ERROR_FAILED_ATTACH_TO_IRQCTRL,
   I2CPLAT_ERROR_FAILED_TO_REGISTER_IST,
   I2CPLAT_ERROR_FAILED_TO_UNREGISTER_IST,
   I2CPLAT_ERROR_IRQCTRL_NOT_INITIALISED,
   I2CPLAT_ERROR_FAILED_TO_OPEN_TLMM,
   I2CPLAT_ERROR_FAILED_TO_CLOSE_TLMM,
   I2CPLAT_ERROR_FAILED_TO_CONFIGURE_GPIO,
   I2CPLAT_ERROR_FAILED_TO_ENABLE_HCLK,
   I2CPLAT_ERROR_FAILED_TO_DISABLE_HCLK,
   I2CPLAT_ERROR_FAILED_TO_CREATE_PNOC_CLIENT,
   I2CPLAT_ERROR_PNOC_CLIENT_NOT_CREATED,
   I2CPLAT_ERROR_FAILED_TO_ENABLE_AHB_CLK,
   I2CPLAT_ERROR_FAILED_TO_ENABLE_APPS_CLK,
   I2CPLAT_ERROR_FAILED_TO_DISABLE_AHB_CLK,
   I2CPLAT_ERROR_FAILED_TO_DISABLE_APPS_CLK,
   I2CPLAT_ERROR_FAILED_TO_ISSUE_PNOC_REQ,
} I2cPlat_Error;
typedef enum I2cPlat_PowerStates
{
   I2CPLAT_POWER_STATE_0,
   I2CPLAT_POWER_STATE_1,
   I2CPLAT_POWER_STATE_2,
} I2cPlat_PowerStates;
typedef enum I2cPlat_TargetInitState
{
   I2CPLAT_TGT_INIT_NOT_DONE = 0,
   I2CPLAT_TGT_INIT_CLK_ALLOCATED = 0x01,
   I2CPLAT_TGT_INIT_HWIO_ALLOCATED = 0x01,
   I2CPLAT_TGT_INIT_GPIO_INITED = 0x02,
   I2CPLAT_TGT_INIT_PNOC_INITED = 0x04,
   I2CPLAT_TGT_INIT_HWIO_INITED = 0x20,
} I2cPlat_TargetInitState;
typedef struct I2cPlat_PropertyType
{
   uint32 aGpioCfg[2];
   uint32 uNumGpios;
   const char* pPeriphSsAddrName;
   uint32 coreOffset;
   uClockIdType ahbClkId;
   uClockIdType appsClkId;
   const char* appsClkIdName;
   const char* hClkIdName;
   DalDeviceHandle* pClkHandle;
   ClockIdType QupAppClkId;
   ClockIdType QupHClkId;
} I2cPlat_PropertyType ;
typedef struct I2cPlat_DescType
{
   uint32 tmpPowerState;
   I2cPlat_TargetInitState initState;
   uint8 *pQupAddr;
   I2cPlat_PropertyType props;
   npa_client_handle hNpaClient;
} I2cPlat_DescType;
typedef void(*IST_HOOK)(void*);
typedef void(*ISR_HOOK)(void*);
int32
I2cPlat_DeInitTarget
(
   I2cPlat_DescType *pPlat
);
int32
I2cPlat_InitTarget
(
   I2cPlat_DescType *pPlat ,
   I2cPlat_PropertyType *pProps
);
int32
I2cPlat_RegisterIstHooks
(
   I2cPlat_DescType *pDev,
   ISR_HOOK IsrHook,
   void* pIsrData,
   IST_HOOK IstHook,
   void* pIstData
);
int32
I2cPlat_UnRegisterIstHooks
(
   I2cPlat_DescType *pDev
);
int32
I2cPlat_ResetTarget
(
   I2cPlat_DescType *pPlat
);
uint8*
I2cPlat_GetQupAddress
(
   I2cPlat_DescType *pPlat
);
int32
I2cPlat_AddPnocVote
(
   I2cPlat_DescType *pPlat,
   uint64 uPeakBandwidthBps,
   uint64 uAvgBandwidthBps
);
int32
I2cPlat_RemovePnocVote
(
   I2cPlat_DescType *pPlat
);
int32 I2cPlat_VoteI2cClkOn
(
   I2cPlat_DescType *pPlat
);
int32 I2cPlat_VoteI2cClkOff
(
   I2cPlat_DescType *pPlat
);
enum I2cDev_Error
{
   I2CDEV_RES_SUCCESS = I2C_RES_SUCCESS,
   I2CDEV_ERR_BASE = I2C_RES_ERROR_CLS_QUP_DEV,
   I2CDEV_ERR_INVALID_DEVICE_ID = 0x30000100,
   I2CDEV_ERR_IN_SEQ_OUT_OF_SYNC ,
   I2CDEV_ERR_INVALID_RUNSTATE ,
   I2CDEV_ERR_RUNSTATE_CHANGE_TIMEOUT ,
   I2CDEV_ERR_NEW_RUNSTATE_INVALID ,
   I2CDEV_ERR_INVALID_POWER_STATE ,
   I2CDEV_ERR_BUS_BUSY ,
   I2CDEV_ERR_BUS_NOT_RELEASED ,
   I2CDEV_ERR_INPUT_DONE_TRANSFERS_NOT ,
   I2CDEV_ERR_INVALID_DEV_HANDLE ,
   I2CDEV_ERR_INVALID_TRANSFER_HANDLE ,
   I2CDEV_ERR_INVALID_TRANSFER_OBJPTR ,
   I2CDEV_ERR_INVALID_SEQUENCE_HANDLE ,
   I2CDEV_ERR_INVALID_SEQUENCE_OBJPTR ,
   I2CDEV_ERR_INVALID_NUMCOMPLETE_POINTER ,
   I2CDEV_ERR_INVALID_TRAN_RESULT_POINTER ,
   I2CDEV_ERR_INVALID_CALLBACK_FN_POINTER ,
   I2CDEV_ERR_INVALID_CALLBACK_ARG_POINTER ,
   I2CDEV_ERR_INVALID_TRANSFER_DIRECTION ,
   I2CDEV_ERR_INPUT_DESC_BUFFER_ALLOC_FAILED,
   I2CDEV_ERR_API_NOT_SUPPORTED ,
   I2CDEV_ERR_INVALID_RUN_STATE ,
   I2CDEV_ERR_INVALID_OUTPUT_BYTE_CNT ,
   I2CDEV_ERR_INVALID_INPUT_BYTE_CNT ,
   I2CDEV_ERR_INVALID_I2C_CLOCK ,
   I2CDEV_ERR_INVALID_I2C_FS_DIVIDER ,
   I2CDEV_ERR_BAM_ERROR ,
};
typedef enum I2cDev_InitState
{
   I2cDev_InitState_Uninitialized = 0x0,
   I2cDev_InitState_DeviceInit_Done = 0x1,
   I2cDev_InitState_HardwareInit_Done = 0x2,
} I2cDev_InitState;
typedef enum I2CDEVQUP_TrCfgValInd
{
   I2CDEVQUP_NumOutBytes_DtIdx = 0x0,
   I2CDEVQUP_NumInBytes_DtIdx = 0x1,
   I2CDEVQUP_NumPrepadBytes_DtIdx = 0x2,
   I2CDEVQUP_Unused_DtIdx = 0x3,
} I2CDEVQUP_TrCfgValInd;
typedef volatile struct I2cDev_QupIsrHwEvt
{
   uint32 uIrqCnt;
   uint32 uQupErrors;
   uint32 uOperational;
   uint32 uOutTranCnt;
   uint32 uInTranCnt;
   uint32 i2cStatus;
} I2cDev_QupIsrHwEvt;
typedef volatile struct I2cDev_QupBamHwEvt
{
   uint32 uBamEvt;
} I2cDev_QupBamHwEvt;
typedef volatile struct I2cDev_QupHwEvtQueue
{
   I2cDev_QupIsrHwEvt aHwEvt[(4)];
   I2cDev_QupIsrHwEvt *pHead;
   I2cDev_QupIsrHwEvt *pTail;
} I2cDev_QupHwEvtQueue;
typedef volatile struct I2cDev_QupInfo
{
   QupRegsType *pQupRegs;
   uint32 uIrqCnt;
   I2cDev_QupHwEvtQueue hwEvtQueue;
   uint32 uInFreeSlotReqCnt;
   uint32 uOutFreeSlotReqCnt;
   uint32 uQupErrors;
   uint32 uOperational;
   uint32 uInFreeSlotServicedCnt;
   uint32 uOutFreeSlotServicedCnt;
   uint32 uCurOutTransfers;
   uint32 uCurInTransfers;
   uint32 i2cStatus;
   QUP_IoModeType eOutFifoMode;
   QUP_IoModeType eInFifoMode;
} I2cDev_QupInfo;
typedef enum I2cDev_TransferState
{
   I2CDEV_TR_STATE_PAD_OUTPUT,
   I2CDEV_TR_STATE_TIME_STAMP_START,
   I2CDEV_TR_STATE_TIME_STAMP_VALUE,
   I2CDEV_TR_STATE_MASTER_START_TAG,
   I2CDEV_TR_STATE_MASTER_ADDR,
   I2CDEV_TR_STATE_START_TAG,
   I2CDEV_TR_STATE_SLAVE_ADDR,
   I2CDEV_TR_STATE_RD_DATA_TAG,
   I2CDEV_TR_STATE_WR_DATA_TAG,
   I2CDEV_TR_STATE_DATA_CNT,
   I2CDEV_TR_STATE_EOT_TAG,
   I2CDEV_TR_STATE_DATA_VALUE,
   I2CDEV_TR_STATE_DATA_PAD,
   I2CDEV_TR_STATE_NOT_IN_TRANSFER,
   I2CDEV_TR_STATE_EXTEND = 0x7FFFFFFF
} I2cDev_TransferState;
typedef enum I2cDev_OutputState
{
   I2CDEV_OUTPUT_IDDLE ,
   I2CDEV_OUTPUT_STALLED ,
   I2CDEV_OUTPUT_FILL_FIFO ,
   I2CDEV_OUTPUT_FILL_FIFO_DONE ,
   I2CDEV_OUTPUT_TRANSMIT_DONE ,
   I2CDEV_OUTPUT_ERROR ,
} I2cDev_OutputState;
typedef struct I2cDev_OutTransferInfo
{
      I2cDev_TransferState eOutTrState;
      uint32 uDataIndex;
      uint32 uNextDataSegIndex;
      I2cIoVec *pIoVec;
      uint32 IoVecBuffIndex;
      uint32 uFieldByteCnt;
} I2cDev_OutTransferInfo;
typedef struct I2cDev_SequenceIoInfo I2cDev_SequenceIoInfo;
typedef struct I2cDev_OutSeqInfo
{
   I2cDev_OutputState eOutputState;
   I2cTransfer *pTransfer;
   uint32 uTotalOutBytes;
   uint32 uCurOutBytes;
   uint32 uNumOutDtBytes;
   uint32 uOutTrCnt;
   uint32 uTrIndex;
   I2cDev_OutTransferInfo outTr;
   I2cDev_SequenceIoInfo *pSeq;
} I2cDev_OutSeqInfo;
typedef enum I2cDev_InputState
{
   I2CDEV_INPUT_IDDLE,
   I2CDEV_INPUT_STALLED,
   I2CDEV_INPUT_MEMORY_FLUSH,
   I2CDEV_INPUT_RECEIVE_FROM_INPUT_FIFO,
   I2CDEV_INPUT_PUSH_DESCRIPTORS,
   I2CDEV_INPUT_WAIT_READ_DATA_READY,
   I2CDEV_INPUT_RECEIVE_DONE,
   I2CDEV_INPUT_ERROR
} I2cDev_InputState;
typedef struct I2cDev_InTransferInfo
{
   I2cDev_TransferState eInTrState;
   uint32 uDataIndex;
   uint32 uNextDataSegIndex;
   I2cIoVec *pIoVec;
   uint8 *pIoVecBuff;
   uint32 IoVecBuffIndex;
   uint32 uFieldByteCnt;
} I2cDev_InTransferInfo;
typedef struct I2cDev_InSeqInfo
{
   I2cDev_InputState eInSeqState;
   I2cTransfer *pTransfer;
   uint32 uTotalInBytes;
   uint32 uNumInDtBytes;
   uint32 uInPadCnt;
   uint32 uInTrCnt;
   uint32 uTrIndex;
   uint32 uLastTrIndex;
   uint32 uNumInBytesReceived;
   I2cDev_InTransferInfo inTr;
   I2cDev_SequenceIoInfo *pSeq;
} I2cDev_InSeqInfo;
typedef enum I2cDev_SequenceIoState
{
   I2CDEV_SEQSTATE_IDDLE,
   I2CDEV_SEQSTATE_INPUT_OUTPUT,
   I2CDEV_SEQSTATE_COMPLETING,
   I2CDEV_SEQSTATE_COMPLETE,
   I2CDEV_SEQSTATE_QUP_STATE_ERROR,
} I2cDev_SequenceIoState;
typedef struct I2cDev_Device I2cDev_Device;
struct I2cDev_SequenceIoInfo
{
   I2cTransfer *pTransfer;
   I2cDev_Device *pDev;
   uint32 uNumTransfers;
   uint32 uSeqTimeoutUs;
   I2cDev_OutSeqInfo outSeqInfo;
   I2cDev_InSeqInfo inSeqInfo;
   boolean bBamTransfer;
   boolean bHsTransfer;
   I2cDev_SequenceIoState eSeqState;
   I2cIoResult ioRes;
};
typedef struct I2cDev_HwInfoType
{
   uint32 uNumInputSlots;
   uint32 uNumOutputSlots;
   uint32 uNumOutputBlockSlots;
   uint32 uNumOutputBlocks;
   uint32 uNumInputBlockSlots;
   uint32 uNumInputBlocks;
   uint32 uHWVersion;
} I2cDev_HwInfoType;
typedef struct I2cDev_State
{
   uint32 uTimeoutUs;
   I2cDev_SequenceIoState eIoState;
   I2cIoResult opResult;
} I2cDev_State;
typedef struct I2cDev_PropertyType
{
   uint8 *virtBlockAddr;
   uint32 uI2cInputClkKhz;
} I2cDev_PropertyType;
struct I2cDev_Device
{
   uint32 devInitState;
   I2cDev_HwInfoType devHwInfo;
   I2cDev_SequenceIoInfo seqInfo;
   I2cDev_QupInfo qupInfo;
   I2cDev_PropertyType props;
   I2cClientConfig clntCfg;
   I2cClientConfig *pLastClntCfg;
};
typedef enum I2cDev_Command
{
   I2CDEV_COMMAND_NULL ,
   I2CDEV_COMMAND_RESET_DEVICE,
} I2cDev_Command;
typedef enum I2cDev_Event
{
   I2CDEV_EVT_ISR,
   I2CDEV_EVT_IST,
   I2CDEV_EVT_BAM,
   I2CDEV_EVT_POLL,
} I2cDev_Event;
int32 I2cDev_Init
(
   I2cDev_Device *pDev,
   I2cDev_PropertyType *pProp
);
int32 I2cDev_DeInit
(
   I2cDev_Device *pDev
);
int32 I2cDev_SubmitSequence
(
   I2cDev_Device *pDev ,
   I2cSequence *pSequence,
   I2cClientConfig *pClntCfg
);
void I2cDev_EventCall
(
   I2cDev_Device *pDev ,
   I2cDev_Event evt ,
   void *pData
);
void I2cDev_GetState
(
   I2cDev_Device *pDev ,
   I2cDev_State *pDevState
);
boolean
I2cDev_IsTransferComplete
(
   I2cDev_Device *pDev
);
int32 I2cDev_AbortSequence
(
   I2cDev_Device *pDev
);
int32 I2cDev_SendCmd
(
   I2cDev_Device *pDev ,
   I2cDev_Command devCmd ,
   void *pCmdData
);
void *qurt_malloc( unsigned int size);
void *qurt_calloc(unsigned int elsize, unsigned int num);
void *qurt_realloc(void *ptr, int newsize);
void qurt_free( void *ptr);
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
typedef union qurt_mutex_aligned8{
    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;
} qurt_mutex_t;
void qurt_mutex_init(qurt_mutex_t *lock);
void qurt_mutex_destroy(qurt_mutex_t *lock);
void qurt_mutex_lock(qurt_mutex_t *lock);
void qurt_mutex_unlock(qurt_mutex_t *lock);
int qurt_mutex_try_lock(qurt_mutex_t *lock);
typedef union {
 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
int qurt_sem_down(qurt_sem_t *sem);
int qurt_sem_try_down(qurt_sem_t *sem);
void qurt_sem_init(qurt_sem_t *sem);
void qurt_sem_destroy(qurt_sem_t *sem);
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
typedef unsigned long long int qurt_pipe_data_t;
typedef struct {
    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;
} qurt_pipe_t;
typedef struct {
  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;
} qurt_pipe_attr_t;
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
void qurt_pipe_destroy(qurt_pipe_t *pipe);
void qurt_pipe_delete(qurt_pipe_t *pipe);
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
int qurt_printf(const char* format, ...);
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;
typedef unsigned int qurt_thread_t;
typedef struct _qurt_thread_attr {
    char name[16];
    unsigned char tcb_partition;
    unsigned char affinity;
    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;
} qurt_thread_attr_t;
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{
    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
void qurt_thread_get_name (char *name, unsigned char max_len);
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
void qurt_thread_stop(void);
int qurt_thread_resume(unsigned int thread_id);
qurt_thread_t qurt_thread_get_id (void);
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
void qurt_thread_set_timetest_id (unsigned short tid);
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
unsigned short qurt_thread_get_timetest_id (void);
void qurt_thread_exit(int status);
int qurt_thread_join(unsigned int tid, int *status);
unsigned int qurt_thread_get_anysignal(void);
int qurt_thread_get_priority (qurt_thread_t threadid);
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
unsigned int qurt_api_version(void);
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
unsigned int qurt_trace_get_marker(void);
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
unsigned int qurt_etm_enable(unsigned int enable_flag);
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
void qurt_profile_reset_idle_pcycles (void);
unsigned long long int qurt_profile_get_thread_pcycles(void);
unsigned long long int qurt_profile_get_thread_tcycles(void);
unsigned long long int qurt_get_core_pcycles(void);
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
void qurt_profile_reset_threadid_pcycles (int thread_id);
void qurt_profile_enable (int enable);
typedef struct {
   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;
} qurt_rmutex2_t;
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
typedef union {
    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;
} qurt_cond_t;
void qurt_cond_init(qurt_cond_t *cond);
void qurt_cond_destroy(qurt_cond_t *cond);
void qurt_cond_signal(qurt_cond_t *cond);
void qurt_cond_broadcast(qurt_cond_t *cond);
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
typedef union {
 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;
} qurt_barrier_t;
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
int qurt_barrier_destroy(qurt_barrier_t *barrier);
int qurt_barrier_wait(qurt_barrier_t *barrier);
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
unsigned int qurt_fastint_deregister(int intno);
unsigned int qurt_isr_register(int intno, void (*fn)(int));
unsigned int qurt_isr_deregister(int intno);
typedef union {
 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;
} qurt_allsignal_t;
void qurt_allsignal_init(qurt_allsignal_t *signal);
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
typedef union {
    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;
} qurt_signal_t;
void qurt_signal_init(qurt_signal_t *signal);
void qurt_signal_destroy(qurt_signal_t *signal);
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
unsigned int qurt_signal_get(qurt_signal_t *signal);
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
typedef qurt_signal_t qurt_anysignal_t;
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
void qurt_rmutex_init(qurt_mutex_t *lock);
void qurt_rmutex_destroy(qurt_mutex_t *lock);
void qurt_rmutex_lock(qurt_mutex_t *lock);
void qurt_rmutex_unlock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
void qurt_pimutex_init(qurt_mutex_t *lock);
void qurt_pimutex_destroy(qurt_mutex_t *lock);
void qurt_pimutex_lock(qurt_mutex_t *lock);
void qurt_pimutex_unlock(qurt_mutex_t *lock);
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
typedef struct {
   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;
   unsigned int queue;
   unsigned int wait_mask;
} qurt_signal2_t;
void qurt_signal2_init(qurt_signal2_t *signal);
void qurt_signal2_destroy(qurt_signal2_t *signal);
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);
int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
int qurt_interrupt_acknowledge(int int_num);
unsigned int qurt_interrupt_deregister(int int_num);
 unsigned int qurt_interrupt_enable(int int_num);
 unsigned int qurt_interrupt_disable(int int_num);
unsigned int qurt_interrupt_status(int int_num, int *status);
unsigned int qurt_interrupt_clear(int int_num);
unsigned int qurt_interrupt_get_registered(void);
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
int qurt_interrupt_raise(unsigned int interrupt_num);
void qurt_interrupt_disable_all(void);
int qurt_isr_subcall(void);
void * qurt_lifo_pop(void *freelist);
void qurt_lifo_push(void *freelist, void *buf);
void qurt_lifo_remove(void *freelist, void *buf);
static inline int qurt_power_shutdown_prepare(void){ return 0;}
int qurt_power_shutdown_enter (int type);
int qurt_power_exit(void);
int qurt_power_apcr_enter (void);
int qurt_power_tcxo_prepare (void);
int qurt_power_tcxo_fail_exit (void);
int qurt_power_tcxo_enter (void);
int qurt_power_tcxo_exit (void);
void qurt_power_override_wait_for_idle(int enable);
void qurt_power_wait_for_idle (void);
void qurt_power_wait_for_active (void);
unsigned int qurt_system_ipend_get (void);
void qurt_system_avscfg_set(unsigned int avscfg_value);
unsigned int qurt_system_avscfg_get(void);
unsigned int qurt_system_vid_get(void);
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );
int qurt_system_tcm_set_size(unsigned int new_size);
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
typedef struct qurt_sysenv_swap_pools {
   unsigned int spoolsize;
   unsigned int spooladdr;
}qurt_sysenv_swap_pools_t;
typedef struct qurt_sysenv_app_heap {
   unsigned int heap_base;
   unsigned int heap_limit;
} qurt_sysenv_app_heap_t ;
typedef struct qurt_sysenv_arch_version {
    unsigned int arch_version;
}qurt_arch_version_t;
typedef struct qurt_sysenv_max_hthreads {
   unsigned int max_hthreads;
}qurt_sysenv_max_hthreads_t;
typedef struct qurt_sysenv_max_pi_prio {
    unsigned int max_pi_prio;
}qurt_sysenv_max_pi_prio_t;
typedef struct qurt_sysenv_timer_hw {
   unsigned int base;
   unsigned int int_num;
}qurt_sysenv_hw_timer_t;
typedef struct qurt_sysenv_procname {
   unsigned int asid;
   char name[64];
}qurt_sysenv_procname_t;
typedef struct qurt_sysenv_stack_profile_count {
   unsigned int count;
}qurt_sysenv_stack_profile_count_t;
typedef struct _qurt_sysevent_error_t
{
    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;
} qurt_sysevent_error_t ;
typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);
unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
void qurt_exception_raise_fatal (void);
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
void qurt_exception_shutdown_fatal2(void);
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);
unsigned int qurt_enable_floating_point_exception(unsigned int mask);
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
void qurt_pmu_set (int reg_id, unsigned int reg_value);
unsigned int qurt_pmu_get (int red_id);
void qurt_pmu_enable (int enable);
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,
        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,
        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,
        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,
        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;
typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;
typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;
typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;
typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;
typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;
struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
typedef struct {
    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;
} qurt_mem_region_attr_t;
typedef struct {
    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];
} qurt_mem_pool_attr_t;
typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;
typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_delete (unsigned int entry_id);
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
unsigned short qurt_tlb_entry_get_available(void);
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
extern qurt_mem_pool_t qurt_mem_default_pool;
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
int qurt_mem_region_delete(qurt_mem_region_t region);
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}
void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
void qurt_l2fetch_disable(void);
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_write(int handle, const void *buf, unsigned len);
int qurt_qdi_read(int handle, void *buf, unsigned len);
int qurt_qdi_close(int handle);
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
extern unsigned long long qurt_sysclock_get_expiry (void);
unsigned long long qurt_sysclock_get_hw_ticks (void);
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;
    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));
    return (unsigned short)ticks;
}
unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
int qurt_spawn_flags(const char * name, int flags);
int qurt_space_switch(int asid);
int qurt_wait(int *status);
int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
typedef struct _qurt_process_attr {
    char name[64];
    int flags;
} qurt_process_attr_t;
int qurt_process_create (qurt_process_attr_t *attr);
int qurt_process_get_id (void);
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
typedef unsigned int mode_t;
int shm_open(const char * name, int oflag, mode_t mode);
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
int shm_close(int fd);
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;
typedef unsigned int qurt_timer_t;
typedef unsigned long long qurt_timer_duration_t;
typedef unsigned long long qurt_timer_time_t;
typedef struct
{
    unsigned int magic;
    qurt_timer_duration_t duration;
    qurt_timer_time_t expiry;
    qurt_timer_duration_t remaining;
    qurt_timer_type_t type;
    unsigned int group;
}
qurt_timer_attr_t;
int qurt_timer_stop (qurt_timer_t timer);
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);
int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
int qurt_timer_delete(qurt_timer_t timer);
int qurt_timer_sleep(qurt_timer_duration_t duration);
int qurt_timer_group_disable (unsigned int group);
int qurt_timer_group_enable (unsigned int group);
void qurt_timer_recover_pc (void);
static inline int qurt_timer_is_init (void) {return 1;};
unsigned long long qurt_timer_get_ticks (void);
int qurt_tls_create_key (int *key, void (*destructor)(void *));
int qurt_tls_set_specific (int key, const void *value);
void *qurt_tls_get_specific (int key);
int qurt_tls_delete_key (int key);
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}
static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}
static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}
int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);
int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_unlock(void);
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_get_mode(void);
int qurt_hvx_get_units(void);
int qurt_hvx_reserve(int num_units);
int qurt_hvx_cancel_reserve(void);
int qurt_hvx_get_lock_val(void);
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;
typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;
typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
unsigned long long qurt_mailbox_get_id(char *name);
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
int qurt_mailbox_delete(unsigned long long mailbox_id);
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};
typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;
typedef struct qurt_island_attr {
    int max_attrs;
    struct qurt_island_attr_resource attrs[1];
} qurt_island_attr_t;
typedef struct {
   int qdi_handle;
} qurt_island_t;
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
void qurt_island_attr_delete (qurt_island_attr_t *attr);
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
int qurt_island_spec_delete (qurt_island_t spec_id);
int qurt_island_enter (qurt_island_t spec_id);
int qurt_island_exit (void);
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
unsigned int qurt_island_get_status (void);
typedef enum I2CSYS_Error
{
   I2CSYS_ERROR_BASE = I2C_RES_ERROR_CLS_DEV_OS,
   I2CSYS_ERROR_TIMEOUT_EVT_CREATE_FAILED,
   I2CSYS_ERROR_CLIENT_EVT_CREATE_FAILED,
   I2CSYS_ERROR_MEM_FREE_FAILED,
   I2CSYS_ERROR_MEM_MALLOC_FAILED,
   I2CSYS_ERROR_PHYS_MEM_MALLOC_FAILED,
   I2CSYS_ERROR_PHYS_MEM_INFO_FAILED,
   I2CSYS_ERROR_MEM_SET_FAILED,
   I2CSYS_ERROR_TIMEOUT_EVT_CLEAR_FAILED,
   I2CSYS_ERROR_CLIENT_EVT_CLEAR_FAILED,
   I2CSYS_ERROR_EVT_CTRL_FAILED,
   I2CSYS_ERROR_EVT_WAIT_FAILED,
   I2CSYS_ERROR_SYNC_CREATE_FAILED,
   I2CSYS_ERROR_SYNC_DESTROY_FAILED,
   I2CSYS_ERROR_INVALID_HANDLE_TYPE,
   I2CSYS_ERROR_NULL_PTR,
} I2CSYS_Error;
typedef struct I2cSys_EventType
{
   qurt_mutex_t i2c_mutex;
} I2cSys_EventType;
typedef struct I2cSys_SyncType
{
   qurt_mutex_t i2c_mutex;
} I2cSys_SyncType;
typedef enum
{
   I2CSYS_RESULT_OK = 0,
   I2CSYS_RESULT_ERROR_BASE = I2C_RES_ERROR_CLS_DEV_OS,
   I2CSYS_RESULT_ERROR_EVT_WAIT_TIMEOUT,
   I2CSYS_RESULT_FIRST_SECTION = I2CSYS_RESULT_ERROR_EVT_WAIT_TIMEOUT,
   I2CSYS_RESULT_SECOND_SECTION = I2CSYS_RESULT_FIRST_SECTION+1000,
} I2cSys_Result;
typedef void* I2CSYS_EVENT_HANDLE;
typedef void* I2CSYS_CRITSECTION_HANDLE;
typedef void* I2CSYS_INTLOCK_HANDLE;
int32
I2cSys_CreateEvent
(
   I2cSys_EventType *pEvent
);
int32
I2cSys_DestroyEvent
(
   I2cSys_EventType *pEvent
);
int32
I2cSys_SetEvent
(
   I2cSys_EventType *pEvent
);
int32
I2cSys_ClearEvent
(
   I2cSys_EventType *pEvent
);
int32
I2cSys_Wait
(
   I2cSys_EventType *pEvent,
   uint32 dwMilliseconds
);
int32
I2cSys_CreateCriticalSection
(
   I2cSys_SyncType *pCritSec
);
int32
I2cSys_EnterCriticalSection
(
   I2cSys_SyncType *pCritSec
);
int32
I2cSys_LeaveCriticalSection
(
   I2cSys_SyncType *pCritSec
);
int32
I2cSys_DestroyCriticalSection
(
   I2cSys_SyncType *pCritSec
);
void
I2cSys_BusyWait
(
   uint32 uTimeMicrosec
);
enum I2cDrv_Error
{
   I2CDRV_ERR_BASE = I2C_RES_ERROR_CLS_I2C_DRV,
   I2CDRV_ERR_INVALID_TRANSFER_POINTER,
   I2CDRV_ERR_INVALID_NUMCOMPLETED_POINTER,
   I2CDRV_ERR_NOT_IMPLEMENTED,
   I2CDRV_ERR_INVALID_BUS_ID,
   I2CDRV_ERR_DEVICE_NOT_INITIALISED,
   I2CDRV_ERR_DEVICE_NOT_POWERED,
   I2CDRV_ERR_DEVICE_BUSY,
   I2CDRV_ERR_NULL_BUS_PTR,
   I2CDRV_ERR_INVALID_BUS_PTR,
   I2CDRV_ERR_OUT_OF_FREE_OBJS,
   I2CDRV_ERR_INVALID_CLIENT_COOKIE,
   I2CDRV_ERR_INVALID_CMD_DATA,
};
typedef enum I2cDrv_OperationalStateType
{
 I2CDRV_OPSTATE_OBJ_FREE = 0x0,
 I2CDRV_OPSTATE_DEV_OPEN = 0x1,
 I2CDRV_OPSTATE_DEV_RW = 0x2,
 I2CDRV_OPSTATE_DEV_BUSY = I2CDRV_OPSTATE_DEV_OPEN | I2CDRV_OPSTATE_DEV_RW,
 I2CDRV_OPSTATE_OBJ_IN_USE = 0x80,
} I2cDrv_OperationalStateType;
typedef enum I2cDrv_PowerStateType
{
 I2CDRV_PWSTATE_OFF = 0x0,
 I2CDRV_PWSTATE_PNOC_ON = 0x1,
 I2CDRV_PWSTATE_I2C_CLKS_ON = 0x2,
} I2cDrv_PowerStateType;
typedef struct I2cDrv_DriverProperty
{
   I2cDrv_I2cBusId eDevId;
   boolean bInterruptBased;
   uint32 uInterruptId;
} I2cDrv_DriverProperty;
typedef struct I2cDrv_DescType I2cDrv_DescType;
typedef struct I2cDrv_DescType* I2cDrv_PDescType;
struct I2cDrv_DescType
{
   uint32 uOpenCnt;
   I2cDev_State devState;
   I2cSys_SyncType serialReqSync;
   I2cSys_SyncType serialOpSync;
   I2cSys_EventType qupEvent;
   int32 opState;
   I2cDrv_PowerStateType pwState;
   I2cDrv_DriverProperty *pDrvProps;
   I2cPlat_DescType plat;
   I2cPlat_PropertyType *pPlatProps;
   I2cDev_Device dev;
   I2cDev_PropertyType *pDevProps;
   I2cDrv_DescType *pNext;
};
extern I2cDrv_DescType *aI2cDev[I2CDRV_I2C_NUM];
extern I2cSys_SyncType i2cDrvSync;
extern boolean bSyncInitialised;
const uint32 i2cDeviceNum = (0);
I2cPlat_PropertyType i2cPlatPropertyArray[] =
{
   { {0x2001c373,0x2001c383 }, 2, "PERIPH_SS", 0x1B5000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP1_APPS_CLK, "gcc_blsp2_ahb_clk", "gcc_blsp2_qup1_i2c_apps_clk", },
};
I2cDev_PropertyType i2cDevPropertyArray[] =
{
   { 0, (19200) },
};
I2cDrv_DriverProperty i2cDrvPropertyArray[] =
{
    { (I2CDRV_I2C_7), (0), (0), },
};
I2cDrv_DescType i2cDrvDescArray[(0)] ;
