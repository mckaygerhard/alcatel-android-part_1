typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef unsigned char boolean;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef enum
{
   DAL_GPIO_MODE_PRIMARY,
   DAL_GPIO_MODE_IO,
   DAL_PLACEHOLDER_DALGpioModeType = 0x7fffffff
}DalGpioModeType;
typedef enum
{
  DAL_GPIO_INACTIVE = 0,
  DAL_GPIO_ACTIVE = 1,
  DAL_PLACEHOLDER_DALGpioStatusType = 0x7fffffff
}DALGpioStatusType;
typedef enum
{
  DAL_GPIO_OWNER_MASTER,
  DAL_GPIO_OWNER_APPS1,
  DAL_PLACEHOLDER_DALGpioOwnerType = 0x7fffffff
}DALGpioOwnerType;
typedef enum
{
  DAL_GPIO_UNLOCKED = 0,
  DAL_GPIO_LOCKED = 1,
  DAL_PLACEHOLDER_DALGpioLockType = 0x7fffffff
}DALGpioLockType;
typedef enum
{
  DAL_GPIO_CLIENT_PWRDB = 0,
  DAL_PLACEHOLDER_DALGpioClientType = 0x7fffffff
}DALGpioClientType;
typedef enum
{
  DAL_GPIO_INPUT = 0,
  DAL_GPIO_OUTPUT = 1,
  DAL_PLACEHOLDER_DALGpioDirectionType = 0x7fffffff
}DALGpioDirectionType;
typedef enum
{
  DAL_GPIO_NO_PULL = 0,
  DAL_GPIO_PULL_DOWN = 0x1,
  DAL_GPIO_KEEPER = 0x2,
  DAL_GPIO_PULL_UP = 0x3,
  DAL_PLACEHOLDER_DALGpioPullType = 0x7fffffff
}DALGpioPullType;
typedef enum
{
  DAL_GPIO_2MA = 0,
  DAL_GPIO_4MA = 0x1,
  DAL_GPIO_6MA = 0x2,
  DAL_GPIO_8MA = 0x3,
  DAL_GPIO_10MA = 0x4,
  DAL_GPIO_12MA = 0x5,
  DAL_GPIO_14MA = 0x6,
  DAL_GPIO_16MA = 0x7,
  DAL_PLACEHOLDER_DALGpioDriveType = 0x7fffffff
}DALGpioDriveType;
typedef enum
{
  DAL_GPIO_LOW_VALUE,
  DAL_GPIO_HIGH_VALUE,
  DAL_PLACEHOLDER_DALGpioValueType = 0x7fffffff
}DALGpioValueType;
typedef uint32 DALGpioSignalType;
typedef uint32 DALGpioIdType;
typedef enum
{
  DAL_GPIO_SLEEP_CONFIG_ALL,
  DAL_GPIO_SLEEP_CONFIG_APPS,
  DAL_GPIO_SLEEP_CONFIG_NUM_MODES,
  DAL_PLACEHOLDER_DALGpioSleepConfigType = 0x7fffffff
}DALGpioSleepConfigType;
typedef enum
{
  DAL_TLMM_GPIO_DISABLE,
  DAL_TLMM_GPIO_ENABLE,
  DAL_PLACEHOLDER_DALGpioEnableType = 0x7fffffff
}DALGpioEnableType;
typedef enum
{
  DAL_TLMM_USB_PORT_SEL,
  DAL_TLMM_PORT_CONFIG_USB,
  DAL_TLMM_PORT_TSIF,
  DAL_TLMM_AUD_PCM,
  DAL_TLMM_UART1,
  DAL_TLMM_UART3,
  DAL_TLMM_LCDC_CFG,
  DAL_TLMM_KEYSENSE_SC_IRQ,
  DAL_TLMM_KEYSENSE_A9_IRQ,
  DAL_TLMM_TCXO_EN,
  DAL_TLMM_SSBI_PMIC,
  DAL_TLMM_PA_RANGE1,
  DAL_TLMM_SPARE_WR_UART_SEL,
  DAL_TLMM_PAD_ALT_FUNC_SDIO_EN,
  DAL_TLMM_SPARE_WR_TCXO_EN,
  DAL_TLMM_SPARE_WR_PA_ON,
  DAL_TLMM_SDC3_CTL,
  DAL_TLMM_SDC4_CTL,
  DAL_TLMM_UIM1_PAD_CTL,
  DAL_TLMM_UIM2_PAD_CTL,
  DAL_TLMM_NUM_PORTS,
  DAL_PLACEHOLDER_DALGpioPortType = 0x7fffffff
}DALGpioPortType;
typedef struct
{
  DALGpioDirectionType eDirection;
  DALGpioPullType ePull;
  DALGpioDriveType eDriveStrength;
}DalTlmm_GpioConfigIdType;
typedef struct
{
  uint32 nGpioNumber;
  uint32 nFunctionSelect;
  DalTlmm_GpioConfigIdType Settings;
  DALGpioValueType eOutputDrive;
}DalTlmm_GpioIdSettingsType;
typedef struct DalTlmm DalTlmm;
struct DalTlmm
{
   struct DalDevice DalDevice;
   DALResult (*ConfigGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*ConfigGpioGroup)(DalDeviceHandle * _h, DALGpioEnableType enable, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*TristateGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*TristateGpioGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*ConfigGpiosForSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*RestoreGpiosFromSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*GetGpioNumber)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, uint32 * number);
   DALResult (*GpioIn)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType* value);
   DALResult (*GpioOut)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType value);
   DALResult (*GpioOutGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_config, uint32 size, DALGpioValueType value);
   DALResult (*SwitchGpioIntOwnership)(DalDeviceHandle * _h, uint32 owner);
   DALResult (*SetTlmmPort)(DalDeviceHandle * _h, DALGpioPortType Port, uint32 value);
   DALResult (*ExternalGpioConfig)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*RequestGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*ReleaseGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*GetGpioOwner)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioOwnerType* owner);
   DALResult (*GetCurrentConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*GetGpioStatus)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioStatusType* status);
   DALResult (*SetInactiveConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType config);
   DALResult (*GetInactiveConfig)(DalDeviceHandle *h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*LockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*UnlockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*IsGpioLocked)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock);
   DALResult (*GpioBypassSleep)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioEnableType enable);
   DALResult (*AttachRemote)(DalDeviceHandle * _h, uint32 processor);
   DALResult (*GetOutput)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value);
   DALResult (*GetGpioId)(DalDeviceHandle * _h, const char* gpio_str, DALGpioIdType* nGpioId);
   DALResult (*ConfigGpioId)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* tGpioSettings);
   DALResult (*ConfigGpioIdInactive)(DalDeviceHandle * _h, DALGpioIdType nGpioId);
   DALResult (*GpioIdOut)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType value);
   DALResult (*GpioIdIn)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *value);
   DALResult (*ReleaseGpioId)(DalDeviceHandle * h, DALGpioIdType nGpioId);
   DALResult (*SelectGpioIdMode)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings);
   DALResult (*GetGpioIdSettings)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings);
   DALResult (*ConfigGpioIdModeIndexed)(DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex);
};
typedef struct DalTlmmHandle DalTlmmHandle;
struct DalTlmmHandle
{
   uint32 dwDalHandleId;
   const DalTlmm * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalTlmm_ConfigGpio(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpio( _h, gpio_config, enable);
}
static __inline DALResult
DalTlmm_ConfigGpioGroup
(
  DalDeviceHandle * _h,
  DALGpioEnableType enable,
  DALGpioSignalType* gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_6(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, enable, gpio_group, size*(sizeof(uint32)));
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioGroup( _h, enable, gpio_group, size);
}
static __inline DALResult
DalTlmm_TristateGpio
(
  DalDeviceHandle* _h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_0(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpio( _h,
                                                   (DALGpioSignalType)gpio_config);
}
static __inline DALResult
DalTlmm_TristateGpioGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, size);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpioGroup( _h, gpio_group, size);
}
static __inline DALResult
DalTlmm_ConfigGpiosForSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpiosForSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpiosForSleep( _h,
                                                            gpio_sleep_config);
}
static __inline DALResult
DalTlmm_RestoreGpiosFromSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RestoreGpiosFromSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h,
        gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RestoreGpiosFromSleep( _h,
                                                      gpio_sleep_config);
}
static __inline DALResult
DalTlmm_GetGpioNumber
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  uint32 *gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioNumber)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioNumber( _h,
                                                     (DALGpioSignalType)gpio_config,
                                                     gpio_number);
}
static __inline DALResult
DalTlmm_GpioIn
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType*value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioIn)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIn( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOut
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOut)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOut( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOutGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOutGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOutGroup( _h,
                                                      gpio_group,
                                                      size, value);
}
static __inline DALResult
DalTlmm_SwitchGpioIntOwnership
(
  DalDeviceHandle *_h,
  uint32 owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SwitchGpioIntOwnership)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SwitchGpioIntOwnership( _h, owner);
}
static __inline DALResult
DalTlmm_SetPort
(
  DalDeviceHandle *_h,
  DALGpioPortType port,
  uint32 value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetTlmmPort)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, port, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetTlmmPort( _h, port, value);
}
static __inline DALResult
DalTlmm_ExternalGpioConfig
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ExternalGpioConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ExternalGpioConfig( _h,
                                    gpio_config, enable);
}
static __inline DALResult
DalTlmm_RequestGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RequestGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RequestGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_ReleaseGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ReleaseGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioOwner
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioOwnerType* owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioOwner)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioOwner( _h, gpio_number, owner);
}
static __inline DALResult
DalTlmm_GetCurrentConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetCurrentConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->GetCurrentConfig( _h, gpio_number, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioStatus
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioStatusType *status
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioStatus)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)status);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioStatus( _h, gpio_number, status);
}
static __inline DALResult
DalTlmm_SetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_GetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_LockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->LockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->LockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_UnlockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->UnlockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->UnlockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_IsGpioLocked(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->IsGpioLocked)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)lock);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->IsGpioLocked( _h, gpio_number, lock);
}
static __inline DALResult
DalTlmm_GpioBypassSleep
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioBypassSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioBypassSleep( _h, gpio_number, enable);
}
static __inline DALResult
DalTlmm_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->AttachRemote)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, processor);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->AttachRemote( _h, processor);
}
static __inline DALResult
DalTlmm_GetOutput(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetOutput)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_number, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetOutput( _h, gpio_number, value);
}
static __inline DALResult
DalTlmm_GetGpioId(DalDeviceHandle * _h, const char* pszGpio, DALGpioIdType* pnGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioId( _h, pszGpio, pnGpioId);
}
static __inline DALResult
DalTlmm_ConfigGpioId(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioId( _h, nGpioId, pUserSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdInactive(DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdInactive( _h, nGpioId);
}
static __inline DALResult
DalTlmm_GpioIdOut(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdOut( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_GpioIdIn(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdIn( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_ReleaseGpioId( DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpioId(_h, nGpioId);
}
static __inline DALResult
DalTlmm_SelectGpioIdMode( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->SelectGpioIdMode(_h, nGpioId, eMode, pUserSettings);
}
static __inline DALResult
DalTlmm_GetGpioIdSettings( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioIdSettings(_h, nGpioId, pGpioSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdModeIndexed( DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex )
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdModeIndexed(_h, nGpioId, nIndex);
}
typedef enum
{
    POWER_INVALID = 0,
    POWER_FULL = (1<<0),
    POWER_075 = (1<<1),
    POWER_050 = (1<<2),
    POWER_025 = (1<<3),
    POWER_OFF = (1<<4),
    _QIDL_PLACEHOLDER_UartPowerType = 0x12345678
} UartPowerType;
typedef enum
{
    EVENT_NONE = 0,
    EVENT_BREAK = (1<<0),
    EVENT_CTS = (1<<1),
    EVENT_DSR = (1<<3),
    EVENT_ERROR = (1<<4),
    EVENT_RX_WAKEUP = (1<<5),
    EVENT_RING = (1<<7),
    EVENT_RLSD = (1<<8),
    EVENT_RX_CHAR = (1<<9),
    EVENT_RX_HUNTCHAR = (1<<10),
    EVENT_TXEMPTY = (1<<11),
    EVENT_BREAK_END = (1<<12),
    _QIDL_PLACEHOLDER_UartLineEventType = 0x12345678
} UartLineEventType;
typedef enum
{
    STATUS_RX_PARITY_ERROR = (1<<0),
    STATUS_RX_FRAME_ERROR = (1<<1),
    STATUS_RX_OVERRUN_ERROR = (1<<2),
    STATUS_RX_BREAK = (1<<3),
    STATUS_RX_HUNT_CHAR_DETECTED = (1<<4),
    STATUS_RX_FULL = (1<<5),
    STATUS_RX_READY = (1<<6),
    STATUS_TX_EMPTY = (1<<7),
    STATUS_TX_READY = (1<<8),
    STATUS_RING_ASSERTED = (1<<16),
    STATUS_RLSD_ASSERTED = (1<<17),
    STATUS_DSR_ASSERTED = (1<<18),
    STATUS_CTS_ASSERTED = (1<<19),
    _QIDL_PLACEHOLDER_UartStatusType = 0x12345678
} UartStatusType;
typedef enum
{
    RTS_DEASSERT,
    RTS_ASSERT,
    RTS_AUTO,
    _QIDL_PLACEHOLDER_UartRtsControlType = 0x12345678
} UartRtsControlType;
typedef enum
{
    CTS_DISABLE,
    CTS_ENABLE,
    _QIDL_PLACEHOLDER_UartCtsControlType = 0x12345678
} UartCtsControlType;
typedef enum
{
  UART_5_BITS_PER_CHAR = (1<<0),
  UART_6_BITS_PER_CHAR = (1<<1),
  UART_7_BITS_PER_CHAR = (1<<2),
  UART_8_BITS_PER_CHAR = (1<<3),
  UART_INVALID_BITS_PER_CHAR = 0x7FFFFFFF
} UartBitsPerCharType;
typedef enum
{
  UART_0_5_STOP_BITS = (1<<0),
  UART_1_0_STOP_BITS = (1<<1),
  UART_1_5_STOP_BITS = (1<<2),
  UART_2_0_STOP_BITS = (1<<3),
  UART_INVALID_STOP_BITS = 0x7FFFFFFF
} UartNumStopBitsType;
typedef enum
{
  UART_NO_PARITY = (1<<0),
  UART_ODD_PARITY = (1<<1),
  UART_EVEN_PARITY = (1<<2),
  UART_SPACE_PARITY = (1<<3),
  UART_INVALID_PARITY = 0x7FFFFFFF
} UartParityModeType;
typedef struct
{
   UartParityModeType ParityMode;
   UartBitsPerCharType BitsPerChar;
   UartNumStopBitsType NumStopBits;
} UartCharFormat;
typedef struct
{
   void *uart_irq;
   void *rx_irq;
   uint32 enable_dma;
   void *Ctxt;
   void (*LineEventNotif)(void *Ctxt, uint32 LineEvents);
   void (*TransmitReadyNotif)(void *Ctxt);
   void (*ReceiveReadyNotif)(void *Ctxt);
} UartInitConfig;
typedef struct
{
   uint32 baud_rate;
   UartCharFormat char_format;
   UartRtsControlType rts_control;
   UartCtsControlType cts_control;
} UartOpenConfig;
typedef struct
{
   DalDevice DalDevice;
   DALResult (*PostInit) (DalDeviceHandle *h, UartInitConfig *init_cfg);
   DALResult (*OpenEx) (DalDeviceHandle *h, UartOpenConfig *open_cfg);
   DALResult (*Read) (DalDeviceHandle *h, uint8 *read_buffer, uint32 buffer_len,
                                                        uint32 *bytes_read, uint32 *overrun_events);
   DALResult (*Write) (DalDeviceHandle *h, const uint8 *write_buffer,
                                                        uint32 buffer_len, uint32 *bytes_written);
   DALResult (*SetRate) (DalDeviceHandle *h, uint32 baud_rate);
   DALResult (*SetCharFormat) (DalDeviceHandle *h, UartCharFormat *char_format);
   DALResult (*PurgeTx) (DalDeviceHandle *h);
   DALResult (*PurgeRx) (DalDeviceHandle *h);
   DALResult (*SetBreak) (DalDeviceHandle *h);
   DALResult (*ClearBreak) (DalDeviceHandle *h);
   DALResult (*TxSingleChar) (DalDeviceHandle *h, uint8 character);
   DALResult (*GetStatus) (DalDeviceHandle *h, uint32 *status);
   DALResult (*TxComplete) (DalDeviceHandle *h, uint32 *tx_complete);
   DALResult (*SetRtsControl) (DalDeviceHandle *h, UartRtsControlType rts_control);
   DALResult (*SetCtsControl) (DalDeviceHandle *h, UartCtsControlType cts_control);
   DALResult (*GetCTS) (DalDeviceHandle *h, uint32 *cts_state);
   DALResult (*PowerCapabilities) (DalDeviceHandle *h, uint32 *power_mask);
   DALResult (*PowerGet) (DalDeviceHandle *h, UartPowerType *power_state);
   DALResult (*PowerSet) (DalDeviceHandle *h, UartPowerType power_state);
   DALResult (*LoopbackSet) (DalDeviceHandle *h, uint32 enabled);
   DALResult (*SetCxm) (DalDeviceHandle *h, uint32 enabled, uint32 sam);
   DALResult (*DumpRegs) (DalDeviceHandle *h);
   DALResult (*GetCxmTxSticky) (DalDeviceHandle *h, uint32 *tx_sticky, uint32 clear);
   DALResult (*CxmTxDirectChar) (DalDeviceHandle *h, uint8 character);
   DALResult (*RxActive) (DalDeviceHandle *h, uint32 *rx_active);
} UartInterface;
static __inline DALResult
DalUart_PostInit(DalDeviceHandle *h, UartInitConfig *init_cfg)
{
   return ((UartInterface *)h->pVtbl)->PostInit(h, init_cfg);
}
static __inline DALResult
DalUart_OpenEx(DalDeviceHandle *h, UartOpenConfig *open_cfg)
{
   return ((UartInterface *)h->pVtbl)->OpenEx(h, open_cfg);
}
static __inline DALResult
DalUart_Read(DalDeviceHandle *h, uint8 *read_buffer, uint32 buffer_len,
             uint32 *bytes_read, uint32 *overrun_events)
{
   return ((UartInterface *)h->pVtbl)->Read(h, read_buffer, buffer_len, bytes_read, overrun_events);
}
static __inline DALResult
DalUart_Write(DalDeviceHandle *h, const uint8 *write_buffer, uint32 buffer_len,
              uint32 *bytes_written)
{
   return ((UartInterface *)h->pVtbl)->Write(h, write_buffer, buffer_len, bytes_written);
}
static __inline DALResult
DalUart_SetRate(DalDeviceHandle *h, uint32 baud_rate)
{
   return ((UartInterface *)h->pVtbl)->SetRate(h, baud_rate);
}
static __inline DALResult
DalUart_SetCharFormat(DalDeviceHandle *h, UartCharFormat *char_format)
{
   return ((UartInterface *)h->pVtbl)->SetCharFormat(h, char_format);
}
static __inline DALResult
DalUart_PurgeTx(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->PurgeTx(h);
}
static __inline DALResult
DalUart_PurgeRx(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->PurgeRx(h);
}
static __inline DALResult
DalUart_SetBreak(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->SetBreak(h);
}
static __inline DALResult
DalUart_ClearBreak(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->ClearBreak(h);
}
static __inline DALResult
DalUart_TxSingleChar(DalDeviceHandle *h, uint8 character)
{
   return ((UartInterface *)h->pVtbl)->TxSingleChar(h, character);
}
static __inline DALResult
DalUart_GetStatus(DalDeviceHandle *h, uint32 *status)
{
   return ((UartInterface *)h->pVtbl)->GetStatus(h, status);
}
static __inline DALResult
DalUart_TxComplete(DalDeviceHandle *h, uint32 *tx_complete)
{
   return ((UartInterface *)h->pVtbl)->TxComplete(h, tx_complete);
}
static __inline DALResult
DalUart_SetRtsControl(DalDeviceHandle *h, UartRtsControlType rts_control)
{
   return ((UartInterface *)h->pVtbl)->SetRtsControl(h, rts_control);
}
static __inline DALResult
DalUart_SetCtsControl(DalDeviceHandle *h, UartCtsControlType cts_control)
{
   return ((UartInterface *)h->pVtbl)->SetCtsControl(h, cts_control);
}
static __inline DALResult
DalUart_GetCTS(DalDeviceHandle *h, uint32 *cts_state)
{
   return ((UartInterface *)h->pVtbl)->GetCTS(h, cts_state);
}
static __inline DALResult
DalUart_PowerCapabilities(DalDeviceHandle *h, uint32 *power_mask)
{
   return ((UartInterface *)h->pVtbl)->PowerCapabilities(h, power_mask);
}
static __inline DALResult
DalUart_PowerGet(DalDeviceHandle *h, UartPowerType *power_state)
{
   return ((UartInterface *)h->pVtbl)->PowerGet(h, power_state);
}
static __inline DALResult
DalUart_PowerSet(DalDeviceHandle *h, UartPowerType power_state)
{
   return ((UartInterface *)h->pVtbl)->PowerSet(h, power_state);
}
static __inline DALResult
DalUart_LoopbackSet(DalDeviceHandle *h, uint32 enabled)
{
   return ((UartInterface *)h->pVtbl)->LoopbackSet(h, enabled);
}
static __inline DALResult
DalUart_SetCxm(DalDeviceHandle *h, uint32 enabled, uint32 sam)
{
   return ((UartInterface *)h->pVtbl)->SetCxm(h, enabled, sam);
}
static __inline DALResult
DalUart_DumpRegs(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->DumpRegs(h);
}
static __inline DALResult
DalUart_GetCxmTxSticky(DalDeviceHandle *h, uint32 *tx_sticky, uint32 clear)
{
   return ((UartInterface *)h->pVtbl)->GetCxmTxSticky(h, tx_sticky, clear);
}
static __inline DALResult
DalUart_CxmTxDirectChar(DalDeviceHandle *h, uint8 character)
{
   return ((UartInterface *)h->pVtbl)->CxmTxDirectChar(h, character);
}
static __inline DALResult
DalUart_RxActive(DalDeviceHandle *h, uint32 *rx_active)
{
   return ((UartInterface *)h->pVtbl)->RxActive(h, rx_active);
}
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum
{
  NPA_RESOURCE_DEFAULT = 0,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,
  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,
  NPA_RESOURCE_VECTOR_STATE = 0x00000004,
  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_REMOTE = 0x00000010,
  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,
  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,
  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,
  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,
  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,
  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,
} npa_resource_attribute;
typedef enum
{
  NPA_NODE_DEFAULT = 0,
  NPA_NODE_NO_LOCK = 0x00000001,
  NPA_NODE_DISABLEABLE = 0x00000002,
  NPA_NODE_FORKABLE = 0x00000004,
} npa_node_attribute;
enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,
  NPA_QUERY_RESOURCE_ATTRIBUTES,
  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};
typedef void* npa_user_data;
typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;
typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;
typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];
  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;
  unsigned int request_attr;
  void (*issue_request)( npa_client_handle client, int new_request );
  struct npa_scheduler_data *request_ptr;
} npa_client;
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;
  const char *name;
  struct npa_resource *resource;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;
  npa_event_callback callback;
  void *reg_data;
  struct npa_event_action *action;
} npa_event;
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );
typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );
typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
  void (*create_client_fcn) ( npa_client * );
  void (*destroy_client_fcn)( npa_client * );
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;
  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;
  struct npa_resource *handle;
} npa_resource_definition;
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;
typedef struct npa_resource
{
  npa_resource_definition *definition;
  npa_node_definition *node;
  unsigned int index;
  npa_client *clients;
  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;
  const npa_resource_plugin *active_plugin;
  npa_resource_state request_state;
  npa_resource_state active_state;
  npa_resource_state internal_state[8];
  npa_resource_state *state_vector;
  npa_resource_state *required_state_vector;
  npa_resource_state *suppressible_state_vector;
  npa_resource_state *suppressible2_state_vector;
  npa_resource_state *semiactive_state_vector;
  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;
  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;
  struct npa_resource_internal_data *_internal;
  unsigned int sequence;
  void *log_handle;
  npa_resource_latency *latency;
} npa_resource;
typedef void* npa_join_token;
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
void npa_define_marker( const char *marker_name );
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
void npa_disable_node( npa_node_definition *node );
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
void npa_resource_lock( npa_resource *resource );
int npa_resource_trylock( npa_resource *resource );
void npa_resource_unlock( npa_resource *resource );
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
unsigned int npa_get_request_attributes( npa_client_handle client );
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
extern const npa_resource_plugin npa_binary_plugin;
extern const npa_resource_plugin npa_max_plugin;
extern const npa_resource_plugin npa_min_plugin;
extern const npa_resource_plugin npa_sum_plugin;
extern const npa_resource_plugin npa_identity_plugin;
extern const npa_resource_plugin npa_always_on_plugin;
extern const npa_resource_plugin npa_impulse_plugin;
extern const npa_resource_plugin npa_or_plugin;
extern const npa_resource_plugin npa_binary_and_plugin;
extern const npa_resource_plugin npa_no_client_plugin;
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);
npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );
npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);
npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);
npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );
npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );
npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
void npa_issue_internal_request( npa_client_handle client );
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
typedef enum {
  ICBID_MASTER_APPSS_PROC = 0,
  ICBID_MASTER_MSS_PROC,
  ICBID_MASTER_MNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC_0 = ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_CNOC_MNOC_MMSS_CFG,
  ICBID_MASTER_CNOC_MNOC_CFG,
  ICBID_MASTER_GFX3D,
  ICBID_MASTER_JPEG,
  ICBID_MASTER_MDP,
  ICBID_MASTER_MDP0 = ICBID_MASTER_MDP,
  ICBID_MASTER_MDPS = ICBID_MASTER_MDP,
  ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P0 = ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P1,
  ICBID_MASTER_VFE,
  ICBID_MASTER_VFE0 = ICBID_MASTER_VFE,
  ICBID_MASTER_CNOC_ONOC_CFG,
  ICBID_MASTER_JPEG_OCMEM,
  ICBID_MASTER_MDP_OCMEM,
  ICBID_MASTER_VIDEO_P0_OCMEM,
  ICBID_MASTER_VIDEO_P1_OCMEM,
  ICBID_MASTER_VFE_OCMEM,
  ICBID_MASTER_LPASS_AHB,
  ICBID_MASTER_QDSS_BAM,
  ICBID_MASTER_SNOC_CFG,
  ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_BIMC_SNOC_0 = ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_CNOC_SNOC,
  ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE0 = ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE1,
  ICBID_MASTER_LPASS_PROC,
  ICBID_MASTER_MSS,
  ICBID_MASTER_MSS_NAV,
  ICBID_MASTER_OCMEM_DMA,
  ICBID_MASTER_PNOC_SNOC,
  ICBID_MASTER_WCSS,
  ICBID_MASTER_QDSS_ETR,
  ICBID_MASTER_USB3,
  ICBID_MASTER_USB3_0 = ICBID_MASTER_USB3,
  ICBID_MASTER_SDCC_1,
  ICBID_MASTER_SDCC_3,
  ICBID_MASTER_SDCC_2,
  ICBID_MASTER_SDCC_4,
  ICBID_MASTER_TSIF,
  ICBID_MASTER_BAM_DMA,
  ICBID_MASTER_BLSP_2,
  ICBID_MASTER_USB_HSIC,
  ICBID_MASTER_BLSP_1,
  ICBID_MASTER_USB_HS,
  ICBID_MASTER_USB_HS1 = ICBID_MASTER_USB_HS,
  ICBID_MASTER_PNOC_CFG,
  ICBID_MASTER_SNOC_PNOC,
  ICBID_MASTER_RPM_INST,
  ICBID_MASTER_RPM_DATA,
  ICBID_MASTER_RPM_SYS,
  ICBID_MASTER_DEHR,
  ICBID_MASTER_QDSS_DAP,
  ICBID_MASTER_SPDM,
  ICBID_MASTER_TIC,
  ICBID_MASTER_SNOC_CNOC,
  ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_GFX3D_GMEM = ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_OVIRT_SNOC,
  ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_SNOC_GVIRT = ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_ONOC_OVIRT,
  ICBID_MASTER_USB_HS2,
  ICBID_MASTER_QPIC,
  ICBID_MASTER_IPA,
  ICBID_MASTER_DSI,
  ICBID_MASTER_MDP1,
  ICBID_MASTER_MDPE = ICBID_MASTER_MDP1,
  ICBID_MASTER_VPU_PROC,
  ICBID_MASTER_VPU,
  ICBID_MASTER_VPU0 = ICBID_MASTER_VPU,
  ICBID_MASTER_CRYPTO_CORE2,
  ICBID_MASTER_PCIE_0,
  ICBID_MASTER_PCIE_1,
  ICBID_MASTER_SATA,
  ICBID_MASTER_UFS,
  ICBID_MASTER_USB3_1,
  ICBID_MASTER_VIDEO_OCMEM,
  ICBID_MASTER_VPU1,
  ICBID_MASTER_VCAP,
  ICBID_MASTER_EMAC,
  ICBID_MASTER_BCAST,
  ICBID_MASTER_MMSS_PROC,
  ICBID_MASTER_SNOC_BIMC_1,
  ICBID_MASTER_SNOC_PCNOC,
  ICBID_MASTER_AUDIO,
  ICBID_MASTER_MM_INT_0,
  ICBID_MASTER_MM_INT_1,
  ICBID_MASTER_MM_INT_2,
  ICBID_MASTER_MM_INT_BIMC,
  ICBID_MASTER_MSS_INT,
  ICBID_MASTER_PCNOC_CFG,
  ICBID_MASTER_PCNOC_INT_0,
  ICBID_MASTER_PCNOC_INT_1,
  ICBID_MASTER_PCNOC_M_0,
  ICBID_MASTER_PCNOC_M_1,
  ICBID_MASTER_PCNOC_S_0,
  ICBID_MASTER_PCNOC_S_1,
  ICBID_MASTER_PCNOC_S_2,
  ICBID_MASTER_PCNOC_S_3,
  ICBID_MASTER_PCNOC_S_4,
  ICBID_MASTER_PCNOC_S_6,
  ICBID_MASTER_PCNOC_S_7,
  ICBID_MASTER_PCNOC_S_8,
  ICBID_MASTER_PCNOC_S_9,
  ICBID_MASTER_QDSS_INT,
  ICBID_MASTER_SNOC_INT_0,
  ICBID_MASTER_SNOC_INT_1,
  ICBID_MASTER_SNOC_INT_BIMC,
  ICBID_MASTER_TCU_0,
  ICBID_MASTER_TCU_1,
  ICBID_MASTER_BIMC_INT_0,
  ICBID_MASTER_BIMC_INT_1,
  ICBID_MASTER_CAMERA,
  ICBID_MASTER_RICA,
  ICBID_MASTER_SNOC_BIMC_2,
  ICBID_MASTER_BIMC_SNOC_1,
  ICBID_MASTER_A0NOC_SNOC,
  ICBID_MASTER_A1NOC_SNOC,
  ICBID_MASTER_A2NOC_SNOC,
  ICBID_MASTER_PIMEM,
  ICBID_MASTER_SNOC_VMEM,
  ICBID_MASTER_CPP,
  ICBID_MASTER_CNOC_A1NOC,
  ICBID_MASTER_PNOC_A1NOC,
  ICBID_MASTER_HMSS,
  ICBID_MASTER_PCIE_2,
  ICBID_MASTER_ROTATOR,
  ICBID_MASTER_VENUS_VMEM,
  ICBID_MASTER_DCC,
  ICBID_MASTER_MCDMA,
  ICBID_MASTER_PCNOC_INT_2,
  ICBID_MASTER_PCNOC_INT_3,
  ICBID_MASTER_PCNOC_INT_4,
  ICBID_MASTER_PCNOC_INT_5,
  ICBID_MASTER_PCNOC_INT_6,
  ICBID_MASTER_PCNOC_S_5,
  ICBID_MASTER_SENSORS_AHB,
  ICBID_MASTER_SENSORS_PROC,
  ICBID_MASTER_QSPI,
  ICBID_MASTER_VFE1,
  ICBID_MASTER_SNOC_INT_2,
  ICBID_MASTER_SMMNOC_BIMC,
  ICBID_MASTER_CRVIRT_A1NOC,
  ICBID_MASTER_XM_USB_HS1,
  ICBID_MASTER_XI_USB_HS1,
  ICBID_MASTER_COUNT,
  ICBID_MASTER_SIZE = 0x7FFFFFFF
} ICBId_MasterType;
typedef enum {
  ICBID_SLAVE_EBI1 = 0,
  ICBID_SLAVE_APPSS_L2,
  ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_BIMC_SNOC_0 = ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_CAMERA_CFG,
  ICBID_SLAVE_DISPLAY_CFG,
  ICBID_SLAVE_OCMEM_CFG,
  ICBID_SLAVE_CPR_CFG,
  ICBID_SLAVE_CPR_XPU_CFG,
  ICBID_SLAVE_MISC_CFG,
  ICBID_SLAVE_MISC_XPU_CFG,
  ICBID_SLAVE_VENUS_CFG,
  ICBID_SLAVE_GFX3D_CFG,
  ICBID_SLAVE_MMSS_CLK_CFG,
  ICBID_SLAVE_MMSS_CLK_XPU_CFG,
  ICBID_SLAVE_MNOC_MPU_CFG,
  ICBID_SLAVE_ONOC_MPU_CFG,
  ICBID_SLAVE_MNOC_BIMC,
  ICBID_SLAVE_SERVICE_MNOC,
  ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_GMEM = ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_SERVICE_ONOC,
  ICBID_SLAVE_APPSS,
  ICBID_SLAVE_LPASS,
  ICBID_SLAVE_USB3,
  ICBID_SLAVE_USB3_0 = ICBID_SLAVE_USB3,
  ICBID_SLAVE_WCSS,
  ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_BIMC_0 = ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_CNOC,
  ICBID_SLAVE_IMEM,
  ICBID_SLAVE_OCIMEM = ICBID_SLAVE_IMEM,
  ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_GVIRT = ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SNOC_PCNOC = ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SERVICE_SNOC,
  ICBID_SLAVE_QDSS_STM,
  ICBID_SLAVE_SDCC_1,
  ICBID_SLAVE_SDCC_3,
  ICBID_SLAVE_SDCC_2,
  ICBID_SLAVE_SDCC_4,
  ICBID_SLAVE_TSIF,
  ICBID_SLAVE_BAM_DMA,
  ICBID_SLAVE_BLSP_2,
  ICBID_SLAVE_USB_HSIC,
  ICBID_SLAVE_BLSP_1,
  ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_USB_HS1 = ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_PDM,
  ICBID_SLAVE_PERIPH_APU_CFG,
  ICBID_SLAVE_PNOC_MPU_CFG,
  ICBID_SLAVE_PRNG,
  ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_PCNOC_SNOC = ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_SERVICE_PNOC,
  ICBID_SLAVE_CLK_CTL,
  ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_PCNOC_MSS = ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_SECURITY,
  ICBID_SLAVE_TCSR,
  ICBID_SLAVE_TLMM,
  ICBID_SLAVE_CRYPTO_0_CFG,
  ICBID_SLAVE_CRYPTO_1_CFG,
  ICBID_SLAVE_IMEM_CFG,
  ICBID_SLAVE_MESSAGE_RAM,
  ICBID_SLAVE_BIMC_CFG,
  ICBID_SLAVE_BOOT_ROM,
  ICBID_SLAVE_CNOC_MNOC_MMSS_CFG,
  ICBID_SLAVE_PMIC_ARB,
  ICBID_SLAVE_SPDM_WRAPPER,
  ICBID_SLAVE_DEHR_CFG,
  ICBID_SLAVE_MPM,
  ICBID_SLAVE_QDSS_CFG,
  ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_CX_CFG = ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_QDSS_APU_CFG,
  ICBID_SLAVE_CNOC_MNOC_CFG,
  ICBID_SLAVE_SNOC_MPU_CFG,
  ICBID_SLAVE_CNOC_ONOC_CFG,
  ICBID_SLAVE_PNOC_CFG,
  ICBID_SLAVE_SNOC_CFG,
  ICBID_SLAVE_EBI1_DLL_CFG,
  ICBID_SLAVE_PHY_APU_CFG,
  ICBID_SLAVE_EBI1_PHY_CFG,
  ICBID_SLAVE_RPM,
  ICBID_SLAVE_CNOC_SNOC,
  ICBID_SLAVE_SERVICE_CNOC,
  ICBID_SLAVE_OVIRT_SNOC,
  ICBID_SLAVE_OVIRT_OCMEM,
  ICBID_SLAVE_USB_HS2,
  ICBID_SLAVE_QPIC,
  ICBID_SLAVE_IPS_CFG,
  ICBID_SLAVE_DSI_CFG,
  ICBID_SLAVE_USB3_1,
  ICBID_SLAVE_PCIE_0,
  ICBID_SLAVE_PCIE_1,
  ICBID_SLAVE_PSS_SMMU_CFG,
  ICBID_SLAVE_CRYPTO_2_CFG,
  ICBID_SLAVE_PCIE_0_CFG,
  ICBID_SLAVE_PCIE_1_CFG,
  ICBID_SLAVE_SATA_CFG,
  ICBID_SLAVE_SPSS_GENI_IR,
  ICBID_SLAVE_UFS_CFG,
  ICBID_SLAVE_AVSYNC_CFG,
  ICBID_SLAVE_VPU_CFG,
  ICBID_SLAVE_USB_PHY_CFG,
  ICBID_SLAVE_RBCPR_MX_CFG,
  ICBID_SLAVE_PCIE_PARF,
  ICBID_SLAVE_VCAP_CFG,
  ICBID_SLAVE_EMAC_CFG,
  ICBID_SLAVE_BCAST_CFG,
  ICBID_SLAVE_KLM_CFG,
  ICBID_SLAVE_DISPLAY_PWM,
  ICBID_SLAVE_GENI,
  ICBID_SLAVE_SNOC_BIMC_1,
  ICBID_SLAVE_AUDIO,
  ICBID_SLAVE_CATS_0,
  ICBID_SLAVE_CATS_1,
  ICBID_SLAVE_MM_INT_0,
  ICBID_SLAVE_MM_INT_1,
  ICBID_SLAVE_MM_INT_2,
  ICBID_SLAVE_MM_INT_BIMC,
  ICBID_SLAVE_MMU_MODEM_XPU_CFG,
  ICBID_SLAVE_MSS_INT,
  ICBID_SLAVE_PCNOC_INT_0,
  ICBID_SLAVE_PCNOC_INT_1,
  ICBID_SLAVE_PCNOC_M_0,
  ICBID_SLAVE_PCNOC_M_1,
  ICBID_SLAVE_PCNOC_S_0,
  ICBID_SLAVE_PCNOC_S_1,
  ICBID_SLAVE_PCNOC_S_2,
  ICBID_SLAVE_PCNOC_S_3,
  ICBID_SLAVE_PCNOC_S_4,
  ICBID_SLAVE_PCNOC_S_6,
  ICBID_SLAVE_PCNOC_S_7,
  ICBID_SLAVE_PCNOC_S_8,
  ICBID_SLAVE_PCNOC_S_9,
  ICBID_SLAVE_PRNG_XPU_CFG,
  ICBID_SLAVE_QDSS_INT,
  ICBID_SLAVE_RPM_XPU_CFG,
  ICBID_SLAVE_SNOC_INT_0,
  ICBID_SLAVE_SNOC_INT_1,
  ICBID_SLAVE_SNOC_INT_BIMC,
  ICBID_SLAVE_TCU,
  ICBID_SLAVE_BIMC_INT_0,
  ICBID_SLAVE_BIMC_INT_1,
  ICBID_SLAVE_RICA_CFG,
  ICBID_SLAVE_SNOC_BIMC_2,
  ICBID_SLAVE_BIMC_SNOC_1,
  ICBID_SLAVE_PNOC_A1NOC,
  ICBID_SLAVE_SNOC_VMEM,
  ICBID_SLAVE_A0NOC_SNOC,
  ICBID_SLAVE_A1NOC_SNOC,
  ICBID_SLAVE_A2NOC_SNOC,
  ICBID_SLAVE_A0NOC_CFG,
  ICBID_SLAVE_A0NOC_MPU_CFG,
  ICBID_SLAVE_A0NOC_SMMU_CFG,
  ICBID_SLAVE_A1NOC_CFG,
  ICBID_SLAVE_A1NOC_MPU_CFG,
  ICBID_SLAVE_A1NOC_SMMU_CFG,
  ICBID_SLAVE_A2NOC_CFG,
  ICBID_SLAVE_A2NOC_MPU_CFG,
  ICBID_SLAVE_A2NOC_SMMU_CFG,
  ICBID_SLAVE_AHB2PHY,
  ICBID_SLAVE_CAMERA_THROTTLE_CFG,
  ICBID_SLAVE_DCC_CFG,
  ICBID_SLAVE_DISPLAY_THROTTLE_CFG,
  ICBID_SLAVE_DSA_CFG,
  ICBID_SLAVE_DSA_MPU_CFG,
  ICBID_SLAVE_SSC_MPU_CFG,
  ICBID_SLAVE_HMSS_L3,
  ICBID_SLAVE_LPASS_SMMU_CFG,
  ICBID_SLAVE_MMAGIC_CFG,
  ICBID_SLAVE_PCIE20_AHB2PHY,
  ICBID_SLAVE_PCIE_2,
  ICBID_SLAVE_PCIE_2_CFG,
  ICBID_SLAVE_PIMEM,
  ICBID_SLAVE_PIMEM_CFG,
  ICBID_SLAVE_QDSS_RBCPR_APU_CFG,
  ICBID_SLAVE_RBCPR_CX,
  ICBID_SLAVE_RBCPR_MX,
  ICBID_SLAVE_SMMU_CPP_CFG,
  ICBID_SLAVE_SMMU_JPEG_CFG,
  ICBID_SLAVE_SMMU_MDP_CFG,
  ICBID_SLAVE_SMMU_ROTATOR_CFG,
  ICBID_SLAVE_SMMU_VENUS_CFG,
  ICBID_SLAVE_SMMU_VFE_CFG,
  ICBID_SLAVE_SSC_CFG,
  ICBID_SLAVE_VENUS_THROTTLE_CFG,
  ICBID_SLAVE_VMEM,
  ICBID_SLAVE_VMEM_CFG,
  ICBID_SLAVE_QDSS_MPU_CFG,
  ICBID_SLAVE_USB3_PHY_CFG,
  ICBID_SLAVE_IPA_CFG,
  ICBID_SLAVE_PCNOC_INT_2,
  ICBID_SLAVE_PCNOC_INT_3,
  ICBID_SLAVE_PCNOC_INT_4,
  ICBID_SLAVE_PCNOC_INT_5,
  ICBID_SLAVE_PCNOC_INT_6,
  ICBID_SLAVE_PCNOC_S_5,
  ICBID_SLAVE_QSPI,
  ICBID_SLAVE_A1NOC_MS_MPU_CFG,
  ICBID_SLAVE_A2NOC_MS_MPU_CFG,
  ICBID_SLAVE_MODEM_Q6_SMMU_CFG,
  ICBID_SLAVE_MSS_MPU_CFG,
  ICBID_SLAVE_MSS_PROC_MS_MPU_CFG,
  ICBID_SLAVE_SKL,
  ICBID_SLAVE_SNOC_INT_2,
  ICBID_SLAVE_SMMNOC_BIMC,
  ICBID_SLAVE_CRVIRT_A1NOC,
  ICBID_SLAVE_COUNT,
  ICBID_SLAVE_SIZE = 0x7FFFFFFF
} ICBId_SlaveType;
typedef enum
{
   ICBARB_ERROR_SUCCESS = 0,
   ICBARB_ERROR_UNSUPPORTED,
   ICBARB_ERROR_INVALID_ARG,
   ICBARB_ERROR_INVALID_MASTER,
   ICBARB_ERROR_NO_ROUTE_TO_SLAVE,
   ICBARB_ERROR_VECTOR_LENGTH_MISMATCH,
   ICBARB_ERROR_OUT_OF_MEMORY,
   ICBARB_ERROR_UNKNOWN,
   ICBARB_ERROR_REQUEST_REJECTED,
   ICBARB_ERROR_MAX,
   ICBARB_ERROR_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_ErrorType;
typedef enum
{
   ICBARB_STATE_NORMAL = 0,
   ICBARB_STATE_OVERSUBSCRIBED,
   ICBARB_STATE_MAX,
   ICBARB_STATE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_StateType;
typedef struct
{
   ICBId_MasterType eMaster;
   ICBId_SlaveType eSlave;
} ICBArb_MasterSlaveType;
typedef struct
{
   npa_callback callback;
   ICBArb_MasterSlaveType *aMasterSlave;
} ICBArb_CreateClientVectorType;
typedef enum
{
   ICBARB_REQUEST_TYPE_1,
   ICBARB_REQUEST_TYPE_2,
   ICBARB_REQUEST_TYPE_3,
   ICBARB_REQUEST_TYPE_1_TIER_3,
   ICBARB_REQUEST_TYPE_2_TIER_3,
   ICBARB_REQUEST_TYPE_3_TIER_3,
   ICBARB_REQUEST_TYPE_1_LAT,
   ICBARB_REQUEST_TYPE_2_LAT,
   ICBARB_REQUEST_TYPE_3_LAT,
   ICBARB_REQUEST_TYPE_MAX,
   ICBARB_REQUEST_TYPE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_RequestTypeType;
typedef struct
{
   uint64 uDataBurst;
   uint32 uTransferTimeUs;
   uint32 uPeriodUs;
   uint32 uLatencyNs;
} ICBArb_Request1Type;
typedef struct
{
   uint64 uThroughPut;
   uint32 uUsagePercentage;
   uint32 uLatencyNs;
} ICBArb_Request2Type;
typedef struct
{
   uint64 uIb;
   uint64 uAb;
   uint32 uLatencyNs;
} ICBArb_Request3Type;
typedef union
{
   ICBArb_Request1Type type1;
   ICBArb_Request2Type type2;
   ICBArb_Request3Type type3;
} ICBArb_RequestUnionType;
typedef struct
{
   ICBArb_RequestTypeType arbType;
   ICBArb_RequestUnionType arbData;
} ICBArb_RequestType;
typedef enum
{
   ICBARB_QUERY_RESOURCE_COUNT = 0,
   ICBARB_QUERY_RESOURCE_NAMES,
   ICBARB_QUERY_SLAVE_PORT_COUNT,
   ICBARB_QUERY_MAX,
   ICBARB_QUERY_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_QueryTypeType;
typedef struct
{
   npa_client_handle client;
   uint32 uNumResource;
} ICBArb_QueryResourceCountType;
typedef struct
{
   npa_client_handle client;
   const char **aResourceNames;
   uint32 uNumResource;
} ICBArb_QueryResourceNamesType;
typedef struct
{
   ICBId_SlaveType eSlaveId;
   uint32 uNumPorts;
} ICBArb_QuerySlavePortCountType;
typedef union
{
   ICBArb_QueryResourceCountType resourceCount;
   ICBArb_QueryResourceNamesType resourceNames;
   ICBArb_QuerySlavePortCountType slavePortCount;
} ICBArb_QueryUnionType;
typedef struct
{
   ICBArb_QueryTypeType queryType;
   ICBArb_QueryUnionType queryData;
} ICBArb_QueryType;
void icbarb_init( void );
ICBArb_CreateClientVectorType *icbarb_fill_client_vector(
  ICBArb_CreateClientVectorType *psVector,
  ICBArb_MasterSlaveType *aMasterSlave,
  npa_callback callback);
npa_client_handle icbarb_create_client(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave);
npa_client_handle icbarb_create_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
npa_client_handle icbarb_create_suppressible_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
ICBArb_ErrorType icbarb_issue_request(
   npa_client_handle client,
   ICBArb_RequestType *aRequest,
   uint32 uNumRequest);
void icbarb_complete_request( npa_client_handle client );
void icbarb_destroy_client( npa_client_handle client );
ICBArb_ErrorType icbarb_query( ICBArb_QueryType *pQuery );
<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
     BLSP_UART1_TX_DATA (((0) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_RX_DATA (((1) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_CTS_N (((2) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_RFR_N (((3) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_TX_DATA (((4) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_RX_DATA (((5) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_CTS_N (((6) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_RFR_N (((7) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_TX_DATA (((8) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_RX_DATA (((9) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_CTS_N (((10) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_RFR_N (((11) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_TX_DATA (((17) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_RX_DATA (((18) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_CTS_N (((19) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_RFR_N (((20) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_TX_DATA (((21) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_RX_DATA (((22) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_CTS_N (((23) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_RFR_N (((24) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_TX_DATA (((25) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_RX_DATA (((26) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_CTS_N (((27) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_RFR_N (((28) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_TX_DATA (((41) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_RX_DATA (((42) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_CTS_N (((43) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_RFR_N (((44) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_TX_DATA (((45) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_RX_DATA (((46) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_CTS_N (((47) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_RFR_N (((48) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_TX_DATA (((49) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_RX_DATA (((50) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_CTS_N (((51) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_RFR_N (((52) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_TX_DATA (((53) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_RX_DATA (((54) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_CTS_N (((55) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_RFR_N (((56) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_TX_DATA (((81) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_RX_DATA (((82) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_CTS_N (((83) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_RFR_N (((84) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_TX_DATA (((85) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_RX_DATA (((86) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_CTS_N (((87) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_RFR_N (((88) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     PnocIBval or PnocABval value = required bandwidth(in terms of bytes)
                                  = required bandwidth/8(in terms of bits).
     In case of UART we can support the maximum baudrate 4Mbps, so we are voting for the max
     bandwidth(500000).
     The BAM IRQ for BLSP1 is the only BAM IRQ that is routed to MSS. You must use a UART on BLSP1
     if you want to use it with BAM.
======================================================================================================= -->
<driver name="Uart">
  <global_def>
    <string name="blsp1_ahb_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_ahb_clk </string>
    <string name="blsp2_ahb_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_ahb_clk </string>
    <string name="uartbam_1_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart1_apps_clk </string>
    <string name="uartbam_2_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart2_apps_clk </string>
    <string name="uartbam_3_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart3_apps_clk </string>
    <string name="uartbam_4_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart4_apps_clk </string>
    <string name="uartbam_5_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart5_apps_clk </string>
    <string name="uartbam_6_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart6_apps_clk </string>
    <string name="uartbam_7_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart1_apps_clk </string>
    <string name="uartbam_8_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart2_apps_clk </string>
    <string name="uartbam_9_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart3_apps_clk </string>
    <string name="uartbam_10_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart4_apps_clk </string>
    <string name="uartbam_11_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart5_apps_clk </string>
    <string name="uartbam_12_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart6_apps_clk </string>
    <string name="uartbam_1_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[1] </string>
    <string name="uartbam_1_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[1] </string>
    <string name="uartbam_1_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[1] </string>
    <string name="uartbam_1_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[1] </string>
    <string name="uartbam_2_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[2] </string>
    <string name="uartbam_2_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[2] </string>
    <string name="uartbam_2_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[2] </string>
    <string name="uartbam_2_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[2] </string>
    <string name="uartbam_3_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[3] </string>
    <string name="uartbam_3_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[3] </string>
    <string name="uartbam_3_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[3] </string>
    <string name="uartbam_3_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[3] </string>
    <string name="uartbam_4_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[4] </string>
    <string name="uartbam_4_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[4] </string>
    <string name="uartbam_4_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[4] </string>
    <string name="uartbam_4_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[4] </string>
    <string name="uartbam_5_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[5] </string>
    <string name="uartbam_5_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[5] </string>
    <string name="uartbam_5_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[5] </string>
    <string name="uartbam_5_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[5] </string>
    <string name="uartbam_6_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[6] </string>
    <string name="uartbam_6_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[6] </string>
    <string name="uartbam_6_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[6] </string>
    <string name="uartbam_6_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[6] </string>
    <string name="uartbam_7_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[7] </string>
    <string name="uartbam_7_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[7] </string>
    <string name="uartbam_7_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[7] </string>
    <string name="uartbam_7_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[7] </string>
    <string name="uartbam_8_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[8] </string>
    <string name="uartbam_8_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[8] </string>
    <string name="uartbam_8_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[8] </string>
    <string name="uartbam_8_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[8] </string>
    <string name="uartbam_9_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[9] </string>
    <string name="uartbam_9_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[9] </string>
    <string name="uartbam_9_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[9] </string>
    <string name="uartbam_9_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[9] </string>
    <string name="uartbam_10_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[10] </string>
    <string name="uartbam_10_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[10] </string>
    <string name="uartbam_10_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[10] </string>
    <string name="uartbam_10_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[10] </string>
    <string name="uartbam_11_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[11] </string>
    <string name="uartbam_11_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[11] </string>
    <string name="uartbam_11_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[11] </string>
    <string name="uartbam_11_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[11] </string>
    <string name="uartbam_12_tx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_tx[12] </string>
    <string name="uartbam_12_rx_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rx[12] </string>
    <string name="uartbam_12_cts_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_cts_n[12] </string>
    <string name="uartbam_12_rfr_name" type=DALPROP_DATA_TYPE_STRING> blsp_uart_rfr_n[12] </string>
  </global_def>
  <!-- =================================================================== -->
  <!-- UARTBAM1 -->
  <!-- =================================================================== -->
  <device id=0x02001000>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x756f000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000001 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM2 -->
  <!-- =================================================================== -->
  <device id=0x02001001>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7570000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000002 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM3 -->
  <!-- =================================================================== -->
  <device id=0x02001002>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7571000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000004 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_rx_name </props>
    <!-- Flow control lines are not used for the ADSP UART
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_rfr_name </props>
    -->
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <!-- Flow control lines are not used for the ADSP UART
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    -->
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM4 -->
  <!-- =================================================================== -->
  <device id=0x02001003>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7572000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000008 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
</device>
  <!-- =================================================================== -->
  <!-- UARTBAM5 -->
  <!-- =================================================================== -->
  <device id=0x02001004>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7573000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000010 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM6 -->
  <!-- =================================================================== -->
  <device id=0x02001005>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7574000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000020 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM7 -->
  <!-- =================================================================== -->
  <device id=0x02001006>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75af000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000040 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM8 -->
  <!-- =================================================================== -->
  <device id=0x02001007>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75b0000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000080 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM9 -->
  <!-- =================================================================== -->
  <device id=0x02001008>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75b1000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000100 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM10 -->
  <!-- =================================================================== -->
  <device id=0x02001009>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75b2000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000200 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM11 -->
  <!-- =================================================================== -->
  <device id=0x0200100A>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75b3000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000400 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM12 -->
  <!-- =================================================================== -->
  <device id=0x0200100B>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75b4000 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="DetectBrk" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="UartIntSelBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7ab040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x00000800 </props>
    <props name="PnocVoteEnable" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
    <props name="PnocIBval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocABval" type=DALPROP_ATTR_TYPE_UINT32> 500000 </props>
    <props name="PnocArbMaster" type=DALPROP_ATTR_TYPE_UINT32> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=DALPROP_ATTR_TYPE_UINT32> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_tx_name </props>
    <props name="GpioRxDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_rx_name </props>
    <props name="GpioCtsDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_cts_name </props>
    <props name="GpioRfrDataName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
</driver>
