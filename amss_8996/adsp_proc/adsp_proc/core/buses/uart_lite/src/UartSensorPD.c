/*==================================================================================================

FILE: Uart.c

DESCRIPTION: This module provides the driver Software for the UART.

                           Copyright (c) 2013-2014 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================
                                            DESCRIPTION
====================================================================================================

GLOBAL FUNCTIONS:
   Uart_Deinit
   Uart_Init
   Uart_Receive
   Uart_Transmit

==================================================================================================*/
/*==================================================================================================
Edit History

$Header: //components/rel/core.adsp/2.7/buses/uart_lite/src/UartSensorPD.c#1 $

==================================================================================================*/

/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/
#include "DDIUartLite.h"
#include "DALSys.h"
#include "Uart.h"
#include "Uart_log.h"

/*==================================================================================================
                                              TYPEDEFS
==================================================================================================*/
typedef struct
{
   DALDEVICEHANDLE      dal_device_handle;
   DALSYSWorkLoopHandle workloop_handle;
   DALSYSEventHandle    event_handle;
   UART_CALLBACK        client_cb;
   void                *client_cb_data;
   uint32               bytes_available;
   EventCbCtxt          cb_ctxt;
} UART_SENSOR_CONTEXT;

/*==================================================================================================
                                          LOCAL VARIABLES
==================================================================================================*/
#define UART_LOGGING_FILE_ID 30
#define MAX_ID_LENGTH        40
#define MAX_PORTS            3

char port_string_id[MAX_PORTS][MAX_ID_LENGTH] = {"UartMainPort",
                                                 "UartSecondPort", 
                                                 "UartThirdPort",
                                                };

/*==================================================================================================
                                     LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
static void      deinit_workloop(UART_SENSOR_CONTEXT *uart_ctxt);
static DALResult init_workloop(UART_SENSOR_CONTEXT *uart_ctxt);
static DALResult uart_isr_workloop(DALSYSEventHandle event, void *cb_data);

/*==================================================================================================
                                          LOCAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: init_workloop

DESCRIPTION:

==================================================================================================*/
static DALResult init_workloop(UART_SENSOR_CONTEXT *uart_ctxt)
{
   DALResult result;

   result = DALSYS_RegisterWorkLoop( 0,
                                     1,
                                     &uart_ctxt->workloop_handle,
                                     NULL );
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DALSYS_RegisterWorkLoop failed");
      goto error;
   }

   result = DALSYS_EventCreate( DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                &uart_ctxt->event_handle,
                                NULL );
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DALSYS_EventCreate failed");
      goto error;
   }

   result = DALSYS_AddEventToWorkLoop(  uart_ctxt->workloop_handle,
                                        uart_isr_workloop,
                                        uart_ctxt,
                                        uart_ctxt->event_handle,
                                        NULL );
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DALSYS_AddEventToWorkLoop failed");
      goto error;
   }

   uart_ctxt->cb_ctxt.cb_event = uart_ctxt->event_handle;

   return UART_SUCCESS;

error:
   return UART_ERROR;
}

/*==================================================================================================

FUNCTION: deinit_workloop

DESCRIPTION:

==================================================================================================*/
static void deinit_workloop(UART_SENSOR_CONTEXT *uart_ctxt)
{
   if (uart_ctxt->event_handle)    { DALSYS_DestroyObject(uart_ctxt->event_handle);    }
   if (uart_ctxt->workloop_handle) { DALSYS_DestroyObject(uart_ctxt->workloop_handle); }
}

/*==================================================================================================

FUNCTION: uart_isr_workloop

DESCRIPTION:

==================================================================================================*/
static DALResult uart_isr_workloop(DALSYSEventHandle event, void *cb_data)
{
   UART_SENSOR_CONTEXT *uart_ctxt = (UART_SENSOR_CONTEXT *)cb_data;
   UART_CALLBACK        client_cb = uart_ctxt->client_cb;
   EventCbCtxt         *cb_ctxt   = &uart_ctxt->cb_ctxt;

   if (client_cb)
   {
      client_cb(cb_ctxt->intr_event, cb_ctxt->bytes_available, uart_ctxt->client_cb_data); 
   }

   return DAL_SUCCESS;
}

/*==================================================================================================
                                          GLOBAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================

FUNCTION: Uart_deinit

DESCRIPTION:

==================================================================================================*/
UartResult Uart_deinit(UartHandle h)
{
   UART_SENSOR_CONTEXT *uart_ctxt = NULL;

   if (NULL == h)
   {
      UART_LOG_0(ERROR,"Calling Uart_deinit with a NULL handle.");
      return UART_ERROR;
   }

   uart_ctxt = (UART_SENSOR_CONTEXT *)h;

   if (uart_ctxt->dal_device_handle)
   {
      DalDevice_Close(uart_ctxt->dal_device_handle);
      DalDevice_Detach(uart_ctxt->dal_device_handle);
   }

   deinit_workloop(uart_ctxt); 

   DALSYS_Free(uart_ctxt); 

   return UART_SUCCESS;
}


/*==================================================================================================

FUNCTION: Uart_init

DESCRIPTION:

==================================================================================================*/
UartResult Uart_init(UartHandle *h, UartPortID id)
{
   UART_SENSOR_CONTEXT *uart_ctxt = NULL;
   DALResult            result;

   if (NULL == h)
   {
      UART_LOG_0(ERROR,"Calling Uart_init with a NULL handle.");
      goto error;
   }

   if (id >= MAX_PORTS)
   {
      UART_LOG_0(ERROR,"Calling Uart_init with a invalid device id.");
      goto error;

   }
   result = DALSYS_Malloc(sizeof(UART_SENSOR_CONTEXT), (void **)(&uart_ctxt));
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DALSYS_Malloc failed");
      goto error;
   }

   memset(uart_ctxt, 0, sizeof(UART_SENSOR_CONTEXT));

   result = DAL_UartLiteDeviceAttach(port_string_id[id], &uart_ctxt->dal_device_handle);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DAL_UartLiteDeviceAttach failed");
      goto error;
   }

   result = DalDevice_Open(uart_ctxt->dal_device_handle, DAL_OPEN_EXCLUSIVE);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DalDevice_Open failed");
      goto error;
   }

   result = init_workloop(uart_ctxt);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"init_workloop failed");
      goto error;
   }

   *h = (UartHandle *)uart_ctxt;

   return UART_SUCCESS;

error:
   Uart_deinit(uart_ctxt);
   return UART_ERROR;
}

/*==================================================================================================

FUNCTION: Uart_receive

DESCRIPTION:

==================================================================================================*/
uint32 Uart_receive(UartHandle h, char *buf, uint32 bytes_to_rx)
{
   UART_SENSOR_CONTEXT *uart_ctxt;
   DALResult result;
   uint32 bytes_read;

   UART_LOG_0(INFO,"+Uart_receive");

   if (NULL == h)
   {
      UART_LOG_0(ERROR,"Calling Uart_receive with a NULL handle.");
      return 0;
   }

   uart_ctxt = (UART_SENSOR_CONTEXT *)h;

   result = DalUartLite_Receive(uart_ctxt->dal_device_handle, buf, bytes_to_rx, &bytes_read);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DalUartLite_Receive failed");
      return 0;
   }

   UART_LOG_0(INFO,"-Uart_receive");

   return bytes_read;
}


/*==================================================================================================

FUNCTION: Uart_register_callback

DESCRIPTION:

==================================================================================================*/
UartResult Uart_register_event_callback(UartHandle h, UART_CALLBACK client_callback, void *cb_data)
{
   UART_SENSOR_CONTEXT *uart_ctxt;
   DALResult result;
   
   if (NULL == h)
   {
      UART_LOG_0(ERROR,"Calling Uart_register_callback with a NULL handle");
      return UART_ERROR;
   }

   if (NULL == client_callback)
   {
      UART_LOG_0(ERROR,"Calling Uart_register_callback with a NULL callback function");
      return UART_ERROR;
   }

   uart_ctxt = (UART_SENSOR_CONTEXT *)h;

   uart_ctxt->client_cb      = client_callback;
   uart_ctxt->client_cb_data = cb_data;

   result = DalUartLite_RegisterEventCb(uart_ctxt->dal_device_handle, &uart_ctxt->cb_ctxt);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DalUartLite_RegisterEventCb failed");
      return UART_ERROR;
   }

   return UART_SUCCESS;
}


/*==================================================================================================

FUNCTION: Uart_transmit

DESCRIPTION:

==================================================================================================*/
uint32 Uart_transmit(UartHandle h, char *buf, uint32 bytes_to_tx)
{
   UART_SENSOR_CONTEXT *uart_ctxt;
   uint32 bytes_sent;
   DALResult result;

   if (NULL == h)
   {
      UART_LOG_0(ERROR,"Calling Uart_transmit with a NULL handle.");
      return 0;
   }

   uart_ctxt = (UART_SENSOR_CONTEXT *)h;

   result = DalUartLite_Transmit(uart_ctxt->dal_device_handle, buf, bytes_to_tx, &bytes_sent);
   if (result != DAL_SUCCESS)
   {
      UART_LOG_0(ERROR,"DalUartLite_Receive failed");
      return 0;
   }

   return bytes_sent;
}
