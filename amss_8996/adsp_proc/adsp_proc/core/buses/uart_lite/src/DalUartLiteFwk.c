/*==================================================================================================

FILE: DalUartFwk.c

DESCRIPTION: Interface between UART driver and DAL framework

                           Copyright (c) 2009-2013 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header: //components/rel/core.adsp/2.7/buses/uart_lite/src/DalUartLiteFwk.c#1 $

==================================================================================================*/
/*==================================================================================================
                                            DESCRIPTION
====================================================================================================

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "DALDeviceId.h"
#include "DALFramework.h"
#include "DDIUartLite.h"
#include "DalUartLiteFwk.h"

/*==================================================================================================
                                     LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

static DALResult UartLite_DeviceAttach     (const char *pszDevName, DALDEVICEID device_id, 
                                            DalDeviceHandle **phandle);
static DALResult UartLite_DeviceDeInit     (DalDeviceHandle *h);
static uint32    UartLite_DeviceDetach     (DalDeviceHandle *h);
static DALResult UartLite_DeviceInfo       (DalDeviceHandle *h, DalDeviceInfo *info, 
                                            uint32 infoSize);
static DALResult UartLite_DeviceInit       (DalDeviceHandle *h);
static DALResult UartLite_DevicePowerEvent (DalDeviceHandle *h, DalPowerCmd PowerCmd, 
                                            DalPowerDomain PowerDomain);
static DALResult UartLite_DeviceSysRequest (DalDeviceHandle *h, DalSysReq request, 
                                            const void *src_buf, uint32 src_len,
                                            void *dest_buf, uint32 dest_len, uint32 *dest_req);

/*==================================================================================================
                                       LOCAL/GLOBAL VARIABLES
==================================================================================================*/


// This global variable is exposed to the DAL framework. 

const DALREG_DriverInfo
DALUartLite_DriverInfo = { UartLite_DeviceAttach, 0, NULL };

// The UART interface is a function table that provides the external interface
// to the driver.  All calls into the driver must go through this table.

static const UartLiteInterface uartlite_interface =
{
   {
      UartLite_DeviceAttach,
      UartLite_DeviceDetach,
      UartLite_DeviceInit,
      UartLite_DeviceDeInit,
      UartLite_DeviceOpen,
      UartLite_DeviceClose,
      UartLite_DeviceInfo,
      UartLite_DevicePowerEvent,
      UartLite_DeviceSysRequest
   },

   UartLite_Receive,
   UartLite_RegisterEventCb,
   UartLite_Transmit,
};

/*==================================================================================================
                                          LOCAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: UartLite_DeviceAttach

DESCRIPTION:
         
==================================================================================================*/
static DALResult UartLite_DeviceAttach(const char *pszDevName, DALDEVICEID device_id, 
                                       DalDeviceHandle **phandle)
{
   DalUartLiteHandle *uart_handle;
   DALResult ret_val = DAL_SUCCESS;

   *phandle = NULL;  // set returned handle to NULL in case of failure below

   if ( DALSYS_Malloc(sizeof(DalUartLiteHandle), (void**) &uart_handle) != DAL_SUCCESS )
   {
      ret_val = DAL_ERROR; goto exit;
   }

   memset(uart_handle, 0, sizeof(DalUartLiteHandle));

   uart_handle->dwDalHandleId = DALDEVICE_INTERFACE_HANDLE_ID;
   uart_handle->pVtbl = &uartlite_interface;
   uart_handle->pClientCtxt = &uart_handle->client_ctxt;
   uart_handle->pClientCtxt->pDALDevCtxt = &uart_handle->device_ctxt;
   uart_handle->pClientCtxt->pDALDevCtxt->dwRefs = 1;
   uart_handle->pClientCtxt->pDALDevCtxt->strDeviceName = pszDevName;

   *phandle = (DalDeviceHandle *)uart_handle;
exit:
   return ret_val;
}

/*==================================================================================================

FUNCTION: UartLite_DeviceDeInit

DESCRIPTION:

==================================================================================================*/
static DALResult UartLite_DeviceDeInit(DalDeviceHandle *h)
{
   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_DeviceDetach

DESCRIPTION:

==================================================================================================*/
static uint32 UartLite_DeviceDetach(DalDeviceHandle *h)
{
   DalUartLiteHandle *uart_handle = (DalUartLiteHandle *)h;

   DALSYS_Free(uart_handle);

   return 0;
}

/*==================================================================================================

FUNCTION: UartLite_DeviceInfo

DESCRIPTION:

==================================================================================================*/
static DALResult UartLite_DeviceInfo(DalDeviceHandle *h, DalDeviceInfo *info, uint32 infoSize)
{
   info->Version = DALUARTLITE_INTERFACE_VERSION;
   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_DeviceInit

DESCRIPTION:

==================================================================================================*/
static DALResult UartLite_DeviceInit(DalDeviceHandle *h)
{
   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_DevicePowerEvent

DESCRIPTION:

==================================================================================================*/
static DALResult UartLite_DevicePowerEvent(DalDeviceHandle *h, DalPowerCmd PowerCmd, 
                                           DalPowerDomain PowerDomain)
{
   return DAL_SUCCESS;

}

/*==================================================================================================

FUNCTION: UartLite_DeviceSysRequest

DESCRIPTION:

==================================================================================================*/
static DALResult UartLite_DeviceSysRequest(DalDeviceHandle *h, DalSysReq request,
                                           const void *src_buf, uint32 src_len,
                                           void *dest_buf, uint32 dest_len, uint32 *dest_req)
{
   return DAL_SUCCESS;
}

