# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/buses/uart_lite/config/dal_uart_adsp_8996.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/buses/uart_lite/config/dal_uart_adsp_8996.xml" 2
<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
======================================================================================================= -->

<driver name="UartLite">

  <!-- =================================================================== -->
  <!-- TARGET SPECIFIC UART PROPERTIES -->
  <!-- =================================================================== -->
  <device id="UartMainPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>

  <device id="UartSecondPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>

  <device id="UartThirdPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>


</driver>
