<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
======================================================================================================= -->
<driver name="UartLite">
  <!-- =================================================================== -->
  <!-- TARGET SPECIFIC UART PROPERTIES -->
  <!-- =================================================================== -->
  <device id="UartMainPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>
  <device id="UartSecondPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>
  <device id="UartThirdPort">
      <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32> 0x1 </props>
  </device>
</driver>
