#ifndef __DDIUARTLITE_H__
#define __DDIUARTLITE_H__
/*==================================================================================================

FILE: DDIUartLite.h

DESCRIPTION: External interface to the UART DAL

                               Copyright (c) 2014 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header: //components/rel/core.adsp/2.7/buses/uart_lite/inc/DDIUartLite.h#1 $

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "DalDevice.h"
#include "Uart.h"

/*==================================================================================================
                                             CONSTANTS
==================================================================================================*/

#define DALUARTLITE_INTERFACE_VERSION DALINTERFACE_VERSION(1,0)

/*==================================================================================================
                                               MACROS
==================================================================================================*/

#define DAL_UartLiteDeviceAttach(DevName, hDalDevice) \
        DAL_StringDeviceAttachEx(NULL, DevName, DALUARTLITE_INTERFACE_VERSION, hDalDevice)

/*==================================================================================================
                                            ENUMERATIONS
==================================================================================================*/

/*==================================================================================================
                                             STRUCTURES
==================================================================================================*/

typedef struct
{
   DALSYSEventHandle  cb_event;
   uint32             bytes_available; 
   UartEvent          intr_event;
}EventCbCtxt;

typedef struct
{
   DalDevice DalDevice;
   DALResult (*Receive)         (DalDeviceHandle *h, uint32 unused, void *read_buffer, 
                                                     uint32 buffer_len, uint32 *bytes_read);
   DALResult (*RegisterEventCb) (DalDeviceHandle *h, EventCbCtxt *cb_ctxt, uint32 unused);
   DALResult (*Transmit)        (DalDeviceHandle *h, uint32 unused, void *write_buffer,
                                                     uint32 buffer_len, uint32 *bytes_written);
} UartLiteInterface;

/*==================================================================================================
                                        FUNCTION PROTOTYPES
==================================================================================================*/


/*!
 * \brief Retrieves data available in receive buffer.
 *
 * Should be called from client in response to call of client's registered
 * receive-ready notification callback.
 *
 * \param in DalDeviceHandle Handle to UART returned by attach
 * \param in read_buffer Buffer to be filled with received data
 * \param in buffer_len Length of buffer pointed to by pReadBuffer
 * \param in bytes_read Buffer to be filled with length of data actually read
 * \return DAL_SUCCESS|DAL_ERROR, See DALStdErr.h
 * \note
 * \warning
 */
static __inline DALResult
DalUartLite_Receive(DalDeviceHandle *h, void *read_buffer, uint32 buffer_len,
                    uint32 *bytes_read)
{
   if(DALISREMOTEHANDLE(h))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)DALPREPREMOTEHANDLE(h);
      return hRemote->pVtbl->FCN_12(DALVTBLIDX(((UartLiteInterface *)h->pVtbl), Receive ), 
                                    h, 0, read_buffer, buffer_len, bytes_read);
   }

   return ((UartLiteInterface *)h->pVtbl)->Receive(h, 0, read_buffer, buffer_len, bytes_read);
}

/*!
 * \brief Sets the current baud rate for device
 *
 * Sets the current baud rate, changing the device's clock source and divisor setting as needed
 *
 * \param in DalDeviceHandle Handle to UART returned by attach
 * \param in cb_ctxt Call back context that contains event handle, bytes available and event
 * \return DAL_SUCCESS|DAL_ERROR, See DALStdErr.h
 * \note
 * \warning
 */
static __inline DALResult
DalUartLite_RegisterEventCb(DalDeviceHandle *h, EventCbCtxt *cb_ctxt)
{
   if(DALISREMOTEHANDLE(h))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)DALPREPREMOTEHANDLE(h);
      return hRemote->pVtbl->FCN_5(DALVTBLIDX(((UartLiteInterface *)h->pVtbl), RegisterEventCb),
                                   h, (void *)cb_ctxt, 0);
   }

   return ((UartLiteInterface *)h->pVtbl)->RegisterEventCb(h, (void *)cb_ctxt, 0);
}

/*!
 * \brief Sends data from supplied buffer.
 *
 * Should be called from client for initial transmission of a quantity of data
 * and in response to call of client's registered transmit-ready notification
 * callback for any follow-on transmissions
 *
 * \param in DalDeviceHandle Handle to UART returned by attach
 * \param in write_buffer Buffer containing data to be sent
 * \param in buffer_len Quantity of data pointed to by write_buffer
 * \param in bytes_written Buffer to be filled with length of data actually transmitted
 * \return DAL_SUCCESS|DAL_ERROR, See DALStdErr.h
 * \note
 * \warning
 */
static __inline DALResult
DalUartLite_Transmit(DalDeviceHandle *h, void *write_buffer, uint32 buffer_len,
                     uint32 *bytes_written)
{
   if(DALISREMOTEHANDLE(h))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)DALPREPREMOTEHANDLE(h);
      return hRemote->pVtbl->FCN_12(DALVTBLIDX(((UartLiteInterface *)h->pVtbl), Transmit ), 
                                    h, 0, write_buffer, buffer_len, bytes_written);
   }

   return ((UartLiteInterface *)h->pVtbl)->Transmit(h, 0, write_buffer, buffer_len, bytes_written);
}
#endif  // __DDIUART_H__
