#include "../config/devcfg_slimbus_adsp_8996.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef unsigned char boolean;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef unsigned long int bool32;
typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;
struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};
struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};
struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};
struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};
typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};
typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};
typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);
DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);
uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);
uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);
void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);
void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);
DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);
DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);
DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);
DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);
DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);
void memory_barrier(void);
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);
DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);
uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;
void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);
void DALFW_MPUnLock(DALFW_MPLOCK *pLock);
uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
typedef enum
{
    BAM_SUCCESS = 0x0,
    BAM_FAILED = 0x1,
    BAM_INVALID_CONFIG = 0x2,
    BAM_INVALID_BUFSIZE = 0x3,
    BAM_POLL_NO_RESULT = 0x4,
    BAM_PIPE_DISABLED = 0x5,
    BAM_PIPE_NOT_EMPTY = 0x6,
    BAM_NOT_ENOUGH_SPACE = 0x7
}bam_status_type;
typedef enum
{
   BAM_O_DESC_DONE = 0x00000001,
   BAM_O_INACTIVE = 0x00000002,
   BAM_O_WAKEUP = 0x00000004,
   BAM_O_OUT_OF_DESC = 0x00000008,
   BAM_O_ERROR = 0x00000010,
   BAM_O_EOT = 0x00000020,
   BAM_O_RST_ERROR = 0x00000040,
   BAM_O_HRESP_ERROR = 0x00000080,
   BAM_O_STREAMING = 0x00010000,
   BAM_O_POLL = 0x001000000,
   BAM_O_NO_Q = 0x002000000,
   BAM_O_WAKEUP_IS_ONESHOT = 0x004000000,
   BAM_O_ACK_TRANSFERS = 0x00800000,
   BAM_O_AUTO_ENABLE = 0x01000000,
   BAM_O_ACK_LAST_ONLY = 0x02000000,
   BAM_O_PIPE_NO_INIT = 0x04000000,
   BAM_O_IS_32_BIT = 0x7fffffff
} bam_options_type;
typedef enum
{
   BAM_EVENT_INVALID = 0,
   BAM_EVENT_EOT,
   BAM_EVENT_DESC_DONE,
   BAM_EVENT_OUT_OF_DESC,
   BAM_EVENT_WAKEUP,
   BAM_EVENT_FLOWOFF,
   BAM_EVENT_INACTIVE,
   BAM_EVENT_ERROR,
   BAM_EVENT_HRESP_ERROR,
   BAM_EVENT_RST_ERROR,
   BAM_EVENT_EMPTY,
   BAM_EVENT_IN_PROGRESS,
   BAM_EVENT_DESC_PROCESSED,
   BAM_EVENT_MAX,
   BAM_EVENT_IS_32_BIT = 0x7fffffff
} bam_event_type;
typedef enum
{
   BAM_IOVEC_FLAG_INT = 0x800,
   BAM_IOVEC_FLAG_EOT = 0x400,
   BAM_IOVEC_FLAG_EOB = 0x200,
   BAM_IOVEC_FLAG_NWD = 0x100,
   BAM_IOVEC_FLAG_CMD = 0x080,
   BAM_IOVEC_FLAG_LCK = 0x040,
   BAM_IOVEC_FLAG_UNLCK = 0x020,
   BAM_IOVEC_FLAG_IMM_CMD = 0x010,
   BAM_IOVEC_FLAG_NO_SUBMIT = 0x008,
   BAM_IOVEC_FLAG_DEFAULT = 0x001
}bam_iovec_options_type;
typedef enum
{
    BAM_DIR_PRODUCER = 0x1,
    BAM_DIR_CONSUMER = 0x0
}bam_dir_type;
typedef enum
{
  BAM_MODE_BAM2BAM = 0x0,
  BAM_MODE_SYSTEM = 0x1
}bam_mode_type;
typedef enum
{
    BAM_IRQ_HRESP_ERR_EN = 0x2,
    BAM_IRQ_ERR_EN = 0x4,
    BAM_IRQ_EMPTY_EN = 0x8,
    BAM_IRQ_TIMER_EN = 0x10
}bam_irq_options_type;
typedef enum
{
   BAM_TIMER_OP_CONFIG = 0,
   BAM_TIMER_OP_RESET,
   BAM_TIMER_OP_READ,
   BAM_TIMER_OP_IS_32_BIT = 0x7fffffff
} bam_timer_oper_type;
typedef enum
{
    BAM_TIMER_MODE_ONESHOT = 0,
    BAM_TIMER_MODE_IS_32_BIT = 0x7fffffff
} bam_timer_mode_type;
typedef enum
{
    BAM_CE_OP_WRITE = 0,
    BAM_CE_OP_READ = 1
}bam_ce_oper_type;
typedef enum
{
    BAM_DUMP_TOP = 0x1,
    BAM_DUMP_PIPE = 0x2
}bam_dump_type;
typedef enum
{
    BAM_INVOKE_CB_DEFAULT = 0,
    BAM_INVOKE_CB_IN_POLL = 1
}bam_cb_mode_type;
typedef DALSYSMemAddr bam_vaddr;
typedef DALSYSMemAddr bam_paddr;
typedef struct _bamconfig
{
  bam_paddr bam_pa;
  bam_vaddr bam_va;
  uint32 bam_irq;
  uint32 bam_irq_mask;
  uint32 sum_thresh;
  uint32 options;
  uint32 bam_mti_irq_pa;
} bam_config_type;
typedef struct _PipeConfig
{
      uint32 options ;
      bam_dir_type dir;
      bam_mode_type mode;
      bam_vaddr desc_base_va;
      bam_paddr desc_base_pa;
      uint32 desc_size;
      uint16 evt_thresh;
      uint32 lock_group;
      bam_paddr peer_base_pa;
      bam_paddr data_base_pa;
      uint32 peer_pipe_num;
      bam_vaddr data_base_va;
      uint32 data_size;
}bam_pipe_config_type;
typedef struct _coredumpconfig
{
    void *user_buffer;
    uint32 buf_size;
    uint32 pipe_mask;
    uint32 peer_pipe_mask;
    void *descfifo_buf;
    uint32 descfifo_size;
    void *datafifo_buf;
    uint32 datafifo_size;
    void *option;
} bam_coredump_config_type;
typedef struct
{
  uint32 buf_pa;
  uint32 buf_size : 16;
  uint32 buf_pa_msb :4;
  uint32 flags : 12;
} bam_iovec_type;
typedef struct
{
    uint32 reg_addr : 24;
    uint32 command : 8;
    uint32 data ;
    uint32 mask;
    uint32 reserved;
} bam_ce_type;
typedef struct _bam_result_type
{
    void *cb_data;
    bam_event_type event;
    union
    {
       struct
       {
           bam_iovec_type iovec;
           uint32 accum_size_bytes;
           void *xfer_cb_data;
       } xfer;
      struct
      {
         uint32 status;
         bam_paddr address;
         uint32 data;
         uint32 bus_ctrls;
      } error;
   } data;
   bam_cb_mode_type cb_mode;
} bam_result_type;
typedef DALSYSMemAddr bam_handle;
typedef void (*bam_callback_func_type)(bam_result_type bam_result);
typedef struct
{
    bam_callback_func_type func;
    void *data;
}bam_callback_type;
typedef struct
{
    bam_timer_oper_type op;
    bam_timer_mode_type mode;
    uint32 timeout_ms;
} bam_timer_ctrl_type;
typedef struct
{
    uint32 curr_timer;
} bam_timer_rslt_type;
typedef struct
{
    uint32 num_pipes;
    uint32 bam_header_buffer_size;
    uint32 bam_buffer_size;
    uint32 bam_pipe_buffer_size;
    uint32 bam_max_desc_size;
}bam_info_type;
void bam_fill_ce(bam_vaddr ce_base, uint32 index ,bam_paddr reg, uint32 cmd, uint64 data, uint32 mask);
bam_status_type bam_get_info(bam_handle bamhandle, bam_info_type *info);
bam_handle bam_init( bam_config_type *bam_cfg, bam_callback_type *bam_cb);
bam_status_type bam_secure_init(bam_config_type *bam_cfg);
bam_status_type bam_deinit(bam_handle handle, uint8 disable_bam);
bam_status_type bam_reset(bam_handle handle);
bam_status_type bam_setirqmode(bam_handle bamhandle, uint32 irq_en,uint32 irq_mask);
bam_status_type bam_pipe_isirqmode(bam_handle pipehandle, uint32* irq_en);
bam_handle bam_pipe_init(bam_handle bamhandle, uint32 pipe_num, bam_pipe_config_type *pipe_cfg, bam_callback_type *pipe_cb);
bam_status_type bam_pipe_deinit(bam_handle pipehandle);
bam_status_type bam_pipe_enable(bam_handle pipehandle);
bam_status_type bam_pipe_disable(bam_handle pipehandle);
bam_status_type bam_pipe_setirqmode(bam_handle pipehandle, uint32 irq_en, uint32 pipe_cfg_opts);
bam_status_type bam_pipe_transfer(bam_handle pipehandle, bam_paddr buf_pa, uint16 buf_size, uint16 xfer_opts, void *user_data);
bam_status_type bam_pipe_bulktransfer(bam_handle pipehandle, bam_paddr buf_pa, uint32 buf_size, uint16 xfer_opts, void *user_data);
uint32 bam_pipe_getfreecount(bam_handle pipehandle);
uint32 bam_pipe_isempty(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_event(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_zlt_event(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_event_sys(bam_handle pipehandle);
bam_status_type bam_pipe_poll(bam_handle pipehandle, bam_result_type *result);
bam_status_type bam_pipe_get_current_descriptor_info(bam_handle pipehandle, bam_result_type *result);
bam_status_type bam_pipe_timerctrl(bam_handle pipehandle, bam_timer_ctrl_type *timer_ctrl, bam_timer_rslt_type *timer_rslt);
bam_status_type bam_core_dump(bam_handle bamhandle, bam_coredump_config_type *coredump_cfg);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum {
    MMPM_STATUS_SUCCESS,
    MMPM_STATUS_FAILED,
    MMPM_STATUS_NOMEMORY,
    MMPM_STATUS_VERSIONNOTSUPPORT,
    MMPM_STATUS_BADCLASS,
    MMPM_STATUS_BADSTATE,
    MMPM_STATUS_BADPARM,
    MMPM_STATUS_INVALIDFORMAT,
    MMPM_STATUS_UNSUPPORTED,
    MMPM_STATUS_RESOURCENOTFOUND,
    MMPM_STATUS_BADMEMPTR,
    MMPM_STATUS_BADHANDLE,
    MMPM_STATUS_RESOURCEINUSE,
    MMPM_STATUS_NOBANDWIDTH,
    MMPM_STATUS_NULLPOINTER,
    MMPM_STATUS_NOTINITIALIZED,
    MMPM_STATUS_RESOURCENOTREQUESTED,
    MMPM_STATUS_CORERESOURCENOTAVAILABLE,
    MMPM_STATUS_MAX,
    MMPM_STATUS_FORCE32BITS = 0x7FFFFFFF
} MmpmStatusType;
typedef MmpmStatusType MMPM_STATUS;
typedef enum {
    MMPM_CORE_ID_NONE = 0,
    MMPM_CORE_ID_2D_GRP = 1,
    MMPM_CORE_ID_3D_GRP = 2,
    MMPM_CORE_ID_MDP = 3,
    MMPM_CORE_ID_VCODEC = 4,
    MMPM_CORE_ID_VPE = 5,
    MMPM_CORE_ID_VFE = 6,
    MMPM_CORE_ID_MIPICSI = 7,
    MMPM_CORE_ID_SENSOR = 8,
    MMPM_CORE_ID_JPEGD = 9,
    MMPM_CORE_ID_JPEGE = 10,
    MMPM_CORE_ID_FABRIC = 11,
    MMPM_CORE_ID_IMEM = 12,
    MMPM_CORE_ID_SMMU = 13,
    MMPM_CORE_ID_ROTATOR = 14,
    MMPM_CORE_ID_TV = 15,
    MMPM_CORE_ID_DSI = 16,
    MMPM_CORE_ID_AUDIOIF = 17,
    MMPM_CORE_ID_GMEM = 18,
    MMPM_CORE_ID_LPASS_START = 100,
    MMPM_CORE_ID_LPASS_ADSP = 101,
    MMPM_CORE_ID_LPASS_CORE = 102,
    MMPM_CORE_ID_LPASS_LPM = 103,
    MMPM_CORE_ID_LPASS_DML = 104,
    MMPM_CORE_ID_LPASS_AIF = 105,
    MMPM_CORE_ID_LPASS_SLIMBUS = 106,
    MMPM_CORE_ID_LPASS_MIDI = 107,
    MMPM_CORE_ID_LPASS_AVSYNC = 108,
    MMPM_CORE_ID_LPASS_HWRSMP = 109,
    MMPM_CORE_ID_LPASS_SRAM = 110,
    MMPM_CORE_ID_LPASS_DCODEC = 111,
    MMPM_CORE_ID_LPASS_SPDIF = 112,
    MMPM_CORE_ID_LPASS_HDMIRX = 113,
    MMPM_CORE_ID_LPASS_HDMITX = 114,
    MMPM_CORE_ID_LPASS_SIF = 115,
    MMPM_CORE_ID_LPASS_BSTC = 116,
    MMPM_CORE_ID_LPASS_ADSP_HVX = 117,
    MMPM_CORE_ID_LPASS_END,
    MMPM_CORE_ID_MAX,
    MMPM_CORE_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmCoreIdType;
typedef enum {
    MMPM_CALLBACK_EVENT_ID_NONE,
    MMPM_CALLBACK_EVENT_ID_IDLE,
    MMPM_CALLBACK_EVENT_ID_BUSY,
    MMPM_CALLBACK_EVENT_ID_THERMAL,
    MMPM_CALLBACK_EVENT_ID_COMPLETE,
    MMPM_CALLBACK_EVENT_ID_MAX,
    MMPM_CALLBACK_EVENT_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmCallbackEventIdType;
typedef struct {
    MmpmCallbackEventIdType eventId;
    uint32 clientId;
    uint32 callbackDataSize;
    void* callbackData;
} MmpmCallbackParamType;
typedef struct {
    uint32 reqTag;
    MMPM_STATUS result;
}MmpmCompletionCallbackDataType;
typedef enum{
    MMPM_CORE_INSTANCE_NONE,
    MMPM_CORE_INSTANCE_0,
    MMPM_CORE_INSTANCE_1,
    MMPM_CORE_INSTANCE_2,
    MMPM_CORE_INSTANCE_MAX,
    MMPM_CORE_INSTANCE_FORCE32BITS = 0x7FFFFFFF
} MmpmCoreInstanceIdType;
typedef struct {
    uint32 rev;
    MmpmCoreIdType coreId;
    MmpmCoreInstanceIdType instanceId;
    char *pClientName;
    uint32 pwrCtrlFlag;
    uint32 callBackFlag;
    uint32 (*MMPM_Callback)(MmpmCallbackParamType *pCbParam);
    uint32 cbFcnStackSize;
} MmpmRegParamType;
typedef enum {
    MMPM_RSC_ID_NONE = 0,
    MMPM_RSC_ID_POWER = 1,
    MMPM_RSC_ID_VREG = 2,
    MMPM_RSC_ID_REG_PROG = 3,
    MMPM_RSC_ID_CORE_CLK = 4,
    MMPM_RSC_ID_CORE_CLK_DOMAIN = 5,
    MMPM_RSC_ID_MEM_BW = 6,
    MMPM_RSC_ID_AXI_EN = 7,
    MMPM_RSC_ID_MIPS = 8,
    MMPM_RSC_ID_SLEEP_LATENCY = 9,
    MMPM_RSC_ID_ACTIVE_STATE = 10,
    MMPM_RSC_ID_PMIC_GPIO = 11,
    MMPM_RSC_ID_RESET = 12,
    MMPM_RSC_ID_MIPS_EXT = 13,
    MMPM_RSC_ID_GENERIC_BW = 14,
    MMPM_RSC_ID_THERMAL = 15,
    MMPM_RSC_ID_MEM_POWER = 16,
    MMPM_RSC_ID_GENERIC_BW_EXT = 17,
    MMPM_RSC_ID_MPPS = 18,
    MMPM_RSC_ID_MAX ,
    MMPM_RSC_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmRscIdType;
typedef enum {
    MMPM_REG_PROG_NONE,
    MMPM_REG_PROG_NORM,
    MMPM_REG_PROG_FAST,
    MMPM_REG_PROG_MAX,
    MMPM_REG_PROG_FORCE32BITS = 0x7FFFFFFF
} MmpmRegProgMatchType;
typedef enum {
    MMPM_CLK_ID_2D_GRP_NONE,
    MMPM_CLK_ID_2D_GRP,
    MMPM_CLK_ID_2D_GRP_MAX,
    MMPM_CLK_ID_2D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkId2dGrpType;
typedef enum {
    MMPM_CLK_ID_3D_GRP_NONE,
    MMPM_CLK_ID_3D_GRP,
    MMPM_CLK_ID_3D_GRP_MAX,
    MMPM_CLK_ID_3D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkId3dGrpType;
typedef enum {
    MMPM_CLK_ID_MDP_NONE,
    MMPM_CLK_ID_MDP,
    MMPM_CLK_ID_MDP_VSYNC,
    MMPM_CLK_ID_MDP_LUT,
    MMPM_CLK_ID_MDP_MAX,
    MMPM_CLK_ID_MDP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdMdpType;
typedef enum {
    MMPM_CLK_ID_VCODEC_NONE,
    MMPM_CLK_ID_VCODEC,
    MMPM_CLK_ID_VCODEC_MAX,
    MMPM_CLK_ID_VCODEC_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVcodecType;
typedef enum {
    MMPM_CLK_ID_VPE_NONE,
    MMPM_CLK_ID_VPE,
    MMPM_CLK_ID_VPE_MAX,
    MMPM_CLK_ID_VPE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVpeType;
typedef enum {
    MMPM_CLK_ID_VFE_NONE,
    MMPM_CLK_ID_VFE,
    MMPM_CLK_ID_VFE_MAX,
    MMPM_CLK_ID_VFE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVfeType;
typedef enum {
    MMPM_CLK_ID_CSI_NONE,
    MMPM_CLK_ID_CSI,
    MMPM_CLK_ID_CSI_VFE,
    MMPM_CLK_ID_CSI_PHY,
    MMPM_CLK_ID_CSI_PHY_TIMER,
    MMPM_CLK_ID_CSI_PIX0,
    MMPM_CLK_ID_CSI_PIX1,
    MMPM_CLK_ID_CSI_RDI0,
    MMPM_CLK_ID_CSI_RDI1,
    MMPM_CLK_ID_CSI_RDI2,
    MMPM_CLK_ID_CSI_MAX,
    MMPM_CLK_ID_CSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdCsiType;
typedef enum {
    MMPM_CLK_ID_SENSOR_NONE,
    MMPM_CLK_ID_SENSOR,
    MMPM_CLK_ID_SENSOR_MAX,
    MMPM_CLK_ID_SENSOR_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdSensorType;
typedef enum {
    MMPM_CLK_ID_JPEGD_NONE,
    MMPM_CLK_ID_JPEGD,
    MMPM_CLK_ID_JPEGD_MAX,
    MMPM_CLK_ID_JPEGD_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdJpegdType;
typedef enum {
    MMPM_CLK_ID_JPEGE_NONE,
    MMPM_CLK_ID_JPEGE,
    MMPM_CLK_ID_JPEGE_MAX,
    MMPM_CLK_ID_JPEGE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdJpegeType;
typedef enum {
    MMPM_CLK_ID_ROTATOR_NONE,
    MMPM_CLK_ID_ROTATOR,
    MMPM_CLK_ID_ROTATOR_MAX,
    MMPM_CLK_ID_ROTATOR_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdRotatorType;
typedef enum {
    MMPM_CLK_ID_TV_NONE,
    MMPM_CLK_ID_TV_ENC,
    MMPM_CLK_ID_TV_DAC,
    MMPM_CLK_ID_TV_MDP,
    MMPM_CLK_ID_TV_HDMI_APP,
    MMPM_CLK_ID_TV_HDMI,
    MMPM_CLK_ID_TV_RGB,
    MMPM_CLK_ID_TV_NPL,
    MMPM_CLK_ID_TV_MAX,
    MMPM_CLK_ID_TV_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdTvType;
typedef enum {
    MMPM_CLK_ID_DSI_NONE,
    MMPM_CLK_ID_DSI,
    MMPM_CLK_ID_DSI_ESC,
    MMPM_CLK_ID_DSI_PIX,
    MMPM_CLK_ID_DSI_BYTE,
    MMPM_CLK_ID_DSI_LVDS,
    MMPM_CLK_ID_DSI_MDP_P2,
    MMPM_CLK_ID_DSI_MAX,
    MMPM_CLK_ID_DSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdDsiType;
typedef enum {
    MMPM_CLK_ID_AUDIOIF_NONE,
    MMPM_CLK_ID_AUDIOIF_PCM,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_SCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_MCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_SCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_MCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_SCLK,
    MMPM_CLK_ID_AUDIOIF_LPASS_SB_REF_CLK,
    MMPM_CLK_ID_AUDIOIF_SPS_SB_REF_CLK,
    MMPM_CLK_ID_AUDIOIF_MAX,
    MMPM_CLK_ID_AUDIOIF_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdAudioIfType;
typedef enum {
    MMPM_CLK_ID_LPASS_NONE = 0,
    MMPM_CLK_ID_LPASS_HWRSP_CORE,
    MMPM_CLK_ID_LPASS_MIDI_CORE,
    MMPM_CLK_ID_LPASS_AVSYNC_XO,
    MMPM_CLK_ID_LPASS_AVSYNC_BT,
    MMPM_CLK_ID_LPASS_AVSYNC_FM,
    MMPM_CLK_ID_LPASS_SLIMBUS_CORE,
    MMPM_CLK_ID_LPASS_AVTIMER_CORE,
    MMPM_CLK_ID_LPASS_ATIME_CORE,
    MMPM_CLK_ID_LPASS_ATIME2_CORE,
    MMPM_CLK_ID_LPASS_ADSP_CORE,
    MMPM_CLK_ID_LPASS_AHB_ROOT,
    MMPM_CLK_ID_LPASS_ENUM_MAX,
    MMPM_CLK_ID_LPASS_FORCE32BITS = 0x7fffffff
} MmpmClkIdLpassType;
typedef enum {
    MMPM_CLK_ID_VCAP_NONE,
    MMPM_CLK_ID_VCAP,
    MMPM_CLK_ID_VCAP_NPL,
    MMPM_CLK_ID_VCAP_MAX,
    MMPM_CLK_ID_VCAP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVcapType;
typedef union {
    MmpmClkId2dGrpType clkId2dGrp;
    MmpmClkId3dGrpType clkId3dGrp;
    MmpmClkIdMdpType clkIdMdp;
    MmpmClkIdVcodecType clkIdVcodec;
    MmpmClkIdVpeType clkIdVpe;
    MmpmClkIdVfeType clkIdVfe;
    MmpmClkIdCsiType clkIdCsi;
    MmpmClkIdSensorType clkIdSensor;
    MmpmClkIdJpegdType clkIdJpegd;
    MmpmClkIdJpegeType clkIdJpege;
    MmpmClkIdRotatorType clkIdRotator;
    MmpmClkIdTvType clkIdTv;
    MmpmClkIdDsiType clkIdDsi;
    MmpmClkIdAudioIfType clkIdAudioIf;
    MmpmClkIdVcapType clkIdVcap;
    MmpmClkIdLpassType clkIdLpass;
} MmpmCoreClkIdType;
typedef enum {
    MMPM_FREQ_AT_LEAST,
    MMPM_FREQ_AT_MOST,
    MMPM_FREQ_CLOSEST,
    MMPM_FREQ_EXACT,
    MMPM_FREQ_MAX,
    MMPM_FREQ_FORCE32BITS = 0x7FFFFFFF
} MmpmFreqMatchType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 clkFreqHz;
    MmpmFreqMatchType freqMatch;
} MmpmClkValType;
typedef struct {
    uint32 numOfClk;
    MmpmClkValType *pClkArray;
} MmpmClkReqType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI0,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdHdmiType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_DSI_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_DSI0,
    MMPM_CLK_DOMAIN_SRC_ID_DSI1,
    MMPM_CLK_DOMAIN_SRC_ID_LVDS,
    MMPM_CLK_DOMAIN_SRC_ID_DSI_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_DSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdDsiType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_Q6PLL,
    MMPM_CLK_DOMAIN_SRC_ID_AUDIOPLL,
    MMPM_CLK_DOMAIN_SRC_ID_PRIUSPLL,
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdLpassType;
typedef union {
    MmpmClkDomainSrcIdHdmiType clkDomainSrcIdHdmi;
    MmpmClkDomainSrcIdDsiType clkDomainSrcIdDsi;
    MmpmClkDomainSrcIdLpassType clkDomainSrcIdLpass;
} MmpmClkDomainSrcIdType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 M_val;
    uint32 N_val;
    uint32 n2D_val;
    uint32 div_val;
    uint32 clkFreqHz;
    MmpmClkDomainSrcIdType clkDomainSrc;
} MmpmClkDomainType;
typedef struct {
    uint32 numOfClk;
    MmpmClkDomainType *pClkDomainArray;
} MmpmClkDomainReqType;
typedef enum {
    MMPM_THERMAL_NONE,
    MMPM_THERMAL_LOW,
    MMPM_THERMAL_NORM,
    MMPM_THERMAL_HIGH_L1,
    MMPM_THERMAL_HIGH_L2,
    MMPM_THERMAL_HIGH_L3,
    MMPM_THERMAL_HIGH_L4,
    MMPM_THERMAL_HIGH_L5,
    MMPM_THERMAL_HIGH_L6,
    MMPM_THERMAL_HIGH_L7,
    MMPM_THERMAL_HIGH_L8,
    MMPM_THERMAL_HIGH_L9,
    MMPM_THERMAL_HIGH_L10,
    MMPM_THERMAL_HIGH_L11,
    MMPM_THERMAL_HIGH_L12,
    MMPM_THERMAL_HIGH_L13,
    MMPM_THERMAL_HIGH_L14,
    MMPM_THERMAL_HIGH_L15,
    MMPM_THERMAL_HIGH_L16,
    MMPM_THERMAL_MAX,
    MMPM_THERMAL_FORCE32BITS = 0x7FFFFFFF
} MmpmThermalType;
typedef enum {
    MMPM_BW_USAGE_2D_GRP_NONE,
    MMPM_BW_USAGE_2D_GRP,
    MMPM_BW_USAGE_2D_GRP_MAX,
    MMPM_BW_USAGE_2D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsage2dGrpType;
typedef enum {
    MMPM_BW_USAGE_3D_GRP_NONE,
    MMPM_BW_USAGE_3D_GRP,
    MMPM_BW_USAGE_3D_GRP_MAX,
    MMPM_BW_USAGE_3D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsage3dGrpType;
typedef enum {
    MMPM_BW_USAGE_MDP_NONE,
    MMPM_BW_USAGE_MDP,
    MMPM_BW_USAGE_MDP_HRES,
    MMPM_BW_USAGE_MDP_MAX,
    MMPM_BW_USAGE_MDP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageMdpType;
typedef enum {
    MMPM_BW_USAGE_VCODEC_NONE,
    MMPM_BW_USAGE_VCODEC_ENC,
    MMPM_BW_USAGE_VCODEC_DEC,
    MMPM_BW_USAGE_VCODEC_ENC_DEC,
    MMPM_BW_USAGE_VCODEC_MAX,
    MMPM_BW_USAGE_VCODEC_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVcodecType;
typedef enum {
    MMPM_BW_USAGE_VPE_NONE,
    MMPM_BW_USAGE_VPE,
    MMPM_BW_USAGE_VPE_MAX,
    MMPM_BW_USAGE_VPE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVpeType;
typedef enum {
    MMPM_BW_USAGE_VFE_NONE,
    MMPM_BW_USAGE_VFE,
    MMPM_BW_USAGE_VFE_MAX,
    MMPM_BW_USAGE_VFE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVfeType;
typedef enum {
    MMPM_BW_USAGE_JPEGD_NONE,
    MMPM_BW_USAGE_JPEGD,
    MMPM_BW_USAGE_JPEGD_MAX,
    MMPM_BW_USAGE_JPEGD_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageJpegdType;
typedef enum {
    MMPM_BW_USAGE_JPEGE_NONE,
    MMPM_BW_USAGE_JPEGE,
    MMPM_BW_USAGE_JPEGE_MAX,
    MMPM_BW_USAGE_JPEGE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageJpegeType;
typedef enum {
    MMPM_BW_USAGE_ROTATOR_NONE,
    MMPM_BW_USAGE_ROTATOR,
    MMPM_BW_USAGE_ROTATOR_MAX,
    MMPM_BW_USAGE_ROTATOR_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageRotatorType;
typedef enum {
    MMPM_BW_USAGE_LPASS_NONE,
    MMPM_BW_USAGE_LPASS_DSP,
    MMPM_BW_USAGE_LPASS_DMA,
    MMPM_BW_USAGE_LPASS_EXT_CPU,
    MMPM_BW_USAGE_LPASS_ENUM_MAX,
    MMPM_BW_USAGE_LPASS_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageLpassType;
typedef union {
    MmpmBwUsage2dGrpType bwUsage2dGrp;
    MmpmBwUsage3dGrpType bwUsage3dGrp;
    MmpmBwUsageMdpType bwUsageMdp;
    MmpmBwUsageVcodecType bwUsageVcodec;
    MmpmBwUsageVpeType bwUsageVpe;
    MmpmBwUsageVfeType bwUsageVfe;
    MmpmBwUsageJpegdType bwUsageJpegd;
    MmpmBwUsageJpegeType bwUsageJpege;
    MmpmBwUsageRotatorType bwUsageRotator;
    MmpmBwUsageLpassType bwUsageLpass;
} MmpmCoreBwUsageType;
typedef enum {
    MMPM_BW_PORT_ID_NONE = 0,
    MMPM_BW_PORT_ID_ADSP_MASTER,
    MMPM_BW_PORT_ID_DML_MASTER,
    MMPM_BW_PORT_ID_AIF_MASTER,
    MMPM_BW_PORT_ID_SLIMBUS_MASTER,
    MMPM_BW_PORT_ID_MIDI_MASTER,
    MMPM_BW_PORT_ID_HWRSMP_MASTER,
    MMPM_BW_PORT_ID_EXT_AHB_MASTER,
    MMPM_BW_PORT_ID_SPDIF_MASTER,
    MMPM_BW_PORT_ID_HDMIRX_MASTER,
    MMPM_BW_PORT_ID_HDMITX_MASTER,
    MMPM_BW_PORT_ID_SIF_MASTER,
    MMPM_BW_PORT_ID_DML_SLAVE,
    MMPM_BW_PORT_ID_AIF_SLAVE,
    MMPM_BW_PORT_ID_SLIMBUS_SLAVE,
    MMPM_BW_PORT_ID_MIDI_SLAVE,
    MMPM_BW_PORT_ID_HWRSMP_SLAVE,
    MMPM_BW_PORT_ID_AVSYNC_SLAVE,
    MMPM_BW_PORT_ID_LPM_SLAVE,
    MMPM_BW_PORT_ID_SRAM_SLAVE,
    MMPM_BW_PORT_ID_EXT_AHB_SLAVE,
    MMPM_BW_PORT_ID_DDR_SLAVE,
    MMPM_BW_PORT_ID_OCMEM_SLAVE,
    MMPM_BW_PORT_ID_PERIFNOC_SLAVE,
    MMPM_BW_PORT_ID_SPDIF_SLAVE,
    MMPM_BW_PORT_ID_HDMIRX_SLAVE,
    MMPM_BW_PORT_ID_HDMITX_SLAVE,
    MMPM_BW_PORT_ID_SIF_SLAVE,
    MMPM_BW_PORT_ID_BSTC_SLAVE,
    MMPM_BW_PORT_ID_DCODEC_SLAVE,
    MMPM_BW_PORT_ID_CORE,
    MMPM_BW_PORT_ID_MAX,
    MMPM_BW_PORT_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmBwPortIdType;
typedef struct {
    uint32 memPhyAddr;
    uint32 axiPort;
    uint32 bwBytePerSec;
    uint32 usagePercentage;
    MmpmCoreBwUsageType bwUsageType;
} MmpmBwValType;
typedef struct {
    uint32 numOfBw;
    MmpmBwValType *pBandWidthArray;
} MmpmBwReqType;
typedef struct{
    MmpmBwPortIdType masterPort;
    MmpmBwPortIdType slavePort;
} MmpmBusRouteType;
typedef struct{
    uint64 Ab;
    uint64 Ib;
} MmpmBusBWDataIbAbType;
typedef struct{
    uint64 bwBytePerSec;
    uint32 usagePercentage;
    MmpmBwUsageLpassType usageType;
} MmpmBusBWDataUsageType;
typedef union{
    MmpmBusBWDataIbAbType busBwAbIb;
    MmpmBusBWDataUsageType busBwValue;
} MmpmBusBWDataType;
typedef struct{
    MmpmBusRouteType busRoute;
    MmpmBusBWDataType bwValue;
} MmpmGenBwValType;
typedef struct {
    uint32 numOfBw;
    MmpmGenBwValType *pBandWidthArray;
} MmpmGenBwReqType;
typedef enum {
    MMPM_MEM_POWER_NONE,
    MMPM_MEM_POWER_OFF,
    MMPM_MEM_POWER_RETENTION,
    MMPM_MEM_POWER_ACTIVE,
    MMPM_MEM_POWER_MAX,
    MMPM_MEM_POWER_FORCE32BITS = 0x7FFFFFFF
} MmpmMemPowerStateType;
typedef enum{
    MMPM_MEM_NONE,
    MMPM_MEM_OCMEM,
    MMPM_MEM_LPASS_LPM,
    MMPM_MEM_SRAM,
    MMPM_MEM_MAX,
    MMPM_MEM_FORCE32BITS = 0x7FFFFFFF
} MmpmMemIdType;
typedef struct{
    MmpmMemIdType memory;
    MmpmMemPowerStateType powerState;
}MmpmMemPowerReqParamType;
typedef struct {
    uint32 gpioId;
    boolean configGpio;
    uint32 gpioVoltageSource;
    boolean gpioModeOnOff;
    uint32 gpioModeSelect;
    uint32 gpioOutBufferConfig;
    boolean invertExtPin;
    uint32 gpioCurrentSourcePulls;
    uint32 gpioOutBufferDriveStrength;
    uint32 gpioDtestBufferOnOff;
    uint32 gpioExtPinConfig;
    uint32 gpioSourceConfig;
    boolean interrupPolarity;
    uint32 uartPath;
} MmpmPmicGpioParamType;
typedef enum {
     MMPM_PMIC_CONFIG,
     MMPM_PMIC_CONFIG_BIAS_VOLTAGE,
     MMPM_PMIC_CONFIG_DIGITAL_INPUT,
     MMPM_PMIC_CONFIG_DIGITAL_OUTPUT,
     MMPM_PMIC_CONFIG_SET_VOLTAGE_SOURCE,
     MMPM_PMIC_CONFIG_MODE_SELECTION,
     MMPM_PMIC_CONFIG_SET_OUTPUT_BUFFER,
     MMPM_PMIC_CONFIG_SET_INVERSION,
     MMPM_PMIC_CONFIG_SET_CURRENT_SOURCE_PULLS,
     MMPM_PMIC_CONFIG_SET_EXT_PIN,
     MMPM_PMIC_CONFIG_SET_OUTPUT_BUF_DRIVE_STRG,
     MMPM_PMIC_CONFIG_SET_SOURCE,
     MMPM_PMIC_CONFIG_SET_INT_POLARITY,
     MMPM_PMIC_CONFIG_SET_MUX_CTRL,
} MmpmPmicGpioConfigType;
typedef struct {
    MmpmPmicGpioConfigType configId;
    MmpmPmicGpioParamType gpioParam;
} MmpmPmicGpioReqType;
typedef enum {
     MMPM_RESET_NONE,
     MMPM_RESET_DEASSERT,
     MMPM_RESET_ASSERT,
     MMPM_RESET_PULSE,
     MMPM_RESET_ENUM_MAX,
     MMPM_RESET_ENUM_FORCE32BITS = 0x7FFFFFFF
} MmpmResetType;
typedef struct {
    MmpmCoreClkIdType clkId;
    MmpmResetType resetType;
} MmpmResetParamType;
typedef struct {
    uint32 numOfReset;
    MmpmResetParamType *pResetParamArray;
} MmpmResetReqType;
typedef enum {
    MMPM_MIPS_REQUEST_NONE = 0,
    MMPM_MIPS_REQUEST_CPU_CLOCK_ONLY,
    MMPM_MIPS_REQUEST_CPU_CLOCK_AND_BW,
    MMPM_MIPS_REQUEST_ENUM_MAX,
    MMPM_MIPS_REQUEST_FORCE32BITS = 0x7FFFFFFF
} MmpmMipsRequestFnType;
typedef struct {
    uint32 mipsTotal;
    uint32 mipsPerThread;
    MmpmBwPortIdType codeLocation;
    MmpmMipsRequestFnType reqOperation;
} MmpmMipsReqType;
typedef struct {
    uint32 mppsTotal;
    uint32 adspFloorClock;
} MmpmMppsReqType;
typedef struct {
    uint32 numOfClk;
    MmpmCoreClkIdType *pClkIdArray;
} MmpmClkIdArrayParamType;
typedef union {
    uint32 vregMilliVolt;
    MmpmRegProgMatchType regProgMatch;
    MmpmPmicGpioReqType *pPmicGpio;
    MmpmClkReqType *pCoreClk;
    MmpmClkDomainReqType *pCoreClkDomain;
    MmpmBwReqType *pBwReq;
    MmpmGenBwReqType *pGenBwReq;
    uint32 sleepMicroSec;
    uint32 mips;
    MmpmResetReqType *pResetReq;
    MmpmMipsReqType *pMipsExt;
    MmpmMppsReqType *pMppsReq;
    MmpmThermalType thermalMitigation;
    MmpmMemPowerReqParamType *pMemPowerState;
    MmpmClkIdArrayParamType *pRelClkIdArray;
    MmpmCoreClkIdType gateClkId;
}MmpmRscParamStructType;
typedef struct {
    MmpmRscIdType rscId;
    MmpmRscParamStructType rscParam;
} MmpmRscParamType;
typedef enum{
     MMPM_API_TYPE_NONE,
     MMPM_API_TYPE_SYNC,
     MMPM_API_TYPE_ASYNC,
     MMPM_API_TYPE_ENUM_MAX,
     MMPM_API_TYPE_FORCE32BITS = 0x7FFFFFFF
} MmpmApiType;
typedef struct {
    MmpmApiType apiType;
    uint32 numOfReq;
    MmpmRscParamType *pReqArray;
    MMPM_STATUS *pStsArray;
    uint32 reqTag;
    void *pExt;
} MmpmRscExtParamType;
typedef enum {
    MMPM_INFO_ID_NONE,
    MMPM_INFO_ID_CORE_CLK,
    MMPM_INFO_ID_CORE_CLK_MAX,
    MMPM_INFO_ID_CORE_CLK_MAX_SVS,
    MMPM_INFO_ID_CORE_CLK_MAX_NOMINAL,
    MMPM_INFO_ID_CORE_CLK_MAX_TURBO,
    MMPM_INFO_ID_MIPS_MAX,
    MMPM_INFO_ID_BW_MAX,
    MMPM_INFO_ID_CRASH_DUMP,
    MMPM_INFO_ID_POWER_SUPPORT,
    MMPM_INFO_ID_CLK_FREQ,
    MMPM_INFO_ID_PERFMON,
    MMPM_INFO_ID_PMIC_GPIO,
    MMPM_INFO_ID_SET_DEBUG_LEVEL,
    MMPM_INFO_ID_MMSS_BUS,
    MMPM_INFO_ID_LPASS_BUS,
    MMPM_INFO_ID_AGGREGATE_CLIENT_CLASS,
    MMPM_INFO_ID_MPPS,
    MMPM_INFO_ID_BW_EXT,
    MMPM_INFO_ID_DCVS_STATE,
    MMPM_INFO_ID_EXT_REQ,
    MMPM_INFO_ID_MAX,
    MMPM_INFO_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmInfoIdType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 clkFreqHz;
} MmpmInfoClkType;
typedef struct {
    uint32 clkId;
    uint32 clkFreqHz;
    uint32 forceMeasure;
} MmpmInfoClkFreqType;
typedef struct
{
    uint32 clientClasses;
    uint32 aggregateMpps;
} MmpmInfoMppsType;
typedef struct
{
    MmpmBusRouteType busRoute;
    uint64 totalBw;
} MmpmInfoBwExtType;
typedef struct {
    uint32 masterMeasureArray[7];
    uint32 measurementConfig[7];
    uint32 latencyMasterPort;
    uint32 holdoffTime;
    uint32 triggerMode;
    void *pDataMsg;
    char pFilename[64];
    uint32 axiClockFreq;
    uint32 clockInfo[10];
    uint32 isClockFreqCalc[10];
} MmpmInfoPerfmonType;
typedef struct {
    uint32 gpioModeSelect;
    uint32 gpioVoltageSource;
    uint32 gpioModeOnOff;
    uint32 gpioOutBufferConfig;
    uint32 gpioOutBufferDriveStrength;
    uint32 gpioCurrentSourcePulls;
    uint32 gpioSourceConfig;
    uint32 gpioDtestBufferOnOff;
    uint32 gpioExtPinConfig;
} MmpmPmicGpioStatusType;
typedef struct {
    uint32 gpioId;
    MmpmPmicGpioStatusType gpioSts;
} MmpmPmicGpioInfoType;
typedef struct {
    uint32 clientId;
    char clientName[32];
    char fabClientName[32];
    uint32 uIbBytePerSec;
    uint32 uAbBytePerSec;
} MmpmBwClientType;
typedef struct {
    uint32 coreId;
    uint32 instanceId;
    uint32 axiPort;
    uint32 slaveId;
    uint32 uIbBytePerSec;
    uint32 uAbBytePerSec;
    uint32 numOfClients;
    MmpmBwClientType client[16];
} MmpmMasterSlaveType;
typedef struct {
    uint32 mmFabClkFreq;
    uint32 appsFabClkFreq;
    uint32 sysFabClkFreq;
    uint32 lpassFabClkFreq;
    uint32 ebiClkFreq;
    uint32 mmImemClkFreq;
    uint32 numOfMasterSlave;
    MmpmMasterSlaveType masterSlave[16];
} MmpmFabStatusInfoType;
typedef union {
    MmpmInfoClkType *pInfoClk;
    boolean bInfoPower;
    MmpmInfoPerfmonType *pInfoPerfmon;
    MmpmInfoClkFreqType *pInfoClkFreqType;
    MmpmPmicGpioInfoType *pPmicGpioStatus;
    uint32 infoSetLotLevel;
    MmpmFabStatusInfoType *pFabStatus;
    uint32 mipsValue;
    uint64 bwValue;
    uint32 aggregateClientClass;
    MmpmInfoMppsType *pMppsInfo;
    MmpmInfoBwExtType *pBwExtInfo;
    boolean dcvsState;
    void *pExtInfo;
} MmpmInfoDataStructType;
typedef struct {
    MmpmInfoIdType infoId;
    MmpmCoreIdType coreId;
    MmpmCoreInstanceIdType instanceId;
    MmpmInfoDataStructType info;
} MmpmInfoDataType;
typedef enum{
    MMPM_PARAM_ID_NONE = 0,
    MMPM_PARAM_ID_RESOURCE_LIMIT,
    MMPM_PARAM_ID_CLIENT_OCMEM_MAP,
    MMPM_PARAM_ID_MEMORY_MAP,
    MMPM_PARAM_ID_CLIENT_CLASS,
    MMPM_PARAM_ID_L2_CACHE_LINE_LOCK,
    MMPM_PARAM_ID_DCVS_PARTICIPATION,
    MMPM_PARAM_ID_ENUM_MAX,
    MMPM_PARAM_ID_Force32bit = 0x7fffffff
} MmpmParameterIdType;
typedef struct
{
    MmpmMemIdType memory;
    uint64 startAddress;
    uint32 size;
} MmpmMemoryMapType;
typedef struct
{
    void* startAddress;
    uint32 size;
    uint32 throttleBlockSize;
    uint32 throttlePauseUs;
} MmpmL2CacheLineLockParameterType;
typedef enum
{
    MMPM_UNKNOWN_CLIENT_CLASS = 0x00,
    MMPM_AUDIO_CLIENT_CLASS = 0x01,
    MMPM_VOICE_CLIENT_CLASS = 0x02,
    MMPM_COMPUTE_CLIENT_CLASS = 0x04,
    MMPM_STREAMING_1HVX_CLIENT_CLASS = 0x08,
    MMPM_STREAMING_2HVX_CLIENT_CLASS = 0x10,
} MmpmClientClassType;
typedef enum
{
    MMPM_DCVS_ADJUST_UP_DOWN,
    MMPM_DCVS_ADJUST_ONLY_UP
} MmpmDcvsEnableOptionsType;
typedef struct
{
    boolean enable;
    MmpmDcvsEnableOptionsType enableOpt;
} MmpmDcvsParticipationType;
typedef struct{
    MmpmParameterIdType paramId;
    void* pParamConfig;
} MmpmParameterConfigType;
typedef struct{
    uint32 regionId;
    uint32 numKeys;
    uint32 *pKey;
} MmpmOcmemMapRegionDescType;
typedef struct{
    uint32 numRegions;
    MmpmOcmemMapRegionDescType *pRegions;
} MmpmOcmemMapType;
uint32 MMPM_Init(char * cmd_line);
uint32 MMPM_Register_Ext(MmpmRegParamType *pRegParam);
MMPM_STATUS MMPM_Deregister_Ext(uint32 clientId);
MMPM_STATUS MMPM_Request(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Release(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Pause(uint32 clientId,
                           MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Resume(uint32 clientId,
                            MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Request_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Release_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Pause_Ext(uint32 clientId,
                               MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Resume_Ext(uint32 clientId,
                                MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_GetInfo(MmpmInfoDataType *pInfoData);
MMPM_STATUS MMPM_SetParameter(uint32 clientId, MmpmParameterConfigType *pParamConfig);
typedef uint32 DalChipInfoVersionType;
typedef uint32 DalChipInfoModemType;
typedef enum
{
  DALCHIPINFO_ID_UNKNOWN = 0,
  DALCHIPINFO_ID_MDM1000 = 1,
  DALCHIPINFO_ID_ESM6235 = 2,
  DALCHIPINFO_ID_QSC6240 = 3,
  DALCHIPINFO_ID_MSM6245 = 4,
  DALCHIPINFO_ID_MSM6255 = 5,
  DALCHIPINFO_ID_MSM6255A = 6,
  DALCHIPINFO_ID_MSM6260 = 7,
  DALCHIPINFO_ID_MSM6246 = 8,
  DALCHIPINFO_ID_QSC6270 = 9,
  DALCHIPINFO_ID_MSM6280 = 10,
  DALCHIPINFO_ID_MSM6290 = 11,
  DALCHIPINFO_ID_MSM7200 = 12,
  DALCHIPINFO_ID_MSM7201 = 13,
  DALCHIPINFO_ID_ESM7205 = 14,
  DALCHIPINFO_ID_ESM7206 = 15,
  DALCHIPINFO_ID_MSM7200A = 16,
  DALCHIPINFO_ID_MSM7201A = 17,
  DALCHIPINFO_ID_ESM7205A = 18,
  DALCHIPINFO_ID_ESM7206A = 19,
  DALCHIPINFO_ID_ESM7225 = 20,
  DALCHIPINFO_ID_MSM7225 = 21,
  DALCHIPINFO_ID_MSM7500 = 22,
  DALCHIPINFO_ID_MSM7500A = 23,
  DALCHIPINFO_ID_MSM7525 = 24,
  DALCHIPINFO_ID_MSM7600 = 25,
  DALCHIPINFO_ID_MSM7601 = 26,
  DALCHIPINFO_ID_MSM7625 = 27,
  DALCHIPINFO_ID_MSM7800 = 28,
  DALCHIPINFO_ID_MDM8200 = 29,
  DALCHIPINFO_ID_QSD8650 = 30,
  DALCHIPINFO_ID_MDM8900 = 31,
  DALCHIPINFO_ID_QST1000 = 32,
  DALCHIPINFO_ID_QST1005 = 33,
  DALCHIPINFO_ID_QST1100 = 34,
  DALCHIPINFO_ID_QST1105 = 35,
  DALCHIPINFO_ID_QST1500 = 40,
  DALCHIPINFO_ID_QST1600 = 41,
  DALCHIPINFO_ID_QST1700 = 42,
  DALCHIPINFO_ID_QSD8250 = 36,
  DALCHIPINFO_ID_QSD8550 = 37,
  DALCHIPINFO_ID_QSD8850 = 38,
  DALCHIPINFO_ID_MDM2000 = 39,
  DALCHIPINFO_ID_MSM7227 = 43,
  DALCHIPINFO_ID_MSM7627 = 44,
  DALCHIPINFO_ID_QSC6165 = 45,
  DALCHIPINFO_ID_QSC6175 = 46,
  DALCHIPINFO_ID_QSC6185 = 47,
  DALCHIPINFO_ID_QSC6195 = 48,
  DALCHIPINFO_ID_QSC6285 = 49,
  DALCHIPINFO_ID_QSC6295 = 50,
  DALCHIPINFO_ID_QSC6695 = 51,
  DALCHIPINFO_ID_ESM6246 = 52,
  DALCHIPINFO_ID_ESM6290 = 53,
  DALCHIPINFO_ID_ESC6270 = 54,
  DALCHIPINFO_ID_ESC6240 = 55,
  DALCHIPINFO_ID_MDM8220 = 56,
  DALCHIPINFO_ID_MDM9200 = 57,
  DALCHIPINFO_ID_MDM9600 = 58,
  DALCHIPINFO_ID_MSM7630 = 59,
  DALCHIPINFO_ID_MSM7230 = 60,
  DALCHIPINFO_ID_ESM7227 = 61,
  DALCHIPINFO_ID_MSM7625D1 = 62,
  DALCHIPINFO_ID_MSM7225D1 = 63,
  DALCHIPINFO_ID_QSD8250A = 64,
  DALCHIPINFO_ID_QSD8650A = 65,
  DALCHIPINFO_ID_MSM7625D2 = 66,
  DALCHIPINFO_ID_MSM7227D1 = 67,
  DALCHIPINFO_ID_MSM7627D1 = 68,
  DALCHIPINFO_ID_MSM7627D2 = 69,
  DALCHIPINFO_ID_MSM8260 = 70,
  DALCHIPINFO_ID_MSM8660 = 71,
  DALCHIPINFO_ID_MDM8200A = 72,
  DALCHIPINFO_ID_QSC6155 = 73,
  DALCHIPINFO_ID_MSM8255 = 74,
  DALCHIPINFO_ID_MSM8655 = 75,
  DALCHIPINFO_ID_ESC6295 = 76,
  DALCHIPINFO_ID_MDM3000 = 77,
  DALCHIPINFO_ID_MDM6200 = 78,
  DALCHIPINFO_ID_MDM6600 = 79,
  DALCHIPINFO_ID_MDM6210 = 80,
  DALCHIPINFO_ID_MDM6610 = 81,
  DALCHIPINFO_ID_QSD8672 = 82,
  DALCHIPINFO_ID_MDM6215 = 83,
  DALCHIPINFO_ID_MDM6615 = 84,
  DALCHIPINFO_ID_APQ8055 = 85,
  DALCHIPINFO_ID_APQ8060 = 86,
  DALCHIPINFO_ID_MSM8960 = 87,
  DALCHIPINFO_ID_MSM7225A = 88,
  DALCHIPINFO_ID_MSM7625A = 89,
  DALCHIPINFO_ID_MSM7227A = 90,
  DALCHIPINFO_ID_MSM7627A = 91,
  DALCHIPINFO_ID_ESM7227A = 92,
  DALCHIPINFO_ID_QSC6195D2 = 93,
  DALCHIPINFO_ID_FSM9200 = 94,
  DALCHIPINFO_ID_FSM9800 = 95,
  DALCHIPINFO_ID_MSM7225AD1 = 96,
  DALCHIPINFO_ID_MSM7227AD1 = 97,
  DALCHIPINFO_ID_MSM7225AA = 98,
  DALCHIPINFO_ID_MSM7225AAD1 = 99,
  DALCHIPINFO_ID_MSM7625AA = 100,
  DALCHIPINFO_ID_MSM7227AA = 101,
  DALCHIPINFO_ID_MSM7227AAD1 = 102,
  DALCHIPINFO_ID_MSM7627AA = 103,
  DALCHIPINFO_ID_MDM9615 = 104,
  DALCHIPINFO_ID_MDM9615M = DALCHIPINFO_ID_MDM9615,
  DALCHIPINFO_ID_MDM8215 = 106,
  DALCHIPINFO_ID_MDM9215 = 107,
  DALCHIPINFO_ID_MDM9215M = DALCHIPINFO_ID_MDM9215,
  DALCHIPINFO_ID_APQ8064 = 109,
  DALCHIPINFO_ID_QSC6270D1 = 110,
  DALCHIPINFO_ID_QSC6240D1 = 111,
  DALCHIPINFO_ID_ESC6270D1 = 112,
  DALCHIPINFO_ID_ESC6240D1 = 113,
  DALCHIPINFO_ID_MDM6270 = 114,
  DALCHIPINFO_ID_MDM6270D1 = 115,
  DALCHIPINFO_ID_MSM8930 = 116,
  DALCHIPINFO_ID_MSM8630 = 117,
  DALCHIPINFO_ID_MSM8230 = 118,
  DALCHIPINFO_ID_APQ8030 = 119,
  DALCHIPINFO_ID_MSM8627 = 120,
  DALCHIPINFO_ID_MSM8227 = 121,
  DALCHIPINFO_ID_MSM8660A = 122,
  DALCHIPINFO_ID_MSM8260A = 123,
  DALCHIPINFO_ID_APQ8060A = 124,
  DALCHIPINFO_ID_MPQ8062 = 125,
  DALCHIPINFO_ID_MSM8974 = 126,
  DALCHIPINFO_ID_MSM8225 = 127,
  DALCHIPINFO_ID_MSM8225D1 = 128,
  DALCHIPINFO_ID_MSM8625 = 129,
  DALCHIPINFO_ID_MPQ8064 = 130,
  DALCHIPINFO_ID_MSM7225AB = 131,
  DALCHIPINFO_ID_MSM7225ABD1 = 132,
  DALCHIPINFO_ID_MSM7625AB = 133,
  DALCHIPINFO_ID_MDM9625 = 134,
  DALCHIPINFO_ID_MSM7125A = 135,
  DALCHIPINFO_ID_MSM7127A = 136,
  DALCHIPINFO_ID_MSM8125AB = 137,
  DALCHIPINFO_ID_MSM8960AB = 138,
  DALCHIPINFO_ID_APQ8060AB = 139,
  DALCHIPINFO_ID_MSM8260AB = 140,
  DALCHIPINFO_ID_MSM8660AB = 141,
  DALCHIPINFO_ID_MSM8930AA = 142,
  DALCHIPINFO_ID_MSM8630AA = 143,
  DALCHIPINFO_ID_MSM8230AA = 144,
  DALCHIPINFO_ID_MSM8626 = 145,
  DALCHIPINFO_ID_MPQ8092 = 146,
  DALCHIPINFO_ID_MSM8610 = 147,
  DALCHIPINFO_ID_MDM8225 = 148,
  DALCHIPINFO_ID_MDM9225 = 149,
  DALCHIPINFO_ID_MDM9225M = 150,
  DALCHIPINFO_ID_MDM8225M = 151,
  DALCHIPINFO_ID_MDM9625M = 152,
  DALCHIPINFO_ID_APQ8064_V2PRIME = 153,
  DALCHIPINFO_ID_MSM8930AB = 154,
  DALCHIPINFO_ID_MSM8630AB = 155,
  DALCHIPINFO_ID_MSM8230AB = 156,
  DALCHIPINFO_ID_APQ8030AB = 157,
  DALCHIPINFO_ID_MSM8226 = 158,
  DALCHIPINFO_ID_MSM8526 = 159,
  DALCHIPINFO_ID_APQ8030AA = 160,
  DALCHIPINFO_ID_MSM8110 = 161,
  DALCHIPINFO_ID_MSM8210 = 162,
  DALCHIPINFO_ID_MSM8810 = 163,
  DALCHIPINFO_ID_MSM8212 = 164,
  DALCHIPINFO_ID_MSM8612 = 165,
  DALCHIPINFO_ID_MSM8112 = 166,
  DALCHIPINFO_ID_MSM8125 = 167,
  DALCHIPINFO_ID_MSM8225Q = 168,
  DALCHIPINFO_ID_MSM8625Q = 169,
  DALCHIPINFO_ID_MSM8125Q = 170,
  DALCHIPINFO_ID_MDM9310 = 171,
  DALCHIPINFO_ID_APQ8064_SLOW_PRIME = 172,
  DALCHIPINFO_ID_MDM8110M = 173,
  DALCHIPINFO_ID_MDM8615M = 174,
  DALCHIPINFO_ID_MDM9320 = 175,
  DALCHIPINFO_ID_MDM9225_1 = 176,
  DALCHIPINFO_ID_MDM9225M_1 = 177,
  DALCHIPINFO_ID_APQ8084 = 178,
  DALCHIPINFO_ID_MSM8130 = 179,
  DALCHIPINFO_ID_MSM8130AA = 180,
  DALCHIPINFO_ID_MSM8130AB = 181,
  DALCHIPINFO_ID_MSM8627AA = 182,
  DALCHIPINFO_ID_MSM8227AA = 183,
  DALCHIPINFO_ID_APQ8074 = 184,
  DALCHIPINFO_ID_MSM8274 = 185,
  DALCHIPINFO_ID_MSM8674 = 186,
  DALCHIPINFO_ID_MDM9635 = 187,
  DALCHIPINFO_ID_FSM9900 = 188,
  DALCHIPINFO_ID_FSM9965 = 189,
  DALCHIPINFO_ID_FSM9955 = 190,
  DALCHIPINFO_ID_FSM9950 = 191,
  DALCHIPINFO_ID_FSM9915 = 192,
  DALCHIPINFO_ID_FSM9910 = 193,
  DALCHIPINFO_ID_MSM8974_PRO = 194,
  DALCHIPINFO_ID_MSM8962 = 195,
  DALCHIPINFO_ID_MSM8262 = 196,
  DALCHIPINFO_ID_APQ8062 = 197,
  DALCHIPINFO_ID_MSM8126 = 198,
  DALCHIPINFO_ID_APQ8026 = 199,
  DALCHIPINFO_ID_MSM8926 = 200,
  DALCHIPINFO_ID_MSM8326 = 205,
  DALCHIPINFO_ID_MSM8916 = 206,
  DALCHIPINFO_ID_MSM8994 = 207,
  DALCHIPINFO_ID_APQ8074_AA = 208,
  DALCHIPINFO_ID_APQ8074_AB = 209,
  DALCHIPINFO_ID_APQ8074_PRO = 210,
  DALCHIPINFO_ID_MSM8274_AA = 211,
  DALCHIPINFO_ID_MSM8274_AB = 212,
  DALCHIPINFO_ID_MSM8274_PRO = 213,
  DALCHIPINFO_ID_MSM8674_AA = 214,
  DALCHIPINFO_ID_MSM8674_AB = 215,
  DALCHIPINFO_ID_MSM8674_PRO = 216,
  DALCHIPINFO_ID_MSM8974_AA = 217,
  DALCHIPINFO_ID_MSM8974_AB = 218,
  DALCHIPINFO_ID_APQ8028 = 219,
  DALCHIPINFO_ID_MSM8128 = 220,
  DALCHIPINFO_ID_MSM8228 = 221,
  DALCHIPINFO_ID_MSM8528 = 222,
  DALCHIPINFO_ID_MSM8628 = 223,
  DALCHIPINFO_ID_MSM8928 = 224,
  DALCHIPINFO_ID_MSM8510 = 225,
  DALCHIPINFO_ID_MSM8512 = 226,
  DALCHIPINFO_ID_MDM9630 = 227,
  DALCHIPINFO_ID_MDM9635M = DALCHIPINFO_ID_MDM9635,
  DALCHIPINFO_ID_MDM9230 = 228,
  DALCHIPINFO_ID_MDM9235M = 229,
  DALCHIPINFO_ID_MDM8630 = 230,
  DALCHIPINFO_ID_MDM9330 = 231,
  DALCHIPINFO_ID_MPQ8091 = 232,
  DALCHIPINFO_ID_MSM8936 = 233,
  DALCHIPINFO_ID_MDM9240 = 234,
  DALCHIPINFO_ID_MDM9340 = 235,
  DALCHIPINFO_ID_MDM9640 = 236,
  DALCHIPINFO_ID_MDM9245M = 237,
  DALCHIPINFO_ID_MDM9645M = 238,
  DALCHIPINFO_ID_MSM8939 = 239,
  DALCHIPINFO_ID_APQ8036 = 240,
  DALCHIPINFO_ID_APQ8039 = 241,
  DALCHIPINFO_ID_MSM8236 = 242,
  DALCHIPINFO_ID_MSM8636 = 243,
  DALCHIPINFO_ID_APQ8064_AU = 244,
  DALCHIPINFO_ID_MSM8909 = 245,
  DALCHIPINFO_ID_MSM8996 = 246,
  DALCHIPINFO_ID_APQ8016 = 247,
  DALCHIPINFO_ID_MSM8216 = 248,
  DALCHIPINFO_ID_MSM8116 = 249,
  DALCHIPINFO_ID_MSM8616 = 250,
  DALCHIPINFO_ID_MSM8992 = 251,
  DALCHIPINFO_ID_APQ8092 = 252,
  DALCHIPINFO_ID_APQ8094 = 253,
  DALCHIPINFO_ID_FSM9008 = 254,
  DALCHIPINFO_ID_FSM9010 = 255,
  DALCHIPINFO_ID_FSM9016 = 256,
  DALCHIPINFO_ID_FSM9055 = 257,
  DALCHIPINFO_ID_MSM8209 = 258,
  DALCHIPINFO_ID_MSM8208 = 259,
  DALCHIPINFO_ID_MDM9209 = 260,
  DALCHIPINFO_ID_MDM9309 = 261,
  DALCHIPINFO_ID_MDM9609 = 262,
  DALCHIPINFO_ID_MSM8239 = 263,
  DALCHIPINFO_ID_MSM8952 = 264,
  DALCHIPINFO_ID_APQ8009 = 265,
  DALCHIPINFO_ID_MSM8956 = 266,
  DALCHIPINFO_ID_QDF2432 = 267,
  DALCHIPINFO_ID_MSM8929 = 268,
  DALCHIPINFO_ID_MSM8629 = 269,
  DALCHIPINFO_ID_MSM8229 = 270,
  DALCHIPINFO_ID_APQ8029 = 271,
  DALCHIPINFO_ID_QCA9618 = 272,
  DALCHIPINFO_ID_IPQ4018 = DALCHIPINFO_ID_QCA9618,
  DALCHIPINFO_ID_QCA9619 = 273,
  DALCHIPINFO_ID_IPQ4019 = DALCHIPINFO_ID_QCA9619,
  DALCHIPINFO_ID_APQ8056 = 274,
  DALCHIPINFO_ID_MSM8609 = 275,
  DALCHIPINFO_ID_FSM9916 = 276,
  DALCHIPINFO_ID_APQ8076 = 277,
  DALCHIPINFO_ID_MSM8976 = 278,
  DALCHIPINFO_ID_MDM9650 = 279,
  DALCHIPINFO_ID_IPQ8065 = 280,
  DALCHIPINFO_ID_IPQ8069 = 281,
  DALCHIPINFO_ID_MSM8939_BC = 282,
  DALCHIPINFO_ID_MDM9250 = 283,
  DALCHIPINFO_ID_MDM9255 = 284,
  DALCHIPINFO_ID_MDM9350 = 285,
  DALCHIPINFO_ID_MDM9655 = 286,
  DALCHIPINFO_ID_IPQ4028 = 287,
  DALCHIPINFO_ID_IPQ4029 = 288,
  DALCHIPINFO_ID_APQ8052 = 289,
  DALCHIPINFO_ID_MDM9607 = 290,
  DALCHIPINFO_ID_APQ8096 = 291,
  DALCHIPINFO_ID_MSM8998 = 292,
  DALCHIPINFO_ID_MSM8953 = 293,
  DALCHIPINFO_ID_MSM8937 = 294,
  DALCHIPINFO_ID_APQ8037 = 295,
  DALCHIPINFO_ID_MDM8207 = 296,
  DALCHIPINFO_ID_MDM9207 = 297,
  DALCHIPINFO_ID_MDM9307 = 298,
  DALCHIPINFO_ID_MDM9628 = 299,
  DALCHIPINFO_ID_MSM8909W = 300,
  DALCHIPINFO_ID_APQ8009W = 301,
  DALCHIPINFO_ID_MSM8996L = 302,
  DALCHIPINFO_ID_MSM8917 = 303,
  DALCHIPINFO_ID_APQ8053 = 304,
  DALCHIPINFO_ID_MSM8996SG = 305,
  DALCHIPINFO_ID_MSM8997 = 306,
  DALCHIPINFO_ID_APQ8017 = 307,
  DALCHIPINFO_ID_MSM8217 = 308,
  DALCHIPINFO_ID_MSM8617 = 309,
  DALCHIPINFO_ID_MSM8996AU = 310,
  DALCHIPINFO_ID_APQ8096AU = 311,
  DALCHIPINFO_ID_APQ8096SG = 312,
  DALCHIPINFO_ID_MSM8940 = 313,
  DALCHIPINFO_ID_MDM9665 = 314,
  DALCHIPINFO_ID_MSM8996SGAU = 315,
  DALCHIPINFO_ID_APQ8096SGAU = 316,
  DALCHIPINFO_NUM_IDS = 317,
  DALCHIPINFO_ID_32BITS = 0x7FFFFFF
} DalChipInfoIdType;
typedef enum
{
  DALCHIPINFO_FAMILY_UNKNOWN = 0,
  DALCHIPINFO_FAMILY_MSM6246 = 1,
  DALCHIPINFO_FAMILY_MSM6260 = 2,
  DALCHIPINFO_FAMILY_QSC6270 = 3,
  DALCHIPINFO_FAMILY_MSM6280 = 4,
  DALCHIPINFO_FAMILY_MSM6290 = 5,
  DALCHIPINFO_FAMILY_MSM7200 = 6,
  DALCHIPINFO_FAMILY_MSM7500 = 7,
  DALCHIPINFO_FAMILY_MSM7600 = 8,
  DALCHIPINFO_FAMILY_MSM7625 = 9,
  DALCHIPINFO_FAMILY_MSM7X30 = 10,
  DALCHIPINFO_FAMILY_MSM7800 = 11,
  DALCHIPINFO_FAMILY_MDM8200 = 12,
  DALCHIPINFO_FAMILY_QSD8650 = 13,
  DALCHIPINFO_FAMILY_MSM7627 = 14,
  DALCHIPINFO_FAMILY_QSC6695 = 15,
  DALCHIPINFO_FAMILY_MDM9X00 = 16,
  DALCHIPINFO_FAMILY_QSD8650A = 17,
  DALCHIPINFO_FAMILY_MSM8X60 = 18,
  DALCHIPINFO_FAMILY_MDM8200A = 19,
  DALCHIPINFO_FAMILY_QSD8672 = 20,
  DALCHIPINFO_FAMILY_MDM6615 = 21,
  DALCHIPINFO_FAMILY_MSM8660 = DALCHIPINFO_FAMILY_MSM8X60,
  DALCHIPINFO_FAMILY_MSM8960 = 22,
  DALCHIPINFO_FAMILY_MSM7625A = 23,
  DALCHIPINFO_FAMILY_MSM7627A = 24,
  DALCHIPINFO_FAMILY_MDM9X15 = 25,
  DALCHIPINFO_FAMILY_MSM8930 = 26,
  DALCHIPINFO_FAMILY_MSM8630 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8230 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_APQ8030 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8627 = 30,
  DALCHIPINFO_FAMILY_MSM8227 = DALCHIPINFO_FAMILY_MSM8627,
  DALCHIPINFO_FAMILY_MSM8974 = 32,
  DALCHIPINFO_FAMILY_MSM8625 = 33,
  DALCHIPINFO_FAMILY_MSM8225 = DALCHIPINFO_FAMILY_MSM8625,
  DALCHIPINFO_FAMILY_APQ8064 = 34,
  DALCHIPINFO_FAMILY_MDM9x25 = 35,
  DALCHIPINFO_FAMILY_MSM8960AB = 36,
  DALCHIPINFO_FAMILY_MSM8930AB = 37,
  DALCHIPINFO_FAMILY_MSM8x10 = 38,
  DALCHIPINFO_FAMILY_MPQ8092 = 39,
  DALCHIPINFO_FAMILY_MSM8x26 = 40,
  DALCHIPINFO_FAMILY_MSM8225Q = 41,
  DALCHIPINFO_FAMILY_MSM8625Q = 42,
  DALCHIPINFO_FAMILY_APQ8x94 = 43,
  DALCHIPINFO_FAMILY_APQ8084 = DALCHIPINFO_FAMILY_APQ8x94,
  DALCHIPINFO_FAMILY_MSM8x32 = 44,
  DALCHIPINFO_FAMILY_MDM9x35 = 45,
  DALCHIPINFO_FAMILY_MSM8974_PRO= 46,
  DALCHIPINFO_FAMILY_FSM9900 = 47,
  DALCHIPINFO_FAMILY_MSM8x62 = 48,
  DALCHIPINFO_FAMILY_MSM8926 = 49,
  DALCHIPINFO_FAMILY_MSM8994 = 50,
  DALCHIPINFO_FAMILY_IPQ8064 = 51,
  DALCHIPINFO_FAMILY_MSM8916 = 52,
  DALCHIPINFO_FAMILY_MSM8936 = 53,
  DALCHIPINFO_FAMILY_MDM9x45 = 54,
  DALCHIPINFO_FAMILY_MSM8996 = 56,
  DALCHIPINFO_FAMILY_APQ8096 = DALCHIPINFO_FAMILY_MSM8996,
  DALCHIPINFO_FAMILY_MSM8992 = 57,
  DALCHIPINFO_FAMILY_MSM8909 = 58,
  DALCHIPINFO_FAMILY_FSM90xx = 59,
  DALCHIPINFO_FAMILY_MSM8952 = 60,
  DALCHIPINFO_FAMILY_QDF2432 = 61,
  DALCHIPINFO_FAMILY_MSM8929 = 62,
  DALCHIPINFO_FAMILY_MSM8956 = 63,
  DALCHIPINFO_FAMILY_QCA961x = 64,
  DALCHIPINFO_FAMILY_IPQ40xx = DALCHIPINFO_FAMILY_QCA961x,
  DALCHIPINFO_FAMILY_MDM9x55 = 65,
  DALCHIPINFO_FAMILY_MDM9x07 = 66,
  DALCHIPINFO_FAMILY_MSM8998 = 67,
  DALCHIPINFO_FAMILY_MSM8953 = 68,
  DALCHIPINFO_FAMILY_MSM8993 = 69,
  DALCHIPINFO_FAMILY_MSM8937 = 70,
  DALCHIPINFO_FAMILY_MSM8917 = 71,
  DALCHIPINFO_FAMILY_MSM8996SG = 72,
  DALCHIPINFO_FAMILY_MSM8997 = 73,
  DALCHIPINFO_NUM_FAMILIES = 74,
  DALCHIPINFO_FAMILY_32BITS = 0x7FFFFFF
} DalChipInfoFamilyType;
typedef struct DalChipInfo DalChipInfo;
struct DalChipInfo
{
   DalDevice DalChipInfoDevice;
   DALResult (*GetChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoVersionType * pnVersion);
   DALResult (*GetRawChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnVersion);
   DALResult (*GetChipId)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoIdType * peId);
   DALResult (*GetRawChipId)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnId);
   DALResult (*GetChipIdString)(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength);
   DALResult (*GetChipFamily)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoFamilyType * peFamily);
   DALResult (*GetModemSupport)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoModemType * pnModem);
};
typedef struct DalChipInfoHandle DalChipInfoHandle;
struct DalChipInfoHandle
{
   uint32 dwDalHandleId;
   const DalChipInfo * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalChipInfo_GetChipVersion(DalDeviceHandle * _h, DalChipInfoVersionType * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipVersion( _h, 0, pnVersion);
}
static __inline DALResult
DalChipInfo_GetRawChipVersion(DalDeviceHandle * _h, uint32 * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipVersion( _h, 0, pnVersion);
}
static __inline DALResult
DalChipInfo_GetChipId(DalDeviceHandle * _h, DalChipInfoIdType * peId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipId( _h, 0, peId);
}
static __inline DALResult
DalChipInfo_GetRawChipId(DalDeviceHandle * _h, uint32 * pnId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipId( _h, 0, pnId);
}
static __inline DALResult
DalChipInfo_GetChipIdString(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_9(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipIdString)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (char * )szIdString, nMaxLength);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipIdString( _h, szIdString, nMaxLength);
}
static __inline DALResult
DalChipInfo_GetChipFamily(DalDeviceHandle * _h, DalChipInfoFamilyType * peFamily)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipFamily)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peFamily);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipFamily( _h, 0, peFamily);
}
static __inline DALResult
DalChipInfo_GetModemSupport(DalDeviceHandle * _h, DalChipInfoModemType * pnModem)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetModemSupport)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnModem);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetModemSupport( _h, 0, pnModem);
}
static __inline DalChipInfoVersionType
DalChipInfo_ChipVersion(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoVersionType nVersion;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return (DalChipInfoVersionType)0;
    }
  }
  DalChipInfo_GetChipVersion(phChipInfo, &nVersion);
  return nVersion;
}
static __inline DalChipInfoIdType
DalChipInfo_ChipId(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoIdType neId;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return DALCHIPINFO_ID_UNKNOWN;
    }
  }
  DalChipInfo_GetChipId(phChipInfo, &neId);
  return neId;
}
static __inline DalChipInfoFamilyType
DalChipInfo_ChipFamily(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoFamilyType neFamily;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return DALCHIPINFO_FAMILY_UNKNOWN;
    }
  }
  DalChipInfo_GetChipFamily(phChipInfo, &neFamily);
  return neFamily;
}
typedef DALResult SBResult;
typedef DalDeviceHandle *SlimBusHandle;
typedef DALSYSMemHandle SlimBusMemHandle;
typedef DALSYSSyncHandle SlimBusSyncHandle;
typedef DALSYSEventHandle SlimBusEventHandle;
typedef DalDeviceHandle *SlimBusTimerHandle;
typedef DalDeviceHandle *SlimBusTlmmHandle;
typedef DalDeviceHandle *SlimBusGpioIntHandle;
typedef DALSYSWorkLoopHandle SlimBusWorkLoopHandle;
typedef DALBOOL SBBOOL;
typedef DALSYSEventObj SlimBusEventObj;
struct SlimBusDrvCtxt;
struct SlimBusDevCtxt;
struct SlimBusClientCtxt;
struct DalSlimBusHandle;
typedef void * (*SlimBusCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DalSlimBusHandle SlimBusRefHandle;
typedef struct SlimBusDrvVtbl SlimBusDrvVtbl;
struct SlimBusDrvVtbl
{
  int (*SlimBus_DriverInit)(struct SlimBusDrvCtxt *);
  int (*SlimBus_DriverDeInit)(struct SlimBusDrvCtxt *);
};
typedef struct
{
  uint32 dwRefs;
  DALDEVICEID DevId;
  uint32 dwDevCtxtRefIdx;
  struct SlimBusDrvCtxt *pSlimBusDrvCtxt;
  uint32 hProp[2];
  uint32 Reserved[16];
} SlimBusBaseDevType;
typedef struct
{
  SlimBusDrvVtbl SlimBusDrvVtbl;
  uint32 dwNumDev;
  uint32 dwSizeDevCtxt;
  uint32 bInit;
  uint32 dwRefs;
} SlimBusBaseDrvType;
typedef struct SlimBusIsrCtxt
{
  uint32 uMsgIntStatus;
  uint32 uFrmIntStatus;
  uint32 uIntfIntStatus;
  uint32 uPgdIntStatus;
  uint32 uPgdPortIntRawStatus;
  uint32 uPgdPortIntMaskedStatus;
  uint32 uPortDisMask;
  uint32 uPortUnderFlowMask;
  uint32 uPortOverFlowMask;
  uint32 uPortDmaMask;
} SlimBusIsrCtxt;
typedef struct
{
  bam_handle hBAM;
  SlimBusTlmmHandle hTlmm;
  SlimBusGpioIntHandle hGpioInt;
  SlimBusSyncHandle hBamSync;
  uint32 hMMPM;
  npa_client_handle hSVS;
  MmpmRegParamType *pMmpmRegParam;
  DalChipInfoVersionType chipVersion;
} SlimBusDevPlatType;
typedef size_t HAL_sb_HandleType;
typedef enum
{
  HAL_SB_PGD_PORT_DATA_ALIGN_LSB = 0,
  HAL_SB_PGD_PORT_DATA_ALIGN_MSB = 1,
  HAL_SB_PGD_PORT_DATA_ALIGN_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_PgdPortDataAlignType;
typedef enum
{
  HAL_SB_DEVICE_MANAGER,
  HAL_SB_DEVICE_INTERFACE,
  HAL_SB_DEVICE_FRAMER,
  HAL_SB_DEVICE_PORTED_GENERIC,
  HAL_SB_DEVICE_NON_PORTED_GENERIC_1,
  HAL_SB_DEVICE_NON_PORTED_GENERIC_2,
  HAL_SB_DEVICE_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_DeviceType;
typedef enum
{
  HAL_SB_NON_PORTED_GENERIC_1,
  HAL_SB_NON_PORTED_GENERIC_2,
  HAL_SB_NGD_DEVICE_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_NgdDeviceType;
typedef enum
{
  HAL_SB_DEVICE_MSG_QUEUE_RX,
  HAL_SB_DEVICE_MSG_QUEUE_TX,
  HAL_SB_DEVICE_MSG_QUEUE_TX_PRIORITY_LOW = HAL_SB_DEVICE_MSG_QUEUE_TX,
  HAL_SB_DEVICE_MSG_QUEUE_TX_PRIORITY_HIGH,
  HAL_SB_DEVICE_MSG_QUEUE_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_DeviceMsgQType;
typedef struct
{
  uint32 UNSPRTD_MSG:1;
  uint32 DATA_TX_COL:1;
  uint32 RECONFIG_OBJECTION:1;
  uint32 EX_ERROR:1;
  uint32 MC_TX_COL:1;
  uint32 LOST_FS:1;
  uint32 LOST_SFS:1;
  uint32 LOST_MS:1;
  uint32 DS_OVERLAP:1;
  uint32 RESERVED:7;
  uint32 DEVICE_CLASS_VERSION:8;
  uint32 DEVICE_CLASS:8;
}HAL_sb_IntfIeType;
typedef struct
{
  uint32 UNSPRTD_MSG:1;
  uint32 RESERVED1:1;
  uint32 RECONFIG_OBJECTION:1;
  uint32 EX_ERROR:1;
  uint32 RESERVED:12;
  uint32 DEVICE_CLASS_VERSION:8;
  uint32 DEVICE_CLASS:8;
}HAL_sb_MgrIeType;
typedef struct
{
  uint32 UNSPRTD_MSG:1;
  uint32 RESERVED1:1;
  uint32 RECONFIG_OBJECTION:1;
  uint32 EX_ERROR:1;
  uint32 ACTIVE_FRAMER:1;
  uint32 FS_TX_COL:1;
  uint32 FI_TX_COL:1;
  uint32 GC_TX_COL:1;
  uint32 CLK_QUAL:2;
  uint32 RESERVED2:6;
  uint32 DEVICE_CLASS_VERSION:8;
  uint32 DEVICE_CLASS:8;
}HAL_sb_FrmIeType;
typedef struct
{
  uint32 UNSPRTD_MSG:1;
  uint32 DATA_TX_COL:1;
  uint32 RECONFIG_OBJECTION:1;
  uint32 EX_ERROR:1;
  uint32 RESERVED1:12;
  uint32 DEVICE_CLASS_VERSION:8;
  uint32 DEVICE_CLASS:8;
}HAL_sb_GdIeType;
typedef enum
{
  HAL_SB_TP_ISOCHRONOUS = 0x0,
  HAL_SB_TP_PUSHED = 0x1,
  HAL_SB_TP_PULLED = 0x2,
  HAL_SB_TP_LOCKED = 0x3,
  HAL_SB_TP_ASYNC_SIMPLEX = 0x4,
  HAL_SB_TP_ASYNC_HALF_DUPLEX=0x5,
  HAL_SB_TP_EX_ASYNC_SIMPLEX=0x6,
  HAL_SB_TP_EX_ASYNC_HALF_DUPLEX=0x7,
  HAL_SB_TP_USER_DEFINED_1=0xE,
  HAL_SB_TP_USER_DEFINED_2=0xF,
  HAL_SB_TP_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_TransportProtocolType;
typedef enum
{
  HAL_SB_MGR_INT_RECONFIG_DONE = 0x1000000,
  HAL_SB_MGR_INT_TX_MSG_NACKED_TWICE = 0x2000000,
  HAL_SB_MGR_INT_TX_MSG_BUF_CONTENTION = 0x4000000,
  HAL_SB_MGR_INT_INVALID_TX_MSG = 0x8000000,
  HAL_SB_MGR_INT_IE_VE_CHANGE = 0x10000000,
  HAL_SB_MGR_INT_DEV_ERR = 0x20000000,
  HAL_SB_MGR_INT_RX_MSG_RCVD = 0x40000000,
  HAL_SB_MGR_INT_TX_MSG_SENT = (int32)0x80000000,
  HAL_SB_MGR_INT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_MgrIntType;
typedef enum
{
  HAL_SB_FRM_INT_DEV_ERR = 0x1,
  HAL_SB_FRM_INT_IE_VE_CHANGE = 0x2,
  HAL_SB_FRM_INT_SLIMBUS_BOOT_COMPLETE = 0x4,
  HAL_SB_FRM_INT_IDLE_MODE_EXITED = 0x8,
  HAL_SB_FRM_INT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_FrmIntType;
typedef enum
{
  HAL_SB_INTF_INT_DEV_ERR = 0x1,
  HAL_SB_INTF_INT_IE_VE_CHANGE = 0x2,
  HAL_SB_INTF_INT_NO_DATA_LINE_ACTIVITY = 0x4,
  HAL_SB_INTF_INT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_IntfIntType;
typedef enum
{
  HAL_SB_NGD_INT_RECONFIG_DONE = 0x1000000,
  HAL_SB_NGD_INT_TX_MSG_NACKED_TWICE = 0x2000000,
  HAL_SB_NGD_INT_TX_MSG_BUF_CONTENTION = 0x4000000,
  HAL_SB_NGD_INT_INVALID_TX_MSG = 0x8000000,
  HAL_SB_NGD_INT_IE_VE_CHANGE = 0x10000000,
  HAL_SB_NGD_INT_DEV_ERR = 0x20000000,
  HAL_SB_NGD_INT_RX_MSG_RCVD = 0x40000000,
  HAL_SB_NGD_INT_TX_MSG_SENT = (int32)0x80000000,
  HAL_SB_NGD_INT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_NgdIntType;
typedef enum
{
  HAL_SB_PGD_INT_DEV_ERR = 0x1,
  HAL_SB_PGD_INT_IE_VE_CHANGE = 0x2,
  HAL_SB_PGD_INT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_PgdIntType;
typedef enum
{
  HAL_SB_INTERFACE_TESTBUS = 0x0,
  HAL_SB_FRAMER_TESTBUS = 0x1,
  HAL_SB_MANAGER_TESTBUS = 0x2,
  HAL_SB_PORTED_GENERIC_TESTBUS = 0x3,
  HAL_SB_NON_PORTED_GENERIC_1_TESTBUS = 0x4,
  HAL_SB_NON_PORTED_GENERIC_2_TESTBUS = 0x5,
  HAL_SB_MP_TESTBUS = 0x6,
  HAL_SB_FRAME_LAYER_TESTBUS = 0x7,
  HAL_SB_DEVICE_TESTBUS_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_TestBusType;
typedef enum
{
  HAL_SB_PGD_PORT_NO_EVENT = 0x0,
  HAL_SB_PGD_PORT_OVERFLOW_EVENT = 0x1,
  HAL_SB_PGD_PORT_UNDERFLOW_EVENT = 0x2,
  HAL_SB_PGD_PORT_DISCONNECT_EVENT = 0x4,
  HAL_SB_PGD_PORT_REMOVE_CHANNEL_EVENT = 0x8,
  HAL_SB_PGD_PORT_DMA_EVENT = 0x10,
  HAL_SB_DEVICE_PGD_PORT_EVENT_FORCE32BITS = 0x7FFFFFFF
}HAL_sb_PgdPortEventType;
typedef void (*HAL_sb_ClkPauseFcnType)(uint32 uPauseTimeUs);
typedef struct HAL_sb_EnvType
{
  HAL_sb_ClkPauseFcnType pfnClkPauseUs;
} HAL_sb_EnvType;
void HAL_sb_ConfigEnv(const HAL_sb_EnvType *pEnv);
void HAL_sb_Init(HAL_sb_HandleType hHandle, char **ppszSbVersion);
void HAL_sb_Reset(HAL_sb_HandleType hHandle);
uint32 HAL_sb_CompGetHwVersion(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IsPortDmaIntSupported(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IsShadowProgressCounterSupported(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IsPortUpperWatermarkSupported(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IsFrameSyncShiftSupported(HAL_sb_HandleType hHandle);
void HAL_sb_CompEnable(HAL_sb_HandleType hHandle);
void HAL_sb_CompDisable(HAL_sb_HandleType hHandle);
void HAL_sb_CompEnablePhaseShift(HAL_sb_HandleType hHandle);
void HAL_sb_CompDisablePhaseShift(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompIsEnabled(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompReset(HAL_sb_HandleType hHandle);
void HAL_sb_CompAssertInterruptEe(HAL_sb_HandleType hHandle, uint32 uEE);
void HAL_sb_CompDeassertInterruptEe(HAL_sb_HandleType hHandle, uint32 uEE);
void HAL_sb_CompTestBusEnable(HAL_sb_HandleType hHandle);
void HAL_sb_CompTestBusDisable(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompTestBusIsEnabled(HAL_sb_HandleType hHandle);
void HAL_sb_CompSetTestBusSelect(HAL_sb_HandleType hHandle,
                                 HAL_sb_TestBusType eTestBus);
HAL_sb_TestBusType HAL_sb_CompGetTestBusSelect(HAL_sb_HandleType hHandle);
void HAL_sb_CompHaltReconfig(HAL_sb_HandleType hHandle);
void HAL_sb_CompUnhaltReconfig(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompIsHaltReconfig(HAL_sb_HandleType hHandle);
void HAL_sb_CompHaltSwReset(HAL_sb_HandleType hHandle);
void HAL_sb_CompUnhaltSwReset(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompIsHaltSwReset(HAL_sb_HandleType hHandle);
void HAL_sb_CompHaltEnChange(HAL_sb_HandleType hHandle);
void HAL_sb_CompUnhaltEnChange(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompIsHaltEnChange(HAL_sb_HandleType hHandle);
void HAL_sb_CompHaltDeviceTxMsg(HAL_sb_HandleType hHandle,
                                HAL_sb_DeviceType eSbDevice);
void HAL_sb_CompUnhaltDeviceTxMsg(HAL_sb_HandleType hHandle,
                                  HAL_sb_DeviceType eSbDevice);
bool32 HAL_sb_CompIsHaltDeviceTxMsg(HAL_sb_HandleType hHandle,
                                    HAL_sb_DeviceType eSbDevice);
void HAL_sb_CompSetNgdEe(HAL_sb_HandleType hHandle,
                         HAL_sb_NgdDeviceType eNGD,
                         uint32 uEE);
uint32 HAL_sb_CompGetNgdEe(HAL_sb_HandleType hHandle,
                                  HAL_sb_NgdDeviceType eNGD);
void HAL_sb_CompSetMgrRscGrpEe(HAL_sb_HandleType hHandle, uint32 uEE);
uint32 HAL_sb_CompGetMgrRscGrpEe(HAL_sb_HandleType hHandle);
void HAL_sb_CompMgrSetTrustDevAccessEnable(HAL_sb_HandleType hHandle);
void HAL_sb_CompMgrSetTrustDevAccessDisable(HAL_sb_HandleType hHandle);
bool32 HAL_sb_CompMgrIsTrustDevAccessEnabled(HAL_sb_HandleType hHandle);
void HAL_sb_CompSetTrustDevLa(HAL_sb_HandleType hHandle,
                              uint32 uLogicalAddress);
uint32 HAL_sb_CompGetTrustDevLa(HAL_sb_HandleType hHandle);
void HAL_sb_CompSetTrustDevEa(HAL_sb_HandleType hHandle, uint8 *pucEnumAddress);
void HAL_sb_CompGetTrustDevEa(HAL_sb_HandleType hHandle, uint8 *pucEnumAddress);
void HAL_sb_DeviceEnable(HAL_sb_HandleType hHandle,
                         HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceDisable(HAL_sb_HandleType hHandle,
                          HAL_sb_DeviceType eSbDevice);
bool32 HAL_sb_DeviceIsEnabled(HAL_sb_HandleType hHandle,
                              HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_DeviceGetErrorStatus(HAL_sb_HandleType hHandle,
                                   HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceIntSetMask(HAL_sb_HandleType hHandle,
                             HAL_sb_DeviceType eSbDevice,
                             uint32 uMask);
uint32 HAL_sb_DeviceIntGetMask(HAL_sb_HandleType hHandle,
                               HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_DeviceIntGetStatus(HAL_sb_HandleType hHandle,
                                 HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceIntClear(HAL_sb_HandleType hHandle,
                           HAL_sb_DeviceType eSbDevice,
                           uint32 uMask);
uint32 HAL_sb_DeviceGetVeStatus(HAL_sb_HandleType hHandle,
                                HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_DeviceGetIeStatus(HAL_sb_HandleType hHandle,
                                HAL_sb_DeviceType eSbDevice);
bool32 HAL_sb_DeviceIsTxMsgBufferBusy(HAL_sb_HandleType hHandle,
                                      HAL_sb_DeviceType eSbDevice);
bool32 HAL_sb_DeviceIsEnumerated(HAL_sb_HandleType hHandle,
                                 HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_DeviceMsgQGetPipeOffset(HAL_sb_HandleType hHandle,
                                      HAL_sb_DeviceType eSbDevice,
                                      HAL_sb_DeviceMsgQType eMsgQ);
uint32 HAL_sb_DeviceTxMsgGetAckedMc(HAL_sb_HandleType hHandle,
                                    HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_DeviceTxMsgGetNackedMc(HAL_sb_HandleType hHandle,
                                     HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceRxMsgQSetBlockSize(HAL_sb_HandleType hHandle,
                                     HAL_sb_DeviceType eSbDevice,
                                     uint32 uBlockSize);
uint32 HAL_sb_DeviceRxMsgQGetBlockSize(HAL_sb_HandleType hHandle,
                                       HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceRxMsgQSetTransSize(HAL_sb_HandleType hHandle,
                                      HAL_sb_DeviceType eSbDevice,
                                      uint32 uTransSize);
uint32 HAL_sb_DeviceRxMsgQGetTransSize(HAL_sb_HandleType hHandle,
                                       HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceRxMsgQSetTimeOutVal(HAL_sb_HandleType hHandle,
                                      HAL_sb_DeviceType eSbDevice,
                                      uint32 uTimeoutValInAhbClkCycles);
uint32 HAL_sb_DeviceRxMsgQGetTimeOutVal(HAL_sb_HandleType hHandle,
                                        HAL_sb_DeviceType eSbDevice);
void HAL_sb_DeviceTxMsgWrite(HAL_sb_HandleType hHandle,
                             HAL_sb_DeviceType eSbDevice,
                             uint32* pBuffer,
                             uint32 uBufferSize);
void HAL_sb_DeviceRxMsgRead(HAL_sb_HandleType hHandle,
                            HAL_sb_DeviceType eSbDevice,
                            uint32* pBuffer,
                            uint32 uBufferSize);
bool32 HAL_sb_DeviceMsgQIsEnabled(HAL_sb_HandleType hHandle,
                                  HAL_sb_DeviceType eSbDevice,
                                  HAL_sb_DeviceMsgQType eMsgQ);
bool32 HAL_sb_DeviceMsgQEnable(HAL_sb_HandleType hHandle,
                               HAL_sb_DeviceType eSbDevice,
                               HAL_sb_DeviceMsgQType eMsgQ);
void HAL_sb_DeviceMsgQDisable(HAL_sb_HandleType hHandle,
                              HAL_sb_DeviceType eSbDevice,
                              HAL_sb_DeviceMsgQType eMsgQ);
bool32 HAL_sb_DeviceIsMsgQsSupported(HAL_sb_HandleType hHandle,
                                     HAL_sb_DeviceType eSbDevice);
uint32 HAL_sb_IntfGetAssignedLA(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IntfIsFrameSyncAcquired(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IntfIsSuperframeSyncAcquired(HAL_sb_HandleType hHandle);
bool32 HAL_sb_IntfIsMessageSyncAcquired(HAL_sb_HandleType hHandle);
void HAL_sb_FrmInternalWakeupEnable(HAL_sb_HandleType hHandle);
void HAL_sb_FrmInternalWakeupDisable(HAL_sb_HandleType hHandle);
bool32 HAL_sb_FrmIsInternalWakeupEnabled(HAL_sb_HandleType hHandle);
uint32 HAL_sb_FrmGetRefClkClkGear(HAL_sb_HandleType hHandle);
void HAL_sb_FrmSetRefClkClkGear(HAL_sb_HandleType hHandle,
                                uint32 uRefClkClkGear);
uint32 HAL_sb_FrmGetBootRootFreq(HAL_sb_HandleType hHandle);
void HAL_sb_FrmSetBootRootFreq(HAL_sb_HandleType hHandle, uint32 uBootRootFreq);
uint32 HAL_sb_FrmGetBootClkGear(HAL_sb_HandleType hHandle);
void HAL_sb_FrmSetBootClkGear(HAL_sb_HandleType hHandle, uint32 uBootClkGear);
uint32 HAL_sb_FrmGetBootSubFrmMode(HAL_sb_HandleType hHandle);
void HAL_sb_FrmSetBootSubFrmMode(HAL_sb_HandleType hHandle,
                                 uint32 uBootSubFrmMode);
void HAL_sb_FrmBootSetActiveFrm(HAL_sb_HandleType hHandle);
void HAL_sb_FrmBootSetUnActiveFrm(HAL_sb_HandleType hHandle);
bool32 HAL_sb_FrmBootIsActiveFrm(HAL_sb_HandleType hHandle);
bool32 HAL_sb_FrmIsSbBooted(HAL_sb_HandleType hHandle);
uint32 HAL_sb_FrmGetAssignedLA(HAL_sb_HandleType hHandle);
uint32 HAL_sb_FrmGetCurSubFrmMode(HAL_sb_HandleType hHandle);
uint32 HAL_sb_FrmGetCurClkGear(HAL_sb_HandleType hHandle);
uint32 HAL_sb_FrmGetCurRootFreq(HAL_sb_HandleType hHandle);
bool32 HAL_sb_FrmIsActiveFrm(HAL_sb_HandleType hHandle);
bool32 HAL_sb_FrmIsInIdleMode(HAL_sb_HandleType hHandle);
void HAL_sb_FrmWakeUp(HAL_sb_HandleType hHandle);
uint32 HAL_sb_PgdGetNumPorts(HAL_sb_HandleType hHandle, uint32 uPgdIndex);
uint32 HAL_sb_PgdGetPipeOffset(HAL_sb_HandleType hHandle, uint32 uPgdIndex);
uint32 HAL_sb_PgdGetNumPcVfrBlks(HAL_sb_HandleType hHandle, uint32 uPgdIndex);
void HAL_sb_PgdPortSetOwnerEe(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uEE,
                              uint32 uPortBitField);
uint32 HAL_sb_PgdPortGetOwnerEe(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uEE);
void HAL_sb_PgdPortSetIntEnEe(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uEE,
                              uint32 uPortBitField);
uint32 HAL_sb_PgdPortGetIntEnEe(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uEE);
uint32 HAL_sb_PgdPortGetIntStatusEe(HAL_sb_HandleType hHandle,
                                    uint32 uPgdIndex,
                                    uint32 uEE);
void HAL_sb_PgdPortClearIntEe(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uEE,
                              uint32 uPortBitField);
void HAL_sb_PgdPortEnable(HAL_sb_HandleType hHandle,
                          uint32 uPgdIndex,
                          uint32 uPortNum);
void HAL_sb_PgdPortDisable(HAL_sb_HandleType hHandle,
                           uint32 uPgdIndex,
                           uint32 uPortNum);
bool32 HAL_sb_PgdPortIsEnabled(HAL_sb_HandleType hHandle,
                               uint32 uPgdIndex,
                               uint32 uPortNum);
void HAL_sb_PgdPortSetWatermark(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum,
                                uint32 uWaterMark);
uint32 HAL_sb_PgdPortGetWatermark(HAL_sb_HandleType hHandle,
                                  uint32 uPgdIndex,
                                  uint32 uPortNum);
void HAL_sb_PgdPortSetUpperWatermark(HAL_sb_HandleType hHandle,
                                     uint32 uPgdIndex,
                                     uint32 uPortNum,
                                     uint32 uWaterMark);
uint32 HAL_sb_PgdPortGetUpperWatermark(HAL_sb_HandleType hHandle,
                                       uint32 uPgdIndex,
                                       uint32 uPortNum);
void HAL_sb_PgdPortUpperWatermarkEnable(HAL_sb_HandleType hHandle,
                                        uint32 uPgdIndex,
                                        uint32 uPortNum);
void HAL_sb_PgdPortUpperWatermarkDisable(HAL_sb_HandleType hHandle,
                                         uint32 uPgdIndex,
                                         uint32 uPortNum);
void HAL_sb_PgdPortPackEnable(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPortNum);
void HAL_sb_PgdPortPackDisable(HAL_sb_HandleType hHandle,
                               uint32 uPgdIndex,
                               uint32 uPortNum);
bool32 HAL_sb_PgdPortIsPackEnabled(HAL_sb_HandleType hHandle,
                                   uint32 uPgdIndex,
                                   uint32 uPortNum);
void HAL_sb_PgdPortSetAlignment(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum,
                                HAL_sb_PgdPortDataAlignType eAlignment);
HAL_sb_PgdPortDataAlignType HAL_sb_PgdPortGetAlignment
                                       (HAL_sb_HandleType hHandle,
                                        uint32 uPgdIndex,
                                        uint32 uPortNum);
void HAL_sb_PgdPortDmaIrqEnable(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum);
void HAL_sb_PgdPortDmaIrqDisable(HAL_sb_HandleType hHandle,
                                 uint32 uPgdIndex,
                                 uint32 uPortNum);
bool32 HAL_sb_PgdPortIsDmaIrqEnabled(HAL_sb_HandleType hHandle,
                                     uint32 uPgdIndex,
                                     uint32 uPortNum);
uint32 HAL_sb_PgdPortGetFifoSize(HAL_sb_HandleType hHandle,
                                 uint32 uPgdIndex,
                                 uint32 uPortNum);
bool32 HAL_sb_PgdPortIsOverflow(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum);
bool32 HAL_sb_PgdPortIsUnderflow(HAL_sb_HandleType hHandle,
                                 uint32 uPgdIndex,
                                 uint32 uPortNum);
uint32 HAL_sb_PgdPortGetPipeNum(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum);
uint32 HAL_sb_PgdPortGetCurrentFifoWordCount(HAL_sb_HandleType hHandle,
                                             uint32 uPgdIndex,
                                             uint32 uPortNum);
bool32 HAL_sb_PgdPortIsWatermarkHit(HAL_sb_HandleType hHandle,
                                    uint32 uPgdIndex,
                                    uint32 uPortNum);
bool32 HAL_sb_PgdPortIsDisconnectEventSet(HAL_sb_HandleType hHandle,
                                          uint32 uPgdIndex,
                                          uint32 uPortNum);
bool32 HAL_sb_PgdPortIsDmaEventSet(HAL_sb_HandleType hHandle,
                                   uint32 uPgdIndex,
                                   uint32 uPortNum);
HAL_sb_PgdPortEventType HAL_sb_PgdPortGetEvents(HAL_sb_HandleType hHandle,
                                                uint32 uPgdIndex,
                                                uint32 uPortNum);
bool32 HAL_sb_PgdPortIsChannelActive(HAL_sb_HandleType hHandle,
                                     uint32 uPgdIndex,
                                     uint32 uPortNum);
uint32 HAL_sb_PgdPortGetChannelNumber(HAL_sb_HandleType hHandle,
                                      uint32 uPgdIndex,
                                      uint32 uPortNum);
uint32 HAL_sb_PgdPortGetSegDistribution(HAL_sb_HandleType hHandle,
                                        uint32 uPgdIndex,
                                        uint32 uPortNum);
HAL_sb_TransportProtocolType HAL_sb_PgdPortGetTransportProtocol(
                                   HAL_sb_HandleType hHandle,
                                   uint32 uPgdIndex,
                                   uint32 uPortNum);
uint32 HAL_sb_PgdPortGetSegLen(HAL_sb_HandleType hHandle,
                               uint32 uPgdIndex,
                               uint32 uPortNum);
void HAL_sb_PgdPortSetBlkSize(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPortNum,
                              uint32 uBlkSize);
uint32 HAL_sb_PgdPortGetBlkSize(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum);
void HAL_sb_PgdPortSetTransSize(HAL_sb_HandleType hHandle,
                                uint32 uPgdIndex,
                                uint32 uPortNum,
                                uint32 uTransSize);
uint32 HAL_sb_PgdPortGetTransSize(HAL_sb_HandleType hHandle,
                                  uint32 uPgdIndex,
                                  uint32 uPortNum);
void HAL_sb_PgdPortSetMultiChanl(HAL_sb_HandleType hHandle,
                                 uint32 uPgdIndex,
                                 uint32 uPortBitField);
uint32 HAL_sb_PgdPortGetMultiChanl(HAL_sb_HandleType hHandle,
                                   uint32 uPgdIndex,
                                   uint32 uPortNum);
void HAL_sb_PgdPortSetRptPeriodForPushPull(HAL_sb_HandleType hHandle,
                                           uint32 uPgdIndex,
                                           uint32 uPortNum,
                                           uint32 uRepeatPeriod);
uint32 HAL_sb_PgdPortGetRptPeriodForPushPull(HAL_sb_HandleType hHandle,
                                             uint32 uPgdIndex,
                                             uint32 uPortNum);
void HAL_sb_PgdPortSetNumSamplesForPushPull(HAL_sb_HandleType hHandle,
                                            uint32 uPgdIndex,
                                            uint32 uPortNum,
                                            uint32 uNumSamples);
uint32 HAL_sb_PgdPortGetNumSamplesForPushPull(HAL_sb_HandleType hHandle,
                                              uint32 uPgdIndex,
                                              uint32 uPortNum);
void HAL_sb_PgdPcSetInitVal(HAL_sb_HandleType hHandle,
                            uint32 uPgdIndex,
                            uint32 uPcNum,
                            uint32 uInitVal);
uint32 HAL_sb_PgdPcGetInitVal(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPcNum);
void HAL_sb_PgdPcSetPortNum(HAL_sb_HandleType hHandle,
                            uint32 uPgdIndex,
                            uint32 uPcNum,
                            uint32 uPortNum);
uint32 HAL_sb_PgdPcGetPortNum(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPcNum);
void HAL_sb_PgdPcLoadInitVal(HAL_sb_HandleType hHandle,
                             uint32 uPgdIndex,
                             uint32 uPcNum);
void HAL_sb_PgdPcSetAndLoadInitVal(HAL_sb_HandleType hHandle,
                                   uint32 uPgdIndex,
                                   uint32 uPcNum,
                                   uint32 uInitVal);
void HAL_sb_PgdPcGetVal(HAL_sb_HandleType hHandle,
                        uint32 uPgdIndex,
                        uint32 uPcNum,
                        uint32 *puWordsDMAed,
                        uint32 *puNumSamplesinFifo);
void HAL_sb_PgdPcGetShadowVal(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPcNum,
                              uint32 *puWordsDMAed,
                              uint32 *puNumSamplesinFifo);
void HAL_sb_PgdPcGetValOnVfrTs(HAL_sb_HandleType hHandle,
                                 uint32 uPgdIndex,
                                 uint32 uPcNum,
                                 uint32 *puWordsDMAed,
                                 uint32 *puNumSamplesinFifo);
bool32 HAL_sb_PgdPcIsVfrIntSet(HAL_sb_HandleType hHandle,
                               uint32 uPgdIndex,
                               uint32 uPcNum);
void HAL_sb_PgdPcClearVfrInt(HAL_sb_HandleType hHandle,
                             uint32 uPgdIndex,
                             uint32 uPcNum);
void HAL_sb_PgdPcGetTimestamp(HAL_sb_HandleType hHandle,
                              uint32 uPgdIndex,
                              uint32 uPcNum,
                              uint64 *puTimestamp,
                              bool32 *pbTimestampValid,
                              bool32 *pbSampleMissed);
typedef enum
{
  SB_MSG_MT_CORE = 0x0,
  SB_MSG_MT_DEST_REFERRED_CLASS = 0x1,
  SB_MSG_MT_DEST_REFERRED_USER = 0x2,
  SB_MSG_MT_SRC_REFERRED_CLASS = 0x5,
  SB_MSG_MT_SRC_REFERRED_USER = 0x6,
} SlimBusMsgMtType;
typedef enum
{
  SB_MSG_DT_UNICAST = 0x0,
  SB_MSG_DT_BROADCAST = 0x1,
} SlimBusMsgDtType;
typedef enum
{
  SB_MSG_MC_REPORT_PRESENT = 0x1,
  SB_MSG_MC_ASSIGN_LOGICAL_ADDRESS = 0x2,
  SB_MSG_MC_RESET_DEVICE = 0x4,
  SB_MSG_MC_CHANGE_LOGICAL_ADDRESS = 0x8,
  SB_MSG_MC_CHANGE_ARBITRATION_PRIORITY = 0x9,
  SB_MSG_MC_REQUEST_SELF_ANNOUNCEMENT = 0xC,
  SB_MSG_MC_REPORT_ABSENT = 0xF,
  SB_MSG_MC_CONNECT_SOURCE = 0x10,
  SB_MSG_MC_CONNECT_SINK = 0x11,
  SB_MSG_MC_DISCONNECT_PORT = 0x14,
  SB_MSG_MC_CHANGE_CONTENT = 0x18,
  SB_MSG_MC_REQUEST_INFORMATION = 0x20,
  SB_MSG_MC_REQUEST_CLEAR_INFORMATION = 0x21,
  SB_MSG_MC_REPLY_INFORMATION = 0x24,
  SB_MSG_MC_CLEAR_INFORMATION = 0x28,
  SB_MSG_MC_REPORT_INFORMATION = 0x29,
  SB_MSG_MC_BEGIN_RECONFIGURATION = 0x40,
  SB_MSG_MC_NEXT_ACTIVE_FRAMER = 0x44,
  SB_MSG_MC_NEXT_SUBFRAME_MODE = 0x45,
  SB_MSG_MC_NEXT_CLOCK_GEAR = 0x46,
  SB_MSG_MC_NEXT_ROOT_FREQUENCY = 0x47,
  SB_MSG_MC_NEXT_PAUSE_CLOCK = 0x4A,
  SB_MSG_MC_NEXT_RESET_BUS = 0x4B,
  SB_MSG_MC_NEXT_SHUTDOWN_BUS = 0x4C,
  SB_MSG_MC_NEXT_DEFINE_CHANNEL = 0x50,
  SB_MSG_MC_NEXT_DEFINE_CONTENT = 0x51,
  SB_MSG_MC_NEXT_ACTIVATE_CHANNEL = 0x54,
  SB_MSG_MC_NEXT_DEACTIVATE_CHANNEL = 0x55,
  SB_MSG_MC_NEXT_REMOVE_CHANNEL = 0x58,
  SB_MSG_MC_RECONFIGURE_NOW = 0x5F,
  SB_MSG_MC_REQUEST_VALUE = 0x60,
  SB_MSG_MC_REQUEST_CHANGE_VALUE = 0x61,
  SB_MSG_MC_REPLY_VALUE = 0x64,
  SB_MSG_MC_CHANGE_VALUE = 0x68,
} SlimBusMsgMcType;
typedef enum
{
  SB_MSG_MC_USR_QC_MASTER_CAPABILITY = 0x00,
  SB_MSG_MC_USR_REPORT_QC_SATELLITE = 0x01,
  SB_MSG_MC_USR_NOTIFY_MGR_SLEEP = 0x05,
  SB_MSG_MC_USR_NOTIFY_MGR_AWAKE = 0x06,
  SB_MSG_MC_USR_LA_ANNOUNCE = 0x0C,
  SB_MSG_MC_USR_LA_QUERY = 0x0D,
  SB_MSG_MC_USR_LA_REPLY = 0x0E,
  SB_MSG_MC_USR_CHANNEL_DEF = 0x20,
  SB_MSG_MC_USR_CHANNEL_DEF_ACTIVATE = 0x21,
  SB_MSG_MC_USR_CHANNEL_CONTROL = 0x23,
  SB_MSG_MC_USR_DO_RECONFIGURE_NOW = 0x24,
  SB_MSG_MC_USR_GENERIC_ACK = 0x25,
  SB_MSG_MC_USR_REQ_MSG_BANDWIDTH = 0x28,
  SB_MSG_MC_USR_REQ_CONNECT_SOURCE = 0x2C,
  SB_MSG_MC_USR_REQ_CONNECT_SINK = 0x2D,
  SB_MSG_MC_USR_REQ_DISCONNECT_PORT = 0x2E,
} SlimBusMsgMcUsrType;
typedef struct SlimBusDrvCtxt SlimBusDrvCtxt;
typedef struct SlimBusDevCtxt SlimBusDevCtxt;
typedef struct SlimBusClientCtxt SlimBusClientCtxt;
struct SlimBusDevTargetCtxt;
typedef struct SlimBusBasicClientCtxt SlimBusBasicClientCtxt;
struct SlimBusIsrCtxt;
typedef struct SlimBusBamCtxt SlimBusBamCtxt;
typedef struct SlimBusChanLAddAssociation SlimBusChanLAddAssociation;
typedef struct SlimBusSinkNode SlimBusSinkNode;
void *qurt_malloc( unsigned int size);
void *qurt_calloc(unsigned int elsize, unsigned int num);
void *qurt_realloc(void *ptr, int newsize);
void qurt_free( void *ptr);
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
typedef union qurt_mutex_aligned8{
    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;
} qurt_mutex_t;
void qurt_mutex_init(qurt_mutex_t *lock);
void qurt_mutex_destroy(qurt_mutex_t *lock);
void qurt_mutex_lock(qurt_mutex_t *lock);
void qurt_mutex_unlock(qurt_mutex_t *lock);
int qurt_mutex_try_lock(qurt_mutex_t *lock);
typedef union {
 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
int qurt_sem_down(qurt_sem_t *sem);
int qurt_sem_try_down(qurt_sem_t *sem);
void qurt_sem_init(qurt_sem_t *sem);
void qurt_sem_destroy(qurt_sem_t *sem);
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
typedef unsigned long long int qurt_pipe_data_t;
typedef struct {
    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;
} qurt_pipe_t;
typedef struct {
  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;
} qurt_pipe_attr_t;
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
void qurt_pipe_destroy(qurt_pipe_t *pipe);
void qurt_pipe_delete(qurt_pipe_t *pipe);
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
int qurt_printf(const char* format, ...);
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;
typedef unsigned int qurt_thread_t;
typedef struct _qurt_thread_attr {
    char name[16];
    unsigned char tcb_partition;
    unsigned char affinity;
    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;
} qurt_thread_attr_t;
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{
    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
void qurt_thread_get_name (char *name, unsigned char max_len);
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
void qurt_thread_stop(void);
int qurt_thread_resume(unsigned int thread_id);
qurt_thread_t qurt_thread_get_id (void);
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
void qurt_thread_set_timetest_id (unsigned short tid);
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
unsigned short qurt_thread_get_timetest_id (void);
void qurt_thread_exit(int status);
int qurt_thread_join(unsigned int tid, int *status);
unsigned int qurt_thread_get_anysignal(void);
int qurt_thread_get_priority (qurt_thread_t threadid);
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
unsigned int qurt_api_version(void);
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
unsigned int qurt_trace_get_marker(void);
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
unsigned int qurt_etm_enable(unsigned int enable_flag);
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
void qurt_profile_reset_idle_pcycles (void);
unsigned long long int qurt_profile_get_thread_pcycles(void);
unsigned long long int qurt_profile_get_thread_tcycles(void);
unsigned long long int qurt_get_core_pcycles(void);
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
void qurt_profile_reset_threadid_pcycles (int thread_id);
void qurt_profile_enable (int enable);
typedef struct {
   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;
} qurt_rmutex2_t;
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
typedef union {
    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;
} qurt_cond_t;
void qurt_cond_init(qurt_cond_t *cond);
void qurt_cond_destroy(qurt_cond_t *cond);
void qurt_cond_signal(qurt_cond_t *cond);
void qurt_cond_broadcast(qurt_cond_t *cond);
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
typedef union {
 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;
} qurt_barrier_t;
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
int qurt_barrier_destroy(qurt_barrier_t *barrier);
int qurt_barrier_wait(qurt_barrier_t *barrier);
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
unsigned int qurt_fastint_deregister(int intno);
unsigned int qurt_isr_register(int intno, void (*fn)(int));
unsigned int qurt_isr_deregister(int intno);
typedef union {
 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;
} qurt_allsignal_t;
void qurt_allsignal_init(qurt_allsignal_t *signal);
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
typedef union {
    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;
} qurt_signal_t;
void qurt_signal_init(qurt_signal_t *signal);
void qurt_signal_destroy(qurt_signal_t *signal);
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
unsigned int qurt_signal_get(qurt_signal_t *signal);
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
typedef qurt_signal_t qurt_anysignal_t;
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
void qurt_rmutex_init(qurt_mutex_t *lock);
void qurt_rmutex_destroy(qurt_mutex_t *lock);
void qurt_rmutex_lock(qurt_mutex_t *lock);
void qurt_rmutex_unlock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
void qurt_pimutex_init(qurt_mutex_t *lock);
void qurt_pimutex_destroy(qurt_mutex_t *lock);
void qurt_pimutex_lock(qurt_mutex_t *lock);
void qurt_pimutex_unlock(qurt_mutex_t *lock);
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
typedef struct {
   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;
   unsigned int queue;
   unsigned int wait_mask;
} qurt_signal2_t;
void qurt_signal2_init(qurt_signal2_t *signal);
void qurt_signal2_destroy(qurt_signal2_t *signal);
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);
int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
int qurt_interrupt_acknowledge(int int_num);
unsigned int qurt_interrupt_deregister(int int_num);
 unsigned int qurt_interrupt_enable(int int_num);
 unsigned int qurt_interrupt_disable(int int_num);
unsigned int qurt_interrupt_status(int int_num, int *status);
unsigned int qurt_interrupt_clear(int int_num);
unsigned int qurt_interrupt_get_registered(void);
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
int qurt_interrupt_raise(unsigned int interrupt_num);
void qurt_interrupt_disable_all(void);
int qurt_isr_subcall(void);
void * qurt_lifo_pop(void *freelist);
void qurt_lifo_push(void *freelist, void *buf);
void qurt_lifo_remove(void *freelist, void *buf);
static inline int qurt_power_shutdown_prepare(void){ return 0;}
int qurt_power_shutdown_enter (int type);
int qurt_power_exit(void);
int qurt_power_apcr_enter (void);
int qurt_power_tcxo_prepare (void);
int qurt_power_tcxo_fail_exit (void);
int qurt_power_tcxo_enter (void);
int qurt_power_tcxo_exit (void);
void qurt_power_override_wait_for_idle(int enable);
void qurt_power_wait_for_idle (void);
void qurt_power_wait_for_active (void);
unsigned int qurt_system_ipend_get (void);
void qurt_system_avscfg_set(unsigned int avscfg_value);
unsigned int qurt_system_avscfg_get(void);
unsigned int qurt_system_vid_get(void);
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );
int qurt_system_tcm_set_size(unsigned int new_size);
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
typedef struct qurt_sysenv_swap_pools {
   unsigned int spoolsize;
   unsigned int spooladdr;
}qurt_sysenv_swap_pools_t;
typedef struct qurt_sysenv_app_heap {
   unsigned int heap_base;
   unsigned int heap_limit;
} qurt_sysenv_app_heap_t ;
typedef struct qurt_sysenv_arch_version {
    unsigned int arch_version;
}qurt_arch_version_t;
typedef struct qurt_sysenv_max_hthreads {
   unsigned int max_hthreads;
}qurt_sysenv_max_hthreads_t;
typedef struct qurt_sysenv_max_pi_prio {
    unsigned int max_pi_prio;
}qurt_sysenv_max_pi_prio_t;
typedef struct qurt_sysenv_timer_hw {
   unsigned int base;
   unsigned int int_num;
}qurt_sysenv_hw_timer_t;
typedef struct qurt_sysenv_procname {
   unsigned int asid;
   char name[64];
}qurt_sysenv_procname_t;
typedef struct qurt_sysenv_stack_profile_count {
   unsigned int count;
}qurt_sysenv_stack_profile_count_t;
typedef struct _qurt_sysevent_error_t
{
    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;
} qurt_sysevent_error_t ;
typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);
unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
void qurt_exception_raise_fatal (void);
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
void qurt_exception_shutdown_fatal2(void);
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);
unsigned int qurt_enable_floating_point_exception(unsigned int mask);
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
void qurt_pmu_set (int reg_id, unsigned int reg_value);
unsigned int qurt_pmu_get (int red_id);
void qurt_pmu_enable (int enable);
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,
        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,
        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,
        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,
        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;
typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;
typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;
typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;
typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;
typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;
struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
typedef struct {
    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;
} qurt_mem_region_attr_t;
typedef struct {
    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];
} qurt_mem_pool_attr_t;
typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;
typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_delete (unsigned int entry_id);
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
unsigned short qurt_tlb_entry_get_available(void);
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
extern qurt_mem_pool_t qurt_mem_default_pool;
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
int qurt_mem_region_delete(qurt_mem_region_t region);
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}
void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
void qurt_l2fetch_disable(void);
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_write(int handle, const void *buf, unsigned len);
int qurt_qdi_read(int handle, void *buf, unsigned len);
int qurt_qdi_close(int handle);
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
extern unsigned long long qurt_sysclock_get_expiry (void);
unsigned long long qurt_sysclock_get_hw_ticks (void);
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;
    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));
    return (unsigned short)ticks;
}
unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
int qurt_spawn_flags(const char * name, int flags);
int qurt_space_switch(int asid);
int qurt_wait(int *status);
int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
typedef struct _qurt_process_attr {
    char name[64];
    int flags;
} qurt_process_attr_t;
int qurt_process_create (qurt_process_attr_t *attr);
int qurt_process_get_id (void);
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
typedef unsigned int mode_t;
int shm_open(const char * name, int oflag, mode_t mode);
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
int shm_close(int fd);
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;
typedef unsigned int qurt_timer_t;
typedef unsigned long long qurt_timer_duration_t;
typedef unsigned long long qurt_timer_time_t;
typedef struct
{
    unsigned int magic;
    qurt_timer_duration_t duration;
    qurt_timer_time_t expiry;
    qurt_timer_duration_t remaining;
    qurt_timer_type_t type;
    unsigned int group;
}
qurt_timer_attr_t;
int qurt_timer_stop (qurt_timer_t timer);
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);
int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
int qurt_timer_delete(qurt_timer_t timer);
int qurt_timer_sleep(qurt_timer_duration_t duration);
int qurt_timer_group_disable (unsigned int group);
int qurt_timer_group_enable (unsigned int group);
void qurt_timer_recover_pc (void);
static inline int qurt_timer_is_init (void) {return 1;};
unsigned long long qurt_timer_get_ticks (void);
int qurt_tls_create_key (int *key, void (*destructor)(void *));
int qurt_tls_set_specific (int key, const void *value);
void *qurt_tls_get_specific (int key);
int qurt_tls_delete_key (int key);
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}
static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}
static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}
int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);
int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_unlock(void);
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_get_mode(void);
int qurt_hvx_get_units(void);
int qurt_hvx_reserve(int num_units);
int qurt_hvx_cancel_reserve(void);
int qurt_hvx_get_lock_val(void);
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;
typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;
typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
unsigned long long qurt_mailbox_get_id(char *name);
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
int qurt_mailbox_delete(unsigned long long mailbox_id);
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};
typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;
typedef struct qurt_island_attr {
    int max_attrs;
    struct qurt_island_attr_resource attrs[1];
} qurt_island_attr_t;
typedef struct {
   int qdi_handle;
} qurt_island_t;
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
void qurt_island_attr_delete (qurt_island_attr_t *attr);
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
int qurt_island_spec_delete (qurt_island_t spec_id);
int qurt_island_enter (qurt_island_t spec_id);
int qurt_island_exit (void);
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
unsigned int qurt_island_get_status (void);
typedef void * ULogHandle;
typedef int32 ULogResult;
typedef enum
{
  ULOG_ERR_INVALIDNAME = 1,
  ULOG_ERR_INVALIDPARAMETER,
  ULOG_ERR_ALREADYCONNECTED,
  ULOG_ERR_ALREADYCREATED,
  ULOG_ERR_NOTCONNECTED,
  ULOG_ERR_ALREADYENABLED,
  ULOG_ERR_INITINCOMPLETE,
  ULOG_ERR_READONLY,
  ULOG_ERR_INVALIDHANDLE,
  ULOG_ERR_MALLOC,
  ULOG_ERR_ASSIGN,
  ULOG_ERR_INSUFFICIENT_BUFFER,
  ULOG_ERR_NAMENOTFOUND,
  ULOG_ERR_MISC,
  ULOG_ERR_OVERRUN,
  ULOG_ERR_NOTSUPPORTED,
} ULOG_ERRORS;
typedef enum
{
  ULOG_VALUE_BUFFER,
  ULOG_VALUE_SHARED_HEADER,
  ULOG_VALUE_READCORE,
  ULOG_VALUE_WRITECORE,
  ULOG_VALUE_LOCKCORE,
  ULOG_VALUE_LOG_BASE = 0x100,
  ULOG_VALUE_LOG_READFLAGS = ULOG_VALUE_LOG_BASE,
} ULOG_VALUE_ID;
typedef enum
{
  ULOG_LOCK_NONE = 0,
  ULOG_LOCK_OS,
  ULOG_LOCK_SPIN,
  ULOG_LOCK_INT,
  ULOG_LOCK_UINT32 = 0x7FFFFFFF
} ULOG_LOCK_TYPE;
typedef enum
{
  ULOG_ALLOC_LOCAL = 0x01,
  ULOG_ALLOC_MANUAL = 0x04,
  ULOG_ALLOC_TYPE_MASK = 0xFF,
  ULOG_ALLOC_UINT32 = 0x7FFFFFFF
} ULOG_ALLOC_TYPE;
typedef enum
{
  ULOG_MEMORY_USAGE_FULLACCESS = 0x0300,
  ULOG_MEMORY_USAGE_READABLE = 0x0100,
  ULOG_MEMORY_USAGE_WRITEABLE = 0x0200,
  ULOG_MEMORY_USAGE_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_USAGE_TYPE;
typedef enum
{
  ULOG_MEMORY_CONFIG_LOCAL = 0x010000,
  ULOG_MEMORY_CONFIG_SHARED = 0x020000,
  ULOG_MEMORY_CONFIG_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_CONFIG_TYPE;
typedef uint64(*ULOG_ALT_TS_SRC)(void);
typedef enum
{
  ULOG_INTERFACE_INVALID = 0,
  ULOG_INTERFACE_RAW,
  ULOG_INTERFACE_REALTIME,
  ULOG_INTERFACE_UINT32 = 0x7FFFFFFF
} ULOG_INTERFACE_TYPE;
typedef enum
{
  ULOG_REALTIME_SUBTYPE_RESERVED_FOR_RAW = 0,
  ULOG_REALTIME_SUBTYPE_PRINTF,
  ULOG_REALTIME_SUBTYPE_BYTEDATA,
  ULOG_REALTIME_SUBTYPE_STRINGDATA,
  ULOG_REALTIME_SUBTYPE_WORDDATA,
  ULOG_REALTIME_SUBTYPE_CSVDATA,
  ULOG_REALTIME_SUBTYPE_VECTOR,
  ULOG_REALTIME_SUBTYPE_MULTIPART,
  ULOG_REALTIME_SUBTYPE_MULTIPART_STREAM_END,
  ULOG_SUBTYPE_REALTIME_TOKENIZED_STRING,
  ULOG_SUBTYPE_RESERVED1,
  ULOG_SUBTYPE_RESERVED2,
  ULOG_SUBTYPE_RESERVED3,
  ULOG_SUBTYPE_RAW8,
  ULOG_SUBTYPE_RAW16,
  ULOG_SUBTYPE_RAW32,
  ULOG_REALTIME_SUBTYPE_UINT32 = 0x7FFFFFFF
} ULOG_REALTIME_SUBTYPES;
typedef enum
{
  ULOG_LOG_WRAPPED = 0x0001,
  ULOG_ERR_LARGEMSG = 0x0002,
  ULOG_ERR_LARGEMSGOUT = 0x0004,
  ULOG_ERR_RESET = 0x0008,
  ULOG_ERR_UINT32 = 0x7FFFFFFF
} ULOG_WRITER_STATUS_TYPE;
typedef enum
{
  ULOG_RD_FLAG_REWIND = 0x0001,
  ULOG_RD_FLAG_UINT32 = 0x7FFFFFFF
} ULOG_RD_FLAG_TYPE;
typedef enum
{
  ULOG_ATTRIBUTE_READ_AUTOREWIND = 0x1,
  ULOG_ATTRIBUTE_UINT32 = 0x7FFFFFFF
} ULOG_ATTRIBUTE_TYPE;
typedef struct
{
  ULogResult (*write) (ULogHandle h, uint32 firstMsgCount, const char * firstMsgContent, uint32 secondMsgCount, const char * secondMsgContent, ULOG_INTERFACE_TYPE interfaceType);
  DALBOOL (*multipartMsgBegin) (ULogHandle h);
  void (*multipartMsgEnd) (ULogHandle h);
} ULOG_CORE_VTABLE;
ULogResult ULogCore_Init(void);
ULogResult ULogCore_LogCreate(ULogHandle * h,
                              const char * logName,
                              uint32 bufferSize,
                              uint32 memoryType,
                              ULOG_LOCK_TYPE lockType,
                              ULOG_INTERFACE_TYPE interfaceType);
void ULogCore_SetLockType( ULogHandle h, ULOG_LOCK_TYPE lockType );
ULogResult ULogCore_HeaderSet(ULogHandle h, char * headerText);
void ULogCore_AttributeSet(ULogHandle h, uint32 attribute);
ULogResult ULogCore_MemoryAssign(ULogHandle h,
                                 ULOG_VALUE_ID id,
                                 void * bufferPtr,
                                 uint32 bufferSize);
ULogResult ULogCore_Enable(ULogHandle h);
DALBOOL ULogCore_IsEnabled(ULogHandle h, ULOG_CORE_VTABLE ** core);
ULogResult ULogCore_Query(ULogHandle h, ULOG_VALUE_ID id, uint32 * value);
ULogResult ULogCore_Read(ULogHandle h,
                         uint32 outputSize,
                         char * outputMem,
                         uint32 * outputCount);
ULogResult ULogCore_ReadEx(ULogHandle h,
                           uint32 outputSize,
                           char * outputMem,
                           uint32 * outputCount,
                           uint32 outputMessageLimit);
ULogResult ULogCore_ReadSessionComplete(ULogHandle h);
ULogResult ULogCore_Allocate(ULogHandle h, uint32 bufferSize);
ULogResult ULogCore_Disable(ULogHandle h);
ULogResult ULogCore_List(uint32 * registeredCount,
                         uint32 nameArraySize,
                         uint32 * namesReadCount,
                         char * nameArray);
ULogResult ULogCore_ListEx(uint32 offsetCount,
                           uint32 * registeredCount,
                           uint32 nameArraySize,
                           uint32 * namesReadCount,
                           char * nameArray);
ULogResult ULogCore_MsgFormat(ULogHandle h,
                              char * msg,
                              char * msgString,
                              uint32 msgStringSize,
                              uint32 * msgConsumed);
ULogResult ULogCore_HeaderRead(ULogHandle h,
                               uint32 headerReadOffset,
                               char * headerString,
                               uint32 headerStringSize,
                               uint32 * headerActualLength);
ULogResult ULogCore_Connect(ULogHandle * h, const char * logName);
void ULogCore_LogLevelSet(ULogHandle h, uint32 level);
uint32 ULogCore_LogLevelGet(ULogHandle h);
ULogHandle ULogCore_TeeAdd(ULogHandle h1, ULogHandle h2);
ULogHandle ULogCore_TeeRemove(ULogHandle h1, ULogHandle h2);
ULogHandle ULogCore_HandleGet(char *logName);
DALBOOL ULogCore_IsLogPresent(ULogHandle h, char *logName);
ULogResult ULogCore_SetTimestampSrcFn(ULogHandle h, ULOG_ALT_TS_SRC altULogTimeStampFn);
ULogResult ULogCore_SetTransportToRAM(ULogHandle h);
ULogResult ULogCore_SetTransportToStm(ULogHandle h, unsigned char protocol_num);
ULogResult ULogCore_SetTransportToStmAscii(ULogHandle h, unsigned char protocol_num);
ULogResult ULogCore_SetTransportToAlt(ULogHandle h, ULOG_CORE_VTABLE* newTansportVTable);
ULogResult ULogDiagAddPlugin(int(*new_pluginfcn)(uint32), uint32 new_plugin_id);
ULogResult ULogDiagAddPluginExt
(
  int(*new_pluginfcnext)(void*, unsigned long int),
  uint32 new_plugin_id
);
ULogResult ULogFront_RawInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RawLog(ULogHandle h,
                            const char * dataArray,
                            uint32 dataCount);
ULogResult ULogFront_RealTimeInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RealTimeStaticInit( ULogHandle * h,
                                         const char * name,
                                         uint32 logMessageSlots,
                                         uint32 logBufMemType,
                                         ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RealTimePrintf(ULogHandle h,
                                    uint32 dataCount,
                                    const char * formatStr,
                                    ...);
ULogResult ULogFront_RealTimeStaticPrintf(ULogHandle h, uint32 msgSlot, uint32 dataCount, const char * formatStr, ...);
ULogResult ULogFront_RealTimeVprintf(ULogHandle h,
                                     uint32 dataCount,
                                     const char * formatStr,
                                     va_list ap);
ULogResult ULogFront_RealTimeData(ULogHandle h, uint32 dataCount, ...);
ULogResult ULogFront_RealTimeCharArray(ULogHandle h,
                                       uint32 byteCount,
                                       char * byteData);
ULogResult ULogFront_RealTimeString(ULogHandle h, char * cStr);
ULogResult ULogFront_RealTimeWordArray(ULogHandle h,
                                       uint32 wordCount,
                                       const uint32 * wordData);
ULogResult ULogFront_RealTimeCsv(ULogHandle h,
                                 uint32 wordCount,
                                 const uint32 * wordData);
ULogResult ULogFront_RealTimeVector(ULogHandle h,
                                    const char * formatStr,
                                    unsigned short entryByteCount,
                                    unsigned short vectorLength,
                                    const void * vector);
DALBOOL ULogFront_RealTimeMultipartMsgBegin (ULogHandle h);
void ULogFront_RealTimeMultipartMsgEnd (ULogHandle h);
typedef void * addr_t;
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
typedef unsigned long qword[ 2 ];
  typedef unsigned long qc_qword[ 2 ];
void qw_set
(
  qc_qword qw,
  uint32 hi,
  uint32 lo
);
void qw_equ
(
  qc_qword qw1,
  qc_qword qw2
);
uint32 qw_hi
(
  qc_qword qw
);
uint32 qw_lo
(
  qc_qword qw
);
void qw_inc
(
  qc_qword qw1,
  uint32 val
);
void qw_dec
(
  qc_qword qw1,
  uint32 val
);
void qw_add
(
  qc_qword sum,
  qc_qword addend,
  qc_qword adder
);
void qw_sub
(
  qc_qword difference,
  qc_qword subtrahend,
  qc_qword subtractor
);
void qw_mul
(
  qc_qword product,
  qc_qword multiplicand,
  uint32 multiplier
);
word qw_div
(
  qc_qword quotient,
  qc_qword dividend,
  word divisor
);
word qw_div_by_power_of_2
(
  qc_qword quotient,
  qc_qword dividend,
  unsigned short num_bits
);
void qw_shift
(
  qc_qword shifticand,
  int shiftidend
);
int qw_cmp
(
  qc_qword qw1,
  qc_qword qw2
);
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
}
msg_get_ssid_ranges_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;
  uint32 range_cnt;
  struct
  {
    uint16 ssid_first;
    uint16 ssid_last;
  }
  ssid_ranges[1];
}
msg_get_ssid_ranges_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
}
msg_get_mask_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint8 status;
  uint8 pad;
  uint32 bld_mask[1];
}
msg_get_mask_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint16 pad;
  uint32 rt_mask[1];
}
msg_set_rt_mask_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint8 status;
  uint8 pad;
  uint32 rt_mask[1];
}
msg_set_rt_mask_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_rsp_type;
typedef struct
{
  uint16 line;
  uint16 ss_id;
  uint32 ss_mask;
}
msg_desc_type;
typedef struct
{
  uint8 cmd_code;
  uint8 ts_type;
  uint8 num_args;
  uint8 drop_cnt;
  qword ts;
}
msg_hdr_type;
typedef enum
{
  MSG_TS_TYPE_CDMA_FULL = 0,
  MSG_TS_TYPE_WIN32,
  MSG_TS_TYPE_GW
}
msg_ts_type;
typedef struct
{
  msg_hdr_type hdr;
  msg_desc_type desc;
  uint32 args[1];
}
msg_ext_type;
typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word msg_level;
}
msg_legacy_req_type;
typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word qty;
  dword drop_cnt;
  dword total_msgs;
  byte level;
  char file[(12+1)];
  word line;
  char fmt[40];
  dword code1;
  dword code2;
  dword code3;
  qword time;
}
msg_legacy_rsp_type;
typedef struct
{
  msg_desc_type desc;
  uint32 msg_hash;
}
msg_qsr_const_type;
typedef struct
{
  msg_qsr_const_type qsr_const_blk;
  const char *fname;
} err_msg_qsr_const_type;
typedef struct
{
  msg_hdr_type hdr;
  const msg_qsr_const_type* qsr_const_data_ptr;
  uint32 qsr_flag;
  uint32 args[1];
}
msg_qsr_store_type;
typedef struct
{
  msg_desc_type desc;
  const char * msg;
}
msg_v2_const_type;
typedef struct
{
  msg_v2_const_type msg_v2_const_blk;
  const char *fname;
} err_msg_v2_const_type;
  void qsr_msg_send ( const msg_qsr_const_type * xx_msg_const_ptr);
  void msg_v2_send ( const msg_v2_const_type * xx_msg_const_ptr);
  void qsr_msg_send_1 (const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1);
  void msg_v2_send_1 (const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1);
  void qsr_msg_send_2 ( const msg_qsr_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
  void msg_v2_send_2 ( const msg_v2_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
  void qsr_msg_send_3 ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
  void msg_v2_send_3 ( const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
  void qsr_msg_send_var ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 num_args, ...);
  void msg_v2_send_var ( const msg_v2_const_type * xx_msg_const_ptr, uint32 num_args, ...);
void msg_qsrerrlog_3 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_qsrerrlog_2 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2);
void msg_qsrerrlog_1 (const err_msg_qsr_const_type* const_blk, uint32 code1);
void msg_qsrerrlog_0 (const err_msg_qsr_const_type* const_blk);
void msg_v2_errlog_3 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_v2_errlog_2 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2);
void msg_v2_errlog_1 (const err_msg_v2_const_type* const_blk, uint32 code1);
void msg_v2_errlog_0 (const err_msg_v2_const_type* const_blk);
typedef struct
{
  msg_desc_type desc;
  const char* fmt;
  const char* fname;
}
msg_const_type;
typedef struct
{
  msg_hdr_type hdr;
  const msg_const_type* const_data_ptr;
  uint32 args[1];
}
msg_ext_store_type;
void msg_init(void);
void msg_send(const msg_const_type* xx_msg_const_ptr);
void msg_send_1(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1);
void msg_send_2(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2);
void msg_send_3(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2, uint32 xx_arg3);
void msg_send_var(const msg_const_type* xx_msg_const_ptr, uint32 num_args,
                  ...);
void msg_sprintf(const msg_const_type* const_blk, ...);
void msg_send_ts(const msg_const_type* const_blk, uint64 timestamp);
void msg_save_3(const msg_const_type* const_blk,
                uint32 xx_arg1, uint32 xx_arg2, uint32 xx_arg3,
                msg_ext_store_type* msg);
void msg_errlog_3(const msg_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_errlog_2(const msg_const_type* const_blk, uint32 code1, uint32 code2);
void msg_errlog_1(const msg_const_type* const_blk, uint32 code1);
void msg_errlog_0(const msg_const_type* const_blk);
boolean msg_status(uint16 ss_id, uint32 ss_mask);
typedef uint8 SlimBusLogicalAddrType;
typedef uint32 SlimBusResourceHandle;
typedef enum
{
  SLIMBUS_PORT_REQ_O_DEFAULT = 0,
  SLIMBUS_PORT_REQ_O_HALF_DUPLEX = 1,
  SLIMBUS_PORT_REQ_O_MULTI_CHAN_GROUP = 2,
  _PLACEHOLDER_SlimBusPortReqType = 0x7fffffff
} SlimBusPortReqType;
typedef enum
{
  SLIMBUS_PORT_O_NONE = 0,
  SLIMBUS_PORT_O_PACKED = 1,
  SLIMBUS_PORT_O_ALIGN_MSB = 2,
  _PLACEHOLDER_SlimBusPortOptionsType = 0x7fffffff
} SlimBusPortOptionsType;
typedef struct
{
  uint32 uWaterMark;
  SlimBusPortOptionsType eOptions;
  uint32 uBlockSize;
  uint32 uTransSize;
  DALBOOL bUpperWaterMarkValid;
  uint32 uUpperWaterMark;
} SlimBusPortConfigType;
typedef struct
{
   void *pBase;
   uint32 uPhysBase;
   uint32 uSize;
   uint32 uMinSize;
} SlimBusMemBufferType;
typedef enum
{
   SLIMBUS_BAM_O_DESC_DONE = 0x00000001,
   SLIMBUS_BAM_O_INACTIVE = 0x00000002,
   SLIMBUS_BAM_O_WAKEUP = 0x00000004,
   SLIMBUS_BAM_O_OUT_OF_DESC = 0x00000008,
   SLIMBUS_BAM_O_ERROR = 0x00000010,
   SLIMBUS_BAM_O_EOT = 0x00000020,
  _PLACEHOLDER_SlimBusBamOptionType = 0x7fffffff
} SlimBusBamOptionType;
typedef struct
{
  void *eBamDev;
  uint32 uPipeIndex;
  SlimBusBamOptionType eOptions;
  SlimBusMemBufferType DescFifo;
  SlimBusMemBufferType Data;
} SlimBusPipeConfigType;
typedef enum
{
   SLIMBUS_BAM_TRIGGER_CALLBACK = 0,
   SLIMBUS_BAM_TRIGGER_WAIT,
  _PLACEHOLDER_SlimBusBamTriggerType = 0x7fffffff
} SlimBusBamTriggerType;
typedef struct
{
   SlimBusBamOptionType eOptions;
   DALSYSEventHandle hEvent;
   void *pUser;
} SlimBusBamRegisterEventType;
typedef struct
{
  uint32 uAddr;
  uint32 uSize : 16;
  uint32 uFlags : 16;
} SlimBusBamIOVecType;
typedef enum
{
   SLIMBUS_BAM_EVENT_INVALID = 0,
   SLIMBUS_BAM_EVENT_EOT,
   SLIMBUS_BAM_EVENT_DESC_DONE,
   SLIMBUS_BAM_EVENT_OUT_OF_DESC,
   SLIMBUS_BAM_EVENT_WAKEUP,
   SLIMBUS_BAM_EVENT_FLOWOFF,
   SLIMBUS_BAM_EVENT_INACTIVE,
   SLIMBUS_BAM_EVENT_ERROR,
   SLIMBUS_BAM_EVENT_MAX,
  _PLACEHOLDER_SlimBusBamEventType = 0x7fffffff
} SlimBusBamEventType;
typedef enum
{
  SLIMBUS_RATE_FAM_4_KHZ,
  SLIMBUS_RATE_FAM_11p025_KHZ,
  SLIMBUS_RATE_FAM_1_HZ,
  SLIMBUS_RATE_FAM_NUM_ENUMS,
  _PLACEHOLDER_SlimBusRateFamilyType = 0x7fffffff
} SlimBusRateFamilyType;
typedef enum
{
  SLIMBUS_PROTO_HARD_ISO,
  SLIMBUS_PROTO_AUTO_ISO,
  SLIMBUS_PROTO_PUSHED,
  SLIMBUS_PROTO_PULLED,
  SLIMBUS_PROTO_ASYNC_SIMPLEX,
  SLIMBUS_PROTO_ASYNC_HALF_DUPLEX,
  SLIMBUS_PROTO_EXT_ASYNC_SIMPLEX,
  SLIMBUS_PROTO_EXT_ASYNC_HALF_DUPLEX,
  SLIMBUS_PROTO_NUM_ENUMS,
  _PLACEHOLDER_SlimBusProtocolType = 0x7fffffff
} SlimBusProtocolType;
typedef enum
{
  SLIMBUS_CHAN_O_NONE = 0,
  SLIMBUS_CHAN_O_AUTO_ACTIVATE = 1,
  SLIMBUS_CHAN_O_PRESERVE_ORDERING = 2,
  _PLACEHOLDER_SlimBusChannelOptionsType = 0x7fffffff
} SlimBusChannelOptionsType;
typedef enum
{
  SLIMBUS_DATA_FORMAT_NOT_INDICATED,
  SLIMBUS_DATA_FORMAT_LPCM_AUDIO,
  SLIMBUS_DATA_FORMAT_IEC61937_COMP_AUDIO,
  SLIMBUS_DATA_FORMAT_PACKED_PDM_AUDIO,
  SLIMBUS_DATA_FORMAT_NUM_ENUMS,
  _PLACEHOLDER_SlimBusDataFormatType = 0x7fffffff
} SlimBusDataFormatType;
typedef enum
{
  SLIMBUS_SIDEBAND_FORMAT_NOT_APPLICABLE,
  SLIMBUS_SIDEBAND_FORMAT_IEC60958_TUNNELING,
  SLIMBUS_SIDEBAND_FORMAT_NUM_ENUMS,
  _PLACEHOLDER_SlimBusSidebandFormatType = 0x7fffffff
} SlimBusSideBandFormatType;
typedef enum
{
  SLIMBUS_PORT_SOURCE_FLOW = 0,
  SLIMBUS_PORT_SINK_FLOW,
  SLIMBUS_PORT_NUM_ENUMS,
  _PLACEHOLDER_SlimBusPortFlowType = 0x7fffffff
} SlimBusPortFlowType;
typedef enum
{
  SLIMBUS_CHAN_CTRL_ACTIVATE,
  SLIMBUS_CHAN_CTRL_DEACTIVATE,
  SLIMBUS_CHAN_CTRL_REMOVE,
  SLIMBUS_CHAN_NUM_ENUMS,
  _PLACEHOLDER_SlimBusChannelCtrlType = 0x7fffffff
} SlimBusChannelCtrlType;
typedef struct
{
  uint32 uSampleSize_bits;
  SlimBusRateFamilyType eBaseSampleRate;
  uint32 uRateMultiplier;
  SlimBusProtocolType eDesiredProtocol;
  SlimBusChannelOptionsType eOptions;
  SlimBusDataFormatType eDataFormat;
  SlimBusSideBandFormatType eSidebandFormat;
} SlimBusChannelReqType;
typedef enum
{
  SLIMBUS_BAM_TRANSMIT,
  SLIMBUS_BAM_RECEIVE,
  SLIMBUS_BAM_DEFAULT,
  SLIMBUS_BAM_NUM_ENUMS,
  _PLACEHOLDER_SlimBusBamTransferType = 0x7fffffff
} SlimBusBamTransferType;
typedef enum
{
  SLIMBUS_EVENT_O_NONE = 0,
  SLIMBUS_EVENT_O_FIFO_OVERRUN = 0x2,
  SLIMBUS_EVENT_O_FIFO_RECEIVE_OVERRUN = SLIMBUS_EVENT_O_FIFO_OVERRUN,
  SLIMBUS_EVENT_O_FIFO_TRANSMIT_OVERRUN = SLIMBUS_EVENT_O_FIFO_OVERRUN,
  SLIMBUS_EVENT_O_FIFO_UNDERRUN = 0x4,
  SLIMBUS_EVENT_O_FIFO_TRANSMIT_UNDERRUN = SLIMBUS_EVENT_O_FIFO_UNDERRUN,
  SLIMBUS_EVENT_O_FIFO_RECEIVE_UNDERRUN = SLIMBUS_EVENT_O_FIFO_UNDERRUN,
  SLIMBUS_EVENT_O_PORT_DISCONNECT = 0x10,
  _PLACEHOLDER_SlimBusPortEventType = 0x7fffffff
} SlimBusPortEventType;
typedef struct
{
  void *pUser;
  SlimBusPortEventType eEvent;
  SlimBusResourceHandle hPort;
} SlimBusEventNotifyType;
typedef enum
{
  SLIMBUS_CHANNEL_STATUS_REMOVED,
  SLIMBUS_CHANNEL_STATUS_INACTIVE,
  SLIMBUS_CHANNEL_STATUS_ACTIVE,
  _PLACEHOLDER_SlimBusDataChannelStatusType = 0x7fffffff
} SlimBusDataChannelStatusType;
typedef enum
{
  SLIMBUS_COUNTER_FREE_RUN,
  SLIMBUS_COUNTER_VFR,
  SLIMBUS_COUNTER_SHADOW,
  _PLACEHOLDER_SlimBusCounterType = 0x7fffffff
} SlimBusCounterType;
typedef enum
{
  SLIMBUS_ERR_NONE = 0,
  SLIMBUS_ERR_MSG_NACKED,
  SLIMBUS_ERR_MSG_RESP_TIMEOUT,
  SLIMBUS_ERR_DATA_CHANNEL_REJECTED,
  _PLACEHOLDER_SlimBusErrorType = 0x7fffffff
} SlimBusErrorType;
typedef enum
{
  SLIMBUS_HW_CAP_COMBINED_PORT_DMA_INT = 0x1,
  SLIMBUS_HW_CAP_TIMESTAMP_SHADOW_COUNTER = 0x2,
  SLIMBUS_HW_CAP_PORT_UPPER_WATERMARK = 0x4,
  _PLACEHOLDER_SlimBusHwCapType = 0x7fffffff
} SlimBusHwCapType;
typedef struct DalSlimBus DalSlimBus;
struct DalSlimBus
{
   struct DalDevice DalDevice;
   DALResult (*GetDeviceLogicalAddr)(DalDeviceHandle * _h, const uint8 *pEA, uint32 uEASize, SlimBusLogicalAddrType * pLA);
   DALResult (*RequestRootFreq)(DalDeviceHandle * _h, uint32 uFreqKhz);
   DALResult (*GetLastError)(DalDeviceHandle * _h, SlimBusErrorType * peError);
   DALResult (*AllocMasterPorts)(DalDeviceHandle * _h, SlimBusPortReqType eReqs, uint32 uMinFifoBytes, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize);
   DALResult (*DeallocMasterPorts)(DalDeviceHandle * _h, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize);
   DALResult (*GetSlavePortHandle)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uPortNum, SlimBusResourceHandle * phPort);
   DALResult (*ConfigMasterPort)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, const SlimBusPortConfigType * pConfig);
   DALResult (*ConfigMasterPipe)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusPortFlowType eFlow, const SlimBusPipeConfigType * pConfig);
   DALResult (*NextReserveMsgBandwidth)(DalDeviceHandle * _h, uint32 uBandwidth_bps);
   DALResult (*ReadValueElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent);
   DALResult (*WriteValueElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucWriteData, uint32 uWriteDataLen, DALSYSEventHandle hEvent);
   DALResult (*ExchangeValueElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucWriteData, uint32 uWriteDataLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent);
   DALResult (*ReadInfoElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent);
   DALResult (*ClearInfoElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucClearMask, uint32 uClearMaskLen, DALSYSEventHandle hEvent);
   DALResult (*ReadAndClearInfoElement)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucClearMask, uint32 uClearMaskLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent);
   DALResult (*AllocDataChannel)(DalDeviceHandle * _h, SlimBusResourceHandle * phChannel);
   DALResult (*NextDefineDataChannel)(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, const SlimBusChannelReqType * pChannelReq);
   DALResult (*ConnectPortToChannel)(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusPortFlowType eFlow, SlimBusResourceHandle hPort);
   DALResult (*DisconnectPortFromChannel)(DalDeviceHandle * _h, SlimBusResourceHandle hPort);
   DALResult (*NextDataChannelControl)(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusChannelCtrlType eCtrl);
   DALResult (*DoReconfigureNow)(DalDeviceHandle * _h);
   DALResult (*GetDataChannelStatus)(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusDataChannelStatusType * peStatus);
   DALResult (*DeallocDataChannel)(DalDeviceHandle * _h, SlimBusResourceHandle hChannel);
   DALResult (*RegisterBamEvent)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamRegisterEventType * pReg);
   DALResult (*SubmitBamTransfer)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void * pUser);
   DALResult (*GetBamIOVec)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec);
   DALResult (*GetBamEvent)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamEventType * peEvent);
   DALResult (*GetPortFifoStatus)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, uint32 * puFifoWordCnt, DALBOOL * pbWatermarkIsHit);
   DALResult (*RegisterPortEventEx)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, DALSYSEventHandle hEvent, void * pUser, SlimBusPortEventType eType);
   DALResult (*GetPortEvent)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusEventNotifyType * pNotify);
   DALResult (*AllocProgressCounter)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusResourceHandle * phCounter);
   DALResult (*DeallocProgressCounter)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter);
   DALResult (*InitProgressCounter)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, uint32 uCount);
   DALResult (*ReadProgressCounter)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, SlimBusCounterType eType, uint32 * puNumDMA, uint32 * puFifoSamples);
   DALResult (*ReadProgressCounterVFRStatus)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, DALBOOL * pbSet);
   DALResult (*ClearProgressCounterVFRStatus)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter);
   DALResult (*AllocResourceGroup)(DalDeviceHandle * _h, SlimBusResourceHandle * hGroup);
   DALResult (*DeallocResourceHroup)(DalDeviceHandle * _h, SlimBusResourceHandle hGroup);
   DALResult (*AddResourceToGroup)(DalDeviceHandle * _h, SlimBusResourceHandle hGroup, uint32 hHandle);
   DALResult (*AllocSharedDataChannel)(DalDeviceHandle * _h, uint32 uChannelNum, SlimBusResourceHandle * phChannel);
   DALResult (*GetBamIOVecEx)(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void **ppUser);
   DALResult (*GetHardwareCapability)(DalDeviceHandle * _h, SlimBusHwCapType *peHwCap);
   DALResult (*ReadProgressCounterTimestamp)(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, uint32 *puNumDMA, uint32 *puFifoSamples, uint64 *puTimestamp, DALBOOL *pbSampleMissed);
   DALResult (*SendUserDefinedMessage)(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 MC, const uint8* pucPayload, uint32 uPayloadLen, DALSYSEventHandle hEvent);
};
typedef struct DalSlimBusHandle DalSlimBusHandle;
struct DalSlimBusHandle
{
   uint32 dwDalHandleId;
   const DalSlimBus * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
DAL_SlimBusDeviceAttach(DALDEVICEID DevId,DalDeviceHandle **phDalDevice)
{
  return DAL_DeviceAttachEx(0,DevId,(((1&0xFFFF)<<16)|(0&0xFFFF)),phDalDevice);
}
static __inline DALResult
DAL_SlimBusDeviceDetach(DalDeviceHandle *_h)
{
  return DAL_DeviceDetach(_h);
}
static __inline DALResult
DalSlimBus_GetDeviceLogicalAddr(DalDeviceHandle * _h, const uint8 *pEA, uint32 uEASize, SlimBusLogicalAddrType * pLA)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetDeviceLogicalAddr( _h, pEA, uEASize, pLA);
}
static __inline DALResult
DalSlimBus_RequestRootFreq(DalDeviceHandle * _h, uint32 uFreqKhz)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->RequestRootFreq( _h, uFreqKhz);
}
static __inline DALResult
DalSlimBus_GetLastError(DalDeviceHandle * _h, SlimBusErrorType * peError)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetLastError( _h, peError);
}
static __inline DALResult
DalSlimBus_GetHardwareCapability(DalDeviceHandle * _h, SlimBusHwCapType *peHwCap)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetHardwareCapability(_h, peHwCap);
}
static __inline DALResult
DalSlimBus_AllocMasterPorts(DalDeviceHandle * _h, SlimBusPortReqType eReqs, uint32 uMinFifoBytes, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AllocMasterPorts( _h, eReqs, uMinFifoBytes, phPortBuf, uPortBufSize);
}
static __inline DALResult
DalSlimBus_DeallocMasterPorts(DalDeviceHandle * _h, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DeallocMasterPorts( _h, phPortBuf, uPortBufSize);
}
static __inline DALResult
DalSlimBus_GetSlavePortHandle(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uPortNum, SlimBusResourceHandle * phPort)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetSlavePortHandle( _h, LA, uPortNum, phPort);
}
static __inline DALResult
DalSlimBus_ConfigMasterPort(DalDeviceHandle * _h, SlimBusResourceHandle hPort, const SlimBusPortConfigType * pConfig)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ConfigMasterPort( _h, hPort, pConfig);
}
static __inline DALResult
DalSlimBus_ConfigMasterPipe(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusPortFlowType eFlow, const SlimBusPipeConfigType * pConfig)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ConfigMasterPipe( _h, hPort, eFlow, pConfig);
}
static __inline DALResult
DalSlimBus_ReadValueElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadValueElement( _h, LA, uByteAddr, pucReadData, uReadDataLen, puActualReadDataLen, hEvent);
}
static __inline DALResult
DalSlimBus_WriteValueElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucWriteData, uint32 uWriteDataLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->WriteValueElement( _h, LA, uByteAddr, uByteSize, pucWriteData, uWriteDataLen, hEvent);
}
static __inline DALResult
DalSlimBus_ExchangeValueElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucWriteData, uint32 uWriteDataLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ExchangeValueElement( _h, LA, uByteAddr, pucWriteData, uWriteDataLen, pucReadData, uReadDataLen, puActualReadDataLen, hEvent);
}
static __inline DALResult
DalSlimBus_ReadInfoElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadInfoElement( _h, LA, uByteAddr, pucReadData, uReadDataLen, puActualReadDataLen, hEvent);
}
static __inline DALResult
DalSlimBus_ClearInfoElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucClearMask, uint32 uClearMaskLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ClearInfoElement( _h, LA, uByteAddr, uByteSize, pucClearMask, uClearMaskLen, hEvent);
}
static __inline DALResult
DalSlimBus_ReadAndClearInfoElement(DalDeviceHandle * _h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucClearMask, uint32 uClearMaskLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadAndClearInfoElement( _h, LA, uByteAddr, pucClearMask, uClearMaskLen, pucReadData, uReadDataLen, puActualReadDataLen, hEvent);
}
static __inline DALResult
DalSlimBus_SendUserDefinedMessage(DalDeviceHandle *_h, SlimBusLogicalAddrType LA, uint32 MC, const uint8* pucPayload, uint32 uPayloadLen, DALSYSEventHandle hEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->SendUserDefinedMessage( _h, LA, MC, pucPayload, uPayloadLen, hEvent);
}
static __inline DALResult
DalSlimBus_AllocDataChannel(DalDeviceHandle * _h, SlimBusResourceHandle * phChannel)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AllocDataChannel( _h, phChannel);
}
static __inline DALResult
DalSlimBus_AllocSharedDataChannel(DalDeviceHandle * _h, uint32 uChannelNum, SlimBusResourceHandle * phChannel)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AllocSharedDataChannel( _h, uChannelNum, phChannel);
}
static __inline DALResult
DalSlimBus_NextDefineDataChannel(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, const SlimBusChannelReqType * pChannelReq)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->NextDefineDataChannel( _h, hChannel, pChannelReq);
}
static __inline DALResult
DalSlimBus_ConnectPortToChannel(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusPortFlowType eFlow, SlimBusResourceHandle hPort)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ConnectPortToChannel( _h, hChannel, eFlow, hPort);
}
static __inline DALResult
DalSlimBus_DisconnectPortFromChannel(DalDeviceHandle * _h, SlimBusResourceHandle hPort)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DisconnectPortFromChannel( _h, hPort);
}
static __inline DALResult
DalSlimBus_NextDataChannelControl(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusChannelCtrlType eCtrl)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->NextDataChannelControl( _h, hChannel, eCtrl);
}
static __inline DALResult
DalSlimBus_NextReserveMsgBandwidth(DalDeviceHandle * _h, uint32 uBandwidth_bps)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->NextReserveMsgBandwidth( _h, uBandwidth_bps);
}
static __inline DALResult
DalSlimBus_DoReconfigureNow(DalDeviceHandle * _h)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DoReconfigureNow( _h);
}
static __inline DALResult
DalSlimBus_GetDataChannelStatus(DalDeviceHandle * _h, SlimBusResourceHandle hChannel, SlimBusDataChannelStatusType * peStatus)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetDataChannelStatus( _h, hChannel, peStatus);
}
static __inline DALResult
DalSlimBus_DeallocDataChannel(DalDeviceHandle * _h, SlimBusResourceHandle hChannel)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DeallocDataChannel( _h, hChannel);
}
static __inline DALResult
DalSlimBus_RegisterBamEvent(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamRegisterEventType * pReg)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->RegisterBamEvent( _h, hPort, eTransferDir, pReg);
}
static __inline DALResult
DalSlimBus_SubmitBamTransfer(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void * pUser)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->SubmitBamTransfer( _h, hPort, eTransferDir, pIOVec, pUser);
}
static __inline DALResult
DalSlimBus_GetBamIOVec(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetBamIOVec( _h, hPort, eTransferDir, pIOVec);
}
static __inline DALResult
DalSlimBus_GetBamIOVecEx(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void **ppUser)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetBamIOVecEx( _h, hPort, eTransferDir, pIOVec, ppUser);
}
static __inline DALResult
DalSlimBus_GetBamEvent(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamEventType * peEvent)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetBamEvent( _h, hPort, eTransferDir, peEvent);
}
static __inline DALResult
DalSlimBus_GetPortFifoStatus(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, uint32 * puFifoWordCnt, DALBOOL * pbWatermarkIsHit)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetPortFifoStatus( _h, hPort, eTransferDir, puFifoWordCnt, pbWatermarkIsHit);
}
static __inline DALResult
DalSlimBus_RegisterPortEventEx(DalDeviceHandle * _h, SlimBusResourceHandle hPort, DALSYSEventHandle hEvent, void * pUser, SlimBusPortEventType eType)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->RegisterPortEventEx( _h, hPort, hEvent, pUser, eType);
}
static __inline DALResult
DalSlimBus_RegisterPortEvent(DalDeviceHandle * _h, SlimBusResourceHandle hPort, DALSYSEventHandle hEvent, void * pUser)
{
   return DalSlimBus_RegisterPortEventEx( _h, hPort, hEvent, pUser, ((SlimBusPortEventType)(SLIMBUS_EVENT_O_FIFO_RECEIVE_OVERRUN| SLIMBUS_EVENT_O_FIFO_TRANSMIT_UNDERRUN| SLIMBUS_EVENT_O_PORT_DISCONNECT)));
}
static __inline DALResult
DalSlimBus_GetPortEvent(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusEventNotifyType * pNotify)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->GetPortEvent( _h, hPort, pNotify);
}
static __inline DALResult
DalSlimBus_AllocProgressCounter(DalDeviceHandle * _h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusResourceHandle * phCounter)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AllocProgressCounter( _h, hPort, eTransferDir, phCounter);
}
static __inline DALResult
DalSlimBus_DeallocProgressCounter(DalDeviceHandle * _h, SlimBusResourceHandle hCounter)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DeallocProgressCounter( _h, hCounter);
}
static __inline DALResult
DalSlimBus_InitProgressCounter(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, uint32 uCount)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->InitProgressCounter( _h, hCounter, uCount);
}
static __inline DALResult
DalSlimBus_ReadProgressCounter(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, SlimBusCounterType eType, uint32 * puNumDMA, uint32 * puFifoSamples)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadProgressCounter( _h, hCounter, eType, puNumDMA, puFifoSamples);
}
static __inline DALResult
DalSlimBus_ReadProgressCounterTimestamp(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, uint32 *puNumDMA, uint32 *puFifoSamples, uint64 *puTimestamp, DALBOOL *pbSampleMissed)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadProgressCounterTimestamp( _h, hCounter, puNumDMA, puFifoSamples, puTimestamp, pbSampleMissed);
}
static __inline DALResult
DalSlimBus_ReadProgressCounterVFRStatus(DalDeviceHandle * _h, SlimBusResourceHandle hCounter, DALBOOL * pbSet)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ReadProgressCounterVFRStatus( _h, hCounter, pbSet);
}
static __inline DALResult
DalSlimBus_ClearProgressCounterVFRStatus(DalDeviceHandle * _h, SlimBusResourceHandle hCounter)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->ClearProgressCounterVFRStatus( _h, hCounter);
}
static __inline DALResult
DalSlimBus_AllocResourceGroup(DalDeviceHandle * _h, SlimBusResourceHandle * phGroup)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AllocResourceGroup( _h, phGroup);
}
static __inline DALResult
DalSlimBus_DeallocResourceGroup(DalDeviceHandle * _h, SlimBusResourceHandle hGroup)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->DeallocResourceHroup( _h, hGroup);
}
static __inline DALResult
DalSlimBus_AddResourceToGroup(DalDeviceHandle * _h, SlimBusResourceHandle hGroup, SlimBusResourceHandle hResource)
{
   return ((DalSlimBusHandle *)_h)->pVtbl->AddResourceToGroup( _h, hGroup, hResource);
}
extern SBResult SlimBus_ClockOn(struct SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_ClockOff(struct SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_VoltageControl(struct SlimBusDevCtxt *pDevCtxt, uint32 uReqClkGear);
typedef enum
{
  SB_MASTER_DRIVER,
  SB_SATELLITE_DRIVER
} SlimBusDriverType;
typedef enum
{
  SB_RATE_COEFF_1 = 0,
  SB_RATE_COEFF_3 = 1,
} SlimBusRateCoeffType;
typedef enum
{
  SB_HANDLE_TYPE_MASTER_PORT = 1,
  SB_HANDLE_TYPE_SLAVE_PORT,
  SB_HANDLE_TYPE_CHANNEL,
  SB_HANDLE_TYPE_PROGRESS_COUNTER,
  SB_HANDLE_TYPE_GROUP,
  SB_HANDLE_TYPE_SHARED_CHANNEL
} SlimBusHandleType;
typedef enum
{
  SB_HANDLE_UNAVAILABLE,
  SB_HANDLE_AVAILABLE,
  SB_HANDLE_ALLOCATED,
  SB_HANDLE_DEALLOCATED
} SlimBusHandleStateType;
typedef enum
{
  SB_ENUM_INACTIVE,
  SB_ENUM_RESERVED,
  SB_ENUM_PRESENT,
  SB_ENUM_ACTIVE,
} SlimBusEnumStateType;
typedef enum
{
  SB_MSG_FIFO_FLAGS_NONE = 0,
  SB_MSG_FIFO_ENABLE_NOTIFY = 1,
  SB_MSG_FIFO_USE_RX_OVERHANG = 2,
} SlimBusMsgFifoFlagType;
typedef enum
{
  SB_WRKRND_O_NO_PORT_MULTI_CHAN = 0x1,
  SB_WRKRND_O_PC_FIFO_SAMPLE_OFFSET = 0x2,
  SB_WRKRND_O_RX_PORT_RESET = 0x4,
  SB_WRKRND_O_IE_CLEAR = 0x8,
  SB_WRKRND_O_SETUP_TRUST = 0x1000,
} SlimBusWorkaroundType;
typedef enum
{
  SB_PAUSE_NOT_PAUSED,
  SB_PAUSE_ENTERING_PAUSE,
  SB_PAUSE_PLACEHOLDER_2,
  SB_PAUSE_PAUSED,
  SB_PAUSE_PLACEHOLDER_4,
  SB_PAUSE_RESUMING_FROM_PAUSE
} SlimBusPauseStateType;
typedef enum
{
  SB_PORT_NOT_DISCONNECTING,
  SB_PORT_DISCONNECTING,
  SB_PORT_WAITING_TO_DISCONNECT
} SlimBusPortDisconnectStateType;
typedef enum
{
  SB_FRM_HANDOVER_IDLE,
  SB_FRM_HANDOVER_PREPARE,
  SB_FRM_HANDOVER_EXECUTE,
  SB_FRM_HANDOVER_CLEANUP
} SlimBusFramerHandoverStateType;
struct SlimBusChanLAddAssociation
{
  uint32 channelNum;
  uint32 portNum;
  SlimBusPortFlowType eFlow;
  SlimBusChanLAddAssociation *pNext;
};
struct SlimBusSinkNode
{
  SlimBusLogicalAddrType LA;
  uint32 SinkPortNum;
  struct SlimBusSinkNode *next;
};
typedef struct
{
  uint32 uStructVer;
  const char *pszInstName;
  uint8 uaMasterEA[6];
  char *pszHwioBase;
  uint32 uHwioBaseOffset;
  uint32 uHwioBase;
  uint32 hBamDev;
  uint32 uIntId;
  uint32 uBamIntId;
  uint32 uMyEE;
  uint32 uaGpioConfig[3];
  uint32 uGpioIntNum;
  uint32 uCoreVer[4];
} SlimBusBSPType;
typedef struct SlimBusLinkNodeType
{
  struct SlimBusLinkNodeType *pNext;
  struct SlimBusLinkNodeType *pPrev;
} SlimBusLinkNodeType;
typedef struct SlimBusDeviceType SlimBusDeviceType;
typedef struct SlimBusMsgFifoType SlimBusMsgFifoType;
struct SlimBusMsgFifoType
{
  uint32 *pBuf;
  uint32 *pPhysBuf;
  uint32 uLen;
  uint32 uOverhang;
  size_t uHeadIdx;
  size_t uTailIdx;
  size_t uAckIdx;
  SBBOOL bBusy;
  SlimBusMsgFifoFlagType eFlags;
  SlimBusBamCtxt *pBamCtxt;
  void *hDevCtxt;
  uint32* (*getBufFcn)(SlimBusClientCtxt *, SlimBusDeviceType *, SlimBusMsgFifoType *, uint32);
  void (*sendBufFcn)(SlimBusDevCtxt *, SlimBusMsgFifoType *);
};
typedef union
{
  uint32 uCount;
  uint32 *pBuf;
} SlimBusTxMarkerType;
typedef struct SlimBusEventNodeType
{
  SlimBusLinkNodeType Node;
  SlimBusLinkNodeType *pFreeRoot;
  SlimBusEventHandle hEvent;
  SlimBusTxMarkerType TxMarker;
  uint32 uRxMarker;
  uint8 *pBuf;
  uint32 *pBufSize;
  SBBOOL bSyncWait;
  SlimBusErrorType *peMsgErr;
} SlimBusEventNodeType;
typedef struct SlimBusChannelType SlimBusChannelType;
typedef struct SlimBusProgressCounterType SlimBusProgressCounterType;
typedef struct SlimBusGroupType SlimBusGroupType;
typedef struct
{
  SlimBusLinkNodeType LinkNode;
  uint32 portNum;
  SlimBusResourceHandle handle;
  SlimBusHandleStateType eState;
  SlimBusPortReqType eReqs;
  SlimBusPortOptionsType eOptions;
  SlimBusBamCtxt *pBamCtxt;
  SBBOOL bBamConnected;
  SlimBusClientCtxt *pClientCtxt;
  SlimBusChannelType *pChannel;
  SlimBusProgressCounterType *pCounter;
  SlimBusPortFlowType eFlow;
  SlimBusEventHandle hEvent;
  void *pEventUserData;
  SlimBusPortEventType eEnabledEvents;
  SlimBusPortEventType eActiveEvents;
  SBBOOL bSignalDisconnect;
  SBBOOL uSignalDescEmptyCnt;
  SlimBusPortDisconnectStateType eDisconnectState;
  uint32 uDescSubmit;
  uint32 uBytesSubmit;
} SlimBusMasterPortType;
struct SlimBusChannelType
{
  SlimBusLinkNodeType GroupNode;
  SlimBusGroupType *pGroup;
  uint32 channelNum;
  SlimBusResourceHandle handle;
  SlimBusHandleStateType eState;
  SlimBusClientCtxt *pClientCtxt;
  SlimBusDataChannelStatusType eNewStatus;
  SlimBusDataChannelStatusType eCurStatus;
  SlimBusLinkNodeType MasterPortList;
  SlimBusChannelReqType ChannelReq;
};
struct SlimBusProgressCounterType
{
  uint32 counterNum;
  SlimBusResourceHandle handle;
  SlimBusHandleStateType eState;
  SlimBusClientCtxt *pClientCtxt;
  SlimBusMasterPortType *pPort;
};
typedef enum
{
  SB_CHANNEL_INACTIVE,
  SB_CHANNEL_NEW,
  SB_CHANNEL_UPDATED,
  SB_CHANNEL_ACTIVE,
  SB_CHANNEL_PENDING_REMOVAL
} SlimBusChannelAllocType;
typedef struct
{
  SlimBusLinkNodeType LinkNode;
  uint32 uDataLineMask;
  uint32 uDataLine;
  uint32 uPrevDataLine;
  SBBOOL bRecentAssignment;
  uint32 channelNum;
  SlimBusChannelAllocType eStatus;
  uint8 ucSegLen;
  uint8 ucProtocol;
  SlimBusRateCoeffType eCoeff;
  uint8 ucRootExp;
  uint32 uCurInterval;
  uint32 uCurOffset;
  uint32 uNewInterval;
  uint32 uNewOffset;
  uint32 uGrpFlag;
  uint32 uCurRefCnt;
  uint32 uNewRefCnt;
  int32 iClkGearDrift;
  int32 iMaxClkGearDrift;
  SBBOOL bIsSrcConnected;
  SlimBusLogicalAddrType SrcLA;
  uint32 SrcPortNum;
  SlimBusSinkNode *pSinkList;
} SlimBusChannelParamType;
struct SlimBusBasicClientCtxt
{
  SlimBusLinkNodeType LinkNode;
  SlimBusMsgFifoType ConfigMsgFifo;
  uint32 uNewReservedMsgSlots;
  uint32 uCurReservedMsgSlots;
  uint32 uConfigTID;
  SlimBusDeviceType *pDevice;
  uint32 uClientNum;
  SlimBusClientCtxt *pClientCtxt;
  SBBOOL bPurgeNeeded;
  uint32 uaChanBitMap[256/32];
};
typedef struct
{
  uint32 numClientSlots;
  SlimBusBasicClientCtxt **paClientCtxt;
  SBBOOL bExpectAnnouce;
  SBBOOL bSendAnnounce;
  uint32 uSatProtoVer;
} SlimBusSatelliteType;
struct SlimBusDeviceType
{
  uint8 aEA[6];
  uint8 DC;
  uint8 DV;
  uint32 uDataLineMask;
  SlimBusLogicalAddrType LA;
  SlimBusEnumStateType eState;
  uint32 uMaxTrans;
  uint32 uAvailTrans;
  SlimBusLinkNodeType TransEventQueue;
  SlimBusEventNodeType *TIDMap[64];
  uint32 uBaseTID;
  uint32 uNextTIDIdx;
  SlimBusSatelliteType *pSat;
  SlimBusChanLAddAssociation *pConnectedChannels;
};
struct SlimBusGroupType
{
  SlimBusLinkNodeType ResourceList;
  SlimBusResourceHandle handle;
};
typedef struct
{
    SlimBusLinkNodeType ChanParamList3;
    SlimBusLinkNodeType ChanParamList1;
    uint32 uSlotsUsed;
} SlimBusDataLineScheduleType;
typedef struct
{
  SlimBusChannelParamType *apChanParams[256];
  SlimBusDataLineScheduleType aDataLineSchedules[8];
  uint32 uSubFrmCode;
  SBBOOL bDataSlotsScheduled;
  uint32 uMsgSlotReq;
  SBBOOL bReconfigBusy;
  uint32 bEnumPending;
  SBBOOL bMasterWorkerBusy;
  SBBOOL bCheckFrmHandSat;
  SlimBusPauseStateType ePause;
  SBBOOL bEnablePause;
  SBBOOL bPurge;
  uint8 aSlotArr[(6144/4)];
  SlimBusLinkNodeType FreeSchedEventStack;
  SlimBusEventNodeType SchedEventNode;
  SlimBusEventHandle hSchedEvent;
  uint32 uSchedTimerCnt;
  SlimBusFramerHandoverStateType eFrmHandState;
  SlimBusLogicalAddrType FrmHandFrmLA;
  SlimBusLogicalAddrType FrmHandNgdLA;
  SlimBusLogicalAddrType FrmHandFrm2LA;
  SlimBusLogicalAddrType FrmHandNgd2LA;
  SBBOOL bSendPowerNotify;
  uint8 uPowerNotifyTID;
} SlimBusScheduleType;
struct SlimBusDevCtxt
{
  SlimBusBaseDevType Base;
  SlimBusDriverType eDrvType;
  HAL_sb_HandleType hSB;
  HAL_sb_DeviceType eMsgDev;
  SlimBusSyncHandle hSync;
  uint8 prnSeed;
  SlimBusBSPType *pBSP;
  SlimBusWorkaroundType eWrkrnd;
  SBBOOL bIsHwOwner;
  SBBOOL bActiveFramer;
  uint32 uFrmClkGear;
  uint32 uNewClkGear;
  uint32 uDefClkGear;
  uint32 uPowerClkGear;
  uint32 uMinClkGear;
  uint32 uSecDlThresh;
  uint32 numClientSlots;
  SlimBusClientCtxt **paClientCtxt;
  SlimBusMasterPortType Ports[24];
  uint32 numPorts;
  SlimBusChannelType *pChannels[32];
  uint32 numChannelSlots;
  uint32 numSharedChannelSlots;
  SlimBusDeviceType *pDevices[32];
  uint32 numDeviceSlots;
  SlimBusGroupType Groups[16];
  uint32 numGroupSlots;
  SlimBusProgressCounterType Counters[24];
  uint32 numCounterSlots;
  SlimBusSyncHandle hPortSync;
  uint32 uPortDisconnectMask;
  uint32 uPortFinalDisconnectMask;
  uint32 uPortConnectMask;
  uint32 uPortBamResetMask;
  uint32 uPortBamResetDeferMask;
  uint32 uBasePortNum;
  uint32 uBaseChanNum;
  uint32 uBaseSharedChanNum;
  SlimBusLogicalAddrType pgdLA;
  uint32 uRootFreqDiv8;
  uint32 uSuperFreq;
  SlimBusMsgFifoType TxMsgFifo;
  SlimBusMsgFifoType RxMsgFifo;
  SlimBusEventHandle hBamEvent;
  uint32 uTxMsgBusyCnt;
  SlimBusMemHandle hPhysMem;
  SlimBusLinkNodeType TxEventQueue;
  SlimBusLinkNodeType RxEventQueue;
  SlimBusWorkLoopHandle hMsgWorkLoop;
  SlimBusEventHandle hMsgWorkLoopEvent;
  SBBOOL bMsgWorkLoopSignalled;
  SlimBusWorkLoopHandle hMasterWorkLoop;
  SlimBusEventHandle hMasterWorkLoopEvent;
  SlimBusLinkNodeType ReconfigList;
  uint32 uLocalReconfigPendCnt;
  SlimBusLinkNodeType MsgWaitList;
  SlimBusLinkNodeType EnumWaitList;
  SlimBusTimerHandle hTimerHandle;
  SlimBusEventHandle hTimerEvent;
  SBBOOL bTimerActive;
  SlimBusTimerHandle hBamResetTimerHandle;
  SlimBusEventHandle hBamResetTimerEvent;
  SlimBusTimerHandle hEnumTimerHandle;
  SlimBusEventHandle hEnumTimerEvent;
  SlimBusTimerHandle hSFTimerHandle;
  SlimBusEventHandle hSFTimerEvent;
  uint32 uSFTimerCnt;
  uint32 uFrameSyncTimerCnt;
  SlimBusEventHandle hClkToggleTimerEvent;
  SlimBusWorkLoopHandle hTimerWorkLoop;
  SlimBusEventHandle hTimerWorkLoopEvent;
  SlimBusClientCtxt *pBamResetClientCtxt;
  SlimBusEventHandle hInitEvent;
  SBBOOL bNoMalloc;
  SBBOOL bInTimer;
  SBBOOL bInExit;
  SBBOOL bUseRxMsgQueue;
  SBBOOL bUseTxMsgQueue;
  SlimBusScheduleType *pSched;
  SlimBusLinkNodeType PauseEventQueue;
  uint32 uNumTxMsgWaiters;
  SBBOOL bClockIsOn;
  uint32 uSatProtoVer;
  SlimBusClientCtxt *pReconfigWaitClient;
  uint32 uLogLevel; ULogHandle hULog;
  SlimBusDevPlatType Plat;
  SlimBusMemHandle hBamDescMem;
  const uint8 *auEeAssign;
  uint32 uOpenCnt;
  SBBOOL bEverOpened;
  SBBOOL bHwInit;
  SBResult (*connectPortFcn)(SlimBusClientCtxt *, SlimBusDeviceType *, SlimBusPortFlowType, uint32, uint32 );
  SBResult (*disconnectPortFcn)(SlimBusClientCtxt *, SlimBusDevCtxt *, SlimBusDeviceType *, uint32 );
  SBResult (*doReconfigSeqFcn)(SlimBusClientCtxt *);
  SBResult (*checkEnterLowPowerFcn)(SlimBusDevCtxt *);
  SBResult (*leaveLowPowerFcn)(SlimBusDevCtxt *);
  SBBOOL bSSRcbRegistered;
  SBBOOL bPendingSSR;
  SlimBusSyncHandle hExtClkSync;
  SlimBusEventHandle hExtClkEvent;
  SlimBusEventObj ExtClkEventObj;
  SBBOOL bCheckingExtClkToggle;
  SBBOOL bExtClkToggleDetected;
  SBBOOL bUseGpioInt;
  SBBOOL bDeviceOpenTimeout;
  SBBOOL bProgBamTrust;
  SlimBusSyncHandle hIsrSync;
  SlimBusIsrCtxt IsrCtxt;
};
struct SlimBusDrvCtxt
{
  SlimBusBaseDrvType Base;
  SlimBusDevCtxt SlimBusDevCtxt[2];
};
struct SlimBusClientCtxt
{
  uint32 dwRefs;
  uint32 dwAccessMode;
  void *pPortCtxt;
  void *hDevCtxt;
  SlimBusRefHandle SlimBusRefHandle;
  SBBOOL bOpen;
  SBBOOL bOpenb4SSR;
  SlimBusEventNodeType EventNodes[8];
  SlimBusLinkNodeType FreeEventStack;
  SlimBusBasicClientCtxt BasicClient;
  SlimBusChannelType **paSharedChannels;
  SlimBusEventHandle hClientEvent;
  SlimBusEventHandle hTimeoutEvent;
  SlimBusEventObj ClientEventObj;
  SlimBusSyncHandle hSync;
  uint32 uNewReservedMsgSlots;
  uint32 uCurReservedMsgSlots;
  SBBOOL bReConfigPending;
  uint32 uPendingReconfigReqs;
  uint32 uSatReconfigTID;
  SlimBusErrorType eLastMsgErr;
  SlimBusErrorType eLastReconfigErr;
  SBBOOL bForceActive;
};
extern int32 g_iSlimBusLogLevelOverride;
extern SlimBusDeviceType* SlimBus_GetDevicePtr
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusLogicalAddrType LA
);
extern SBResult SlimBus_DeviceInitTarget(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_DeviceEnableInterrupts(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_DeviceDisableInterrupts(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_DeviceInitHardware(SlimBusDevCtxt *pDevCtxt);
extern SlimBusDeviceType* SlimBus_NewDeviceStruct( void );
extern uint32 *SlimBus_GetMsgBuffer
(
  SlimBusClientCtxt *pClientCtxt,
  SlimBusDeviceType *pDevice,
  SlimBusMsgFifoType *pMsgFifo,
  uint32 uNumBytes
);
extern void SlimBus_DefaultSendBuffer
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusMsgFifoType *pMsgFifo
);
extern SBResult SlimBus_SetupMsgQueues(SlimBusDevCtxt *pDevCtxt);
extern SlimBusEventNodeType *SlimBus_EnqueueMsgEvent
(
  SlimBusMsgFifoType *pMsgFifo,
  SlimBusClientCtxt *pClientCtxt,
  uint32 *pBuf,
  SlimBusEventHandle hEvent
);
extern uint32* SlimBus_PeekBuffer
(
  SlimBusMsgFifoType *pMsgFifo,
  uint32 *puNumWords
);
extern SBResult
SlimBus_DeviceInitHardware(SlimBusDevCtxt *pDevCtxt);
static __inline uint32* SlimBus_PopBuffer
(
  SlimBusMsgFifoType *pMsgFifo,
  uint32 *puNumWords
)
{
  uint32 uNumWords = 0;
  uint32 *pBuf = SlimBus_PeekBuffer(pMsgFifo, &uNumWords);
  if ( uNumWords > 0 )
  {
    pMsgFifo->uTailIdx += uNumWords;
  }
  if (0 != puNumWords)
  {
    *puNumWords = uNumWords;
  }
  return pBuf;
}
static __inline void SlimBus_NormalizeIndexMinMax
(
  size_t *puIdx,
  size_t uMinIdx,
  size_t uMaxIdx,
  uint32 uLen
)
{
  if (0 != puIdx)
  {
    size_t uIdx = *puIdx;
    uint32 uLenMask = uLen-1;
    uIdx = (uMinIdx&~uLenMask) | (uIdx&uLenMask);
    if (uIdx < uMinIdx)
    {
      uIdx += uLen;
    }
    if (uIdx > uMaxIdx)
    {
      uIdx -= uLen;
    }
    if (uIdx > uMaxIdx)
    {
      uIdx = uMinIdx;
    }
    *puIdx = uIdx;
  }
}
static __inline uint8 SlimBus_GetNextMasterTID(SlimBusDevCtxt *pDevCtxt)
{
  if (SB_MASTER_DRIVER == pDevCtxt->eDrvType)
  {
    return 0;
  }
  else
  {
    SlimBusDeviceType *pDevice = ((pDevCtxt)->pDevices[0xff&(32 -1)]);
    do {
      pDevice->uNextTIDIdx = (pDevice->uNextTIDIdx + 1) % 64;
    } while (pDevice->TIDMap[pDevice->uNextTIDIdx] != 0);
    return (uint8) (pDevice->uBaseTID + pDevice->uNextTIDIdx);
  }
}
extern void SlimBus_ProcessSignalList
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusLinkNodeType *pSignalList
);
extern SBResult SlimBus_ProcessRxMsg
(
  SlimBusDevCtxt *pDevCtxt,
  uint32 *pDataBuf,
  SlimBusLinkNodeType *pSignalList
);
extern uint32 SlimBus_SubmitRxDescriptors( SlimBusMsgFifoType *pMsgFifo );
extern SBResult
SlimBus_DeviceOpen(SlimBusHandle h, uint32 mode);
extern SBResult
SlimBus_DeviceClose(SlimBusHandle h);
extern uint32 SlimBus_DeviceDetach(SlimBusHandle h);
extern SBResult
SlimBus_GetDeviceLogicalAddr(SlimBusHandle h, const uint8 *pEA, uint32 uEASize, SlimBusLogicalAddrType * pLA);
extern SBResult
SlimBus_RequestRootFreq(SlimBusHandle h, uint32 uFreqKhz);
extern SBResult
SlimBus_GetLastError(SlimBusHandle h, SlimBusErrorType * peError);
extern SBResult
SlimBus_GetHardwareCapability(SlimBusHandle h, SlimBusHwCapType *peHwCap);
extern SBResult
SlimBus_DeallocMasterPorts(SlimBusHandle h, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize);
extern SBResult
SlimBus_AllocMasterPorts(SlimBusHandle h, SlimBusPortReqType eReqs, uint32 uMinFifoBytes, SlimBusResourceHandle * phPortBuf, uint32 uPortBufSize);
extern SBResult
SlimBus_GetSlavePortHandle(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uPortNum, SlimBusResourceHandle * phPort);
extern SBResult
SlimBus_ConfigMasterPort(SlimBusHandle h, SlimBusResourceHandle hPort, const SlimBusPortConfigType * pConfig);
extern SBResult
SlimBus_NextReserveMsgBandwidth(SlimBusHandle h, uint32 uBandwidth_bps);
extern SBResult
SlimBus_ReadValueElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_WriteValueElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucWriteData, uint32 uWriteDataLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_ExchangeValueElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucWriteData, uint32 uWriteDataLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_ReadInfoElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_ClearInfoElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, uint32 uByteSize, const uint8* pucClearMask, uint32 uClearMaskLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_ReadAndClearInfoElement(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 uByteAddr, const uint8* pucClearMask, uint32 uClearMaskLen, uint8* pucReadData, uint32 uReadDataLen, uint32* puActualReadDataLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_SendUserDefinedMessage(SlimBusHandle h, SlimBusLogicalAddrType LA, uint32 MC, const uint8* pucPayload, uint32 uPayloadLen, SlimBusEventHandle hEvent);
extern SBResult
SlimBus_AllocDataChannel(SlimBusHandle h, SlimBusResourceHandle * phChannel);
extern SBResult
SlimBus_AllocSharedDataChannel(SlimBusHandle h, uint32 uChannelNum, SlimBusResourceHandle * phChannel);
extern SBResult
SlimBus_NextDefineDataChannel(SlimBusHandle h, SlimBusResourceHandle hChannel, const SlimBusChannelReqType * pChannelReq);
extern SBResult
SlimBus_ConnectPortToChannel(SlimBusHandle h, SlimBusResourceHandle hChannel, SlimBusPortFlowType eFlow, SlimBusResourceHandle hPort);
extern SBResult
SlimBus_DisconnectPortFromChannel(SlimBusHandle h, SlimBusResourceHandle hPort);
extern SBResult
SlimBus_NextDataChannelControl(SlimBusHandle h, SlimBusResourceHandle hChannel, SlimBusChannelCtrlType eCtrl);
extern SBResult
SlimBus_DoReconfigureNow(SlimBusHandle h);
extern SBResult
SlimBus_GetDataChannelStatus(SlimBusHandle h, SlimBusResourceHandle hChannel, SlimBusDataChannelStatusType * peStatus);
extern SBResult
SlimBus_DeallocDataChannel(SlimBusHandle h, SlimBusResourceHandle hChannel);
extern SBResult
SlimBus_GetPortFifoStatus(SlimBusHandle h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, uint32 * puFifoWordCnt, SBBOOL * pbWatermarkIsHit);
extern SBResult
SlimBus_RegisterPortEventEx
(
  SlimBusHandle h,
  SlimBusResourceHandle hPort,
  SlimBusEventHandle hEvent,
  void * pUser,
  SlimBusPortEventType eType
);
extern SBResult
SlimBus_GetPortEvent
(
  SlimBusHandle h,
  SlimBusResourceHandle hPort,
  SlimBusEventNotifyType * pNotify
);
extern SBResult
SlimBus_AllocProgressCounter(SlimBusHandle h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusResourceHandle * phCounter);
extern SBResult
SlimBus_DeallocProgressCounter(SlimBusHandle h, SlimBusResourceHandle hCounter);
extern SBResult
SlimBus_InitProgressCounter(SlimBusHandle h, SlimBusResourceHandle hCounter, uint32 uCount);
extern SBResult
SlimBus_ReadProgressCounter(SlimBusHandle h, SlimBusResourceHandle hCounter, SlimBusCounterType eType, uint32 * puNumDMA, uint32 * puFifoSamples);
extern SBResult
SlimBus_ReadProgressCounterTimestamp(SlimBusHandle h, SlimBusResourceHandle hCounter, uint32 *puNumDMA, uint32 *puFifoSamples, uint64 *puTimestamp, SBBOOL *pbSampleMissed);
extern SBResult
SlimBus_ReadProgressCounterVFRStatus(SlimBusHandle h, SlimBusResourceHandle hCounter, SBBOOL * pbSet);
extern SBResult
SlimBus_ClearProgressCounterVFRStatus(SlimBusHandle h, SlimBusResourceHandle hCounter);
extern SBResult
SlimBus_AllocResourceGroup(SlimBusHandle h, SlimBusResourceHandle * phGroup);
extern SBResult
SlimBus_DeallocResourceGroup(SlimBusHandle h, SlimBusResourceHandle hGroup);
extern SBResult
SlimBus_AddResourceToGroup(SlimBusHandle h, SlimBusResourceHandle hGroup, SlimBusResourceHandle hResource);
extern SBResult
SlimBus_DeviceInitHardware(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_DeviceInitInternal(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBusCommonIsr(SlimBusDevCtxt *pDevCtxt, struct SlimBusIsrCtxt *pIsrCtxt);
extern SBResult SlimBusCommonDpc(SlimBusDevCtxt *pDevCtxt, struct SlimBusIsrCtxt *pIsrCtxt, SlimBusLinkNodeType *pSignalList);
extern SBResult SlimBus_HandleIsrRxMsgs
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusLinkNodeType *pSignalList
);
extern void SlimBusTimerCallback(SlimBusDevCtxt *pDevCtxt);
extern void SlimBusBamResetTimerCallback(SlimBusDevCtxt *pDevCtxt);
extern void SlimBusEnumTimerCallback(SlimBusDevCtxt *pDevCtxt);
extern void SlimBusSFTimerCallback(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_AllocMsgBuffers(SlimBusDevCtxt *pDevCtxt, uint32 *puOffset);
extern void SlimBus_SendTxBamBuffer
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusMsgFifoType *pMsgFifo
);
extern uint32 *SlimBus_GetTxMsgBuffer
(
  SlimBusClientCtxt *pClientCtxt,
  SlimBusDeviceType *pDevice,
  SlimBusMsgFifoType *pMsgFifo,
  uint32 uNumBytes
);
extern void SlimBus_ProcessPortDisconnection
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusMasterPortType *pPort,
  SlimBusPortEventType *peEvents
);
extern void EnablePortInterrupt(SlimBusDevCtxt *pDevCtxt, uint32 uIntMask);
extern void DisablePortInterrupt(SlimBusDevCtxt *pDevCtxt, uint32 uIntMask);
extern void SlimBus_CleanupMsgEvent
(
  SlimBusDeviceType *pDevice,
  uint32 TID,
  SlimBusEventNodeType *pEventNode,
  SlimBusLinkNodeType *pRootNode
);
extern void SlimBus_ResetMsgFifo(SlimBusMsgFifoType *pMsgFifo);
extern SBResult SlimBus_MsgWorkerCallback
(
  SlimBusDevCtxt *pDevCtxt,
  SlimBusLinkNodeType *pSignalList
);
extern SBResult SlimBus_WaitForMsgEvent
(
  SlimBusClientCtxt *pClientCtxt,
  SlimBusEventNodeType *pEventNode
);
extern SBBOOL SlimBus_ReadyForLowPowerMode(SlimBusDevCtxt *pDevCtxt);
extern void SlimBus_CheckFramerStateInternal(SlimBusDevCtxt *pDevCtxt);
extern SBBOOL SlimBus_HardwareIsEnumerated(SlimBusDevCtxt *pDevCtxt);
typedef uint32 DALInterruptID;
typedef void * DALISRCtx;
typedef void * (*DALISR)(DALISRCtx);
typedef void * DALIRQCtx;
typedef void * (*DALIRQ)(DALIRQCtx);
typedef enum
{
  DALINTCNTLR_NO_POWER_COLLAPSE,
  DALINTCNTLR_POWER_COLLAPSE,
  PLACEHOLDER_InterruptControllerSleepType = 0x7fffffff
} InterruptControllerSleepType;
typedef struct DalInterruptController DalInterruptController;
struct DalInterruptController
{
   struct DalDevice DalDevice;
   DALResult (*RegisterISR)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable);
   DALResult (*RegisterIST)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable,char* pISTName);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, DALInterruptID intrID,
                             const DALSYSEventHandle hEvent, uint32 bEnable);
   DALResult (*Unregister)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDone)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptEnable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDisable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptClear)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptStatus)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*RegisterIRQHandler)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable);
  DALResult (*SetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID, uint32 nTrigger);
  DALResult (*IsInterruptPending)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*MapWakeupInterrupt)(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nWakeupIntID);
  DALResult (*IsAnyInterruptPending)(DalDeviceHandle * _h, uint32* bState,uint32 size);
  DALResult (*Sleep)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*Wakeup)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*GetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger, uint32 size);
  DALResult (*GetInterruptID)(DalDeviceHandle * _h, const char *szIntrName, uint32* pnIntrID, uint32 size);
  DALResult (*LogState)(DalDeviceHandle * _h, void *pULog);
};
typedef struct DalInterruptControllerHandle DalInterruptControllerHandle;
struct DalInterruptControllerHandle
{
   uint32 dwDalHandleId;
   const DalInterruptController * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
DalInterruptController_RegisterISR(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterISR( _h, intrID,
                                                                   isr, ctx, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIST(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags, char* pISTName)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIST( _h, intrID,
                                                                   isr, ctx, IntrFlags,pISTName);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterEvent(DalDeviceHandle * _h, DALInterruptID intrID,
                            const DALSYSEventHandle hEvent, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterEvent( _h, intrID,
                                                                        hEvent, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Unregister(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Unregister( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDone(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDone( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptEnable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptEnable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDisable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDisable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptTrigger( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptClear(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptClear( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptStatus
(
  DalDeviceHandle * _h,
  DALInterruptID intrID
)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptStatus( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIRQHandler(DalDeviceHandle * _h, DALInterruptID intrID,const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIRQHandler( _h, intrID,
                                                                   irq, ctx, bEnable);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_SetTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->SetInterruptTrigger( _h,
            intrID, nTrigger);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptPending(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptPending( _h,
            intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptEnabled(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptEnabled( _h,
           intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_MapWakeupInterrupt(DalDeviceHandle * _h, DALInterruptID intrID, uint32 WakeupIntID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->MapWakeupInterrupt( _h,
           intrID, WakeupIntID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsAnyInterruptPending(DalDeviceHandle * _h, uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsAnyInterruptPending( _h,
           (uint32*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Sleep(DalDeviceHandle * _h,InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Sleep( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Wakeup(DalDeviceHandle * _h, InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Wakeup( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptTrigger( _h,
             intrID,eTrigger,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptID(DalDeviceHandle * _h, const char * szIntrName,DALInterruptID* pnIntrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptID( _h, szIntrName, pnIntrID,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_LogState(DalDeviceHandle * _h, void *pULog)
{
   return ((DalInterruptControllerHandle *)_h)->pVtbl->LogState( _h, pULog);
}
typedef struct DalTimer DalTimer;
struct DalTimer
{
   struct DalDevice DalDevice;
   DALResult (*Register)(DalDeviceHandle * _h, const DALSYSEventHandle hEvent, uint32 time);
   DALResult (*UnRegister)(DalDeviceHandle * _h, const DALSYSEventHandle hEvent);
   DALResult (*GetTimerCount)(DalDeviceHandle * _h, void * timer_count, uint32 size);
};
typedef struct DalTimerHandle DalTimerHandle;
struct DalTimerHandle
{
   uint32 dwDalHandleId;
   const DalTimer * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalTimer_Register(DalDeviceHandle * _h, const DALSYSEventHandle hEvent, uint32 time)
{
   return ((DalTimerHandle *)_h)->pVtbl->Register( _h, hEvent, time);
}
static __inline DALResult
DalTimer_UnRegister(DalDeviceHandle * _h, const DALSYSEventHandle hEvent)
{
   return ((DalTimerHandle *)_h)->pVtbl->UnRegister( _h, hEvent);
}
static __inline DALResult
DalTimer_GetTimerCount(DalDeviceHandle * _h, void * timer_count, uint32 size)
{
   return ((DalTimerHandle *)_h)->pVtbl->GetTimerCount( _h, timer_count, size);
}
static __inline uint8 *DalHWIO_DerefWeakPointer(uint8 **ppAddress)
{
  return (ppAddress != 0 ? *ppAddress : 0);
}
typedef struct DalHWIO DalHWIO;
struct DalHWIO
{
   struct DalDevice DalDevice;
   DALResult (*MapRegion)(DalDeviceHandle * _h, const char * szBase, uint8 **ppAddress);
   DALResult (*UnMapRegion)(DalDeviceHandle * _h, uint8 *pVirtAddress);
   DALResult (*MapRegionByAddress)(DalDeviceHandle * _h, uint8 *pPhysAddress, uint8 **ppAddress);
   DALResult (*MapAllRegions)(DalDeviceHandle * _h);
};
typedef struct DalHWIOHandle DalHWIOHandle;
struct DalHWIOHandle
{
   uint32 dwDalHandleId;
   const DalHWIO *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult DalHWIO_MapRegion
(
  DalDeviceHandle *_h,
  const char *szBase,
  uint8 **ppAddress
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->MapRegion( _h, szBase, ppAddress);
}
static __inline DALResult DalHWIO_MapRegionByAddress
(
  DalDeviceHandle *_h,
  uint8 *pPhysAddress,
  uint8 **ppAddress
)
{
  return
    ((DalHWIOHandle *)_h)->pVtbl->MapRegionByAddress(
      _h, pPhysAddress, ppAddress);
}
static __inline DALResult DalHWIO_UnMapRegion
(
  DalDeviceHandle *_h,
  uint8 *pVirtAddress
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->UnMapRegion( _h, pVirtAddress);
}
static __inline DALResult DalHWIO_MapAllRegions
(
  DalDeviceHandle *_h
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->MapAllRegions(_h);
}
typedef enum
{
   DAL_GPIO_MODE_PRIMARY,
   DAL_GPIO_MODE_IO,
   DAL_PLACEHOLDER_DALGpioModeType = 0x7fffffff
}DalGpioModeType;
typedef enum
{
  DAL_GPIO_INACTIVE = 0,
  DAL_GPIO_ACTIVE = 1,
  DAL_PLACEHOLDER_DALGpioStatusType = 0x7fffffff
}DALGpioStatusType;
typedef enum
{
  DAL_GPIO_OWNER_MASTER,
  DAL_GPIO_OWNER_APPS1,
  DAL_PLACEHOLDER_DALGpioOwnerType = 0x7fffffff
}DALGpioOwnerType;
typedef enum
{
  DAL_GPIO_UNLOCKED = 0,
  DAL_GPIO_LOCKED = 1,
  DAL_PLACEHOLDER_DALGpioLockType = 0x7fffffff
}DALGpioLockType;
typedef enum
{
  DAL_GPIO_CLIENT_PWRDB = 0,
  DAL_PLACEHOLDER_DALGpioClientType = 0x7fffffff
}DALGpioClientType;
typedef enum
{
  DAL_GPIO_INPUT = 0,
  DAL_GPIO_OUTPUT = 1,
  DAL_PLACEHOLDER_DALGpioDirectionType = 0x7fffffff
}DALGpioDirectionType;
typedef enum
{
  DAL_GPIO_NO_PULL = 0,
  DAL_GPIO_PULL_DOWN = 0x1,
  DAL_GPIO_KEEPER = 0x2,
  DAL_GPIO_PULL_UP = 0x3,
  DAL_PLACEHOLDER_DALGpioPullType = 0x7fffffff
}DALGpioPullType;
typedef enum
{
  DAL_GPIO_2MA = 0,
  DAL_GPIO_4MA = 0x1,
  DAL_GPIO_6MA = 0x2,
  DAL_GPIO_8MA = 0x3,
  DAL_GPIO_10MA = 0x4,
  DAL_GPIO_12MA = 0x5,
  DAL_GPIO_14MA = 0x6,
  DAL_GPIO_16MA = 0x7,
  DAL_PLACEHOLDER_DALGpioDriveType = 0x7fffffff
}DALGpioDriveType;
typedef enum
{
  DAL_GPIO_LOW_VALUE,
  DAL_GPIO_HIGH_VALUE,
  DAL_PLACEHOLDER_DALGpioValueType = 0x7fffffff
}DALGpioValueType;
typedef uint32 DALGpioSignalType;
typedef uint32 DALGpioIdType;
typedef enum
{
  DAL_GPIO_SLEEP_CONFIG_ALL,
  DAL_GPIO_SLEEP_CONFIG_APPS,
  DAL_GPIO_SLEEP_CONFIG_NUM_MODES,
  DAL_PLACEHOLDER_DALGpioSleepConfigType = 0x7fffffff
}DALGpioSleepConfigType;
typedef enum
{
  DAL_TLMM_GPIO_DISABLE,
  DAL_TLMM_GPIO_ENABLE,
  DAL_PLACEHOLDER_DALGpioEnableType = 0x7fffffff
}DALGpioEnableType;
typedef enum
{
  DAL_TLMM_USB_PORT_SEL,
  DAL_TLMM_PORT_CONFIG_USB,
  DAL_TLMM_PORT_TSIF,
  DAL_TLMM_AUD_PCM,
  DAL_TLMM_UART1,
  DAL_TLMM_UART3,
  DAL_TLMM_LCDC_CFG,
  DAL_TLMM_KEYSENSE_SC_IRQ,
  DAL_TLMM_KEYSENSE_A9_IRQ,
  DAL_TLMM_TCXO_EN,
  DAL_TLMM_SSBI_PMIC,
  DAL_TLMM_PA_RANGE1,
  DAL_TLMM_SPARE_WR_UART_SEL,
  DAL_TLMM_PAD_ALT_FUNC_SDIO_EN,
  DAL_TLMM_SPARE_WR_TCXO_EN,
  DAL_TLMM_SPARE_WR_PA_ON,
  DAL_TLMM_SDC3_CTL,
  DAL_TLMM_SDC4_CTL,
  DAL_TLMM_UIM1_PAD_CTL,
  DAL_TLMM_UIM2_PAD_CTL,
  DAL_TLMM_NUM_PORTS,
  DAL_PLACEHOLDER_DALGpioPortType = 0x7fffffff
}DALGpioPortType;
typedef struct
{
  DALGpioDirectionType eDirection;
  DALGpioPullType ePull;
  DALGpioDriveType eDriveStrength;
}DalTlmm_GpioConfigIdType;
typedef struct
{
  uint32 nGpioNumber;
  uint32 nFunctionSelect;
  DalTlmm_GpioConfigIdType Settings;
  DALGpioValueType eOutputDrive;
}DalTlmm_GpioIdSettingsType;
typedef struct DalTlmm DalTlmm;
struct DalTlmm
{
   struct DalDevice DalDevice;
   DALResult (*ConfigGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*ConfigGpioGroup)(DalDeviceHandle * _h, DALGpioEnableType enable, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*TristateGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*TristateGpioGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*ConfigGpiosForSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*RestoreGpiosFromSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*GetGpioNumber)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, uint32 * number);
   DALResult (*GpioIn)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType* value);
   DALResult (*GpioOut)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType value);
   DALResult (*GpioOutGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_config, uint32 size, DALGpioValueType value);
   DALResult (*SwitchGpioIntOwnership)(DalDeviceHandle * _h, uint32 owner);
   DALResult (*SetTlmmPort)(DalDeviceHandle * _h, DALGpioPortType Port, uint32 value);
   DALResult (*ExternalGpioConfig)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*RequestGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*ReleaseGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*GetGpioOwner)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioOwnerType* owner);
   DALResult (*GetCurrentConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*GetGpioStatus)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioStatusType* status);
   DALResult (*SetInactiveConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType config);
   DALResult (*GetInactiveConfig)(DalDeviceHandle *h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*LockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*UnlockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*IsGpioLocked)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock);
   DALResult (*GpioBypassSleep)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioEnableType enable);
   DALResult (*AttachRemote)(DalDeviceHandle * _h, uint32 processor);
   DALResult (*GetOutput)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value);
   DALResult (*GetGpioId)(DalDeviceHandle * _h, const char* gpio_str, DALGpioIdType* nGpioId);
   DALResult (*ConfigGpioId)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* tGpioSettings);
   DALResult (*ConfigGpioIdInactive)(DalDeviceHandle * _h, DALGpioIdType nGpioId);
   DALResult (*GpioIdOut)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType value);
   DALResult (*GpioIdIn)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *value);
   DALResult (*ReleaseGpioId)(DalDeviceHandle * h, DALGpioIdType nGpioId);
   DALResult (*SelectGpioIdMode)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings);
   DALResult (*GetGpioIdSettings)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings);
   DALResult (*ConfigGpioIdModeIndexed)(DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex);
};
typedef struct DalTlmmHandle DalTlmmHandle;
struct DalTlmmHandle
{
   uint32 dwDalHandleId;
   const DalTlmm * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalTlmm_ConfigGpio(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpio( _h, gpio_config, enable);
}
static __inline DALResult
DalTlmm_ConfigGpioGroup
(
  DalDeviceHandle * _h,
  DALGpioEnableType enable,
  DALGpioSignalType* gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_6(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, enable, gpio_group, size*(sizeof(uint32)));
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioGroup( _h, enable, gpio_group, size);
}
static __inline DALResult
DalTlmm_TristateGpio
(
  DalDeviceHandle* _h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_0(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpio( _h,
                                                   (DALGpioSignalType)gpio_config);
}
static __inline DALResult
DalTlmm_TristateGpioGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, size);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpioGroup( _h, gpio_group, size);
}
static __inline DALResult
DalTlmm_ConfigGpiosForSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpiosForSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpiosForSleep( _h,
                                                            gpio_sleep_config);
}
static __inline DALResult
DalTlmm_RestoreGpiosFromSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RestoreGpiosFromSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h,
        gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RestoreGpiosFromSleep( _h,
                                                      gpio_sleep_config);
}
static __inline DALResult
DalTlmm_GetGpioNumber
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  uint32 *gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioNumber)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioNumber( _h,
                                                     (DALGpioSignalType)gpio_config,
                                                     gpio_number);
}
static __inline DALResult
DalTlmm_GpioIn
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType*value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioIn)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIn( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOut
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOut)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOut( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOutGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOutGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOutGroup( _h,
                                                      gpio_group,
                                                      size, value);
}
static __inline DALResult
DalTlmm_SwitchGpioIntOwnership
(
  DalDeviceHandle *_h,
  uint32 owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SwitchGpioIntOwnership)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SwitchGpioIntOwnership( _h, owner);
}
static __inline DALResult
DalTlmm_SetPort
(
  DalDeviceHandle *_h,
  DALGpioPortType port,
  uint32 value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetTlmmPort)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, port, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetTlmmPort( _h, port, value);
}
static __inline DALResult
DalTlmm_ExternalGpioConfig
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ExternalGpioConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ExternalGpioConfig( _h,
                                    gpio_config, enable);
}
static __inline DALResult
DalTlmm_RequestGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RequestGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RequestGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_ReleaseGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ReleaseGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioOwner
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioOwnerType* owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioOwner)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioOwner( _h, gpio_number, owner);
}
static __inline DALResult
DalTlmm_GetCurrentConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetCurrentConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->GetCurrentConfig( _h, gpio_number, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioStatus
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioStatusType *status
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioStatus)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)status);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioStatus( _h, gpio_number, status);
}
static __inline DALResult
DalTlmm_SetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_GetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_LockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->LockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->LockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_UnlockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->UnlockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->UnlockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_IsGpioLocked(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->IsGpioLocked)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)lock);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->IsGpioLocked( _h, gpio_number, lock);
}
static __inline DALResult
DalTlmm_GpioBypassSleep
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioBypassSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioBypassSleep( _h, gpio_number, enable);
}
static __inline DALResult
DalTlmm_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->AttachRemote)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, processor);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->AttachRemote( _h, processor);
}
static __inline DALResult
DalTlmm_GetOutput(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetOutput)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_number, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetOutput( _h, gpio_number, value);
}
static __inline DALResult
DalTlmm_GetGpioId(DalDeviceHandle * _h, const char* pszGpio, DALGpioIdType* pnGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioId( _h, pszGpio, pnGpioId);
}
static __inline DALResult
DalTlmm_ConfigGpioId(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioId( _h, nGpioId, pUserSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdInactive(DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdInactive( _h, nGpioId);
}
static __inline DALResult
DalTlmm_GpioIdOut(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdOut( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_GpioIdIn(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdIn( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_ReleaseGpioId( DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpioId(_h, nGpioId);
}
static __inline DALResult
DalTlmm_SelectGpioIdMode( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->SelectGpioIdMode(_h, nGpioId, eMode, pUserSettings);
}
static __inline DALResult
DalTlmm_GetGpioIdSettings( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioIdSettings(_h, nGpioId, pGpioSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdModeIndexed( DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex )
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdModeIndexed(_h, nGpioId, nIndex);
}
typedef struct
{
    SlimBusLogicalAddrType LA;
    uint8 uaEA[6];
    uint8 uDataLineMask;
} SlimBusDeviceDalProps;
static __inline void * SlimBus_GetDevHandle(SlimBusDevCtxt *pDevCtxt)
{
  return pDevCtxt;
}
extern SBResult SlimBus_InitDriverType(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitParams(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitMasterParams(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitSatParams(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitServices(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitMasterServices(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_InitGpios(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_DeinitGpios(SlimBusDevCtxt *pDevCtxt);
extern SBBOOL SlimBus_UseSatelliteClockControl(SlimBusDevCtxt *pDevCtxt);
static __inline SBResult SlimBus_StopIdle(SlimBusDevCtxt *pDevCtxt)
{
  return 0;
}
static __inline SBResult SlimBus_ResumeIdle(SlimBusDevCtxt *pDevCtxt)
{
  return 0;
}
static __inline SBResult SlimBus_Malloc(uint32 dwSize, void **ppVoid, uint32 uTag)
{
  return DALSYS_Malloc(dwSize, ppVoid);
}
static __inline void SlimBus_Free(void *pMem)
{
  (void) DALSYS_Free(pMem);
}
static __inline void SlimBus_MarkDeviceStatic(SlimBusDevCtxt *pDevCtxt)
{
  DALFW_MarkDeviceStatic((DALDevCtxt*)pDevCtxt);
}
static __inline void SlimBus_LockDevice(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt &&
      0 != pDevCtxt->hSync)
  {
    DALSYS_SyncEnter(pDevCtxt->hSync);
  }
}
static __inline void SlimBus_UnlockDevice(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt &&
      0 != pDevCtxt->hSync)
  {
    DALSYS_SyncLeave(pDevCtxt->hSync);
  }
}
static __inline void SlimBus_LockExtClkToggle(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt &&
      0 != pDevCtxt->hExtClkSync)
  {
    DALSYS_SyncEnter(pDevCtxt->hExtClkSync);
  }
}
static __inline void SlimBus_UnlockExtClkToggle(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt &&
      0 != pDevCtxt->hExtClkSync)
  {
    DALSYS_SyncLeave(pDevCtxt->hExtClkSync);
  }
}
extern SBResult SlimBus_ClkGpioIntEnable(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_ClkGpioIntDisable(SlimBusDevCtxt *pDevCtxt);
static __inline SBResult SlimBus_CreateClientLock(SlimBusClientCtxt *pCtxt)
{
  return DALSYS_SyncCreate(0x00000020, &pCtxt->hSync, 0);
}
static __inline void SlimBus_LockClient(SlimBusClientCtxt *pCtxt)
{
  if (0 != pCtxt &&
      0 != pCtxt->hSync)
  {
    DALSYS_SyncEnter(pCtxt->hSync);
  }
}
static __inline void SlimBus_UnlockClient(SlimBusClientCtxt *pCtxt)
{
  if (0 != pCtxt &&
      0 != pCtxt->hSync)
  {
    DALSYS_SyncLeave(pCtxt->hSync);
  }
}
static __inline SBResult SlimBus_EventReset(SlimBusEventHandle hEvent)
{
  return DALSYS_EventCtrlEx(hEvent,0x00000200,0,0,0);
}
static __inline SBResult SlimBus_EventWait(SlimBusEventHandle hEvent)
{
  return DALSYS_EventWait(hEvent);
}
static __inline SBResult SlimBus_EventTimedWait
(
  SlimBusClientCtxt *pClientCtxt,
  SlimBusEventHandle hEvent,
  uint32 uTimeoutUsec
)
{
  SBResult result = -1;
  if (0 != pClientCtxt)
  {
    uint32 uIdx = 1;
    DALSYSEventHandle hEvents[] = { hEvent, pClientCtxt->hTimeoutEvent };
    DALSYS_EventCtrlEx(pClientCtxt->hTimeoutEvent,0x00000200,0,0,0);
    result = DALSYS_EventMultipleWait(hEvents,
                                      sizeof(hEvents)/sizeof(hEvents[0]),
                                      uTimeoutUsec,
                                      &uIdx);
    if (0 == result &&
        uIdx > 0)
    {
      result = SLIMBUS_ERR_MSG_RESP_TIMEOUT;
    }
  }
  return result;
}
static __inline SBResult SlimBus_EventTrigger(SlimBusEventHandle hEvent)
{
  return DALSYS_EventCtrlEx(hEvent,0x00000100,0,0,0);
}
static __inline SBResult SlimBus_EventTriggerEx(SlimBusEventHandle hEvent, uint32 uUserData)
{
  return DALSYS_EventCtrlEx(hEvent, 0x00000100, uUserData, 0, 0);
}
static __inline SBResult SlimBus_CreateSyncEvent(SlimBusEventHandle *phEvent, SlimBusEventObj *pEventObj)
{
  return DALSYS_EventCreate(0x00000100, phEvent, pEventObj);
}
static __inline SBResult SlimBus_CreateCallbackEvent(SlimBusEventHandle *phEvent, SlimBusEventObj *pEventObj, SlimBusCallbackFunc cbFunc, void *cbData)
{
  SBResult result;
  result = DALSYS_EventCreate(0x00000020, phEvent, pEventObj);
  if (0 == result)
  {
    result = DALSYS_SetupCallbackEvent(*phEvent, cbFunc, cbData);
  }
  return result;
}
static __inline SBResult SlimBus_CreateTimeoutEvent(SlimBusEventHandle *phEvent, SlimBusEventObj *pEventObj)
{
  return DALSYS_EventCreate( (0x00000040|
                              0x00000200|
                              0x00000000),
                             phEvent,
                             pEventObj );
}
static __inline struct SlimBusDevCtxt *SB_GET_PDEVCTXT(void *hDevCtxt)
{
  return (struct SlimBusDevCtxt*)(hDevCtxt);
}
static __inline struct SlimBusClientCtxt *SB_GET_PCLIENTCTXT(void *hCtxt)
{
  return ((DalSlimBusHandle *)hCtxt)->pClientCtxt;
}
static __inline void SlimBus_InterruptAcquireLock(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt->hIsrSync)
  {
    DALSYS_SyncEnter(pDevCtxt->hIsrSync);
  }
}
static __inline void SlimBus_InterruptReleaseLock(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt->hIsrSync)
  {
    DALSYS_SyncLeave(pDevCtxt->hIsrSync);
  }
}
static __inline void SlimBus_BamAcquireLock(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt->Plat.hBamSync)
  {
    DALSYS_SyncEnter(pDevCtxt->Plat.hBamSync);
  }
}
static __inline void SlimBus_BamReleaseLock(SlimBusDevCtxt *pDevCtxt)
{
  if (0 != pDevCtxt->Plat.hBamSync)
  {
    DALSYS_SyncLeave(pDevCtxt->Plat.hBamSync);
  }
}
static __inline void SlimBus_PortAcquireLock(SlimBusDevCtxt *pDevCtxt, SlimBusMasterPortType *pPort)
{
                               ;
  if (0 != pDevCtxt->hPortSync)
  {
    DALSYS_SyncEnter(pDevCtxt->hPortSync);
  }
}
static __inline void SlimBus_PortReleaseLock(SlimBusDevCtxt *pDevCtxt, SlimBusMasterPortType *pPort)
{
                               ;
  if (0 != pDevCtxt->hPortSync)
  {
    DALSYS_SyncLeave(pDevCtxt->hPortSync);
  }
}
static __inline SBResult SlimBus_PortCreateLock(SlimBusDevCtxt *pDevCtxt, SlimBusMasterPortType *pPort)
{
                               ;
  if (0 == pDevCtxt->hPortSync)
  {
    return DALSYS_SyncCreate(0x00000020, &pDevCtxt->hPortSync, 0);
  }
  else
  {
    return 0;
  }
}
void SlimBus_TimerStart(SlimBusDevCtxt *pDevCtxt, SlimBusTimerHandle hTimer,
                        SlimBusEventHandle hEvent, uint32 uTimeoutUsec);
void SlimBus_TimerStop(SlimBusDevCtxt *pDevCtxt, SlimBusTimerHandle hTimer,
                       SlimBusEventHandle hEvent);
extern SBResult SlimBus_GpioIntDisable(SlimBusDevCtxt *pDevCtxt);
extern SBResult SlimBus_GpioIntEnable(SlimBusDevCtxt *pDevCtxt);
extern SBBOOL SlimBus_CheckGpioClockToggle(SlimBusDevCtxt *pDevCtxt);
static __inline SBResult SlimBus_PhysMemAlloc(uint32 uLen, SlimBusMemHandle *phMem)
{
  return DALSYS_MemRegionAlloc((0x00080000|0x00000040|0x00000C00)|0x00200000,
                               0xFFFFFFFF,
                               0xFFFFFFFF,
                               uLen, phMem, 0);
}
static __inline SBResult SlimBus_PhysMemInfo(SlimBusMemHandle hMem, size_t *puVirtAddr, size_t *puPhysAddr)
{
  DALSYSMemInfo MemInfo;
  SBResult result;
  result = DALSYS_MemInfo(hMem, &MemInfo);
  if (0 == result)
  {
    if (0 != puVirtAddr)
    {
      *puVirtAddr = MemInfo.VirtualAddr;
    }
    if (0 != puPhysAddr)
    {
      *puPhysAddr = MemInfo.PhysicalAddr;
    }
  }
  return result;
}
static __inline SBResult SlimBus_DestroyObject(DALSYSObjHandle hObj)
{
  return DALSYS_DestroyObject(hObj);
}
static __inline SBResult SlimBus_DestroyEvent(SlimBusEventHandle hEvent)
{
  return SlimBus_DestroyObject(hEvent);
}
static __inline void SlimBus_MemoryBarrier(void *pMem)
{
  memory_barrier();
}
static __inline void SlimBus_ErrorFatal(SlimBusDevCtxt *pDevCtxt, const char *pszStr)
{
  DALSYS_LogEvent(pDevCtxt->Base.DevId, 1, "%s", pszStr);
}
static __inline void SlimBus_LogLevelInit(SlimBusDevCtxt *pDevCtxt)
{
  DALSYSPropertyVar PropVar;
  if (-1 != g_iSlimBusLogLevelOverride &&
      g_iSlimBusLogLevelOverride >= 0)
  {
    pDevCtxt->uLogLevel = (uint32)g_iSlimBusLogLevelOverride;
  }
  else if ( 0 == DALSYS_GetPropertyValue( pDevCtxt->Base.hProp,
                                                    "log_level", 0, &PropVar ) &&
            0x02 == PropVar.dwType )
  {
    pDevCtxt->uLogLevel = PropVar.Val.dwVal;
  }
  else
  {
    pDevCtxt->uLogLevel = 1;
  }
}
extern SBResult
SlimBus_ConfigMasterPipe( DalDeviceHandle * h, SlimBusResourceHandle hPort, SlimBusPortFlowType eFlow, const SlimBusPipeConfigType * pConfig);
extern SBResult
SlimBus_RegisterBamEvent( DalDeviceHandle * h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamRegisterEventType * pReg);
extern SBResult
SlimBus_SubmitBamTransfer( DalDeviceHandle * h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void * pUser);
extern SBResult
SlimBus_GetBamIOVec( DalDeviceHandle * h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec);
extern SBResult
SlimBus_GetBamIOVecEx(DalDeviceHandle * h, SlimBusResourceHandle hPort, SlimBusBamTransferType eTransferDir, SlimBusBamIOVecType * pIOVec, void **ppUser);
SlimBusBSPType SlimBusBSP[] =
{
  {
    2,
    "SLIMBUS",
    { 0x00, 0x00, 0x60, 0x01, 0x17, 0x02 },
    "LPASS",
    0x001c0000,
    0x091c0000,
    0x09184000,
    11,
    12,
    0,
    { (((70) & 0x3FF)<< 4 | ((1) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_KEEPER) & 0x3) << 15| ((DAL_GPIO_8MA)& 0xF) << 17| 0x20000000),
      (((71) & 0x3FF)<< 4 | ((1) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_KEEPER) & 0x3) << 15| ((DAL_GPIO_8MA)& 0xF) << 17| 0x20000000),
      (((72) & 0x3FF)<< 4 | ((1) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_KEEPER) & 0x3) << 15| ((DAL_GPIO_8MA)& 0xF) << 17| 0x20000000) },
    71,
    { 1, 1, 1 }
  }
};
const SlimBusDeviceDalProps sbDeviceProps[] =
{
    {0xc0, {0x00, 0x00, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc1, {0x00, 0x01, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc2, {0x00, 0x03, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc3, {0x00, 0x04, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc4, {0x00, 0x05, 0x60, 0x01, 0x17, 0x02}, 0x03},
    {0xca, {0x00, 0x00, 0xa0, 0x01, 0x17, 0x02}, 0x01},
    {0xcb, {0x00, 0x01, 0xa0, 0x01, 0x17, 0x02}, 0x03},
    {0xcc, {0x00, 0x00, 0x30, 0x01, 0x17, 0x02}, 0x01},
    {0xcd, {0x00, 0x01, 0x30, 0x01, 0x17, 0x02}, 0x01}
};
const uint32 sbNumDeviceProps = sizeof(sbDeviceProps) / sizeof(SlimBusDeviceDalProps);
SlimBusBSPType SlimBusBSP2[] =
{
  {
    2,
    "SLIMBUS_QCA",
    { 0x01, 0x00, 0x60, 0x01, 0x17, 0x02 },
    "LPASS",
    0x00240000,
    0x09240000,
    0x09204000,
    86,
    87,
    0,
    { (((74) & 0x3FF)<< 4 | ((1) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_KEEPER) & 0x3) << 15| ((DAL_GPIO_8MA)& 0xF) << 17| 0x20000000),
      (((73) & 0x3FF)<< 4 | ((1) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_KEEPER) & 0x3) << 15| ((DAL_GPIO_8MA)& 0xF) << 17| 0x20000000) },
    73,
    { 1, 1, 1 }
  }
};
const SlimBusDeviceDalProps sbDeviceProps2[] =
{
    {0xc5, {0x01, 0x00, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc6, {0x01, 0x01, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc7, {0x01, 0x03, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc8, {0x01, 0x04, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xc9, {0x01, 0x05, 0x60, 0x01, 0x17, 0x02}, 0x01},
    {0xce, {0x00, 0x00, 0x20, 0x01, 0x17, 0x02}, 0x01},
    {0xcf, {0x00, 0x01, 0x20, 0x01, 0x17, 0x02}, 0x01}
};
const uint32 sbNumDeviceProps2 = sizeof(sbDeviceProps2) / sizeof(SlimBusDeviceDalProps);
const MmpmRegParamType sbMmpmRegParam =
{
  0x00000002,
  MMPM_CORE_ID_LPASS_SLIMBUS,
  MMPM_CORE_INSTANCE_0,
  "slimbus",
  0x0000,
  0x0000,
  0,
  0
};
const MmpmRegParamType sbMmpmRegParam2 =
{
  0x00000002,
  MMPM_CORE_ID_LPASS_SLIMBUS,
  MMPM_CORE_INSTANCE_1,
  "slimbus_qca",
  0x0000,
  0x0000,
  0,
  0
};
