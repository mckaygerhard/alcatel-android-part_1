# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/mproc/smd/config/smd_amdsr.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/mproc/smd/config/smd_amdsr.xml" 2
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/mproc/smd/config/smd_amdsr.xml"
<driver name="NULL">
  <global_def></global_def>
  <device id="/core/mproc/smd">
    <props name="smd_intr_enabled" type= DALPROP_ATTR_TYPE_UINT32>0x4f</props>
  </device>
</driver>

<!-- Disable interrupts on edges where remote endpoint is not present.
     Corresponding bit is 0 if processor is not supported

  0100 1111 = 0x4f (configuration for msm)

  Below is bit position for each processor

  APPS = 0x1 (0000 0001)
  MODEM = 0x2 (0000 0010)
  ADSP = 0x4 (0000 0100)
  SSC = 0x8 (0000 1000)
  WCNSS = 0x10 (0001 0000)
  MdmFW = 0x20 (0010 0000)
  RPM = 0x40 (0100 0000)
-->
