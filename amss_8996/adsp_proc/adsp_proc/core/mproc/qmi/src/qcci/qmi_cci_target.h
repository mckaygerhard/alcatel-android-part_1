#ifndef QMI_CCI_TARGET_H
#define QMI_CCI_TARGET_H
/******************************************************************************
  @file    qmi_cci_target.h
  @brief   OS Specific routines internal to QCCI.

  DESCRIPTION
  This header provides an OS abstraction to QCCI.

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "qurt.h"
#include "smem_log.h"
#include "err.h"
#include "assert.h"
#include "timer.h"
#include "qmi_common.h"
#include "qmi_cci_target_ext.h"
#include "qmi_idl_lib_internal.h"

typedef qurt_mutex_t qmi_cci_lock_type;
#define LOCK(ptr)        qurt_mutex_lock(ptr)
#define UNLOCK(ptr)      qurt_mutex_unlock(ptr)
#define LOCK_INIT(ptr)   qurt_mutex_init(ptr)
#define LOCK_DEINIT(ptr) qurt_mutex_destroy(ptr)

#define MALLOC(size)      malloc(size)
#define CALLOC(num, size) calloc(num, size)
#define FREE(ptr)         free(ptr)
#define REALLOC(ptr,size) realloc(ptr, size)

#define QMI_CCI_LOG_EVENT_TX            (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x00)
#define QMI_CCI_LOG_EVENT_TX_EXT        (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x04)
#define QMI_CCI_LOG_EVENT_RX            (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x01)
#define QMI_CCI_LOG_EVENT_RX_EXT        (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x05)
#define QMI_CCI_LOG_EVENT_ERROR         (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x03)

#define QMI_CCI_OS_SIGNAL_VALID(ptr) ((ptr)->inited == TRUE)
#define QMI_CCI_OS_EXT_SIGNAL_VALID(ptr) QMI_CCI_OS_SIGNAL_VALID(ptr)

#define QMI_CCI_OS_EXT_SIGNAL_INIT(ptr, os_params) \
  do { \
    qurt_anysignal_t *tmp_sig_ptr;  \
    ptr = os_params; \
    (ptr)->inited = FALSE;  \
    (ptr)->timed_out = 0; \
    (ptr)->timer_inited = FALSE; \
    if((ptr)->sig == 0) { \
      (ptr)->sig = 0x1; \
    } else if(!is_pow_2((ptr)->sig)) {  \
      break; \
    } \
    if((ptr)->ext_signal) { \
      tmp_sig_ptr = (ptr)->ext_signal;  \
    } else {  \
      tmp_sig_ptr = &((ptr)->signal); \
      qurt_anysignal_init(tmp_sig_ptr); \
    } \
    if((ptr)->timer_sig != 0 && (ptr)->timer_sig != (ptr)->sig && is_pow_2((ptr)->timer_sig)) { \
      timer_error_type err; \
      err = timer_def_osal(&((ptr)->timer), NULL, TIMER_NATIVE_OS_SIGNAL_TYPE, tmp_sig_ptr, (ptr)->timer_sig); \
      if (err == TE_SUCCESS) { \
        (ptr)->timer_inited = TRUE; \
      } else { \
        MSG_1(MSG_SSID_ONCRPC, MSG_LEGACY_ERROR, "QCCI: ATS timer creation failed with error=%d.", err); \
      } \
    } \
    (ptr)->inited = TRUE; \
  } while(0)

#ifdef FEATURE_QMI_SMEM_LOG

/* Assumes addr_len is MAX_ADDR_LEN. Change needed if qmi_cci_common.c changes
 * to invalidate this assumption */
#define QMI_CCI_OS_LOG_TX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len) \
  do {  \
    uint32_t *int_addr = (uint32_t *)(addr);  \
    SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_TX_EXT, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, (svc_obj)->service_id, int_addr[0], int_addr[1], int_addr[2]); \
  } while(0)

/* Assumes addr_len is MAX_ADDR_LEN. Change needed if qmi_cci_common.c changes
 * to invalidate this assumption */
#define QMI_CCI_OS_LOG_RX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len) \
  do {  \
    uint32_t *int_addr = (uint32_t *)(addr);  \
    SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_RX_EXT, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, (svc_obj)->service_id, int_addr[0], int_addr[1], int_addr[2]); \
  } while(0)

#else

#define QMI_CCI_OS_LOG_TX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len)
#define QMI_CCI_OS_LOG_RX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len)

/* Sensors does not have ASSERT defined */
#ifdef ASSERT
#undef ASSERT
#endif

#define ASSERT(cond)

#endif

#define QMI_CCI_OS_LOG_ERROR() qcci_log_error(__FILENAME__, __LINE__)

size_t strlcpy(char *dst, const char *src, size_t siz);
static __inline void qcci_log_error(char *filename, unsigned int line)
{
  uint32 name[5];
  strlcpy((char *)name, filename, sizeof(name));
#ifdef FEATURE_QMI_SMEM_LOG
  SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_ERROR, name[0], name[1], name[2], name[3], 
      name[4], line);
#endif
#ifdef FEATURE_QMI_MSG
  MSG_2(MSG_SSID_ONCRPC, MSG_LEGACY_ERROR,
        "Runtime error. File 0x%s, Line: %d", filename, line);
#endif
}

#endif

