/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             Diagnostic Subsystem Buffer Allocation Manager

General Description
  This file contains routines that manage the diag output buffer.
  This allocation scheme is modeled as a circular buffer. Currently, 
  diag output buffer is used for streamed data (LOG, MSG).

Initialization and Sequencing Requirements diagbuf_mpd_init() must be 
called before using this service.

Copyright (c) 2014 by QUALCOMM Technologies, Incorporated. 
All Rights Reserved. Qualcomm Confidential and Proprietary 
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                              Edit History

 $Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/12/14   vk      Created file. To fix 8996 compilation issue. 
===========================================================================*/


#include "customer.h"

#include "comdef.h"
#include "diag_v.h" /* not needed */
#include "diagbuf_v.h" /* not needed */
#include "diagbuf_mpd.h"
#include "diagcomm_v.h"
#include "diagdiag_v.h"
#include "diagi_v.h"
#include "diagtarget.h"
#include "diagtune.h"
#include "err.h"
#include "stringl.h"

#include <stdio.h>

#include "osal.h"
#include "diag_cfg.h"
#include "qurt_qdi.h"
#include "qurt_qdi_driver.h"

#define DIAG_MAX_MUTEX_NAME_SIZE 40

/*===========================================================================

FUNCTION DIAGBUF_MPD_INIT

DESCRIPTION
   This function allocates memory of type diagbuf_mpd_type, 
   initializes it and returns pointer to this location to the
   UserPD. UserPD uses this pointer to access it's diagbuf which
   is allocated in diagbuf_mpd_buf_init() and pointer to it, is
   saved in diagbuf_mpd_type->diagbuf_mpd_buf. It is assumed
   that this function is operating at the privileged level to
   make the Qurt_qdi_user_malloc call.

PARAMETERS 
   int client_handle - that is passed to diag_qdi_invoke
   
RETURN VALUE
   diagbuf_mpd_type if qurt_qdi_user_malloc is successful
   NULL otherwise
   
===========================================================================*/
diagbuf_mpd_type* diagbuf_mpd_init( int client_handle )
{
  diagbuf_mpd_type *rv = NULL;
  char mutex_name[DIAG_MAX_MUTEX_NAME_SIZE];
  int return_val = 0;

  //Initialize the shared memory between root PD and user PD
  rv = (diagbuf_mpd_type *)qurt_qdi_user_malloc(client_handle, sizeof(diagbuf_mpd_type));

  if ( rv == NULL )
  {
    return (rv);
  }
  //initialize all counters to zero
  rv->diagbuf_mpd_head             = 0;
  rv->diagbuf_mpd_tail             = 0; 
  rv->diagbuf_size                 = 0; 
  rv->msg_alloc_count              = 0; 
  rv->msg_drop_count               = 0;
  rv->log_alloc_count              = 0; 
  rv->log_drop_count               = 0;
  rv->diagbuf_mpd_commit_size      = 0;
  rv->diagbuf_mpd_commit_threshold = 0; 
  rv->diagbuf_mpd_drain_threshold  = 0; 

  //Set the process ID of the diagmpd buf
  rv->pid = qurt_getpid();

  //Figure out syntax for getting the process ID.
  snprintf(mutex_name, DIAG_MAX_MUTEX_NAME_SIZE, "MUTEX_DIAGBUF_MPD_CS_%d", (int)rv->pid);

  //Initialize diagbuf mutex
  rv->diagbuf_mpd_buf_cs.name = mutex_name;
  return_val = osal_init_mutex(&rv->diagbuf_mpd_buf_cs);
  ASSERT(OSAL_SUCCESS == return_val);

  return (rv);
} /* diagbuf_mpd_init */

/*===========================================================================

FUNCTION DIAGBUF_MPD_BUF_INIT

DESCRIPTION
   This function allocates and initializes a diagbuf to be used
   by UserPD. It copies the allocated pointer to the UserPD's
   object passed in param *ptr. This allows UserPD to access
   this pointer for writing diag traffic to this buffer which is
   "it's diagbuf" with shared access to Guest OS. This function
   returns allocated pointer to the caller.
   It is assumed that this function is operating at the
   privileged level to make the Qurt_qdi_user_malloc call.

PARAMETERS 
   int client_handle     - that is passed to diag_qdi_invoke
   diagbuf_mpd_type *ptr - pointer to UserPD object
   int buf_len           - length of the diagbuf requested by
                           a UserPD
   
RETURN VALUE
   uint8* pointer to buffer allocated
   NULL otherwise
   
===========================================================================*/
uint8* diagbuf_mpd_buf_init( int client_handle, diagbuf_mpd_type *ptr, int buf_len )
{
  uint8 *rv = NULL;

  if ( ptr )
  {
    /*Allocate the data for the diagbuf portion of the structure */
    rv = qurt_qdi_user_malloc(client_handle, buf_len);


    if ( rv != NULL )
    {
      /* Copy the pointer value to the user PD object passed in */
      if ( qurt_qdi_copy_to_user(client_handle, &(ptr->diagbuf_mpd_buf), &rv, sizeof(rv)) < 0 )
      {
        qurt_qdi_user_free(client_handle, rv);
        ptr->diagbuf_size = 0;
        rv = NULL;
      }
      else
      {
        diagbuf_header_type *pkt_hdr; /* pointer to the first header */

        /* 
           Setting the first status field to DIAGBUF_UNCOMMIT_S is all
           the initizalization needed
        */
        pkt_hdr = ((diagbuf_header_type *)&rv[0]);
        pkt_hdr->status = DIAGBUF_UNUSED_S;
        pkt_hdr->length = buf_len;

        ptr->diagbuf_size                 = buf_len;
        ptr->diagbuf_mpd_commit_threshold = (ptr->diagbuf_size * DIAG_MULTIPD_BUF_COMMIT_THRESHOLD_PER) / 100;
        ptr->diagbuf_mpd_drain_threshold  = (ptr->diagbuf_size * DIAG_MULTIPD_BUF_DRAIN_THRESHOLD_PER) / 100; 
        ptr->diagbuf_mpd_drain_timer      = DIAG_MULTIPD_DRAIN_TIMER; 
        ptr->in_use = TRUE;
      }
    }
  }
  return (rv);
} /* diagbuf_mpd_buf_init */

/*===========================================================================

FUNCTION DIAGBUF_MPD_DEINIT

DESCRIPTION
   This function cleans up the diag allocation buffer at
   startup.
 
PARAMETERS 
   int client_handle     - that is passed to diag_qdi_invoke
   diagbuf_mpd_type *ptr - pointer to UserPD object
   
RETURN VALUE
   None
 
===========================================================================*/
void diagbuf_mpd_deinit( int client_handle, diagbuf_mpd_type *buf )
{
  int return_val;

  //Delete the mutex that was previously used
  if ( buf )
  {
    return_val = osal_delete_mutex(&buf->diagbuf_mpd_buf_cs);
    ASSERT(OSAL_SUCCESS == return_val);

    if ( buf->diagbuf_mpd_buf )
    {
      qurt_qdi_user_free(client_handle, buf->diagbuf_mpd_buf);
    }
    qurt_qdi_user_free(client_handle, buf);
  }
} /* diagbuf_mpd_deinit */

/*===========================================================================
FUNCTION DIAGBUF_MPD_CLEANUP

DESCRIPTION
  Updates in_use field in the client's diagbuf, indicating that
  the buffer is not in use. 
 
PARAMETERS 
   None
   
RETURN VALUE 
   None
   
===========================================================================*/
void diagbuf_mpd_cleanup( void )
{
/*  if ( diagmpd_buf )
  {
    diagmpd_buf->in_use = FALSE;
    //qurt_qdi_user_free(diagmpd_buf->diagbuf_mpd_buf);
    //qurt_qdi_user_free(diagmpd_buf);
    //diagmpd_buf = NULL;
  }
  */
} /* diagbuf_mpd_cleanup */
