<!-- See DDIIPCInt.h for the enum these values come from. -->
<driver name="IPCInt">
  <device id=DALDEVICEID_IPCINT>
    <props name="SourceProc" type=DALPROP_ATTR_TYPE_UINT32>
      6
    </props>
    <props name="IPCINT_PHYSICAL_ADDRESS" type=DALPROP_ATTR_TYPE_UINT32>
      0x9000000
    </props>
    <props name="IPCINT_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
      0x385000
    </props>
  </device>
</driver>
