# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
<driver name="TLMM">

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/hwio/msm8996/msmhwiobase.h" 1
# 3 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h" 1
# 280 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
   DAL_GPIO_MODE_PRIMARY,
   DAL_GPIO_MODE_IO,
   DAL_PLACEHOLDER_DALGpioModeType = 0x7fffffff
}DalGpioModeType;
# 295 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_INACTIVE = 0,
  DAL_GPIO_ACTIVE = 1,

  DAL_PLACEHOLDER_DALGpioStatusType = 0x7fffffff

}DALGpioStatusType;
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_OWNER_MASTER,
  DAL_GPIO_OWNER_APPS1,

  DAL_PLACEHOLDER_DALGpioOwnerType = 0x7fffffff

}DALGpioOwnerType;
# 333 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_UNLOCKED = 0,
  DAL_GPIO_LOCKED = 1,


  DAL_PLACEHOLDER_DALGpioLockType = 0x7fffffff


}DALGpioLockType;
# 354 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_CLIENT_PWRDB = 0,


  DAL_PLACEHOLDER_DALGpioClientType = 0x7fffffff


}DALGpioClientType;
# 390 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_INPUT = 0,
  DAL_GPIO_OUTPUT = 1,


  DAL_PLACEHOLDER_DALGpioDirectionType = 0x7fffffff


}DALGpioDirectionType;
# 413 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_NO_PULL = 0,
  DAL_GPIO_PULL_DOWN = 0x1,
  DAL_GPIO_KEEPER = 0x2,
  DAL_GPIO_PULL_UP = 0x3,


  DAL_PLACEHOLDER_DALGpioPullType = 0x7fffffff


}DALGpioPullType;
# 437 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_2MA = 0,
  DAL_GPIO_4MA = 0x1,
  DAL_GPIO_6MA = 0x2,
  DAL_GPIO_8MA = 0x3,
  DAL_GPIO_10MA = 0x4,
  DAL_GPIO_12MA = 0x5,
  DAL_GPIO_14MA = 0x6,
  DAL_GPIO_16MA = 0x7,


  DAL_PLACEHOLDER_DALGpioDriveType = 0x7fffffff


}DALGpioDriveType;
# 465 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_LOW_VALUE,
  DAL_GPIO_HIGH_VALUE,


  DAL_PLACEHOLDER_DALGpioValueType = 0x7fffffff


}DALGpioValueType;
# 493 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef uint32 DALGpioSignalType;
# 506 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef uint32 DALGpioIdType;
# 521 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_GPIO_SLEEP_CONFIG_ALL,
  DAL_GPIO_SLEEP_CONFIG_APPS,
  DAL_GPIO_SLEEP_CONFIG_NUM_MODES,

  DAL_PLACEHOLDER_DALGpioSleepConfigType = 0x7fffffff

}DALGpioSleepConfigType;
# 542 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_TLMM_GPIO_DISABLE,
  DAL_TLMM_GPIO_ENABLE,


  DAL_PLACEHOLDER_DALGpioEnableType = 0x7fffffff


}DALGpioEnableType;
# 582 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef enum
{
  DAL_TLMM_USB_PORT_SEL,
  DAL_TLMM_PORT_CONFIG_USB,
  DAL_TLMM_PORT_TSIF,

  DAL_TLMM_AUD_PCM,
  DAL_TLMM_UART1,
  DAL_TLMM_UART3,
  DAL_TLMM_LCDC_CFG,
  DAL_TLMM_KEYSENSE_SC_IRQ,

  DAL_TLMM_KEYSENSE_A9_IRQ,

  DAL_TLMM_TCXO_EN,
  DAL_TLMM_SSBI_PMIC,
  DAL_TLMM_PA_RANGE1,
  DAL_TLMM_SPARE_WR_UART_SEL,
  DAL_TLMM_PAD_ALT_FUNC_SDIO_EN,
  DAL_TLMM_SPARE_WR_TCXO_EN,
  DAL_TLMM_SPARE_WR_PA_ON,
  DAL_TLMM_SDC3_CTL,
  DAL_TLMM_SDC4_CTL,
  DAL_TLMM_UIM1_PAD_CTL,
  DAL_TLMM_UIM2_PAD_CTL,


  DAL_TLMM_NUM_PORTS,
  DAL_PLACEHOLDER_DALGpioPortType = 0x7fffffff

}DALGpioPortType;
# 625 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef struct
{
  DALGpioDirectionType eDirection;
  DALGpioPullType ePull;
  DALGpioDriveType eDriveStrength;

}DalTlmm_GpioConfigIdType;
# 641 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/TlmmDefs.h"
typedef struct
{
  uint32 nGpioNumber;
  uint32 nFunctionSelect;
  DalTlmm_GpioConfigIdType Settings;
  DALGpioValueType eOutputDrive;
}DalTlmm_GpioIdSettingsType;
# 4 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h" 1


# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long long uint64;




typedef long long int64;




typedef unsigned char byte;



typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 4 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/PlatformInfoDefs.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/PlatformInfoDefs.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 144 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 29 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/PlatformInfoDefs.h" 2
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/PlatformInfoDefs.h"
typedef enum
{
  DALPLATFORMINFO_TYPE_UNKNOWN = 0x00,
  DALPLATFORMINFO_TYPE_SURF = 0x01,
  DALPLATFORMINFO_TYPE_FFA = 0x02,
  DALPLATFORMINFO_TYPE_FLUID = 0x03,
  DALPLATFORMINFO_TYPE_FUSION = 0x04,
  DALPLATFORMINFO_TYPE_OEM = 0x05,
  DALPLATFORMINFO_TYPE_QT = 0x06,
  DALPLATFORMINFO_TYPE_CDP = DALPLATFORMINFO_TYPE_SURF,

  DALPLATFORMINFO_TYPE_MTP_MDM = 0x07,
  DALPLATFORMINFO_TYPE_MTP_MSM = 0x08,
  DALPLATFORMINFO_TYPE_MTP = DALPLATFORMINFO_TYPE_MTP_MSM,

  DALPLATFORMINFO_TYPE_LIQUID = 0x09,
  DALPLATFORMINFO_TYPE_DRAGONBOARD = 0x0A,
  DALPLATFORMINFO_TYPE_QRD = 0x0B,
  DALPLATFORMINFO_TYPE_EVB = 0x0C,
  DALPLATFORMINFO_TYPE_HRD = 0x0D,
  DALPLATFORMINFO_TYPE_DTV = 0x0E,
  DALPLATFORMINFO_TYPE_RUMI = 0x0F,
  DALPLATFORMINFO_TYPE_VIRTIO = 0x10,
  DALPLATFORMINFO_TYPE_GOBI = 0x11,
  DALPLATFORMINFO_TYPE_CBH = 0x12,
  DALPLATFORMINFO_TYPE_BTS = 0x13,
  DALPLATFORMINFO_TYPE_XPM = 0x14,
  DALPLATFORMINFO_TYPE_RCM = 0x15,
  DALPLATFORMINFO_TYPE_DMA = 0x16,
  DALPLATFORMINFO_TYPE_STP = 0x17,
  DALPLATFORMINFO_TYPE_SBC = 0x18,
  DALPLATFORMINFO_TYPE_ADP = 0x19,

  DALPLATFORMINFO_NUM_TYPES = 0x20,


  DALPLATFORMINFO_TYPE_32BITS = 0x7FFFFFFF

} DalPlatformInfoPlatformType;





typedef enum
{
  DALPLATFORMINFO_KEY_UNKNOWN = 0x00,
  DALPLATFORMINFO_KEY_DDR_FREQ = 0x01,
  DALPLATFORMINFO_KEY_GFX_FREQ = 0x02,
  DALPLATFORMINFO_KEY_CAMERA_FREQ = 0x03,
  DALPLATFORMINFO_KEY_FUSION = 0x04,
  DALPLATFORMINFO_KEY_CUST = 0x05,

  DALPLATFORMINFO_NUM_KEYS = 0x06,


  DALPLATFORMINFO_KEY_32BITS = 0x7FFFFFFF

} DalPlatformInfoKeyType;







typedef enum
{
  DALPLATFORMINFO_SUBTYPE_UNKNOWN = 0x00,
  DALPLATFORMINFO_SUBTYPE_CSFB = 0x01,
  DALPLATFORMINFO_SUBTYPE_SVLTE1 = 0x02,
  DALPLATFORMINFO_SUBTYPE_SVLTE2A = 0x03,
  DALPLATFORMINFO_SUBTYPE_SVLTE2B = 0x04,
  DALPLATFORMINFO_SUBTYPE_SVLTE2C = 0x05,
  DALPLATFORMINFO_SUBTYPE_SGLTE = 0x06,

  DALPLATFORMINFO_NUM_SUBTYPES = 0x07,


  DALPLATFORMINFO_SUBTYPE_32BITS = 0x7FFFFFFF

} DalPlatformInfoPlatformSubtype;






typedef struct
{
  DalPlatformInfoPlatformType platform;
  uint32 version;
  uint32 subtype;
  boolean fusion;
} DalPlatformInfoPlatformInfoType;
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/PlatformInfoDefs.h"
typedef struct DalPlatformInfoSMemPMICType
{
  uint32 nPMICModel;
  uint32 nPMICVersion;
} DalPlatformInfoSMemPMICType;






typedef struct
{
  uint32 nFormat;
  uint32 eChipId;
  uint32 nChipVersion;
  char aBuildId[32];

  uint32 nRawChipId;
  uint32 nRawChipVersion;
  DalPlatformInfoPlatformType ePlatformType;
  uint32 nPlatformVersion;
  uint32 bFusion;
  uint32 nPlatformSubtype;
  DalPlatformInfoSMemPMICType aPMICInfo[3];

} DalPlatformInfoSMemType;
# 5 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h" 2
# 121 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h"
typedef struct
{
   uint32 nGpioNumber;
   uint32 nFunctionSelect;

}TLMMGpioIdType;
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h"
typedef struct
{
  DalPlatformInfoPlatformInfoType Platform;
  uint32 nKVPS;
  uint32 nKVPSType;
  const char* pszPlatform;
  const char* pszPlatConfigs;

}TLMMPlatformMapType;
# 160 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/DALTLMMPropDef.h"
typedef struct {
  uint32 nDir;
  uint32 nPull;
  uint32 nDrive;
  uint32 nOutput;
}TLMMGpioIdCfgType;
# 5 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_CDP.xml" 1
  <device id="/tlmm/cdp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>
# 7 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_MTP.xml" 1
  <device id="/tlmm/mtp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
# 8 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_FLUID.xml" 1
  <device id="/tlmm/fluid1">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
# 9 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_LiQUID.xml" 1
  <device id="/tlmm/liquid">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="epm_int_n" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="hdmi_mux_sel" type="TLMMGpioIdType">{25, 0}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="gesture_reset_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="gp_pdm_2a" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="epm_marker11" type="TLMMGpioIdType">{91, 0}</props>
    <props name="epm_marker2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="epm_global_en" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="hdmi_mux_en" type="TLMMGpioIdType">{96, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
# 10 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_FUSION.xml" 1
  <device id="/tlmm/fusion">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clk" type="TLMMGpioIdType">{32, 255}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreq_n" type="TLMMGpioIdType">{36, 255}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="slimbus_clk" type="TLMMGpioIdType">{70, 255}</props>
    <props name="slimbus_data0" type="TLMMGpioIdType">{71, 255}</props>
    <props name="slimbus_data1" type="TLMMGpioIdType">{72, 255}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="wlan_en" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_ir1_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{126, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreq_n" type="TLMMGpioIdType">{131, 255}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>
# 11 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/PlatformIO_DB.xml" 1
  <device id="/tlmm/dragonboard">
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="disp_gpio3" type="TLMMGpioIdType">{9, 255}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="cam_mclk3" type="TLMMGpioIdType">{16, 1}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="cam_irq" type="TLMMGpioIdType">{24, 255}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam0_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam0_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="edu_gp" type="TLMMGpioIdType">{39, 255}</props>
    <props name="edu_irq" type="TLMMGpioIdType">{40, 255}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_spi_mosi[3]" type="TLMMGpioIdType">{45, 1}</props>
    <props name="blsp_spi_miso[3]" type="TLMMGpioIdType">{46, 1}</props>
    <props name="blsp_spi_cs_n[3]" type="TLMMGpioIdType">{47, 1}</props>
    <props name="blsp_spi_clk[3]" type="TLMMGpioIdType">{48, 1}</props>
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{49, 255}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{50, 255}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{51, 255}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{52, 255}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="qua_mi2s_mclk" type="TLMMGpioIdType">{57, 1}</props>
    <props name="qua_mi2s_sck" type="TLMMGpioIdType">{58, 1}</props>
    <props name="qua_mi2s_ws" type="TLMMGpioIdType">{59, 1}</props>
    <props name="qua_mi2s_data0" type="TLMMGpioIdType">{60, 1}</props>
    <props name="qua_mi2s_data1" type="TLMMGpioIdType">{61, 1}</props>
    <props name="qua_mi2s_data2" type="TLMMGpioIdType">{62, 1}</props>
    <props name="qua_mi2s_data3" type="TLMMGpioIdType">{63, 1}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 0}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 0}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 0}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 0}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="ts1_reset_n" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="disp_gpio2" type="TLMMGpioIdType">{81, 255}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_spi_miso[12]" type="TLMMGpioIdType">{86, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts0_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{98, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{101, 0}</props>
    <props name="pcie_slt_prsnt_n" type="TLMMGpioIdType">{102, 255}</props>
    <props name="usb_hub_reset_n" type="TLMMGpioIdType">{103, 255}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{104, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{105, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{106, 0}</props>
    <props name="disp_gpio1" type="TLMMGpioIdType">{107, 255}</props>
    <props name="ts_int_1_n" type="TLMMGpioIdType">{108, 255}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 255}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 255}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 255}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 255}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 255}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 255}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 255}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 255}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 255}</props>
    <props name="edu_gpio1" type="TLMMGpioIdType">{127, 255}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{133, 0}</props>
    <props name="edu_gpio3" type="TLMMGpioIdType">{134, 255}</props>
    <props name="bl0_en" type="TLMMGpioIdType">{135, 0}</props>
    <props name="qspi_cs_n_1" type="TLMMGpioIdType">{138, 2}</props>
    <props name="ssbi2" type="TLMMGpioIdType">{139, 2}</props>
    <props name="ssbi1" type="TLMMGpioIdType">{140, 2}</props>
    <props name="qspi_cs_n_0" type="TLMMGpioIdType">{141, 2}</props>
    <props name="qspi_reset_n" type="TLMMGpioIdType">{142, 0}</props>
    <props name="qspi_clk" type="TLMMGpioIdType">{145, 2}</props>
    <props name="qspi_data[0]" type="TLMMGpioIdType">{146, 2}</props>
    <props name="qspi_data[1]" type="TLMMGpioIdType">{147, 2}</props>
    <props name="qspi_data[2]" type="TLMMGpioIdType">{148, 2}</props>
    <props name="qspi_data[3]" type="TLMMGpioIdType">{149, 2}</props>
  </device>
# 12 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/tlmm/config/msm8996/TLMMChipset.xml" 2
 <global_def>
     <var_seq name="tlmm_port_cfg" type=DALPROP_DATA_TYPE_UINT32_SEQ>
      0xFFFFFFFF, 0, 0, 0, 0,
      end
     </var_seq>
   </global_def>
   <device id=DALDEVICEID_TLMM>
     <props name="tlmm_base" type=DALPROP_ATTR_TYPE_UINT32>
      0x01000000
     </props>
     <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>
      0x00010000
     </props>
     <props name="tlmm_total_gpio" type=DALPROP_ATTR_TYPE_UINT32>
      150
     </props>
     <props name="tlmm_ports" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
      tlmm_port_cfg
     </props>
     <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32>
      0x1
     </props>
    <props name="TLMMDefaultPlatformGroup" type="TLMMPlatformMapType">
      {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 0}, 0, 0, "/tlmm/mtp001", "/tlmm/mtpcfgs"}
    </props>
    <props name="TlmmPlatformGroups" type="TLMMPlatformMapType" array="True">
      {{{DALPLATFORMINFO_TYPE_LIQUID, 1, 0, 0}, 0, 0, "/tlmm/liquid", "/tlmm/liquidcfgs"},
       {{DALPLATFORMINFO_TYPE_CDP, 1, 0, 0}, 0, 0, "/tlmm/cdp001", "/tlmm/cdpcfgs"},
       {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 0}, 0, 0, "/tlmm/mtp001", "/tlmm/mtpcfgs"},
       {{DALPLATFORMINFO_TYPE_FLUID, 1, 0, 0}, 0, 0, "/tlmm/fluid1", "/tlmm/fluid1cfgs"},
       {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 1}, 0, 0, "/tlmm/fusion", "/tlmm/fusioncfgs"},
       {{DALPLATFORMINFO_TYPE_DRAGONBOARD, 1, 0, 0}, 0, 0, "/tlmm/dragonboard", "/tlmm/dbcgs"}}
    </props>
   </device>
</driver>
