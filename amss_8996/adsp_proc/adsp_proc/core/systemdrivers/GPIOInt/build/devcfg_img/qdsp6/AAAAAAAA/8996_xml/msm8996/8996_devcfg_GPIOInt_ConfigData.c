#include "../msm8996/devcfg_GPIOInt.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef uint32 GPIOINTISRCtx;
typedef void * (*GPIOINTISR)(GPIOINTISRCtx);
typedef enum{
  GPIOINT_DEVICE_MODEM,
  GPIOINT_DEVICE_SPS,
  GPIOINT_DEVICE_LPA_DSP,
  GPIOINT_DEVICE_RPM,
  GPIOINT_DEVICE_APPS,
  GPIOINT_DEVICE_WCN,
  GPIOINT_DEVICE_DSP,
  GPIOINT_DEVICE_NONE,
  PLACEHOLDER_GPIOIntProcessorType = 0x7fffffff
}GPIOIntProcessorType;
typedef enum{
  GPIOINT_TRIGGER_HIGH,
  GPIOINT_TRIGGER_LOW,
  GPIOINT_TRIGGER_RISING,
  GPIOINT_TRIGGER_FALLING,
  GPIOINT_TRIGGER_DUAL_EDGE,
  PLACEHOLDER_GPIOIntTriggerType = 0x7fffffff
}GPIOIntTriggerType;
typedef struct GPIOInt GPIOInt;
struct GPIOInt
{
   struct DalDevice DalDevice;
   DALResult (*SetTrigger)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger);
   DALResult (*RegisterIsr)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger,
                              DALSYSEventHandle event);
   DALResult (*DeRegisterEvent)(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event);
   DALResult (*DeregisterIsr)(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr);
   DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*IsInterruptPending)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*Save)(DalDeviceHandle * _h);
   DALResult (*Restore)(DalDeviceHandle * _h);
   DALResult (*DisableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*InterruptNotify)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*MonitorInterrupts)(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable);
   DALResult (*MapMPMInterrupt)(DalDeviceHandle * h, uint32 gpio, uint32 mpm_interrupt_id);
   DALResult (*AttachRemote)(DalDeviceHandle * h, uint32 processor);
   DALResult (*TriggerInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*ClearInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*IsInterruptSet)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*SetDirectConnectGPIOMapping)(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line);
   DALResult (*IsInterruptRegistered)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*DisableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*GPIOInt_RegisterIsrEx)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags);
};
typedef struct GPIOIntHandle GPIOIntHandle;
struct GPIOIntHandle
{
   uint32 dwDalHandleId;
   const GPIOInt * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
GPIOInt_SetTrigger(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->SetTrigger)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, trigger);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->SetTrigger( _h, gpio, trigger);
}
static __inline DALResult
GPIOInt_RegisterIsr(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterIsr( _h, gpio, trigger,isr,param);
   }
   else
       return -1;
}
static __inline DALResult
GPIOInt_RegisterIsrEx(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->GPIOInt_RegisterIsrEx( _h, gpio, trigger,isr,param, nFlags);
   }
   else
     return -1;
}
static __inline DALResult
GPIOInt_RegisterEvent(DalDeviceHandle * _h, uint32 gpio,
                                         GPIOIntTriggerType trigger,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterEvent( _h, gpio, trigger, event);
   }
   else
       return -1;
}
static __inline DALResult
GPIOInt_DeRegisterEvent(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->DeRegisterEvent( _h, gpio,event);
   }
   else
     return -1;
}
static __inline DALResult
GPIOInt_DeregisterIsr(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->DeregisterIsr( _h, gpio, isr);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_IsInterruptEnabled(DalDeviceHandle * _h, uint32 gpio, uint32* state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptEnabled)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptEnabled( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_IsInterruptPending(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptPending)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptPending( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_IsInterruptSet(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptSet)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptSet( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_Save(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Save( _h);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_Restore(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Restore( _h);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_DisableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->DisableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->DisableInterrupt( _h, gpio);
}
static __inline DALResult
GPIOInt_EnableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->EnableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->EnableInterrupt( _h, gpio);
}
static __inline DALResult
GPIOInt_InterruptNotify(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->InterruptNotify)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->InterruptNotify( _h, gpio);
}
static __inline DALResult
GPIOInt_MonitorInterrupts(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable)
{
  if(!(((DALHandle)_h) & 0x00000001))
   {
   return ((GPIOIntHandle *)_h)->pVtbl->MonitorInterrupts( _h,isr,enable);
}
  else
    return -1;
}
static __inline DALResult
GPIOInt_MapMPMInterrupt(DalDeviceHandle * _h, uint32 gpio, uint32 mpm_interrupt_id)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->MapMPMInterrupt( _h, gpio,mpm_interrupt_id);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->AttachRemote( _h, processor);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_TriggerInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->TriggerInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }
}
void GPIOInt_Init(void);
static __inline DALResult
GPIOInt_ClearInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->ClearInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_SetDirectConnectGPIOMapping(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->SetDirectConnectGPIOMapping( _h, gpio, direct_connect_line);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_IsInterruptRegistered(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptRegistered)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptRegistered( _h, gpio, (void *)state, 1);
}
typedef unsigned char boolean;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef unsigned long int bool32;
enum
{
  HAL_GPIOINT_0 = 0,
  HAL_GPIOINT_1,
  HAL_GPIOINT_2,
  HAL_GPIOINT_3,
  HAL_GPIOINT_4,
  HAL_GPIOINT_5,
  HAL_GPIOINT_6,
  HAL_GPIOINT_7,
  HAL_GPIOINT_8,
  HAL_GPIOINT_9,
  HAL_GPIOINT_10,
  HAL_GPIOINT_11,
  HAL_GPIOINT_12,
  HAL_GPIOINT_13,
  HAL_GPIOINT_14,
  HAL_GPIOINT_15,
  HAL_GPIOINT_16,
  HAL_GPIOINT_17,
  HAL_GPIOINT_18,
  HAL_GPIOINT_19,
  HAL_GPIOINT_20,
  HAL_GPIOINT_21,
  HAL_GPIOINT_22,
  HAL_GPIOINT_23,
  HAL_GPIOINT_24,
  HAL_GPIOINT_25,
  HAL_GPIOINT_26,
  HAL_GPIOINT_27,
  HAL_GPIOINT_28,
  HAL_GPIOINT_29,
  HAL_GPIOINT_30,
  HAL_GPIOINT_31,
  HAL_GPIOINT_32,
  HAL_GPIOINT_33,
  HAL_GPIOINT_34,
  HAL_GPIOINT_35,
  HAL_GPIOINT_36,
  HAL_GPIOINT_37,
  HAL_GPIOINT_38,
  HAL_GPIOINT_39,
  HAL_GPIOINT_40,
  HAL_GPIOINT_41,
  HAL_GPIOINT_42,
  HAL_GPIOINT_43,
  HAL_GPIOINT_44,
  HAL_GPIOINT_45,
  HAL_GPIOINT_46,
  HAL_GPIOINT_47,
  HAL_GPIOINT_48,
  HAL_GPIOINT_49,
  HAL_GPIOINT_50,
  HAL_GPIOINT_51,
  HAL_GPIOINT_52,
  HAL_GPIOINT_53,
  HAL_GPIOINT_54,
  HAL_GPIOINT_55,
  HAL_GPIOINT_56,
  HAL_GPIOINT_57,
  HAL_GPIOINT_58,
  HAL_GPIOINT_59,
  HAL_GPIOINT_60,
  HAL_GPIOINT_61,
  HAL_GPIOINT_62,
  HAL_GPIOINT_63,
  HAL_GPIOINT_64,
  HAL_GPIOINT_65,
  HAL_GPIOINT_66,
  HAL_GPIOINT_67,
  HAL_GPIOINT_68,
  HAL_GPIOINT_69,
  HAL_GPIOINT_70,
  HAL_GPIOINT_71,
  HAL_GPIOINT_72,
  HAL_GPIOINT_73,
  HAL_GPIOINT_74,
  HAL_GPIOINT_75,
  HAL_GPIOINT_76,
  HAL_GPIOINT_77,
  HAL_GPIOINT_78,
  HAL_GPIOINT_79,
  HAL_GPIOINT_80,
  HAL_GPIOINT_81,
  HAL_GPIOINT_82,
  HAL_GPIOINT_83,
  HAL_GPIOINT_84,
  HAL_GPIOINT_85,
  HAL_GPIOINT_86,
  HAL_GPIOINT_87,
  HAL_GPIOINT_88,
  HAL_GPIOINT_89,
  HAL_GPIOINT_90,
  HAL_GPIOINT_91,
  HAL_GPIOINT_92,
  HAL_GPIOINT_93,
  HAL_GPIOINT_94,
  HAL_GPIOINT_95,
  HAL_GPIOINT_96,
  HAL_GPIOINT_97,
  HAL_GPIOINT_98,
  HAL_GPIOINT_99,
  HAL_GPIOINT_100,
  HAL_GPIOINT_101,
  HAL_GPIOINT_102,
  HAL_GPIOINT_103,
  HAL_GPIOINT_104,
  HAL_GPIOINT_105,
  HAL_GPIOINT_106,
  HAL_GPIOINT_107,
  HAL_GPIOINT_108,
  HAL_GPIOINT_109,
  HAL_GPIOINT_110,
  HAL_GPIOINT_111,
  HAL_GPIOINT_112,
  HAL_GPIOINT_113,
  HAL_GPIOINT_114,
  HAL_GPIOINT_115,
  HAL_GPIOINT_116,
  HAL_GPIOINT_117,
  HAL_GPIOINT_118,
  HAL_GPIOINT_119,
  HAL_GPIOINT_120,
  HAL_GPIOINT_121,
  HAL_GPIOINT_122,
  HAL_GPIOINT_123,
  HAL_GPIOINT_124,
  HAL_GPIOINT_125,
  HAL_GPIOINT_126,
  HAL_GPIOINT_127,
  HAL_GPIOINT_128,
  HAL_GPIOINT_129,
  HAL_GPIOINT_130,
  HAL_GPIOINT_131,
  HAL_GPIOINT_132,
  HAL_GPIOINT_133,
  HAL_GPIOINT_134,
  HAL_GPIOINT_135,
  HAL_GPIOINT_136,
  HAL_GPIOINT_137,
  HAL_GPIOINT_138,
  HAL_GPIOINT_139,
  HAL_GPIOINT_140,
  HAL_GPIOINT_141,
  HAL_GPIOINT_142,
  HAL_GPIOINT_143,
  HAL_GPIOINT_144,
  HAL_GPIOINT_145,
  HAL_GPIOINT_146,
  HAL_GPIOINT_147,
  HAL_GPIOINT_148,
  HAL_GPIOINT_149,
  HAL_GPIOINT_150,
  HAL_GPIOINT_151,
  HAL_GPIOINT_152,
  HAL_GPIOINT_153,
  HAL_GPIOINT_154,
  HAL_GPIOINT_155,
  HAL_GPIOINT_156,
  HAL_GPIOINT_157,
  HAL_GPIOINT_158,
  HAL_GPIOINT_159,
  HAL_GPIOINT_160,
  HAL_GPIOINT_161,
  HAL_GPIOINT_162,
  HAL_GPIOINT_163,
  HAL_GPIOINT_164,
  HAL_GPIOINT_165,
  HAL_GPIOINT_166,
  HAL_GPIOINT_167,
  HAL_GPIOINT_168,
  HAL_GPIOINT_169,
  HAL_GPIOINT_170,
  HAL_GPIOINT_171,
  HAL_GPIOINT_172,
  HAL_GPIOINT_173,
  HAL_GPIOINT_174,
  HAL_GPIOINT_175,
  HAL_GPIOINT_176,
  HAL_GPIOINT_177,
  HAL_GPIOINT_178,
  HAL_GPIOINT_179,
  HAL_GPIOINT_180,
  HAL_GPIOINT_181,
  HAL_GPIOINT_NUM
};
typedef enum
{
  HAL_GPIOINT_DIRECTCONNECT_0 = 0,
  HAL_GPIOINT_DIRECTCONNECT_1 = 1,
  HAL_GPIOINT_DIRECTCONNECT_2 = 2,
  HAL_GPIOINT_DIRECTCONNECT_3 = 3,
  HAL_GPIOINT_DIRECTCONNECT_4 = 4,
  HAL_GPIOINT_DIRECTCONNECT_5 = 5,
  HAL_GPIOINT_DIRECTCONNECT_6 = 6,
  HAL_GPIOINT_DIRECTCONNECT_7 = 7,
  HAL_GPIOINT_DIRECTCONNECT_8 = 8,
  HAL_GPIOINT_DIRECTCONNECT_9 = 9,
  HAL_GPIOINT_DIRECTCONNECT_10 = 10,
  HAL_GPIOINT_DIRECTCONNECT_11 = 11,
  HAL_GPIOINT_DIRECTCONNECT_12 = 12,
  HAL_GPIOINT_DIRECTCONNECT_13 = 13,
  HAL_GPIOINT_DIRECTCONNECT_14 = 14,
  HAL_GPIOINT_DIRECTCONNECT_15 = 15,
  HAL_GPIOINT_SPECIALCONN = 0x7E,
  HAL_GPIOINT_SUMMARY = 0x7F
} HAL_gpioint_InterruptType;
typedef enum
{
  HAL_GPIOINT_TRIGGER_HIGH = 0,
  HAL_GPIOINT_TRIGGER_LOW = 1,
  HAL_GPIOINT_TRIGGER_RISING = 2,
  HAL_GPIOINT_TRIGGER_FALLING = 3,
  HAL_GPIOINT_TRIGGER_DUAL_EDGE = 4
} HAL_gpioint_TriggerType;
typedef enum
{
  HAL_GPIOINT_MSS_PROC,
  HAL_GPIOINT_SPS_PROC,
  HAL_GPIOINT_LPA_DSP_PROC,
  HAL_GPIOINT_RPM_PROC,
  HAL_GPIOINT_SC_PROC,
  HAL_GPIOINT_WCN_PROC,
  HAL_GPIOINT_DEVICE_DSP,
  HAL_GPIOINT_NONE_PROC
} HAL_gpioint_ProcessorType;
typedef enum
{
  HAL_GPIOINT_GROUP1 = 0,
  HAL_GPIOINT_GROUP2 = 1,
} HAL_gpioint_GroupType;
typedef struct
{
  void * pTargetData;
} HAL_gpioint_ContextType;
typedef struct
{
  HAL_gpioint_ProcessorType eProcessor;
  uint32* nGPIOIntBase;
  uint32* nGPIOIntBasePhys;
  uint32 nHalTargetProcVersion;
}HAL_gpioint_TargetDataType;
void HAL_gpioint_Init ( HAL_gpioint_ContextType * pTarget );
void HAL_gpioint_Reset ( void );
void HAL_gpioint_Save ( void );
void HAL_gpioint_Restore ( void );
void HAL_gpioint_Enable ( uint32 nGPIO );
void HAL_gpioint_Disable ( uint32 nGPIO );
void HAL_gpioint_Clear ( uint32 nGPIO );
void HAL_gpioint_EnableRawStatus ( uint32 nGPIO );
void HAL_gpioint_GetPending ( HAL_gpioint_GroupType eGroup, uint32 *pnGPIO );
void HAL_gpioint_GetNumber ( uint32 *pnNumber );
void HAL_gpioint_GetTrigger ( uint32 nGPIO, HAL_gpioint_TriggerType *peTrigger );
void HAL_gpioint_GetGroup ( uint32 nGPIO, HAL_gpioint_GroupType *peGroup);
void HAL_gpioint_SetTrigger ( uint32 nGPIO, HAL_gpioint_TriggerType eTrigger );
boolean HAL_gpioint_IsSupported ( uint32 nGPIO );
boolean HAL_gpioint_IsPending ( uint32 nGPIO );
boolean HAL_gpioint_IsEnabled ( uint32 nGPIO );
boolean HAL_gpioint_IsSet ( uint32 nGPIO);
void HAL_gpioint_SetTargetProcessor ( HAL_gpioint_ProcessorType eProcId, uint32 nGpio, HAL_gpioint_InterruptType eInterrupt );
void HAL_gpioint_SetDirConnIntrPolarity ( HAL_gpioint_TriggerType eTrigger, uint32 nGPIO );
void HAL_gpioint_SetDirConnIntr ( uint32 nGPIO, HAL_gpioint_InterruptType eDirConnIntr );
void HAL_gpioint_DisableDirConnIntr ( uint32 nGPIO );
void HAL_gpioint_GetTargetProcessor ( uint32 nGpio,HAL_gpioint_ProcessorType *peProcId);
void HAL_gpioint_TriggerInterrupt ( uint32 nGPIO);
typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;
struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};
struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};
struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};
struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};
typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};
typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};
typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);
DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);
uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);
uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);
void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);
void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);
DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);
DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);
DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);
DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);
DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);
void memory_barrier(void);
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);
DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);
uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;
void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);
void DALFW_MPUnLock(DALFW_MPLOCK *pLock);
uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
typedef struct _DAL_LOCK
{
   uint32 lock;
   uint32 reserved;
   uint32 type;
   uint32 version;
}
DAL_LOCK;
typedef struct _DALGLB_HEADER
{
   uint32 size;
   char name[12];
   DAL_LOCK lock;
}
DALGLB_HEADER;
DALResult
DALGLBCTXT_Init(void);
DALResult
DALGLBCTXT_AllocCtxt(const char * name, uint32 size_req, uint32 unused_param,
      void ** ctxt);
DALResult
DALGLBCTXT_FindCtxt(const char * name, void ** ctxt);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef struct GPIOIntDrvCtxt GPIOIntDrvCtxt;
typedef struct GPIOIntDevCtxt GPIOIntDevCtxt;
typedef struct GPIOIntClientCtxt GPIOIntClientCtxt;
typedef enum{
  GPIOINT_PLATFORM_L4,
  GPIOINT_PLATFORM_WM,
  GPIOINT_PLATFORM_SYMBIAN,
  GPIOINT_PLATFORM_LINUX,
  GPIOINT_PLATFORM_BLAST,
  GPIOINT_PLATFORM_NONE,
  PLACEHOLDER_GPIOIntPlatformType = 0x7fffffff
}GPIOIntPlatformType;
typedef struct
{
  GPIOINTISR isr;
  GPIOINTISRCtx isr_param;
  DALSYSEventHandle event;
  uint32 cnt;
  uint32 last;
  HAL_gpioint_InterruptType intr_type;
  HAL_gpioint_TriggerType intr_trigger;
  uint32 mpm_intr_id;
  uint16 qurt_intr_id;
  uint16 gpio_intr_flags;
} GPIOIntDataType;
typedef struct
{
  uint32 gpio;
  HAL_gpioint_InterruptType interrupt;
  uint32 interrupt_id;
  uint32 flags;
} GPIOIntConfigMapType;
typedef struct
{
  uint32 gpio;
  uint64 timestamp;
} GPIOIntLogEntryType;
typedef struct
{
  uint32 index;
  GPIOIntLogEntryType entries[100];
} GPIOIntLogType;
typedef struct
{
  DALGLB_HEADER hdr;
  uint32 gpioint_reg_state[1];
}GPIOIntGlbCtxtType;
typedef struct
{
  uint32 gpio;
 GPIOIntDevCtxt *device;
} GPIOIntIsrDataType;
typedef struct
{
  uint8 GPIOInt_Init;
  GPIOIntDataType state[150];
  GPIOIntLogType log;
  uint32 gpio_number;
  GPIOIntGlbCtxtType *gpioint_glb_ctxt;
  uint32 gpioint_unhandled;
  uint32 direct_intr_number;
  HAL_gpioint_ProcessorType processor;
  DALSYSEventHandle summary_intr_event;
  DALSYSEventHandle default_event;
  uint32 default_param;
  uint32 summary_param;
  uint32 summary_intr_id;
  npa_client_handle npa_client;
  uint32 non_mpm_interrupts;
  GPIOINTISR wakeup_isr;
  GPIOIntConfigMapType *gpioint_config_map;
  uint32* gpioint_physical_address;
  uint32* gpioint_virtual_address;
  uint8 interrupt_controller;
  uint8 gpioint_uimage;
  uint8 is_initialized;
} GPIOIntCntrlType;
typedef struct GPIOIntDALVtbl GPIOIntDALVtbl;
struct GPIOIntDALVtbl
{
 int (*GPIOIntr_DriverInit)(GPIOIntDrvCtxt *);
 int (*GPIOIntr_DriverDeInit)(GPIOIntDrvCtxt *);
};
struct GPIOIntDevCtxt
{
  uint32 dwRefs;
  DALDEVICEID DevId;
  uint32 dwDevCtxtRefIdx;
  GPIOIntDrvCtxt *pGPIOIntDrvCtxt;
  uint32 hProp[2];
  uint32 Reserved[16];
  DALSYSSyncObj GPIOIntSyncObject;
  DALSYSSyncHandle gpioint_synchronization;
  DalDeviceHandle * tlmm_handle;
  DalDeviceHandle * intr_handle;
  DalDeviceHandle * hwio_handle;
  DalDeviceHandle * timetick_handle;
  DalDeviceHandle * hGPIOMgr;
  DalDeviceHandle * hDalMpmHandle;
  GPIOIntCntrlType * gpioint_cntrl;
  int gpioint_qdi;
};
struct GPIOIntDrvCtxt
{
  GPIOIntDALVtbl GPIOIntDALVtbl;
  uint32 dwNumDev;
  uint32 dwSizeDevCtxt;
  uint32 bInit;
  uint32 dwRefs;
  GPIOIntDevCtxt GPIOIntDevCtxt[2];
};
struct GPIOIntClientCtxt
{
  uint32 dwRefs;
  uint32 dwAccessMode;
  void *pPortCtxt;
  GPIOIntDevCtxt *pGPIOIntDevCtxt;
  GPIOIntHandle GPIOIntHandle;
  uint8 is_initialized;
};
DALResult GPIOIntr_DriverInit(GPIOIntDrvCtxt *);
DALResult GPIOIntr_DriverDeInit(GPIOIntDrvCtxt *);
DALResult GPIOIntr_DeviceInit(GPIOIntClientCtxt *);
DALResult GPIOIntr_DeviceDeInit(GPIOIntClientCtxt *);
DALResult GPIOIntr_Reset(GPIOIntClientCtxt *);
DALResult GPIOIntr_PowerEvent(GPIOIntClientCtxt *,DalPowerCmd, DalPowerDomain);
DALResult GPIOIntr_Open(GPIOIntClientCtxt *,uint32);
DALResult GPIOIntr_Close(GPIOIntClientCtxt *);
DALResult GPIOIntr_Info(GPIOIntClientCtxt *, DalDeviceInfo *, uint32);
DALResult GPIOIntr_InheritObjects(GPIOIntClientCtxt *,DALInheritSrcPram *,DALInheritDestPram *);
DALResult
GPIOIntr_GetTargetData
(
  GPIOIntClientCtxt *pCtxt
);
DALResult
GPIOIntr_AttachToDals
(
  GPIOIntDevCtxt *device
);
void
GPIOIntr_Isr
(
  void *pCtxt
);
void
GPIOIntr_RunIsr
(
  GPIOIntDevCtxt *device,
  uint32 gpio
);
void
GPIOIntr_LogEvent
(
  GPIOIntDevCtxt *device,
  uint32 gpio
);
uint32
GPIOIntr_GetFreeDirectConnectInterruptId
(
  GPIOIntDevCtxt * device,
  uint32 gpio
);
uint32
GPIOIntr_GetInterruptId
(
  GPIOIntDevCtxt *device,
  uint32 gpio
);
uint32
GPIOIntr_GetDirectHWInterruptId
(
  GPIOIntDevCtxt *device,
  uint32 gpio
);
void
GPIOIntr_ConfigMPMWakeup
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  boolean bEnable
);
DALResult
GPIOIntr_ClearInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult GPIOIntr_SetTrigger
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  GPIOIntTriggerType trigger
);
DALResult GPIOIntr_RegisterIsr
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  GPIOIntTriggerType trigger,
  GPIOINTISR isr,
  GPIOINTISRCtx param
);
DALResult GPIOIntr_DeregisterIsr
( GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  GPIOINTISR isr
);
DALResult GPIOIntr_RegisterEvent
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  GPIOIntTriggerType trigger,
  DALSYSEventHandle event
) ;
DALResult GPIOIntr_DeRegisterEvent
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  DALSYSEventHandle event
);
DALResult
GPIOIntr_IsInterruptEnabled
(
  GPIOIntClientCtxt *pCtxt,
  uint32 gpio,
  uint32 * state
);
DALResult
GPIOIntr_IsInterruptRegistered
(
  GPIOIntClientCtxt *pCtxt,
  uint32 gpio,
  uint32 * state
);
DALResult
GPIOIntr_IsInterruptPending
(
  GPIOIntClientCtxt *pCtxt,
  uint32 gpio,
  uint32 * state
);
DALResult
GPIOIntr_IsInterruptSet
(
  GPIOIntClientCtxt *pCtxt,
  uint32 gpio,
  uint32 * state
);
DALResult
GPIOIntr_Save
(
  GPIOIntClientCtxt * pCtxt
);
DALResult GPIOIntr_Restore
(
  GPIOIntClientCtxt *
);
DALResult GPIOIntr_DisableInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult GPIOIntr_EnableInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult GPIOIntr_DisableGPIOInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult GPIOIntr_EnableGPIOInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult
GPIOIntr_TriggerInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio
);
DALResult GPIOIntr_MapMPMInterrupt
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  uint32 mpm_interrupt_id
);
DALResult GPIOIntr_AttachRemote
(
  GPIOIntClientCtxt* pCtxt,
  uint32 processor
);
DALResult
GPIOIntr_SetDirectConnectGPIOMapping
(
  GPIOIntClientCtxt * pCtxt,
  uint32 gpio,
  uint32 direct_connect_interrupt
);
DALResult
GPIOIntr_MapGPIOIntTrigger
(
  uint32 nGPIOTrigger,
  uint32* nIntCTrigger
);
GPIOIntConfigMapType interrupt_config_map[] =
{
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_0, 24,0x0010},
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_1, 25,0x0010},
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_2, 26,0},
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_3, 27,0},
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_4, 28,0},
  {0xFFFF, HAL_GPIOINT_DIRECTCONNECT_5, 29,0},
};
