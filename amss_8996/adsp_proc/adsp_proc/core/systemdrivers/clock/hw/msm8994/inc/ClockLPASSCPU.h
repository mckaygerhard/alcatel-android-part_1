#ifndef __CLOCKCPUBSP_H__
#define __CLOCKCPUBSP_H__

/*=========================================================================

                    C L O C K   D E V I C E   D R I V E R
               
              C P U   B O A R D   S U P P O R T   P A C K A G E

GENERAL DESCRIPTION
  This file contains the BSP interface definitions for the
  MSM DAL Clock Device Driver CPU data structures.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

      Copyright (c) 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.

==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //components/rel/core.adsp/2.7/systemdrivers/clock/hw/msm8994/inc/ClockLPASSCPU.h#3 $

when       who     what, where, why 
--------   ---     --------------------------------------------------------- 
11/07/12   dcf     Initial release.

==========================================================================*/ 

/*=========================================================================
      Includes
==========================================================================*/

#include "npa_remote_resource.h"


/*=========================================================================
      Constants, Macros & Typedefs
==========================================================================*/


/*
 * Define aliases for the NPA resource nodes here as they may be 
 * used in multiple places within the clock code. 
*/
#define CLOCK_NPA_RESOURCE_QDSS "/clk/qdss"
#define CLOCK_NPA_RESOURCE_PNOC "/clk/pnoc"
#define CLOCK_NPA_RESOURCE_SNOC "/clk/snoc"
#define CLOCK_NPA_RESOURCE_CNOC "/clk/cnoc"
#define CLOCK_NPA_RESOURCE_BIMC "/clk/bimc"
#define CLOCK_NPA_RESOURCE_OCMEM "/clk/ocmem"
#define CLOCK_NPA_RESOURCE_AGGR0 "/clk/agr0"
#define CLOCK_NPA_RESOURCE_AGGR1 "/clk/agr1"
#define CLOCK_NPA_RESOURCE_AGGR2 "/clk/agr2"




/*
 * ClockCPUConfigType
 *
 * Configuration parameters for a performance level of a CPU.
 * 
 * CoreConfig     - CPU core configuration 
 * Mux          - General mux configuration
 * nVDDCPU        - VDD CPU value in millivolts
 * nLDOQ6UV       - Q6SS LDO voltage requirement in uV
 * nStrapACCVal   - ACC value
 */
typedef struct
{
  HAL_clk_ClockConfigType  CoreConfig;
  ClockMuxConfigType       Mux;
  uint32                   nVDDCPU;
  uint32                   nLDOQ6UV;
  uint32                   nStrapACCVal;
} ClockCPUConfigType;


/*
 * Actual format for the data stored in the BSP for a CPU. 
 *  
 * nMinPerfLevel       - The minimum supported performance level for the CPU on this chipset. 
 * nMaxPerfLevel       - The maximum supported performance level for the CPU on this chipset. 
 * nInitPerfLevel      - The performance level that should be programmed during initialization by 
 *                       the clock driver.
 * nTotalConfigs       - The total performance level configurations available on a chip. 
 * HWVersion           - Version of the chip HW this configuration is for.
 * panPerfLevel        - A pointer to an array of performance levels. 
 */
typedef struct
{
  uint32              nMinPerfLevel;
  uint32              nMaxPerfLevel;
  uint32              nInitPerfLevel;
  uint32              nTotalConfigs;
  uint32              *panPerfLevel;
  ClockHWVersionType  HWVersion;
} ClockCPUPerfConfigType;


/*
 * LPASS PLL to actual PLL mapping 
 *  
 * nSource     - The supported PLL for this chipset. 
 * bInitialize - Specifies if the driver should initialize this during clock initialization. 
 */
typedef struct
{
  HAL_clk_SourceType nSource;
  boolean            bInitialize;
} ClockImagePLLConfigType;


/*
 * LPASS BSP data 
 *  
 * pCPUConfig             - Contains CPU-specific configurtion information. 
 * pCPUPerConfig          - Contains the performance level information for the CPU. 
 * bGlobalLDOEnable       - Flag specifying whether the embedded LDO should be enabled.
 * pIntermediateCPUConfig - Clock configuration to use while reconfiguring main CPU source.
 * bLDOSupportedByEFuse    - Flag that will be set if the EFUSE determins eLDO can be used. 
 */
typedef struct
{
  ClockCPUConfigType       *pCPUConfig;
  ClockCPUPerfConfigType   *pCPUPerfConfig;
  boolean                   bGlobalLDOEnable;
  ClockCPUConfigType       *pIntermediateCPUConfig;
  boolean                   bLDOSupportedByEFuse;
} ClockImageConfigType;

/*
 * ClockSourceInitType
 *  
 * eSource       - Source that needs to be initialized
 * nFreqHz       - Frequency to initialize to if applicable, otherwise 0 for default. 
 *  
 */
typedef struct
{
  HAL_clk_SourceType eSource;
  uint32             nFreqHz;
}ClockSourceInitType;


/*
 * NPA Remote Node support flags type to determine 
 * chipset specific initialization of NPA Remote Nodes. 
 *  
 * bOCMEM - Specifies if OCMEM is supported 
 *  
*/
typedef struct
{
  boolean bOCMEM;

}ClockNPARemoteNodeSupportType;


/*
 * ClockLDOInitType
 *
 * BSP data for LDO programming
 *
 * nLDOCFG0          - The value for the first configuration register.
 * nLDOCFG1          - The value for the second configuration register.
 * nLDOCFG2          - The value for the third configuration register.
 * nRetentionVoltage - The retention voltage in mV.
 * nVddRequestDelay  - The initial operating voltage in mV.
 */
typedef struct
{
  uint32               nLDOCFG0;
  uint32               nLDOCFG1;
  uint32               nLDOCFG2;
  uint32               nRetentionVoltage;
  uint32               nOperatingVoltage;
}ClockLDODataType;


/*
 * ClockNPAResourcePubType
 *
 * BSP data for publishing NPA nodes to user domains
 *
 * nDomain           - The domain that requires the resource published.
 * ppszResourceList  - An array of string client resources to publish.
 * nTotalResources   - The number of resources to publish.
 */
typedef struct
{
   uint32 nDomain;
   const char** ppszResourceList;
   uint32 nTotalResources;
}ClockNPAResourcePubType;


#endif  /* __CLOCKCPUBSP_H__ */ 

