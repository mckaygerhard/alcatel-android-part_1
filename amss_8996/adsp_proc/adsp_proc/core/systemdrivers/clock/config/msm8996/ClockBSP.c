/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock regime bsp data for DAL based driver.

==============================================================================

                             Edit History

$Header: //components/rel/core.adsp/2.7/systemdrivers/clock/config/msm8996/ClockBSP.c#19 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
04/13/15   dcf     Ported for 8996.
04/12/14   dcf     Created. 

==============================================================================
            Copyright (c) 2014 - 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"
#include "ClockLPASSCPU.h"
#include "comdef.h"

/*=========================================================================
      Data Declarations
==========================================================================*/

/*
 *  SourceFreqConfig_XO
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    .nFreqHz    = 19200 * 1000,
    .HALConfig  = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    .nFreqHz    = 600000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 31,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0x40,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL0[] =
{
  {
    .nFreqHz    = 564480 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 2,
      .nL             = 29,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x66666666,
      .nAlphaU        = 0x00000066,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL1
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL1[] =
{
  {
    .nFreqHz    = 249600 * 1000,                             /* SourceFreqConfig_LPAPLL1[0] */
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO4,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 13,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0x00, 0x00}, {0x02, 0x00} },
  },
  {
    .nFreqHz    = 345600 * 1000,                             /* SourceFreqConfig_LPAPLL1[1] */
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO4,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 18,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0x00, 0x00}, {0x02, 0x00} },
  },
  /* Calibration Frequency. */
  {
    .nFreqHz    = 364800 * 1000,                             /* SourceFreqConfig_LPAPLL1[2] */
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO4,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 19,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0x00, 0x00}, {0x02, 0x00} },
  },
  /* end Calibration Frequency. */
  {
    .nFreqHz    = 422400 * 1000,                             /* SourceFreqConfig_LPAPLL1[3] */
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO4,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 22,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0x00, 0x00}, {0x02, 0x00} },
  },
  {
    .nFreqHz    = 480000 * 1000,                             /* SourceFreqConfig_LPAPLL1[4] */
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO4,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 25,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0x00, 0x00}, {0x02, 0x00} },
  },

  /*
   * V2 Frequencies start with VCO3 with F-min above F-max for v1.
   */

  {
    /* .nFreqHz    = */  499200 * 1000,                             /* SourceFreqConfig_LPAPLL1[5] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  26,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  537600 * 1000,                             /* SourceFreqConfig_LPAPLL1[6] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  28,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  595200 * 1000,                             /* SourceFreqConfig_LPAPLL1[7] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  31,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  633600 * 1000,                             /* SourceFreqConfig_LPAPLL1[8] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  33,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  652800 * 1000,                             /* SourceFreqConfig_LPAPLL1[9] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  34,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },

  /*
   * Calibration frequency for this PLL.
   */
  {
    /* .nFreqHz    = */  710400 * 1000,                             /* SourceFreqConfig_LPAPLL1[10] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  37,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  729600 * 1000,                             /* SourceFreqConfig_LPAPLL1[11] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  38,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  825600 * 1000,                             /* SourceFreqConfig_LPAPLL1[12] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  43,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  844800 * 1000,                             /* SourceFreqConfig_LPAPLL1[13] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  44,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
    /* .nFreqHz    = */  960000 * 1000,                             /* SourceFreqConfig_LPAPLL1[14] */
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  50,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
  },

  /* last entry */
  { 0 }
};

const ClockSourceFreqConfigType CPUCalibrationFreqV2 = 
{
  /* .nFreqHz    = */  710400 * 1000,
  /* .HALConfig  = */
  {
    /* .eSource        = */  HAL_CLK_SOURCE_XO,
    /* .eVCO           = */  HAL_CLK_PLL_VCO3,
    /* .nPreDiv        = */  1,
    /* .nPostDiv       = */  1,
    /* .nL             = */  37,
    /* .nM             = */  0,
    /* .nN             = */  0,
    /* .nVCOMultiplier = */  0,
    /* .nAlpha         = */  0x00000000,
    /* .nAlphaU        = */  0x00,
  },
  /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
  /* .HWVersion  = */  { {0x02, 0x00}, {0x03, 0x00} },
};


const ClockSourceFreqConfigType CPUCalibrationFreqV3 =
{
  /* .nFreqHz    = */  729600 * 1000,
  /* .HALConfig  = */
  {
    /* .eSource        = */  HAL_CLK_SOURCE_XO,
    /* .eVCO           = */  HAL_CLK_PLL_VCO3,
    /* .nPreDiv        = */  1,
    /* .nPostDiv       = */  1,
    /* .nL             = */  38,
    /* .nM             = */  0,
    /* .nN             = */  0,
    /* .nVCOMultiplier = */  0,
    /* .nAlpha         = */  0x00000000,
    /* .nAlphaU        = */  0x00,
  },
  /* .eVRegLevel = */  VCS_CORNER_LOW_MINUS,
  /* .HWVersion  = */  { {0x03, 0x00}, {0xFF, 0xFF} },
};


/*
 *  SourceFreqConfig_LPAPLL2
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL2[] =
{
  {
    .nFreqHz    = 614400 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 32,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  ClockSourcesToInit
 *
 *  Array of sources and settings to initialize at runtime.
 */
const ClockSourceInitType ClockSourcesToInit[] =
{
   { HAL_CLK_SOURCE_LPAPLL0, 564480 * 1000 },
   { HAL_CLK_SOURCE_LPAPLL2, 614400 * 1000 },
   { HAL_CLK_SOURCE_NULL,    NULL}
};



/*
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(XO),

    .nConfigMask            = 0,
    .pSourceFreqConfig      = SourceFreqConfig_XO,
  },
  {
    SOURCE_NAME(GPLL0),

    .nConfigMask            = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_GPLL0,
  },
  {
    SOURCE_NAME(LPAPLL0),

    .nConfigMask            = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_LPAPLL0,
  },
  {
    SOURCE_NAME(LPAPLL1),

    .nConfigMask            = CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_LPAPLL1,
    .pCalibrationFreqConfig = &SourceFreqConfig_LPAPLL1[2],
    .eDisableMode           = HAL_CLK_SOURCE_DISABLE_MODE_FREEZE
  },
  {
    SOURCE_NAME(LPAPLL2),

    .nConfigMask            = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig      = SourceFreqConfig_LPAPLL2,
  },

  /* last entry */
  { SOURCE_NAME(NULL) }

};


/*----------------------------------------------------------------------*/
/* GCC   Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * BLSP1QUP1I2CAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1I2CAPPSClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_LOW,         },
  { 0 }
};


/*
 * BLSP1QUP1SPIAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1SPIAPPSClockConfig[] =
{
  {     960000, { HAL_CLK_SOURCE_XO,             20,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {    9600000, { HAL_CLK_SOURCE_XO,             4,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   15000000, { HAL_CLK_SOURCE_GPLL0,          20,     1,      4,      4       }, VCS_CORNER_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, VCS_CORNER_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * BLSP1UART1APPS clock configurations
 */
const ClockMuxConfigType BLSP1UART1APPSClockConfig[] =
{
  {    3686400, { HAL_CLK_SOURCE_GPLL0,          2,      96,     15625,  15625   }, VCS_CORNER_LOW,         },
  {    7372800, { HAL_CLK_SOURCE_GPLL0,          2,      192,    15625,  15625   }, VCS_CORNER_LOW,         },
  {   14745600, { HAL_CLK_SOURCE_GPLL0,          2,      384,    15625,  15625   }, VCS_CORNER_LOW,         },
  {   16000000, { HAL_CLK_SOURCE_GPLL0,          10,     2,      15,     15      }, VCS_CORNER_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   24000000, { HAL_CLK_SOURCE_GPLL0,          10,     1,      5,      5       }, VCS_CORNER_LOW,         },
  {   32000000, { HAL_CLK_SOURCE_GPLL0,          2,      4,      75,     75      }, VCS_CORNER_NOMINAL,     },
  {   40000000, { HAL_CLK_SOURCE_GPLL0,          30,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   46400000, { HAL_CLK_SOURCE_GPLL0,          2,      29,     375,    375     }, VCS_CORNER_NOMINAL,     },
  {   48000000, { HAL_CLK_SOURCE_GPLL0,          25,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   51200000, { HAL_CLK_SOURCE_GPLL0,          2,      32,     375,    375     }, VCS_CORNER_NOMINAL,     },
  {   56000000, { HAL_CLK_SOURCE_GPLL0,          2,      7,      75,     75      }, VCS_CORNER_NOMINAL,     },
  {   58982400, { HAL_CLK_SOURCE_GPLL0,          2,      1536,   15625,  15625   }, VCS_CORNER_NOMINAL,     },
  {   60000000, { HAL_CLK_SOURCE_GPLL0,          20,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  {   63157895, { HAL_CLK_SOURCE_GPLL0,          19,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};



/*----------------------------------------------------------------------*/
/* LPASS Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * AON clock configurations
 */
const ClockMuxConfigType AONClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,         2,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   76800000, { HAL_CLK_SOURCE_LPAPLL2,    8,     0,      0,      0       }, VCS_CORNER_LOW,         },
  {  153600000, { HAL_CLK_SOURCE_LPAPLL2,    4,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * ATIME clock configurations
 */
const ClockMuxConfigType ATIMEClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   30720000, { HAL_CLK_SOURCE_LPAPLL2,        8,      0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   30720000, { HAL_CLK_SOURCE_LPAPLL2,        8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   30720000, { HAL_CLK_SOURCE_LPAPLL2,        8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   61440000, { HAL_CLK_SOURCE_LPAPLL2,        4,      0,      0,      0       }, VCS_CORNER_LOW,         },
  {  122880000, { HAL_CLK_SOURCE_LPAPLL2,        2,      0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * AUDSLIMBUS clock configurations
 */
const ClockMuxConfigType AUDSLIMBUSClockConfig[] =
{
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 0 }
};


/*
 * CORE clock configurations
 */
const ClockMuxConfigType COREClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,         2,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   38400000, { HAL_CLK_SOURCE_LPAPLL2,   16,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   76800000, { HAL_CLK_SOURCE_LPAPLL2,    8,     0,      0,      0       }, VCS_CORNER_LOW,         },
  {  153600000, { HAL_CLK_SOURCE_LPAPLL2,    4,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * EXTMCLK0 clock configurations
 */
const ClockMuxConfigType EXTMCLK0ClockConfig[] =
{
  {     352800, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      32,     32      }, VCS_CORNER_LOW_MINUS,   },
  {     512000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {     705600, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {     768000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {    1024000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    1411200, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    1536000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    2048000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    2822400, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    3072000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    4096000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    5644800, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    6144000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    8192000, { HAL_CLK_SOURCE_LPAPLL2,        30,     0,      0,      0       }, VCS_CORNER_LOW,         },
  {   11289600, { HAL_CLK_SOURCE_LPAPLL0,        10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   12288000, { HAL_CLK_SOURCE_LPAPLL2,        20,     0,      0,      0       }, VCS_CORNER_LOW,         },
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,        10,     0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * LPAIFPCMOE clock configurations
 */
const ClockMuxConfigType LPAIFPCMOEClockConfig[] =
{
  {     352800, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      32,     32      }, VCS_CORNER_LOW_MINUS,   },
  {     512000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {     705600, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {     768000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      16,     16      }, VCS_CORNER_LOW_MINUS,   },
  {    1024000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    1411200, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    1536000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      8,      8       }, VCS_CORNER_LOW_MINUS,   },
  {    2048000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    2822400, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    3072000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      4,      4       }, VCS_CORNER_LOW_MINUS,   },
  {    4096000, { HAL_CLK_SOURCE_LPAPLL2,        30,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    5644800, { HAL_CLK_SOURCE_LPAPLL0,        10,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    6144000, { HAL_CLK_SOURCE_LPAPLL2,        20,     1,      2,      2       }, VCS_CORNER_LOW_MINUS,   },
  {    8192000, { HAL_CLK_SOURCE_LPAPLL2,        30,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {    8192000, { HAL_CLK_SOURCE_LPAPLL2,        30,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {    8192000, { HAL_CLK_SOURCE_LPAPLL2,        30,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   11289600, { HAL_CLK_SOURCE_LPAPLL0,        10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  {   12288000, { HAL_CLK_SOURCE_LPAPLL2,        20,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   12288000, { HAL_CLK_SOURCE_LPAPLL2,        20,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   12288000, { HAL_CLK_SOURCE_LPAPLL2,        20,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,        10,     0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,        10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   24576000, { HAL_CLK_SOURCE_LPAPLL2,        10,     0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   61440000, { HAL_CLK_SOURCE_LPAPLL2,         4,     0,      0,      0       }, VCS_CORNER_LOW,         },
  {  122880000, { HAL_CLK_SOURCE_LPAPLL2,         2,     0,      0,      0       }, VCS_CORNER_NOMINAL      },

  { 0 }
};


/*
 * RESAMPLER clock configurations
 */
const ClockMuxConfigType RESAMPLERClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,        2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,         },
  {   76800000, { HAL_CLK_SOURCE_LPAPLL2,   8,      0,      0,      0       }, VCS_CORNER_LOW,         { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  {   76800000, { HAL_CLK_SOURCE_LPAPLL2,   8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  {   76800000, { HAL_CLK_SOURCE_LPAPLL2,   8,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  {  153600000, { HAL_CLK_SOURCE_LPAPLL2,   4,      0,      0,      0       }, VCS_CORNER_LOW,         },
  {  307200000, { HAL_CLK_SOURCE_LPAPLL2,   2,      0,      0,      0       }, VCS_CORNER_NOMINAL,     },
  { 0 }
};


/*
 * XO clock configurations
 */
const ClockMuxConfigType XOClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, VCS_CORNER_LOW_MINUS,   },
  { 0 }
};



/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"lpass_q6core",                                CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_qdsp_sway_aon_clk",                CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_aon_clk",                       CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_bus_timeout_aon_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_mpu_cfg_aon_clk",               CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_q6_ahbm_mpu_aon_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_qos_ahbs_aon_clk",              CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_sysnoc_sway_aon_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"q6ss_ahbm_aon_clk",                           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"q6ss_ahbs_aon_clk",                           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_peripheral_smmu_client_core_clk",  CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_core_clk",                         CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_aud_slimbus_core_clk",             CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_qca_slimbus_core_clk",             CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_lpm_core_clk",                     CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_sysnoc_mport_core_clk",            CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_NONE,         (void*)0,                                             0                       } 
};

/*
 * Resources that need to be published to domains outside GuestOS.
 */
static const char *ClockPubResource[] =
{
   CLOCK_NPA_RESOURCE_QDSS
};

ClockNPAResourcePubType ClockResourcePub = 
{
   SENSOR_PD,
   ClockPubResource,
   1
};



/*
 * Initial CX voltage level.
 */
const VCSCornerType CXVRegInitLevelConfig[] =
{
  VCS_CORNER_LOW
};



/*=========================================================================
      Type Definitions and Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Mux configuration for different CPU frequencies. 
 */
static ClockCPUConfigType Clock_Q6Config [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   86400 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[1]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   172800 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[1]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   249600 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   422400 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_NOMINAL,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[3]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   480000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_TURBO,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[4]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  { 0 }
};

/*
 * Mux configuration for different CPU frequencies. 
 */
static ClockCPUConfigType Clock_Q6ConfigV2 [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x00, 0x00}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V2 */
    /* .Mux             = */
    {
      /* .nFreq             = */  124800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[5] // 499.2 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V2 */
    /* .Mux             = */
    {
      /* .nFreq             = */  268800 * 1000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[6] // 537.6 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V2 */
    /* .Mux             = */
    {
      /* .nFreq             = */  422400000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[13] // 844.8 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V2 */
    /* .Mux             = */
    {
      /* .nFreq             = */  633600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_NOMINAL,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[8] // 633.6 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V2 */
    /* .Mux             = */
    {
      /* .nFreq             = */  729600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_TURBO,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[11] // 729.6 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  { 0 }
};

/*
 * Mux configuration for different CPU frequencies. 
 */
static ClockCPUConfigType Clock_Q6ConfigV3 [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   VCS_CORNER_LOW_MINUS,
    /* .HWVersion         */   { {0x00, 0x00}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V3 */
    /* .Mux             = */
    {
      /* .nFreq             = */  124800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[5] // 499.2 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V3 */
    /* .Mux             = */
    {
      /* .nFreq             = */  297600 * 1000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_LOW_MINUS,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[7] // 595.2 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V3 */
    {
      /* .Mux               */
      /* .nFreq             */   480000 * 1000,
      /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
      /* .eVRegLevel        */   VCS_CORNER_LOW,
      /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
      /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[14] // 960.0 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V3 */
    /* .Mux             = */
    {
      /* .nFreq             = */  652800000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_NOMINAL,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[9] // 652.8 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  {
    /* .CoreConfig      = */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,             /* V3 */
    /* .Mux             = */
    {
      /* .nFreq             = */  825600000,
      /* .HALConfig         = */  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      /* .eVRegLevel        = */  VCS_CORNER_TURBO,
      /* .HWVersion         = */  { {0x02, 0x00}, {0x03, 0x00} },
      /* .pSourceFreqConfig = */  &SourceFreqConfig_LPAPLL1[12] // 825.6 MHz
    },
    /* .nStrapACCVal      */ 0x00000020
  },
  { 0 }
};




/*
 * Enumeration of CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
enum
{
  CLOCK_CPU_PERF_LEVEL_0,  /*  19.2 MHz */
  CLOCK_CPU_PERF_LEVEL_1,  /*  86.4 MHz */
  CLOCK_CPU_PERF_LEVEL_2,  /* 172.8 MHz */
  CLOCK_CPU_PERF_LEVEL_3,  /* 249.6 MHz */
  CLOCK_CPU_PERF_LEVEL_4,  /* 422.4 MHz */
  CLOCK_CPU_PERF_LEVEL_5,  /* 480.0 MHz */

  CLOCK_CPU_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of generic CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
static const uint32 Clock_Q6PerfLevels[] =
{
  CLOCK_CPU_PERF_LEVEL_0,  /*  19.2 MHz */
  CLOCK_CPU_PERF_LEVEL_1,  /*  86.4 MHz */
  CLOCK_CPU_PERF_LEVEL_2,  /* 172.8 MHz */
  CLOCK_CPU_PERF_LEVEL_3,  /* 249.6 MHz */
  CLOCK_CPU_PERF_LEVEL_4,  /* 422.4 MHz */
  CLOCK_CPU_PERF_LEVEL_5,  /* 480.0 MHz */
};


/*
 * Performance level configuration data for the Q6 clock.
 */
static ClockCPUPerfConfigType Clock_Q6PerfConfig[] =
{
  {
    .HWVersion      = {{0, 0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_CPU_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_CPU_PERF_LEVEL_5,
    .anPerfLevel    = (uint32*)Clock_Q6PerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_Q6PerfLevels)
  },
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfig =
{
  .bEnableDCS              = TRUE,
  .pCPUConfig              = Clock_Q6Config,
  .pCPUPerfConfig          = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
//  .pConfigBusConfig        = Clock_BUSMSSCONFIGConfig,
//  .pConfigBusPerfConfig    = &Clock_BUSMSSCONFIGPerfConfig
};

/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfigV2 =
{
  .bEnableDCS              = TRUE,
  .pCPUConfig              = Clock_Q6ConfigV2,
  .pCPUPerfConfig          = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
//  .pConfigBusConfig        = Clock_BUSMSSCONFIGConfig,
//  .pConfigBusPerfConfig    = &Clock_BUSMSSCONFIGPerfConfig
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfigV3 =
{
  .bEnableDCS              = TRUE,
  .pCPUConfig              = Clock_Q6ConfigV3,
  .pCPUPerfConfig          = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ARR_SIZE(Clock_Q6PerfConfig),
//  .pConfigBusConfig        = Clock_BUSMSSCONFIGConfig,
//  .pConfigBusPerfConfig    = &Clock_BUSMSSCONFIGPerfConfig
};



const HAL_clk_HWIOBaseType ClockLPASSHWIOBases = 
{
   /* nPhysAddress */ 0x09000000,
   /* nSize        */ 0x00400000
};

