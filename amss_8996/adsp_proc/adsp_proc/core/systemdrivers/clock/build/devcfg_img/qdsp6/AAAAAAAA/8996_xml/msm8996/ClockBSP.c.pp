# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c" 2
# 31 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h" 1
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 107 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;







typedef unsigned char byte;



typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
      typedef long long int64;



      typedef unsigned long long uint64;
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h" 2
# 57 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h"
typedef uint32 ClockIdType;




typedef uint32 SourceIdType;







typedef enum
{
  CLOCK_FREQUENCY_HZ_AT_LEAST = 0,
  CLOCK_FREQUENCY_HZ_AT_MOST = 1,
  CLOCK_FREQUENCY_HZ_CLOSEST = 2,
  CLOCK_FREQUENCY_HZ_EXACT = 3,

  CLOCK_FREQUENCY_KHZ_AT_LEAST = 0x10,
  CLOCK_FREQUENCY_KHZ_AT_MOST = 0x11,
  CLOCK_FREQUENCY_KHZ_CLOSEST = 0x12,
  CLOCK_FREQUENCY_KHZ_EXACT = 0x13,

  CLOCK_FREQUENCY_MHZ_AT_LEAST = 0x20,
  CLOCK_FREQUENCY_MHZ_AT_MOST = 0x21,
  CLOCK_FREQUENCY_MHZ_CLOSEST = 0x22,
  CLOCK_FREQUENCY_MHZ_EXACT = 0x23,
} ClockFrequencyType;






typedef enum
{
  CLOCK_RESET_DEASSERT = 0,
  CLOCK_RESET_ASSERT = 1,
  CLOCK_RESET_PULSE = 2
} ClockResetType;





typedef enum
{
  CLOCK_QDSS_LEVEL_OFF,
  CLOCK_QDSS_LEVEL_DEBUG,
  CLOCK_QDSS_LEVEL_HSDEBUG,
} ClockQDSSLevelType;






typedef enum
{
  CLOCK_CONFIG_MMSS_SOURCE_CSI0 = 1,
  CLOCK_CONFIG_MMSS_SOURCE_CSI1 = 2,
  CLOCK_CONFIG_MMSS_SOURCE_CSI2 = 3,
  CLOCK_CONFIG_LPASS_CORE_MEM_ON = 4,
  CLOCK_CONFIG_LPASS_CORE_MEM_OFF = 5,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_ON = 6,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_OFF = 7,
  CLOCK_CONFIG_LPASS__HW_CTL_ON = 8,
  CLOCK_CONFIG_LPASS__HW_CTL_OFF = 9,
  CLOCK_CONFIG_AUTOGATE_ENABLE = 10,
  CLOCK_CONFIG_AUTOGATE_DISABLE = 11,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_DIG_CODEC = 12,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_PRI_MI2S = 13,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_SEC_MI2S = 14,


  CLOCK_CONFIG_TOTAL = 15

} ClockConfigType;







typedef enum
{
  CLOCK_SOURCE_NONE,
  CLOCK_SOURCE_PRIMARY,
  CLOCK_SOURCE_SECONDARY,
  CLOCK_SOURCE_TERNERY,

  CLOCK_SOURCE_TOTAL

}ClockSourceType;





typedef enum
{
  CLOCK_LOG_STATE_CLOCK_FREQUENCY = (1 << 0),
} ClockLogStateFlags;
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h"
typedef uint32 ClockPowerDomainIdType;
# 186 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h"
typedef enum
{
  CLOCK_SLEEP_MODE_HALT,
  CLOCK_SLEEP_MODE_POWER_COLLAPSE
} ClockSleepModeType;
# 213 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/ClockDefs.h"
typedef enum
{
  CLOCK_CPU_MSS_Q6,
  CLOCK_CPU_LPASS_Q6,
  CLOCK_CPU_APPS_C0,
  CLOCK_CPU_APPS_C1,
  CLOCK_CPU_APPS_C2,
  CLOCK_CPU_APPS_C3,

  CLOCK_CPU_NUM_OF_CPUS
} ClockCPUType;
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h" 1
# 57 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h"
typedef unsigned long int bool32;
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h" 2
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_SOURCE_NULL = 0,




  HAL_CLK_SOURCE_XO,




  HAL_CLK_SOURCE_GPLL0,
  HAL_CLK_SOURCE_GPLL0_DIV2,
  HAL_CLK_SOURCE_GPLL0_EARLY,
  HAL_CLK_SOURCE_GPLL1,
  HAL_CLK_SOURCE_GPLL1_DIV2,
  HAL_CLK_SOURCE_GPLL1_EARLY,
  HAL_CLK_SOURCE_GPLL2,
  HAL_CLK_SOURCE_GPLL2_DIV2,
  HAL_CLK_SOURCE_GPLL2_EARLY,
  HAL_CLK_SOURCE_GPLL3,
  HAL_CLK_SOURCE_GPLL3_DIV2,
  HAL_CLK_SOURCE_GPLL3_EARLY,
  HAL_CLK_SOURCE_GPLL4,
  HAL_CLK_SOURCE_GPLL4_DIV2,
  HAL_CLK_SOURCE_GPLL4_EARLY,
  HAL_CLK_SOURCE_LPAPLL0,
  HAL_CLK_SOURCE_LPAPLL0_DIV2,
  HAL_CLK_SOURCE_LPAPLL0_DIV5,
  HAL_CLK_SOURCE_LPAPLL1,
  HAL_CLK_SOURCE_LPAPLL2,
  HAL_CLK_SOURCE_LPAPLL2_DIV2,
  HAL_CLK_SOURCE_LPAPLL2_DIV5,

  HAL_CLK_SOURCE_AUD_REF_CLK,

  HAL_CLK_NUM_OF_CONFIG_SOURCES,




  HAL_CLK_SOURCE_EXTERNAL,
  HAL_CLK_SOURCE_EXTERNAL2,
  HAL_CLK_NUM_OF_EXTERNAL_SOURCES,




  HAL_CLK_SOURCE_SLEEPCLK,
  HAL_CLK_SOURCE_GROUND,
  HAL_CLK_SOURCE_CLKTEST,
  HAL_CLK_SOURCE_PLLTEST,

  HAL_CLK_NUM_OF_SOURCES,

  HAL_CLK_SOURCE_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_SourceType;
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_CONFIG_NULL = 0,





  HAL_CLK_CONFIG_MMSS_SOURCE_CSI0 = 1,
  HAL_CLK_CONFIG_MMSS_SOURCE_CSI1 = 2,
  HAL_CLK_CONFIG_MMSS_SOURCE_CSI2 = 3,
  HAL_CLK_CONFIG_LPASS_CORE_MEM_ON = 4,
  HAL_CLK_CONFIG_LPASS_CORE_MEM_OFF = 5,
  HAL_CLK_CONFIG_LPASS_PERIPH_MEM_ON = 6,
  HAL_CLK_CONFIG_LPASS_PERIPH_MEM_OFF = 7,
  HAL_CLK_CONFIG_LPASS_HW_CTL_ON = 8,
  HAL_CLK_CONFIG_LPASS_HW_CTL_OFF = 9,
  HAL_CLK_CONFIG_AUTOGATE_ENABLE = 10,
  HAL_CLK_CONFIG_AUTOGATE_DISABLE = 11,
  HAL_CLK_CONFIG_DIG_CODEC_SRC_SEL_DIG_CODEC = 12,
  HAL_CLK_CONFIG_DEG_CODEC_SRC_SEL_PRI_MI2S = 13,
  HAL_CLK_CONFIG_DEG_CODEC_SRC_SEL_SEC_MI2S = 14,




  HAL_CLK_CONFIG_ASYNC_MODE = 1000,
  HAL_CLK_CONFIG_SYNC_MODE,
  HAL_CLK_CONFIG_1X_MODE,
  HAL_CLK_CONFIG_2X_MODE,

  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCB,
  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCC,
  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCD,
  HAL_CLK_CONFIG_Q6SS_CORE_LOW_JITTER_PLL,
  HAL_CLK_CONFIG_Q6SS_CORE_PLL_MAIN,

  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_MUX,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_PLL2,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_HSPLL1,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_HSPLL2,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_RAMP_CLOCK,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_JTAG_TCK,
  HAL_CLK_CONFIG_ACPU_CORE_SOURCE_MUX_SYNC_TO_AXI,
  HAL_CLK_CONFIG_ACPU_SLEEP_USE_SLEEP_EN,
  HAL_CLK_CONFIG_ACPU_SLEEP_MASK_CORE_CLK,
  HAL_CLK_CONFIG_ACPU_SLEEP_WAIT_FOR_AXI_CLK,
  HAL_CLK_CONFIG_ACPU_NO_POWER_DOWN_REQ_ON_SWFI,
  HAL_CLK_CONFIG_ACPU_POWER_DOWN_REQ_ON_SWFI,
  HAL_CLK_CONFIG_ACPU_POWER_UP_REQ_ON_INTERRUPT,
  HAL_CLK_CONFIG_ACPU_NO_POWER_UP_REQ_ON_INTERRUPT,

  HAL_CLK_CONFIG_VFE_VIDEO,
  HAL_CLK_CONFIG_VFE_CAMERA,
  HAL_CLK_CONFIG_VFE_EXTERNAL,

  HAL_CLK_CONFIG_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_ClockConfigType;
# 199 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_CLOCK_API_ENABLE,
  HAL_CLK_CLOCK_API_DISABLE,
  HAL_CLK_CLOCK_API_RESET,
  HAL_CLK_CLOCK_API_INVERT,
  HAL_CLK_CLOCK_API_ISON,
  HAL_CLK_CLOCK_API_ISENABLED,
  HAL_CLK_CLOCK_API_CONFIG,
  HAL_CLK_CLOCK_API_CONFIGDIVIDER,
  HAL_CLK_CLOCK_API_CALCFREQ,

  HAL_CLK_CLOCK_API_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_ClockAPIType;
# 226 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef struct
{
  HAL_clk_SourceType eSource;
  uint32 nDiv2x;
  uint32 nM;
  uint32 nN;
  uint32 n2D;
} HAL_clk_ClockMuxConfigType;
# 242 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_CONFIG_SOURCE_NULL = 0,

  HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE,
  HAL_CLK_CONFIG_PLL_FSM_MODE_DISABLE,
  HAL_CLK_CONFIG_PLL_AUX_OUTPUT_DISABLE,
  HAL_CLK_CONFIG_PLL_AUX_OUTPUT_ENABLE,
  HAL_CLK_CONFIG_PLL_AUX2_OUTPUT_DISABLE,
  HAL_CLK_CONFIG_PLL_AUX2_OUTPUT_ENABLE,
  HAL_CLK_CONFIG_PLL_TEST_OUTPUT_DISABLE,
  HAL_CLK_CONFIG_PLL_TEST_OUTPUT_ENABLE,
  HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_DISABLE,
  HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
  HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_DISABLE,
  HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE,

  HAL_CLK_CONFIG_XO_CHIP_BUFFER_USING_EXT_CLK_SRC,
  HAL_CLK_CONFIG_XO_USING_EXT_CRYSTAL,
  HAL_CLK_CONFIG_XO_GAIN_0,
  HAL_CLK_CONFIG_XO_GAIN_1,
  HAL_CLK_CONFIG_XO_GAIN_2,
  HAL_CLK_CONFIG_XO_GAIN_3,
  HAL_CLK_CONFIG_XO_GAIN_4,
  HAL_CLK_CONFIG_XO_GAIN_5,
  HAL_CLK_CONFIG_XO_GAIN_6,
  HAL_CLK_CONFIG_XO_GAIN_7,

  HAL_CLK_CONFIG_PLL_SRC_SEL_AUD_REF_CLK,

  HAL_CLK_SOURCE_CONFIG_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_SourceConfigType;
# 282 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_PLL_VCO1,
  HAL_CLK_PLL_VCO2,
  HAL_CLK_PLL_VCO3,
  HAL_CLK_PLL_VCO4,

  HAL_CLK_NUM_OF_PLL_VCOS,

  HAL_CLK_PLL_VCO_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_PLLVCOType;
# 313 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef struct
{
  HAL_clk_SourceType eSource;
  HAL_clk_PLLVCOType eVCO;
  uint32 nPreDiv;
  uint32 nPostDiv;
  uint32 nL;
  uint32 nM;
  uint32 nN;
  uint32 nVCOMultiplier;
  uint32 nAlpha;
  uint32 nAlphaU;
} HAL_clk_PLLConfigType;
# 343 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_FOOTSWITCH_CONFIG_CORE_FORCE_ON,
  HAL_CLK_FOOTSWITCH_CONFIG_CORE_ALLOW_OFF,
  HAL_CLK_FOOTSWITCH_CONFIG_PERIPHERAL_FORCE_ON,
  HAL_CLK_FOOTSWITCH_CONFIG_PERIPHERAL_ALLOW_OFF,
  HAL_CLK_FOOTSWITCH_CONFIG_PERIPHERAL_FORCE_OFF,
  HAL_CLK_FOOTSWITCH_CONFIG_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_FootswitchConfigType;
# 371 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef struct
{
  void (*BusyWait) ( uint32 uSeconds );
  uint32 nChipVersion;
  uint32 nChipId;
  uint32 nChipFamily;
} HAL_clk_ContextType;
# 391 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef struct
{
  uint32 nPhysAddr;
  uint32 nSize;
  uint32 *pnBaseAddr;
} HAL_clk_HWIOBaseType;
# 420 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_SOURCE_CONFIG_MODE_NORMAL,
  HAL_CLK_SOURCE_CONFIG_MODE_SLEW,
  HAL_CLK_SOURCE_CONFIG_MODE_PENDING,

  HAL_CLK_NUM_OF_SOURCE_CONFIG_MODES,

  HAL_CLK_SOURCE_CONFIG_MODES_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_SourceConfigMode;
# 457 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
typedef enum
{
  HAL_CLK_SOURCE_DISABLE_MODE_NORMAL = 0,
  HAL_CLK_SOURCE_DISABLE_MODE_STANDBY = 1,
  HAL_CLK_SOURCE_DISABLE_MODE_SAVE = 2,
  HAL_CLK_SOURCE_DISABLE_MODE_FREEZE = 3,
  HAL_CLK_SOURCE_DISABLE_MODES_FORCE32BITS = 0x7FFFFFFF
} HAL_clk_SourceDisableModeType;






typedef void *HAL_clk_ClockHandleType;





typedef void *HAL_clk_ClockDomainHandleType;





typedef void *HAL_clk_PowerDomainHandleType;
# 497 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hal/clk/inc/HALclk.h"
void HAL_clk_Init ( HAL_clk_ContextType *pHALClkCtxt );
void HAL_clk_Reset ( void );
void HAL_clk_Save ( void );
void HAL_clk_Restore ( void );

void HAL_clk_EnableClock ( HAL_clk_ClockHandleType pClockHandle );
void HAL_clk_DisableClock ( HAL_clk_ClockHandleType pClockHandle );
void HAL_clk_ConfigClock ( HAL_clk_ClockHandleType pClockHandle, HAL_clk_ClockConfigType eConfig );
void HAL_clk_ConfigClockDivider ( HAL_clk_ClockHandleType pClockHandle, uint32 nDiv );
void HAL_clk_ConfigFootswitch ( HAL_clk_ClockHandleType pClockHandle, HAL_clk_FootswitchConfigType eFSConfig );
void HAL_clk_ResetClock ( HAL_clk_ClockHandleType pClockHandle, boolean bAssert );
boolean HAL_clk_IsClockSupported ( HAL_clk_ClockHandleType pClockHandle, HAL_clk_ClockAPIType eAPI );
boolean HAL_clk_IsClockEnabled ( HAL_clk_ClockHandleType pClockHandle );
boolean HAL_clk_IsClockOn ( HAL_clk_ClockHandleType pClockHandle );
boolean HAL_clk_IsClockReset ( HAL_clk_ClockHandleType pClockHandle );
void HAL_clk_InvertClock ( HAL_clk_ClockHandleType pClockHandle, boolean bInvert );

void HAL_clk_CalcClockFreq ( HAL_clk_ClockHandleType pClockHandle, uint32 *pnFreqHz );
void HAL_clk_DetectClockConfig ( HAL_clk_ClockHandleType pClockHandle, uint32 *pnConfig, boolean *pbValid );
void HAL_clk_DetectClockDivider ( HAL_clk_ClockHandleType pClockHandle, uint32 *pnDiv );

void HAL_clk_SaveClockState ( void );
void HAL_clk_ResetClockState ( void );
void HAL_clk_RestoreClockState ( void );

void HAL_clk_ClockSleepEnable ( HAL_clk_ClockHandleType pClockHandle );
void HAL_clk_ClockSleepReset ( void );

void HAL_clk_ConfigClockMux ( HAL_clk_ClockDomainHandleType pClockDomainHandle, const HAL_clk_ClockMuxConfigType *pmConfig );
void HAL_clk_DetectClockMuxConfig ( HAL_clk_ClockDomainHandleType pClockDomainHandle, HAL_clk_ClockMuxConfigType *pmConfig );
boolean HAL_clk_IsClockDomainSupported ( HAL_clk_ClockDomainHandleType pClockDomainHandle );
void HAL_clk_WaitForClockDomainOff ( HAL_clk_ClockDomainHandleType pClockDomainHandle );

boolean HAL_clk_EnableSource ( HAL_clk_SourceType eSource, HAL_clk_SourceDisableModeType eMode, void *pData );
void HAL_clk_DisableSource ( HAL_clk_SourceType eSource, HAL_clk_SourceDisableModeType eMode, void *pData );
boolean HAL_clk_EnableSourceVote ( HAL_clk_SourceType eSource );
void HAL_clk_DisableSourceVote ( HAL_clk_SourceType eSource );
void HAL_clk_ConfigSource ( HAL_clk_SourceType eSource, HAL_clk_SourceConfigType eConfig );
boolean HAL_clk_IsSourceEnabled ( HAL_clk_SourceType eSource );
boolean HAL_clk_IsSourceVoteEnabled ( HAL_clk_SourceType eSource );
boolean HAL_clk_ConfigPLL ( HAL_clk_SourceType eSource, const HAL_clk_PLLConfigType *pmConfig, HAL_clk_SourceConfigMode eMode );
void HAL_clk_DetectPLLConfig ( HAL_clk_SourceType eSource, HAL_clk_PLLConfigType *pmConfig );

void HAL_clk_EnablePowerDomain ( HAL_clk_PowerDomainHandleType pPowerDomainHandle );
void HAL_clk_DisablePowerDomain ( HAL_clk_PowerDomainHandleType pPowerDomainHandle );
boolean HAL_clk_IsPowerDomainSupported ( HAL_clk_PowerDomainHandleType pPowerDomainHandle );
boolean HAL_clk_IsPowerDomainEnabled ( HAL_clk_PowerDomainHandleType pPowerDomainHandle );
boolean HAL_clk_IsPowerDomainOn ( HAL_clk_PowerDomainHandleType pPowerDomainHandle );

void HAL_clk_GetHWIOBases ( HAL_clk_HWIOBaseType **paHWIOBases );

HAL_clk_ClockHandleType HAL_clk_GetNextClockInDomain(HAL_clk_ClockDomainHandleType pClockDomainHandle, uint32 nClockIndex);
void HAL_clk_GetNextClockDomain ( HAL_clk_ClockDomainHandleType pClockDomainHandle, HAL_clk_ClockDomainHandleType *pNextClockDomainHandle );
void HAL_clk_GetNextPowerDomain ( HAL_clk_PowerDomainHandleType pPowerDomainHandle, HAL_clk_PowerDomainHandleType *pNextPowerDomainHandle );

uint32 HAL_clk_GetNumberOfClocks ( void );
uint32 HAL_clk_GetNumberOfClockDomains ( void );
uint32 HAL_clk_GetNumberOfPowerDomains ( void );

void HAL_clk_GetClockName ( HAL_clk_ClockHandleType pClockHandle, const char **szClockName );
void HAL_clk_GetPowerDomainName ( HAL_clk_PowerDomainHandleType pPowerDomainHandle, const char **szPowerDomainName );

uint32 HAL_clk_GetChipVersion ( void );
uint32 HAL_clk_GetChipId ( void );
uint32 HAL_clk_GetChipFamily ( void );
uint32 HAL_clk_GetTestClockId ( HAL_clk_ClockHandleType pClockHandle );

boolean HAL_clk_ValidateRegSetTimeout ( uint32 nAddr, uint32 nMask, uint32 nTimeoutUS );
boolean HAL_clk_ValidateRegClearTimeout ( uint32 nAddr, uint32 nMask, uint32 nTimeoutUS );

uint32 HAL_clk_GetNumberOfClocksInDomain( HAL_clk_ClockDomainHandleType pClockDomainHandle );
void HAL_clk_AdjustSourceFrequency ( HAL_clk_SourceType eSource, HAL_clk_PLLConfigType *pPLLConfig );
void HAL_clk_GetSourceDelta ( HAL_clk_SourceType eSource, HAL_clk_PLLConfigType *pPLLConfig );
void HAL_clk_SelectClockSource ( HAL_clk_ClockDomainHandleType pClockDomainHandle, HAL_clk_SourceType eSource );
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdErr.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 1
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};

typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};







typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};

typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;

};

struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};

typedef struct DalDeviceHandle * DALDEVICEHANDLE;

typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};

struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};






static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}

static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}

static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}

static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}

static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}

static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}

static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}

static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
# 218 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);






DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);





DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
# 314 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 2


typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};


typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;

typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};


typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};


typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};

typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};

typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};







typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};

typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};

typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);


typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};

typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;

typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;

typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;


typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};






typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;

    DALSYSMemObj *pObj;

    DALBOOL prealloc;
};

typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
# 243 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);

typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);

typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);

typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);

typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);

typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);

typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);

typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);

typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);

typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);

typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);

typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);

typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);

typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);

typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);

typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);

typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);

typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);

typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);

typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};


typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);

typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};

typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 10 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 11 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 2 3 4
# 29 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
typedef _Sizet size_t;




int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));

void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));


char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));

void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
# 106 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
# 130 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 227 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitMod(DALSYSConfig * pCfg);
# 238 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_DeInitMod(void);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
# 279 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
# 310 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 334 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 358 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
# 394 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
# 415 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
# 434 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
# 458 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
# 473 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
# 492 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
# 508 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
# 519 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
# 589 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
# 634 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
# 698 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
# 712 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
# 758 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Free(void *pmem);
# 771 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_BusyWait(uint32 pause_time_us);
# 786 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 802 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 833 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
# 865 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 882 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
# 900 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
# 913 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
# 927 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 DALSYS_SetThreadPriority(uint32 priority);
# 943 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALDeviceId.h" 1
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h" 2
# 109 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
typedef uint32 DalChipInfoVersionType;






typedef uint32 DalChipInfoModemType;




typedef enum
{
  DALCHIPINFO_ID_UNKNOWN = 0,
  DALCHIPINFO_ID_MDM1000 = 1,
  DALCHIPINFO_ID_ESM6235 = 2,
  DALCHIPINFO_ID_QSC6240 = 3,
  DALCHIPINFO_ID_MSM6245 = 4,
  DALCHIPINFO_ID_MSM6255 = 5,
  DALCHIPINFO_ID_MSM6255A = 6,
  DALCHIPINFO_ID_MSM6260 = 7,
  DALCHIPINFO_ID_MSM6246 = 8,
  DALCHIPINFO_ID_QSC6270 = 9,
  DALCHIPINFO_ID_MSM6280 = 10,
  DALCHIPINFO_ID_MSM6290 = 11,
  DALCHIPINFO_ID_MSM7200 = 12,
  DALCHIPINFO_ID_MSM7201 = 13,
  DALCHIPINFO_ID_ESM7205 = 14,
  DALCHIPINFO_ID_ESM7206 = 15,
  DALCHIPINFO_ID_MSM7200A = 16,
  DALCHIPINFO_ID_MSM7201A = 17,
  DALCHIPINFO_ID_ESM7205A = 18,
  DALCHIPINFO_ID_ESM7206A = 19,
  DALCHIPINFO_ID_ESM7225 = 20,
  DALCHIPINFO_ID_MSM7225 = 21,
  DALCHIPINFO_ID_MSM7500 = 22,
  DALCHIPINFO_ID_MSM7500A = 23,
  DALCHIPINFO_ID_MSM7525 = 24,
  DALCHIPINFO_ID_MSM7600 = 25,
  DALCHIPINFO_ID_MSM7601 = 26,
  DALCHIPINFO_ID_MSM7625 = 27,
  DALCHIPINFO_ID_MSM7800 = 28,
  DALCHIPINFO_ID_MDM8200 = 29,
  DALCHIPINFO_ID_QSD8650 = 30,
  DALCHIPINFO_ID_MDM8900 = 31,
  DALCHIPINFO_ID_QST1000 = 32,
  DALCHIPINFO_ID_QST1005 = 33,
  DALCHIPINFO_ID_QST1100 = 34,
  DALCHIPINFO_ID_QST1105 = 35,
  DALCHIPINFO_ID_QST1500 = 40,
  DALCHIPINFO_ID_QST1600 = 41,
  DALCHIPINFO_ID_QST1700 = 42,
  DALCHIPINFO_ID_QSD8250 = 36,
  DALCHIPINFO_ID_QSD8550 = 37,
  DALCHIPINFO_ID_QSD8850 = 38,
  DALCHIPINFO_ID_MDM2000 = 39,
  DALCHIPINFO_ID_MSM7227 = 43,
  DALCHIPINFO_ID_MSM7627 = 44,
  DALCHIPINFO_ID_QSC6165 = 45,
  DALCHIPINFO_ID_QSC6175 = 46,
  DALCHIPINFO_ID_QSC6185 = 47,
  DALCHIPINFO_ID_QSC6195 = 48,
  DALCHIPINFO_ID_QSC6285 = 49,
  DALCHIPINFO_ID_QSC6295 = 50,
  DALCHIPINFO_ID_QSC6695 = 51,
  DALCHIPINFO_ID_ESM6246 = 52,
  DALCHIPINFO_ID_ESM6290 = 53,
  DALCHIPINFO_ID_ESC6270 = 54,
  DALCHIPINFO_ID_ESC6240 = 55,
  DALCHIPINFO_ID_MDM8220 = 56,
  DALCHIPINFO_ID_MDM9200 = 57,
  DALCHIPINFO_ID_MDM9600 = 58,
  DALCHIPINFO_ID_MSM7630 = 59,
  DALCHIPINFO_ID_MSM7230 = 60,
  DALCHIPINFO_ID_ESM7227 = 61,
  DALCHIPINFO_ID_MSM7625D1 = 62,
  DALCHIPINFO_ID_MSM7225D1 = 63,
  DALCHIPINFO_ID_QSD8250A = 64,
  DALCHIPINFO_ID_QSD8650A = 65,
  DALCHIPINFO_ID_MSM7625D2 = 66,
  DALCHIPINFO_ID_MSM7227D1 = 67,
  DALCHIPINFO_ID_MSM7627D1 = 68,
  DALCHIPINFO_ID_MSM7627D2 = 69,
  DALCHIPINFO_ID_MSM8260 = 70,
  DALCHIPINFO_ID_MSM8660 = 71,
  DALCHIPINFO_ID_MDM8200A = 72,
  DALCHIPINFO_ID_QSC6155 = 73,
  DALCHIPINFO_ID_MSM8255 = 74,
  DALCHIPINFO_ID_MSM8655 = 75,
  DALCHIPINFO_ID_ESC6295 = 76,
  DALCHIPINFO_ID_MDM3000 = 77,
  DALCHIPINFO_ID_MDM6200 = 78,
  DALCHIPINFO_ID_MDM6600 = 79,
  DALCHIPINFO_ID_MDM6210 = 80,
  DALCHIPINFO_ID_MDM6610 = 81,
  DALCHIPINFO_ID_QSD8672 = 82,
  DALCHIPINFO_ID_MDM6215 = 83,
  DALCHIPINFO_ID_MDM6615 = 84,
  DALCHIPINFO_ID_APQ8055 = 85,
  DALCHIPINFO_ID_APQ8060 = 86,
  DALCHIPINFO_ID_MSM8960 = 87,
  DALCHIPINFO_ID_MSM7225A = 88,
  DALCHIPINFO_ID_MSM7625A = 89,
  DALCHIPINFO_ID_MSM7227A = 90,
  DALCHIPINFO_ID_MSM7627A = 91,
  DALCHIPINFO_ID_ESM7227A = 92,
  DALCHIPINFO_ID_QSC6195D2 = 93,
  DALCHIPINFO_ID_FSM9200 = 94,
  DALCHIPINFO_ID_FSM9800 = 95,
  DALCHIPINFO_ID_MSM7225AD1 = 96,
  DALCHIPINFO_ID_MSM7227AD1 = 97,
  DALCHIPINFO_ID_MSM7225AA = 98,
  DALCHIPINFO_ID_MSM7225AAD1 = 99,
  DALCHIPINFO_ID_MSM7625AA = 100,
  DALCHIPINFO_ID_MSM7227AA = 101,
  DALCHIPINFO_ID_MSM7227AAD1 = 102,
  DALCHIPINFO_ID_MSM7627AA = 103,
  DALCHIPINFO_ID_MDM9615 = 104,
  DALCHIPINFO_ID_MDM9615M = DALCHIPINFO_ID_MDM9615,
  DALCHIPINFO_ID_MDM8215 = 106,
  DALCHIPINFO_ID_MDM9215 = 107,
  DALCHIPINFO_ID_MDM9215M = DALCHIPINFO_ID_MDM9215,
  DALCHIPINFO_ID_APQ8064 = 109,
  DALCHIPINFO_ID_QSC6270D1 = 110,
  DALCHIPINFO_ID_QSC6240D1 = 111,
  DALCHIPINFO_ID_ESC6270D1 = 112,
  DALCHIPINFO_ID_ESC6240D1 = 113,
  DALCHIPINFO_ID_MDM6270 = 114,
  DALCHIPINFO_ID_MDM6270D1 = 115,
  DALCHIPINFO_ID_MSM8930 = 116,
  DALCHIPINFO_ID_MSM8630 = 117,
  DALCHIPINFO_ID_MSM8230 = 118,
  DALCHIPINFO_ID_APQ8030 = 119,
  DALCHIPINFO_ID_MSM8627 = 120,
  DALCHIPINFO_ID_MSM8227 = 121,
  DALCHIPINFO_ID_MSM8660A = 122,
  DALCHIPINFO_ID_MSM8260A = 123,
  DALCHIPINFO_ID_APQ8060A = 124,
  DALCHIPINFO_ID_MPQ8062 = 125,
  DALCHIPINFO_ID_MSM8974 = 126,
  DALCHIPINFO_ID_MSM8225 = 127,
  DALCHIPINFO_ID_MSM8225D1 = 128,
  DALCHIPINFO_ID_MSM8625 = 129,
  DALCHIPINFO_ID_MPQ8064 = 130,
  DALCHIPINFO_ID_MSM7225AB = 131,
  DALCHIPINFO_ID_MSM7225ABD1 = 132,
  DALCHIPINFO_ID_MSM7625AB = 133,
  DALCHIPINFO_ID_MDM9625 = 134,
  DALCHIPINFO_ID_MSM7125A = 135,
  DALCHIPINFO_ID_MSM7127A = 136,
  DALCHIPINFO_ID_MSM8125AB = 137,
  DALCHIPINFO_ID_MSM8960AB = 138,
  DALCHIPINFO_ID_APQ8060AB = 139,
  DALCHIPINFO_ID_MSM8260AB = 140,
  DALCHIPINFO_ID_MSM8660AB = 141,
  DALCHIPINFO_ID_MSM8930AA = 142,
  DALCHIPINFO_ID_MSM8630AA = 143,
  DALCHIPINFO_ID_MSM8230AA = 144,
  DALCHIPINFO_ID_MSM8626 = 145,
  DALCHIPINFO_ID_MPQ8092 = 146,
  DALCHIPINFO_ID_MSM8610 = 147,
  DALCHIPINFO_ID_MDM8225 = 148,
  DALCHIPINFO_ID_MDM9225 = 149,
  DALCHIPINFO_ID_MDM9225M = 150,
  DALCHIPINFO_ID_MDM8225M = 151,
  DALCHIPINFO_ID_MDM9625M = 152,
  DALCHIPINFO_ID_APQ8064_V2PRIME = 153,
  DALCHIPINFO_ID_MSM8930AB = 154,
  DALCHIPINFO_ID_MSM8630AB = 155,
  DALCHIPINFO_ID_MSM8230AB = 156,
  DALCHIPINFO_ID_APQ8030AB = 157,
  DALCHIPINFO_ID_MSM8226 = 158,
  DALCHIPINFO_ID_MSM8526 = 159,
  DALCHIPINFO_ID_APQ8030AA = 160,
  DALCHIPINFO_ID_MSM8110 = 161,
  DALCHIPINFO_ID_MSM8210 = 162,
  DALCHIPINFO_ID_MSM8810 = 163,
  DALCHIPINFO_ID_MSM8212 = 164,
  DALCHIPINFO_ID_MSM8612 = 165,
  DALCHIPINFO_ID_MSM8112 = 166,
  DALCHIPINFO_ID_MSM8125 = 167,
  DALCHIPINFO_ID_MSM8225Q = 168,
  DALCHIPINFO_ID_MSM8625Q = 169,
  DALCHIPINFO_ID_MSM8125Q = 170,
  DALCHIPINFO_ID_MDM9310 = 171,
  DALCHIPINFO_ID_APQ8064_SLOW_PRIME = 172,
  DALCHIPINFO_ID_MDM8110M = 173,
  DALCHIPINFO_ID_MDM8615M = 174,
  DALCHIPINFO_ID_MDM9320 = 175,
  DALCHIPINFO_ID_MDM9225_1 = 176,
  DALCHIPINFO_ID_MDM9225M_1 = 177,
  DALCHIPINFO_ID_APQ8084 = 178,
  DALCHIPINFO_ID_MSM8130 = 179,
  DALCHIPINFO_ID_MSM8130AA = 180,
  DALCHIPINFO_ID_MSM8130AB = 181,
  DALCHIPINFO_ID_MSM8627AA = 182,
  DALCHIPINFO_ID_MSM8227AA = 183,
  DALCHIPINFO_ID_APQ8074 = 184,
  DALCHIPINFO_ID_MSM8274 = 185,
  DALCHIPINFO_ID_MSM8674 = 186,
  DALCHIPINFO_ID_MDM9635 = 187,
  DALCHIPINFO_ID_FSM9900 = 188,
  DALCHIPINFO_ID_FSM9965 = 189,
  DALCHIPINFO_ID_FSM9955 = 190,
  DALCHIPINFO_ID_FSM9950 = 191,
  DALCHIPINFO_ID_FSM9915 = 192,
  DALCHIPINFO_ID_FSM9910 = 193,
  DALCHIPINFO_ID_MSM8974_PRO = 194,
  DALCHIPINFO_ID_MSM8962 = 195,
  DALCHIPINFO_ID_MSM8262 = 196,
  DALCHIPINFO_ID_APQ8062 = 197,
  DALCHIPINFO_ID_MSM8126 = 198,
  DALCHIPINFO_ID_APQ8026 = 199,
  DALCHIPINFO_ID_MSM8926 = 200,
  DALCHIPINFO_ID_MSM8326 = 205,
  DALCHIPINFO_ID_MSM8916 = 206,
  DALCHIPINFO_ID_MSM8994 = 207,
  DALCHIPINFO_ID_APQ8074_AA = 208,
  DALCHIPINFO_ID_APQ8074_AB = 209,
  DALCHIPINFO_ID_APQ8074_PRO = 210,
  DALCHIPINFO_ID_MSM8274_AA = 211,
  DALCHIPINFO_ID_MSM8274_AB = 212,
  DALCHIPINFO_ID_MSM8274_PRO = 213,
  DALCHIPINFO_ID_MSM8674_AA = 214,
  DALCHIPINFO_ID_MSM8674_AB = 215,
  DALCHIPINFO_ID_MSM8674_PRO = 216,
  DALCHIPINFO_ID_MSM8974_AA = 217,
  DALCHIPINFO_ID_MSM8974_AB = 218,
  DALCHIPINFO_ID_APQ8028 = 219,
  DALCHIPINFO_ID_MSM8128 = 220,
  DALCHIPINFO_ID_MSM8228 = 221,
  DALCHIPINFO_ID_MSM8528 = 222,
  DALCHIPINFO_ID_MSM8628 = 223,
  DALCHIPINFO_ID_MSM8928 = 224,
  DALCHIPINFO_ID_MSM8510 = 225,
  DALCHIPINFO_ID_MSM8512 = 226,
  DALCHIPINFO_ID_MDM9630 = 227,
  DALCHIPINFO_ID_MDM9635M = DALCHIPINFO_ID_MDM9635,
  DALCHIPINFO_ID_MDM9230 = 228,
  DALCHIPINFO_ID_MDM9235M = 229,
  DALCHIPINFO_ID_MDM8630 = 230,
  DALCHIPINFO_ID_MDM9330 = 231,
  DALCHIPINFO_ID_MPQ8091 = 232,
  DALCHIPINFO_ID_MSM8936 = 233,
  DALCHIPINFO_ID_MDM9240 = 234,
  DALCHIPINFO_ID_MDM9340 = 235,
  DALCHIPINFO_ID_MDM9640 = 236,
  DALCHIPINFO_ID_MDM9245M = 237,
  DALCHIPINFO_ID_MDM9645M = 238,
  DALCHIPINFO_ID_MSM8939 = 239,
  DALCHIPINFO_ID_APQ8036 = 240,
  DALCHIPINFO_ID_APQ8039 = 241,
  DALCHIPINFO_ID_MSM8236 = 242,
  DALCHIPINFO_ID_MSM8636 = 243,
  DALCHIPINFO_ID_APQ8064_AU = 244,
  DALCHIPINFO_ID_MSM8909 = 245,
  DALCHIPINFO_ID_MSM8996 = 246,
  DALCHIPINFO_ID_APQ8016 = 247,
  DALCHIPINFO_ID_MSM8216 = 248,
  DALCHIPINFO_ID_MSM8116 = 249,
  DALCHIPINFO_ID_MSM8616 = 250,
  DALCHIPINFO_ID_MSM8992 = 251,
  DALCHIPINFO_ID_APQ8092 = 252,
  DALCHIPINFO_ID_APQ8094 = 253,
  DALCHIPINFO_ID_FSM9008 = 254,
  DALCHIPINFO_ID_FSM9010 = 255,
  DALCHIPINFO_ID_FSM9016 = 256,
  DALCHIPINFO_ID_FSM9055 = 257,
  DALCHIPINFO_ID_MSM8209 = 258,
  DALCHIPINFO_ID_MSM8208 = 259,
  DALCHIPINFO_ID_MDM9209 = 260,
  DALCHIPINFO_ID_MDM9309 = 261,
  DALCHIPINFO_ID_MDM9609 = 262,
  DALCHIPINFO_ID_MSM8239 = 263,
  DALCHIPINFO_ID_MSM8952 = 264,
  DALCHIPINFO_ID_APQ8009 = 265,
  DALCHIPINFO_ID_MSM8956 = 266,
  DALCHIPINFO_ID_QDF2432 = 267,
  DALCHIPINFO_ID_MSM8929 = 268,
  DALCHIPINFO_ID_MSM8629 = 269,
  DALCHIPINFO_ID_MSM8229 = 270,
  DALCHIPINFO_ID_APQ8029 = 271,
  DALCHIPINFO_ID_QCA9618 = 272,
  DALCHIPINFO_ID_IPQ4018 = DALCHIPINFO_ID_QCA9618,
  DALCHIPINFO_ID_QCA9619 = 273,
  DALCHIPINFO_ID_IPQ4019 = DALCHIPINFO_ID_QCA9619,
  DALCHIPINFO_ID_APQ8056 = 274,
  DALCHIPINFO_ID_MSM8609 = 275,
  DALCHIPINFO_ID_FSM9916 = 276,
  DALCHIPINFO_ID_APQ8076 = 277,
  DALCHIPINFO_ID_MSM8976 = 278,
  DALCHIPINFO_ID_MDM9650 = 279,
  DALCHIPINFO_ID_IPQ8065 = 280,
  DALCHIPINFO_ID_IPQ8069 = 281,
  DALCHIPINFO_ID_MSM8939_BC = 282,
  DALCHIPINFO_ID_MDM9250 = 283,
  DALCHIPINFO_ID_MDM9255 = 284,
  DALCHIPINFO_ID_MDM9350 = 285,
  DALCHIPINFO_ID_MDM9655 = 286,
  DALCHIPINFO_ID_IPQ4028 = 287,
  DALCHIPINFO_ID_IPQ4029 = 288,
  DALCHIPINFO_ID_APQ8052 = 289,
  DALCHIPINFO_ID_MDM9607 = 290,
  DALCHIPINFO_ID_APQ8096 = 291,
  DALCHIPINFO_ID_MSM8998 = 292,
  DALCHIPINFO_ID_MSM8953 = 293,
  DALCHIPINFO_ID_MSM8937 = 294,
  DALCHIPINFO_ID_APQ8037 = 295,
  DALCHIPINFO_ID_MDM8207 = 296,
  DALCHIPINFO_ID_MDM9207 = 297,
  DALCHIPINFO_ID_MDM9307 = 298,
  DALCHIPINFO_ID_MDM9628 = 299,
  DALCHIPINFO_ID_MSM8909W = 300,
  DALCHIPINFO_ID_APQ8009W = 301,
  DALCHIPINFO_ID_MSM8996L = 302,
  DALCHIPINFO_ID_MSM8917 = 303,
  DALCHIPINFO_ID_APQ8053 = 304,
  DALCHIPINFO_ID_MSM8996SG = 305,
  DALCHIPINFO_ID_MSM8997 = 306,
  DALCHIPINFO_ID_APQ8017 = 307,
  DALCHIPINFO_ID_MSM8217 = 308,
  DALCHIPINFO_ID_MSM8617 = 309,
  DALCHIPINFO_ID_MSM8996AU = 310,
  DALCHIPINFO_ID_APQ8096AU = 311,
  DALCHIPINFO_ID_APQ8096SG = 312,
  DALCHIPINFO_ID_MSM8940 = 313,
  DALCHIPINFO_ID_MDM9665 = 314,
  DALCHIPINFO_ID_MSM8996SGAU = 315,
  DALCHIPINFO_ID_APQ8096SGAU = 316,

  DALCHIPINFO_NUM_IDS = 317,
  DALCHIPINFO_ID_32BITS = 0x7FFFFFF
} DalChipInfoIdType;





typedef enum
{
  DALCHIPINFO_FAMILY_UNKNOWN = 0,
  DALCHIPINFO_FAMILY_MSM6246 = 1,
  DALCHIPINFO_FAMILY_MSM6260 = 2,
  DALCHIPINFO_FAMILY_QSC6270 = 3,
  DALCHIPINFO_FAMILY_MSM6280 = 4,
  DALCHIPINFO_FAMILY_MSM6290 = 5,
  DALCHIPINFO_FAMILY_MSM7200 = 6,
  DALCHIPINFO_FAMILY_MSM7500 = 7,
  DALCHIPINFO_FAMILY_MSM7600 = 8,
  DALCHIPINFO_FAMILY_MSM7625 = 9,
  DALCHIPINFO_FAMILY_MSM7X30 = 10,
  DALCHIPINFO_FAMILY_MSM7800 = 11,
  DALCHIPINFO_FAMILY_MDM8200 = 12,
  DALCHIPINFO_FAMILY_QSD8650 = 13,
  DALCHIPINFO_FAMILY_MSM7627 = 14,
  DALCHIPINFO_FAMILY_QSC6695 = 15,
  DALCHIPINFO_FAMILY_MDM9X00 = 16,
  DALCHIPINFO_FAMILY_QSD8650A = 17,
  DALCHIPINFO_FAMILY_MSM8X60 = 18,
  DALCHIPINFO_FAMILY_MDM8200A = 19,
  DALCHIPINFO_FAMILY_QSD8672 = 20,
  DALCHIPINFO_FAMILY_MDM6615 = 21,
  DALCHIPINFO_FAMILY_MSM8660 = DALCHIPINFO_FAMILY_MSM8X60,
  DALCHIPINFO_FAMILY_MSM8960 = 22,
  DALCHIPINFO_FAMILY_MSM7625A = 23,
  DALCHIPINFO_FAMILY_MSM7627A = 24,
  DALCHIPINFO_FAMILY_MDM9X15 = 25,
  DALCHIPINFO_FAMILY_MSM8930 = 26,
  DALCHIPINFO_FAMILY_MSM8630 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8230 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_APQ8030 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8627 = 30,
  DALCHIPINFO_FAMILY_MSM8227 = DALCHIPINFO_FAMILY_MSM8627,
  DALCHIPINFO_FAMILY_MSM8974 = 32,
  DALCHIPINFO_FAMILY_MSM8625 = 33,
  DALCHIPINFO_FAMILY_MSM8225 = DALCHIPINFO_FAMILY_MSM8625,
  DALCHIPINFO_FAMILY_APQ8064 = 34,
  DALCHIPINFO_FAMILY_MDM9x25 = 35,
  DALCHIPINFO_FAMILY_MSM8960AB = 36,
  DALCHIPINFO_FAMILY_MSM8930AB = 37,
  DALCHIPINFO_FAMILY_MSM8x10 = 38,
  DALCHIPINFO_FAMILY_MPQ8092 = 39,
  DALCHIPINFO_FAMILY_MSM8x26 = 40,
  DALCHIPINFO_FAMILY_MSM8225Q = 41,
  DALCHIPINFO_FAMILY_MSM8625Q = 42,
  DALCHIPINFO_FAMILY_APQ8x94 = 43,
  DALCHIPINFO_FAMILY_APQ8084 = DALCHIPINFO_FAMILY_APQ8x94,
  DALCHIPINFO_FAMILY_MSM8x32 = 44,
  DALCHIPINFO_FAMILY_MDM9x35 = 45,
  DALCHIPINFO_FAMILY_MSM8974_PRO= 46,
  DALCHIPINFO_FAMILY_FSM9900 = 47,
  DALCHIPINFO_FAMILY_MSM8x62 = 48,
  DALCHIPINFO_FAMILY_MSM8926 = 49,
  DALCHIPINFO_FAMILY_MSM8994 = 50,
  DALCHIPINFO_FAMILY_IPQ8064 = 51,
  DALCHIPINFO_FAMILY_MSM8916 = 52,
  DALCHIPINFO_FAMILY_MSM8936 = 53,
  DALCHIPINFO_FAMILY_MDM9x45 = 54,
  DALCHIPINFO_FAMILY_MSM8996 = 56,
  DALCHIPINFO_FAMILY_APQ8096 = DALCHIPINFO_FAMILY_MSM8996,
  DALCHIPINFO_FAMILY_MSM8992 = 57,
  DALCHIPINFO_FAMILY_MSM8909 = 58,
  DALCHIPINFO_FAMILY_FSM90xx = 59,
  DALCHIPINFO_FAMILY_MSM8952 = 60,
  DALCHIPINFO_FAMILY_QDF2432 = 61,
  DALCHIPINFO_FAMILY_MSM8929 = 62,
  DALCHIPINFO_FAMILY_MSM8956 = 63,
  DALCHIPINFO_FAMILY_QCA961x = 64,
  DALCHIPINFO_FAMILY_IPQ40xx = DALCHIPINFO_FAMILY_QCA961x,
  DALCHIPINFO_FAMILY_MDM9x55 = 65,
  DALCHIPINFO_FAMILY_MDM9x07 = 66,
  DALCHIPINFO_FAMILY_MSM8998 = 67,
  DALCHIPINFO_FAMILY_MSM8953 = 68,
  DALCHIPINFO_FAMILY_MSM8993 = 69,
  DALCHIPINFO_FAMILY_MSM8937 = 70,
  DALCHIPINFO_FAMILY_MSM8917 = 71,
  DALCHIPINFO_FAMILY_MSM8996SG = 72,
  DALCHIPINFO_FAMILY_MSM8997 = 73,

  DALCHIPINFO_NUM_FAMILIES = 74,
  DALCHIPINFO_FAMILY_32BITS = 0x7FFFFFF
} DalChipInfoFamilyType;
# 540 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
typedef struct DalChipInfo DalChipInfo;
struct DalChipInfo
{
   DalDevice DalChipInfoDevice;
   DALResult (*GetChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoVersionType * pnVersion);
   DALResult (*GetRawChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnVersion);
   DALResult (*GetChipId)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoIdType * peId);
   DALResult (*GetRawChipId)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnId);
   DALResult (*GetChipIdString)(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength);
   DALResult (*GetChipFamily)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoFamilyType * peFamily);
   DALResult (*GetModemSupport)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoModemType * pnModem);
};

typedef struct DalChipInfoHandle DalChipInfoHandle;
struct DalChipInfoHandle
{
   uint32 dwDalHandleId;
   const DalChipInfo * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
# 594 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetChipVersion(DalDeviceHandle * _h, DalChipInfoVersionType * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipVersion( _h, 0, pnVersion);
}
# 623 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetRawChipVersion(DalDeviceHandle * _h, uint32 * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipVersion( _h, 0, pnVersion);
}
# 654 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetChipId(DalDeviceHandle * _h, DalChipInfoIdType * peId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipId( _h, 0, peId);
}
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetRawChipId(DalDeviceHandle * _h, uint32 * pnId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipId( _h, 0, pnId);
}
# 717 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetChipIdString(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_9(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipIdString)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (char * )szIdString, nMaxLength);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipIdString( _h, szIdString, nMaxLength);
}
# 747 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetChipFamily(DalDeviceHandle * _h, DalChipInfoFamilyType * peFamily)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipFamily)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peFamily);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipFamily( _h, 0, peFamily);
}
# 777 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DALResult
DalChipInfo_GetModemSupport(DalDeviceHandle * _h, DalChipInfoModemType * pnModem)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetModemSupport)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnModem);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetModemSupport( _h, 0, pnModem);
}
# 807 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DalChipInfoVersionType
DalChipInfo_ChipVersion(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoVersionType nVersion;

  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);

    if (eResult != 0)
    {
      return (DalChipInfoVersionType)0;
    }
  }

  DalChipInfo_GetChipVersion(phChipInfo, &nVersion);

  return nVersion;

}
# 850 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DalChipInfoIdType
DalChipInfo_ChipId(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoIdType neId;

  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);

    if (eResult != 0)
    {
      return DALCHIPINFO_ID_UNKNOWN;
    }
  }

  DalChipInfo_GetChipId(phChipInfo, &neId);

  return neId;

}
# 892 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIChipInfo.h"
static __inline DalChipInfoFamilyType
DalChipInfo_ChipFamily(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoFamilyType neFamily;

  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);

    if (eResult != 0)
    {
      return DALCHIPINFO_FAMILY_UNKNOWN;
    }
  }

  DalChipInfo_GetChipFamily(phChipInfo, &neFamily);

  return neFamily;

}
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/VCSDefs.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/VCSDefs.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;


typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),


  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),


  NPA_CLIENT_REQUIRED = (1 << 6),

  NPA_CLIENT_ISOCHRONOUS = (1 << 7),

  NPA_CLIENT_IMPULSE = (1 << 8),


  NPA_CLIENT_LIMIT_MAX = (1 << 9),

  NPA_CLIENT_VECTOR = (1 << 10),



  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),


  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),



  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),

} npa_client_type;




typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,

  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,



  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,


  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,

  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,

  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,

  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,

  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;




typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,


  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,


  NPA_EVENT_CHANGE,


  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,



  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,


  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,

  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,

  NPA_NUM_EVENT_TYPES
} npa_event_type;
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,


  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,






  NPA_REQUEST_NEXT_AWAKE = 0x00000002,






  NPA_REQUEST_CHANGED_TYPE = 0x00000004,






  NPA_REQUEST_BEST_EFFORT = 0x00000008,


  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,

} npa_request_attribute;
# 210 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
enum {

  NPA_QUERY_CURRENT_STATE,

  NPA_QUERY_CLIENT_ACTIVE_REQUEST,



  NPA_QUERY_ACTIVE_MAX,



  NPA_QUERY_RESOURCE_MAX,


  NPA_QUERY_RESOURCE_DISABLED,

  NPA_QUERY_RESOURCE_LATENCY,

  NPA_QUERY_CURRENT_AGGREGATION,


  NPA_MAX_PUBLIC_QUERY = 1023,


  NPA_QUERY_RESERVED_BEGIN = 1024,


  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,

  NPA_QUERY_RESERVED_END = 4095


};


typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;





typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;

typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;


typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );

typedef void ( *npa_simple_callback )( void *context );


typedef struct npa_client * npa_client_handle;


typedef struct npa_event * npa_event_handle;


typedef struct npa_custom_event * npa_custom_event_handle;


typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;


typedef struct npa_prepost_change_data
{
  const char *resource_name;


  npa_resource_state from_state;
  npa_resource_state to_state;


  void *data;
} npa_prepost_change_data;


typedef struct npa_link * npa_query_handle;





typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,

   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;


typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
# 391 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
# 491 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
# 566 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_client( npa_client_handle client );
# 579 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
# 614 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
# 660 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
# 729 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_impulse_request( npa_client_handle client );
# 763 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
# 848 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
# 869 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
# 887 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_complete_request( npa_client_handle client );
# 906 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_cancel_request( npa_client_handle client );
# 917 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
# 941 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_client_type_required( npa_client_handle client );
# 964 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_client_type_suppressible( npa_client_handle client );
# 1012 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
# 1097 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
# 1127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
# 1168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
# 1188 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_custom_event( npa_event_handle event );
# 1206 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_event_handle( npa_event_handle event );
# 1224 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_handle npa_create_query_handle( const char * resource_name );
# 1237 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_query_handle( npa_query_handle query );
# 1254 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
# 1283 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
# 1302 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
# 1321 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
# 1343 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_resource_available( const char *resource_name );
# 1364 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
# 1389 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
# 1409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
# 1602 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
# 1617 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef unsigned int (*npa_join_function) ( void *, void * );






void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );







npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );







void npa_join_request( npa_client_handle client );
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h" 2
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef enum
{

  NPA_RESOURCE_DEFAULT = 0,


  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,


  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,


  NPA_RESOURCE_VECTOR_STATE = 0x00000004,


  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,


  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,



  NPA_RESOURCE_REMOTE = 0x00000010,



  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,


  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,





  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,



  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,


  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,


  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,


  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,

} npa_resource_attribute;


typedef enum
{
  NPA_NODE_DEFAULT = 0,



  NPA_NODE_NO_LOCK = 0x00000001,


  NPA_NODE_DISABLEABLE = 0x00000002,


  NPA_NODE_FORKABLE = 0x00000004,

} npa_node_attribute;




enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,

  NPA_QUERY_RESOURCE_ATTRIBUTES,

  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};






typedef void* npa_user_data;



typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;



typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;



typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];

  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;

  unsigned int request_attr;




  void (*issue_request)( npa_client_handle client, int new_request );



  struct npa_scheduler_data *request_ptr;
} npa_client;
# 215 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;


  const char *name;
  struct npa_resource *resource;

  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;

  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;

  npa_event_callback callback;



  void *reg_data;



  struct npa_event_action *action;
} npa_event;
# 263 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
# 280 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );







typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );



typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
# 320 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
# 344 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );



typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
# 366 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  void (*create_client_fcn) ( npa_client * );





  void (*destroy_client_fcn)( npa_client * );
# 388 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
# 411 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
# 441 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;

  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;



  struct npa_resource *handle;
} npa_resource_definition;
# 467 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;



typedef struct npa_resource
{

  npa_resource_definition *definition;


  npa_node_definition *node;


  unsigned int index;

  npa_client *clients;


  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;





  const npa_resource_plugin *active_plugin;




  npa_resource_state request_state;


  npa_resource_state active_state;



  npa_resource_state internal_state[8];





  npa_resource_state *state_vector;
# 532 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  npa_resource_state *required_state_vector;






  npa_resource_state *suppressible_state_vector;






  npa_resource_state *suppressible2_state_vector;
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  npa_resource_state *semiactive_state_vector;

  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;


  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;


  struct npa_resource_internal_data *_internal;


  unsigned int sequence;


  void *log_handle;



  npa_resource_latency *latency;
} npa_resource;



typedef void* npa_join_token;
# 621 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
# 649 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
# 678 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_marker( const char *marker_name );
# 710 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
# 763 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
# 792 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
# 834 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
# 888 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
# 915 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_disable_node( npa_node_definition *node );
# 925 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
# 941 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_resource_lock( npa_resource *resource );
# 955 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
int npa_resource_trylock( npa_resource *resource );
# 965 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_resource_unlock( npa_resource *resource );
# 981 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
# 992 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
unsigned int npa_get_request_attributes( npa_client_handle client );
# 1008 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
# 1034 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
# 1052 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
extern const npa_resource_plugin npa_binary_plugin;


extern const npa_resource_plugin npa_max_plugin;


extern const npa_resource_plugin npa_min_plugin;


extern const npa_resource_plugin npa_sum_plugin;







extern const npa_resource_plugin npa_identity_plugin;


extern const npa_resource_plugin npa_always_on_plugin;


extern const npa_resource_plugin npa_impulse_plugin;


extern const npa_resource_plugin npa_or_plugin;



extern const npa_resource_plugin npa_binary_and_plugin;


extern const npa_resource_plugin npa_no_client_plugin;
# 1094 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);


npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );


npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );


npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );


npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);


npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);


npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );


npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );


npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
# 1181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_internal_request( npa_client_handle client );
# 1210 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
# 1229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
# 1245 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
# 1266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
# 1281 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
# 1297 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
# 1315 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
# 1332 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
# 1351 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
# 1371 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
# 1393 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/VCSDefs.h" 2
# 61 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/VCSDefs.h"
typedef enum
{
  VCS_RAIL_APCS,
  VCS_RAIL_CX,
  VCS_RAIL_MSS,
  VCS_RAIL_MX,

  VCS_RAIL_NUM_OF_RAILS
} VCSRailType;





typedef enum
{
  VCS_CORNER_OFF = 0,
  VCS_CORNER_RETENTION = 1,
  VCS_CORNER_LOW_MINUS = 2,
  VCS_CORNER_LOW = 3,
  VCS_CORNER_LOW_PLUS = 4,
  VCS_CORNER_NOMINAL = 5,
  VCS_CORNER_NOMINAL_PLUS = 6,
  VCS_CORNER_TURBO = 7,
  VCS_CORNER_MAX = VCS_CORNER_TURBO,

  VCS_CORNER_NUM_OF_CORNERS
} VCSCornerType;





typedef enum
{
  VCS_RAIL_MODE_CPR = 0,
  VCS_RAIL_MODE_MVC = 1,

  VCS_RAIL_MODE_NUM_OF_MODES
} VCSRailModeType;





typedef enum
{
  VCS_NPA_RAIL_EVENT_PRE_CHANGE = NPA_TRIGGER_CUSTOM_EVENT1,
  VCS_NPA_RAIL_EVENT_POST_CHANGE = NPA_TRIGGER_CUSTOM_EVENT2,
  VCS_NPA_RAIL_EVENT_LIMIT_MAX = NPA_TRIGGER_CUSTOM_EVENT3,
} VCSNPARailEventType;





typedef enum
{
  VCS_NPA_LDO_EVENT_PRE_CHANGE = NPA_TRIGGER_CUSTOM_EVENT1,
  VCS_NPA_LDO_EVENT_POST_CHANGE = NPA_TRIGGER_CUSTOM_EVENT2,
} VCSNPALDOEventType;





typedef struct
{
  struct
  {
    VCSCornerType eCorner;

  } PreChange;
  struct
  {
    VCSCornerType eCorner;

  } PostChange;
} VCSNPARailEventDataType;





typedef struct
{
  struct
  {
    VCSCornerType eCorner;
    uint32 nVoltageUV;
  } PreChange;
  struct
  {
    VCSCornerType eCorner;
    uint32 nVoltageUV;
  } PostChange;
} VCSNPALDOEventDataType;
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h" 2
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  struct
  {
    uint8 nMajor;
    uint8 nMinor;
  } Min;

  struct
  {
    uint8 nMajor;
    uint8 nMinor;
  } Max;

  DalChipInfoFamilyType eChipInfoFamily;
  const DalChipInfoIdType *aeChipInfoId;

} ClockHWVersionType;
# 225 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  uint32 nFreqHz;
  HAL_clk_PLLConfigType HALConfig;
  VCSCornerType eVRegLevel;
  ClockHWVersionType HWVersion;
} ClockSourceFreqConfigType;
# 247 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  HAL_clk_SourceType eSource;
  const char *szName;
  uint32 nConfigMask;
  ClockSourceFreqConfigType *pSourceFreqConfig;
  ClockSourceFreqConfigType *pCalibrationFreqConfig;
  HAL_clk_SourceDisableModeType eDisableMode;
} ClockSourceConfigType;
# 269 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  uint32 nFreqHz;
  HAL_clk_ClockMuxConfigType HALConfig;
  VCSCornerType eVRegLevel;
  ClockHWVersionType HWVersion;
  ClockSourceFreqConfigType *pSourceFreqConfig;
} ClockMuxConfigType;
# 287 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  uint32 nLogSize;
  uint8 nGlobalLogFlags;
} ClockLogType;







typedef struct
{
  const char *szName;
} ClockXOVoterType;
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef const void *ClockPropertyValueType;
# 323 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  const char *szName;
  ClockPropertyValueType Value;
} ClockPropertyType;
# 341 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef enum
{
  CLOCK_FLAG_NODE_TYPE_NONE = 0,
  CLOCK_FLAG_NODE_TYPE_CLOCK = 1,
  CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN = 2,
  CLOCK_FLAG_NODE_TYPE_SOURCE = 3,
  CLOCK_FLAG_NODE_TYPE_POWER_DOMAIN = 4,
  CLOCK_FLAG_NODE_TYPE_MAX_SUPPORTED = 5
} ClockFlagNodeType;
# 363 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  ClockFlagNodeType eNodeType;
  void *pID;
  uint32 nFlag;
} ClockFlagInitType;
# 380 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/src/ClockBSP.h"
typedef struct
{
  boolean bRUMI;
  boolean bVirtio;
} ClockStubType;
# 32 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h" 1
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h" 1
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
typedef struct npa_remote_resource_definition
{
  char *local_resource_name;
  char *remote_resource_name;
  char *protocol_type;
  const npa_resource_plugin *plugin;
  npa_resource_driver_fcn driver_fcn;
  const char *units;
  npa_resource_state max;
  unsigned int attributes;

  npa_user_data data;



  npa_resource *handle;
} npa_remote_resource_definition;
# 69 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
typedef struct npa_remote_node_continuation
{
  npa_node_definition *node;
  npa_resource_state initial_state;
  npa_callback callback;
  npa_user_data cb_data;
  npa_remote_resource_definition *definition;
} npa_remote_node_continuation;
# 105 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
void npa_remote_define_resource_cb( npa_remote_resource_definition *resource,
                                    npa_resource_state initial_state,
                                    npa_callback define_callback,
                                    npa_user_data define_context );
# 132 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
npa_resource_state
npa_remote_resource_local_aggregation_driver_fcn(
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );

npa_resource_state
npa_remote_resource_local_aggregation_no_initial_request_driver_fcn (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );

npa_resource_state
npa_remote_resource_remote_aggregation_driver_fcn (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
# 162 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
void npa_remote_resource_available( void *context );

typedef enum
{
  NPA_PD_0 = 0,
  NPA_PD_1 = (1 << 0),
  NPA_PD_2 = (1 << 1),

  NPA_ALL_PDS = (int)0xFFFFFFFF
} npa_remote_domain_id;
# 199 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
void npa_remote_publish_resource( unsigned int remote_domains,
                                  const char *resource_name );
# 214 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_remote_resource.h"
void npa_remote_publish_resources( unsigned int remote_domains,
                                   unsigned int num_resources,
                                   const char *resources[] );
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  HAL_clk_ClockConfigType CoreConfig;
  ClockMuxConfigType Mux;
  VCSCornerType eCornerCX;
  uint32 nStrapACCVal;
  uint32 nSleepDiv2x;
} ClockCPUConfigType;
# 102 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  ClockHWVersionType HWVersion;
  uint32 nMinPerfLevel;
  uint32 nMaxPerfLevel;
  uint32 *anPerfLevel;
  uint32 nNumPerfLevels;
} ClockCPUPerfConfigType;
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  HAL_clk_SourceType nSource;
  boolean bInitialize;
} ClockImagePLLConfigType;
# 137 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  boolean bEnableDCS;
  ClockCPUConfigType *pCPUConfig;
  ClockCPUPerfConfigType *pCPUPerfConfig;
  uint32 nNumCPUPerfLevelConfigs;


} ClockImageBSPConfigType;
# 154 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  HAL_clk_SourceType eSource;
  uint32 nFreqHz;
}ClockSourceInitType;
# 168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
  boolean bOCMEM;

}ClockNPARemoteNodeSupportType;
# 184 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/hw/msm8996/inc/ClockLPASSCPU.h"
typedef struct
{
   uint32 nDomain;
   const char** ppszResourceList;
   uint32 nTotalResources;
}ClockNPAResourcePubType;
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 1
# 152 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/target.h" 1
# 77 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/target.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h"
# 1 "./custaaaaaaaaq.h" 1
# 11 "./custaaaaaaaaq.h"
# 1 "./targaaaaaaaaq.h" 1
# 12 "./custaaaaaaaaq.h" 2
# 32 "./custaaaaaaaaq.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/custtarget.h" 1
# 33 "./custaaaaaaaaq.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h" 2
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/target.h" 2
# 153 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/armasm.h" 1
# 154 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 2
# 186 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
typedef void * addr_t;
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
# 806 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
# 1095 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c" 2
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    .nFreqHz = 19200 * 1000,
    .HALConfig = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0, 0}, {0xFF, 0xFF} },
  },

  { 0 }
};







static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    .nFreqHz = 600000 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO3,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 31,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0x40,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0, 0}, {0xFF, 0xFF} },
  },

  { 0 }
};







static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL0[] =
{
  {
    .nFreqHz = 564480 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO3,
      .nPreDiv = 1,
      .nPostDiv = 2,
      .nL = 29,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0x66666666,
      .nAlphaU = 0x00000066,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0, 0}, {0xFF, 0xFF} },
  },

  { 0 }
};







static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL1[] =
{
  {
    .nFreqHz = 249600 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO4,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 13,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0x00, 0x00}, {0x02, 0x00} },
  },
  {
    .nFreqHz = 345600 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO4,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 18,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0x00, 0x00}, {0x02, 0x00} },
  },

  {
    .nFreqHz = 364800 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO4,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 19,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0x00, 0x00}, {0x02, 0x00} },
  },

  {
    .nFreqHz = 422400 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO4,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 22,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0x00, 0x00}, {0x02, 0x00} },
  },
  {
    .nFreqHz = 480000 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO4,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 25,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0x00, 0x00}, {0x02, 0x00} },
  },





  {
                         499200 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               26,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         537600 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               28,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         595200 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               31,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         633600 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               33,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         652800 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               34,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },




  {
                         710400 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               37,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         729600 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               38,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         825600 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               43,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         844800 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               44,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },
  {
                         960000 * 1000,

    {
                               HAL_CLK_SOURCE_XO,
                               HAL_CLK_PLL_VCO3,
                               1,
                               1,
                               50,
                               0,
                               0,
                               0,
                               0x00000000,
                               0x00,
    },
                         VCS_CORNER_LOW_MINUS,
                         { {0x02, 0x00}, {0xFF, 0xFF} },
  },


  { 0 }
};

const ClockSourceFreqConfigType CPUCalibrationFreqV2 =
{
                       710400 * 1000,

  {
                             HAL_CLK_SOURCE_XO,
                             HAL_CLK_PLL_VCO3,
                             1,
                             1,
                             37,
                             0,
                             0,
                             0,
                             0x00000000,
                             0x00,
  },
                       VCS_CORNER_LOW_MINUS,
                       { {0x02, 0x00}, {0x03, 0x00} },
};


const ClockSourceFreqConfigType CPUCalibrationFreqV3 =
{
                       729600 * 1000,

  {
                             HAL_CLK_SOURCE_XO,
                             HAL_CLK_PLL_VCO3,
                             1,
                             1,
                             38,
                             0,
                             0,
                             0,
                             0x00000000,
                             0x00,
  },
                       VCS_CORNER_LOW_MINUS,
                       { {0x03, 0x00}, {0xFF, 0xFF} },
};







static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL2[] =
{
  {
    .nFreqHz = 614400 * 1000,
    .HALConfig =
    {
      .eSource = HAL_CLK_SOURCE_XO,
      .eVCO = HAL_CLK_PLL_VCO3,
      .nPreDiv = 1,
      .nPostDiv = 1,
      .nL = 32,
      .nM = 0,
      .nN = 0,
      .nVCOMultiplier = 0,
      .nAlpha = 0,
      .nAlphaU = 0,
    },
    .eVRegLevel = VCS_CORNER_LOW_MINUS,
    .HWVersion = { {0, 0}, {0xFF, 0xFF} },
  },

  { 0 }
};







const ClockSourceInitType ClockSourcesToInit[] =
{
   { HAL_CLK_SOURCE_LPAPLL0, 564480 * 1000 },
   { HAL_CLK_SOURCE_LPAPLL2, 614400 * 1000 },
   { HAL_CLK_SOURCE_NULL, 0}
};






const ClockSourceConfigType SourceConfig[] =
{
  {
    .eSource = HAL_CLK_SOURCE_XO, .szName = "XO",

    .nConfigMask = 0,
    .pSourceFreqConfig = SourceFreqConfig_XO,
  },
  {
    .eSource = HAL_CLK_SOURCE_GPLL0, .szName = "GPLL0",

    .nConfigMask = (1 << 0),
    .pSourceFreqConfig = SourceFreqConfig_GPLL0,
  },
  {
    .eSource = HAL_CLK_SOURCE_LPAPLL0, .szName = "LPAPLL0",

    .nConfigMask = (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3),
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL0,
  },
  {
    .eSource = HAL_CLK_SOURCE_LPAPLL1, .szName = "LPAPLL1",

    .nConfigMask = (1 << 1) | (1 << 2) | (1 << 3),
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL1,
    .pCalibrationFreqConfig = &SourceFreqConfig_LPAPLL1[2],
    .eDisableMode = HAL_CLK_SOURCE_DISABLE_MODE_FREEZE
  },
  {
    .eSource = HAL_CLK_SOURCE_LPAPLL2, .szName = "LPAPLL2",

    .nConfigMask = (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3),
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL2,
  },


  { .eSource = HAL_CLK_SOURCE_NULL, .szName = "NULL" }

};
# 548 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
const ClockMuxConfigType BLSP1QUP1I2CAPPSClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 50000000, { HAL_CLK_SOURCE_GPLL0, 24, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 0 }
};





const ClockMuxConfigType BLSP1QUP1SPIAPPSClockConfig[] =
{
  { 960000, { HAL_CLK_SOURCE_XO, 20, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 4800000, { HAL_CLK_SOURCE_XO, 8, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 9600000, { HAL_CLK_SOURCE_XO, 4, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 15000000, { HAL_CLK_SOURCE_GPLL0, 20, 1, 4, 4 }, VCS_CORNER_LOW, },
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 25000000, { HAL_CLK_SOURCE_GPLL0, 24, 1, 2, 2 }, VCS_CORNER_LOW, },
  { 50000000, { HAL_CLK_SOURCE_GPLL0, 24, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType BLSP1UART1APPSClockConfig[] =
{
  { 3686400, { HAL_CLK_SOURCE_GPLL0, 2, 96, 15625, 15625 }, VCS_CORNER_LOW, },
  { 7372800, { HAL_CLK_SOURCE_GPLL0, 2, 192, 15625, 15625 }, VCS_CORNER_LOW, },
  { 14745600, { HAL_CLK_SOURCE_GPLL0, 2, 384, 15625, 15625 }, VCS_CORNER_LOW, },
  { 16000000, { HAL_CLK_SOURCE_GPLL0, 10, 2, 15, 15 }, VCS_CORNER_LOW, },
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 24000000, { HAL_CLK_SOURCE_GPLL0, 10, 1, 5, 5 }, VCS_CORNER_LOW, },
  { 32000000, { HAL_CLK_SOURCE_GPLL0, 2, 4, 75, 75 }, VCS_CORNER_NOMINAL, },
  { 40000000, { HAL_CLK_SOURCE_GPLL0, 30, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 46400000, { HAL_CLK_SOURCE_GPLL0, 2, 29, 375, 375 }, VCS_CORNER_NOMINAL, },
  { 48000000, { HAL_CLK_SOURCE_GPLL0, 25, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 51200000, { HAL_CLK_SOURCE_GPLL0, 2, 32, 375, 375 }, VCS_CORNER_NOMINAL, },
  { 56000000, { HAL_CLK_SOURCE_GPLL0, 2, 7, 75, 75 }, VCS_CORNER_NOMINAL, },
  { 58982400, { HAL_CLK_SOURCE_GPLL0, 2, 1536, 15625, 15625 }, VCS_CORNER_NOMINAL, },
  { 60000000, { HAL_CLK_SOURCE_GPLL0, 20, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 63157895, { HAL_CLK_SOURCE_GPLL0, 19, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
const ClockMuxConfigType AONClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 76800000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 153600000, { HAL_CLK_SOURCE_LPAPLL2, 4, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType ATIMEClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 30720000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 30720000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 30720000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 61440000, { HAL_CLK_SOURCE_LPAPLL2, 4, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 122880000, { HAL_CLK_SOURCE_LPAPLL2, 2, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType AUDSLIMBUSClockConfig[] =
{
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 0 }
};





const ClockMuxConfigType COREClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 38400000, { HAL_CLK_SOURCE_LPAPLL2, 16, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 76800000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 153600000, { HAL_CLK_SOURCE_LPAPLL2, 4, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType EXTMCLK0ClockConfig[] =
{
  { 352800, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 32, 32 }, VCS_CORNER_LOW_MINUS, },
  { 512000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 705600, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 768000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 1024000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 1411200, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 1536000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 2048000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 2822400, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 3072000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 4096000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 5644800, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 6144000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 8192000, { HAL_CLK_SOURCE_LPAPLL2, 30, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 11289600, { HAL_CLK_SOURCE_LPAPLL0, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2, 20, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType LPAIFPCMOEClockConfig[] =
{
  { 352800, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 32, 32 }, VCS_CORNER_LOW_MINUS, },
  { 512000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 705600, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 768000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 16, 16 }, VCS_CORNER_LOW_MINUS, },
  { 1024000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 1411200, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 1536000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 8, 8 }, VCS_CORNER_LOW_MINUS, },
  { 2048000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 2822400, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 3072000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 4, 4 }, VCS_CORNER_LOW_MINUS, },
  { 4096000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 5644800, { HAL_CLK_SOURCE_LPAPLL0, 10, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 6144000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1, 2, 2 }, VCS_CORNER_LOW_MINUS, },
  { 8192000, { HAL_CLK_SOURCE_LPAPLL2, 30, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 8192000, { HAL_CLK_SOURCE_LPAPLL2, 30, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 8192000, { HAL_CLK_SOURCE_LPAPLL2, 30, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 11289600, { HAL_CLK_SOURCE_LPAPLL0, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2, 20, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2, 20, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2, 20, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2, 10, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 61440000, { HAL_CLK_SOURCE_LPAPLL2, 4, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 122880000, { HAL_CLK_SOURCE_LPAPLL2, 2, 0, 0, 0 }, VCS_CORNER_NOMINAL },

  { 0 }
};





const ClockMuxConfigType RESAMPLERClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 76800000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW, { {0x00, 0x00}, {0x02, 0x00}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 76800000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x00, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996SG }},
  { 76800000, { HAL_CLK_SOURCE_LPAPLL2, 8, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, { {0x02, 0x00}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_MSM8996 }},
  { 153600000, { HAL_CLK_SOURCE_LPAPLL2, 4, 0, 0, 0 }, VCS_CORNER_LOW, },
  { 307200000, { HAL_CLK_SOURCE_LPAPLL2, 2, 0, 0, 0 }, VCS_CORNER_NOMINAL, },
  { 0 }
};





const ClockMuxConfigType XOClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 }, VCS_CORNER_LOW_MINUS, },
  { 0 }
};
# 752 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
const ClockLogType ClockLogDefaultConfig[] =
{
  {
                             4096,
                             0x12
  }
};





const ClockFlagInitType ClockFlagInitConfig[] =
{
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"lpass_q6core", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_qdsp_sway_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_bus_timeout_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_mpu_cfg_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_q6_ahbm_mpu_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_qos_ahbs_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_wrapper_sysnoc_sway_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"q6ss_ahbm_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"q6ss_ahbs_aon_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_peripheral_smmu_client_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_aud_slimbus_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_qca_slimbus_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_lpm_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_CLOCK, (void*)"audio_core_sysnoc_mport_core_clk", 0x00000200 },
  { CLOCK_FLAG_NODE_TYPE_NONE, (void*)0, 0 }
};




static const char *ClockPubResource[] =
{
   "/clk/qdss"
};

ClockNPAResourcePubType ClockResourcePub =
{
   NPA_PD_1,
   ClockPubResource,
   1
};






const VCSCornerType CXVRegInitLevelConfig[] =
{
  VCS_CORNER_LOW
};
# 830 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
static ClockCPUConfigType Clock_Q6Config [] =
{
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               19200 * 1000,
                               { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
                               VCS_CORNER_LOW_MINUS,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_XO[0]
                             },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               86400 * 1000,
                               { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
                               VCS_CORNER_LOW_MINUS,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_LPAPLL1[1]
                             },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               172800 * 1000,
                               { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
                               VCS_CORNER_LOW_MINUS,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_LPAPLL1[1]
                             },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               249600 * 1000,
                               { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                               VCS_CORNER_LOW,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_LPAPLL1[0]
                             },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               422400 * 1000,
                               { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                               VCS_CORNER_NOMINAL,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_LPAPLL1[3]
                             },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               480000 * 1000,
                               { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                               VCS_CORNER_TURBO,
                               { {0x0, 0x0}, {0xFF, 0xFF} },
                               &SourceFreqConfig_LPAPLL1[4]
                             },
                             0x00000020
  },
  { 0 }
};




static ClockCPUConfigType Clock_Q6ConfigV2 [] =
{
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               19200 * 1000,
                               { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
                               VCS_CORNER_LOW_MINUS,
                               { {0x00, 0x00}, {0xFF, 0xFF} },
                               &SourceFreqConfig_XO[0]
                             },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  124800000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
                                  VCS_CORNER_LOW_MINUS,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[5]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  268800 * 1000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
                                  VCS_CORNER_LOW_MINUS,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[6]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  422400000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
                                  VCS_CORNER_LOW,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[13]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  633600000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                                  VCS_CORNER_NOMINAL,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[8]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  729600000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                                  VCS_CORNER_TURBO,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[11]
    },
                             0x00000020
  },
  { 0 }
};




static ClockCPUConfigType Clock_Q6ConfigV3 [] =
{
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
                             {
                               19200 * 1000,
                               { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
                               VCS_CORNER_LOW_MINUS,
                               { {0x00, 0x00}, {0xFF, 0xFF} },
                               &SourceFreqConfig_XO[0]
                             },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  124800000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
                                  VCS_CORNER_LOW_MINUS,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[5]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  297600 * 1000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
                                  VCS_CORNER_LOW_MINUS,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[7]
    },
                             0x00000020
  },
  {
                             HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    {

                                 480000 * 1000,
                                 { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
                                 VCS_CORNER_LOW,
                                 { {0x0, 0x0}, {0xFF, 0xFF} },
                                 &SourceFreqConfig_LPAPLL1[14]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  652800000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                                  VCS_CORNER_NOMINAL,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[9]
    },
                             0x00000020
  },
  {
                              HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,

    {
                                  825600000,
                                  { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
                                  VCS_CORNER_TURBO,
                                  { {0x02, 0x00}, {0x03, 0x00} },
                                  &SourceFreqConfig_LPAPLL1[12]
    },
                             0x00000020
  },
  { 0 }
};
# 1066 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/clock/config/msm8996/ClockBSP.c"
enum
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,
  CLOCK_CPU_PERF_LEVEL_5,

  CLOCK_CPU_PERF_LEVEL_TOTAL
};






static const uint32 Clock_Q6PerfLevels[] =
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,
  CLOCK_CPU_PERF_LEVEL_5,
};





static ClockCPUPerfConfigType Clock_Q6PerfConfig[] =
{
  {
    .HWVersion = {{0, 0}, {0xFF, 0xFF}},
    .nMinPerfLevel = CLOCK_CPU_PERF_LEVEL_1,
    .nMaxPerfLevel = CLOCK_CPU_PERF_LEVEL_5,
    .anPerfLevel = (uint32*)Clock_Q6PerfLevels,
    .nNumPerfLevels = ( sizeof( (Clock_Q6PerfLevels) ) / sizeof( (Clock_Q6PerfLevels[0]) ) )
  },
};





const ClockImageBSPConfigType ClockImageBSPConfig =
{
  .bEnableDCS = 1,
  .pCPUConfig = Clock_Q6Config,
  .pCPUPerfConfig = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ( sizeof( (Clock_Q6PerfConfig) ) / sizeof( (Clock_Q6PerfConfig[0]) ) ),


};




const ClockImageBSPConfigType ClockImageBSPConfigV2 =
{
  .bEnableDCS = 1,
  .pCPUConfig = Clock_Q6ConfigV2,
  .pCPUPerfConfig = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ( sizeof( (Clock_Q6PerfConfig) ) / sizeof( (Clock_Q6PerfConfig[0]) ) ),


};





const ClockImageBSPConfigType ClockImageBSPConfigV3 =
{
  .bEnableDCS = 1,
  .pCPUConfig = Clock_Q6ConfigV3,
  .pCPUPerfConfig = Clock_Q6PerfConfig,
  .nNumCPUPerfLevelConfigs = ( sizeof( (Clock_Q6PerfConfig) ) / sizeof( (Clock_Q6PerfConfig[0]) ) ),


};



const HAL_clk_HWIOBaseType ClockLPASSHWIOBases =
{
                      0x09000000,
                      0x00400000
};
