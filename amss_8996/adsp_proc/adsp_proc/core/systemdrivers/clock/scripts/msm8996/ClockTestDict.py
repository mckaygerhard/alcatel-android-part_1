CLOCK_TEST_DICT = {

  0x10000: 'gcc_sys_noc_axi_clk',
  0x10001: 'gcc_sys_noc_qdss_stm_axi_clk',
  0x10002: 'gcc_sys_noc_hmss_ahb_clk',
  0x10003: 'gcc_snoc_cnoc_ahb_clk',
  0x10004: 'gcc_snoc_pnoc_ahb_clk',
  0x10005: 'gcc_sys_noc_at_clk',
  0x10006: 'gcc_sys_noc_usb3_axi_clk',
  0x10007: 'gcc_sys_noc_ufs_axi_clk',
  0x10008: 'gcc_snoc_qosgen_extref',
  0x10008: 'gcc_snoc_qosgen_extref',
  0x10009: 'gcc_sys_noc_pimem_axi_clk',
  0x1000A: 'gcc_sys_noc_hs_axi_clk',
  0x1000B: 'gcc_bimc_nius_hs_axi_clk',
  0x1000C: 'gcc_bimc_nius_axi_clk',
  0x1000D: 'gcc_bimc_nius_cfg_ahb_clk',
  0x1000E: 'gcc_cfg_noc_ahb_clk',
  0x1000F: 'gcc_cfg_noc_ddr_cfg_clk',
  0x10010: 'gcc_cfg_noc_tic_clk',
  0x10011: 'gcc_periph_noc_ahb_clk',
  0x10012: 'gcc_periph_noc_cfg_ahb_clk',
  0x10013: 'gcc_periph_noc_at_clk',
  0x10014: 'gcc_periph_noc_usb20_ahb_clk',
  0x10015: 'gcc_noc_conf_xpu_ahb_clk',
  0x10016: 'gcc_imem_axi_clk',
  0x10017: 'gcc_imem_cfg_ahb_clk',
  0x10018: 'gcc_mmss_sys_noc_axi_clk',
  0x10019: 'gcc_mmss_noc_cfg_ahb_clk',
  0x1001A: 'gcc_mmss_noc_at_clk',
  0x1001B: 'mmss_gcc_dbg_clk',
  0x1001B: 'mmss_gcc_dbg_clk',
  0x1001C: 'gcc_mmss_bimc_gfx_clk',
  0x1001D: 'gcc_pimem_axi_clk',
  0x1001E: 'gcc_pimem_ahb_clk',
  0x1001F: 'gcc_rbcpr_xpu_ahb_clk',
  0x10020: 'gcc_qdss_mpu_ahb_clk',
  0x10021: 'gcc_qdss_dap_ahb_clk',
  0x10022: 'gcc_qdss_cfg_ahb_clk',
  0x10023: 'gcc_qdss_at_clk',
  0x10024: 'gcc_qdss_etr_usb_clk',
  0x10025: 'gcc_qdss_stm_clk',
  0x10026: 'gcc_qdss_traceclkin_clk',
  0x10027: 'gcc_qdss_tsctr_div2_clk',
  0x10028: 'gcc_qdss_tsctr_div3_clk',
  0x10029: 'gcc_qdss_dap_clk',
  0x1002A: 'gcc_qdss_tsctr_div4_clk',
  0x1002B: 'gcc_qdss_tsctr_div8_clk',
  0x1002C: 'gcc_qdss_tsctr_div16_clk',
  0x1002D: 'gcc_usb30_master_clk',
  0x1002E: 'gcc_usb30_sleep_clk',
  0x1002F: 'gcc_usb30_mock_utmi_clk',
  0x10030: 'gcc_usb3_phy_aux_clk',
  0x10031: 'gcc_usb3_phy_pipe_clk',
  0x10032: 'usb3_phy_wrapper_gcc_usb3_pipe_clk',
  0x10032: 'usb3_phy_wrapper_gcc_usb3_pipe_clk',
  0x10033: 'qusb2phy_prim_gcc_usb30_utmi_clk',
  0x10033: 'qusb2phy_prim_gcc_usb30_utmi_clk',
  0x10034: 'qusb2phy_sec_gcc_usb20s_utmi_clk',
  0x10034: 'qusb2phy_sec_gcc_usb20s_utmi_clk',
  0x10035: 'gcc_usb20_master_clk',
  0x10036: 'gcc_usb20_sleep_clk',
  0x10037: 'gcc_usb20_mock_utmi_clk',
  0x10038: 'gcc_usb_phy_cfg_ahb2phy_clk',
  0x10039: 'gcc_sdcc1_apps_clk',
  0x1003A: 'gcc_sdcc1_ahb_clk',
  0x1003B: 'gcc_sdcc2_apps_clk',
  0x1003C: 'gcc_sdcc2_ahb_clk',
  0x1003D: 'gcc_sdcc3_apps_clk',
  0x1003E: 'gcc_sdcc3_ahb_clk',
  0x1003F: 'gcc_sdcc4_apps_clk',
  0x10040: 'gcc_sdcc4_ahb_clk',
  0x10041: 'gcc_blsp1_ahb_clk',
  0x10042: 'gcc_blsp1_sleep_clk',
  0x10043: 'gcc_blsp1_qup1_spi_apps_clk',
  0x10044: 'gcc_blsp1_qup1_i2c_apps_clk',
  0x10045: 'gcc_blsp1_uart1_apps_clk',
  0x10046: 'gcc_blsp1_uart1_sim_clk',
  0x10047: 'gcc_blsp1_qup2_spi_apps_clk',
  0x10048: 'gcc_blsp1_qup2_i2c_apps_clk',
  0x10049: 'gcc_blsp1_uart2_apps_clk',
  0x1004A: 'gcc_blsp1_uart2_sim_clk',
  0x1004B: 'gcc_blsp1_qup3_spi_apps_clk',
  0x1004C: 'gcc_blsp1_qup3_i2c_apps_clk',
  0x1004D: 'gcc_blsp1_uart3_apps_clk',
  0x1004E: 'gcc_blsp1_uart3_sim_clk',
  0x1004F: 'gcc_blsp1_qup4_spi_apps_clk',
  0x10050: 'gcc_blsp1_qup4_i2c_apps_clk',
  0x10051: 'gcc_blsp1_uart4_apps_clk',
  0x10052: 'gcc_blsp1_uart4_sim_clk',
  0x10053: 'gcc_blsp1_qup5_spi_apps_clk',
  0x10054: 'gcc_blsp1_qup5_i2c_apps_clk',
  0x10055: 'gcc_blsp1_uart5_apps_clk',
  0x10056: 'gcc_blsp1_uart5_sim_clk',
  0x10057: 'gcc_blsp1_qup6_spi_apps_clk',
  0x10058: 'gcc_blsp1_qup6_i2c_apps_clk',
  0x10059: 'gcc_blsp1_uart6_apps_clk',
  0x1005A: 'gcc_blsp1_uart6_sim_clk',
  0x1005B: 'gcc_blsp2_ahb_clk',
  0x1005C: 'gcc_blsp2_sleep_clk',
  0x1005D: 'gcc_blsp2_qup1_spi_apps_clk',
  0x1005E: 'gcc_blsp2_qup1_i2c_apps_clk',
  0x1005F: 'gcc_blsp2_uart1_apps_clk',
  0x10060: 'gcc_blsp2_uart1_sim_clk',
  0x10061: 'gcc_blsp2_qup2_spi_apps_clk',
  0x10062: 'gcc_blsp2_qup2_i2c_apps_clk',
  0x10063: 'gcc_blsp2_uart2_apps_clk',
  0x10064: 'gcc_blsp2_uart2_sim_clk',
  0x10065: 'gcc_blsp2_qup3_spi_apps_clk',
  0x10066: 'gcc_blsp2_qup3_i2c_apps_clk',
  0x10067: 'gcc_blsp2_uart3_apps_clk',
  0x10068: 'gcc_blsp2_uart3_sim_clk',
  0x10069: 'gcc_blsp2_qup4_spi_apps_clk',
  0x1006A: 'gcc_blsp2_qup4_i2c_apps_clk',
  0x1006B: 'gcc_blsp2_uart4_apps_clk',
  0x1006C: 'gcc_blsp2_uart4_sim_clk',
  0x1006D: 'gcc_blsp2_qup5_spi_apps_clk',
  0x1006E: 'gcc_blsp2_qup5_i2c_apps_clk',
  0x1006F: 'gcc_blsp2_uart5_apps_clk',
  0x10070: 'gcc_blsp2_uart5_sim_clk',
  0x10071: 'gcc_blsp2_qup6_spi_apps_clk',
  0x10072: 'gcc_blsp2_qup6_i2c_apps_clk',
  0x10073: 'gcc_blsp2_uart6_apps_clk',
  0x10074: 'gcc_blsp2_uart6_sim_clk',
  0x10075: 'gcc_periph_noc_mpu_cfg_ahb_clk',
  0x10076: 'gcc_pdm_ahb_clk',
  0x10077: 'gcc_pdm_xo4_clk',
  0x10078: 'gcc_pdm2_clk',
  0x10079: 'gcc_prng_ahb_clk',
  0x1007A: 'gcc_tsif_ahb_clk',
  0x1007B: 'gcc_tsif_ref_clk',
  0x1007C: 'gcc_tsif_inactivity_timers_clk',
  0x1007D: 'gcc_tcsr_ahb_clk',
  0x1007E: 'gcc_boot_rom_ahb_clk',
  0x1007F: 'gcc_msg_ram_ahb_clk',
  0x10080: 'gcc_tlmm_ahb_clk',
  0x10081: 'gcc_tlmm_clk',
  0x10082: 'gcc_mpm_ahb_clk',
  0x10083: 'gcc_rpm_proc_fclk',
  0x10084: 'gcc_rpm_proc_hclk',
  0x10085: 'gcc_rpm_bus_ahb_clk',
  0x10086: 'gcc_rpm_sleep_clk',
  0x10087: 'gcc_rpm_timer_clk',
  0x10088: 'gcc_rpm_mst_m2_cnoc_ahb_clk',
  0x10089: 'gcc_cnoc_mst_rpm_ahb_clk',
  0x1008A: 'gcc_sec_ctrl_acc_clk',
  0x1008B: 'gcc_sec_ctrl_ahb_clk',
  0x1008C: 'gcc_sec_ctrl_clk',
  0x1008D: 'gcc_sec_ctrl_sense_clk',
  0x1008E: 'gcc_sec_ctrl_boot_rom_patch_clk',
  0x1008F: 'gcc_spmi_ser_clk',
  0x10090: 'gcc_spmi_cnoc_ahb_clk',
  0x10091: 'gcc_spmi_ahb_clk',
  0x10092: 'gcc_spdm_cfg_ahb_clk',
  0x10093: 'gcc_spdm_mstr_ahb_clk',
  0x10094: 'gcc_spdm_ff_clk',
  0x10095: 'gcc_spdm_bimc_cy_clk',
  0x10096: 'gcc_spdm_snoc_cy_clk',
  0x10097: 'gcc_spdm_pnoc_cy_clk',
  0x10098: 'gcc_spdm_rpm_cy_clk',
  0x10099: 'gcc_ce1_clk',
  0x1009A: 'gcc_ce1_axi_clk',
  0x1009B: 'gcc_ce1_ahb_clk',
  0x1009C: 'gcc_ahb_clk',
  0x1009D: 'gcc_xo_clk',
  0x1009E: 'gcc_xo_div4_clk',
  0x1009F: 'gcc_im_sleep_clk',
  0x100A0: 'gcc_bimc_xo_clk',
  0x100A1: 'gcc_bimc_cfg_ahb_clk',
  0x100A2: 'gcc_bimc_sleep_clk',
  0x100A3: 'gcc_bimc_sysnoc_axi_clk',
  0x100A4: 'gcc_bimc_clk',
  0x100A5: 'gcc_bimc_hmss_axi_clk',
  0x100A7: 'gcc_bimc_sysnoc_hs_axi_clk',
  0x100A8: 'gcc_bimc_pimem_axi_clk',
  0x100A9: 'gcc_ddr_dim_cfg_clk',
  0x100AA: 'gcc_bimc_ddr_cpll0_clk',
  0x100AB: 'gcc_bimc_ddr_cpll1_clk',
  0x100AC: 'gcc_ddr_dim_sleep_clk',
  0x100AD: 'gcc_bimc_ddr_ch0_clk',
  0x100AE: 'gcc_bimc_ddr_ch1_clk',
  0x100AF: 'gcc_bimc_gfx_clk',
  0x100B0: 'gcc_lpass_q6_axi_clk',
  0x100B1: 'gcc_lpass_q6_smmu_axi_clk',
  0x100B2: 'gcc_lpass_sway_clk',
  0x100B3: 'gcc_lpass_q6_smmu_ahb_clk',
  0x100B4: 'gcc_lpass_core_smmu_ahb_clk',
  0x100B5: 'gcc_lpass_smmu_aon_ahb_clk',
  0x100B6: 'lpass_gcc_dbg_clk',
  0x100B6: 'lpass_gcc_dbg_clk',
  0x100B7: 'gcc_hmss_ahb_clk',
  0x100B8: 'gcc_hmss_slv_axi_clk',
  0x100B9: 'gcc_hmss_mstr_axi_clk',
  0x100BA: 'gcc_hmss_rbcpr_clk',
  0x100BB: 'hmss_gcc_dbg_clk',
  0x100BB: 'hmss_gcc_dbg_clk',
  0x100BC: 'gcc_snoc_bus_timeout0_ahb_clk',
  0x100BD: 'gcc_snoc_bus_timeout2_ahb_clk',
  0x100BE: 'gcc_snoc_bus_timeout1_ahb_clk',
  0x100BF: 'gcc_snoc_bus_timeout3_ahb_clk',
  0x100C0: 'gcc_snoc_bus_timeout_extref',
  0x100C0: 'gcc_snoc_bus_timeout_extref',
  0x100C1: 'gcc_pnoc_bus_timeout0_ahb_clk',
  0x100C2: 'gcc_pnoc_bus_timeout1_ahb_clk',
  0x100C3: 'gcc_pnoc_bus_timeout2_ahb_clk',
  0x100C4: 'gcc_pnoc_bus_timeout3_ahb_clk',
  0x100C5: 'gcc_pnoc_bus_timeout4_ahb_clk',
  0x100C6: 'gcc_cnoc_bus_timeout0_ahb_clk',
  0x100C7: 'gcc_cnoc_bus_timeout1_ahb_clk',
  0x100C8: 'gcc_cnoc_bus_timeout2_ahb_clk',
  0x100C9: 'gcc_cnoc_bus_timeout3_ahb_clk',
  0x100CA: 'gcc_cnoc_bus_timeout4_ahb_clk',
  0x100CB: 'gcc_cnoc_bus_timeout5_ahb_clk',
  0x100CC: 'gcc_cnoc_bus_timeout6_ahb_clk',
  0x100CD: 'gcc_cnoc_bus_timeout7_ahb_clk',
  0x100CE: 'gcc_cnoc_bus_timeout8_ahb_clk',
  0x100CF: 'gcc_cnoc_bus_timeout9_ahb_clk',
  0x100D0: 'gcc_cnoc_bus_timeout_extref',
  0x100D0: 'gcc_cnoc_bus_timeout_extref',
  0x100D1: 'gcc_qdss_apb2jtag_clk',
  0x100D2: 'gcc_rbcpr_cx_clk',
  0x100D3: 'gcc_rbcpr_cx_ahb_clk',
  0x100D4: 'gcc_rbcpr_mx_clk',
  0x100D5: 'gcc_rbcpr_mx_ahb_clk',
  0x100D6: 'qusb2phy_gcc_clk_test_prim',
  0x100D6: 'qusb2phy_gcc_clk_test_prim',
  0x100D7: 'qusb2phy_gcc_clk_test_sec',
  0x100D7: 'qusb2phy_gcc_clk_test_sec',
  0x100D8: 'gpll0_dtest',
  0x100D8: 'gpll0_dtest',
  0x100D9: 'gpll0_lock_det',
  0x100D9: 'gpll0_lock_det',
  0x100DA: 'gpll1_dtest',
  0x100DA: 'gpll1_dtest',
  0x100DB: 'gpll1_lock_det',
  0x100DB: 'gpll1_lock_det',
  0x100DC: 'gpll2_dtest',
  0x100DC: 'gpll2_dtest',
  0x100DD: 'gpll2_lock_det',
  0x100DD: 'gpll2_lock_det',
  0x100DE: 'gpll3_dtest',
  0x100DE: 'gpll3_dtest',
  0x100DF: 'gpll3_lock_det',
  0x100DF: 'gpll3_lock_det',
  0x100E0: 'mpm_gcc_temp_sensor_ringosc_clk',
  0x100E0: 'mpm_gcc_temp_sensor_ringosc_clk',
  0x100E1: 'gpll4_dtest',
  0x100E1: 'gpll4_dtest',
  0x100E2: 'gpll4_lock_det',
  0x100E2: 'gpll4_lock_det',
  0x100E3: 'gcc_gp1_clk',
  0x100E4: 'gcc_gp2_clk',
  0x100E5: 'gcc_gp3_clk',
  0x100E6: 'gcc_pcie_0_slv_axi_clk',
  0x100E7: 'gcc_pcie_0_mstr_axi_clk',
  0x100E8: 'gcc_pcie_0_cfg_ahb_clk',
  0x100E9: 'gcc_pcie_0_aux_clk',
  0x100EA: 'gcc_pcie_0_pipe_clk',
  0x100EC: 'gcc_pcie_1_slv_axi_clk',
  0x100ED: 'gcc_pcie_1_mstr_axi_clk',
  0x100EE: 'gcc_pcie_1_cfg_ahb_clk',
  0x100EF: 'gcc_pcie_1_aux_clk',
  0x100F0: 'gcc_pcie_1_pipe_clk',
  0x100F2: 'gcc_pcie_2_slv_axi_clk',
  0x100F3: 'gcc_pcie_2_mstr_axi_clk',
  0x100F4: 'gcc_pcie_2_cfg_ahb_clk',
  0x100F5: 'gcc_pcie_2_aux_clk',
  0x100F6: 'gcc_pcie_2_pipe_clk',
  0x100F8: 'gcc_pcie_phy_cfg_ahb_clk',
  0x100F9: 'gcc_pcie_phy_aux_clk',
  0x100FA: 'gcc_dcd_xo_clk',
  0x100FB: 'gcc_obt_odt_clk',
  0x100FC: 'gcc_ufs_axi_clk',
  0x100FD: 'gcc_ufs_ahb_clk',
  0x100FE: 'gcc_ufs_tx_cfg_clk',
  0x100FF: 'gcc_ufs_rx_cfg_clk',
  0x10100: 'gcc_ufs_tx_symbol_0_clk',
  0x10101: 'gcc_ufs_rx_symbol_0_clk',
  0x10102: 'gcc_ufs_rx_symbol_1_clk',
  0x10103: 'ufs_tx_symbol_0_clk',
  0x10103: 'ufs_tx_symbol_0_clk',
  0x10104: 'ufs_rx_symbol_0_clk',
  0x10104: 'ufs_rx_symbol_0_clk',
  0x10105: 'ufs_rx_symbol_1_clk',
  0x10105: 'ufs_rx_symbol_1_clk',
  0x10106: 'gcc_ufs_unipro_core_clk',
  0x10107: 'gcc_ufs_ice_core_clk',
  0x10108: 'gcc_ufs_sys_clk_core_clk',
  0x10109: 'gcc_ufs_tx_symbol_clk_core_clk',
  0x1010A: 'gcc_ssc_snoc_ahbm_clk',
  0x1010B: 'gcc_sys_noc_ssc_q6_axi_clk',
  0x1010C: 'gcc_ssc_cnoc_ahbs_clk',
  0x1010D: 'gcc_ssc_cnoc_mpu_clk',
  0x1010E: 'gcc_ssc_at_clk',
  0x1010F: 'gcc_ssc_apb_clk',
  0x10110: 'gcc_ssc_xo_clk',
  0x10111: 'ssc_gcc_dbg_clk',
  0x10111: 'ssc_gcc_dbg_clk',
  0x10112: 'gcc_vddcx_vs_clk',
  0x10113: 'gcc_vddmx_vs_clk',
  0x10114: 'gcc_vdda_vs_clk',
  0x10115: 'gcc_vs_ctrl_clk',
  0x10116: 'gcc_aggre0_snoc_axi_clk',
  0x10117: 'gcc_aggre0_cnoc_ahb_clk',
  0x10118: 'gcc_aggre0_noc_at_clk',
  0x10119: 'gcc_smmu_aggre0_axi_clk',
  0x1011A: 'gcc_smmu_aggre0_ahb_clk',
  0x1011B: 'gcc_aggre0_noc_qosgen_extref',
  0x1011B: 'gcc_aggre0_noc_qosgen_extref',
  0x1011C: 'gcc_aggre1_snoc_axi_clk',
  0x1011D: 'gcc_aggre1_cnoc_ahb_clk',
  0x1011E: 'gcc_aggre1_noc_at_clk',
  0x1011F: 'gcc_aggre1_pnoc_ahb_clk',
  0x10120: 'gcc_smmu_aggre1_axi_clk',
  0x10121: 'gcc_smmu_aggre1_ahb_clk',
  0x10122: 'gcc_aggre1_noc_qosgen_extref',
  0x10122: 'gcc_aggre1_noc_qosgen_extref',
  0x10123: 'gcc_aggre2_snoc_axi_clk',
  0x10124: 'gcc_aggre2_cnoc_ahb_clk',
  0x10125: 'gcc_aggre2_noc_at_clk',
  0x10126: 'gcc_aggre2_ufs_axi_clk',
  0x10127: 'gcc_aggre2_usb3_axi_clk',
  0x10128: 'gcc_smmu_aggre2_axi_clk',
  0x10129: 'gcc_smmu_aggre2_ahb_clk',
  0x1012A: 'gcc_aggre2_noc_qosgen_extref',
  0x1012A: 'gcc_aggre2_noc_qosgen_extref',
  0x1012B: 'gcc_dcc_ahb_clk',
  0x1012C: 'gcc_aggre0_noc_mpu_cfg_ahb_clk',
  0x1012D: 'gcc_aggre1_noc_mpu_cfg_ahb_clk',
  0x1012E: 'gcc_aggre2_noc_mpu_cfg_ahb_clk',
  0x20008: 'q6ss_sleep_clk_src',
  0x20009: 'bcr_slp_clk_src',
  0x2000A: 'sleep_clk_src',
  0x2000B: 'lpaif_spkr_clk_src',
  0x2000C: 'lpaif_pri_clk_src',
  0x2000D: 'lpaif_sec_clk_src',
  0x2000E: 'lpaif_ter_clk_src',
  0x2000F: 'lpaif_quad_clk_src',
  0x20010: 'lpaif_pcmoe_clk_src',
  0x20011: 'atime_clk_src',
  0x20012: 'resampler_clk_src',
  0x20013: 'aud_slimbus_clk_src',
  0x20014: 'qca_slimbus_clk_src',
  0x20015: 'core_clk_src',
  0x20016: 'aon_clk_src',
  0x20017: 'qos_xo_clk_src',
  0x20018: 'ext_mclk0_clk_src',
  0x20019: 'ext_mclk1_clk_src',
  0x2001A: 'ext_mclk2_clk_src',
  0x2001C: 'q6_smmu_gdsc_xo_clk_src',
  0x2001D: 'xo_clk_src',
  0x2001E: 'stc_xo_clk_src',
  0x2001F: 'q6_xo_clk_src',
  0x20020: 'gdsc_xo_clk_src',
  0x20022: 'qos_fixed_lat_counter_src',
  0x20023: 'audio_core_gdsc_xo_clk',
  0x20024: 'audio_core_lpaif_codec_spkr_osr_clk',
  0x20025: 'audio_core_lpaif_codec_spkr_ibit_clk',
  0x20027: 'audio_core_lpaif_pri_ibit_clk',
  0x20029: 'audio_core_lpaif_sec_ibit_clk',
  0x2002B: 'audio_core_lpaif_ter_ibit_clk',
  0x2002D: 'audio_core_lpaif_quad_ibit_clk',
  0x2002E: 'audio_core_lpaif_pcm_data_oe_clk',
  0x2002F: 'lpass_core_avsync_atime_clk',
  0x20030: 'audio_core_avsync_stc_xo_clk',
  0x20031: 'audio_core_resampler_core_clk',
  0x20032: 'audio_core_aud_slimbus_clk',
  0x20033: 'audio_core_qca_slimbus_clk',
  0x20034: 'audio_core_core_clk',
  0x20035: 'audio_core_sysnoc_mport_core_clk',
  0x20036: 'audio_core_aud_slimbus_core_clk',
  0x20037: 'audio_core_qca_slimbus_core_clk',
  0x20038: 'audio_core_lpm_core_clk',
  0x2003C: 'audio_core_peripheral_smmu_client_core_clk',
  0x2003D: 'audio_core_qdsp_sway_aon_clk',
  0x2003E: 'audio_core_peripheral_smmu_cfg_cnoc_clk',
  0x2003F: 'audio_core_sysnoc_sway_snoc_clk',
  0x20040: 'audio_core_bcr_slp_clk',
  0x20043: 'audio_wrapper_sysnoc_sway_snoc_clk',
  0x20044: 'audio_wrapper_aon_clk',
  0x20045: 'audio_wrapper_qos_ahbs_aon_clk',
  0x20046: 'audio_wrapper_ext_mclk0_clk',
  0x20047: 'audio_wrapper_ext_mclk1_clk',
  0x20048: 'audio_wrapper_ext_mclk2_clk',
  0x20049: 'audio_wrapper_sysnoc_sway_aon_clk',
  0x2004A: 'q6ss_ahbm_aon_clk',
  0x2004B: 'q6ss_bcr_slp_clk',
  0x2004C: 'audio_wrapper_q6_smmu_gdsc_xo_clk',
  0x2004E: 'audio_wrapper_mpu_cfg_aon_clk',
  0x2004F: 'audio_wrapper_q6_ahbm_mpu_aon_clk',
  0x20050: 'q6ss_ahbs_aon_clk',
  0x20051: 'audio_wrapper_qos_xo_lat_counter_clk',
  0x20052: 'audio_wrapper_qos_dmonitor_fixed_lat_counter_clk',
  0x20053: 'audio_wrapper_qos_danger_fixed_lat_counter_clk',
  0x20054: 'core_qos_fixed_lat_counter_src',
  0x20055: 'audio_core_qos_dmonitor_fixed_lat_counter_clk',
  0x30001: 'mmss_mmagic_ahb_clk',
  0x30002: 'mmss_bto_ahb_clk',
  0x30003: 'mmss_misc_ahb_clk',
  0x30004: 'mmss_mmagic_axi_clk',
  0x30005: 'mmss_s0_axi_clk',
  0x30006: 'mmpll4_dtest',
  0x30006: 'mmpll4_dtest',
  0x30007: 'mmpll4_lock_det',
  0x30007: 'mmpll4_lock_det',
  0x30009: 'vmem_maxi_clk',
  0x3000A: 'vmem_ahb_clk',
  0x3000B: 'gcc_mmss_bimc_gfx_clk',
  0x3000B: 'gcc_mmss_bimc_gfx_clk',
  0x3000C: 'gpu_ahb_clk',
  0x3000D: 'gpu_gx_gfx3d_clk',
  0x3000E: 'video_core_clk',
  0x3000F: 'video_axi_clk',
  0x30010: 'video_maxi_clk',
  0x30011: 'video_ahb_clk',
  0x30012: 'mmss_rbcpr_clk',
  0x30013: 'mmss_rbcpr_ahb_clk',
  0x30014: 'mdss_mdp_clk',
  0x30016: 'mdss_pclk0_clk',
  0x30017: 'mdss_pclk1_clk',
  0x30018: 'mdss_extpclk_clk',
  0x30019: 'mmss_spdm_rm_maxi_clk',
  0x3001A: 'video_subcore0_clk',
  0x3001B: 'video_subcore1_clk',
  0x3001C: 'mdss_vsync_clk',
  0x3001D: 'mdss_hdmi_clk',
  0x3001E: 'mdss_byte0_clk',
  0x3001F: 'mdss_byte1_clk',
  0x30020: 'mdss_esc0_clk',
  0x30021: 'mdss_esc1_clk',
  0x30022: 'mdss_ahb_clk',
  0x30023: 'mdss_hdmi_ahb_clk',
  0x30024: 'mdss_axi_clk',
  0x30025: 'camss_top_ahb_clk',
  0x30026: 'camss_micro_ahb_clk',
  0x30027: 'camss_gp0_clk',
  0x30028: 'camss_gp1_clk',
  0x30029: 'camss_mclk0_clk',
  0x3002A: 'camss_mclk1_clk',
  0x3002B: 'camss_mclk2_clk',
  0x3002C: 'camss_mclk3_clk',
  0x3002D: 'camss_cci_clk',
  0x3002E: 'camss_cci_ahb_clk',
  0x3002F: 'camss_csi0phytimer_clk',
  0x30030: 'camss_csi1phytimer_clk',
  0x30031: 'camss_csi2phytimer_clk',
  0x30032: 'camss_jpeg0_clk',
  0x30033: 'camss_ispif_ahb_clk',
  0x30034: 'camss_jpeg2_clk',
  0x30035: 'camss_jpeg_ahb_clk',
  0x30036: 'camss_jpeg_axi_clk',
  0x30037: 'camss_ahb_clk',
  0x30038: 'camss_vfe0_clk',
  0x30039: 'camss_vfe1_clk',
  0x3003A: 'camss_cpp_clk',
  0x3003B: 'camss_cpp_ahb_clk',
  0x3003C: 'camss_vfe_ahb_clk',
  0x3003D: 'camss_vfe_axi_clk',
  0x3003E: 'gpu_gx_rbbmtimer_clk',
  0x3003F: 'camss_csi_vfe0_clk',
  0x30040: 'camss_csi_vfe1_clk',
  0x30041: 'camss_csi0_clk',
  0x30042: 'camss_csi0_ahb_clk',
  0x30043: 'camss_csi0phy_clk',
  0x30044: 'camss_csi0rdi_clk',
  0x30045: 'camss_csi0pix_clk',
  0x30046: 'camss_csi1_clk',
  0x30047: 'camss_csi1_ahb_clk',
  0x30048: 'camss_csi1phy_clk',
  0x30049: 'camss_csi1rdi_clk',
  0x3004A: 'camss_csi1pix_clk',
  0x3004B: 'camss_csi2_clk',
  0x3004C: 'camss_csi2_ahb_clk',
  0x3004D: 'camss_csi2phy_clk',
  0x3004E: 'camss_csi2rdi_clk',
  0x3004F: 'camss_csi2pix_clk',
  0x30050: 'camss_csi3_clk',
  0x30051: 'camss_csi3_ahb_clk',
  0x30052: 'camss_csi3phy_clk',
  0x30053: 'camss_csi3rdi_clk',
  0x30054: 'camss_csi3pix_clk',
  0x30055: 'mmss_spdm_cpp_clk',
  0x30056: 'mmss_spdm_jpeg0_clk',
  0x30057: 'mmss_spdm_jpeg_dma_clk',
  0x30058: 'mmss_spdm_mdp_clk',
  0x30059: 'mmss_spdm_axi_clk',
  0x3005A: 'mmss_spdm_video_core_clk',
  0x3005B: 'mmss_spdm_vfe0_clk',
  0x3005C: 'mmss_spdm_vfe1_clk',
  0x3005D: 'mmss_spdm_jpeg2_clk',
  0x3005E: 'mmss_spdm_pclk1_clk',
  0x3005F: 'mmss_spdm_gfx3d_clk',
  0x30060: 'mmss_spdm_ahb_clk',
  0x30061: 'mmss_spdm_pclk0_clk',
  0x30062: 'mmss_spdm_csi0_clk',
  0x30063: 'mmss_spdm_rm_axi_clk',
  0x30064: 'mmpll0_dtest',
  0x30064: 'mmpll0_dtest',
  0x30065: 'mmpll0_lock_det',
  0x30065: 'mmpll0_lock_det',
  0x30066: 'mmpll1_dtest',
  0x30066: 'mmpll1_dtest',
  0x30067: 'mmpll1_lock_det',
  0x30067: 'mmpll1_lock_det',
  0x30068: 'mmpll2_dtest',
  0x30068: 'mmpll2_dtest',
  0x30069: 'mmpll2_lock_det',
  0x30069: 'mmpll2_lock_det',
  0x3006A: 'mmpll3_dtest',
  0x3006A: 'mmpll3_dtest',
  0x3006B: 'mmpll3_lock_det',
  0x3006B: 'mmpll3_lock_det',
  0x3006C: 'mmpll5_dtest',
  0x3006C: 'mmpll5_dtest',
  0x3006D: 'mmpll5_lock_det',
  0x3006D: 'mmpll5_lock_det',
  0x3006E: 'mmpll9_dtest',
  0x3006E: 'mmpll9_dtest',
  0x3006F: 'mmpll9_lock_det',
  0x3006F: 'mmpll9_lock_det',
  0x30070: 'mmss_mmagic_maxi_clk',
  0x30071: 'camss_vfe0_stream_clk',
  0x30072: 'camss_vfe1_stream_clk',
  0x30073: 'camss_cpp_vbif_ahb_clk',
  0x30074: 'mmss_mmagic_cfg_ahb_clk',
  0x30075: 'dsa_core_clk',
  0x30076: 'dsa_noc_cfg_ahb_clk',
  0x30077: 'mmss_misc_cxo_clk',
  0x3007A: 'camss_cpp_axi_clk',
  0x3007B: 'camss_jpeg_dma_clk',
  0x3007C: 'mmpll0_enable_status',
  0x3007C: 'mmpll0_enable_status',
  0x3007D: 'mmpll1_enable_status',
  0x3007D: 'mmpll1_enable_status',
  0x3007E: 'mmpll2_enable_status',
  0x3007E: 'mmpll2_enable_status',
  0x3007F: 'mmpll3_enable_status',
  0x3007F: 'mmpll3_enable_status',
  0x30080: 'mmpll4_enable_status',
  0x30080: 'mmpll4_enable_status',
  0x30081: 'mmpll5_enable_status',
  0x30081: 'mmpll5_enable_status',
  0x30082: 'mmpll9_enable_status',
  0x30082: 'mmpll9_enable_status',
  0x30083: 'mmpll8_dtest',
  0x30083: 'mmpll8_dtest',
  0x30084: 'mmpll8_lock_det',
  0x30084: 'mmpll8_lock_det',
  0x30085: 'mmpll8_enable_status',
  0x30085: 'mmpll8_enable_status',
  0x30086: 'camss_vfe0_ahb_clk',
  0x30087: 'camss_vfe1_ahb_clk',
  0x30088: 'gpu_aon_isense_clk',
  0x30089: 'fd_core_clk',
  0x3008A: 'fd_core_uar_clk',
  0x3008B: 'gfx3d_aon_clk_src',
  0x3008C: 'fd_ahb_clk',
  0x3008D: 'mdss_edppixel_clk',
  0x3008E: 'mdss_edplink_clk',
  0x3008F: 'mdss_edpaux_clk',
  0x30090: 'mdss_edpgtc_clk',
  0x30091: 'camss_csiphy0_3p_clk',
  0x30092: 'camss_csiphy1_3p_clk',
  0x30093: 'camss_csiphy2_3p_clk',
  0x30094: 'smmu_vfe_ahb_clk',
  0x30095: 'smmu_vfe_axi_clk',
  0x30096: 'smmu_cpp_ahb_clk',
  0x30097: 'smmu_cpp_axi_clk',
  0x30098: 'smmu_jpeg_ahb_clk',
  0x30099: 'smmu_jpeg_axi_clk',
  0x3009A: 'mmagic_camss_axi_clk',
  0x3009B: 'smmu_rot_ahb_clk',
  0x3009C: 'smmu_rot_axi_clk',
  0x3009D: 'smmu_mdp_ahb_clk',
  0x3009E: 'smmu_mdp_axi_clk',
  0x3009F: 'mmagic_mdss_axi_clk',
  0x300A0: 'smmu_video_ahb_clk',
  0x300A1: 'smmu_video_axi_clk',
  0x300A2: 'mmagic_video_axi_clk',
  0x300A3: 'mmagic_bimc_axi_clk',
  0x300A4: 'throttle_camss_ahb_clk',
  0x300A5: 'throttle_mdss_ahb_clk',
  0x300A6: 'throttle_video_ahb_clk',
  0x300A7: 'throttle_camss_cxo_clk',
  0x300A8: 'throttle_mdss_cxo_clk',
  0x300A9: 'throttle_video_cxo_clk',
  0x300AA: 'throttle_camss_axi_clk',
  0x300AB: 'throttle_mdss_axi_clk',
  0x300AC: 'throttle_video_axi_clk',
  0x300AD: 'mmagic_camss_noc_cfg_ahb_clk',
  0x300AE: 'mmagic_mdss_noc_cfg_ahb_clk',
  0x300AF: 'mmagic_video_noc_cfg_ahb_clk',
  0x300B0: 'mmagic_bimc_noc_cfg_ahb_clk',
  0x40001: 'scc_qup_spi1_clk',
  0x40002: 'scc_qup_i2c1_clk',
  0x40003: 'scc_qup_spi2_clk',
  0x40004: 'scc_qup_i2c2_clk',
  0x40005: 'scc_qup_spi3_clk',
  0x40006: 'scc_qup_i2c3_clk',
  0x40007: 'scc_uart_dm_uart1_clk',
  0x40008: 'scc_uart_dm_uart2_clk',
  0x40009: 'scc_uart_dm_uart3_clk',
  0x4000A: 'scc_dbg_tsctr_clk',
  0x4000B: 'scc_q6_spm_clk',
  0x4000C: 'scc_crif_clk',
  0x4000D: 'scc_csr_h_clk',
  0x4000E: 'scc_blsp_h_clk',
  0x4000F: 'scc_data_h_clk',
  0x40010: 'scc_q6_ahbm_clk',
  0x40011: 'scc_q6_ahbs_clk',
  0x40012: 'scc_cfg_ahb_clk',
  0x40013: 'scc_smem_clk',
  0x40014: 'scc_ahb_timeout_clk',
  0x40015: 'scc_q6_xpu2_client_clk',
  0x40016: 'scc_q6_xpu2_config_clk',
  0x40017: 'scc_at_clk',
  0x40018: 'scc_q6_atbm_clk',
  0x40019: 'scc_pclkdbg_clk',
  0x4001A: 'scc_vs_vddmx_clk',
  0x4001B: 'scc_vs_vddcx_clk',
  0x4001C: 'q6_pll_dtest',
  0x4001C: 'q6_pll_dtest',
  0x4001D: 'q6_pll_lock_det',
  0x4001D: 'q6_pll_lock_det',
  0x4001E: 'q6ss_dbg_clk',
  0x4001E: 'q6ss_dbg_clk',
  0x4001F: 'scc_peel_pll_lock_det',
  0x4001F: 'scc_peel_pll_lock_det',

}

