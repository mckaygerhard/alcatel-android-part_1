# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c" 2
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h" 1
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long long uint64;




typedef long long int64;




typedef unsigned char byte;



typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 27 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdErr.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 1
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};

typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};







typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};

typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;

};

struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};

typedef struct DalDeviceHandle * DALDEVICEHANDLE;

typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};

struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};






static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}

static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}

static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}

static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}

static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}

static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}

static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}

static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
# 218 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);






DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);





DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
# 314 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 2


typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};


typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;

typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};


typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};


typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};

typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};

typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};







typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};

typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};

typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);


typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};

typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;

typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;

typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;


typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};






typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;

    DALSYSMemObj *pObj;

    DALBOOL prealloc;
};

typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
# 243 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);

typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);

typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);

typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);

typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);

typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);

typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);

typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);

typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);

typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);

typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);

typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);

typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);

typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);

typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);

typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);

typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);

typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);

typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);

typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};


typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);

typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};

typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 10 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 11 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 2 3 4
# 29 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
typedef _Sizet size_t;




int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));

void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));


char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));

void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
# 106 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
# 130 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 227 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitMod(DALSYSConfig * pCfg);
# 238 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_DeInitMod(void);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
# 279 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
# 310 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 334 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 358 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
# 394 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
# 415 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
# 434 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
# 458 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
# 473 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
# 492 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
# 508 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
# 519 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
# 589 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
# 634 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
# 698 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
# 712 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
# 758 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Free(void *pmem);
# 771 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_BusyWait(uint32 pause_time_us);
# 786 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 802 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 833 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
# 865 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 882 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
# 900 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
# 913 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
# 927 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 DALSYS_SetThreadPriority(uint32 priority);
# 943 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 2





typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;

struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};

struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};

struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};

struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};

typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};

typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};

typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);

DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);

uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);

uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);


void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);

void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);

DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);






DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);

DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);

DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);

DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);

void memory_barrier(void);
# 232 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
# 284 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);






DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
# 322 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);






uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);






uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);







uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
# 370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
# 379 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
# 392 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);







void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
# 409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);






typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;






void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);






void DALFW_MPUnLock(DALFW_MPLOCK *pLock);






uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h" 1
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 144 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h" 2
# 57 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h"
typedef unsigned long int bool32;
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h" 2
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h" 2
# 121 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
static __inline uint8 *DalHWIO_DerefWeakPointer(uint8 **ppAddress)
{
  return (ppAddress != 0 ? *ppAddress : 0);
}
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
typedef struct DalHWIO DalHWIO;
struct DalHWIO
{
   struct DalDevice DalDevice;
   DALResult (*MapRegion)(DalDeviceHandle * _h, const char * szBase, uint8 **ppAddress);
   DALResult (*UnMapRegion)(DalDeviceHandle * _h, uint8 *pVirtAddress);
   DALResult (*MapRegionByAddress)(DalDeviceHandle * _h, uint8 *pPhysAddress, uint8 **ppAddress);
   DALResult (*MapAllRegions)(DalDeviceHandle * _h);
};

typedef struct DalHWIOHandle DalHWIOHandle;
struct DalHWIOHandle
{
   uint32 dwDalHandleId;
   const DalHWIO *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblen;
};
# 199 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
static __inline DALResult DalHWIO_MapRegion
(
  DalDeviceHandle *_h,
  const char *szBase,
  uint8 **ppAddress
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->MapRegion( _h, szBase, ppAddress);
}
# 235 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
static __inline DALResult DalHWIO_MapRegionByAddress
(
  DalDeviceHandle *_h,
  uint8 *pPhysAddress,
  uint8 **ppAddress
)
{
  return
    ((DalHWIOHandle *)_h)->pVtbl->MapRegionByAddress(
      _h, pPhysAddress, ppAddress);
}
# 276 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
static __inline DALResult DalHWIO_UnMapRegion
(
  DalDeviceHandle *_h,
  uint8 *pVirtAddress
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->UnMapRegion( _h, pVirtAddress);
}
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIHWIO.h"
static __inline DALResult DalHWIO_MapAllRegions
(
  DalDeviceHandle *_h
)
{
  return ((DalHWIOHandle *)_h)->pVtbl->MapAllRegions(_h);
}
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h" 2








typedef struct HWIODrvCtxt HWIODrvCtxt;
typedef struct HWIODevCtxt HWIODevCtxt;
typedef struct HWIOClientCtxt HWIOClientCtxt;
# 38 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h"
typedef struct
{
  char *szName;
  uint32 nOffset;
  uint32 dwSizeInBytes;
} HWIOModuleType;







typedef struct
{
  char *szName;
  DALSYSMemAddr pPhysAddr;
  uint32 dwSizeInBytes;
  DALSYSMemAddr pVirtAddr;
  HWIOModuleType *pModules;
} HWIOPhysRegionType;
# 67 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/src/DalHWIO.h"
typedef struct
{
  DALSYSMemAddr pVirtAddr;
  uint16 dwRefCount;
  DALSYSMemHandle hMem;
} HWIOVirtRegionType;






typedef struct HWIODALVtbl HWIODALVtbl;
struct HWIODALVtbl
{
  int (*HWIO_DriverInit)(HWIODrvCtxt *);
  int (*HWIO_DriverDeInit)(HWIODrvCtxt *);
};

struct HWIODevCtxt
{



  uint32 dwRefs;
  DALDEVICEID DevId;
  uint32 dwDevCtxtRefIdx;
  HWIODrvCtxt *pHWIODrvCtxt;
  uint32 hProp[2];
  uint32 Reserved[16];




};

struct HWIODrvCtxt
{



  HWIODALVtbl HWIODALVtbl;
  uint32 dwNumDev;
  uint32 dwSizeDevCtxt;
  uint32 bInit;
  uint32 dwRefs;
  HWIODevCtxt HWIODevCtxt[1];




  HWIOPhysRegionType *PhysMap;
  HWIOVirtRegionType *VirtMap;




  DALSYSSyncHandle hMapSync;
  DALSYSSyncObj mMapSyncObj;
};





struct HWIOClientCtxt
{



  uint32 dwRefs;
  uint32 dwAccessMode;
  void *pPortCtxt;
  HWIODevCtxt *pHWIODevCtxt;
  DalHWIOHandle DalHWIOHandle;




};






DALResult HWIO_DriverInit(HWIODrvCtxt *);
DALResult HWIO_DriverDeInit(HWIODrvCtxt *);
DALResult HWIO_DeviceInit(HWIOClientCtxt *);
DALResult HWIO_DeviceDeInit(HWIOClientCtxt *);
DALResult HWIO_Reset(HWIOClientCtxt *);
DALResult HWIO_PowerEvent(HWIOClientCtxt *, DalPowerCmd, DalPowerDomain);
DALResult HWIO_Open(HWIOClientCtxt *, uint32);
DALResult HWIO_Close(HWIOClientCtxt *);
DALResult HWIO_Info(HWIOClientCtxt *,DalDeviceInfo *, uint32);
DALResult HWIO_InheritObjects(HWIOClientCtxt *,DALInheritSrcPram *,DALInheritDestPram *);





DALResult HWIO_MapRegion( HWIODrvCtxt *, const char *, uint8 **);
DALResult HWIO_MapRegionByAddress( HWIODrvCtxt *, uint8 *, uint8 **);
DALResult HWIO_UnMapRegion( HWIODrvCtxt *, uint8 *);
DALResult HWIO_MapAllRegions( HWIODrvCtxt *);





void HWIO_InitBasePointers (HWIODrvCtxt *);
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/hwio/msm8996/msmhwiobase.h" 1
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/hwio/config/msm8996/HWIOBaseMap.c" 2






static HWIOModuleType HWIOModules_SPDM_WRAPPER_TOP[] =
{
  { "SPDM_SPDM_CREG", 0x00000000, 0x00000120 },
  { "SPDM_SPDM_OLEM", 0x00001000, 0x0000015c },
  { "SPDM_SPDM_RTEM", 0x00002000, 0x00000318 },
  { "SPDM_APU0132_1", 0x00003000, 0x00000280 },
  { "SPDM_SPDM_SREG", 0x00004000, 0x00000120 },
  { "SPDM_VMIDMT_IDX_1_SSD0", 0x00005000, 0x00001000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS[] =
{
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_SECURITY_CONTROL[] =
{
  { "SECURITY_CONTROL_CORE", 0x00000000, 0x00007000 },
  { "SECURE_CHANNEL", 0x00008000, 0x00004200 },
  { "KEY_CTRL", 0x00008000, 0x00004000 },
  { "CRI_CM", 0x0000c000, 0x00000100 },
  { "CRI_CM_EXT", 0x0000c100, 0x00000100 },
  { "SEC_CTRL_APU_APU1132_37", 0x0000e000, 0x00001480 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM[] =
{
  { "RPM_DEC", 0x00080000, 0x00002000 },
  { "RPM_QTMR_AC", 0x00082000, 0x00001000 },
  { "RPM_F0_QTMR_V1_F0", 0x00083000, 0x00001000 },
  { "RPM_F1_QTMR_V1_F1", 0x00084000, 0x00001000 },
  { "RPM_MSTR_MPU", 0x00089000, 0x00000c00 },
  { "RPM_VMIDMT", 0x00088000, 0x00001000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_CLK_CTL[] =
{
  { "GCC_CLK_CTL_REG", 0x00000000, 0x00090000 },
  { "GCC_RPU_RPU0032_144_L12", 0x00090000, 0x00004a00 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_MPM2_MPM[] =
{
  { "MPM2_MPM", 0x00000000, 0x00001000 },
  { "MPM2_G_CTRL_CNTR", 0x00001000, 0x00001000 },
  { "MPM2_G_RD_CNTR", 0x00002000, 0x00001000 },
  { "MPM2_SLP_CNTR", 0x00003000, 0x00001000 },
  { "MPM2_QTIMR_AC", 0x00004000, 0x00001000 },
  { "MPM2_QTIMR_V1", 0x00005000, 0x00001000 },
  { "MPM2_TSYNC", 0x00006000, 0x00001000 },
  { "MPM2_APU", 0x00007000, 0x00000880 },
  { "MPM2_TSENS0", 0x00008000, 0x00001000 },
  { "MPM2_TSENS0_TSENS0_TM", 0x00009000, 0x00001000 },
  { "MPM2_WDOG", 0x0000a000, 0x00000020 },
  { "MPM2_PSHOLD", 0x0000b000, 0x00001000 },
  { "MPM2_TSENS1", 0x0000c000, 0x00001000 },
  { "MPM2_TSENS1_TSENS1_TM", 0x0000d000, 0x00001000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_CONFIG_NOC[] =
{
  { "CONFIG_NOC", 0x00000000, 0x00000080 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_SYSTEM_NOC[] =
{
  { "SYSTEM_NOC", 0x00000000, 0x0000a100 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_A2_NOC_AGGRE2_NOC[] =
{
  { "A2_NOC_AGGRE2_NOC", 0x00000000, 0x00008100 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_CORE_TOP_CSR[] =
{
  { "TCSR_MUTEX_RPU1132_64_L12", 0x00000000, 0x00002200 },
  { "TCSR_TCSR_MUTEX", 0x00040000, 0x00040000 },
  { "TCSR_REGS_RPU1132_32_L12", 0x00080000, 0x00001200 },
  { "TCSR_TCSR_REGS", 0x000a0000, 0x00020000 },
  { "TCSR_VREF_VREF_CM_QREFS_VBG_INT_VREF_SW", 0x000b9000, 0x00000080 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_TLMM[] =
{
  { "TLMM_APU1032_175", 0x00000000, 0x00005980 },
  { "TLMM_CSR", 0x00010000, 0x00300000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_QDSS_ISTARI[] =
{
  { "QDSS_DAPROM", 0x00000000, 0x00001000 },
  { "QDSS_QDSSCSR", 0x00001000, 0x00001000 },
  { "QDSS_CXSTM_8_32_32_TRUE", 0x00002000, 0x00001000 },
  { "QDSS_TPDA_TPDA_TPDA_S8_W64_D8_M64_CSF9D1CB23", 0x00003000, 0x00001000 },
  { "QDSS_TPDM_TPDM_TPDM_ATB64_ATCLK_DSB256_CS319DD72A", 0x00004000, 0x00001000 },
  { "QDSS_CTI0_CTI0_CSCTI", 0x00010000, 0x00001000 },
  { "QDSS_CTI1_CTI1_CSCTI", 0x00011000, 0x00001000 },
  { "QDSS_CTI2_CTI2_CSCTI", 0x00012000, 0x00001000 },
  { "QDSS_CTI3_CTI3_CSCTI", 0x00013000, 0x00001000 },
  { "QDSS_CTI4_CTI4_CSCTI", 0x00014000, 0x00001000 },
  { "QDSS_CTI5_CTI5_CSCTI", 0x00015000, 0x00001000 },
  { "QDSS_CTI6_CTI6_CSCTI", 0x00016000, 0x00001000 },
  { "QDSS_CTI7_CTI7_CSCTI", 0x00017000, 0x00001000 },
  { "QDSS_CTI8_CTI8_CSCTI", 0x00018000, 0x00001000 },
  { "QDSS_CTI9_CTI9_CSCTI", 0x00019000, 0x00001000 },
  { "QDSS_CTI10_CTI10_CSCTI", 0x0001a000, 0x00001000 },
  { "QDSS_CTI11_CTI11_CSCTI", 0x0001b000, 0x00001000 },
  { "QDSS_CTI12_CTI12_CSCTI", 0x0001c000, 0x00001000 },
  { "QDSS_CTI13_CTI13_CSCTI", 0x0001d000, 0x00001000 },
  { "QDSS_CTI14_CTI14_CSCTI", 0x0001e000, 0x00001000 },
  { "QDSS_CSTPIU_CSTPIU_CSTPIU", 0x00020000, 0x00001000 },
  { "QDSS_IN_FUN0_IN_FUN0_CXATBFUNNEL_128W8SP", 0x00021000, 0x00001000 },
  { "QDSS_IN_FUN1_IN_FUN1_CXATBFUNNEL_128W8SP", 0x00022000, 0x00001000 },
  { "QDSS_IN_FUN2_IN_FUN2_CXATBFUNNEL_128W8SP", 0x00023000, 0x00001000 },
  { "QDSS_IN_FUN3_IN_FUN3_CXATBFUNNEL_128W8SP", 0x00024000, 0x00001000 },
  { "QDSS_MERG_FUN_MERG_FUN_CXATBFUNNEL_128W4SP", 0x00025000, 0x00001000 },
  { "QDSS_REPL64_REPL64_CXATBREPLICATOR_64WP", 0x00026000, 0x00001000 },
  { "QDSS_ETFETB_ETFETB_CXTMC_F128W64K", 0x00027000, 0x00001000 },
  { "QDSS_ETR_ETR_CXTMC_R64W32D", 0x00028000, 0x00001000 },
  { "QDSS_SSC_CTI_0_SSC_CTI_0_CSCTI", 0x00030000, 0x00001000 },
  { "QDSS_SSC_CTI_1_SSC_CTI_1_CSCTI", 0x00031000, 0x00001000 },
  { "QDSS_SSC_STM_SSC_STM_CXSTM_2_4_4_TRUE", 0x00033000, 0x00001000 },
  { "QDSS_SSC_FUN0_SSC_FUN0_CXATBFUNNEL_32W2SP", 0x00034000, 0x00001000 },
  { "QDSS_SSC_ETFETB_SSC_ETFETB_CXTMC_F32W8K", 0x00035000, 0x00001000 },
  { "QDSS_VSENSE_CONTROLLER", 0x00038000, 0x00001000 },
  { "QDSS_MSS_QDSP6_CTI_MSS_QDSP6_CTI_CSCTI", 0x00040000, 0x00001000 },
  { "QDSS_QDSP6_CTI_QDSP6_CTI_CSCTI", 0x00044000, 0x00001000 },
  { "QDSS_RPM_M3_CTI_RPM_M3_CTI_CSCTI", 0x00048000, 0x00001000 },
  { "QDSS_PRNG_TPDM_PRNG_TPDM_TPDM_ATB32_APCLK_CMB32_CS5CFE4153", 0x0004c000, 0x00001000 },
  { "QDSS_PIMEM_TPDM_PIMEM_TPDM_TPDM_ATB64_APCLK_GPRCLK_BC8_TC2_CMB64_DSB64_CS4528A7E6", 0x00050000, 0x00001000 },
  { "QDSS_DCC_TPDM_DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1", 0x00054000, 0x00001000 },
  { "QDSS_ISTARI_APB2JTAG", 0x00058000, 0x00004000 },
  { "QDSS_VMIDDAP_VMIDDAP_VMIDMT_IDX_2_SSD1", 0x00076000, 0x00001000 },
  { "QDSS_VMIDETR_VMIDETR_VMIDMT_IDX_2_SSD1", 0x00077000, 0x00001000 },
  { "QDSS_NDPBAM_NDPBAM_BAM_NDP_TOP_AUTO_SCALE_V2_0", 0x00080000, 0x00019000 },
  { "QDSS_NDPBAM_BAM", 0x00084000, 0x00015000 },
  { "QDSS_ISDB_CTI_ISDB_CTI_CSCTI", 0x00141000, 0x00001000 },
  { "QDSS_GPMU_CTI_GPMU_CTI_CSCTI", 0x00142000, 0x00001000 },
  { "QDSS_ARM9_CTI_ARM9_CTI_CSCTI", 0x00180000, 0x00001000 },
  { "QDSS_VENUS_ARM9_ETM_VENUS_ARM9_ETM_A9ETM_V2", 0x00181000, 0x00001000 },
  { "QDSS_MMSS_FUNNEL_MMSS_FUNNEL_CXATBFUNNEL_64W8SP", 0x00184000, 0x00001000 },
  { "QDSS_MMSS_DSAT_TPDM_MMSS_DSAT_TPDM_TPDM_ATB32_APCLK_GPRCLK_BC32_TC3_DSB128_CS7D2CD278", 0x00185000, 0x00001000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_PMIC_ARB[] =
{
  { "SPMI_CFG_TOP", 0x00000000, 0x0000d000 },
  { "SPMI_GENI_CFG", 0x0000a000, 0x00000700 },
  { "SPMI_CFG", 0x0000a700, 0x00001a00 },
  { "SPMI_PIC", 0x01800000, 0x00200000 },
  { "PMIC_ARB_MPU1132_18_M25L12_AHB", 0x0000e000, 0x00000b00 },
  { "PMIC_ARB_CORE", 0x0000f000, 0x00001000 },
  { "PMIC_ARB_CORE_REGISTERS", 0x00400000, 0x00800000 },
  { "PMIC_ARB_CORE_REGISTERS_OBS", 0x00c00000, 0x00800000 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_PERIPH_SS[] =
{
  { "PERIPH_SS_PDM_PERPH_WEB", 0x00000000, 0x00004000 },
  { "PERIPH_SS_PDM_WEB_TCXO4", 0x00000000, 0x00004000 },
  { "PERIPH_SS_BUS_TIMEOUT_0_BUS_TIMEOUT", 0x00004000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_1_BUS_TIMEOUT", 0x00005000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_2_BUS_TIMEOUT", 0x00006000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_3_BUS_TIMEOUT", 0x00007000, 0x00001000 },
  { "PERIPH_SS_BUS_TIMEOUT_4_BUS_TIMEOUT", 0x00008000, 0x00001000 },
  { "PERIPH_SS_MPU_MPU1132A_4_M15L7_AHB", 0x0000f000, 0x00000400 },
  { "PERIPH_SS_USB_PHYS_AHB2PHY", 0x00010000, 0x00008000 },
  { "PERIPH_SS_AHB2PHY_BROADCAST_SWMAN", 0x00017000, 0x00000400 },
  { "PERIPH_SS_AHB2PHY_SWMAN", 0x00016000, 0x00000400 },
  { "PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_SEC_CM_QUSB2_LQ_1EX", 0x00012000, 0x00000180 },
  { "PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PRIM_CM_QUSB2_LQ_1EX", 0x00011000, 0x00000180 },
  { "PERIPH_SS_USB3PHY_USB3PHY_CM_USB3_SW", 0x00010000, 0x00000800 },
  { "PERIPH_SS_USB3PHY_QSERDES_COM_QSERDES_COM_USB3_QSRV_COM", 0x00010000, 0x000001c4 },
  { "PERIPH_SS_USB3PHY_QSERDES_TX_QSERDES_TX_USB3_TX_MPHY", 0x00010200, 0x00000130 },
  { "PERIPH_SS_USB3PHY_QSERDES_RX_QSERDES_RX_USB3_RX_MPHY", 0x00010400, 0x00000200 },
  { "PERIPH_SS_USB3PHY_USB3_PHY_USB3_PHY_USB3_PCS", 0x00010600, 0x000001a8 },
  { "PERIPH_SS_QSPI_QSPI", 0x00018000, 0x00000100 },
  { "PERIPH_SS_SDC1_SDCC5_TOP", 0x00040000, 0x00026800 },
  { "PERIPH_SS_SDC1_SDCC_XPU2", 0x00042000, 0x00000300 },
  { "PERIPH_SS_SDC1_SDCC_ICE", 0x00043000, 0x00008000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_REGS", 0x00043000, 0x00002000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_LUT_KEYS", 0x00045000, 0x00001000 },
  { "PERIPH_SS_SDC1_SDCC_ICE_XPU2", 0x00046000, 0x00000380 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5", 0x00064000, 0x00000800 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC", 0x00064900, 0x00000500 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_CMDQ", 0x00064e00, 0x00000200 },
  { "PERIPH_SS_SDC1_SDCC_SDCC5_HC_SCM", 0x00065000, 0x00000100 },
  { "PERIPH_SS_SDC1_SDCC_SCM", 0x00066000, 0x00000800 },
  { "PERIPH_SS_SDC2_SDCC5_TOP", 0x00080000, 0x00026800 },
  { "PERIPH_SS_SDC2_SDCC_XPU2", 0x00082000, 0x00000300 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5", 0x000a4000, 0x00000800 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC", 0x000a4900, 0x00000500 },
  { "PERIPH_SS_SDC2_SDCC_SDCC5_HC_SCM", 0x000a5000, 0x00000100 },
  { "PERIPH_SS_SDC2_SDCC_SCM", 0x000a6000, 0x00000800 },
  { "PERIPH_SS_SDC4_SDCC5_TOP", 0x00100000, 0x00026800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5", 0x00124000, 0x00000800 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC", 0x00124900, 0x00000500 },
  { "PERIPH_SS_SDC4_SDCC_SDCC5_HC_SCM", 0x00125000, 0x00000100 },
  { "PERIPH_SS_BLSP1_BLSP", 0x00140000, 0x00040000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM", 0x00144000, 0x0002b000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_XPU2", 0x00142000, 0x00002000 },
  { "PERIPH_SS_BLSP1_BLSP_BAM_VMIDMT", 0x00140000, 0x00001000 },
  { "PERIPH_SS_BLSP1_BLSP_UART0_UART0_DM", 0x0016f000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART1_UART1_DM", 0x00170000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART2_UART2_DM", 0x00171000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART3_UART3_DM", 0x00172000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART4_UART4_DM", 0x00173000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_UART5_UART5_DM", 0x00174000, 0x00000200 },
  { "PERIPH_SS_BLSP1_BLSP_QUP0", 0x00175000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP1", 0x00176000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP2", 0x00177000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP3", 0x00178000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP4", 0x00179000, 0x00000600 },
  { "PERIPH_SS_BLSP1_BLSP_QUP5", 0x0017a000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP", 0x00180000, 0x00040000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM", 0x00184000, 0x0002b000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_XPU2", 0x00182000, 0x00002000 },
  { "PERIPH_SS_BLSP2_BLSP_BAM_VMIDMT", 0x00180000, 0x00001000 },
  { "PERIPH_SS_BLSP2_BLSP_UART0_UART0_DM", 0x001af000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART1_UART1_DM", 0x001b0000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART2_UART2_DM", 0x001b1000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART3_UART3_DM", 0x001b2000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART4_UART4_DM", 0x001b3000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_UART5_UART5_DM", 0x001b4000, 0x00000200 },
  { "PERIPH_SS_BLSP2_BLSP_QUP0", 0x001b5000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP1", 0x001b6000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP2", 0x001b7000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP3", 0x001b8000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP4", 0x001b9000, 0x00000600 },
  { "PERIPH_SS_BLSP2_BLSP_QUP5", 0x001ba000, 0x00000600 },
  { "PERIPH_SS_TSIF_TSIF_12SEG_WRAPPER", 0x001c0000, 0x00040000 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_BAM_LITE_TOP_AUTO_SCALE_V2_0", 0x001c0000, 0x00027000 },
  { "PERIPH_SS_TSIF_TSIF_BAM", 0x001c4000, 0x00023000 },
  { "PERIPH_SS_TSIF_TSIF_0_TSIF_0_TSIF", 0x001e7000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_1_TSIF_1_TSIF", 0x001e8000, 0x00000200 },
  { "PERIPH_SS_TSIF_TSIF_TSIF_TSPP", 0x001e9000, 0x00001000 },
  { "PERIPH_SS_USB1_HS_USB20S", 0x00200000, 0x00200000 },
  { "PERIPH_SS_USB1_HS_DWC_USB3", 0x00200000, 0x0000cd00 },
  { "PERIPH_SS_USB1_HS_USB30_QSCRATCH", 0x002f8800, 0x00000400 },
  { "PERIPH_SS_USB1_HS_USB30_QSRAM_REGS", 0x002fc000, 0x00000100 },
  { "PERIPH_SS_USB1_HS_USB30_DBM_REGFILE", 0x002f8000, 0x00000400 },
  { 0, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS[] =
{
  { "LPASS_LPASS_CC_REG", 0x00000000, 0x0003f000 },
  { "LPASS_LPASS_TCSR", 0x00080000, 0x0001f000 },
  { "LPASS_LPASS_QOS", 0x000a0000, 0x00004000 },
  { "LPASS_QOS_QOS_GENERIC", 0x000a0000, 0x00000100 },
  { "LPASS_QOS_QOS_DEBUG", 0x000a0100, 0x00000200 },
  { "LPASS_QOS_QOS_DANGER", 0x000a0300, 0x00000200 },
  { "LPASS_QOS_QOS_INTERRUPTS", 0x000a1000, 0x00002000 },
  { "LPASS_QOS_QOS_THROTTLE_WRAPPER_1", 0x000a3000, 0x00001000 },
  { "LPASS_AHBE_TIME", 0x000b7000, 0x00001000 },
  { "LPASS_Q6SS_MPU", 0x000b8000, 0x00001600 },
  { "LPASS_LPASS_CSR", 0x000c0000, 0x0001d000 },
  { "LPASS_AHBI_TIME", 0x000f6000, 0x00001000 },
  { "LPASS_LPASS_SYNC_WRAPPER", 0x000f7000, 0x00000ffd },
  { "LPASS_AVTIMER", 0x000f7000, 0x00000100 },
  { "LPASS_LPASS_HDMITX", 0x000f8000, 0x00007000 },
  { "LPASS_ATIMER", 0x000ff000, 0x00000100 },
  { "LPASS_LPA_IF", 0x00100000, 0x0001fffd },
  { "LPASS_LPASS_LPM", 0x00120000, 0x00010000 },
  { "LPASS_TLB_PL", 0x00130000, 0x00005000 },
  { "LPASS_RESAMPLER", 0x00138000, 0x00008000 },
  { "LPASS_CORE_LPASS_CORE_QOS", 0x00140000, 0x00003000 },
  { "LPASS_CORE_QOS_QOS_GENERIC", 0x00140000, 0x00000100 },
  { "LPASS_CORE_QOS_QOS_CORE_DEBUG", 0x00140100, 0x00000200 },
  { "LPASS_CORE_QOS_QOS_INTERRUPTS", 0x00141000, 0x00002000 },
  { "LPASS_AUD_SBMASTER0_BASE", 0x00180000, 0x00080000 },
  { "LPASS_AUD_SB_SLIMBUS_BAM_LITE", 0x00180000, 0x00036000 },
  { "LPASS_AUD_SB_BAM", 0x00184000, 0x00032000 },
  { "LPASS_AUD_SLIMBUS", 0x001c0000, 0x0002c000 },
  { "LPASS_QCA_SBMASTER0_BASE", 0x00200000, 0x00080000 },
  { "LPASS_QCA_SB_SLIMBUS_BAM_LITE", 0x00200000, 0x0002a000 },
  { "LPASS_QCA_SB_BAM", 0x00204000, 0x00026000 },
  { "LPASS_QCA_SLIMBUS", 0x00240000, 0x0002c000 },
  { "LPASS_QDSP6V60SS_PUBLIC", 0x00300000, 0x00080000 },
  { "LPASS_QDSP6V60SS_PUB", 0x00300000, 0x00004040 },
  { "LPASS_QDSP6V60SS_PRIVATE", 0x00380000, 0x00080000 },
  { "LPASS_QDSP6V60SS_CSR", 0x00380000, 0x00008028 },
  { "LPASS_QDSP6V60SS_L2VIC", 0x00390000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_QTMR_AC", 0x003a0000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F0_0", 0x003a1000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F1_1", 0x003a2000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F2_2", 0x003a3000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_SAW3", 0x003b0000, 0x00001000 },
  { 0, 0, 0 }
};

HWIOPhysRegionType HWIOBaseMap[] =
{
  {
    "SPDM_WRAPPER_TOP",
    (DALSYSMemAddr)0x00048000,
    0x00006000,
    (DALSYSMemAddr)0xe0048000,
    HWIOModules_SPDM_WRAPPER_TOP
  },
  {
    "RPM_SS_MSG_RAM_START_ADDRESS",
    (DALSYSMemAddr)0x00068000,
    0x00006000,
    (DALSYSMemAddr)0xe0168000,
    HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS
  },
  {
    "SECURITY_CONTROL",
    (DALSYSMemAddr)0x00070000,
    0x00010000,
    (DALSYSMemAddr)0xe0270000,
    HWIOModules_SECURITY_CONTROL
  },
  {
    "RPM",
    (DALSYSMemAddr)0x00200000,
    0x00090000,
    (DALSYSMemAddr)0xe0300000,
    HWIOModules_RPM
  },
  {
    "CLK_CTL",
    (DALSYSMemAddr)0x00300000,
    0x000a0000,
    (DALSYSMemAddr)0xe0400000,
    HWIOModules_CLK_CTL
  },
  {
    "MPM2_MPM",
    (DALSYSMemAddr)0x004a0000,
    0x0000e000,
    (DALSYSMemAddr)0xe05a0000,
    HWIOModules_MPM2_MPM
  },
  {
    "CONFIG_NOC",
    (DALSYSMemAddr)0x00500000,
    0x00001000,
    (DALSYSMemAddr)0xe0600000,
    HWIOModules_CONFIG_NOC
  },
  {
    "SYSTEM_NOC",
    (DALSYSMemAddr)0x00520000,
    0x0000b000,
    (DALSYSMemAddr)0xe0720000,
    HWIOModules_SYSTEM_NOC
  },
  {
    "A2_NOC_AGGRE2_NOC",
    (DALSYSMemAddr)0x00580000,
    0x00009000,
    (DALSYSMemAddr)0xe0880000,
    HWIOModules_A2_NOC_AGGRE2_NOC
  },
  {
    "CORE_TOP_CSR",
    (DALSYSMemAddr)0x00700000,
    0x000c0000,
    (DALSYSMemAddr)0xe0900000,
    HWIOModules_CORE_TOP_CSR
  },
  {
    "TLMM",
    (DALSYSMemAddr)0x01000000,
    0x00300000,
    (DALSYSMemAddr)0xe0c00000,
    HWIOModules_TLMM
  },
  {
    "QDSS_QDSS_ISTARI",
    (DALSYSMemAddr)0x03000000,
    0x00800000,
    (DALSYSMemAddr)0xe1000000,
    HWIOModules_QDSS_QDSS_ISTARI
  },
  {
    "PMIC_ARB",
    (DALSYSMemAddr)0x04000000,
    0x02000000,
    (DALSYSMemAddr)0xe2000000,
    HWIOModules_PMIC_ARB
  },
  {
    "PERIPH_SS",
    (DALSYSMemAddr)0x07400000,
    0x00400000,
    (DALSYSMemAddr)0xe4000000,
    HWIOModules_PERIPH_SS
  },
  {
    "LPASS",
    (DALSYSMemAddr)0x09000000,
    0x00400000,
    (DALSYSMemAddr)0xee000000,
    HWIOModules_LPASS
  },
  { 0, 0, 0, 0, 0 }
};
