typedef unsigned char boolean;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
      typedef long long int64;
      typedef unsigned long long uint64;
typedef void * addr_t;
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum
{
  NPA_RESOURCE_DEFAULT = 0,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,
  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,
  NPA_RESOURCE_VECTOR_STATE = 0x00000004,
  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_REMOTE = 0x00000010,
  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,
  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,
  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,
  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,
  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,
  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,
} npa_resource_attribute;
typedef enum
{
  NPA_NODE_DEFAULT = 0,
  NPA_NODE_NO_LOCK = 0x00000001,
  NPA_NODE_DISABLEABLE = 0x00000002,
  NPA_NODE_FORKABLE = 0x00000004,
} npa_node_attribute;
enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,
  NPA_QUERY_RESOURCE_ATTRIBUTES,
  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};
typedef void* npa_user_data;
typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;
typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;
typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];
  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;
  unsigned int request_attr;
  void (*issue_request)( npa_client_handle client, int new_request );
  struct npa_scheduler_data *request_ptr;
} npa_client;
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;
  const char *name;
  struct npa_resource *resource;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;
  npa_event_callback callback;
  void *reg_data;
  struct npa_event_action *action;
} npa_event;
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );
typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );
typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
  void (*create_client_fcn) ( npa_client * );
  void (*destroy_client_fcn)( npa_client * );
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;
  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;
  struct npa_resource *handle;
} npa_resource_definition;
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;
typedef struct npa_resource
{
  npa_resource_definition *definition;
  npa_node_definition *node;
  unsigned int index;
  npa_client *clients;
  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;
  const npa_resource_plugin *active_plugin;
  npa_resource_state request_state;
  npa_resource_state active_state;
  npa_resource_state internal_state[8];
  npa_resource_state *state_vector;
  npa_resource_state *required_state_vector;
  npa_resource_state *suppressible_state_vector;
  npa_resource_state *suppressible2_state_vector;
  npa_resource_state *semiactive_state_vector;
  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;
  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;
  struct npa_resource_internal_data *_internal;
  unsigned int sequence;
  void *log_handle;
  npa_resource_latency *latency;
} npa_resource;
typedef void* npa_join_token;
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
void npa_define_marker( const char *marker_name );
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
void npa_disable_node( npa_node_definition *node );
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
void npa_resource_lock( npa_resource *resource );
int npa_resource_trylock( npa_resource *resource );
void npa_resource_unlock( npa_resource *resource );
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
unsigned int npa_get_request_attributes( npa_client_handle client );
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
extern const npa_resource_plugin npa_binary_plugin;
extern const npa_resource_plugin npa_max_plugin;
extern const npa_resource_plugin npa_min_plugin;
extern const npa_resource_plugin npa_sum_plugin;
extern const npa_resource_plugin npa_identity_plugin;
extern const npa_resource_plugin npa_always_on_plugin;
extern const npa_resource_plugin npa_impulse_plugin;
extern const npa_resource_plugin npa_or_plugin;
extern const npa_resource_plugin npa_binary_and_plugin;
extern const npa_resource_plugin npa_no_client_plugin;
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);
npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );
npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);
npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);
npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );
npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );
npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
void npa_issue_internal_request( npa_client_handle client );
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
typedef enum
{
  VCS_RAIL_APCS,
  VCS_RAIL_CX,
  VCS_RAIL_MSS,
  VCS_RAIL_MX,
  VCS_RAIL_NUM_OF_RAILS
} VCSRailType;
typedef enum
{
  VCS_CORNER_OFF = 0,
  VCS_CORNER_RETENTION = 1,
  VCS_CORNER_LOW_MINUS = 2,
  VCS_CORNER_LOW = 3,
  VCS_CORNER_LOW_PLUS = 4,
  VCS_CORNER_NOMINAL = 5,
  VCS_CORNER_NOMINAL_PLUS = 6,
  VCS_CORNER_TURBO = 7,
  VCS_CORNER_MAX = VCS_CORNER_TURBO,
  VCS_CORNER_NUM_OF_CORNERS
} VCSCornerType;
typedef enum
{
  VCS_RAIL_MODE_CPR = 0,
  VCS_RAIL_MODE_MVC = 1,
  VCS_RAIL_MODE_NUM_OF_MODES
} VCSRailModeType;
typedef enum
{
  VCS_NPA_RAIL_EVENT_PRE_CHANGE = NPA_TRIGGER_CUSTOM_EVENT1,
  VCS_NPA_RAIL_EVENT_POST_CHANGE = NPA_TRIGGER_CUSTOM_EVENT2,
  VCS_NPA_RAIL_EVENT_LIMIT_MAX = NPA_TRIGGER_CUSTOM_EVENT3,
} VCSNPARailEventType;
typedef enum
{
  VCS_NPA_LDO_EVENT_PRE_CHANGE = NPA_TRIGGER_CUSTOM_EVENT1,
  VCS_NPA_LDO_EVENT_POST_CHANGE = NPA_TRIGGER_CUSTOM_EVENT2,
} VCSNPALDOEventType;
typedef struct
{
  struct
  {
    VCSCornerType eCorner;
  } PreChange;
  struct
  {
    VCSCornerType eCorner;
  } PostChange;
} VCSNPARailEventDataType;
typedef struct
{
  struct
  {
    VCSCornerType eCorner;
    uint32 nVoltageUV;
  } PreChange;
  struct
  {
    VCSCornerType eCorner;
    uint32 nVoltageUV;
  } PostChange;
} VCSNPALDOEventDataType;
typedef uint32 ClockIdType;
typedef uint32 SourceIdType;
typedef enum
{
  CLOCK_FREQUENCY_HZ_AT_LEAST = 0,
  CLOCK_FREQUENCY_HZ_AT_MOST = 1,
  CLOCK_FREQUENCY_HZ_CLOSEST = 2,
  CLOCK_FREQUENCY_HZ_EXACT = 3,
  CLOCK_FREQUENCY_KHZ_AT_LEAST = 0x10,
  CLOCK_FREQUENCY_KHZ_AT_MOST = 0x11,
  CLOCK_FREQUENCY_KHZ_CLOSEST = 0x12,
  CLOCK_FREQUENCY_KHZ_EXACT = 0x13,
  CLOCK_FREQUENCY_MHZ_AT_LEAST = 0x20,
  CLOCK_FREQUENCY_MHZ_AT_MOST = 0x21,
  CLOCK_FREQUENCY_MHZ_CLOSEST = 0x22,
  CLOCK_FREQUENCY_MHZ_EXACT = 0x23,
} ClockFrequencyType;
typedef enum
{
  CLOCK_RESET_DEASSERT = 0,
  CLOCK_RESET_ASSERT = 1,
  CLOCK_RESET_PULSE = 2
} ClockResetType;
typedef enum
{
  CLOCK_QDSS_LEVEL_OFF,
  CLOCK_QDSS_LEVEL_DEBUG,
  CLOCK_QDSS_LEVEL_HSDEBUG,
} ClockQDSSLevelType;
typedef enum
{
  CLOCK_CONFIG_MMSS_SOURCE_CSI0 = 1,
  CLOCK_CONFIG_MMSS_SOURCE_CSI1 = 2,
  CLOCK_CONFIG_MMSS_SOURCE_CSI2 = 3,
  CLOCK_CONFIG_LPASS_CORE_MEM_ON = 4,
  CLOCK_CONFIG_LPASS_CORE_MEM_OFF = 5,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_ON = 6,
  CLOCK_CONFIG_LPASS_PERIPH_MEM_OFF = 7,
  CLOCK_CONFIG_LPASS__HW_CTL_ON = 8,
  CLOCK_CONFIG_LPASS__HW_CTL_OFF = 9,
  CLOCK_CONFIG_AUTOGATE_ENABLE = 10,
  CLOCK_CONFIG_AUTOGATE_DISABLE = 11,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_DIG_CODEC = 12,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_PRI_MI2S = 13,
  CLOCK_CONFIG_DIG_CODEC_SRC_SEL_SEC_MI2S = 14,
  CLOCK_CONFIG_TOTAL = 15
} ClockConfigType;
typedef enum
{
  CLOCK_SOURCE_NONE,
  CLOCK_SOURCE_PRIMARY,
  CLOCK_SOURCE_SECONDARY,
  CLOCK_SOURCE_TERNERY,
  CLOCK_SOURCE_TOTAL
}ClockSourceType;
typedef enum
{
  CLOCK_LOG_STATE_CLOCK_FREQUENCY = (1 << 0),
} ClockLogStateFlags;
typedef uint32 ClockPowerDomainIdType;
typedef enum
{
  CLOCK_SLEEP_MODE_HALT,
  CLOCK_SLEEP_MODE_POWER_COLLAPSE
} ClockSleepModeType;
typedef enum
{
  CLOCK_CPU_MSS_Q6,
  CLOCK_CPU_LPASS_Q6,
  CLOCK_CPU_APPS_C0,
  CLOCK_CPU_APPS_C1,
  CLOCK_CPU_APPS_C2,
  CLOCK_CPU_APPS_C3,
  CLOCK_CPU_NUM_OF_CPUS
} ClockCPUType;
typedef uint32 DalChipInfoVersionType;
typedef uint32 DalChipInfoModemType;
typedef enum
{
  DALCHIPINFO_ID_UNKNOWN = 0,
  DALCHIPINFO_ID_MDM1000 = 1,
  DALCHIPINFO_ID_ESM6235 = 2,
  DALCHIPINFO_ID_QSC6240 = 3,
  DALCHIPINFO_ID_MSM6245 = 4,
  DALCHIPINFO_ID_MSM6255 = 5,
  DALCHIPINFO_ID_MSM6255A = 6,
  DALCHIPINFO_ID_MSM6260 = 7,
  DALCHIPINFO_ID_MSM6246 = 8,
  DALCHIPINFO_ID_QSC6270 = 9,
  DALCHIPINFO_ID_MSM6280 = 10,
  DALCHIPINFO_ID_MSM6290 = 11,
  DALCHIPINFO_ID_MSM7200 = 12,
  DALCHIPINFO_ID_MSM7201 = 13,
  DALCHIPINFO_ID_ESM7205 = 14,
  DALCHIPINFO_ID_ESM7206 = 15,
  DALCHIPINFO_ID_MSM7200A = 16,
  DALCHIPINFO_ID_MSM7201A = 17,
  DALCHIPINFO_ID_ESM7205A = 18,
  DALCHIPINFO_ID_ESM7206A = 19,
  DALCHIPINFO_ID_ESM7225 = 20,
  DALCHIPINFO_ID_MSM7225 = 21,
  DALCHIPINFO_ID_MSM7500 = 22,
  DALCHIPINFO_ID_MSM7500A = 23,
  DALCHIPINFO_ID_MSM7525 = 24,
  DALCHIPINFO_ID_MSM7600 = 25,
  DALCHIPINFO_ID_MSM7601 = 26,
  DALCHIPINFO_ID_MSM7625 = 27,
  DALCHIPINFO_ID_MSM7800 = 28,
  DALCHIPINFO_ID_MDM8200 = 29,
  DALCHIPINFO_ID_QSD8650 = 30,
  DALCHIPINFO_ID_MDM8900 = 31,
  DALCHIPINFO_ID_QST1000 = 32,
  DALCHIPINFO_ID_QST1005 = 33,
  DALCHIPINFO_ID_QST1100 = 34,
  DALCHIPINFO_ID_QST1105 = 35,
  DALCHIPINFO_ID_QST1500 = 40,
  DALCHIPINFO_ID_QST1600 = 41,
  DALCHIPINFO_ID_QST1700 = 42,
  DALCHIPINFO_ID_QSD8250 = 36,
  DALCHIPINFO_ID_QSD8550 = 37,
  DALCHIPINFO_ID_QSD8850 = 38,
  DALCHIPINFO_ID_MDM2000 = 39,
  DALCHIPINFO_ID_MSM7227 = 43,
  DALCHIPINFO_ID_MSM7627 = 44,
  DALCHIPINFO_ID_QSC6165 = 45,
  DALCHIPINFO_ID_QSC6175 = 46,
  DALCHIPINFO_ID_QSC6185 = 47,
  DALCHIPINFO_ID_QSC6195 = 48,
  DALCHIPINFO_ID_QSC6285 = 49,
  DALCHIPINFO_ID_QSC6295 = 50,
  DALCHIPINFO_ID_QSC6695 = 51,
  DALCHIPINFO_ID_ESM6246 = 52,
  DALCHIPINFO_ID_ESM6290 = 53,
  DALCHIPINFO_ID_ESC6270 = 54,
  DALCHIPINFO_ID_ESC6240 = 55,
  DALCHIPINFO_ID_MDM8220 = 56,
  DALCHIPINFO_ID_MDM9200 = 57,
  DALCHIPINFO_ID_MDM9600 = 58,
  DALCHIPINFO_ID_MSM7630 = 59,
  DALCHIPINFO_ID_MSM7230 = 60,
  DALCHIPINFO_ID_ESM7227 = 61,
  DALCHIPINFO_ID_MSM7625D1 = 62,
  DALCHIPINFO_ID_MSM7225D1 = 63,
  DALCHIPINFO_ID_QSD8250A = 64,
  DALCHIPINFO_ID_QSD8650A = 65,
  DALCHIPINFO_ID_MSM7625D2 = 66,
  DALCHIPINFO_ID_MSM7227D1 = 67,
  DALCHIPINFO_ID_MSM7627D1 = 68,
  DALCHIPINFO_ID_MSM7627D2 = 69,
  DALCHIPINFO_ID_MSM8260 = 70,
  DALCHIPINFO_ID_MSM8660 = 71,
  DALCHIPINFO_ID_MDM8200A = 72,
  DALCHIPINFO_ID_QSC6155 = 73,
  DALCHIPINFO_ID_MSM8255 = 74,
  DALCHIPINFO_ID_MSM8655 = 75,
  DALCHIPINFO_ID_ESC6295 = 76,
  DALCHIPINFO_ID_MDM3000 = 77,
  DALCHIPINFO_ID_MDM6200 = 78,
  DALCHIPINFO_ID_MDM6600 = 79,
  DALCHIPINFO_ID_MDM6210 = 80,
  DALCHIPINFO_ID_MDM6610 = 81,
  DALCHIPINFO_ID_QSD8672 = 82,
  DALCHIPINFO_ID_MDM6215 = 83,
  DALCHIPINFO_ID_MDM6615 = 84,
  DALCHIPINFO_ID_APQ8055 = 85,
  DALCHIPINFO_ID_APQ8060 = 86,
  DALCHIPINFO_ID_MSM8960 = 87,
  DALCHIPINFO_ID_MSM7225A = 88,
  DALCHIPINFO_ID_MSM7625A = 89,
  DALCHIPINFO_ID_MSM7227A = 90,
  DALCHIPINFO_ID_MSM7627A = 91,
  DALCHIPINFO_ID_ESM7227A = 92,
  DALCHIPINFO_ID_QSC6195D2 = 93,
  DALCHIPINFO_ID_FSM9200 = 94,
  DALCHIPINFO_ID_FSM9800 = 95,
  DALCHIPINFO_ID_MSM7225AD1 = 96,
  DALCHIPINFO_ID_MSM7227AD1 = 97,
  DALCHIPINFO_ID_MSM7225AA = 98,
  DALCHIPINFO_ID_MSM7225AAD1 = 99,
  DALCHIPINFO_ID_MSM7625AA = 100,
  DALCHIPINFO_ID_MSM7227AA = 101,
  DALCHIPINFO_ID_MSM7227AAD1 = 102,
  DALCHIPINFO_ID_MSM7627AA = 103,
  DALCHIPINFO_ID_MDM9615 = 104,
  DALCHIPINFO_ID_MDM9615M = DALCHIPINFO_ID_MDM9615,
  DALCHIPINFO_ID_MDM8215 = 106,
  DALCHIPINFO_ID_MDM9215 = 107,
  DALCHIPINFO_ID_MDM9215M = DALCHIPINFO_ID_MDM9215,
  DALCHIPINFO_ID_APQ8064 = 109,
  DALCHIPINFO_ID_QSC6270D1 = 110,
  DALCHIPINFO_ID_QSC6240D1 = 111,
  DALCHIPINFO_ID_ESC6270D1 = 112,
  DALCHIPINFO_ID_ESC6240D1 = 113,
  DALCHIPINFO_ID_MDM6270 = 114,
  DALCHIPINFO_ID_MDM6270D1 = 115,
  DALCHIPINFO_ID_MSM8930 = 116,
  DALCHIPINFO_ID_MSM8630 = 117,
  DALCHIPINFO_ID_MSM8230 = 118,
  DALCHIPINFO_ID_APQ8030 = 119,
  DALCHIPINFO_ID_MSM8627 = 120,
  DALCHIPINFO_ID_MSM8227 = 121,
  DALCHIPINFO_ID_MSM8660A = 122,
  DALCHIPINFO_ID_MSM8260A = 123,
  DALCHIPINFO_ID_APQ8060A = 124,
  DALCHIPINFO_ID_MPQ8062 = 125,
  DALCHIPINFO_ID_MSM8974 = 126,
  DALCHIPINFO_ID_MSM8225 = 127,
  DALCHIPINFO_ID_MSM8225D1 = 128,
  DALCHIPINFO_ID_MSM8625 = 129,
  DALCHIPINFO_ID_MPQ8064 = 130,
  DALCHIPINFO_ID_MSM7225AB = 131,
  DALCHIPINFO_ID_MSM7225ABD1 = 132,
  DALCHIPINFO_ID_MSM7625AB = 133,
  DALCHIPINFO_ID_MDM9625 = 134,
  DALCHIPINFO_ID_MSM7125A = 135,
  DALCHIPINFO_ID_MSM7127A = 136,
  DALCHIPINFO_ID_MSM8125AB = 137,
  DALCHIPINFO_ID_MSM8960AB = 138,
  DALCHIPINFO_ID_APQ8060AB = 139,
  DALCHIPINFO_ID_MSM8260AB = 140,
  DALCHIPINFO_ID_MSM8660AB = 141,
  DALCHIPINFO_ID_MSM8930AA = 142,
  DALCHIPINFO_ID_MSM8630AA = 143,
  DALCHIPINFO_ID_MSM8230AA = 144,
  DALCHIPINFO_ID_MSM8626 = 145,
  DALCHIPINFO_ID_MPQ8092 = 146,
  DALCHIPINFO_ID_MSM8610 = 147,
  DALCHIPINFO_ID_MDM8225 = 148,
  DALCHIPINFO_ID_MDM9225 = 149,
  DALCHIPINFO_ID_MDM9225M = 150,
  DALCHIPINFO_ID_MDM8225M = 151,
  DALCHIPINFO_ID_MDM9625M = 152,
  DALCHIPINFO_ID_APQ8064_V2PRIME = 153,
  DALCHIPINFO_ID_MSM8930AB = 154,
  DALCHIPINFO_ID_MSM8630AB = 155,
  DALCHIPINFO_ID_MSM8230AB = 156,
  DALCHIPINFO_ID_APQ8030AB = 157,
  DALCHIPINFO_ID_MSM8226 = 158,
  DALCHIPINFO_ID_MSM8526 = 159,
  DALCHIPINFO_ID_APQ8030AA = 160,
  DALCHIPINFO_ID_MSM8110 = 161,
  DALCHIPINFO_ID_MSM8210 = 162,
  DALCHIPINFO_ID_MSM8810 = 163,
  DALCHIPINFO_ID_MSM8212 = 164,
  DALCHIPINFO_ID_MSM8612 = 165,
  DALCHIPINFO_ID_MSM8112 = 166,
  DALCHIPINFO_ID_MSM8125 = 167,
  DALCHIPINFO_ID_MSM8225Q = 168,
  DALCHIPINFO_ID_MSM8625Q = 169,
  DALCHIPINFO_ID_MSM8125Q = 170,
  DALCHIPINFO_ID_MDM9310 = 171,
  DALCHIPINFO_ID_APQ8064_SLOW_PRIME = 172,
  DALCHIPINFO_ID_MDM8110M = 173,
  DALCHIPINFO_ID_MDM8615M = 174,
  DALCHIPINFO_ID_MDM9320 = 175,
  DALCHIPINFO_ID_MDM9225_1 = 176,
  DALCHIPINFO_ID_MDM9225M_1 = 177,
  DALCHIPINFO_ID_APQ8084 = 178,
  DALCHIPINFO_ID_MSM8130 = 179,
  DALCHIPINFO_ID_MSM8130AA = 180,
  DALCHIPINFO_ID_MSM8130AB = 181,
  DALCHIPINFO_ID_MSM8627AA = 182,
  DALCHIPINFO_ID_MSM8227AA = 183,
  DALCHIPINFO_ID_APQ8074 = 184,
  DALCHIPINFO_ID_MSM8274 = 185,
  DALCHIPINFO_ID_MSM8674 = 186,
  DALCHIPINFO_ID_MDM9635 = 187,
  DALCHIPINFO_ID_FSM9900 = 188,
  DALCHIPINFO_ID_FSM9965 = 189,
  DALCHIPINFO_ID_FSM9955 = 190,
  DALCHIPINFO_ID_FSM9950 = 191,
  DALCHIPINFO_ID_FSM9915 = 192,
  DALCHIPINFO_ID_FSM9910 = 193,
  DALCHIPINFO_ID_MSM8974_PRO = 194,
  DALCHIPINFO_ID_MSM8962 = 195,
  DALCHIPINFO_ID_MSM8262 = 196,
  DALCHIPINFO_ID_APQ8062 = 197,
  DALCHIPINFO_ID_MSM8126 = 198,
  DALCHIPINFO_ID_APQ8026 = 199,
  DALCHIPINFO_ID_MSM8926 = 200,
  DALCHIPINFO_ID_MSM8326 = 205,
  DALCHIPINFO_ID_MSM8916 = 206,
  DALCHIPINFO_ID_MSM8994 = 207,
  DALCHIPINFO_ID_APQ8074_AA = 208,
  DALCHIPINFO_ID_APQ8074_AB = 209,
  DALCHIPINFO_ID_APQ8074_PRO = 210,
  DALCHIPINFO_ID_MSM8274_AA = 211,
  DALCHIPINFO_ID_MSM8274_AB = 212,
  DALCHIPINFO_ID_MSM8274_PRO = 213,
  DALCHIPINFO_ID_MSM8674_AA = 214,
  DALCHIPINFO_ID_MSM8674_AB = 215,
  DALCHIPINFO_ID_MSM8674_PRO = 216,
  DALCHIPINFO_ID_MSM8974_AA = 217,
  DALCHIPINFO_ID_MSM8974_AB = 218,
  DALCHIPINFO_ID_APQ8028 = 219,
  DALCHIPINFO_ID_MSM8128 = 220,
  DALCHIPINFO_ID_MSM8228 = 221,
  DALCHIPINFO_ID_MSM8528 = 222,
  DALCHIPINFO_ID_MSM8628 = 223,
  DALCHIPINFO_ID_MSM8928 = 224,
  DALCHIPINFO_ID_MSM8510 = 225,
  DALCHIPINFO_ID_MSM8512 = 226,
  DALCHIPINFO_ID_MDM9630 = 227,
  DALCHIPINFO_ID_MDM9635M = DALCHIPINFO_ID_MDM9635,
  DALCHIPINFO_ID_MDM9230 = 228,
  DALCHIPINFO_ID_MDM9235M = 229,
  DALCHIPINFO_ID_MDM8630 = 230,
  DALCHIPINFO_ID_MDM9330 = 231,
  DALCHIPINFO_ID_MPQ8091 = 232,
  DALCHIPINFO_ID_MSM8936 = 233,
  DALCHIPINFO_ID_MDM9240 = 234,
  DALCHIPINFO_ID_MDM9340 = 235,
  DALCHIPINFO_ID_MDM9640 = 236,
  DALCHIPINFO_ID_MDM9245M = 237,
  DALCHIPINFO_ID_MDM9645M = 238,
  DALCHIPINFO_ID_MSM8939 = 239,
  DALCHIPINFO_ID_APQ8036 = 240,
  DALCHIPINFO_ID_APQ8039 = 241,
  DALCHIPINFO_ID_MSM8236 = 242,
  DALCHIPINFO_ID_MSM8636 = 243,
  DALCHIPINFO_ID_APQ8064_AU = 244,
  DALCHIPINFO_ID_MSM8909 = 245,
  DALCHIPINFO_ID_MSM8996 = 246,
  DALCHIPINFO_ID_APQ8016 = 247,
  DALCHIPINFO_ID_MSM8216 = 248,
  DALCHIPINFO_ID_MSM8116 = 249,
  DALCHIPINFO_ID_MSM8616 = 250,
  DALCHIPINFO_ID_MSM8992 = 251,
  DALCHIPINFO_ID_APQ8092 = 252,
  DALCHIPINFO_ID_APQ8094 = 253,
  DALCHIPINFO_ID_FSM9008 = 254,
  DALCHIPINFO_ID_FSM9010 = 255,
  DALCHIPINFO_ID_FSM9016 = 256,
  DALCHIPINFO_ID_FSM9055 = 257,
  DALCHIPINFO_ID_MSM8209 = 258,
  DALCHIPINFO_ID_MSM8208 = 259,
  DALCHIPINFO_ID_MDM9209 = 260,
  DALCHIPINFO_ID_MDM9309 = 261,
  DALCHIPINFO_ID_MDM9609 = 262,
  DALCHIPINFO_ID_MSM8239 = 263,
  DALCHIPINFO_ID_MSM8952 = 264,
  DALCHIPINFO_ID_APQ8009 = 265,
  DALCHIPINFO_ID_MSM8956 = 266,
  DALCHIPINFO_ID_QDF2432 = 267,
  DALCHIPINFO_ID_MSM8929 = 268,
  DALCHIPINFO_ID_MSM8629 = 269,
  DALCHIPINFO_ID_MSM8229 = 270,
  DALCHIPINFO_ID_APQ8029 = 271,
  DALCHIPINFO_ID_QCA9618 = 272,
  DALCHIPINFO_ID_IPQ4018 = DALCHIPINFO_ID_QCA9618,
  DALCHIPINFO_ID_QCA9619 = 273,
  DALCHIPINFO_ID_IPQ4019 = DALCHIPINFO_ID_QCA9619,
  DALCHIPINFO_ID_APQ8056 = 274,
  DALCHIPINFO_ID_MSM8609 = 275,
  DALCHIPINFO_ID_FSM9916 = 276,
  DALCHIPINFO_ID_APQ8076 = 277,
  DALCHIPINFO_ID_MSM8976 = 278,
  DALCHIPINFO_ID_MDM9650 = 279,
  DALCHIPINFO_ID_IPQ8065 = 280,
  DALCHIPINFO_ID_IPQ8069 = 281,
  DALCHIPINFO_ID_MSM8939_BC = 282,
  DALCHIPINFO_ID_MDM9250 = 283,
  DALCHIPINFO_ID_MDM9255 = 284,
  DALCHIPINFO_ID_MDM9350 = 285,
  DALCHIPINFO_ID_MDM9655 = 286,
  DALCHIPINFO_ID_IPQ4028 = 287,
  DALCHIPINFO_ID_IPQ4029 = 288,
  DALCHIPINFO_ID_APQ8052 = 289,
  DALCHIPINFO_ID_MDM9607 = 290,
  DALCHIPINFO_ID_APQ8096 = 291,
  DALCHIPINFO_ID_MSM8998 = 292,
  DALCHIPINFO_ID_MSM8953 = 293,
  DALCHIPINFO_ID_MSM8937 = 294,
  DALCHIPINFO_ID_APQ8037 = 295,
  DALCHIPINFO_ID_MDM8207 = 296,
  DALCHIPINFO_ID_MDM9207 = 297,
  DALCHIPINFO_ID_MDM9307 = 298,
  DALCHIPINFO_ID_MDM9628 = 299,
  DALCHIPINFO_ID_MSM8909W = 300,
  DALCHIPINFO_ID_APQ8009W = 301,
  DALCHIPINFO_ID_MSM8996L = 302,
  DALCHIPINFO_ID_MSM8917 = 303,
  DALCHIPINFO_ID_APQ8053 = 304,
  DALCHIPINFO_ID_MSM8996SG = 305,
  DALCHIPINFO_ID_MSM8997 = 306,
  DALCHIPINFO_ID_APQ8017 = 307,
  DALCHIPINFO_ID_MSM8217 = 308,
  DALCHIPINFO_ID_MSM8617 = 309,
  DALCHIPINFO_ID_MSM8996AU = 310,
  DALCHIPINFO_ID_APQ8096AU = 311,
  DALCHIPINFO_ID_APQ8096SG = 312,
  DALCHIPINFO_ID_MSM8940 = 313,
  DALCHIPINFO_ID_MDM9665 = 314,
  DALCHIPINFO_ID_MSM8996SGAU = 315,
  DALCHIPINFO_ID_APQ8096SGAU = 316,
  DALCHIPINFO_NUM_IDS = 317,
  DALCHIPINFO_ID_32BITS = 0x7FFFFFF
} DalChipInfoIdType;
typedef enum
{
  DALCHIPINFO_FAMILY_UNKNOWN = 0,
  DALCHIPINFO_FAMILY_MSM6246 = 1,
  DALCHIPINFO_FAMILY_MSM6260 = 2,
  DALCHIPINFO_FAMILY_QSC6270 = 3,
  DALCHIPINFO_FAMILY_MSM6280 = 4,
  DALCHIPINFO_FAMILY_MSM6290 = 5,
  DALCHIPINFO_FAMILY_MSM7200 = 6,
  DALCHIPINFO_FAMILY_MSM7500 = 7,
  DALCHIPINFO_FAMILY_MSM7600 = 8,
  DALCHIPINFO_FAMILY_MSM7625 = 9,
  DALCHIPINFO_FAMILY_MSM7X30 = 10,
  DALCHIPINFO_FAMILY_MSM7800 = 11,
  DALCHIPINFO_FAMILY_MDM8200 = 12,
  DALCHIPINFO_FAMILY_QSD8650 = 13,
  DALCHIPINFO_FAMILY_MSM7627 = 14,
  DALCHIPINFO_FAMILY_QSC6695 = 15,
  DALCHIPINFO_FAMILY_MDM9X00 = 16,
  DALCHIPINFO_FAMILY_QSD8650A = 17,
  DALCHIPINFO_FAMILY_MSM8X60 = 18,
  DALCHIPINFO_FAMILY_MDM8200A = 19,
  DALCHIPINFO_FAMILY_QSD8672 = 20,
  DALCHIPINFO_FAMILY_MDM6615 = 21,
  DALCHIPINFO_FAMILY_MSM8660 = DALCHIPINFO_FAMILY_MSM8X60,
  DALCHIPINFO_FAMILY_MSM8960 = 22,
  DALCHIPINFO_FAMILY_MSM7625A = 23,
  DALCHIPINFO_FAMILY_MSM7627A = 24,
  DALCHIPINFO_FAMILY_MDM9X15 = 25,
  DALCHIPINFO_FAMILY_MSM8930 = 26,
  DALCHIPINFO_FAMILY_MSM8630 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8230 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_APQ8030 = DALCHIPINFO_FAMILY_MSM8930,
  DALCHIPINFO_FAMILY_MSM8627 = 30,
  DALCHIPINFO_FAMILY_MSM8227 = DALCHIPINFO_FAMILY_MSM8627,
  DALCHIPINFO_FAMILY_MSM8974 = 32,
  DALCHIPINFO_FAMILY_MSM8625 = 33,
  DALCHIPINFO_FAMILY_MSM8225 = DALCHIPINFO_FAMILY_MSM8625,
  DALCHIPINFO_FAMILY_APQ8064 = 34,
  DALCHIPINFO_FAMILY_MDM9x25 = 35,
  DALCHIPINFO_FAMILY_MSM8960AB = 36,
  DALCHIPINFO_FAMILY_MSM8930AB = 37,
  DALCHIPINFO_FAMILY_MSM8x10 = 38,
  DALCHIPINFO_FAMILY_MPQ8092 = 39,
  DALCHIPINFO_FAMILY_MSM8x26 = 40,
  DALCHIPINFO_FAMILY_MSM8225Q = 41,
  DALCHIPINFO_FAMILY_MSM8625Q = 42,
  DALCHIPINFO_FAMILY_APQ8x94 = 43,
  DALCHIPINFO_FAMILY_APQ8084 = DALCHIPINFO_FAMILY_APQ8x94,
  DALCHIPINFO_FAMILY_MSM8x32 = 44,
  DALCHIPINFO_FAMILY_MDM9x35 = 45,
  DALCHIPINFO_FAMILY_MSM8974_PRO= 46,
  DALCHIPINFO_FAMILY_FSM9900 = 47,
  DALCHIPINFO_FAMILY_MSM8x62 = 48,
  DALCHIPINFO_FAMILY_MSM8926 = 49,
  DALCHIPINFO_FAMILY_MSM8994 = 50,
  DALCHIPINFO_FAMILY_IPQ8064 = 51,
  DALCHIPINFO_FAMILY_MSM8916 = 52,
  DALCHIPINFO_FAMILY_MSM8936 = 53,
  DALCHIPINFO_FAMILY_MDM9x45 = 54,
  DALCHIPINFO_FAMILY_MSM8996 = 56,
  DALCHIPINFO_FAMILY_APQ8096 = DALCHIPINFO_FAMILY_MSM8996,
  DALCHIPINFO_FAMILY_MSM8992 = 57,
  DALCHIPINFO_FAMILY_MSM8909 = 58,
  DALCHIPINFO_FAMILY_FSM90xx = 59,
  DALCHIPINFO_FAMILY_MSM8952 = 60,
  DALCHIPINFO_FAMILY_QDF2432 = 61,
  DALCHIPINFO_FAMILY_MSM8929 = 62,
  DALCHIPINFO_FAMILY_MSM8956 = 63,
  DALCHIPINFO_FAMILY_QCA961x = 64,
  DALCHIPINFO_FAMILY_IPQ40xx = DALCHIPINFO_FAMILY_QCA961x,
  DALCHIPINFO_FAMILY_MDM9x55 = 65,
  DALCHIPINFO_FAMILY_MDM9x07 = 66,
  DALCHIPINFO_FAMILY_MSM8998 = 67,
  DALCHIPINFO_FAMILY_MSM8953 = 68,
  DALCHIPINFO_FAMILY_MSM8993 = 69,
  DALCHIPINFO_FAMILY_MSM8937 = 70,
  DALCHIPINFO_FAMILY_MSM8917 = 71,
  DALCHIPINFO_FAMILY_MSM8996SG = 72,
  DALCHIPINFO_FAMILY_MSM8997 = 73,
  DALCHIPINFO_NUM_FAMILIES = 74,
  DALCHIPINFO_FAMILY_32BITS = 0x7FFFFFF
} DalChipInfoFamilyType;
typedef struct DalChipInfo DalChipInfo;
struct DalChipInfo
{
   DalDevice DalChipInfoDevice;
   DALResult (*GetChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoVersionType * pnVersion);
   DALResult (*GetRawChipVersion)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnVersion);
   DALResult (*GetChipId)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoIdType * peId);
   DALResult (*GetRawChipId)(DalDeviceHandle * _h, uint32 nNotUsed, uint32 * pnId);
   DALResult (*GetChipIdString)(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength);
   DALResult (*GetChipFamily)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoFamilyType * peFamily);
   DALResult (*GetModemSupport)(DalDeviceHandle * _h, uint32 nNotUsed, DalChipInfoModemType * pnModem);
};
typedef struct DalChipInfoHandle DalChipInfoHandle;
struct DalChipInfoHandle
{
   uint32 dwDalHandleId;
   const DalChipInfo * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalChipInfo_GetChipVersion(DalDeviceHandle * _h, DalChipInfoVersionType * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipVersion( _h, 0, pnVersion);
}
static __inline DALResult
DalChipInfo_GetRawChipVersion(DalDeviceHandle * _h, uint32 * pnVersion)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipVersion)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnVersion);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipVersion( _h, 0, pnVersion);
}
static __inline DALResult
DalChipInfo_GetChipId(DalDeviceHandle * _h, DalChipInfoIdType * peId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipId( _h, 0, peId);
}
static __inline DALResult
DalChipInfo_GetRawChipId(DalDeviceHandle * _h, uint32 * pnId)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetRawChipId)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnId);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetRawChipId( _h, 0, pnId);
}
static __inline DALResult
DalChipInfo_GetChipIdString(DalDeviceHandle * _h, char * szIdString, uint32 nMaxLength)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_9(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipIdString)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (char * )szIdString, nMaxLength);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipIdString( _h, szIdString, nMaxLength);
}
static __inline DALResult
DalChipInfo_GetChipFamily(DalDeviceHandle * _h, DalChipInfoFamilyType * peFamily)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetChipFamily)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)peFamily);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetChipFamily( _h, 0, peFamily);
}
static __inline DALResult
DalChipInfo_GetModemSupport(DalDeviceHandle * _h, DalChipInfoModemType * pnModem)
{
   if((((DALHandle)_h) & 0x00000001))
{
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalChipInfoHandle *)_h)->pVtbl)->GetModemSupport)-(uint32 *)(((DalChipInfoHandle *)_h)->pVtbl)), _h, (uint32 )0, (uint32 *)pnModem);
   }
   return ((DalChipInfoHandle *)_h)->pVtbl->GetModemSupport( _h, 0, pnModem);
}
static __inline DalChipInfoVersionType
DalChipInfo_ChipVersion(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoVersionType nVersion;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return (DalChipInfoVersionType)0;
    }
  }
  DalChipInfo_GetChipVersion(phChipInfo, &nVersion);
  return nVersion;
}
static __inline DalChipInfoIdType
DalChipInfo_ChipId(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoIdType neId;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return DALCHIPINFO_ID_UNKNOWN;
    }
  }
  DalChipInfo_GetChipId(phChipInfo, &neId);
  return neId;
}
static __inline DalChipInfoFamilyType
DalChipInfo_ChipFamily(void)
{
  static DalDeviceHandle *phChipInfo = 0;
  DALResult eResult;
  DalChipInfoFamilyType neFamily;
  if (phChipInfo == 0)
  {
    eResult =
      DAL_DeviceAttachEx(0,0x0200006F,(((1&0xFFFF)<<16)|(0&0xFFFF)),&phChipInfo);
    if (eResult != 0)
    {
      return DALCHIPINFO_FAMILY_UNKNOWN;
    }
  }
  DalChipInfo_GetChipFamily(phChipInfo, &neFamily);
  return neFamily;
}
typedef unsigned long int bool32;
typedef enum
{
  HAL_LDO_Q6,
  HAL_LDO_NUM_OF_LDOS,
  HAL_LDO_LDO_FORCE32BITS = 0x7FFFFFFF
} HAL_ldo_LDOType;
typedef struct
{
  uint32 nPhysAddr;
  uint32 nSize;
  uint32 *pnBaseAddr;
} HAL_ldo_HWIOBaseType;
typedef void *HAL_ldo_LDOHandleType;
typedef struct
{
  void (*BusyWait) (uint32 uSeconds);
  uint32 nChipVersion;
  uint32 nChipId;
  uint32 nChipFamily;
} HAL_ldo_ContextType;
void HAL_ldo_Init
(
  HAL_ldo_ContextType *pHALldoCtxt
);
void HAL_ldo_GetHWIOBases
(
  HAL_ldo_HWIOBaseType **paHWIOBases
);
void HAL_ldo_ConfigLDO
(
  HAL_ldo_LDOType eLDO
);
void HAL_ldo_EnableLDO
(
  HAL_ldo_LDOType eLDO
);
void HAL_ldo_DisableLDO
(
  HAL_ldo_LDOType eLDO
);
void HAL_ldo_SetLDOVoltage
(
  HAL_ldo_LDOType eLDO,
  uint32 nVoltageUV
);
void HAL_ldo_SetLDORetentionVoltage
(
  HAL_ldo_LDOType eLDO,
  uint32 nVoltageUV
);
uint32 HAL_ldo_GetChipVersion
(
  void
);
uint32 HAL_ldo_GetChipId
(
  void
);
uint32 HAL_ldo_GetChipFamily
(
  void
);
typedef struct
{
  uint32 nLogSize;
} VCSLogType;
typedef const void *VCSPropertyValueType;
typedef struct
{
  struct
  {
    uint8 nMajor;
    uint8 nMinor;
  } Min;
  struct
  {
    uint8 nMajor;
    uint8 nMinor;
  } Max;
  DalChipInfoFamilyType eChipInfoFamily;
  const DalChipInfoIdType *aeChipInfoId;
} VCSHWVersionType;
typedef struct
{
  VCSCornerType eCorner;
  uint32 nVoltageUV;
} VCSCornerVoltageType;
typedef struct
{
  VCSCornerType eCorner;
  uint32 nMinUV;
  uint32 nMaxUV;
} VCSCornerVoltageRangeType;
typedef struct
{
  VCSCornerType eCornerMin;
  VCSCornerType eCornerMax;
  VCSCornerType eCornerInit;
  VCSCornerVoltageRangeType *pVoltageRange;
  uint32 nNumVoltageRanges;
  VCSHWVersionType HWVersion;
} VCSRailCornerConfigType;
typedef struct
{
  HAL_ldo_LDOType eLDO;
  const char *szName;
  uint32 nHeadroom;
  VCSCornerVoltageType *pCornerVoltage;
  uint32 nNumCornerVoltages;
  boolean bEnable;
} VCSLDOConfigType;
typedef struct
{
  ClockCPUType eCPU;
  const char *szName;
  VCSRailType eRail;
  VCSLDOConfigType *pLDOConfig;
} VCSCPUConfigType;
typedef struct
{
  VCSRailType eRail;
  const char *szName;
  VCSRailCornerConfigType *pCornerConfig;
  uint32 nNumCornerConfigs;
  const char *szNameDependency;
  boolean bEnableCPR;
  boolean bEnableDVS;
} VCSRailConfigType;
typedef struct
{
  VCSRailConfigType *pRailConfig;
  uint32 nNumRailConfigs;
  VCSCPUConfigType *pCPUConfig;
  uint32 nNumCPUConfigs;
  uint32 *pnCornerPMICMap;
} VCSBSPConfigType;
enum
{
   PMIC_NPA_MODE_ID_ANT_MODE_0 = 0,
   PMIC_NPA_MODE_ID_ANT_MODE_1 = 1,
   PMIC_NPA_MODE_ID_ANT_MODE_2 = 2,
   PMIC_NPA_MODE_ID_ANT_MODE_3 = 3,
};
enum
{
   PMIC_NPA_MODE_ID_AUDIO_STANDBY = 0,
   PMIC_NPA_MODE_ID_AUDIO_ACTIVE_LOW = 1,
   PMIC_NPA_MODE_ID_AUDIO_ACTIVE_MED = 2,
   PMIC_NPA_MODE_ID_AUDIO_ACTIVE_HIGH = 3,
   PMIC_NPA_MODE_ID_AUDIO_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_BT_STANDBY = 0,
   PMIC_NPA_MODE_ID_BT_ACT_SOC = 1,
   PMIC_NPA_MODE_ID_BT_ACT_ALL = 2,
   PMIC_NPA_MODE_ID_BT_MAX = 3,
};
enum
{
   PMIC_NPA_MODE_ID_CLK_REGIME_DIG_LOW_DIG = 0,
   PMIC_NPA_MODE_ID_CLK_REGIME_DIG_NORMAL_DIG = 1,
   PMIC_NPA_MODE_ID_CLK_REGIME_DIG_HIGH_DIG = 2,
   PMIC_NPA_MODE_ID_CLK_REGIME_VDD_DIG_MIN = 3,
   PMIC_NPA_MODE_ID_CLK_REGIME_DIG_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_0 = 0,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_1 = 1,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_2 = 2,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_3 = 3,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_4 = 4,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_5 = 5,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_6 = 6,
   PMIC_NPA_MODE_ID_CLK_REGIME_MODE_7 = 7,
};
enum
{
   PMIC_NPA_MODE_ID_CODEC_PATH_OFF = 0,
   PMIC_NPA_MODE_ID_CODEC_1PATH_ON = 1,
   PMIC_NPA_MODE_ID_CODEC_2PATH_ON = 2,
   PMIC_NPA_MODE_ID_CODEC_3PATH_ON = 3,
   PMIC_NPA_MODE_ID_CODEC_4PATH_ON = 4,
   PMIC_NPA_MODE_ID_CODEC_MAX = 5,
};
enum
{
   PMIC_NPA_MODE_ID_CORNER_LEVEL_NO_VOTE = 0,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_1 = 1,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_2 = 2,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_3 = 3,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_4 = 4,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_5 = 5,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_6 = 6,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_7 = 7,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_8 = 8,
   PMIC_NPA_MODE_ID_CORNER_LEVEL_MAX = 9,
};
enum
{
   PMIC_NPA_GROUP_ID_eMMC_OFF = 0,
   PMIC_NPA_GROUP_ID_eMMC_ACTIVE = 1,
   PMIC_NPA_GROUP_ID_eMMC_SLEEP = 2,
   PMIC_NPA_GROUP_ID_eMMC_MAX = 3,
};
enum
{
   PMIC_NPA_MODE_ID_GENERIC_STANDBY = 0,
   PMIC_NPA_MODE_ID_GENERIC_ACTIVE = 1,
   PMIC_NPA_MODE_ID_GENERIC_MAX = 2,
};
enum
{
   PMIC_NPA_MODE_ID_OFF = 0,
   PMIC_NPA_MODE_ID_STANDBY = 1,
   PMIC_NPA_MODE_ID_ACTIVE_NOM_LOW = 2,
   PMIC_NPA_MODE_ID_ACTIVE_SS_LOW = 3,
   PMIC_NPA_MODE_ID_ACTIVE_NOM_HIGH = 4,
   PMIC_NPA_MODE_ID_ACTIVE_SS_HIGH = 5,
   PMIC_NPA_MODE_ID_NOM_HIGH_RX = 6,
   PMIC_NPA_MODE_ID_MAX = 7,
};
enum
{
   PMIC_NPA_MODE_ID_GPS_MODE_0 = 0,
   PMIC_NPA_MODE_ID_GPS_MODE_1 = 1,
   PMIC_NPA_MODE_ID_GPS_MODE_2 = 2,
   PMIC_NPA_MODE_ID_GPS_MODE_3 = 3,
   PMIC_NPA_MODE_ID_GPS_MODE_4 = 4,
   PMIC_NPA_MODE_ID_GPS_MODE_5 = 5,
   PMIC_NPA_MODE_ID_GPS_MODE_6 = 6,
   PMIC_NPA_MODE_ID_GPS_MODE_7 = 7,
   PMIC_NPA_MODE_ID_GPS_MODE_8 = 8,
   PMIC_NPA_MODE_ID_GPS_MODE_MAX = 9,
};
enum
{
   PMIC_NPA_MODE_ID_HKADC_OFF = 0,
   PMIC_NPA_MODE_ID_HKADC_ACTIVE = 1,
   PMIC_NPA_MODE_ID_HKADC_MAX = 2,
};
enum
{
   PMIC_NPA_MODE_ID_MODE_0 = 0,
   PMIC_NPA_MODE_ID_MODE_1 = 1,
   PMIC_NPA_MODE_ID_MODE_2 = 2,
   PMIC_NPA_MODE_ID_MODE_3 = 3,
   PMIC_NPA_MODE_ID_MODE_4 = 4,
   PMIC_NPA_MODE_ID_MODE_5 = 5,
   PMIC_NPA_MODE_ID_MODE_6 = 6,
   PMIC_NPA_MODE_ID_MODE_MAX = 7,
};
enum
{
   PMIC_NPA_MODE_ID_INVALID = 0,
};
enum
{
   PMIC_NPA_MODE_ID_MCPM_LOW_DIG = 0,
   PMIC_NPA_MODE_ID_MCPM_NORMAL_DIG = 1,
   PMIC_NPA_MODE_ID_MCPM_HIGH_DIG = 2,
   PMIC_NPA_MODE_ID_MCPM_DIG_MIN = 3,
   PMIC_NPA_MODE_ID_MCPM_DIG_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_MCPM_MODE_0 = 0,
   PMIC_NPA_MODE_ID_MCPM_MODE_1 = 1,
   PMIC_NPA_MODE_ID_MCPM_MODE_2 = 2,
   PMIC_NPA_MODE_ID_MCPM_MODE_3 = 3,
   PMIC_NPA_MODE_ID_MCPM_MODE_4 = 4,
   PMIC_NPA_MODE_ID_MCPM_MODE_5 = 5,
   PMIC_NPA_MODE_ID_MCPM_MODE_6 = 6,
   PMIC_NPA_MODE_ID_MCPM_MODE_7 = 7,
   PMIC_NPA_MODE_ID_MCPM_MODE_8 = 8,
   PMIC_NPA_MODE_ID_MCPM_MODE_MAX = 9,
};
enum
{
   PMIC_NPA_MODE_ID_MEM_DEEP_STANDBY = 0,
   PMIC_NPA_MODE_ID_MEM_SELF_REFRESH = 1,
   PMIC_NPA_MODE_ID_MEM_ACTIVE = 2,
   PMIC_NPA_MODE_ID_MEM_MAX = 3,
};
enum
{
   PMIC_NPA_MODE_ID_PA_MODE_0 = 0,
   PMIC_NPA_MODE_ID_PA_MODE_1 = 1,
   PMIC_NPA_MODE_ID_PA_MODE_2 = 2,
   PMIC_NPA_MODE_ID_PA_MODE_3 = 3,
};
enum
{
   PMIC_NPA_MODE_ID_PLL_MODE_0 = 0,
   PMIC_NPA_MODE_ID_PLL_MODE_1 = 1,
   PMIC_NPA_MODE_ID_PLL_MODE_2 = 2,
   PMIC_NPA_MODE_ID_PLL_MODE_3 = 3,
   PMIC_NPA_MODE_ID_PLL_MODE_4 = 4,
   PMIC_NPA_MODE_ID_PLL_MODE_5 = 5,
   PMIC_NPA_MODE_ID_PLL_MODE_MAX = 6,
};
enum
{
   PMIC_NPA_MODE_ID_CORE_RAIL_OFF = 0,
   PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION = 1,
   PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS = 2,
   PMIC_NPA_MODE_ID_CORE_RAIL_LOW = 3,
   PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL = 4,
   PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL_PLUS = 5,
   PMIC_NPA_MODE_ID_CORE_RAIL_TURBO = 6,
   PMIC_NPA_MODE_ID_CORE_RAIL_MAX = 7,
};
enum
{
   PMIC_NPA_MODE_ID_RF_SLEEP = 0,
   PMIC_NPA_MODE_ID_RF_UMTS_NOMINAL = 1,
   PMIC_NPA_MODE_ID_RF_AUTOCAL = 1,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_C2K_RX = 2,
   PMIC_NPA_MODE_ID_RF_1X_NOMINAL = 2,
   PMIC_NPA_MODE_ID_RF_UMTS_SS = 3,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_C2K_RX = 3,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_C2K_RXTX = 4,
   PMIC_NPA_MODE_ID_RF_1X_SS = 4,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_C2K_RXTX = 5,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_UMTS_RX = 6,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_UMTS_RX = 7,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_UMTS_RXTX = 8,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_UMTS_RXTX = 9,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_LTE_EVDO_RX = 10,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_LTE_EVDO_RX = 11,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_LTE_EVDO_RXTX = 12,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_LTE_EVDO_RXTX = 13,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_SV_LTE_EVDO_RX = 14,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_SV_LTE_EVDO_RX = 15,
   PMIC_NPA_MODE_ID_RF_DVS_HIGH_SV_LTE_EVDO_RXTX = 16,
   PMIC_NPA_MODE_ID_RF_DVS_LOW_SV_LTE_EVDO_RXTX = 17,
   PMIC_NPA_MODE_ID_RF_MAX = 18,
};
enum
{
   PMIC_NPA_MODE_ID_RF_GPS_EXIT = 0,
   PMIC_NPA_MODE_ID_RF_GPS_SLEEP = 1,
   PMIC_NPA_MODE_ID_RF_GPS_ON_NOMINAL = 2,
   PMIC_NPA_MODE_ID_RF_GPS_ON_SS = 3,
   PMIC_NPA_MODE_ID_RF_GPS_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_RF_MODE_0 = 0,
   PMIC_NPA_MODE_ID_RF_MODE_1 = 1,
   PMIC_NPA_MODE_ID_RF_MODE_2 = 2,
   PMIC_NPA_MODE_ID_RF_MODE_3 = 3,
   PMIC_NPA_MODE_ID_RF_MODE_4 = 4,
   PMIC_NPA_MODE_ID_RF_MODE_5 = 5,
   PMIC_NPA_MODE_ID_RF_MODE_6 = 6,
   PMIC_NPA_MODE_ID_RF_MODE_7 = 7,
   PMIC_NPA_MODE_ID_RF_MODE_8 = 8,
   PMIC_NPA_MODE_ID_RF_MODE_9 = 9,
   PMIC_NPA_MODE_ID_RF_MODE_10 = 10,
   PMIC_NPA_MODE_ID_RF_MODE_11 = 11,
   PMIC_NPA_MODE_ID_RF_MODE_12 = 12,
   PMIC_NPA_MODE_ID_RF_MODE_13 = 13,
   PMIC_NPA_MODE_ID_RF_MODE_14 = 14,
   PMIC_NPA_MODE_ID_RF_MODE_15 = 15,
   PMIC_NPA_MODE_ID_RF_MODE_16 = 16,
   PMIC_NPA_MODE_ID_RF_MODE_17 = 17,
   PMIC_NPA_MODE_ID_RF_MODE_18 = 18,
   PMIC_NPA_MODE_ID_RF_MODE_19 = 19,
   PMIC_NPA_MODE_ID_RF_MODE_20 = 20,
   PMIC_NPA_MODE_ID_RF_MODE_21 = 21,
   PMIC_NPA_MODE_ID_RF_MODE_22 = 22,
   PMIC_NPA_MODE_ID_RF_MODE_23 = 23,
   PMIC_NPA_MODE_ID_RF_MODE_24 = 24,
   PMIC_NPA_MODE_ID_RF_MODE_25 = 25,
   PMIC_NPA_MODE_ID_RF_MODE_26 = 26,
   PMIC_NPA_MODE_ID_RF_MODE_27 = 27,
   PMIC_NPA_MODE_ID_RF_MODE_28 = 28,
   PMIC_NPA_MODE_ID_RF_MODE_29 = 29,
   PMIC_NPA_MODE_ID_RF_MODE_30 = 30,
   PMIC_NPA_MODE_ID_RF_MODE_31 = 31,
   PMIC_NPA_MODE_ID_RF_MODE_32 = 32,
   PMIC_NPA_MODE_ID_RF_MODE_33 = 33,
   PMIC_NPA_MODE_ID_RF_MODE_34 = 34,
   PMIC_NPA_MODE_ID_RF_MODE_35 = 35,
   PMIC_NPA_MODE_ID_RF_MODE_36 = 36,
   PMIC_NPA_MODE_ID_RF_MODE_37 = 37,
   PMIC_NPA_MODE_ID_RF_MODE_38 = 38,
   PMIC_NPA_MODE_ID_RF_MODE_39 = 39,
   PMIC_NPA_MODE_ID_RF_MODE_MAX = 40,
};
enum
{
   PMIC_NPA_MODE_ID_RF1_GPS_EXIT = 0,
   PMIC_NPA_MODE_ID_RF1_GPS_SLEEP = 1,
   PMIC_NPA_MODE_ID_RF1_GPS_DVS_HIGH_RX = 2,
   PMIC_NPA_MODE_ID_RF1_GPS_DVS_LOW_RX = 3,
   PMIC_NPA_MODE_ID_RF1_GPS_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_RPM_INIT_MODE_0 = 0,
   PMIC_NPA_MODE_ID_RPM_INIT_MODE_1 = 1,
   PMIC_NPA_MODE_ID_RPM_INIT_MODE_MAX = 2,
};
enum
{
   PMIC_NPA_GROUP_ID_SD_OFF = 0,
   PMIC_NPA_GROUP_ID_SD_ACTIVE = 1,
   PMIC_NPA_GROUP_ID_SD_MAX = 2,
};
enum
{
   PMIC_NPA_MODE_ID_SD_MODE_OFF = 0,
   PMIC_NPA_MODE_ID_SD_MODE_1 = 1,
   PMIC_NPA_MODE_ID_SD_MODE_2 = 2,
   PMIC_NPA_MODE_ID_SD_MODE_MAX = 3,
};
enum
{
   PMIC_NPA_MODE_ID_SENSOR_POWER_OFF = 0,
   PMIC_NPA_MODE_ID_SENSOR_LPM = 1,
   PMIC_NPA_MODE_ID_SENSOR_POWER_ON = 2,
   PMIC_NPA_MODE_ID_SENSOR_MAX = 3,
};
enum
{
   PMIC_NPA_MODE_ID_TOUCH_SCREEN_MODE_0 = 0,
   PMIC_NPA_MODE_ID_TOUCH_SCREEN_MODE_1 = 1,
   PMIC_NPA_MODE_ID_TOUCH_SCREEN_MODE_2 = 2,
   PMIC_NPA_MODE_ID_TOUCH_SCREEN_MODE_3 = 3,
   PMIC_NPA_MODE_ID_TOUCH_SCREEN_MODE_MAX = 4,
};
enum
{
   PMIC_NPA_MODE_ID_UIM_STANDBY = 0,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C_LOW = 1,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C = 2,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C_HIGH = 3,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B_LOW = 4,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B = 5,
   PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B_HIGH = 6,
   PMIC_NPA_MODE_ID_UIM_MAX = 7,
};
enum
{
   PMIC_NPA_MODE_ID_UIM_MODE_1 = 0,
   PMIC_NPA_MODE_ID_UIM_MODE_2 = 1,
   PMIC_NPA_MODE_ID_UIM_MODE_3 = 2,
   PMIC_NPA_MODE_ID_UIM_MODE_4 = 3,
   PMIC_NPA_MODE_ID_UIM_MODE_5 = 4,
   PMIC_NPA_MODE_ID_UIM_MODE_6 = 5,
   PMIC_NPA_MODE_ID_UIM_MODE_7 = 6,
   PMIC_NPA_MODE_ID_UIM_MODE_8 = 7,
   PMIC_NPA_MODE_ID_UIM_MODE_9 = 8,
   PMIC_NPA_MODE_ID_UIM_MODE_10 = 9,
   PMIC_NPA_MODE_ID_UIM_MODE_11 = 10,
   PMIC_NPA_MODE_ID_UIM_MODE_12 = 11,
   PMIC_NPA_MODE_ID_UIM_MODE_13 = 12,
   PMIC_NPA_MODE_ID_UIM_MODE_MAX = 13,
};
enum
{
   PMIC_NPA_MODE_ID_USB_CHG_DETACHED = 0,
   PMIC_NPA_MODE_ID_USB_CHG_MAX = 1,
};
enum
{
   PMIC_NPA_MODE_ID_USB_FS_POWER_OFF = 0,
   PMIC_NPA_MODE_ID_USB_FS_PERI_LPM_0 = 1,
   PMIC_NPA_MODE_ID_USB_FS_PERI_LPM_1 = 2,
   PMIC_NPA_MODE_ID_USB_FS_PERI_LPM_2 = 3,
   PMIC_NPA_MODE_ID_USB_FS_PERI_LPM_3 = 4,
   PMIC_NPA_MODE_ID_USB_FS_HOST_LPM_0 = 5,
   PMIC_NPA_MODE_ID_USB_FS_HOST_LPM_1 = 6,
   PMIC_NPA_MODE_ID_USB_FS_HOST_LPM_2 = 7,
   PMIC_NPA_MODE_ID_USB_FS_MAX = 8,
};
enum
{
   PMIC_NPA_MODE_ID_USB_HS_POWER_OFF = 0,
   PMIC_NPA_MODE_ID_USB_HS_PERI_LPM_0 = 1,
   PMIC_NPA_MODE_ID_USB_HS_PERI_LPM_1 = 2,
   PMIC_NPA_MODE_ID_USB_HS_PERI_LPM_2 = 3,
   PMIC_NPA_MODE_ID_USB_HS_PERI_LPM_3 = 4,
   PMIC_NPA_MODE_ID_USB_HS_HOST_LPM_0 = 5,
   PMIC_NPA_MODE_ID_USB_HS_HOST_LPM_1 = 6,
   PMIC_NPA_MODE_ID_USB_HS_HOST_LPM_2 = 7,
   PMIC_NPA_MODE_ID_USB_HS_HOST_LPM_3 = 8,
   PMIC_NPA_MODE_ID_USB_HS_MAX = 9,
};
enum
{
   PMIC_NPA_MODE_ID_USB_OFF = 0,
   PMIC_NPA_MODE_ID_USB_PERPH_SUSPEND = 1,
   PMIC_NPA_MODE_ID_USB_PERPH_ACTIVE = 2,
   PMIC_NPA_MODE_ID_USB_HOST_SUSPEND = 3,
   PMIC_NPA_MODE_ID_USB_HOST_ACTIVE = 4,
   PMIC_NPA_MODE_ID_USB_MAX = 5,
};
enum
{
   PMIC_NPA_MODE_ID_WCN_MODE_0 = 0,
   PMIC_NPA_MODE_ID_WCN_MODE_1 = 1,
   PMIC_NPA_MODE_ID_WCN_MODE_2 = 2,
   PMIC_NPA_MODE_ID_WCN_MODE_3 = 3,
   PMIC_NPA_MODE_ID_WCN_MODE_4 = 4,
   PMIC_NPA_MODE_ID_WCN_MODE_5 = 5,
   PMIC_NPA_MODE_ID_WCN_MODE_MAX = 6,
};
enum
{
   PMIC_NPA_MODE_ID_XO_SHUTDOWN_MODE_0 = 0,
   PMIC_NPA_MODE_ID_XO_SHUTDOWN_MODE_1 = 1,
   PMIC_NPA_MODE_ID_XO_SHUTDOWN_MODE_MAX = 2,
};
static VCSRailCornerConfigType VCS_CornerConfigCX[] =
{
  {
    .eCornerMin = VCS_CORNER_OFF,
    .eCornerMax = VCS_CORNER_MAX,
    .eCornerInit = VCS_CORNER_NOMINAL,
    .pVoltageRange = 0,
    .nNumVoltageRanges = 0,
    .HWVersion = { {0x0, 0x0}, {0xFF, 0xFF} },
  }
};
static VCSRailCornerConfigType VCS_CornerConfigMX[] =
{
  {
    .eCornerMin = VCS_CORNER_OFF,
    .eCornerMax = VCS_CORNER_MAX,
    .eCornerInit = VCS_CORNER_NOMINAL,
    .pVoltageRange = 0,
    .nNumVoltageRanges = 0,
    .HWVersion = { {0x0, 0x0}, {0xFF, 0xFF} },
  }
};
static VCSRailConfigType VCS_RailConfigs[] =
{
  {
    .eRail = VCS_RAIL_CX,
    .szName = "/vdd/cx",
    .pCornerConfig = VCS_CornerConfigCX,
    .nNumCornerConfigs = ( sizeof( (VCS_CornerConfigCX) ) / sizeof( (VCS_CornerConfigCX[0]) ) ),
    .szNameDependency = "/pmic/client/rail_cx",
    .bEnableCPR = 0,
    .bEnableDVS = 1
  },
  {
    .eRail = VCS_RAIL_MX,
    .szName = "/vdd/mx",
    .pCornerConfig = VCS_CornerConfigMX,
    .nNumCornerConfigs = ( sizeof( (VCS_CornerConfigMX) ) / sizeof( (VCS_CornerConfigMX[0]) ) ),
    .szNameDependency = "/pmic/client/rail_mx",
    .bEnableCPR = 0,
    .bEnableDVS = 1
  }
};
static VCSCornerVoltageType VCS_LDOCornerVoltageLPASSQ6[] =
{
  {
    .eCorner = VCS_CORNER_LOW_MINUS,
    .nVoltageUV = 775000
  },
};
static VCSLDOConfigType VCS_LDOConfigLPASSQ6 =
{
  .eLDO = HAL_LDO_Q6,
  .szName = "/vdd/ldo/lpass_q6",
  .nHeadroom = 1,
  .pCornerVoltage = VCS_LDOCornerVoltageLPASSQ6,
  .nNumCornerVoltages = ( sizeof( (VCS_LDOCornerVoltageLPASSQ6) ) / sizeof( (VCS_LDOCornerVoltageLPASSQ6[0]) ) ),
  .bEnable = 0
};
static VCSCPUConfigType VCS_CPUConfigs[] =
{
  {
    .eCPU = CLOCK_CPU_LPASS_Q6,
    .szName = "/clk/cpu",
    .eRail = VCS_RAIL_CX,
    .pLDOConfig = &VCS_LDOConfigLPASSQ6,
  },
};
uint32 VCS_CornerPMICMap[VCS_CORNER_NUM_OF_CORNERS] =
{
  PMIC_NPA_MODE_ID_CORE_RAIL_OFF,
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION,
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS,
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW,
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO,
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO
};
const VCSLogType VCS_LogDefaultConfig[] =
{
  {
                      4096,
  }
};
const VCSBSPConfigType VCS_BSPConfig =
{
  .pRailConfig = VCS_RailConfigs,
  .nNumRailConfigs = ( sizeof( (VCS_RailConfigs) ) / sizeof( (VCS_RailConfigs[0]) ) ),
  .pCPUConfig = VCS_CPUConfigs,
  .nNumCPUConfigs = ( sizeof( (VCS_CPUConfigs) ) / sizeof( (VCS_CPUConfigs[0]) ) ),
  .pnCornerPMICMap = VCS_CornerPMICMap
};
