#ifndef VCSLPASS_H
#define VCSLPASS_H
/*
===========================================================================
*/
/**
  @file VCSLPASS.h 
  
  Internal header file for the VCS device driver on the LPASS image.
*/
/*  
  ====================================================================

  Copyright (c) 2011-14 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.adsp/2.7/systemdrivers/vcs/hw/msm8996/lpass/src/VCSLPASS.h#1 $
  $DateTime: 2014/12/05 08:38:22 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.

  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "DDIVCS.h"


/*=========================================================================
      Macro Definitions
==========================================================================*/


/*
 * NPA client names
 */
#define VCS_NPA_CLIENT_NAME_VDD_LPASS_DISABLE_SCALING "/vdd/lpass/disable_scaling"


/*=========================================================================
      Type Definitions
==========================================================================*/

/*=========================================================================
      Functions
==========================================================================*/


#endif /* !VCSLPASS_H */

