/*
===========================================================================
*/
/**
  @file VCSLPASS.c 
  
  Main entry point for the LPASS VCS driver.
*/
/*  
  ====================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.adsp/2.7/systemdrivers/vcs/hw/msm8996/lpass/src/VCSLPASS.c#1 $
  $DateTime: 2014/12/05 08:38:22 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "VCSDriver.h"
//#include "VCSLPASS.h"
//#include "pmapp_pwr.h"
#include "pmapp_npa.h"
//#include <CoreIni.h>
//#include "cpr.h"

/*=========================================================================
      Macros
==========================================================================*/


/*
 * VCS configuration names in EFS .ini file
 * Shared with ClockDriver
 */
#define VCS_EFS_INI_FILENAME                   "/nv/item_files/clock/settings.ini"

/*
 * EFS Sections for LPASS VCS.
 */
#define VCS_EFS_LPASS_RAIL_CX_CONFIG_SECTION    "LPASS_VDDCX"

/*
 * EFS Keys for LPASS VCS.
 */
#define VCS_EFS_RAIL_DVS_FLAG                  "EnableDVS"
#define VCS_EFS_RAIL_CPR_FLAG                  "EnableCPR"
#define VCS_EFS_RAIL_MIN_CORNER                "MinCorner"
#define VCS_EFS_RAIL_MAX_CORNER                "MaxCorner"

#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MIN     "RetentionMin"
#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MAX     "RetentionMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MIN     "LowMinusMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MAX     "LowMinusMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_MIN           "LowMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_MAX           "LowMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MIN       "NominalMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MAX       "NominalMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MIN  "NominalPlusMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MAX  "NominalPlusMax"

#define VCS_EFS_RAIL_VOLTAGE_TURBO_MIN         "TurboMin"
#define VCS_EFS_RAIL_VOLTAGE_TURBO_MAX         "TurboMax"


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Extern Definitions
==========================================================================*/


//extern void npa_update_resource_state(npa_resource *resource, npa_resource_state new_state);


/*=========================================================================
      Function prototypes
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/


//VCSImageCtxtType VCS_ImageCtxt;


/* =========================================================================
      Prototypes
==========================================================================*/


/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : VCS_DetectLDOEFuse
** =========================================================================*/
/**
  Read LDO Trimmed Info

  This function will read an efuse field to detect whether the LDO has
  been trimmed and is safe to enable the LDO output.

  @return
  None.

  @dependencies
  None.
*/
#if 0
static void VCS_DetectLDOEFuse
(
  VCSLDONodeType *pLDO
)
{

} /* END VCS_DetectLDOEFuse */
#endif


/* =========================================================================
**  Function : VCS_SetRailMode
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailMode
(
  VCSDrvCtxt     *pDrvCtxt,
  VCSRailType     eRail,
  VCSRailModeType eMode
)
{
  /*
   * VDD_MSS is not supported on LPASS.
   */
  return DAL_ERROR_NOT_SUPPORTED;

} /* END VCS_SetRailMode */


/* =========================================================================
**  Function : VCS_SetRailVoltage
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailVoltage
(
  VCSDrvCtxt  *pDrvCtxt,
  VCSRailType  eRail,
  uint32       nVoltageUV
)
{
  /*
   * VDD_MSS is not supported on LPASS.
   */
  return DAL_ERROR_NOT_SUPPORTED;

} /* END VCS_SetRailMode */



/* =========================================================================
**  Function : VCS_LoadNV_CX
** =========================================================================*/
/**
  Load EFS data for CX rail.

  @param pDrvCtxt [in] -- Pointer to the VCS driver context.
  @param hConfig [in] -- Handle to core config.

  @return
  DAL_SUCCESS -- Successfully parsed EFS data for CX.
  DAL_ERROR -- Failed to parse EFS data for CX.

  @dependencies
  None.
*/
#if 0
static DALResult VCS_LoadNV_CX
(
  VCSDrvCtxt       *pDrvCtxt,
  CoreConfigHandle  hConfig
)
{
  VCSRailNodeType         *pRail;
  uint32                   nReadResult, nData;
  VCSNPARailEventDataType  RailEventData;

  /*-----------------------------------------------------------------------*/
  /* Get rail nodes.                                                       */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_CX];
  if (pRail == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Invalid CX rail structure");
  }

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: EFS data update.                              */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Update max corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_LPASS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MAX_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMax = MIN(nData, VCS_CORNER_MAX);
  }

  /*-----------------------------------------------------------------------*/
  /* Update min corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_LPASS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MIN_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMin = MIN(nData, pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Update DVS.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_LPASS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_DVS_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableDVS |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableDVS = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: EFS data update.                                */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Notify clients of the new max limit for this rail.                    */
  /*-----------------------------------------------------------------------*/

  RailEventData.PreChange.eCorner  = VCS_CORNER_OFF;
  RailEventData.PostChange.eCorner = pRail->eCornerMax;

  npa_dispatch_custom_events(
    pRail->resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_LIMIT_MAX,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: limit max update.                             */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Verify votes reduced within supported range.                          */
  /*-----------------------------------------------------------------------*/

  if (pRail->resource.handle->request_state > pRail->eCornerMax)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: request[%l] on rail[%s] above max[%l]",
      pRail->resource.handle->request_state,
      pRail->pBSPConfig->szName,
      pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Trigger impulse to update the rail active state.                      */
  /* NOTE: We update the resoure state to 0 in order for the impulse       */
  /*       request to make it's way into the driver function.              */
  /*       This workaround will be replaced by a new NPA API.              */
  /*-----------------------------------------------------------------------*/

  npa_update_resource_state(pRail->resource.handle, 0);
  npa_issue_impulse_request(pRail->hClientImpulse);

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: limit max update.                               */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END of VCS_LoadNV_CX */
#endif


/* =========================================================================
**  Function : VCS_LoadNV
** =========================================================================*/
/*
  See VCSMSS.h
*/

void VCS_LoadNV
(
  void
)
{
#if 0
  VCSDrvCtxt       *pDrvCtxt;
  CoreConfigHandle  hConfig;
  DALResult         eResult;

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Read clock configuration file.                                        */
  /*-----------------------------------------------------------------------*/

  hConfig = CoreIni_ConfigCreate(VCS_EFS_INI_FILENAME);
  if (hConfig == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_INFO,
      "DALLOG Device VCS: Unable to read EFS file: %s",
      VCS_EFS_INI_FILENAME);

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Load the EFS data for CX.                                             */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_LoadNV_CX(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: unable to load EFS data for rail CX");

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Destroy the handle.                                                   */
  /*-----------------------------------------------------------------------*/

  CoreIni_ConfigDestroy(hConfig);
#endif
} /* END VCS_LoadNV */


/* =========================================================================
**  Function : VCS_InitQ6LDO
** =========================================================================*/
/**
  Initialize the Q6 LDO

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_SUCCESS -- Initialization was successful.
  DAL_ERROR_INTERNAL -- Q6 CPU node was not found.

  @dependencies
  None.
*/
#if 0

static DALResult VCS_InitQ6LDO
(
  VCSDrvCtxt *pDrvCtxt
)
{
  VCSCPUNodeType *pCPU;
  VCSLDONodeType *pLDO;

  /*-----------------------------------------------------------------------*/
  /* Get the Q6 CPU node.                                                  */
  /*-----------------------------------------------------------------------*/

  pCPU = pDrvCtxt->apCPUMap[CLOCK_CPU_MSS_Q6];
  if (pCPU == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to get CPU node.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the LDO node.                                                     */
  /*-----------------------------------------------------------------------*/

  pLDO = pCPU->pLDO;
  if (pLDO == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to get LDO node.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Read the EFuse to determine whether LDO output should be allowed.     */
  /*-----------------------------------------------------------------------*/

  VCS_DetectLDOEFuse(pLDO);

  /*-----------------------------------------------------------------------*/
  /* Ensure we're in BHS mode before setting up the LDO config.            */
  /*-----------------------------------------------------------------------*/

  HAL_ldo_DisableLDO(pLDO->eLDO);

  /*-----------------------------------------------------------------------*/
  /* Program the LDO_CFG registers per recommendation from HW.             */
  /*-----------------------------------------------------------------------*/

  HAL_ldo_ConfigLDO(pLDO->eLDO);

  /*-----------------------------------------------------------------------*/
  /* Program the retention voltage.                                        */
  /*-----------------------------------------------------------------------*/

  HAL_ldo_SetLDORetentionVoltage(pLDO->eLDO, pLDO->pBSPConfig->nRetVoltageUV);

  return DAL_SUCCESS;

}
#endif


/* =========================================================================
**  Function : VCS_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCS_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
#if 0
  DALResult            eResult;
  VCSPropertyValueType PropVal;

  /*-----------------------------------------------------------------------*/
  /* Assign the image context.                                             */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt->pImageCtxt = &VCS_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get the image BSP/XML.                                                */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_GetPropertyValue("VCSImageBSPConfig", &PropVal);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: VCS_GetPropertyValue failed.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Store the pointer to the image BSP in the image context.              */
  /*-----------------------------------------------------------------------*/

  VCS_ImageCtxt.pBSPConfig = (VCSImageBSPConfigType *)PropVal;
#endif
  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6 LDO.                                                */
  /*-----------------------------------------------------------------------*/

#if 0
  eResult = VCS_InitQ6LDO(pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to init Q6 LDO.");

    return eResult;
  }
#endif

  /*-----------------------------------------------------------------------*/
  /* Done.                                                                 */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCS_InitImage */


/* =========================================================================
**  Function : VCSStub_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCSStub_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCSStub_InitImage */

