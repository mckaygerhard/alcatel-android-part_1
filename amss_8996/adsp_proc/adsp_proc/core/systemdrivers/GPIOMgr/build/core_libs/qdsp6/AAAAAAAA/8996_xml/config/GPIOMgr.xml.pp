# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgr.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgr.xml" 2
<driver name="GPIOMgr">
  <device id="GPIOManager">
    <props name="GPIOMGR_NUM_GPIOS" type=DALPROP_ATTR_TYPE_UINT32>
      150
    </props>
    <props name="GPIOMGR_DIRCONN_CONFIG_MAP" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      GPIOMgrConfigMap
    </props>
    <props name="GPIOMGR_PD_CONFIG_MAP" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      GPIOMgrConfigPD
    </props>
    <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32>
      0x1
    </props>
  </device>
</driver>
