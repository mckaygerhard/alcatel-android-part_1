#===============================================================================
#                    Copyright 2009 - 2014 Qualcomm Technologies Incorporated.
#                           All Rights Reserved.
#                      Qualcomm Confidential and Proprietary
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/pkg/mpss/dev/aratin.aratin/modem_proc/core/dal/drivers/InterruptController/build/SConscript#1 $
#  $DateTime: 2011/08/03 19:36:07 $
#  $Author: coresvc $
#  $Change: 1866457 $
#
#===============================================================================
# DAL InterruptController Lib
#-------------------------------------------------------------------------------
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/systemdrivers/InterruptController"
SRCPATHSCRIPTS = env['BUILD_ROOT']+'/core/systemdrivers/InterruptController/scripts'
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

INTC_IMAGES = ['CORE_ADSP_ROOT', 'CORE_ADSP_USER']
INTC_IMAGES_ROOT = ['CORE_ADSP_ROOT']

#-------------------------------------------------------------------------------
# Source Code
#-------------------------------------------------------------------------------

env.PublishPrivateApi('DAL_INTERRUPTCONTROLLER', [
   "${INC_ROOT}/core/api/kernel/libstd/stringl",
   "${INC_ROOT}/core/api/kernel/qurt",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src",
   "${INC_ROOT}/core/systemdrivers/InterruptController/inc",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src/qurt/kernel",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src/qurt/kernel/uimage",
])

env.Replace(SRC_DIR='qurt')
env.Append(CPPDEFINES = ["DALINTERRUPT_LOG"])
env.Append(CPPDEFINES = ["INTERRUPT_LOG_ENTRIES=0"])
#env.Append(CPPDEFINES = ["DALINTERRUPT_MPM_WAKEUP"])
env.Append(CPPPATH = [
   "${INC_ROOT}/core/api/kernel/qurt",
 ])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DEBUGTOOLS',
   'MPROC',
   'POWER',
   'DEBUGTRACE',
   # needs to be last also contains wrong comdef.h      
   'KERNEL',
]

CBSP_RESTRICTED_API = [
   'DAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DEBUGTOOLS',
   'MPROC',
   'POWER',
   # needs to be last also contains wrong comdef.h      
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_RESTRICTED_API)


#-------------------------------------------------------------------------------
# Source Code
#-------------------------------------------------------------------------------
DAL_INTERRUPT_CONTROLLER_SOURCES = [
   '${BUILDPATH}/src/DALInterruptControllerInfo.c',
   '${BUILDPATH}/src/DALInterruptControllerFwk.c',
   '${BUILDPATH}/src/qurt/kernel/DALInterruptController.c',
   '${BUILDPATH}/src/utils/DALInterruptController_utils.c'
]

INTERRUPT_UIMAGE = [
  '${BUILDPATH}/src/qurt/kernel/uimage/uInterruptControllerStub.c'
]

if 'USES_SENSOR_IMG' in env or 'USES_AUDIO_IMG' in env:
  DAL_INTERRUPT_CONTROLLER_SOURCES.extend(['${BUILDPATH}/config/8996/kernel/InterruptConfigData.c'])

try:
  if env['IMAGE_NAME'] in ['ADSP_PROC']:
    env.AddCMMScripts ('ADSP', [SRCPATHSCRIPTS], { 'InterruptController.cmm' : ' Interrupt Controller', 'InterruptLog.cmm' : ' Interrupt Log' }, 'DAL')
except:
  pass
#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddLibrary(INTC_IMAGES, '${BUILDPATH}/DALInterruptController', DAL_INTERRUPT_CONTROLLER_SOURCES)
env.AddLibrary(INTC_IMAGES, '${BUILDPATH}/uInterrupt', INTERRUPT_UIMAGE)
   
if 'USES_DEVCFG' in env:
   if (('USES_AUDIO_PD' in env) or ('USES_AUDIO_IMG' in env)):
     DEVCFG_IMG_AUDIO = ['DEVCFG_CORE_QDSP6_AUDIO_SW']
     env.AddDevCfgInfo(DEVCFG_IMG_AUDIO, 
     {
        '8994_xml' : ['${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8994/user/InterruptController.xml'],
        '8996_xml' : ['${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/kernel/InterruptController.xml',
                      '${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c'],
     })
   else:
     DEVCFG_IMG = ['DAL_DEVCFG_IMG']
     env.AddDevCfgInfo(DEVCFG_IMG, 
     {
        '8994_xml' : ['${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8994/kernel/InterruptController.xml',
                      '${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8994/kernel/InterruptConfigData.c'],    
        '8996_xml' : ['${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/kernel/InterruptController.xml',
                      '${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c'],                     
     })
    

