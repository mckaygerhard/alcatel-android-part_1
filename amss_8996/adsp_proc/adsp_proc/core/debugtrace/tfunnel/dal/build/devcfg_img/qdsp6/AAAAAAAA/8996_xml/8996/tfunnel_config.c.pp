# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/tfunnel_config.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/tfunnel_config.c" 2
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/tfunnel_config.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 107 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;







typedef unsigned char byte;



typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
      typedef long long int64;



      typedef unsigned long long uint64;
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/tfunnel_config.c" 2





typedef struct TypeTFunnelPortEnableConfig TFunnelPortEnableConfig;




struct TypeTFunnelPortEnableConfig{
   uint32 funnel_id;
   uint32 port_id;
   TFunnelPortEnableConfig *pFunnelConfig;
};




static TFunnelPortEnableConfig tfunnel_merge_port0[]={
   {2,0,0}
};

static TFunnelPortEnableConfig tfunnel_merge_port1[]={
{2,1,0}
};



TFunnelPortEnableConfig tfunnel_port_rpm[]={
   {0,0,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_mpss[]={
   {0,1,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_adsp[]={
   {0,2,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_system_noc[]={
   {0,3,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_apps_etm[]={
   {0,4,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_mmss_noc[]={
   {0,5,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_peripheral_noc[]={
   {0,6,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_stm[]={
   {0,7,tfunnel_merge_port0}
};

TFunnelPortEnableConfig tfunnel_port_rpm_itm[]={
   {1,1,tfunnel_merge_port1}
};


TFunnelPortEnableConfig tfunnel_port_mmss[]={
   {1,2,tfunnel_merge_port1}
};

TFunnelPortEnableConfig tfunnel_port_pronto[]={
   {1,2,tfunnel_merge_port1}
};


TFunnelPortEnableConfig tfunnel_port_bimc[]={
   {1,3,tfunnel_merge_port1}
};

TFunnelPortEnableConfig tfunnel_port_modem[]={
   {1,4,tfunnel_merge_port1}
};

TFunnelPortEnableConfig tfunnel_port_ocmem_noc[]={
   {1,5,tfunnel_merge_port1}
};
