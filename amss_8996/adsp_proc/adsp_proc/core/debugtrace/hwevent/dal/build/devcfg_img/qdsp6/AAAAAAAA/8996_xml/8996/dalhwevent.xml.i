<driver name="HWEvent">
<device id="DALDEVICEID_HWEVENT">
    <props name="hwevent_config" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
     hwevent_config_table
    </props>
    <props name="hwevent_config_size" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
     table_size_array
    </props>
    <props name="hwevent_addr_check" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
     hwevent_addr_table
    </props>
    <props name="hwevent_addr_check_size" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
     addr_range_table_size
    </props>
</device>
</driver>
