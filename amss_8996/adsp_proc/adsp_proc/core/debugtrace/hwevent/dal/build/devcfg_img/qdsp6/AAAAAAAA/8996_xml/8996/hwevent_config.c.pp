# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/hwevent/config/8996/hwevent_config.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/hwevent/config/8996/hwevent_config.c" 2
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/hwevent/config/8996/hwevent_config.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 107 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;







typedef unsigned char byte;



typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
      typedef long long int64;



      typedef unsigned long long uint64;
# 29 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/hwevent/config/8996/hwevent_config.c" 2

typedef struct HWEventRegisterValue_t HWEventRegisterValue;
typedef struct HWEventAddrRange_t HWEventAddrRange;




struct HWEventRegisterValue_t{
   uint32 reg_phys_addr;
   uint32 reg_value;
};

struct HWEventAddrRange_t
{
   uint32 lower_range;
   uint32 higher_range;
   uint32 unlock_value;
};

typedef struct HWEventTableSize_t HWEventTableSize;
struct HWEventTableSize_t {
   uint32 size;
};


const HWEventRegisterValue hwevent_config_table[]={
{0,0}
};







const HWEventTableSize table_size_array[]={
   {sizeof(hwevent_config_table)/sizeof(HWEventRegisterValue)},
 };

const HWEventAddrRange hwevent_addr_table[]={
{0x930200c,0x930200c,0x0},
{0x9091000,0x909107c,0x0},
{0,0,0},
};




const HWEventTableSize addr_range_table_size[]={
   {sizeof(hwevent_addr_table)/sizeof(HWEventAddrRange)}
 };
