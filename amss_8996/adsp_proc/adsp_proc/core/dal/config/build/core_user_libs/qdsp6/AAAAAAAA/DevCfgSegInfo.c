#include "DALSysInt.h" 

#define DEVCFG_TCSR_SOC_HW_ADDR	0x007A8000


extern unsigned int __8996_devcfg_data_addr_base__;

DEVCFG_TARGET_INFO devcfg_target_soc_info[ ] =
{
	{ 12292, (DALProps *)&__8996_devcfg_data_addr_base__, NULL, 0},
	{ 12288, (DALProps *)&__8996_devcfg_data_addr_base__, NULL, 0},
	{0, NULL, NULL, 0}
};
