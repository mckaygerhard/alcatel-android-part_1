#include <customer.h> 
/**
	@note: This is being marked as write only because the DATA image
	should not be technically allowed.
*/ 

.section .8996_DEVCFG_DATA,"aw", @progbits
.align 8
.globl __8996_devcfg_data_addr_base__
.type  __8996_devcfg_data_addr_base__, @function

__8996_devcfg_data_addr_base__:
	.word 0
	.size __8996_devcfg_data_addr_base__, .-__8996_devcfg_data_addr_base__
	.space (DEVCFG_DATA_SEG_SIZE-0x4), 0

