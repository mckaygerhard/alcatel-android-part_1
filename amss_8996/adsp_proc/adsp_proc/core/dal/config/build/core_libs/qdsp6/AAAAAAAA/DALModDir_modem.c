#include "DALStdDef.h" 
#include "DALReg.h" 

#include"DALSysTypes.h"
extern DALREG_DriverInfo DALI2C_DriverInfo;
extern DALREG_DriverInfo DALAxiCfg_DriverInfo;
extern DALREG_DriverInfo DALUart_DriverInfo;
extern DALREG_DriverInfo DALUartLite_DriverInfo;
extern DALREG_DriverInfo DALSystem_DriverInfo;
extern DALREG_DriverInfo DALTimer_DriverInfo;
extern DALREG_DriverInfo DALSystemCall_DriverInfo;
extern DALREG_DriverInfo DALHWEvent_DriverInfo;
extern DALREG_DriverInfo DALSTMCfg_DriverInfo;
extern DALREG_DriverInfo DALSTMTrace_DriverInfo;
extern DALREG_DriverInfo DALTFunnel_DriverInfo;
extern DALREG_DriverInfo DALTMC_DriverInfo;
extern DALREG_DriverInfo DALmpm_DriverInfo;
extern DALREG_DriverInfo DALGPIOInt_DriverInfo;
extern DALREG_DriverInfo DALGPIOMgr_DriverInfo;
extern DALREG_DriverInfo DALInterruptController_DriverInfo;
extern DALREG_DriverInfo DALChipInfo_DriverInfo;
extern DALREG_DriverInfo DALClock_DriverInfo;
extern DALREG_DriverInfo DALHWIO_DriverInfo;
extern DALREG_DriverInfo DALIPCInt_DriverInfo;
extern DALREG_DriverInfo DALPlatformInfo_DriverInfo;
extern DALREG_DriverInfo DALTimetick_DriverInfo;
extern DALREG_DriverInfo DALTLMM_DriverInfo;
extern DALREG_DriverInfo DALVCS_DriverInfo;

static DALREG_DriverInfo * DALDriverInfoArr[] = {
	& DALI2C_DriverInfo,
	& DALAxiCfg_DriverInfo,
	& DALUart_DriverInfo,
	& DALUartLite_DriverInfo,
	& DALSystem_DriverInfo,
	& DALTimer_DriverInfo,
	& DALSystemCall_DriverInfo,
	& DALHWEvent_DriverInfo,
	& DALSTMCfg_DriverInfo,
	& DALSTMTrace_DriverInfo,
	& DALTFunnel_DriverInfo,
	& DALTMC_DriverInfo,
	& DALmpm_DriverInfo,
	& DALGPIOInt_DriverInfo,
	& DALGPIOMgr_DriverInfo,
	& DALInterruptController_DriverInfo,
	& DALChipInfo_DriverInfo,
	& DALClock_DriverInfo,
	& DALHWIO_DriverInfo,
	& DALIPCInt_DriverInfo,
	& DALPlatformInfo_DriverInfo,
	& DALTimetick_DriverInfo,
	& DALTLMM_DriverInfo,
	& DALVCS_DriverInfo,
};

DALREG_DriverInfoList gDALModDriverInfoList = {24, DALDriverInfoArr}; 



StringDevice DAL_Mod_driver_list[] = {
			{"/core/buses/icb/arb",2639912943u, 0, &DALAxiCfg_DriverInfo, 0, NULL },
			{"UartMainPort",3995683947u, 0, &DALUartLite_DriverInfo, 0, NULL },
			{"UartSecondPort",275744706u, 0, &DALUartLite_DriverInfo, 0, NULL },
			{"UartThirdPort",1664369473u, 0, &DALUartLite_DriverInfo, 0, NULL },
			{"DALDEVICEID_HWEVENT",8325331u, 0, &DALHWEvent_DriverInfo, 0, NULL },
			{"DALDEVICEID_TMC",1565009686u, 0, &DALTMC_DriverInfo, 0, NULL },
			{"/dev/mpm",3784643084u, 0, &DALmpm_DriverInfo, 0, NULL },
			{"GPIOManager",1556117711u, 0, &DALGPIOMgr_DriverInfo, 0, NULL },
			{"SystemTimer",3596230123u, 0, &DALTimetick_DriverInfo, 0, NULL },
			{"WakeUpTimer",1181484147u, 0, &DALTimetick_DriverInfo, 0, NULL },
			{"/tlmm/cdp001",3103503109u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"/tlmm/mtp001",3513831775u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"/tlmm/fluid1",3230640130u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"/tlmm/liquid",3461764261u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"/tlmm/fusion",3241241969u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"/tlmm/dragonboard",1489218944u, 0, &DALTLMM_DriverInfo, 0, NULL },
			{"VCS",193472945u, 0, &DALVCS_DriverInfo, 0, NULL }
};

DALProps DAL_Mod_Info = {NULL, 0 ,17, DAL_Mod_driver_list};
