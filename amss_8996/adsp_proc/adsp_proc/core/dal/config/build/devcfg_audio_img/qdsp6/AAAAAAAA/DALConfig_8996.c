#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "dalconfig.h"
#include "err_inject_crash.h"
#include "DDIGPIOInt.h"
#include "DalDevice.h"
#include "DALStdDef.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DALStdErr.h"
#include "DALReg.h"
#include "DDIInterruptController.h"
#include "DALInterruptControllerConfig.h"
#include "DALInterruptController.h"
#include "DALFramework.h"
#include "atomic_ops.h"
#include "qurt_atomic_ops.h"
#include "atomic_ops_plat.h"
#include "qurt_anysignal.h"
#include "qurt_signal.h"
#include "qurt_thread.h"
#include "DALInterruptController_utils.h"
#include "qurt.h"
#include "qurt_consts.h"
#include "qurt_alloc.h"
#include "qurt_futex.h"
#include "qurt_mutex.h"
#include "qurt_pipe.h"
#include "qurt_sem.h"
#include "qurt_printf.h"
#include "qurt_assert.h"
#include "qurt_trace.h"
#include "qurt_cycles.h"
#include "qurt_cond.h"
#include "qurt_rmutex2.h"
#include "qurt_barrier.h"
#include "qurt_fastint.h"
#include "qurt_allsignal.h"
#include "qurt_rmutex.h"
#include "qurt_pimutex.h"
#include "qurt_signal2.h"
#include "qurt_pimutex2.h"
#include "qurt_int.h"
#include "qurt_lifo.h"
#include "qurt_power.h"
#include "qurt_event.h"
#include "qurt_pmu.h"
#include "qurt_tlb.h"
#include "qurt_types.h"
#include "qurt_memory.h"
#include "qurt_error.h"
#include "qurt_qdi.h"
#include "qurt_qdi_constants.h"
#include "qurt_qdi_imacros.h"
#include "qurt_sclk.h"
#include "qurt_space.h"
#include "qurt_process.h"
#include "qurt_shmem.h"
#include "qurt_timer.h"
#include "qurt_tls.h"
#include "qurt_thread_context.h"
#include "qurt_hvx.h"
#include "qurt_mailbox.h"
#include "qurt_island.h"
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#ifndef DAL_CONFIG_IMAGE_MODEM 
#define DAL_CONFIG_IMAGE_MODEM 
#endif 
extern void * SlimBusBSP_8996_xml;
extern void * sbDeviceProps_8996_xml;
extern void * sbNumDeviceProps_8996_xml;
extern void * sbMmpmRegParam_8996_xml;
extern void * SlimBusBSP2_8996_xml;
extern void * sbDeviceProps2_8996_xml;
extern void * sbNumDeviceProps2_8996_xml;
extern void * sbMmpmRegParam2_8996_xml;
extern void * bam_tgt_config_8996_xml;
extern void * interrupt_config_map_8996_xml;
extern void * pInterruptControllerConfigData_8996_xml;
extern void * lpaif_prop_8996_xml;
extern void * audioif_dma_prop_8996_xml;
extern void * hdmiout_dma_prop_8996_xml;
extern void * genericclk_prop_8996_xml;
extern void * avtimer_prop_8996_xml;
extern void * resampler_prop_8996_xml;
extern void * vfr_prop_8996_xml;
extern void * afe_riva_prop_8996_xml;
extern void * afe_slimbus_prop_8996_xml;
extern void * midi_prop_8996_xml;
extern void * lpass_prop_8996_xml;
extern void * spdiftx_prop_8996_xml;
extern void * hdmi_output_prop_8996_xml;
extern void * lpm_prop_8996_xml;
extern void * lpa_prop_8996_xml;
extern void * lsm_mmpm_prop_8996_xml;
extern void * cvd_devcfg_clock_table_8996_xml;
extern void * cvd_devcfg_voice_use_case_na_values_8996_xml;
extern void * cvd_devcfg_mmpm_core_info_8996_xml;
extern void * cvd_devcfg_parser_bw_table_8996_xml;

const DALSYSPropStructTblType DALPROP_StructPtrs_8996_xml[32] =  {
	 {sizeof(void *), &SlimBusBSP_8996_xml},
	 {sizeof(void *), &sbDeviceProps_8996_xml},
	 {sizeof(void *), &sbNumDeviceProps_8996_xml},
	 {sizeof(void *), &sbMmpmRegParam_8996_xml},
	 {sizeof(void *), &SlimBusBSP2_8996_xml},
	 {sizeof(void *), &sbDeviceProps2_8996_xml},
	 {sizeof(void *), &sbNumDeviceProps2_8996_xml},
	 {sizeof(void *), &sbMmpmRegParam2_8996_xml},
	 {sizeof(void *), &bam_tgt_config_8996_xml},
	 {sizeof(void *), &interrupt_config_map_8996_xml},
	 {sizeof(void *), &pInterruptControllerConfigData_8996_xml},
	 {sizeof(void *), &lpaif_prop_8996_xml},
	 {sizeof(void *), &audioif_dma_prop_8996_xml},
	 {sizeof(void *), &hdmiout_dma_prop_8996_xml},
	 {sizeof(void *), &genericclk_prop_8996_xml},
	 {sizeof(void *), &avtimer_prop_8996_xml},
	 {sizeof(void *), &resampler_prop_8996_xml},
	 {sizeof(void *), &vfr_prop_8996_xml},
	 {sizeof(void *), &afe_riva_prop_8996_xml},
	 {sizeof(void *), &afe_slimbus_prop_8996_xml},
	 {sizeof(void *), &midi_prop_8996_xml},
	 {sizeof(void *), &lpass_prop_8996_xml},
	 {sizeof(void *), &spdiftx_prop_8996_xml},
	 {sizeof(void *), &hdmi_output_prop_8996_xml},
	 {sizeof(void *), &lpm_prop_8996_xml},
	 {sizeof(void *), &lpa_prop_8996_xml},
	 {sizeof(void *), &lsm_mmpm_prop_8996_xml},
	 {sizeof(void *), &cvd_devcfg_clock_table_8996_xml},
	 {sizeof(void *), &cvd_devcfg_voice_use_case_na_values_8996_xml},
	 {sizeof(void *), &cvd_devcfg_mmpm_core_info_8996_xml},
	 {sizeof(void *), &cvd_devcfg_parser_bw_table_8996_xml},
	{0, 0 } 
 };
const uint32 DALPROP_PropBin_8996_xml[] = {

			0x000011b4, 0x00000048, 0x00000530, 0x00000554, 0x00000558, 
			0x00000006, 0x0200013d, 0x00000558, 0x0200013e, 0x0000064c, 
			0x0200014e, 0x0000076c, 0x0200014d, 0x00000794, 0x020000ab, 
			0x000007c0, 0x02000004, 0x0000083c, 0x5f707362, 0x61746164, 
			0x5f736900, 0x7473616d, 0x64007265, 0x75616665, 0x635f746c, 
			0x6b636f6c, 0x6165675f, 0x65650072, 0x7373615f, 0x006e6769, 
			0x6f636573, 0x7261646e, 0x6c645f79, 0x7268745f, 0x00687365, 
			0x69766564, 0x705f6563, 0x73706f72, 0x6d756e00, 0x7665645f, 
			0x5f656369, 0x706f7270, 0x6d6d0073, 0x725f6d70, 0x705f6765, 
			0x6d617261, 0x6f727000, 0x61625f67, 0x72745f6d, 0x00747375, 
			0x6d6d6c74, 0x6d616e5f, 0x6c740065, 0x6f5f6d6d, 0x65736666, 
			0x6c740074, 0x765f6d6d, 0x73006c61, 0x6e5f7376, 0x75006170, 
			0x675f6573, 0x5f6f6970, 0x00746e69, 0x5f676f6c, 0x6576656c, 
			0x756e006c, 0x6f6c5f6d, 0x5f6c6163, 0x74726f70, 0x6f6c0073, 
			0x5f6c6163, 0x74726f70, 0x7361625f, 0x6f6c0065, 0x5f6c6163, 
			0x6e616863, 0x5f6c656e, 0x65736162, 0x61687300, 0x5f646572, 
			0x6e616863, 0x5f6c656e, 0x65736162, 0x6d756e00, 0x636f6c5f, 
			0x635f6c61, 0x746e756f, 0x00737265, 0x5f636965, 0x73617263, 
			0x6e655f68, 0x656c6261, 0x63696500, 0x6172635f, 0x745f6873, 
			0x00657079, 0x5f636965, 0x73617263, 0x65645f68, 0x0079616c, 
			0x67616d69, 0x64695f65, 0x6d747300, 0x5f70735f, 0x65736162, 
			0x6464615f, 0x74730072, 0x61625f6d, 0x705f6573, 0x0074726f, 
			0x5f6d7473, 0x5f6d756e, 0x74726f70, 0x74730073, 0x6c635f6d, 
			0x5f6d6961, 0x00676174, 0x5f6d7473, 0x73796870, 0x6464615f, 
			0x61620072, 0x67745f6d, 0x6f635f74, 0x6769666e, 0x49504700, 
			0x544e494f, 0x5241545f, 0x5f544547, 0x434f5250, 0x5245565f, 
			0x4e4f4953, 0x4d554e00, 0x5f524542, 0x445f464f, 0x43455249, 
			0x4f435f54, 0x43454e4e, 0x4e495f54, 0x52524554, 0x53545055, 
			0x49504700, 0x544e494f, 0x5948505f, 0x41434953, 0x44415f4c, 
			0x53455244, 0x52500053, 0x5345434f, 0x00524f53, 0x4d4d5553, 
			0x5f595241, 0x52544e49, 0x0044495f, 0x5f445541, 0x45444f43, 
			0x4e495f43, 0x50475f54, 0x4d5f4f49, 0x41005041, 0x455f4455, 
			0x565f5458, 0x495f5246, 0x475f544e, 0x5f4f4950, 0x5f50414d, 
			0x49440030, 0x54434552, 0x4e4f435f, 0x5443454e, 0x4e4f435f, 
			0x5f474946, 0x0050414d, 0x45544e49, 0x50555252, 0x4c505f54, 
			0x4f465441, 0x58004d52, 0x48535f4f, 0x4f445455, 0x525f4e57, 
			0x00435253, 0x45544e49, 0x50555252, 0x4f435f54, 0x4749464e, 
			0x5441445f, 0x49550041, 0x5245544e, 0x54505552, 0x4e4f435f, 
			0x4c4f5254, 0x0052454c, 0x454d4954, 0x41425f52, 0x54004553, 
			0x52454d49, 0x46464f5f, 0x00544553, 0x4d6d754e, 0x6c75646f, 
			0x4c007365, 0x66696170, 0x706f7250, 0x75727453, 0x74507463, 
			0x75410072, 0x496f6964, 0x616d4466, 0x706f7250, 0x75727453, 
			0x74507463, 0x64480072, 0x754f696d, 0x616d4474, 0x706f7250, 
			0x75727453, 0x74507463, 0x65470072, 0x6972656e, 0x6b6c6363, 
			0x706f7250, 0x75727453, 0x74507463, 0x76410072, 0x656d6974, 
			0x6f725072, 0x72745370, 0x50746375, 0x52007274, 0x6d617365, 
			0x72656c70, 0x706f7250, 0x75727453, 0x74507463, 0x66560072, 
			0x6f725072, 0x72745370, 0x50746375, 0x41007274, 0x69524546, 
			0x72506176, 0x7453706f, 0x74637572, 0x00727450, 0x53454641, 
			0x626d696c, 0x72507375, 0x7453706f, 0x74637572, 0x00727450, 
			0x6964694d, 0x706f7250, 0x75727453, 0x74507463, 0x704c0072, 
			0x50737361, 0x53706f72, 0x63757274, 0x72745074, 0x64705300, 
			0x78546669, 0x706f7250, 0x75727453, 0x74507463, 0x64480072, 
			0x754f696d, 0x74757074, 0x706f7250, 0x75727453, 0x74507463, 
			0x504c0072, 0x6f72504d, 0x72745370, 0x50746375, 0x4c007274, 
			0x72504150, 0x7453706f, 0x74637572, 0x00727450, 0x5f6d736c, 
			0x6d706d6d, 0x6f72705f, 0x74735f70, 0x74637572, 0x7274705f, 
			0x4f4c4300, 0x545f4b43, 0x454c4241, 0x45535500, 0x5341435f, 
			0x414e5f45, 0x4c41565f, 0x504d4d00, 0x4f435f4d, 0x495f4552, 
			0x004f464e, 0x4d504d4d, 0x5f57425f, 0x55514552, 0x4d455249, 
			0x00544e45, 0x4d504d4d, 0x454c535f, 0x4c5f5045, 0x4e455441, 
			0x43005943, 0x454c4359, 0x45505f53, 0x504b5f52, 0x4341465f, 
			0x00524f54, 0x4d504d4d, 0x4359435f, 0x5f53454c, 0x5f524550, 
			0x4b434150, 0x4e005445, 0x45424d55, 0x464f5f52, 0x5f57485f, 
			0x45524854, 0x00534441, 0x5f58414d, 0x435f3651, 0x5f45524f, 
			0x434f4c43, 0x484d5f4b, 0x756e005a, 0x65735f6d, 0x6e656d67, 
			0x68007374, 0x00687361, 0x4d4d4c54, 0x6d702f00, 0x632f6369, 
			0x6e65696c, 0x61722f74, 0x635f6c69, 0x782f0078, 0x78632f6f, 
			0x0000006f, 0x02010002, 0x00000012, 0x00000000, 0x00000000, 
			0x00000002, 0x00000009, 0x00000001, 0x00000002, 0x00000013, 
			0x00000008, 0x00000018, 0x00000026, 0x00000000, 0x00000002, 
			0x00000030, 0x00000020, 0x00000012, 0x00000044, 0x00000001, 
			0x00000012, 0x00000051, 0x00000002, 0x00000012, 0x00000062, 
			0x00000003, 0x00000002, 0x00000071, 0x00000000, 0x00000011, 
			0x00000080, 0x00000000, 0x00000002, 0x0000008a, 0x00144000, 
			0x00000002, 0x00000096, 0x00000002, 0x00000011, 0x0000009f, 
			0x00000005, 0x00000002, 0x000000a7, 0x00000000, 0x00000002, 
			0x000000b4, 0x00000004, 0x00000002, 0x000000be, 0x00000016, 
			0x00000002, 0x000000ce, 0x00000000, 0x00000002, 0x000000de, 
			0x00000001, 0x00000002, 0x000000f1, 0x00000080, 0x00000002, 
			0x00000105, 0x00000016, 0xff00ff00, 0x00000012, 0x00000000, 
			0x00000004, 0x00000002, 0x00000009, 0x00000001, 0x00000002, 
			0x00000013, 0x00000008, 0x00000018, 0x00000026, 0x00000000, 
			0x00000012, 0x00000044, 0x00000005, 0x00000012, 0x00000051, 
			0x00000006, 0x00000012, 0x00000062, 0x00000007, 0x00000002, 
			0x00000071, 0x00000000, 0x00000011, 0x00000080, 0x00000000, 
			0x00000002, 0x0000008a, 0x00145000, 0x00000002, 0x00000096, 
			0x00000002, 0x00000011, 0x0000009f, 0x00000005, 0x00000002, 
			0x000000a7, 0x00000000, 0x00000002, 0x000000b4, 0x00000004, 
			0x00000002, 0x000000be, 0x0000000a, 0x00000002, 0x000000ce, 
			0x00000000, 0x00000002, 0x000000de, 0x00000001, 0x00000002, 
			0x000000f1, 0x00000080, 0x00000002, 0x00000105, 0x0000000a, 
			0xff00ff00, 0x00000002, 0x00000118, 0x00000000, 0x00000002, 
			0x00000129, ERR_INJECT_ERR_FATAL, 0x00000002, 0x00000138, 0x0000005a, 
			0xff00ff00, 0x00000002, 0x00000148, 0x00000090, 0xff00ff00, 
			0x00000002, 0x00000151, 0x08000000, 0x00000002, 0x00000162, 
			0x00000f80, 0x00000002, 0x00000170, 0x00000080, 0xff00ff00, 
			0x00000002, 0x0000017e, 0x00000001, 0x00000002, 0x0000018c, 
			0x03002000, 0xff00ff00, 0x00000012, 0x0000019a, 0x00000008, 
			0xff00ff00, 0x00000002, 0x000001a9, 0x00000001, 0x00000002, 
			0x000001c5, 0x00000006, 0x00000002, 0x000001e9, 0x01000000, 
			0x00000002, 0x00000202, GPIOINT_DEVICE_LPA_DSP, 0x00000002, 0x0000020c, 
			0x00000026, 0x00000008, 0x0000021c, 0x00351901, 0x00000008, 
			0x00000233, 0x00221801, 0x00000012, 0x0000024e, 0x00000009, 
			0x00000002, 0x00000268, 0x00000002, 0x00000011, 0x0000027b, 
			0x0000001a, 0xff00ff00, 0x00000012, 0x0000028c, 0x0000000a, 
			0x00000002, 0x000002a2, 0x00000001, 0xff00ff00, 0x00000002, 
			0x000002b8, 0x09000000, 0x00000002, 0x000002c3, 0x003a2000, 
			0xff00ff00, 0x00000002, 0x000002d0, 0x00000000, 0xff00ff00, 
			0x00000012, 0x000002db, 0x0000000b, 0xff00ff00, 0x00000012, 
			0x000002ee, 0x0000000c, 0xff00ff00, 0x00000012, 0x00000306, 
			0x0000000d, 0xff00ff00, 0x00000012, 0x0000031e, 0x0000000e, 
			0xff00ff00, 0x00000012, 0x00000336, 0x0000000f, 0xff00ff00, 
			0x00000012, 0x0000034b, 0x00000010, 0xff00ff00, 0x00000012, 
			0x00000362, 0x00000011, 0xff00ff00, 0x00000012, 0x00000373, 
			0x00000012, 0xff00ff00, 0x00000012, 0x00000388, 0x00000013, 
			0xff00ff00, 0x00000012, 0x000003a0, 0x00000014, 0xff00ff00, 
			0x00000012, 0x000003b2, 0x00000015, 0xff00ff00, 0x00000012, 
			0x000003c5, 0x00000016, 0xff00ff00, 0x00000012, 0x000003da, 
			0x00000017, 0xff00ff00, 0x00000012, 0x000003f2, 0x00000018, 
			0xff00ff00, 0x00000012, 0x00000403, 0x00000019, 0xff00ff00, 
			0x00000012, 0x00000414, 0x0000001a, 0xff00ff00, 0x00000012, 
			0x0000042d, 0x0000001b, 0x00000012, 0x00000439, 0x0000001c, 
			0x00000012, 0x00000449, 0x0000001d, 0x00000012, 0x00000458, 
			0x0000001e, 0x00000002, 0x0000046c, 0x00000019, 0x00000002, 
			0x0000047f, 0x00000fa0, 0x00000002, 0x00000494, 0x00000004, 
			0x00000002, 0x000004ab, 0x00000004, 0x00000002, 0x000004c0, 
			0x000002d9, 0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 
			0x00000008, 0x000004e3, 0xd781845f, 0x8e2c6cc0, 0xf646c13e, 
			0xdf9db555, 0x70f91ebe, 0x6f8a6ada, 0x191cd11d, 0xe7bcfde8, 
			0x7415272a, 0xac82553b, 0xd4091903, 0xc067f609, 0x6cad3971, 
			0x94bf4968, 0xabd56e91, 0xf241bc63, 0x682c8151, 0xd5cead2c, 
			0x90c475a0, 0x95085945, 0xb1166dfb, 0x4c2dd799, 0x8016dbae, 
			0x83062c3f, 0x000000f8, 0xff00ff00, 0x00000002, 0x000004d6, 
			0x00000003, 0x00000008, 0x000004e3, 0xa0ee8d5f, 0x333a2519, 
			0x6a2b2560, 0x33d2726e, 0xc5cc301a, 0xbbf68ed9, 0x751ae9db, 
			0x8dca99bc, 0xb7b3b5d6, 0x6330262b, 0xe499af97, 0xcb0fb944, 
			0xf499c233, 0x0dfe1ad2, 0x187d6561, 0x548f3655, 0xc59966c5, 
			0xe9c86d73, 0x448f6025, 0x8b400c84, 0xb290036a, 0xbba804e1, 
			0x42c36977, 0x4a378014, 0x00000094, 0xff00ff00, 0x00000002, 
			0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 0x5b237d5f, 
			0x57cb387a, 0x473f2ced, 0xb7454bd3, 0x0ee01cac, 0xa51b2eab, 
			0xbb0f983e, 0x2f7058a2, 0x8e6562c8, 0x15f817f9, 0x84216b61, 
			0x32c69cd5, 0x8f0b7fa6, 0x6235acb1, 0xa541c48a, 0x99280012, 
			0x8ec4ce2c, 0xef1ad1ae, 0xcdfd4a2f, 0xdc998cab, 0xb4e19760, 
			0x853e0fc5, 0xde00d120, 0x2cd1c0ef, 0x0000008e, 0xff00ff00, 
			0x00000002, 0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 
			0x3f93945f, 0x9b23f682, 0x3a9d44c4, 0x9b40b933, 0x45427fb2, 
			0xde0f48af, 0x3dccde9f, 0x040835c2, 0x6c105176, 0xb9ac6548, 
			0xd127c055, 0x8fb50c4f, 0xe1e306ea, 0x747ed90c, 0x49af344b, 
			0x1a859fcc, 0x89eb9bf1, 0xe575b0d2, 0x8578e039, 0x4e0e8867, 
			0xf0d9cb74, 0x9cd27d7a, 0x27f1fdb9, 0x9a9425d8, 0x00000066, 
			0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 0x00000008, 
			0x000004e3, 0x3a909a5f, 0x26d9f76c, 0x705f677e, 0x8244bb12, 
			0x8ea8b145, 0xb53b204c, 0xf37bb7bf, 0x0ecd74bb, 0xfe5d8633, 
			0x77c23619, 0x0e8ae57d, 0xfe53ca62, 0x3242388f, 0x42912481, 
			0x2a5f3b06, 0x29377485, 0xb48d48b6, 0xe8f7c1ce, 0x04b441f0, 
			0xd14a0df6, 0x9cecf9ca, 0xfd25a8da, 0x601fbf4e, 0x80493be3, 
			0x000000bc, 0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 
			0x00000008, 0x000004e3, 0xbcedfe5f, 0x9c6ec3d6, 0x0209f7c4, 
			0x36cacd4f, 0xc2aa907e, 0xccb4101a, 0x2dcc2cfb, 0x7d83ee48, 
			0x78c1a3cd, 0x8eb87e27, 0x313399c6, 0x3637a29f, 0x4bad1f13, 
			0x3705948e, 0x72959acd, 0x03684ebe, 0xa1683be5, 0xc433591f, 
			0xb2c2398a, 0xac0acdcc, 0xc65b1eee, 0xf35c13df, 0x4c49ad34, 
			0x562043a4, 0x0000009f, 0xff00ff00, 0x00000002, 0x000004d6, 
			0x00000003, 0x00000008, 0x000004e3, 0x6138305f, 0x5d650f86, 
			0xa74145c9, 0xe39533a8, 0x372ff3a4, 0xfe9e9f10, 0x6324bd82, 
			0x0c5cf7b7, 0x1ea8eb89, 0xb469f2c7, 0xcea43512, 0xd560b485, 
			0x6ffd22af, 0x454e4201, 0x2ad17aa0, 0xd479f4d1, 0x705742f9, 
			0xbfc980b2, 0x2d74a5a1, 0x73e80c0e, 0xda5ae95d, 0x547bab66, 
			0x7b4b8e12, 0xaac149b0, 0x000000aa, 0xff00ff00, 0x00000002, 
			0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 0xf27be05f, 
			0xca248641, 0xeff2ef77, 0x1a4fd78d, 0xc3002128, 0x60508935, 
			0x5c173b1c, 0x90a2efa8, 0x40b00f11, 0xcdca6c6e, 0xb18db885, 
			0xae329032, 0x3d8ec8e4, 0x86e4e92b, 0xe5e8e0df, 0x74f5caf2, 
			0xe7f6c6e3, 0xd41c2bbd, 0x33373660, 0x4d1df8fe, 0x33ed4b88, 
			0xd284bd83, 0x43226462, 0xb5275a1d, 0x00000084, 0xff00ff00, 
			0x00000002, 0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 
			0xf87d135f, 0x8de3ebfe, 0x81a131d0, 0xdda37e8f, 0x3509fafd, 
			0xf3c08be4, 0x6d2e11bd, 0xfa0ce47f, 0x055b8727, 0x1a1d814a, 
			0xab2ade7c, 0x77808365, 0xb9291f89, 0x2184fd8e, 0x8f1d3bc3, 
			0xf367164b, 0x75866567, 0xa859df90, 0xb284aa78, 0x19feb62b, 
			0x02b70c0a, 0x6673fe6d, 0x7c1c983d, 0x6a5bc259, 0x000000a4, 
			0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 0x00000008, 
			0x000004e3, 0xab6e765f, 0x1c8730ea, 0x87cb9eae, 0x4c33bd71, 
			0xb4e4ed64, 0xe0273cde, 0xdbdca6b5, 0x0f9db8a1, 0xdee2eec1, 
			0x6e3f1a72, 0xeaa0c026, 0x5c04bb17, 0x427ccf68, 0xe12edd41, 
			0xa06ea509, 0x36b96117, 0xf34b1675, 0x9b46c0d7, 0x680e28b7, 
			0xca824ab6, 0x62c28ae3, 0x7fc8b486, 0x47e1a07a, 0xa8da1ba2, 
			0x000000cf, 0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 
			0x00000008, 0x000004e3, 0xb872275f, 0x78135666, 0xaaaa91d5, 
			0x5a879441, 0x408430be, 0x8c3d80a9, 0x1184d10e, 0xda73cb12, 
			0xabc1c2c1, 0x47e791cb, 0x6f5181a3, 0x3cbd3ce5, 0x8688a047, 
			0x4751267c, 0xa79912ac, 0x12c9198d, 0xeef0bba3, 0x91925700, 
			0x89ab4f26, 0xa59be716, 0x2115ef10, 0x44db03fa, 0xb28feb51, 
			0xe00d2b94, 0x00000038, 0xff00ff00, 0x00000002, 0x000004d6, 
			0x00000003, 0x00000008, 0x000004e3, 0x77bf525f, 0x686cb067, 
			0x69a35d92, 0xb206a132, 0x74204ea7, 0x6c1b4f12, 0x00ce419b, 
			0xb91e8e0e, 0xd9b26304, 0x35a29d7e, 0x1644ca1f, 0x6d92ed0b, 
			0x25867356, 0xb61c9d65, 0xd7fb8a45, 0xd7abfce5, 0x07cd6136, 
			0xe2fb5fb9, 0x68ec4337, 0x76960916, 0x66215978, 0x9a63b556, 
			0x865e6ccd, 0x7b34c58b, 0x0000002b, 0xff00ff00, 0x00000002, 
			0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 0x746c025f, 
			0x87c411e6, 0x88d07f15, 0x934caa91, 0x29714dc9, 0x00c0875f, 
			0xeaa74326, 0x6336cb7a, 0x67f3ca07, 0x00e1fe90, 0xe006b225, 
			0x418f2e5a, 0x46f2914a, 0xca6638b0, 0x5ef70d70, 0xa667d98e, 
			0xd9b76042, 0x19a72d7f, 0xaf8e3ffa, 0xc0129701, 0xf4c6dbd2, 
			0x72a81689, 0xa54b0e59, 0xa58e0c35, 0x000000a5, 0xff00ff00, 
			0x00000002, 0x000004d6, 0x00000003, 0x00000008, 0x000004e3, 
			0xd223f95f, 0x429d4834, 0x9da16d3b, 0x7905e5eb, 0x59920932, 
			0xb85c32ec, 0x563430f5, 0x1beedde7, 0xea2bfa54, 0x30b8a468, 
			0x9117a942, 0x69b2c41f, 0xbe19bbdd, 0xd8fbfe60, 0x8f74a06a, 
			0xa778d378, 0x669494df, 0x96515194, 0xe0315011, 0x7a479523, 
			0x74592332, 0x27e71b37, 0x8abb3a0b, 0x027c4f34, 0x00000002, 
			0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 0x00000008, 
			0x000004e3, 0xae3e2e5f, 0xf7760c58, 0x5da5b573, 0xebfe2bc4, 
			0xef110f17, 0xbcb4d873, 0x7ef0db39, 0x6c900f4c, 0x531fee82, 
			0x3c613c48, 0x458e90b3, 0x3bd70aaa, 0x04395c2a, 0x8fabfe7a, 
			0x35bde3cf, 0x41059f09, 0xe64d0e3c, 0xe1c51f2c, 0x27a5d2bc, 
			0x90f1b206, 0xd15dbf6d, 0xe1733d95, 0x8c56dbfe, 0x2cfa8a2e, 
			0x0000005a, 0xff00ff00, 0x00000002, 0x000004d6, 0x00000003, 
			0x00000008, 0x000004e3, 0x8707925f, 0x10eaa805, 0x3361bccc, 
			0xe03841a4, 0xaf450c82, 0xe3a13031, 0xf419e46a, 0xe6d09ae8, 
			0x7b1e3572, 0xe449e041, 0x53146f88, 0xeb62e9ea, 0x61439d93, 
			0xd419f734, 0x869b3030, 0xd87f697c, 0x93e41f6e, 0x22ba5921, 
			0x48d26a91, 0x69b48d9c, 0xb388ce77, 0x94c0d5f6, 0xee4fa229, 
			0xb5b56472, 0x0000006e, 0xff00ff00 };



const StringDevice driver_list_8996_xml[] = {
			{"tms_eic",3954984681u, 1844, NULL, 0, NULL },
			{"tms_diag",1665439693u, 1884, NULL, 0, NULL },
			{"/core/hwengines/bam",1285428979u, 1968, NULL, 0, NULL },
			{"Busywait_QTimer",1066675310u, 2136, NULL, 0, NULL },
			{"dynamic_modules",60570562u, 2164, NULL, 0, NULL },
			{"LPAIF",227230449u, 2180, NULL, 0, NULL },
			{"AUDIOIFDMA",1545482552u, 2196, NULL, 0, NULL },
			{"HDMIOUTDMA",190183249u, 2212, NULL, 0, NULL },
			{"GENERICCLK",977122844u, 2228, NULL, 0, NULL },
			{"AVTIMER",1576932157u, 2244, NULL, 0, NULL },
			{"RESAMPLER",2182523568u, 2260, NULL, 0, NULL },
			{"VFR",193473043u, 2276, NULL, 0, NULL },
			{"AFERiva",933332611u, 2292, NULL, 0, NULL },
			{"AFESlimbus",3168426160u, 2308, NULL, 0, NULL },
			{"MIDI",2089322568u, 2324, NULL, 0, NULL },
			{"LPASS",227230792u, 2340, NULL, 0, NULL },
			{"SPDIFTX",3094724903u, 2356, NULL, 0, NULL },
			{"HDMITX",3033296691u, 2372, NULL, 0, NULL },
			{"LPM",193462478u, 2388, NULL, 0, NULL },
			{"LPA",193462466u, 2404, NULL, 0, NULL },
			{"LSM",193462577u, 2420, NULL, 0, NULL },
			{"QCOM_CVD",536065169u, 2436, NULL, 0, NULL },
			{"/statichashes/AlacDecoderModule.so.1",3729086211u, 2548, NULL, 0, NULL },
			{"/statichashes/DolbyMobileModule.so.1",3821631374u, 2672, NULL, 0, NULL },
			{"/statichashes/ApeDecoderModule.so.1",3458978760u, 2796, NULL, 0, NULL },
			{"/statichashes/DTS_HPX_MODULE.so.1",10979733u, 2920, NULL, 0, NULL },
			{"/statichashes/WmaProDecoderModule.so.1",3198417128u, 3044, NULL, 0, NULL },
			{"/statichashes/FlacDecoderModule.so.1",463653928u, 3168, NULL, 0, NULL },
			{"/statichashes/mmecns_module.so.1",803170718u, 3292, NULL, 0, NULL },
			{"/statichashes/EtsiEaacPlusEncAndCmnModule.so.1",684221926u, 3416, NULL, 0, NULL },
			{"/statichashes/fluence_voiceplus_module.so.1",3871400022u, 3540, NULL, 0, NULL },
			{"/statichashes/VorbisDecoderModule.so.1",1720273127u, 3664, NULL, 0, NULL },
			{"/statichashes/SrsTruMediaModule.so.1",1479894031u, 3788, NULL, 0, NULL },
			{"/statichashes/HeaacDecoderModule.so.1",617796932u, 3912, NULL, 0, NULL },
			{"/statichashes/SAPlusCmnModule.so.1",1532347186u, 4036, NULL, 0, NULL },
			{"/statichashes/AudioSphereModule.so.1",3660239125u, 4160, NULL, 0, NULL },
			{"/statichashes/DolbySurroundModule.so.1",2616300472u, 4284, NULL, 0, NULL },
			{"/statichashes/WmaStdDecoderModule.so.1",2964775010u, 4408, NULL, 0, NULL }
};
