#ifndef RCEVT_INTERNAL_H
#define RCEVT_INTERNAL_H
/** vi: tw=128 ts=3 sw=3 et :
@file rcevt_internal.h
@brief This file contains the API for the Run Control Framework, API 2.1
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2015 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
===============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.adsp/2.7/debugtools/api/rcevt_internal.h#4 $
$DateTime: 2015/06/17 12:21:03 $
$Change: 8389501 $
$Author: pwbldsvc $
===============================================================================*/

#include "tms_rcevt.h"

#endif
