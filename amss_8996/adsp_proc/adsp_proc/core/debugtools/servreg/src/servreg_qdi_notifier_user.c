/*
#============================================================================
#  Name:
#    servreg_qdi_notifier_user.c
#
#  Description:
#    Service Registry notifier file for user image. This module serves as the end-point
#    of communication via qdi.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "qurt.h"
#include "msg.h"
#include "tms_utils.h"

#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_notifier.h"
#include "servreg_qdi_notifier_user.h"
#include "servreg_qdi_client.h"    /* Include for user PD only */

#define SERVREG_NOTIF_WORKER_TASK_NAME                "srnotifworker"
#define SERVREG_NOTIF_WORKER_TASK_STACK               2048

static struct
{
   qurt_thread_t tid;

} servreg_listen_internal;

/* Service Registry Notifier node structure. List of SERVREG_QDI_NOTIF_HANDLE'S */
struct servreg_qdi_notif_node_s
{
   uint32_t notif_signature;
   SERVREG_MON_HANDLE sr_mon_handle;
   SERVREG_REMOTE_HANDLE sr_remote_handle;
   struct servreg_qdi_notif_node_s* next;
};
typedef struct servreg_qdi_notif_node_s servreg_qdi_notif_node_t, * servreg_qdi_notif_node_p;

/* Type casts as accessor functions */
#define sr_qdi_notif_node2sr_qdi_notif_handle(x)        ((SERVREG_QDI_NOTIF_HANDLE)x)
#define sr_qdi_notif_handle2sr_qdi_notif_node(x)        ((servreg_qdi_notif_node_p)x)

/* Pool Allocations Notif node */
struct servreg_qdi_notif_node_pool_s
{
   struct servreg_qdi_notif_node_s servreg_qdi_notif_node_pool[SERVREG_QDI_NOTIF_NODE_POOL_SIZE];
   struct servreg_qdi_notif_node_pool_s * next;
};
typedef struct servreg_qdi_notif_node_pool_s servreg_qdi_notif_node_pool_t, * servreg_qdi_notif_node_pool_p;

/* Internal structure */
struct servreg_qdi_notif_node_internal_s
{
   servreg_qdi_notif_node_pool_p servreg_qdi_notif_node_pool_head_p;
   servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_free_p;
   servreg_mutex_t mutex;
   servreg_mutex_t mutex_create;
   SERVREG_BOOL dynamic_use;
   unsigned long init_flag;
};

struct servreg_qdi_notif_node_internal_s servreg_qdi_notif_node_internal;
servreg_qdi_notif_node_pool_t servreg_qdi_notif_node_pool_static;

/* Head node of the servreg notification list */
servreg_qdi_notif_node_p servreg_notif_list_head = SERVREG_NULL;

/* Static functions in this file */
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_init(void);
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_alloc(void);
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_free(servreg_qdi_notif_node_p servreg_qdi_notif_node_p);
static void servreg_qdi_notif_node_internal_init(void);

/** =====================================================================
 * Function:
 *     servreg_qdi_notif_node_pool_init
 *
 * Description:
 *     Initializes the memory pool for notifier node structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qdi_notif_node_p : Returns the first free notifier node space from the pool
 * =====================================================================  */
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_init(void)
{
   servreg_qdi_notif_node_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_head_p)
   {
      next_pool = &servreg_qdi_notif_node_pool_static;
   }
   else if (SERVREG_TRUE == servreg_qdi_notif_node_internal.dynamic_use)
   {
      next_pool = (servreg_qdi_notif_node_pool_p)servreg_malloc(sizeof(servreg_qdi_notif_node_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_QDI_NOTIF_NODE_POOL_SIZE; i++)
      {
         if (i != (SERVREG_QDI_NOTIF_NODE_POOL_SIZE - 1))
         {
            next_pool->servreg_qdi_notif_node_pool[i].next = &(next_pool->servreg_qdi_notif_node_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_qdi_notif_node_pool[i].next = servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p;
         }

         next_pool->servreg_qdi_notif_node_pool[i].notif_signature = SERVREG_QDI_NOTIF_SIGNATURE;
         next_pool->servreg_qdi_notif_node_pool[i].sr_mon_handle = SERVREG_NULL;
         next_pool->servreg_qdi_notif_node_pool[i].sr_remote_handle = 0;
      }

      servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p = &(next_pool->servreg_qdi_notif_node_pool[0]);
      next_pool->next = servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_head_p;
      servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_head_p = next_pool;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_qdi_notif_node_pool_init() malloc failed");
      return SERVREG_NULL;
   }

   return servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_notif_node_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_qdi_notif_node_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_alloc(void)
{
   servreg_qdi_notif_node_p ret;
   servreg_qdi_notif_node_p sr_qdi_notif_node;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex));

   if (SERVREG_NULL == servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p)
   {
      sr_qdi_notif_node = servreg_qdi_notif_node_pool_init();
   }
   else
   {
      sr_qdi_notif_node = servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p;
   }

   if (SERVREG_NULL != sr_qdi_notif_node)
   {
      servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p = sr_qdi_notif_node->next;
      sr_qdi_notif_node->next = SERVREG_NULL;
      ret = sr_qdi_notif_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_qdi_notif_node_pool_alloc() alloc failed");
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_notif_node_pool_free
 *
 * Description:
 *     Reclaims back the sr_qdi_notif_node to the memory pool
 *
 * Parameters:
 *     sr_qdi_notif_node : space to be reclaimed back
 *
 * Returns:
 *    servreg_qdi_notif_node_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_qdi_notif_node_p servreg_qdi_notif_node_pool_free(servreg_qdi_notif_node_p sr_qdi_notif_node)
{
   servreg_qdi_notif_node_p ret = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex));

   if(SERVREG_NULL != sr_qdi_notif_node)
   {
      sr_qdi_notif_node->sr_mon_handle = SERVREG_NULL;
      sr_qdi_notif_node->sr_remote_handle = 0;

      sr_qdi_notif_node->next = servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p;
      servreg_qdi_notif_node_internal.servreg_qdi_notif_node_pool_free_p = sr_qdi_notif_node;
      ret = sr_qdi_notif_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_qdi_notif_node_pool_free() sr_qdi_notif_node is NULL and cannot be freed");
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_notif_node_internal_init
 *
 * Description:
 *     Initialization of the internal memory pools and other internals
 *     for sr notifier nodes
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qdi_notif_node_internal_init(void)
{
   servreg_mutex_init_dal(&(servreg_qdi_notif_node_internal.mutex));
   servreg_mutex_init_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex));

   servreg_qdi_notif_node_internal.dynamic_use = TRUE;
   servreg_qdi_notif_node_pool_init();

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex));
}

/** =====================================================================
 * Function:
 *     servreg_get_qdi_notif_node
 *
 * Description:
 *     Checks if a notifier node already exists with the sr monitor handle. If it does 
 *     exists it returns a pointer to that notif node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the notifier node
 * =====================================================================  */
SERVREG_QDI_NOTIF_HANDLE servreg_get_qdi_notif_node(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_qdi_notif_node_p sr_qdi_notif_node = SERVREG_NULL;
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   sr_qdi_notif_node = servreg_notif_list_head;

   while(SERVREG_NULL != sr_qdi_notif_node)
   {
      if(sr_mon_handle == sr_qdi_notif_node->sr_mon_handle)
      {
         sr_qdi_notif_handle = sr_qdi_notif_node2sr_qdi_notif_handle(sr_qdi_notif_node);
         break;
      }
      else
      {
         sr_qdi_notif_node = sr_qdi_notif_node->next;
      }
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   return sr_qdi_notif_handle;
}

/** =====================================================================
 * Function:
 *     servreg_create_qdi_notif_node
 *
 * Description:
 *     Creates a sr notif node with the given sr_mon_handle and sr_remote_handle
 *
 * Parameters:
 *     sr_mon_handle    : Handle to an existing service state which is mapped by domain + service 
 *                        or just domain name
 *     sr_remote_handle : Remote (root PD) notifier node address
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the notifier node
 * =====================================================================  */
SERVREG_QDI_NOTIF_HANDLE servreg_create_qdi_notif_node(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_REMOTE_HANDLE sr_remote_handle)
{
   servreg_qdi_notif_node_p sr_qdi_notif_node = SERVREG_NULL;  
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;

   /* Check if the srnotif node exists */
   //sr_qdi_notif_handle = servreg_get_qdi_notif_node(sr_mon_handle);

   //if(SERVREG_NULL == sr_qdi_notif_handle)
   //{
      servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

      sr_qdi_notif_node = servreg_qdi_notif_node_pool_alloc();

      if(SERVREG_NULL != sr_qdi_notif_node)
      {
         /*  Insert to head of list */
         sr_qdi_notif_node->next = servreg_notif_list_head;
         /* Update head */
         servreg_notif_list_head = sr_qdi_notif_node;

         sr_qdi_notif_node->sr_mon_handle = sr_mon_handle;
         sr_qdi_notif_node->sr_remote_handle = sr_remote_handle;
         sr_qdi_notif_handle = sr_qdi_notif_node2sr_qdi_notif_handle(sr_qdi_notif_node);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_create_qdi_notif_node() sr_qdi_notif_node alloc failed \n");
      }

      servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));
   //}
   return sr_qdi_notif_handle;
}

/** =====================================================================
 * Function:
 *     servreg_delete_qdi_notif_node
 *
 * Description:
 *     Deletes a sr notif node given the sr notif handle
 *
 * Parameters:
 *     sr_qdi_notif_handle  : Handle to the notifier node to be deleted
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_delete_qdi_notif_node(SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle)
{
   servreg_qdi_notif_node_p sr_qdi_notif_node = SERVREG_NULL, sr_notif_prev = SERVREG_NULL, sr_qdi_notif_node_del = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   sr_qdi_notif_node = servreg_notif_list_head;
   sr_qdi_notif_node_del = sr_qdi_notif_handle2sr_qdi_notif_node(sr_qdi_notif_handle);

   if(SERVREG_NULL != sr_qdi_notif_node_del)
   {
      if(SERVREG_QDI_NOTIF_SIGNATURE == sr_qdi_notif_node_del->notif_signature)
      {
         while(SERVREG_NULL != sr_qdi_notif_node)
         {
            if(sr_qdi_notif_node == sr_qdi_notif_node_del)
            {
               if(SERVREG_NULL == sr_notif_prev)
               {
                  servreg_notif_list_head = sr_qdi_notif_node->next;
               }
               else
               {
                  sr_notif_prev->next = sr_qdi_notif_node->next;
               }

               sr_qdi_notif_node->next = SERVREG_NULL;

               /* Reclaim back the notif msg to free pool */
               if(SERVREG_NULL != servreg_qdi_notif_node_pool_free(sr_qdi_notif_node))
               {
                  ret = SERVREG_SUCCESS;
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_delete_qdi_notif_node() pool_free returned NULL ");
               }
               break;
            }
            else
            {
               sr_notif_prev = sr_qdi_notif_node;
               sr_qdi_notif_node = sr_qdi_notif_node->next;  
            }
         } /* while() */
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_delete_qdi_notif_node() sr_qdi_notif_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_delete_qdi_notif_node() sr_qdi_notif_handle is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_map_remote_handle
 *
 * Description:
 *     Given the remote notif handle i.e root pd notif node address, return
 *     the user pd's corresponding monitor handle
 *
 * Parameters:
 *     sr_remote_handle : Remote (root PD) notifier node address
 *
 * Returns:
 *     SERVREG_MON_HANDLE :  Handle to an existing service state which is mapped by domain + service 
 *                           or just domain name
 * =====================================================================  */
SERVREG_MON_HANDLE servreg_map_remote_handle(SERVREG_REMOTE_HANDLE sr_remote_handle)
{
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   servreg_qdi_notif_node_p sr_qdi_notif_node = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   sr_qdi_notif_node = servreg_notif_list_head;

   while(SERVREG_NULL != sr_qdi_notif_node)
   {
      if(sr_remote_handle == sr_qdi_notif_node->sr_remote_handle)
      {
         sr_mon_handle = sr_qdi_notif_node->sr_mon_handle;
         break;
      }
      else
      {
         sr_qdi_notif_node = sr_qdi_notif_node->next;
      }
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   return sr_mon_handle;
}

/** =====================================================================
 * Task:
 *     servreg_notif_worker_task
 *
 * Description:
 *     This is the service register notifier worker task which keeps track of state
 *     changes in the root PD's notifier nodes and reports back to the user
 *     PD notifier nodes
 *
 * Parameters:
 *     None
 * 
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_notif_worker_task(void *argv __attribute__((unused)))
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_REMOTE_HANDLE sr_remote_handle;
   uint32_t new_curr_state;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   uint32_t transaction_id;

   //ret = servreg_qdi_client_init();

   //if (SERVREG_SUCCESS == ret)
   //{
      do
      {
         /* Blocking wait within qdi driver */
         ret = servreg_qdi_invoke_worker_wait();

         if (SERVREG_SUCCESS == ret)
         {
            servreg_qdi_invoke_get_remote_handle(&sr_remote_handle, &new_curr_state, &transaction_id);
            sr_mon_handle = servreg_map_remote_handle(sr_remote_handle);
            if(SERVREG_NULL != sr_mon_handle)
            {
               /* Get the transaction_id and decrease the value by one as servreg_set_state increases it by one */
               transaction_id = transaction_id - 1;
               servreg_set_transaction_id(sr_mon_handle, transaction_id);
               servreg_set_state(sr_mon_handle, new_curr_state);
            }
         }

      } while (1);
   //}
   //else
   //{
   //   qurt_thread_exit(1);
   //}

}

/** =====================================================================
 * Function:
 *     servreg_register_remote_listener
 *
 * Description:
 *     Register client as a remote listener because the service is not a local service.
 *     QDI is used as the communication medium for registering the listener
 *     with the remote service registry framework.
 *
 *     For every service, remote registration is done only once and one proxy listener in 
 *     the root PD will be registered that represents all the listeners in the user PD
 *     for that service.
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_remote_listener(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle, uint32_t * sr_curr_remote_state, uint32_t * sr_remote_transaction_id)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_REMOTE_HANDLE sr_remote_handle;
   uint32_t  sr_remote_state, sr_transaction_id;
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;

   /* Check if the srnotif node exists */
   sr_qdi_notif_handle = servreg_get_qdi_notif_node(sr_mon_handle);

   /* Create a new notif node and register a new proxy listener in root-pd only if the srnotif node does not exist */
   if(SERVREG_NULL == sr_qdi_notif_handle)
   {
      servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

      ret = servreg_qdi_invoke_register_remote_listener(service_name, &sr_remote_handle, &sr_remote_state, &sr_transaction_id);
      if(ret == SERVREG_SUCCESS)
      {
         /* Create a Service Notifier node to list the sr_mon_handle to get remote notifications from root pd */
         sr_qdi_notif_handle = servreg_create_qdi_notif_node(sr_mon_handle, sr_remote_handle);
         if(SERVREG_NULL != sr_qdi_notif_handle)
         {
            /* Get the initial state of the remote handle and set that state in the local handle also */
            *sr_curr_remote_state = sr_remote_state;
            *sr_remote_transaction_id = sr_transaction_id;
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_register_remote_listener() could not register as a remote listener in the notifier pool\n");
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_register_remote_listener() servreg_qdi_invoke_register_remote_listener() failed \n");
      }

      servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));
   }

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_deregister_remote_listener
 *
 * Description:
 *     De-register the remote proxy listener only if there are no more local listeners 
 *     for that service.
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_deregister_remote_listener(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   uint32_t sr_listener_count = 0;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   sr_listener_count = servreg_get_listener_ref_count(sr_mon_handle);

   if(sr_listener_count == 0)
   {
      ret = servreg_qdi_invoke_deregister_remote_listener(service_name);
   }
   else
   {
      ret = SERVREG_SUCCESS;
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_free_remote_handle
 *
 * Description:
 *     Free the handle created when clients register as a remote listener. 
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_free_remote_handle(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;

   sr_qdi_notif_handle = servreg_get_qdi_notif_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   if(SERVREG_NULL != sr_qdi_notif_handle)
   {
      ret = servreg_qdi_invoke_free_remote_handle(service_name);
   }

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_set_remote_ack
 *
 * Description:
 *     Set the remote ack count for the remote proxy listener via QDI only if 
 *     all the local acks have been received
 *
 * Parameters:
 *     service_name       : Service name that the client is interested in 
 *     SERVREG_MON_HANDLE : Opaque handle to existing event
 *     curr_state         : state for which the ACK will be set. Not used in local service registry
 *                          cause the next state is not updated until all the ACK's for the current
 *                          state have received.
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_set_remote_ack(SERVREG_NAME service_name, SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE curr_state, uint32_t sr_transaction_id)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   ret = servreg_qdi_invoke_set_remote_ack(service_name, sr_transaction_id);

   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_create_qmi_table_entry
 *
 * Description:
 *     Stores the qmi_instance_id value for that domain_name
 *
 * Parameters:
 *     domain_name : string which has the "soc/domain/subdomain" info
 *     qmi_instance_id : instance id used to establish a qmi connection for remote service updates
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_create_qmi_table_entry(SERVREG_NAME domain_name, uint32 qmi_instance_id)
{
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_qdi_notif_node_internal.mutex_create));

   ret = servreg_qdi_invoke_create_qmi_entry(domain_name, qmi_instance_id);

   if(SERVREG_SUCCESS == ret)
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI_NOTIF: in servreg_create_qmi_table_entry() created entry in root-pd");
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI_NOTIF: in servreg_create_qmi_table_entry() failed ");
   }
   
   servreg_mutex_unlock_dal(&(servreg_qdi_notif_node_internal.mutex_create));
}

/** =====================================================================
 * Function:
 *     servreg_notifier_init
 *
 * Description:
 *     Initialization function for the service registry notifier module
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_notifier_init(void)
{
   qurt_thread_attr_t attr;
   static unsigned long stack[SERVREG_NOTIF_WORKER_TASK_STACK/sizeof(unsigned long)];

   servreg_qdi_notif_node_internal_init();

   qurt_thread_attr_init(&attr);
   qurt_thread_attr_set_name(&attr, SERVREG_NOTIF_WORKER_TASK_NAME);
   qurt_thread_attr_set_stack_addr(&attr, stack);
   qurt_thread_attr_set_stack_size(&attr, sizeof(stack));
   qurt_thread_attr_set_priority(&attr, qurt_thread_get_priority(qurt_thread_get_id()) - 1);
   qurt_thread_attr_set_affinity(&attr, QURT_THREAD_ATTR_AFFINITY_DEFAULT);

   qurt_thread_create(&servreg_listen_internal.tid, &attr, servreg_notif_worker_task, NULL);
}

