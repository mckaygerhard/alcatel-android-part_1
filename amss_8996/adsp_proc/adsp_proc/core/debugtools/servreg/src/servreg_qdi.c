/*
#============================================================================
#  Name:
#    servreg_qdi.c 
#
#  Description:
#    Implements QDI layer for Service registry that goes to the root image
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "qurt.h"
#include "msg.h"
#include "tms_utils.h"

#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_monitor_qurt.h"
#include "servreg_notifier.h"
#include "servreg_qdi_notifier_root.h"
#include "servreg_qdi.h"        /* Include for root PD only */

qurt_mutex_t servnotif_qdi_mutex;
static qurt_qdi_obj_t servnotif_qdiobj;

static servnotif_qdi_device_data_t servreg_qdi_clients_data[SERVREG_QDI_MAX_PDS];
static servnotif_qdi_device_t servreg_qdi_clients[SERVREG_QDI_MAX_PDS];
static servnotif_qdi_pid_state_t servreg_pid_table[SERVREG_QDI_MAX_PDS];

/* Static functions in this file */
static void servreg_qdi_release(qurt_qdi_obj_t* qdiobj);
static int servreg_qdi_open(int client_handle, SERVREG_NAME name, uint32_t name_len);
static int servreg_qdi_close(int client_handle, uint32_t pid);
static int servreg_qdi_worker_wait(int client_handle, uint32_t pid);
static int servreg_qdi_register_remote_listener(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len, uint32_t *remote_addr, uint32_t *remote_state, uint32_t * remote_transaction_id);
static int servreg_qdi_deregister_remote_listener(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len);
static int servreg_qdi_free_remote_handle(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len);
static int servreg_qdi_set_remote_ack(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len, uint32_t transaction_id);
static int servreg_qdi_get_remote_handle(int client_handle, uint32_t pid, uint32_t * remote_addr, uint32_t *remote_state, uint32_t *transaction_id);

/** =====================================================================
 * Function:
 *     servreg_qdi_release
 *
 * Description: 
 *     Deallocates the specified device handle.
 *
 * Parameters:
 *    qdiobj : Pointer to opener object for the device
 *
 * Returns:
 *    None
 * =====================================================================  */
static void servreg_qdi_release(qurt_qdi_obj_t* qdiobj)
{
   servnotif_qdi_device_t *clientobj = (servnotif_qdi_device_t *)qdiobj;
   servnotif_qdi_device_data_t *dataobj;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   /* Serialize driver access when clearing the pid value */
   qurt_pimutex_lock(&servnotif_qdi_mutex);

   dataobj = &servreg_qdi_clients_data[clientobj->pid];

   ret = servreg_delete_qdi_notif_list(&dataobj->servreg_qdi_notif_node_list_head, &dataobj->sig_t, dataobj->sig_m);

   if(SERVREG_SUCCESS == ret)
   {
      qurt_anysignal_destroy(&dataobj->sig_t);
      dataobj->servreg_qdi_notif_node_list_head = SERVREG_NULL;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_release() servreg_delete_qdi_notif_list() failed ");
   }

   servreg_pid_table[clientobj->pid].pid_connected = SERVREG_FALSE;

   qurt_pimutex_unlock(&servnotif_qdi_mutex);

   return;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_open
 *
 * Description: 
 *     Opens a new qdi driver and obtains a specified handle
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     name           : User pd name that will register with root srm as SERVICE_STATE_UP
 *     len            : name length
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_open(int client_handle, SERVREG_NAME name, uint32_t name_len)
{
   servnotif_qdi_device_t *clientobj;
   servnotif_qdi_device_data_t *dataobj;
   uint32_t pid, sr_len = name_len;
   SERVREG_NAME domain = SERVREG_NULL;
   int ret = -1, ret_handle = -1, i = 0;

   /* Serialize driver access when assigning the pid value */
   qurt_pimutex_lock(&servnotif_qdi_mutex);
   for(i = 0; i < SERVREG_QDI_MAX_PDS; i++)
   {
      if(servreg_pid_table[i].pid_connected == SERVREG_FALSE)
      {
         pid = servreg_pid_table[i].pid;
         servreg_pid_table[i].pid_connected = SERVREG_TRUE;
         break;
      }
   }
   qurt_pimutex_unlock(&servnotif_qdi_mutex);

   if(i == SERVREG_QDI_MAX_PDS)
   {
      ERR_FATAL("Max Processes %d reached", i, 0, 0);
   }

   clientobj = &servreg_qdi_clients[pid];
   clientobj->qdiobj.invoke = servreg_qdi_invoke;
   clientobj->qdiobj.refcnt = QDI_REFCNT_INIT;
   clientobj->qdiobj.release = servreg_qdi_release;
   clientobj->pid = pid;

   dataobj = &servreg_qdi_clients_data[pid];
   dataobj->pid = pid;
   qurt_anysignal_init(&dataobj->sig_t);
   dataobj->sig_m = SERVREG_QDI_MASK_VALUE;
   dataobj->servreg_qdi_notif_node_list_head = SERVREG_NULL;

   ret_handle = qurt_qdi_handle_create_from_obj_t(client_handle, &clientobj->qdiobj);

   /* Register user pd as service up */
   if(-1 != ret_handle)
   {
      domain = servreg_malloc(sr_len);

      if (SERVREG_NULL != domain)
      {
         /* Get the service name from user space */
         ret = qurt_qdi_copy_from_user(client_handle, domain, name, sr_len);

         if(-1 != ret)
         {
            dataobj->sr_mon_handle = servreg_alloc_monitor_handle(domain, SERVREG_NULL);

            if(SERVREG_NULL != dataobj->sr_mon_handle)
            {
               /* Always set the pd service itself as synchronous */
               if(SERVREG_SUCCESS != servreg_set_synchronous(dataobj->sr_mon_handle))
               {
                  ERR_FATAL("SERVREG_QDI: servreg_set_synchronous() failed", 0, 0, 0);
               }

               if(SERVREG_SUCCESS == servreg_set_state(dataobj->sr_mon_handle, SERVREG_SERVICE_STATE_UP))
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: User PD recorded in Root-PD as UP \n");
               }
               else
               {
                  ERR_FATAL("SERVREG_QDI: servreg_set_state() failed", 0, 0, 0);
               }
            }
            else
            {
               ERR_FATAL("SERVREG_QDI: dataobj->sr_mon_handle NULL", 0, 0, 0);
            }
         }
         else
         {
            ERR_FATAL("SERVREG_QDI: qurt_qdi_copy_from_user() failed ", 0, 0, 0);
         }

         servreg_free(domain);
      }
      else
      {
         ERR_FATAL("SERVREG_QDI: malloc failed ", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_QDI: qurt_qdi_handle_create_from_obj_t() failed ", 0, 0, 0);
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   /* Need to return back the handle created once the pd is set as UP*/
   return ret_handle;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_close
 *
 * Description: 
 *     Closes the specified driver, releasing any resources associated with the open driver.
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_close(int client_handle, uint32_t pid)
{
   int ret = -1;
   servnotif_qdi_device_data_t *dataobj;

   dataobj = &servreg_qdi_clients_data[pid];

   if(SERVREG_SUCCESS == servreg_set_state(dataobj->sr_mon_handle, SERVREG_SERVICE_STATE_DOWN))
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: in servreg_qdi_close() User-PD state is set to DOWN");
      ret = 0;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: servreg_set_state() failed");
   }
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_worker_wait
 *
 * Description: 
 *     User pd invokes this function that waits on a qurt signal to be set
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_worker_wait(int client_handle, uint32_t pid)
{
   int ret = -1;
   servnotif_qdi_device_data_t *dataobj;

   dataobj = &servreg_qdi_clients_data[pid]; 

   /* Wait till you get notified of any service state change */
   qurt_signal_wait_cancellable(&dataobj->sig_t, dataobj->sig_m, QURT_SIGNAL_ATTR_WAIT_ANY, NULL);
   /* Clear the bit immediately so that we don't miss consecutive signal set */
   qurt_anysignal_clear(&dataobj->sig_t, dataobj->sig_m);

   ret = 0;

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_register_remote_listener
 *
 * Description: 
 *     Register the user pd proxy listener in the root pd's pool
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     name           : service name that the user-pd is interested in
 *     name_len       : length of the name
 *     remote_address : send back the root pd's monitor handle address to the user pd
 *     remote_state   : send back the root pd's state associated with the monitor handle
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_register_remote_listener(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len, uint32_t *remote_addr, uint32_t *remote_state, uint32_t *remote_transaction_id)
{
   SERVREG_RESULT result = SERVREG_FAILURE;
   int ret = -1;
   SERVREG_NAME service_name = SERVREG_NULL;
   uint32_t sr_len = name_len;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;
   uint32_t addr, transaction_id;
   SERVREG_SERVICE_STATE state;
   servnotif_qdi_device_data_t *dataobj;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);

   dataobj = &servreg_qdi_clients_data[pid]; 

   service_name = servreg_malloc(sr_len);
   if(SERVREG_NULL != service_name)
   {
      /* Get the service name from user space */
      ret = qurt_qdi_copy_from_user(client_handle, service_name, name, sr_len);
      if (-1 != ret)
      {
         /* Allocate or get the existing SERVREG HANDLE */
         sr_mon_handle = servreg_alloc_monitor_handle(service_name, SERVREG_NULL);
         if(SERVREG_NULL != sr_mon_handle)
         {
            result = servreg_register_listener_qurt(sr_mon_handle, &dataobj->sig_t, dataobj->sig_m);
            if(SERVREG_SUCCESS == result)
            {
               /* Create a Service Notifier node to list the sr_mon_handle to get remote notifications from root pd */
               sr_qdi_notif_handle = servreg_create_qdi_notif_node(sr_mon_handle, &dataobj->servreg_qdi_notif_node_list_head);
               if(SERVREG_NULL != sr_qdi_notif_handle)
               {
                  /* Send the address of the remote handle to user space */
                  addr = (uint32_t)sr_qdi_notif_handle;
                  ret = qurt_qdi_copy_to_user(client_handle, remote_addr, &addr, sizeof(uint32_t));
                  if(-1 != ret)
                  {
                     state = servreg_get_service_curr_state(sr_mon_handle);
                     ret = qurt_qdi_copy_to_user(client_handle, remote_state, &state, sizeof(uint32_t));
                     if(-1 != ret)
                     {
                        transaction_id = servreg_get_transaction_id(sr_mon_handle);
                        ret = qurt_qdi_copy_to_user(client_handle, remote_transaction_id, &transaction_id, sizeof(uint32_t));

                        if(-1 == ret)
                        {
                           MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() qurt_qdi_copy_to_user() failed \n");
                        }
                     }
                     else
                     {
                        MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() qurt_qdi_copy_to_user() failed \n");
                     }
                  }
                  else
                  {
                     MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() qurt_qdi_copy_to_user() failed \n");
                  }
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() sr_qdi_notif_handle is null\n");
                  ret = -1;
               }
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() could not register as a listener in the monitor pool\n");
               ret = -1;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() sr_mon_handle NULL");
            ret = -1;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() qurt_qdi_copy_from_user() failed\n");
      }

      servreg_free(service_name);
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_register_remote_listener() malloc failed\n");
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_deregister_remote_listener
 *
 * Description: 
 *     De-register the user-pd proxy listener
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     name           : service name that the user-pd is wants to de-register
 *     name_len       : length of the name
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_deregister_remote_listener(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len)
{
   SERVREG_RESULT result = SERVREG_FAILURE;
   int ret = -1;
   SERVREG_NAME service_name = SERVREG_NULL;
   uint32_t sr_len = name_len;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   servnotif_qdi_device_data_t *dataobj;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);
   dataobj = &servreg_qdi_clients_data[pid]; 

   service_name = servreg_malloc(sr_len);
   if(SERVREG_NULL != service_name)
   {
      /* Get the service name from user space */
      ret = qurt_qdi_copy_from_user(client_handle, service_name, name, sr_len);
      if (-1 != ret)
      {
         /* Get the existing SERVREG HANDLE */
         sr_mon_handle = servreg_get_sr_mon_handle(service_name);
         if(SERVREG_NULL != sr_mon_handle)
         {
            result = servreg_deregister_listener_qurt(sr_mon_handle, &dataobj->sig_t, dataobj->sig_m);
            if(SERVREG_SUCCESS != result)
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_deregister_remote_listener() servreg_deregister_listener_qurt() failed");
               ret = -1;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_deregister_remote_listener() sr_mon_handle NULL");
            ret = -1;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_deregister_remote_listener()  qurt_qdi_copy_from_user() failed");
      }

      servreg_free(service_name);
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_deregister_remote_listener() malloc failed");
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_free_remote_handle
 *
 * Description: 
 *     Free the handle associated with user pd proxy listener
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     name           : service name that the user-pd is wants to de-register
 *     name_len       : length of the name
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_free_remote_handle(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len)
{
   SERVREG_RESULT result = SERVREG_FAILURE;
   int ret = -1;
   SERVREG_NAME service_name = SERVREG_NULL;
   uint32_t sr_len = name_len;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   //servnotif_qdi_device_data_t *dataobj;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);
   //dataobj = &servreg_qdi_clients_data[pid]; 

   service_name = servreg_malloc(sr_len);
   if(SERVREG_NULL != service_name)
   {
      /* Get the service name from user space */
      ret = qurt_qdi_copy_from_user(client_handle, service_name, name, sr_len);
      if (-1 != ret)
      {
         /* Get the existing SERVREG HANDLE */
         sr_mon_handle = servreg_get_sr_mon_handle(service_name);
         if(SERVREG_NULL != sr_mon_handle)
         {
            /* Free the proxy monitor handle */
            result = servreg_free_monitor_handle(sr_mon_handle);

            if(SERVREG_SUCCESS != result)
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() servreg_free_monitor_handle() failed");
               ret = -1;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() sr_mon_handle NULL");
            ret = -1;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() qurt_qdi_copy_from_user() failed");
      }

      servreg_free(service_name);
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() malloc failed");
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_set_remote_ack
 *
 * Description: 
 *     Set the ack count for the user pd proxy listener
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     name           : service name that the user-pd is wants to de-register
 *     name_len       : length of the name
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_set_remote_ack(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len, uint32_t transaction_id)
{
   SERVREG_RESULT result = SERVREG_FAILURE;
   int ret = -1;
   SERVREG_NAME service_name = SERVREG_NULL;
   uint32_t sr_len = name_len;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   //servnotif_qdi_device_data_t *dataobj;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);
   //dataobj = &servreg_qdi_clients_data[pid]; 

   service_name = servreg_malloc(sr_len);
   if(SERVREG_NULL != service_name)
   {
      /* Get the service name from user space */
      ret = qurt_qdi_copy_from_user(client_handle, service_name, name, sr_len);
      if (-1 != ret)
      {
         /* Get the existing SERVREG HANDLE */
         sr_mon_handle = servreg_get_sr_mon_handle(service_name);
         if(SERVREG_NULL != sr_mon_handle)
         {
            result = servreg_set_ack(sr_mon_handle, transaction_id);

            if(SERVREG_SUCCESS != result)
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() servreg_set_ack() failed");
               ERR_FATAL("SERVREG_QDI: in servreg_qdi_free_remote_handle() servreg_set_ack() failed ", 0, 0, 0);
               ret = -1;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() sr_mon_handle NULL");
            ERR_FATAL("SERVREG_QDI: in servreg_qdi_free_remote_handle() sr_mon_handle NULL ", 0, 0, 0);
            ret = -1;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() qurt_qdi_copy_from_user() failed");
         ERR_FATAL("SERVREG_QDI: in servreg_qdi_free_remote_handle() qurt_qdi_copy_from_user() failed ", 0, 0, 0);
      }

      servreg_free(service_name);
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_free_remote_handle() malloc failed");
      ERR_FATAL("SERVREG_QDI: in servreg_qdi_free_remote_handle() malloc failed", 0, 0, 0);
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_get_remote_handle
 *
 * Description: 
 *     Function checks which notifier handle's state changed
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     remote_addr    : address of the monitor handle address who's state change
 *     remote_state   : state of the monitor handle who's state changed
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_get_remote_handle(int client_handle, uint32_t pid, uint32_t * remote_addr, uint32_t *remote_state, uint32_t *remote_transaction_id)
{
   int ret = -1;
   SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle = SERVREG_NULL;
   SERVREG_SERVICE_STATE state;
   uint32_t addr, transaction_id;
   servnotif_qdi_device_data_t *dataobj;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);
   dataobj = &servreg_qdi_clients_data[pid]; 

   sr_qdi_notif_handle = servreg_get_qdi_notif_node_state_change(&dataobj->servreg_qdi_notif_node_list_head);
   if(SERVREG_NULL != sr_qdi_notif_handle)
   {
      /* Send the address of the root handle to user space */
      addr = (uint32_t)sr_qdi_notif_handle;
      ret = qurt_qdi_copy_to_user(client_handle, remote_addr, &addr, sizeof(uint32_t));
      if (-1 != ret)
      {
         /* Send the state of the remote handle to user space */
         state = servreg_notif_get_curr_state(sr_qdi_notif_handle);
         ret = qurt_qdi_copy_to_user(client_handle, remote_state, &state, sizeof(uint32_t));
         if (-1 != ret)
         {
            /* Send the transaction id of the remote handle to user space */
            sr_mon_handle = servreg_notif_get_mon_handle(sr_qdi_notif_handle);
            transaction_id = servreg_get_transaction_id(sr_mon_handle);
            ret = qurt_qdi_copy_to_user(client_handle, remote_transaction_id, &transaction_id, sizeof(uint32_t));

            if(-1 == ret)
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_get_remote_handle() qurt_qdi_copy_to_user failed");
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_get_remote_handle() qurt_qdi_copy_to_user failed");
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_get_remote_handle() qurt_qdi_copy_to_user failed");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_get_remote_handle() sr_qdi_notif_handle is null");
   }
   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_create_qmi_entry
 *
 * Description: 
 *     Set the ack count for the user pd proxy listener
 *
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.
 *     pid            : user pd id
 *     name           : service name that the user-pd is wants to de-register
 *     name_len       : length of the name
 *
 * Returns:
 *     SERVREG_SUCCESS (0) or SERVREG_FAILURE(-1)
 * =====================================================================  */
static int servreg_qdi_create_qmi_entry(int client_handle, uint32_t pid, SERVREG_NAME name, uint32_t name_len, uint32_t qmi_instance_id)
{
   int ret = -1;
   SERVREG_NAME service_name = SERVREG_NULL;
   uint32_t sr_len = name_len;
   //servnotif_qdi_device_data_t *dataobj;

   //qurt_pimutex_lock(&servnotif_qdi_mutex);
   //dataobj = &servreg_qdi_clients_data[pid]; 

   service_name = servreg_malloc(sr_len);
   if(SERVREG_NULL != service_name)
   {
      /* Get the service name from user space */
      ret = qurt_qdi_copy_from_user(client_handle, service_name, name, sr_len);
      if (-1 != ret)
      {
         servreg_create_qmi_table_entry(service_name, qmi_instance_id);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_create_qmi_entry() qurt_qdi_copy_from_user() failed");
         ERR_FATAL("SERVREG_QDI: in servreg_qdi_create_qmi_entry() qurt_qdi_copy_from_user() failed ", 0, 0, 0);
      }

      servreg_free(service_name);
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: in servreg_qdi_create_qmi_entry() malloc failed");
      ERR_FATAL("SERVREG_QDI: in servreg_qdi_create_qmi_entry() malloc failed", 0, 0, 0);
   }

   //qurt_pimutex_unlock(&servnotif_qdi_mutex);
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_invoke
 *
 * Description: 
 *     This gives the canonical form for the arguments to a QDI
 *     driver invocation function.
 * 
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.  
 *
 *     qurt_qdi_obj_t : Points at the qdi_object_t structure
 *                      on which this QDI request is being made.
 *
 *     int method     : The integer QDI method which represents
 *                      the request type.
 *
 *     qurt_qdi_arg_t a1 to a3 :  The first three general purpose arguments
 *                                to the invocation function are passed in
 *                                these slots.
 *
 *     qurt_qdi_arg_t a4 to a9 :   Arguments beyond the first three are
 *                                 passed on the stack.
 * Returns:
 *     -1 for failure and 0 for success
 * =====================================================================  */
int servreg_qdi_invoke(int client_handle,
                       qurt_qdi_obj_t* obj,
                       int servnotif_qdi_method,
                       qurt_qdi_arg_t a1,
                       qurt_qdi_arg_t a2,
                       qurt_qdi_arg_t a3,
                       qurt_qdi_arg_t a4,
                       qurt_qdi_arg_t a5,
                       qurt_qdi_arg_t a6,
                       qurt_qdi_arg_t a7,
                       qurt_qdi_arg_t a8,
                       qurt_qdi_arg_t a9
                      )
{
   int ret = -1;
   char task_name[QURT_MAX_NAME_LEN];
   servnotif_qdi_device_t *clientobj = (servnotif_qdi_device_t *)obj;
   uint32_t pid = clientobj->pid;

   qurt_thread_get_name(task_name, sizeof(task_name));

   /* Driver Methods */
   switch (servnotif_qdi_method)
   {
      /* API, servreg_qdi_client_init */
      case QDI_OPEN:
      {
         ret = servreg_qdi_open(client_handle, a2.ptr, a3.num);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device open %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_close */
      case QDI_CLOSE:
      {
         if(SERVREG_SUCCESS != servreg_qdi_close(client_handle, pid))
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_QDI: servreg_qdi_close() failed");
         }

         ret = qurt_qdi_method_default(client_handle, obj, servnotif_qdi_method, a1, a2, a3, a4, a5, a6, a7, a8, a9);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device close %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_worker_wait */
      case SERVREG_QDI_WORKER_WAIT:
      {
         ret = servreg_qdi_worker_wait(client_handle, pid);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_worker_wait() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_register_remote_listener */
      case SERVREG_QDI_REG_REMOTE_LISTENER:
      {
         ret = servreg_qdi_register_remote_listener(client_handle, pid, a1.ptr, a2.num, a3.ptr, a4.ptr, a5.ptr);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_register_remote_listener() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_deregister_remote_listener */
      case SERVREG_QDI_DEREG_REMOTE_LISTENER:
      {
         ret = servreg_qdi_deregister_remote_listener(client_handle, pid, a1.ptr, a2.num);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_deregister_remote_listener() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_free_remote_handle */
      case SERVREG_QDI_FREE_REMOTE_HANDLE:
      {
         ret = servreg_qdi_free_remote_handle(client_handle, pid, a1.ptr, a2.num);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_free_remote_handle() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_set_remote_ack */
      case SERVREG_QDI_SET_REMOTE_ACK:
      {
         ret = servreg_qdi_set_remote_ack(client_handle, pid, a1.ptr, a2.num, a3.num);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_set_remote_ack() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_get_remote_handle */
      case SERVREG_QDI_GET_REMOTE_HANDLE:
      {
         ret = servreg_qdi_get_remote_handle(client_handle, pid, a1.ptr, a2.ptr, a3.ptr);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_get_remote_handle() method %s == %d", task_name, ret);
         break;
      }

      /* API, servreg_qdi_invoke_create_qmi_entry */
      case SERVREG_QDI_CREATE_QMI_ENTRY:
      {
         ret = servreg_qdi_create_qmi_entry(client_handle, pid, a1.ptr, a2.num, a3.num);
         MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device servreg_qdi_create_qmi_entry() method %s == %d", task_name, ret);
         break;
      }

      /* Default Method Handler */
      default:
      {
         ret = qurt_qdi_method_default(client_handle, obj, servnotif_qdi_method, a1, a2, a3, a4, a5, a6, a7, a8, a9);
         break; 
      }
   } /* switch() */

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_qdi_init
 *
 * Description:
 *     Initialization function. Registers a QDI device with the generic QDI object
 *
 * Parameters:
 *     None
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qdi_init(void)
{
   int ret = -1, i = 0;
   qurt_qdi_obj_t *opener = &servnotif_qdiobj;

   qurt_pimutex_init(&servnotif_qdi_mutex);

   qurt_pimutex_lock(&servnotif_qdi_mutex);

   opener->invoke = servreg_qdi_invoke;
   opener->refcnt = QDI_REFCNT_INIT;
   opener->release = servreg_qdi_release;

   for(i = 0; i < SERVREG_QDI_MAX_PDS; i++)
   {
      servreg_pid_table[i].pid_connected = SERVREG_FALSE;
      servreg_pid_table[i].pid = i;
   }

   /* Register the Driver */
   ret = qurt_qdi_devname_register(SERVREG_QDI_DEVICE, opener);
   if(ret == 0)
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_QDI: device = /dev/servnotif registered successfully");
   }
   else
   {
      ERR_FATAL( "SERVREG_QDI: qurt_qdi_devname_register failed", 0, 0, 0);
   }

   qurt_pimutex_unlock(&servnotif_qdi_mutex);
}


