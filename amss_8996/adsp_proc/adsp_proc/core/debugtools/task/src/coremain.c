/** vi: tw=128 ts=3 sw=3 et
@file coremain.c
@brief This file contains the API details for the COREMAIN/MAIN
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2015 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.adsp/2.7/debugtools/task/src/coremain.c#2 $
$DateTime: 2015/04/27 16:08:36 $
$Change: 7990390 $
$Author: pwbldsvc $
===============================================================================*/

#include "rcinit_dal.h"
#include "qurt.h"
#include "err.h"

void bootstrap_exception_handler(void); // forward reference
void bootstrap_rcinit_task(void); // forward reference

/**
API, coremain_main
@return None.
*/
void coremain_main(void)
{
   DALSYS_InitMod(NULL);               // no error return

   bootstrap_exception_handler();      // creates context for error processing

   bootstrap_rcinit_task();            // creates context for rcinit processing
}

#if !defined(TASK_EXCLUDE_MAIN)
/**
API, main
@return None.
*/
int main(void)
{
   DALSYSEventHandle hEvent;

   coremain_main(); // when main is not delivered here, this is the entry to start cbsp

   // Block Forever

   if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_NORMAL, &hEvent, NULL))
   {
      ERR_FATAL("DALSYS_EventCreate in main() has unexpectedly failed.", 0, 0, 0);
   }

   if (DAL_SUCCESS != DALSYS_EventWait(hEvent))
   {
      ERR_FATAL("DALSYS_EventWait in main() has unexpectedly failed.", 0, 0, 0);
   }

   ERR_FATAL("DALSYS_EventWait in main() has unexpectedly returned.", 0, 0, 0);

   /* NOTREACHED */

   return (0);
}
#endif

