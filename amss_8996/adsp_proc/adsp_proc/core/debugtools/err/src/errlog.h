#ifndef ERRLOG_H
#define ERRLOG_H
/*===========================================================================

                   L O G  P A C K E T S  F O R  E R R

DESCRIPTION
  This header file contains the definitions of log structure for core dump
 
Copyright (c) 2007 - 2015   by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.adsp/2.7/debugtools/err/src/errlog.h#5 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/15   abh     Added changes for extended smem logging
07/25/07   tbg     Initial revision (pulled code from rexlog.h)

===========================================================================*/


/* -----------------------------------------------------------------------
** Includes 
** ----------------------------------------------------------------------- */
#include "comdef.h"
#ifdef ERR_HW_ARMV8
#include "errlog_armv8.h"
#elif ERR_HW_QDSP6
#include "errlog_q6.h"
#endif 

#include "DDITimetick.h"
#include "err.h"

#include "erri_sizes.h"   /* For err specific size Macros */

/* -----------------------------------------------------------------------
** MACROs
** ----------------------------------------------------------------------- */

#ifndef QURT_THREAD_ATTR_NAME_MAXLEN
#define QURT_THREAD_ATTR_NAME_MAXLEN 16
#endif /* QURT_THREAD_ATTR_NAME_MAXLEN */

/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */

typedef enum
{
  ERR_ARCH_UNKNOWN = 0,
  ERR_ARCH_ARM,
  ERR_ARCH_QDSP6,
  SIZEOF_ERR_ARCH_TYPE
} err_arch_type;

typedef enum
{
  ERR_OS_UNKNOWN = 0,
  ERR_OS_REX,
  ERR_OS_L4, 
  ERR_OS_BLAST, 
  ERR_OS_QURT, 
  SIZEOF_ERR_OS_TYPE
} err_os_type;

/* Defines for stack undeflow /overflow */
#define STACK_NO_ERROR      (1<<0)
#define STACK_OVERFLOW      (1<<1)
#define STACK_UNDERFLOW     (1<<2)
#define STACK_ERR_OBTAINING (1<<3)

#define NUM_QDSP6_GP_REGS 32

typedef struct
{
  err_arch_type type;
  uint32 version;
  union arch_coredump_union regs;
} arch_coredump_type;

/* update this version whenever QDSP6 arch_coredump_type changes */
#define ERR_ARCH_COREDUMP_VER  2
#define ERR_ARCH_COREDUMP_TYPE ERR_ARCH_QDSP6
#define ERR_ARCH_QDSP_INUSE
 
/************************************************************************
 *                        OS_COREDUMP_TYPES
 ************************************************************************/

/****************
 *    QURT
 ****************/

#define ERR_OS_TCB_TYPE void

typedef struct
{
  err_os_type type;
  uint32 version;  
  ERR_OS_TCB_TYPE* tcb_ptr; 
} os_coredump_type;

/* update this version whenever L4 os_coredump_type changes */
#define ERR_OS_COREDUMP_VER  1
#define ERR_OS_COREDUMP_TYPE ERR_OS_QURT

/************************************************************************
 *                         ERR_COREDUMP_TYPE
 ************************************************************************/

#define ERR_LOG_NUM_PARAMS 3

typedef struct
{
  err_cb_ptr            err_cb;
  boolean               cb_start;
  DalTimetickTime64Type cb_start_tick;
} err_cb_preflush_external_type;

typedef struct
{
  char qc_image_version_string[ERR_IMAGE_VERSION_SIZE];
  char image_variant_string[ERR_IMAGE_VERSION_SIZE];
  char oem_image_version_string[ERR_IMAGE_VERSION_SIZE];
  char oem_image_uuid_string[ERR_IMAGE_VERSION_SIZE];
} image_coredump_type;

typedef struct
{
  uint32                          version;
  uint32                          linenum;
  uint64                          timestamp;
  uint64                          uptime;
  DalTimetickTime64Type           err_handler_start_time;
  DalTimetickTime64Type           err_handler_end_time;
  char                            filename[ERR_LOG_MAX_FILE_LEN];
  char                            message[ERR_LOG_MAX_MSG_LEN];
  uint32                          param[ERR_LOG_NUM_PARAMS];
  char                            aux_msg[ERR_LOG_MAX_MSG_LEN];
  err_cb_preflush_external_type   err_next_to_STM;
  err_cb_preflush_external_type   err_preflush_external[ERR_MAX_PREFLUSH_CB+1];
  err_cb_preflush_external_type   err_postflush_external;
  err_cb_ptr                      err_current_cb;
  uint32                          stack_error;
  uint32                          crumb_trail_bmsk;
  const err_const_type *          compressed_ptr;
  unsigned int                    tid;
  unsigned int                      pd_id;
  char                            tcb_name[QURT_THREAD_ATTR_NAME_MAXLEN];
  boolean                         is_exception;
} err_coredump_type;

/* update this version whenever err_coredump_type changes */
#define ERR_COREDUMP_VER  4

/************************************************************************
 *                         ERR_TRACE_TYPE
 ************************************************************************/

#define ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_PRE                    (1<<0)
#define ERR_CRUMB_TRAIL_BMSK_EXCEPTION_PRE                    (1<<1)
#define ERR_CRUMB_TRAIL_BMSK_JETTISON_CORE_POST               (1<<2)
#define ERR_CRUMB_TRAIL_BMSK_OVERRIDE_PRE                     (1<<3)
#define ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_RAISE_EXCEPTION_PRE    (1<<4)
#define ERR_CRUMB_TRAIL_BMSK_CACHCE_CLEAN_PRE                 (1<<5)
#define ERR_CRUMB_TRAIL_BMSK_DECOMPRESS_MSG_PRE               (1<<6)
#define ERR_CRUMB_TRAIL_BMSK_F3_TRACE_PRE                     (1<<7)
#define ERR_CRUMB_TRAIL_BMSK_COREDUMP_UPDATED_PRE             (1<<8)
#define ERR_CRUMB_TRAIL_BMSK_SMEM_LOG_PRE                     (1<<9)
#define ERR_CRUMB_TRAIL_BMSK_STORE_INFO_PRE                   (1<<10)
#define ERR_CRUMB_TRAIL_BMSK_FORMAT_MESSAGE_PRE               (1<<11)
#define ERR_CRUMB_TRAIL_BMSK_LOG_SSR_PRE                      (1<<12)
#define ERR_CRUMB_TRAIL_BMSK_LOG_EXTERNEL_CBS_PRE             (1<<13)
#define ERR_CRUMB_TRAIL_BMSK_RAISE_TO_KERNEL_PRE              (1<<14)
#define ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_JETTISION_CORE_POST    (1<<15)
#define ERR_CRUMB_TRAIL_BMSK_EMERGENCY_RECOVERY_PRE           (1<<16)
#define ERR_CRUMB_TRAIL_BMSK_DYNAMIC_CALLBACK_PRE             (1<<17)
#define ERR_CRUMB_TRAIL_BMSK_FATAL_NOTIFY_INVOKED             (1<<18)

/************************************************************************
 *                           COREDUMP_TYPE
 ************************************************************************/


typedef struct
{
  uint32 version;
  arch_coredump_type arch;
  os_coredump_type os;
  err_coredump_type err;
  image_coredump_type image;
} coredump_type;

 /* update this version whenever coredump_type changes */
#define ERR_COREDUMP_VERSION  2
 
/************************************************************************
 *                           FATAL_PARAM_TYPE
 ************************************************************************/
typedef unsigned long err_param_t;

typedef struct {
  unsigned int line;     /* __LINE__ */
  const char *file_name; /* __FILENAME__ */
  const char *format;    /* printf format err message */
  err_param_t param1;    /* printf arg1 */
  err_param_t param2;    /* printf arg2 */
  err_param_t param3;    /* printf arg3 */
  ERR_OS_TCB_TYPE  * tcb;
  const err_const_type* msg_const_ptr; /* orignal (compressed) msg_const */
  uint32 array[SIZEOF_ARCH_COREDUMP_REGISTERS];
  uint32 crumb_trail_bmsk;
} err_fatal_params_type;

/************************************************************************
 *                           MESSAGE TYPES
 ************************************************************************/

#define ERR_LOG_MSG(str) MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, str);
#define ERR_LOG_MSG_ERROR(str) MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, str);
#define ERR_LOG_MSG_FATAL_1(str, x) MSG_1(MSG_SSID_TMS, MSG_LEGACY_FATAL, str, x);


/*===========================================================================
FUNCTION ERR_LOG_INIT

DESCRIPTION
  Initialize buffer to be used for writing log information.
  Calling the function subsequent times will reset the location and size
  of the buffer.

PARAMETERS
  char *buff     Pointer to buffer
  uint32 size    Size of buffer

RETURNS
 
============================================================================*/
void err_log_init(char *buff, uint32 size);

/*===========================================================================
FUNCTION ERR_GENERATE_LOG

DESCRIPTION
  This is the function called by error services when a log report needs to
  be generated.

  err_log_init() must be called prior to calling this function.

PARAMETERS
  None

RETURNS 
  uint32          Number of bytes written to buffer.

============================================================================*/

uint32 err_generate_log(void);


/*===========================================================================
MACRO ERR_CRUMB_TRAIL_BMSK

DESCRIPTION
  Used to log state of err_handling

PARAMETERS
  None

RETURNS 
  uint32          Number of bytes written to buffer.

============================================================================*/
extern coredump_type coredump;
extern err_fatal_params_type err_fatal_params;

#define ERR_CRUMB_TRAIL_BMSK(x)\
do {\
    (coredump.err.crumb_trail_bmsk |= (x));\
    asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &(coredump.err.crumb_trail_bmsk) )));\
} while (0)

#define ERR_CRUMB_TRAIL_BMSK_ISLAND(x)\
do {\
    err_fatal_params.crumb_trail_bmsk |= (x);\
} while (0)

#endif /* ERRLOG_H */
