# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 14 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_types.h" 1
# 27 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_types.h"
typedef unsigned char boolean;




typedef unsigned long int uint32;




typedef signed long int int32;




typedef unsigned long long uint64;




typedef signed long long int64;




typedef unsigned char byte;




typedef signed char int8;




typedef unsigned long int bool32;




typedef unsigned char uint8;
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_defs.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned short uint16;
# 48 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef signed short int16;
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 1
# 27 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdErr.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 1
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};

typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};







typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};

typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;

};

struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};

typedef struct DalDeviceHandle * DALDEVICEHANDLE;

typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};

struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};






static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}

static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}

static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}

static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}

static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}

static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}

static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}

static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
# 218 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);






DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);





DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
# 314 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 2


typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};


typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;

typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};


typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};


typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};

typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};

typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};







typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};

typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};

typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);


typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};

typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;

typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;

typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;


typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};






typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;

    DALSYSMemObj *pObj;

    DALBOOL prealloc;
};

typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
# 243 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);

typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);

typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);

typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);

typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);

typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);

typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);

typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);

typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);

typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);

typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);

typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);

typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);

typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);

typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);

typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);

typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);

typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);

typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);

typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};


typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);

typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};

typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 10 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 11 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 2 3 4
# 29 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
typedef _Sizet size_t;




int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));

void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));


char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));

void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
# 106 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
# 130 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 227 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitMod(DALSYSConfig * pCfg);
# 238 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_DeInitMod(void);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
# 279 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
# 310 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 334 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 358 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
# 394 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
# 415 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
# 434 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
# 458 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
# 473 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
# 492 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
# 508 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
# 519 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
# 589 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
# 634 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
# 698 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
# 712 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
# 758 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Free(void *pmem);
# 771 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_BusyWait(uint32 pause_time_us);
# 786 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 802 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 833 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
# 865 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 882 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
# 900 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
# 913 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
# 927 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 DALSYS_SetThreadPriority(uint32 priority);
# 943 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 2





typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;

struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};

struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};

struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};

struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};

typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};

typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};

typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);

DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);

uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);

uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);


void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);

void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);

DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);






DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);

DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);

DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);

DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);

void memory_barrier(void);
# 232 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
# 284 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);






DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
# 322 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);






uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);






uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);







uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
# 370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
# 379 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
# 392 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);







void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
# 409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);






typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;






void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);






void DALFW_MPUnLock(DALFW_MPLOCK *pLock);






uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h" 2
# 61 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
typedef struct coreUtils_Q_LinkStruct
{
  struct coreUtils_Q_LinkStruct *pNext;


  struct coreUtils_Q_LinkStruct *pPrev;

} coreUtils_Q_LinkType;
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
typedef struct coreUtils_Q_HeadLinkStruct
{
  struct coreUtils_Q_LinkStruct *pNext;


  struct coreUtils_Q_LinkStruct *pPrev;


} coreUtils_Q_HeadLinkType;
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
typedef struct coreUtils_Q_Struct
{
  coreUtils_Q_LinkType link;


  int nCnt;


} coreUtils_Q_Type;







typedef struct {
   coreUtils_Q_LinkType link;
} coreUtils_Q_GenericItemType;
# 128 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
typedef int (*coreUtils_Q_CompareFuncType)(void* pItem, void *pCompareVal);
# 137 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
typedef void (*coreUtils_Q_ActionFuncType)(void *pItem, void *pParam);
# 182 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
coreUtils_Q_Type* coreUtils_Q_Init ( coreUtils_Q_Type *q_ptr );
# 203 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
coreUtils_Q_LinkType* coreUtils_Q_Link ( void *pItem, coreUtils_Q_LinkType *pLink );
# 226 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void coreUtils_Q_Put ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_Get ( coreUtils_Q_Type *pQ );
# 270 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_LastGet ( coreUtils_Q_Type *pQ );
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
int coreUtils_Q_Cnt ( coreUtils_Q_Type *pQ );
# 315 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_Check ( coreUtils_Q_Type *pQ );
# 338 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_LastCheck ( coreUtils_Q_Type *pQ );
# 360 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_Next(
   coreUtils_Q_Type *pQ,
   coreUtils_Q_LinkType *pLink
);
# 384 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void coreUtils_Q_Insert ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLinkToInsert, coreUtils_Q_LinkType *pLinkPrev);
# 405 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void coreUtils_Q_Delete ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
# 427 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
boolean coreUtils_Q_DeleteExt ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
# 456 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void* coreUtils_Q_LinearSearch(
  coreUtils_Q_Type *pQ,
  coreUtils_Q_CompareFuncType compareFunc,
  void *pCompareVal
);
# 491 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/inc/coreUtils.h"
void coreUtils_Q_LinearDelete(
  coreUtils_Q_Type *pQ,
  coreUtils_Q_CompareFuncType compareFunc,
  void *pParam,
  coreUtils_Q_ActionFuncType actionFunc
);
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdio.h" 1 3 4
# 60 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdio.h" 3 4
typedef struct _Mbstatet
 {
 unsigned long _Wchar;
 unsigned short _Byte, _State;
 } _Mbstatet;
# 77 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdio.h" 3 4
typedef struct fpos_t
 {
 _Longlong _Off;
 _Mbstatet _Wstate;
 } fpos_t;



struct _Dnk_filet
 {
 unsigned short _Mode;
 unsigned char _Idx;
 int _Handle;

 unsigned char *_Buf, *_Bend, *_Next;
 unsigned char *_Rend, *_Wend, *_Rback;

 _Wchart *_WRback, _WBack[2];
 unsigned char *_Rsave, *_WRend, *_WWend;

 _Mbstatet _Wstate;
 char *_Tmpnam;
 unsigned char _Back[8], _Cbuf;
 };



typedef struct _Dnk_filet _Filet;


typedef _Filet FILE;



extern FILE _Stdin, _Stdout, _Stderr;

void clearerr(FILE *) __attribute__((__nothrow__));
int fclose(FILE *) __attribute__((__nothrow__));
int feof(FILE *) __attribute__((__nothrow__));
int ferror(FILE *) __attribute__((__nothrow__));
int fflush(FILE *) __attribute__((__nothrow__));
int fgetc(FILE *) __attribute__((__nothrow__));
int fgetpos(FILE *, fpos_t *) __attribute__((__nothrow__));
char *fgets(char *, int, FILE *) __attribute__((__nothrow__));
FILE *fopen(const char *, const char *) __attribute__((__nothrow__));





int fprintf(FILE *, const char *, ...) __attribute__((__nothrow__));
int fputc(int, FILE *) __attribute__((__nothrow__));
int fputs(const char *, FILE *) __attribute__((__nothrow__));
size_t fread(void *, size_t, size_t, FILE *) __attribute__((__nothrow__));
FILE *freopen(const char *, const char *,
 FILE *) __attribute__((__nothrow__));





int fscanf(FILE * , const char *, ...) __attribute__((__nothrow__));
int fseek(FILE *, long, int) __attribute__((__nothrow__));
int fsetpos(FILE *, const fpos_t *) __attribute__((__nothrow__));
long ftell(FILE *) __attribute__((__nothrow__));
size_t fwrite(const void *, size_t, size_t,
 FILE *) __attribute__((__nothrow__));
char *gets(char *) __attribute__((__nothrow__));
void perror(const char *) __attribute__((__nothrow__));

int fseeko (FILE *, long, int) __attribute__((__nothrow__));
long ftello (FILE *) __attribute__((__nothrow__));
int getchar_unlocked (void) __attribute__((__nothrow__));
int getc_unlocked (FILE *) __attribute__((__nothrow__));
int putchar_unlocked (int) __attribute__((__nothrow__));
int putc_unlocked (int, FILE *) __attribute__((__nothrow__));





int printf(const char *, ...) __attribute__((__nothrow__));
int puts(const char *) __attribute__((__nothrow__));
int remove(const char *) __attribute__((__nothrow__));
int rename(const char *, const char *) __attribute__((__nothrow__));
void rewind(FILE *) __attribute__((__nothrow__));





int scanf(const char *, ...) __attribute__((__nothrow__));
void setbuf(FILE * , char *) __attribute__((__nothrow__));
int setvbuf(FILE * , char *, int, size_t) __attribute__((__nothrow__));





int sprintf(char *, const char *, ...) __attribute__((__nothrow__));





int sscanf(const char *, const char *, ...) __attribute__((__nothrow__));
FILE *tmpfile(void) __attribute__((__nothrow__));
char *tmpnam(char *) __attribute__((__nothrow__));
int ungetc(int, FILE *) __attribute__((__nothrow__));
int vfprintf(FILE *, const char *, _Va_list) __attribute__((__nothrow__));
int vprintf(const char *, _Va_list) __attribute__((__nothrow__));
int vsprintf(char *, const char *, _Va_list) __attribute__((__nothrow__));


FILE *fdopen(int, const char *) __attribute__((__nothrow__));
int fileno(FILE *) __attribute__((__nothrow__));
int getw(FILE *) __attribute__((__nothrow__));
int putw(int, FILE *) __attribute__((__nothrow__));


long _Fgpos(FILE *, fpos_t *) __attribute__((__nothrow__));
int _Flocale(FILE *, const char *, int) __attribute__((__nothrow__));
void _Fsetlocale(FILE *, int) __attribute__((__nothrow__));
int _Fspos(FILE *, const fpos_t *, long, int) __attribute__((__nothrow__));


void _Lockfilelock(_Filet *) __attribute__((__nothrow__));
void _Unlockfilelock(_Filet *) __attribute__((__nothrow__));


extern FILE *_Files[20];







int snprintf(char *, size_t,
 const char *, ...) __attribute__((__nothrow__));
int vsnprintf(char *, size_t,
 const char *, _Va_list) __attribute__((__nothrow__));
int vfscanf(FILE *,
 const char *, _Va_list) __attribute__((__nothrow__));
int vscanf(const char *, _Va_list) __attribute__((__nothrow__));
int vsscanf(const char *,
 const char *, _Va_list) __attribute__((__nothrow__));
# 245 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdio.h" 3 4
int getc(FILE *) __attribute__((__nothrow__));
int getchar(void) __attribute__((__nothrow__));
int putc(int, FILE *) __attribute__((__nothrow__));
int putchar(int) __attribute__((__nothrow__));
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 1 3 4
# 38 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 3 4
typedef _Mbstatet mbstate_t;
# 50 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 3 4
struct tm;
struct _Dnk_filet;
# 61 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 3 4
typedef _Wchart wchar_t;




typedef _Wintt wint_t;




wint_t fgetwc(_Filet *) __attribute__((__nothrow__));
wchar_t *fgetws(wchar_t *, int,
 _Filet *) __attribute__((__nothrow__));
wint_t fputwc(wchar_t, _Filet *) __attribute__((__nothrow__));
int fputws(const wchar_t *,
 _Filet *) __attribute__((__nothrow__));
int fwide(_Filet *, int) __attribute__((__nothrow__));
int fwprintf(_Filet *,
 const wchar_t *, ...) __attribute__((__nothrow__));
int fwscanf(_Filet *,
 const wchar_t *, ...) __attribute__((__nothrow__));
wint_t getwc(_Filet *) __attribute__((__nothrow__));
wint_t getwchar(void) __attribute__((__nothrow__));
wint_t putwc(wchar_t, _Filet *) __attribute__((__nothrow__));
wint_t putwchar(wchar_t) __attribute__((__nothrow__));
int swprintf(wchar_t *, size_t,
 const wchar_t *, ...) __attribute__((__nothrow__));
int swscanf(const wchar_t *,
 const wchar_t *, ...) __attribute__((__nothrow__));
wint_t ungetwc(wint_t, _Filet *) __attribute__((__nothrow__));
int vfwprintf(_Filet *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vswprintf(wchar_t *, size_t,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vwprintf(const wchar_t *, _Va_list) __attribute__((__nothrow__));
int wprintf(const wchar_t *, ...) __attribute__((__nothrow__));
int wscanf(const wchar_t *, ...) __attribute__((__nothrow__));


int vfwscanf(_Filet *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vswscanf(const wchar_t *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vwscanf(const wchar_t *, _Va_list) __attribute__((__nothrow__));



size_t mbrlen(const char *,
 size_t, mbstate_t *) __attribute__((__nothrow__));
size_t mbrtowc(wchar_t *, const char *,
 size_t, mbstate_t *) __attribute__((__nothrow__));
size_t mbsrtowcs(wchar_t *,
 const char **, size_t, mbstate_t *) __attribute__((__nothrow__));
int mbsinit(const mbstate_t *) __attribute__((__nothrow__));
size_t wcrtomb(char *,
 wchar_t, mbstate_t *) __attribute__((__nothrow__));
size_t wcsrtombs(char *,
 const wchar_t **, size_t, mbstate_t *) __attribute__((__nothrow__));
long wcstol(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));


_Longlong wcstoll(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));
_ULonglong wcstoull(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));



wchar_t *wcscat(wchar_t *, const wchar_t *) __attribute__((__nothrow__));
int wcscmp(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcscpy(wchar_t *, const wchar_t *) __attribute__((__nothrow__));
size_t wcslen(const wchar_t *) __attribute__((__nothrow__));
int wcsncmp(const wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wcsncpy(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));

int wcscoll(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
size_t wcscspn(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcsncat(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
size_t wcsspn(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcstok(wchar_t *, const wchar_t *,
 wchar_t **) __attribute__((__nothrow__));
size_t wcsxfrm(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
int wmemcmp(const wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemcpy(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemmove(wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemset(wchar_t *, wchar_t, size_t) __attribute__((__nothrow__));


size_t wcsftime(wchar_t *, size_t,
 const wchar_t *, const struct tm *) __attribute__((__nothrow__));

wint_t _Btowc(int) __attribute__((__nothrow__));
int _Wctob(wint_t) __attribute__((__nothrow__));
double _WStod(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
float _WStof(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
long double _WStold(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
unsigned long _WStoul(const wchar_t *, wchar_t **, int) __attribute__((__nothrow__));
# 184 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 3 4
wchar_t *wmemchr(const wchar_t *, wchar_t, size_t);




# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/xwcstod.h" 1 3 4
# 22 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/xwcstod.h" 3 4
double wcstod(const wchar_t *, wchar_t **);
unsigned long wcstoul(const wchar_t *, wchar_t **, int);
# 189 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 2 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/xwstr.h" 1 3 4
# 61 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/xwstr.h" 3 4
wchar_t *wcschr(const wchar_t *, wchar_t);
wchar_t *wcspbrk(const wchar_t *, const wchar_t *);
wchar_t *wcsrchr(const wchar_t *, wchar_t);
wchar_t *wcsstr(const wchar_t *, const wchar_t *);
wint_t btowc(int);
int wctob(wint_t);


float wcstof(const wchar_t *,
 wchar_t **);
long double wcstold(const wchar_t *,
 wchar_t **);
# 190 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/wchar.h" 2 3 4
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h" 2
# 70 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
typedef unsigned short wchar;
# 108 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t strlcat(char *dst, const char *src, size_t siz);
# 137 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t wcslcat(wchar_t *dst, const wchar_t *src, size_t siz);
# 160 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t wstrlcat(wchar* dst, const wchar* src, size_t siz);
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t strlcpy(char *dst, const char *src, size_t siz);
# 209 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t wcslcpy(wchar_t *dst, const wchar_t *src, size_t siz);
# 231 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t wstrlcpy(wchar* dst, const wchar* src, size_t siz);
# 246 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t wstrlen(const wchar *src);
# 269 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int wstrcmp(const wchar *s1, const wchar *s2);
# 294 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int wstrncmp(const wchar *s1, const wchar *s2, size_t n);
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int strcasecmp(const char * s1, const char * s2);
# 330 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int strncasecmp(const char * s1, const char * s2, size_t n);
# 395 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
unsigned int std_scanul(const char * pchBuf, int nRadix, const char ** ppchEnd, int *pnError);
# 422 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t memscpy(void *dst, size_t dst_size, const void *src, size_t src_size);







static __inline size_t memscpy_i
(
  void *dst,
  size_t dst_size,
  const void *src,
  size_t src_size
)
{
  size_t copy_size = (dst_size <= src_size)? dst_size : src_size;

  memcpy(dst, src, copy_size);

  return copy_size;
}
# 474 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t memsmove(void *dst, size_t dst_size, const void *src, size_t src_size);







static __inline size_t memsmove_i
(
  void *dst,
  size_t dst_size,
  const void *src,
  size_t src_size
)
{
  size_t copy_size = (dst_size <= src_size)? dst_size : src_size;

  memmove(dst, src, copy_size);

  return copy_size;
}
# 525 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
void* secure_memset(void* ptr, int value, size_t len);
# 556 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int timesafe_memcmp(const void* ptr1, const void* ptr2, size_t len);
# 587 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
int timesafe_strncmp(const char* ptr1, const char* ptr2, size_t len);
# 608 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/libstd/stringl/stringl.h"
size_t strnlen(const char *str, size_t maxlen);
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h" 2
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
typedef enum
{
     Adsppm_Api_Type_None,
     Adsppm_Api_Type_Sync,
     Adsppm_Api_Type_Async,
     Adsppm_Ape_Type_Enum_Max,
     Adsppm_Api_Type_Force8bits = 0x7F
} AdsppmApiType;




typedef enum
{
    Adsppm_Status_Success,
    Adsppm_Status_Failed,
    Adsppm_Status_NoMemory,
    Adsppm_Status_VersionNotSupport,
    Adsppm_Status_BadClass,
    Adsppm_Status_BadState,
    Adsppm_Status_BadParm,
    Adsppm_Status_InvalidFormat,
    Adsppm_Status_UnSupported,
    Adsppm_Status_ResourceNotFound,
    Adsppm_Status_BadMemPtr,
    Adsppm_Status_BadHandle,
    Adsppm_Status_ResourceInUse,
    Adsppm_Status_NoBandwidth,
    Adsppm_Status_NullPointer,
    Adsppm_Status_NotInitialized,
    Adsppm_Status_ResourceNotRequested,
    Adsppm_Status_CoreResourceNotAvailable,
    Adsppm_Status_Max,
    Adsppm_Status_Force32Bits = 0x7FFFFFFF
} AdsppmStatusType;

typedef AdsppmStatusType Adsppm_Status;




typedef enum
{
    Adsppm_Core_Id_None = 0,
    Adsppm_Core_Id_ADSP = 1,
    Adsppm_Core_Id_LPASS_Core = 2,
    Adsppm_Core_Id_LPM = 3,
    Adsppm_Core_Id_DML = 4,
    Adsppm_Core_Id_AIF = 5,
    Adsppm_Core_Id_SlimBus = 6,
    Adsppm_Core_Id_Midi = 7,
    Adsppm_Core_Id_AVsync = 8,
    Adsppm_Core_Id_HWRSMP = 9,
    Adsppm_Core_Id_SRam = 10,
    Adsppm_Core_Id_DCodec = 11,
    Adsppm_Core_Id_Spdif = 12,
    Adsppm_Core_Id_Hdmirx = 13,
    Adsppm_Core_Id_Hdmitx = 14,
    Adsppm_Core_Id_Sif = 15,
    Adsppm_Core_Id_BSTC = 16,
    Adsppm_Core_Id_HVX = 17,
    Adsppm_Core_Id_Max,
    Adsppm_Core_Id_Force8Bits = 0x7F
} AdsppmCoreIdType;




typedef enum
{
    Adsppm_Instance_Id_None = 0,
    Adsppm_Instance_Id_0 = 1,
    Adsppm_Instance_Id_1 = 2,
    Adsppm_Instance_Id_2 = 3,
    Adsppm_Instance_Id_Max,
    Adsppm_Instance_Id_Force8Bits = 0x7F
} AdsppmInstanceIdType;




typedef enum
{
    Adsppm_Rsc_Id_None = 0,
    Adsppm_Rsc_Id_Power = 1,
    Adsppm_Rsc_Id_Core_Clk = 2,
    Adsppm_Rsc_Id_Sleep_Latency = 3,
    Adsppm_Rsc_Id_Mips = 4,
    Adsppm_Rsc_Id_BW = 5,
    Adsppm_Rsc_Id_Thermal = 6,
    Adsppm_Rsc_Id_MemPower = 7,
    Adsppm_Rsc_Id_Core_Clk_Domain = 8,
    Adsppm_Rsc_Id_Max,
    Adsppm_Rsc_Id_Force8Bits = 0x7F
} AdsppmRscIdType;




typedef enum
{
    Adsppm_State_ACMInit = 0x1,
    Adspmm_State_HalIntrInit = 0x2,
    Adsppm_State_HalHwIoInit = 0x4,
    Adsppm_State_HalClkRgmInit = 0x8,
    Adsppm_State_HalBusInit = 0x10,
    Adsppm_State_HalSlpInit = 0x20,
    Adsppm_State_CoreCtxLockInit = 0x80,
    Adsppm_State_CoreRMInit = 0x100,
    Adsppm_State_CoreAMAsyncInit = 0x200,
    Adsppm_State_CoreMIPSInit = 0x400,
    Adsppm_State_CoreBUSInit = 0x800,
    Adsppm_State_CoreAHBMInit = 0x1000,
    Adsppm_State_CorePWRInit = 0x2000,
    Adsppm_State_CoreCLKInit = 0x4000,
    Adsppm_State_CoreSLEEPInit = 0x8000,
    Adsppm_State_CoreTHERMALInit = 0x10000,
    Adsppm_State_CoreMEMPWRInit = 0x20000,
    Adsppm_State_CoreCMInit = 0x40000,
    Adsppm_State_CoreCPMInit = 0x80000,
    Adsppm_State_CoreDCVSInit = 0x100000,
    Adsppm_State_CoreEXTBWInit = 0x200000,
    Adsppm_State_CoreADSPCLKInit = 0x400000,
    Adsppm_State_CoreQCMInit = 0x800000,
    Adsppm_State_CoreBMRegisterEvent = 0x1000000,
    Adsppm_State_Force32bit = 0x7fffffff
} AdsppmInitStateType;







typedef enum
{
    Adsppm_Callback_Event_Id_None,
    Adsppm_Callback_Event_Id_Thermal = 0x0002,
    Adsppm_Callback_Event_Id_Async_Complete = 0x0004,
    Adsppm_Callback_Event_Id_Idle,
    Adsppm_Callback_Event_Id_Busy,
    Adsppm_Callback_Event_Id_Max,
    Adsppm_Callback_Event_Id_Force32Bits = 0x7FFFFFFF
} AdsppmCallbackEventIdType;




typedef struct
{
    AdsppmCallbackEventIdType eventId;
    uint32 clientId;
    uint32 callbackDataSize;
    void *callbackData;

} AdsppmCallbackParamType;







typedef enum
{
    Adsppm_Thermal_NONE,
    Adsppm_Thermal_LOW,
    Adsppm_Thermal_NORM,
    Adsppm_Thermal_High_L1,
    Adsppm_Thermal_High_L2,
    Adsppm_Thermal_High_L3,
    Adsppm_Thermal_High_L4,
    Adsppm_Thermal_High_L5,
    Adsppm_Thermal_High_L6,
    Adsppm_Thermal_High_L7,
    Adsppm_Thermal_High_L8,
    Adsppm_Thermal_High_L9,
    Adsppm_Thermal_High_L10,
    Adsppm_Thermal_High_L11,
    Adsppm_Thermal_High_L12,
    Adsppm_Thermal_High_L13,
    Adsppm_Thermal_High_L14,
    Adsppm_Thermal_High_L15,
    Adsppm_Thermal_High_L16,
    Adsppm_Thermal_Max,
    Adsppm_Thermal_Force32Bits = 0x7FFFFFFF
} AdsppmThermalType;







typedef enum
{
    AdsppmClk_None = 0,
    AdsppmClk_Adsp_Core,

    AdsppmClk_Ahb_Root,
    AdsppmClk_AhbI_Hclk,
    AdsppmClk_AhbX_Hclk,
    AdsppmClk_HwRsp_Hclk,
    AdsppmClk_Dml_Hclk,
    AdsppmClk_Aif_Hclk,
    AdsppmClk_Aif_Csr_Hclk,
    AdsppmClk_Slimbus_Hclk,
    AdsppmClk_Slimbus_cbc,
    AdsppmClk_Slimbus2_Hclk,
    AdsppmClk_Slimbus2_cbc,
    AdsppmClk_Midi_Hclk,
    AdsppmClk_AvSync_Hclk,
    AdsppmClk_Atimer_Hclk,
    AdsppmClk_Lpm_Hclk,
    AdsppmClk_Lpm_cbc,
    AdsppmClk_Csr_Hclk,
    AdsppmClk_Dcodec_Hclk,
    AdsppmClk_Spdif_Hmclk,
    AdsppmClk_Spdif_Hsclk,
    AdsppmClk_Hdmirx_Hclk,
    AdsppmClk_Hdmitx_Hclk,
    AdsppmClk_Sif_Hclk,
    AdsppmClk_Bstc_Hclk,
    AdsppmClk_Smmu_Adsp_Hclk,
    AdsppmClk_Smmu_Lpass_Hclk,
    AdsppmClk_Sysnoc_Hclk,
    AdsppmClk_Sysnoc_cbc,
    AdsppmClk_Bus_Timeout_Hclk,
    AdsppmClk_Tlb_Preload_Hclk,
    AdsppmClk_Qos_Hclk,
    AdsppmClk_Qdsp_Sway_Hclk,


    AdsppmClk_AhbE_Hclk,
    AdsppmClk_Adsp_Hmclk,
    AdsppmClk_Adsp_Hsclk,
    AdsppmClk_Sram_Hclk,
    AdsppmClk_Lcc_Hclk,
    AdsppmClk_Security_Hclk,
    AdsppmClk_Wrapper_Security_Hclk,
    AdsppmClk_Wrapper_Br_Hclk,
    AdsppmClk_Audio_Core_AON,
    AdsppmClk_Audio_Wrapper_AON,


    AdsppmClk_HwRsp_Core,
    AdsppmClk_Midi_Core,
    AdsppmClk_AvSync_Xo,
    AdsppmClk_AvSync_Bt,
    AdsppmClk_AvSync_Fm,
    AdsppmClk_Slimbus_Core,
    AdsppmClk_Slimbus2_Core,
    AdsppmClk_Avtimer_core,
    AdsppmClk_Atime_core,
    AdsppmClk_Atime2_core,

    AdsppmClk_EnumMax,
    AdsppmClk_EnumForce32Bit = 0x7fffffff
} AdsppmClkIdType;







typedef enum
{
    AdsppmBusPort_None = 0,
    AdsppmBusPort_Adsp_Master,
    AdsppmBusPort_Dml_Master,
    AdsppmBusPort_Aif_Master,
    AdsppmBusPort_Slimbus_Master,
    AdsppmBusPort_Slimbus2_Master,
    AdsppmBusPort_Midi_Master,
    AdsppmBusPort_HwRsmp_Master,
    AdsppmBusPort_Ext_Ahb_Master,
    AdsppmBusPort_Spdif_Master,
    AdsppmBusPort_Hdmirx_Master,
    AdsppmBusPort_Hdmitx_Master,
    AdsppmBusPort_Sif_Master,
    AdsppmBusPort_Dml_Slave,
    AdsppmBusPort_Aif_Slave,
    AdsppmBusPort_Slimbus_Slave,
    AdsppmBusPort_Slimbus2_Slave,
    AdsppmBusPort_Midi_Slave,
    AdsppmBusPort_HwRsmp_Slave,
    AdsppmBusPort_AvSync_Slave,
    AdsppmBusPort_Lpm_Slave,
    AdsppmBusPort_Sram_Slave,
    AdsppmBusPort_Ext_Ahb_Slave,
    AdsppmBusPort_Ddr_Slave,
    AdsppmBusPort_Ocmem_Slave,
    AdsppmBusPort_PerifNoc_Slave,
    AdsppmBusPort_Spdif_Slave,
    AdsppmBusPort_Hdmirx_Slave,
    AdsppmBusPort_Hdmitx_Slave,
    AdsppmBusPort_Sif_Slave,
    AdsppmBusPort_Bstc_Slave,
    AdsppmBusPort_Dcodec_Slave,
    AdsppmBusPort_Core,
    AdsppmBusPort_Adsp_Slave,
    AdsppmBusPort_EnumMax,
    AdsppmBusPort_EnumForce32Bit = 0x7fffffff
} AdsppmBusPortIdType;




typedef struct
{
    AdsppmBusPortIdType masterPort;
    AdsppmBusPortIdType slavePort;
} AdsppmBusRouteType;




typedef struct
{
    uint64 Ab;
    uint64 Ib;
    uint32 latencyNs;
} AdsppmBusBWDataIbAbType;




typedef enum
{
    Adsppm_BwUsage_None,
    Adsppm_BwUsage_DSP,
    Adsppm_BwUsage_DMA,
    Adsppm_BwUsage_EXT_CPU,
    Adsppm_BwUsage_Enum_Max,
    Adsppm_BwUsage_Force32Bbits = 0x7FFFFFFF
} AdsppmBwUsageLpassType;




typedef struct
{
    uint64 bwBytePerSec;
    uint32 usagePercentage;
    AdsppmBwUsageLpassType usageType;
} AdsppmBusBWDataUsageType;

typedef union
{
    AdsppmBusBWDataIbAbType busBwAbIb;
    AdsppmBusBWDataUsageType busBwValue;
} AdsppmBusBWDataType;




typedef enum
{
    AdsppmBusBWOperation_BW = 1,
    AdsppmBusBWOperation_RegAccess
}AdsppmBusBWOperationType;




typedef struct
{
    AdsppmBusRouteType busRoute;
    AdsppmBusBWDataType bwValue;
    AdsppmBusBWOperationType bwOperation;
} AdsppmBusBWRequestValueType;

typedef enum{
    Adsppm_BwReqClass_Generic,
    Adsppm_BwReqClass_Compensated,
    Adsppm_BwReqClass_Force32Bbits = 0x7FFFFFFF
} AdsppmBwRequestClass;




typedef struct
{
    uint32 numOfBw;
    AdsppmBusBWRequestValueType pBwArray[8];
    AdsppmBwRequestClass requestClass;
} AdsppmBwReqType;
# 441 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
typedef struct
{
    uint64 adspDdrBwAb;
    uint64 adspDdrBwIb;
    uint64 extAhbDdrBwAb;
    uint64 extAhbDdrBwIb;

    uint64 intAhbBwAb;
    uint64 intAhbBwIb;
} AdsppmInfoAggregatedBwType;





typedef struct
{
    uint32 ahbeFreqHz;
} AdsppmInfoAhbType;





typedef struct
{
    AdsppmBusBWDataIbAbType dcvsVote;
    AdsppmBusBWDataIbAbType clientsFloorVote;
    AdsppmBusBWDataIbAbType clientsFinalVote;
    AdsppmBusBWDataIbAbType finalVoteToNpa;
} AdsppmInfoDcvsAdspDdrBwType;





typedef struct
{
    uint32 dcvsVote;
    uint32 clientsFloorVote;
    uint32 clientsFinalVote;
    uint32 finalVoteToNpa;
} AdsppmInfoDcvsAdspClockType;


typedef enum
{
    ADSPPM_PRIVATE_INFO_AGGREGATED_BW = 0,
    ADSPPM_PRIVATE_INFO_AHB = 1,
    ADSPPM_PRIVATE_INFO_ADSPDDR_BW = 2,
    ADSPPM_PRIVATE_INFO_ADSPCLOCK = 3,
} AdsppmInfoPrivateTypeType;

typedef struct
{


    AdsppmInfoPrivateTypeType type;
    union
    {

        AdsppmInfoAggregatedBwType aggregatedBw;

        AdsppmInfoAhbType ahb;

        AdsppmInfoDcvsAdspDdrBwType adspDdrBw;

        AdsppmInfoDcvsAdspClockType adspClock;
    };
} AdsppmInfoPrivateType;




typedef enum
{
    MipsRequestOperation_None = 0,
    MipsRequestOperation_MIPS = 1,
    MipsRequestOperation_BWandMIPS = 2,
    MipsRequestOperation_MAX = 3,
    MipsRequestOperation_EnumForce8Bit = 0x7f
} AdsppmMipsOperationType;

typedef enum
{
    Adsppm_Q6ClockRequestUsageType_Mips,
    Adsppm_Q6ClockRequestUsageType_Mpps,
    Adsppm_Q6ClockRequestUsageType_Force32Bbits = 0x7FFFFFFF
} AdsppmQ6ClockRequestUsageType;




typedef struct
{
    uint32 mipsTotal;
    uint32 mipsPerThread;
    AdsppmBusPortIdType codeLocation;
    AdsppmMipsOperationType reqOperation;
} AdsppmMipsRequestType;




typedef struct
{
    uint32 mppsTotal;
    uint32 adspFloorClock;
} AdsppmMppsRequestType;




typedef struct
{

    union
    {
        AdsppmMipsRequestType mipsRequestData;
        AdsppmMppsRequestType mppsRequestData;
    } AdsppmQ6ClockRequestType;
    AdsppmQ6ClockRequestUsageType usageType;

} AdsppmQ6ClockRequestInfoType;




typedef struct
{
    uint32 mipsTotal;
    uint32 mipsPerThread;
} AdsppmMipsDataType;




typedef struct
{
    AdsppmMipsDataType mipsData;
    AdsppmBusBWRequestValueType mipsToBW;
} AdsppmMIPSToBWAggregateType;




typedef struct
{
    AdsppmMipsDataType mipsData;
    uint32 qDSP6Clock;
} AdsppmMIPSToClockAggregateType;







typedef enum
{
    Adsppm_RegProg_None,
    Adsppm_RegProg_Norm,
    Adsppm_RegProg_Fast,
    Adsppm_RegProg_Enum_Max,
    Adsppm_RegProg_EnumForce32Bit = 0x7fffffff
} AdsppmRegProgMatchType;







typedef enum
{
    Adsppm_Freq_At_Least,
    Adsppm_Freq_At_Most,
    Adsppm_Freq_Closest,
    Adsppm_Freq_Exact,
    Adsppm_Freq_Max,
    Adsppm_Freq_Force8Bits = 0x7F
} AdsppmFreqMatchType;




typedef enum
{
    Adsppm_Info_Type_None,
    Adsppm_Info_Type_Max_Value,
    Adsppm_Info_Type_Min_Value,
    Adsppm_Info_Type_Current_Value,
    Adsppm_Info_Type_Max,
    Adsppm_Info_Type_Force8Bits = 0x7F
} AdsppmInfoType;




typedef struct
{
    AdsppmClkIdType clkId;
    uint32 clkFreqHz;
    AdsppmFreqMatchType freqMatch;
} AdsppmClkValType;




typedef struct
{
    uint32 clkId;
    uint32 clkFreqHz;
    uint32 forceMeasure;

} AdsppmInfoClkFreqType;




typedef struct
{
    uint32 numOfClk;
    AdsppmClkValType pClkArray[8];
} AdsppmClkRequestType;







typedef enum
{
    Adsppm_Clk_Domain_Src_Id_Lpass_None,
    Adsppm_Clk_Domain_Src_Id_Primary_PLL,
    Adsppm_Clk_Domain_Src_Id_Secondary_PLL,
    Adsppm_Clk_Domain_Src_Id_Ternery_PLL,
    Adsppm_Clk_Domain_Src_Id_Lpass_Max,
    Adsppm_Clk_Domain_Src_Id_Lpass_Force32bits = 0x7FFFFFFF
} AdsppmClkDomainSrcIdLpassType;

typedef union
{
    AdsppmClkDomainSrcIdLpassType clkDomainSrcIdLpass;
} AdsppmClkDomainSrcIdType;




typedef struct
{
    AdsppmClkIdType clkId;
    uint32 clkFreqHz;
    AdsppmClkDomainSrcIdType clkDomainSrc;
} AdsppmClkDomainType;




typedef struct
{
    uint32 numOfClk;
    AdsppmClkDomainType pClkDomainArray[8];
} AdsppmClkDomainReqType;







typedef enum
{
    Adsppm_Mem_Power_None,
    Adsppm_Mem_Power_Off,
    Adsppm_Mem_Power_Retention,
    Adsppm_Mem_Power_Active,
    Adsppm_Mem_Power_Max,
    Adsppm_Mem_Power_Force32Bits = 0x7FFFFFFF
} AdsppmMemPowerStateType;




typedef enum
{
    Adsppm_Mem_None,
    Adsppm_Mem_Ocmem,
    Adsppm_Mem_Lpass_LPM,
    Adsppm_Mem_Sram,
    Adsppm_Mem_Max,
    Adsppm_MEM_FORCE32BITS = 0x7FFFFFFF
} AdsppmMemIdType;




typedef struct
{
    AdsppmMemIdType memory;
    AdsppmMemPowerStateType powerState;
} AdsppmMemPowerReqParamType;







typedef enum
{
    Adsppm_Param_Id_None = 0,
    Adsppm_Param_Id_Resource_Limit,
    Adsppm_Param_Id_Client_Ocmem_Map,
    Adsppm_Param_Id_Memory_Map,
    Adsppm_Param_Id_Enum_Max,
    Adsppm_Param_Id_Force32bit = 0x7fffffff
} AdsppmParameterIdType;







typedef struct
{
    AdsppmParameterIdType paramId;
    void *pParamConfig;
} AdsppmParameterConfigType;
# 790 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
Adsppm_Status ADSPPM_Init(void);






uint32 ADSPPM_IsInitialized(void);
# 806 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
void ADSPPM_EnterSleep(void);
# 815 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm.h"
void ADSPPM_ExitSleep(void);
# 15 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h" 1
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;


typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),


  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),


  NPA_CLIENT_REQUIRED = (1 << 6),

  NPA_CLIENT_ISOCHRONOUS = (1 << 7),

  NPA_CLIENT_IMPULSE = (1 << 8),


  NPA_CLIENT_LIMIT_MAX = (1 << 9),

  NPA_CLIENT_VECTOR = (1 << 10),



  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),


  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),



  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),

} npa_client_type;




typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,

  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,



  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,


  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,

  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,

  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,

  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,

  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;




typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,


  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,


  NPA_EVENT_CHANGE,


  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,



  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,


  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,

  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,

  NPA_NUM_EVENT_TYPES
} npa_event_type;
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,


  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,






  NPA_REQUEST_NEXT_AWAKE = 0x00000002,






  NPA_REQUEST_CHANGED_TYPE = 0x00000004,






  NPA_REQUEST_BEST_EFFORT = 0x00000008,


  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,

} npa_request_attribute;
# 210 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
enum {

  NPA_QUERY_CURRENT_STATE,

  NPA_QUERY_CLIENT_ACTIVE_REQUEST,



  NPA_QUERY_ACTIVE_MAX,



  NPA_QUERY_RESOURCE_MAX,


  NPA_QUERY_RESOURCE_DISABLED,

  NPA_QUERY_RESOURCE_LATENCY,

  NPA_QUERY_CURRENT_AGGREGATION,


  NPA_MAX_PUBLIC_QUERY = 1023,


  NPA_QUERY_RESERVED_BEGIN = 1024,


  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,

  NPA_QUERY_RESERVED_END = 4095


};


typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;





typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;

typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;


typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );

typedef void ( *npa_simple_callback )( void *context );


typedef struct npa_client * npa_client_handle;


typedef struct npa_event * npa_event_handle;


typedef struct npa_custom_event * npa_custom_event_handle;


typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;


typedef struct npa_prepost_change_data
{
  const char *resource_name;


  npa_resource_state from_state;
  npa_resource_state to_state;


  void *data;
} npa_prepost_change_data;


typedef struct npa_link * npa_query_handle;





typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,

   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;


typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
# 391 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
# 491 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
# 566 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_client( npa_client_handle client );
# 579 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
# 614 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
# 660 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
# 729 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_impulse_request( npa_client_handle client );
# 763 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
# 848 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
# 869 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
# 887 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_complete_request( npa_client_handle client );
# 906 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_cancel_request( npa_client_handle client );
# 917 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
# 941 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_client_type_required( npa_client_handle client );
# 964 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_client_type_suppressible( npa_client_handle client );
# 1012 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
# 1097 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
# 1127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
# 1168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
# 1188 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_custom_event( npa_event_handle event );
# 1206 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_event_handle( npa_event_handle event );
# 1224 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_handle npa_create_query_handle( const char * resource_name );
# 1237 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_destroy_query_handle( npa_query_handle query );
# 1254 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
# 1283 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
# 1302 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
# 1321 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
# 1343 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
npa_query_status npa_query_resource_available( const char *resource_name );
# 1364 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
# 1389 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
# 1409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
# 1602 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
# 1617 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa.h"
typedef unsigned int (*npa_join_function) ( void *, void * );






void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );







npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );







void npa_join_request( npa_client_handle client );
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h" 2
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef enum
{

  NPA_RESOURCE_DEFAULT = 0,


  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,


  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,


  NPA_RESOURCE_VECTOR_STATE = 0x00000004,


  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,


  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,



  NPA_RESOURCE_REMOTE = 0x00000010,



  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,


  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,





  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,



  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,


  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,


  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,


  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,

} npa_resource_attribute;


typedef enum
{
  NPA_NODE_DEFAULT = 0,



  NPA_NODE_NO_LOCK = 0x00000001,


  NPA_NODE_DISABLEABLE = 0x00000002,


  NPA_NODE_FORKABLE = 0x00000004,

} npa_node_attribute;




enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,

  NPA_QUERY_RESOURCE_ATTRIBUTES,

  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};






typedef void* npa_user_data;



typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;



typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;



typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];

  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;

  unsigned int request_attr;




  void (*issue_request)( npa_client_handle client, int new_request );



  struct npa_scheduler_data *request_ptr;
} npa_client;
# 215 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;


  const char *name;
  struct npa_resource *resource;

  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;

  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;

  npa_event_callback callback;



  void *reg_data;



  struct npa_event_action *action;
} npa_event;
# 263 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
# 280 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );







typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );



typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
# 320 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
# 344 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );



typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
# 366 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  void (*create_client_fcn) ( npa_client * );





  void (*destroy_client_fcn)( npa_client * );
# 388 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
# 411 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
# 441 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;

  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;



  struct npa_resource *handle;
} npa_resource_definition;
# 467 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;



typedef struct npa_resource
{

  npa_resource_definition *definition;


  npa_node_definition *node;


  unsigned int index;

  npa_client *clients;


  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;





  const npa_resource_plugin *active_plugin;




  npa_resource_state request_state;


  npa_resource_state active_state;



  npa_resource_state internal_state[8];





  npa_resource_state *state_vector;
# 532 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  npa_resource_state *required_state_vector;






  npa_resource_state *suppressible_state_vector;






  npa_resource_state *suppressible2_state_vector;
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
  npa_resource_state *semiactive_state_vector;

  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;


  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;


  struct npa_resource_internal_data *_internal;


  unsigned int sequence;


  void *log_handle;



  npa_resource_latency *latency;
} npa_resource;



typedef void* npa_join_token;
# 621 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
# 649 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
# 678 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_marker( const char *marker_name );
# 710 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
# 763 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
# 792 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
# 834 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
# 888 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
# 915 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_disable_node( npa_node_definition *node );
# 925 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
# 941 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_resource_lock( npa_resource *resource );
# 955 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
int npa_resource_trylock( npa_resource *resource );
# 965 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_resource_unlock( npa_resource *resource );
# 981 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
# 992 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
unsigned int npa_get_request_attributes( npa_client_handle client );
# 1008 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
# 1034 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
# 1052 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
extern const npa_resource_plugin npa_binary_plugin;


extern const npa_resource_plugin npa_max_plugin;


extern const npa_resource_plugin npa_min_plugin;


extern const npa_resource_plugin npa_sum_plugin;







extern const npa_resource_plugin npa_identity_plugin;


extern const npa_resource_plugin npa_always_on_plugin;


extern const npa_resource_plugin npa_impulse_plugin;


extern const npa_resource_plugin npa_or_plugin;



extern const npa_resource_plugin npa_binary_and_plugin;


extern const npa_resource_plugin npa_no_client_plugin;
# 1094 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);


npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );


npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );


npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );


npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);


npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);


npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );


npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );


npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
# 1181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_issue_internal_request( npa_client_handle client );
# 1210 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
# 1229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
# 1245 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
# 1266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
# 1281 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
# 1297 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
# 1315 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
# 1332 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
# 1351 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
# 1371 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
# 1393 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/npa_resource.h"
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbid.h" 1
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbid.h"
typedef enum {
  ICBID_MASTER_APPSS_PROC = 0,
  ICBID_MASTER_MSS_PROC,
  ICBID_MASTER_MNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC_0 = ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_CNOC_MNOC_MMSS_CFG,
  ICBID_MASTER_CNOC_MNOC_CFG,
  ICBID_MASTER_GFX3D,
  ICBID_MASTER_JPEG,
  ICBID_MASTER_MDP,
  ICBID_MASTER_MDP0 = ICBID_MASTER_MDP,
  ICBID_MASTER_MDPS = ICBID_MASTER_MDP,
  ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P0 = ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P1,
  ICBID_MASTER_VFE,
  ICBID_MASTER_VFE0 = ICBID_MASTER_VFE,
  ICBID_MASTER_CNOC_ONOC_CFG,
  ICBID_MASTER_JPEG_OCMEM,
  ICBID_MASTER_MDP_OCMEM,
  ICBID_MASTER_VIDEO_P0_OCMEM,
  ICBID_MASTER_VIDEO_P1_OCMEM,
  ICBID_MASTER_VFE_OCMEM,
  ICBID_MASTER_LPASS_AHB,
  ICBID_MASTER_QDSS_BAM,
  ICBID_MASTER_SNOC_CFG,
  ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_BIMC_SNOC_0 = ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_CNOC_SNOC,
  ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE0 = ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE1,
  ICBID_MASTER_LPASS_PROC,
  ICBID_MASTER_MSS,
  ICBID_MASTER_MSS_NAV,
  ICBID_MASTER_OCMEM_DMA,
  ICBID_MASTER_PNOC_SNOC,
  ICBID_MASTER_WCSS,
  ICBID_MASTER_QDSS_ETR,
  ICBID_MASTER_USB3,
  ICBID_MASTER_USB3_0 = ICBID_MASTER_USB3,
  ICBID_MASTER_SDCC_1,
  ICBID_MASTER_SDCC_3,
  ICBID_MASTER_SDCC_2,
  ICBID_MASTER_SDCC_4,
  ICBID_MASTER_TSIF,
  ICBID_MASTER_BAM_DMA,
  ICBID_MASTER_BLSP_2,
  ICBID_MASTER_USB_HSIC,
  ICBID_MASTER_BLSP_1,
  ICBID_MASTER_USB_HS,
  ICBID_MASTER_USB_HS1 = ICBID_MASTER_USB_HS,
  ICBID_MASTER_PNOC_CFG,
  ICBID_MASTER_SNOC_PNOC,
  ICBID_MASTER_RPM_INST,
  ICBID_MASTER_RPM_DATA,
  ICBID_MASTER_RPM_SYS,
  ICBID_MASTER_DEHR,
  ICBID_MASTER_QDSS_DAP,
  ICBID_MASTER_SPDM,
  ICBID_MASTER_TIC,
  ICBID_MASTER_SNOC_CNOC,
  ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_GFX3D_GMEM = ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_OVIRT_SNOC,
  ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_SNOC_GVIRT = ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_ONOC_OVIRT,
  ICBID_MASTER_USB_HS2,
  ICBID_MASTER_QPIC,
  ICBID_MASTER_IPA,
  ICBID_MASTER_DSI,
  ICBID_MASTER_MDP1,
  ICBID_MASTER_MDPE = ICBID_MASTER_MDP1,
  ICBID_MASTER_VPU_PROC,
  ICBID_MASTER_VPU,
  ICBID_MASTER_VPU0 = ICBID_MASTER_VPU,
  ICBID_MASTER_CRYPTO_CORE2,
  ICBID_MASTER_PCIE_0,
  ICBID_MASTER_PCIE_1,
  ICBID_MASTER_SATA,
  ICBID_MASTER_UFS,
  ICBID_MASTER_USB3_1,
  ICBID_MASTER_VIDEO_OCMEM,
  ICBID_MASTER_VPU1,
  ICBID_MASTER_VCAP,
  ICBID_MASTER_EMAC,
  ICBID_MASTER_BCAST,
  ICBID_MASTER_MMSS_PROC,
  ICBID_MASTER_SNOC_BIMC_1,
  ICBID_MASTER_SNOC_PCNOC,
  ICBID_MASTER_AUDIO,
  ICBID_MASTER_MM_INT_0,
  ICBID_MASTER_MM_INT_1,
  ICBID_MASTER_MM_INT_2,
  ICBID_MASTER_MM_INT_BIMC,
  ICBID_MASTER_MSS_INT,
  ICBID_MASTER_PCNOC_CFG,
  ICBID_MASTER_PCNOC_INT_0,
  ICBID_MASTER_PCNOC_INT_1,
  ICBID_MASTER_PCNOC_M_0,
  ICBID_MASTER_PCNOC_M_1,
  ICBID_MASTER_PCNOC_S_0,
  ICBID_MASTER_PCNOC_S_1,
  ICBID_MASTER_PCNOC_S_2,
  ICBID_MASTER_PCNOC_S_3,
  ICBID_MASTER_PCNOC_S_4,
  ICBID_MASTER_PCNOC_S_6,
  ICBID_MASTER_PCNOC_S_7,
  ICBID_MASTER_PCNOC_S_8,
  ICBID_MASTER_PCNOC_S_9,
  ICBID_MASTER_QDSS_INT,
  ICBID_MASTER_SNOC_INT_0,
  ICBID_MASTER_SNOC_INT_1,
  ICBID_MASTER_SNOC_INT_BIMC,
  ICBID_MASTER_TCU_0,
  ICBID_MASTER_TCU_1,
  ICBID_MASTER_BIMC_INT_0,
  ICBID_MASTER_BIMC_INT_1,
  ICBID_MASTER_CAMERA,
  ICBID_MASTER_RICA,
  ICBID_MASTER_SNOC_BIMC_2,
  ICBID_MASTER_BIMC_SNOC_1,
  ICBID_MASTER_A0NOC_SNOC,
  ICBID_MASTER_A1NOC_SNOC,
  ICBID_MASTER_A2NOC_SNOC,
  ICBID_MASTER_PIMEM,
  ICBID_MASTER_SNOC_VMEM,
  ICBID_MASTER_CPP,
  ICBID_MASTER_CNOC_A1NOC,
  ICBID_MASTER_PNOC_A1NOC,
  ICBID_MASTER_HMSS,
  ICBID_MASTER_PCIE_2,
  ICBID_MASTER_ROTATOR,
  ICBID_MASTER_VENUS_VMEM,
  ICBID_MASTER_DCC,
  ICBID_MASTER_MCDMA,
  ICBID_MASTER_PCNOC_INT_2,
  ICBID_MASTER_PCNOC_INT_3,
  ICBID_MASTER_PCNOC_INT_4,
  ICBID_MASTER_PCNOC_INT_5,
  ICBID_MASTER_PCNOC_INT_6,
  ICBID_MASTER_PCNOC_S_5,
  ICBID_MASTER_SENSORS_AHB,
  ICBID_MASTER_SENSORS_PROC,
  ICBID_MASTER_QSPI,
  ICBID_MASTER_VFE1,
  ICBID_MASTER_SNOC_INT_2,
  ICBID_MASTER_SMMNOC_BIMC,
  ICBID_MASTER_CRVIRT_A1NOC,
  ICBID_MASTER_XM_USB_HS1,
  ICBID_MASTER_XI_USB_HS1,
  ICBID_MASTER_COUNT,
  ICBID_MASTER_SIZE = 0x7FFFFFFF
} ICBId_MasterType;




typedef enum {
  ICBID_SLAVE_EBI1 = 0,
  ICBID_SLAVE_APPSS_L2,
  ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_BIMC_SNOC_0 = ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_CAMERA_CFG,
  ICBID_SLAVE_DISPLAY_CFG,
  ICBID_SLAVE_OCMEM_CFG,
  ICBID_SLAVE_CPR_CFG,
  ICBID_SLAVE_CPR_XPU_CFG,
  ICBID_SLAVE_MISC_CFG,
  ICBID_SLAVE_MISC_XPU_CFG,
  ICBID_SLAVE_VENUS_CFG,
  ICBID_SLAVE_GFX3D_CFG,
  ICBID_SLAVE_MMSS_CLK_CFG,
  ICBID_SLAVE_MMSS_CLK_XPU_CFG,
  ICBID_SLAVE_MNOC_MPU_CFG,
  ICBID_SLAVE_ONOC_MPU_CFG,
  ICBID_SLAVE_MNOC_BIMC,
  ICBID_SLAVE_SERVICE_MNOC,
  ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_GMEM = ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_SERVICE_ONOC,
  ICBID_SLAVE_APPSS,
  ICBID_SLAVE_LPASS,
  ICBID_SLAVE_USB3,
  ICBID_SLAVE_USB3_0 = ICBID_SLAVE_USB3,
  ICBID_SLAVE_WCSS,
  ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_BIMC_0 = ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_CNOC,
  ICBID_SLAVE_IMEM,
  ICBID_SLAVE_OCIMEM = ICBID_SLAVE_IMEM,
  ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_GVIRT = ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SNOC_PCNOC = ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SERVICE_SNOC,
  ICBID_SLAVE_QDSS_STM,
  ICBID_SLAVE_SDCC_1,
  ICBID_SLAVE_SDCC_3,
  ICBID_SLAVE_SDCC_2,
  ICBID_SLAVE_SDCC_4,
  ICBID_SLAVE_TSIF,
  ICBID_SLAVE_BAM_DMA,
  ICBID_SLAVE_BLSP_2,
  ICBID_SLAVE_USB_HSIC,
  ICBID_SLAVE_BLSP_1,
  ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_USB_HS1 = ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_PDM,
  ICBID_SLAVE_PERIPH_APU_CFG,
  ICBID_SLAVE_PNOC_MPU_CFG,
  ICBID_SLAVE_PRNG,
  ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_PCNOC_SNOC = ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_SERVICE_PNOC,
  ICBID_SLAVE_CLK_CTL,
  ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_PCNOC_MSS = ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_SECURITY,
  ICBID_SLAVE_TCSR,
  ICBID_SLAVE_TLMM,
  ICBID_SLAVE_CRYPTO_0_CFG,
  ICBID_SLAVE_CRYPTO_1_CFG,
  ICBID_SLAVE_IMEM_CFG,
  ICBID_SLAVE_MESSAGE_RAM,
  ICBID_SLAVE_BIMC_CFG,
  ICBID_SLAVE_BOOT_ROM,
  ICBID_SLAVE_CNOC_MNOC_MMSS_CFG,
  ICBID_SLAVE_PMIC_ARB,
  ICBID_SLAVE_SPDM_WRAPPER,
  ICBID_SLAVE_DEHR_CFG,
  ICBID_SLAVE_MPM,
  ICBID_SLAVE_QDSS_CFG,
  ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_CX_CFG = ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_QDSS_APU_CFG,
  ICBID_SLAVE_CNOC_MNOC_CFG,
  ICBID_SLAVE_SNOC_MPU_CFG,
  ICBID_SLAVE_CNOC_ONOC_CFG,
  ICBID_SLAVE_PNOC_CFG,
  ICBID_SLAVE_SNOC_CFG,
  ICBID_SLAVE_EBI1_DLL_CFG,
  ICBID_SLAVE_PHY_APU_CFG,
  ICBID_SLAVE_EBI1_PHY_CFG,
  ICBID_SLAVE_RPM,
  ICBID_SLAVE_CNOC_SNOC,
  ICBID_SLAVE_SERVICE_CNOC,
  ICBID_SLAVE_OVIRT_SNOC,
  ICBID_SLAVE_OVIRT_OCMEM,
  ICBID_SLAVE_USB_HS2,
  ICBID_SLAVE_QPIC,
  ICBID_SLAVE_IPS_CFG,
  ICBID_SLAVE_DSI_CFG,
  ICBID_SLAVE_USB3_1,
  ICBID_SLAVE_PCIE_0,
  ICBID_SLAVE_PCIE_1,
  ICBID_SLAVE_PSS_SMMU_CFG,
  ICBID_SLAVE_CRYPTO_2_CFG,
  ICBID_SLAVE_PCIE_0_CFG,
  ICBID_SLAVE_PCIE_1_CFG,
  ICBID_SLAVE_SATA_CFG,
  ICBID_SLAVE_SPSS_GENI_IR,
  ICBID_SLAVE_UFS_CFG,
  ICBID_SLAVE_AVSYNC_CFG,
  ICBID_SLAVE_VPU_CFG,
  ICBID_SLAVE_USB_PHY_CFG,
  ICBID_SLAVE_RBCPR_MX_CFG,
  ICBID_SLAVE_PCIE_PARF,
  ICBID_SLAVE_VCAP_CFG,
  ICBID_SLAVE_EMAC_CFG,
  ICBID_SLAVE_BCAST_CFG,
  ICBID_SLAVE_KLM_CFG,
  ICBID_SLAVE_DISPLAY_PWM,
  ICBID_SLAVE_GENI,
  ICBID_SLAVE_SNOC_BIMC_1,
  ICBID_SLAVE_AUDIO,
  ICBID_SLAVE_CATS_0,
  ICBID_SLAVE_CATS_1,
  ICBID_SLAVE_MM_INT_0,
  ICBID_SLAVE_MM_INT_1,
  ICBID_SLAVE_MM_INT_2,
  ICBID_SLAVE_MM_INT_BIMC,
  ICBID_SLAVE_MMU_MODEM_XPU_CFG,
  ICBID_SLAVE_MSS_INT,
  ICBID_SLAVE_PCNOC_INT_0,
  ICBID_SLAVE_PCNOC_INT_1,
  ICBID_SLAVE_PCNOC_M_0,
  ICBID_SLAVE_PCNOC_M_1,
  ICBID_SLAVE_PCNOC_S_0,
  ICBID_SLAVE_PCNOC_S_1,
  ICBID_SLAVE_PCNOC_S_2,
  ICBID_SLAVE_PCNOC_S_3,
  ICBID_SLAVE_PCNOC_S_4,
  ICBID_SLAVE_PCNOC_S_6,
  ICBID_SLAVE_PCNOC_S_7,
  ICBID_SLAVE_PCNOC_S_8,
  ICBID_SLAVE_PCNOC_S_9,
  ICBID_SLAVE_PRNG_XPU_CFG,
  ICBID_SLAVE_QDSS_INT,
  ICBID_SLAVE_RPM_XPU_CFG,
  ICBID_SLAVE_SNOC_INT_0,
  ICBID_SLAVE_SNOC_INT_1,
  ICBID_SLAVE_SNOC_INT_BIMC,
  ICBID_SLAVE_TCU,
  ICBID_SLAVE_BIMC_INT_0,
  ICBID_SLAVE_BIMC_INT_1,
  ICBID_SLAVE_RICA_CFG,
  ICBID_SLAVE_SNOC_BIMC_2,
  ICBID_SLAVE_BIMC_SNOC_1,
  ICBID_SLAVE_PNOC_A1NOC,
  ICBID_SLAVE_SNOC_VMEM,
  ICBID_SLAVE_A0NOC_SNOC,
  ICBID_SLAVE_A1NOC_SNOC,
  ICBID_SLAVE_A2NOC_SNOC,
  ICBID_SLAVE_A0NOC_CFG,
  ICBID_SLAVE_A0NOC_MPU_CFG,
  ICBID_SLAVE_A0NOC_SMMU_CFG,
  ICBID_SLAVE_A1NOC_CFG,
  ICBID_SLAVE_A1NOC_MPU_CFG,
  ICBID_SLAVE_A1NOC_SMMU_CFG,
  ICBID_SLAVE_A2NOC_CFG,
  ICBID_SLAVE_A2NOC_MPU_CFG,
  ICBID_SLAVE_A2NOC_SMMU_CFG,
  ICBID_SLAVE_AHB2PHY,
  ICBID_SLAVE_CAMERA_THROTTLE_CFG,
  ICBID_SLAVE_DCC_CFG,
  ICBID_SLAVE_DISPLAY_THROTTLE_CFG,
  ICBID_SLAVE_DSA_CFG,
  ICBID_SLAVE_DSA_MPU_CFG,
  ICBID_SLAVE_SSC_MPU_CFG,
  ICBID_SLAVE_HMSS_L3,
  ICBID_SLAVE_LPASS_SMMU_CFG,
  ICBID_SLAVE_MMAGIC_CFG,
  ICBID_SLAVE_PCIE20_AHB2PHY,
  ICBID_SLAVE_PCIE_2,
  ICBID_SLAVE_PCIE_2_CFG,
  ICBID_SLAVE_PIMEM,
  ICBID_SLAVE_PIMEM_CFG,
  ICBID_SLAVE_QDSS_RBCPR_APU_CFG,
  ICBID_SLAVE_RBCPR_CX,
  ICBID_SLAVE_RBCPR_MX,
  ICBID_SLAVE_SMMU_CPP_CFG,
  ICBID_SLAVE_SMMU_JPEG_CFG,
  ICBID_SLAVE_SMMU_MDP_CFG,
  ICBID_SLAVE_SMMU_ROTATOR_CFG,
  ICBID_SLAVE_SMMU_VENUS_CFG,
  ICBID_SLAVE_SMMU_VFE_CFG,
  ICBID_SLAVE_SSC_CFG,
  ICBID_SLAVE_VENUS_THROTTLE_CFG,
  ICBID_SLAVE_VMEM,
  ICBID_SLAVE_VMEM_CFG,
  ICBID_SLAVE_QDSS_MPU_CFG,
  ICBID_SLAVE_USB3_PHY_CFG,
  ICBID_SLAVE_IPA_CFG,
  ICBID_SLAVE_PCNOC_INT_2,
  ICBID_SLAVE_PCNOC_INT_3,
  ICBID_SLAVE_PCNOC_INT_4,
  ICBID_SLAVE_PCNOC_INT_5,
  ICBID_SLAVE_PCNOC_INT_6,
  ICBID_SLAVE_PCNOC_S_5,
  ICBID_SLAVE_QSPI,
  ICBID_SLAVE_A1NOC_MS_MPU_CFG,
  ICBID_SLAVE_A2NOC_MS_MPU_CFG,
  ICBID_SLAVE_MODEM_Q6_SMMU_CFG,
  ICBID_SLAVE_MSS_MPU_CFG,
  ICBID_SLAVE_MSS_PROC_MS_MPU_CFG,
  ICBID_SLAVE_SKL,
  ICBID_SLAVE_SNOC_INT_2,
  ICBID_SLAVE_SMMNOC_BIMC,
  ICBID_SLAVE_CRVIRT_A1NOC,
  ICBID_SLAVE_COUNT,
  ICBID_SLAVE_SIZE = 0x7FFFFFFF
} ICBId_SlaveType;
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h" 2
# 51 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
typedef enum
{
   ICBARB_ERROR_SUCCESS = 0,
   ICBARB_ERROR_UNSUPPORTED,
   ICBARB_ERROR_INVALID_ARG,
   ICBARB_ERROR_INVALID_MASTER,
   ICBARB_ERROR_NO_ROUTE_TO_SLAVE,
   ICBARB_ERROR_VECTOR_LENGTH_MISMATCH,
   ICBARB_ERROR_OUT_OF_MEMORY,
   ICBARB_ERROR_UNKNOWN,
   ICBARB_ERROR_REQUEST_REJECTED,


   ICBARB_ERROR_MAX,

   ICBARB_ERROR_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_ErrorType;




typedef enum
{
   ICBARB_STATE_NORMAL = 0,
   ICBARB_STATE_OVERSUBSCRIBED,


   ICBARB_STATE_MAX,

   ICBARB_STATE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_StateType;




typedef struct
{
   ICBId_MasterType eMaster;
   ICBId_SlaveType eSlave;
} ICBArb_MasterSlaveType;

typedef struct
{
   npa_callback callback;
   ICBArb_MasterSlaveType *aMasterSlave;
} ICBArb_CreateClientVectorType;




typedef enum
{

   ICBARB_REQUEST_TYPE_1,
   ICBARB_REQUEST_TYPE_2,
   ICBARB_REQUEST_TYPE_3,


   ICBARB_REQUEST_TYPE_1_TIER_3,
   ICBARB_REQUEST_TYPE_2_TIER_3,
   ICBARB_REQUEST_TYPE_3_TIER_3,


   ICBARB_REQUEST_TYPE_1_LAT,
   ICBARB_REQUEST_TYPE_2_LAT,
   ICBARB_REQUEST_TYPE_3_LAT,


   ICBARB_REQUEST_TYPE_MAX,

   ICBARB_REQUEST_TYPE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_RequestTypeType;




typedef struct
{
   uint64 uDataBurst;
   uint32 uTransferTimeUs;
   uint32 uPeriodUs;
   uint32 uLatencyNs;

} ICBArb_Request1Type;




typedef struct
{
   uint64 uThroughPut;
   uint32 uUsagePercentage;
   uint32 uLatencyNs;

} ICBArb_Request2Type;




typedef struct
{
   uint64 uIb;
   uint64 uAb;
   uint32 uLatencyNs;

} ICBArb_Request3Type;




typedef union
{
   ICBArb_Request1Type type1;
   ICBArb_Request2Type type2;
   ICBArb_Request3Type type3;
} ICBArb_RequestUnionType;




typedef struct
{
   ICBArb_RequestTypeType arbType;
   ICBArb_RequestUnionType arbData;
} ICBArb_RequestType;




typedef enum
{

   ICBARB_QUERY_RESOURCE_COUNT = 0,


   ICBARB_QUERY_RESOURCE_NAMES,


   ICBARB_QUERY_SLAVE_PORT_COUNT,


   ICBARB_QUERY_MAX,

   ICBARB_QUERY_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_QueryTypeType;




typedef struct
{
   npa_client_handle client;
   uint32 uNumResource;
} ICBArb_QueryResourceCountType;







typedef struct
{
   npa_client_handle client;
   const char **aResourceNames;
   uint32 uNumResource;
} ICBArb_QueryResourceNamesType;




typedef struct
{
   ICBId_SlaveType eSlaveId;
   uint32 uNumPorts;
} ICBArb_QuerySlavePortCountType;




typedef union
{
   ICBArb_QueryResourceCountType resourceCount;
   ICBArb_QueryResourceNamesType resourceNames;
   ICBArb_QuerySlavePortCountType slavePortCount;
} ICBArb_QueryUnionType;




typedef struct
{
   ICBArb_QueryTypeType queryType;
   ICBArb_QueryUnionType queryData;
} ICBArb_QueryType;
# 256 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
void icbarb_init( void );
# 297 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
ICBArb_CreateClientVectorType *icbarb_fill_client_vector(
  ICBArb_CreateClientVectorType *psVector,
  ICBArb_MasterSlaveType *aMasterSlave,
  npa_callback callback);
# 319 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
npa_client_handle icbarb_create_client(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave);
# 346 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
npa_client_handle icbarb_create_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
# 377 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
npa_client_handle icbarb_create_suppressible_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
# 411 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
ICBArb_ErrorType icbarb_issue_request(
   npa_client_handle client,
   ICBArb_RequestType *aRequest,
   uint32 uNumRequest);
# 426 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
void icbarb_complete_request( npa_client_handle client );
# 442 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
void icbarb_destroy_client( npa_client_handle client );
# 455 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/buses/icbarb.h"
ICBArb_ErrorType icbarb_query( ICBArb_QueryType *pQuery );
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h" 1
# 56 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
typedef enum
{
  DALIPCINT_PROC_NULL = 0,
  DALIPCINT_PROC_MCPU = 1,
  DALIPCINT_PROC_MDSPSW = 2,
  DALIPCINT_PROC_MDSPFW = 3,
  DALIPCINT_PROC_ACPU = 4,
  DALIPCINT_PROC_ACPU1 = 5,
  DALIPCINT_PROC_ADSP = 6,
  DALIPCINT_PROC_SPS = 7,
  DALIPCINT_PROC_RPM = 8,
  DALIPCINT_PROC_RIVA = 9,
  DALIPCINT_PROC_WCN = DALIPCINT_PROC_RIVA,
  DALIPCINT_PROC_GSS = 10,
  DALIPCINT_PROC_LPASS = 11,
  DALIPCINT_PROC_TZ = 12,

  DALIPCINT_NUM_PROCS = 13,
  DALIPCINT_PROC_32BITS = 0x7FFFFFF


} DalIPCIntProcessorType;
# 86 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
typedef enum
{
  DALIPCINT_GP_0 = 0,
  DALIPCINT_GP_1 = 1,
  DALIPCINT_GP_2 = 2,
  DALIPCINT_GP_3 = 3,
  DALIPCINT_GP_4 = 4,
  DALIPCINT_GP_5 = 5,
  DALIPCINT_GP_6 = 6,
  DALIPCINT_GP_7 = 7,
  DALIPCINT_GP_8 = 8,
  DALIPCINT_GP_9 = 9,
  DALIPCINT_GP_LOW = DALIPCINT_GP_0,
  DALIPCINT_GP_MED = DALIPCINT_GP_1,
  DALIPCINT_GP_HIGH = DALIPCINT_GP_2,
  DALIPCINT_SOFTRESET = 10,
  DALIPCINT_WAKEUP = 11,


  DALIPCINT_NUM_INTS = 12,
  DALIPCINT_INT_32BITS = 0x7FFFFFF


} DalIPCIntInterruptType;
# 119 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
typedef struct DalIPCInt DalIPCInt;
struct DalIPCInt
{
  struct DalDevice DalDevice;
  DALResult (*Trigger)(DalDeviceHandle * _h, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*TriggerBySource)(DalDeviceHandle * _h, DalIPCIntProcessorType eSource, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*IsSupported)(DalDeviceHandle * _h, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*IsSupportedBySource)(DalDeviceHandle * _h, DalIPCIntProcessorType eSource, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
};

typedef struct DalIPCIntHandle DalIPCIntHandle;
struct DalIPCIntHandle
{
  uint32 dwDalHandleId;
  const DalIPCInt * pVtbl;
  void * pClientCtxt;
};
# 168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
static __inline DALResult DalIPCInt_Trigger
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt
)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->Trigger( _h, eTarget, eInterrupt);
}
# 204 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
static __inline DALResult DalIPCInt_TriggerBySource
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eSource,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->TriggerBySource(
     _h, eSource, eTarget, eInterrupt);
}
# 235 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
static __inline DALResult DalIPCInt_IsSupported
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->IsSupported(
     _h, eTarget, eInterrupt);
}
# 270 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIIPCInt.h"
static __inline DALResult DalIPCInt_IsSupportedBySource
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eSource,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt
)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->IsSupportedBySource(
     _h, eSource, eTarget, eInterrupt);
}




void IPCInt_Init(void);
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h" 1
# 146 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum {
    MMPM_STATUS_SUCCESS,
    MMPM_STATUS_FAILED,
    MMPM_STATUS_NOMEMORY,
    MMPM_STATUS_VERSIONNOTSUPPORT,
    MMPM_STATUS_BADCLASS,
    MMPM_STATUS_BADSTATE,
    MMPM_STATUS_BADPARM,
    MMPM_STATUS_INVALIDFORMAT,
    MMPM_STATUS_UNSUPPORTED,
    MMPM_STATUS_RESOURCENOTFOUND,
    MMPM_STATUS_BADMEMPTR,
    MMPM_STATUS_BADHANDLE,
    MMPM_STATUS_RESOURCEINUSE,
    MMPM_STATUS_NOBANDWIDTH,
    MMPM_STATUS_NULLPOINTER,
    MMPM_STATUS_NOTINITIALIZED,
    MMPM_STATUS_RESOURCENOTREQUESTED,
    MMPM_STATUS_CORERESOURCENOTAVAILABLE,

    MMPM_STATUS_MAX,
    MMPM_STATUS_FORCE32BITS = 0x7FFFFFFF

} MmpmStatusType;


typedef MmpmStatusType MMPM_STATUS;
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum {
    MMPM_CORE_ID_NONE = 0,

    MMPM_CORE_ID_2D_GRP = 1,
    MMPM_CORE_ID_3D_GRP = 2,
    MMPM_CORE_ID_MDP = 3,
    MMPM_CORE_ID_VCODEC = 4,
    MMPM_CORE_ID_VPE = 5,
    MMPM_CORE_ID_VFE = 6,
    MMPM_CORE_ID_MIPICSI = 7,

    MMPM_CORE_ID_SENSOR = 8,
    MMPM_CORE_ID_JPEGD = 9,
    MMPM_CORE_ID_JPEGE = 10,
    MMPM_CORE_ID_FABRIC = 11,
    MMPM_CORE_ID_IMEM = 12,
    MMPM_CORE_ID_SMMU = 13,
    MMPM_CORE_ID_ROTATOR = 14,
    MMPM_CORE_ID_TV = 15,
    MMPM_CORE_ID_DSI = 16,
    MMPM_CORE_ID_AUDIOIF = 17,
    MMPM_CORE_ID_GMEM = 18,


    MMPM_CORE_ID_LPASS_START = 100,
    MMPM_CORE_ID_LPASS_ADSP = 101,
    MMPM_CORE_ID_LPASS_CORE = 102,
    MMPM_CORE_ID_LPASS_LPM = 103,
    MMPM_CORE_ID_LPASS_DML = 104,

    MMPM_CORE_ID_LPASS_AIF = 105,

    MMPM_CORE_ID_LPASS_SLIMBUS = 106,

    MMPM_CORE_ID_LPASS_MIDI = 107,
    MMPM_CORE_ID_LPASS_AVSYNC = 108,

    MMPM_CORE_ID_LPASS_HWRSMP = 109,

    MMPM_CORE_ID_LPASS_SRAM = 110,
    MMPM_CORE_ID_LPASS_DCODEC = 111,
    MMPM_CORE_ID_LPASS_SPDIF = 112,
    MMPM_CORE_ID_LPASS_HDMIRX = 113,
    MMPM_CORE_ID_LPASS_HDMITX = 114,
    MMPM_CORE_ID_LPASS_SIF = 115,
    MMPM_CORE_ID_LPASS_BSTC = 116,

    MMPM_CORE_ID_LPASS_ADSP_HVX = 117,

    MMPM_CORE_ID_LPASS_END,

    MMPM_CORE_ID_MAX,
    MMPM_CORE_ID_FORCE32BITS = 0x7FFFFFFF

} MmpmCoreIdType;






typedef enum {
    MMPM_CALLBACK_EVENT_ID_NONE,

    MMPM_CALLBACK_EVENT_ID_IDLE,


    MMPM_CALLBACK_EVENT_ID_BUSY,

    MMPM_CALLBACK_EVENT_ID_THERMAL,

    MMPM_CALLBACK_EVENT_ID_COMPLETE,

    MMPM_CALLBACK_EVENT_ID_MAX,

    MMPM_CALLBACK_EVENT_ID_FORCE32BITS = 0x7FFFFFFF

} MmpmCallbackEventIdType;





typedef struct {
    MmpmCallbackEventIdType eventId;




    uint32 clientId;


    uint32 callbackDataSize;


    void* callbackData;


} MmpmCallbackParamType;





typedef struct {
    uint32 reqTag;
    MMPM_STATUS result;
}MmpmCompletionCallbackDataType;
# 353 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum{
    MMPM_CORE_INSTANCE_NONE,
    MMPM_CORE_INSTANCE_0,
    MMPM_CORE_INSTANCE_1,
    MMPM_CORE_INSTANCE_2,
    MMPM_CORE_INSTANCE_MAX,
    MMPM_CORE_INSTANCE_FORCE32BITS = 0x7FFFFFFF


} MmpmCoreInstanceIdType;
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    uint32 rev;




    MmpmCoreIdType coreId;




    MmpmCoreInstanceIdType instanceId;



    char *pClientName;





    uint32 pwrCtrlFlag;





    uint32 callBackFlag;




    uint32 (*MMPM_Callback)(MmpmCallbackParamType *pCbParam);


    uint32 cbFcnStackSize;


} MmpmRegParamType;
# 424 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum {
    MMPM_RSC_ID_NONE = 0,


    MMPM_RSC_ID_POWER = 1,



    MMPM_RSC_ID_VREG = 2,



    MMPM_RSC_ID_REG_PROG = 3,


    MMPM_RSC_ID_CORE_CLK = 4,







    MMPM_RSC_ID_CORE_CLK_DOMAIN = 5,



    MMPM_RSC_ID_MEM_BW = 6,


    MMPM_RSC_ID_AXI_EN = 7,

    MMPM_RSC_ID_MIPS = 8,



    MMPM_RSC_ID_SLEEP_LATENCY = 9,



    MMPM_RSC_ID_ACTIVE_STATE = 10,

    MMPM_RSC_ID_PMIC_GPIO = 11,

    MMPM_RSC_ID_RESET = 12,



    MMPM_RSC_ID_MIPS_EXT = 13,



    MMPM_RSC_ID_GENERIC_BW = 14,


    MMPM_RSC_ID_THERMAL = 15,


    MMPM_RSC_ID_MEM_POWER = 16,


    MMPM_RSC_ID_GENERIC_BW_EXT = 17,






    MMPM_RSC_ID_MPPS = 18,





    MMPM_RSC_ID_MAX ,


    MMPM_RSC_ID_FORCE32BITS = 0x7FFFFFFF


} MmpmRscIdType;





typedef enum {
    MMPM_REG_PROG_NONE,
    MMPM_REG_PROG_NORM,
    MMPM_REG_PROG_FAST,
    MMPM_REG_PROG_MAX,
    MMPM_REG_PROG_FORCE32BITS = 0x7FFFFFFF

} MmpmRegProgMatchType;






typedef enum {
    MMPM_CLK_ID_2D_GRP_NONE,

    MMPM_CLK_ID_2D_GRP,


    MMPM_CLK_ID_2D_GRP_MAX,

    MMPM_CLK_ID_2D_GRP_FORCE32BITS = 0x7FFFFFFF

} MmpmClkId2dGrpType;





typedef enum {
    MMPM_CLK_ID_3D_GRP_NONE,

    MMPM_CLK_ID_3D_GRP,


    MMPM_CLK_ID_3D_GRP_MAX,

    MMPM_CLK_ID_3D_GRP_FORCE32BITS = 0x7FFFFFFF

} MmpmClkId3dGrpType;





typedef enum {
    MMPM_CLK_ID_MDP_NONE,

    MMPM_CLK_ID_MDP,

    MMPM_CLK_ID_MDP_VSYNC,


    MMPM_CLK_ID_MDP_LUT,

    MMPM_CLK_ID_MDP_MAX,

    MMPM_CLK_ID_MDP_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdMdpType;





typedef enum {
    MMPM_CLK_ID_VCODEC_NONE,

    MMPM_CLK_ID_VCODEC,


    MMPM_CLK_ID_VCODEC_MAX,

    MMPM_CLK_ID_VCODEC_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdVcodecType;





typedef enum {
    MMPM_CLK_ID_VPE_NONE,

    MMPM_CLK_ID_VPE,

    MMPM_CLK_ID_VPE_MAX,

    MMPM_CLK_ID_VPE_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdVpeType;





typedef enum {
    MMPM_CLK_ID_VFE_NONE,

    MMPM_CLK_ID_VFE,

    MMPM_CLK_ID_VFE_MAX,

    MMPM_CLK_ID_VFE_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdVfeType;





typedef enum {
    MMPM_CLK_ID_CSI_NONE,

    MMPM_CLK_ID_CSI,


    MMPM_CLK_ID_CSI_VFE,


    MMPM_CLK_ID_CSI_PHY,


    MMPM_CLK_ID_CSI_PHY_TIMER,


    MMPM_CLK_ID_CSI_PIX0,



    MMPM_CLK_ID_CSI_PIX1,


    MMPM_CLK_ID_CSI_RDI0,


    MMPM_CLK_ID_CSI_RDI1,


    MMPM_CLK_ID_CSI_RDI2,


    MMPM_CLK_ID_CSI_MAX,

    MMPM_CLK_ID_CSI_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdCsiType;





typedef enum {
    MMPM_CLK_ID_SENSOR_NONE,

    MMPM_CLK_ID_SENSOR,


    MMPM_CLK_ID_SENSOR_MAX,

    MMPM_CLK_ID_SENSOR_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdSensorType;





typedef enum {
    MMPM_CLK_ID_JPEGD_NONE,

    MMPM_CLK_ID_JPEGD,


    MMPM_CLK_ID_JPEGD_MAX,

    MMPM_CLK_ID_JPEGD_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdJpegdType;





typedef enum {
    MMPM_CLK_ID_JPEGE_NONE,

    MMPM_CLK_ID_JPEGE,


    MMPM_CLK_ID_JPEGE_MAX,

    MMPM_CLK_ID_JPEGE_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdJpegeType;





typedef enum {
    MMPM_CLK_ID_ROTATOR_NONE,

    MMPM_CLK_ID_ROTATOR,


    MMPM_CLK_ID_ROTATOR_MAX,

    MMPM_CLK_ID_ROTATOR_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdRotatorType;





typedef enum {
    MMPM_CLK_ID_TV_NONE,

    MMPM_CLK_ID_TV_ENC,


    MMPM_CLK_ID_TV_DAC,


    MMPM_CLK_ID_TV_MDP,


    MMPM_CLK_ID_TV_HDMI_APP,


    MMPM_CLK_ID_TV_HDMI,


    MMPM_CLK_ID_TV_RGB,

    MMPM_CLK_ID_TV_NPL,

    MMPM_CLK_ID_TV_MAX,

    MMPM_CLK_ID_TV_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdTvType;





typedef enum {
    MMPM_CLK_ID_DSI_NONE,

    MMPM_CLK_ID_DSI,

    MMPM_CLK_ID_DSI_ESC,


    MMPM_CLK_ID_DSI_PIX,


    MMPM_CLK_ID_DSI_BYTE,


    MMPM_CLK_ID_DSI_LVDS,

    MMPM_CLK_ID_DSI_MDP_P2,

    MMPM_CLK_ID_DSI_MAX,

    MMPM_CLK_ID_DSI_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdDsiType;





typedef enum {
    MMPM_CLK_ID_AUDIOIF_NONE,

    MMPM_CLK_ID_AUDIOIF_PCM,


    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_MCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_SCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_MCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_SCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_MCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_SCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_MCLK,


    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_SCLK,


    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_MCLK,


    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_SCLK,


    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_MCLK,


    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_SCLK,


    MMPM_CLK_ID_AUDIOIF_LPASS_SB_REF_CLK,


    MMPM_CLK_ID_AUDIOIF_SPS_SB_REF_CLK,


    MMPM_CLK_ID_AUDIOIF_MAX,

    MMPM_CLK_ID_AUDIOIF_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdAudioIfType;
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum {
    MMPM_CLK_ID_LPASS_NONE = 0,

    MMPM_CLK_ID_LPASS_HWRSP_CORE,

    MMPM_CLK_ID_LPASS_MIDI_CORE,

    MMPM_CLK_ID_LPASS_AVSYNC_XO,

    MMPM_CLK_ID_LPASS_AVSYNC_BT,

    MMPM_CLK_ID_LPASS_AVSYNC_FM,

    MMPM_CLK_ID_LPASS_SLIMBUS_CORE,

    MMPM_CLK_ID_LPASS_AVTIMER_CORE,

    MMPM_CLK_ID_LPASS_ATIME_CORE,

    MMPM_CLK_ID_LPASS_ATIME2_CORE,


    MMPM_CLK_ID_LPASS_ADSP_CORE,

    MMPM_CLK_ID_LPASS_AHB_ROOT,

    MMPM_CLK_ID_LPASS_ENUM_MAX,

    MMPM_CLK_ID_LPASS_FORCE32BITS = 0x7fffffff

} MmpmClkIdLpassType;




typedef enum {
    MMPM_CLK_ID_VCAP_NONE,

    MMPM_CLK_ID_VCAP,

    MMPM_CLK_ID_VCAP_NPL,

    MMPM_CLK_ID_VCAP_MAX,

    MMPM_CLK_ID_VCAP_FORCE32BITS = 0x7FFFFFFF

} MmpmClkIdVcapType;




typedef union {

    MmpmClkId2dGrpType clkId2dGrp;
    MmpmClkId3dGrpType clkId3dGrp;
    MmpmClkIdMdpType clkIdMdp;

    MmpmClkIdVcodecType clkIdVcodec;
    MmpmClkIdVpeType clkIdVpe;

    MmpmClkIdVfeType clkIdVfe;
    MmpmClkIdCsiType clkIdCsi;

    MmpmClkIdSensorType clkIdSensor;
    MmpmClkIdJpegdType clkIdJpegd;
    MmpmClkIdJpegeType clkIdJpege;
    MmpmClkIdRotatorType clkIdRotator;
    MmpmClkIdTvType clkIdTv;
    MmpmClkIdDsiType clkIdDsi;

    MmpmClkIdAudioIfType clkIdAudioIf;
    MmpmClkIdVcapType clkIdVcap;


    MmpmClkIdLpassType clkIdLpass;




} MmpmCoreClkIdType;





typedef enum {
    MMPM_FREQ_AT_LEAST,
    MMPM_FREQ_AT_MOST,
    MMPM_FREQ_CLOSEST,
    MMPM_FREQ_EXACT,
    MMPM_FREQ_MAX,
    MMPM_FREQ_FORCE32BITS = 0x7FFFFFFF

} MmpmFreqMatchType;





typedef struct {
    MmpmCoreClkIdType clkId;



    uint32 clkFreqHz;

    MmpmFreqMatchType freqMatch;



} MmpmClkValType;






typedef struct {
    uint32 numOfClk;

    MmpmClkValType *pClkArray;


} MmpmClkReqType;







typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_NONE,

    MMPM_CLK_DOMAIN_SRC_ID_HDMI0,

    MMPM_CLK_DOMAIN_SRC_ID_HDMI_MAX,

    MMPM_CLK_DOMAIN_SRC_ID_HDMI_FORCE32BITS = 0x7FFFFFFF

} MmpmClkDomainSrcIdHdmiType;





typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_DSI_NONE,

    MMPM_CLK_DOMAIN_SRC_ID_DSI0,

    MMPM_CLK_DOMAIN_SRC_ID_DSI1,

    MMPM_CLK_DOMAIN_SRC_ID_LVDS,

    MMPM_CLK_DOMAIN_SRC_ID_DSI_MAX,

    MMPM_CLK_DOMAIN_SRC_ID_DSI_FORCE32BITS = 0x7FFFFFFF

} MmpmClkDomainSrcIdDsiType;





typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_NONE,

    MMPM_CLK_DOMAIN_SRC_ID_Q6PLL,

    MMPM_CLK_DOMAIN_SRC_ID_AUDIOPLL,

    MMPM_CLK_DOMAIN_SRC_ID_PRIUSPLL,

    MMPM_CLK_DOMAIN_SRC_ID_LPASS_MAX,

    MMPM_CLK_DOMAIN_SRC_ID_LPASS_FORCE32BITS = 0x7FFFFFFF

} MmpmClkDomainSrcIdLpassType;





typedef union {

    MmpmClkDomainSrcIdHdmiType clkDomainSrcIdHdmi;

    MmpmClkDomainSrcIdDsiType clkDomainSrcIdDsi;



    MmpmClkDomainSrcIdLpassType clkDomainSrcIdLpass;



} MmpmClkDomainSrcIdType;




typedef struct {
    MmpmCoreClkIdType clkId;


    uint32 M_val;

    uint32 N_val;

    uint32 n2D_val;

    uint32 div_val;

    uint32 clkFreqHz;


    MmpmClkDomainSrcIdType clkDomainSrc;

} MmpmClkDomainType;





typedef struct {
    uint32 numOfClk;

    MmpmClkDomainType *pClkDomainArray;

} MmpmClkDomainReqType;






typedef enum {
    MMPM_THERMAL_NONE,
    MMPM_THERMAL_LOW,
    MMPM_THERMAL_NORM,

    MMPM_THERMAL_HIGH_L1,
    MMPM_THERMAL_HIGH_L2,
    MMPM_THERMAL_HIGH_L3,
    MMPM_THERMAL_HIGH_L4,
    MMPM_THERMAL_HIGH_L5,
    MMPM_THERMAL_HIGH_L6,
    MMPM_THERMAL_HIGH_L7,
    MMPM_THERMAL_HIGH_L8,
    MMPM_THERMAL_HIGH_L9,
    MMPM_THERMAL_HIGH_L10,
    MMPM_THERMAL_HIGH_L11,
    MMPM_THERMAL_HIGH_L12,
    MMPM_THERMAL_HIGH_L13,
    MMPM_THERMAL_HIGH_L14,
    MMPM_THERMAL_HIGH_L15,
    MMPM_THERMAL_HIGH_L16,
    MMPM_THERMAL_MAX,
    MMPM_THERMAL_FORCE32BITS = 0x7FFFFFFF

} MmpmThermalType;







typedef enum {
    MMPM_BW_USAGE_2D_GRP_NONE,
    MMPM_BW_USAGE_2D_GRP,
    MMPM_BW_USAGE_2D_GRP_MAX,
    MMPM_BW_USAGE_2D_GRP_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsage2dGrpType;





typedef enum {
    MMPM_BW_USAGE_3D_GRP_NONE,
    MMPM_BW_USAGE_3D_GRP,
    MMPM_BW_USAGE_3D_GRP_MAX,
    MMPM_BW_USAGE_3D_GRP_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsage3dGrpType;





typedef enum {
    MMPM_BW_USAGE_MDP_NONE,
    MMPM_BW_USAGE_MDP,
    MMPM_BW_USAGE_MDP_HRES,
    MMPM_BW_USAGE_MDP_MAX,
    MMPM_BW_USAGE_MDP_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageMdpType;





typedef enum {
    MMPM_BW_USAGE_VCODEC_NONE,

    MMPM_BW_USAGE_VCODEC_ENC,

    MMPM_BW_USAGE_VCODEC_DEC,

    MMPM_BW_USAGE_VCODEC_ENC_DEC,

    MMPM_BW_USAGE_VCODEC_MAX,

    MMPM_BW_USAGE_VCODEC_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageVcodecType;





typedef enum {
    MMPM_BW_USAGE_VPE_NONE,
    MMPM_BW_USAGE_VPE,
    MMPM_BW_USAGE_VPE_MAX,
    MMPM_BW_USAGE_VPE_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageVpeType;





typedef enum {
    MMPM_BW_USAGE_VFE_NONE,
    MMPM_BW_USAGE_VFE,
    MMPM_BW_USAGE_VFE_MAX,
    MMPM_BW_USAGE_VFE_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageVfeType;





typedef enum {
    MMPM_BW_USAGE_JPEGD_NONE,
    MMPM_BW_USAGE_JPEGD,
    MMPM_BW_USAGE_JPEGD_MAX,
    MMPM_BW_USAGE_JPEGD_FORCE32BITS = 0x7FFFFFFF


} MmpmBwUsageJpegdType;





typedef enum {
    MMPM_BW_USAGE_JPEGE_NONE,
    MMPM_BW_USAGE_JPEGE,
    MMPM_BW_USAGE_JPEGE_MAX,
    MMPM_BW_USAGE_JPEGE_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageJpegeType;





typedef enum {
    MMPM_BW_USAGE_ROTATOR_NONE,
    MMPM_BW_USAGE_ROTATOR,
    MMPM_BW_USAGE_ROTATOR_MAX,
    MMPM_BW_USAGE_ROTATOR_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageRotatorType;





typedef enum {
    MMPM_BW_USAGE_LPASS_NONE,
    MMPM_BW_USAGE_LPASS_DSP,
    MMPM_BW_USAGE_LPASS_DMA,
    MMPM_BW_USAGE_LPASS_EXT_CPU,

    MMPM_BW_USAGE_LPASS_ENUM_MAX,
    MMPM_BW_USAGE_LPASS_FORCE32BITS = 0x7FFFFFFF

} MmpmBwUsageLpassType;





typedef union {

    MmpmBwUsage2dGrpType bwUsage2dGrp;
    MmpmBwUsage3dGrpType bwUsage3dGrp;
    MmpmBwUsageMdpType bwUsageMdp;
    MmpmBwUsageVcodecType bwUsageVcodec;
    MmpmBwUsageVpeType bwUsageVpe;
    MmpmBwUsageVfeType bwUsageVfe;
    MmpmBwUsageJpegdType bwUsageJpegd;
    MmpmBwUsageJpegeType bwUsageJpege;
    MmpmBwUsageRotatorType bwUsageRotator;


    MmpmBwUsageLpassType bwUsageLpass;



} MmpmCoreBwUsageType;




typedef enum {
    MMPM_BW_PORT_ID_NONE = 0,
    MMPM_BW_PORT_ID_ADSP_MASTER,
    MMPM_BW_PORT_ID_DML_MASTER,
    MMPM_BW_PORT_ID_AIF_MASTER,
    MMPM_BW_PORT_ID_SLIMBUS_MASTER,
    MMPM_BW_PORT_ID_MIDI_MASTER,
    MMPM_BW_PORT_ID_HWRSMP_MASTER,
    MMPM_BW_PORT_ID_EXT_AHB_MASTER,

    MMPM_BW_PORT_ID_SPDIF_MASTER,
    MMPM_BW_PORT_ID_HDMIRX_MASTER,
    MMPM_BW_PORT_ID_HDMITX_MASTER,
    MMPM_BW_PORT_ID_SIF_MASTER,
    MMPM_BW_PORT_ID_DML_SLAVE,
    MMPM_BW_PORT_ID_AIF_SLAVE,
    MMPM_BW_PORT_ID_SLIMBUS_SLAVE,
    MMPM_BW_PORT_ID_MIDI_SLAVE,
    MMPM_BW_PORT_ID_HWRSMP_SLAVE,
    MMPM_BW_PORT_ID_AVSYNC_SLAVE,
    MMPM_BW_PORT_ID_LPM_SLAVE,
    MMPM_BW_PORT_ID_SRAM_SLAVE,
    MMPM_BW_PORT_ID_EXT_AHB_SLAVE,
    MMPM_BW_PORT_ID_DDR_SLAVE,
    MMPM_BW_PORT_ID_OCMEM_SLAVE,
    MMPM_BW_PORT_ID_PERIFNOC_SLAVE,
    MMPM_BW_PORT_ID_SPDIF_SLAVE,
    MMPM_BW_PORT_ID_HDMIRX_SLAVE,
    MMPM_BW_PORT_ID_HDMITX_SLAVE,
    MMPM_BW_PORT_ID_SIF_SLAVE,
    MMPM_BW_PORT_ID_BSTC_SLAVE,
    MMPM_BW_PORT_ID_DCODEC_SLAVE,
    MMPM_BW_PORT_ID_CORE,



    MMPM_BW_PORT_ID_MAX,
    MMPM_BW_PORT_ID_FORCE32BITS = 0x7FFFFFFF

} MmpmBwPortIdType;






typedef struct {

    uint32 memPhyAddr;

    uint32 axiPort;



    uint32 bwBytePerSec;



    uint32 usagePercentage;

    MmpmCoreBwUsageType bwUsageType;


} MmpmBwValType;





typedef struct {
    uint32 numOfBw;

    MmpmBwValType *pBandWidthArray;

} MmpmBwReqType;







typedef struct{
    MmpmBwPortIdType masterPort;




    MmpmBwPortIdType slavePort;



} MmpmBusRouteType;




typedef struct{
    uint64 Ab;
    uint64 Ib;
} MmpmBusBWDataIbAbType;




typedef struct{
    uint64 bwBytePerSec;

    uint32 usagePercentage;

    MmpmBwUsageLpassType usageType;



} MmpmBusBWDataUsageType;





typedef union{
    MmpmBusBWDataIbAbType busBwAbIb;

    MmpmBusBWDataUsageType busBwValue;

} MmpmBusBWDataType;





typedef struct{
    MmpmBusRouteType busRoute;

    MmpmBusBWDataType bwValue;

} MmpmGenBwValType;





typedef struct {
    uint32 numOfBw;


    MmpmGenBwValType *pBandWidthArray;

} MmpmGenBwReqType;





typedef enum {
    MMPM_MEM_POWER_NONE,
    MMPM_MEM_POWER_OFF,
    MMPM_MEM_POWER_RETENTION,
    MMPM_MEM_POWER_ACTIVE,
    MMPM_MEM_POWER_MAX,
    MMPM_MEM_POWER_FORCE32BITS = 0x7FFFFFFF

} MmpmMemPowerStateType;




typedef enum{
    MMPM_MEM_NONE,
    MMPM_MEM_OCMEM,
    MMPM_MEM_LPASS_LPM,
    MMPM_MEM_SRAM,
    MMPM_MEM_MAX,
    MMPM_MEM_FORCE32BITS = 0x7FFFFFFF

} MmpmMemIdType;




typedef struct{
    MmpmMemIdType memory;




    MmpmMemPowerStateType powerState;



}MmpmMemPowerReqParamType;





typedef struct {
    uint32 gpioId;

    boolean configGpio;

    uint32 gpioVoltageSource;

    boolean gpioModeOnOff;

    uint32 gpioModeSelect;

    uint32 gpioOutBufferConfig;

    boolean invertExtPin;

    uint32 gpioCurrentSourcePulls;

    uint32 gpioOutBufferDriveStrength;

    uint32 gpioDtestBufferOnOff;

    uint32 gpioExtPinConfig;

    uint32 gpioSourceConfig;

    boolean interrupPolarity;

    uint32 uartPath;

} MmpmPmicGpioParamType;





typedef enum {
     MMPM_PMIC_CONFIG,

     MMPM_PMIC_CONFIG_BIAS_VOLTAGE,

     MMPM_PMIC_CONFIG_DIGITAL_INPUT,

     MMPM_PMIC_CONFIG_DIGITAL_OUTPUT,

     MMPM_PMIC_CONFIG_SET_VOLTAGE_SOURCE,

     MMPM_PMIC_CONFIG_MODE_SELECTION,

     MMPM_PMIC_CONFIG_SET_OUTPUT_BUFFER,

     MMPM_PMIC_CONFIG_SET_INVERSION,

     MMPM_PMIC_CONFIG_SET_CURRENT_SOURCE_PULLS,

     MMPM_PMIC_CONFIG_SET_EXT_PIN,

     MMPM_PMIC_CONFIG_SET_OUTPUT_BUF_DRIVE_STRG,

     MMPM_PMIC_CONFIG_SET_SOURCE,

     MMPM_PMIC_CONFIG_SET_INT_POLARITY,

     MMPM_PMIC_CONFIG_SET_MUX_CTRL,

} MmpmPmicGpioConfigType;
# 1542 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    MmpmPmicGpioConfigType configId;

    MmpmPmicGpioParamType gpioParam;

} MmpmPmicGpioReqType;




typedef enum {
     MMPM_RESET_NONE,
     MMPM_RESET_DEASSERT,
     MMPM_RESET_ASSERT,
     MMPM_RESET_PULSE,
     MMPM_RESET_ENUM_MAX,
     MMPM_RESET_ENUM_FORCE32BITS = 0x7FFFFFFF

} MmpmResetType;






typedef struct {
    MmpmCoreClkIdType clkId;

    MmpmResetType resetType;

} MmpmResetParamType;




typedef struct {
    uint32 numOfReset;

    MmpmResetParamType *pResetParamArray;

} MmpmResetReqType;





typedef enum {
    MMPM_MIPS_REQUEST_NONE = 0,

    MMPM_MIPS_REQUEST_CPU_CLOCK_ONLY,


    MMPM_MIPS_REQUEST_CPU_CLOCK_AND_BW,




    MMPM_MIPS_REQUEST_ENUM_MAX,

    MMPM_MIPS_REQUEST_FORCE32BITS = 0x7FFFFFFF

} MmpmMipsRequestFnType;




typedef struct {
    uint32 mipsTotal;

    uint32 mipsPerThread;

    MmpmBwPortIdType codeLocation;



    MmpmMipsRequestFnType reqOperation;



} MmpmMipsReqType;




typedef struct {
    uint32 mppsTotal;

    uint32 adspFloorClock;

} MmpmMppsReqType;
# 1642 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    uint32 numOfClk;

    MmpmCoreClkIdType *pClkIdArray;

} MmpmClkIdArrayParamType;
# 1732 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef union {

    uint32 vregMilliVolt;







    MmpmRegProgMatchType regProgMatch;







    MmpmPmicGpioReqType *pPmicGpio;







    MmpmClkReqType *pCoreClk;







    MmpmClkDomainReqType *pCoreClkDomain;







    MmpmBwReqType *pBwReq;







    MmpmGenBwReqType *pGenBwReq;







    uint32 sleepMicroSec;







    uint32 mips;
# 1809 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
    MmpmResetReqType *pResetReq;







    MmpmMipsReqType *pMipsExt;







    MmpmMppsReqType *pMppsReq;







    MmpmThermalType thermalMitigation;







    MmpmMemPowerReqParamType *pMemPowerState;
# 1850 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
    MmpmClkIdArrayParamType *pRelClkIdArray;
# 1860 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
    MmpmCoreClkIdType gateClkId;






}MmpmRscParamStructType;
# 1876 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    MmpmRscIdType rscId;




    MmpmRscParamStructType rscParam;

} MmpmRscParamType;
# 1897 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum{
     MMPM_API_TYPE_NONE,
     MMPM_API_TYPE_SYNC,
     MMPM_API_TYPE_ASYNC,
     MMPM_API_TYPE_ENUM_MAX,
     MMPM_API_TYPE_FORCE32BITS = 0x7FFFFFFF


} MmpmApiType;




typedef struct {
    MmpmApiType apiType;




    uint32 numOfReq;


    MmpmRscParamType *pReqArray;


    MMPM_STATUS *pStsArray;


    uint32 reqTag;


    void *pExt;


} MmpmRscExtParamType;
# 1942 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum {
    MMPM_INFO_ID_NONE,



    MMPM_INFO_ID_CORE_CLK,



    MMPM_INFO_ID_CORE_CLK_MAX,



    MMPM_INFO_ID_CORE_CLK_MAX_SVS,


    MMPM_INFO_ID_CORE_CLK_MAX_NOMINAL,


    MMPM_INFO_ID_CORE_CLK_MAX_TURBO,




    MMPM_INFO_ID_MIPS_MAX,

    MMPM_INFO_ID_BW_MAX,



    MMPM_INFO_ID_CRASH_DUMP,

    MMPM_INFO_ID_POWER_SUPPORT,



    MMPM_INFO_ID_CLK_FREQ,



    MMPM_INFO_ID_PERFMON,

    MMPM_INFO_ID_PMIC_GPIO,

    MMPM_INFO_ID_SET_DEBUG_LEVEL,

    MMPM_INFO_ID_MMSS_BUS,

    MMPM_INFO_ID_LPASS_BUS,



    MMPM_INFO_ID_AGGREGATE_CLIENT_CLASS,

    MMPM_INFO_ID_MPPS,

    MMPM_INFO_ID_BW_EXT,

    MMPM_INFO_ID_DCVS_STATE,


    MMPM_INFO_ID_EXT_REQ,


    MMPM_INFO_ID_MAX,

    MMPM_INFO_ID_FORCE32BITS = 0x7FFFFFFF

} MmpmInfoIdType;






typedef struct {
    MmpmCoreClkIdType clkId;

    uint32 clkFreqHz;

} MmpmInfoClkType;
# 2034 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    uint32 clkId;

    uint32 clkFreqHz;

    uint32 forceMeasure;




} MmpmInfoClkFreqType;

typedef struct
{
    uint32 clientClasses;



    uint32 aggregateMpps;


} MmpmInfoMppsType;

typedef struct
{
    MmpmBusRouteType busRoute;

    uint64 totalBw;


} MmpmInfoBwExtType;
# 2100 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    uint32 masterMeasureArray[7];


    uint32 measurementConfig[7];


    uint32 latencyMasterPort;

    uint32 holdoffTime;

    uint32 triggerMode;


    void *pDataMsg;

    char pFilename[64];

    uint32 axiClockFreq;


    uint32 clockInfo[10];


    uint32 isClockFreqCalc[10];



} MmpmInfoPerfmonType;




typedef struct {
    uint32 gpioModeSelect;

    uint32 gpioVoltageSource;

    uint32 gpioModeOnOff;

    uint32 gpioOutBufferConfig;

    uint32 gpioOutBufferDriveStrength;

    uint32 gpioCurrentSourcePulls;

    uint32 gpioSourceConfig;

    uint32 gpioDtestBufferOnOff;

    uint32 gpioExtPinConfig;

} MmpmPmicGpioStatusType;





typedef struct {
    uint32 gpioId;
    MmpmPmicGpioStatusType gpioSts;

} MmpmPmicGpioInfoType;
# 2187 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef struct {
    uint32 clientId;

    char clientName[32];

    char fabClientName[32];

    uint32 uIbBytePerSec;


    uint32 uAbBytePerSec;


} MmpmBwClientType;




typedef struct {
    uint32 coreId;

    uint32 instanceId;

    uint32 axiPort;

    uint32 slaveId;

    uint32 uIbBytePerSec;


    uint32 uAbBytePerSec;


    uint32 numOfClients;

    MmpmBwClientType client[16];

} MmpmMasterSlaveType;




typedef struct {
    uint32 mmFabClkFreq;

    uint32 appsFabClkFreq;

    uint32 sysFabClkFreq;

    uint32 lpassFabClkFreq;

    uint32 ebiClkFreq;

    uint32 mmImemClkFreq;

    uint32 numOfMasterSlave;

    MmpmMasterSlaveType masterSlave[16];

} MmpmFabStatusInfoType;






typedef union {

    MmpmInfoClkType *pInfoClk;




    boolean bInfoPower;



    MmpmInfoPerfmonType *pInfoPerfmon;




    MmpmInfoClkFreqType *pInfoClkFreqType;





    MmpmPmicGpioInfoType *pPmicGpioStatus;

    uint32 infoSetLotLevel;

    MmpmFabStatusInfoType *pFabStatus;



    uint32 mipsValue;





    uint64 bwValue;





    uint32 aggregateClientClass;







    MmpmInfoMppsType *pMppsInfo;



    MmpmInfoBwExtType *pBwExtInfo;



    boolean dcvsState;







    void *pExtInfo;





} MmpmInfoDataStructType;





typedef struct {
    MmpmInfoIdType infoId;
# 2347 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
    MmpmCoreIdType coreId;




    MmpmCoreInstanceIdType instanceId;





    MmpmInfoDataStructType info;


} MmpmInfoDataType;
# 2370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
typedef enum{
    MMPM_PARAM_ID_NONE = 0,



    MMPM_PARAM_ID_RESOURCE_LIMIT,



    MMPM_PARAM_ID_CLIENT_OCMEM_MAP,

    MMPM_PARAM_ID_MEMORY_MAP,

    MMPM_PARAM_ID_CLIENT_CLASS,

    MMPM_PARAM_ID_L2_CACHE_LINE_LOCK,

    MMPM_PARAM_ID_DCVS_PARTICIPATION,

    MMPM_PARAM_ID_ENUM_MAX,

    MMPM_PARAM_ID_Force32bit = 0x7fffffff

} MmpmParameterIdType;





typedef struct
{
    MmpmMemIdType memory;
    uint64 startAddress;

    uint32 size;
} MmpmMemoryMapType;







typedef struct
{
    void* startAddress;

    uint32 size;


    uint32 throttleBlockSize;



    uint32 throttlePauseUs;


} MmpmL2CacheLineLockParameterType;


typedef enum
{
    MMPM_UNKNOWN_CLIENT_CLASS = 0x00,
    MMPM_AUDIO_CLIENT_CLASS = 0x01,
    MMPM_VOICE_CLIENT_CLASS = 0x02,
    MMPM_COMPUTE_CLIENT_CLASS = 0x04,
    MMPM_STREAMING_1HVX_CLIENT_CLASS = 0x08,

    MMPM_STREAMING_2HVX_CLIENT_CLASS = 0x10,

} MmpmClientClassType;



typedef enum
{
    MMPM_DCVS_ADJUST_UP_DOWN,
    MMPM_DCVS_ADJUST_ONLY_UP
} MmpmDcvsEnableOptionsType;


typedef struct
{
    boolean enable;

    MmpmDcvsEnableOptionsType enableOpt;


} MmpmDcvsParticipationType;




typedef struct{
    MmpmParameterIdType paramId;




    void* pParamConfig;
# 2482 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
} MmpmParameterConfigType;




typedef struct{
    uint32 regionId;

    uint32 numKeys;

    uint32 *pKey;

} MmpmOcmemMapRegionDescType;




typedef struct{
    uint32 numRegions;

    MmpmOcmemMapRegionDescType *pRegions;


} MmpmOcmemMapType;
# 2537 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
uint32 MMPM_Init(char * cmd_line);
# 2571 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
uint32 MMPM_Register_Ext(MmpmRegParamType *pRegParam);
# 2596 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Deregister_Ext(uint32 clientId);
# 2646 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Request(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
# 2679 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Release(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
# 2717 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Pause(uint32 clientId,
                           MmpmRscParamType *pMmpmRscParam);
# 2746 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Resume(uint32 clientId,
                            MmpmRscParamType *pMmpmRscParam);
# 2805 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Request_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
# 2838 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Release_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
# 2874 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Pause_Ext(uint32 clientId,
                               MmpmRscExtParamType *pRscExtParam);
# 2910 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_Resume_Ext(uint32 clientId,
                                MmpmRscExtParamType *pRscExtParam);
# 2941 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_GetInfo(MmpmInfoDataType *pInfoData);
# 2969 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/mmpm.h"
MMPM_STATUS MMPM_SetParameter(uint32 clientId, MmpmParameterConfigType *pParamConfig);
# 24 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_error.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h" 1
# 24 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_consts.h" 1
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h" 2
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
# 96 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,


        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,



        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,



        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,

        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;


typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;



typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;


typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;


typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;


typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;



struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
# 186 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef struct {

    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;

} qurt_mem_region_attr_t;



typedef struct {

    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];

} qurt_mem_pool_attr_t;



typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;



typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h" 2
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
extern qurt_mem_pool_t qurt_mem_default_pool;
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
# 106 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
# 160 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
# 202 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
# 221 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
# 291 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
# 340 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
# 420 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_delete(qurt_mem_region_t region);
# 442 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
# 466 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
# 486 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
# 506 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
# 527 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
# 546 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
# 566 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
# 585 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
# 635 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
# 655 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
# 703 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
# 724 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}

void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
# 782 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 813 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
# 838 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 859 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
# 877 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
# 896 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
# 916 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
# 950 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 979 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
# 1004 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 1026 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
# 1044 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
# 1081 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
# 1111 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
# 1123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
void qurt_l2fetch_disable(void);
# 1138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
# 1156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h" 2
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
typedef enum
{
    AsicRsc_None = 0x0,
    AsicRsc_Power_Mem = 0x1,
    AsicRsc_Power_ADSP = 0x2,
    AsicRsc_Power_Core = 0x4,
    AsicRsc_MIPS_Clk = 0x8,
    AsicRsc_MIPS_BW = 0x10,
    AsicRsc_BW_Internal = 0x20,
    AsicRsc_BW_External = 0x40,
    AsicRsc_Core_Clk = 0x80,
    AsicRsc_Latency = 0x100,
    AsicRsc_Thermal = 0x200,
    AsicRsc_Power_Ocmem = 0x400,
    AsicRsc_Power_Hvx = 0x800,
    AsicRsc_EnumForce32bit = 0x7fffffff
} AsicHwRscIdType;






typedef void (*Asic_Rsc_AggregationFnType)(void *pInputData, void *pOutputData);







typedef enum
{
    AsicClk_TypeNone = 0,
    AsicClk_TypeNpa,
    AsicClk_TypeDalFreqSet,
    AsicClk_TypeDalEnable,
    AsicClk_TypeInternalCGC,


    AsicClk_TypeCBC,




    AsicClk_TypeDalDomainSrc
} AsicClkTypeType;




typedef enum
{
    AsicClk_CntlNone = 0,
    AsicClk_CntlOff,
    AsicClk_CntlAlwaysON,
    AsicClk_CntlAlwaysON_DCG,

    AsicClk_CntlSW,
    AsicClk_CntlSW_DCG

} AsicClkCntlTypeType;




typedef enum
{
    AsicClk_MemCntlNone = 0,
    AsicClk_MemCntlAlwaysRetain,
} AsicClkMemRetentionType;

typedef struct
{
    uint32 baseAddr;
    uint32 physAddr;
    uint32 size;
} AsicHwioRegRangeType;




typedef struct
{
    uint32 regOffset;
    uint32 enableMask;
    uint32 hwctlMask;
    uint32 stateMask;
} AsicClkHwIoInfoType;






typedef struct
{
    AdsppmClkIdType clkId;
    AsicClkTypeType clkType;
    AsicClkCntlTypeType clkCntlType;
    AsicClkMemRetentionType clkMemCntlType;
    union
    {
        AsicClkHwIoInfoType hwioInfo;
        char clkName[64];
    } clkInfo;
    AdsppmClkIdType clkSrcId;
    AdsppmMemIdType memoryId;
} AsicClkDescriptorType;





typedef struct
{
    const uint32 numClocks;
    const AdsppmClkIdType *pClocks;
} AsicClockArrayType;




typedef struct
{
    AdsppmBusPortIdType adsppmMaster;
    ICBId_MasterType icbarbMaster;
} AsicBusExtMasterType;

typedef struct
{
    AdsppmBusPortIdType adsppmSlave;
    ICBId_SlaveType icbarbSlave;
} AsicBusExtSlaveType;

typedef struct
{
    AsicBusExtMasterType masterPort;
    AsicBusExtSlaveType slavePort;
} AsicBusExtRouteType;






typedef enum
{
    AsicBusPort_None = 0,
    AsicBusPort_AhbE_M = 0x1,
    AsicBusPort_AhbE_S = 0x2,
    AsicBusPort_AhbI_M = 0x4,
    AsicBusPort_AhbI_S = 0x8,
    AsicBusPort_AhbX_M = 0x10,
    AsicBusPort_AhbX_S = 0x20,
    AsicBusPort_Ahb_Any = 0x3F,
    AsicBusPort_Ext_M = 0x40,
    AsicBusPort_Ext_S = 0x80,
    AsicBusPort_Ext_Any = 0xC0,
    AsicBusPort_Any = 0xFF
} AsicBusPortConnectionType;




typedef struct
{
    AdsppmBusPortIdType port;
    AsicBusPortConnectionType portConnection;
    AdsppmClkIdType busClk;
    AsicClockArrayType regProgClocks;
    union
    {
        ICBId_MasterType icbarbMaster;
        ICBId_SlaveType icbarbSlave;
    } icbarbId;
    AdsppmBusPortIdType accessPort;
} AsicBusPortDescriptorType;





typedef struct
{
    const uint32 numBusPorts;
    const AdsppmBusPortIdType *pBusPorts;
} AsicBusPortArrayType;

typedef struct
{
    uint64 bwThreshold;
    uint64 bwVote;
    uint32 latencyVote;
    uint32 snocFloorVoteMhz;
    uint32 honestFlag;
} AsicCompensatedDdrBwTableEntryType;




typedef enum
{
    AsicPowerDomain_AON = 0,
    AsicPowerDomain_Adsp = 1,
    AsicPowerDomain_LpassCore = 2,
    AsicPowerDomain_Lpm = 3,
    AsicPowerDomain_SRam = 4,
    AsicPowerDomain_Ocmem = 5,
    AsicPowerDomain_Hvx = 6,
    AsicPowerDomain_EnumMax
} AsicPowerDomainType;

typedef struct
{
    AsicPowerDomainType pwrDomain;
    char pwrDomainName[64];
    AsicHwRscIdType pwrDomainType;
    AdsppmClkIdType clkId;
    DalIPCIntInterruptType intrReinitTrigger;
    uint32 intrReinitDone;
    AsicClockArrayType securityClocks;
} AsicPowerDomainDescriptorType;







typedef struct
{
    AdsppmCoreIdType coreId;
    AsicHwRscIdType hwResourceId[Adsppm_Rsc_Id_Max];
    AsicPowerDomainType pwrDomain;
    AsicClockArrayType coreClockInstances;
    AsicBusPortArrayType masterBusPortInstances;
    AsicBusPortArrayType slaveBusPortInstances;
    AdsppmInstanceIdType numInstances;


} AsicCoreDescType;




typedef struct
{
    uint64 startAddr;
    uint32 size;
} AsicAddressRangeType;




typedef struct
{
    AdsppmMemIdType memId;
    AsicPowerDomainType pwrDomain;
} AsicMemDescriptorType;




typedef struct
{
    const AsicMemDescriptorType *pDescriptor;
    AsicAddressRangeType virtAddr;
} AsicMemDescriptorFullType;





typedef struct
{
    MmpmClientClassType aggregateClass;
    qurt_cache_partition_size_t mainPartSize;
} AsicCachePartitionConfigType;

typedef struct
{
    const uint32 numEntries;
    const AsicCachePartitionConfigType* pConfigEntries;
} AsicCachePartitionConfigTableType;




typedef enum
{
    AsicFeatureId_Adsp_Clock_Scaling,
    AsicFeatureId_Adsp_LowTemp_Voltage_Restriction,
    AsicFeatureId_Adsp_PC,
    AsicFeatureId_Ahb_Scaling,
    AsicFeatureId_Ahb_Sw_CG,
    AsicFeatureId_Ahb_DCG,
    AsicFeatureId_LpassCore_PC,
    AsicFeatureId_LpassCore_PC_TZ_Handshake,
    AsicFeatureId_Bus_Scaling,
    AsicFeatureId_CoreClk_Scaling,
    AsicFeatureId_Min_Adsp_BW_Vote,
    AsicFeatureId_InitialState,
    AsicFeatureId_TimelineOptimisationMips,
    AsicFeatureId_TimelineOptimisationBw,
    AsicFeatureId_TimelineOptimisationAhb,
    AsicFeatureId_LpassClkSleepOptimization,
    AsicFeatureId_LPMRetention,
    AsicFeatureId_DomainCoreClocks,
    AsicFeatureId_CachePartitionControl,
    AsicFeatureId_DcvsControl,
    AsicFeatureId_AhbeScaling,
    AsicFeatureId_CacheSizeBWScaling,
    AsicFeatureId_enum_max,
    AsicFeatureId_force_32bit = 0x7FFFFFFF
} AsicFeatureIdType;

typedef enum
{
    AsicFeatureState_Disabled,
    AsicFeatureState_Enabled,
    AsicFeatureState_Limited
} AsicFeatureStateType;

typedef struct
{
    AsicFeatureStateType state;
    uint64 min;
    uint64 max;
} AsicFeatureDescType;




Adsppm_Status ACM_Init(void);

uint32 ACM_GetDebugLevel(void);

AsicHwRscIdType ACM_GetHwRscId(AdsppmCoreIdType, AdsppmRscIdType);






Adsppm_Status ACM_BusBWAggregate(AdsppmBusBWDataIbAbType *pAggregateBwIbAbValue, AdsppmBusBWDataType *pBwValue);

Adsppm_Status ACM_GetBWFromMips(AdsppmMIPSToBWAggregateType *pMipsAggregateData);

Adsppm_Status ACM_GetClockFromMips(AdsppmMIPSToClockAggregateType *pMipsAggregateData);

Adsppm_Status ACM_GetClockFromBW(uint32 *pClock, AdsppmBusBWDataIbAbType *pAHBBwData);

void ACM_GetCompensatedDdrBwTableEntry( uint64 vote, AsicCompensatedDdrBwTableEntryType *pTableEntry);

void ACM_ApplyAdspDdrBwConcurrencyFactor(uint32 numVotes, uint64 maxBw, uint64* pAb, uint64* pIb);

uint32 ACM_GetMipsFromMpps(uint32 mppsTotal, uint32 numDominantThreads);

Adsppm_Status ACM_ApplyAhbBwCompensation(uint64 bwVoteIn, uint64* pBwVoteOut);

Adsppm_Status ACM_GetAhbeFromAdspFreq(uint32 adspClockFreq, uint32* ahbClockFreq);

Adsppm_Status ACM_GetBWScalingFactorFromCacheSize(uint32 cachesize, uint32* scalingfactor);





int ACMBus_GetNumberOfExtRoutes(void);





void ACMBus_GetExtRoutes(AsicBusExtRouteType *pExtRoutes);
# 440 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
AdsppmClkIdType ACMClk_GetInstanceCoreClockId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmClkIdType coreClock);
# 450 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
AdsppmBusPortIdType ACMBus_GetInstanceMasterBusPortId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmBusPortIdType masterBusPort);
# 460 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
AdsppmBusPortIdType ACMBus_GetInstanceSlaveBusPortId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmBusPortIdType slaveBusPort);







AdsppmBusPortIdType ACMBus_GetCoreSlavePort(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId);






const AsicBusPortDescriptorType *ACMBus_GetPortDescriptor(AdsppmBusPortIdType);







AsicBusPortConnectionType ACMBus_GetPortConnections(AdsppmBusPortIdType port);






AsicClkTypeType ACMClk_GetClockType(AdsppmClkIdType);






const AsicClkDescriptorType *ACMClk_GetClockDescriptor(AdsppmClkIdType);






const AsicPowerDomainDescriptorType *ACMClk_GetPwrDomainDescriptor(AsicPowerDomainType);






AsicPowerDomainType ACMPwr_GetPowerDomain(AdsppmCoreIdType);






AsicPowerDomainType ACMPwr_GetMemPowerDomain(AdsppmMemIdType mem);






AsicHwRscIdType ACMPwr_GetMemPowerType(AdsppmMemIdType mem);






const AsicCoreDescType *ACM_GetCoreDescriptor(AdsppmCoreIdType);




uint32 ACM_GetTZSecureInterrupt(void);




AsicFeatureStateType ACM_GetFeatureStatus(AsicFeatureIdType);




AsicFeatureDescType *ACM_GetFeatureDescriptor(AsicFeatureIdType featureId);







uint64 ACM_AdjustParamValue(AsicFeatureIdType featureId, uint64 value);






uint64 ACM_ms_to_sclk(uint64 ms);






uint64 ACM_us_to_qclk(uint64 us);





const int ACMBus_GetNumberOfMipsBwRoutes(void);





const AdsppmBusRouteType *ACMBus_GetMipsBwRoutes(void);





uint32 ACM_GetHwThreadNumber(void);





uint64 ACM_GetLprVoteTimeoutValue(void);





AsicHwioRegRangeType *ACM_GetLpassRegRange(void);





AsicHwioRegRangeType *ACM_GetL2ConfigRegRange(void);






AsicMemDescriptorFullType *ACM_GetMemoryDescriptor(AdsppmMemIdType mem);






AsicAddressRangeType *ACM_GetVirtMemAddressRange(AdsppmMemIdType mem);
# 628 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/inc/asic.h"
AdsppmStatusType ACM_SetVirtMemAddressRange(AdsppmMemIdType mem, uint64 addr, uint32 size);

AsicCachePartitionConfigTableType* ACM_GetCachePartitionConfig();
# 16 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/src/asic_internal.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/asic/src/asic_internal.h"
typedef struct
{
    const int numElements;
    const AsicCoreDescType *pCores;
} AsicCoreDescriptorArrayType;

typedef struct
{
    int numElements;
    AsicMemDescriptorFullType pMemories[Adsppm_Mem_Max];
} AsicMemDescriptorFullArrayType;

typedef struct
{
    const int numElements;
    const AsicMemDescriptorType *pPwrDomainDesc;
} AsicMemDescriptorArrayType;

typedef struct
{
    const int numElements;
    const AsicClkDescriptorType *pClocks;
} AsicClockDescriptorArrayType;

typedef struct
{
    const int numElements;
    const AsicBusPortDescriptorType *pPorts;
} AsicBusPortDescriptorArrayType;

typedef struct
{
    const int numElements;
    const AdsppmBusRouteType *pRoutes;
} AsicBusRouteArrayType;

typedef struct
{
    const int numElements;
    const uint64 *pRegProgSpeeds;
} AsicRegProgSpeedArrayType;

typedef struct
{
    const int numElements;
    const AsicPowerDomainDescriptorType *pPwrDomains;
} AsicPwrDescriptorArrayType;

typedef struct
{
    const int numElements;
    const AsicCompensatedDdrBwTableEntryType *pRows;
} AsicCompensatedDdrBwTableType;

typedef struct
{
    uint64 bwThreshold;
    uint64 bwVote;
} AsicCompensatedAhbBwTableEntryType;

typedef struct
{
    const int numElements;
    const AsicCompensatedAhbBwTableEntryType *pRows;
} AsicCompensatedAhbBwTableType;

typedef struct
{


    uint32 adspDdrConcurrencyVotersThreshold;


    uint32 adspDdrConcurrencyFactorArray[3];
} AsicBwConcurrencySettingsType;

typedef struct
{
    uint32 oneThreadActive;
    uint32 twoThreadActive;
    uint32 threeThreadActive;
    uint32 fourThreadActive;
} AsicThreadLoadingInfoType;

typedef struct
{
    const int numElements;
    const AsicThreadLoadingInfoType *pRows;
} AsicThreadLoadingInfoTableType;

typedef struct
{
    const int numElements;
    const uint32 *pData;
} AsicAudioVoiceCppTrendFactorsType;

typedef struct
{
    uint32 adspFreqHz;
    uint32 ahbeFreqHz;
} AsicAdspToAhbeFreqTableEntry;

typedef struct
{
    uint32 cachesize;
    uint32 scalingfactor;
} AsicAdspCacheSizeBWScalingTableEntry;

typedef struct
{
    const int numElements;
    const AsicAdspCacheSizeBWScalingTableEntry *pRows;
} AsicAdspCacheSizeBWScalingTableType;


typedef struct
{
    const int numElements;
    const AsicAdspToAhbeFreqTableEntry *pRows;
} AsicAdspToAhbeFreqTableType;




typedef enum
{
    AsicFnSetId_Default = 0
} AsicFnSetIdType;

typedef struct
{
    AsicFnSetIdType functionSet;
    uint32 debugLevel;
    uint32 adspHwThreadNumber;
    uint64 adsppmLprTimeoutValue;
    uint32 chipVersion;
    AsicHwioRegRangeType *lpassRegRange;
    AsicHwioRegRangeType *l2ConfigRegRange;
    AsicCoreDescriptorArrayType *cores;
    AsicMemDescriptorFullArrayType memories;
    AsicClockDescriptorArrayType *clocks;
    AsicBusPortDescriptorArrayType *busPorts;
    AsicBusRouteArrayType *extBusRoutes;
    AsicBusRouteArrayType *mipsBusRoutes;
    AsicRegProgSpeedArrayType *regProgSpeeds;
    AsicPwrDescriptorArrayType *pwrDomains;
    AsicCompensatedDdrBwTableType *compensatedDdrBwTable;
    AsicCompensatedAhbBwTableType *compensatedAhbBwTable;
    AsicThreadLoadingInfoTableType *threadLoadingInfoTable;
    AsicAudioVoiceCppTrendFactorsType *audioVoiceCppFactors;
    AsicCachePartitionConfigTableType *cachePartitionConfigTable;
    AsicBwConcurrencySettingsType* bwConcurrencySettings;
    AsicAdspToAhbeFreqTableType* adspToAhbeFreqTable;
    AsicAdspCacheSizeBWScalingTableType* cacheSizeBWscalingTable;
    Adsppm_Status (*pFn_GetBWFromMips)(AdsppmMIPSToBWAggregateType *pMipsAggregateData);
    Adsppm_Status (*pFn_GetClockFromMips)(AdsppmMIPSToClockAggregateType *pMipsAggregateData);
    Adsppm_Status (*pFn_GetClockFromBW)(uint32 *pClocks, AdsppmBusBWDataIbAbType *pAHBBwData);
    Adsppm_Status (*pFn_BusBWAggregate)(AdsppmBusBWDataIbAbType *pAggregateBwIbAbValue, AdsppmBusBWDataType *pBwValue);
    AsicFeatureDescType features[AsicFeatureId_enum_max];
} AsicConfigType;

typedef struct
{
    uint32 numClients;
    uint32 factor;
} AsicFactorType;






Adsppm_Status asicGetAsicConfig(AsicConfigType *pConfig);
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h" 1
# 36 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/src/core_internal.h" 1
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/src/common/core/src/core_internal.h"
typedef struct{
    DALSYSSyncHandle resourceLock[Adsppm_Rsc_Id_Max - 1];
    coreUtils_Q_Type rscReqQ[Adsppm_Rsc_Id_Max - 1];


    uint32 periodicUseCase;
} AdsppmCoreCtxType;






AdsppmCoreCtxType *GetAdsppmCoreContext(void);






uint32 IsPeriodicUseCase(void);






void SetPeriodicUseCase(uint32 periodic);
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h" 1
# 24 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
typedef void * ULogHandle;
typedef int32 ULogResult;
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
typedef enum
{
  ULOG_ERR_INVALIDNAME = 1,
  ULOG_ERR_INVALIDPARAMETER,
  ULOG_ERR_ALREADYCONNECTED,
  ULOG_ERR_ALREADYCREATED,
  ULOG_ERR_NOTCONNECTED,
  ULOG_ERR_ALREADYENABLED,
  ULOG_ERR_INITINCOMPLETE,
  ULOG_ERR_READONLY,
  ULOG_ERR_INVALIDHANDLE,
  ULOG_ERR_MALLOC,
  ULOG_ERR_ASSIGN,
  ULOG_ERR_INSUFFICIENT_BUFFER,

  ULOG_ERR_NAMENOTFOUND,
  ULOG_ERR_MISC,
  ULOG_ERR_OVERRUN,
  ULOG_ERR_NOTSUPPORTED,
} ULOG_ERRORS;





typedef enum
{

  ULOG_VALUE_BUFFER,
  ULOG_VALUE_SHARED_HEADER,
  ULOG_VALUE_READCORE,
  ULOG_VALUE_WRITECORE,
  ULOG_VALUE_LOCKCORE,


  ULOG_VALUE_LOG_BASE = 0x100,
  ULOG_VALUE_LOG_READFLAGS = ULOG_VALUE_LOG_BASE,
} ULOG_VALUE_ID;






typedef enum
{
  ULOG_LOCK_NONE = 0,




  ULOG_LOCK_OS,





  ULOG_LOCK_SPIN,


  ULOG_LOCK_INT,



  ULOG_LOCK_UINT32 = 0x7FFFFFFF
} ULOG_LOCK_TYPE;




typedef enum
{
  ULOG_ALLOC_LOCAL = 0x01,


  ULOG_ALLOC_MANUAL = 0x04,




  ULOG_ALLOC_TYPE_MASK = 0xFF,


  ULOG_ALLOC_UINT32 = 0x7FFFFFFF
} ULOG_ALLOC_TYPE;




typedef enum
{
  ULOG_MEMORY_USAGE_FULLACCESS = 0x0300,


  ULOG_MEMORY_USAGE_READABLE = 0x0100,


  ULOG_MEMORY_USAGE_WRITEABLE = 0x0200,


  ULOG_MEMORY_USAGE_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_USAGE_TYPE;




typedef enum
{
  ULOG_MEMORY_CONFIG_LOCAL = 0x010000,






  ULOG_MEMORY_CONFIG_SHARED = 0x020000,




  ULOG_MEMORY_CONFIG_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_CONFIG_TYPE;
# 176 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
typedef uint64(*ULOG_ALT_TS_SRC)(void);






typedef enum
{
  ULOG_INTERFACE_INVALID = 0,

  ULOG_INTERFACE_RAW,


  ULOG_INTERFACE_REALTIME,


  ULOG_INTERFACE_UINT32 = 0x7FFFFFFF
} ULOG_INTERFACE_TYPE;





typedef enum
{
  ULOG_REALTIME_SUBTYPE_RESERVED_FOR_RAW = 0,
  ULOG_REALTIME_SUBTYPE_PRINTF,
  ULOG_REALTIME_SUBTYPE_BYTEDATA,
  ULOG_REALTIME_SUBTYPE_STRINGDATA,
  ULOG_REALTIME_SUBTYPE_WORDDATA,
  ULOG_REALTIME_SUBTYPE_CSVDATA,
  ULOG_REALTIME_SUBTYPE_VECTOR,
  ULOG_REALTIME_SUBTYPE_MULTIPART,
  ULOG_REALTIME_SUBTYPE_MULTIPART_STREAM_END,

  ULOG_SUBTYPE_REALTIME_TOKENIZED_STRING,
  ULOG_SUBTYPE_RESERVED1,
  ULOG_SUBTYPE_RESERVED2,
  ULOG_SUBTYPE_RESERVED3,
  ULOG_SUBTYPE_RAW8,
  ULOG_SUBTYPE_RAW16,
  ULOG_SUBTYPE_RAW32,

  ULOG_REALTIME_SUBTYPE_UINT32 = 0x7FFFFFFF
} ULOG_REALTIME_SUBTYPES;
# 231 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
typedef enum
{
  ULOG_LOG_WRAPPED = 0x0001,



  ULOG_ERR_LARGEMSG = 0x0002,




  ULOG_ERR_LARGEMSGOUT = 0x0004,



  ULOG_ERR_RESET = 0x0008,

  ULOG_ERR_UINT32 = 0x7FFFFFFF
} ULOG_WRITER_STATUS_TYPE;




typedef enum
{
  ULOG_RD_FLAG_REWIND = 0x0001,

  ULOG_RD_FLAG_UINT32 = 0x7FFFFFFF
} ULOG_RD_FLAG_TYPE;




typedef enum
{
  ULOG_ATTRIBUTE_READ_AUTOREWIND = 0x1,




  ULOG_ATTRIBUTE_UINT32 = 0x7FFFFFFF
} ULOG_ATTRIBUTE_TYPE;





typedef struct
{
  ULogResult (*write) (ULogHandle h, uint32 firstMsgCount, const char * firstMsgContent, uint32 secondMsgCount, const char * secondMsgContent, ULOG_INTERFACE_TYPE interfaceType);
  DALBOOL (*multipartMsgBegin) (ULogHandle h);
  void (*multipartMsgEnd) (ULogHandle h);
} ULOG_CORE_VTABLE;
# 301 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Init(void);
# 331 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_LogCreate(ULogHandle * h,
                              const char * logName,
                              uint32 bufferSize,
                              uint32 memoryType,
                              ULOG_LOCK_TYPE lockType,
                              ULOG_INTERFACE_TYPE interfaceType);
# 349 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
void ULogCore_SetLockType( ULogHandle h, ULOG_LOCK_TYPE lockType );
# 370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_HeaderSet(ULogHandle h, char * headerText);
# 385 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
void ULogCore_AttributeSet(ULogHandle h, uint32 attribute);
# 408 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_MemoryAssign(ULogHandle h,
                                 ULOG_VALUE_ID id,
                                 void * bufferPtr,
                                 uint32 bufferSize);
# 427 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Enable(ULogHandle h);
# 441 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
DALBOOL ULogCore_IsEnabled(ULogHandle h, ULOG_CORE_VTABLE ** core);
# 461 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Query(ULogHandle h, ULOG_VALUE_ID id, uint32 * value);
# 502 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Read(ULogHandle h,
                         uint32 outputSize,
                         char * outputMem,
                         uint32 * outputCount);
# 526 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_ReadEx(ULogHandle h,
                           uint32 outputSize,
                           char * outputMem,
                           uint32 * outputCount,
                           uint32 outputMessageLimit);
# 547 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_ReadSessionComplete(ULogHandle h);
# 568 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Allocate(ULogHandle h, uint32 bufferSize);
# 581 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Disable(ULogHandle h);
# 598 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_List(uint32 * registeredCount,
                         uint32 nameArraySize,
                         uint32 * namesReadCount,
                         char * nameArray);
# 623 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_ListEx(uint32 offsetCount,
                           uint32 * registeredCount,
                           uint32 nameArraySize,
                           uint32 * namesReadCount,
                           char * nameArray);
# 647 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_MsgFormat(ULogHandle h,
                              char * msg,
                              char * msgString,
                              uint32 msgStringSize,
                              uint32 * msgConsumed);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_HeaderRead(ULogHandle h,
                               uint32 headerReadOffset,
                               char * headerString,
                               uint32 headerStringSize,
                               uint32 * headerActualLength);
# 689 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_Connect(ULogHandle * h, const char * logName);
# 705 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
void ULogCore_LogLevelSet(ULogHandle h, uint32 level);
# 722 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
uint32 ULogCore_LogLevelGet(ULogHandle h);
# 762 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogHandle ULogCore_TeeAdd(ULogHandle h1, ULogHandle h2);
# 779 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogHandle ULogCore_TeeRemove(ULogHandle h1, ULogHandle h2);
# 791 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogHandle ULogCore_HandleGet(char *logName);
# 805 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
DALBOOL ULogCore_IsLogPresent(ULogHandle h, char *logName);
# 820 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_SetTimestampSrcFn(ULogHandle h, ULOG_ALT_TS_SRC altULogTimeStampFn);
# 834 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_SetTransportToRAM(ULogHandle h);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_SetTransportToStm(ULogHandle h, unsigned char protocol_num);
# 864 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_SetTransportToStmAscii(ULogHandle h, unsigned char protocol_num);
# 877 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogCore_SetTransportToAlt(ULogHandle h, ULOG_CORE_VTABLE* newTansportVTable);
# 907 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULog.h"
ULogResult ULogDiagAddPlugin(int(*new_pluginfcn)(uint32), uint32 new_plugin_id);
ULogResult ULogDiagAddPluginExt
(
  int(*new_pluginfcnext)(void*, unsigned long int),
  uint32 new_plugin_id
);
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h" 1
# 16 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h" 2
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RawInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
# 59 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RawLog(ULogHandle h,
                            const char * dataArray,
                            uint32 dataCount);
# 97 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeStaticInit( ULogHandle * h,
                                         const char * name,
                                         uint32 logMessageSlots,
                                         uint32 logBufMemType,
                                         ULOG_LOCK_TYPE logLockType);
# 146 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimePrintf(ULogHandle h,
                                    uint32 dataCount,
                                    const char * formatStr,
                                    ...);





ULogResult ULogFront_RealTimeStaticPrintf(ULogHandle h, uint32 msgSlot, uint32 dataCount, const char * formatStr, ...);
# 256 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeVprintf(ULogHandle h,
                                     uint32 dataCount,
                                     const char * formatStr,
                                     va_list ap);
# 277 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeData(ULogHandle h, uint32 dataCount, ...);
# 327 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeCharArray(ULogHandle h,
                                       uint32 byteCount,
                                       char * byteData);
# 345 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeString(ULogHandle h, char * cStr);
# 360 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeWordArray(ULogHandle h,
                                       uint32 wordCount,
                                       const uint32 * wordData);
# 377 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeCsv(ULogHandle h,
                                 uint32 wordCount,
                                 const uint32 * wordData);
# 398 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
ULogResult ULogFront_RealTimeVector(ULogHandle h,
                                    const char * formatStr,
                                    unsigned short entryByteCount,
                                    unsigned short vectorLength,
                                    const void * vector);
# 420 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
DALBOOL ULogFront_RealTimeMultipartMsgBegin (ULogHandle h);
# 433 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/ULogFront.h"
void ULogFront_RealTimeMultipartMsgEnd (ULogHandle h);
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg.h" 1
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 1
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h"
# 1 "./custaaaaaaaaq.h" 1
# 11 "./custaaaaaaaaq.h"
# 1 "./targaaaaaaaaq.h" 1
# 12 "./custaaaaaaaaq.h" 2
# 32 "./custaaaaaaaaq.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/custtarget.h" 1
# 33 "./custaaaaaaaaq.h" 2
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/build/cust/customer.h" 2
# 82 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 2


# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 1
# 149 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 144 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 150 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 2


# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/target.h" 1
# 153 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/armasm.h" 1
# 154 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h" 2
# 186 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
typedef void * addr_t;
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
# 806 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
# 1095 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/comdef.h"
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
# 85 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h" 1
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h" 2
# 95 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
typedef unsigned long qword[ 2 ];



  typedef unsigned long qc_qword[ 2 ];
# 214 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_set
(
  qc_qword qw,
  uint32 hi,
  uint32 lo
);
# 235 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_equ
(
  qc_qword qw1,

  qc_qword qw2

);
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
uint32 qw_hi
(
  qc_qword qw
);
# 272 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
uint32 qw_lo
(
  qc_qword qw
);
# 290 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_inc
(
  qc_qword qw1,
  uint32 val
);
# 309 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_dec
(
  qc_qword qw1,
  uint32 val
);
# 330 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_add
(
  qc_qword sum,
  qc_qword addend,
  qc_qword adder
);
# 353 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_sub
(
  qc_qword difference,
  qc_qword subtrahend,
  qc_qword subtractor
);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_mul
(
  qc_qword product,
  qc_qword multiplicand,
  uint32 multiplier
);
# 398 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
word qw_div
(
  qc_qword quotient,
  qc_qword dividend,
  word divisor
);
# 425 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
word qw_div_by_power_of_2
(
  qc_qword quotient,
  qc_qword dividend,
  unsigned short num_bits
);
# 447 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
void qw_shift
(
  qc_qword shifticand,
  int shiftidend
);
# 469 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/qw.h"
int qw_cmp
(
  qc_qword qw1,
  qc_qword qw2
);
# 86 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h" 1
# 70 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h" 1
# 102 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
}
msg_get_ssid_ranges_req_type;
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;

  uint32 range_cnt;

  struct
  {
    uint16 ssid_first;
    uint16 ssid_last;
  }
  ssid_ranges[1];
}
msg_get_ssid_ranges_rsp_type;
# 146 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
}
msg_get_mask_req_type;
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;

  uint8 status;
  uint8 pad;

  uint32 bld_mask[1];


}
msg_get_mask_rsp_type;
# 190 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint16 pad;
  uint32 rt_mask[1];

}
msg_set_rt_mask_req_type;
# 211 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint8 status;
  uint8 pad;
  uint32 rt_mask[1];

}
msg_set_rt_mask_rsp_type;
# 233 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_req_type;
# 251 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_rsp_type;
# 272 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  uint16 line;
  uint16 ss_id;
  uint32 ss_mask;
}
msg_desc_type;






typedef struct
{
  uint8 cmd_code;
  uint8 ts_type;
  uint8 num_args;
  uint8 drop_cnt;
  qword ts;
}
msg_hdr_type;






typedef enum
{
  MSG_TS_TYPE_CDMA_FULL = 0,
  MSG_TS_TYPE_WIN32,
  MSG_TS_TYPE_GW
}
msg_ts_type;
# 323 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct
{
  msg_hdr_type hdr;
  msg_desc_type desc;
  uint32 args[1];


}
msg_ext_type;
# 353 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_pkt_defs.h"
typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word msg_level;
}
msg_legacy_req_type;






typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word qty;



  dword drop_cnt;
  dword total_msgs;
  byte level;
  char file[(12+1)];
  word line;
  char fmt[40];
  dword code1;
  dword code2;
  dword code3;
  qword time;
}
msg_legacy_rsp_type;
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgcfg.h" 1
# 115 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgcfg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgtgt.h" 1
# 120 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgtgt.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_mask.h" 1
# 121 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgtgt.h" 2
# 116 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msgcfg.h" 2
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h" 2
# 106 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
typedef struct
{
  msg_desc_type desc;
  uint32 msg_hash;
}
msg_qsr_const_type;
# 126 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
typedef struct
{
  msg_qsr_const_type qsr_const_blk;
  const char *fname;
} err_msg_qsr_const_type;
# 143 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
typedef struct
{
  msg_hdr_type hdr;
  const msg_qsr_const_type* qsr_const_data_ptr;
  uint32 qsr_flag;
  uint32 args[1];
}
msg_qsr_store_type;
# 201 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
typedef struct
{
  msg_desc_type desc;
  const char * msg;

}
msg_v2_const_type;

typedef struct
{
  msg_v2_const_type msg_v2_const_blk;
  const char *fname;
} err_msg_v2_const_type;
# 598 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void qsr_msg_send ( const msg_qsr_const_type * xx_msg_const_ptr);
# 625 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void msg_v2_send ( const msg_v2_const_type * xx_msg_const_ptr);
# 653 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void qsr_msg_send_1 (const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1);
# 680 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void msg_v2_send_1 (const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1);
# 709 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void qsr_msg_send_2 ( const msg_qsr_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
# 738 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void msg_v2_send_2 ( const msg_v2_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
# 768 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void qsr_msg_send_3 ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
# 798 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void msg_v2_send_3 ( const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
# 829 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void qsr_msg_send_var ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 num_args, ...);
# 857 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
  void msg_v2_send_var ( const msg_v2_const_type * xx_msg_const_ptr, uint32 num_args, ...);
# 888 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_qsrerrlog_3 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
# 917 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_qsrerrlog_2 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2);
# 945 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_qsrerrlog_1 (const err_msg_qsr_const_type* const_blk, uint32 code1);
# 972 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_qsrerrlog_0 (const err_msg_qsr_const_type* const_blk);
# 1001 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_v2_errlog_3 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
# 1030 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_v2_errlog_2 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2);
# 1059 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_v2_errlog_1 (const err_msg_v2_const_type* const_blk, uint32 code1);
# 1088 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_qsr.h"
void msg_v2_errlog_0 (const err_msg_v2_const_type* const_blk);
# 87 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h" 1
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_malloc( unsigned int size);
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_calloc(unsigned int elsize, unsigned int num);
# 100 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_realloc(void *ptr, int newsize);
# 119 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void qurt_free( void *ptr);
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_futex.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_futex.h"
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
typedef union qurt_mutex_aligned8{

    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;

} qurt_mutex_t;
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_init(qurt_mutex_t *lock);
# 103 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_destroy(qurt_mutex_t *lock);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_lock(qurt_mutex_t *lock);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_unlock(qurt_mutex_t *lock);
# 175 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
int qurt_mutex_try_lock(qurt_mutex_t *lock);
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h" 1
# 24 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
typedef union {

 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_down(qurt_sem_t *sem);
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_try_down(qurt_sem_t *sem);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_init(qurt_sem_t *sem);
# 177 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_destroy(qurt_sem_t *sem);
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
# 212 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h" 2
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
typedef unsigned long long int qurt_pipe_data_t;


typedef struct {

    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;

} qurt_pipe_t;


typedef struct {

  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;

} qurt_pipe_attr_t;
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
# 123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
# 208 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
# 230 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
# 252 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_destroy(qurt_pipe_t *pipe);
# 277 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_delete(qurt_pipe_t *pipe);
# 303 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 328 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
# 356 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 382 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
# 416 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
# 447 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 466 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_printf.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_printf.h"
int qurt_printf(const char* format, ...);
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_assert.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_assert.h"
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h" 2
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;


typedef unsigned int qurt_thread_t;


typedef struct _qurt_thread_attr {

    char name[16];
    unsigned char tcb_partition;

    unsigned char affinity;

    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;


} qurt_thread_attr_t;
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{

    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
# 175 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
# 201 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
# 223 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
# 280 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
# 307 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
# 337 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
# 364 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
# 384 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_get_name (char *name, unsigned char max_len);
# 411 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
# 422 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_stop(void);
# 437 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_resume(unsigned int thread_id);
# 449 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
qurt_thread_t qurt_thread_get_id (void);
# 465 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
# 482 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_timetest_id (unsigned short tid);
# 501 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
# 536 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
# 551 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned short qurt_thread_get_timetest_id (void);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_exit(int status);
# 596 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_join(unsigned int tid, int *status);
# 611 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned int qurt_thread_get_anysignal(void);
# 632 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_get_priority (qurt_thread_t threadid);
# 655 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned int qurt_api_version(void);
# 685 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
# 48 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h" 1
# 85 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_trace_get_marker(void);
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
# 172 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
# 194 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_enable(unsigned int enable_flag);
# 211 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
# 246 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
# 276 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_reset_idle_pcycles (void);
# 48 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_profile_get_thread_pcycles(void);
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_profile_get_thread_tcycles(void);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_get_core_pcycles(void);
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
# 148 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_reset_threadid_pcycles (int thread_id);
# 170 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_enable (int enable);
# 50 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h" 1
# 32 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
typedef struct {

   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;

} qurt_rmutex2_t;
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h" 2








typedef union {

    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;

} qurt_cond_t;
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_init(qurt_cond_t *cond);
# 97 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_destroy(qurt_cond_t *cond);
# 127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_signal(qurt_cond_t *cond);
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_broadcast(qurt_cond_t *cond);
# 187 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
# 220 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
# 52 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h" 1
# 38 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
typedef union {

 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;

} qurt_barrier_t;
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_destroy(qurt_barrier_t *barrier);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_wait(qurt_barrier_t *barrier);
# 53 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h" 1
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
# 113 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_fastint_deregister(int intno);
# 136 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_isr_register(int intno, void (*fn)(int));
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_isr_deregister(int intno);
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
typedef union {

 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;

} qurt_allsignal_t;
# 64 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_init(qurt_allsignal_t *signal);
# 86 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
# 106 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h" 1
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
typedef union {

    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;

} qurt_signal_t;
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_init(qurt_signal_t *signal);
# 91 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_destroy(qurt_signal_t *signal);
# 128 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
# 188 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
# 214 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
# 234 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
unsigned int qurt_signal_get(qurt_signal_t *signal);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
# 298 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h" 2






typedef qurt_signal_t qurt_anysignal_t;
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
# 73 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
# 105 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
# 130 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
# 56 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h" 1
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_init(qurt_mutex_t *lock);
# 70 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_destroy(qurt_mutex_t *lock);
# 100 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_lock(qurt_mutex_t *lock);
# 120 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_unlock(qurt_mutex_t *lock);
# 145 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
# 58 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h" 1
# 50 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_init(qurt_mutex_t *lock);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_destroy(qurt_mutex_t *lock);
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_lock(qurt_mutex_t *lock);
# 134 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_unlock(qurt_mutex_t *lock);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
# 59 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
typedef struct {

   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;




   unsigned int queue;
   unsigned int wait_mask;

} qurt_signal2_t;
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_init(qurt_signal2_t *signal);
# 95 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_destroy(qurt_signal2_t *signal);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
# 189 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
# 215 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
# 235 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
# 260 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);

int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
# 60 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h" 1
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
# 123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_interrupt_acknowledge(int int_num);
# 145 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_deregister(int int_num);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_enable(int int_num);
# 184 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_disable(int int_num);
# 201 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_status(int int_num, int *status);
# 217 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_clear(int int_num);
# 233 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_get_registered(void);
# 252 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
# 273 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
# 289 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_interrupt_raise(unsigned int interrupt_num);
# 303 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
void qurt_interrupt_disable_all(void);
# 319 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_isr_subcall(void);
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h"
void * qurt_lifo_pop(void *freelist);
# 58 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h"
void qurt_lifo_push(void *freelist, void *buf);

void qurt_lifo_remove(void *freelist, void *buf);
# 64 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
static inline int qurt_power_shutdown_prepare(void){ return 0;}
# 74 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_enter (int type);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_exit(void);
# 121 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_apcr_enter (void);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_prepare (void);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_fail_exit (void);
# 170 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_enter (void);
# 182 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_exit (void);
# 209 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_override_wait_for_idle(int enable);
# 233 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_wait_for_idle (void);
# 249 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_wait_for_active (void);
# 269 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_ipend_get (void);
# 282 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_system_avscfg_set(unsigned int avscfg_value);
# 294 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_avscfg_get(void);
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_vid_get(void);
# 351 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );

int qurt_system_tcm_set_size(unsigned int new_size);
# 373 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
# 65 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
typedef struct qurt_sysenv_swap_pools {

   unsigned int spoolsize;
   unsigned int spooladdr;

}qurt_sysenv_swap_pools_t;


typedef struct qurt_sysenv_app_heap {

   unsigned int heap_base;
   unsigned int heap_limit;

} qurt_sysenv_app_heap_t ;


typedef struct qurt_sysenv_arch_version {

    unsigned int arch_version;

}qurt_arch_version_t;


typedef struct qurt_sysenv_max_hthreads {

   unsigned int max_hthreads;

}qurt_sysenv_max_hthreads_t;


typedef struct qurt_sysenv_max_pi_prio {

    unsigned int max_pi_prio;

}qurt_sysenv_max_pi_prio_t;


typedef struct qurt_sysenv_timer_hw {

   unsigned int base;
   unsigned int int_num;

}qurt_sysenv_hw_timer_t;


typedef struct qurt_sysenv_procname {

   unsigned int asid;
   char name[64];

}qurt_sysenv_procname_t;


typedef struct qurt_sysenv_stack_profile_count {

   unsigned int count;

}qurt_sysenv_stack_profile_count_t;




typedef struct _qurt_sysevent_error_t
{

    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;

} qurt_sysevent_error_t ;


typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
# 128 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
# 144 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
# 161 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
# 212 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
# 229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
# 246 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
# 288 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);

unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
# 326 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
# 350 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
# 372 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_raise_fatal (void);
# 387 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
# 402 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_shutdown_fatal2(void);
# 425 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);

unsigned int qurt_enable_floating_point_exception(unsigned int mask);
# 457 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
# 483 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h" 1
# 53 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
void qurt_pmu_set (int reg_id, unsigned int reg_value);
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
unsigned int qurt_pmu_get (int red_id);
# 96 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
void qurt_pmu_enable (int enable);
# 67 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h" 1
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);


int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_delete (unsigned int entry_id);
# 101 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
# 147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
unsigned short qurt_tlb_entry_get_available(void);
# 168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
# 69 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi_constants.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi_imacros.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 2
# 101 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
# 130 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_write(int handle, const void *buf, unsigned len);
# 150 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_read(int handle, void *buf, unsigned len);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_close(int handle);
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h" 1
# 85 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
# 111 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
# 131 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
# 147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern unsigned long long qurt_sysclock_get_expiry (void);
# 163 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
unsigned long long qurt_sysclock_get_hw_ticks (void);
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
# 207 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;

    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));

    return (unsigned short)ticks;
}


unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_spawn_flags(const char * name, int flags);
# 69 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_space_switch(int asid);
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_wait(int *status);






int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
# 73 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h" 1
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
typedef struct _qurt_process_attr {

    char name[64];
    int flags;

} qurt_process_attr_t;
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
int qurt_process_create (qurt_process_attr_t *attr);
# 60 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
int qurt_process_get_id (void);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
# 132 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
# 159 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
# 74 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
typedef unsigned int mode_t;
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
int shm_open(const char * name, int oflag, mode_t mode);
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
int shm_close(int fd);
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h" 1
# 87 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;







typedef unsigned int qurt_timer_t;


typedef unsigned long long qurt_timer_duration_t;


typedef unsigned long long qurt_timer_time_t;


typedef struct
{

    unsigned int magic;

    qurt_timer_duration_t duration;

    qurt_timer_time_t expiry;

    qurt_timer_duration_t remaining;

    qurt_timer_type_t type;


    unsigned int group;


}
qurt_timer_attr_t;
# 155 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_stop (qurt_timer_t timer);
# 185 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
# 213 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);

int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
# 236 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
# 285 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
# 306 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
# 330 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
# 414 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
# 435 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
# 453 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_delete(qurt_timer_t timer);
# 478 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_sleep(qurt_timer_duration_t duration);
# 497 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_group_disable (unsigned int group);
# 513 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_group_enable (unsigned int group);






void qurt_timer_recover_pc (void);
# 529 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
static inline int qurt_timer_is_init (void) {return 1;};
# 539 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
unsigned long long qurt_timer_get_ticks (void);
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_create_key (int *key, void (*destructor)(void *));
# 56 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_set_specific (int key, const void *value);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
void *qurt_tls_get_specific (int key);
# 90 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_delete_key (int key);
# 77 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread_context.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread_context.h"
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}

static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}

static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}



int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);

int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
# 82 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
# 103 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_unlock(void);
# 127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
# 150 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_mode(void);
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_units(void);
# 204 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_reserve(int num_units);
# 229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_cancel_reserve(void);
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_lock_val(void);
# 79 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h" 1
# 31 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;


typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;


typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
unsigned long long qurt_mailbox_get_id(char *name);
# 104 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
# 125 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
# 143 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_delete(unsigned long long mailbox_id);
# 159 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h" 1
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};

typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;



typedef struct qurt_island_attr {

    int max_attrs;
    struct qurt_island_attr_resource attrs[1];

} qurt_island_attr_t;


typedef struct {
   int qdi_handle;
} qurt_island_t;
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
void qurt_island_attr_delete (qurt_island_attr_t *attr);
# 109 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
# 152 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
# 197 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
# 219 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_spec_delete (qurt_island_t spec_id);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_enter (qurt_island_t spec_id);
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_exit (void);
# 290 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
# 305 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
unsigned int qurt_island_get_status (void);
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 89 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h" 2
# 119 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
typedef struct
{
  msg_desc_type desc;
  const char* fmt;
  const char* fname;
}
msg_const_type;







typedef struct
{
  msg_hdr_type hdr;
  const msg_const_type* const_data_ptr;
  uint32 args[1];
}
msg_ext_store_type;
# 1242 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_init(void);
# 1268 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send(const msg_const_type* xx_msg_const_ptr);
# 1295 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send_1(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1);
# 1323 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send_2(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2);
# 1353 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send_3(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2, uint32 xx_arg3);
# 1382 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send_var(const msg_const_type* xx_msg_const_ptr, uint32 num_args,
                  ...);
# 1410 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_sprintf(const msg_const_type* const_blk, ...);
# 1440 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_send_ts(const msg_const_type* const_blk, uint64 timestamp);
# 1468 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_save_3(const msg_const_type* const_blk,
                uint32 xx_arg1, uint32 xx_arg2, uint32 xx_arg3,
                msg_ext_store_type* msg);
# 1498 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_errlog_3(const msg_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
# 1526 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_errlog_2(const msg_const_type* const_blk, uint32 code1, uint32 code2);
# 1553 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_errlog_1(const msg_const_type* const_blk, uint32 code1);
# 1579 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
void msg_errlog_0(const msg_const_type* const_blk);
# 1665 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg_diag_service.h"
boolean msg_status(uint16 ss_id, uint32 ss_mask);
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/msg.h" 2
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h" 2
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/inc/adsppm_utils.h"
void adsppm_lock(DALSYSSyncHandle lock);
void adsppm_unlock(DALSYSSyncHandle lock);

ULogHandle GetUlogHandle(void);

const char *adsppm_getBusPortName(AdsppmBusPortIdType busPort);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2


# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/lpass_clocks_hwio_8996.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/hwio/msm8996/msmhwiobase.h" 1
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/limits.h" 1 3 4
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/adsppm/config/8996/adsppm_configdata_8996.c" 2

AsicHwioRegRangeType lpassRegRange_8996 = {0xee000000, 0x09000000, 0x00400000};





AsicHwioRegRangeType l2ConfigRegRange_8996 = {(0x09400000 + 0x00180000 + 0x00020000), (0x09400000 + 0x00180000 + 0x00020000), 0x1004 };




const AdsppmClkIdType coreClocks_SlimBus_8996[] = {AdsppmClk_Slimbus_Core, AdsppmClk_Slimbus2_Core};

const AdsppmClkIdType coreClocks_HwRsp_8996[] = {AdsppmClk_HwRsp_Core};




const AdsppmBusPortIdType masterPorts_ADSP_8996[] = {AdsppmBusPort_Adsp_Master};
const AdsppmBusPortIdType masterPorts_DML_8996[] = {AdsppmBusPort_Dml_Master};
const AdsppmBusPortIdType masterPorts_AIF_8996[] = {AdsppmBusPort_Aif_Master};
const AdsppmBusPortIdType masterPorts_SlimBus_8996[] = {AdsppmBusPort_Slimbus_Master, AdsppmBusPort_Slimbus2_Master};

const AdsppmBusPortIdType masterPorts_HWRSMP_8996[] = {AdsppmBusPort_HwRsmp_Master};


const AdsppmBusPortIdType masterPorts_Hdmitx_8996[] = {AdsppmBusPort_Hdmitx_Master};





const AdsppmBusPortIdType slavePorts_ADSP_8996[] = {AdsppmBusPort_Adsp_Slave};
const AdsppmBusPortIdType slavePorts_LPM_8996[] = {AdsppmBusPort_Lpm_Slave};
const AdsppmBusPortIdType slavePorts_DML_8996[] = {AdsppmBusPort_Dml_Slave};
const AdsppmBusPortIdType slavePorts_AIF_8996[] = {AdsppmBusPort_Aif_Slave};
const AdsppmBusPortIdType slavePorts_SlimBus_8996[] = {AdsppmBusPort_Slimbus_Slave, AdsppmBusPort_Slimbus2_Slave};

const AdsppmBusPortIdType slavePorts_AVsync_8996[] = {AdsppmBusPort_AvSync_Slave};
const AdsppmBusPortIdType slavePorts_HWRSMP_8996[] = {AdsppmBusPort_HwRsmp_Slave};




const AdsppmBusPortIdType slavePorts_Hdmitx_8996[] = {AdsppmBusPort_Hdmitx_Slave};






const AsicCoreDescType cores_array_8996[] =
{
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_ADSP,
                {
                        AsicRsc_None,
                        AsicRsc_Power_ADSP,
                        AsicRsc_None,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_Adsp,
                {0, 0},
                {(sizeof(masterPorts_ADSP_8996)/sizeof(masterPorts_ADSP_8996[0])), &masterPorts_ADSP_8996[0]},
                {(sizeof(slavePorts_ADSP_8996)/sizeof(slavePorts_ADSP_8996[0])), &slavePorts_ADSP_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_LPASS_Core,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_LPM,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Mem,
                        AsicRsc_None,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {0, 0},
                {(sizeof(slavePorts_LPM_8996)/sizeof(slavePorts_LPM_8996[0])), &slavePorts_LPM_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_DML,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_None,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {(sizeof(masterPorts_DML_8996)/sizeof(masterPorts_DML_8996[0])), &masterPorts_DML_8996[0]},
                {(sizeof(slavePorts_DML_8996)/sizeof(slavePorts_DML_8996[0])), &slavePorts_DML_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_AIF,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_None,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {(sizeof(masterPorts_AIF_8996)/sizeof(masterPorts_AIF_8996[0])), &masterPorts_AIF_8996[0]},
                {(sizeof(slavePorts_AIF_8996)/sizeof(slavePorts_AIF_8996[0])), &slavePorts_AIF_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_SlimBus,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_Core_Clk,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {(sizeof(coreClocks_SlimBus_8996)/sizeof(coreClocks_SlimBus_8996[0])), &coreClocks_SlimBus_8996[0]},
                {(sizeof(masterPorts_SlimBus_8996)/sizeof(masterPorts_SlimBus_8996[0])), &masterPorts_SlimBus_8996[0]},
                {(sizeof(slavePorts_SlimBus_8996)/sizeof(slavePorts_SlimBus_8996[0])), &slavePorts_SlimBus_8996[0]},
                Adsppm_Instance_Id_1
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_AVsync,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_Core_Clk,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {0, 0},
                {(sizeof(slavePorts_AVsync_8996)/sizeof(slavePorts_AVsync_8996[0])), &slavePorts_AVsync_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_HWRSMP,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_Core_Clk,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {(sizeof(coreClocks_HwRsp_8996)/sizeof(coreClocks_HwRsp_8996[0])), &coreClocks_HwRsp_8996[0]},
                {(sizeof(masterPorts_HWRSMP_8996)/sizeof(masterPorts_HWRSMP_8996[0])), &masterPorts_HWRSMP_8996[0]},
                {(sizeof(slavePorts_HWRSMP_8996)/sizeof(slavePorts_HWRSMP_8996[0])), &slavePorts_HWRSMP_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None

        },
        {
                Adsppm_Core_Id_Hdmitx,
                {
                        AsicRsc_None,
                        AsicRsc_Power_Core,
                        AsicRsc_None,
                        AsicRsc_Latency,
                        AsicRsc_MIPS_Clk | AsicRsc_MIPS_BW,
                        AsicRsc_BW_Internal | AsicRsc_BW_External,
                        AsicRsc_Thermal
                },
                AsicPowerDomain_LpassCore,
                {0, 0},
                {(sizeof(masterPorts_Hdmitx_8996)/sizeof(masterPorts_Hdmitx_8996[0])), &masterPorts_Hdmitx_8996[0]},
                {(sizeof(slavePorts_Hdmitx_8996)/sizeof(slavePorts_Hdmitx_8996[0])), &slavePorts_Hdmitx_8996[0]},
                Adsppm_Instance_Id_0
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_None,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_AON,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_None
        },
        {
                Adsppm_Core_Id_HVX,
                {
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None,
                        AsicRsc_None
                },
                AsicPowerDomain_Hvx,
                {0, 0},
                {0, 0},
                {0, 0},
                Adsppm_Instance_Id_0
        }
};

AsicCoreDescriptorArrayType cores_8996 = {(sizeof(cores_array_8996)/sizeof(cores_array_8996[0])), &cores_array_8996[0]};

const AsicMemDescriptorType memories_array_8996[] =
{
        {
                Adsppm_Mem_None,
                AsicPowerDomain_AON
        },
        {
                Adsppm_Mem_Ocmem,
                AsicPowerDomain_Ocmem
        },
        {
                Adsppm_Mem_Lpass_LPM,
                AsicPowerDomain_Lpm
        },
        {
                Adsppm_Mem_Sram,
                AsicPowerDomain_SRam
        }
};

AsicMemDescriptorArrayType memories_8996 = {(sizeof(memories_array_8996)/sizeof(memories_array_8996[0])), &memories_array_8996[0]};




const AsicClkDescriptorType clocks_array_8996[] =
{
        {
                AdsppmClk_None,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Adsp_Core,
                AsicClk_TypeNpa,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "/clk/cpu",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Ahb_Root,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_core_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AhbI_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AhbX_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0001b000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Sysnoc_cbc,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_HwRsp_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000f000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Dml_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00016000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Aif_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Aif_Csr_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000e000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00010000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Slimbus_cbc,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus_cbc,
                AsicClk_TypeCBC,
                AsicClk_CntlSW,
                AsicClk_MemCntlAlwaysRetain,
                .clkInfo.clkName = "audio_core_aud_slimbus_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus2_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00011000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Slimbus2_cbc,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus2_cbc,
                AsicClk_TypeCBC,
                AsicClk_CntlSW,
                AsicClk_MemCntlAlwaysRetain,
                .clkInfo.clkName = "audio_core_qca_slimbus_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Midi_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AvSync_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000c000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Atimer_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00015000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Lpm_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000d000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Lpm_cbc,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Lpm_cbc,
                AsicClk_TypeCBC,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_lpm_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_Lpass_LPM
        },
        {
                AdsppmClk_Csr_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000a000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Dcodec_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Spdif_Hmclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Spdif_Hsclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Hdmirx_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Hdmitx_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlSW_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00012000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Sif_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Bstc_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Smmu_Adsp_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Smmu_Lpass_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_peripheral_smmu_client_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Sysnoc_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00017000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Sysnoc_cbc,
                AsicClk_TypeCBC,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_sysnoc_mport_core_clk",
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Bus_Timeout_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0001a000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Tlb_Preload_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00014000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Qos_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x0000b000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Qdsp_Sway_Hclk,
                AsicClk_TypeInternalCGC,
                AsicClk_CntlAlwaysON_DCG,
                AsicClk_MemCntlNone,
                .clkInfo.hwioInfo =
                {
                        (0x000c0000 + 0x00018000),
                        0x1,
                        0x2,
                        0
                },
                AdsppmClk_Ahb_Root,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AhbE_Hclk,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_aon_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Adsp_Hmclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "q6ss_ahbm_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Adsp_Hsclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "q6ss_ahbs_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Sram_Hclk,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Lcc_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_qdsp_sway_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Security_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_sysnoc_sway_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Wrapper_Security_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_mpu_cfg_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Wrapper_Br_Hclk,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_q6_ahbm_mpu_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Audio_Core_AON,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_qos_ahbs_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Audio_Wrapper_AON,
                AsicClk_TypeDalEnable,
                AsicClk_CntlAlwaysON,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_wrapper_bus_timeout_aon_clk",
                AdsppmClk_AhbE_Hclk,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_HwRsp_Core,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_resampler_core_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Midi_Core,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AvSync_Xo,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AvSync_Bt,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_AvSync_Fm,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus_Core,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_aud_slimbus_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Slimbus2_Core,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_qca_slimbus_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Avtimer_core,
                AsicClk_TypeDalEnable,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_avsync_stc_xo_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Atime_core,
                AsicClk_TypeDalFreqSet,
                AsicClk_CntlSW,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "audio_core_avsync_atime_clk",
                AdsppmClk_None,
                Adsppm_Mem_None
        },
        {
                AdsppmClk_Atime2_core,
                AsicClk_TypeNone,
                AsicClk_CntlNone,
                AsicClk_MemCntlNone,
                .clkInfo.clkName = "",
                AdsppmClk_None,
                Adsppm_Mem_None
        }
};

AsicClockDescriptorArrayType clocks_8996 = {(sizeof(clocks_array_8996)/sizeof(clocks_array_8996[0])), &clocks_array_8996[0]};




const AdsppmClkIdType regProgClocks_Dml_8996[] = {AdsppmClk_Dml_Hclk};
const AdsppmClkIdType regProgClocks_Aif_8996[] = {AdsppmClk_Aif_Csr_Hclk};
const AdsppmClkIdType regProgClocks_Slimbus_8996[] = {AdsppmClk_Slimbus_Hclk};
const AdsppmClkIdType regProgClocks_Slimbus2_8996[] = {AdsppmClk_Slimbus2_Hclk};

const AdsppmClkIdType regProgClocks_HwRsmp_8996[] = {AdsppmClk_HwRsp_Hclk};
const AdsppmClkIdType regProgClocks_AvSync_8996[] = {AdsppmClk_AvSync_Hclk};
const AdsppmClkIdType regProgClocks_Lpm_8996[] = {AdsppmClk_Lpm_Hclk};
const AdsppmClkIdType regProgClocks_Sram_8996[] = {AdsppmClk_Sram_Hclk};


const AdsppmClkIdType regProgClocks_Hdmitx_8996[] = {AdsppmClk_Hdmitx_Hclk};



const AdsppmClkIdType regProgClocks_Adsp_8996[] = {AdsppmClk_Adsp_Hmclk, AdsppmClk_Adsp_Hsclk};




const AsicBusPortDescriptorType busPorts_array_8996[] =
{
        {
                AdsppmBusPort_None,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_APPSS_PROC},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Adsp_Master,
                AsicBusPort_AhbE_M | AsicBusPort_Ext_M,
                AdsppmClk_Adsp_Hmclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_PROC},
                AdsppmBusPort_Adsp_Master
        },
        {
                AdsppmBusPort_Dml_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_Dml_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Aif_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_Aif_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Slimbus_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_Slimbus_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Slimbus2_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_Slimbus2_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Midi_Master,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_APPSS_PROC},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_HwRsmp_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_HwRsp_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Ext_Ahb_Master,
                AsicBusPort_Ext_M,
                AdsppmClk_AhbX_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Spdif_Master,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_APPSS_PROC},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Hdmirx_Master,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_APPSS_PROC},
                AdsppmBusPort_None

        },
        {
                AdsppmBusPort_Hdmitx_Master,
                AsicBusPort_AhbI_M,
                AdsppmClk_Hdmitx_Hclk,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_LPASS_AHB},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Sif_Master,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbMaster = ICBID_MASTER_APPSS_PROC},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Dml_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Dml_Hclk,
                {(sizeof(regProgClocks_Dml_8996)/sizeof(regProgClocks_Dml_8996[0])), &regProgClocks_Dml_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Aif_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Aif_Csr_Hclk,
                {(sizeof(regProgClocks_Aif_8996)/sizeof(regProgClocks_Aif_8996[0])), &regProgClocks_Aif_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Slimbus_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Slimbus_Hclk,
                {(sizeof(regProgClocks_Slimbus_8996)/sizeof(regProgClocks_Slimbus_8996[0])), &regProgClocks_Slimbus_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Slimbus2_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Slimbus2_Hclk,
                {(sizeof(regProgClocks_Slimbus2_8996)/sizeof(regProgClocks_Slimbus2_8996[0])), &regProgClocks_Slimbus2_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Midi_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_HwRsmp_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_HwRsp_Hclk,
                {(sizeof(regProgClocks_HwRsmp_8996)/sizeof(regProgClocks_HwRsmp_8996[0])), &regProgClocks_HwRsmp_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_AvSync_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_AvSync_Hclk,
                {(sizeof(regProgClocks_AvSync_8996)/sizeof(regProgClocks_AvSync_8996[0])), &regProgClocks_AvSync_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Lpm_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Lpm_Hclk,
                {(sizeof(regProgClocks_Lpm_8996)/sizeof(regProgClocks_Lpm_8996[0])), &regProgClocks_Lpm_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Sram_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Ext_Ahb_Slave,
                AsicBusPort_Ext_S,
                AdsppmClk_AhbE_Hclk,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Ddr_Slave,
                AsicBusPort_Ext_S,
                AdsppmClk_Smmu_Lpass_Hclk,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_EBI1},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Ocmem_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_OCMEM},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_PerifNoc_Slave,
                AsicBusPort_Ext_S,
                AdsppmClk_Smmu_Lpass_Hclk,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_BLSP_1},
                AdsppmBusPort_Ext_Ahb_Master
        },
        {
                AdsppmBusPort_Spdif_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Hdmirx_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None

        },
        {
                AdsppmBusPort_Hdmitx_Slave,
                AsicBusPort_AhbI_S,
                AdsppmClk_Hdmitx_Hclk,
                {(sizeof(regProgClocks_Hdmitx_8996)/sizeof(regProgClocks_Hdmitx_8996[0])), &regProgClocks_Hdmitx_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Ext_Ahb_Slave
        },
        {
                AdsppmBusPort_Sif_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Bstc_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Dcodec_Slave,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Core,
                0,
                AdsppmClk_None,
                {0, 0},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_None
        },
        {
                AdsppmBusPort_Adsp_Slave,
                AsicBusPort_AhbE_S,
                AdsppmClk_Adsp_Hsclk,
                {(sizeof(regProgClocks_Adsp_8996)/sizeof(regProgClocks_Adsp_8996[0])), &regProgClocks_Adsp_8996[0]},
                {.icbarbSlave = ICBID_SLAVE_LPASS},
                AdsppmBusPort_Adsp_Master
        }
};

AsicBusPortDescriptorArrayType busPorts_8996 = {(sizeof(busPorts_array_8996)/sizeof(busPorts_array_8996[0])), &busPorts_array_8996[0]};




const AdsppmBusRouteType extBusRoutes_array_8996[] =
{
        {
                AdsppmBusPort_Adsp_Master,
                AdsppmBusPort_Ddr_Slave
        },
        {
                AdsppmBusPort_Adsp_Master,
                AdsppmBusPort_PerifNoc_Slave
        },
        {
                AdsppmBusPort_Ext_Ahb_Master,
                AdsppmBusPort_Ddr_Slave
        }
};

AsicBusRouteArrayType extBusRoutes_8996 = {(sizeof(extBusRoutes_array_8996)/sizeof(extBusRoutes_array_8996[0])), &extBusRoutes_array_8996[0]};

const AdsppmBusRouteType mipsBwRoutes_array_8996[] =
{
        {
                AdsppmBusPort_Adsp_Master,
                AdsppmBusPort_Ddr_Slave
        }
};

AsicBusRouteArrayType mipsBwRoutes_8996 = {(sizeof(mipsBwRoutes_array_8996)/sizeof(mipsBwRoutes_array_8996[0])), &mipsBwRoutes_array_8996[0]};




const uint64 regProgSpeeds_array_8996[] =
{
        0,
        19200000*4,
        61410000*4
};

AsicRegProgSpeedArrayType regProgSpeeds_8996 = {(sizeof(regProgSpeeds_array_8996)/sizeof(regProgSpeeds_array_8996[0])), &regProgSpeeds_array_8996[0]};




const AsicPowerDomainDescriptorType pwrDomains_array_8996[] =
{
        {
                AsicPowerDomain_AON,
                "",
                AsicRsc_None,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_Adsp,
                "/core/cpu/latency",
                AsicRsc_Power_ADSP,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_LpassCore,
                "VDD_AUDIO_CORE",
                AsicRsc_Power_Core,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_Lpm,
                "",
                AsicRsc_Power_Mem,
                AdsppmClk_Lpm_cbc,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_SRam,
                "",
                AsicRsc_None,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_Ocmem,
                "",
                AsicRsc_None,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        },
        {
                AsicPowerDomain_Hvx,
                "/clk/hvx",
                AsicRsc_Power_Hvx,
                AdsppmClk_None,
                DALIPCINT_NUM_INTS,
                0,
                {0, 0}
        }
};

AsicPwrDescriptorArrayType pwrDomains_8996 = {(sizeof(pwrDomains_array_8996)/sizeof(pwrDomains_array_8996[0])), &pwrDomains_array_8996[0]};






const AsicCompensatedDdrBwTableEntryType compensatedDdrBw_array_8996[] = {

        { 10000000, 153000000, 0, 0, 0 },
        { 60000000, 399000000, 0, 0, 0 },
        { 96000000, -1, 600, 50, 1 },
        { 120000000, -1, 450, 50, 1 },
        { 155000000, -1, 450, 100, 1 },
        { 230000000, -1, 300, 150, 1 },
        { 250000000, -1, 300, 200, 1 },
        { 0xffffffffffffffffULL, -1, 300, 266, 1 }
};

AsicCompensatedDdrBwTableType compensatedDdrBwTable_8996 = {(sizeof(compensatedDdrBw_array_8996)/sizeof(compensatedDdrBw_array_8996[0])), &compensatedDdrBw_array_8996[0]};

const AsicCompensatedAhbBwTableEntryType compensatedAhbBw_array_8996[] = {

    { 9784305, 61440000 },
    { 19568611, 122880000 },
    { 39137222, 245760000 },
    { 490240000, 490240000 },
    { 0xffffffffffffffffULL, -1 },
};

AsicCompensatedAhbBwTableType compensatedAhbBwTable_8996 = {(sizeof(compensatedAhbBw_array_8996)/sizeof(compensatedAhbBw_array_8996[0])), &compensatedAhbBw_array_8996[0]};

const AsicThreadLoadingInfoType threadLoadingData_array_8996[] =
{
        { 90, 10, 0, 0 },
        { 65, 25, 10, 0 },
        { 50, 30, 20, 0 }
};

AsicThreadLoadingInfoTableType threadLoadingData_8996 = {(sizeof(threadLoadingData_array_8996)/sizeof(threadLoadingData_array_8996[0])), &threadLoadingData_array_8996[0]};

const uint32 audioVoiceCppFactors_array_8996[] =
{
        3500,
        2500,
        1700,
           0
};

AsicAudioVoiceCppTrendFactorsType audioVoiceCppFactors_8996 = {
        (sizeof(audioVoiceCppFactors_array_8996)/sizeof(audioVoiceCppFactors_array_8996[0])), &audioVoiceCppFactors_array_8996[0]};






const AsicCachePartitionConfigType cachePartitionConfig_array_8996[] = {



    { MMPM_AUDIO_CLIENT_CLASS |
      MMPM_COMPUTE_CLIENT_CLASS, HALF_SIZE },

    { MMPM_VOICE_CLIENT_CLASS |
      MMPM_COMPUTE_CLIENT_CLASS, HALF_SIZE },

    { MMPM_AUDIO_CLIENT_CLASS |
      MMPM_VOICE_CLIENT_CLASS |
      MMPM_COMPUTE_CLIENT_CLASS, HALF_SIZE },

};

AsicCachePartitionConfigTableType cachePartitionConfigTable_8996 =
    { (sizeof(cachePartitionConfig_array_8996)/sizeof(cachePartitionConfig_array_8996[0])), &cachePartitionConfig_array_8996[0] };

const AsicBwConcurrencySettingsType bwConcurrencySettings_8996 =
{

    3,
    {
        256,
        384,
        512,
    }
};

const AsicAdspToAhbeFreqTableEntry adspToAhbeFreq_array_8996v1[] = {

    { 172800000, 38400000 },
    { 249600000, 76800000 },
    { 422400000, 153600000 },
    { 480600000, 153600000 },
};

AsicAdspToAhbeFreqTableType adspToAhbeFreqTable_8996v1 =
    { (sizeof(adspToAhbeFreq_array_8996v1)/sizeof(adspToAhbeFreq_array_8996v1[0])), &adspToAhbeFreq_array_8996v1[0] };

const AsicAdspToAhbeFreqTableEntry adspToAhbeFreq_array_8996v2[] = {

    { 297600000, 38400000 },
    { 460800000, 76800000 },
    { 652800000, 153600000 },
    { 729600000, 153600000 },
};

AsicAdspToAhbeFreqTableType adspToAhbeFreqTable_8996v2 =
    { (sizeof(adspToAhbeFreq_array_8996v2)/sizeof(adspToAhbeFreq_array_8996v2[0])), &adspToAhbeFreq_array_8996v2[0] };

const AsicAdspToAhbeFreqTableEntry adspToAhbeFreq_array_8996v3[] = {

    { 297600000, 38400000 },
    { 480000000, 76800000 },
    { 652800000, 153600000 },
    { 825600000, 153600000 },
};

const AsicAdspCacheSizeBWScalingTableEntry adspcachesizebwscaling_array_8996[] = {
    { FULL_SIZE, 67 },
    { THREE_QUARTER_SIZE, 100 },
    { HALF_SIZE, 100 },
};

AsicAdspCacheSizeBWScalingTableType adspcachesizebwscaling_8996 = {(sizeof(adspcachesizebwscaling_array_8996)/sizeof(adspcachesizebwscaling_array_8996[0])), &adspcachesizebwscaling_array_8996[0]};

AsicAdspToAhbeFreqTableType adspToAhbeFreqTable_8996v3 =
    { (sizeof(adspToAhbeFreq_array_8996v3)/sizeof(adspToAhbeFreq_array_8996v3[0])), &adspToAhbeFreq_array_8996v3[0] };

const AsicFeatureDescType features_8996 [AsicFeatureId_enum_max] =
{

        {
                AsicFeatureState_Enabled,



                80,

                825,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                76000000,
                614400000,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Disabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                32768,
                0x7FFFFFFF,
        },

        {
                AsicFeatureState_Enabled,
                0,
                307200000,
        },

        {
                AsicFeatureState_Enabled,
                10000000,
                0x7FFFFFFF,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Disabled,
                240,
                905
        },

        {
                AsicFeatureState_Disabled,
                76000000*8,
                0x7FFFFFFF,
        },

        {
                AsicFeatureState_Disabled,
                60000000,
                153600000,
        },

        {
                AsicFeatureState_Disabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Disabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

        {
                AsicFeatureState_Enabled,
                0,
                0,
        },

};
