/*==============================================================================
  FILE:         sleep_os.c

  OVERVIEW:     This file provides the sleep task and OS-specific
                functionality for the processors with Qurt running on them.

  DEPENDENCIES: sleepOS_mainTask() is the entry point to the lowest-priority
                thread processing.

                Copyright (c) 2011-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/src/os/qurt/sleep_os.c#5 $
$DateTime: 2015/07/16 09:52:19 $
==============================================================================*/
#include <stdlib.h>
#include "sleepi.h"
#include "rcinit.h"
#include "qurt.h"
#include "npa.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"
#include "synthRegistry.h"
#include "timer.h"
#include "sleep_config.h"
#include "CoreIni.h"
#include "sleep_npa_scheduler.h"
#include "sleep_os.h"
#include "sleep_osi.h"
#include "sleep_pmi.h"
#include "sleep_idle_plugin.h"
#include "sleep_target.h"
#include "sleep_solveri.h"
#include "lookup_table_solver.h"
#include "rpmclient.h"
#include "sleep_stats.h"
#include "sleep_statsi.h"
#include "DDIClock.h"
#include "sleep_utils.h"
#include "sleepActivei.h"

#ifdef USLEEP_ISLAND_MODE_ENABLE
#include "uSleep.h"
#include "uSleep_os.h"
#endif

/*==============================================================================
                                  DEFINES
 =============================================================================*/
/* Size of main sleep task stack */
#define SLEEP_STACK_SIZE      2560

/* Sleep task priority, (needs to be lowest in the system) */
#define SLEEP_TASK_PRI        254

/* Size of PMI handler task stack */
#define SLEEP_PMI_STACK_SIZE  2048

/* PMI handler task priority */
#define SLEEP_PMI_TASK_PRI    8   /* as per OS team's suggestion */

/* PMI interupt ID and signal mask used to register the OS signal
 * that will be triggered as part of the APCR bringup sequence. */
#define SLEEP_PMI_ID          0
#define SLEEP_PMI_MASK        (1 << 0)

/* Max number of characters in the full path to the sleep ini file. */
#define MAX_INI_PATH_LEN      64

/* Macro to check if any interrupt is pending at L2Vic.
 *
 * On QDSP6 V4 and above, the L2 Interrupt Controller is attached to INT31
 * of QDSP6'es IPEND register. All User configurable interrupts in the system
 * are attached ot L2 VIC. This makes it sufficient to check for L2 MSB 
 */
#define IPEND_IS_MSB_SET(ipend_status)  ((ipend_status) & (1 << 31))

/* PMI handler thread name */
#define PMI_HANDLER_THREAD_NAME "PMI_Handler"

/*==============================================================================
                          INTERNAL TYPE DEFINITIONS
 =============================================================================*/
/**
 * sleep_handles
 *
 * @brief Main sleep structure for various node and device handles used 
 *        during the current sleep cycle
 */
typedef struct sleep_handles_s
{
  /* Handle used to query the /core/cpu/wakeup node */
  npa_query_handle  wakeup_node_query_handle;

  /* Handle used to query the /sleep/max_duration node */
  npa_query_handle max_duration_node_query_handle;

  /* Handle used to query the /core/cpu/latency node */
  npa_query_handle  latency_node_query_handle;

  /* Handle used to query the /sleep/lpr node */
  npa_query_handle  sleep_lpr_query_handle;

  /* Handle used to query the /clk/cpu resource */
  npa_query_handle  cpu_clk_freq_query_handle;

  /* DAL Client of Clock driver for requesting cpu frequency during Sleep */
  DalDeviceHandle   *dal_clk_handle;
}sleep_handles;

/**
 * sleep_state
 *
 * @brief Main sleep state structure used for current sleep 
 *        cycle data
 */
typedef struct sleep_state_s
{
  /* The synthLPRM to notify if the sleep duration changes.
   * This is the output mode of the solver */
  sleep_synth_lprm *selected_synthLPRM;

  /* Copy of hard deadline for NPA (expected sleep exit time) in absolute ticks */
  uint64 hard_deadline_copy;

  /* Solver input object containing various restrictions for a given
   * sleep cycle. */
  sleep_solver_input *solver_input;

  /* Object containing output from Sleep solver based on various
   * input restrictions. */
  sleep_solver_output *solver_output;

  /* Object containing various statistics related information from
   * the last complete sleep cycle.*/
  sleepStats_input cycle_stats;

  /* Flag to avoid unnecessary execution of Sleep exit path logic 
   *
   * This flag is useful in two scenarios:
   * 1. Sleep was bailed due to one of the many restrictions.
   * 2. Target(Q6) woke up from APCR based low power modes and by the time
   *    control comes to Sleep task, exit logic is already executed from
   *    IST context. We don't need to execute it again.
   *
   * Must be set to TRUE if we were to enter solver based low power modes
   * at idle time and to FALSE while executing exit logic.
   */
  boolean execute_sleep_exit_path;

  /* Flag to indicate if processor is running in sleep context. */
  boolean processor_in_stm;

}sleep_state;

/*==============================================================================
                           FORWARD DECLARATIONS
 =============================================================================*/
static uint32 sleepOS_performLPM(void);
static uint32 sleepOS_override(void);
static uint32 sleepOS_holdoffNoMode(void);
static void sleepOS_performLPMExit(void);

void sleepHoldoff_registerRCinitCompletionEvent(void);
static void sleepOS_pmiHandler(void *context) USLEEP_CODE_SECTION;

/*==============================================================================
                           EXTERNAL VARIABLES
 =============================================================================*/
/* Stores target specific config */
extern sleep_target_config g_sleepTargetConfig;

/*==============================================================================
                           INTERNAL VARIABLES
 =============================================================================*/
/* Global sleep state that contains various pieces of data used in the current sleep cycle */
static sleep_state g_sleepState = {0};

/* Various node and device handles */
static sleep_handles g_sleepHandles;

/* Boolean that indicates whether we should enter any low power modes or not.
 *
 * This variable provides a simpler mechanism to disable low power modes
 * for developing/testing features that are not quite dependent on low 
 * power modes. Note that in the production code, it will be set so that
 * low power modes are always allowed.
 */
#ifdef FEATURE_DISABLE_SLEEP_MODES
volatile boolean g_sleepAllowLowPowerModes = FALSE;
#else
volatile boolean g_sleepAllowLowPowerModes = TRUE;
#endif

/*==============================================================================
                   INTERNAL VARIABLES IN ISLAND SECTION
 =============================================================================*/
/* Function pointer table for different Sleep behavior based on various
 * restrictions
 *
 *  DEFAULT   : Execute Idle-time Solver
 *  OVERRIDE  : qurt_override_wait_for_idle is used to perform lpm
 *  LPI       : Low power island function is used
 *  HOLD_OFF  : No Low-power Mode selection is performed
 */
static sleep_idle_entry_ptr_type g_sleepIdleEntryTable[SLEEP_OS_IDLE_MODE_NUM_STATES] USLEEP_DATA_SECTION = 
{
  sleepOS_performLPM,      /* SLEEP_OS_IDLE_DEFAULT */
  sleepOS_override,        /* SLEEP_OS_IDLE_MODE_OVERRIDE */
  NULL,                    /* SLEEP_OS_LPI */
  sleepOS_holdoffNoMode    /* SLEEP_OS_IDLE_HOLD_OFF */
};

/* Pointer to use as the entry point when the system is idle.
 * This pointer is controlled by the /sleep/idle/plugin node. */
static volatile sleep_idle_entry_ptr_type * volatile g_idleEntryPtr USLEEP_DATA_SECTION = &g_sleepIdleEntryTable[SLEEP_OS_IDLE_MODE_HOLD_OFF];

/* Function pointer to use for PMI exit handling. The pointer is set based on
 * if the system is in normal operational or island mode */
static volatile sleep_PMI_handler_ptr_type g_PMIFncPtr USLEEP_DATA_SECTION = NULL;

/* Main sleep task stack must be in TCM since task is also located in TCM. To
 * do that we have to statically declare an array in island section */
static uint8 g_sleepTaskStack[SLEEP_STACK_SIZE] USLEEP_DATA_SECTION;

/* PMI handler task stack must be in TCM since task is also located in TCM. To
 * do that we have to statically declare an array in island section */
static uint8 g_sleepPMITaskStack[SLEEP_PMI_STACK_SIZE] USLEEP_DATA_SECTION;

/* Signal that the PMI task waits on from the OS to begin the APCR exit process */ 
static qurt_anysignal_t g_pmiSignal USLEEP_DATA_SECTION;

/*==============================================================================
                       INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/**
 * sleepOS_pmiHandler
 *
 * @brief Function responsible for handling Power Manager Interrupt (PMI).
 *
 * @param context: Thread context or data. (used for prototype compliance only)
 */
static void sleepOS_pmiHandler(void *context)
{
  /* Registering for PMI and associated signal */
  qurt_anysignal_init(&g_pmiSignal);

  /* Interrupt registration must be done in the context of the handler. */
  if(QURT_EOK != qurt_interrupt_register(SLEEP_PMI_ID, &g_pmiSignal,
                                         SLEEP_PMI_MASK))
  {
    sleepLog_printf(SLEEP_LOG_LEVEL_ERROR, 0, 
                    "Power Management Interrupt Registration failed");
    CORE_VERIFY(0);
  }

  /* PMI interrupt monitoring and handling task */
  while(1)
  {
    qurt_anysignal_wait(&g_pmiSignal, SLEEP_PMI_MASK);
   
    /* Call correct handler */
    g_PMIFncPtr();

    /* Signal is handled - Clear it */
    qurt_anysignal_clear(&g_pmiSignal, SLEEP_PMI_MASK);

    /* Interrupt has been handled, inform QuRT */
    qurt_interrupt_acknowledge(SLEEP_PMI_ID);
  }
}

/**
 * sleepOS_registerPMIHandler
 *
 * @brief As part interrupt registration with QuRT, this function creates 
 *        a thread that will execute function to handle the Power Management
 *        Interrupt (PMI).
 *
 * @note As per pthread specifics, must be called from thread which does not terminate
 *       before the newly created thread does i.e. usually from Sleep task entry 
 *       function.
 */
static void sleepOS_registerPMIHandler(void)
{
  qurt_thread_t      pmiHandlerThread;
  qurt_thread_attr_t pmiHandlerAttr;
  
  /* Set default handler */
  sleepPMI_setHandlerFunctionPTR(sleepOS_PMIPerformer);

  /* Initialize attribute */
  qurt_thread_attr_init(&pmiHandlerAttr);

  /* setting up attributes for pmi handler thread */
  qurt_thread_attr_set_name(&pmiHandlerAttr, PMI_HANDLER_THREAD_NAME);
  qurt_thread_attr_set_tcb_partition(&pmiHandlerAttr, 1); /* locate in uImage */
  qurt_thread_attr_set_priority(&pmiHandlerAttr, SLEEP_PMI_TASK_PRI );
  qurt_thread_attr_set_stack_size(&pmiHandlerAttr, SLEEP_PMI_STACK_SIZE);
  qurt_thread_attr_set_stack_addr(&pmiHandlerAttr, (void *)g_sleepPMITaskStack);
  
  /* Fill in the stack with a marker to try and find overflows */
  memset(g_sleepPMITaskStack, 0xF8, sizeof(g_sleepPMITaskStack));

  /* Creating thread to run PMI handler */
  CORE_VERIFY(QURT_EOK == qurt_thread_create(&pmiHandlerThread, &pmiHandlerAttr,
                                             sleepOS_pmiHandler, NULL));

  return;
}

/**
 * sleepOS_qurtInit
 * 
 * @brief Initialize the sleep subsystem.
 */
static void sleepOS_qurtInit( void )
{
  /* Get clock handle first - Subsequent initialization can result in 
   * cpu frequency query through ATS. Initialize query handler first */
  CORE_VERIFY_PTR(g_sleepHandles.cpu_clk_freq_query_handle =
                  npa_create_query_handle("/clk/cpu"));

  /* Initialize the logging subsystem. */
  sleepLog_initialize();
  sleepLog_setLevel( SLEEP_LOG_LEVEL_MAX );

  /* Initialize target-independent NPA nodes. */
  sleepNPA_initialize();

  /* Initialize LPR / LPRM tracking subsystem. */
  sleepLPR_initialize();

  /* Initialize statistical data system */
  sleepStats_init();

  /* Initialize target-dependent stuff (nodes, modes, etc.). */
  sleepTarget_initialize();

  /* Initialize the uSleep system */
#ifdef USLEEP_ISLAND_MODE_ENABLE
  uSleepOS_initialize();
#endif

  /* Register for RCInit completion event */
  sleepHoldoff_registerRCinitCompletionEvent();

  /* Register interrupt handler for interrupt 0 (also known as Power 
   * Management Interrupt) */
  sleepOS_registerPMIHandler();

  /* Try to get query handles for the resources we just created.
     These should be ready immediately, but check them anyway. */
  CORE_VERIFY_PTR(g_sleepHandles.wakeup_node_query_handle =
                  npa_create_query_handle( "/core/cpu/wakeup") );

  CORE_VERIFY_PTR(g_sleepHandles.max_duration_node_query_handle =
                  npa_create_query_handle( "/sleep/max_duration") );

  CORE_VERIFY_PTR(g_sleepHandles.latency_node_query_handle =
                  npa_create_query_handle( "/core/cpu/latency") );

  CORE_VERIFY_PTR(g_sleepHandles.sleep_lpr_query_handle =
                  npa_create_query_handle( "/sleep/lpr") );

  /* Set the initial solver configuration. */
  sleepSolver_setSolverFunction(&sleep_lookup_table_solver);

  /* Attaching Dal handle to clock driver */
  CORE_DAL_VERIFY(DAL_DeviceAttach(DALDEVICEID_CLOCK, 
                                   &g_sleepHandles.dal_clk_handle));

  return;
}

/** 
 * sleepOS_setFrequency 
 *  
 * @brief Sets max frequency at current voltage when executing sleep.
 *
 * This function sets the max frequency for current voltage level when 
 * executing the sleep solver and executing low power modes.
 *
 * @return Returns the new/updated frequency in KHz.
 */
static uint32 sleepOS_setFrequency(void)
{
  uint32  cpuFreqHz;
  uint32  cpuFreqKhz;
  bool    bRequired = TRUE;

  CORE_VERIFY_PTR(g_sleepHandles.dal_clk_handle);

  CORE_DAL_VERIFY(DalClock_SetCPUMaxFrequencyAtCurrentVoltage(
                   g_sleepHandles.dal_clk_handle,
                   bRequired,
                   &cpuFreqHz) );

  cpuFreqKhz = cpuFreqHz / 1000;
  
  sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 1, 
                  "Sleep CPU frequency set (%u Khz)", cpuFreqKhz);

  return cpuFreqKhz;
}

/**
 * sleepOS_unsetFrequency 
 *
 * @brief Releasing Sleep request on CPU clock
 */
static void sleepOS_unsetFrequency(void)
{
  uint32  cpuFreqHz;
  bool    bRequired = FALSE;

  CORE_DAL_VERIFY(DalClock_SetCPUMaxFrequencyAtCurrentVoltage(
                   g_sleepHandles.dal_clk_handle,
                   bRequired,
                   &cpuFreqHz) );

  return;
}

/**
 * sleepOS_waitForIdle 
 *
 * @brief Suspend Sleep until processor is idle
 *  
 * @note  This call prepares for power collapse and masks the 
 *        global interrupt if HW threads are all wait
 * 
 */
static void sleepOS_waitForIdle(void)
{
  qurt_power_wait_for_idle();
  return;
}

/**
 * sleepOS_enterIdle 
 *
 * @brief Execute Halt until interrupt
 */
static void sleepOS_enterIdle(void)
{
  /* Wait for active exits STM, clear flag */
  g_sleepState.processor_in_stm = FALSE;

  /* Unmask global interrupt which was masked in
   * call to sleep_os_wait_for_idle() and places 
   * sleep task in ready queue. 
   * Returns when any other thread is awakened */
  qurt_power_wait_for_active();
  return;
}

/** 
 * sleepOS_exitSTM 
 *  
 * @brief This function reverses any special requirements for sleep task
 *        It is designed to be called before exiting sleepOS_performLPM
 */
static void sleepOS_exitSTM(void)
{
  /* We are about to exit from Sleep context - set flag to false */
  g_sleepState.processor_in_stm = FALSE;

  sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING,
                      SLEEP_EARLY_EXIT_STM_NUM_ARGS,
                      SLEEP_EARLY_EXIT_STM_STR, 
                      SLEEP_EARLY_EXIT_STM);

  g_sleepState.cycle_stats.sleep_exit_stm = CoreTimetick_Get64();
  qurt_power_exit();

  return;
}

/**
 * sleepOS_performLPMEnter
 * 
 * @brief Function to Enter low-power mode selected by Full Active Time
 *        solver.
 *
 * @param context: Reference to the context from which it is being called. 
 *  
 * @return TRUE if sleep mode was entered 
 *         FALSE if any error or early exit occured 
 */
static boolean sleepOS_performLPMEnter(void* context)
{
  uint64               sleepStart;
  sleepOS_IdleModeType idleModeCfg = (sleepOS_IdleModeType)context;

  /* Execute common sleep entry checks */
  if(0 == (sleepStart = sleepOS_prepareForSleep(idleModeCfg)))
  {
    return FALSE;
  }

  /* Setting flag to indicate that processor is in Sleep context */
  g_sleepState.processor_in_stm = TRUE;

  /* Disable deferrable timers */
  sleepTarget_deferTimers();

  /* Query for pointer to solver input parameters to the Active time solver  */
  g_sleepState.solver_input = sleepActive_GetSolverInput();

  /* Query for pointer to the last selected mode by Active time solver */
  g_sleepState.solver_output = sleepActive_GetSolverOutput();

  /* synth lprm to notify if sleep duration changes */
  g_sleepState.selected_synthLPRM = g_sleepState.solver_output->selected_synthLPRM;

  /* Set selected mode for later use in statistics */
  g_sleepState.cycle_stats.mode = g_sleepState.solver_output->selected_synthLPRM;

  /* Set fLUT for later use in statistics */
  g_sleepState.cycle_stats.fLUT = g_sleepState.solver_input->fLUT;

  /* Setting a flag to indicate that after this point we will need to
   * perform exit logic */
  g_sleepState.execute_sleep_exit_path = TRUE;

  /* Copy hard_deadline value used for solver input to local copy for use
   * by NPA */
  g_sleepState.hard_deadline_copy = g_sleepState.solver_input->hard_deadlines.minimum;

  /* Calculate the backoff deadline, and pass that to the enter functions. */
  g_sleepState.cycle_stats.backoff_deadline = 
    g_sleepState.hard_deadline_copy - *g_sleepState.solver_output->backoff_ptr;
 
  /* Time in STM - not sleep start time */
  sleepStart = CoreTimetick_Get64() - sleepStart;

  sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 2 + (2*2),
                  "Entering modes (hard deadline: 0x%llx) "
                  "(backoff deadline: 0x%llx) "
                  "(backoff: 0x%x) "
                  "(Sleep entry time: %d)",
                  ULOG64_DATA(g_sleepState.hard_deadline_copy),
                  ULOG64_DATA(g_sleepState.cycle_stats.backoff_deadline),
                  *g_sleepState.solver_output->backoff_ptr,
                  (uint32)sleepStart);

  synthLPRM_enter(g_sleepState.solver_output->selected_synthLPRM, 
                  g_sleepState.cycle_stats.backoff_deadline, 
                  g_sleepState.cycle_stats.fLUT);

  return TRUE;
}

/** 
 * sleepOS_performLPMExit 
 *  
 * @brief Function to execute Exit path while coming out of solver based 
 *        low power modes during idle time.
 *
 * Depending on the low power modes we enter, this could be called either
 * from Sleep or an IST context.
 *
 * @param context: Reference to context from which it is being called 
 *                 (unused for now - more for complying with prototype
 *                 of interrupt handler). But if we need to differentiate
 *                 this could be used.
 *
 * @return none
 */
static void sleepOS_performLPMExit(void)
{
  uint64        currTick     = CoreTimetick_Get64();
  static uint32 WBCount      = 0;
  uint32        used_backoff = *g_sleepState.solver_output->backoff_ptr;

  sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 0, "Exiting modes");

  /* Logging point of return to stats module */
  sleepStats_putLprTimeData(currTick, SLEEP_STATS_TIME_MSTR_RETURN_TYPE);

  /* Setting the flag to FALSE to avoid any unnecessary execution 
   * of the exit path afterwards. */
  g_sleepState.execute_sleep_exit_path = FALSE;

  /* Get VID register value to report last interrupt that fired */
  g_sleepState.cycle_stats.master_interrupt = qurt_system_vid_get();

  g_sleepState.selected_synthLPRM = NULL;

  /* Exit functions of low power modes */
  synthLPRM_exit(g_sleepState.solver_output->selected_synthLPRM, 
                 g_sleepState.cycle_stats.fLUT);

  /* NPA scheduler can update the hard dealine as part of the enter function. 
   * Set backoff deadline to updated value while still in STM */
  g_sleepState.cycle_stats.backoff_deadline = 
    g_sleepState.hard_deadline_copy - used_backoff;

  /* Get sleep wakeup time & reason as recorded from standalone or RPM modes */ 
  g_sleepState.cycle_stats.actual_wakeup_time = 
    sleepStats_getLprTimeData(SLEEP_STATS_TIME_WAKEUP_TYPE);

  g_sleepState.cycle_stats.master_wakeup_reason = 
    (sleepStats_wakeup_reason)sleepStats_getMiscData(SLEEP_STATS_MISC_WAKEUP_REASON_TYPE);

  /* Fill in remaining stat input data for logging */ 
  g_sleepState.cycle_stats.hard_deadline = g_sleepState.hard_deadline_copy;

  /* Get last OS boot statistics */
  g_sleepState.cycle_stats.os_overhead.count =     
    qurt_power_shutdown_get_hw_ticks(&g_sleepState.cycle_stats.os_overhead.sleep_time,
                                     &g_sleepState.cycle_stats.os_overhead.awake_time);

  /* Set count to 0 if we performed APCR and WB didn't run which would mean
   * the timestamps are not valid */
  if(WBCount == g_sleepState.cycle_stats.os_overhead.count)
  {
    g_sleepState.cycle_stats.os_overhead.count = 0;
  }
  else
  {
    WBCount = g_sleepState.cycle_stats.os_overhead.count;
  }

  /* Sleep Exit STM */
  sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING, 
                      SLEEP_EXIT_STM_NUM_ARGS,
                      SLEEP_EXIT_STM_STR, 
                      SLEEP_EXIT_STM);

  /* We are about to exit from Sleep context - set flag to false */
  g_sleepState.processor_in_stm = FALSE;

  /* Actual sleep exit STM time */ 
  g_sleepState.cycle_stats.sleep_exit_stm = CoreTimetick_Get64();

  /* Exit STM
   * All statistical data must be filled in prior to exiting STM or a race
   * condition will be created between processing in the sleep task and the
   * PMI exit handler. */
  qurt_power_exit();
  sleepTarget_undeferTimers();

  sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING, 
                      SLEEP_EXIT_IDLE_NUM_ARGS, 
                      SLEEP_EXIT_IDLE_STR, 
                      SLEEP_EXIT_IDLE,
                      ULOG64_DATA(g_sleepState.cycle_stats.sleep_exit_stm));
  
  return;
}

/**
 * sleepOS_holdoffNoMode
 *
 * @brief This function simply returns and does not perform any type of low 
 *        power mode. Usually this is during system initialization or in a
 *        situation where we don't want target to enter any low power modes
 *        at all (e.g. debug, bring up). 
 *
 * @note Controlled via idle plugin.
 */
static uint32 sleepOS_holdoffNoMode(void)
{
  /* Due to some holdoff client, we cannot simply perform any low power modes.
   * Simply return. At some point, sleep function pointer would change when all
   * holdoff requests are released. Until then, we will keep coming here. */
  return 0;
}

/**
 * sleepOS_override
 *
 * @brief This function is used to enter all wait or cached low power modes.
 *
 * This function puts calling task (Sleep) in to waiting state until all 
 * other task/thread go idle. It must be called when OS override
 * flag is set. That will avoid scheduling Sleep task when all threads go
 * idle. We will get unblocked in this function only when some other task
 * (e.g. ATS) clears OS override flag.
 *
 * @return Always 0 (for prototype compliance).
 */
static uint32 sleepOS_override(void)
{
  /* Wait for all other HW threads to go idle. */
  sleepOS_waitForIdle();

  sleepLog_printf( SLEEP_LOG_LEVEL_DEBUG, 0, "OS override is disabled" );

  /* Unmask global interrupt which was masked in above call */
  qurt_power_exit();

  return 0;
}

/*==============================================================================
                       EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/*
 * sleepOS_prepareForSleep
 */
uint64 sleepOS_prepareForSleep(sleepOS_IdleModeType idleModeCfg)
{
  uint64 sleepStart;

  /* Wait for all other HW threads to go idle. If the override setting is 
   * disabled, this function will return and the system will already be in
   * STM. */
  sleepOS_waitForIdle();

  /* Mark start of this sleep cycle */
  sleepStart = CoreTimetick_Get64();
  sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING, SLEEP_ENTER_IDLE_NUM_ARGS,
                      SLEEP_ENTER_IDLE_STR, 
                      SLEEP_ENTER_IDLE,
                      (uint32)idleModeCfg);

  /* Check to ensure that no rpm msgs are in-flight */
  if(FALSE == rpm_is_mq_empty())
  {
    sleepLog_QDSSEvent( SLEEP_EARLY_EXIT_STM_NUM_ARGS,
                        SLEEP_EARLY_EXIT_STM);

    sleepLog_printf(SLEEP_LOG_LEVEL_DEBUG, 0,
                    "Short SWFI (reason: RPM message in flight)" );
    sleepStart = 0;
    sleepOS_enterIdle();
  }
  /* Check the flag that allows low power modes */
  else if(0 == g_sleepAllowLowPowerModes)
  {
    sleepStart = 0;
    sleepOS_enterIdle();
  }
  /* Ensure that sleep mode function has not changed */
  else if(FALSE == sleepOS_verifyIdleModeConfig(idleModeCfg))
  {
    sleepLog_QDSSEvent(SLEEP_EARLY_EXIT_STM_NUM_ARGS,
                       SLEEP_EARLY_EXIT_STM);

    sleepLog_printf(SLEEP_LOG_LEVEL_DEBUG, 0, 
                    "Sleep cycle skipped "
                    "(reason: Idle Plugin state changed)");

    sleepStart = 0;
    qurt_power_exit();
  }

  return sleepStart;
}

/*
 * sleepOS_getFrequency
 */
uint32 sleepOS_getFrequency(void)
{
  npa_query_type query;

  query.data.value = 0;

  CORE_VERIFY(NPA_QUERY_SUCCESS == 
                npa_query(g_sleepHandles.cpu_clk_freq_query_handle, 
                          NPA_QUERY_CURRENT_STATE,
                          &query) );

  return query.data.value;
}

/*
 * sleepOS_PMIPerformer
 */
void sleepOS_PMIPerformer(void)
{
  /* Calling PMI handler */
  sleepOS_performLPMExit();
  return;
}

/*
 * sleepOS_getLastTimerDeadline
 */
uint64 sleepOS_getLastTimerDeadline(void)
{
  return g_sleepState.solver_input->timer_deadline;
}

/*
 * sleepOS_offsetSleepDuration
 */
void sleepOS_offsetSleepDuration(uint64 offset)
{
  /* Adjust the sleep duration, so that the new duration gets passed
   * to the rest of the enter functions. */
  if(NULL != g_sleepState.selected_synthLPRM)
  {
    g_sleepState.selected_synthLPRM->wakeup_tick += offset;
  }

  /* Adjust the hard deadline as well, in order to correctly calculate
   * whether we woke up late before exiting sleep. */
  g_sleepState.hard_deadline_copy += offset;

  return;
}

/*
 * sleepOS_getHardDeadline
 */
uint64 sleepOS_getHardDeadline(void)
{
  return g_sleepState.hard_deadline_copy;
}

/*
 * sleepOS_verifyIdleModeConfig
 */
boolean sleepOS_verifyIdleModeConfig(sleepOS_IdleModeType idleMode)
{
  CORE_VERIFY(idleMode < SLEEP_OS_IDLE_MODE_NUM_STATES);
  return (*g_idleEntryPtr == g_sleepIdleEntryTable[idleMode])? TRUE : FALSE;
}

/*
 * sleepOS_setLPIEntryFunction
 */
void sleepOS_setLPIEntryFunction(sleep_idle_entry_ptr_type lpiFunction)
{
  g_sleepIdleEntryTable[SLEEP_OS_IDLE_MODE_LPI] = lpiFunction;
  return;
}

/*
 * sleepOS_getLPIEntryFunction
 */
sleep_idle_entry_ptr_type sleepOS_getLPIEntryFunction(void)
{
  return g_sleepIdleEntryTable[SLEEP_OS_IDLE_MODE_LPI];
}

/*
 * sleepOS_configIdleMode
 */
void sleepOS_configIdleMode(sleepOS_IdleModeType  newIdleMode)
{
  CORE_VERIFY(newIdleMode < SLEEP_OS_IDLE_MODE_NUM_STATES);
  g_idleEntryPtr = &g_sleepIdleEntryTable[newIdleMode];

  /* Mode specific action - currently it is just for one mode but if this 
   * grows, preferably use switch case. */
  if( SLEEP_OS_IDLE_MODE_OVERRIDE == newIdleMode )
  {
    /* No Sleep task intervention - QuRT performs all wait directly. */
    qurt_power_override_wait_for_idle(TRUE);
  }
  else
  {
    /* Sleep task needs to be scheduled. */
    qurt_power_override_wait_for_idle(FALSE);    
  }

  return;
}

/*
 * sleepPMI_setHandlerFunctionPTR
 */
void sleepPMI_setHandlerFunctionPTR(sleep_PMI_handler_ptr_type newPMIPtr)
{
  g_PMIFncPtr = newPMIPtr;
  return;
}

/*
 * sleepOS_createConfigHandle
 */
CoreConfigHandle sleepOS_createConfigHandle(void)
{
#if 0
  char path[MAX_INI_PATH_LEN];

  /* Build the path to the sleep_config.ini file */
  snprintf(path, MAX_INI_PATH_LEN,
           "/nv/item_files/sleep/core0/sleep_config.ini");

  return(CoreIni_ConfigCreate(path));
#else
  return NULL;
#endif
}

/**
 * sleepOS_destroyConfigHandle
 */
void sleepOS_destroyConfigHandle(CoreConfigHandle config)
{
  /*CoreIni_ConfigDestroy(config);*/
  return;
}

/**
 * sleepOS_readEFSConfig 
 *  
 * @brief Initialize the sleep LPR subsystem.  When this callback is 
 *        called, the EFS subsystem has been initialized.  We rely on
 *        that in order to read the config files.
 *
 * @note Not all masters have access to EFS and require this function.
 */
void sleepOS_readEFSConfig(void)
{
#if 0
  boolean       sleep_config_value  = FALSE;
  unsigned int  ret                 = FALSE;

  /* Check to see if we should do any LPRMs at all. */
  ret = sleepConfig_readBool((char *)"all_lprms", 
                             NULL,
                             (char *)"disable", 
                             &sleep_config_value );

  if(ret)
  {
    g_sleepAllowLowPowerModes = !sleep_config_value;
  }

  if(g_sleepAllowLowPowerModes == FALSE)
  {
    sleepLog_printf(SLEEP_LOG_LEVEL_WARNING, 0,
                    "WARNING (message: \"All sleep modes are disabled"
                    " in the global config\")" );
  }
#endif

  return;
}

/**
 * sleepOS_performLPM
 *
 * @brief Put the system in to the lowest possible power state allowed by
 *        imposed restrictions at run time.
 *
 * This function usually solves for the low power modes that can be entered
 * in special sleep context.
 *
 * @return Always 0 (for prototype compliance)
 */ 
static uint32 sleepOS_performLPM(void)
{
  boolean sleepModeEntered;
  void    *context = (void *)(SLEEP_OS_IDLE_MODE_DEFAULT);

  sleepModeEntered = sleepOS_performLPMEnter(context);

  /* Check if we need to execute exit path from here. It might already 
   * have been executed from other context.
   *
   * @Note
   * We are relying on a fact that when we come out of low power mode after
   * idle solver based Sleep, only one HW thread is running any SW. For modes,
   * that trigger PMI from SPM, exit path must be executed from IST context.
   * This is currently ensured by QuRT. If it changes, we will have to insert
   * additional check so that we don't execute exit path from here if we
   * had entered in to any mode with PMI.
   */
  if(g_sleepState.execute_sleep_exit_path)
  {
    sleepOS_performLPMExit();
  }

  if(TRUE == sleepModeEntered)
  {
    /* Log and update any statistic data in sleep task context, but only if
     * a low power mode was actually entered. */
    sleepStats_logWakeup(&g_sleepState.cycle_stats);
  }

  return 0;
}

/*
 * sleepOS_isProcessorInSTM
 */
uint32 sleepOS_isProcessorInSTM(void)
{
  return (TRUE == g_sleepState.processor_in_stm ? 1 : 0);
}

/**
 * sleepOS_mainTask
 * 
 * @brief Main sleep task that will call various low power modes
 *        when the system goes idle
 *
 * Note: located in uImage section
 */
void sleepOS_mainTask
(
  /* Parameter received from Main Control task - ignored */
  void *ignored    /*lint -esym(715,ignored) */
)
{
  sleep_idle_entry_ptr_type sleepFunctionPtr;

  /* Read NV items, etc. here to check for modes that are disabled. */
  sleepOS_readEFSConfig();

  /* Main sleep task processing - Enter as many low power modes as possible. */
  while(1)
  {
    sleepFunctionPtr = *g_idleEntryPtr;

    if(sleepFunctionPtr != NULL)
    {
      /* Call the idle entry point. */
      sleepFunctionPtr();
    }
  }

  CORE_VERIFY(0);
}

/**
 * sleepOS_taskInit
 * 
 * @brief RCInit function that will create the main sleep task and initilize 
 *        all of the sleep data structures 
 */
void sleepOS_taskInit(void)
{
  qurt_thread_attr_t  attr;
  qurt_thread_t       tid;

  /* Initialize the sleep subsystem */
  sleepOS_qurtInit();

  /* Initialize main sleep thread which needs to be located in the island
   * section */ 
  qurt_thread_attr_init(&attr);

  /* Stack memory has to be statically allocated due to island section
   * location requirement */
  qurt_thread_attr_set_stack_size(&attr, SLEEP_STACK_SIZE);
  qurt_thread_attr_set_stack_addr(&attr, (void *)g_sleepTaskStack);

  qurt_thread_attr_set_priority(&attr, SLEEP_TASK_PRI);
  qurt_thread_attr_set_tcb_partition(&attr, 0);
  qurt_thread_attr_set_name(&attr, "sleep");

  /* Fill in the stack with a marker to try and find overflows */
  memset(g_sleepTaskStack, 0xF8, sizeof(g_sleepTaskStack));

  /* Create the main sleep task in island section memory */
  qurt_thread_create(&tid, &attr, sleepOS_mainTask, NULL);

  return;
}

