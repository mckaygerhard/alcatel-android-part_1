/*==============================================================================
  FILE:         sleepActive.c
  
  OVERVIEW:     This file implements the Active Time solver thread and the 
                interfaces associated with it.
  
  DEPENDENCIES: None

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
=================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/src/active/sleepActive.c#4 $
$DateTime: 2015/07/16 09:52:19 $
===============================================================================*/
#include "stdint.h"
#include "npa.h"
#include "rcinit.h"
#include "sleepActive.h"
#include "sleep_log.h"
#include "sleep_solveri.h"
#include "sleep_target.h"
#include "qurt_signal.h"
#include "CoreVerify.h"
#include "sleep_osi.h"
#include "sleep_npa_scheduler.h"
#include "timer.h"

/*==============================================================================
                                 CONSTANTS
  =============================================================================*/
/* Active Time Solver depends on being able to query 
 * following nodes for evaluating low-power modes */
static const char* sleep_active_deps[] = 
{
  "/sleep/idle/plugin", 
  "/sleep/max_duration", 
  "/core/cpu/latency",
  "/clk/cpu"
};

/*==============================================================================
                              INTERNAL DEFINITIONS
  =============================================================================*/
/* Signal Object used for communicating with Active Time Solver */
static qurt_signal_t sleepActiveSigObj;

/* State of Solver Inputs that are handled by idle plugin */
static sleep_solver_input solverInput;

/* Last solver output selected by Active Time Solver */
static sleep_solver_output lastSolverOutput;

/* Query handle for the current state of idle/plugin node */
static npa_query_handle npaIdlePluginHandle;

/* Query handle for the current state of max_duation node */
static npa_query_handle npaMaxDurationHandle;

/* Query handle for the current state of latency node */
static npa_query_handle npaLatencyHandle;

/* Event handle for changes in "/clk/cpu" state */
static npa_event_handle npaFreqChangeEventHandle;

/* Threshold timer object. */
static timer_type sleepActiveThresholdTimer;

/*==============================================================================
                           STATIC FUNCTION DEFINITIONS
  =============================================================================*/
/**
 * @brief Update the Frequency LUT for statistics profiling  
 * 
 */
static void sleepActive_UpdateFreqLUT( void )
{
  solverInput.last_cpu_freq_khz = sleepOS_getFrequency();

  if( TRUE != sleepStats_getTableEntry( solverInput.last_cpu_freq_khz, 
                                        &solverInput.fLUT ) )
  {
    /* If the function returns !TRUE, we need to allocate a new fLUT and 
     * realloc all of the cache & statistic arrays */
    solverInput.fLUT =
        sleepStats_addFrequencyTable( solverInput.fLUT, 
                                      solverInput.last_cpu_freq_khz );
  }
}

/**
 * @brief Threshold timer callback function. It sets signals for durations
 *        so that ATS can re-solve low power modes with updated durations. 
 *
 * @param data: Callback context data (Unused - for prototype compliance)
 *
 * @note There seems to be a discrepancy about context type. Timer definition 
 *       expects context of time_osal_notify_data type whereas callback
 *       expects timer_cb_data_type. While both map to 'unsigned long',
 *       it can lead to error.
 */
static void sleepActive_threshTimerCB( timer_cb_data_type data )
{
  /* We don't really need to set SOFT_DURATION signal since it is just best
   * case duration (not absolute value) and its own signal will be set if 
   * that changes anyhow but just to keep it simple for now. */
  sleepActive_SetSignal( SLEEP_SIGNAL_SOFT_DURATION | 
                         SLEEP_SIGNAL_HARD_DURATION );
}

/**
 * @brief Sets the threshold timer which will expire at the deadline provided
 *        by solver. It is expected to be called after solver has finished.
 *
 * @param output: Output object used while calling solver.
 */
static void sleepActive_setThresholdTimer( sleep_solver_output *output )
{
  if( (output->threshold_deadline != 0) && 
      (output->threshold_deadline != output->ref_deadline) )
  {
    /* We have valid threshold deadline and there is a possibility of 
     * different mode becoming optimal as duration reduces. */
    timer_set_absolute( &sleepActiveThresholdTimer, 
                        output->threshold_deadline );

    sleepLog_printf( SLEEP_LOG_LEVEL_ATS, (1*2),
                     "Threshold timer set (Deadline: 0x%llx)",
                     output->threshold_deadline );
  }
}

/**
 * @brief Execute the Active Time solver and determine whether mode selection
 *        has changed
 *
 *        Performs the following operations:
 *        1. Execute solver based on last updated solver input
 *        2. If mode selection results in Cached Mode then
 *           unconfigure previous configured mode ( only if cached )
 *           and configure the newly selected cached mode
 *        3. If mode selection results in Uncached Mode then 
 *           unconfigure previous configured mode ( only if cached )
 *        4. Calls sleepOS_ConfigIdleMode to set the appropriate entry 
 *           depending on whether the selected mode is cached or uncached
 * 
 */
static void sleepActive_TriggerSolver( void )
{
  /* Last synthLPRM that was selected by idle plugin solver invocation */ 
  sleep_synth_lprm* lastSynthMode = lastSolverOutput.selected_synthLPRM;
  sleep_solver_output solver_output;

  sleepSolver_solveMode( &solverInput, &solver_output );

  sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_ATS, SLEEP_EXIT_SOLVER_NUM_ARGS,
                       SLEEP_EXIT_SOLVER_STR, SLEEP_EXIT_SOLVER );

  /* Active-Time solver should always be able to choose a cached mode */
  CORE_VERIFY_PTR( solver_output.selected_synthLPRM );

  /* Set a threshold timer in case this selected mode becomes stale. */
  sleepActive_setThresholdTimer( &solver_output );  

  /* Update the last selected solver output data */
  lastSolverOutput = solver_output;

  /* If Mode selection did not change, then return */
  if ( lastSynthMode == solver_output.selected_synthLPRM )
  {
    return;
  }

  /* Transition across Synth Modes if solver output has changed */
  if (lastSynthMode != NULL && 
      TRUE == synthLPRM_isAttributeSet(lastSynthMode, 
                                       (SYNTH_MODE_ATTR_LPR_CACHEABLE | SYNTH_MODE_ATTR_LPI),
                                       FALSE)) 
  {
    sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 1, 
                     "Unconfiguring previous cached mode (name: \"%s\")",
                     lastSynthMode->name );

    /* Unconfigure previously configured Cached Synth Mode */
    synthLPRM_cachedModeExit( lastSynthMode );

    /* Unconfigure previously configured Cached Synth Mode */
    if(TRUE == synthLPRM_isAttributeSet(lastSynthMode, SYNTH_MODE_ATTR_LPI, TRUE))
    {
      /* Ensure the LPI entry function is cleared */
      sleepOS_setLPIEntryFunction(NULL);
    }
  }
  
  /* Configure sleep task to run the correct function */
  if(TRUE == synthLPRM_isAttributeSet(solver_output.selected_synthLPRM, 
                                      SYNTH_MODE_ATTR_LPI, TRUE))
  {
    /* Set OS Idle Mode for LPI mode
     * The LPI attribute takes priority over the cacheable attribute */
    sleepOS_configIdleMode(SLEEP_OS_IDLE_MODE_LPI);
  }
  else if(TRUE == synthLPRM_isAttributeSet(solver_output.selected_synthLPRM, 
                                           SYNTH_MODE_ATTR_LPR_CACHEABLE, TRUE))
  {
    /* Configure OS Idle Mode for Cached LPR mode */
    sleepOS_configIdleMode(SLEEP_OS_IDLE_MODE_OVERRIDE);
  }
  else
  {
    /* Configure OS Idle Mode for Uncached Synth Mode */
    sleepOS_configIdleMode(SLEEP_OS_IDLE_MODE_DEFAULT);
  }

  /* If synth is cacheable, run its enter functions */
  if(TRUE == synthLPRM_isAttributeSet(solver_output.selected_synthLPRM, 
                                      SYNTH_MODE_ATTR_LPR_CACHEABLE,
                                      FALSE))
  {
    sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 0, "Configuring cached mode" );

    /* Enter the cacheable modes */
    synthLPRM_cachedModeEnter( solver_output.selected_synthLPRM,
                               solverInput.hard_deadlines.normal );
  }
  
  return;
}

/**
 * @brief Clear a signal or mask of signals on Active Time solver thread
 *
 * Signals set on the Active Time solver are used to determine 
 * execution conditions that have changed. Signals should be cleared
 * as soon they are processed.
 * 
 * @param sig: signal or signal mask to be cleared on Active Time solver
 */
static void sleepActive_ClearSignal( uint32 sig )
{
  qurt_signal_clear( &sleepActiveSigObj, sig );
}

/**
 * @brief This Callback is triggered when "core/cpu" frequency state changes.
 *
 * @param See npa_callback type
 */
static void sleepActive_ProcessFreqChangeEvent( void *context, 
                                                unsigned int event,
                                                void *event_data,
                                                unsigned int event_data_size )
{
  if ( event == NPA_EVENT_CHANGE )
  {
    sleepActive_SetSignal( SLEEP_SIGNAL_CPU_FREQUENCY );
  }
}

/**
 * @brief Updates solver input based on set signals
 *
 * @param signals: Data signals that are set on Active Time Solver
 */
static void sleepActive_ProcessDataSignals( uint32 signals )
{
  npa_query_type  qres;

  /* Clear Data Signals, if any */
  sleepActive_ClearSignal( signals & ~SLEEP_SIGNAL_CONTROL );

  if ( signals & SLEEP_SIGNAL_LATENCY && npaLatencyHandle )
  {
    /* Get the latency budget from the latency node.  The latency node will
     * return the minimum latency budget being requested by all clients.
     * This lets us know how quickly we need to respond to an interrupt when
     * it occurs. */
    CORE_VERIFY( NPA_QUERY_SUCCESS == 
                 npa_query( npaLatencyHandle, NPA_QUERY_CURRENT_STATE,
                            &qres ) );
    solverInput.latency_budget = qres.data.value;
  }

  if ( signals & SLEEP_SIGNAL_SOFT_DURATION && npaMaxDurationHandle )
  {
    /* Get the sleep duration from the max duration node.  This will return the
     * "soft" max duration.  Soft wakeups are those that are hinted at by other
     * subsystems.  Soft wakeups allow us to calculate which mode we will enter,
     * so we don't waste time entering a mode that we are going to have to wake
     * up from soon anyway. */
    CORE_VERIFY( NPA_QUERY_SUCCESS == 
                 npa_query( npaMaxDurationHandle, NPA_QUERY_CURRENT_STATE,
                            &qres ) );
    solverInput.soft_duration =  qres.data.value;
  }

  if ( signals & SLEEP_SIGNAL_CPU_FREQUENCY )
  {
    /* CPU Frequency changed.  Update the statistics frequency LUT */
    sleepActive_UpdateFreqLUT();
  }

  if ( signals & SLEEP_SIGNAL_HARD_DURATION )
  {
    /* First non deferrable timer expiry changed, threshold timer expired
     * or npa scheduler deadline changed - Sleep may have different hard 
     * duration. 
     *
     * Context switch could happen while calculating hard duration and we
     * may be past hard deadline - however in that case we should already
     * have another signal pending so it should be ok. */
    sleepTarget_getHardWakeupTime(&solverInput.hard_deadlines);
  }

  sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 0, "Processed data signals" );
}

/**
 * @brief Updates solver signal mask based on set signals
 *
 * @param signals: Control signals that are set on Active Time Solver
 *
 * @return boolean denoting whether further processing should continue
 */
static boolean sleepActive_ProcessControlSignals( uint32 signals )
{
  boolean stopProcessing = 0;

  /* Process updated Control signals, if any */
  if ( signals & SLEEP_SIGNAL_CONTROL )
  {
    sleepOS_IdleModeType idleMode;
    npa_query_type  qres;

    /* Clear Control Signal */
    sleepActive_ClearSignal( SLEEP_SIGNAL_CONTROL );

    if ( npaIdlePluginHandle )
    {
      CORE_VERIFY( NPA_QUERY_SUCCESS == 
                   npa_query( npaIdlePluginHandle, NPA_QUERY_CURRENT_STATE,
                              &qres ) );
    
      idleMode = qres.data.value;
    }
    else
    {
      /* npaIdlePluginHandle is not initialized, HOLD_OFF until
       * handle becomes available */
      idleMode = SLEEP_OS_IDLE_MODE_HOLD_OFF;
    }

    /* If HOLD_OFF is requested in idle/plugin then Active
     * Time solver should configure for this mode and suspend
     * further processing */
    if ( idleMode == SLEEP_OS_IDLE_MODE_HOLD_OFF )
    {
      /* Configure sleepOS appropriately */
      sleepOS_configIdleMode(idleMode);

      /* Force solver to pick (and setup) a new mode when switching out of
       * any special handling mode */
      lastSolverOutput.selected_synthLPRM = NULL;
      stopProcessing = 1;
    }
    else
    {
      /* Since idle/plugin is not forcing a constraint, continue 
       * processing the data signals */
      stopProcessing = 0;
    }

    sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 1, 
                     "Processed control signals (idle mode: %u)", 
                     (uint32)idleMode );
  }

  return stopProcessing;
}

/**
 * @brief Returns any pending signals if Active Time solver is enabled
 *
 * @param sig_mask: Mask used to control whether Active Time solver is
 *                  is listening to only:
 *                    i. the control signal (SLEEP_SIGNAL_CONTROL)
 *                    ii. all signals (SLEEP_SIGNAL_MASK_ALL)
 */
static uint32 sleepActive_WaitforSignal( uint32 sig_mask )
{
  uint32 sigs;
  
  sigs = qurt_signal_wait( &sleepActiveSigObj, sig_mask, QURT_SIGNAL_ATTR_WAIT_ANY );
  
  return sigs;
}

/**
 * @brief Called when dependencies of Active Time Solver become available 
 */
static void sleepActive_ResourceAvailCB( void* context, 
                                         unsigned int event, 
                                         void* resources,
                                         unsigned int resourcesSize )
{
  /* Initialize Query Handles */
  CORE_VERIFY_PTR( npaIdlePluginHandle =
                   npa_create_query_handle( "/sleep/idle/plugin" ) );

  CORE_VERIFY_PTR( npaMaxDurationHandle =
                   npa_create_query_handle( "/sleep/max_duration" ) );

  CORE_VERIFY_PTR( npaLatencyHandle =
                   npa_create_query_handle( "/core/cpu/latency" ) );

  CORE_VERIFY_PTR( npaFreqChangeEventHandle =
                   npa_create_event_cb( "/clk/cpu",
                                        "ATS Input",
                                        NPA_TRIGGER_CHANGE_EVENT,
                                        sleepActive_ProcessFreqChangeEvent,
                                        NULL ) );

  /* Trigger the inputs corresponding to the available nodes */
  sleepActive_SetSignal( SLEEP_SIGNAL_CONTROL       |
                         SLEEP_SIGNAL_SOFT_DURATION |
                         SLEEP_SIGNAL_LATENCY       |
                         SLEEP_SIGNAL_CPU_FREQUENCY );
}

/**
 * @brief Initializes the Active Time Solver 
 *        Required to be called as part of rcinit. 
 */
static void sleepActive_Init( void )
{
  /* Setup Solver Input Defaults */
  solverInput.fLUT            = 0;
  solverInput.latency_budget  = 1;
  solverInput.synth_attr_mask = SYNTH_MODE_ATTR_LPR_MASK_ANY;
  solverInput.soft_duration   = UINT64_MAX;

  memset(&solverInput.hard_deadlines, -1, sizeof(solverInput.hard_deadlines));

  /* Initialize signal type */
  qurt_signal_init( &sleepActiveSigObj );

  /* Defining threshold timer. */
  CORE_VERIFY( TE_SUCCESS == 
               timer_def_osal( &sleepActiveThresholdTimer,    /* Timer obj */
                               &timer_null_group,             /* Timer group */
                               TIMER_FUNC1_CB_TYPE,           /* Notify type */
                               sleepActive_threshTimerCB,     /* Callback fcn */
                               (time_osal_notify_data)0 ) );  /* Context */

  /* Register to receive signal when Hard Duration is updated */
  CORE_VERIFY ( TE_SUCCESS == timer_register_for_non_defer_expiry_change(
                  TIMER_NATIVE_OS_SIGNAL_TYPE,
                  &sleepActiveSigObj,
                  SLEEP_SIGNAL_HARD_DURATION ) );

  /* Manually trigger Active time solver to qury hard duration */
  sleepActive_SetSignal( SLEEP_SIGNAL_HARD_DURATION ); 
                  
  /* Register to Callback when resources becomes available */
  npa_resources_available_cb( NPA_ARRAY(sleep_active_deps),
                              sleepActive_ResourceAvailCB,
                              NULL );
}

/*==============================================================================
                            GLOBAL FUNCTION DEFINITIONS
  =============================================================================*/
/* 
 * sleepActive_GetSolverOutput
 */
sleep_solver_output *sleepActive_GetSolverOutput(void)
{
  return ( &lastSolverOutput );
}

/* 
 * sleepActive_GetSolverInput 
 */
sleep_solver_input *sleepActive_GetSolverInput(void)
{
  return ( &solverInput );
}

/*
 * sleepActive_GetSolverHardDeadlines
 */
const sleep_solver_deadlines *sleepActive_GetSolverHardDeadlines(void)
{
  return (&solverInput.hard_deadlines);
}

/* 
 * sleepActive_SetSignal 
 */
void sleepActive_SetSignal( sleepActive_SignalType signals )
{
  qurt_signal_set( &sleepActiveSigObj, (uint32)signals );
}

/**
 * @brief Processing loop for Active Time Solver.
 *
 * Executes from the "sleepsolver" thread context
 */
void sleepActive_Thread( unsigned long int unused_param )
{
  uint32 sig_mask = SLEEP_SIGNAL_CONTROL;
  uint32 signals = 0;
  boolean disableActiveTimeSolver;

  /* Initialize the Active-time Solver */
  sleepActive_Init( );

  /* Notify RCInit that all basic initialization is complete. */
  rcinit_handshake_startup();
  
  while(1)
  {
    signals = sleepActive_WaitforSignal( sig_mask );

    sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_ATS, SLEEP_ATS_SIGNALS_NUM_ARGS, 
                         SLEEP_ATS_SIGNALS_STR, SLEEP_ATS_SIGNALS,
                         signals );
    
    /*Process and Clear Control signals, if any */
    disableActiveTimeSolver = sleepActive_ProcessControlSignals( signals );

    /* Skip further processing of Data Signals if Control Signals
     * cause Active Time solver to be disabled.
     * Additionally, modify mask to only listed for Control Signal
     * changes until Active Timer solver is enabled again. */
    if ( disableActiveTimeSolver == TRUE )
    {
      sig_mask = SLEEP_SIGNAL_CONTROL;
      continue;
    }
    else
    {
      sig_mask = SLEEP_SIGNAL_MASK_ALL;
    }

    /*Process updated Input signals, if any */
    sleepActive_ProcessDataSignals( signals );
    
    /* Trigger solver with updated input. Solver will 
     * configure the appropriate sleepOS_IdleMode if required */
    sleepActive_TriggerSolver( );

  } /* while */
}

