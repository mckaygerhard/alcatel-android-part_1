#ifndef SLEEPACTIVEI_H
#define SLEEPACTIVEI_H
/*==============================================================================
  FILE:         sleepActivei.h

  OVERVIEW:     This file contains internal APIs exposed by the Sleep Active-time 
                Solver Thread.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/src/active/sleepActivei.h#1 $
$DateTime: 2015/07/16 09:52:19 $
==============================================================================*/
#include "sleep_solveri.h"

/*==============================================================================
                              GLOBAL FUNCTION DECLARATIONS
  =============================================================================*/
/**
 * @brief Returns the input parameters used by Sleep Active for 
 *        input to Active time solver
 *  
 * @return A pointer to the solver input struct
 */
sleep_solver_input *sleepActive_GetSolverInput(void);

/**
 * @brief Returns the low-power mode selected by Active time 
 *        solver
 *  
 * @return A pointer to the last chosen solver output struct
 */
sleep_solver_output *sleepActive_GetSolverOutput(void);

#endif /* SLEEPACTIVEI_H */

