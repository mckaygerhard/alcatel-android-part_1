#!/usr/bin/env python

class qurt_config:
    def genheader_subcommand(self, arglist):
        from lib.genheader import genheader_cmd
        return genheader_cmd(arglist)
    def genkernel_subcommand(self, arglist):
        from lib.genkernel import genkernel_cmd
        return genkernel_cmd(arglist)
    def update_subcommand(self, arglist):
        from lib.merge import merge_cmd
        return merge_cmd(arglist)
    def usage(self):
        cmds = sorted([z.rsplit('_',1)[0] for z in dir(self) if z.endswith('_subcommand')])
        str = 'First argument must be one of:\n  ' + ', '.join(cmds)
        raise Exception(str)
    def run_command(self, argv):
        from traceback import format_exc as tbstr
        progname = argv[0]
        try:
            print ' '.join(argv)
            raw_args = argv[1:]
            args = [s for s in raw_args if not s == '--traceback']
            if args == raw_args:
                tbstr = None
            try:
                subfunc = getattr(self, '%s_subcommand' % args[0])
            except StandardError:
                self.usage()
            return subfunc(args[1:])
        except (SystemExit, KeyboardInterrupt):
            raise
        except Exception, err:
            if tbstr:
                print tbstr()
            print '%s: Error:\n*** %s' % (progname, err)
        except:
            raise
        return 1
    def main(self):
        import sys
        sys.exit(self.run_command(sys.argv))

def attach_config_vars(cfg):
    #
    #  This function is called by the kernel build procedure
    #   to get access to the configuration variable database.
    #
    import os
    import imp
    lib = imp.load_module('lib',*imp.find_module('lib',[os.path.dirname(__file__)]))
    from lib.genkernel import QurtVars
    QurtVars(cfg)

if __name__ == '__main__':
    qurt_config().main()    # Never returns

# Signatures of the files that this depends on
# cc075e34378c1b53fefddf3fa539c6e1 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/cust_config_template.c
# 76f1267a2a78695c278d9906048d6a4e /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/default_build_config.def
# 5498974ebcca428bef2f9ae584c59d2c /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/static_build_config.def
# fb029620d16a7ed5f97289883f996027 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/qurt_tlb_unlock.xml
# ae8917c65d309ca530fc79eb36cd6ef3 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/qurt_default.lcs
# 206b9e12f5643aab9b5c41da144c95f3 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/__init__.py
# 3bbd1e0d3509abf5437482d213a9aaec /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/build_qurt_config.py
# 9dbe95fb17059e23e02557c064bf4bee /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/build_xml.py
# 2a1fa635b029fcb2e9484c39aa64c25e /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/elf_info_patch.py
# 92948080d10d75e5f94f4777d21bb646 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/ezxml.py
# b64c24a985bab7cf763740ad0221da7d /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/genheader.py
# 88c30bdc2fa2f78de9262163bbeaa4b9 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/genkernel.py
# c083132b08dbc9fe4f0e2ad37e4cab81 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/interrupt_xml.py
# c71b950b2a9bf3894c57da579587c4b4 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/kernel_xml.py
# 95acc27b7e3bddf9e00b564bd1346090 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/machine_xml.py
# 0dcdb7dcc6e15226b8ea7e4febe611ae /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/memsection_xml.py
# 4852cc7e53e5baead4d6e5e7a9b02b4d /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/merge.py
# 4ffa9ed630bd335beb3517b8494d7c1b /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/parse_build_params.py
# c1cfa0039fe2f9b4dd1eda0e94472d3b /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/parse_spec.py
# 31733f66f57d2783f3b7e99338fdb9ae /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/physpool_xml.py
# 94ea469cffbb6a63290a6412f2ca6361 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/program_xml.py
# b4ebb61dd2b7c0dd7b6fb0edc986c61f /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/qurt.py
# 86d948d527330e14a4897fffd89f6994 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/qurt_config_vars.py
# 6a84a6f95bf5ad9b80c6983eeab7ee39 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/build_params.txt
# bd78f2c49461b2505950c3ca99ebefb4 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/Input/cust_config.c
# 0150e9474cfb0cc1462868b8457284a1 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/qurt-image-build.py
# 78be7b3e3f906aa085065fb67efc6045 /local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/kernel/qurt/build/avs_adsp_user/qdsp6/AAAAAAAA/install/ADSPv60MP/scripts/lib/qurt_consts.py
