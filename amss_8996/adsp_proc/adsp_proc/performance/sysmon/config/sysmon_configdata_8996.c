/*=============================================================================
 * FILE:                 sysmon_configdata_8994.c
 *
 * DESCRIPTION:
 *    Sysmon device configuration data for 8994 ADSP
 *
 * Copyright (c) 2014 QUALCOMM Technologies, Incorporated.
 * All Rights Reserved.
 * QUALCOMM Proprietary.
  ===========================================================================*/

/*=============================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 * when         who          what, where, why
 * ----------   ------     ------------------------------------------------
 ============================================================================*/

#include <stdlib.h>
#include "DALDeviceId.h"
#include "DDIChipInfo.h"
#include "sysmon_devcfg_int.h"

const sd_devcfg_bus_clk_table_t bus_clk_table[]=
{
        {
        /* freqkHz */               37500,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        },
        {
        /* freqkHz */               100000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        },
        {
        /* freqkHz */               200000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW,
        },
        {
        /* freqkHz */               300000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        },
        {
        /* freqkHz */               400000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        },
        {
        /* freqkHz */               533000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_HIGH,
        },
};

const sd_devcfg_bus_clk_desc_t bus_clk_descriptor =
{
        sizeof(bus_clk_table)/sizeof(sd_devcfg_bus_clk_table_t),
        (sd_devcfg_bus_clk_table_t *)bus_clk_table
};

const sd_devcfg_core_clk_table_t core_clk_table[]=
{
        {
        /* freqkHz */               19200,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        },
        {
        /* freqkHz */               86400,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x01,0x00,0x02,0x00)}
        },
        {
        /* freqkHz */               124800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x02,0x00,0xff,0xff)}
        },
        {
        /* freqkHz */               124800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x00,0x00,0xff,0xff), 
                                     DALCHIPINFO_FAMILY_MSM8996SG}
        },
        {
        /* freqkHz */               172800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x01,0x00,0x02,0x00)}
        },
        {
        /* freqkHz */               249600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW,
        /* hwVersion */             {SD_HW_VER(0x01,0x00,0x02,0x00)}
        },
        {
        /* freqkHz */               268800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x02,0x00,0x03,0x00)}
        },
        {
        /* freqkHz */               297600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x03,0x00,0xff,0xff)}
        },
        {
        /* freqkHz */               297600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS,
        /* hwVersion */             {SD_HW_VER(0x00,0x00,0xff,0xff), 
                                     DALCHIPINFO_FAMILY_MSM8996SG}
        },
        {
        /* freqkHz */               422400,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        /* hwVersion */             {SD_HW_VER(0x01,0x00,0x02,0x00)}
        },
        {
        /* freqkHz */               422400,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW,
        /* hwVersion */             {SD_HW_VER(0x02,0x00,0x03,0x00)}
        },
        {
        /* freqkHz */               480000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_HIGH,
        /* hwVersion */             {SD_HW_VER(0x01,0x00,0x02,0x00)}
        },
        {
        /* freqkHz */               480000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW,
        /* hwVersion */             {SD_HW_VER(0x03,0x00,0xff,0xff)}
        },
        {
        /* freqkHz */               480000,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_LOW,
        /* hwVersion */             {SD_HW_VER(0x00,0x00,0xff,0xff), 
                                     DALCHIPINFO_FAMILY_MSM8996SG}
        },
        {
        /* freqkHz */               633600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        /* hwVersion */             {SD_HW_VER(0x02,0x00,0x03,0x00)}
        },
        {
        /* freqkHz */               652800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        /* hwVersion */             {SD_HW_VER(0x03,0x00,0xff,0xff)}
        },
        {
        /* freqkHz */               652800,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_NOMINAL,
        /* hwVersion */             {SD_HW_VER(0x00,0x00,0xff,0xff), 
                                     DALCHIPINFO_FAMILY_MSM8996SG}
        },
        {
        /* freqkHz */               729600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_HIGH,
        /* hwVersion */             {SD_HW_VER(0x02,0x00,0x03,0x00)}
        },
        {
        /* freqkHz */               825600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_HIGH,
        /* hwVersion */             {SD_HW_VER(0x03,0x00,0xff,0xff)}
        },
        {
        /* freqkHz */               825600,
        /* vRegLevel */             SYSMON_CLOCK_VREG_LEVEL_HIGH,
        /* hwVersion */             {SD_HW_VER(0x00,0x00,0xff,0xff), 
                                     DALCHIPINFO_FAMILY_MSM8996SG}
        },
};

const sd_devcfg_core_clk_desc_t core_clk_descriptor =
{
        sizeof(core_clk_table)/sizeof(sd_devcfg_core_clk_table_t),
        (sd_devcfg_core_clk_table_t *)core_clk_table
};
