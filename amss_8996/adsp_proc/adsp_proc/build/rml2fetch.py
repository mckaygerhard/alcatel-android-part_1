#============================================================================
#
# SKELETON Builders build rules
#
# GENERAL DESCRIPTION
#    Contains builder(s) to <create, process, etc, insert your description here>.
#    replace all instances of SKELETON or skeleton with your builder name.
#
# Copyright (c) 2009-2015 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#----------------------------------------------------------------------------
#
#  $Header: //components/rel/dspbuild.adsp/2.8.2/rml2fetch.py#1 $
#  $DateTime: 2016/01/21 21:54:46 $
#  $Author: pwbldsvc $
#  $Change: 9768170 $
# 
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# YYYY-MM-DD   who     what, where, why
# ----------   ---     ---------------------------------------------------------
#
# 
#============================================================================
import os
import re
import pdb

#----------------------------------------------------------------------------
# Global values
# Global definitions used within this script for example tool cmd definitions 
# which might be use in different places.
#----------------------------------------------------------------------------
TOOL_CMD = "${BUILD_ROOT}/tools/xyz/foo${EXE_EXT}"

#----------------------------------------------------------------------------
# Hooks for SCons
# These are the function entry points SCons calls when loading this script.
#----------------------------------------------------------------------------
def exists(env):
   '''
   Used by SCons to make sure scripts are not allowed to be 
   loaded multiple times per environment.
   '''
   return env.Detect('rml2fetch_builder')

def generate(env):
   '''
   This is the entry point called by SCons when loading this script.
   This should call other generate functions as a script might define 
   multiple builders or methods.
   '''
   rml2fetch_generate(env)

#============================================================================
# build rules
#============================================================================
def rml2fetch_generate(env):
   rml2fetch_act = env.GetBuilderAction(rml2fetch_builder)
   
   rml2fetch_bld = env.Builder(
      action = rml2fetch_act,
      #emitter=rml2fetch_emitter,
      #source_scanner=rml2fetch_source_scanner,
      #target_scanner=rml2fetch_target_scanner,
      #suffix = '.my_extention'
   )

   env.Append(BUILDERS = {'RmL2Fetch' : rml2fetch_bld})
   

#----------------------------------------------------------------------------
# builder action
#----------------------------------------------------------------------------
def rml2fetch_builder(target, source, env):
    target_full = str(target[0])
    source_full = str(source[0])
#    env.PrintInfo("Runing rml2fetch command to generate bootimage_relocflag_withdummyseg.pbn")
#    cmd = ''.join(['${BUILD_ROOT}/build/hexagon-rml2fetch.exe', ' ${BUILD_MS_ROOT}/bootimage_relocflag_withdummyseg.pbn', ' -o source_full'])
#    data, err, rv = env.ExecCmds(cmd)
    
#    pdb.set_trace()
    env.PrintInfo("Runing rml2fetch command to generate bootimage_relocflag_withdummyseg.2.pbn")
#    pdb.set_trace() 
    

    if env['PLATFORM'] == 'linux':
        env.PrintInfo("Runing rml2fetch command on Linux to generate bootimage_relocflag_withdummyseg.2.pbn")
#        cmd2 = ''.join([ '${BUILD_ROOT}/build/hexagon-rml2fetch ', source_full, ' -o ', target_full])
        cmd2 = ''.join([ 'cp ', source_full, ' ', target_full])
        data, err, rv = env.ExecCmds(cmd2)
    elif env['PLATFORM'] == 'windows':
        env.PrintInfo("Runing rml2fetch command on Windows to generate bootimage_relocflag_withdummyseg.2.pbn")
        cmd2 = ''.join([ '${BUILD_ROOT}/build/hexagon-rml2fetch.exe ', source_full, ' -o ', target_full])
        data, err, rv = env.ExecCmds(cmd2)
    else :
        env.PrintInfo("Runing rml2fetch command without any Linux or Windows to generate bootimage_relocflag_withdummyseg.2.pbn")
   
#----------------------------------------------------------------------------
# Add Emitter to add clean actions and other misc
#----------------------------------------------------------------------------
def rml2fetch_emitter(target, source, env):

   env.Depends(target, env.RealPath(TOOL_CMD))
   
   return (target, source)

