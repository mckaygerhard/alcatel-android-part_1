/*
#============================================================================
#  Name:                                                                     
#    servreg_local_db_augtogen.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Tue Oct  4 00:59:28 2016 
#============================================================================
*/

/*Service Registry local database of "soc/domain/subdomain/provider/service"*/
/*Service is said to be LOCAL if the client and the service are in the same soc/domain/subdomain*/

#include "string.h"

struct servreg_entry_s
{
char * service_name;
unsigned int service_data_valid;
unsigned int service_data;
};
typedef struct servreg_entry_s servreg_entry_t, * servreg_entry_p;

servreg_entry_t servreg_local_services[]={
   {"tms/servreg", 0 , 0},
};

unsigned int servreg_service_count = sizeof(servreg_local_services)/sizeof(servreg_local_services[0]);
char *servreg_local_domain = "msm/adsp/audio_pd";
char *servreg_soc_name = "msm";
char *servreg_domain_name = "adsp";
char *servreg_subdomain_name = "audio_pd";
unsigned int servreg_qmi_instance_id = 4 ;
