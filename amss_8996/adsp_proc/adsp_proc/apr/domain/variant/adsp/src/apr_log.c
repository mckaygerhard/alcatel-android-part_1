/*
  Copyright (C) 2009-2012, 2013 QUALCOMM Technologies, Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/apr.adsp/2.3/domain/variant/adsp/src/apr_log.c#5 $
  $Author: pwbldsvc $
*/

#include <stdarg.h> /* va_list */
#include <stdio.h> /* vsnprintf() */


#include "log.h"
#include "err.h"
#include "msg.h"

#include "mmstd.h"

#include "apr_comdef.h"
#include "apr_errcodes.h"
#include "aprv2_packet.h"
#include "aprv2p1_packet.h"
#include "aprv2_msg_if.h"
#include "apr_lock.h"
#include "apr_misc.h"

#include "apr_log.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

#define APR_ERROR_LOGGING
  /*
  * Send error messages into a separate log which can be analyzed 
  * from crash dump
  */

/* #define USE_SPIN_ON_PANIC */
  /* Hold the call from progressing on panic. A debugger can be attached and
   * the full thread context can be analyzed and manually resumed.
   */

/* #define USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES */
  /* This functionality will be deprecated as soon as QXDM plugin is made
   * available to parse LOG_APR_ADSP_C log packets.
   */

#ifdef USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES
/*  #define USE_DISABLE_PACKET_EXCHANGE_MESSAGES */
#endif /* USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES */

#define APR_LOG_HISTORY_BYTE_SIZE_V ( 8 * 1024 )

#ifndef LOG_APR_ADSP_C
  #define LOG_APR_ADSP_C ( ( 0x4D2 ) + LOG_1X_BASE_C )
#endif /* LOG_APR_ADSP_C */

#ifndef MSG_SSID_APR_ADSP
  #define MSG_SSID_APR_ADSP ( MSG_SSID_AVS )
  #define MSG_BUILD_MASK_MSG_SSID_APR_ADSP ( MSG_BUILD_MASK_MSG_SSID_AVS )
#endif /* MSG_SSID_APR_ADSP */

#ifdef APR_ERROR_LOGGING
  #define APR_LOG_MAX_FILENAME_SIZE_V ( 256 )
  #define APR_LOG_MAX_SHORT_FILENAME_SIZE_V ( 31 )
  #define APR_LOG_ERROR_HISTORY_ENTRIES_V ( 16 )
  #define APR_LOG_ERROR_HISTORY_ENTRY_SIZE_V ( 128 )
  #define APR_LOG_MAX_APR_USERS ( 24 )
  #define APR_LOG_NO_MEMORY_ERROR_LOG_SIZE ( APR_LOG_MAX_APR_USERS * 2 )
#endif /* APR_ERROR_LOGGING */

/*****************************************************************************
 * Definitions                                                               *
 ****************************************************************************/

typedef struct apr_log_history_t apr_log_history_t;
struct apr_log_history_t
{
  uint8_t start_marker[ 8 ];
  uint32_t size;
  uint32_t offset;
  uint8_t log[ APR_LOG_HISTORY_BYTE_SIZE_V ];
  uint8_t end_marker[ 8 ];
};

#ifdef APR_ERROR_LOGGING
typedef struct apr_log_error_history_t apr_log_error_history_t;
struct apr_log_error_history_t
{
  uint8_t start_marker[ 8 ];
  uint32_t idx;
  uint32_t total_units;
  uint32_t unit_size;
  uint32_t total_size;
  uint8_t log[ APR_LOG_ERROR_HISTORY_ENTRIES_V ][ APR_LOG_ERROR_HISTORY_ENTRY_SIZE_V ];
  uint32_t out_of_memory_log[ APR_LOG_NO_MEMORY_ERROR_LOG_SIZE ]; 
  bool_t is_first_out_of_memory_failure;
  uint8_t end_marker[ 8 ];
};
#endif /* APR_ERROR_LOGGING */

/*****************************************************************************
 * Variables                                                                 *
 ****************************************************************************/

#ifdef USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES
static bool_t apr_core_f3_enable_flag = TRUE;
#endif /* USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES */

static apr_lock_t apr_log_printf_mutex;
static char apr_log_printf_buffer[ 256 ];

static apr_lock_t apr_log_history_mutex;
static apr_log_history_t apr_log_history;

#ifdef APR_ERROR_LOGGING
static apr_log_error_history_t apr_log_error_history;
static char_t apr_log_scratch_buffer[ MMSTD_MAX( APR_LOG_ERROR_MAX_MSG_SIZE_V, APR_LOG_MAX_SHORT_FILENAME_SIZE_V ) ];

static apr_lock_t apr_log_error_mutex;
#endif /* APR_ERROR_LOGGING */

/*****************************************************************************
 * Routines                                                                  *
 ****************************************************************************/

APR_INTERNAL int32_t apr_log_init ( void )
{
  int32_t rc;
  uint32_t checkpoint = 0;

  for ( ;; )
  {
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &apr_log_printf_mutex );
    if ( rc ) break;
    checkpoint = 1;

    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &apr_log_history_mutex );
    if ( rc ) break;
    checkpoint = 2;

    ( void ) mmstd_memcpy( apr_log_history.start_marker, sizeof( apr_log_history.start_marker ), "APRADSP1", 8 );
    apr_log_history.size = APR_LOG_HISTORY_BYTE_SIZE_V;
    ( void ) mmstd_memset( apr_log_history.log, 0xFF, sizeof( apr_log_history.log ) );
    ( void ) mmstd_memcpy( apr_log_history.end_marker, sizeof( apr_log_history.end_marker ), "APRADSP2", 8 );
    checkpoint = 3;

#ifdef APR_ERROR_LOGGING
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &apr_log_error_mutex );
    if ( rc ) break;
    checkpoint = 4;

    ( void ) mmstd_memcpy( apr_log_error_history.start_marker, sizeof( apr_log_error_history.start_marker ), "APRADSP3", 8 );
    apr_log_error_history.idx = 0;
    apr_log_error_history.total_units = APR_LOG_ERROR_HISTORY_ENTRIES_V;
    apr_log_error_history.unit_size = APR_LOG_ERROR_HISTORY_ENTRY_SIZE_V;
    apr_log_error_history.total_size = sizeof( apr_log_error_history );
    ( void ) mmstd_memset( apr_log_error_history.log, 0xFF, sizeof( apr_log_error_history.log ) );
    ( void ) mmstd_memset( apr_log_error_history.out_of_memory_log, 0x00, sizeof(apr_log_error_history.out_of_memory_log ) );
    apr_log_error_history.is_first_out_of_memory_failure = FALSE;
    ( void ) mmstd_memcpy( apr_log_error_history.end_marker, sizeof( apr_log_error_history.end_marker ), "APRADSP4", 8 );
#endif /* APR_ERROR_LOGGING */

    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 4:
    /*-fallthru */
  case 3:
    /*-fallthru */
  case 2:
    ( void ) apr_lock_destroy( apr_log_history_mutex );
    /*-fallthru */
  case 1:
    ( void ) apr_lock_destroy( apr_log_printf_mutex );
    /*-fallthru */
  default:
    break;
  }

  return APR_EFAILED;
}

APR_INTERNAL int32_t apr_log_deinit ( void )
{
  ( void ) apr_lock_destroy( apr_log_history_mutex );
  ( void ) apr_lock_destroy( apr_log_printf_mutex );

  return APR_EOK;
}

APR_INTERNAL void apr_log_printf ( const char * fmt, ... )
{
  va_list args;

  va_start( args, fmt );
  ( void ) apr_lock_enter( apr_log_printf_mutex );
  vsnprintf( apr_log_printf_buffer, sizeof( apr_log_printf_buffer ), fmt, args );
  MSG_SPRINTF_1( MSG_SSID_APR_ADSP, MSG_LVL_LOW, "%s", apr_log_printf_buffer );
  ( void ) apr_lock_leave( apr_log_printf_mutex );
  va_end( args );
}

APR_INTERNAL void apr_log_panic_printf ( const char * fmt, ... )
{
  va_list args;

  va_start( args, fmt );
  ( void ) apr_lock_enter( apr_log_printf_mutex );
  vsnprintf( apr_log_printf_buffer, sizeof( apr_log_printf_buffer ), fmt, args );
  MSG_SPRINTF_1( MSG_SSID_APR_ADSP, MSG_LVL_FATAL, "%s", apr_log_printf_buffer );
  ( void ) apr_lock_leave( apr_log_printf_mutex );
  va_end( args );

#ifdef USE_SPIN_ON_PANIC
  MSG_FATAL( "PANICKING", 0, 0, 0 );

  for ( ;; )
  {
    apr_misc_sleep( 5000000 );
  }
#else
  ERR_FATAL( "PANICKING", 0, 0, 0 );
#endif /* USE_SPIN_ON_PANIC */
}

#ifdef USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES

/* TODO: In the commercial code we need to make the analyzer just do DIAG
         logging and have an offline PC parser tool to interpret the payload.
         These SUPER_HACK printfs are only good for prototype debugging and
         must never be used in production code!!!!!
*/
static void apr_log_pktv2_analyze_packet (
  uint32_t handle,
  aprv2_packet_t* packet
)
{
  apr_log_printf(
    "send(%08X,%08X)<%04X-%04X><%04X-%04X>{%08X}[%08X],%d",
    handle,
    packet,
    packet->src_addr, packet->src_port,
    packet->dst_addr, packet->dst_port,
    packet->token,
    packet->opcode,
    APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header )
  );
}

static void apr_log_pktv2p1_generic_analyze (
  uint32_t handle,
  aprv2p1_packet_t* packet,
  char_t* text
)
{
  apr_log_printf(
    "send(%08X,%08X)|%08X|<%04X-%04X><%04X-%04X>{%08X}[%08X],%d,%s",
    handle,
    packet,
    packet->trace,
    packet->src_addr, packet->src_port,
    packet->dst_addr, packet->dst_port,
    packet->token,
    packet->opcode,
    APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header ),
    text
  );
}

static void apr_log_pktv2p1_analyze_packet (
  uint32_t handle,
  aprv2p1_packet_t* packet
)
{
  switch ( packet->opcode )
  {
  case APRV2_IBASIC_RSP_RESULT:
    apr_log_printf(
      "send(%08X,%08X)|%08X|<%04X-%04X><%04X-%04X>{%08X}[%08X],%d,%08X,result=%08X",
      handle,
      packet,
      packet->trace,
      packet->src_addr, packet->src_port,
      packet->dst_addr, packet->dst_port,
      packet->token,
      packet->opcode,
      APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header ),
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->opcode,
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status
    );
    break;

  case APRV2_IBASIC_EVT_ACCEPTED:
    apr_log_printf(
      "send(%08X,%08X)|%08X|<%04X-%04X><%04X-%04X>{%08X}[%08X],%d,%08X,accepted",
      handle,
      packet,
      packet->trace,
      packet->src_addr, packet->src_port,
      packet->dst_addr, packet->dst_port,
      packet->token,
      packet->opcode,
      APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header ),
      APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_evt_accepted_t, packet )->opcode
    );
    break;

  case 0x00010003:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICVD_CMD_CREATE_SESSION" );
    break;

  case 0x00010009:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_ENABLE" );
    break;

  case 0x0001000A:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_STANDBY" );
    break;

  case 0x0001000B:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_DISABLE" );
    break;

  case 0x00010011:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CDMA_CMD_SET_ENC_RATE_LIMIT" );
    break;

  case 0x00010013:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CDMA_CMD_SET_ENC_RATE_MODULATION" );
    break;

  case 0x00010014:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_VOCAMR_CMD_SET_ENC_RATE" );
    break;

  case 0x00010015:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_VOCAMRWB_CMD_SET_ENC_RATE" );
    break;

#ifdef USE_DISABLE_PACKET_EXCHANGE_MESSAGES
  case 0x00010017:
  case 0x00010018:
  case 0x00010019:
    break;
#else
  case 0x00010017:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_SEND_ENC_BUFFER" );
    break;

  case 0x00010018:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER" );
    break;

  case 0x00010019:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_SEND_DEC_BUFFER" );
    break;
#endif /* USE_DISABLE_PACKET_EXCHANGE_MESSAGES */

  case 0x0001001B:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_APR_OP_CMD_CREATE" );
    break;

  case 0x0001001C:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_APR_OP_CMD_DESTROY" );
    break;

  case 0x0001001D:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_SET_NETWORK" );
    break;

  case 0x0001001E:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_BRINGUP" );
    break;

  case 0x0001001F:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_TEARDOWN" );
    break;

  case 0x00010027:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_STREAM_SETUP" );
    break;

  case 0x00010028:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_STREAM_TEARDOWN" );
    break;

  case 0x00010036:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICVC_CMD_CREATE_SESSION" );
    break;

  case 0x00010038:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICONF_CMD_DISABLE" );
    break;

  case 0x00010039:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICONF_CMD_ENABLE" );
    break;

  case 0x0001003A:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICONF_CMD_SET_NETWORK" );
    break;

  case 0x0001003B:
    apr_log_pktv2p1_generic_analyze( handle, packet, "APRV2_IBASIC_CMD_CREATE_SESSION" );
    break;

  case 0x0001003C:
    apr_log_pktv2p1_generic_analyze( handle, packet, "APRV2_IBASIC_CMD_DESTROY_SESSION" );
    break;

  case 0x0001003F:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_SET_RX_MEDIA_TYPE" );
    break;

  case 0x00010040:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_SET_TX_MEDIA_TYPE" );
    break;

  case 0x000100A2:
    apr_log_pktv2p1_generic_analyze( handle, packet, "CAD_VOICE_OP_CMD_CALIBRATE_PP" );
    break;

  case 0x000100A3:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_GET_PARAM" );
    break;

  case 0x000100A4:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_SET_PARAM" );
    break;

  case 0x000100A5:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_EVT_SET_PARAM_START" );
    break;

  case 0x0001133D:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICOMMON_CMD_SET_PARAM_V2" );
    break;

  case 0x0001133E:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICOMMON_CMD_GET_PARAM_V2" );
    break;

  case 0x00012A0F:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG" );
    break;

  case 0x000112BF:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_CREATE_FULL_CONTROL_SESSION_V2" );
    break;

  case 0x000112C6:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_SET_DEVICE_V2" );
    break;

  case 0x00011370:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_SET_SAMPLING_RATE" );
    break;

  case 0x00011371:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_REGISTER_DEVICE_CONFIG" );
    break;

  case 0x00011372:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_DEREGISTER_DEVICE_CONFIG" );
    break;

  case 0x00011373:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V2" );
    break;

  case 0x00011276:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_DEREGISTER_CALIBRATION_DATA" );
    break;

  case 0x00011374:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA" );
    break;

  case 0x00011375:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOCPROC_CMD_DEREGISTER_VOLUME_CALIBRATION_DATA" );
    break;

  case 0x00011368:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_SET_VAR_VOC_SAMPLING_RATE" );
    break;

  case 0x00011369:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_REGISTER_CALIBRATION_DATA_V2" );
    break;

  case 0x0001127A:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_DEREGISTER_CALIBRATION_DATA" );
    break;

  case 0x0001136A:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_SET_PACKET_EXCHANGE_MODE" );
    break;

  case 0x0001136B:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_CMD_SET_OOB_PACKET_EXCHANGE_CONFIG" );
    break;

  case 0x0001136C:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_READY" );
    break;

  case 0x0001136D:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_CONSUMED" );
    break;

  case 0x0001136E:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST" );
    break;

  case 0x0001136F:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_READY" );
    break;

  case 0x0001138B:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOLUME_CMD_MUTE_V2" );
    break;

  case 0x000112C1:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOLUME_CMD_SET_NUMBER_OF_STEPS" );
    break;

  case 0x000112C2:
    apr_log_pktv2p1_generic_analyze( handle, packet, "VSS_IVOLUME_CMD_SET_STEP" );
    break;

  default:
    apr_log_pktv2p1_generic_analyze( handle, packet, "unrecognized" );
    break;
  }
}

#endif /* USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES */

static void apr_log_shorten_filename (
  char* full_name,
  char* short_name,
  uint32_t short_name_len
)
{
#ifdef APR_ERROR_LOGGING
  uint32_t index;
  uint32_t len;
  char_t character;

  len = mmstd_strnlen( full_name, APR_LOG_MAX_FILENAME_SIZE_V );

  /* Find the last '/' or '\' in the filename
   * in order to be able to remove the path.
   */
  for ( index = len; index > 0; --index )
  {
    character = full_name[ index - 1 ];
    if ( ( character == '/' ) ||
         ( character == '\\' ) )
    { /* Remove the slash. */
      break;
    }
  }

  if ( index == len )
  { /* Handle inproper input case where only path name exist, e.g. path:\ */
    index = 0;
  }

  ( void ) mmstd_strlcpy( short_name, &full_name[ index ], short_name_len );

  /* Ensure NULL termination. */
  if ( ( len - index ) < short_name_len )
  {
    short_name[ len - index ] = '\0';
  }
  else 
  {
    short_name[ short_name_len - 1 ] = '\0';
  }
#endif /* APR_ERROR_LOGGING */
}

static void apr_log_circ_write (
  uint8_t* const buf,
  uint32_t const buf_size,
  uint32_t* offset,
  const uint8_t* const data,
  uint32_t const data_size
)
{
  uint32_t endpos;
  uint32_t segment_size;
  uint32_t remaining_size;

  remaining_size = ( ( data_size > buf_size ) ? buf_size : data_size );
  endpos = ( *offset + remaining_size );
  segment_size = ( buf_size - *offset );

  /* Check whether buffer is full or not */
  if ( endpos < buf_size )
  {
    ( void ) mmstd_memcpy( &buf[ *offset ], segment_size, data, remaining_size );
    *offset = endpos;
  }
  else
  {
    if ( endpos == buf_size )
    {
      /* Reset offset to 0 if end position and buffer size are equals */
      ( void ) mmstd_memcpy( &buf[ *offset ], segment_size, data, remaining_size );
      *offset = 0;
    }
    else
    {
      /* Since current data cannot be fitted into the buffer, write only 
       * data which cab be fitted at he end of buffer and write rest of the data
       * from begining of the buffer
       */       
      ( void ) mmstd_memcpy( &buf[ *offset ], segment_size, data, segment_size );
      remaining_size -= segment_size;
      *offset = remaining_size;
      ( void ) mmstd_memcpy( buf, buf_size, &data[ segment_size ], remaining_size );
    }
  }
}

/* Assume packet is not NULL. */
APR_INTERNAL int32_t apr_log_pktv2_packet (
  uint32_t handle,
  aprv2_packet_t* packet
)
{
  uint8_t *log_ptr;
  uint32_t packet_size;
  uint32_t marker;

#ifdef APR_USER_PD_FEATURE
  uint16_t dst_domain_id;
  uint16_t src_domain_id;
  uint32_t temp_packet_size;

  /* Check if source and destination addresses are local */
  src_domain_id = APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, packet->src_addr );
  dst_domain_id = APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, packet->dst_addr );

  if( src_domain_id != dst_domain_id )
  {
    temp_packet_size = APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header );
    packet_size = APRV2_PKT_GET_HEADER_BYTE_SIZE( packet->header );
    /* Since we are logging only packet header here, we need to update hearder
     * for correct size to support parser tool
     */
    packet->header &= ~( APRV2_PKT_PACKET_SIZE_MASK );
    packet->header |= APR_SET_FIELD( APRV2_PKT_PACKET_SIZE, packet_size );
  }
  else
#endif /* APR_USER_PD_FEATURE */
  {
    packet_size = APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header );
  }

  /* Log APR packet to SOS buffer. */
  {
    ( void ) apr_lock_enter( apr_log_history_mutex );

    marker = 0xE91111E9;
    apr_log_circ_write(
      apr_log_history.log, apr_log_history.size, &apr_log_history.offset,
      ( ( uint8_t* ) &marker ), sizeof( marker ) );
      /* Write the sync word. */
    apr_log_circ_write(
      apr_log_history.log, apr_log_history.size, &apr_log_history.offset,
      ( ( uint8_t* ) packet ), packet_size );
      /* Write the packet. */
    apr_log_history.offset = ( ( ( apr_log_history.offset + 3 ) >> 2 ) << 2 );
      /* Align to 32-bits. */

    ( void ) apr_lock_leave( apr_log_history_mutex );
  }

  /* Log APR packet to DIAG when enabled. */
  log_ptr = ( ( uint8_t* ) log_alloc( LOG_APR_ADSP_C,
              ( sizeof( log_hdr_type ) + packet_size ) ) );
  if ( log_ptr != NULL )
  {
    ( void ) mmstd_memcpy( ( log_ptr + sizeof( log_hdr_type ) ), packet_size,
	                       packet, packet_size );
    log_commit( log_ptr );
  }
#ifdef USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES
  else
  if ( apr_core_f3_enable_flag == TRUE )
  {
    switch ( APR_GET_FIELD( APRV2_PKT_VERSION, packet->header ) )
    {
    case APRV2P1_PKT_VERSION_V:
      apr_log_pktv2p1_analyze_packet( handle, ( ( aprv2p1_packet_t* ) packet ) );
      break;

    default:
      apr_log_pktv2_analyze_packet( handle, packet );
      break;
    }
  }
#endif /* USE_DEBUG_ANALYZE_PACKETS_TO_F3_MESSAGES */

#ifdef APR_USER_PD_FEATURE
  if( src_domain_id != dst_domain_id )
  {
    /* Revert actual packet size */
    packet->header &= ~( APRV2_PKT_PACKET_SIZE_MASK );
    packet->header |= APR_SET_FIELD( APRV2_PKT_PACKET_SIZE, temp_packet_size );
  }
#endif /* APR_USER_PD_FEATURE */

  return APR_EOK;
}

/* Assumes all pointers are not NULL and that all sizes are correct. */
static int32_t apr_log_write_segment (
  /* in/out */ void** dst_buffer,
  /* in/out */ uint32_t* dst_left,
  /* in */ void* src_buffer,
  /* in */ uint32_t src_size
)
{
  if ( src_size > *dst_left )
  {
    return APR_EFAILED;
  }

  ( void ) mmstd_memcpy( *dst_buffer, *dst_left, src_buffer, src_size );
  *( ( uint8_t** ) dst_buffer ) += src_size;
  *dst_left -= src_size;

  return APR_EOK;
}

/* Calling this function will log the current error to a circular buffer
** which can be recovered and parsed from a crash dump. The user can pass 
** the offending packet (if one exists) the line number and filename and 
** a short string (up to APR_LOG_ERROR_MAX_MSG_SIZE_V bytes). If the 
** provided string is longer than APR_LOG_ERROR_MAX_MSG_SIZE_V, only the 
** first APR_LOG_ERROR_MAX_MSG_SIZE_V bytes will be logged. 
*/
APR_INTERNAL int32_t apr_log_error (
  uint32_t handle, 
  aprv2_packet_t* packet, 
  uint32_t line_number,
  char* filename,
  char* msg, 
  int32_t param_1,
  int32_t param_2,
  int32_t param_3
)
{
#ifdef APR_ERROR_LOGGING
  int32_t rc;
  uint32_t checkpoint = 0;
  uint32_t log_entry_idx;
  uint8_t* buffer_ptr;
  uint32_t buffer_left;
  uint32_t write_size;

  /* Increment index to ensure safe access to error log buffer. */
  ( void ) apr_lock_enter( apr_log_error_mutex );
  log_entry_idx = apr_log_error_history.idx;
  apr_log_error_history.idx = ( apr_log_error_history.idx + 1 ) % apr_log_error_history.total_units;
  ( void ) apr_lock_leave( apr_log_error_mutex );

  /* Remove path from filename to reduce string size. */
  apr_log_shorten_filename( filename, apr_log_scratch_buffer, sizeof( apr_log_scratch_buffer ) );

  buffer_ptr = &apr_log_error_history.log[ log_entry_idx ][0];
  buffer_left = sizeof( apr_log_error_history.log[ log_entry_idx ] );

  for ( ;; )
  {
    checkpoint = 1;
    if ( packet == NULL )
    {
      break;
    }

    checkpoint = 2;
    write_size = sizeof( aprv2_packet_t );
    rc = apr_log_write_segment( ( ( void** ) &buffer_ptr ), &buffer_left, packet, write_size );
    if ( rc ) break;

    checkpoint = 3;
    write_size = MMSTD_MIN( sizeof( apr_log_scratch_buffer ), APR_LOG_MAX_SHORT_FILENAME_SIZE_V );
    rc = apr_log_write_segment( ( ( void** ) &buffer_ptr ), &buffer_left, apr_log_scratch_buffer, write_size );
    if ( rc ) break;

    checkpoint = 4;
    write_size = sizeof( line_number );
    rc = apr_log_write_segment( ( ( void** ) &buffer_ptr ), &buffer_left, &line_number, write_size );
    if ( rc ) break;

    checkpoint = 5;
    /* Write out the formatted string up to the amount of space left. */
    ( void ) mmstd_snprintf( ( ( char_t* ) buffer_ptr ), buffer_left,
                             msg, param_1, param_2, param_3 );

    return APR_EOK;
  }

  MSG_ERROR( "Error checkpoint (%d)", checkpoint, 0, 0 );

  switch ( checkpoint )
  {
  default:
    /* Nothing to clean up. */
    break;
  }

#endif /* APR_ERROR_LOGGING */

  return APR_EFAILED;
}

APR_INTERNAL  void apr_log_out_of_memory_error ( 
  uint32_t* handles, 
  uint32_t* memq_items, 
  uint32_t handle_count 
)
{
#ifdef APR_ERROR_LOGGING
  uint32_t index;

  if ( apr_log_error_history.is_first_out_of_memory_failure == FALSE )
  {
    if ( handle_count > APR_LOG_MAX_APR_USERS )
    {
      handle_count = APR_LOG_MAX_APR_USERS;
    }
    for ( index = 0; index < handle_count; index += 2 )
    {
      apr_log_error_history.out_of_memory_log[ index ] = handles[ index ];
      apr_log_error_history.out_of_memory_log[ index + 1 ] = memq_items[ index ];
    }
    apr_log_error_history.is_first_out_of_memory_failure = TRUE;
  }
#endif /* APR_ERROR_LOGGING */

  return;
}


