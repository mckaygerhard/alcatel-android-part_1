/*
  Copyright (C) 2010 - 2012 Qualcomm Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/apr.adsp/2.3/custom/user/src/aprv2_ipc.c#2 $
  $Author: pwbldsvc $
*/

#include "aprv2_ids_domains.h"
#include "aprv2_api_inline.h"
#include "apr_errcodes.h" /*TODO: remove this once moved to latest aprv2_api_inline.h. */
#include "aprv2_ipc.h"
/* #include "apr_smdl.h" */
#include "aprv2_mpd_i.h" /* for sending and receiving messages from kernel */
#include "msg.h"

//#include "sys_m_messages.h"
//#include "rcecb.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/
/*
#define APRV2_IPC_AUDIO_PORT_NAME ( "apr_audio_svc" )
#define APRV2_IPC_VOICE_PORT_NAME ( "apr_voice_svc" )
#define APRV2_IPC_APPS2_PORT_NAME ( "apr_apps2" )
*/
/*****************************************************************************
 * Variables                                                                 *
 ****************************************************************************/
/*
static apr_smdl_port_handle_t aprv2_ipc_apps_qdsp_port = NULL;
static apr_smdl_port_handle_t aprv2_ipc_apps_qdsp_port2 = NULL;
static apr_smdl_port_handle_t aprv2_ipc_modem_qdsp_port = NULL;

#ifdef USE_INIT_IPC_HACK
aprv2_ipc_send_fn_t aprv2_ipc_send_fn = aprv2_ipc_send_dummy;
#endif //USE_INIT_IPC_HACK
*/



/*

static bool_t aprv2_ipc_is_modem_up;
*/
/*****************************************************************************
 * Core Routine Implementations                                              *
 ****************************************************************************/
/*
static void aprv2_ipc_ssr_modem_after_powerup ( void )
{
}

static void aprv2_ipc_ssr_modem_before_shutdown ( void )
{
  aprv2_ipc_is_modem_up = FALSE;
}
*/
/* TODO: Consider moving rx packet handling to apr_smdl implementation. */
/*
static int32_t aprv2_ipc_tx_cb (
  void* tx_cb_data,
  void* tx_write_buf,
  uint32_t size
)
{
  aprv2_packet_t* packet;

  if( tx_write_buf != NULL )
  {
    packet = ( ( aprv2_packet_t* ) tx_write_buf );
    ( void )__aprv2_cmd_free( packet->dst_addr, packet );
  }

  return APR_EOK;
}
*/

static int32_t aprv2_mpd_cb ( 
  aprv2_mpd_event_t ev, 
  void* payload, 
  uint32_t sz
)
{
  int32_t rc = APR_EOK;
  int32_t handle;
  aprv2_packet_t* new_packet;
  
  switch(ev)
  {
  case APRV2_MPD_EV_RX_DATA: /* received rx data */
    new_packet = ( ( aprv2_packet_t* ) payload );

    handle = ( ( uint32_t ) ( ( ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, new_packet->dst_addr ) ) << 8 ) |
                              ( APR_GET_FIELD( APRV2_PKT_SERVICE_ID, new_packet->dst_addr ) ) ) );

    ( void ) __aprv2_cmd_forward( handle, new_packet );
    break;

  case APRV2_MPD_EV_SEND_FAILED:
      rc = APR_EFAILED;
      break;
  }

  if( rc )
  {
    MSG_ERROR( "ADSP aprv2_mpd_cb failed, rc = %d." , rc, 0, 0 );
  }
  return rc;
}

/*
static int32_t aprv2_ipc_rx_cb (
  void* rx_cb_data,
  void* rx_read_buf,
  uint32_t size
)
{
  int32_t rc;
  uint32_t packet_size;
  uint32_t alloc_type;
  int32_t handle;
  aprv2_packet_t* new_packet;
  
  for ( ;; )
  {
    new_packet = ( ( aprv2_packet_t* ) rx_read_buf );

    packet_size = APRV2_PKT_GET_PACKET_BYTE_SIZE( new_packet->header );

    alloc_type = ( ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, new_packet->header ) ==
                     APRV2_PKT_MSGTYPE_CMDRSP_V ) ?
                     APRV2_ALLOC_TYPE_RESPONSE_RAW :
                     APRV2_ALLOC_TYPE_COMMAND_RAW );

    rc = __aprv2_cmd_alloc(
          new_packet->dst_addr, //SUPER_HACK: Allocate memory from destination service's packet pool
          alloc_type, packet_size, &new_packet );
    if ( rc ) break;

    memcpy( new_packet, rx_read_buf, packet_size );

    handle = ( ( uint32_t ) ( ( ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, new_packet->dst_addr ) ) << 8 ) |
                              ( APR_GET_FIELD( APRV2_PKT_SERVICE_ID, new_packet->dst_addr ) ) ) );

    ( void ) __aprv2_cmd_forward( handle, new_packet );

    return APR_EOK;
  }

  MSG_ERROR( "MODEM aprv2_ipc_rx_cb failed, rc = %d." , rc, 0, 0 );
  return rc;
}

static void aprv2_ipc_modem_open_cb ( void )
{
  aprv2_ipc_is_modem_up = TRUE;
}
*/
APR_INTERNAL int32_t aprv2_ipc_init ( void )
{
  int32_t rc;
  uint32_t checkpoint = 0;
 // RCECB_HANDLE ssr_handle;

  MSG_HIGH( "ADSP aprv2_ipc_init starting initialization sequence.", 0, 0, 0 );

  for ( ;; )
  {
    /*
#ifndef SIM_DEFINED
    aprv2_ipc_is_modem_up = TRUE;
    rc = apr_smdl_init( );
    if ( rc ) break;
    checkpoint = 1;
// removed by cz.
    rc = apr_smdl_open( APRV2_IPC_AUDIO_PORT_NAME, sizeof( APRV2_IPC_AUDIO_PORT_NAME ),
                        APR_SMDL_APPS_QDSP_CHANNEL_ID, aprv2_ipc_rx_cb, NULL, NULL,
                        aprv2_ipc_tx_cb, NULL, APR_SMDL_STANDARD_FIFO, &aprv2_ipc_apps_qdsp_port );
    if ( rc ) break;
    checkpoint = 2;

    rc = apr_smdl_open( APRV2_IPC_APPS2_PORT_NAME, sizeof( APRV2_IPC_APPS2_PORT_NAME ),
                        APR_SMDL_APPS_QDSP_CHANNEL_ID, aprv2_ipc_rx_cb, NULL, NULL,
                        aprv2_ipc_tx_cb, NULL, APR_SMDL_STANDARD_FIFO, &aprv2_ipc_apps_qdsp_port2 );
    if ( rc ) break;
    checkpoint = 3;

    rc = apr_smdl_open( APRV2_IPC_VOICE_PORT_NAME, sizeof( APRV2_IPC_VOICE_PORT_NAME ),
                        APR_SMDL_MODEM_QDSP_CHANNEL_ID, aprv2_ipc_rx_cb, NULL, 
                        aprv2_ipc_modem_open_cb, aprv2_ipc_tx_cb, NULL, APR_SMDL_STANDARD_FIFO, 
                        &aprv2_ipc_modem_qdsp_port );
    if ( rc ) break;

    ssr_handle = rcecb_register_context_name( SYS_M_SSR_MODEM_BEFORE_SHUTDOWN, (void *)aprv2_ipc_ssr_modem_before_shutdown );
    if ( ssr_handle == NULL ) MSG_HIGH ( "aprv2_ipc_init: SSR Registration failed", 0, 0, 0 );
    ssr_handle = rcecb_register_context_name( SYS_M_SSR_MODEM_AFTER_POWERUP, (void *)aprv2_ipc_ssr_modem_after_powerup );
    if ( ssr_handle == NULL ) MSG_HIGH ( "aprv2_ipc_init: SSR Registration failed", 0, 0, 0 );
*//*
#endif

#ifdef USE_INIT_IPC_HACK
    aprv2_ipc_send_fn = aprv2_ipc_send;
#endif *//*USE_INIT_IPC_HACK*/

    /* register MPD call-back */
    aprv2_mpd_set_rx_cb(aprv2_mpd_cb);
    MSG_HIGH( "ADSP aprv2_ipc_init succeed.", 0, 0, 0 );
    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 3:
    //( void ) apr_smdl_close( aprv2_ipc_apps_qdsp_port2 );
    /*-fallthru */
  case 2:
    //( void ) apr_smdl_close( aprv2_ipc_apps_qdsp_port );
    /*-fallthru */
  case 1:
    /* ( void ) apr_smdl_deinit( ); */
    /*-fallthru */
  default:
    break;
  }

  MSG_ERROR( "ADSP aprv2_ipc_init failed after checkpoint=%d.", checkpoint, 0, 0);

  return rc;
}

APR_INTERNAL int32_t aprv2_ipc_deinit ( void )
{
  MSG_HIGH( "ADSP aprv2_ipc_deinit start.", 0, 0 ,0 );

#ifndef SIM_DEFINED
  //( void ) apr_smdl_close( aprv2_ipc_modem_qdsp_port );
  //( void ) apr_smdl_close( aprv2_ipc_apps_qdsp_port2 );
  //( void ) apr_smdl_close( aprv2_ipc_apps_qdsp_port );
 // ( void ) apr_smdl_deinit( );
#endif

  return APR_EOK;
}

APR_INTERNAL int32_t aprv2_ipc_is_domain_local (
  uint16_t domain_id
)
{
  switch ( domain_id )
  {
  case APRV2_IDS_DOMAIN_ID_ADSP_V:
    return APR_EOK;

  default:
    return APR_EFAILED;
  }
}

APR_INTERNAL int32_t aprv2_ipc_send (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint16_t dst_domain_id;

  if ( packet == NULL )
  {
    return APR_EBADPARAM;
  }
/*
#ifndef SIM_DEFINED
  if ( ( aprv2_ipc_apps_qdsp_port == NULL ) ||
       ( aprv2_ipc_apps_qdsp_port2 == NULL ) ||
       ( aprv2_ipc_modem_qdsp_port == NULL ) )
  {
    return APR_ENOTREADY;
  }
#endif
*/

  dst_domain_id  = APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, packet->dst_addr );

  switch ( dst_domain_id )
  {
  case APRV2_IDS_DOMAIN_ID_PC_V:
    /*-fallthru */
  case APRV2_IDS_DOMAIN_ID_MODEM_V:
  case APRV2_IDS_DOMAIN_ID_APPS2_V:
  case APRV2_IDS_DOMAIN_ID_APPS_V:
    rc = aprv2_mpd_send(packet);
    //rc = APR_ELPC;
    break;

  case APRV2_IDS_DOMAIN_ID_ADSP_V:
    rc = APR_ELPC;
    break;

  default:
    MSG_ERROR( "ADSP apr_ipc_send(0x%08X) sending to invaild domain.", 
               packet, 0, 0 );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

/*
#ifdef USE_INIT_IPC_HACK
APR_INTERNAL int32_t aprv2_ipc_send_dummy (
  aprv2_packet_t* packet
)
{
  int32_t rc = APR_EOK;
  uint16_t dst_domain_id;
  aprv2_cmd_free_t free_args;

  if ( NULL == packet )
  {
    return APR_EBADPARAM;
  }

  dst_domain_id  = APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, packet->dst_addr );

  switch ( dst_domain_id )
  {
    case APRV2_IDS_DOMAIN_ID_ADSP_V:
      rc = APR_ELPC;
      break;

    default:
      MSG_ERROR( "aprv2_ipc_send_dummy(0x%08X) IPC not ready or send to invalid domain", packet, 0, 0 );
      free_args.handle = 0;
      free_args.packet = packet;
      ( void ) aprv2_cmd_free( &free_args );
       break;
  }

  return rc;
}
#endif //USE_INIT_IPC_HACK
*/

