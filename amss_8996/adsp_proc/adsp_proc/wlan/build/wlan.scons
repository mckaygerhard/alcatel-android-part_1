#===============================================================================
#
# Wireless Local Area Network (WLAN)for ADSP and
# WLS QMI Server (WiFi Location Service QMI Server) for integrated WLAN solution
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/wlan.adsp/1.1/build/wlan.scons#7 $
#  $DateTime: 2015/10/29 10:03:29 $
#  $Author: pwbldsvc $
#  $Change: 9316860 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')
env = env.Clone()
env.Append(CPPDEFINES=['MSG_BT_SSID_DFLT=MSG_SSID_ONCRPC'])
#env.Append(CFLAGS = '-Werror')

#-------------------------------------------------------------------------------
# Integrated WLAN?
#-------------------------------------------------------------------------------

integrated_wlan=1
wls_bid = env['BUILD_ID']

if wls_bid.find("QCA") != -1 :
    integrated_wlan=1
else:
 	integrated_wlan=0

print 'WLS integrated_wlan = ' , integrated_wlan

if integrated_wlan==0:
	env.Append(CPPDEFINES=['CONFIG_WLS_INTEGRATED_WLAN=0'])
else:
	env.Append(CPPDEFINES=['CONFIG_WLS_INTEGRATED_WLAN=1'])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

if integrated_wlan == 0:
	env.PublishPrivateApi('WLAN', [
	    '${BUILD_ROOT}/wlan/src/wls/core',
	    '${BUILD_ROOT}/wlan/src/wls/util',
	    '${BUILD_ROOT}/wlan/src/wls/qmi_intf',
	    '${BUILD_ROOT}/wlan/src/wls/unit_test',
	    '${BUILD_ROOT}/wlan/src/wmi/src',
	    '${BUILD_ROOT}/wlan/src/wmi/include',
	    '${BUILD_ROOT}/wlan/src/glink'
	])
else:
	env.PublishPrivateApi('WLS', [
	    '../src/wls/core',
	    '../src/wls/util',
	    '../src/wls/qmi_intf',
	    '../src/wls/unit_test',
	    '../src/wmi/src',
	    '../src/wmi/include',
	    '../src/glink',
	    '../src/wlif'
	])

#--------------------------------------------------------------------------------
# Necessary Public API's
#--------------------------------------------------------------------------------
CORE_PUBLIC_APIS = [
   'DEBUGTOOLS',
   'DAL',
   'MPROC',
   'DIAG',
   'STORAGE',
   'SERVICES',
   'SYSTEMDRIVERS',
   'MPROC_QMI_PRIVATE',
#   'WIREDCONNECTIVITY',
   'KERNEL',
]

env.RequirePublicApi(CORE_PUBLIC_APIS, area='core')
env.RequirePublicApi(['TEST','COMMON','WLS'], area='QMIMSGS')
#env.RequireRestrictedApi(CORE_PUBLIC_APIS)

if integrated_wlan == 1:
	# WLAN_APIS
	WLS_QMISVR_API = [
	   'WLAN_MAC_CORE_API',
	   'WLAN_TARGET_API',
	#   'WLAN_HOST_FW_API',
	   'WLAN_OSIF_API',
#	   'WLAN_WAL_API',
#	   'WLAN_WAL_AR_API',
#       'WLAN_WHAL_API',
	#   "WLAN_WLS_QMISVC_API",
	# ????   
	   'WLAN_PROTOCOL_API',
	   'WLAN_PROTOCOL_SRC_API',
	      #HALPHY
	   'WLAN_HALPHY_API',
       'WLAN_PERF_ALGS_API',
	]
#	env.RequireRestrictedApi(WLS_QMISVR_API)

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

WLAN_WLS_SOURCES  =  [

   # top level code
   '../src/wls/core/wls_main.c',
   
   # location service code
   '../src/wls/core/wls_loc_server.c',
   
   # wlan service code
   '../src/wls/core/wls_wlan_service_server.c',
   '../src/wls/core/wls_wlan_service_data.c',
   
   # fw code
   '../src/wls/core/wls_fw.c',

   # ipc code
   '../src/wls/core/wls_ipc.c',

   # util code
   '../src/wls/util/wls_util.c',

   # unit test code
   '../src/wls/unit_test/wls_loc_client.c',
   '../src/wls/unit_test/wls_wlan_client.c',
   '../src/wls/unit_test/wls_fw_sim.c',

   # qmi interface codes
   '../src/wls/qmi_intf/wireless_lan_proxy_service_impl_v01.c',
   '../src/wls/qmi_intf/wireless_lan_proxy_service_v01.c',
  
]

WLAN_WMI_SOURCES  =  [
   '../src/wmi/src/wmi_unified.c'
]

if env['MSM_ID'] in ['8994']:
   WLS_GLINK_SOURCES  =  [
      '../src/glink/wls_glink.c'
   ]
elif env['MSM_ID'] in ['8992']:
   WLS_GLINK_SOURCES  =  [
      '../src/glink/wls_glink.c'
   ]
elif env['MSM_ID'] in ['8996']:
   WLS_GLINK_SOURCES  =  [
      '../src/glink/wls_glink_v2.c'
   ]

if integrated_wlan == 1:
    WLS_WLIF_SOURCES = [
   		'../src/wlif/wls_wlif.c'
	]

# need to remove once they are placed in qmimsgs
if integrated_wlan == 1:
	WLAN_WLS_SOURCES += [ 
		'../src/wls/qmi_intf/wlan_location_service_v01.c',
    ]

if integrated_wlan == 1:
	env.AddBinaryLibrary(['CNSS_IMAGE'],'../src/wls_adsp',WLAN_WLS_SOURCES)
	env.AddBinaryLibrary(['CNSS_IMAGE'],'../src/wlan_wmi_lib',WLAN_WMI_SOURCES)
	env.AddBinaryLibrary(['CNSS_IMAGE'],'../src/wls_wlif_lib',WLS_WLIF_SOURCES)
else:
	env.AddBinaryLibrary(['QDSP6_SW_IMAGE','CBSP_QDSP6_SW_IMAGE','CORE_QDSP6_SW'],'${BUILDPATH}/wls_adsp',WLAN_WLS_SOURCES)
	env.AddBinaryLibrary(['QDSP6_SW_IMAGE','CBSP_QDSP6_SW_IMAGE','CORE_QDSP6_SW'],'${BUILDPATH}/wlan_wmi_lib',WLAN_WMI_SOURCES)
	env.AddBinaryLibrary(['QDSP6_SW_IMAGE','CBSP_QDSP6_SW_IMAGE','CORE_QDSP6_SW'],'${BUILDPATH}/wls_glink_lib',WLS_GLINK_SOURCES)

# header, .idl and some .c files are not deleted by AddBinaryLibrary so cleaning up using env.cleanpack
CLEAN_LIST=[]
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/wls/core")) 
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/wls/util")) 
CLEAN_LIST.extend(env.FindFiles(['*.c', '*.h'], "../src/wls/qmi_intf")) 
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/wls/unit_test")) 
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/wmi/src")) 
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/wmi/include")) 
CLEAN_LIST.extend(env.FindFiles(['*.h'], "../src/glink")) 
CLEAN_LIST.extend(env.FindFiles(['*.c'], "../src/glink")) 
CLEAN_LIST.extend(env.FindFiles(['*.idl'], "../src/wls/qmi_intf")) 
CLEAN_LIST.extend(env.FindFiles(['*.c', '*.h'], "../src/wlif"))
env.CleanPack(['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE','CORE_QDSP6_SW'], CLEAN_LIST) 

#-------------------------------------------------------------------------------

if integrated_wlan == 0:
	if 'USES_RCINIT' in env:
	   RCINIT_IMG = ['CORE_MODEM', 'CORE_QDSP6_SW', 'CORE_GSS']
	   env.AddRCInitFunc(
	    RCINIT_IMG,
	    {
	     'sequence_group'             : 'RCINIT_GROUP_3',                 # required
	     'init_name'                  : 'wls',                            # required
	     'init_function'              : 'wls_init',                       # required
		 'dependencies'               : ['qmi_fw']
	    })
