#===============================================================================
# Copyright (c) 2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#===============================================================================
Import('env')

env = env.Clone()

platform_root = '${BUILD_ROOT}/platform'
env.Replace(ADSP_PLATFORM_ROOT = platform_root)
build_folder = str(env.RealPath('${BUILDPATH}'))

APIS=[
  'FASTRPC_PROCESS',
  'FASTRPC_THREAD_QUEUE',
  'FASTRPC_PORT',
  'FASTRPC_INVOKE',
  'FASTRPC_LOADER',
  'PLATFORM_LIBS',
  'MOD_TABLE',
  'QAIC',
  'HAPSDK',
  'REMOTE',
  'PLS',
  'STDDEF',
  'A1STD',
  'ATOMIC',
  'UTILS',
  'QLIST',
  'HAP',
  'QI',
  ]

if 'USES_FASTRPC_SHELL_IMG' in env:
  APIS += ['FASTRPC_HEAP']

env.RequireRestrictedApi(APIS)
env.RequirePublicApi(APIS)


core_public_apis = [
   'KERNEL',
   ]

env.RequirePublicApi(core_public_apis, area='core')
env.PLRegister("fastrpc_exception",['FASTRPC_SHELL_IMG'])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${ADSP_PLATFORM_ROOT}/fastrpc_process/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

SHELL_DEBUG_SOURCES = [
  '${BUILDPATH}/fastrpc_uprocess.c',
  env.QaicSkelFromIdl('adsp_process_group'),
  env.QaicSkelFromIdl('adsp_current_process'),
  env.PLCreateDebugFile(str(build_folder + '/fastrpc_exception_debug.c'), str(env.RealPath('${ADSP_PLATFORM_ROOT}/fastrpc_process/src/fastrpc_exception.c'))),
  ]

SHELL_SOURCES = [
  '${BUILDPATH}/fastrpc_uprocess.c',
  '${BUILDPATH}/fastrpc_exception.c',
  env.QaicSkelFromIdl('adsp_process_group'),
  env.QaicSkelFromIdl('adsp_current_process'),
  ]

SOURCES =  [
  '${BUILDPATH}/fastrpc_kprocess.c',
  env.QaicSkelFromIdl('adsp_process_group'),
  env.QaicSkelFromIdl('adsp_current_process'),
  env.QaicSkelFromIdl('adsp_default_listener'),
  ]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
lib = env.AddBinaryLibrary(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE', 'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
 'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE'],'${BUILDPATH}/fastrpc_process', SOURCES)

lib = env.AddBinaryLibrary(['FASTRPC_SHELL_IMG'],'${BUILDPATH}/fastrpc_uprocess', SHELL_SOURCES)
lib = env.AddBinaryLibrary(['FASTRPC_SHELL_IMG'],'${BUILDPATH}/fastrpc_uprocess_debug', SHELL_DEBUG_SOURCES)

#-------------------------------------------------------------------------------
# Load Subunits (sys)
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

# Clean / pack rules
CLEAN_LIST=[]
CLEAN_LIST.extend(env.FindFiles(['*.c', '*.cpp', '*.h', '*.s'], '${ADSP_PLATFORM_ROOT}/fastrpc_process/src'))

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE', 'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
     'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE', 'FASTRPC_SHELL_IMG'], CLEAN_LIST)
