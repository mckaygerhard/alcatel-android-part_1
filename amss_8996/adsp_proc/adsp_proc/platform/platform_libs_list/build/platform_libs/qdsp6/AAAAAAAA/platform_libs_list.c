#include "platform_libs_list.h"
#include "platform_libs.h"

PL_DEP(HAP_utils)
PL_DEP(HAP_diag)
PL_DEP(rtld)
PL_DEP(platform_fs_qdi)
PL_DEP(vr_qdi)
PL_DEP(platform_qdi)
PL_DEP(rpcversion)
PL_DEP(listener)
PL_DEP(perf)
PL_DEP(fastrpc_smd)
PL_DEP(platform_fs)
PL_DEP(sigverify)
PL_DEP(HAP_ps)

struct platform_lib* (*pl_list[])(void) = {
	PL_ENTRY(HAP_utils),
	PL_ENTRY(HAP_diag),
	PL_ENTRY(rtld),
	PL_ENTRY(platform_fs_qdi),
	PL_ENTRY(vr_qdi),
	PL_ENTRY(platform_qdi),
	PL_ENTRY(rpcversion),
	PL_ENTRY(listener),
	PL_ENTRY(perf),
	PL_ENTRY(fastrpc_smd),
	PL_ENTRY(platform_fs),
	PL_ENTRY(sigverify),
	PL_ENTRY(HAP_ps),
	0
};
