# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c" 2
# 13 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c"
#pragma clang diagnostic ignored "-Wunused-function"



# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/inc/sec_hwio.h" 1
# 15 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/inc/sec_hwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 107 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;







typedef unsigned char byte;



typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
      typedef long long int64;



      typedef unsigned long long uint64;
# 16 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/inc/sec_hwio.h" 2

typedef struct {
    uint32 serial_number_addr;
    uint32 chip_id_addr;
    uint32 oem_id_addr;
    uint32 reserved1;
    uint32 reserved2;
    uint32 reserved3;
}pl_sechwio_type;

uint32 SecHWIO_GetSerialNumber(void);
uint16 SecHWIO_GetChipID(void);
uint16 SecHWIO_GetOEMID(void);
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h" 1
# 57 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/HALcomdef.h"
typedef unsigned long int bool32;
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/HALhwio.h" 2
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/hwio/msm8996/msmhwiobase.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/msmhwioreg.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/platform/security/config/msm8996/sec_hwio_devcfg.c" 2

pl_sechwio_type pl_sechwio = {
    ((0xe0270000 + 0x00000000) + 0x00004138),
    ((0xe0270000 + 0x00000000) + 0x0000413c),
    ((0xe0270000 + 0x00000000) + 0x000060f8),
    0,
    0,
    0
};
