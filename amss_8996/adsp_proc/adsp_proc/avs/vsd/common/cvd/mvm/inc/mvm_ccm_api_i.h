#ifndef __MVM_CCM_API_I_H__
#define __MVM_CCM_API_I_H__

/*
  Copyright (C) 2013-2014 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/mvm/inc/mvm_ccm_api_i.h#1 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "aprv2_packet.h"

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/

/****************************************************************************
 * PROTOTYPES                                                               *
 ****************************************************************************/

/**
  Initialize the MVM Concurrency Monitor (MVM-CCM). Must be called before any
  other MVM-CCM APIs can be called, and must be called only once.

  @param[in] mvm_service_apr_handle APR handle of the MVM service.
  @param[in] mvm_service_apr_addr   APR address of the MVM service.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t mvm_ccm_init ( 
  uint32_t mvm_service_apr_handle,
  uint16_t mvm_service_apr_addr
);

/**
  Deinitialize the MVM-CCM.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t mvm_ccm_deinit ( void );

/**
  Process an MVM-CCM event.

  @param[in] packet Event packet to be processed.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t mvm_ccm_process_evt (
  aprv2_packet_t* packet
);

/**
  Process an MVM-CCM command.

  @param[in] packet Command packet to be processed.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t mvm_ccm_process_cmd (
  aprv2_packet_t* packet
);

#endif /* __MVM_CCM_API_I_H__ */

