/*
  Copyright (C) 2013-2015, 2015 QUALCOMM Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary.

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/mvm/src/mvm_ccm.c#1 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include "msg.h"
#include "err.h"

#include "adsp_vpm_api.h"
#ifndef WINSIM
  #include "mmpm.h"
  #include "npa.h"
  #include "ClockDefs.h"
#endif /* !WINSIM */

#include "apr_list.h"
#include "apr_lock.h"
#include "apr_memmgr.h"
#include "aprv2_api_inline.h"

#include "vss_public_if.h"

#include "vss_private_if.h"

#include "mvm_ccm_api_i.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define MVM_CCM_HEAP_SIZE_V ( 500 )

#define MVM_CCM_MAX_ACTIVE_MVM_SESSIONS_V ( 5 )
  /**< Maximum number of MVM sessions that can be active concurrently.
       TODO: This number is tentative. It is based on the maximum number of
       MVM full control sessions that can be created. Do we want to limit
       the number of active voice sessions in CVD or do we rely on HLOS to
       reject un-supported concurrency use cases? */

#define MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V ( 3 )
  /**< Number of resources to request/release for each MMPM request/release.
       Currently in each MMPM request/release, MVM-CCM requests/releases 3
       resources: MIPS, Bus bandwidth and sleep latency. */

#define MVM_CCM_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

#define MVM_CCM_MMPM_CLK_REQUEST_TAG    ( 1 )
#define MVM_CCM_MMPM_CLK_OVERRIDE_TAG   ( 2 )
#define MVM_CCM_MMPM_CLK_RELEASE_TAG    ( 3 )
#define MVM_CCM_DEFAULT_MPPS_SCALE_FACTOR ( 20 )    /**< actual value = 2.0 */
#define MVM_CCM_DEFAULT_BW_SCALE_FACTOR   ( 10 )    /**< actual value = 1.0 */
#define MVM_CCM_SCALE_FACTOR_DIV        ( 10.0 )
#define MVM_CCM_MAX_NUM_CLK_PERF_LEVELS ( 6 )

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/
typedef struct mvm_ccm_clk_control_config_t
{
  uint32_t minor_version;
  uint32_t size;
  uint32_t num_columns;
  void* columns;
}
  mvm_ccm_clk_control_config_t;

typedef struct mvm_ccm_system_config_t
{
  uint16_t num_voice_sessions;
    /**< Number of voice sessions active in the system.
         Note that we could use mvm_ccm_active_mvm_session_tracking_used_q.size
         to keep track of the number of active voice sessions. However, having
         num_voice_sessions together with the rest of the system configurations
         makes the code clean. */
  uint16_t num_nb_streams;
    /**< Number of narrow-band (8 KHz) streams active in the system. */
  uint16_t num_wb_streams;
    /**< Number of wide-band (16 KHz) streams active in the system. */
  uint16_t num_swb_streams;
    /**< Number of super wide-band (32 KHz) streams active in the system. */
  uint16_t num_fb_plus_streams;
    /**< Number of full-band (48 KHz) or higher streams active in the system. */
  uint16_t num_nb_vocprocs;
    /**< Number of narrow-band (8 KHz) vocprocs active in the system. */
  uint16_t num_wb_vocprocs;
    /**< Number of wide-band (16 Khz) vocprocs active in the system. */
  uint16_t num_swb_vocprocs;
    /**< Number of super wide-band (32 KHz) vocprocs active in the system. */
  uint16_t num_fb_plus_vocprocs;
    /**< Number of full-band (48 Khz) or higher vocprocs active
         in the system. */
  uint32_t total_kpps;
    /**< Total KPPS for all the streams and vocprocs active in the system. */
  uint32_t total_core_kpps;
    /**< VDSP's current Q6 core KPPS that has been requested by
         MVM-CCM to MMPM. */
  uint32_t tx_topology_id;
    /**< Tx vocproc topology ID. Applicable only if there is a single active
         voice session which has a single vocproc attached to it. It is ignored
         otherwise. */
  uint32_t rx_topology_id;
    /**< Rx vocproc topology ID. Applicable only if there is a single active
         voice session which has a single vocproc attached to it. It is ignored
         otherwise. */
  uint32_t media_id;
    /**< Media ID. Applicable only if there is a single active
         voice session which has a single vocproc and/or stream attached to it.
         It is ignored otherwise. */
  uint16_t vfr_mode;
    /**< The VFR mode. Applicable only if there is a single active voice
         session in the system, which has a single vocproc and/or stream
         attached to it. It is ignored otherwise.
         The supported values:\n
         #VSS_ICOMMON_VFR_MODE_SOFT \n
         #VSS_ICOMMON_VFR_MODE_HARD */
  uint32_t tx_num_channels;
    /**< Tx Number of channels. Applicable only if there is a single active
         voice session which has a single vocproc attached to it. It is ignored
         otherwise. */
  uint32_t num_clk_perf_levels;
    /**< Number of clock levels. */
  uint32_t clk_perf_level[ MVM_CCM_MAX_NUM_CLK_PERF_LEVELS ];
    /**< Clock levels. */
  uint32_t tx_mpps_scale_factor;
    /** Tx MPPS scale factor. */
  uint32_t tx_bw_scale_factor;
    /** Tx BW scale factor. */
  uint32_t rx_mpps_scale_factor;
    /** Rx MPPS scale factor. */
  uint32_t rx_bw_scale_factor;
    /** Rx BW scale factor. */
}
  mvm_ccm_system_config_t;

typedef struct mvm_ccm_active_mvm_session_tracking_obj_t
{
  uint16_t handle;
    /**< Handle to this MVM session. */
  uint16_t num_nb_streams;
    /**< Number of narrow-band (8 KHz) streams connected to this MVM
         session. */
  uint16_t num_wb_streams;
    /**< Number of wide-band (16 KHz) streams connected to this MVM
         session */
  uint16_t num_swb_streams;
    /**< Number of super wide-band (32 KHz) streams connected to this MVM
         session */
  uint16_t num_fb_plus_streams;
    /**< Number of full-band (48 KHz) or higher streams connected to this MVM
         session */
  uint16_t num_nb_vocprocs;
    /**< Number of narrow-band (8 KHz) vocprocs connected to this MVM
         session. */
  uint16_t num_wb_vocprocs;
    /**< Number of wide-band (16 Khz) vocprocs connected to this MVM
         session */
  uint16_t num_swb_vocprocs;
    /**< Number of super wide-band (32 KHz) vocprocs connected to this MVM
         session */
  uint16_t num_fb_plus_vocprocs;
    /**< Number of full-band (48 Khz) or higher vocprocs connected to this MVM
         session */
  uint32_t total_kpps;
    /**< Total KPPS for all the streams and vocprocs connected to this MVM
         session. */
  uint32_t tx_topology_id;
    /**< Tx vocproc topology ID. Applicable only if there is a single vocproc
         connected to this MVM session. It is ignored otherwise. */
  uint32_t rx_topology_id;
    /**< Rx vocproc topology ID. Applicable only if there is a single vocproc
         connected to this MVM session. It is ignored otherwise. */
  uint32_t media_id;
    /**< Media ID. Applicable only if there is a single vocproc and/or stream
         connected to this MVM session. It is ignored otherwise. */
  uint16_t vfr_mode;
    /**< The VFR mode. The supported values:\n
         The supported values:\n
         #VSS_ICOMMON_VFR_MODE_SOFT \n
         #VSS_ICOMMON_VFR_MODE_HARD */
  uint32_t tx_num_channels;
    /**< Tx Number of channels. Applicable only if there is a single active
         voice session which has a single vocproc attached to it. It is ignored
         otherwise. */
  uint32_t tx_mpps_scale_factor;
    /** Tx MPPS scale factor. */
  uint32_t tx_bw_scale_factor;
    /** Tx BW scale factor. */
  uint32_t rx_mpps_scale_factor;
    /** Rx MPPS scale factor. */
  uint32_t rx_bw_scale_factor;
    /** Rx BW scale factor. */
}
  mvm_ccm_active_mvm_session_tracking_obj_t;

typedef struct mvm_ccm_active_mvm_session_tracking_list_item_t
{
  apr_list_node_t link;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj;
}
  mvm_ccm_active_mvm_session_tracking_list_item_t;

typedef struct mvm_ccm_clk_override_tracking_obj_t
{
  uint16_t handle;
  /**< Handle to this MVM session. */
}
  mvm_ccm_clk_override_tracking_obj_t;

typedef struct mvm_ccm_clk_override_tracking_list_item_t
{
  apr_list_node_t link;
  mvm_ccm_clk_override_tracking_obj_t session_tracking_obj;
}
  mvm_ccm_clk_override_tracking_list_item_t;

typedef enum mvm_ccm_clock_event_enum_t
{
  MVM_CCM_CLK_EVENT_NONE,
  MVM_CCM_CLK_EVENT_SET_OVERRIDE,
  MVM_CCM_CLK_EVENT_RESET_OVERRIDE,
  MVM_CCM_CLK_EVENT_SET_SYSTEM_CONFIG,
  MVM_CCM_CLK_EVENT_MMPM_CB,
  MVM_CCM_CLK_EVENT_ABORT,
  MVM_CCM_CLK_EVENT_INVALID
}
  mvm_ccm_clock_event_enum_t;

typedef enum mvm_ccm_clock_state_enum_t
{
  MVM_CCM_CLK_CTRL_STATE_IDLE,
    /**< There is no pending response callback from MMPM to MVM_CCM. */
  MVM_CCM_CLK_CTRL_STATE_WAIT,
    /**< MVM_CCM is waiting for response callback from MMPM. */
  MVM_CCM_CLK_CTRL_STATE_ABORT,
    /**< MVM_CCM got deinitialized. */
  MVM_CCM_CLK_CTRL_STATE_INVALID
}
  mvm_ccm_clock_state_enum_t;

typedef struct mvm_ccm_clk_state_t
{
  mvm_ccm_clock_state_enum_t state;
  uint16_t pending_override_flag;
    /**< When set to '1', indicates that there is a pending request for
         high clock. */
  uint16_t pending_config_flag;
    /**< When set to '1', indicates that there is pending clock
         configuration request from one or more MVM sessions to MVM_CCM. */
  uint16_t override_flag;
    /**< When set to '1', indicates that currently the clock is set to
         maximum, overriding the system configuration. As long as this flag
         is set, MVM_CCM should not vote for any other clock than maximum. */
  uint16_t pending_reset_override_flag;
    /**< When set to '1', indicates that there is a pending request
         to reset the clock to as per system configuration. */
  apr_lock_t lock;
}
  mvm_ccm_clk_state_t;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Heap Management. */
static uint8_t mvm_ccm_heap_pool[ MVM_CCM_HEAP_SIZE_V ];
static apr_memmgr_type mvm_ccm_heapmgr;

/* Active MVM session management. */
static mvm_ccm_active_mvm_session_tracking_list_item_t mvm_ccm_active_mvm_session_tracking_list_item_pool[ MVM_CCM_MAX_ACTIVE_MVM_SESSIONS_V ];
static apr_list_t mvm_ccm_active_mvm_session_tracking_free_q;
static apr_list_t mvm_ccm_active_mvm_session_tracking_used_q;

static mvm_ccm_clk_override_tracking_list_item_t mvm_ccm_clk_override_tracking_list_item_pool[ MVM_CCM_MAX_ACTIVE_MVM_SESSIONS_V ];
static apr_list_t mvm_ccm_clk_override_tracking_free_q;
static apr_list_t mvm_ccm_clk_override_tracking_used_q;

/* Overall system configuration management. */
static mvm_ccm_system_config_t mvm_ccm_system_config;
static uint32_t mvm_ccm_max_mips_value;

static uint32_t mvm_ccm_mvm_service_apr_handle;
static uint16_t mvm_ccm_mvm_service_apr_addr;

static mvm_ccm_clk_state_t mvm_ccm_clk_state;

#if ( ADSPPM_INTEGRATION == 1 )
static char_t mvm_ccm_mmpm_client_name[] = "MVM_CCM";
static uint32_t mvm_ccm_mmpm_client_id;
#endif

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

static void mvm_ccm_clk_state_ctrl ( mvm_ccm_clock_event_enum_t event );
#ifndef WINSIM
APR_INTERNAL int32_t mvm_ccm_get_custom_topo_clock_and_bus_config (
  mvm_ccm_system_config_t* use_case,
  cvd_devcfg_parser_clock_and_bus_config_t* clock_and_bus_config
);
#endif
static bool_t mvm_ccm_is_not_internal_topology( uint32_t topology_id );

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

/****************************************************************************
 * MVM-CCM CORE ROUTINES                                                    *
 ****************************************************************************/

static int32_t mvm_ccm_get_active_mvm_session_tracking_obj (
  uint16_t mvm_session_handle,
  mvm_ccm_active_mvm_session_tracking_obj_t** ret_session_tracking_obj
)
{
  int32_t rc;
  mvm_ccm_active_mvm_session_tracking_list_item_t* session_tracking_list_item;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj = NULL;

  if ( ret_session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_tracking_list_item =
    ( ( mvm_ccm_active_mvm_session_tracking_list_item_t* )
      &mvm_ccm_active_mvm_session_tracking_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next(
           &mvm_ccm_active_mvm_session_tracking_used_q,
           ( ( apr_list_node_t* ) session_tracking_list_item ),
           ( ( apr_list_node_t** ) &session_tracking_list_item ) );
    if ( rc ) break;

    session_tracking_obj = session_tracking_list_item->session_tracking_obj;

    if ( session_tracking_obj->handle == mvm_session_handle )
    { /* Found a session with the client specified MVM session handle. */
      break;
    }
    else
    { /* Keep looking. */
      session_tracking_obj = NULL;
    }
  }

  if ( session_tracking_obj == NULL )
  {
    rc = APR_ENOTEXIST;
  }
  else
  {
    rc = APR_EOK;
    *ret_session_tracking_obj = session_tracking_obj;
  }

  return rc;
}

static int32_t mvm_ccm_create_active_mvm_session_tracking_obj (
  mvm_ccm_active_mvm_session_tracking_obj_t** ret_session_tracking_obj
)
{
  int32_t rc;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj;

  if ( ret_session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    session_tracking_obj = apr_memmgr_malloc(
                             &mvm_ccm_heapmgr,
                             sizeof( mvm_ccm_active_mvm_session_tracking_obj_t ) );
    if ( session_tracking_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_create_active_mvm_session_tracking_obj(): Failed to "
           "allocate memory for session tracking object." );
      rc = APR_ENORESOURCE;
      break;
    }

    { /* Initialize the session tracking object. */
      session_tracking_obj->handle = APR_NULL_V;
      session_tracking_obj->num_nb_streams = 0;
      session_tracking_obj->num_wb_streams = 0;
      session_tracking_obj->num_swb_streams = 0;
      session_tracking_obj->num_fb_plus_streams = 0;
      session_tracking_obj->num_nb_vocprocs = 0;
      session_tracking_obj->num_wb_vocprocs = 0;
      session_tracking_obj->num_swb_vocprocs = 0;
      session_tracking_obj->num_fb_plus_vocprocs = 0;
      session_tracking_obj->total_kpps = 0;
      session_tracking_obj->tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
      session_tracking_obj->rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
      session_tracking_obj->media_id = VSS_MEDIA_ID_NONE;
      session_tracking_obj->vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
    }

    *ret_session_tracking_obj = session_tracking_obj;
    rc = APR_EOK;
    break;
  }

  return rc;
}

static int32_t mvm_ccm_destroy_active_mvm_session_tracking_obj (
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj
)
{
  if ( session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  apr_memmgr_free( &mvm_ccm_heapmgr, session_tracking_obj );

  return APR_EOK;
}

static int32_t mvm_ccm_queue_active_mvm_session_tracking_obj (
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj
)
{
  int32_t rc;
  mvm_ccm_active_mvm_session_tracking_list_item_t* session_tracking_list_item;

  if ( session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    { /* Get a free active MVM session tracking list item. */
      rc = apr_list_remove_head(
             &mvm_ccm_active_mvm_session_tracking_free_q,
             ( ( apr_list_node_t** ) &session_tracking_list_item ) );
      if ( rc )
      { /* No free session tracking list item is available. */
        rc = APR_ENORESOURCE;
        break;
      }
    }

    { /* Queue the new active MVM session tracking obj. */
      session_tracking_list_item->session_tracking_obj = session_tracking_obj;

      ( void ) apr_list_add_tail(
                 &mvm_ccm_active_mvm_session_tracking_used_q,
                 &session_tracking_list_item->link );
    }

    break;
  }

  return rc;
}

static int32_t mvm_ccm_dequeue_active_mvm_session_tracking_obj (
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj
)
{
  int32_t rc;
  mvm_ccm_active_mvm_session_tracking_list_item_t* session_tracking_list_item;

  if ( session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_tracking_list_item =
    ( ( mvm_ccm_active_mvm_session_tracking_list_item_t* )
      &mvm_ccm_active_mvm_session_tracking_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next(
           &mvm_ccm_active_mvm_session_tracking_used_q,
           ( ( apr_list_node_t* ) session_tracking_list_item ),
           ( ( apr_list_node_t** ) &session_tracking_list_item ) );
    if ( rc ) break;

    if ( session_tracking_obj == session_tracking_list_item->session_tracking_obj )
    {
      ( void ) apr_list_delete(
                 &mvm_ccm_active_mvm_session_tracking_used_q,
                 &session_tracking_list_item->link );
      ( void ) apr_list_add_tail(
                 &mvm_ccm_active_mvm_session_tracking_free_q,
                 &session_tracking_list_item->link );
      break;
    }
  }

  return rc;
}

static int32_t mvm_ccm_queue_clk_override_request (
  uint16_t session_handle
)
{
  int32_t rc = APR_EOK;
  mvm_ccm_clk_override_tracking_list_item_t* session_tracking_list_item;
  bool_t match_found = FALSE;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "mvm_ccm_queue_clk_override_request: mvm_session 0x%x", session_handle );

  /* check if there is already one request queued from same session. */
  session_tracking_list_item =
    ( ( mvm_ccm_clk_override_tracking_list_item_t* )
       &mvm_ccm_clk_override_tracking_used_q.dummy );

  for ( ;; )
  {
    for ( ;; )
    { /* Traverse the list to check if item already tracked. */
      rc = apr_list_get_next(
             &mvm_ccm_clk_override_tracking_used_q,
             ( ( apr_list_node_t* ) session_tracking_list_item ),
             ( ( apr_list_node_t** ) &session_tracking_list_item ) );
      if ( rc ) break;

      if ( session_handle == session_tracking_list_item->session_tracking_obj.handle )
      {
        match_found = TRUE;
        break;
      }
    }

    if ( match_found == FALSE )
    {
      { /* Get a free clk override tracking list item. */
        rc = apr_list_remove_head(
               &mvm_ccm_clk_override_tracking_free_q,
               ( ( apr_list_node_t** ) &session_tracking_list_item ) );

        if ( rc )
        { /* No free session tracking list item is available. */
          rc = APR_ENORESOURCE;
          break;
        }
      }

      { /* Queue the new clk override tracking obj. */
        session_tracking_list_item->session_tracking_obj.handle = session_handle;
        ( void ) apr_list_add_tail(
                   &mvm_ccm_clk_override_tracking_used_q,
                   &session_tracking_list_item->link );
      }
    }

    break;
  }

  return rc;
}

static int32_t mvm_ccm_dequeue_clk_override_request( uint16_t session_handle )
{
  int32_t rc =APR_EOK;
  mvm_ccm_clk_override_tracking_list_item_t* session_tracking_list_item;

  session_tracking_list_item =
	  ( ( mvm_ccm_clk_override_tracking_list_item_t* )
          &mvm_ccm_clk_override_tracking_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next(
           &mvm_ccm_clk_override_tracking_used_q,
           ( ( apr_list_node_t* ) session_tracking_list_item ),
           ( ( apr_list_node_t** ) &session_tracking_list_item ) );
    if ( rc ) break;

    if ( session_handle == session_tracking_list_item->session_tracking_obj.handle)
    {
      ( void ) apr_list_delete(
                 &mvm_ccm_clk_override_tracking_used_q,
                 &session_tracking_list_item->link );
      ( void ) apr_list_add_tail(
                 &mvm_ccm_clk_override_tracking_free_q,
                 &session_tracking_list_item->link );
      break;
    }
  }

  return APR_EOK;
}

#ifndef WINSIM

uint32 mvm_ccm_mmpm_callback( MmpmCallbackParamType *pCbParam )
{
  MmpmCompletionCallbackDataType *callback_data;

  callback_data = ( MmpmCompletionCallbackDataType* )pCbParam->callbackData;

  switch( callback_data->reqTag )
  {
    case MVM_CCM_MMPM_CLK_REQUEST_TAG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "mvm_ccm_mmpm_callback: clk request status %d",
             callback_data->result );
      break;

    case MVM_CCM_MMPM_CLK_OVERRIDE_TAG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "mvm_ccm_mmpm_callback: clk override request status %d",
             callback_data->result );
      break;

    case MVM_CCM_MMPM_CLK_RELEASE_TAG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "mvm_ccm_mmpm_callback: clk release request status %d",
             callback_data->result );
      break;

    default:
      break;
  }
  mvm_ccm_clk_state_ctrl( MVM_CCM_CLK_EVENT_MMPM_CB );

  return APR_EOK;
}

static int32_t mvm_ccm_mmpm_register ( void )
{
  int32_t rc = APR_EOK;

#if ( ADSPPM_INTEGRATION == 1 )

  MmpmRegParamType register_param;
  cvd_devcfg_parser_mmpm_core_info_t mmpm_core_info;
  MmpmClientClassType class_type;
  MmpmParameterConfigType param_cfg;
  MMPM_STATUS status = MMPM_STATUS_SUCCESS;
  MmpmDcvsParticipationType dcvs_participation;

  for ( ;; )
  {
    rc = cvd_devcfg_parser_get_mmpm_core_info( &mmpm_core_info );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_mmpm_register(): Failed to get mmpm core info from "
             "cvd devcfg parser, rc = 0x%08X", rc );
      break;
    }

    register_param.rev = MMPM_REVISION;
    register_param.coreId = mmpm_core_info.core_id;
    register_param.instanceId = mmpm_core_info.instance_id;
    register_param.pClientName = mvm_ccm_mmpm_client_name;
    register_param.pwrCtrlFlag = PWR_CTRL_NONE;
    register_param.callBackFlag = CALLBACK_REQUEST_COMPLETE;
    register_param.MMPM_Callback = mvm_ccm_mmpm_callback;
    register_param.cbFcnStackSize = 10000; /* As a worst case approximation. */

    mvm_ccm_mmpm_client_id = MMPM_Register_Ext( &register_param );
    if ( mvm_ccm_mmpm_client_id == 0 )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_mmpm_register(): Failed to register with MMPM." );
      rc = APR_EFAILED;
      break;
    }

    //Register for client class as Voice to ADSPPM.
    class_type = MMPM_VOICE_CLIENT_CLASS;
    param_cfg.pParamConfig = (void *)&class_type;
    param_cfg.paramId = MMPM_PARAM_ID_CLIENT_CLASS;
    status = MMPM_SetParameter( mvm_ccm_mmpm_client_id, &param_cfg );
    if(status != MMPM_STATUS_SUCCESS)
    {
      MSG_2(MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_mmpm_register(): MMPM_SetParameter class type failed "
            "for client id %lu with status %lu ",
            mvm_ccm_mmpm_client_id, (uint32_t)status);
      rc = APR_EFAILED;
      break;
    }
    else
    {
      MSG_1(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_mmpm_register(): MMPM_SetParameter class type success "
            "for client id %lu", mvm_ccm_mmpm_client_id);
    }

    //Register for DCVS up only
    //CVD's core/bus votes are subject to automatic up adjustment by DCVS
    dcvs_participation.enable = TRUE;
    dcvs_participation.enableOpt = MMPM_DCVS_ADJUST_ONLY_UP;
    param_cfg.pParamConfig = (void*)&dcvs_participation;
    param_cfg.paramId = MMPM_PARAM_ID_DCVS_PARTICIPATION;
    status = MMPM_SetParameter( mvm_ccm_mmpm_client_id, &param_cfg );
    if(status != MMPM_STATUS_SUCCESS)
    {
      MSG_2(MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_mmpm_register(): MMPM_SetParameter DCVS participation for "
                                             "Only UP failed for client id %lu with status %lu ",
                                             mvm_ccm_mmpm_client_id, (uint32_t)status);
      rc = APR_EFAILED;
      break;
    }
    else
    {
      MSG_1(MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_mmpm_register(): MMPM_SetParameter DCVS participation for Only UP"
                                            " success for client id %lu", mvm_ccm_mmpm_client_id);
    }

    break;
  }

#endif

  return rc;
}

static int32_t mvm_ccm_mmpm_deregister ( void )
{

#if ( ADSPPM_INTEGRATION == 1 )

  MMPM_STATUS mmpm_rc;

  mmpm_rc = MMPM_Deregister_Ext( mvm_ccm_mmpm_client_id );
  if ( mmpm_rc != MMPM_STATUS_SUCCESS )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_mmpm_deregister(): Failed to de-register with MMPM for "
           "client ID %d, mmpm_rc = %d ", mvm_ccm_mmpm_client_id, mmpm_rc );

    return APR_EFAILED;
  }

#endif

  return APR_EOK;
}

static int32_t mvm_ccm_mmpm_request (
  MmpmMppsReqType* mpps,
  MmpmGenBwReqType* bw,
  uint32_t sleep_latency_us
)
{
  int32_t rc = APR_EOK;

#if ( ADSPPM_INTEGRATION == 1 )

  uint32_t bw_req_count;
  MMPM_STATUS mmpm_global_rc;
  MMPM_STATUS mmpm_individual_rc[ MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V ];
  MmpmRscParamType rsc_param[ MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V ];
  MmpmRscExtParamType request_param;

  if ( ( mpps == NULL ) || ( bw == NULL ) )
  {
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  rsc_param[ 0 ].rscId = MMPM_RSC_ID_MPPS;
  rsc_param[ 0 ].rscParam.pMppsReq = mpps;

  rsc_param[ 1 ].rscId = MMPM_RSC_ID_GENERIC_BW_EXT;
  rsc_param[ 1 ].rscParam.pGenBwReq = bw;

  rsc_param[ 2 ].rscId = MMPM_RSC_ID_SLEEP_LATENCY;
  rsc_param[ 2 ].rscParam.sleepMicroSec = sleep_latency_us;

  request_param.apiType = MMPM_API_TYPE_ASYNC;
  request_param.numOfReq = MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V;
  request_param.pReqArray = rsc_param;
  request_param.pStsArray = mmpm_individual_rc;
  request_param.reqTag = MVM_CCM_MMPM_CLK_REQUEST_TAG;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "mvm_ccm_mmpm_request(): Total MPPS = %d, Core Floor Clock = %d, "
         "Num of BW req = %d", mpps->mppsTotal, mpps->adspFloorClock,
         bw->numOfBw );

  for ( bw_req_count = 0; bw_req_count < bw->numOfBw; ++bw_req_count )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "mvm_ccm_mmpm_request(): BW req master port = %d, Slave port = %d",
           bw->pBandWidthArray[bw_req_count].busRoute.masterPort,
           bw->pBandWidthArray[ bw_req_count ].busRoute.slavePort );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "mvm_ccm_mmpm_request(): Bytes per sec = %d, Usage percentage = %d, "
           "Usage type = %d", bw->pBandWidthArray[bw_req_count].bwValue.busBwValue.bwBytePerSec,
           bw->pBandWidthArray[bw_req_count].bwValue.busBwValue.usagePercentage,
           bw->pBandWidthArray[ bw_req_count ].bwValue.busBwValue.usageType );
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "mvm_ccm_mmpm_request(): Sleep latency = %d", sleep_latency_us );

  mmpm_global_rc = MMPM_Request_Ext( mvm_ccm_mmpm_client_id, &request_param );
  if ( mmpm_global_rc != MMPM_STATUS_SUCCESS )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_mmpm_request(): MMPM request failed. MIPS req rc = %d, "
           "BW req rc = %d, Sleep latency req rc = %d", mmpm_individual_rc[0],
           mmpm_individual_rc[1], mmpm_individual_rc[ 2 ] );

    rc = APR_EFAILED;
  }

#endif

  return rc;
}

static int32_t mvm_ccm_mmpm_release ( void )
{
  int32_t rc = APR_EOK;

#if ( ADSPPM_INTEGRATION == 1 )

  MMPM_STATUS mmpm_global_rc;
  MMPM_STATUS mmpm_individual_rc[ MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V ];
  MmpmRscParamType rsc_param[ MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V ];
  MmpmRscExtParamType release_param;

  /* MMPM release only requires resource IDs to be indicated, not the resource
   * values such as MIPS number. All the resources under the resource IDs will
   * be released.
   */
  rsc_param[ 0 ].rscId = MMPM_RSC_ID_MPPS;
  rsc_param[ 1 ].rscId = MMPM_RSC_ID_GENERIC_BW_EXT;
  rsc_param[ 2 ].rscId = MMPM_RSC_ID_SLEEP_LATENCY;

  release_param.apiType = MMPM_API_TYPE_ASYNC;
  release_param.numOfReq = MVM_CCM_NUM_RSCS_PER_MMPM_REQUEST_V;
  release_param.pReqArray = rsc_param;
  release_param.pStsArray = mmpm_individual_rc;
  release_param.reqTag = MVM_CCM_MMPM_CLK_RELEASE_TAG;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
       "mvm_ccm_mmpm_release(): Releasing MMPM resources." );

  mmpm_global_rc = MMPM_Release_Ext( mvm_ccm_mmpm_client_id, &release_param );
  if ( mmpm_global_rc != MMPM_STATUS_SUCCESS )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_mmpm_release(): MMPM release failed. MIPS release rc = %d, "
           "BW release rc = %d, Sleep release rc = %d", mmpm_individual_rc[0],
           mmpm_individual_rc[1], mmpm_individual_rc[ 2 ] );

    rc = APR_EFAILED;
  }

#endif

  return rc;
}

static int32_t mvm_ccm_mmpm_get_max_clock_info( void )
{
  int32_t rc = APR_EOK;
  npa_query_status sts =  NPA_QUERY_SUCCESS;
  npa_query_type query_result;

  sts = npa_query_by_name( "/clk/cpu", NPA_QUERY_RESOURCE_MAX, &query_result );
  if ( sts == NPA_QUERY_SUCCESS )
  {
    mvm_ccm_max_mips_value = query_result.data.state/1000;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "npa_query_by_name(): success mvm_ccm_max_mips_value=%d",
           mvm_ccm_max_mips_value );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "npa_query_by_name(): Failed" );

    /* Setting max mips as 600 in case the NPA query fails. */
    mvm_ccm_max_mips_value = 600;
    rc = APR_EFAILED;
  }

  return rc;
}

static uint32_t mvm_ccm_get_cpu_clock_value( uint32_t option )
{
  npa_query_status status =  NPA_QUERY_SUCCESS;
  npa_query_type result;

  status = npa_query_by_name( "/clk/cpu", option, &result );
  if ( status != NPA_QUERY_SUCCESS )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_get_cpu_clock_value(): Failed" );
    return 0;
  }
  return result.data.value;
}

static int32_t mvm_ccm_get_clock_level_info( void )
{
  int32_t rc = APR_EOK;
  uint32_t num_clk_perf_levels;
  uint32_t clk_perf_level;
  uint32_t i;

  num_clk_perf_levels = mvm_ccm_get_cpu_clock_value( CLOCK_NPA_QUERY_NUM_PERF_LEVELS );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvm_ccm_get_clock_level_info(): num_clk_perf_levels = %d", num_clk_perf_levels );

  if ( !num_clk_perf_levels || num_clk_perf_levels > MVM_CCM_MAX_NUM_CLK_PERF_LEVELS )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_get_clock_level_info() failed: num_clk_perf_levels = %d, max = %d",
           num_clk_perf_levels, MVM_CCM_MAX_NUM_CLK_PERF_LEVELS );
    return APR_EFAILED;
  }

  for ( i = 0; i < num_clk_perf_levels; i++)
  {
    clk_perf_level = mvm_ccm_get_cpu_clock_value( (CLOCK_NPA_QUERY_PERF_LEVEL_KHZ+i) );
    if ( !clk_perf_level )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_get_clock_level_info(): Failed to get clock value" );
      return APR_EFAILED;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "mvm_ccm_get_clock_level_info(): CLOCK_NPA_QUERY_PERF_LEVEL_KHZ[%d] = %d", i, clk_perf_level );
    mvm_ccm_system_config.clk_perf_level[i] = clk_perf_level;
  }

  mvm_ccm_system_config.num_clk_perf_levels = num_clk_perf_levels;

  return rc;
}

#endif /* !WINSIM */

static int32_t mvm_ccm_update_system_config (
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj,
  bool_t is_addition
)
{
  int32_t rc = APR_EOK;
  mvm_ccm_active_mvm_session_tracking_list_item_t* session_tracking_list_item;
#ifndef WINSIM
  cvd_devcfg_parser_voice_use_case_t use_case;
  cvd_devcfg_parser_clock_and_bus_config_t clock_and_bus_config;
#endif

  if ( session_tracking_obj == NULL )
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  if ( is_addition )
  { /* Add the session configurations into the overall system configurations. */
    mvm_ccm_system_config.num_voice_sessions += 1;

    mvm_ccm_system_config.num_nb_streams +=
      session_tracking_obj->num_nb_streams;

    mvm_ccm_system_config.num_wb_streams +=
      session_tracking_obj->num_wb_streams;

    mvm_ccm_system_config.num_swb_streams +=
      session_tracking_obj->num_swb_streams;

    mvm_ccm_system_config.num_fb_plus_streams +=
      session_tracking_obj->num_fb_plus_streams;

    mvm_ccm_system_config.num_nb_vocprocs +=
      session_tracking_obj->num_nb_vocprocs;

    mvm_ccm_system_config.num_wb_vocprocs +=
      session_tracking_obj->num_wb_vocprocs;

    mvm_ccm_system_config.num_swb_vocprocs +=
      session_tracking_obj->num_swb_vocprocs;

    mvm_ccm_system_config.num_fb_plus_vocprocs +=
      session_tracking_obj->num_fb_plus_vocprocs;

    mvm_ccm_system_config.total_kpps +=
      session_tracking_obj->total_kpps;
  }
  else
  { /* Remove the session configurations from the overall system configurations. */
    mvm_ccm_system_config.num_voice_sessions -= 1;

    mvm_ccm_system_config.num_nb_streams -=
      session_tracking_obj->num_nb_streams;

    mvm_ccm_system_config.num_wb_streams -=
      session_tracking_obj->num_wb_streams;

    mvm_ccm_system_config.num_swb_streams -=
      session_tracking_obj->num_swb_streams;

    mvm_ccm_system_config.num_fb_plus_streams -=
      session_tracking_obj->num_fb_plus_streams;

    mvm_ccm_system_config.num_nb_vocprocs -=
      session_tracking_obj->num_nb_vocprocs;

    mvm_ccm_system_config.num_wb_vocprocs -=
      session_tracking_obj->num_wb_vocprocs;

    mvm_ccm_system_config.num_swb_vocprocs -=
      session_tracking_obj->num_swb_vocprocs;

    mvm_ccm_system_config.num_fb_plus_vocprocs -=
      session_tracking_obj->num_fb_plus_vocprocs;

    mvm_ccm_system_config.total_kpps -=
      session_tracking_obj->total_kpps;
  }

  /* Note if there are multiple active MVM sessions, the topology IDs, and
   * vfr_mode are don't cares.
   */
  if ( mvm_ccm_system_config.num_voice_sessions == 1 )
  {
    rc = apr_list_peak_head(
           &mvm_ccm_active_mvm_session_tracking_used_q,
           ( ( apr_list_node_t** ) &session_tracking_list_item ) );
    MVM_CCM_PANIC_ON_ERROR( rc );

    mvm_ccm_system_config.tx_topology_id =
      session_tracking_list_item->session_tracking_obj->tx_topology_id;

    mvm_ccm_system_config.rx_topology_id =
      session_tracking_list_item->session_tracking_obj->rx_topology_id;

    mvm_ccm_system_config.vfr_mode =
      session_tracking_list_item->session_tracking_obj->vfr_mode;

    mvm_ccm_system_config.media_id =
      session_tracking_list_item->session_tracking_obj->media_id;

    mvm_ccm_system_config.tx_num_channels =
      session_tracking_list_item->session_tracking_obj->tx_num_channels;
    mvm_ccm_system_config.tx_mpps_scale_factor =
    session_tracking_list_item->session_tracking_obj->tx_mpps_scale_factor;
    mvm_ccm_system_config.tx_bw_scale_factor =
    session_tracking_list_item->session_tracking_obj->tx_bw_scale_factor;
    mvm_ccm_system_config.rx_mpps_scale_factor =
    session_tracking_list_item->session_tracking_obj->rx_mpps_scale_factor;
    mvm_ccm_system_config.rx_bw_scale_factor =
    session_tracking_list_item->session_tracking_obj->rx_bw_scale_factor;
  }

  for ( ;; )
  {
    if ( mvm_ccm_system_config.num_voice_sessions == 0 )
    {
      rc = APR_EOK;
      break;
    }
#if defined( WINSIM )
    /* Using a fake total_core_kpps value on simulator. */
    mvm_ccm_system_config.total_core_kpps = 100000;
	rc = APR_EOK;
#else
    {  /* Get the clock and bus configuration from device config based on the
        * current use case.
        */
      use_case.num_voice_sessions = mvm_ccm_system_config.num_voice_sessions;
      use_case.num_nb_streams = mvm_ccm_system_config.num_nb_streams;
      use_case.num_wb_streams = mvm_ccm_system_config.num_wb_streams;
      use_case.num_swb_streams = mvm_ccm_system_config.num_swb_streams;
      use_case.num_fb_plus_streams = mvm_ccm_system_config.num_fb_plus_streams;
      use_case.num_nb_vocprocs = mvm_ccm_system_config.num_nb_vocprocs;
      use_case.num_wb_vocprocs = mvm_ccm_system_config.num_wb_vocprocs;
      use_case.num_swb_vocprocs = mvm_ccm_system_config.num_swb_vocprocs;
      use_case.num_fb_plus_vocprocs = mvm_ccm_system_config.num_fb_plus_vocprocs;
      use_case.tx_topology_id = mvm_ccm_system_config.tx_topology_id;
      use_case.rx_topology_id = mvm_ccm_system_config.rx_topology_id;
      use_case.vfr_mode = mvm_ccm_system_config.vfr_mode;
      use_case.media_id = mvm_ccm_system_config.media_id;

      if ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.tx_topology_id ) )
      {
        /* Replace with equivalent internal topologies */
        if ( 1 == mvm_ccm_system_config.tx_num_channels )
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS;
        }
        else if ( 2 == mvm_ccm_system_config.tx_num_channels )
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCEV5;
        }
        else
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_QM_FLUENCE_PROV2;
        }
      }

      if ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.rx_topology_id ) )
      {
        /* Replace with equivalent internal topologies */
        use_case.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_RX_DEFAULT;
      }

      rc = cvd_devcfg_parser_get_clock_and_bus_config(
             &use_case, &clock_and_bus_config );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_update_system_config():"
             " Cannot find clock and bus configuration for usecase rc = 0x%08X", rc );
        break;
      }
      if ( ( mvm_ccm_system_config.num_voice_sessions == 1 ) &&
           ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.tx_topology_id ) ||
           mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.rx_topology_id ) ) )
      {
        /* Update clock voting for custom topology */
        rc = mvm_ccm_get_custom_topo_clock_and_bus_config(
             &mvm_ccm_system_config, &clock_and_bus_config );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_update_system_config():"
             " Cannot find clock and bus configuration for custom topology usecase rc = 0x%08X", rc );
          break;
        }
      }

      mvm_ccm_system_config.total_core_kpps =
        ( clock_and_bus_config.core_floor_clock_hz /
          ( clock_and_bus_config.cycles_per_thousand_instr_packets ) );
    }
#endif
    break;
  }
  return rc;
}

static void mvm_ccm_broadcast_configuration( void )
{
  int32_t rc;
  vss_iccm_evt_active_sessions_t evt_active_sessions;
  mvm_ccm_active_mvm_session_tracking_list_item_t* session_tracking_list_item;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj;

  /* Broadcast the system configuration to all active MVM sessions. */
  evt_active_sessions.num_voice_sessions = mvm_ccm_system_config.num_voice_sessions;
  evt_active_sessions.num_nb_streams = mvm_ccm_system_config.num_nb_streams;
  evt_active_sessions.num_wb_streams = mvm_ccm_system_config.num_wb_streams;
  evt_active_sessions.num_swb_streams = mvm_ccm_system_config.num_swb_streams;
  evt_active_sessions.num_fb_plus_streams = mvm_ccm_system_config.num_fb_plus_streams;
  evt_active_sessions.num_nb_vocprocs = mvm_ccm_system_config.num_nb_vocprocs;
  evt_active_sessions.num_wb_vocprocs = mvm_ccm_system_config.num_wb_vocprocs;
  evt_active_sessions.num_swb_vocprocs = mvm_ccm_system_config.num_swb_vocprocs;
  evt_active_sessions.num_fb_plus_vocprocs = mvm_ccm_system_config.num_fb_plus_vocprocs;
  evt_active_sessions.total_core_kpps = mvm_ccm_system_config.total_core_kpps;

  session_tracking_list_item =
    ( ( mvm_ccm_active_mvm_session_tracking_list_item_t* )
      &mvm_ccm_active_mvm_session_tracking_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &mvm_ccm_active_mvm_session_tracking_used_q,
                            ( ( apr_list_node_t* ) session_tracking_list_item ),
                            ( ( apr_list_node_t** ) &session_tracking_list_item ) );
    if ( rc ) break;

    session_tracking_obj = session_tracking_list_item->session_tracking_obj;

    {
      rc = __aprv2_cmd_alloc_send(
               mvm_ccm_mvm_service_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               mvm_ccm_mvm_service_apr_addr, APR_NULL_V,
               mvm_ccm_mvm_service_apr_addr, session_tracking_obj->handle,
               0, VSS_ICCM_EVT_ACTIVE_SESSIONS,
               &evt_active_sessions, sizeof( evt_active_sessions ) );
      MVM_CCM_PANIC_ON_ERROR( rc );
    }
  }

}

static int32_t mvm_ccm_commit_system_config ( void )
{
  int32_t rc;
#ifndef WINSIM
  uint32_t total_million_packet_per_second;
  cvd_devcfg_parser_voice_use_case_t use_case;
  cvd_devcfg_parser_clock_and_bus_config_t clock_and_bus_config;
  MmpmMppsReqType mpps_request;
#endif /* !WINSIM */

  for ( ;; )
  {

#if defined( WINSIM )

    if ( mvm_ccm_system_config.num_voice_sessions == 0 )
    {
      rc = APR_EOK;
      break;
    }

    /* Using a fake total_core_kpps value on simulator. */
    mvm_ccm_system_config.total_core_kpps = 100000;

#else

    if ( mvm_ccm_system_config.num_voice_sessions == 0 )
    { /* Release the MMPM resources if there are no active MVM sessions. */
      rc = mvm_ccm_mmpm_release( );
      break;
    }

    {
      /* Get the clock and bus configuration from device config based on the
       * current use case.
       */
      use_case.num_voice_sessions = mvm_ccm_system_config.num_voice_sessions;
      use_case.num_nb_streams = mvm_ccm_system_config.num_nb_streams;
      use_case.num_wb_streams = mvm_ccm_system_config.num_wb_streams;
      use_case.num_swb_streams = mvm_ccm_system_config.num_swb_streams;
      use_case.num_fb_plus_streams = mvm_ccm_system_config.num_fb_plus_streams;
      use_case.num_nb_vocprocs = mvm_ccm_system_config.num_nb_vocprocs;
      use_case.num_wb_vocprocs = mvm_ccm_system_config.num_wb_vocprocs;
      use_case.num_swb_vocprocs = mvm_ccm_system_config.num_swb_vocprocs;
      use_case.num_fb_plus_vocprocs = mvm_ccm_system_config.num_fb_plus_vocprocs;
      use_case.tx_topology_id = mvm_ccm_system_config.tx_topology_id;
      use_case.rx_topology_id = mvm_ccm_system_config.rx_topology_id;
      use_case.media_id = mvm_ccm_system_config.media_id;
      use_case.vfr_mode = mvm_ccm_system_config.vfr_mode;

      if ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.tx_topology_id ) )
      {
        /* Replace with equivalent internal topologies */
        if( 1 == mvm_ccm_system_config.tx_num_channels )
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS;
        }
        else if ( 2 == mvm_ccm_system_config.tx_num_channels )
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCEV5;
        }
        else
        {
          use_case.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_TX_QM_FLUENCE_PROV2;
        }
      }

      if ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.rx_topology_id ) )
      {
        /* Replace with equivalent internal topologies */
        use_case.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_RX_DEFAULT;
      }

      rc = cvd_devcfg_parser_get_clock_and_bus_config(
             &use_case, &clock_and_bus_config );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_commit_system_config():"
             " Cannot find clock and bus configuration for usecase rc = 0x%08X", rc );
        break;
      }
      if ( ( mvm_ccm_system_config.num_voice_sessions == 1 ) &&
           ( mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.tx_topology_id ) ||
           mvm_ccm_is_not_internal_topology( mvm_ccm_system_config.rx_topology_id ) ) )
      {
        /* Update clock voting for custom topology */
        rc = mvm_ccm_get_custom_topo_clock_and_bus_config(
             &mvm_ccm_system_config, &clock_and_bus_config );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_commit_system_config():"
             " Cannot find clock and bus configuration for custom topology usecase rc = 0x%08X", rc );
          break;
        }
      }
    }

    if ( mvm_ccm_clk_override_tracking_used_q.size == 0 )
    { /* Request resources from MMPM. */
      total_million_packet_per_second =
        ( ( mvm_ccm_system_config.total_kpps + 1000 - 1 ) / 1000 );

      mpps_request.mppsTotal = ( total_million_packet_per_second );
      mpps_request.adspFloorClock = ( clock_and_bus_config.core_floor_clock_hz / 1000000 );

      rc = mvm_ccm_mmpm_request(
             &mpps_request,
             &clock_and_bus_config.bw_requirement,
             clock_and_bus_config.sleep_latency_us );
      if ( rc ) break;
    }

    mvm_ccm_system_config.total_core_kpps =
      ( clock_and_bus_config.core_floor_clock_hz /
        ( clock_and_bus_config.cycles_per_thousand_instr_packets ) );

#endif /* WINSIM */

    break;
  }

  return rc;
}

static int32_t mvm_ccm_mvm_voice_session_active_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  uint16_t mvm_session_handle;
  uint32_t payload_size;
  vss_imvm_evt_voice_session_active_t* in_args;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj;

  for ( ;; )
  {
    if ( packet == NULL )
      MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );

    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
    if ( payload_size != sizeof( vss_imvm_evt_voice_session_active_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_mvm_voice_session_active_evt_processing(): Unexpected "
             "payload size, %d != %d", payload_size,
             sizeof( vss_imvm_evt_voice_session_active_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD(
                vss_imvm_evt_voice_session_active_t, packet );

    mvm_session_handle = packet->src_port;

    rc = mvm_ccm_get_active_mvm_session_tracking_obj(
           mvm_session_handle, &session_tracking_obj );
    if ( rc == APR_ENOTEXIST )
    { /* A new session is becoming active. */
      rc = mvm_ccm_create_active_mvm_session_tracking_obj( &session_tracking_obj );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_ccm_mvm_voice_session_active_evt_processing(): Failed to "
               "create session tracking obj, rc = 0x%08X", rc );
        break;
      }
      checkpoint = 1;

      { /* Track this session's configurations. */
        session_tracking_obj->handle = mvm_session_handle;
        session_tracking_obj->num_nb_streams = in_args->num_nb_streams;
        session_tracking_obj->num_wb_streams = in_args->num_wb_streams;
        session_tracking_obj->num_swb_streams = in_args->num_swb_streams;
        session_tracking_obj->num_fb_plus_streams = in_args->num_fb_plus_streams;
        session_tracking_obj->num_nb_vocprocs = in_args->num_nb_vocprocs;
        session_tracking_obj->num_wb_vocprocs = in_args->num_wb_vocprocs;
        session_tracking_obj->num_swb_vocprocs = in_args->num_swb_vocprocs;
        session_tracking_obj->num_fb_plus_vocprocs = in_args->num_fb_plus_vocprocs;
        session_tracking_obj->total_kpps = in_args->total_kpps;
        session_tracking_obj->tx_topology_id = in_args->tx_topology_id;
        session_tracking_obj->rx_topology_id = in_args->rx_topology_id;
        session_tracking_obj->media_id = in_args->media_id;
        session_tracking_obj->vfr_mode = in_args->vfr_mode;
        session_tracking_obj->tx_num_channels = in_args->tx_num_channels;
        session_tracking_obj->tx_mpps_scale_factor =
            in_args->tx_mpps_scale_factor;
        session_tracking_obj->tx_bw_scale_factor =
            in_args->tx_bw_scale_factor;
        session_tracking_obj->rx_mpps_scale_factor =
            in_args->rx_mpps_scale_factor;
        session_tracking_obj->rx_bw_scale_factor =
            in_args->rx_bw_scale_factor;
      }

      rc = mvm_ccm_queue_active_mvm_session_tracking_obj( session_tracking_obj );
      MVM_CCM_PANIC_ON_ERROR( rc );
      checkpoint = 2;

      rc = mvm_ccm_update_system_config( session_tracking_obj, TRUE );
      MVM_CCM_PANIC_ON_ERROR( rc );
    }
    else
    { /* Configuration changes on an existing active session. */

      { /* Remove the existing session configurations from the system. */
        rc = mvm_ccm_update_system_config( session_tracking_obj, FALSE );
        MVM_CCM_PANIC_ON_ERROR( rc );
      }

      { /* Update this session's configurations. */
        session_tracking_obj->num_nb_streams = in_args->num_nb_streams;
        session_tracking_obj->num_wb_streams = in_args->num_wb_streams;
        session_tracking_obj->num_swb_streams = in_args->num_swb_streams;
        session_tracking_obj->num_fb_plus_streams = in_args->num_fb_plus_streams;
        session_tracking_obj->num_nb_vocprocs = in_args->num_nb_vocprocs;
        session_tracking_obj->num_wb_vocprocs = in_args->num_wb_vocprocs;
        session_tracking_obj->num_swb_vocprocs = in_args->num_swb_vocprocs;
        session_tracking_obj->num_fb_plus_vocprocs = in_args->num_fb_plus_vocprocs;
        session_tracking_obj->total_kpps = in_args->total_kpps;
        session_tracking_obj->tx_topology_id = in_args->tx_topology_id;
        session_tracking_obj->rx_topology_id = in_args->rx_topology_id;
        session_tracking_obj->media_id = in_args->media_id;
        session_tracking_obj->vfr_mode = in_args->vfr_mode;
        session_tracking_obj->tx_num_channels = in_args->tx_num_channels;
        session_tracking_obj->tx_mpps_scale_factor =
            in_args->tx_mpps_scale_factor;
        session_tracking_obj->tx_bw_scale_factor =
            in_args->tx_bw_scale_factor;
        session_tracking_obj->rx_mpps_scale_factor =
            in_args->rx_mpps_scale_factor;
        session_tracking_obj->rx_bw_scale_factor =
            in_args->rx_bw_scale_factor;
      }

      rc = mvm_ccm_update_system_config( session_tracking_obj, TRUE );
      MVM_CCM_PANIC_ON_ERROR( rc );
    }

    mvm_ccm_clk_state_ctrl( MVM_CCM_CLK_EVENT_SET_SYSTEM_CONFIG );
    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 2:
    ( void ) mvm_ccm_dequeue_active_mvm_session_tracking_obj( session_tracking_obj );
    /*-fallthru */

  case 1:
    ( void ) mvm_ccm_destroy_active_mvm_session_tracking_obj( session_tracking_obj );
    /*-fallthru */

  default:
    break;
  }

  return rc;
}

static int32_t mvm_ccm_mvm_voice_session_inactive_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint16_t mvm_session_handle;
  mvm_ccm_active_mvm_session_tracking_obj_t* session_tracking_obj = NULL;

  for ( ;; )
  {
    mvm_session_handle = packet->src_port;

    rc = mvm_ccm_get_active_mvm_session_tracking_obj(
           mvm_session_handle, &session_tracking_obj );
    if ( rc == APR_ENOTEXIST )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_mvm_voice_session_inactive_evt_processing(): Cannot find "
             "session tracking obj from MVM session handle 0x%08X",
             mvm_session_handle );
      rc = APR_EBADPARAM;
      break;
    }

    rc = mvm_ccm_update_system_config( session_tracking_obj, FALSE );
    MVM_CCM_PANIC_ON_ERROR( rc );

    rc = mvm_ccm_dequeue_active_mvm_session_tracking_obj( session_tracking_obj );
    MVM_CCM_PANIC_ON_ERROR( rc );

    rc = mvm_ccm_destroy_active_mvm_session_tracking_obj( session_tracking_obj );
    MVM_CCM_PANIC_ON_ERROR( rc );

    mvm_ccm_clk_state_ctrl( MVM_CCM_CLK_EVENT_SET_SYSTEM_CONFIG );

      break;
    }

  return rc;
}

static int32_t mvm_ccm_override_clk ( void )
{
  int32_t rc = APR_EOK;

#if ( ADSPPM_INTEGRATION == 1 )
  MMPM_STATUS mmpm_global_rc;
  uint32_t numOfReq = 0;

  MmpmRscExtParamType request_param;
  MMPM_STATUS mmpm_individual_rc[ 2 ];
  MmpmRscParamType rsc_param[ 2 ];

  MmpmMppsReqType mmpmMppsParam;
  uint32_t mmpm_cpp;
  uint32_t max_q6_core_clock;

  MmpmGenBwReqType MmpmBwParams;
  MmpmGenBwValType bw;

  /* Default values will be used if getting values failed. */
  ( void ) cvd_devcfg_parser_get_mmpm_cpp( &mmpm_cpp );
  ( void ) cvd_devcfg_parser_get_max_q6_core_clock( &max_q6_core_clock );

  /* Setting MPPS requirement to zero and voting for max Q6 core clock */
  mmpmMppsParam.mppsTotal = 0;
  mmpmMppsParam.adspFloorClock = max_q6_core_clock;

  rsc_param[ numOfReq ].rscId = MMPM_RSC_ID_MPPS;
  rsc_param[ numOfReq ].rscParam.pMppsReq = &mmpmMppsParam;
  numOfReq++;

  ( void ) cvd_devcfg_parser_get_max_bw( &bw );
  MmpmBwParams.numOfBw = 1;
  MmpmBwParams.pBandWidthArray = &bw;

  rsc_param[ numOfReq ].rscId = MMPM_RSC_ID_GENERIC_BW_EXT;
  rsc_param[ numOfReq ].rscParam.pGenBwReq = &MmpmBwParams;
  numOfReq++;

  request_param.apiType = MMPM_API_TYPE_ASYNC;
  request_param.numOfReq = numOfReq;
  request_param.pReqArray = rsc_param;
  request_param.pStsArray = mmpm_individual_rc;
  request_param.reqTag = MVM_CCM_MMPM_CLK_OVERRIDE_TAG;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvm_ccm_override_clk: Total MPPS = %d, ADSP Floor Clock = %d ",
         mmpmMppsParam.mppsTotal, mmpmMppsParam.adspFloorClock );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvm_ccm_override_clk: Inst. BW = %d, Average Bw Usage = %d percent",
         bw.bwValue.busBwValue.bwBytePerSec,
         bw.bwValue.busBwValue.usagePercentage );

  mmpm_global_rc = MMPM_Request_Ext( mvm_ccm_mmpm_client_id, &request_param );
  if ( mmpm_global_rc != MMPM_STATUS_SUCCESS )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_ccm_override_clk(): MMPM request failed. MPPS req rc = %d ",
           mmpm_individual_rc[ 0 ] );
    rc = APR_EFAILED;
  }  

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvm_ccm_override_clk: Status array, Rsc_req1 = %d, Res_req2 = %d ", 
         mmpm_individual_rc[0], mmpm_individual_rc[1] );

#endif

  return rc;
}

static int32_t mvm_ccm_restore_clk (
  aprv2_packet_t* packet
)
{
  if ( packet == NULL )
  {
    MVM_CCM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "mvm_ccm_restore_clk: mvm_session 0x%x, num_override_requests %d",
         packet->src_port, mvm_ccm_clk_override_tracking_used_q.size );

  ( void )mvm_ccm_dequeue_clk_override_request( packet->src_port );

  if ( mvm_ccm_clk_override_tracking_used_q.size == 0 )
  {
    mvm_ccm_clk_state_ctrl( MVM_CCM_CLK_EVENT_RESET_OVERRIDE );
  }

  return APR_EOK;
}

APR_INTERNAL int32_t mvm_ccm_init (
  uint32_t mvm_service_apr_handle,
  uint16_t mvm_service_apr_addr
)
{
  uint32_t session_count;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "====== mvm_ccm_init()======" );

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap(
      &mvm_ccm_heapmgr, ( ( void* ) &mvm_ccm_heap_pool ),
      sizeof( mvm_ccm_heap_pool ), NULL, NULL );
      /**< Note that locking is not required since the custom heap is always
           used in the MVM TASK context. */
  }

  { /* Initialize the active MVM session tracking lists. */
    ( void ) apr_list_init_v2(
               &mvm_ccm_active_mvm_session_tracking_free_q, NULL, NULL );
      /**< Note that locking is not required since the list is always used in
           the MVM TASK context. */

    for ( session_count = 0; session_count < MVM_CCM_MAX_ACTIVE_MVM_SESSIONS_V; ++session_count )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &mvm_ccm_active_mvm_session_tracking_list_item_pool[ session_count ] );
      ( void ) apr_list_add_tail(
                 &mvm_ccm_active_mvm_session_tracking_free_q,
                 ( ( apr_list_node_t* )
                   &mvm_ccm_active_mvm_session_tracking_list_item_pool[ session_count ] ) );
    }

    ( void ) apr_list_init_v2( &mvm_ccm_active_mvm_session_tracking_used_q,
                               NULL, NULL );
      /**< Note that locking is not required since the list is always used in
           the MVM TASK context. */
  }

  { /* Initialize the clk override session tracking lists. */
    ( void ) apr_list_init_v2(
               &mvm_ccm_clk_override_tracking_free_q, NULL, NULL );
      /**< Note that locking is not required since the list is always used in
           the MVM TASK context. */

    for ( session_count = 0; session_count < MVM_CCM_MAX_ACTIVE_MVM_SESSIONS_V; ++session_count )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &mvm_ccm_clk_override_tracking_list_item_pool[ session_count ] );
      ( void ) apr_list_add_tail(
                 &mvm_ccm_clk_override_tracking_free_q,
                 ( ( apr_list_node_t* )
                 &mvm_ccm_clk_override_tracking_list_item_pool[ session_count ] ) );
    }

    ( void ) apr_list_init_v2( &mvm_ccm_clk_override_tracking_used_q,
                               NULL, NULL );
    /**< Note that locking is not required since the list is always used in
         the MVM TASK context. */
  }

  { /* Initialize the system configuration. */
    uint32_t i;
    mvm_ccm_system_config.num_voice_sessions = 0;
    mvm_ccm_system_config.num_nb_streams = 0;
    mvm_ccm_system_config.num_wb_streams = 0;
    mvm_ccm_system_config.num_swb_streams = 0;
    mvm_ccm_system_config.num_fb_plus_streams = 0;
    mvm_ccm_system_config.num_nb_vocprocs = 0;
    mvm_ccm_system_config.num_wb_vocprocs = 0;
    mvm_ccm_system_config.num_swb_vocprocs = 0;
    mvm_ccm_system_config.num_fb_plus_vocprocs = 0;
    mvm_ccm_system_config.total_kpps = 0;
    mvm_ccm_system_config.total_core_kpps = 0;
    mvm_ccm_system_config.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    mvm_ccm_system_config.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    mvm_ccm_system_config.media_id = VSS_MEDIA_ID_NONE;
    mvm_ccm_system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
    mvm_ccm_system_config.num_clk_perf_levels = 0;
    for ( i = 0; i < MVM_CCM_MAX_NUM_CLK_PERF_LEVELS; i++)
    {
      mvm_ccm_system_config.clk_perf_level[i] = 0;
    }
    mvm_ccm_system_config.tx_mpps_scale_factor = 0;
    mvm_ccm_system_config.tx_bw_scale_factor = 0;
    mvm_ccm_system_config.rx_mpps_scale_factor = 0;
    mvm_ccm_system_config.rx_bw_scale_factor = 0;
  }

  { /* Store the MVM service APR handle and address. */
    mvm_ccm_mvm_service_apr_handle = mvm_service_apr_handle;
    mvm_ccm_mvm_service_apr_addr = mvm_service_apr_addr;
  }

  /* Initialize clock control state variables */
  mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_IDLE;
  mvm_ccm_clk_state.pending_config_flag = 0;
  mvm_ccm_clk_state.pending_override_flag = 0;
  mvm_ccm_clk_state.pending_reset_override_flag = 0;
  mvm_ccm_clk_state.override_flag = 0;
  ( void )apr_lock_create( APR_LOCK_TYPE_MUTEX, &mvm_ccm_clk_state.lock );

#ifndef WINSIM
  {  /* Register with MMPM. */
    ( void ) mvm_ccm_mmpm_register( );

    ( void ) mvm_ccm_mmpm_get_max_clock_info();
  }

  mvm_ccm_get_clock_level_info();

#endif /* !WINSIM */

  return APR_EOK;
}

APR_INTERNAL int32_t mvm_ccm_deinit ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,  "====== mvm_ccm_deinit()======" );

#ifndef WINSIM

  /* Deregister with MMPM. */
  ( void ) mvm_ccm_mmpm_deregister( );

#endif /* !WINSIM */

  { /* Release the active MVM session tracking lists. */
    ( void ) apr_list_destroy( &mvm_ccm_active_mvm_session_tracking_used_q );
    ( void ) apr_list_destroy( &mvm_ccm_active_mvm_session_tracking_free_q );

    /* Release the clk override MVM session tracking lists. */
    ( void ) apr_list_destroy( &mvm_ccm_clk_override_tracking_used_q );
    ( void ) apr_list_destroy( &mvm_ccm_clk_override_tracking_free_q );

    mvm_ccm_clk_state_ctrl ( MVM_CCM_CLK_EVENT_ABORT );
    ( void ) apr_lock_destroy( mvm_ccm_clk_state.lock );
  }

  return APR_EOK;
}

APR_INTERNAL int32_t mvm_ccm_process_evt (
  aprv2_packet_t* packet
)
{
  int32_t rc;

  if ( packet == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "mvm_ccm_process_evt(): Input event packet is NULL." );
    return APR_EBADPARAM;
  }

  switch ( packet->opcode )
  {
  case VSS_IMVM_EVT_VOICE_SESSION_ACTIVE:
    rc = mvm_ccm_mvm_voice_session_active_evt_processing( packet );
    break;

  case VSS_IMVM_EVT_VOICE_SESSION_INACTIVE:
    rc = mvm_ccm_mvm_voice_session_inactive_evt_processing( packet );
    break;

  default:
    { /* Handle error. */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_process_evt(): Unsupported event opcode 0x%08X, "
             "dropping it.", packet->opcode );
      rc = APR_EUNSUPPORTED;
    }
    break;
  }

  ( void ) __aprv2_cmd_free( mvm_ccm_mvm_service_apr_handle, packet );

  return rc;
}

APR_INTERNAL int32_t mvm_ccm_process_cmd (
  aprv2_packet_t* packet
)
{
  int32_t rc;

  if ( packet == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "mvm_ccm_process_evt(): Input command packet is NULL." );
    return APR_EBADPARAM;
  }

  switch ( packet->opcode )
  {
  case VSS_IMVM_CMD_SET_MAX_CLOCK:
    {
      if(mvm_ccm_clk_override_tracking_used_q.size == 0)
      {
        mvm_ccm_clk_state_ctrl ( MVM_CCM_CLK_EVENT_SET_OVERRIDE );
      }
      rc = mvm_ccm_queue_clk_override_request( packet->src_port );
    }
    break;

  case VSS_IMVM_CMD_RESET_MAX_CLOCK:
    {
    rc = mvm_ccm_restore_clk( packet );
    }
    break;

  default:
    { /* Handle error. */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_ccm_process_cmd(): Unsupported command opcode 0x%08X, "
             "dropping it.", packet->opcode );
      rc = APR_EUNSUPPORTED;
    }
    break;
  }

  ( void ) __aprv2_cmd_end_command( mvm_ccm_mvm_service_apr_handle, packet, rc );

  return APR_EOK;
}

static void mvm_ccm_clk_state_ctrl ( mvm_ccm_clock_event_enum_t event )
{
  int32_t rc = APR_EOK;

  apr_lock_enter( mvm_ccm_clk_state.lock );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,\
         "mvm_ccm_clk_state_ctrl: state: %d, event %d, override_flag %d",\
         mvm_ccm_clk_state.state, event, mvm_ccm_clk_state.override_flag );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,\
         "mvm_ccm_clk_state_ctrl: pending_override_flag: %d, pending_config_flag %d, "
         "pending_reset_override_flag %d",mvm_ccm_clk_state.pending_override_flag,\
         mvm_ccm_clk_state.pending_config_flag,\
         mvm_ccm_clk_state.pending_reset_override_flag );

  switch ( mvm_ccm_clk_state.state )
  {

  case MVM_CCM_CLK_CTRL_STATE_IDLE:
    {
      switch ( event )
      {
      case MVM_CCM_CLK_EVENT_SET_OVERRIDE:
        {
          if( !mvm_ccm_clk_state.override_flag )
          {
            mvm_ccm_clk_state.override_flag = 1;
            mvm_ccm_clk_state.pending_override_flag = 0;
            mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_WAIT;
            /* Request MMPM for maximum clock */
            rc = mvm_ccm_override_clk( );
          } else {
            /* 'mvm_ccm_clk_override_tracking_used_q' guards not to send override request
               while one request is already active. */
            MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_clk_state_ctrl: EVENT UNEXPECTED" );
          }
        }
        break;

      case MVM_CCM_CLK_EVENT_RESET_OVERRIDE:
        {
          /* Reset flags and fall back to usecase level clock. */
          mvm_ccm_clk_state.override_flag = 0;
          mvm_ccm_clk_state.pending_reset_override_flag = 0;
          mvm_ccm_clk_state.pending_config_flag = 0;
          mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_WAIT;

          /* Request clock as per system configuration. */
          mvm_ccm_commit_system_config( );
          mvm_ccm_broadcast_configuration( );
        }
        break;

      case MVM_CCM_CLK_EVENT_SET_SYSTEM_CONFIG:
        {
          if ( !mvm_ccm_clk_state.override_flag )
          {
            mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_WAIT;
            /* Request clock as per system configuration. */
            mvm_ccm_commit_system_config( );
          }
          else
          {
            mvm_ccm_clk_state.pending_config_flag = 1;
          }
          mvm_ccm_broadcast_configuration( );
        }
        break;

      case MVM_CCM_CLK_EVENT_MMPM_CB:
        { /* ERR: unexpected. */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_ccm_clk_state_ctrl: EVENT UNEXPECTED" );
        }
        break;

      case MVM_CCM_CLK_EVENT_ABORT:
        {
          mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_ABORT;
          mvm_ccm_clk_state.pending_config_flag = 0;
          mvm_ccm_clk_state.pending_override_flag = 0;
          mvm_ccm_clk_state.pending_reset_override_flag = 0;
          mvm_ccm_clk_state.override_flag = 0;
        }
        break;

      default:
        break;
      }
    }
    break;

  case MVM_CCM_CLK_CTRL_STATE_WAIT:
    {
      switch ( event )
      {
      case MVM_CCM_CLK_EVENT_SET_OVERRIDE:
        {
          mvm_ccm_clk_state.pending_override_flag = 1;

          /* This is required to avoid race condition between set and reset override clock requests
             while in wait WAIT state. The logic ensures only the last request remains. */
          mvm_ccm_clk_state.pending_reset_override_flag = 0;
        }
        break;

      case MVM_CCM_CLK_EVENT_SET_SYSTEM_CONFIG:
        {
          mvm_ccm_clk_state.pending_config_flag = 1;
          mvm_ccm_broadcast_configuration( );
        }
        break;

      case MVM_CCM_CLK_EVENT_RESET_OVERRIDE:
        {
          mvm_ccm_clk_state.pending_reset_override_flag = 1;

          /* This is required to avoid race condition between set and reset override clock requests
             while in WAIT state. The logic ensures only the last request remains. */
          mvm_ccm_clk_state.pending_override_flag = 0;
        }
        break;

      case MVM_CCM_CLK_EVENT_MMPM_CB:
        {
          if( mvm_ccm_clk_state.pending_override_flag )
          {
            mvm_ccm_clk_state.override_flag = 1;
            mvm_ccm_clk_state.pending_override_flag = 0;
            /* Request MMPM for maximum clock. */
            rc = mvm_ccm_override_clk( );

          }else{
            mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_IDLE;

            /* Reset to system config clock incase a new set system config cmd is pending or
             * a revert high clock cmd is pending.
             */
            if ( ( mvm_ccm_clk_state.pending_config_flag &&
                   mvm_ccm_clk_state.override_flag != 1 ) ||
                 ( mvm_ccm_clk_state.pending_reset_override_flag == 1 )
               )
            {
              /* Request clock as per system configuration. */
              rc = __aprv2_cmd_alloc_send(
                       mvm_ccm_mvm_service_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                       mvm_ccm_mvm_service_apr_addr, APR_NULL_V ,
                       mvm_ccm_mvm_service_apr_addr, APR_NULL_V,
                       APR_NULL_V, VSS_IMVM_CMD_RESET_MAX_CLOCK,
                       NULL, 0 );
              MVM_CCM_PANIC_ON_ERROR( rc );
            }
          }
        }
        break;

      case MVM_CCM_CLK_EVENT_ABORT:
        {
          mvm_ccm_clk_state.state = MVM_CCM_CLK_CTRL_STATE_ABORT;
          mvm_ccm_clk_state.pending_config_flag = 0;
          mvm_ccm_clk_state.pending_override_flag = 0;
          mvm_ccm_clk_state.override_flag = 0;
          mvm_ccm_clk_state.pending_reset_override_flag = 0;
        }
        break;

      default:
        break;
      }
    }
    break;

  case MVM_CCM_CLK_CTRL_STATE_ABORT:
  default:
    {
      mvm_ccm_clk_state.pending_config_flag = 0;
      mvm_ccm_clk_state.pending_override_flag = 0;
      mvm_ccm_clk_state.override_flag = 0;
      mvm_ccm_clk_state.pending_reset_override_flag = 0;
    }
    break;
  }

  apr_lock_leave( mvm_ccm_clk_state.lock );

}

/* Map requested clock to supported clock levels.
   Both input and output are in KHz. */
APR_INTERNAL uint32_t mvm_ccm_map_clock_to_perf_level (
  uint32_t clock_in_khz
)
{
  uint32_t i;
  uint32_t max_q6_core_clock_in_mhz;

  ( void ) cvd_devcfg_parser_get_max_q6_core_clock( &max_q6_core_clock_in_mhz );

  /* Cap to max supported clcok if input is too high. */
  if ( clock_in_khz > ( max_q6_core_clock_in_mhz * 1000 ) )
  {
    return ( max_q6_core_clock_in_mhz * 1000 );
  }

  if ( !mvm_ccm_system_config.num_clk_perf_levels )
  {
    return clock_in_khz;
  }

  for ( i = 0; i < mvm_ccm_system_config.num_clk_perf_levels; i++ )
  {
    if ( clock_in_khz <= mvm_ccm_system_config.clk_perf_level[i] )
    {
      clock_in_khz = mvm_ccm_system_config.clk_perf_level[i];
      break;
    }
  }

  return clock_in_khz;
}

#ifndef WINSIM
/* Compute clock voting for the custom topology use cases.
 */
APR_INTERNAL int32_t mvm_ccm_get_custom_topo_clock_and_bus_config (
  mvm_ccm_system_config_t* use_case,
  cvd_devcfg_parser_clock_and_bus_config_t* clock_and_bus_config
)
{
  MmpmBusBWDataUsageType *busBwValue;
  uint32_t mpps_scale_factor;
  uint32_t bw_scale_factor;
  uint32_t i;
  MmpmGenBwReqType* bw;
  uint32_t clock_in_hz;

  if ( ( use_case == NULL ) || ( clock_and_bus_config == NULL ) )
  {
    return APR_EBADPARAM;
  }
  /* Pick the maximum scale factor */
  if ( use_case->tx_mpps_scale_factor > use_case->rx_mpps_scale_factor )
    mpps_scale_factor = use_case->tx_mpps_scale_factor;
  else
    mpps_scale_factor = use_case->rx_mpps_scale_factor;

  if ( use_case->tx_bw_scale_factor > use_case->rx_bw_scale_factor )
    bw_scale_factor = use_case->tx_bw_scale_factor;
  else
    bw_scale_factor = use_case->rx_bw_scale_factor;

  if ( ( mpps_scale_factor == 0 ) || ( bw_scale_factor == 0 ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                               "No scale factors, use defaults" );
    mpps_scale_factor = MVM_CCM_DEFAULT_MPPS_SCALE_FACTOR;
    bw_scale_factor = MVM_CCM_DEFAULT_BW_SCALE_FACTOR;
  }
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                         "mpps_scale_factor %d/10, bw_scale_factor %d/10",
                                         mpps_scale_factor,
                                         bw_scale_factor );
  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                          "clock_hz %d, cycles_per_thousand_instr_packets %d, total_kpps %d",
                                          clock_and_bus_config->core_floor_clock_hz,
                                          clock_and_bus_config->cycles_per_thousand_instr_packets,
                                          mvm_ccm_system_config.total_kpps );

  /* floor_clock = total_mpps * cpp * mpps_scale_factor */
  clock_in_hz = ( mvm_ccm_system_config.total_kpps * clock_and_bus_config->cycles_per_thousand_instr_packets / 
                  MVM_CCM_SCALE_FACTOR_DIV * mpps_scale_factor );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                         "clock_hz %d before mapping", clock_in_hz );

  clock_and_bus_config->core_floor_clock_hz = mvm_ccm_map_clock_to_perf_level( clock_in_hz / 1000 ) * 1000;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                         "clock_hz %d after mapping", clock_and_bus_config->core_floor_clock_hz );

  /* bw = avg_bw * bw_scale_factor */
  bw = &clock_and_bus_config->bw_requirement;
  for ( i = 0; i < bw->numOfBw; i++ )
  {
    busBwValue = &bw->pBandWidthArray[i].bwValue.busBwValue;
    busBwValue->bwBytePerSec =
    ( busBwValue->bwBytePerSec / MVM_CCM_SCALE_FACTOR_DIV * bw_scale_factor );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                           "BW [MSB=0x%x, LSB=0x%08x] bps, usage percentage %d",
                                           ( busBwValue->bwBytePerSec >> 32 ),
                                           ( busBwValue->bwBytePerSec & ( (uint32_t) 0XFFFFFFFF ) ),
                                           busBwValue->usagePercentage );
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ccm_get_custom_topo_clock_and_bus_config(): " \
                                         "clock_hz %d, sleep_latency_us %d",
                                         clock_and_bus_config->core_floor_clock_hz,
                                         clock_and_bus_config->sleep_latency_us );
  return APR_EOK;
}
#endif /* !WINSIM */

static bool_t mvm_ccm_is_not_internal_topology( uint32_t topology_id )
{
  switch ( topology_id )
  {
    case VPM_TX_NONE:
    case VPM_TX_SM_ECNS:
    case VPM_TX_SM_FLUENCEV5:
    case VPM_TX_SM_ECNS_V2:
    case VPM_TX_DM_FLUENCE:
    case VPM_TX_DM_FLUENCEV5:
    case VPM_TX_DM_VPECNS:
    case VPM_TX_DM_FLUENCEV5_BROADSIDE:
    case VPM_TX_QM_FLUENCE_PRO:
    case VPM_TX_QM_FLUENCE_PROV2:
    case VPM_TX_SINGLE_MIC_DYNAMIC_TOPOLOGY:
    case VPM_TX_DUAL_MIC_DYNAMIC_TOPOLOGY:
    case VPM_TX_QUAD_MIC_DYNAMIC_TOPOLOGY:
    case VPM_RX_DEFAULT:
    case VPM_RX_DEFAULT_V2:
    case VPM_RX_DYNAMIC_TOPOLOGY:
      return FALSE;
    default:
      break;
  }
  return TRUE;
}
