/*
  Copyright (C) 2009-2016 QUALCOMM Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/cvs/src/cvs_module.c#1 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "msg.h"
#include "err.h"
#include "mmstd.h"
#include "log_codes.h"

#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"
#include "aprv2_api_inline.h"
#include "aprv2_msg_if.h"

#include "adsp_vparams_api.h"
#include "adsp_vcmn_api.h"

#include "vocsvc_avtimer_api.h"

#include "vccm_api.h"
#include "cvs_api_i.h"
#include "cvs_mailbox_timer_api_i.h"
#include "cvd_log_i.h"
#include "cvd_cal_log_i.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/


static char_t cvs_my_dns[] = "qcom.audio.cvs";
static uint16_t cvs_my_addr;

static char_t cvs_cvp_dns[] = "qcom.audio.cvp";
static uint16_t cvs_cvp_addr;

static char_t cvs_mvm_dns[] = "qcom.audio.mvm";
static uint16_t cvs_mvm_addr;

static char_t cvs_vsm_dns[] = "qcom.audio.vsm";
static uint16_t cvs_vsm_addr;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Lock Management */
static apr_lock_t cvs_int_lock;
static apr_lock_t cvs_med_task_lock;
static apr_lock_t cvs_low_task_lock;
static apr_lock_t cvs_ref_cnt_lock;

/* Heap Management */
static uint8_t cvs_heap_pool[ CVS_HEAP_SIZE_V ];
static apr_memmgr_type cvs_heapmgr;

/* Object Management */
static apr_objmgr_object_t cvs_object_table[ CVS_MAX_OBJECTS_V ];
static apr_objmgr_t cvs_objmgr;

/* Command Queue Management */
static cvs_work_item_t cvs_cmd_pool[ CVS_NUM_COMMANDS_V ];
static apr_list_t cvs_free_cmd_q;
static apr_list_t cvs_high_task_incoming_cmd_q;
static apr_list_t cvs_med_task_incoming_cmd_q;
static apr_list_t cvs_low_task_incoming_cmd_q;

/* IST Management */
static apr_event_t cvs_thread_event;
static cvs_thread_state_enum_t cvs_task_state = CVS_THREAD_STATE_ENUM_INIT;
static apr_thread_t cvs_high_task_handle;
static apr_event_t cvs_high_task_event;
static uint8_t cvs_high_task_stack[ CVS_HIGH_TASK_STACK_SIZE ];
static apr_thread_t cvs_med_task_handle;
static apr_event_t cvs_med_task_event;
static uint8_t cvs_med_task_stack[ CVS_MED_TASK_STACK_SIZE ];
static apr_thread_t cvs_low_task_handle;
static apr_event_t cvs_low_task_event;
static uint8_t cvs_low_task_stack[ CVS_LOW_TASK_STACK_SIZE ];

/* Session Management */
static cvs_pending_control_t cvs_high_task_pending_ctrl;
static cvs_pending_control_t cvs_med_task_pending_ctrl;
static cvs_pending_control_t cvs_low_task_pending_ctrl;

/* Subsystem restart (SSR) management. */
static cvs_pending_control_t cvs_ssr_pending_ctrl;
static cvs_ssr_cleanup_cmd_tracking_t cvs_ssr_cleanup_cmd_tracking;

/* APR Resource */
static uint32_t cvs_apr_handle;

/* Session tracking */
static cvs_generic_item_t cvs_session_list_pool[ CVS_MAX_NUM_SESSIONS ];
static apr_list_t cvs_session_list_free_q;
static apr_list_t cvs_session_q;

/* Flag to indicate if the CVS mailbox timer driver is initialized. The driver
 * is initialized only upon the first time that mailbox packet exchagne feature
 * is used. If it is initialized, it will be deinitialized upon cvs_deinit is
 * called.
 */
static bool_t cvs_is_mailbox_timer_initialized = FALSE;

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

static int32_t cvs_free_object ( cvs_object_t* object );

/* Update session_ctrl.state, with lock protection. */
 static void cvs_update_session_ctrl_state (
   cvs_session_object_t* session_obj,
   cvs_state_enum_t state
 );
/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void cvs_cache_dtx_mode (
  cvs_session_object_t* session_obj,
  void* payload
)
{
  vss_istream_cmd_set_enc_dtx_mode_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( vss_istream_cmd_set_enc_dtx_mode_t* )payload;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_AMR_NB:
      session_obj->cached_voc_properties_amr.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_AMR_WB:
      session_obj->cached_voc_properties_amrwb.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_EAMR:
      session_obj->cached_voc_properties_eamr.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      session_obj->cached_voc_properties_4gvnw.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      session_obj->cached_voc_properties_4gvnw2k.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_EFR:
      session_obj->cached_voc_properties_efr.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_FR:
      session_obj->cached_voc_properties_fr.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_HR:
      session_obj->cached_voc_properties_hr.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_G711_ALAW:
      session_obj->cached_voc_properties_g711_alaw.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_G711_MULAW:
      session_obj->cached_voc_properties_g711_mulaw.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_G711_LINEAR:
      session_obj->cached_voc_properties_g711_linear.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_G729:
      session_obj->cached_voc_properties_g729.dtx_mode = in_args->enable;
      break;

    case VSS_MEDIA_ID_EVS:
      session_obj->cached_voc_properties_evs.dtx_mode = in_args->enable;
      break;
  }

  ( void ) apr_lock_leave( session_obj->lock );
}

static void cvs_cache_rate_modulation (
  cvs_session_object_t* session_obj,
  void* payload
)
{
  vss_istream_cmd_cdma_set_enc_rate_modulation_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( vss_istream_cmd_cdma_set_enc_rate_modulation_t* )payload;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      session_obj->cached_voc_properties_qcelp13k.reduced_rate_mode = in_args->mode;
      break;

    case VSS_MEDIA_ID_EVRC:
      session_obj->cached_voc_properties_evrc.reduced_rate_mode = in_args->mode;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );
}

static void cvs_cache_minmax_rate (
  cvs_session_object_t* session_obj, 
  void* payload
)
{
  vss_istream_cmd_cdma_set_enc_minmax_rate_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( vss_istream_cmd_cdma_set_enc_minmax_rate_t* )payload;

  /* Cache property. */
  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      session_obj->cached_voc_properties_qcelp13k.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_qcelp13k.max_rate = in_args->max_rate;
      break;

    case VSS_MEDIA_ID_EVRC:
      session_obj->cached_voc_properties_evrc.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_evrc.max_rate = in_args->max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NB:
      session_obj->cached_voc_properties_4gvnb.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_4gvnb.max_rate = in_args->max_rate;
      break;

    case VSS_MEDIA_ID_4GV_WB:
      session_obj->cached_voc_properties_4gvwb.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_4gvwb.max_rate = in_args->max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      session_obj->cached_voc_properties_4gvnw.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_4gvnw.max_rate = in_args->max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      session_obj->cached_voc_properties_4gvnw2k.min_rate = in_args->min_rate;
      session_obj->cached_voc_properties_4gvnw2k.max_rate = in_args->max_rate;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

}

static void cvs_cache_enc_rate ( 
  cvs_session_object_t* session_obj, 
  void* payload
)
{
  vss_istream_cmd_voc_amr_set_enc_rate_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( vss_istream_cmd_voc_amr_set_enc_rate_t* )payload;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      session_obj->cached_voc_properties_qcelp13k.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_4GV_NB:
      session_obj->cached_voc_properties_4gvnb.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_4GV_WB:
      session_obj->cached_voc_properties_4gvwb.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      session_obj->cached_voc_properties_4gvnw.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      session_obj->cached_voc_properties_4gvnw2k.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_AMR_NB:
      session_obj->cached_voc_properties_amr.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_EAMR:
      session_obj->cached_voc_properties_eamr.rate = in_args->mode;
      break;

    case VSS_MEDIA_ID_AMR_WB:
      session_obj->cached_voc_properties_amrwb.rate = in_args->mode;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

}

static void cvs_cache_channel_aware_mode (
  cvs_session_object_t* session_obj,
  void* payload
)
{
  cvs_enc_channel_aware_mode_params_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( cvs_enc_channel_aware_mode_params_t* )payload;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_EVS:
      session_obj->cached_voc_properties_evs.channel_aware_enabled = 
        in_args->channel_aware_enabled;
      session_obj->cached_voc_properties_evs.fec_offset = in_args->fec_offset;
      session_obj->cached_voc_properties_evs.fer_rate = in_args->fer_rate;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );
}

static void cvs_cache_enc_operating_mode (
  cvs_session_object_t* session_obj,
  void* payload
)
{
  vss_istream_cmd_set_evs_voc_enc_operating_mode_t* in_args;

  if ( payload == NULL )
  {
    return;
  }
  in_args = ( vss_istream_cmd_set_evs_voc_enc_operating_mode_t* )payload;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_EVS:
      session_obj->cached_voc_properties_evs.mode = in_args->mode;
      session_obj->cached_voc_properties_evs.bandwidth = in_args->bandwidth;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );
}

static void cvs_cache_voc_property (
  cvs_session_object_t* session_obj, 
  void* payload,
  cvs_voc_property_enum_t property
)
{
  if ( payload == NULL )
  {
    return;
  }

  switch ( property )
  {
    case CVS_VOC_PROPERTY_ENUM_DTX:
      cvs_cache_dtx_mode( session_obj, payload );
      break;

    case CVS_VOC_PROPERTY_ENUM_RATE_MODULATION:
      cvs_cache_rate_modulation( session_obj, payload );
      break;

    case CVS_VOC_PROPERTY_ENUM_MINMAX_RATE:
      cvs_cache_minmax_rate( session_obj, payload );
      break;

    case CVS_VOC_PROPERTY_ENUM_ENC_RATE:
      cvs_cache_enc_rate( session_obj, payload );
      break;

    case CVS_VOC_PROPERTY_ENUM_CHANNEL_AWARE_MODE:
      cvs_cache_channel_aware_mode( session_obj, payload );
      break;

    case CVS_VOC_PROPERTY_ENUM_ENC_OPERATING_MODE:
      cvs_cache_enc_operating_mode( session_obj, payload );
      break;
  }

}

static bool_t cvs_media_id_is_valid (
  uint32_t media_id
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_13K:
  case VSS_MEDIA_ID_EVRC:
  case VSS_MEDIA_ID_4GV_NB:
  case VSS_MEDIA_ID_4GV_WB:
  case VSS_MEDIA_ID_4GV_NW:
  case VSS_MEDIA_ID_4GV_NW2K:
  case VSS_MEDIA_ID_AMR_NB:
  case VSS_MEDIA_ID_AMR_WB:
  case VSS_MEDIA_ID_EAMR:
  case VSS_MEDIA_ID_EFR:
  case VSS_MEDIA_ID_FR:
  case VSS_MEDIA_ID_HR:
  case VSS_MEDIA_ID_PCM_8_KHZ:
  case VSS_MEDIA_ID_PCM_16_KHZ:
  case VSS_MEDIA_ID_PCM_32_KHZ:
  case VSS_MEDIA_ID_PCM_44_1_KHZ:
  case VSS_MEDIA_ID_PCM_48_KHZ:
  case VSS_MEDIA_ID_G711_ALAW:
  case VSS_MEDIA_ID_G711_ALAW_V2:
  case VSS_MEDIA_ID_G711_MULAW:
  case VSS_MEDIA_ID_G711_MULAW_V2:
  case VSS_MEDIA_ID_G711_LINEAR:
  case VSS_MEDIA_ID_G729:
  case VSS_MEDIA_ID_G722:
  case VSS_MEDIA_ID_EVS:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_MEDIA_ID_NONE is not considered a valid parameter because
   * system cannot run without a media type.
   */

  return rc;
}

static bool_t cvs_media_id_is_nb_sr (
  uint32_t media_id
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_NONE:
  case VSS_MEDIA_ID_13K:
  case VSS_MEDIA_ID_EVRC:
  case VSS_MEDIA_ID_4GV_NB:
  case VSS_MEDIA_ID_AMR_NB:
  case VSS_MEDIA_ID_EFR:
  case VSS_MEDIA_ID_FR:
  case VSS_MEDIA_ID_HR:
  case VSS_MEDIA_ID_PCM_8_KHZ:
  case VSS_MEDIA_ID_G711_ALAW:
  case VSS_MEDIA_ID_G711_ALAW_V2:
  case VSS_MEDIA_ID_G711_MULAW:
  case VSS_MEDIA_ID_G711_MULAW_V2:
  case VSS_MEDIA_ID_G711_LINEAR:
  case VSS_MEDIA_ID_G729:
  case VSS_MEDIA_ID_G722:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_MEDIA_ID_NONE is not considered a valid parameter because
   * system cannot run without a media type.
   */

  return rc;
}

static bool_t cvs_vocoder_is_variable (
  uint32_t media_id /* Assumes the media_id is valid. */
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_EVS:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t cvs_var_voc_sr_is_valid (
  uint32_t sr
)
{
  bool_t rc;

  switch ( sr )
  {
  case 8000:
  case 16000:
  case 32000:
  case 41000:
  case 48000:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t cvs_voc_op_mode_is_valid (
  uint32_t voc_op_mode
)
{
  bool_t rc;

  switch ( voc_op_mode )
  {
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_SWB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_FB:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static int32_t cvs_determine_closest_supported_pp_sr (
  uint32_t enc_dec_sr,
  uint32_t* ret_pp_sr
)
{
  switch ( enc_dec_sr )
  {
  case 8000:
    *ret_pp_sr = 8000;
    break;

  case 16000:
    *ret_pp_sr = 16000;
    break;

  case 32000:
    *ret_pp_sr = 32000;
    break;
    
  case 41000:
  case 48000:
    *ret_pp_sr = 48000;
    break;

  default:
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return APR_EBADPARAM;
    }
  }

  return APR_EOK;
}

/* Determine if the EVS operating mode is valid */
static bool_t cvs_evs_validate_enc_operating_mode (
  uint32_t mode,
  uint32_t bandwidth,
  uint32_t requested_var_voc_tx_sampling_rate
)
{
  bool_t is_valid = FALSE;

  /* Making sure the modes and bandwidths requested are supported */
  switch ( mode )
  {
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_6_60_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_8_85_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_12_65_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_14_25_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_15_85_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_18_25_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_19_85_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_23_05_KBPS:
    case CVS_EVS_OPERATING_MODE_AMRWB_IO_23_85_KBPS:
      {
        if ( VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB == bandwidth )
        {
          is_valid = TRUE;
        }
      }
      break;

    case CVS_EVS_OPERATING_MODE_EVS_5_90_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_7_20_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_8_00_KBPS:
      {
        if ( VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB >= bandwidth )
        {
          is_valid = TRUE;
        }
      }
      break;

    case CVS_EVS_OPERATING_MODE_EVS_9_60_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_13_20_KBPS:
      {
        if ( VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB >= bandwidth )
        {
          is_valid = TRUE;
        }
      }
      break;

    case CVS_EVS_OPERATING_MODE_EVS_16_40_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_24_40_KBPS:
      {
        if ( VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB >= bandwidth )
        {
          is_valid = TRUE;
        }
      }
      break;

    case CVS_EVS_OPERATING_MODE_EVS_32_00_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_48_00_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_64_00_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_96_00_KBPS:
    case CVS_EVS_OPERATING_MODE_EVS_128_00_KBPS:
      {
        if ( VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB != bandwidth )
        {
          is_valid = TRUE;
        }
      }
      break;

    default:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_validate_enc_operating_mode:" \
                                                "Invalid Bandwidth=%d for mode=%d",
                                                bandwidth, mode );
        is_valid = FALSE;
      }
      break;
  }

  /* Need to validate bandwidth relative to Tx sampling rate */
  if ( is_valid )
  {
    is_valid = FALSE;
    switch ( bandwidth )
    {
      case VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB:
        {
          if ( requested_var_voc_tx_sampling_rate >= 8000 )
          {
            is_valid = TRUE;
          }
        }
        break;

      case VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB:
        {
          if ( requested_var_voc_tx_sampling_rate >= 16000 )
          {
            is_valid = TRUE;
          }
        }
        break;

      case VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB:
        {
          if ( requested_var_voc_tx_sampling_rate >= 32000 )
          {
            is_valid = TRUE;
          }
        }
        break;

      case VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB:
        {
          if ( requested_var_voc_tx_sampling_rate >= 48000 )
          {
            is_valid = TRUE;
          }
        }
        break;

      default:
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_validate_enc_operating_mode:" \
                                                  "Invalid Bandwidth=%d for Tx Sampling Rate=%d",
                                                  bandwidth, requested_var_voc_tx_sampling_rate );
          is_valid = FALSE;
        }
        break;
    }
  }

  if ( !is_valid )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_validate_enc_operating_mode:" \
                                            "Invalid Bandwidth=%d for mode=%d and " \
                                            "Tx Sampling Rate=%d",
                                            bandwidth, mode, 
                                            requested_var_voc_tx_sampling_rate );
  }

  return is_valid;
}

/* Determine if the EVS Encoder Channel Aware Mode command is valid from 
the VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE command */

static bool_t cvs_evs_validate_enc_channel_aware_mode (
  uint8_t fec_offset,
  uint8_t fer_rate
)
{
  bool_t is_valid = FALSE;

  switch ( fec_offset )
  {
  case CVS_EVS_FEC_OFFSET_2:
  case CVS_EVS_FEC_OFFSET_3:
  case CVS_EVS_FEC_OFFSET_5:
  case CVS_EVS_FEC_OFFSET_7:
    {
      is_valid = TRUE;
    }
    break;

  default:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_validate_enc_channel_aware_mode: " \
                                              "Bad FEC Offset value=%d",
                                              fec_offset );
      is_valid = FALSE;
    }
    break;
  }

  if ( is_valid )
  {
    switch ( fer_rate )
    {
    case CVS_EVS_FER_RATE_LOW:
    case CVS_EVS_FER_RATE_HIGH:
      {
        is_valid = TRUE;
      }
      break;
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_validate_enc_channel_aware_mode: " \
                                                "Bad FER Rate value=%d",
                                                fer_rate );
        is_valid = FALSE;
      }
      break;
    }
  }

  return is_valid;
}

/* Determine the target_set enc, dec sampling rates for the EVS media
type.  Parameters ret_dec_sampling_rate and ret_enc_sampling_rate should 
be non-NULL*/
static int32_t cvs_evs_sanitize_sampling_rates (
  uint32_t requested_rx_sampling_rate,
  uint32_t requested_tx_sampling_rate,
  uint32_t* ret_dec_sampling_rate,
  uint32_t* ret_enc_sampling_rate
)
{
  int32_t rc = APR_EOK;

  if ( ( ret_dec_sampling_rate == NULL ) || ( ret_enc_sampling_rate == NULL ) )
  {
    return APR_EFAILED;
  }

  switch ( requested_rx_sampling_rate )
  {
    case 8000:
      {
        *ret_dec_sampling_rate = 8000;
        break;
      }
    case 16000:
      {
        *ret_dec_sampling_rate = 16000;
        break;
      }
    case 32000:
      {
        *ret_dec_sampling_rate = 32000;
        break;
      }
    case 0:
    case 48000:
      {
        *ret_dec_sampling_rate = 48000;
        break;
      }
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_sanitize_sampling_rates: " \
                                                "Invalid Rx Sampling Rate %d",
                                                requested_rx_sampling_rate );
        rc = APR_EBADPARAM;
        break;
      }
  }

  switch ( requested_tx_sampling_rate )
  {
    case 8000:
      {
        *ret_enc_sampling_rate = 8000;
        break;
      }
    case 16000:
      {
        *ret_enc_sampling_rate = 16000;
        break;
      }
    case 32000:
      {
        *ret_enc_sampling_rate = 32000;
        break;
      }
    case 0:
    case 48000:
      {
        *ret_enc_sampling_rate = 48000;
        break;
      }
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_evs_sanitize_sampling_rates: " \
                                                "Invalid Tx Sampling Rate %d",
                                                requested_tx_sampling_rate );
        rc = APR_EBADPARAM;
        break;
      }
  }

  return rc;
}

/* Determine the target_set enc, dec and pp sampling rates based on the
 * media type and the client requested variable vocoder sampling rates.
 */
static int32_t cvs_determine_target_sampling_rates (
  uint32_t media_id,
  uint32_t requested_var_voc_rx_sampling_rate,
  uint32_t requested_var_voc_tx_sampling_rate,
  uint32_t* ret_dec_sr,
  uint32_t* ret_enc_sr,
  uint32_t* ret_rx_pp_sr,
  uint32_t* ret_tx_pp_sr
)
{
  int32_t rc = APR_EOK;

  if ( ( ret_dec_sr == NULL ) || ( ret_enc_sr == NULL ) ||
       ( ret_rx_pp_sr == NULL ) || ( ret_tx_pp_sr == NULL ) )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  for ( ;; )
  {
    if ( cvs_media_id_is_valid( media_id ) == FALSE ||
         cvs_var_voc_sr_is_valid( requested_var_voc_rx_sampling_rate ) == FALSE ||
         cvs_var_voc_sr_is_valid( requested_var_voc_tx_sampling_rate ) == FALSE )
    {
      rc = APR_EBADPARAM;
      break;
    }

    /* First determine the enc/dec sampling rates. */
    switch ( media_id )
    {
    case VSS_MEDIA_ID_13K:
    case VSS_MEDIA_ID_EVRC:
    case VSS_MEDIA_ID_4GV_NB:
    case VSS_MEDIA_ID_AMR_NB:
    case VSS_MEDIA_ID_EFR:
    case VSS_MEDIA_ID_FR:
    case VSS_MEDIA_ID_HR:
    case VSS_MEDIA_ID_PCM_8_KHZ:
    case VSS_MEDIA_ID_G711_ALAW:
    case VSS_MEDIA_ID_G711_MULAW:
    case VSS_MEDIA_ID_G711_ALAW_V2:
    case VSS_MEDIA_ID_G711_MULAW_V2:
    case VSS_MEDIA_ID_G711_LINEAR:
    case VSS_MEDIA_ID_G729:
    case VSS_MEDIA_ID_G722:
      {
        *ret_dec_sr = 8000;
        *ret_enc_sr = 8000;
      }
      break;

    case VSS_MEDIA_ID_4GV_WB:
    case VSS_MEDIA_ID_4GV_NW:
    case VSS_MEDIA_ID_4GV_NW2K:
    case VSS_MEDIA_ID_AMR_WB:
    case VSS_MEDIA_ID_EAMR:
    case VSS_MEDIA_ID_PCM_16_KHZ:
      {
        *ret_dec_sr = 16000;
        *ret_enc_sr = 16000;
      }
      break;

    case VSS_MEDIA_ID_PCM_32_KHZ:
      {
        *ret_dec_sr = 32000;
        *ret_enc_sr = 32000;
      }
      break;
      
    case VSS_MEDIA_ID_PCM_44_1_KHZ:
      {
        *ret_dec_sr = 41000;
        *ret_enc_sr = 41000;
      }
      break;

    case VSS_MEDIA_ID_PCM_48_KHZ:
      {
        *ret_dec_sr = 48000;
        *ret_enc_sr = 48000;
      }
      break;

    case VSS_MEDIA_ID_EVS:
      {
        rc = cvs_evs_sanitize_sampling_rates( requested_var_voc_rx_sampling_rate, 
                                              requested_var_voc_tx_sampling_rate,
                                              ret_dec_sr, ret_enc_sr );
      }
      break;

    default:
      {
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        rc = APR_EBADPARAM;
      }
      break;
    }

    if ( rc )
      break;

    /* Now determine the supported closest mataching higher sample rates
     * for the pp based on the enc/dec sample rates.
     */
    rc = cvs_determine_closest_supported_pp_sr( *ret_dec_sr, ret_rx_pp_sr );
    if ( rc )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    rc = cvs_determine_closest_supported_pp_sr( *ret_enc_sr, ret_tx_pp_sr );
    if ( rc )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return rc;
}

static int32_t cvs_determine_target_rx_pp_sr_from_feature (
  uint32_t feature_id,
  uint32_t media_id
)
{
  int32_t rx_pp_sampling_rate = 0;

  switch (feature_id)
  {
    case VSS_ICOMMON_CAL_FEATURE_BEAMR:
      rx_pp_sampling_rate = 16000;
      break;

    default:
      {
        /** Need to expand to higher bands in the future **/
        if ( cvs_media_id_is_nb_sr( media_id ) )
          rx_pp_sampling_rate = 8000;
        else
          rx_pp_sampling_rate = 16000;
      }
      break;
  }

  return rx_pp_sampling_rate;
}

static int32_t cvs_verify_param_data_size (
  uint32_t param_size
)
{
  /* By design, all voice parameter size must be multiple of 4 bytes. */
  if ( param_size & 0x3 )
  {
    return APR_EFAILED;
  }

  return APR_EOK;
}

static uint32_t cvs_find_vocproc (
  cvs_session_object_t* my_session,
  uint32_t vdsp_session_handle,
  uint32_t direction
)
{
  uint8_t cnt = CVS_MAX_ATTACHED_VOCPROC_CNT;
  uint32_t rslt= CVS_MAX_ATTACHED_VOCPROC_CNT;
  uint16_t *p_dir_array = NULL;

  if (CVS_DIRECTION_RX == direction)
  {
    p_dir_array = (uint16_t *)&my_session->active_set.attach_table.rx_device_handle;
  }
  else if (CVS_DIRECTION_TX == direction)
  {
    p_dir_array = (uint16_t *)&my_session->active_set.attach_table.tx_device_handle;
  }

  if( p_dir_array )
  {
    for (cnt=0; cnt < CVS_MAX_ATTACHED_VOCPROC_CNT; cnt++)
    {
      if ( vdsp_session_handle == p_dir_array[cnt] )
      {
        rslt = cnt;
        break;
      }
    }
  }

  return rslt;
}

static void cvs_remove_vocproc (
  cvs_session_object_t* p_my_session,
  uint32_t vdsp_session_handle,
  uint32_t direction,
  uint32_t cvp_session_handle
)
{
  uint8_t   cnt             = CVS_MAX_ATTACHED_VOCPROC_CNT;
  uint16_t  *p_dir_array    = NULL;
  uint16_t  *p_attach_cnt   = NULL;
  bool_t    *p_isready           = NULL;
  uint16_t  *p_cvp_handle_array  = NULL;

  switch ( direction )
  {
  case CVS_DIRECTION_RX:
    {
      p_dir_array    = (uint16_t *)(p_my_session->active_set.attach_table.rx_device_handle);
      p_attach_cnt   = (uint16_t *)&(p_my_session->active_set.attach_table.rx_device_cnt);
      p_isready     = (bool_t  *)(p_my_session->active_set.attach_table.cvp_isready);
      p_cvp_handle_array = (uint16_t *)(p_my_session->active_set.attach_table.cvp_handles);
    }
    break;
  case CVS_DIRECTION_TX:
    {
      p_dir_array     = (uint16_t *)(p_my_session->active_set.attach_table.tx_device_handle);
      p_attach_cnt    = (uint16_t *)&(p_my_session->active_set.attach_table.tx_device_cnt);
      p_isready     = (bool_t  *)(p_my_session->active_set.attach_table.cvp_isready);
      p_cvp_handle_array = (uint16_t *)(p_my_session->active_set.attach_table.cvp_handles);
    }
    break;
  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( ( p_dir_array != NULL ) && ( p_attach_cnt > 0 ) )
  {
    for (cnt=0; cnt < CVS_MAX_ATTACHED_VOCPROC_CNT; cnt++)
    {
      if ( vdsp_session_handle == p_dir_array[cnt] )
      {
        p_dir_array[cnt] = (uint16_t)CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        (*p_attach_cnt)= (*p_attach_cnt) - 1;

        p_cvp_handle_array[cnt] = (uint16_t)CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        p_isready[cnt] = FALSE;
        break;
      }
    }
  }

    /* Re-arranging attach table */

  if ( cnt < ( *p_attach_cnt ) )
  {
    for ( cnt = cnt + 1; cnt < CVS_MAX_ATTACHED_VOCPROC_CNT ; cnt++ )
    {
      if ( p_dir_array[cnt] != (uint16_t)CVS_ATTACHED_VOCPROC_HANDLE_INVALID )
      {
        p_dir_array[cnt - 1] = p_dir_array[cnt];
        p_cvp_handle_array[cnt - 1] = p_cvp_handle_array[cnt];
        p_isready[cnt - 1] = p_isready[cnt];

        p_dir_array[cnt]     = (uint16_t)CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        p_cvp_handle_array[cnt] = (uint16_t)CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        p_isready[cnt] = FALSE;
      }
      else
      {
        break;
      }
    }
  }
}

void cvs_add_vocproc (
  cvs_session_object_t* p_my_session,
  uint32_t vdsp_session_handle,
  uint32_t direction,
  uint32_t cvp_session_handle
)
{
  uint32_t  i_index         = 0;
  uint16_t  *p_dir_array    = NULL;
  uint16_t  *p_attach_cnt   = NULL;
  bool_t    *p_isready           = NULL;
  uint16_t  *p_cvp_handle_array  = NULL;

  switch ( direction )
  {
  case CVS_DIRECTION_RX:
    {
      p_dir_array   = (uint16_t  *)(p_my_session->active_set.attach_table.rx_device_handle);
      p_attach_cnt  = (uint16_t  *)&(p_my_session->active_set.attach_table.rx_device_cnt);
      p_isready     = (bool_t  *)(p_my_session->active_set.attach_table.cvp_isready);
      p_cvp_handle_array = (uint16_t *)(p_my_session->active_set.attach_table.cvp_handles);
    }
    break;
  case CVS_DIRECTION_TX:
    {
      p_dir_array   = (uint16_t  *)(p_my_session->active_set.attach_table.tx_device_handle);
      p_attach_cnt  = (uint16_t  *)&(p_my_session->active_set.attach_table.tx_device_cnt);
      p_isready     = (bool_t  *)(p_my_session->active_set.attach_table.cvp_isready);
      p_cvp_handle_array = (uint16_t *)(p_my_session->active_set.attach_table.cvp_handles);
    }
    break;
  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( p_dir_array && p_attach_cnt )
  {
    for (; i_index < CVS_MAX_ATTACHED_VOCPROC_CNT; i_index++)
    {
      if (CVS_ATTACHED_VOCPROC_HANDLE_INVALID == p_dir_array[i_index] )
      {
        p_dir_array[i_index] = ( ( uint16_t ) vdsp_session_handle );
        (*p_attach_cnt)= (*p_attach_cnt) + 1;
        p_isready[i_index] = FALSE;
        p_cvp_handle_array[i_index] = cvp_session_handle;
        break;
      }
    }
  }
}

/* Attach to the stream the reported direciton of the vocproc */
static void cvs_attach_vocproc (
  cvs_session_object_t* p_my_session,
  uint32_t vdsp_session_handle,
  uint32_t direction,
  uint32_t cvp_session_handle
)
{
  if(CVS_DIRECTION_RX == direction || CVS_DIRECTION_RX_TX == direction)
  { /* Check if not connected as RX already */
    if (CVS_MAX_ATTACHED_VOCPROC_CNT==cvs_find_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_RX))
    {/* Add connection */
      cvs_add_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_RX, cvp_session_handle);
    }
  }
  if(CVS_DIRECTION_TX == direction || CVS_DIRECTION_RX_TX == direction)
  { /* Check if not connected as TX already */
    if (CVS_MAX_ATTACHED_VOCPROC_CNT==cvs_find_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_TX))
    {/* Add connection */
      cvs_add_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_TX, cvp_session_handle);
    }
  }
}

/* Detach from the stream the reported direction of the vocproc */
static void cvs_detach_vocproc (
  cvs_session_object_t* p_my_session,
  uint32_t vdsp_session_handle,
  uint32_t direction,
  uint32_t cvp_session_handle
)
{

  if(CVS_DIRECTION_RX == direction || CVS_DIRECTION_RX_TX == direction)
  { /* Check if not connected as RX already */
    if (CVS_MAX_ATTACHED_VOCPROC_CNT!=cvs_find_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_RX))
    {/* Add connection */
      cvs_remove_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_RX, cvp_session_handle);
    }
  }
  if(CVS_DIRECTION_TX == direction || CVS_DIRECTION_RX_TX == direction)
  { /* Check if not connected as TX already */
    if (CVS_MAX_ATTACHED_VOCPROC_CNT!=cvs_find_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_TX))
    {/* Add connection */
      cvs_remove_vocproc(p_my_session, vdsp_session_handle, CVS_DIRECTION_TX, cvp_session_handle);
    }
  }
}

static void cvs_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvs_int_lock );
}

static void cvs_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvs_int_lock );
}

static void cvs_med_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvs_med_task_lock );
}

static void cvs_med_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvs_med_task_lock );
}

static void cvs_low_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvs_low_task_lock );
}

static void cvs_low_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvs_low_task_lock );
}

static void cvs_signal_run (
  cvs_thread_priority_enum_t priority
)
{
  switch( priority )
  {
  case CVS_THREAD_PRIORITY_ENUM_HIGH:
    apr_event_signal( cvs_high_task_event );
    break;

  case CVS_THREAD_PRIORITY_ENUM_MED:
    apr_event_signal( cvs_med_task_event );
    break;

  case CVS_THREAD_PRIORITY_ENUM_LOW:
    apr_event_signal( cvs_low_task_event );
    break;

  default:
    break;
  }

  return;
}

static int32_t cvs_get_object
(
  uint32_t handle,
  cvs_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &cvs_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( cvs_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static int32_t cvs_typecast_object (
  apr_objmgr_object_t* store,
  cvs_object_type_enum_t type,
  cvs_object_t** ret_obj
)
{
  cvs_object_t* obj;

  if ( ( store == NULL ) || ( ret_obj == NULL ) )
  {
    return APR_EBADPARAM;
  }

  obj = ( ( cvs_object_t* ) store->any.ptr );

  if ( ( obj == NULL ) || ( obj->header.type != type ) )
  {
    return APR_EFAILED;
  }

  *ret_obj = obj;

  return APR_EOK;
}

static int32_t cvs_get_typed_object (
  uint32_t handle,
  cvs_object_type_enum_t type,
  cvs_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* store;

  if ( handle == 0 )
  {
    return APR_EHANDLE;
  }

  rc = apr_objmgr_find_object( &cvs_objmgr, handle, &store );
  if ( rc ) return rc;

  rc = cvs_typecast_object( store, type, ret_obj );
  if ( rc ) return rc;

  return APR_EOK;
}

/* Get session object from handle.
   Increment reference counter. */
static uint32_t cvs_access_session_obj_start (
  uint32_t handle,
  bool_t is_indirect_handle,
  cvs_session_object_t** ret_session_obj,
  cvs_indirection_object_t** ret_indirect_obj
)
{
  int32_t rc;
  cvs_indirection_object_t* indirect_obj;
  cvs_session_object_t* session_obj;

  for ( ;; )
  {
    if ( handle == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    ( void ) apr_lock_enter( cvs_ref_cnt_lock );
    if ( is_indirect_handle == TRUE )
    {
      rc = cvs_get_typed_object( handle,
                                 CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( cvs_object_t** ) &indirect_obj ) );
      if ( rc )
      {
        rc = APR_EHANDLE;
        break;
      }

      handle = indirect_obj->session_handle;
      
      if ( ret_indirect_obj != NULL )
      {
        *ret_indirect_obj = indirect_obj;
      }
    }

    rc = cvs_get_typed_object( handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    if ( rc )
    {
      rc = APR_EHANDLE;
      break;
    }

    if ( ret_session_obj != NULL )
    {
      *ret_session_obj = session_obj;
    }

    /* Session is to be destroyed. Reject the acess to the session. */
    if ( session_obj->ref_cnt == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    session_obj->ref_cnt++;

    break;
  }

  ( void ) apr_lock_leave( cvs_ref_cnt_lock );

  return rc;
}

/* Decrement reference counter. */
static uint32_t cvs_access_session_obj_end (
  uint32_t handle,
  bool_t is_indirect_handle
)
{
  int32_t rc;
  cvs_indirection_object_t* indirect_obj;
  cvs_session_object_t* session_obj;

  for ( ;; )
  {
    if ( handle == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    ( void ) apr_lock_enter( cvs_ref_cnt_lock );
    if ( is_indirect_handle == TRUE )
    {
      rc = cvs_get_typed_object( handle,
                                 CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( cvs_object_t** ) &indirect_obj ) );
      if ( rc )
      {
        rc = APR_EHANDLE;
        break;
      }

      handle = indirect_obj->session_handle;
    }

    rc = cvs_get_typed_object( handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    if ( rc )
    {
      rc = APR_EHANDLE;
      break;
    }

    if ( session_obj->ref_cnt == 0 )
    { /* Session is to be destroyed. No-op. */
      rc = APR_EOK;
      break;
    }
    else
    {
      session_obj->ref_cnt--;
    }

    /* Signal Destroy Command Processing thread after 
     * decrementing session_obj reference count.
     * Destroy Command is not processed until session_obj 
     * is not accessed by any other command.
     */
    ( void ) apr_event_signal( cvs_low_task_event );


    rc = APR_EOK;
    break;
  }
  
  ( void ) apr_lock_leave( cvs_ref_cnt_lock );

  return rc;
}

/****************************************************************************
 * PENDING COMMAND ROUTINES                                                 *
 ****************************************************************************/

static int32_t cvs_pending_control_init (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init( &ctrl->cmd_q, NULL, NULL );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVS_PENDING_CMD_STATE_ENUM_FETCH;
  ctrl->packet = NULL;
  ctrl->pendjob_obj = NULL;

  return APR_EOK;
}

static int32_t cvs_pending_control_destroy (
  cvs_pending_control_t* ctrl
)
{
#if 0 /* There's nothing to destroy if everything is in a good state. */
  int32_t rc;
#endif /* 0 */

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

#if 0 /* There's nothing to destroy if everything is in a good state. */
  rc = apr_list_destroy( &ctrl->cmd_q );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVS_PENDING_CMD_STATE_ENUM_UNINITIALIZED;
  ctrl->packet = NULL;
  ctrl->pendjob_obj = NULL;
#endif /* 0 */

  return APR_EOK;
}

/****************************************************************************
 * CVS WORK QUEUE ROUTINES                                                  *
 ****************************************************************************/

static void cvs_queue_pending_packet (
  apr_list_t* pending_cmd_q,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_work_item_t* work_item;
  int do_once = 0;
  uint16_t client_addr;

  if ( pending_cmd_q == NULL )
  {
    return;
  }

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }
  do
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &cvs_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Queue the incoming command to the pending command queue. We don't
       * need to signal do work because after the incoming command
       * handler is done the pending command handler routine will be
       * called.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( pending_cmd_q, &work_item->link );
    }

    return;
  }
  while ( do_once );

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, rc );
    CVS_COMM_ERROR( rc, client_addr );
  }
}

static void cvs_queue_incoming_packet (
  cvs_thread_priority_enum_t priority,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_work_item_t* work_item;
  apr_list_t* incoming_cmd_q;
  uint16_t client_addr;

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }

  switch ( priority )
  {
  case CVS_THREAD_PRIORITY_ENUM_HIGH:
    incoming_cmd_q = &cvs_high_task_incoming_cmd_q;
    break;

  case CVS_THREAD_PRIORITY_ENUM_MED:
    incoming_cmd_q = &cvs_med_task_incoming_cmd_q;
    break;

  case CVS_THREAD_PRIORITY_ENUM_LOW:
    incoming_cmd_q = &cvs_low_task_incoming_cmd_q;
    break;

  default:
    return;
  }

  for ( ;; )
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &cvs_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Report command acceptance when requested. */
      if ( priority == CVS_THREAD_PRIORITY_ENUM_HIGH )
      {
        /* Accept the command in APR dispatcher context. */
        rc = __aprv2_cmd_accept_command( cvs_apr_handle, packet );
        if ( rc )
        { /* Can't report so abort the command. */
          ( void ) apr_list_add_tail( &cvs_free_cmd_q, &work_item->link );
          break;
        }
      }
    }

    { /* Queue the new command to the incoming command queue and signal do
       * work.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( incoming_cmd_q, &work_item->link );

      cvs_signal_run( priority );
    }

    return;
  }

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, rc );
    CVS_COMM_ERROR( rc, client_addr );
  }
}

/****************************************************************************
 * CVS DEFAULT RESPONSE PROCESSING ROUTINES                                 *
 ****************************************************************************/

static void cvs_rsp_fn_default_event (
  aprv2_packet_t* packet
)
{
  /* The default event handler just drops the packet. A specific event
   * handler routine should be written to something more useful.
   */
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_set_default_response_table (
  cvs_response_fn_table_t table
)
{
  int i;

  if ( table == NULL )
  {
    return;
  }

  /* Initialize the state response handler function table. */
  for ( i = 0; i < CVS_RESPONSE_FN_ENUM_MAX; ++i )
  {
    table[ i ] = cvs_rsp_fn_default_event;
  }
}

static void cvs_simple_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

    /* If completed command failed, log the error. */
    if ( obj->status != APR_EOK )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_simple_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                              APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                              obj->status );
    }
  }

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Destroy the simple job object once done. This is primarily useful for
 * analyzing/debugging the flow of jobs in logs.
 */
static void cvs_simple_self_destruct_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* obj;
  uint32_t status;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    /* This rsp function should NOT be used for commands returning anything
     * other than APRV2_IBASIC_RSP_RESULT (i.e. custom responses). However,
     * for robustness' sake, we do this check before interpreting the payload.
     */
    if ( packet->opcode == APRV2_IBASIC_RSP_RESULT )
    {
      status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

      /* If completed command failed, log the error. */
      if ( status != APR_EOK )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_simple_self_destruct_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                                APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                                status );
      }
    }

    ( void ) cvs_free_object( ( cvs_object_t* ) obj );
  }

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/****************************************************************************
 * CUSTOM RESPONSE PROCESSING ROUTINES (NOT FOR STATE MACHINE TRANSITION)   *
 ****************************************************************************/

static void cvs_forward_command_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_track_job_object_t* forwarding_job_obj;
  aprv2_ibasic_rsp_result_t* p_payload;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &forwarding_job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* Overwrite the orignal opcode here */
  p_payload  = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet );
  p_payload->opcode = forwarding_job_obj->orig_opcode;

  /* This should copy the original packet's content and create new one */
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvs_my_addr, forwarding_job_obj->orig_dst_port,
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         APRV2_IBASIC_RSP_RESULT,
         APRV2_PKT_GET_PAYLOAD( void, packet ),
         APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  CVS_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  ( void ) cvs_free_object( ( cvs_object_t* ) forwarding_job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_forward_command_get_param_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_track_job_object_t* forwarding_job_obj;
  uint32_t payload_len;
  uint8_t* payload;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &forwarding_job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* If the get param is for GET_UI_PROPERTY, translate the response opcode to RSP_GET_UI_PROP */
  if ( forwarding_job_obj->orig_opcode == VSS_ICOMMON_CMD_GET_UI_PROPERTY )
  {
    packet->opcode = VSS_ICOMMON_RSP_GET_UI_PROPERTY;
  }

  payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_len >= 16 )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_forward_command_get_param_rsp_fn(): Payload[0-2]=0x%08x, " \
                                          "0x%08x, 0x%08x",
                                          *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) ), 
                                          *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ), 
                                          *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_forward_command_get_param_rsp_fn(): Payload[3]=0x%08x",
                                          *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
  }
  else if ( payload_len >= 4 )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_forward_command_get_param_rsp_fn(): Payload[0]=0x%08x",
                                          *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) ) );
  }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_forward_command_get_param_rsp_fn(): Get Param Response Token=%d",
                                           forwarding_job_obj->orig_token );

  /* This should copy the original packet's content and create new one */
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvs_my_addr, forwarding_job_obj->orig_dst_port,
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         packet->opcode,
         APRV2_PKT_GET_PAYLOAD( void, packet ),
         APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  CVS_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  ( void ) cvs_free_object( ( cvs_object_t* ) forwarding_job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Response function for VSM_CMD_GET_KPPS.
   The command and response handling are both in low prioirty thread.
   No need to guard kpps_info and the flag here. */
static void cvs_get_kpps_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;
  vsm_get_kpps_ack_t* get_kpps_ack;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  job_obj->is_completed = TRUE;
  job_obj->status = APR_EOK;

  get_kpps_ack = APRV2_PKT_GET_PAYLOAD( vsm_get_kpps_ack_t, packet );

  /* kpps info is only read/write here. No need of lock. */
  if ( session_obj->kpps_info.enc != get_kpps_ack->venc_kpps )
  {
    session_obj->kpps_info.enc = get_kpps_ack->venc_kpps;
    session_obj->is_kpps_changed = TRUE;
  }

  if ( session_obj->kpps_info.dec != get_kpps_ack->vdec_kpps )
  {
    session_obj->kpps_info.dec = get_kpps_ack->vdec_kpps;
    session_obj->is_kpps_changed = TRUE;
  }

  if ( session_obj->kpps_info.dec_pp != get_kpps_ack->vdecpp_kpps )
  {
    session_obj->kpps_info.dec_pp = get_kpps_ack->vdecpp_kpps;
    session_obj->is_kpps_changed = TRUE;
  }

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_get_kpps_rsp_fn(): venc_kpps = %d, vdec_kpps = %d, vdecpp_kpps = %d",
                                        get_kpps_ack->venc_kpps,
                                        get_kpps_ack->vdec_kpps,
                                        get_kpps_ack->vdecpp_kpps );

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Response function for VSM_CMD_GET_DELAY.
   The command and response handling are both in low prioirty thread.
   No need to guard kpps_info and the flag here. */
static void cvs_get_avsync_delays_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;
  vsm_get_delay_ack_t* avsync_delays;
  uint32_t payload_len;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  job_obj->is_completed = TRUE;
  job_obj->status = APR_EOK;

  /* Clear the previously cahed delay stats. */
  session_obj->avsync_delay_info.stream_rx_algorithmic_delay = 0;
  session_obj->avsync_delay_info.stream_tx_algorithmic_delay = 0;

  avsync_delays = APRV2_PKT_GET_PAYLOAD( vsm_get_delay_ack_t, packet );
  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_len != sizeof( vsm_get_delay_ack_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_avsync_delays_rsp_fn(): Invalid data. Payload len: %d, expected len: %d",
                                            payload_len,
                                            sizeof( vsm_get_delay_ack_t ) );

    ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
    return;
  }

  session_obj->avsync_delay_info.stream_rx_algorithmic_delay = avsync_delays->vdec_delay;
  session_obj->avsync_delay_info.stream_tx_algorithmic_delay = avsync_delays->venc_delay;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_get_avsync_delays_rsp_fn(): vdec_delay = %d, venc_delay = %d ",
                                        avsync_delays->vdec_delay,
                                        avsync_delays->venc_delay );

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Response function for calibration command.
   The command and response handling are both in low prioirty thread.
   No need to guard kpps_info and the flag here. */
static void cvs_calibrate_common_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void )cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->common_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_calibrate_common_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }

  session_obj->common_cal.set_param_rsp_cnt++;

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE);
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Response function to count the responses for the commands issued by CVS to
 * itself in each of the cleanup sequencer state, when doing cleanup due to the
 * subsystem where CVS clients reside is being restarted.
 */
static void cvs_ssr_cleanup_cmd_result_count_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  uint32_t command;
  uint32_t status;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* If the last operation failed, log the error. */
  command = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->opcode;
  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_ssr_cleanup_cmd_result_count_rsp_fn(): Command 0x%08X failed with 0x%08X",
                                            command, status );
  }

  /* Count the number of command responses received. */
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt++;

  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* vsm_mem_handle is only accessed in low priority thread.
   No need to lock it. */
static void cvs_map_oob_pktexg_mem_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;
  voice_rsp_shared_mem_map_regions_t* mem_map_rsp;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  job_obj->is_completed = TRUE;
  job_obj->status = APR_EOK;

  /* Save the vsm mem handle. */
  mem_map_rsp = APRV2_PKT_GET_PAYLOAD( voice_rsp_shared_mem_map_regions_t, packet );
  session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle =
    mem_map_rsp->mem_map_handle;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_map_oob_pktexg_mem_rsp_fn(): vsm_mem_handle = 0x%08X.",
         mem_map_rsp->mem_map_handle );

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/****************************************************************************
 * CVS OBJECT CREATION AND DESTRUCTION ROUTINES                             *
 ****************************************************************************/

static int32_t cvs_mem_alloc_object (
  uint32_t size,
  cvs_object_t** ret_object
)
{
  int32_t rc;
  cvs_object_t* obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the new CVS object. */
    obj = apr_memmgr_malloc( &cvs_heapmgr, size );
    if ( obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_mem_alloc_object(): Out of memory, requestd size(%d)",
                                              size );
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the CVS object. */
    rc = apr_objmgr_alloc_object( &cvs_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_mem_alloc_object(): Out of objects(0x%08X)",
                                              rc );
      apr_memmgr_free( &cvs_heapmgr, obj );
      return APR_ENORESOURCE;
    }

    /* Link the CVS object to the handle. */
    objmgr_obj->any.ptr = obj;

    ( void )mmstd_memset( obj, 0xFD, size );

    /* Initialize the base CVD object header. */
    obj->header.handle = objmgr_obj->handle;
    obj->header.type = CVS_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = obj;

  return APR_EOK;
}

static int32_t cvs_mem_free_object (
  cvs_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &cvs_objmgr, object->header.handle );
  apr_memmgr_free( &cvs_heapmgr, object );

  return APR_EOK;
}

static int32_t cvs_create_session_object (
  cvs_session_object_t** ret_session_obj
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  cvs_session_object_t* session_obj;
  uint32_t i;

  for ( ;; )
  {
    if ( ret_session_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_create_session_object(): sizeof( cvs_session_object_t ): %d",
                                          sizeof( cvs_session_object_t ) );

    /* Allocate the session object. */
    rc = cvs_mem_alloc_object( sizeof( cvs_session_object_t ),
                               ( ( cvs_object_t** ) &session_obj ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_session_object(): failed to allocate session object, result: 0x%08X",
                                              rc );
      break;
    }

    checkpoint = 1;

    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->lock );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_session_object(): " \
                                              "Failed to create lock, rc = (0x%08x)", rc );
      break;
    }
    
    checkpoint = 2;
 
    { /* Initialize the session object. */
      ( void ) mmstd_memset( &session_obj->shared_heap, 0,
                             sizeof( session_obj->shared_heap ) );
      ( void ) mmstd_memset( &session_obj->session_name, 0,
                             sizeof( session_obj->session_name ) );
      ( void ) mmstd_memset( &session_obj->generic_pool, 0,
                             sizeof( session_obj->generic_pool ) );

      for( i = 0; i < CVS_MAX_ATTACHED_VOCPROC_CNT; ++i )
      {
        session_obj->target_set.attach_table.rx_device_handle[ i ] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        session_obj->target_set.attach_table.tx_device_handle[ i ] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        session_obj->active_set.attach_table.rx_device_handle[ i ] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        session_obj->active_set.attach_table.tx_device_handle[ i ] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;

        session_obj->active_set.attach_table.cvp_isready[i] = FALSE;
        session_obj->target_set.attach_table.cvp_isready[i] = FALSE;

        session_obj->active_set.attach_table.cvp_handles[i] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
        session_obj->target_set.attach_table.cvp_handles[i] = CVS_ATTACHED_VOCPROC_HANDLE_INVALID;
      }

      session_obj->target_set.attach_table.cvp_ready_cnt = 0;
      session_obj->active_set.attach_table.cvp_ready_cnt = 0;

      session_obj->active_set.attach_table.rx_device_cnt = APR_NULL_V;
      session_obj->active_set.attach_table.tx_device_cnt = APR_NULL_V;
      session_obj->target_set.attach_table.rx_device_cnt = APR_NULL_V;
      session_obj->target_set.attach_table.tx_device_cnt = APR_NULL_V;

      session_obj->is_enable_received_from_mvm = 0;   /* FALSE */
      session_obj->master_client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->master_client_port = APR_NULL_V;
      session_obj->master_cvs_port = APR_NULL_V;
      session_obj->vsm_stream_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->vsm_stream_handle = APR_NULL_V;
      session_obj->attached_mvm_handle = APR_NULL_V;

      session_obj->is_stream_config_changed = FALSE;

      session_obj->voc_operating_mode_info.is_rx_mode_received = FALSE;
      session_obj->voc_operating_mode_info.is_tx_mode_received = FALSE;
      session_obj->voc_operating_mode_info.rx_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
      session_obj->voc_operating_mode_info.tx_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;

      /* Initialize the free generic objects queue. */
      ( void ) apr_list_init_v2( &session_obj->free_item_q, NULL, NULL );
      for ( i = 0; i < CVS_MAX_GENERIC_OBJECTS_PER_SESSION_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &session_obj->generic_pool[ i ] );
        ( void ) apr_list_add_tail(
                   &session_obj->free_item_q,
                   ( ( apr_list_node_t* ) &session_obj->generic_pool[ i ] ) );
      }

      /* Initialize the indirection queue. */
      ( void ) apr_list_init_v2( &session_obj->indirection_q, NULL, NULL );

      /* Initialize the session and stream state machine control variables. */
      session_obj->session_ctrl.transition_job_handle = APR_NULL_V;
      session_obj->session_ctrl.pendjob_handle = APR_NULL_V;

      session_obj->session_ctrl.state = CVS_STATE_ENUM_RESET;
      session_obj->session_ctrl.goal = CVS_GOAL_ENUM_NONE;
      session_obj->session_ctrl.action = CVS_ACTION_ENUM_NONE;
      session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

      session_obj->is_var_voc_sr_requested = FALSE;
      session_obj->requested_var_voc_rx_sampling_rate = CVS_DEFAULT_VAR_VOC_DEC_SAMPLING_RATE;
      session_obj->requested_var_voc_tx_sampling_rate = CVS_DEFAULT_VAR_VOC_ENC_SAMPLING_RATE;

      session_obj->active_set.direction = ( ( uint16_t ) APR_UNDEFINED_ID_V );    /* RX, TX, or RX+TX. */
      session_obj->active_set.media_id = VSS_MEDIA_ID_NONE;
      session_obj->active_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
      session_obj->active_set.system_config.media_id = VSS_MEDIA_ID_NONE;
      session_obj->active_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
      session_obj->active_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
      session_obj->active_set.system_config.dec_sr = CVS_DEFAULT_DEC_SR;
      session_obj->active_set.system_config.enc_sr = CVS_DEFAULT_ENC_SR;
      session_obj->active_set.system_config.rx_pp_sr = CVS_DEFAULT_RX_PP_SR;
      session_obj->active_set.system_config.tx_pp_sr = CVS_DEFAULT_TX_PP_SR;
      session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
      session_obj->active_set.system_config.vsid = 0;
      session_obj->active_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
      session_obj->active_set.voice_timing.enc_offset = 0;
      session_obj->active_set.voice_timing.dec_req_offset = 0;
      session_obj->active_set.voice_timing.dec_offset = 0;
      session_obj->active_set.voice_timing.dec_pp_start_offset = 0;
      session_obj->active_set.voice_timing.vp_tx_delivery_offset = 0;
      session_obj->active_set.record.enable_flag = CVS_DISABLED;
      session_obj->active_set.record.mode = ( ( uint32_t ) APR_UNDEFINED_ID_V );
      session_obj->active_set.record.port_id = VSS_IRECORD_PORT_ID_DEFAULT;
      session_obj->active_set.record.tx_tap_point = VSS_IRECORD_TAP_POINT_NONE;
      session_obj->active_set.record.rx_tap_point = VSS_IRECORD_TAP_POINT_NONE;
      session_obj->active_set.playback.enable_flag = CVS_DISABLED;
      session_obj->active_set.playback.port_id = VSS_IPLAYBACK_PORT_ID_DEFAULT;
      session_obj->active_set.attach_detach_vocproc_handle = APR_NULL_V;
      session_obj->active_set.attach_detach_stream_handle = APR_NULL_V;

      session_obj->target_set.direction = ( ( uint16_t ) APR_UNDEFINED_ID_V );    /* RX, TX, or RX+TX. */
      session_obj->target_set.media_id = VSS_MEDIA_ID_NONE;
      session_obj->target_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
      session_obj->target_set.system_config.media_id = VSS_MEDIA_ID_NONE;
      session_obj->target_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
      session_obj->target_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
      session_obj->target_set.system_config.dec_sr = CVS_DEFAULT_DEC_SR;
      session_obj->target_set.system_config.enc_sr = CVS_DEFAULT_ENC_SR;
      session_obj->target_set.system_config.rx_pp_sr = CVS_DEFAULT_RX_PP_SR;
      session_obj->target_set.system_config.tx_pp_sr = CVS_DEFAULT_TX_PP_SR;
      session_obj->target_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
      session_obj->target_set.system_config.vsid = 0;
      session_obj->target_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
      session_obj->target_set.system_config.call_num = 0;
      session_obj->target_set.voice_timing.enc_offset = 0;
      session_obj->target_set.voice_timing.dec_req_offset = 0;
      session_obj->target_set.voice_timing.dec_offset = 0;
      session_obj->target_set.voice_timing.dec_pp_start_offset = 0;
      session_obj->target_set.voice_timing.vp_tx_delivery_offset = 0;
      session_obj->target_set.record.enable_flag = CVS_DISABLED;
      session_obj->target_set.record.mode = ( ( uint32_t ) APR_UNDEFINED_ID_V );
      session_obj->target_set.record.port_id = VSS_IRECORD_PORT_ID_DEFAULT;
      session_obj->target_set.record.tx_tap_point = VSS_IRECORD_TAP_POINT_NONE;
      session_obj->target_set.record.rx_tap_point = VSS_IRECORD_TAP_POINT_NONE;
      session_obj->target_set.playback.enable_flag = CVS_DISABLED;
      session_obj->target_set.playback.port_id = VSS_IPLAYBACK_PORT_ID_DEFAULT;
      session_obj->target_set.attach_detach_vocproc_handle = APR_NULL_V;
      session_obj->target_set.attach_detach_stream_handle = APR_NULL_V;

      /* Initiaize stream mute variables. */
      session_obj->active_set.mute.tx_mute_flag = VSS_IVOLUME_MUTE_OFF;
      session_obj->active_set.mute.rx_mute_flag = VSS_IVOLUME_MUTE_OFF;
      session_obj->active_set.mute.tx_ramp_duration = 0;
      session_obj->active_set.mute.rx_ramp_duration = 0;

      session_obj->target_set.mute.tx_mute_flag = VSS_IVOLUME_MUTE_OFF;
      session_obj->target_set.mute.rx_mute_flag = VSS_IVOLUME_MUTE_OFF;
      session_obj->target_set.mute.tx_ramp_duration = 0;
      session_obj->target_set.mute.rx_ramp_duration = 0;
	  
      /* Initiaize ITTYOOB status variable. */
      session_obj->tty_info.is_ittyoob_registered = FALSE;
      session_obj->tty_info.ittyoob_client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->tty_info.ittyoob_client_port = APR_NULL_V;
      session_obj->tty_info.tty_mode = VSS_ITTY_MODE_DISABLED;

      /* Initiaize IAVSYNC client info/delay values. */
      session_obj->avsync_client_rx_info.is_enabled = FALSE;
      session_obj->avsync_client_rx_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->avsync_client_rx_info.client_port = APR_NULL_V;

      session_obj->avsync_client_tx_info.is_enabled = FALSE;
      session_obj->avsync_client_tx_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->avsync_client_tx_info.client_port = APR_NULL_V;

      session_obj->avsync_delay_info.stream_rx_algorithmic_delay = 0;
      session_obj->avsync_delay_info.stream_tx_algorithmic_delay = 0;
      session_obj->avsync_delay_info.total_rx_delay = 0;
      session_obj->avsync_delay_info.total_tx_delay = 0;

      session_obj->set_rx_dtmf_detect.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->set_rx_dtmf_detect.client_port = APR_NULL_V;
      session_obj->set_rx_dtmf_detect.cvs_port = APR_NULL_V;
      session_obj->set_rx_dtmf_detect.enable_flag = CVS_DISABLED;

      session_obj->set_dtmf_gen.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->set_dtmf_gen.client_port = APR_NULL_V;
      session_obj->set_dtmf_gen.cvs_port = APR_NULL_V;

      session_obj->vpcm_info.is_enabled = FALSE;
      session_obj->vpcm_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->vpcm_info.client_handle = APR_NULL_V;
      session_obj->vpcm_info.cvs_handle = APR_NULL_V;
      session_obj->vpcm_info.mem_handle = 0;

      session_obj->packet_exchange_info.mode = VSS_IPKTEXG_MODE_IN_BAND;

      session_obj->packet_exchange_info.oob_info.is_configured = FALSE;
      session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw = 0;
      session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw = 0;
      session_obj->packet_exchange_info.oob_info.config.enc_buf_size = 0;
      session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw = 0;
      session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_msw = 0;
      session_obj->packet_exchange_info.oob_info.config.dec_buf_size = 0;
      session_obj->packet_exchange_info.oob_info.config.mem_handle = 0;

      session_obj->packet_exchange_info.mailbox_info.is_configured = FALSE;
      session_obj->packet_exchange_info.mailbox_info.config.cvs_mem_handle = 0;
      session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf = NULL;
      session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf_mem_size = 0;
      session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf = NULL;
      session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf_mem_size = 0;
      session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle = 0;
      session_obj->packet_exchange_info.mailbox_info.config.oob_enc_buf = NULL;
      session_obj->packet_exchange_info.mailbox_info.config.oob_dec_buf = NULL;
      session_obj->packet_exchange_info.mailbox_info.config.max_enc_pkt_size = 0;
      session_obj->packet_exchange_info.mailbox_info.config.tx_ref_timestamp_us = 0;
      session_obj->packet_exchange_info.mailbox_info.config.rx_ref_timestamp_us = 0;
      session_obj->packet_exchange_info.mailbox_info.config.request_to_start_timestamp_us = 0;
      session_obj->packet_exchange_info.mailbox_info.config.rx_req_and_dequeue_time_diff_us =
        CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
      session_obj->packet_exchange_info.mailbox_info.config.is_start_requested = FALSE;
      session_obj->packet_exchange_info.mailbox_info.config.is_reseted = FALSE;
      session_obj->packet_exchange_info.mailbox_info.config.is_time_ref_received = FALSE;
      session_obj->packet_exchange_info.mailbox_info.config.is_rx_expiry_proc_disabled = FALSE;
      session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
      session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timestamp_us = 0;
      ( void )mmstd_memset(
                &session_obj->packet_exchange_info.mailbox_info.stats, 0,
                sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) );


      { /* BACKWARD COMPATIBILITY */
        session_obj->common_cal.is_registered = FALSE;
        session_obj->common_cal.is_evaluated = FALSE;
        session_obj->common_cal.is_calibrate_needed = FALSE;
        session_obj->common_cal.required_index_mem_size = 0;
        session_obj->common_cal.table_handle = NULL;
        session_obj->common_cal.vsm_mem_handle = 0;
        ( void ) mmstd_memset( &session_obj->common_cal.matching_entries, 0,
                               sizeof( session_obj->common_cal.matching_entries ) );
        session_obj->common_cal.num_matching_entries = 0;
        session_obj->common_cal.set_param_rsp_cnt = 0;
        session_obj->common_cal.set_param_failed_rsp_cnt = 0;
      }

      session_obj->static_cal.is_registered = FALSE;
      session_obj->static_cal.is_calibrate_needed = FALSE;
      session_obj->static_cal.table_handle = APR_NULL_V;
      session_obj->static_cal.query_handle = APR_NULL_V;
      ( void ) mmstd_memset( &session_obj->static_cal.matching_entries, 0,
                             sizeof( session_obj->static_cal.matching_entries ) );
      ( void ) mmstd_memset( session_obj->static_cal.query_key_columns, 0,
                             sizeof( session_obj->static_cal.query_key_columns ) );

      session_obj->kpps_info.enc = 0;
      session_obj->kpps_info.dec = 0;
      session_obj->kpps_info.dec_pp = 0;
      session_obj->is_kpps_changed = FALSE;

      session_obj->eamr_mode_change_notification_info.is_enabled = FALSE;
      session_obj->eamr_mode_change_notification_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->eamr_mode_change_notification_info.client_port = APR_NULL_V;
      session_obj->eamr_mode_change_notification_info.mode = VSS_ISTREAM_EAMR_MODE_NARROWBAND;
        /**<
         * The default mode of eAMR is narrowband. Refer to the comments under
         * eamr_mode_change_notification_info in cvs_session_object_t for
         * details.
         */

      session_obj->evs_bandwidth_change_notification_info.is_enabled = FALSE;
      session_obj->evs_bandwidth_change_notification_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->evs_bandwidth_change_notification_info.client_port = APR_NULL_V;
      session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET;
      session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET;    
        /**<
         * The default RX vocoder bandwidth of EVS is not set until the first 
         * VSM_EVT_VOC_OPERATING_MODE_UPDATE when EVS is used. 
         */       
		 
      session_obj->packet_logging_info.voice_call_num = 1;
      session_obj->packet_logging_info.rx_packet_seq_num = 0;
      session_obj->packet_logging_info.tx_packet_seq_num = 0;

      session_obj->ref_cnt = 1;
    }

    /* Complete initialization. */
    session_obj->header.type = CVS_OBJECT_TYPE_ENUM_SESSION;
      /* Mark the actual object type here to indicate that the object has been
       * fully initialized. Otherwise, on an error the destructor called at
       * the checkpoint handler would inadvertently try to free resources in
       * the object that have not been allocated yet or have already been
       * freed by the clean up sequence.
       */

    session_obj->cached_voc_properties_qcelp13k.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_qcelp13k.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_qcelp13k.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_qcelp13k.reduced_rate_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_evrc.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_evrc.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_evrc.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_evrc.reduced_rate_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_4gvnb.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnb.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnb.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_4gvwb.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvwb.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvwb.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_4gvnw.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_4gvnw2k.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw2k.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw2k.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_4gvnw2k.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_amr.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_amr.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_amrwb.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_amrwb.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_eamr.rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
    session_obj->cached_voc_properties_eamr.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_efr.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_fr.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_hr.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_g711_alaw.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_g711_mulaw.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_g711_linear.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_g729.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    session_obj->cached_voc_properties_evs.mode = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
    session_obj->cached_voc_properties_evs.bandwidth = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
    session_obj->cached_voc_properties_evs.channel_aware_enabled = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
    session_obj->cached_voc_properties_evs.fec_offset = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
    session_obj->cached_voc_properties_evs.fer_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
    session_obj->cached_voc_properties_evs.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;

    *ret_session_obj = session_obj;

    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_session_object(): failed, checkpoint=%d",
                                          checkpoint );

  switch ( checkpoint )
  {
  case 2:
    ( void ) apr_lock_destroy( session_obj->lock );
    /*-fallthru */
  case 1:
    ( void ) cvs_free_object( ( cvs_object_t* ) session_obj );
    /*-fallthru */

  default:
    break;
  }

  return rc;
}

static int32_t cvs_create_indirection_object (
  uint16_t client_addr,
  char_t* req_session_name,
  uint32_t req_session_name_size,
  uint32_t access_bits,
  cvs_indirection_object_t** ret_indirect_obj,
  cvs_session_object_t** ret_session_obj
)
{
  int32_t rc;
  uint32_t result;
  uint32_t dst_len;
  uint32_t checkpoint = 0;
  cvs_indirection_object_t* indirect_obj;
  cvs_session_object_t* session_obj = NULL;
  cvs_generic_item_t* generic_item;

  for ( ;; )
  {
    if ( ( ret_indirect_obj == NULL ) ||
         ( ret_session_obj == NULL )  ||
         ( req_session_name_size > CVS_MAX_SESSION_NAME_SIZE ) )
    {
      rc = APR_EBADPARAM;
      break;
    }

    /* See if a session with this name has already been created. */
    if ( req_session_name_size > 0 )
    {
      generic_item = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

      for ( ;; )
      {
        rc = apr_list_get_next( &cvs_session_q,
                                ( ( apr_list_node_t* ) generic_item ),
                                ( ( apr_list_node_t** ) &generic_item ) );
        if ( rc ) break;

        rc = cvs_get_typed_object( generic_item->handle,
                                   CVS_OBJECT_TYPE_ENUM_SESSION,
                                   ( ( cvs_object_t** ) &session_obj ) );
        CVS_PANIC_ON_ERROR( rc );

        if ( mmstd_strncmp( session_obj->session_name,
                            sizeof( session_obj->session_name ),
                            req_session_name,
                            req_session_name_size )
             == 0 )
        {
          /* Found a session with the requested name. */
          break;
        }
        else
        {
          /* Keep looking. */
          session_obj = NULL;
        }
      }
    }

    rc = cvs_mem_alloc_object( sizeof( cvs_indirection_object_t ),
                               ( ( cvs_object_t** ) &indirect_obj ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      break;
    }
    checkpoint = 1;

    if ( session_obj == NULL )
    {
      /* Didn't find a session with the requested name, so create a new session. */

      if ( cvs_session_q.size == CVS_MAX_NUM_SESSIONS )
      {
        rc = APR_ENORESOURCE;
        break;
      }

      rc = cvs_create_session_object( &session_obj );
      if ( rc )
      {
        break;
      }
      checkpoint = 2;

      if ( req_session_name_size > 0 )
      { /* Save the session name. */
        dst_len = MMSTD_MIN( sizeof( session_obj->session_name ),
                             req_session_name_size );
        result = mmstd_strlcpy( session_obj->session_name, req_session_name,
                                dst_len );

        if ( result >= dst_len )
        {/* Truncation happened. */
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_indirection_object: ERROR, input truncation" );
          rc = APR_EBADPARAM;
          break;
        }
      }

      /* Add the new session to the session tracking list. */
      rc = apr_list_remove_head( &cvs_session_list_free_q,
                                 ( ( apr_list_node_t** ) &generic_item ) );

      generic_item->handle = session_obj->header.handle;

      ( void ) apr_list_add_tail( &cvs_session_q, &generic_item->link );
    }

    { /* Initialize the indirection object. */
      indirect_obj->header.type = CVS_OBJECT_TYPE_ENUM_INDIRECTION;

      indirect_obj->client_addr = client_addr;
      indirect_obj->session_handle = session_obj->header.handle;
      indirect_obj->access_bits = access_bits;
    }

    { /* Associate the indirection object to the session object. */
      rc = apr_list_remove_head( &session_obj->free_item_q,
                                 ( ( apr_list_node_t** ) &generic_item ) );
      CVS_PANIC_ON_ERROR( rc );

      generic_item->handle = indirect_obj->header.handle;

      ( void ) apr_list_add_tail( &session_obj->indirection_q, &generic_item->link );
    }

    *ret_indirect_obj = indirect_obj;
    *ret_session_obj = session_obj;

    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_indirection_object(): failed, checkpoint=%d",
                                          checkpoint );

  switch ( checkpoint )
  {
  case 2:
    {
      rc = cvs_free_object( ( cvs_object_t* ) session_obj );
      CVS_PANIC_ON_ERROR( rc );
    }
    /*-fallthru */

  case 1:
    {
      /* Make sure the indirect_obj is not linked to a session when freeing it. */
      indirect_obj->header.type = CVS_OBJECT_TYPE_ENUM_UNINITIALIZED;

      rc = cvs_free_object( ( cvs_object_t* ) indirect_obj );
      CVS_PANIC_ON_ERROR( rc );
    }
    /*-fallthru */

  default:
    break;
  }

  return rc;
}

/* This function assumes the input parameters are good. */
static int32_t cvs_find_object_from_session (
  apr_list_t* list,
  uint32_t handle,
  cvs_generic_item_t** ret_generic_item
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item;

  generic_item = ( ( cvs_generic_item_t* ) &list->dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( list,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc )
    {
      return APR_EFAILED;
    }

    if ( generic_item->handle == handle )
    {
      break;
    }
  }

  *ret_generic_item = generic_item;

  return APR_EOK;
}

static int32_t cvs_destroy_indirection_object (
  cvs_indirection_object_t* indirect_obj
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_generic_item_t* generic_item;

  rc = cvs_get_typed_object( indirect_obj->session_handle,
                             CVS_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvs_object_t** ) &session_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  if ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                      indirect_obj->access_bits ) )
  {
    /* Write access. Lock them. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->master_client_addr = APRV2_PKT_INIT_ADDR_V;
    session_obj->master_client_port = APR_NULL_V;
    session_obj->master_cvs_port = APR_NULL_V;
    ( void )apr_lock_leave( session_obj->lock );
  }

  /* Disassociate the indirection object from the session object. */
  rc = cvs_find_object_from_session( &session_obj->indirection_q,
                                     indirect_obj->header.handle,
                                     &generic_item );
  CVS_PANIC_ON_ERROR( rc );

  ( void ) apr_list_delete( &session_obj->indirection_q, &generic_item->link );
  ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );

  return APR_EOK;
}

static int32_t cvs_destroy_session_object (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item;
  cvs_indirection_object_t* indirect_object;

  /* Remove session object. */
  generic_item = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc )
      break;

    if ( generic_item->handle == session_obj->header.handle )
    {
      apr_list_delete ( &cvs_session_q, ( apr_list_node_t* ) generic_item );
      apr_list_add_tail( &cvs_session_list_free_q, &generic_item->link );
      break;
    }
  }

  /* Remove indirect object. */
  generic_item = ( ( cvs_generic_item_t* )&session_obj->indirection_q.dummy );
  rc = apr_list_get_next( &session_obj->indirection_q,
                          ( ( apr_list_node_t* ) generic_item ),
                          ( ( apr_list_node_t** ) &generic_item ) );
  if ( rc )
  {
    /* At this point, we should have exact one item here. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_destroy_session_object(): " \
           "failed to get indirect object, rc = (0x%08x)", rc );
  }
  else
  {
    ( void ) cvs_get_typed_object( generic_item->handle,
                                   CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                                  ( ( cvs_object_t** ) &indirect_object ) );
    ( void ) apr_list_delete( &session_obj->indirection_q, &generic_item->link );
    ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
    ( void ) cvs_mem_free_object( ( cvs_object_t* ) indirect_object );
  }

  ( void )apr_lock_destroy( session_obj->lock );

  ( void )cvs_free_object( ( cvs_object_t* ) session_obj );

  return APR_EOK;
}

static int32_t cvs_create_simple_job_object (
  uint32_t context_handle,
  cvs_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvs_mem_alloc_object( sizeof( cvs_simple_job_object_t ),
                             ( ( cvs_object_t** ) &job_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_simple_job_object(): failed to allocate object, result: 0x%08X",
                                            rc );
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    job_obj->header.type = CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    job_obj->context_handle = context_handle;
    cvs_set_default_response_table( job_obj->fn_table );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_result_rsp_fn;
    job_obj->is_accepted = FALSE;
    job_obj->is_completed = FALSE;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvs_create_track_job_object (
  uint32_t context_handle,
  cvs_track_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvs_track_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvs_mem_alloc_object( sizeof( cvs_track_job_object_t ),
                             ( ( cvs_object_t** ) &job_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_track_job_object(): failed to allocate object, result: 0x%08X",
                                            rc );
    return APR_ENORESOURCE;
  }

  { /* Initialize the track job object. */
    job_obj->header.type = CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    job_obj->context_handle = context_handle;
    cvs_set_default_response_table( job_obj->fn_table );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_result_rsp_fn;
    job_obj->is_accepted = FALSE;
    job_obj->is_completed = FALSE;
    job_obj->status = APR_UNDEFINED_ID_V;
    job_obj->orig_src_service = APRV2_PKT_INIT_ADDR_V;
    job_obj->orig_src_port = APR_NULL_V;
    job_obj->orig_dst_port = APR_NULL_V;
    job_obj->orig_opcode = APR_UNDEFINED_ID_V;
    job_obj->orig_token = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvs_create_sequencer_job_object (
  cvs_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvs_sequencer_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvs_mem_alloc_object( sizeof( cvs_sequencer_job_object_t ),
                             ( ( cvs_object_t** ) &job_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = CVS_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvs_free_object (
  cvs_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Perform object-specific clean-up necessary. */
  switch ( object->header.type )
  {
  case CVS_OBJECT_TYPE_ENUM_UNINITIALIZED:
    break;

  case CVS_OBJECT_TYPE_ENUM_SESSION:
    break;

  case CVS_OBJECT_TYPE_ENUM_INDIRECTION:
    ( void ) cvs_destroy_indirection_object( &object->indirection );
    break;

  case CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB:
    break;

  case CVS_OBJECT_TYPE_ENUM_SEQUENCER_JOB:
    break;

  case CVS_OBJECT_TYPE_ENUM_INVALID:
    break;

  default:
    break;
  }

  /* Free the object memory and object handle. */
  ( void ) cvs_mem_free_object( object );

  return APR_EOK;
}

/****************************************************************************
 * CVS CALIBRATION HELPER FUNCTIONS                                          *
 ****************************************************************************/

/* BACKWARD COMPATIBILITY */
static int32_t cvs_calibrate_common (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;
  cvd_cal_column_t columns[ CVS_NUM_COMMON_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;
  uint32_t set_param_cnt;

  if ( session_obj == NULL )
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    session_obj->common_cal.num_matching_entries = 0;
    session_obj->common_cal.set_param_rsp_cnt = 0;
    session_obj->common_cal.set_param_failed_rsp_cnt = 0;

    if ( session_obj->common_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_calibrate_common(): Calibration data is not registered. " \
                                           "Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    mmstd_memset( columns, 0, sizeof( columns ) );
    columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    columns[ 0 ].value = session_obj->target_set.system_config.network_id;

    switch ( session_obj->active_set.direction )
    {
      case CVS_DIRECTION_RX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.rx_pp_sr;
        cal_key.num_columns = 2;
      }
      break;

      case CVS_DIRECTION_TX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.tx_pp_sr;
        cal_key.num_columns = 2;
      }
      break;

      case CVS_DIRECTION_RX_TX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.rx_pp_sr;
        columns[ 2 ].value = session_obj->target_set.system_config.tx_pp_sr;
        cal_key.num_columns = 3;
      }
      break;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
    }

    cal_key.columns = columns;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_calibrate_common(): Network 0x%08X, Rx SR %d, Tx SR %d",
                                          columns[0].value,
                                          columns[1].value,
                                          columns[ 2 ].value );

    rc = cvd_cal_query_table(
           session_obj->common_cal.table_handle,
           &cal_key, sizeof( session_obj->common_cal.matching_entries ),
           session_obj->common_cal.matching_entries,
           &session_obj->common_cal.num_matching_entries );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_calibrate_common(): cvd_cal_query_table failed, " \
                                              "rc=0x%08X. Not calibrating.",
                                              rc );
      break;
    }

    if ( session_obj->common_cal.num_matching_entries == 0 )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_calibrate_common(): Cannot find a matching entry. " \
                                           "Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_calibrate_common(): Found %d matched entries",
                                          session_obj->common_cal.num_matching_entries );

    for ( set_param_cnt = 0; set_param_cnt < session_obj->common_cal.num_matching_entries; ++set_param_cnt )
    {
      rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVS_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_calibrate_common_rsp_fn;

      cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                              session_obj->common_cal.matching_entries[ set_param_cnt ].start_ptr );
      set_param.payload_size = session_obj->common_cal.matching_entries[ set_param_cnt ].size;
      set_param.mem_map_handle = session_obj->common_cal.vsm_mem_handle;

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_param, sizeof( set_param ) );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }

    break;
  }

  return rc;
}

static int32_t cvs_calibrate_static (
  cvs_session_object_t* session_obj,
  cvs_simple_job_object_t* job_obj
)
{
  int32_t rc;
  voice_cmd_set_param_v3_t set_param;
  cvd_cal_key_t cal_key;
  cvd_cal_column_t* query_key_columns;
  uint32_t column_index = 0;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  if ( session_obj == NULL )
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->static_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_calibrate_static(): Static calibration data is not registered. " \
                                           "Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    query_key_columns = session_obj->static_cal.query_key_columns;

    mmstd_memset( query_key_columns, 0, sizeof( session_obj->static_cal.query_key_columns ) );
    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.network_id;
    column_index += 1;

    switch ( session_obj->active_set.direction )
    {
    case CVS_DIRECTION_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;
      }
      break;

    case CVS_DIRECTION_RX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    case CVS_DIRECTION_RX_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_MEDIA_ID;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.media_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_FEATURE;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.feature;

    cal_key.columns = query_key_columns;
    cal_key.num_columns = ( column_index + 1 );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS Static configured for the following settings. cvs_calibrate_static(): Network 0x%08X, Tx PP SR %d, Rx PP SR %d",
                                          session_obj->target_set.system_config.network_id,
                                          session_obj->target_set.system_config.tx_pp_sr,
                                          session_obj->target_set.system_config.rx_pp_sr );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS Static configured for the following settings. cvs_calibrate_static(): Tx voc op mode 0x%08X, Rx voc op mode 0x%08X, Media ID 0x%08X",
                                          session_obj->target_set.system_config.tx_voc_op_mode,
                                          session_obj->target_set.system_config.rx_voc_op_mode,
                                          session_obj->target_set.system_config.media_id );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS Static configured for the following settings. cvs_calibrate_static(): Feature 0x%08X.",
                                          session_obj->target_set.system_config.feature );

    rc = cvd_cal_query_init(
           session_obj->static_cal.table_handle, &cal_key,
           session_obj->static_cal.matching_entries,
           sizeof( session_obj->static_cal.matching_entries ),
           &session_obj->static_cal.query_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_calibrate_static(): cvd_cal_query_table_init failed, " \
                                              "rc=0x%08X. Not calibrating.",
                                              rc );
      break;
    }

    {
      /* Send the calibration query handle to VSM for retrieving and applying
       * the calibration values.
       */
      set_param.cal_handle = session_obj->static_cal.query_handle;

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V3,
             &set_param, sizeof( set_param ) );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }

    /* Log cal data. */
    {
      log_info_data.table_handle = session_obj->static_cal.table_handle;
      log_info_data.cal_query_handle = session_obj->static_cal.query_handle;
      log_info_data.data_seq_num = 0;

      log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) | 
                            ( session_obj->master_cvs_port ) );
      log_info.call_num = session_obj->target_set.system_config.call_num;
      log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
      log_info.data_container_header_size = sizeof ( log_info_data );
      log_info.data_container_header = &log_info_data;
      log_info.payload_size = 0;
      log_info.payload_buf = NULL;

      ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_STREAM_STATIC_OUTPUT,
                                ( void* )&log_info, sizeof( log_info ) );
    }

    break;
  }

  return rc;
}

/****************************************************************************
 * CVS SUBSYSTEM RESTART (SSR) HELPER FUNCTIONS                             *
 ****************************************************************************/

/* Stop each of the VPCM sessions on behalf of CVS clients who resides in
 * a subsystem that is being restarted.
 */
static int32_t cvs_ssr_stop_vpcm (
  uint8_t domain_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t* job_obj;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = cvs_get_typed_object( generic_item_1->handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    if ( ( session_obj->vpcm_info.is_enabled == TRUE ) &&
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                          session_obj->vpcm_info.client_addr ) == domain_id ) )
    {
      rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
      CVS_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
        cvs_ssr_cleanup_cmd_result_count_rsp_fn;

      /* The command can be sent to any of the session indirection handles.
       * Use the first indirection handle in the session indirection queue.
       */
      apr_list_peak_head( &session_obj->indirection_q,
                          ( ( apr_list_node_t** ) &generic_item_2 ) );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, APR_NULL_V,
             cvs_my_addr, ( ( uint16_t ) generic_item_2->handle ),
             job_obj->header.handle,
             VSS_IVPCM_CMD_STOP,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_my_addr );

      cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
    }
  }

  return APR_EOK;
}

/* For each of the CVS sessions, deregister OOB TTY on behalf of CVS clients
 * who reside in a subsystem that is being restarted.
 */
static int32_t cvs_ssr_deregister_oob_tty (
  uint8_t domain_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t* job_obj;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = cvs_get_typed_object( generic_item_1->handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    if ( ( session_obj->tty_info.is_ittyoob_registered == TRUE ) &&
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                          session_obj->tty_info.ittyoob_client_addr ) == domain_id ) )
    {
      rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
      CVS_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
        cvs_ssr_cleanup_cmd_result_count_rsp_fn;

      /* The command can be sent to any of the session indirection handles.
       * Use the first indirection handle in the session indirection queue.
       */
      apr_list_peak_head( &session_obj->indirection_q,
                          ( ( apr_list_node_t** ) &generic_item_2 ) );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, APR_NULL_V,
             cvs_my_addr, ( ( uint16_t ) generic_item_2->handle ),
             job_obj->header.handle,
             VSS_ITTYOOB_CMD_DEREGISTER,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_my_addr );

      cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
    }
  }

  return APR_EOK;
}

/* For each of the CVS sessions, cancel the change event notifications on
 * behalf of CVS clients who reside in a subsystem that is being restarted.
 */
static int32_t cvs_ssr_cancel_event_class_registration (
  uint8_t domain_id,
  uint32_t class_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t* job_obj;
  vss_inotify_cmd_cancel_event_class_t cancel_event_class;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;
  
  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );
  
  switch ( class_id )
  {
    case VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE:
    case VSS_ISTREAM_EVENT_CLASS_EVS_BANDWIDTH_CHANGE:
      {
        cancel_event_class.class_id = class_id;   

        for ( ;; )
        {
          rc = apr_list_get_next( &cvs_session_q,
                                  ( ( apr_list_node_t* ) generic_item_1 ),
                                  ( ( apr_list_node_t** ) &generic_item_1 ) );
          if ( rc ) break;

          rc = cvs_get_typed_object( generic_item_1->handle,
                                     CVS_OBJECT_TYPE_ENUM_SESSION,
                                     ( ( cvs_object_t** ) &session_obj ) );
          CVS_PANIC_ON_ERROR( rc );

          if ( ( ( VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE == class_id ) &&
                 ( session_obj->eamr_mode_change_notification_info.is_enabled == TRUE ) &&
                 ( APR_GET_FIELD(
                    APRV2_PKT_DOMAIN_ID,
                    session_obj->eamr_mode_change_notification_info.client_addr ) == domain_id ) ) ||
               ( ( VSS_ISTREAM_EVENT_CLASS_EVS_BANDWIDTH_CHANGE == class_id ) &&
                 ( session_obj->evs_bandwidth_change_notification_info.is_enabled == TRUE ) &&
                 ( APR_GET_FIELD(
                    APRV2_PKT_DOMAIN_ID,
                    session_obj->evs_bandwidth_change_notification_info.client_addr ) == domain_id ) ) )
          {
            rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
            CVS_PANIC_ON_ERROR( rc );
            job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
              cvs_ssr_cleanup_cmd_result_count_rsp_fn;

            /* The command can be sent to any of the session indirection handles.
             * Use the first indirection handle in the session indirection queue.
             */
            apr_list_peak_head( &session_obj->indirection_q,
                                ( ( apr_list_node_t** ) &generic_item_2 ) );

            rc = __aprv2_cmd_alloc_send(
                   cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   cvs_my_addr, APR_NULL_V,
                   cvs_my_addr, ( ( uint16_t ) generic_item_2->handle ),
                   job_obj->header.handle,
                   VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS,
                   &cancel_event_class, sizeof( cancel_event_class ) );
            CVS_COMM_ERROR( rc, cvs_my_addr );

            cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
          }
        }   
      }
      break;
      
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_ssr_cancel_event_class_registration: Class ID 0x%08x not recognized",
                                                class_id );       
      }
      break;    
  }

  return APR_EOK;
}

/* For each of the CVS sessions, reset the packet exchange mode to in-band on
 * behalf of CVS clients who reside in a subsystem that is being restarted.
 */
static int32_t cvs_ssr_reset_packet_exchange_mode (
  uint8_t domain_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirection_obj;
  cvs_simple_job_object_t* job_obj;
  vss_ipktexg_cmd_set_mode_t set_pktexg_mode;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  set_pktexg_mode.mode = VSS_IPKTEXG_MODE_IN_BAND;

  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = cvs_get_typed_object( generic_item_1->handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    if ( ( ( session_obj->packet_exchange_info.mode ==
             VSS_IPKTEXG_MODE_OUT_OF_BAND ) ||
           ( session_obj->packet_exchange_info.mode ==
             VSS_IPKTEXG_MODE_MAILBOX ) ) &&
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                          session_obj->master_client_addr ) == domain_id ) )
    {
      /* Find the full control session handle to send the
       * VSS_IPKTEXG_CMD_SET_MODE command, as this command is only allowed to
       * be issued to full control session handle.
       */
      generic_item_2 = ( ( cvs_generic_item_t* ) &session_obj->indirection_q.dummy );

      for ( ;; )
      {
        rc = apr_list_get_next( &session_obj->indirection_q,
                                ( ( apr_list_node_t* ) generic_item_2 ),
                                ( ( apr_list_node_t** ) &generic_item_2 ) );
        if ( rc )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_ssr_reset_packet_exchange_mode(): " \
                                                "Unable to find the full control session handle" );
          break;
        }

        rc = cvs_get_typed_object( generic_item_2->handle,
                                   CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                                   ( ( cvs_object_t** ) &indirection_obj ) );

        if ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                            indirection_obj->access_bits ) )
        {
          rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
          CVS_PANIC_ON_ERROR( rc );
          job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
            cvs_ssr_cleanup_cmd_result_count_rsp_fn;

          /* The command must be sent to the full control session handle. */
          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, APR_NULL_V,
                 cvs_my_addr, ( ( uint16_t ) indirection_obj->header.handle ),
                 job_obj->header.handle,
                 VSS_IPKTEXG_CMD_SET_MODE,
                 &set_pktexg_mode, sizeof( set_pktexg_mode ) );
          CVS_COMM_ERROR( rc, cvs_my_addr );

          cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;

          break;
        }
      }
    }
  }

  return APR_EOK;
}

 /* For each of the CVS sessions, cancel the AVSync RX delay and TX timestamp notifications on
  * behalf of CVS clients who reside in a subsystem that is being restarted.
  */
static int32_t cvs_ssr_deregister_avsync (
  uint8_t domain_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t* job_obj;
  vss_inotify_cmd_cancel_event_class_t cancel_event_class;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = cvs_get_typed_object( generic_item_1->handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    if ( ( session_obj->avsync_client_rx_info.is_enabled == TRUE ) &&
         ( APR_GET_FIELD(
             APRV2_PKT_DOMAIN_ID,
             session_obj->avsync_client_rx_info.client_addr ) == domain_id ) )
    {
      cancel_event_class.class_id = VSS_IAVSYNC_EVENT_CLASS_RX;
      rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
      CVS_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
        cvs_ssr_cleanup_cmd_result_count_rsp_fn;

      /* The command can be sent to any of the session indirection handles.
       * Use the first indirection handle in the session indirection queue.
       */
      apr_list_peak_head( &session_obj->indirection_q,
                          ( ( apr_list_node_t** ) &generic_item_2 ) );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, APR_NULL_V,
             cvs_my_addr, ( ( uint16_t ) generic_item_2->handle ),
             job_obj->header.handle,
             VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS,
             &cancel_event_class, sizeof( cancel_event_class ) );
      CVS_COMM_ERROR( rc, cvs_my_addr );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ssr_deregister_avsync(): CVS Cancel IAVSYNC RX on behalf of Client Addr: 0x%04X",
                                             session_obj->avsync_client_rx_info.client_addr );

      cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
    }

    if ( ( session_obj->avsync_client_tx_info.is_enabled == TRUE ) &&
         ( APR_GET_FIELD(
             APRV2_PKT_DOMAIN_ID,
             session_obj->avsync_client_tx_info.client_addr ) == domain_id ) )
    {
      cancel_event_class.class_id = VSS_IAVSYNC_EVENT_CLASS_TX;
      rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
      CVS_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
        cvs_ssr_cleanup_cmd_result_count_rsp_fn;

      /* The command can be sent to any of the session indirection handles.
       * Use the first indirection handle in the session indirection queue.
       */
      apr_list_peak_head( &session_obj->indirection_q,
                          ( ( apr_list_node_t** ) &generic_item_2 ) );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, APR_NULL_V,
             cvs_my_addr, ( ( uint16_t ) generic_item_2->handle ),
             job_obj->header.handle,
             VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS,
             &cancel_event_class, sizeof( cancel_event_class ) );
      CVS_COMM_ERROR( rc, cvs_my_addr );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ssr_deregister_avsync(): CVS Cancel IAVSYNC TX on behalf of Client Addr: 0x%04X",
                                             session_obj->avsync_client_tx_info.client_addr );

      cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
    }
  }

  return APR_EOK;
}

/* Destroy the CVS session control handles (indirection handle) on behalf of
 * CVS clients who reside in a subsystem that is being restarted.
 */
static int32_t cvs_ssr_destroy_session (
  uint8_t domain_id
)
{
  int32_t rc;
  cvs_generic_item_t* generic_item_1;
  cvs_generic_item_t* generic_item_2;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirection_obj;
  cvs_simple_job_object_t* job_obj;

  cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( cvs_generic_item_t* ) &cvs_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = cvs_get_typed_object( generic_item_1->handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) &session_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    generic_item_2 = ( ( cvs_generic_item_t* ) &session_obj->indirection_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->indirection_q,
                              ( ( apr_list_node_t* ) generic_item_2 ),
                              ( ( apr_list_node_t** ) &generic_item_2 ) );
      if ( rc ) break;

      rc = cvs_get_typed_object( generic_item_2->handle,
                                 CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( cvs_object_t** ) &indirection_obj ) );

      if ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                          indirection_obj->client_addr ) == domain_id )
      {
        rc = cvs_create_simple_job_object( APR_NULL_V, &job_obj );
        CVS_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
          cvs_ssr_cleanup_cmd_result_count_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, APR_NULL_V,
               cvs_my_addr, ( ( uint16_t ) indirection_obj->header.handle ),
               job_obj->header.handle,
               APRV2_IBASIC_CMD_DESTROY_SESSION,
               NULL, 0 );
        CVS_COMM_ERROR( rc, cvs_my_addr );

        cvs_ssr_cleanup_cmd_tracking.num_cmd_issued++;
      }
    }
  }

  return APR_EOK;
}

/****************************************************************************
 * CVS MAILBOX PACKET EXCHANGE HELPER FUNCTIONS                             *
 ****************************************************************************/

/* Note that for mailbox packet exchange, all the client side errors detected
 * are logged with "Client error:" in the message. Errors that are not expected
 * to happen often are logged with HIGH message, errors that can happen during
 * steady state are logged with MED message. The errors are not logged using
 * ERROR message because such messages are usually intepreted by others to mean
 * CVD issues (results in CVD support overhead). In the future, CVD should
 * request for a new log code, e.g. CVD_CLIENT_ERROR_LOG_CODE to log all client
 * side errors.
 */

static int32_t cvs_pktexg_mailbox_get_max_pkt_size (
  uint32_t media_id,
  uint32_t* ret_max_enc_pkt_size,
  uint32_t* ret_max_dec_pkt_size
)
{
  uint32_t max_media_data_size;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_13K:
    max_media_data_size = 35;
    break;

  case VSS_MEDIA_ID_EVRC:
    max_media_data_size = 23;
    break;

  case VSS_MEDIA_ID_4GV_NB:
    max_media_data_size = 23;
    break;

  case VSS_MEDIA_ID_4GV_WB:
    max_media_data_size = 23;
    break;

  case VSS_MEDIA_ID_4GV_NW:
    max_media_data_size = 23;
    break;

  case VSS_MEDIA_ID_4GV_NW2K:
    max_media_data_size = 23;
    break;

  case VSS_MEDIA_ID_AMR_NB:
    max_media_data_size = 32;
    break;

  case VSS_MEDIA_ID_AMR_WB:
    max_media_data_size = 62;
    break;

  case VSS_MEDIA_ID_EAMR:
    max_media_data_size = 32;
    break;

  case VSS_MEDIA_ID_EFR:
    max_media_data_size = 32;
    break;

  case VSS_MEDIA_ID_FR:
    max_media_data_size = 34;
    break;

  case VSS_MEDIA_ID_HR:
    max_media_data_size = 15;
    break;

  case VSS_MEDIA_ID_PCM_8_KHZ:
    max_media_data_size = 320;
    break;

  case VSS_MEDIA_ID_PCM_16_KHZ:
    max_media_data_size = 640;
    break;
    
  case VSS_MEDIA_ID_PCM_32_KHZ:
    max_media_data_size = 1280;
    break;    

  case VSS_MEDIA_ID_PCM_44_1_KHZ:
    max_media_data_size = 1764;
    break;

  case VSS_MEDIA_ID_PCM_48_KHZ:
    max_media_data_size = 1920;
    break;

  case VSS_MEDIA_ID_G711_ALAW:
    max_media_data_size = 322;
    break;

  case VSS_MEDIA_ID_G711_MULAW:
    max_media_data_size = 322;
    break;

  case VSS_MEDIA_ID_G711_ALAW_V2:
    max_media_data_size = 161;
    break;

  case VSS_MEDIA_ID_G711_MULAW_V2:
    max_media_data_size = 161;
    break;

  case VSS_MEDIA_ID_G729:
    max_media_data_size = 22;
    break;

  case VSS_MEDIA_ID_G722:
    max_media_data_size = 161;
    break;

  default:
    return APR_EBADPARAM;
  }

  /* The maximum vocoder packet size includes both the packet header
   * (vss_ipktexg_mailbox_enc_packet_t for encoder and
   * vss_ipktexg_mailbox_dec_packet_t for decoder) and media data. The size
   * is rounded up to multiple of CVS_CACHE_LINE_SIZE to meet the cache line
   * size alignment.
   */
  if ( ret_max_enc_pkt_size != NULL )
  {
    *ret_max_enc_pkt_size =
      ( ( max_media_data_size + sizeof( vss_ipktexg_mailbox_enc_packet_t ) +
          CVS_CACHE_LINE_SIZE - 1 ) & ( ~ ( CVS_CACHE_LINE_SIZE - 1 ) ) );
  }

  if ( ret_max_dec_pkt_size != NULL )
  {
    *ret_max_dec_pkt_size =
      ( ( max_media_data_size + sizeof( vss_ipktexg_mailbox_dec_packet_t ) +
          CVS_CACHE_LINE_SIZE - 1 ) & ( ~ ( CVS_CACHE_LINE_SIZE - 1 ) ) );
  }

  return APR_EOK;
}

/* Identify the change in Rx path AVSync delay. If the delay changes by
 * CVS_MAILBOX_AVSYNC_RX_DELAY_UPDATE_THRESHOLD_US or more:
 * 1. Update the Rx path AVSync delay.
 * 2. If the client has registered for the VSS_IAVSYNC_EVENT_CLASS_RX event
 *    class, send the updated Rx path AVsync delay to the client.
 */
static int32_t cvs_pktexg_mailbox_update_rx_avsync_delay (
  cvs_session_object_t* session_obj,
  uint64_t current_timestamp_us,
  uint64_t dec_req_timestamp_us
)
{
  int32_t rc;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;
  uint32_t rx_req_and_dequeue_time_diff_us;
  uint32_t rx_delay_change;
  vss_iavsync_evt_rx_path_delay_t rx_path_delay;
  uint64_t current_time_since_start_requested;
  uint64_t dec_req_time_since_start_requested;

  if ( session_obj == NULL )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

  current_time_since_start_requested =
    ( current_timestamp_us - mailbox_config->request_to_start_timestamp_us );
  dec_req_time_since_start_requested =
    ( dec_req_timestamp_us - mailbox_config->request_to_start_timestamp_us );

  rx_req_and_dequeue_time_diff_us =
    ( ( uint32_t ) ( current_time_since_start_requested - dec_req_time_since_start_requested ) );

  if ( rx_req_and_dequeue_time_diff_us > mailbox_config->rx_req_and_dequeue_time_diff_us )
  {
    rx_delay_change =
      ( rx_req_and_dequeue_time_diff_us - mailbox_config->rx_req_and_dequeue_time_diff_us );

    if ( rx_delay_change >= CVS_MAILBOX_AVSYNC_RX_DELAY_UPDATE_THRESHOLD_US )
    {
      session_obj->avsync_delay_info.total_rx_delay += rx_delay_change;
      mailbox_config->rx_req_and_dequeue_time_diff_us = rx_req_and_dequeue_time_diff_us;
    }
  }
  else
  {
    rx_delay_change =
      ( mailbox_config->rx_req_and_dequeue_time_diff_us - rx_req_and_dequeue_time_diff_us );

    if ( rx_delay_change >= CVS_MAILBOX_AVSYNC_RX_DELAY_UPDATE_THRESHOLD_US )
    {
      session_obj->avsync_delay_info.total_rx_delay -= rx_delay_change;
      mailbox_config->rx_req_and_dequeue_time_diff_us = rx_req_and_dequeue_time_diff_us;
    }
  }

  if ( ( session_obj->avsync_client_rx_info.is_enabled == TRUE ) &&
       ( rx_delay_change >= CVS_MAILBOX_AVSYNC_RX_DELAY_UPDATE_THRESHOLD_US ) )
  {
    rx_path_delay.delay_us = session_obj->avsync_delay_info.total_rx_delay;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->avsync_client_rx_info.client_addr,
           session_obj->avsync_client_rx_info.client_port,
           0, VSS_IAVSYNC_EVT_RX_PATH_DELAY,
           &rx_path_delay, sizeof( rx_path_delay ) );
    CVS_COMM_ERROR( rc, session_obj->avsync_client_rx_info.client_addr );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_pktexg_mailbox_update_rx_avsync_delay(): IAVSYNC RX Client Addr: 0x%04X",
                                            session_obj->avsync_client_rx_info.client_addr );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_pktexg_mailbox_update_rx_avsync_delay(): IAVSYNC RX Client Port: 0x%04X",
                                            session_obj->avsync_client_rx_info.client_port );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_pktexg_mailbox_update_rx_avsync_delay(): IAVSYNC RX Path Delay: %d",
                                            rx_path_delay.delay_us );

  }

  return APR_EOK;
}

/* Check if there is a pending vocoding request in the circular buffer. */
static int32_t cvs_pktexg_mailbox_is_voc_req_exist (
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* circ_buf,
  uint32_t circ_buf_mem_size,
  uint32_t voc_req_unit_size
)
{
  int32_t rc;
  uint32_t circ_buf_data_size;

  for ( ;; )
  {
    if ( circ_buf == NULL )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    circ_buf_data_size = 
      ( circ_buf_mem_size - sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );

    if ( circ_buf->read_offset == circ_buf->write_offset )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_is_voc_req_exist(): " \
           "Client error: Circular buffer empty." );
      rc = APR_EFAILED;
      break;
    }

    if ( ( circ_buf->write_offset % voc_req_unit_size ) != 0 )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_is_voc_req_exist(): " \
             "Client error: Write offset %d is not a multiple of request size %d.",
             circ_buf->write_offset, voc_req_unit_size );
      rc = APR_EFAILED;
      break;
    }

    if ( circ_buf->write_offset >= circ_buf_data_size )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_is_voc_req_exist(): " \
             "Client error: Write offset %d is at or beyond the end of the circular buffer. " \
             "Total circular buffer size for holding request is %d.", 
             circ_buf->write_offset, circ_buf_data_size );

      rc = APR_EFAILED;
      break;
    }

    if ( circ_buf->read_offset >= circ_buf_data_size )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_is_voc_req_exist(): " \
             "Client error: Read offset %d is corrupted by client, which is at or " \
             "beyond the end of the circular buffer. Total circular buffer size for " \
             "holding request is %d.",
             circ_buf->read_offset, circ_buf_data_size );

      rc = APR_EFAILED;
      break;
    }

    return APR_EOK;
  }

  return rc;
}

/* Validate and get an encoder buffer. */
static int32_t cvs_pktexg_mailbox_validate_and_get_enc_buffer (
  cvs_session_object_t* session_obj,
  vss_ipktexg_mailbox_enc_request_t* enc_req,
  vss_ipktexg_mailbox_enc_packet_t** ret_enc_buffer
)
{
  int32_t rc;
  cvd_virt_addr_t enc_buf_virt_addr;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;

  for ( ;; )
  {
    if ( ( session_obj == NULL ) || ( enc_req == NULL ) ||
         ( ret_enc_buffer == NULL ) )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

    rc = cvd_mem_mapper_validate_attributes_align(
           mailbox_config->cvs_mem_handle, enc_req->mem_address );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_enc_packet(): " \
             "Client error: Mis-aligned encoder packet buffer mem_address: 0x%016X.",
             enc_req->mem_address );

      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align(
           mailbox_config->cvs_mem_handle, enc_req->mem_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_enc_packet(): " \
             "Client error: Mis-aligned encoder packet buffer size: %d.",
             enc_req->mem_size );

      break;
    }

    if ( enc_req->mem_size < mailbox_config->max_enc_pkt_size )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_enc_packet(): " \
             "Client error: Encoder packet buffer size %d not large enough to hold max encoder packet size %d.",
             enc_req->mem_size, mailbox_config->max_enc_pkt_size );

      rc = APR_EFAILED;
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region(
           mailbox_config->cvs_mem_handle, enc_req->mem_address, enc_req->mem_size );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_enc_packet(): " \
             "Client error: Encoder packet buffer is not within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d.",
             mailbox_config->cvs_mem_handle, enc_req->mem_address, enc_req->mem_size );

      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( mailbox_config->cvs_mem_handle,
                                             enc_req->mem_address,
                                             &enc_buf_virt_addr );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_validate_and_get_enc_packet(): " \
             "Cannot get virtual address, mem_handle: 0x%08X, addr: 0x%016X.",
             mailbox_config->cvs_mem_handle, enc_req->mem_address );

      break;
    }

    *ret_enc_buffer = 
      ( ( vss_ipktexg_mailbox_enc_packet_t* ) enc_buf_virt_addr.ptr );

    return APR_EOK;
  }

  return rc;
}

/* Validate and get a decoder buffer. */
static int32_t cvs_pktexg_mailbox_validate_and_get_dec_buffer (
  cvs_session_object_t* session_obj,
  vss_ipktexg_mailbox_dec_request_t* dec_req,
  vss_ipktexg_mailbox_dec_packet_t** ret_dec_buffer
)
{
  int32_t rc;
  cvd_virt_addr_t dec_buf_virt_addr;
  vss_ipktexg_mailbox_dec_packet_t* dec_buffer;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;

  for ( ;; )
  {
    if ( ( session_obj == NULL ) || ( dec_req == NULL ) ||
         ( ret_dec_buffer == NULL ) )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

    rc = cvd_mem_mapper_validate_attributes_align(
           mailbox_config->cvs_mem_handle, dec_req->mem_address );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Mis-aligned decoder packet buffer mem_address: 0x%016X.",
             dec_req->mem_address );

      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( 
           mailbox_config->cvs_mem_handle, dec_req->mem_address, dec_req->mem_size );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Decoder packet buffer is not within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d.",
             mailbox_config->cvs_mem_handle, dec_req->mem_address, dec_req->mem_size );

      break;
    }

    if ( dec_req->mem_size < sizeof( vss_ipktexg_mailbox_dec_packet_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Invalid decoder packet buffer mem size %d < decoder packet header size %d.",
             dec_req->mem_size, sizeof( vss_ipktexg_mailbox_dec_packet_t ) );

      rc = APR_EFAILED;
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( mailbox_config->cvs_mem_handle,
                                             dec_req->mem_address,
                                             &dec_buf_virt_addr );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Cannot get virtual address, mem_handle: 0x%08X, addr: 0x%016X.",
             mailbox_config->cvs_mem_handle, dec_req->mem_address );

      break;
    }

    ( void ) cvd_mem_mapper_cache_invalidate_v2(
      &dec_buf_virt_addr, dec_req->mem_size );

    dec_buffer = ( ( vss_ipktexg_mailbox_dec_packet_t* ) dec_buf_virt_addr.ptr );

    if ( dec_buffer->minor_version != 0 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Unsupported decoder packet minor version %d. Supported minor version is 0. ", 
             dec_buffer->minor_version );

      rc = APR_EFAILED;
      break;
    }

    if ( dec_buffer->media_id != session_obj->active_set.media_id )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Invalid decoder packet media ID 0x%08X. Media ID configured on the stream is 0x%08X. ", 
             dec_buffer->media_id, session_obj->active_set.media_id );

      rc = APR_EFAILED;
      break;
    }

    if ( dec_buffer->data_size >
         ( CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE - sizeof( vsm_oob_pkt_exchange_header_t ) ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): "\
             "Client error: Decoder packet data size %d larger than maximum possible decoder packet data size %d.",
             dec_buffer->data_size,
             ( CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE - sizeof( vsm_oob_pkt_exchange_header_t ) ) );

      rc = APR_EFAILED;
      break;
    }

    if ( dec_buffer->data_size > 
         ( dec_req->mem_size - sizeof( vss_ipktexg_mailbox_dec_packet_t ) ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_validate_and_get_dec_buffer(): " \
             "Client error: Decoder packet data size %d does not fit within the decoder packet buffer of size %d.",
             dec_buffer->data_size, dec_req->mem_size );

      rc = APR_EFAILED;
      break;
    }

    *ret_dec_buffer = dec_buffer;

    return APR_EOK;
  }

  return rc;
}

/* Complete a vocoding request by advancing the circular buffer read offset. */
static int32_t cvs_pktexg_mailbox_complete_voc_request (
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* circ_buf,
  uint32_t circ_buf_mem_size,
  uint32_t voc_req_unit_size
)
{
  uint32_t circ_buf_data_size;
  cvd_virt_addr_t read_offset_virt_addr;

  if ( circ_buf == NULL )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  circ_buf_data_size = 
    ( circ_buf_mem_size - sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );

  circ_buf->read_offset = 
    ( ( circ_buf->read_offset + voc_req_unit_size ) % circ_buf_data_size );

  read_offset_virt_addr.ptr = &circ_buf->read_offset;

  ( void ) cvd_mem_mapper_cache_flush_v2( 
             &read_offset_virt_addr, sizeof( circ_buf->read_offset ) );

  return APR_EOK;
}

/* Caller takes care of locking. */
static int32_t cvs_pktexg_mailbox_tx_processing (
  cvs_session_object_t* session_obj,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* tx_circ_buf;
  cvd_virt_addr_t tx_circ_buf_virt_addr;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;
  vss_ipktexg_mailbox_enc_request_t* enc_req;
  vss_ipktexg_mailbox_enc_packet_t* mailbox_enc_packet;
  vsm_oob_pkt_exchange_header_t* oob_enc_packet;
  cvd_virt_addr_t mailbox_enc_packet_virt_addr;
  uint64_t current_timestamp_us = 0;
  uint64_t capture_timestamp_us;
  cvs_mailbox_packet_exchange_stats_t* stats;

  for ( ;; )
  {
    /* Get the current timestamp. */
    rc = cvs_mailbox_timer_get_time( &current_timestamp_us );

    /* Read access to packet_exchange_info. 
       Caller takes care of locking. */
    mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;
    stats = &session_obj->packet_exchange_info.mailbox_info.stats;

    tx_circ_buf = mailbox_config->tx_circ_buf;
    tx_circ_buf_virt_addr.ptr = tx_circ_buf;

    ( void ) cvd_mem_mapper_cache_invalidate_v2(
      &tx_circ_buf_virt_addr, mailbox_config->tx_circ_buf_mem_size );

    rc = cvs_pktexg_mailbox_is_voc_req_exist(
           tx_circ_buf, mailbox_config->tx_circ_buf_mem_size,
           sizeof( vss_ipktexg_mailbox_enc_request_t ) );
    if ( rc )
    {
      stats->num_no_tx_req_in_buf++;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_tx_processing(): " \
             "Client error: No pending request. Dropping encoder packet. Detected for %d times " \
             "since encoder started", stats->num_no_tx_req_in_buf );

      break;
    }

    enc_req = ( ( vss_ipktexg_mailbox_enc_request_t* )
                ( ( ( uint8_t* ) tx_circ_buf ) + 
                  sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) +
                  tx_circ_buf->read_offset ) );

    rc = cvs_pktexg_mailbox_validate_and_get_enc_buffer(
           session_obj, enc_req, &mailbox_enc_packet );
    if ( rc )
    {
      stats->num_invalid_tx_req_dropped++;

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_tx_processing(): " \
             "Client error: Failed to get encoder buffer, rc = 0x%08X. Dropping encoder packet. " \
             "Detected for %d times since encoder started",
             rc, stats->num_invalid_tx_req_dropped );

      ( void ) cvs_pktexg_mailbox_complete_voc_request(
                 tx_circ_buf, mailbox_config->tx_circ_buf_mem_size,
                 sizeof( vss_ipktexg_mailbox_enc_request_t ) );

      break;
    }

    oob_enc_packet = ( ( vsm_oob_pkt_exchange_header_t* ) mailbox_config->oob_enc_buf );

    capture_timestamp_us = ( current_timestamp_us -
                             session_obj->avsync_delay_info.total_tx_delay );

    mailbox_enc_packet->minor_version = 0;
    mailbox_enc_packet->capture_timestamp_us = capture_timestamp_us;
    mailbox_enc_packet->status_mask = 0;
    mailbox_enc_packet->media_id = oob_enc_packet->media_type;
    mailbox_enc_packet->data_size = oob_enc_packet->size;

    ( void ) mmstd_memcpy(
      ( ( ( uint8_t*) mailbox_enc_packet ) + sizeof( vss_ipktexg_mailbox_enc_packet_t ) ),
      ( enc_req->mem_size - sizeof( vss_ipktexg_mailbox_enc_packet_t ) ),
      ( ( ( uint8_t*) oob_enc_packet ) + sizeof( vsm_oob_pkt_exchange_header_t ) ),
      oob_enc_packet->size );

    mailbox_enc_packet_virt_addr.ptr = mailbox_enc_packet;
    ( void ) cvd_mem_mapper_cache_flush_v2( &mailbox_enc_packet_virt_addr, enc_req->mem_size );

    ( void ) cvs_pktexg_mailbox_complete_voc_request(
               tx_circ_buf, mailbox_config->tx_circ_buf_mem_size,
               sizeof( vss_ipktexg_mailbox_enc_request_t ) );

    /* Log the encoder packet. */
    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port. 
       Caller takes care of locking. */
    ( void ) cvd_log_commit_data(
               LOG_ADSP_CVD_STREAM_TX,
               ++session_obj->packet_logging_info.tx_packet_seq_num,
               oob_enc_packet->size,
               ( ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port ),
               session_obj->packet_logging_info.voice_call_num,
               capture_timestamp_us,
               CVD_LOG_ENC_OUTPUT_TAP_POINT,
               oob_enc_packet->media_type,
               ( ( ( uint8_t* ) oob_enc_packet ) + sizeof( vsm_oob_pkt_exchange_header_t ) ) );

    break;
  }

  /* Read access to vsm_stream_handle. 
     Caller takes care of locking. */
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         packet->token, VSM_EVT_OOB_ENC_BUF_CONSUMED,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return rc;
}

/* Caller takes care of locking. */
static int32_t cvs_pktexg_mailbox_rx_processing (
  cvs_session_object_t* session_obj,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* rx_circ_buf;
  cvd_virt_addr_t rx_circ_buf_virt_addr;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;
  vss_ipktexg_mailbox_dec_request_t* dec_req;
  vss_ipktexg_mailbox_dec_packet_t* mailbox_dec_packet;
  vsm_oob_pkt_exchange_header_t* oob_dec_packet;
  cvd_virt_addr_t oob_dec_packet_virt_addr;
  uint64_t current_timestamp_us = 0;
  uint64_t dec_req_timestamp_us;
  cvs_mailbox_packet_exchange_stats_t* stats;

  for ( ;; )
  {
    /* Get the current timestamp. */
    rc = cvs_mailbox_timer_get_time( &current_timestamp_us );

    /* Read access to packet_exchange_info. 
       Caller takes care of locking. */
    mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;
    stats = &session_obj->packet_exchange_info.mailbox_info.stats;

    rx_circ_buf = mailbox_config->rx_circ_buf;
    rx_circ_buf_virt_addr.ptr = rx_circ_buf;

    ( void ) cvd_mem_mapper_cache_invalidate_v2(
      &rx_circ_buf_virt_addr, mailbox_config->rx_circ_buf_mem_size );

    for ( ;; )
    { /* Handle expired decoding requests by completing them. */
      rc = cvs_pktexg_mailbox_is_voc_req_exist(
             rx_circ_buf, mailbox_config->rx_circ_buf_mem_size,
             sizeof( vss_ipktexg_mailbox_dec_request_t ) );
      if ( rc )
      {
        stats->num_no_rx_req_in_buf++;

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_rx_processing(): " \
               "Client error: No pending request. Detected for %d times since decoder started.",
               stats->num_no_rx_req_in_buf );

        break;
      }

      dec_req = ( ( vss_ipktexg_mailbox_dec_request_t* )
                  ( ( ( uint8_t* ) rx_circ_buf ) +
                    sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) +
                    rx_circ_buf->read_offset ) );

      if ( ( current_timestamp_us - mailbox_config->request_to_start_timestamp_us ) >
           ( dec_req->timestamp_us - mailbox_config->request_to_start_timestamp_us +
             CVS_MAILBOX_DEC_WINDOW ) )
      {
        /* Complete the decoding request if the request has expired (past the
         * current decoding window.
         */
        stats->num_expired_rx_req_dropped++;

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_rx_processing(): " \
               "Decoding request timestamp %d is expired. Current time is %d. " \
               "Detected for %d times since decoder started.",
               dec_req->timestamp_us, current_timestamp_us, stats->num_expired_rx_req_dropped );

        ( void ) cvs_pktexg_mailbox_complete_voc_request(
                   rx_circ_buf, mailbox_config->rx_circ_buf_mem_size,
                   sizeof( vss_ipktexg_mailbox_dec_request_t ) );
      }
      else
      {
        break;
      }
    }

    if ( rc ) break;

    if ( ( dec_req->timestamp_us - mailbox_config->request_to_start_timestamp_us ) >
         ( current_timestamp_us - mailbox_config->request_to_start_timestamp_us ) )
    {
      stats->num_future_rx_req_skipped++;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_rx_processing(): " \
             "Decoding request timestamp %d is in the future. Current time is %d. " \
             "Decoder packet will not be decoded until later. Detected for %d times " \
             "since decoder started.",
             dec_req->timestamp_us, current_timestamp_us, stats->num_future_rx_req_skipped );

      rc = APR_EFAILED;
      break;
    }

    rc = cvs_pktexg_mailbox_validate_and_get_dec_buffer(
           session_obj, dec_req, &mailbox_dec_packet );
    if ( rc )
    {
      stats->num_invalid_rx_req_dropped++;

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_rx_processing(): " \
             "Client error: Failed to get decoder buffer, rc = 0x%08X. Detected for %d times since " \
             "decoder started.", rc, stats->num_invalid_rx_req_dropped );

      ( void ) cvs_pktexg_mailbox_complete_voc_request(
                 rx_circ_buf, mailbox_config->rx_circ_buf_mem_size, 
                 sizeof( vss_ipktexg_mailbox_dec_request_t ) );

      break;
    }

    if ( mailbox_dec_packet->status_mask & CVS_MAILBOX_VOC_PACKET_ERROR_STATUS_MASK )
    {
      stats->num_error_status_rx_req_dropped++;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_rx_processing(): " \
             "Decoder packet error status bit is set. Dropping decoder packet and " \
             "completing decoding request. Detected for %d times since decoder started.",
             stats->num_error_status_rx_req_dropped );

      ( void ) cvs_pktexg_mailbox_complete_voc_request(
                 rx_circ_buf, mailbox_config->rx_circ_buf_mem_size, 
                 sizeof( vss_ipktexg_mailbox_dec_request_t ) );

      rc = APR_EFAILED;
      break;
    }

    dec_req_timestamp_us = dec_req->timestamp_us;
    oob_dec_packet = ( ( vsm_oob_pkt_exchange_header_t* ) mailbox_config->oob_dec_buf );

    oob_dec_packet->time_stamp = ( ( uint32_t ) dec_req_timestamp_us );
    oob_dec_packet->media_type = mailbox_dec_packet->media_id;
    oob_dec_packet->size = mailbox_dec_packet->data_size;

    ( void ) mmstd_memcpy(
      ( ( ( uint8_t*) oob_dec_packet ) + sizeof( vsm_oob_pkt_exchange_header_t ) ),
      ( CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE - sizeof( vsm_oob_pkt_exchange_header_t ) ),
      ( ( ( uint8_t*) mailbox_dec_packet ) + sizeof( vss_ipktexg_mailbox_dec_packet_t ) ),
      mailbox_dec_packet->data_size );

    oob_dec_packet_virt_addr.ptr = oob_dec_packet;
    ( void ) cvd_mem_mapper_cache_flush_v2(
               &oob_dec_packet_virt_addr, CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE );

    /* Read access to vsm_stream_handle. 
       Caller takes care of locking. */
    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           packet->token, VSM_EVT_OOB_DEC_BUF_READY,
           NULL, 0 );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    ( void ) cvs_pktexg_mailbox_complete_voc_request(
               rx_circ_buf, mailbox_config->rx_circ_buf_mem_size,
               sizeof( vss_ipktexg_mailbox_dec_request_t ) );

    /* Log the vocoder packet. */
    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port. 
       Caller takes care of the locking. */
    ( void ) cvd_log_commit_data(
               LOG_ADSP_CVD_STREAM_RX,
               ++session_obj->packet_logging_info.rx_packet_seq_num,
               oob_dec_packet->size,
               ( ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port ),
               session_obj->packet_logging_info.voice_call_num,
               dec_req_timestamp_us,
               CVD_LOG_DEC_INPUT_TAP_POINT,
               oob_dec_packet->media_type,
               ( ( ( uint8_t* ) oob_dec_packet ) + sizeof( vsm_oob_pkt_exchange_header_t ) ) );

    rc = cvs_pktexg_mailbox_update_rx_avsync_delay(
           session_obj, current_timestamp_us, dec_req_timestamp_us );

    break;
  }

  if ( mailbox_config->rx_expiry_timer_handle != APR_NULL_V )
  {
    ( void ) cvs_mailbox_timer_destroy( mailbox_config->rx_expiry_timer_handle );
    mailbox_config->rx_expiry_timer_handle = APR_NULL_V;
  }

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return rc;
}

static void cvs_mailbox_pktexg_rx_expiry_timer_cb (
  void* cb_data
)
{
  int32_t rc;
  uint16_t cvs_port;

  if ( cb_data == NULL )
    return;

  cvs_port = *( ( uint16_t* ) cb_data );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, cvs_port,
         cvs_my_addr, cvs_port,
         0, VSS_IPKTEXG_CMD_MAILBOX_PROCESS_RX_EXPIRY,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_my_addr );
}

/****************************************************************************
 * CVS STATE MACHINE RESPONSE FUNCTIONS                                     *
 ****************************************************************************/

static void cvs_simple_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  /* Complete the current action. */
  session_obj->session_ctrl.status =
    APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* If completed action failed, log the error. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_simple_transition_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                            APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                            session_obj->session_ctrl.status );
  }

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_create_stream_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* Write access to vsm_stream_addr. */
  ( void ) apr_lock_enter( session_obj->lock );
  session_obj->vsm_stream_addr = cvs_vsm_addr;
  ( void ) apr_lock_leave( session_obj->lock );

  if ( APR_EOK == session_obj->session_ctrl.status )
  { 
    /* Write access to vsm_stream_handle. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->vsm_stream_handle = packet->src_port;
    ( void ) apr_lock_leave( session_obj->lock );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_create_stream_transition_result_rsp_fn(): VSM addr: 0x%04X, VSM handle: 0x%04X",
                                          session_obj->vsm_stream_addr,
                                          session_obj->vsm_stream_handle );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_create_stream_transition_result_rsp_fn(): Create VSM session failed with result 0x%08X",
                                            session_obj->session_ctrl.status );
  }

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_destroy_stream_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;

  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( APR_EOK == session_obj->session_ctrl.status )
  { 
    /* Write access to vsm_stream_addr/_handle. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->vsm_stream_addr = APRV2_PKT_INIT_ADDR_V;
    session_obj->vsm_stream_handle = APR_NULL_V;
    ( void ) apr_lock_leave( session_obj->lock );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_destroy_stream_transition_result_rsp_fn(): Destroy VSM session failed with result 0x%08X",
                                            session_obj->session_ctrl.status );
  }

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_attach_vocproc_cvp_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );
  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  switch (packet->opcode)
  {
  case VSS_IVOCPROC_RSP_ATTACH_STREAM:
    { /* Store the returned VPM handle and direction from CVP */
      /* TBD: Why putting this into pending control?
         Put to low priority task for now. */
      cvs_pending_control_t* ctrl = &cvs_low_task_pending_ctrl;

      ctrl->temp_data[0] = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_rsp_attach_stream_t, packet )->vdsp_session_handle;
      ctrl->temp_data[1] = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_rsp_attach_stream_t, packet )->direction;
      session_obj->session_ctrl.status = APR_EOK;
    }
    break;

   case VSS_IVOCPROC_RSP_DETACH_STREAM:
   { /* Store the returned VPM handle and direction from CVP */
      /* TBD: Why do we put this into pending control?
         Put to low priority task for now. */
     cvs_pending_control_t* ctrl = &cvs_low_task_pending_ctrl;

     ctrl->temp_data[0] = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_rsp_detach_stream_t, packet )->vdsp_session_handle;
     ctrl->temp_data[1] = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_rsp_detach_stream_t, packet )->direction;
     session_obj->session_ctrl.status = APR_EOK;
   }
   break;

  case APRV2_IBASIC_RSP_RESULT:
  default:
    { /* Error */
      session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
    }
    break;
  }

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

static void cvs_map_memory_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  cvs_session_object_t* session_obj;
  voice_rsp_shared_mem_map_regions_t* mem_map_rsp;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvs_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* This function shall only be called in low priority task.
     No need to error fatal if the return is not APR_EOK. */
  ( void ) cvs_access_session_obj_start( job_obj->context_handle, FALSE, &session_obj, NULL );

  /* Save the vsm mem handle and complete the current action. */
  mem_map_rsp = APRV2_PKT_GET_PAYLOAD( voice_rsp_shared_mem_map_regions_t, packet );
  session_obj->shared_heap.vsm_mem_handle = mem_map_rsp->mem_map_handle;

  session_obj->session_ctrl.status = APR_EOK;

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) cvs_access_session_obj_end( job_obj->context_handle, FALSE );
  ( void ) cvs_free_object( ( cvs_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
}

/****************************************************************************
 * CVS STATE MACHINE TRANSITION ACTION FUNCTIONS                            *
 ****************************************************************************/

static int32_t cvs_action_create_stream (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_create_session_t create_session;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_create_stream_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  /* Only direction is sent to VSM, the other params to be sent later */
  create_session.mode = session_obj->target_set.direction;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, APR_NULL_V,
         job_obj->header.handle, VSM_CMD_CREATE_SESSION,
         &create_session, sizeof( create_session ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_destroy_stream (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_destroy_stream_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_DESTROY_SESSION,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_reinit_stream (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_reinit_session_t reinit_session;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  /* Only direction is sent to VSM, the other params to be sent later */
  reinit_session.mode = session_obj->target_set.direction;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_REINIT_SESSION,
         &reinit_session, sizeof( reinit_session ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_media_type (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_media_type_t set_media_type;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  set_media_type.media_type = session_obj->target_set.media_id;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_MEDIA_TYPE,
         &set_media_type, sizeof( set_media_type ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_var_voc_sampling_rate (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_samp_rate_t set_samp_rate;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  set_samp_rate.rx_samp_rate = session_obj->requested_var_voc_rx_sampling_rate;
  set_samp_rate.tx_samp_rate = session_obj->requested_var_voc_tx_sampling_rate;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_STREAM_PP_SAMP_RATE,
         &set_samp_rate, sizeof( set_samp_rate ) );

  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_dtx_mode (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_enc_set_dtx_mode_t set_dtx;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_AMR_NB:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_amr.dtx_mode;
      break;

    case VSS_MEDIA_ID_AMR_WB:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_amrwb.dtx_mode;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_4gvnw.dtx_mode;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_4gvnw2k.dtx_mode;
      break;

    case VSS_MEDIA_ID_EAMR:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_eamr.dtx_mode;
      break;

    case VSS_MEDIA_ID_EFR:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_efr.dtx_mode;
      break;

    case VSS_MEDIA_ID_FR:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_fr.dtx_mode;
      break;

    case VSS_MEDIA_ID_HR:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_hr.dtx_mode;
      break;

    case VSS_MEDIA_ID_G711_ALAW:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_g711_alaw.dtx_mode;
      break;

    case VSS_MEDIA_ID_G711_MULAW:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_g711_mulaw.dtx_mode;
      break;

    case VSS_MEDIA_ID_G711_LINEAR:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_g711_linear.dtx_mode;
      break;

    case VSS_MEDIA_ID_G729:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_g729.dtx_mode;
      break;

    case VSS_MEDIA_ID_EVS:
      set_dtx.dtx_mode = session_obj->cached_voc_properties_evs.dtx_mode;
      break;

    default:
      set_dtx.dtx_mode = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
    "cvs_action_set_cached_dtx_mode: cached DTX value 0x%x", set_dtx.dtx_mode );

  if ( set_dtx.dtx_mode == CVS_STREAM_PROPERTY_NOT_SET_UINT32 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_DTX_MODE,
         &set_dtx, sizeof( set_dtx ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_rate_modulation (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_enc_set_rate_mod_t set_rate_mod;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      set_rate_mod.rate_modulation_param = 
        session_obj->cached_voc_properties_qcelp13k.reduced_rate_mode;
      break;

    case VSS_MEDIA_ID_EVRC:
      set_rate_mod.rate_modulation_param = 
        session_obj->cached_voc_properties_evrc.reduced_rate_mode;
      break;

    /* This command applies only to QCELP-13K and EVRC vocoders. 
       Hence avoiding to set for other vocoders.
    */
    default:
      set_rate_mod.rate_modulation_param = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
    "cvs_action_set_cached_reduced_rate_mode: cached reduced rate mode 0x%x",
    set_rate_mod.rate_modulation_param );

  if ( set_rate_mod.rate_modulation_param == CVS_STREAM_PROPERTY_NOT_SET_UINT32 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_RATE_MOD,
         &set_rate_mod, sizeof( set_rate_mod ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_minmax_rate (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_enc_set_minmax_rate_t minmax_rate;

  minmax_rate.media_type = session_obj->active_set.media_id;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      minmax_rate.min_rate = session_obj->cached_voc_properties_qcelp13k.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_qcelp13k.max_rate;
      break;

    case VSS_MEDIA_ID_EVRC:
      minmax_rate.min_rate = session_obj->cached_voc_properties_evrc.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_evrc.max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NB:
      minmax_rate.min_rate = session_obj->cached_voc_properties_4gvnb.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_4gvnb.max_rate;
      break;

    case VSS_MEDIA_ID_4GV_WB:
      minmax_rate.min_rate = session_obj->cached_voc_properties_4gvwb.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_4gvwb.max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      minmax_rate.min_rate = session_obj->cached_voc_properties_4gvnw.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_4gvnw.max_rate;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      minmax_rate.min_rate = session_obj->cached_voc_properties_4gvnw2k.min_rate;
      minmax_rate.max_rate = session_obj->cached_voc_properties_4gvnw2k.max_rate;
      break;

    /* This property is applicable only for CDMA vocoders.*/
    default:
      minmax_rate.min_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
      minmax_rate.max_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_cached_minmax_rate:"
    " cached min_rate 0x%x, max_rate 0x%x", minmax_rate.min_rate, minmax_rate.max_rate );

  if ( ( minmax_rate.min_rate == CVS_STREAM_PROPERTY_NOT_SET_UINT32 ) ||
       ( minmax_rate.max_rate == CVS_STREAM_PROPERTY_NOT_SET_UINT32 )
     )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_MINMAX_RATE,
         &minmax_rate, sizeof( minmax_rate ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_enc_rate (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_enc_set_rate_t rate;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_13K:
      rate.encoder_rate = session_obj->cached_voc_properties_qcelp13k.rate;
      break;

    case VSS_MEDIA_ID_EVRC:
      rate.encoder_rate = session_obj->cached_voc_properties_evrc.rate;
      break;

    case VSS_MEDIA_ID_4GV_NB:
      rate.encoder_rate = session_obj->cached_voc_properties_4gvnb.rate;
      break;

    case VSS_MEDIA_ID_4GV_WB:
      rate.encoder_rate = session_obj->cached_voc_properties_4gvwb.rate;
      break;

    case VSS_MEDIA_ID_4GV_NW:
      rate.encoder_rate = session_obj->cached_voc_properties_4gvnw.rate;
      break;

    case VSS_MEDIA_ID_4GV_NW2K:
      rate.encoder_rate = session_obj->cached_voc_properties_4gvnw2k.rate;
      break;

    case VSS_MEDIA_ID_AMR_NB:
      rate.encoder_rate = session_obj->cached_voc_properties_amr.rate;
      break;

    case VSS_MEDIA_ID_EAMR:
      rate.encoder_rate = session_obj->cached_voc_properties_eamr.rate;
      break;

    case VSS_MEDIA_ID_AMR_WB:
      rate.encoder_rate = session_obj->cached_voc_properties_amrwb.rate;
      break;

    default:
      rate.encoder_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT32;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
    "cvs_action_set_cached_enc_rate: cached enc rate 0x%x", rate.encoder_rate );

  rate.media_type = session_obj->active_set.media_id;

  if ( rate.encoder_rate == CVS_STREAM_PROPERTY_NOT_SET_UINT32 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_RATE,
         &rate, sizeof( rate ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_channel_aware_mode (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_evs_enc_channel_aware_mode_enable_t set_enc_channel_aware_mode;
  cvs_enc_channel_aware_mode_params_t cvs_cached_params;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_EVS:
      {
        cvs_cached_params.channel_aware_enabled =
          session_obj->cached_voc_properties_evs.channel_aware_enabled;
        cvs_cached_params.fec_offset = 
          session_obj->cached_voc_properties_evs.fec_offset;
        cvs_cached_params.fer_rate = 
          session_obj->cached_voc_properties_evs.fer_rate;
      }
      break;

    /* This command applies only to EVS vocoder. 
       Hence avoiding to set for other vocoders.
    */
    default:
      {
        cvs_cached_params.channel_aware_enabled = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
        cvs_cached_params.fec_offset = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
        cvs_cached_params.fer_rate = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
      }
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_cached_channel_aware_mode:"
    "channel_aware_enabled 0x%x, fec_offset 0x%x, fer_rate 0x%x",
    cvs_cached_params.channel_aware_enabled, cvs_cached_params.fec_offset,
    cvs_cached_params.fer_rate );

  if ( cvs_cached_params.channel_aware_enabled == CVS_STREAM_PROPERTY_NOT_SET_UINT8 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  if ( cvs_cached_params.channel_aware_enabled )
  {
    set_enc_channel_aware_mode.fec_offset = cvs_cached_params.fec_offset;
    set_enc_channel_aware_mode.fer_rate = cvs_cached_params.fer_rate;

    rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               job_obj->header.handle, VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE,
               &set_enc_channel_aware_mode, sizeof( vsm_set_evs_enc_channel_aware_mode_enable_t ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }
  else
  {
    rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               job_obj->header.handle, VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE,
               NULL, 0 );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  return APR_EOK;
}

static int32_t cvs_action_set_cached_enc_operating_mode (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_evs_enc_operating_mode_t set_enc_operating_mode;

  ( void ) apr_lock_enter( session_obj->lock );
  switch ( session_obj->active_set.media_id )
  {
    case VSS_MEDIA_ID_EVS:
      set_enc_operating_mode.mode = 
        session_obj->cached_voc_properties_evs.mode;
      set_enc_operating_mode.bandwidth = 
        session_obj->cached_voc_properties_evs.bandwidth;
      break;

    /* This command applies only to EVS vocoder. 
       Hence avoiding to set for other vocoders.
    */
    default:
      set_enc_operating_mode.mode = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
      set_enc_operating_mode.bandwidth = CVS_STREAM_PROPERTY_NOT_SET_UINT8;
      break;
  }
  ( void ) apr_lock_leave( session_obj->lock );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
    "cvs_action_set_cached_enc_operating_mode: cached operating mode 0x%x, bandwidth 0x%x",
    set_enc_operating_mode.mode, set_enc_operating_mode.bandwidth );

  if ( ( set_enc_operating_mode.mode == CVS_STREAM_PROPERTY_NOT_SET_UINT8 ) || 
       ( set_enc_operating_mode.bandwidth == CVS_STREAM_PROPERTY_NOT_SET_UINT8 )
     )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               job_obj->header.handle, VSM_CMD_SET_EVS_ENC_OPERATING_MODE,
               &set_enc_operating_mode, sizeof( vsm_set_evs_enc_operating_mode_t ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_cached_voc_property (
  cvs_session_object_t* session_obj,
  cvs_voc_property_enum_t property
)
{
  int32_t rc = APR_EOK;

  switch ( property )
  {
    case CVS_VOC_PROPERTY_ENUM_DTX:
      rc = cvs_action_set_cached_dtx_mode( session_obj );
      break;

    case CVS_VOC_PROPERTY_ENUM_RATE_MODULATION:
      rc = cvs_action_set_cached_rate_modulation( session_obj );
      break;

    case CVS_VOC_PROPERTY_ENUM_MINMAX_RATE:
      rc = cvs_action_set_cached_minmax_rate( session_obj );
      break;

    case CVS_VOC_PROPERTY_ENUM_ENC_RATE:
      rc = cvs_action_set_cached_enc_rate( session_obj );
      break;

    case CVS_VOC_PROPERTY_ENUM_CHANNEL_AWARE_MODE:
      rc = cvs_action_set_cached_channel_aware_mode( session_obj );
      break;

    case CVS_VOC_PROPERTY_ENUM_ENC_OPERATING_MODE:
      rc = cvs_action_set_cached_enc_operating_mode( session_obj );
      break;

  }

  return rc;
}

static int32_t cvs_action_register_voc_op_mode_update_event (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_cmd_register_event_t register_event;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  /* Write access to voc_operating_mode_info. */
  ( void ) apr_lock_enter( session_obj->lock );
  session_obj->voc_operating_mode_info.is_rx_mode_received = FALSE;
  session_obj->voc_operating_mode_info.is_tx_mode_received = FALSE;
  ( void ) apr_lock_leave( session_obj->lock );

  register_event.event_id = VSM_EVT_VOC_OPERATING_MODE_UPDATE;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_REGISTER_EVENT,
         &register_event, sizeof( register_event ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_packet_exchange_mode (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_pkt_exchange_mode_t set_pkt_exchange_mode;

  if ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_IN_BAND )
  { /* VSM uses in-band packet exchange mode by default. */
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  /* If CVS's packet exchange mode is not VSS_IPKTEXG_MODE_IN_BAND. Set the 
   * packet exchange mode on VSM to OOB. CVS will use OOB with VSM when CVS's
   * packet exchange mode is either VSS_IPKTEXG_MODE_OUT_OF_BAND or 
   * VSS_IPKTEXG_MODE_MAILBOX.
   */
  set_pkt_exchange_mode.pkt_exchange_mode = VSS_IPKTEXG_MODE_OUT_OF_BAND;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_PKT_EXCHANGE_MODE,
         &set_pkt_exchange_mode, sizeof( set_pkt_exchange_mode ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_oob_packet_exchange_config (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_config_packet_exchange_t vsm_set_oob_config;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;

  /* CVS will set the OOB packet exchange config on VSM if CVS is configured
   * for OOB or mailbox.
   */
  if ( session_obj->packet_exchange_info.oob_info.is_configured == TRUE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_oob_packet_exchange_config(): OOB configured on CVS." );
    vsm_set_oob_config = session_obj->packet_exchange_info.oob_info.config;
  }
  else
  if ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_oob_packet_exchange_config(): Mailbox configured on CVS." );

    mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

    vsm_set_oob_config.mem_handle = mailbox_config->vsm_mem_handle;
    vsm_set_oob_config.enc_buf_addr_lsw = ( ( uint32_t ) mailbox_config->oob_enc_buf );
    vsm_set_oob_config.enc_buf_addr_msw = 0;
    vsm_set_oob_config.enc_buf_size = CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE;
    vsm_set_oob_config.dec_buf_addr_lsw = ( ( uint32_t ) mailbox_config->oob_dec_buf );
    vsm_set_oob_config.dec_buf_addr_msw = 0;
    vsm_set_oob_config.dec_buf_size = CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE;
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_oob_packet_exchange_config(): " \
         "OOB not configured on VSM, VSM default to in-band." );

    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG,
         &vsm_set_oob_config, sizeof( vsm_set_oob_config ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_reconfig_mailbox_timing (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_mvm_addr, session_obj->attached_mvm_handle,
         0, VSS_IPKTEXG_EVT_MAILBOX_TIMING_RECONFIG,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_mvm_addr );

  return APR_EOK;
}

static int32_t cvs_action_set_voice_timing (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_timing_params_t timing_param;
  vsm_set_timing_params_v2_t timing_param_v2;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] =
    cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  if ( session_obj->target_set.system_config.vsid == 0 )
  {
    /* BACKWARD COMPATIBILITY */
    /* If the CVD client does not provide a VSID, use VSM_CMD_SET_TIMING_PARAMS
     * to pass the VFR mode and stream timing offsets to VSM.
     */

    { /* Populate the timing parameters based on target_set. */
      timing_param.mode =
        session_obj->target_set.system_config.vfr_mode;

      timing_param.enc_offset =
        session_obj->target_set.voice_timing.enc_offset;

      timing_param.dec_req_offset =
        session_obj->target_set.voice_timing.dec_req_offset;

      timing_param.dec_offset =
        session_obj->target_set.voice_timing.dec_offset;

      timing_param.decpp_offset =
        session_obj->target_set.voice_timing.dec_pp_start_offset;

      timing_param.vptx_delivery_offset =
        session_obj->target_set.voice_timing.vp_tx_delivery_offset;
    }

    { /* Set timing on VSM. */
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_voice_timing(): VFR mode = %d, enc_offset = %d, " \
                                            "dec_req_offset = %d",
                                            timing_param.mode,
                                            timing_param.enc_offset,
                                            timing_param.dec_req_offset );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_voice_timing(): dec_offset = %d, decpp_offset = %d, " \
                                            "vptx_delivery_offset = %d",
                                            timing_param.dec_offset,
                                            timing_param.decpp_offset,
                                            timing_param.vptx_delivery_offset );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             job_obj->header.handle, VSM_CMD_SET_TIMING_PARAMS,
             &timing_param, sizeof( timing_param ) );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
  }
  else
  {
    { /* Populate the timing parameters based on target_set. */
      timing_param_v2.vsid =
        session_obj->target_set.system_config.vsid;

      timing_param_v2.mode =
        session_obj->target_set.system_config.vfr_mode;

      timing_param_v2.enc_offset =
        session_obj->target_set.voice_timing.enc_offset;

      timing_param_v2.dec_req_offset =
        session_obj->target_set.voice_timing.dec_req_offset;

      timing_param_v2.dec_offset =
        session_obj->target_set.voice_timing.dec_offset;

      timing_param_v2.decpp_offset =
        session_obj->target_set.voice_timing.dec_pp_start_offset;

      timing_param_v2.vptx_delivery_offset =
        session_obj->target_set.voice_timing.vp_tx_delivery_offset;
    }

    { /* Set timing on VSM. */
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_voice_timing(): VSID = 0x%08X, VFR mode = %d, " \
                                            "enc_offset = %d",
                                            timing_param_v2.vsid,
                                            timing_param_v2.mode,
                                            timing_param_v2.enc_offset );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_voice_timing(): dec_req_offset = %d, " \
                                            "dec_offset = %d, decpp_offset =%d",
                                            timing_param_v2.dec_req_offset,
                                            timing_param_v2.dec_offset,
                                            timing_param_v2.decpp_offset );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_voice_timing(): vptx_delivery_offset = %d",
                                            timing_param_v2.vptx_delivery_offset );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             job_obj->header.handle, VSM_CMD_SET_TIMING_PARAMS_V2,
             &timing_param_v2, sizeof( timing_param_v2 ) );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
  }

  return APR_EOK;
}

static int32_t cvs_action_set_tx_mute(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  voice_set_soft_mute_v2_t set_mute_args;
  cvs_simple_job_object_t *job_obj;

  /* Setting Tx Mute Properties. */
  set_mute_args.direction = CVS_DIRECTION_TX;
  set_mute_args.ramp_duration = 0;

  /* Read access to target_set.mute. */
  ( void ) apr_lock_enter( session_obj->lock );
  set_mute_args.mute = session_obj->target_set.mute.tx_mute_flag;
  ( void ) apr_lock_leave( session_obj->lock );

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VOICE_CMD_SET_SOFT_MUTE_V2,
         &set_mute_args, sizeof( set_mute_args ) );
  CVS_PANIC_ON_ERROR( rc );

  return rc;
}

static int32_t cvs_action_set_rx_mute(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  voice_set_soft_mute_v2_t set_mute_args;
  cvs_simple_job_object_t *job_obj;
  
  /* Setting Rx Mute Properties. */
  set_mute_args.direction = CVS_DIRECTION_RX;
  set_mute_args.ramp_duration = 0;

  /* Read access to target_set.mute. */
  ( void ) apr_lock_enter( session_obj->lock );
  set_mute_args.mute = session_obj->target_set.mute.rx_mute_flag;
  ( void ) apr_lock_leave( session_obj->lock );

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VOICE_CMD_SET_SOFT_MUTE_V2,
         &set_mute_args, sizeof( set_mute_args ) );
  CVS_PANIC_ON_ERROR( rc );

  return rc;
}

static int32_t cvs_action_set_ui_properties (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;

  if ( session_obj->shared_heap.ui_prop_cache.data_len == 0 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  set_param.payload_address_lsw = ( ( uint32_t ) session_obj->shared_heap.ui_prop_cache.data );
  set_param.payload_address_msw = 0;
  set_param.payload_size = session_obj->shared_heap.ui_prop_cache.data_len;
  set_param.mem_map_handle = session_obj->shared_heap.vsm_mem_handle;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_ui_properties(): payload_address_lsw: 0x%08X, payload_address_msw: 0x%08X",
                                        set_param.payload_address_lsw,
                                        set_param.payload_address_msw );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_set_ui_properties(): payload_size: %d, mem_map_handle: 0x%08X",
                                        set_param.payload_size,
                                        set_param.mem_map_handle );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
         &set_param, sizeof( set_param ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_send_avsync_rx_delay_notification(
  cvs_session_object_t *session_obj
)
{
  int32_t rc;
  uint32_t rx_delay = session_obj->avsync_delay_info.total_rx_delay;
  vss_iavsync_evt_rx_path_delay_t rx_path_delay;
  uint32_t client_addr;

  client_addr = session_obj->avsync_client_rx_info.client_addr;

  rx_path_delay.delay_us = rx_delay;

  if( session_obj->avsync_client_rx_info.is_enabled == TRUE )
  {
    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           client_addr, session_obj->avsync_client_rx_info.client_port,
           APR_NULL_V, VSS_IAVSYNC_EVT_RX_PATH_DELAY,
           &rx_path_delay, sizeof( rx_path_delay ) );
    CVS_COMM_ERROR( rc, client_addr );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_action_send_avsync_rx_delay_notification(): IAVSYNC RX Client Addr: 0x%04X",
                                            session_obj->avsync_client_rx_info.client_addr );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_action_send_avsync_rx_delay_notification(): IAVSYNC RX Client Port: 0x%04X",
                                            session_obj->avsync_client_rx_info.client_port );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_action_send_avsync_rx_delay_notification(): IAVSYNC RX Path Delay: %d",
                                            rx_path_delay.delay_us );

  }

  session_obj->session_ctrl.status = APR_EOK;

  return APR_EIMMEDIATE;
}

static int32_t cvs_action_send_ready_notification(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         cvs_my_addr, session_obj->master_cvs_port,
         session_obj->master_client_addr, session_obj->master_client_port,
         APR_NULL_V, VSS_ISTREAM_EVT_READY,
         NULL, 0 );
  CVS_COMM_ERROR( rc, session_obj->master_client_addr );

  return APR_EOK;
}

static int32_t cvs_action_send_not_ready_notification(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         cvs_my_addr, session_obj->master_cvs_port,
         session_obj->master_client_addr, session_obj->master_client_port,
         APR_NULL_V, VSS_ISTREAM_EVT_NOT_READY,
         NULL, 0 );
  CVS_COMM_ERROR( rc, session_obj->master_client_addr );

  return APR_EOK;
}

static int32_t cvs_action_map_shared_memory (
  cvs_session_object_t* session_obj
)
{
#ifdef WINSIM

  session_obj->session_ctrl.status = APR_EOK; /* Fake the completion. */
  return APR_EIMMEDIATE; /* To trigger state machine in WINSIM */

#else
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  struct voice_map_args_t {
    voice_cmd_shared_mem_map_regions_t map_memory;
    voice_shared_map_region_payload_t mem_region;
  } voice_map_args;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_MAP_MEMORY ] = cvs_map_memory_transition_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  voice_map_args.map_memory.mem_pool_id = VOICE_MAP_MEMORY_SHMEM8_4K_POOL;
  voice_map_args.map_memory.num_regions = 1;
  voice_map_args.map_memory.property_flag = 2; /* Indicates mapping heap memory. */
  voice_map_args.mem_region.shm_addr_lsw = ( ( uint32_t ) &session_obj->shared_heap );
  voice_map_args.mem_region.shm_addr_msw = 0;
  voice_map_args.mem_region.mem_size_bytes = sizeof( session_obj->shared_heap );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, APR_NULL_V,
         job_obj->header.handle, VOICE_CMD_SHARED_MEM_MAP_REGIONS,
         &voice_map_args, sizeof( voice_map_args ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_map_shared_memory(): mem_pool_id: 0x%08X, num_regions: %d, property_flag: 0x%08X",
                                        voice_map_args.map_memory.mem_pool_id,
                                        voice_map_args.map_memory.num_regions,
                                        voice_map_args.map_memory.property_flag );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_map_shared_memory(): shm_addr_lsw: 0x%08X, shm_addr_msw: 0x%08X, mem_size_bytes: %d",
                                        voice_map_args.mem_region.shm_addr_lsw,
                                        voice_map_args.mem_region.shm_addr_msw,
                                        voice_map_args.mem_region.mem_size_bytes );

  return APR_EOK;

#endif /* WINSIM */

}

static int32_t cvs_action_unmap_shared_memory (
  cvs_session_object_t* session_obj
)
{
#ifdef WINSIM

  session_obj->session_ctrl.status = APR_EOK; /* Fake the completion. */
  return APR_EIMMEDIATE; /* To trigger state machine in WINSIM */

#else

  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  voice_cmd_shared_mem_unmap_regions_t mem_unmap;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  mem_unmap.mem_map_handle = session_obj->shared_heap.vsm_mem_handle;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_unmap_shared_memory(): vsm_mem_handle: 0x%08X",
                                        mem_unmap.mem_map_handle );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, APR_NULL_V,
         job_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
         &mem_unmap, sizeof( mem_unmap ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;

#endif /* WINSIM */

}

static int32_t cvs_action_run(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_START_SESSION,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;
}

static int32_t cvs_action_get_max_mailbox_pkt_size ( 
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;

  mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

  rc = cvs_pktexg_mailbox_get_max_pkt_size( session_obj->active_set.media_id,
                                            &mailbox_config->max_enc_pkt_size,
                                            NULL );

  return rc;
}

static int32_t cvs_action_start_record(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_start_record_v2_t record_param;

  if ( session_obj->active_set.record.enable_flag == CVS_ENABLED )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    record_param.rx_tap_point = session_obj->active_set.record.rx_tap_point;
    record_param.tx_tap_point = session_obj->active_set.record.tx_tap_point;
    record_param.aud_ref_port = session_obj->active_set.record.port_id;
    record_param.record_mode = session_obj->active_set.record.mode;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_start_record(): rx_tap_point: 0x%08X, tx_tap_point: 0x%08X",
                                          record_param.rx_tap_point,
                                          record_param.tx_tap_point );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_start_record(): port_id: 0x%04X, mode: 0x%08X",
                                          record_param.aud_ref_port,
                                          record_param.record_mode );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VSM_CMD_START_RECORD_V2,
           &record_param, sizeof( record_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    return APR_EOK;
  }
  else
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }
}

static int32_t cvs_action_start_playback(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_start_playback_t playback_param;

  if ( session_obj->active_set.playback.enable_flag == CVS_ENABLED )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    playback_param.aud_ref_port = session_obj->active_set.playback.port_id;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_start_playback(): port_id: 0x%04X",
                                          playback_param.aud_ref_port );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VSM_CMD_START_PLAYBACK_V2,
           &playback_param, sizeof( playback_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    return APR_EOK;
  }
  else
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }
}

static int32_t cvs_action_rx_dtmf_detect(
  cvs_session_object_t* session_obj,
  uint32_t enable_flag
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vsm_set_rx_dtmf_detection_t vsm_rx_dtmf_detect;

  /* Read access to set_rx_dtmf_detect. */
  ( void ) apr_lock_enter( session_obj->lock );
  if( session_obj->set_rx_dtmf_detect.enable_flag == CVS_ENABLED )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    vsm_rx_dtmf_detect.enable = enable_flag;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_action_rx_dtmf_detect(): enable: %d",
                                          vsm_rx_dtmf_detect.enable );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VSM_CMD_SET_RX_DTMF_DETECTION,
           &vsm_rx_dtmf_detect, sizeof( vsm_rx_dtmf_detect ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    ( void ) apr_lock_leave( session_obj->lock );

    return APR_EOK;
  }
  else
  {
    ( void ) apr_lock_leave( session_obj->lock );

    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }
}

static int32_t cvs_action_stop_record(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  if ( session_obj->active_set.record.enable_flag == CVS_ENABLED )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VSM_CMD_STOP_RECORD,
           NULL, 0 );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    return APR_EOK;
  }
  else
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }
}

static int32_t cvs_action_stop_playback(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  if ( session_obj->active_set.playback.enable_flag == CVS_ENABLED )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VSM_CMD_STOP_PLAYBACK,
           NULL, 0 );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    return APR_EOK;
  }
  else
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }
}

static int32_t cvs_action_stop(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

#ifdef USE_CAD_AFE_HACK

  /* TODO: Need to mute the stream because it still runs for
   *       same vocoder type handover optimization.
   */

  session_obj->session_ctrl.status = APR_EOK;
  return APR_EIMMEDIATE;

#else

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_STOP_SESSION,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  return APR_EOK;

#endif /* USE_CAD_AFE_HACK */

}

static int32_t cvs_action_reset_mailbox_book_keeping (
  cvs_session_object_t* session_obj 
)
{
  cvs_mailbox_packet_exchange_config_t* mailbox_config;

  mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

  mailbox_config->tx_ref_timestamp_us = 0;
  mailbox_config->rx_ref_timestamp_us = 0;
  mailbox_config->request_to_start_timestamp_us = 0;
  mailbox_config->rx_req_and_dequeue_time_diff_us =
    CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
  mailbox_config->is_start_requested = FALSE;
  mailbox_config->is_reseted = FALSE;
  mailbox_config->is_time_ref_received = FALSE;
  mailbox_config->is_rx_expiry_proc_disabled = FALSE;
  mailbox_config->rx_expiry_timestamp_us = 0;
  if ( session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle != APR_NULL_V )
  {
    ( void ) cvs_mailbox_timer_destroy(
               session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle );
    session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
  }

  ( void )mmstd_memset(
            &session_obj->packet_exchange_info.mailbox_info.stats, 0,
            sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) );

  return APR_EOK;
}

static int32_t cvs_action_send_evs_bandwidth_notification (
  cvs_session_object_t* session_obj 
)
{
  int32_t rc;
  vss_istream_evt_evs_rx_bandwidth_changed_t evs_rx_bandwidth_changed;
  
  if ( ( session_obj->evs_bandwidth_change_notification_info.is_enabled == TRUE ) &&
       ( session_obj->active_set.media_id == VSS_MEDIA_ID_EVS ) && 
       ( session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth != 
          session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth ) )   
  {
    evs_rx_bandwidth_changed.bandwidth = session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->evs_bandwidth_change_notification_info.client_addr,
           session_obj->evs_bandwidth_change_notification_info.client_port,
           0, VSS_ISTREAM_EVT_EVS_RX_BANDWIDTH_CHANGED,
           &evs_rx_bandwidth_changed, sizeof( evs_rx_bandwidth_changed ) );
    CVS_COMM_ERROR(
      rc, session_obj->evs_bandwidth_change_notification_info.client_addr );
    
    /* Updated last sent Rx Bandwidth */
    session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = 
      session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;
  }
  
  session_obj->session_ctrl.status = APR_EOK;
  
  return APR_EIMMEDIATE;
}

/****************************************************************************
 * CVS STATE MACHINE HELPER FUNCTIONS                                       *
 ****************************************************************************/

static int32_t cvs_do_complete_goal (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  aprv2_ibasic_rsp_result_t res;

  /* Complete the pending command and stay in the same state. */
  res.opcode = APR_UNDEFINED_ID_V;
  res.status = session_obj->session_ctrl.status;
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ), /* This src_port is used for debugging only. */
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ), /* This dst_port is used for debugging only. */
         session_obj->session_ctrl.pendjob_handle,
         APRV2_IBASIC_RSP_RESULT, &res, sizeof( res ) );
  CVS_COMM_ERROR( rc, cvs_my_addr );

  session_obj->session_ctrl.transition_job_handle = APR_NULL_V;
  session_obj->session_ctrl.pendjob_handle = APR_NULL_V;
  session_obj->session_ctrl.goal = CVS_GOAL_ENUM_NONE;
  session_obj->session_ctrl.action = CVS_ACTION_ENUM_NONE;
  session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

  return APR_EOK;
}

static void cvs_update_vsm_attach_table (
  cvs_session_object_t* session_obj,
  uint32_t opcode
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  aprv2_packet_t* new_packet;
  uint32_t handle_size;
  uint32_t payload_size;
  cvs_session_vocproc_table_t* attach_table;
  vsm_routing_config_t* routing_args;
  uint32_t insert_index;
  uint32_t handle_index;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_transition_result_rsp_fn;

  { /* VSM Expects an array of TX handles + RX handles */
    attach_table = &session_obj->active_set.attach_table;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_update_vsm_attach_table(): rx_device_count: %d, tx_device_count: %d",
                                          attach_table->rx_device_cnt,
                                          attach_table->tx_device_cnt );

    handle_size = sizeof( attach_table->rx_device_handle[ 0 ] );
    payload_size = ( ( attach_table->tx_device_cnt + attach_table->rx_device_cnt ) * handle_size ) +
	                   sizeof(routing_args->tx_device_count) +
					   sizeof(routing_args->rx_device_count) ;

    rc = __aprv2_cmd_alloc_ext(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle,
           opcode,
           payload_size,
           &new_packet );
    CVS_PANIC_ON_ERROR( rc );

    /* Copy payload */
    routing_args = APRV2_PKT_GET_PAYLOAD( vsm_routing_config_t, new_packet );

    routing_args->tx_device_count = attach_table->tx_device_cnt;
    routing_args->rx_device_count = attach_table->rx_device_cnt;

    insert_index = 0;

    for ( handle_index = 0; handle_index < attach_table->tx_device_cnt; handle_index += 1 )
    {
      routing_args->handles[ insert_index ] = session_obj->active_set.attach_table.tx_device_handle[ handle_index ];
      insert_index += 1;
    }

    for ( handle_index = 0; handle_index < attach_table->rx_device_cnt; handle_index += 1 )
    {
      routing_args->handles[ insert_index ] = session_obj->active_set.attach_table.rx_device_handle[ handle_index ];
      insert_index += 1;
    }

    rc = __aprv2_cmd_forward( cvs_apr_handle, new_packet );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }
}

static void cvs_attach_vocproc_update_cvp (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  vss_ivocproc_cmd_attach_stream_t vocproc_attach;
  uint16_t stream_handle = session_obj->target_set.attach_detach_stream_handle;
  uint16_t vocproc_handle = session_obj->target_set.attach_detach_vocproc_handle;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_attach_vocproc_cvp_transition_result_rsp_fn;

  /* Tell VocProc the stream's direction. */
  vocproc_attach.direction = session_obj->active_set.direction;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, stream_handle,
         cvs_cvp_addr, vocproc_handle,
         job_obj->header.handle, VSS_IVOCPROC_CMD_ATTACH_STREAM,
         &vocproc_attach, sizeof( vss_ivocproc_cmd_attach_stream_t ) );
  CVS_COMM_ERROR( rc, cvs_cvp_addr );
}

static void cvs_detach_vocproc_update_cvp (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;
  uint16_t stream_handle = session_obj->target_set.attach_detach_stream_handle;
  uint16_t vocproc_handle = session_obj->target_set.attach_detach_vocproc_handle;

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_attach_vocproc_cvp_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, stream_handle,
         cvs_cvp_addr, vocproc_handle,
         job_obj->header.handle, VSS_IVOCPROC_CMD_DETACH_STREAM,
         NULL, 0 );
  CVS_COMM_ERROR( rc, cvs_cvp_addr );
}

static int32_t cvs_do_goal_vocproc_attachment (
  cvs_session_object_t* session_obj,
  cvs_goal_enum_t attach_goal
)
{
  int32_t rc;
  cvs_state_enum_t state;

  switch ( session_obj->session_ctrl.action )
  {
  case CVS_ACTION_ENUM_NONE:
    {
      /* Send message to CVP and proceed to updating VSM */
      state = ( session_obj->session_ctrl.state == CVS_STATE_ENUM_IDLE ) ?
              CVS_STATE_ENUM_IDLE_ENTRY : CVS_STATE_ENUM_RUN_ENTRY;
      cvs_update_session_ctrl_state( session_obj, state) ;
      session_obj->session_ctrl.action  = CVS_ACTION_ENUM_UPDATE_VSM_ATTACH_TABLE;
      session_obj->session_ctrl.status  = APR_UNDEFINED_ID_V;    /* This insures that it will stay in IDLE_ENTRY until message from CVP comes */

      if (CVS_GOAL_ENUM_ATTACH_VOCPROC == attach_goal)
      {
        cvs_attach_vocproc_update_cvp(session_obj);
      }
      else
      {
        cvs_detach_vocproc_update_cvp(session_obj);
      }
    }
    break;

  case CVS_ACTION_ENUM_UPDATE_VSM_ATTACH_TABLE:
    {
      /* Check if CVP update was successfull */
      if (APR_EOK==session_obj->session_ctrl.status)
      {
        /* TBD: Why put data in pending ctrl?
           Put into low pirority task for now. */
        cvs_pending_control_t* ctrl = &cvs_low_task_pending_ctrl;

        state = ( session_obj->session_ctrl.state == CVS_STATE_ENUM_IDLE ) ?
                CVS_STATE_ENUM_IDLE_ENTRY : CVS_STATE_ENUM_RUN_ENTRY;
        cvs_update_session_ctrl_state( session_obj, state );
        session_obj->session_ctrl.action = CVS_ACTION_ENUM_COMPLETE;
        session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;    /* Only tranistion when status updates */

        if (CVS_GOAL_ENUM_ATTACH_VOCPROC == attach_goal)
        {
          /* Update the session's local settings */
          cvs_attach_vocproc(session_obj, ctrl->temp_data[0], ctrl->temp_data[1], session_obj->target_set.attach_detach_vocproc_handle);
          /* Now update the VSM attach table */
          ( void )cvs_update_vsm_attach_table(session_obj, VSM_CMD_ATTACH_VOICE_PROC);
        }
        else
        {
          /* Now update the VSM attach table */
          ( void )cvs_update_vsm_attach_table(session_obj, VSM_CMD_DETACH_VOICE_PROC);
          /* Update the session's local settings */
          cvs_detach_vocproc(session_obj, ctrl->temp_data[0], ctrl->temp_data[1], session_obj->target_set.attach_detach_vocproc_handle);
        }
      }
      else
      {
        /* Terminate Goal (this should send message back to client with error) */
        ( void )cvs_do_complete_goal(session_obj);
        return APR_EOK;
      }
    }
    break;

  case CVS_ACTION_ENUM_COMPLETE:
    {
      /* End VSM update job and end the over all goal! */
      rc = cvs_do_complete_goal( session_obj );
    }
    rc = APR_EOK;
    return rc;

  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EPENDING;  /* Not done */
}

static void cvs_log_state_info (
  cvs_session_object_t* session_obj
)
{
  static cvs_state_enum_t prev_state = CVS_STATE_ENUM_UNINITIALIZED;
  static cvs_goal_enum_t prev_goal = CVS_GOAL_ENUM_UNINITIALIZED;
  static cvs_action_enum_t prev_action = CVS_ACTION_ENUM_UNINITIALIZED;

  if ( session_obj == APR_NULL_V )
    return;

  /* Show new state information. */
  if ( ( prev_state != session_obj->session_ctrl.state ) ||
       ( prev_goal != session_obj->session_ctrl.goal ) ||
       ( prev_action != session_obj->session_ctrl.action ) )
  {
    /* Update information. */
    prev_state = session_obj->session_ctrl.state;
    prev_goal = session_obj->session_ctrl.goal;
    prev_action = session_obj->session_ctrl.action;

    /* Log goal. */
    switch ( session_obj->session_ctrl.goal )
    {
    case CVS_GOAL_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_NONE", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_CREATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_CREATE", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_REINIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_REINIT", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_DESTROY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_DESTROY", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_DISABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_DISABLE", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_ENABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_ENABLE", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_ATTACH_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_ATTACH_VOCPROC", session_obj->header.handle );
      break;

    case CVS_GOAL_ENUM_DETACH_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_GOAL_ENUM_DETACH_VOCPROC", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVS_GOAL_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log state. */
    switch ( session_obj->session_ctrl.state )
    {
    case CVS_STATE_ENUM_RESET_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_RESET_ENTRY", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_RESET:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_RESET", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_IDLE_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_IDLE_ENTRY", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_IDLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_IDLE", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_RUN_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_RUN_ENTRY", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_RUN:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_RUN", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_ERROR_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_ERROR_ENTRY", session_obj->header.handle );
      break;

    case CVS_STATE_ENUM_ERROR:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_STATE_ENUM_ERROR", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVS_STATE_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log action. */
    switch ( session_obj->session_ctrl.action )
    {
    case CVS_ACTION_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_NONE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_COMPLETE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_COMPLETE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_CONTINUE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_CONTINUE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_UPDATE_VSM_ATTACH_TABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_UPDATE_VSM_ATTACH_TABLE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_CREATE_SESSION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_CREATE_SESSION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_REINIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_REINIT", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_MEDIA_TYPE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_MEDIA_TYPE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_PACKET_EXCHANGE_MODE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_PACKET_EXCHANGE_MODE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_OOB_PACKET_EXCHANGE_CONFIG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_OOB_PACKET_EXCHANGE_CONFIG", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_RECONFIG_MAILBOX_TIMING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_RECONFIG_MAILBOX_TIMING", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_VOICE_TIMING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_VOICE_TIMING", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_TX_MUTE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_TX_MUTE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_RX_MUTE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_RX_MUTE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_UI_PROPERTIES:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_UI_PROPERTIES", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_GET_MAX_MAILBOX_PKT_SIZE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_GET_MAX_MAILBOX_PKT_SIZE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_RUN:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_RUN", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_STOP:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_STOP", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_RESET_MAILBOX_BOOK_KEEPING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_RESET_MAILBOX_BOOK_KEEPING", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_DESTROY_SESSION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_DESTROY_SESSION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_MAP_SHARED_MEMORY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_MAP_SHARED_MEMORY", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_UNMAP_SHARED_MEMORY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_UNMAP_SHARED_MEMORY", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_START_RECORD:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_START_RECORD", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_STOP_RECORD:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_STOP_RECORD", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_START_PLAYBACK:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_START_PLAYBACK", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_STOP_PLAYBACK:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_STOP_PLAYBACK", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_RX_DTMF_DETECT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_RX_DTMF_DETECT", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SEND_READY_NOTIFICATION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SEND_READY_NOTIFICATION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SEND_NOT_READY_NOTIFICATION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SEND_NOT_READY_NOTIFICATION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_OPEN_AVTIMER:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_OPEN_AVTIMER", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SEND_AVSYNC_RX_DELAY_NOTIFICATION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SEND_AVSYNC_RX_DELAY_NOTIFICATION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_CLOSE_AVTIMER:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_CLOSE_AVTIMER", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_DTX_MODE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_DTX_MODE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_VAR_VOC_SAMPLING_RATE:
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVS_ACTION_ENUM_SET_VAR_VOC_SAMPLING_RATE" );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_RATE_MODULATION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_RATE_MODULATION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_MINMAX_RATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_MINMAX_RATE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_ENC_RATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_ENC_RATE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_CHANNEL_AWARE_MODE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_CHANNEL_AWARE_MODE", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CACHED_ENC_OPERATING_MODE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CACHED_ENC_OPERATING_MODE", session_obj->header.handle );
      break;
      
    case CVS_ACTION_ENUM_SEND_EVS_BANDWIDTH_NOTIFICATION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SEND_EVS_BANDWIDTH_NOTIFICATION", session_obj->header.handle );
      break;

    case CVS_ACTION_ENUM_SET_CLOCKS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_SET_CLOCKS", session_obj->header.handle  );
      break;

    case CVS_ACTION_ENUM_REVERT_CLOCKS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVS_ACTION_ENUM_REVERT_CLOCKS", session_obj->header.handle  );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVS_ACTION_ENUM_INVALID", session_obj->header.handle );
      break;
    }
  }
}

/****************************************************************************
 * CVS SESSION STATE MACHINE                                                *
 ****************************************************************************/

 /* Write access to session_ctrl.state. 
    Lock protect it with per session lock. */
 static void cvs_update_session_ctrl_state (
   cvs_session_object_t* session_obj,
   cvs_state_enum_t state
 )
 {
   /* This function assumes that the input parameters are valid. */
   apr_lock_enter( session_obj->lock );
   session_obj->session_ctrl.state = state;
   apr_lock_leave( session_obj->lock );
 }

static int32_t cvs_state_reset_entry (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  /* In case of failure , end the goal. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_state_reset_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
    rc = cvs_do_complete_goal( session_obj );
    CVS_PANIC_ON_ERROR( rc );
  }

  cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET );
  return APR_EIMMEDIATE;
}

static int32_t cvs_state_reset (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVS_GOAL_ENUM_NONE:
    break;

  case CVS_GOAL_ENUM_CREATE:
    { /* New action: Acquire resource handles. (RESET to INIT transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_SET_CLOCKS:
       {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_CREATE_SESSION;
          session_obj->session_ctrl.status = APR_EOK;

          rc = vccm_mmpm_request_clocks( VCCM_CLIENT_ID_CVS );
       }
       return APR_EIMMEDIATE;

      case CVS_ACTION_ENUM_CREATE_SESSION:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_MAP_SHARED_MEMORY;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_create_stream( session_obj );

          /* Write access to active_set.direction. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.direction = session_obj->target_set.direction;
          ( void ) apr_lock_leave( session_obj->lock );
        }
        return rc;

      case CVS_ACTION_ENUM_MAP_SHARED_MEMORY:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_MEDIA_TYPE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_map_shared_memory( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_MEDIA_TYPE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_media_type( session_obj );

          /* Write access to active_set.media_id. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.media_id = session_obj->target_set.media_id;
          ( void ) apr_lock_leave( session_obj->lock );
        }
        return rc;

      case CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_register_voc_op_mode_update_event( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE:
        {
          /* Note that this action assumes voice firmware operates properly,
           * i.e. voice firmware will properly send the vocoder operating mode
           * update events upon event registration. If voice firmware doesn't
           * operate properly, the CVS state machine will stall in this action
           * and one has to look at the logs to debug. In the future, CVD may
           * need to implement a timeout mechanism for handling VCP
           * unresponsiveness.
           */
          if ( ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) &&
                 ( session_obj->voc_operating_mode_info.is_rx_mode_received == TRUE ) ) ||
               ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) &&
                 ( session_obj->voc_operating_mode_info.is_tx_mode_received == TRUE ) ) ||
               ( ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) &&
                 ( session_obj->voc_operating_mode_info.is_rx_mode_received == TRUE ) &&
                 ( session_obj->voc_operating_mode_info.is_tx_mode_received == TRUE ) ) )
          {
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_REVERT_CLOCKS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          { /* Continue waiting for the vocoder mode update events. */
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE;
            session_obj->session_ctrl.status = APR_EOK;
          }
        }
        return APR_EOK;

      case CVS_ACTION_ENUM_REVERT_CLOCKS:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_EOK;

          rc = vccm_mmpm_release_clocks( VCCM_CLIENT_ID_CVS );
        }
        return APR_EIMMEDIATE;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVS_GOAL_ENUM_DESTROY:
    { /* Complete the pending command and stay in the same state. */
      session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
      rc = cvs_do_complete_goal( session_obj );
    }
    break;

  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvs_state_idle_entry (
  cvs_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  /* In case of failure , end the goal. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_state_idle_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE );
  return APR_EIMMEDIATE;
}

static int32_t cvs_state_idle (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVS_GOAL_ENUM_NONE:
    break;

  case CVS_GOAL_ENUM_CREATE:
  case CVS_GOAL_ENUM_DISABLE:
    { /* Complete the pending command and stay in the same state. */
      session_obj->session_ctrl.status = APR_EOK; /*  TODO: Handle errors. */
      rc = cvs_do_complete_goal( session_obj );
    }
    break;

  case CVS_GOAL_ENUM_REINIT:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_REINIT:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_MEDIA_TYPE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_reinit_stream( session_obj );

          session_obj->common_cal.is_calibrate_needed =
            session_obj->common_cal.is_registered; /* BACKWARD COMPATIBILITY */

          session_obj->static_cal.is_calibrate_needed =
            session_obj->static_cal.is_registered;

          /* Write access to active_set.direction/packet_logging_info. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.direction = session_obj->target_set.direction;
          session_obj->packet_logging_info.voice_call_num++;
          session_obj->packet_logging_info.rx_packet_seq_num = 0;
          session_obj->packet_logging_info.tx_packet_seq_num = 0;
          ( void ) apr_lock_leave( session_obj->lock );

          /* Reset the eAMR mode to default (i.e. narrowband) after reinitialize
           * VSM. Refer to the comments under
           * eamr_mode_change_notification_info in cvs_session_object_t for
           * details.
           */
          session_obj->eamr_mode_change_notification_info.mode = 
            VSS_ISTREAM_EAMR_MODE_NARROWBAND;

          /* Reset the EVS Bandwidth to default (i.e. super wide-band) after 
           * reinitialize VSM. Refer to the comments under
           * evs_bandwidth_change_notification_info in cvs_session_object_t for
           * details.
           */            
          session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = 
            CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET;  
            
          session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = 
            CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET;

          session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
        }
        return rc;

      case CVS_ACTION_ENUM_SET_MEDIA_TYPE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_DTX_MODE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_media_type( session_obj );

          /* Write access to target_set.media_id. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.media_id = session_obj->target_set.media_id;
          ( void ) apr_lock_leave( session_obj->lock );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_DTX_MODE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_RATE_MODULATION;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_DTX );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_RATE_MODULATION:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_MINMAX_RATE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_RATE_MODULATION );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_MINMAX_RATE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_ENC_RATE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_MINMAX_RATE );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_ENC_RATE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_CHANNEL_AWARE_MODE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_ENC_RATE );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_CHANNEL_AWARE_MODE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_CACHED_ENC_OPERATING_MODE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_CHANNEL_AWARE_MODE );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_CACHED_ENC_OPERATING_MODE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_VAR_VOC_SAMPLING_RATE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_cached_voc_property( session_obj,
                 CVS_VOC_PROPERTY_ENUM_ENC_OPERATING_MODE );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_VAR_VOC_SAMPLING_RATE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_var_voc_sampling_rate( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_register_voc_op_mode_update_event( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE:
        {
          /* Note that this action assumes voice firmware operates properly,
           * i.e. voice firmware will properly send the vocoder operating mode
           * update events upon event registration. If voice firmware doesn't
           * operate properly, the CVS state machine will stall in this action
           * and one has to look at the logs to debug. In the future, CVD may
           * need to implement a timeout mechanism for handling VCP
           * unresponsiveness.
           */
          if ( ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) &&
                 ( session_obj->voc_operating_mode_info.is_rx_mode_received == TRUE ) ) ||
               ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) &&
                 ( session_obj->voc_operating_mode_info.is_tx_mode_received == TRUE ) ) ||
               ( ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) &&
                 ( session_obj->voc_operating_mode_info.is_rx_mode_received == TRUE ) &&
                 ( session_obj->voc_operating_mode_info.is_tx_mode_received == TRUE ) ) )
          {
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_PACKET_EXCHANGE_MODE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          { /* Continue waiting for the vocoder mode update events. */
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE;
            session_obj->session_ctrl.status = APR_EOK;
          }
        }
        return APR_EOK;

      case CVS_ACTION_ENUM_SET_PACKET_EXCHANGE_MODE:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_OOB_PACKET_EXCHANGE_CONFIG;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_packet_exchange_mode( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_OOB_PACKET_EXCHANGE_CONFIG:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_COMPLETE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_oob_packet_exchange_config( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK; /*  TODO: Handle errors. */
          rc = cvs_do_complete_goal( session_obj );
        }
        break;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVS_GOAL_ENUM_ATTACH_VOCPROC:
    {
      ( void ) cvs_do_goal_vocproc_attachment( session_obj,
                                               CVS_GOAL_ENUM_ATTACH_VOCPROC );
    }
    break;

  case CVS_GOAL_ENUM_DETACH_VOCPROC:
    {
      ( void ) cvs_do_goal_vocproc_attachment( session_obj,
                                               CVS_GOAL_ENUM_DETACH_VOCPROC );
    }
    break;

  case CVS_GOAL_ENUM_DESTROY:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_UNMAP_SHARED_MEMORY:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_DESTROY_SESSION;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_unmap_shared_memory( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_DESTROY_SESSION:
        { /* Destroy stream session. */
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RESET_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_destroy_stream( session_obj );
        }
        return rc;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVS_GOAL_ENUM_ENABLE:
    { /* New action: Run the voice stream. (IDLE to RUN transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_CONTINUE:
      case CVS_ACTION_ENUM_RECONFIG_MAILBOX_TIMING:
        {
          if ( ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_MAILBOX ) &&
               ( session_obj->packet_exchange_info.mailbox_info.is_configured == FALSE ) )
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_state_idle(): " \
                 "Stream packet exchange mode is mailbox but mailbox not configured." );

            /* Complete the pending command and stay in the same state. */
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_COMPLETE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }

          if ( ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE ) &&
               ( session_obj->packet_exchange_info.mailbox_info.config.is_time_ref_received == FALSE ) )
          {
            rc = cvs_action_reconfig_mailbox_timing( session_obj );

            /* Complete the pending command and stay in the same state. */
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_COMPLETE;
            session_obj->session_ctrl.status = APR_EOK;
          }
          else
          {
            cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
            session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_VOICE_TIMING;
            session_obj->session_ctrl.status = APR_EOK;
          }
        }
        return APR_EIMMEDIATE;

      case CVS_ACTION_ENUM_SET_VOICE_TIMING:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_TX_MUTE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_voice_timing( session_obj );

          /* Update active set. */
          session_obj->active_set.system_config.vsid = session_obj->target_set.system_config.vsid;
          session_obj->active_set.system_config.vfr_mode = session_obj->target_set.system_config.vfr_mode;
          session_obj->active_set.voice_timing = session_obj->target_set.voice_timing;
        }
        return rc;

      case CVS_ACTION_ENUM_SET_TX_MUTE:
        {
          /*Push down the MUTE settigns*/
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_RX_MUTE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_tx_mute( session_obj);
		  
          /* Update active set. */
          /* Write access to active_set.mute.
             Read access to target_set.mute. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.mute.tx_mute_flag = session_obj->target_set.mute.tx_mute_flag;
          session_obj->active_set.mute.tx_ramp_duration = session_obj->target_set.mute.tx_ramp_duration;
          ( void ) apr_lock_leave( session_obj->lock );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_RX_MUTE:
        {
          /*Push down the MUTE settigns*/
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SET_UI_PROPERTIES;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_rx_mute( session_obj);
		  
          /* Update active set. */
          /* Write access to active_set.mute.
             Read access to target_set.mute. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->active_set.mute.rx_mute_flag = session_obj->target_set.mute.rx_mute_flag;
          session_obj->active_set.mute.rx_ramp_duration = session_obj->target_set.mute.rx_ramp_duration;
          ( void ) apr_lock_leave( session_obj->lock );
        }
        return rc;

      case CVS_ACTION_ENUM_SET_UI_PROPERTIES:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SEND_AVSYNC_RX_DELAY_NOTIFICATION;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_set_ui_properties( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_SEND_AVSYNC_RX_DELAY_NOTIFICATION:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SEND_EVS_BANDWIDTH_NOTIFICATION;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_send_avsync_rx_delay_notification( session_obj );
        }
        return rc;
        
      case CVS_ACTION_ENUM_SEND_EVS_BANDWIDTH_NOTIFICATION:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SEND_READY_NOTIFICATION;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_send_evs_bandwidth_notification( session_obj );
        }
        return rc;
        

      case CVS_ACTION_ENUM_SEND_READY_NOTIFICATION:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_GET_MAX_MAILBOX_PKT_SIZE;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvs_action_send_ready_notification( session_obj );
        }
        return APR_EIMMEDIATE;

      case CVS_ACTION_ENUM_GET_MAX_MAILBOX_PKT_SIZE:
        { /* Get the maximum mailbox vocoder packet size. */
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_RUN;
          session_obj->session_ctrl.status = APR_EOK;

          if ( ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE ) &&
               ( cvs_media_id_is_valid( session_obj->active_set.media_id ) == TRUE ) )
          {
            rc = cvs_action_get_max_mailbox_pkt_size( session_obj );
          }
        }
        return APR_EIMMEDIATE;

      case CVS_ACTION_ENUM_RUN:
        { /* Run the voice stream. */
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_START_RECORD;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_run( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_START_RECORD:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_START_PLAYBACK;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_start_record( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_START_PLAYBACK:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_RX_DTMF_DETECT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_start_playback( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_RX_DTMF_DETECT:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_rx_dtmf_detect( session_obj, VSM_RX_DTMF_DETECTION_ENABLE );
        }
        return rc;

      case CVS_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK;
          rc = cvs_do_complete_goal( session_obj );
        }
        break;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvs_state_run_entry (
  cvs_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_state_run_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN );
  return APR_EIMMEDIATE;
}

static int32_t cvs_state_run (
  cvs_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVS_GOAL_ENUM_NONE:
    break;

  case CVS_GOAL_ENUM_DESTROY:
  case CVS_GOAL_ENUM_DISABLE:
    { /* New action: Stop the voice stream. (RUN to IDLE transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_STOP_RECORD:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_STOP_PLAYBACK;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_stop_record( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_STOP_PLAYBACK:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_RX_DTMF_DETECT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_stop_playback( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_RX_DTMF_DETECT:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_STOP;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_rx_dtmf_detect( session_obj, VSM_RX_DTMF_DETECTION_DISABLE );
        }
        return rc;

      case CVS_ACTION_ENUM_STOP:
        { /* Stop the voice stream. */
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_RESET_MAILBOX_BOOK_KEEPING;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvs_action_stop( session_obj );
        }
        return rc;

      case CVS_ACTION_ENUM_RESET_MAILBOX_BOOK_KEEPING:
        {
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_RUN_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_SEND_NOT_READY_NOTIFICATION;
          session_obj->session_ctrl.status = APR_EOK;

          if ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE )
          {
            rc = cvs_action_reset_mailbox_book_keeping( session_obj );
          }
        }
        return APR_EIMMEDIATE;

      case CVS_ACTION_ENUM_SEND_NOT_READY_NOTIFICATION:
        { /* Send NOT_READY notification to full control client. */
          cvs_update_session_ctrl_state( session_obj, CVS_STATE_ENUM_IDLE_ENTRY );
          session_obj->session_ctrl.action = CVS_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvs_action_send_not_ready_notification( session_obj );
        }
        return APR_EIMMEDIATE;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVS_GOAL_ENUM_ATTACH_VOCPROC:
    {
      if ( APR_EOK == cvs_do_goal_vocproc_attachment( session_obj, CVS_GOAL_ENUM_ATTACH_VOCPROC ) )
      {
        return APR_EOK;
      }
    }
    break;

  case CVS_GOAL_ENUM_DETACH_VOCPROC:
    {
      if ( APR_EOK == cvs_do_goal_vocproc_attachment( session_obj, CVS_GOAL_ENUM_DETACH_VOCPROC ) )
      {
        return APR_EOK;
      }
    }
    break;

  case CVS_GOAL_ENUM_ENABLE:
    { /* New action: Re-enable. (RUN to IDLE transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case CVS_ACTION_ENUM_NONE:
      case CVS_ACTION_ENUM_CONTINUE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
          rc = cvs_do_complete_goal( session_obj );
        }
        return APR_EOK;

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvs_state_control (
  cvs_session_object_t* session_obj
)
{
  int32_t rc = APR_EOK;

  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  do
  {
    cvs_log_state_info( session_obj );

    switch ( session_obj->session_ctrl.state )
    {
    case CVS_STATE_ENUM_RESET_ENTRY:
      rc = cvs_state_reset_entry( session_obj );
      break;

    case CVS_STATE_ENUM_RESET:
      rc = cvs_state_reset( session_obj );
      break;

    case CVS_STATE_ENUM_IDLE_ENTRY:
      rc = cvs_state_idle_entry( session_obj );
      break;

    case CVS_STATE_ENUM_IDLE:
      rc = cvs_state_idle( session_obj );
      break;

    case CVS_STATE_ENUM_RUN_ENTRY:
      rc = cvs_state_run_entry( session_obj );
      break;

    case CVS_STATE_ENUM_RUN:
      rc = cvs_state_run( session_obj );
      break;

    default:
      rc = APR_EUNEXPECTED;
      break;
    }
  }
  while ( rc == APR_EIMMEDIATE );

  cvs_log_state_info( session_obj );

  if ( rc == APR_ECONTINUE )
  { /* TODO: Use a timer to trigger the state machine to run after 5ms. */
  }

  return rc;
}

/****************************************************************************
 * Helper Functions                                                         *
 ****************************************************************************/

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t cvs_helper_open_session_control (
  cvs_pending_control_t* ctrl,
  cvs_indirection_object_t** ret_indirect_obj,
  cvs_session_object_t** ret_session_obj
)
{
  int32_t rc;
  cvs_indirection_object_t* indirect_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    if ( ctrl == NULL )
    {
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( ret_session_obj == NULL )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvs_get_typed_object( ctrl->packet->dst_port,
                               CVS_OBJECT_TYPE_ENUM_INDIRECTION,
                               ( ( cvs_object_t** ) &indirect_obj ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_helper_open_session_control(): Invalid handle: cmd_opcode=0x%08X src_addr=0x%08X req_handle=0x%08X",
                                              ctrl->packet->opcode,
                                              ctrl->packet->src_addr,
                                              ctrl->packet->dst_port );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      rc = APR_EHANDLE;
      break;
    }

    rc = cvs_get_typed_object( indirect_obj->session_handle,
                               CVS_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvs_object_t** ) ret_session_obj ) );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      rc = APR_EHANDLE;
      break;
    }

    if ( ret_indirect_obj != NULL )
    {
      *ret_indirect_obj = indirect_obj;
    }

    break;
  }

  return rc;
}

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t cvs_helper_validate_payload_size_control (
  cvs_pending_control_t* ctrl,
  uint32_t valid_size
)
{
  int32_t rc;
  uint32_t size;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
  if ( size != valid_size )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_helper_validate_payload_size_control(): Unexpected size, %d != %d",
                                            size, valid_size );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );

    return APR_EBADPARAM;
  }

  return APR_EOK;
}

static bool_t cvs_helper_verify_full_control (
  cvs_pending_control_t* ctrl,
  cvs_indirection_object_t* indirect_obj
)
{
  int32_t rc;
  uint16_t client_addr;

  if ( ctrl == NULL || indirect_obj == NULL )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return FALSE;
  }

  if ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                      indirect_obj->access_bits ) )
  {
    return TRUE;
  }
  else
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_helper_verify_full_control(): Access denied: opcode=0x%08X src_addr=0x%08X src_port=0x%08X",
                                            ctrl->packet->opcode,
                                            ctrl->packet->src_addr,
                                            ctrl->packet->src_port );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EDENIED );
    CVS_COMM_ERROR( rc, client_addr );

    return FALSE;
  }
}

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t cvs_helper_create_new_goal_control (
  cvs_pending_control_t* ctrl,
  cvs_session_object_t* session_obj,
  cvs_goal_enum_t new_goal
)
{
  int32_t rc;
  cvs_simple_job_object_t* job_obj;

  if ( ( ctrl == NULL ) || ( session_obj == NULL ) )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( session_obj->session_ctrl.goal != CVS_GOAL_ENUM_NONE )
  { /* The session shouldn't be doing anything. */
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );

  session_obj->session_ctrl.goal = new_goal;
    /* The goal is reset by the state machine on completion. */
  session_obj->session_ctrl.pendjob_handle = job_obj->header.handle;
    /* The pendjob_handle will signal completion. The pendjob_handle is
     * reset by the state machine on completion.
     */
  ctrl->pendjob_obj = ( ( cvs_object_t* ) job_obj );
    /* pendjob_obj stores the job_obj to be checked for completion and to be
     * freed by the current pending command control.
     */

  return APR_EOK;
}

/* Returns APR_EOK when done and packet completed, else APR_EPENDING. */
static int32_t cvs_helper_simple_wait_for_goal_completion_control (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  /* Wait until the job is done. */
  if ( ctrl->pendjob_obj->simple_job.is_completed )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    rc = cvs_free_object( ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts);
    CVS_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  return APR_EPENDING;
}

static void cvs_send_avsync_tx_timestamp(
  cvs_session_object_t* session_obj
)
{
  int32_t rc;
  vocsvc_avtimer_timestamp_t avtimer_timestamp;
  vss_iavsync_evt_tx_timestamp_t avsync_tx_timestamp;

  avtimer_timestamp.timestamp_us = 0;
  avsync_tx_timestamp.timestamp_us = 0;

  if( session_obj->avsync_client_tx_info.is_enabled == TRUE )
  {
#ifndef WINSIM
    rc = vocsvc_avtimer_get_time( &avtimer_timestamp );
    avsync_tx_timestamp.timestamp_us = avtimer_timestamp.timestamp_us - session_obj->avsync_delay_info.total_tx_delay ;
#endif

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_send_avsync_tx_timestamp(): "
           "AVTIMER timestamp %d, total_tx_delay %d",
           avtimer_timestamp.timestamp_us, session_obj->avsync_delay_info.total_tx_delay);

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->avsync_client_tx_info.client_addr,
           session_obj->avsync_client_tx_info.client_port,
           NULL, VSS_IAVSYNC_EVT_TX_TIMESTAMP,
           &avsync_tx_timestamp, sizeof( avsync_tx_timestamp ) );
    CVS_PANIC_ON_ERROR( rc );  
  }

  return;
}

/****************************************************************************
 * NON-GATING COMMAND PROCESSING FUNCTIONS                                  *
 ****************************************************************************/

/*
  This function access session object in following ways:
  - Read and write access:
    1. packet_logging_info.rx_packet_seq_num
  - Ready only access:
    1. packet_logging_info.call_num
    2. attached_mvm_handle
    3. master_cvs_port
    4. packet_exchange_info
  For read and write accessed items, we lock every access instance in 
  other thread.
  For read only accessed items, we only lock write access instance in 
  other thread.
*/
static int32_t cvs_stream_forward_pkt_exchange_evt_to_client_processing (
  aprv2_packet_t* packet,
  uint32_t forward_opcode
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ipktexg_evt_in_band_send_enc_buffer_t* enc_buf = NULL;
  vsm_oob_pkt_exchange_header_t* oob_enc_payload = NULL;
  uint32_t voc_packet_size;
  uint32_t* oob_enc_buf;
  uint64_t enc_buf_virt_addr = 0;
  uint32_t enc_buf_size;
  uint64_t avtimer_timestamp = 0;
  uint32_t handle;

  handle = packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_forward_pkt_exchange_evt_to_client_processing(): "
                                            "Invalid session handle: 0x%04X, result: "
                                            "0x%08X. Drop evt, Opcode: 0x%08X.",
                                            packet->dst_port,
                                            rc,
                                            packet->opcode );
    ( void ) cvs_access_session_obj_end( handle, FALSE );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
    return APR_EOK;
  }

  if ( packet->opcode == VSM_EVT_SEND_ENC_PACKET )
  {
   /* If the client is listening for VSS_IAVSYNC_EVENT_CLASS_TX
    * send VSS_IAVSYNC_EVT_TX_TIMESTAMP - TX path timestamp at MIC
    * in microseconds.
    */
    if ( session_obj->avsync_client_tx_info.is_enabled == TRUE )
    {
      cvs_send_avsync_tx_timestamp( session_obj );
    }
    enc_buf = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_evt_in_band_send_enc_buffer_t, packet );
    if ( APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) < sizeof( vss_ipktexg_evt_in_band_send_enc_buffer_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_forward_pkt_exchange_evt_to_client_processing(): "
                                              "Invalid payload size: %d < %d",
                                              APRV2_PKT_GET_PAYLOAD_BYTE_SIZE(packet->header),
                                              sizeof( vss_ipktexg_evt_in_band_send_enc_buffer_t ) );
      ( void ) cvs_access_session_obj_end( handle, FALSE );
      ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
      return APR_EOK;
    }
    else
    {
      voc_packet_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) - sizeof( vss_ipktexg_evt_in_band_send_enc_buffer_t );
    }

    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port. */
    ( void ) apr_lock_enter( session_obj->lock );
    ( void ) cvd_log_commit_data( LOG_ADSP_CVD_STREAM_TX,
                                  ++session_obj->packet_logging_info.tx_packet_seq_num,
                                  voc_packet_size,
                                  ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port,
                                  session_obj->packet_logging_info.voice_call_num,
                                  0,
                                  CVD_LOG_ENC_OUTPUT_TAP_POINT,
                                  enc_buf->media_id,
                                  ( ( uint8_t* )enc_buf ) + sizeof( vss_ipktexg_evt_in_band_send_enc_buffer_t ) );
    ( void ) apr_lock_leave( session_obj->lock );
  }

  if ( packet->opcode == VSM_EVT_OOB_ENC_BUF_READY )
  {
    /* TODO: use cvd memory mapper get pointer API. */
    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port/packet_exchange_info. */
    ( void ) apr_lock_enter( session_obj->lock );

    oob_enc_buf = ( uint32_t* )session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw;
    ( void ) cvd_log_commit_data( LOG_ADSP_CVD_STREAM_TX,
                                  ++session_obj->packet_logging_info.tx_packet_seq_num,
                                  *( oob_enc_buf + 2 ),
                                  ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port,
                                  session_obj->packet_logging_info.voice_call_num,
                                  *oob_enc_buf,
                                  CVD_LOG_ENC_OUTPUT_TAP_POINT,
                                  *( oob_enc_buf + 1 ),
                                  ( uint8_t* )( oob_enc_buf + 3 ) );

    /* Timestamp in OOB Packet updated used for AVSync ignored otherwise. */
    oob_enc_payload = ( ( vsm_oob_pkt_exchange_header_t* ) oob_enc_buf );
    enc_buf_virt_addr = ( uint64_t ) ( ( ( uint32_t )enc_buf_virt_addr ) | session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw );
    enc_buf_virt_addr = ( enc_buf_virt_addr << 32 ) | session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw;
    enc_buf_size = session_obj->packet_exchange_info.oob_info.config.enc_buf_size;

    ( void ) apr_lock_leave( session_obj->lock );

#ifndef WINSIM
    /* Get the current timestamp from AVTimer. */
    rc = vocsvc_avtimer_get_time( ( vocsvc_avtimer_timestamp_t* ) &avtimer_timestamp );
#endif /* WINSIM */
    /* Reduce the Total TX delay <processing + Algorithmic > from the current timestamp. */
    avtimer_timestamp -= session_obj->avsync_delay_info.total_tx_delay;
    oob_enc_payload->time_stamp = ( ( uint32_t) avtimer_timestamp );
    cvd_mem_mapper_cache_flush( enc_buf_virt_addr, enc_buf_size );
  }

  packet->opcode = forward_opcode;
  packet->src_addr = cvs_my_addr;
  packet->src_port = session_obj->master_cvs_port;
  packet->dst_addr = session_obj->master_client_addr;
  packet->dst_port = session_obj->master_client_port;

  ( void ) cvs_access_session_obj_end( handle, FALSE );
  rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
  CVS_COMM_ERROR( rc, session_obj->master_client_addr );

  return APR_EOK;
}

/*
  This function access session object in following ways:
  - Read and write access:
    1. packet_logging_info.rx_packet_seq_num
  - Ready only access:
    1. packet_logging_info.call_num
    2. attached_mvm_handle
    3. master_cvs_port
    4. packet_exchange_info
  For read and write accessed items, we lock every access instance in 
  other thread.
  For read only accessed items, we only lock write access instance in 
  other thread.
*/
static int32_t cvs_stream_forward_pkt_exchange_evt_to_vsm_processing (
  aprv2_packet_t* packet,
  uint32_t forward_opcode
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ipktexg_evt_in_band_send_dec_buffer_t* dec_buf = NULL;
  uint32_t voc_packet_size;
  uint32_t* oob_dec_buf;
  uint32_t indirect_handle;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
  if ( rc )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_forward_pkt_exchange_evt_to_vsm_processing(): "
                                            "Invalid session handle: 0x%04X, result: 0x%08X. "
                                            "Drop evt, Opcode: 0x%08X.",
                                            packet->dst_port,
                                            rc,
                                            packet->opcode );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

    return APR_EOK;
  }

  if ( packet->opcode == VSS_IPKTEXG_EVT_IN_BAND_SEND_DEC_BUFFER )
  {
    dec_buf = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_evt_in_band_send_dec_buffer_t, packet );
    if ( APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) < sizeof( vss_ipktexg_evt_in_band_send_dec_buffer_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_forward_pkt_exchange_evt_to_vsm_processing(): "
                                              "Invalid payload size: %d < %d",
                                              APRV2_PKT_GET_PAYLOAD_BYTE_SIZE(packet->header),
                                              sizeof( vss_ipktexg_evt_in_band_send_dec_buffer_t ) );
      ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );
      ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
      return APR_EOK;
    }
    else
    {
      voc_packet_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) - sizeof( vss_ipktexg_evt_in_band_send_dec_buffer_t );
    }

    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port. */
    ( void ) apr_lock_enter( session_obj->lock );
    ( void ) cvd_log_commit_data( LOG_ADSP_CVD_STREAM_RX,
                                  ++session_obj->packet_logging_info.rx_packet_seq_num,
                                  voc_packet_size,
                                  ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port,
                                  session_obj->packet_logging_info.voice_call_num,
                                  0,
                                  CVD_LOG_DEC_INPUT_TAP_POINT,
                                  dec_buf->media_id,
                                  ( ( uint8_t* )dec_buf ) + sizeof( vss_ipktexg_evt_in_band_send_dec_buffer_t ) );
    ( void ) apr_lock_leave( session_obj->lock );
  }

  if ( packet->opcode == VSS_IPKTEXG_EVT_OOB_NOTIFY_DEC_BUFFER_READY )
  {
    /* Read/write access to packet_logging_info. 
       Read access to attached_mvm_handle/master_cvs_port/packet_exchange_info. */
    apr_lock_enter( session_obj->lock );

    /* TODO: use cvd memory mapper get pointer API. */
    oob_dec_buf = ( uint32_t* )session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw;
    ( void ) cvd_log_commit_data( LOG_ADSP_CVD_STREAM_RX,
                                  ++session_obj->packet_logging_info.rx_packet_seq_num,
                                  *( oob_dec_buf + 2 ),
                                  ( session_obj->attached_mvm_handle << 16 ) | session_obj->master_cvs_port,
                                  session_obj->packet_logging_info.voice_call_num,
                                  *oob_dec_buf,
                                  CVD_LOG_DEC_INPUT_TAP_POINT,
                                  *( oob_dec_buf + 1 ),
                                  ( uint8_t* )( oob_dec_buf + 3 ) );

    ( void ) apr_lock_leave( session_obj->lock );
  }

  packet->opcode = forward_opcode;
  packet->src_addr = cvs_my_addr;
  packet->src_port = ( ( uint16_t ) session_obj->header.handle );
  packet->dst_addr = session_obj->vsm_stream_addr;
  packet->dst_port = session_obj->vsm_stream_handle;

  rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

/*
  This function access session object in following ways:
  - Read only
    1. packet_exchange_info
  We do lock here. We also lock in the thread where write access is performed.
  Note the actually packet processing function may also perform lock operations.
*/
static int32_t cvs_stream_oob_enc_buf_ready_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint32_t handle;

  handle = packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_oob_enc_buf_ready_evt_processing(): "
           "Invalid session handle: 0x%04X, result: 0x%08X. Drop evt.", packet->dst_port, rc );

    ( void ) cvs_access_session_obj_end( handle, FALSE );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

    return APR_EOK;
  }

  /* Read access to packet_exchange_info.mode. */
  ( void ) apr_lock_enter( session_obj->lock );
  if ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_MAILBOX )
  {
    rc = cvs_pktexg_mailbox_tx_processing( session_obj, packet );
  }
  else
  { /* Forward OOB event to client if CVS's packet exchange mode is OOB. */
    rc = cvs_stream_forward_pkt_exchange_evt_to_client_processing(
           packet, VSS_IPKTEXG_EVT_OOB_NOTIFY_ENC_BUFFER_READY );
  }
  ( void ) apr_lock_leave( session_obj->lock );

  ( void ) cvs_access_session_obj_end( handle, FALSE );

  return rc;
}

/*
  This function access session object in following ways:
  - Read only
    1. packet_exchange_info
  We do lock here. We also lock in the thread where write access is performed.
  Note the actually packet processing function may also perform lock operations.
*/
static int32_t cvs_stream_oob_dec_buf_request_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint32_t handle;

  handle = packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_oob_dec_buf_request_evt_processing(): "
           "Invalid session handle: 0x%04X, result: 0x%08X. Drop evt.", packet->dst_port, rc );

    ( void ) cvs_access_session_obj_end( handle, FALSE );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

    return APR_EOK;
  }

  /* Read access to packet_exchange_info. */
  ( void ) apr_lock_enter( session_obj->lock );
  if ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_MAILBOX )
  {
    rc = cvs_pktexg_mailbox_rx_processing( session_obj, packet );
  }
  else
  { /* Forward OOB event to client if CVS's packet exchange mode is OOB. */
    rc = cvs_stream_forward_pkt_exchange_evt_to_client_processing(
           packet, VSS_IPKTEXG_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST );
  }
  ( void ) apr_lock_leave( session_obj->lock );

  ( void ) cvs_access_session_obj_end( handle, FALSE );
  return rc;
}

/*
  This function access session object in following ways:
  - Read only
    1. vpcm_info
    2. vsm_stream_handle
  We do lock here. We also lock in the thread where write access is performed.
*/
static int32_t cvs_vpcm_evt_push_buffer_to_vsm_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ivpcm_evt_push_buffer_v2_t* payload;
  uint32_t payload_len;
  voice_evt_push_host_pcm_buf_v2_t push_buf_event;
  uint64_t out_buf_virt_addr = ( uint64_t ) NULL;
  uint64_t in_buf_virt_addr = ( uint64_t ) NULL;
  uint32_t cvd_mem_handle = ( uint64_t ) NULL;
  uint32_t indirect_handle;
  bool_t is_lock_acquired = TRUE;

  for ( ;; )
  {
    indirect_handle = packet->dst_port;
    rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
    if ( rc != APR_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Invalid params, packet->dst_port: 0x%4X",
                                              packet->dst_port );
      is_lock_acquired = FALSE;
      break;
    }

    /* Read access to vpcm_info. 
       Lock the whole processing here to make sure buffer is not
       released in the middle. */
    ( void ) apr_lock_enter( session_obj->lock );

    if ( session_obj->vpcm_info.is_enabled == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): VPCM not started" );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_evt_push_buffer_v2_t, packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    if ( payload_len != sizeof( vss_ivpcm_evt_push_buffer_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Invalid data. Payload len: %d, expected len: %d",
                                              payload_len,
                                              sizeof( vss_ivpcm_evt_push_buffer_v2_t ) );
      break;
    }

    cvd_mem_handle = session_obj->vpcm_info.mem_handle;

    if ( payload->push_buf_mask & VSS_IVPCM_PUSH_BUFFER_MASK_OUTPUT_BUFFER )
    {
      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->out_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Mis-aligned out_buf_mem_address: 0x%016X.",
                                                payload->out_buf_mem_address );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->out_buf_mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Mis-aligned out_buf_mem_size: %d.",
                                                payload->out_buf_mem_size );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( cvd_mem_handle, payload->out_buf_mem_address,
                                                     payload->out_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Memory is not within range, mem_handle: 0x%08X, out_buf_mem_address: 0x%016X, size: %d. ",
                                                cvd_mem_handle,
                                                payload->out_buf_mem_address,
                                                payload->out_buf_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( cvd_mem_handle,
                                            payload->out_buf_mem_address,
                                            &out_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Cannot get virtual address, mem_handle: 0x%08X, out_buf_mem_address: 0x%016X, ",
                                                cvd_mem_handle,
                                                payload->out_buf_mem_address );
        rc = APR_EFAILED;
        break;
      }
    }

    if ( payload->push_buf_mask & VSS_IVPCM_PUSH_BUFFER_MASK_INPUT_BUFFER )
    {
      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->in_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Mis-aligned in_buf_mem_address: 0x%016X.",
                                                payload->in_buf_mem_address );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( cvd_mem_handle, payload->in_buf_mem_address,
                                                     payload->in_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Memory is not within range, mem_handle: 0x%08X, in_buf_mem_address: 0x%016X, size: %d.",
                                                cvd_mem_handle,
                                                payload->in_buf_mem_address,
                                                payload->in_buf_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( cvd_mem_handle,
                                            payload->in_buf_mem_address,
                                            &in_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Cannot get virtual address, mem_handle: 0x%08X, in_buf_mem_address: 0x%016X.",
                                                cvd_mem_handle,
                                                payload->in_buf_mem_address );
        rc = APR_EFAILED;
        break;
      }
    }

    switch( payload->tap_point )
    {
    case VSS_IVPCM_TAP_POINT_TX_DEFAULT:
      push_buf_event.tap_point = VOICESTREAM_MODULE_TX;
      break;

    case VSS_IVPCM_TAP_POINT_RX_DEFAULT:
      push_buf_event.tap_point = VOICESTREAM_MODULE_RX;
      break;

    default:
      /* Drop and free the push buffer event. */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_push_buffer_to_vsm_processing(): Invalid tap point: 0x%08X",
                                              payload->tap_point );
      rc = APR_EBADPARAM;
      break;
    }

    if ( rc ) break;

    push_buf_event.mask = payload->push_buf_mask;
    push_buf_event.rd_buff_addr_lsw = ( ( uint32_t ) out_buf_virt_addr );
    push_buf_event.rd_buff_addr_msw = ( ( uint32_t ) ( out_buf_virt_addr >> 32 ) );
    push_buf_event.wr_buff_addr_lsw = ( ( uint32_t ) in_buf_virt_addr );
    push_buf_event.wr_buff_addr_msw = ( ( uint32_t ) ( in_buf_virt_addr >> 32 ) );
    push_buf_event.rd_buff_size = payload->out_buf_mem_size;
    push_buf_event.wr_buff_size = payload->in_buf_mem_size;
    push_buf_event.sampling_rate = payload->sampling_rate;
    push_buf_event.wr_num_chan = payload->num_in_channels;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           packet->token, VOICE_EVT_PUSH_HOST_BUF_V2,
           &push_buf_event, sizeof( push_buf_event ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    break;
  }

  if ( is_lock_acquired == TRUE )
  {
    ( void ) apr_lock_leave( session_obj->lock );
  }

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return APR_EOK;
}

/*
  This function access session object in following ways:
  - Read only
    1. vpcm_info
  We do lock here. We also lock in the thread where write access is performed.
*/
static int32_t cvs_vpcm_evt_notify_to_client_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ivpcm_evt_notify_v2_t notify_event;
  voice_evt_notify_host_pcm_buf_v2_t* payload;
  uint64_t out_buf_virt_addr;
  uint64_t out_buf_mem_addr;
  uint64_t in_buf_virt_addr;
  uint64_t in_buf_mem_addr;
  uint32_t handle;
  bool_t is_lock_acquired = TRUE;

  handle = packet->dst_port;
  for ( ;; )
  {
    rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
    if ( rc != APR_EOK || session_obj->vpcm_info.client_addr == APRV2_PKT_INIT_ADDR_V )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_notify_to_client_processing(): Invalid params, packet->dst_port: 0x%4X",
                                              packet->dst_port);
      is_lock_acquired = FALSE;
      break;
    }

    /* Read access to vpcm_info. 
       Lock the whole processing here to make sure buffer is not
       released in the middle. */
    ( void ) apr_lock_enter( session_obj->lock );

    payload = APRV2_PKT_GET_PAYLOAD( voice_evt_notify_host_pcm_buf_v2_t, packet );

    switch( payload->tap_point )
    {
    case VOICESTREAM_MODULE_TX:
      notify_event.tap_point = VSS_IVPCM_TAP_POINT_TX_DEFAULT;
      break;

    case VOICESTREAM_MODULE_RX:
      notify_event.tap_point = VSS_IVPCM_TAP_POINT_RX_DEFAULT;
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_notify_to_client_processing(): Invalid tap point: 0x%8X",
                                              payload->tap_point);
      rc = APR_EBADPARAM;
      break;
    }

    if ( rc ) break;

    notify_event.notify_mask = payload->mask;
    if ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER )
    {
      out_buf_virt_addr = payload->rd_buff_addr_msw;
      out_buf_virt_addr = ( ( out_buf_virt_addr << 32 ) | payload->rd_buff_addr_lsw );

      rc = cvd_mem_mapper_get_mem_addr(
             session_obj->vpcm_info.mem_handle,
             out_buf_virt_addr,
             &out_buf_mem_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_notify_to_client_processing(): Unable to get mem_addr for out_buf_virt_addr: 0x%016X",
                                                out_buf_virt_addr );
        break;
      }

      notify_event.out_buf_mem_address = out_buf_mem_addr;
    }

    if ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER )
    {
      in_buf_virt_addr = payload->wr_buff_addr_msw;
      in_buf_virt_addr = ( ( in_buf_virt_addr << 32 ) | payload->wr_buff_addr_lsw );

      rc = cvd_mem_mapper_get_mem_addr(
             session_obj->vpcm_info.mem_handle,
             in_buf_virt_addr,
             &in_buf_mem_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_evt_notify_to_client_processing(): Unable to get mem_addr for in_buf_virt_addr: 0x%016X",
                                                in_buf_virt_addr );
        break;
      }

      notify_event.in_buf_mem_address = in_buf_mem_addr;
    }

    notify_event.filled_out_size = payload->rd_buff_size;
    notify_event.request_buf_size = payload->wr_buff_size;
    notify_event.sampling_rate = payload->sampling_rate;

    /* For OUTPUT only mode or dual mic OUTPUT-INPUT mode, write buffer size
       and hence request buffer size will be zero and need to be derived from
       read buffer size. */
    if ( notify_event.request_buf_size == 0 )
    {
      /* Derive request buffer size from read buffer size. */
      notify_event.request_buf_size = payload->rd_buff_size;

      /* If the output buffer mask is not set, set the filled out size to zero. */
      if ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER ) == 0 )
      {
        notify_event.filled_out_size = 0;
      }
    }

    /* If the mode is NONE or INPUT only, number of output channels is derived
       from num of write channels from FW. */
    if ( ( notify_event.notify_mask &
           ( VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER | VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER ) ) == 0
         || ( ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER ) != 0 )
         && ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER ) == 0 ) ) )
    {
      /* Number of expected input channels. */
      notify_event.num_out_channels = payload->wr_num_chan;
    }
    else /* If OUTPUT only or OUTPUT-INPUT mode. */
    {
      /* Number of output channels.  */
      notify_event.num_out_channels = payload->rd_num_chan;
    }

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, session_obj->vpcm_info.cvs_handle,
           session_obj->vpcm_info.client_addr, session_obj->vpcm_info.client_handle,
           packet->token, VSS_IVPCM_EVT_NOTIFY_V2,
           &notify_event, sizeof( notify_event ) );
    CVS_COMM_ERROR( rc, session_obj->vpcm_info.client_addr );

    break;
  }

  if ( is_lock_acquired == TRUE )
  {
    ( void ) apr_lock_leave( session_obj->lock );
  }

  ( void ) cvs_access_session_obj_end( handle, FALSE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return APR_EOK;
}

/*
  This function access session object in following ways:
  - Read only
    1. set_dtmf_gen
  We do lock here. We also lock in the thread where write access is performed.
*/
static int32_t cvs_forward_evt_dtmf_generation_ended_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint32_t handle;

  handle = p_packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc == APR_EOK )
  { /* Redirect this event to the client. */
    p_packet->src_addr = cvs_my_addr;
    p_packet->src_port = session_obj->set_dtmf_gen.cvs_port;
    p_packet->dst_addr = session_obj->set_dtmf_gen.client_addr;
    p_packet->dst_port = session_obj->set_dtmf_gen.client_port;
    rc = __aprv2_cmd_forward( cvs_apr_handle, p_packet );
    CVS_COMM_ERROR( rc, session_obj->set_dtmf_gen.client_addr );
  }
  else
  {
    rc = __aprv2_cmd_free( cvs_apr_handle, p_packet );
  }

  ( void ) cvs_access_session_obj_end( handle, FALSE );
  return APR_EOK;
}

/*
  This function access session object in following ways:
  - Read only
    1. set_rx_dtmf_detect
  We do lock here. We also lock in the thread where write access is performed.
*/
static int32_t cvs_forward_evt_rx_dtmf_detected_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint32_t handle;

  handle = p_packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc == APR_EOK )
  { /* Redirect this event to the client. */
    p_packet->src_addr = cvs_my_addr;
    /* Read access to set_rx_dtmf_detect. */
    ( void ) apr_lock_enter( session_obj->lock );
    p_packet->src_port = session_obj->set_rx_dtmf_detect.cvs_port;
    p_packet->dst_addr = session_obj->set_rx_dtmf_detect.client_addr;
    p_packet->dst_port = session_obj->set_rx_dtmf_detect.client_port;
    rc = __aprv2_cmd_forward( cvs_apr_handle, p_packet );
    CVS_COMM_ERROR( rc, session_obj->set_rx_dtmf_detect.client_addr );
    ( void ) apr_lock_leave( session_obj->lock );
  }
  else
  {
    rc = __aprv2_cmd_free( cvs_apr_handle, p_packet );
  }

  ( void ) cvs_access_session_obj_end( handle, FALSE );
  return APR_EOK;
}

static int32_t cvs_set_ui_property_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t* job_obj;
  vss_icommon_cmd_set_ui_property_t* payload;
  uint32_t payload_size;
  voice_set_param_v2_t set_param;
  bool_t is_cached = FALSE;
  uint8_t* cur_ui_prop_cache_slot;
  uint32_t remaining_ui_prop_cache_len;
  uint8_t* destination_ui_prop_cache_slot = NULL;
  uint8_t* remaining_ui_prop;
  uint32_t remaining_ui_prop_len;
  voice_param_data_t* cached_ui_prop;
  uint32_t cached_ui_prop_len;
  uint16_t client_addr;
  uint32_t indirect_handle;

  for ( ;; )
  {
    indirect_handle = packet->dst_port;
    rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              packet->dst_port, rc );
      rc = APR_EHANDLE;
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_ui_property_t, packet );
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    if ( payload_size < sizeof( vss_icommon_cmd_set_ui_property_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Unexpected payload size, %d < %d",
                                              payload_size,
                                              sizeof( vss_icommon_cmd_set_ui_property_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvs_verify_param_data_size( payload->param_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Invalid UI param data size, %d",
                                              payload->param_size );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload->param_size > CVS_MAX_UI_PROP_DATA_LEN )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Invalid param data length %d > Max length %d",
                                              payload->param_size,
                                              CVS_MAX_UI_PROP_DATA_LEN );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload_size != ( payload->param_size + sizeof( voice_param_data_t ) ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Invalid payload size %d != %d",
                                              payload_size,
                                              ( payload->param_size + sizeof( voice_param_data_t ) ) );
      rc = APR_EBADPARAM;
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_set_ui_property_cmd_processing(): Module Id 0x%08X / Param Id 0x%08X received ",
                                           payload->module_id,
                                           payload->param_id );

    cur_ui_prop_cache_slot = session_obj->shared_heap.ui_prop_cache.data;
    remaining_ui_prop_cache_len = sizeof( session_obj->shared_heap.ui_prop_cache.data );
    remaining_ui_prop_len = session_obj->shared_heap.ui_prop_cache.data_len;

    /* Cache UI properties. */
    while ( remaining_ui_prop_cache_len > 0 )
    {
      cached_ui_prop = ( ( voice_param_data_t* ) cur_ui_prop_cache_slot );
      cached_ui_prop_len = ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) );

      if ( ( cached_ui_prop->module_id == payload->module_id ) &&
           ( cached_ui_prop->param_id == payload->param_id ) )
      {
        /* Value already exists. Cache it. */
        if ( cached_ui_prop_len != payload_size )
        { /* New UI property data size is different than the cached UI property data. */
          /* Shuffle the UI property cache to accommodate the new size. */
          remaining_ui_prop_len -= cached_ui_prop_len;
          remaining_ui_prop = ( cur_ui_prop_cache_slot + cached_ui_prop_len );

          while ( remaining_ui_prop_len >= cached_ui_prop_len )
          {
            ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, remaining_ui_prop, cached_ui_prop_len );
            remaining_ui_prop_len -= cached_ui_prop_len;
            remaining_ui_prop += cached_ui_prop_len;
            cur_ui_prop_cache_slot += cached_ui_prop_len;
            remaining_ui_prop_cache_len -= cached_ui_prop_len;
          }

          if ( remaining_ui_prop_len != 0 )
          {
            ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, remaining_ui_prop, remaining_ui_prop_len );
            cur_ui_prop_cache_slot += remaining_ui_prop_len;
            remaining_ui_prop_cache_len -= remaining_ui_prop_len;
          }

          ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, payload, payload_size );

          if ( cached_ui_prop_len < payload_size )
          { /* Increase the cached data length due to increased UI prop data. */
            session_obj->shared_heap.ui_prop_cache.data_len += ( payload_size - cached_ui_prop_len );
          }
          else
          { /* Decrease the cached data length due to decreased UI prop data. */
            session_obj->shared_heap.ui_prop_cache.data_len -= ( cached_ui_prop_len - payload_size );

            /* Set the decreased part of the cache that used to contain data to zero. */
            ( void ) mmstd_memset( ( cur_ui_prop_cache_slot + payload_size ), 0,
                                   ( cached_ui_prop_len - payload_size ) );
          }
        }
        else
        {
          ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, payload, payload_size );
        }

        destination_ui_prop_cache_slot = cur_ui_prop_cache_slot;
        is_cached = TRUE;
        break;
      }

      /* Save the free slot. */
      if ( ( cached_ui_prop->module_id == 0 ) && ( cached_ui_prop->param_id == 0 ) )
      {
        destination_ui_prop_cache_slot = cur_ui_prop_cache_slot;
        break;
      }

      cur_ui_prop_cache_slot += cached_ui_prop_len;
      remaining_ui_prop_cache_len -= cached_ui_prop_len;
      remaining_ui_prop_len -= cached_ui_prop_len;
    }

    /* If not already cached, copy the parameter to the first free slot */
    if( ( is_cached == FALSE ) && ( destination_ui_prop_cache_slot != NULL ) &&
        ( session_obj->shared_heap.ui_prop_cache.num_ui_prop != CVS_MAX_UI_PROP ) )
    {
      ( void ) mmstd_memcpy( destination_ui_prop_cache_slot, remaining_ui_prop_cache_len, payload, payload_size );
      session_obj->shared_heap.ui_prop_cache.data_len += payload_size;
      session_obj->shared_heap.ui_prop_cache.num_ui_prop++;
      is_cached = TRUE;
    }

    if ( is_cached == FALSE )
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Reached Maximum cached entries %d",
                                               CVS_MAX_UI_PROP );
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_ui_property_cmd_processing(): Error caching Module id 0x%08X Param id 0x%08X",
                                               payload->module_id,
                                               payload->param_id );
       rc = APR_ENORESOURCE;
       break;
    }

    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

    /* Send SET_PARAM command to VSM. */
    set_param.payload_address_lsw = ( ( uint32_t ) destination_ui_prop_cache_slot );
    set_param.payload_address_msw = 0;
    set_param.payload_size = payload_size;
    set_param.mem_map_handle = session_obj->shared_heap.vsm_mem_handle;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_set_ui_property_cmd_processing(): payload_address_lsw: 0x%08X, payload_address_msw: 0x%08X",
                                          set_param.payload_address_lsw,
                                          set_param.payload_address_msw );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_set_ui_property_cmd_processing(): payload_size: %d, mem_map_handle: 0x%08X",
                                          set_param.payload_size,
                                          set_param.mem_map_handle );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
           &set_param, sizeof( set_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    break;
  }

  client_addr = packet->src_addr;
  rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

static int32_t cvs_get_ui_property_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_track_job_object_t* track_job_obj;
  vss_icommon_cmd_get_ui_property_t* payload;
  uint32_t payload_size;
  voice_get_param_v2_t get_param;
  bool_t ui_prop_exists = FALSE;
  uint8_t* cur_ui_prop_cache_slot;
  uint32_t remaining_ui_prop_cache_len;
  voice_param_data_t* cached_ui_prop;
  uint32_t get_ui_status;
  aprv2_packet_t* rsp_packet;
  uint8_t* rsp_payload;
  uint32_t rsp_payload_size;
  uint32_t rsp_payload_left;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_ui_property_cmd_processing(): Invalid session handle: 0x%04X, result: 0x%08X",
                                            packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_get_ui_property_t, packet );
  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_size != sizeof( vss_icommon_cmd_get_ui_property_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_ui_property_cmd_processing(): Unexpected payload size, %d != %d",
                                            payload_size,
                                            sizeof( vss_icommon_cmd_get_ui_property_t ) );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  rc = cvs_verify_param_data_size( payload->param_size );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_ui_property_cmd_processing(): Invalid UI param data size, %d",
                                            payload->param_size );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  remaining_ui_prop_cache_len = session_obj->shared_heap.ui_prop_cache.data_len;
  cur_ui_prop_cache_slot = session_obj->shared_heap.ui_prop_cache.data;

  while ( remaining_ui_prop_cache_len != 0 )
  {
    cached_ui_prop = ( ( voice_param_data_t* ) cur_ui_prop_cache_slot );

    if ( ( cached_ui_prop->module_id == payload->module_id ) &&
         ( cached_ui_prop->param_id == payload->param_id ) )
    {
      ui_prop_exists = TRUE;
      get_ui_status = 0;
      break;
    }

    cur_ui_prop_cache_slot += ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) );
    remaining_ui_prop_cache_len -= ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) );
  }

  if ( ui_prop_exists == TRUE )
  {
    rsp_payload_size = ( sizeof( vss_icommon_rsp_get_ui_property_t ) +
                         cached_ui_prop->param_size );

    rc = __aprv2_cmd_alloc_ext(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvs_my_addr, packet->dst_port,
           packet->src_addr, packet->src_port,
           packet->token, VSS_ICOMMON_RSP_GET_UI_PROPERTY,
           rsp_payload_size, &rsp_packet );
    CVS_PANIC_ON_ERROR( rc );

    rsp_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, rsp_packet );
    rsp_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( rsp_packet->header );

    ( void ) mmstd_memcpy( rsp_payload, rsp_payload_left, &get_ui_status, sizeof( get_ui_status ) );
    rsp_payload_left -= sizeof( get_ui_status );
    ( void ) mmstd_memcpy( ( rsp_payload + sizeof( get_ui_status ) ), rsp_payload_left,
                           cached_ui_prop, ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) ) );

    rc = __aprv2_cmd_forward( cvs_apr_handle, rsp_packet );
    CVS_COMM_ERROR( rc, client_addr );
  }
  else
  { /* If the UI property is not cached, get it from VSM. */
    rc = cvs_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVS_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_PARAM ] = cvs_forward_command_get_param_rsp_fn;

    /* Store the original destination and opcode info from the packet */
    track_job_obj->orig_src_service = packet->src_addr;
    track_job_obj->orig_src_port = packet->src_port;
    track_job_obj->orig_dst_port = packet->dst_port;
    track_job_obj->orig_opcode = packet->opcode;
    track_job_obj->orig_token = packet->token;

    get_param.payload_address_lsw = 0;
    get_param.payload_address_msw = 0;
    get_param.module_id = payload->module_id;
    get_param.param_id = payload->param_id;
    get_param.param_max_size = ( ( uint16_t ) ( payload->param_size + sizeof( voice_param_data_t ) ) );
    get_param.reserved = 0;
    get_param.mem_map_handle = 0; /* in-band */

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_get_ui_property_cmd_processing(): Token=%d Param ID=0x%08x",
                                          packet->token, payload->param_id );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    

  return APR_EOK;
}

/*
  This function access session object in following way:
  - Read only access
     1. vsm_stream_handle
     2. active_set.media_id
   Write access of vsm_stream_handle and media_id in other thread 
   will be locked.
*/
static int32_t cvs_stream_set_enc_rate_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  vss_istream_cmd_voc_amr_set_enc_rate_t* in_args;
  cvs_simple_job_object_t* job_obj;
  vsm_enc_set_rate_t vsm_enc_rate_param;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, &indirect_obj );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_enc_rate_cmd_processing(): "
           "Invalid session handle: 0x%04X, result: 0x%08X", packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  if ( ( sizeof( vss_istream_cmd_voc_amrwb_set_enc_rate_t ) ==
         sizeof( vss_istream_cmd_voc_qcelp13k_set_rate_t ) ) &&
       ( sizeof( vss_istream_cmd_voc_qcelp13k_set_rate_t ) ==
         sizeof( vss_istream_cmd_voc_amr_set_enc_rate_t ) ) &&
       ( sizeof( vss_istream_cmd_voc_amr_set_enc_rate_t ) ==
         sizeof( vss_istream_cmd_voc_4gvnb_set_rate_t ) ) &&
       ( sizeof( vss_istream_cmd_voc_4gvnb_set_rate_t ) ==
         sizeof( vss_istream_cmd_voc_4gvwb_set_rate_t ) ) &&
       ( sizeof( vss_istream_cmd_voc_4gvwb_set_rate_t ) ==
         sizeof( vss_istream_cmd_voc_4gvnw_set_rate_t ) ) &&
       ( sizeof( vss_istream_cmd_voc_4gvnw_set_rate_t ) ==
         sizeof( vss_istream_cmd_voc_4gvnw2k_set_rate_t ) ) )
  {
    //Good to go
  }
  else
  {
    /* All media types must have teh same structure if this is not true anymore then this function needs to accomodate */
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  if ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                      indirect_obj->access_bits ) == FALSE )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_enc_rate_cmd_processing(): "
           "Client is passive, dst_port: 0x%04X.", packet->dst_port );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_voc_amr_set_enc_rate_t,
                                   packet );

  /* Cache voc property. */
  cvs_cache_voc_property( session_obj, ( void* )in_args, CVS_VOC_PROPERTY_ENUM_ENC_RATE );

  rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

  /* Read access to active_set.media and vsm_stream_handle. */
  ( void ) apr_lock_enter( session_obj->lock );

  vsm_enc_rate_param.media_type = session_obj->active_set.media_id;
  vsm_enc_rate_param.encoder_rate = in_args->mode;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_enc_rate_cmd_processing(): "
         "media_type: 0x%08X, encoder_rate: %d",
         vsm_enc_rate_param.media_type,
         vsm_enc_rate_param.encoder_rate );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_RATE,
         &vsm_enc_rate_param, sizeof( vsm_enc_set_rate_t ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  ( void ) apr_lock_leave( session_obj->lock );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

/*
  This function access session object in following way:
  - Read only access
     1. vsm_stream_handle
     2. active_set.media_id
   Write access of vsm_stream_handle and media_id in other thread 
   will be locked.
*/
static int32_t cvs_stream_cdma_set_enc_minmax_rate_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint32_t payload_size;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  cvs_simple_job_object_t* job_obj;
  vss_istream_cmd_cdma_set_enc_minmax_rate_t* in_args;
  vsm_enc_set_minmax_rate_t set_minmax_rate;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, &indirect_obj );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_cdma_set_enc_minmax_rate_cmd_processing(): "
           "Invalid session handle: 0x%04X, result: 0x%08X", packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_size != sizeof( vss_istream_cmd_cdma_set_enc_minmax_rate_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_cdma_set_enc_minmax_rate_cmd_processing(): "
           "Unexpected payload size, %d != %d",
           payload_size,
           sizeof( vss_istream_cmd_cdma_set_enc_minmax_rate_t ) );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  if ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                      indirect_obj->access_bits ) == FALSE )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_cdma_set_enc_minmax_rate_cmd_processing(): "
           "Client is passive, dst_port: 0x%04X.", packet->dst_port );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_cdma_set_enc_minmax_rate_t,
                                   packet );

  /* Cache voc property. */
  cvs_cache_voc_property( session_obj,
    ( void* )in_args, CVS_VOC_PROPERTY_ENUM_MINMAX_RATE );

  rc = cvs_create_simple_job_object(
         session_obj->header.handle,
         ( ( cvs_simple_job_object_t** ) &job_obj ) );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

  /* Read access to active_set.media and vsm_stream_handle. */
  ( void ) apr_lock_enter( session_obj->lock );

  set_minmax_rate.media_type = session_obj->active_set.media_id;
  set_minmax_rate.min_rate = in_args->min_rate;
  set_minmax_rate.max_rate = in_args->max_rate;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_cdma_set_enc_minmax_rate_cmd_processing(): "
         "media_type: 0x%08X, min rate: %d, max rate: %d",
         set_minmax_rate.media_type,
         set_minmax_rate.min_rate,
         set_minmax_rate.max_rate );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_ENC_SET_MINMAX_RATE,
         &set_minmax_rate, sizeof( set_minmax_rate ) );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  ( void ) apr_lock_leave( session_obj->lock );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

static int32_t cvs_set_tty_mode_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_itty_cmd_set_tty_mode_t *payload;
  uint32_t payload_len;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;
  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_itty_cmd_set_tty_mode(): Invalid session handle: 0x%04X, " \
                                            "result: 0x%08X",
                                            packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );

    ( void )cvs_access_session_obj_end( indirect_handle, TRUE );
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vss_itty_cmd_set_tty_mode_t, packet );
  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_len != sizeof( vss_itty_cmd_set_tty_mode_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_tty_mode_cmd_processing(): Invalid data. Payload len: %d, expected len: %d",
                                            payload_len,
                                            sizeof( vss_itty_cmd_set_tty_mode_t ) );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EFAILED );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );
    return APR_EOK;
  }

  /* Caching the TTY mode from MVM. */
  session_obj->tty_info.tty_mode = payload->mode;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );
  return APR_EOK;
}

/*
  This function access session object in following way:
  - Read only access
     1. vsm_stream_handle
   Write access of vsm_stream_handle in other thread will be locked.
*/
static int32_t cvs_delegate_to_vsm_stream_cmd_processing (
  aprv2_packet_t* packet,
  uint32_t forward_opcode
)
{
  int32_t rc;
  cvs_track_job_object_t* track_job_obj;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  bool_t is_full_control;
  uint16_t client_addr;
  uint32_t indirect_handle;
  vss_istream_cmd_set_enc_dtx_mode_t* dtx_mode;
  vss_istream_cmd_cdma_set_enc_rate_modulation_t* rate_modulation;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, &indirect_obj );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_delegate_to_vsm_stream_cmd_processing(): Invalid session handle: 0x%04X, result: 0x%08X",
                                            packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  is_full_control = APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                                   indirect_obj->access_bits );
  if ( is_full_control == FALSE )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_delegate_to_vsm_stream_cmd_processing(): Client is pasive, dst_port: 0x%04X.",
                                            packet->dst_port );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  /* Cache voc property. */
  switch ( packet->opcode )
  {
    case VSS_ISTREAM_CMD_SET_ENC_DTX_MODE:
    {
      dtx_mode = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_enc_dtx_mode_t, packet );
      cvs_cache_voc_property( session_obj,
        ( void* )dtx_mode, CVS_VOC_PROPERTY_ENUM_DTX );
    }
    break;

    case VSS_ISTREAM_CMD_CDMA_SET_ENC_RATE_MODULATION:
    {
      rate_modulation = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_cdma_set_enc_rate_modulation_t, packet );
      cvs_cache_voc_property( session_obj,
        ( void* )rate_modulation, CVS_VOC_PROPERTY_ENUM_RATE_MODULATION );
    }
    break;
  }

  rc = cvs_create_track_job_object( session_obj->header.handle, &track_job_obj );
  CVS_PANIC_ON_ERROR( rc );
  track_job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_forward_command_result_rsp_fn;

  /* Store the original destination and opcode info from the packet */
  track_job_obj->orig_src_service = packet->src_addr;
  track_job_obj->orig_src_port = packet->src_port;
  track_job_obj->orig_dst_port = packet->dst_port;
  track_job_obj->orig_opcode = packet->opcode;
  track_job_obj->orig_token = packet->token;

  /* This should copy the original packet's content and create new one */
  /* Read access to vsm_stream_handle. */
  ( void ) apr_lock_enter( session_obj->lock );
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvs_vsm_addr, session_obj->vsm_stream_handle,
         track_job_obj->header.handle,
         forward_opcode,
         APRV2_PKT_GET_PAYLOAD( void, packet ),
         APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  ( void ) apr_lock_leave( session_obj->lock );
  CVS_COMM_ERROR( rc, cvs_vsm_addr );

  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

/* 
  This function access session object in following way:
  - Read and write access
     1. set_dtmf_gen
  - Read only access
     1. vsm_stream_handle
   Every access instance of set_dtmf_gen in other thread will be locked.
   Write access of vsm_stream_handle in other thread will be locked.
*/
static int32_t cvs_set_dtmf_generation_cmd_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  vss_istream_cmd_set_dtmf_generation_t *p_in_args;
  uint32_t in_size;
  cvs_session_object_t *p_session;
  cvs_simple_job_object_t *job_obj;
  vsm_set_dtmf_generation_t vsm_set_dtmf_gen;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = p_packet->src_addr;

  indirect_handle = p_packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &p_session, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_dtmf_generation_cmd_processing(): Invalid session handle: 0x%04X, result: 0x%08X",
                                            p_packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  p_in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_dtmf_generation_t, p_packet );
  in_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet->header );

  if ( in_size != sizeof( vss_istream_cmd_set_dtmf_generation_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_dtmf_generation_cmd_processing(): Invalid data. Payload len: %d, expected len: %d",
                                            in_size,
                                            sizeof( vss_istream_cmd_set_dtmf_generation_t ) );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EFAILED );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  /* Write access to set_dtmf_gen. */
  ( void ) apr_lock_enter( p_session->lock );
  p_session->set_dtmf_gen.client_addr = p_packet->src_addr;
  p_session->set_dtmf_gen.client_port = p_packet->src_port;
  p_session->set_dtmf_gen.cvs_port = p_packet->dst_port;
  ( void ) apr_lock_leave( p_session->lock );

  rc = cvs_create_simple_job_object( p_session->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

  vsm_set_dtmf_gen.direction = p_in_args->direction;
  vsm_set_dtmf_gen.mixing = p_in_args->mix_flag;
  vsm_set_dtmf_gen.tone1 = p_in_args->tone_1;
  vsm_set_dtmf_gen.tone2 = p_in_args->tone_2;
  vsm_set_dtmf_gen.gain = p_in_args->gain;
  vsm_set_dtmf_gen.duration = p_in_args->duration;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_set_dtmf_generation_cmd_processing(): direction: %d, mixing: %d, tone1: %d",
                                        vsm_set_dtmf_gen.direction,
                                        vsm_set_dtmf_gen.mixing,
                                        vsm_set_dtmf_gen.tone1 );
  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_set_dtmf_generation_cmd_processing(): tone2: %d, gain: %d, duration: %d",
                                        vsm_set_dtmf_gen.tone2,
                                        vsm_set_dtmf_gen.gain,
                                        vsm_set_dtmf_gen.duration );

  /* Read access to vsm_stream_handle. */
  ( void ) apr_lock_enter( p_session->lock );
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( uint16_t ) p_session->header.handle,
         p_session->vsm_stream_addr, p_session->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_DTMF_GENERATION,
         &vsm_set_dtmf_gen, sizeof( vsm_set_dtmf_gen ) );
  ( void ) apr_lock_leave( p_session->lock );
  CVS_COMM_ERROR( rc, p_session->vsm_stream_addr );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
  return APR_EOK;
}

/* 
  This function access session object in following way:
  - Read and write access
     1. set_rx_dtmf_detect
  - Read only access
     1. vsm_stream_handle
   Every access instance of set_rx_dtmf_detect in other thread will be locked.
   Write access of vsm_stream_handle in other thread will be locked.
*/
static int32_t cvs_set_rx_dtmf_detection_cmd_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  vss_istream_cmd_set_rx_dtmf_detection_t *p_in_args;
  uint32_t in_size;
  cvs_session_object_t *p_session;
  cvs_simple_job_object_t *job_obj;
  vsm_set_rx_dtmf_detection_t vsm_rx_dtmf_detect;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = p_packet->src_addr;

  indirect_handle = p_packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &p_session, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_rx_dtmf_detection_cmd_processing(): Invalid session handle: 0x%04X, result: 0x%08X",
                                            p_packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  p_in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_rx_dtmf_detection_t, p_packet );
  in_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet->header );

  if ( in_size != sizeof( vss_istream_cmd_set_rx_dtmf_detection_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,"cvs_set_rx_dtmf_detection_cmd_processing(): Invalid data. Payload len: %d, expected len: %d",
                                             in_size,
                                             sizeof( vss_istream_cmd_set_rx_dtmf_detection_t ) );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EFAILED );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  /* Read/write access to set_rx_dtmf_detect. 
     Read access to vsm_stream_handle. */
  ( void ) apr_lock_enter( p_session->lock );

  /* If the current rx dtmf detect status is same as the request,
     return already done. */
  if ( p_session->set_rx_dtmf_detect.enable_flag == p_in_args->enable )
  {
    rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EALREADY );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) apr_lock_leave( p_session->lock );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  /* Store the enable status. */
  p_session->set_rx_dtmf_detect.enable_flag = p_in_args->enable;

  /* If the request is to enable, save the client addr and port. */
  if ( p_session->set_rx_dtmf_detect.enable_flag )
  {
    p_session->set_rx_dtmf_detect.client_addr = p_packet->src_addr;
    p_session->set_rx_dtmf_detect.client_port = p_packet->src_port;
    p_session->set_rx_dtmf_detect.cvs_port = p_packet->dst_port;
  }
  else
  {
    /* If the request is to disable, check whether the client
       is same as the one that issued enable. */
    if ( p_session->set_rx_dtmf_detect.client_addr != p_packet->src_addr ||
         p_session->set_rx_dtmf_detect.client_port != p_packet->src_port )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      ( void ) apr_lock_leave( p_session->lock );
      ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
      return APR_EOK;
    }
  }

  rc = cvs_create_simple_job_object( p_session->header.handle, &job_obj );
  CVS_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

  vsm_rx_dtmf_detect.enable = p_in_args->enable;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_set_rx_dtmf_detection_cmd_processing(): enable: %d",
                                        vsm_rx_dtmf_detect.enable );

  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         cvs_my_addr, ( uint16_t ) p_session->header.handle,
         p_session->vsm_stream_addr, p_session->vsm_stream_handle,
         job_obj->header.handle, VSM_CMD_SET_RX_DTMF_DETECTION,
         &vsm_rx_dtmf_detect, sizeof( vsm_rx_dtmf_detect ) );
  CVS_COMM_ERROR( rc, p_session->vsm_stream_addr );

  ( void ) apr_lock_leave( p_session->lock );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
  return APR_EOK;
}

/* This function access session object in following way:
   - Read and write access
     1. target_set.mute
     2. active_set.mute
   - Read only access
     1. session_ctrl.state
     2. vsm_stream_handle
   Every access instance of "mute" in other thread will be locked.
*/
static int32_t cvs_volume_mute_v2_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ivolume_cmd_mute_v2_t* payload;
  voice_set_soft_mute_v2_t set_soft_mute;
  cvs_session_object_t* session_obj;
  cvs_simple_job_object_t *job_obj;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;

  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_volume_mute_v2_cmd_processing(): " \
           "Invalid session handle: 0x%04X, result: 0x%08X", packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vss_ivolume_cmd_mute_v2_t, packet );

  /* Validate payload content. */
  if ( ( ( payload->direction != VSS_IVOLUME_DIRECTION_TX ) &&
         ( payload->direction != VSS_IVOLUME_DIRECTION_RX ) ) ||
       ( ( payload->mute_flag != VSS_IVOLUME_MUTE_OFF ) &&
         ( payload->mute_flag != VSS_IVOLUME_MUTE_ON ) ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_volume_mute_v2_cmd_processing(): " \
           "Unexpected params, direction: %d, mute_flag: %d",
           payload->direction, payload->mute_flag );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EBADPARAM );
    CVS_COMM_ERROR( rc, client_addr );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );   
    return APR_EBADPARAM;
  }

  set_soft_mute.direction = payload->direction;
  set_soft_mute.mute = payload->mute_flag;
  set_soft_mute.ramp_duration = payload->ramp_duration_ms;

  /* Read/write access to target_set.mute.
     read access to vsm_stream_handle/session_ctrl.state. */
  ( void ) apr_lock_enter( session_obj->lock );

  switch ( payload->direction )
  {
    case CVS_DIRECTION_TX:
      session_obj->target_set.mute.tx_mute_flag = payload->mute_flag ;
      session_obj->target_set.mute.tx_ramp_duration = payload->ramp_duration_ms;
      break;

    case CVS_DIRECTION_RX:
      session_obj->target_set.mute.rx_mute_flag = payload->mute_flag ;
      session_obj->target_set.mute.rx_ramp_duration = payload->ramp_duration_ms;
      break;
	  
    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
  }
	  
  /* If in RUN state, set mute on the VDSP.
   * If in IDLE state, leave settings in the target_set.
   * They will be picked up when we go to RUN. 
   */
  if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
  {
    rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVS_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
           job_obj->header.handle, VOICE_CMD_SET_SOFT_MUTE_V2,
           &set_soft_mute, sizeof( set_soft_mute ) );
    CVS_PANIC_ON_ERROR( rc );

    session_obj->active_set.mute.tx_mute_flag = session_obj->target_set.mute.tx_mute_flag ;
    session_obj->active_set.mute.rx_mute_flag = session_obj->target_set.mute.rx_mute_flag ;
    session_obj->active_set.mute.tx_ramp_duration = session_obj->target_set.mute.tx_ramp_duration;
    session_obj->active_set.mute.rx_ramp_duration = session_obj->target_set.mute.rx_ramp_duration;
  }

  ( void ) apr_lock_leave( session_obj->lock );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE ); 

  return APR_EOK;
}

/*
  This function accesses session object in follow ways:
  - Ready only
    1. tty_info
  They are lock protected here.The write access in other thread will
  need to lock protected.
*/
static int32_t cvs_forward_ittyoob_evt_tx_char_to_client(
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vsm_evt_outofband_tty_tx_char_detected_t* payload;
  vss_ittyoob_evt_tx_char_t tx_tty_char;
  uint32_t handle;

  handle = packet->dst_port;
  rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
  if ( rc != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_forward_ittyoob_evt_tx_char_to_client(): Invalid handle, packet->dst_port: 0x%4X",
                                            packet->dst_port );
    rc = __aprv2_cmd_free( cvs_apr_handle, packet );

    ( void ) cvs_access_session_obj_end( handle, FALSE );
    return APR_EOK;
  }

  /* Read access to tty_info. */
  ( void ) apr_lock_enter( session_obj->lock );

  /* Take care of scenario where TX event is generated after deregistration. */
  if( session_obj->tty_info.is_ittyoob_registered == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_forward_ittyoob_evt_tx_char_to_client(): Dropping " \
                                          "EVENT_TTY_TX_CHAR" );
    ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
    ( void ) apr_lock_leave( session_obj->lock );
    ( void ) cvs_access_session_obj_end( handle, FALSE );
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vsm_evt_outofband_tty_tx_char_detected_t, packet );

  tx_tty_char.tty_char = payload->tty_char;
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_forward_ittyoob_evt_tx_char_to_client(): TTY TX Char : %c",
                                        payload->tty_char );

  /* Send VSS_ITTYOOB_EVT_TX_CHAR to client. */
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->tty_info.ittyoob_client_addr, session_obj->tty_info.ittyoob_client_port,
         APR_NULL_V, VSS_ITTYOOB_EVT_SEND_TX_CHAR,
         &tx_tty_char, sizeof( tx_tty_char ) );
  CVS_COMM_ERROR( rc, session_obj->tty_info.ittyoob_client_addr );

  ( void ) apr_lock_leave( session_obj->lock );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
  ( void ) cvs_access_session_obj_end( handle, FALSE );

  return APR_EOK;
}

/*
  This function accesses session object in follow ways:
  - Ready only
    1. tty_info
    2. vsm_stream_addr
    3. vsm_stream_handle
    4. session_ctrl.state
  They are lock protected here.The write access in other thread will
  need to lock protected.
*/
static int32_t cvs_forward_ittyoob_cmd_rx_char_to_vsm(
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ittyoob_cmd_rx_char_t* payload;
  vsm_evt_outofband_tty_rx_char_push_t tty_rx_char;
  cvs_track_job_object_t* track_job_obj;
  uint16_t client_addr;
  uint32_t indirect_handle;

  client_addr = packet->src_addr;

  indirect_handle = packet->dst_port;
  rc = cvs_access_session_obj_start( indirect_handle, TRUE, &session_obj, NULL );
  if ( rc != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_forward_ittyoob_cmd_rx_char_to_vsm(): Invalid handle, packet->dst_port: 0x%4X",
                                            packet->dst_port );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EHANDLE );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  /* Read access to tty_info/vsm_stream_addr/vsm_stream_handle. */
  ( void ) apr_lock_enter( session_obj->lock );

  if ( session_obj->tty_info.is_ittyoob_registered == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_forward_ittyoob_cmd_rx_char_to_vsm(): Registration Not Done " \
                                          "For IOOBTTY SERVICE" );

   /* Send #APR_ENOSERVICE as command Response. */
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_EFAILED );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) apr_lock_leave( session_obj->lock );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );    
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vss_ittyoob_cmd_rx_char_t, packet);
  tty_rx_char.tty_char = payload->tty_char;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_forward_ittyoob_cmd_rx_char_to_vsm(): OOBTTY RX Char: %c",
                                        payload->tty_char );

  /* Need to check whether stream has sent VSS_ISTREAM_EVT_READY to its
     client, other respond with #APR_ENOTREADY to client. */

  if( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
  {
    rc = cvs_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVS_PANIC_ON_ERROR( rc );

    track_job_obj->orig_src_service = packet->src_addr;
    track_job_obj->orig_src_port = packet->src_port;
    track_job_obj->orig_dst_port = packet->dst_port;
    track_job_obj->orig_token = packet->token;
    track_job_obj->orig_opcode = VSS_ITTYOOB_CMD_SEND_RX_CHAR;

   /* Send event VSM_EVT_OUTOFBAND_TTY_RX_CHAR_PUSH to VSM. */
    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
           track_job_obj->header.handle, VSM_EVT_OUTOFBAND_TTY_RX_CHAR_PUSH,
           &tty_rx_char, sizeof( tty_rx_char ) );
    CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_forward_ittyoob_cmd_rx_char_to_vsm(): CVS is not in " \
                                          "RUN state " );

    /* Send #APR_ENOTREADY as command response to client. */
    rc = __aprv2_cmd_end_command( cvs_apr_handle, packet, APR_ENOTREADY );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) apr_lock_leave( session_obj->lock );
    ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );  
      
    return APR_EOK;
  }

  ( void ) apr_lock_leave( session_obj->lock );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );
  ( void ) cvs_access_session_obj_end( indirect_handle, TRUE );

  return APR_EOK;
}

/* This function does not touch session object.
   There is no need of lock here. 
*/
static int32_t cvs_forward_ittyoob_rx_char_done_cmd_response_to_client(
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_track_job_object_t* forwarding_job_obj;
  aprv2_ibasic_rsp_result_t basic_rsp;

  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                               ( ( cvs_object_t** ) &forwarding_job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* Send #APR_EOK command response to client.
     this should copy the original packet's content and create new one. */
  basic_rsp.opcode = forwarding_job_obj->orig_opcode;
  basic_rsp.status = APR_EOK;
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvs_my_addr, forwarding_job_obj->orig_dst_port,
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         APRV2_IBASIC_RSP_RESULT,
         &basic_rsp, sizeof( basic_rsp ) );
  CVS_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_forward_ittyoob_rx_char_done_cmd_response_to_client(): " \
                                      "VSM_EVT_OUTOFBAND_TTY_RX_CHAR_ACCEPTED recieved" );

  /* Free the cvs_track_object. */
  ( void ) cvs_free_object( ( cvs_object_t* ) forwarding_job_obj );

  /* Free the incoming packet. */
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return APR_EOK;
}

/* This function does not touch session object.
   There is no need of lock here. 
*/
static int32_t cvs_forward_ittyoob_evt_error_cmd_response_to_client(
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_track_job_object_t* forwarding_job_obj;
  aprv2_ibasic_rsp_result_t basic_rsp;

  /* Get the track_object created for house keeping. */
  rc = cvs_get_typed_object( packet->token, CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                               ( ( cvs_object_t** ) &forwarding_job_obj ) );
  CVS_PANIC_ON_ERROR( rc );

  /* Send #APR_EFAILED command response to client.
    This should copy the original packet's content and create new one. */
  basic_rsp.opcode = forwarding_job_obj->orig_opcode;
  basic_rsp.status = APR_EFAILED;
  rc = __aprv2_cmd_alloc_send(
         cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvs_my_addr, forwarding_job_obj->orig_dst_port,
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         APRV2_IBASIC_RSP_RESULT,
         &basic_rsp, sizeof( basic_rsp ) );
  CVS_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_forward_ittyoob_evt_error_cmd_response_to_client(): " \
                                      "VSM_EVT_OUTOFBAND_TTY_RX_CHAR_ACCEPTED recieved" );

  /* Free the cvs_track_object. */
  ( void ) cvs_free_object( ( cvs_object_t* ) forwarding_job_obj );

  /* Free the incoming packet. */
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return APR_EOK;
}

/* 
  This function accesses session object in following ways:
  - Read and write access
    1. voc_operating_mode_info
    2. eamr_mode_change_notification_info
  - Read only access
    1. active_set.direction
    2. attached_mvm_handle
    4. active_set.media_id
  For the sake of simplicity, we lock the function from begining
  to end (Assumption: this is event handling, which should be fast).
  For read and write accessed items, we lock every access instance in 
  other thread.
  For read only accessed items, we only lock write access instance in 
  other thread.
*/
static int32_t cvs_voc_operating_mode_update_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  bool_t is_update_mvm_required = FALSE;
  cvs_session_object_t* session_obj;
  vsm_evt_voc_operating_mode_update_t* payload;
  vss_istream_evt_voc_operating_mode_update_t voc_op_mode_update;
  vss_istream_evt_eamr_mode_changed_t eamr_mode_changed;
  vss_istream_evt_evs_rx_bandwidth_changed_t evs_rx_bandwidth_changed;
  bool_t is_notify_eamr_mode_changed = FALSE;
  bool_t is_notify_evs_bandwidth_changed = FALSE;
  uint32_t payload_len;
  uint32_t handle;

  handle = packet->dst_port;
  for ( ;; )
  {
    rc = cvs_access_session_obj_start( handle, FALSE, &session_obj, NULL );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_voc_operating_mode_update_evt_processing(): " \
                                              "Invalid session handle: 0x%04X, result: 0x%08X.",
                                              packet->dst_port,
                                              rc );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vsm_evt_voc_operating_mode_update_t, packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    if ( payload_len != sizeof( vsm_evt_voc_operating_mode_update_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_voc_operating_mode_update_evt_processing(): " \
                                              "Invalid payload len: %d, expected len: %d, Drop evt",
                                              payload_len,
                                              sizeof( vsm_evt_voc_operating_mode_update_t ) );
      break;
    }

    if ( cvs_voc_op_mode_is_valid( payload->mode ) == FALSE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_voc_operating_mode_update_evt_processing(): " \
                                              "Invalid vocoder operating mode 0x%08X.",
                                              payload->mode );
      break;
    }

    if ( ( payload->direction != VSM_VOC_OPERATING_MODE_DIRECTION_TX ) &&
         ( payload->direction != VSM_VOC_OPERATING_MODE_DIRECTION_RX ) )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_voc_operating_mode_update_evt_processing(): " \
                                              "Invalid direction 0x%04X .", payload->direction );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_voc_operating_mode_update_evt_processing(): " \
           "Event: direction = (%d), mode = (0x%08x)",
           payload->direction, payload->mode );

    /* Read/write access to voc_operating_mode_info.
       Read access to active_set. */
    ( void ) apr_lock_enter( session_obj->lock );

    switch ( session_obj->active_set.direction )
    {
    case CVS_DIRECTION_RX:
      {
        if ( payload->direction == VSM_VOC_OPERATING_MODE_DIRECTION_RX )
        {
          session_obj->voc_operating_mode_info.is_rx_mode_received = TRUE;
          session_obj->voc_operating_mode_info.rx_mode = payload->mode;
          is_update_mvm_required = TRUE;
        }
      }
      break;

    case CVS_DIRECTION_TX:
      {
        if ( payload->direction == VSM_VOC_OPERATING_MODE_DIRECTION_TX )
        {
          session_obj->voc_operating_mode_info.is_tx_mode_received = TRUE;
          session_obj->voc_operating_mode_info.tx_mode = payload->mode;
          is_update_mvm_required = TRUE;
        }
      }
      break;

    case CVS_DIRECTION_RX_TX:
      {
        if ( payload->direction == VSM_VOC_OPERATING_MODE_DIRECTION_RX )
        {
          session_obj->voc_operating_mode_info.is_rx_mode_received = TRUE;
          session_obj->voc_operating_mode_info.rx_mode = payload->mode;
        }

        if ( payload->direction == VSM_VOC_OPERATING_MODE_DIRECTION_TX )
        {
          session_obj->voc_operating_mode_info.is_tx_mode_received = TRUE;
          session_obj->voc_operating_mode_info.tx_mode = payload->mode;
        }

        if ( ( session_obj->voc_operating_mode_info.is_rx_mode_received == TRUE ) &&
             ( session_obj->voc_operating_mode_info.is_tx_mode_received == TRUE ) )
        {
          is_update_mvm_required = TRUE;
        }
      }
      break;

    default:
      {
        rc = APR_EUNEXPECTED;
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      }
      break;
    }

    if ( rc ) break;

    if ( ( is_update_mvm_required == TRUE ) &&
         ( session_obj->attached_mvm_handle != APR_NULL_V ) )
    { /* Send VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event to MVM. */
      voc_op_mode_update.rx_mode = session_obj->voc_operating_mode_info.rx_mode;
      voc_op_mode_update.tx_mode = session_obj->voc_operating_mode_info.tx_mode;

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_mvm_addr, session_obj->attached_mvm_handle,
             0, VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE,
             &voc_op_mode_update, sizeof( voc_op_mode_update ) );
      CVS_COMM_ERROR( rc, cvs_mvm_addr );
    }

    /* Send eAMR mode change event to registered client if the mode is changed.
     * Refer to the comments under eamr_mode_change_notification_info in
     * cvs_session_object_t for details.
     */
    if ( ( session_obj->eamr_mode_change_notification_info.is_enabled == TRUE ) &&
         ( session_obj->active_set.media_id == VSS_MEDIA_ID_EAMR ) )
    {
      switch ( session_obj->voc_operating_mode_info.rx_mode )
      {
      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NB:
        {
          if ( session_obj->eamr_mode_change_notification_info.mode ==
               VSS_ISTREAM_EAMR_MODE_WIDEBAND )
          {
            is_notify_eamr_mode_changed = TRUE;

            session_obj->eamr_mode_change_notification_info.mode = 
              VSS_ISTREAM_EAMR_MODE_NARROWBAND;
          }
        }
        break;

      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB:
        {
          if ( session_obj->eamr_mode_change_notification_info.mode ==
               VSS_ISTREAM_EAMR_MODE_NARROWBAND )
          {
            is_notify_eamr_mode_changed = TRUE;

            session_obj->eamr_mode_change_notification_info.mode = 
              VSS_ISTREAM_EAMR_MODE_WIDEBAND;
          }
        }
        break;

      default:
        break;
      }

      if ( is_notify_eamr_mode_changed == TRUE )
      {
        eamr_mode_changed.mode = session_obj->eamr_mode_change_notification_info.mode;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->eamr_mode_change_notification_info.client_addr,
               session_obj->eamr_mode_change_notification_info.client_port,
               0, VSS_ISTREAM_EVT_EAMR_MODE_CHANGED,
               &eamr_mode_changed, sizeof( eamr_mode_changed ) );
        CVS_COMM_ERROR(
          rc, session_obj->eamr_mode_change_notification_info.client_addr );
      }
    }
    
    /* Send EVS bandwidth change event to registered client if the mode is changed.
     * Refer to the comments under evs_bandwidth_change_notification_info in
     * cvs_session_object_t for details.
     */
    if ( session_obj->active_set.media_id == VSS_MEDIA_ID_EVS )
    {
      switch ( session_obj->voc_operating_mode_info.rx_mode )
      {
      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NB:
        {
          if ( session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth !=
               VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB )
          {
            is_notify_evs_bandwidth_changed = TRUE;

            session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = 
              VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB;
          }
        }
        break;

      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB:
        {
          if ( session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth !=
               VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB )
          {
            is_notify_evs_bandwidth_changed = TRUE;

            session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = 
              VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB;
          }
        }
        break;
      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_SWB:
        {
          if ( session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth !=
               VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB )
          {
            is_notify_evs_bandwidth_changed = TRUE;

            session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = 
              VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB;
          }
        }
        break;

      case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_FB:
        {
          if ( session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth !=
               VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB )
          {
            is_notify_evs_bandwidth_changed = TRUE;

            session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth = 
              VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB;
          }
        }
        break;
        
      default:
        break;
      }

      /* If CVS is not in RUN state, notification will be sent during IDLE->RUN transition via 
       * cvs_action_send_evs_bandwidth_notification(). Refer to the comments under
       * evs_bandwidth_change_notification_info in cvs_session_object_t for details. */
      if ( ( session_obj->evs_bandwidth_change_notification_info.is_enabled == TRUE ) &&
           ( is_notify_evs_bandwidth_changed == TRUE ) &&
           ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN ) )
      {
        evs_rx_bandwidth_changed.bandwidth = session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->evs_bandwidth_change_notification_info.client_addr,
               session_obj->evs_bandwidth_change_notification_info.client_port,
               0, VSS_ISTREAM_EVT_EVS_RX_BANDWIDTH_CHANGED,
               &evs_rx_bandwidth_changed, sizeof( evs_rx_bandwidth_changed ) );
        CVS_COMM_ERROR(
          rc, session_obj->evs_bandwidth_change_notification_info.client_addr );
        
        /* Updated last sent Rx Bandwidth */
        session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = 
          session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;
      }
    }
    break;
  }

  cvs_signal_run( CVS_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  ( void ) apr_lock_leave( session_obj->lock );
  ( void ) cvs_access_session_obj_end( handle, FALSE );
  ( void ) __aprv2_cmd_free( cvs_apr_handle, packet );

  return APR_EOK;
}

static int32_t cvs_eval_voice_state_decision (
  uint16_t cvp_ready_cnt,
  bool_t is_enable_received_from_mvm,
  cvs_goal_enum_t* ret_next_state
)
{
  cvs_goal_enum_t next_state;

  if ( ret_next_state == NULL )
  {
    return APR_EBADPARAM;
  }

  /*
    Problem:
      There are 2 clients, MVM and CVP which could cause CVS state
      transition.
      1. MVM sending VSS_ISTREAM_CMD_ENABLE, and
      2. CVP sending VSS_IVOCPROC_EVT_READY/VSS_IVOCPROC_EVT_NOT_READY
      are two different requests.

    Strategy:
      The evaluation of cvp_ready_cnt and is_enable_received_from_mvm
      be evaluated into integers which recommends the goal to perform on the CVS state
      machine.

      Decision matrix:

              \ CVP_ready_cnt
         MVM     \      0        |  1
      ---------------------------------------
      disable      |  disable  |  disable
      ---------|-----------------------------
      enable       |  disable  |  enable
  */

  if ( ( cvp_ready_cnt == 0 ) ||
       ( is_enable_received_from_mvm == FALSE ) )
  {
    next_state = CVS_GOAL_ENUM_DISABLE;
  }
  else
  {
    next_state = CVS_GOAL_ENUM_ENABLE;
  }

  *ret_next_state = next_state;

  return APR_EOK;
}


static int32_t cvs_stream_vocproc_evt_not_ready_processing (
   cvs_pending_control_t* ctrl
)
{
  /* This functio will process VSS_IVOCPROC_EVT_NOT_READY received from CVP.
     If CVP has sent vocproc not ready, check if stream can be stopped */
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t cnt = 0;
  uint16_t *p_cvp_array = NULL;
  bool_t *p_isready = NULL;
  cvs_goal_enum_t next_goal;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {

    p_cvp_array = (uint16_t *)&session_obj->active_set.attach_table.cvp_handles;
    p_isready   = (bool_t *)&session_obj->active_set.attach_table.cvp_isready;

    /* Check if this CVP is attached, if yes, mark cvp_isready = FALSE */
    if ( p_cvp_array && p_isready )
    {
      for ( cnt = 0 ; cnt < CVS_MAX_ATTACHED_VOCPROC_CNT; cnt++ )
      {
        if (ctrl->packet->src_port == p_cvp_array[cnt] )
        {
          if ( p_isready[cnt] == TRUE )
          {
            session_obj->active_set.attach_table.cvp_ready_cnt = 
              session_obj->active_set.attach_table.cvp_ready_cnt - 1;
          }
          p_isready[cnt] = FALSE;
          break;
        }
      }
    }

    ( void ) cvs_eval_voice_state_decision( session_obj->active_set.attach_table.cvp_ready_cnt,
                                            session_obj->is_enable_received_from_mvm, &next_goal );

    rc = cvs_helper_create_new_goal_control( ctrl, session_obj, next_goal );
    if ( rc )
    {
      rc = __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );
      CVS_PANIC_ON_ERROR( rc );
      return APR_EOK;
    }

  }
  else
  {
    if ( ctrl->pendjob_obj->simple_job.is_completed )
    {
      rc = cvs_free_object( ctrl->pendjob_obj );
      CVS_PANIC_ON_ERROR( rc );
      ctrl->pendjob_obj = NULL;

      rc = __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );
      CVS_PANIC_ON_ERROR( rc );

      return APR_EOK;
    }
  }

  rc = cvs_state_control( session_obj );

  return APR_EPENDING;
}

/* Update vocproc ready state in attach_table*/
static int32_t cvs_stream_vocproc_evt_ready_processing (
   cvs_pending_control_t* ctrl
)
{
  /* This functio will process VSS_IVOCPROC_EVT_READY received from CVP.
     If CVP has sent vocproc not ready, check if stream can be stopped */
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t cnt = 0;
  uint16_t *p_cvp_array = NULL;
  bool_t *p_isready = NULL;
  cvs_goal_enum_t next_goal;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {

    p_cvp_array = (uint16_t *)&session_obj->active_set.attach_table.cvp_handles;
    p_isready   = (bool_t *)&session_obj->active_set.attach_table.cvp_isready;

    /* Check if this CVP is attached, if yes, mark cvp_isready = TRUE */
    if ( p_cvp_array && p_isready )
    {
      for ( cnt = 0; cnt < CVS_MAX_ATTACHED_VOCPROC_CNT; cnt++ )
      {
        if (ctrl->packet->src_port == p_cvp_array[cnt] )
        {
          if ( p_isready[cnt] == FALSE )
          {
            session_obj->active_set.attach_table.cvp_ready_cnt = 
              session_obj->active_set.attach_table.cvp_ready_cnt + 1;
          }
          p_isready[cnt] = TRUE;
          break;
        }
      }
    }

    /* If CVS is already in RUN state, no need to set GOAL_ENABLE again */
    if( session_obj->session_ctrl.state != CVS_STATE_ENUM_RUN )
    {
      ( void ) cvs_eval_voice_state_decision( session_obj->active_set.attach_table.cvp_ready_cnt,
                                                session_obj->is_enable_received_from_mvm, &next_goal );

      rc = cvs_helper_create_new_goal_control( ctrl, session_obj, next_goal );
      if ( rc )
      {
         rc = __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );
         CVS_PANIC_ON_ERROR( rc );
         return APR_EOK;
      }
    }
    else
    {
      rc = __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );
      CVS_PANIC_ON_ERROR( rc );
      return APR_EOK;
    }
  }
  else
  {
    if ( ctrl->pendjob_obj->simple_job.is_completed )
    {
      rc = cvs_free_object( ctrl->pendjob_obj );
      CVS_PANIC_ON_ERROR( rc );
      ctrl->pendjob_obj = NULL;

      rc = __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );
      CVS_PANIC_ON_ERROR( rc );
      return APR_EOK;
    }
  }

  rc = cvs_state_control( session_obj );

  return APR_EPENDING;
}

/****************************************************************************
 * GATING COMMAND PROCESSING FUNCTIONS                                      *
 ****************************************************************************/

static int32_t cvs_core_create_session_cmd_ctrl (
  cvs_pending_control_t* ctrl,
  bool_t is_full_ctrl
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  uint32_t access_bits;
  cvs_indirection_object_t* indirect_obj = NULL;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_create_full_control_session_t* payload = NULL;
  uint32_t payload_size;
  char_t* req_session_name;
  uint32_t req_session_name_size;
  uint32_t media_id;
  uint32_t target_dec_sr;
  uint32_t target_enc_sr;
  uint32_t target_rx_pp_sr;
  uint32_t target_tx_pp_sr;
  uint16_t client_addr;
  cvs_simple_job_object_t* job_obj;
  voice_cmd_shared_mem_unmap_regions_t mem_unmap;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      access_bits = APR_SET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                                   ( ( is_full_ctrl ) ? 1 : 0 ) );
      if ( is_full_ctrl )
      {
        payload = APRV2_PKT_GET_PAYLOAD(
                    vss_istream_cmd_create_full_control_session_t, ctrl->packet );
        payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

        if ( payload_size < sizeof( vss_istream_cmd_create_full_control_session_t ) )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_core_create_session_cmd_ctrl(): Unexpected create full control payload size, %d < %d",
                                                  payload_size,
                                                  sizeof( vss_istream_cmd_create_full_control_session_t ) );
          rc = APR_EBADPARAM;
          break;
        }

        req_session_name = ( ( ( char_t* ) payload ) +
                           sizeof( vss_istream_cmd_create_full_control_session_t ) );
        req_session_name_size = ( payload_size -
                                sizeof( vss_istream_cmd_create_full_control_session_t ) );
      }
      else
      {
        req_session_name = APRV2_PKT_GET_PAYLOAD( char_t, ctrl->packet );;
        req_session_name_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
      }

      if ( req_session_name_size > 0 )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_core_create_session_cmd_ctrl(): Session name = %s",
                                              req_session_name );
      }

      rc = cvs_create_indirection_object( client_addr,
                                          req_session_name,
                                          req_session_name_size,
                                          access_bits,
                                          &indirect_obj,
                                          &session_obj );
      if ( rc ) break;
      checkpoint = 1;

      /* Setup for the first full control client. */
      if ( is_full_ctrl )
      {
        if ( session_obj->master_client_addr == APRV2_PKT_INIT_ADDR_V )
        {
          switch ( payload->direction )
          {
          case CVS_DIRECTION_RX:
            media_id = payload->dec_media_type;
            break;

          case CVS_DIRECTION_TX:
            media_id = payload->enc_media_type;
            break;

          case CVS_DIRECTION_RX_TX:
            {
              if ( payload->enc_media_type != payload->dec_media_type )
              {
                rc = APR_EBADPARAM;
                break;
              }

              media_id = payload->enc_media_type;
            }
            break;

          default:
            rc = APR_EBADPARAM;
            break;
          }

          if ( rc ) break;

          /* Write access. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->master_client_addr = ctrl->packet->src_addr;
          session_obj->master_client_port = ctrl->packet->src_port;
          session_obj->master_cvs_port = ( ( uint16_t ) indirect_obj->header.handle );
          session_obj->target_set.direction = payload->direction;
          ( void ) apr_lock_leave( session_obj->lock );

          /* Note there is no need to cache the network ID provided in the create
           * session payload. The network ID will always be proviced by MVM through
           * VSS_ICOMMON_CMD_SET_SYSTEM_COPNFIG. */

          if ( TRUE == cvs_media_id_is_valid( media_id ) )
          {
            /* Determine the target_set enc, dec and pp sampling rates based on the
             * media type and the client requested variable vocoder sampling rates.
             * (And validate paramters in the process.)
             */
            rc = cvs_determine_target_sampling_rates(
                        media_id,
                        session_obj->requested_var_voc_rx_sampling_rate,
                        session_obj->requested_var_voc_tx_sampling_rate,
                        &target_dec_sr,
                        &target_enc_sr,
                        &target_rx_pp_sr,
                        &target_tx_pp_sr );
            if ( rc ) break;

            session_obj->target_set.media_id = media_id;
            session_obj->target_set.system_config.media_id = media_id;
            session_obj->target_set.system_config.dec_sr = target_dec_sr;
            session_obj->target_set.system_config.enc_sr = target_enc_sr;
            session_obj->target_set.system_config.rx_pp_sr = target_rx_pp_sr;
            session_obj->target_set.system_config.tx_pp_sr = target_tx_pp_sr;
          }
        }
        else
        { /* Allow only one full control session per named session. */
          rc = APR_EALREADY;
          break;
        }
      }

      /* Update the packet->dst_port to the new session handle. */
      ctrl->packet->dst_port = ( ( uint16_t ) indirect_obj->header.handle );

      /* If this is not the first handle being opened for this session then
         state machine is already out of reset and there's no need to run it. */
      if ( session_obj->indirection_q.size >= 2 )
      {
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }
      else
      {
        if ( is_full_ctrl == FALSE )
        {
          /* If passive control is created first, assume a default direction of RX + TX. */
          session_obj->target_set.direction = CVS_DIRECTION_RX_TX;
        }

        rc = cvs_helper_create_new_goal_control( ctrl, session_obj, CVS_GOAL_ENUM_CREATE );
        if ( rc )
        {
          rc = APR_EOK;
          break;
        }
      }
    }
    else
    {
      /* Wait until the job is done. */
      if ( ctrl->pendjob_obj->simple_job.is_completed )
      {
        if ( ctrl->pendjob_obj->simple_job.status )
        { /* Destroy the session and respond a failure. */
          rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
          CVS_PANIC_ON_ERROR( rc );

          /* Unmap shared memory if required. */
          if ( session_obj->shared_heap.vsm_mem_handle != APR_NULL_V )
          {
            rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
            CVS_PANIC_ON_ERROR( rc );
            job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

            mem_unmap.mem_map_handle = session_obj->shared_heap.vsm_mem_handle;

            rc = __aprv2_cmd_alloc_send(
                   cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                   cvs_vsm_addr, APR_NULL_V,
                   job_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
                   &mem_unmap, sizeof( mem_unmap ) );
            CVS_COMM_ERROR( rc, client_addr );
          }

          /* if there was a VSM session created with valid handle,
             then destroy it. */
          if ( session_obj->vsm_stream_handle != APR_NULL_V )
          {
            rc = cvs_create_simple_job_object( session_obj->header.handle, &job_obj );
            CVS_PANIC_ON_ERROR( rc );
            job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_self_destruct_result_rsp_fn;

            rc = __aprv2_cmd_alloc_send(
                   cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                   cvs_vsm_addr, session_obj->vsm_stream_handle,
                   job_obj->header.handle,
                   VSM_CMD_DESTROY_SESSION, NULL, 0 );
            CVS_COMM_ERROR( rc, client_addr );
          }

          rc = cvs_get_typed_object ( ctrl->packet->dst_port,CVS_OBJECT_TYPE_ENUM_INDIRECTION, ( cvs_object_t** ) &indirect_obj );
          CVS_PANIC_ON_ERROR( rc );

          rc = cvs_free_object( ( cvs_object_t* ) indirect_obj );
          CVS_PANIC_ON_ERROR( rc );
          rc = cvs_destroy_session_object( session_obj );
          CVS_PANIC_ON_ERROR( rc );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, ctrl->pendjob_obj->simple_job.status );
          CVS_COMM_ERROR( rc, client_addr );
        }
        else
        {
          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
          CVS_COMM_ERROR( rc, client_addr );
        }

        rc = cvs_free_object( ctrl->pendjob_obj );
        CVS_PANIC_ON_ERROR( rc );

        break;
      }
    }

    /* Run the CVD session state machine. The CVD session state machine should
     * only need to run when there are pending commands to process.
     */
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    CVS_PANIC_ON_ERROR( rc );
    rc = cvs_state_control( session_obj );

    return APR_EPENDING;
  }

  if ( rc )
  {
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
    CVS_COMM_ERROR( rc, client_addr );

    switch ( checkpoint )
    {
    case 1:
      {
        rc = cvs_free_object( ( cvs_object_t* ) indirect_obj );
        CVS_PANIC_ON_ERROR( rc );
        rc = cvs_destroy_session_object( session_obj );
        CVS_PANIC_ON_ERROR( rc );
      }
      /*-fallthru */

    default:
      break;
    }
  }

  return APR_EOK;
}

static int32_t cvs_destroy_session_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vsm_set_pkt_exchange_mode_t set_pkt_exchange_mode;
  voice_cmd_shared_mem_unmap_regions_t voice_unmap_memory;
  uint16_t client_addr;

  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE_TO_INBAND, 
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_CONFIGURATION,
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_CLEAR_OOB_CONFIGURATION,
    CVS_SEQUENCER_ENUM_DESTROY_SESSION,
    CVS_SEQUENCER_ENUM_WAIT_3,
    CVS_SEQUENCER_ENUM_CHECK_SESSION_REF_COUNT,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc )
  {
    return APR_EOK;
  }

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* Fail the Destroy Command if the State Machine is not in IDLE state
     * and CVS is attached to vocproc for FULL control session.
     * This guarantees that destroy command is not processed while the
     * state machine is in RUN state and session object resources are 
     * accessed by other commands (from other threads)
     */
    if ( ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                        indirect_obj->access_bits ) ) &&
         ( session_obj->session_ctrl.state != CVS_STATE_ENUM_IDLE ) &&
         ( session_obj->active_set.attach_table.cvp_ready_cnt != 0 )
       )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvs_destroy_session_cmd_ctrl(): Cannot handle DESTROY CMD right now. " \
             "state=0x%08X cvp_ready_cnt=%d",session_obj->session_ctrl.state,
             session_obj->active_set.attach_table.cvp_ready_cnt );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    if ( ( APR_GET_FIELD( CVS_INDIRECTION_ACCESS_FULL_CONTROL,
                        indirect_obj->access_bits ) ) &&
         ( ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_MAILBOX ) ||
           ( session_obj->packet_exchange_info.mode == VSS_IPKTEXG_MODE_OUT_OF_BAND ) )
       )
    {/* If the full control client destroy the session, and the current packet
      * exchange mode is mailbox, clear the mailbox packet exchange settings.
      */
      seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE_TO_INBAND;
    }
    else
    {
      seqjob_obj->state = CVS_SEQUENCER_ENUM_DESTROY_SESSION;
    }
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE_TO_INBAND:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
 
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );
 
        set_pkt_exchange_mode.pkt_exchange_mode = VSS_IPKTEXG_MODE_IN_BAND;
 
        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_PKT_EXCHANGE_MODE,
               &set_pkt_exchange_mode, sizeof( set_pkt_exchange_mode ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
 
        session_obj->packet_exchange_info.mode = VSS_IPKTEXG_MODE_IN_BAND;
 
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;
 
      case CVS_SEQUENCER_ENUM_WAIT_1:
      {
        /* Wait for the VSM_CMD_SET_PKT_EXCHANGE_MODE to be processed by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;
 
        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }
 
        if ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_CONFIGURATION;
        }
        else if ( session_obj->packet_exchange_info.oob_info.is_configured == TRUE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAR_OOB_CONFIGURATION;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_DESTROY_SESSION;
        }
        
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      continue;
 
      case CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_CONFIGURATION:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
 
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );
 
        voice_unmap_memory.mem_map_handle =
        session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle;
 
        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
               &voice_unmap_memory, sizeof( voice_unmap_memory ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
 
        apr_memmgr_free( &cvs_heapmgr, 
        session_obj->packet_exchange_info.mailbox_info.config.oob_enc_buf );

        /* Clear the cached mailbox settings. */
        session_obj->packet_exchange_info.mailbox_info.is_configured = FALSE;
        session_obj->packet_exchange_info.mailbox_info.config.cvs_mem_handle = 0;
        session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf = NULL;
        session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf_mem_size = 0;
        session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf = NULL;
        session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf_mem_size = 0;
        session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle = 0;
        session_obj->packet_exchange_info.mailbox_info.config.max_enc_pkt_size = 0;
        session_obj->packet_exchange_info.mailbox_info.config.oob_enc_buf = NULL;
        session_obj->packet_exchange_info.mailbox_info.config.oob_dec_buf = NULL;
        session_obj->packet_exchange_info.mailbox_info.config.tx_ref_timestamp_us = 0;
        session_obj->packet_exchange_info.mailbox_info.config.rx_ref_timestamp_us = 0;
        session_obj->packet_exchange_info.mailbox_info.config.request_to_start_timestamp_us = 0;
        session_obj->packet_exchange_info.mailbox_info.config.rx_req_and_dequeue_time_diff_us =
          CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
        session_obj->packet_exchange_info.mailbox_info.config.is_start_requested = FALSE;
        session_obj->packet_exchange_info.mailbox_info.config.is_reseted = FALSE;
        session_obj->packet_exchange_info.mailbox_info.config.is_time_ref_received = FALSE;
        session_obj->packet_exchange_info.mailbox_info.config.is_rx_expiry_proc_disabled = FALSE;
        session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timestamp_us = 0;

        if ( session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle != APR_NULL_V )
        {
          ( void ) cvs_mailbox_timer_destroy(
                    session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle );
          session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
        }

        ( void )mmstd_memset(
                  &session_obj->packet_exchange_info.mailbox_info.stats, 0,
                  sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) 
                  );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;
      }
      continue;

      case CVS_SEQUENCER_ENUM_WAIT_2:
      {
        /* Wait for the VOICE_CMD_SHARED_MEM_UNMAP_REGIONS to be processed by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        if ( session_obj->packet_exchange_info.oob_info.is_configured == TRUE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAR_OOB_CONFIGURATION;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_DESTROY_SESSION;
        }
        
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      continue;

      case CVS_SEQUENCER_ENUM_CLEAR_OOB_CONFIGURATION:
      {
        apr_lock_enter( session_obj->lock );

        /* Clear the cached oob packate exchange settings. */
        session_obj->packet_exchange_info.oob_info.is_configured = FALSE;
        session_obj->packet_exchange_info.oob_info.config.mem_handle = 0;
        session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw = 0;
        session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw = 0;
        session_obj->packet_exchange_info.oob_info.config.enc_buf_size = 0;
        session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw = 0;
        session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_msw = 0;
        session_obj->packet_exchange_info.oob_info.config.dec_buf_size = 0;

        apr_lock_leave( session_obj->lock );
        seqjob_obj->state = CVS_SEQUENCER_ENUM_DESTROY_SESSION;
      }
      continue;

      case CVS_SEQUENCER_ENUM_DESTROY_SESSION:
      {
        if ( session_obj->indirection_q.size >= 2 )
        {
          ( void ) cvs_free_object( ( cvs_object_t* ) indirect_obj );
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );

        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        /* The session shouldn't be doing anything. */
        if ( session_obj->session_ctrl.goal != CVS_GOAL_ENUM_NONE )
        {
          CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        }

        /* The goal is reset by the state machine on completion. */
        session_obj->session_ctrl.goal = CVS_GOAL_ENUM_DESTROY;
        /* The pendjob_handle will signal completion. The pendjob_handle is
         * reset by the state machine on completion.
         */
        session_obj->session_ctrl.pendjob_handle = subjob_obj->header.handle;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_3;
      }
      continue;

      case CVS_SEQUENCER_ENUM_WAIT_3:
      {
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          rc = cvs_state_control( session_obj );
          return APR_EPENDING;
        }

        /* Session object reference count is set to 1 during session creation
         * hence decrementing it. When reference count hits 0 its guaranteed 
         * that no other command is accessing the session object.
         */
        apr_lock_enter( cvs_ref_cnt_lock );
        --session_obj->ref_cnt;
        apr_lock_leave( cvs_ref_cnt_lock );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_CHECK_SESSION_REF_COUNT;
      }
      continue;

      case CVS_SEQUENCER_ENUM_CHECK_SESSION_REF_COUNT:
      {
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        apr_lock_enter( cvs_ref_cnt_lock );
        if ( session_obj->ref_cnt > 0)
        {
          apr_lock_leave( cvs_ref_cnt_lock );
          return APR_EPENDING;
        }
        apr_lock_leave( cvs_ref_cnt_lock );

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
        ( void )cvs_destroy_session_object( session_obj );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
      }
      continue;

      case CVS_SEQUENCER_ENUM_COMPLETE:
      {
        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
        return APR_EOK;
      }

      default:
        CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
    }
    return APR_EPENDING;
  }
  return APR_EOK;
}

static int32_t cvs_stream_update_attached_vocproc_cmd_ctrl (
  cvs_pending_control_t* ctrl,
  cvs_goal_enum_t do_goal
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    switch ( do_goal )
    {
    case CVS_GOAL_ENUM_ATTACH_VOCPROC:
      {
        /* Store the CVP and CVS handles given for attaching */
        session_obj->target_set.attach_detach_vocproc_handle =
          APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_attach_vocproc_t, ctrl->packet )->handle;

        session_obj->target_set.attach_detach_stream_handle = ctrl->packet->dst_port;
      }
      break;

    case CVS_GOAL_ENUM_DETACH_VOCPROC:
      {
        /* Store the CVP and CVS handles given for detaching */
        session_obj->target_set.attach_detach_vocproc_handle =
          APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_detach_vocproc_t, ctrl->packet )->handle;

        session_obj->target_set.attach_detach_stream_handle = ctrl->packet->dst_port;
      }
      break;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    rc = cvs_helper_create_new_goal_control( ctrl, session_obj, do_goal );
    if ( rc ) return APR_EOK;

  }
  else  /* Goal executing. */
  {
    /* Completed flag will be set once CVP/VSM are updated or if there is an error. */
    if ( ctrl->pendjob_obj->simple_job.is_completed )
    { /* Return packet to sender (CVS's client). */
      client_addr = ctrl->packet->src_addr;

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet,
                                    ctrl->pendjob_obj->simple_job.status );
      CVS_COMM_ERROR( rc, client_addr );

      rc = cvs_free_object( ctrl->pendjob_obj );
      CVS_PANIC_ON_ERROR( rc );

      return APR_EOK;
    }
  }

  rc = cvs_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t cvs_stream_enable_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_goal_enum_t next_goal;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( cvs_media_id_is_valid( session_obj->active_set.media_id ) == FALSE )
      { /* Stream cannot run without valid media ID. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_enable_cmd_ctrl: Cannot enable stream, media id has not been set" );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_ENOTREADY );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      if ( session_obj->is_stream_config_changed == TRUE )
      {
        /* If the stream configuration has changed since the last time when
         * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed, end the
         * current command and send VSS_ISTREAM_EVT_RECONFIG to MVM. Note that
         * the stream can only receive the enable command if it has been attached
         * to an MVM session. Therefore the session_obj->attached_mvm_handle is
         * guaranteed to be valid.
         */
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_mvm_addr, session_obj->attached_mvm_handle,
               0, VSS_ISTREAM_EVT_RECONFIG,
               NULL, 0 );
        CVS_COMM_ERROR( rc, cvs_mvm_addr );

        break;
      }

      session_obj->is_enable_received_from_mvm = TRUE;

      ( void ) cvs_eval_voice_state_decision( session_obj->active_set.attach_table.cvp_ready_cnt,
                                              session_obj->is_enable_received_from_mvm, &next_goal );

      rc = cvs_helper_create_new_goal_control( ctrl, session_obj, next_goal );
      if ( rc ) break;
    }
    else
    {
      rc = cvs_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK ) break;
    }

    rc = cvs_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

/* TODO: Modify cvs_stream_disable_cmd_ctrl() so that it handles the
         disable sequence sequence. We need to stop the voice stream because
         DISABLE is a type of tear down.

         Then do STANDBY!  Just mute the voice stream.
*/

static int32_t cvs_stream_disable_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_goal_enum_t next_goal;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    session_obj->is_enable_received_from_mvm = FALSE;
    ( void ) cvs_eval_voice_state_decision( session_obj->active_set.attach_table.cvp_ready_cnt,
                                            session_obj->is_enable_received_from_mvm, &next_goal );

    rc = cvs_helper_create_new_goal_control( ctrl, session_obj, next_goal );
    if ( rc ) return APR_EOK;
  }
  else
  {
    rc = cvs_helper_simple_wait_for_goal_completion_control( ctrl );
    if ( rc == APR_EOK ) return APR_EOK;
  }

  rc = cvs_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t cvs_stream_reinit_cmd_ctrl(
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_create_new_goal_control( ctrl, session_obj, CVS_GOAL_ENUM_REINIT );
    if ( rc ) return APR_EOK;
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet,
                                  ctrl->pendjob_obj->simple_job.status );
    CVS_COMM_ERROR( rc, client_addr );

    rc = cvs_free_object( ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    return APR_EOK;
  }

  rc = cvs_state_control( session_obj );

  return APR_EPENDING;
}

/* Handles recording via pseudo port. */
static int32_t cvs_stream_start_record_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_start_record_t* in_args;
  vsm_start_record_v2_t record_param;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_istream_cmd_start_record_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_start_record_t,
                                     ctrl->packet );

    /* Recording already enabled. */
    if ( session_obj->active_set.record.enable_flag == CVS_ENABLED )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->active_set.record.enable_flag = CVS_ENABLED;
    session_obj->active_set.record.rx_tap_point = in_args->rx_tap_point;
    session_obj->active_set.record.tx_tap_point = in_args->tx_tap_point;
    session_obj->active_set.record.port_id = VSS_IRECORD_PORT_ID_DEFAULT;
    session_obj->active_set.record.mode = VSS_IRECORD_MODE_TX_RX_MIXING;

    /* Not in RUN State, cache and return immediately.
     * Cached value will be applied on transition to RUN state.
     */
    if ( session_obj->session_ctrl.state != CVS_STATE_ENUM_RUN )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_simple_job_object( session_obj->header.handle,
           ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    record_param.rx_tap_point = in_args->rx_tap_point;
    record_param.tx_tap_point = in_args->tx_tap_point;
    record_param.aud_ref_port = VSS_IRECORD_PORT_ID_DEFAULT;
    record_param.record_mode = VSS_IRECORD_MODE_TX_RX_MIXING;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_record_cmd_ctrl(): rx_tap_point: 0x%08X, tx_tap_point: 0x%08X",
                                          record_param.rx_tap_point,
                                          record_param.tx_tap_point );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_record_cmd_ctrl(): port_id: 0x%04X, mode: 0x%08X",
                                          record_param.aud_ref_port,
                                          record_param.record_mode );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           ctrl->pendjob_obj->header.handle, VSM_CMD_START_RECORD_V2,
           &record_param, sizeof( record_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

/* Handles recording via any port, pseudo or real. */
static int32_t cvs_stream_start_record_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_irecord_cmd_start_t* in_args;
  vsm_start_record_v2_t record_param;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_irecord_cmd_start_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_irecord_cmd_start_t,
                                     ctrl->packet );

    /* Recording already enabled. */
    if ( session_obj->active_set.record.enable_flag == CVS_ENABLED )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->active_set.record.enable_flag = CVS_ENABLED;
    session_obj->active_set.record.rx_tap_point = in_args->rx_tap_point;
    session_obj->active_set.record.tx_tap_point = in_args->tx_tap_point;
    session_obj->active_set.record.port_id = in_args->port_id;
    session_obj->active_set.record.mode = in_args->mode;

    /* Not in RUN State, cache and return immediately.
     * Cached value will be applied on transition to RUN state.
     */
    if ( session_obj->session_ctrl.state != CVS_STATE_ENUM_RUN )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_simple_job_object( session_obj->header.handle,
           ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    record_param.rx_tap_point = in_args->rx_tap_point;
    record_param.tx_tap_point = in_args->tx_tap_point;
    record_param.aud_ref_port = in_args->port_id;
    record_param.record_mode = in_args->mode;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_record_v2_cmd_ctrl(): rx_tap_point: 0x%08X, tx_tap_point: 0x%08X",
                                          record_param.rx_tap_point,
                                          record_param.tx_tap_point );
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_record_v2_cmd_ctrl(): port_id: 0x%04X, mode: 0x%08X",
                                          record_param.aud_ref_port,
                                          record_param.record_mode );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           ctrl->pendjob_obj->header.handle, VSM_CMD_START_RECORD_V2,
           &record_param, sizeof( record_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

/* VSS_ISTREAM_CMD_STOP_RECORD is deprecated and VSS_IRECORD_CMD_STOP
 * is introduced. The two commands have the same GUID. They behave the same
 * and share the following command handling function.
 */
static int32_t cvs_stream_stop_record_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_enable_state_enum_t enable_flag;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    enable_flag = session_obj->active_set.record.enable_flag;
    session_obj->active_set.record.enable_flag = CVS_DISABLED;

    if ( ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN ) &&
         ( enable_flag == CVS_ENABLED ) )
    {
      rc = cvs_create_simple_job_object( session_obj->header.handle,
             ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             ctrl->pendjob_obj->header.handle, VSM_CMD_STOP_RECORD,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
    else if (( session_obj->session_ctrl.state == CVS_STATE_ENUM_RESET )
    		|| ( session_obj->session_ctrl.state == CVS_STATE_ENUM_IDLE ))
    {
    	rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    	CVS_COMM_ERROR( rc, client_addr );
    	return APR_EOK;
    }
    else
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

/* Handles playback via pseudo port. */
static int32_t cvs_stream_start_playback_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vsm_start_playback_t playback_param;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* Playback already enabled. */
    if ( session_obj->active_set.playback.enable_flag == CVS_ENABLED )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->active_set.playback.enable_flag = CVS_ENABLED;
    session_obj->active_set.playback.port_id = VSS_IPLAYBACK_PORT_ID_DEFAULT;

    /* Not in RUN State, cache and return immediately.
     * Cached value will be applied on transition to RUN state.
     */
    if ( session_obj->session_ctrl.state != CVS_STATE_ENUM_RUN )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_simple_job_object( session_obj->header.handle,
           ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    playback_param.aud_ref_port = VSS_IPLAYBACK_PORT_ID_DEFAULT;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_playback_cmd_ctrl(): port_id: 0x%04X",
                                          playback_param.aud_ref_port );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           ctrl->pendjob_obj->header.handle, VSM_CMD_START_PLAYBACK_V2,
           &playback_param, sizeof( playback_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

/* Handles playback via any port, pseudo or real. */
static int32_t cvs_stream_start_playback_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_iplayback_cmd_start_t* in_args;
  vsm_start_playback_t playback_param;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_iplayback_cmd_start_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_iplayback_cmd_start_t,
                                     ctrl->packet );

    /* Playback already enabled. */
    if ( session_obj->active_set.playback.enable_flag == CVS_ENABLED )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->active_set.playback.enable_flag = CVS_ENABLED;
    session_obj->active_set.playback.port_id = in_args->port_id;

    /* Not in RUN State, cache and return immediately.
     * Cached value will be applied on transition to RUN state.
     */
    if ( session_obj->session_ctrl.state != CVS_STATE_ENUM_RUN )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_simple_job_object( session_obj->header.handle,
           ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
    CVS_PANIC_ON_ERROR( rc );

    playback_param.aud_ref_port = in_args->port_id;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_start_playback_v2_cmd_ctrl(): port_id: 0x%04X",
                                          playback_param.aud_ref_port );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           ctrl->pendjob_obj->header.handle, VSM_CMD_START_PLAYBACK_V2,
           &playback_param, sizeof( playback_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

/* VSS_ISTREAM_CMD_STOP_PLAYBACK is deprecated and VSS_IPLAYBACK_CMD_STOP
 * is introduced. The two commands have the same GUID. They behave the same
 * and share the following command handling function.
 */
static int32_t cvs_stream_stop_playback_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_enable_state_enum_t enable_flag;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    enable_flag = session_obj->active_set.playback.enable_flag;
    session_obj->active_set.playback.enable_flag = CVS_DISABLED;

    if ( ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN ) &&
         ( enable_flag == CVS_ENABLED ) )
    {
      rc = cvs_create_simple_job_object( session_obj->header.handle,
             ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             ctrl->pendjob_obj->header.handle, VSM_CMD_STOP_PLAYBACK,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
	else if (( session_obj->session_ctrl.state == CVS_STATE_ENUM_RESET )
				|| ( session_obj->session_ctrl.state == CVS_STATE_ENUM_IDLE ))
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );	  
      return APR_EOK;
    }
    else
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }

  if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
  {
    uint32_t sts = ctrl->pendjob_obj->simple_job.status;

    ( void ) cvs_free_object( ctrl->pendjob_obj );
    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
    CVS_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

static int32_t cvs_stream_set_media_type_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  cvs_simple_job_object_t* subjob_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  vss_istream_cmd_set_media_type_t* in_args;
  vsm_set_media_type_t set_media_type;
  vsm_set_samp_rate_t set_samp_rate;
  uint32_t target_dec_sampling_rate;
  uint32_t target_enc_sampling_rate;
  uint32_t target_rx_pp_sampling_rate;
  uint32_t target_tx_pp_sampling_rate;
  uint32_t media_id = VSS_MEDIA_ID_NONE;
  uint16_t client_addr;
  uint32_t status = APR_EOK;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_SET_VSM_MEDIA_TYPE, /* BACKWARD COMPATIBILITY */
    CVS_SEQUENCER_ENUM_WAIT_1, /* BACKWARD COMPATIBILITY */
    CVS_SEQUENCER_ENUM_IS_MEDIA_VAR_VOC,
    CVS_SEQUENCER_ENUM_SET_VAR_VOC_SAMP_RATE,
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
            ctrl, sizeof( vss_istream_cmd_set_media_type_t ) );
    if ( rc ) return APR_EOK;

    /* This command is allowed only with full control handle. */
    if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
      return APR_EOK;

    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Setting media type when the stream is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_media_type_cmd_ctrl(): Stream is running." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_media_type_t,
                                      ctrl->packet );

    switch ( session_obj->active_set.direction )
    {
    case CVS_DIRECTION_RX:
      media_id = in_args->rx_media_id;
      break;

    case CVS_DIRECTION_TX:
      media_id = in_args->tx_media_id;
      break;

    case CVS_DIRECTION_RX_TX:
      {
        if ( in_args->rx_media_id != in_args->tx_media_id )
        { /* VSM currently doesn't support independent Rx and Tx media types. */
          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
          CVS_COMM_ERROR( rc, client_addr );
          rc = APR_EBADPARAM;
        }

        media_id = in_args->rx_media_id;
      }
      break;

    default:
      rc = APR_EUNEXPECTED;
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    if ( rc ) return APR_EOK;

    if ( cvs_media_id_is_valid( media_id ) == FALSE )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_sequencer_job_object(
          ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_VSM_MEDIA_TYPE;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVS_SEQUENCER_ENUM_SET_VSM_MEDIA_TYPE:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
 
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        /* Save the media id in target set, and update active set if media id is
          * set successfully on VSM. */
        session_obj->target_set.media_id = media_id;

        session_obj->voc_operating_mode_info.is_rx_mode_received = FALSE;
        session_obj->voc_operating_mode_info.is_tx_mode_received = FALSE;

        set_media_type.media_type = media_id;

        rc = __aprv2_cmd_alloc_send(
                cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                cvs_vsm_addr, session_obj->vsm_stream_handle,
                subjob_obj->header.handle, VSM_CMD_SET_MEDIA_TYPE,
                &set_media_type, sizeof( set_media_type ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;

    case CVS_SEQUENCER_ENUM_WAIT_1:
      {
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == TRUE )
        {
          status = subjob_obj->status;
          if ( status == APR_EOK )
          {
            if ( ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) &&
                   ( session_obj->voc_operating_mode_info.is_rx_mode_received == FALSE ) ) ||
                 ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) &&
                   ( session_obj->voc_operating_mode_info.is_tx_mode_received == FALSE ) ) ||
                 ( ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) &&
                   ( ( session_obj->voc_operating_mode_info.is_rx_mode_received == FALSE ) ||
                     ( session_obj->voc_operating_mode_info.is_tx_mode_received == FALSE ) 
                   ) )
               )
            {
              /* Wait for the Tx and Rx path vocoder operating mode updates from VSM
               * due to media ID update on the stream.
               */
              return APR_EPENDING;
            }

            if ( session_obj->active_set.media_id != session_obj->target_set.media_id )
            {
              session_obj->is_stream_config_changed = TRUE;
              /* Write access to target_set.media_id. */
              ( void ) apr_lock_enter( session_obj->lock );
              session_obj->active_set.media_id = session_obj->target_set.media_id;
              ( void ) apr_lock_leave( session_obj->lock );
            }

            seqjob_obj->state = CVS_SEQUENCER_ENUM_IS_MEDIA_VAR_VOC;
          }
          else
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_media_type_cmd_ctrl():" \
                                                    "VSM_CMD_SET_MEDIA_TYPE returned error=0x%08x.",
                                                    status);
            seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          }

          ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
        }
        else
        {
          return APR_EPENDING;
        }
      }
      continue;

    case CVS_SEQUENCER_ENUM_IS_MEDIA_VAR_VOC:
      {
        /* Check to see if changed Media ID is Variable Vocoder to apply sampling rates */
        if ( cvs_vocoder_is_variable( session_obj->active_set.media_id ) )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_VAR_VOC_SAMP_RATE;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
        }
      }
      continue;

    case CVS_SEQUENCER_ENUM_SET_VAR_VOC_SAMP_RATE:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
 
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        set_samp_rate.rx_samp_rate = session_obj->requested_var_voc_rx_sampling_rate;
        set_samp_rate.tx_samp_rate = session_obj->requested_var_voc_tx_sampling_rate;

        rc = __aprv2_cmd_alloc_send(
                cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                cvs_vsm_addr, session_obj->vsm_stream_handle,
                subjob_obj->header.handle, VSM_CMD_SET_STREAM_PP_SAMP_RATE,
                &set_samp_rate, sizeof( set_samp_rate ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;
      }
      continue;

    case CVS_SEQUENCER_ENUM_WAIT_2:
      {
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == TRUE )
        {
          status = subjob_obj->status;
          if ( APR_EOK != status )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_media_type_cmd_ctrl():" \
                                                    "VSM_CMD_SET_STREAM_PP_SAMP_RATE returned error=0x%08x.",
                                                    status);
          }

          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
        }
        else
        {
          return APR_EPENDING;
        }
      }
      continue;

    case CVS_SEQUENCER_ENUM_COMPLETE:
      {
        /* Determine the target_set enc, dec and pp sampling rates based on the
         * media type and the client requested variable vocoder sampling rates.
         */
        rc = cvs_determine_target_sampling_rates(
               session_obj->target_set.media_id,
                  session_obj->requested_var_voc_rx_sampling_rate,
                  session_obj->requested_var_voc_tx_sampling_rate,
                  &target_dec_sampling_rate,
                  &target_enc_sampling_rate,
                  &target_rx_pp_sampling_rate,
                  &target_tx_pp_sampling_rate );
        if ( rc )
        {
          /* Inputs to cvs_determine_target_sampling_rates are valid, any error
           * is unexpected.
           */
          CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }

        /* Save parameters in target set to be picked up by the
         * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command sequencer.
         */
        session_obj->target_set.system_config.media_id = session_obj->active_set.media_id;
        session_obj->target_set.system_config.dec_sr = target_dec_sampling_rate;
        session_obj->target_set.system_config.enc_sr = target_enc_sampling_rate;
        session_obj->target_set.system_config.rx_pp_sr = target_rx_pp_sampling_rate;
        session_obj->target_set.system_config.tx_pp_sr = target_tx_pp_sampling_rate;

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
        CVS_COMM_ERROR( rc, client_addr );
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

  return APR_EFAILED;
}

static int32_t cvs_stream_set_var_voc_sampling_rate_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_set_var_voc_sampling_rate_t* in_args;
  uint32_t requested_var_voc_rx_sampling_rate;
  uint32_t requested_var_voc_tx_sampling_rate;
  uint32_t target_dec_sr;
  uint32_t target_enc_sr;
  uint32_t target_rx_pp_sr;
  uint32_t target_tx_pp_sr;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_istream_cmd_set_var_voc_sampling_rate_t ) );
    if ( rc ) break;

    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Setting sampling rate when the stream is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_var_voc_sampling_rate_cmd_ctrl(): " \
                                            "Stream is running." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( cvs_media_id_is_valid( session_obj->active_set.media_id ) == FALSE )
    { /* If no media type has been set, reject this command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_var_voc_sampling_rate_cmd_ctrl(): " \
                                            "Media type has not been set yet." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( cvs_vocoder_is_variable( session_obj->active_set.media_id ) == FALSE )
    { /* If media type is not variable sample rate vocoder, reject this command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_var_voc_sampling_rate_cmd_ctrl(): " \
                                            "Media type does not support variable sapmle rates." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_var_voc_sampling_rate_t,
                                     ctrl->packet );

    switch ( session_obj->active_set.direction )
    {
    case CVS_DIRECTION_RX:
      {
        requested_var_voc_tx_sampling_rate = session_obj->requested_var_voc_tx_sampling_rate;
        requested_var_voc_rx_sampling_rate = in_args->rx;
      }
      break;

    case CVS_DIRECTION_TX:
      {
        requested_var_voc_tx_sampling_rate = in_args->tx;
        requested_var_voc_rx_sampling_rate = session_obj->requested_var_voc_rx_sampling_rate;
      }
      break;

    case CVS_DIRECTION_RX_TX:
      {
        requested_var_voc_tx_sampling_rate = in_args->tx;
        requested_var_voc_rx_sampling_rate = in_args->rx;
      }
      break;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    /* Determine the target_set enc, dec and pp sampling rates based on the
     * media type and the client requested variable vocoder sampling rates.
     * (And validate paramters in the process.)
     */
    rc = cvs_determine_target_sampling_rates(
                session_obj->active_set.media_id,
                requested_var_voc_rx_sampling_rate,
                requested_var_voc_tx_sampling_rate,
                &target_dec_sr,
                &target_enc_sr,
                &target_rx_pp_sr,
                &target_tx_pp_sr );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    /* Cache the client requested variable vocoder sample rates.
     * The reason for caching these here is the following scenario:
     *   1. Client sets EVS.
     *   2. Client sets var voc sample rates to 16, 32.
     *   3. System runs at 16, 32.
     *   4. Client sets AMR-NB.
     *   5. System runs at 8, 8.
     *   6. Client goes back to EVS.
     *   7. Now we run at the sample rates that the client requested earlier (16, 32)
     *      instead of just going back to the defaults (8, 8).
     */
    session_obj->requested_var_voc_rx_sampling_rate = requested_var_voc_rx_sampling_rate;
    session_obj->requested_var_voc_tx_sampling_rate = requested_var_voc_tx_sampling_rate;

    if ( ( session_obj->target_set.system_config.dec_sr != target_dec_sr ) ||
         ( session_obj->target_set.system_config.enc_sr != target_enc_sr ) )
    {
      session_obj->is_stream_config_changed = TRUE;

      /* Save parameters in target set to be picked up by the
       * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command sequencer.
       */
      session_obj->target_set.system_config.dec_sr = target_dec_sr;
      session_obj->target_set.system_config.enc_sr = target_enc_sr;
      session_obj->target_set.system_config.rx_pp_sr = target_rx_pp_sr;
      session_obj->target_set.system_config.tx_pp_sr = target_tx_pp_sr;
    }

    session_obj->is_var_voc_sr_requested = TRUE;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_set_system_config_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_icommon_cmd_set_system_config_t* in_args;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  voice_set_param_v2_t set_param;
  aprv2_packet_t* packet;
  uint8_t* packet_payload = NULL;
  uint32_t payload_size = 0;
  uint32_t packet_payload_left = 0;
  vss_icommon_rsp_set_system_config_t set_system_config_rsp;
  cvs_hdvoice_enable_param_info_t hdvoice_enable_param_info;
  uint16_t client_addr;
  uint32_t disable_tty = VSM_TTY_MODE_OFF;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_CALIBRATE_COMMON, /* BACKWARD COMPATIBILITY */
    CVS_SEQUENCER_ENUM_WAIT_1, /* BACKWARD COMPATIBILITY */
    CVS_SEQUENCER_ENUM_HDVOICE_DISABLE,
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_HDVOICE_ENABLE,
    CVS_SEQUENCER_ENUM_WAIT_3,
    CVS_SEQUENCER_ENUM_CALIBRATE_STATIC,
    CVS_SEQUENCER_ENUM_WAIT_4,
    CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE,
    CVS_SEQUENCER_ENUM_WAIT_5,
    CVS_SEQUENCER_ENUM_SET_TTY_MODE,
    CVS_SEQUENCER_ENUM_WAIT_6,
    CVS_SEQUENCER_ENUM_SET_UI_PROPERTIES, /* BACKWARD COMPATIBILITY FOR WIDEVOICE */
    CVS_SEQUENCER_ENUM_WAIT_7,
    CVS_SEQUENCER_ENUM_GET_KPPS,
    CVS_SEQUENCER_ENUM_WAIT_8,
    CVS_SEQUENCER_ENUM_GET_AVSYNC_DELAYS,
    CVS_SEQUENCER_ENUM_WAIT_9,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Setting system config when the stream is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Stream is running." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_icommon_cmd_set_system_config_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_system_config_t,
                                     ctrl->packet );

    session_obj->target_set.system_config.network_id = in_args->network_id;
    session_obj->target_set.system_config.rx_voc_op_mode = in_args->rx_voc_op_mode;
    session_obj->target_set.system_config.tx_voc_op_mode = in_args->tx_voc_op_mode;
    session_obj->target_set.system_config.feature = in_args->feature;
    session_obj->target_set.system_config.vsid = in_args->vsid;
    session_obj->target_set.system_config.vfr_mode = in_args->vfr_mode;
    session_obj->target_set.system_config.call_num = in_args->call_num;

    if ( TRUE == cvs_vocoder_is_variable( session_obj->active_set.media_id ) )
    {
      /* For EVS, MVM dictates the variable vocoder sampling rate value */
      if ( VSS_MEDIA_ID_EVS == session_obj->active_set.media_id )
      {
        session_obj->target_set.system_config.dec_sr = in_args->dec_sr;
        session_obj->target_set.system_config.enc_sr = in_args->enc_sr;
        session_obj->target_set.system_config.rx_pp_sr = in_args->rx_pp_sr;
        session_obj->target_set.system_config.tx_pp_sr = in_args->tx_pp_sr;
      }
    }
    else
    {
      session_obj->target_set.system_config.rx_pp_sr = cvs_determine_target_rx_pp_sr_from_feature 
                                                        ( session_obj->target_set.system_config.feature,
                                                          session_obj->target_set.system_config.media_id );
    }

    if ( ( session_obj->active_set.system_config.rx_pp_sr !=
           session_obj->target_set.system_config.rx_pp_sr ) ||
         ( session_obj->active_set.system_config.tx_pp_sr !=
           session_obj->target_set.system_config.tx_pp_sr ) ||
         ( session_obj->active_set.system_config.network_id !=
           session_obj->target_set.system_config.network_id ) )
    { /* BACKWARD COMPATIBILITY */
      session_obj->common_cal.is_calibrate_needed =
        session_obj->common_cal.is_registered;
    }

    if ( ( session_obj->active_set.system_config.rx_pp_sr !=
           session_obj->target_set.system_config.rx_pp_sr ) ||
         ( session_obj->active_set.system_config.tx_pp_sr !=
           session_obj->target_set.system_config.tx_pp_sr ) ||
         ( session_obj->active_set.system_config.network_id !=
           session_obj->target_set.system_config.network_id ) ||
         ( session_obj->active_set.system_config.media_id !=
           session_obj->target_set.system_config.media_id ) ||
         ( session_obj->active_set.system_config.rx_voc_op_mode !=
           session_obj->target_set.system_config.rx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.tx_voc_op_mode !=
           session_obj->target_set.system_config.tx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.feature !=
           session_obj->target_set.system_config.feature ) )
    {
      session_obj->static_cal.is_calibrate_needed =
        session_obj->static_cal.is_registered;
    }

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_CALIBRATE_COMMON;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      /* Currently there are no variable sampling rate vocoders. Once there
       * are such vocoders, and the CVD client is currently using one of
       * these vocoders, we must:
       * 1. Call the voice firmware API to set the vocoder sampling rate.
       * 2. Wait for the vocoder sampling rate to be set.
       * 3. Call the voice firmware API to set the stream PP sampling rate.
       * 4. Wait for the stream PP sampling rate to be set.
       */
    case CVS_SEQUENCER_ENUM_CALIBRATE_COMMON: /* BACKWARD COMPATIBILITY */
      { /* Apply the common calibration data to VSM. */
        if ( session_obj->common_cal.is_calibrate_needed == FALSE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_HDVOICE_DISABLE;
          continue;
        }

        rc = cvs_calibrate_common( session_obj );
        if ( rc )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE;
          continue;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_1: /* BACKWARD COMPATIBILITY */
      { /* Wait for the common calibration data to be applied to VSM. */
        if ( session_obj->common_cal.set_param_rsp_cnt <
             session_obj->common_cal.num_matching_entries )
        {
          return APR_EPENDING;
        }

        if ( session_obj->common_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for common calibration is %d",
                                                  session_obj->common_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE;
        continue;
      }

    case CVS_SEQUENCER_ENUM_HDVOICE_DISABLE:
      { 
        if ( session_obj->active_set.system_config.feature == session_obj->target_set.system_config.feature )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CALIBRATE_STATIC;
          continue;
        }
        else if (session_obj->active_set.system_config.feature == VSS_ICOMMON_CAL_FEATURE_NONE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_HDVOICE_ENABLE;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        switch ( session_obj->active_set.system_config.feature )
        {
          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV;
            break;

          case VSS_ICOMMON_CAL_FEATURE_BEAMR:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_BEAMR;
            break;

          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV_V2;
            break;

          default:
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Undefined " \
                                                    "Feature ID to disable 0x%08X",
                                                    session_obj->active_set.system_config.feature );
            break;
        }

        hdvoice_enable_param_info.param_hdr.param_id = VOICE_PARAM_MOD_ENABLE;
        hdvoice_enable_param_info.param_hdr.param_size = sizeof(uint32_t);
        hdvoice_enable_param_info.param_hdr.reserved = 0;
        hdvoice_enable_param_info.enable = FALSE;
        hdvoice_enable_param_info.reserved = 0;


        set_param.payload_address_lsw = 0;
        set_param.payload_address_msw = 0;
        set_param.payload_size = sizeof(hdvoice_enable_param_info);
        set_param.mem_map_handle = 0;

        payload_size = sizeof(set_param) + sizeof(hdvoice_enable_param_info);

        rc = __aprv2_cmd_alloc_ext(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               payload_size, &packet );
        CVS_PANIC_ON_ERROR( rc );

        packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
        packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

        ( void ) mmstd_memcpy( packet_payload, packet_payload_left,
                               &set_param, sizeof (voice_set_param_v2_t) );
        packet_payload_left -= sizeof(voice_set_param_v2_t);
        ( void ) mmstd_memcpy( packet_payload + sizeof(voice_set_param_v2_t), packet_payload_left,
                               &hdvoice_enable_param_info, sizeof (cvs_hdvoice_enable_param_info_t) );

        rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the HD voice feature to be disabled by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS disabling Feature ID 0x%08X, VCP returned status 0x%08X. cvs_stream_set_system_config_cmd_ctrl()",
                                              session_obj->active_set.system_config.feature,
                                              subjob_obj->status );

        session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_HDVOICE_ENABLE;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_HDVOICE_ENABLE:
      { /* Enable/Disable HD Voice feature if feature to be enabled has changed */
        if ( session_obj->target_set.system_config.feature == VSS_ICOMMON_CAL_FEATURE_NONE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CALIBRATE_STATIC;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_3;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        switch ( session_obj->target_set.system_config.feature )
        {
          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV;
            break;

          case VSS_ICOMMON_CAL_FEATURE_BEAMR:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_BEAMR;
            break;

          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV_V2;
            break;

          default:
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Undefined " \
                                                    "Feature ID to enable 0x%08X",
                                                    session_obj->target_set.system_config.feature );
            break;
        }

        hdvoice_enable_param_info.param_hdr.param_id = VOICE_PARAM_MOD_ENABLE;
        hdvoice_enable_param_info.param_hdr.param_size = sizeof(uint32_t);
        hdvoice_enable_param_info.param_hdr.reserved = 0;
        hdvoice_enable_param_info.enable = TRUE;
        hdvoice_enable_param_info.reserved = 0;


        set_param.payload_address_lsw = 0;
        set_param.payload_address_msw = 0;
        set_param.payload_size = sizeof(hdvoice_enable_param_info);
        set_param.mem_map_handle = 0;

        payload_size = sizeof(set_param) + sizeof(hdvoice_enable_param_info);

        rc = __aprv2_cmd_alloc_ext(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               payload_size, &packet );
        CVS_PANIC_ON_ERROR( rc );

        packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
        packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

        ( void ) mmstd_memcpy( packet_payload, packet_payload_left,
                               &set_param, sizeof (voice_set_param_v2_t) );
        packet_payload_left -= sizeof(voice_set_param_v2_t);
        ( void ) mmstd_memcpy( packet_payload + sizeof(voice_set_param_v2_t), packet_payload_left,
                               &hdvoice_enable_param_info, sizeof (cvs_hdvoice_enable_param_info_t) );

        rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for the static calibration data to be processed by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS enabling Feature ID 0x%08X, VCP returned status 0x%08X. cvs_stream_set_system_config_cmd_ctrl()",
                                              session_obj->target_set.system_config.feature,
                                              subjob_obj->status );

        session_obj->active_set.system_config.feature =
          session_obj->target_set.system_config.feature;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_CALIBRATE_STATIC;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_CALIBRATE_STATIC:
      { /* Apply the static calibration data to VSM. */
        if ( session_obj->static_cal.is_calibrate_needed == FALSE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_4;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        rc = cvs_calibrate_static( session_obj, subjob_obj );
        if ( rc )
        {
          ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );

          seqjob_obj->state = CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE;
          continue;
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for the static calibration data to be processed by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE;

        ( void ) cvd_cal_query_deinit( session_obj->static_cal.query_handle );
        session_obj->static_cal.query_handle = APR_NULL_V;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_DISABLE_TTY_MODE:
      {/* Appy the TTY mode that was cached.*/
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_5;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        if ( session_obj->tty_info.is_ittyoob_registered == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
               "Disabling TTY for in-band" );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, session_obj->vsm_stream_handle,
                 subjob_obj->header.handle, VSM_CMD_SET_TTY_MODE,
                 &disable_tty,
                 sizeof ( vsm_set_tty_mode_t ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
        else
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
               "Disabling TTY for out-of-band" );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, session_obj->vsm_stream_handle,
                 subjob_obj->header.handle, VSM_CMD_OUTOFBAND_SET_TTY_MODE,
                 &disable_tty,
                 sizeof ( vsm_outofband_set_tty_mode_t ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for set tty command to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_TTY_MODE;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_SET_TTY_MODE:
      {/* Apply the TTY mode that was cached.*/
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_6;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        if ( session_obj->tty_info.is_ittyoob_registered == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
               "Setting TTY for out-of-band" );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, session_obj->vsm_stream_handle,
                 subjob_obj->header.handle, VSM_CMD_OUTOFBAND_SET_TTY_MODE,
                 &session_obj->tty_info.tty_mode,
                 sizeof ( session_obj->tty_info.tty_mode ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
        else
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
               "Setting TTY for in-band" );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, session_obj->vsm_stream_handle,
                 subjob_obj->header.handle, VSM_CMD_SET_TTY_MODE,
                 &session_obj->tty_info.tty_mode,
                 sizeof ( session_obj->tty_info.tty_mode ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_6:
      { /* Wait for set tty command to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_UI_PROPERTIES;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_SET_UI_PROPERTIES:
      { /* Set UI property. BACKWARD COMPATIBILITY FOR WIDEVOICE. */
        if ( session_obj->shared_heap.ui_prop_cache.data_len == 0 )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
               "No UI properties currently set" );
          seqjob_obj->state = CVS_SEQUENCER_ENUM_GET_KPPS;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_7;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        set_param.payload_address_lsw = ( ( uint32_t ) session_obj->shared_heap.ui_prop_cache.data );
        set_param.payload_address_msw = 0;
        set_param.payload_size = session_obj->shared_heap.ui_prop_cache.data_len;
        set_param.mem_map_handle = session_obj->shared_heap.vsm_mem_handle;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): " \
                                              "UI prop payload_address_lsw: 0x%08X, payload_address_msw: 0x%08X",
                                              set_param.payload_address_lsw,
                                              set_param.payload_address_msw );

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_system_config_cmd_ctrl(): payload_size: %d, " \
                                              "mem_map_handle: 0x%08X",
                                              set_param.payload_size,
                                              set_param.mem_map_handle );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               &set_param, sizeof( set_param ) );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_7:
      { /* Wait for set UI property to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_GET_KPPS;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_GET_KPPS:
      { /* Retrieve the VSM's KPPS requirement. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_8;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_KPPS ] = cvs_get_kpps_rsp_fn;

        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_GET_KPPS,
               NULL, 0 );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_8:
      { /* Wait for get KPPS to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_GET_AVSYNC_DELAYS;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_GET_AVSYNC_DELAYS:
      {/* Retrieve the Alogorithmic AVSync delay stats. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_9;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_simple_result_rsp_fn;
        subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_AVSYNC_DELAY ] = cvs_get_avsync_delays_rsp_fn;

        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_GET_DELAY,
               NULL, 0 );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
      }
      /*-fallthru*/

    case CVS_SEQUENCER_ENUM_WAIT_9:
      {/* Wait for get AVSYNC delays to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        session_obj->common_cal.is_calibrate_needed = FALSE; /* BACKWARD COMPATIBILITY */
        session_obj->static_cal.is_calibrate_needed = FALSE;
        session_obj->is_stream_config_changed = FALSE;

        session_obj->active_set.system_config.network_id =
          session_obj->target_set.system_config.network_id;

        session_obj->active_set.system_config.media_id =
          session_obj->target_set.system_config.media_id;

        session_obj->active_set.system_config.rx_voc_op_mode =
          session_obj->target_set.system_config.rx_voc_op_mode;

        session_obj->active_set.system_config.tx_voc_op_mode =
          session_obj->target_set.system_config.tx_voc_op_mode;

        session_obj->active_set.system_config.dec_sr =
          session_obj->target_set.system_config.dec_sr;

        session_obj->active_set.system_config.enc_sr =
          session_obj->target_set.system_config.enc_sr;

        session_obj->active_set.system_config.rx_pp_sr =
          session_obj->target_set.system_config.rx_pp_sr;

        session_obj->active_set.system_config.tx_pp_sr =
          session_obj->target_set.system_config.tx_pp_sr;

        session_obj->active_set.system_config.feature =
          session_obj->target_set.system_config.feature;

        set_system_config_rsp.enc_kpps = session_obj->kpps_info.enc;
        set_system_config_rsp.dec_kpps = session_obj->kpps_info.dec;
        set_system_config_rsp.dec_pp_kpps = session_obj->kpps_info.dec_pp;
        set_system_config_rsp.enc_sr = session_obj->active_set.system_config.enc_sr;
        set_system_config_rsp.dec_sr = session_obj->active_set.system_config.dec_sr;
        set_system_config_rsp.stream_media_id = session_obj->active_set.media_id;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
               ctrl->packet->dst_addr, ctrl->packet->dst_port,
               ctrl->packet->src_addr, ctrl->packet->src_port,
               ctrl->packet->token, VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG,
               &set_system_config_rsp, sizeof( set_system_config_rsp ) );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvs_vpcm_start_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_ivpcm_cmd_start_v2_t* payload;
  vss_ivpcm_tap_point_t* tap_points;
  voice_tap_point_v2_t dst_tap_point;
  uint32_t num_tap_points;
  uint32_t payload_len;
  uint32_t expected_payload_len;
  uint32_t tap_point_index;
  uint32_t target_payload_len;
  aprv2_packet_t* dst_packet;
  uint8_t* dst_payload;
  uint32_t dst_payload_left;
  uint32_t cvd_mem_handle;
  uint32_t vsm_mem_handle;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Only one START command is allowed. */
      if ( session_obj->vpcm_info.is_enabled == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): VPCM already started" );
        rc = APR_EALREADY;
        break;
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_cmd_start_v2_t, ctrl->packet );
      payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_len < sizeof( vss_ivpcm_cmd_start_v2_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Unexpected payload size, %d < %d",
                                                payload_len,
                                                sizeof( vss_ivpcm_cmd_start_v2_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      num_tap_points = payload->num_tap_points;

      if ( num_tap_points == 0 )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Invalid num tap points(0)" );
        rc = APR_EBADPARAM;
        break;
      }

      /* expected_payload_len = ( sizeof( mem_handle ) + sizeof( num_tap_points ) +
                                  ( num_tap_points * sizeof( each tap point ) ) ). */
      expected_payload_len = ( sizeof( uint32_t ) + sizeof( uint32_t ) +
                               ( num_tap_points * sizeof( vss_ivpcm_tap_point_t ) ) );

      if ( payload_len != expected_payload_len )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Invalid data. Payload len: %d, expected len: %d",
                                                payload_len,
                                                expected_payload_len );
        rc = APR_EBADPARAM;
        break;
      }

      cvd_mem_handle = payload->mem_handle;

      rc = cvd_mem_mapper_validate_handle( cvd_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X.",
                                                cvd_mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      ( void ) cvd_mem_mapper_get_vsm_mem_handle( cvd_mem_handle, &vsm_mem_handle );

      tap_points = ( ( vss_ivpcm_tap_point_t* )
                      ( ( ( uint8_t* ) payload ) + sizeof( vss_ivpcm_cmd_start_v2_t ) ) );

      /* target_payload_len = ( sizeof( num_tap_points ) +
                                ( num_tap_points * sizeof( each tap point ) ) ). */
      target_payload_len = ( sizeof( uint32_t ) +
                             ( num_tap_points * sizeof( voice_tap_point_v2_t ) ) );

      rc = cvs_create_simple_job_object(
             session_obj->header.handle,
             ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_ext(
            cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
            cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
            cvs_vsm_addr, session_obj->vsm_stream_handle,
            ctrl->pendjob_obj->header.handle, VOICE_CMD_START_HOST_PCM_V2,
            target_payload_len, &dst_packet );
      CVS_PANIC_ON_ERROR( rc );

      dst_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, dst_packet );
      dst_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( dst_packet->header );

      /* Copy number of tap points. */
      ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &num_tap_points, sizeof( num_tap_points ) );
      dst_payload_left -= sizeof( num_tap_points );
      dst_payload += sizeof( num_tap_points );

      for ( tap_point_index = 0; tap_point_index < num_tap_points; ++tap_point_index )
      {
        switch( tap_points->tap_point )
        {
        case VSS_IVPCM_TAP_POINT_TX_DEFAULT:
          dst_tap_point.tap_point = VOICESTREAM_MODULE_TX;
          break;

        case VSS_IVPCM_TAP_POINT_RX_DEFAULT:
          dst_tap_point.tap_point = VOICESTREAM_MODULE_RX;
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Invalid tap point: 0x%8X",
                                                  tap_points->tap_point );
          /* Free the allocated packet. */
          ( void ) __aprv2_cmd_free( cvs_apr_handle, dst_packet );
          rc = APR_EBADPARAM;
          break;
        }

        if ( rc ) break;

        switch( tap_points->direction )
        {
        case VSS_IVPCM_TAP_POINT_DIR_OUT:
          dst_tap_point.direction = VOICE_HOST_PCM_READ;
          break;

        case VSS_IVPCM_TAP_POINT_DIR_IN:
          dst_tap_point.direction = VOICE_HOST_PCM_WRITE;
          break;

        case VSS_IVPCM_TAP_POINT_DIR_OUT_IN:
          dst_tap_point.direction = ( VOICE_HOST_PCM_READ | VOICE_HOST_PCM_WRITE );
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_start_v2_cmd_ctrl(): Invalid dir: %d",
                                                  tap_points->direction );
          /* Free the allocated packet. */
          ( void ) __aprv2_cmd_free( cvs_apr_handle, dst_packet );
          rc = APR_EBADPARAM;
          break;
        }

        if ( rc ) break;

        dst_tap_point.sampling_rate = tap_points->sampling_rate;
        dst_tap_point.duration_ms = tap_points->duration;
        dst_tap_point.reserved = 0;
        dst_tap_point.mem_map_handle = vsm_mem_handle;

        /* Copy each tap point structure to destination payload. */
        ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &dst_tap_point, sizeof( dst_tap_point ) );
        dst_payload_left -= sizeof( dst_tap_point );
        dst_payload += sizeof( dst_tap_point );
        /* Advance to the next tap point. */
        tap_points += 1;
      }

      if ( rc ) break;

      rc = __aprv2_cmd_forward( cvs_apr_handle, dst_packet );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
    else
    if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
    {
      uint32_t status = ctrl->pendjob_obj->simple_job.status ;

      if ( status == APR_EOK )
      { /* Store the addr and handle of the client on start command success. */
        /* Write access to vpcm_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->vpcm_info.client_addr = ctrl->packet->src_addr;
        session_obj->vpcm_info.client_handle = ctrl->packet->src_port;
        session_obj->vpcm_info.cvs_handle = ctrl->packet->dst_port;
        session_obj->vpcm_info.is_enabled = TRUE;
        payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_cmd_start_v2_t, ctrl->packet );
        session_obj->vpcm_info.mem_handle = payload->mem_handle;
        ( void ) apr_lock_leave( session_obj->lock );
      }

      ( void ) cvs_free_object( ( ( cvs_object_t* ) ctrl->pendjob_obj ) );

      rc = status;
      break;
    }

    return APR_EPENDING;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_vpcm_stop_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Only one STOP command is allowed. */
      if ( session_obj->vpcm_info.is_enabled == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_vpcm_stop_cmd_ctrl(): VPCM not started" );
        rc = APR_EALREADY;
        break;
      }

      rc = cvs_create_simple_job_object(
             session_obj->header.handle,
             ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVS_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             ctrl->pendjob_obj->header.handle, VOICE_CMD_STOP_HOST_PCM,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }
    else
    if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
    {
      uint32_t status = ctrl->pendjob_obj->simple_job.status;

      if ( status == APR_EOK )
      {
        /* Write access to vpcm_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->vpcm_info.is_enabled = FALSE;
        session_obj->vpcm_info.client_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->vpcm_info.client_handle = APR_NULL_V;
        session_obj->vpcm_info.cvs_handle = APR_NULL_V;
        session_obj->vpcm_info.mem_handle = 0;
        ( void ) apr_lock_leave( session_obj->lock );
      }

      ( void ) cvs_free_object( ( ( cvs_object_t* ) ctrl->pendjob_obj ) );

      rc = status;
      break;
    }

    return APR_EPENDING;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_pktexg_set_mode_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_ipktexg_cmd_set_mode_t* payload;
  vsm_set_pkt_exchange_mode_t set_pkt_exchange_mode;
  voice_cmd_shared_mem_unmap_regions_t voice_unmap_memory;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_CLEAR_OOB_PKTEXG_CONFIG,
      /**< 
        * Clear the CVS OOB packet exchange config if the packet exchange mode
        * was VSS_IPKTEXG_MODE_OUT_OF_BAND and the client is setting the packet
        * exchange mode to something other than VSS_IPKTEXG_MODE_OUT_OF_BAND.
        */
    CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_PKTEXG_CONFIG,
      /**< 
        * Clear the CVS mailbox packet exchange config if the packet exchange 
        * mode was VSS_IPKTEXG_MODE_MAILBOX and the client is setting the 
        * packet exchange mode to something other than
        * VSS_IPKTEXG_MODE_MAILBOX.
        */
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ipktexg_cmd_set_mode_t ) );
    if ( rc ) return APR_EOK;

    if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
    {
      return APR_EOK;
    }

    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Stream must not be running when setting the packet exchange mode. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_set_mode_cmd_ctrl(): Client error: Stream is running." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_cmd_set_mode_t,
                                     ctrl->packet );

    if ( ( payload->mode != VSS_IPKTEXG_MODE_IN_BAND ) &&
         ( payload->mode != VSS_IPKTEXG_MODE_OUT_OF_BAND ) && 
         ( payload->mode != VSS_IPKTEXG_MODE_MAILBOX ) )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_set_mode_cmd_ctrl(): " \
             "Client error: Invalid packet exchange mode 0x%08X.", payload->mode );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.set_pktexg_mode.mode = payload->mode;

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVS_SEQUENCER_ENUM_SET_PKTEXG_MODE:
      { /* Sets the packet exchange mode on VSM. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;

        if ( seqjob_obj->use_case.set_pktexg_mode.mode == VSS_IPKTEXG_MODE_IN_BAND )
        {
          set_pkt_exchange_mode.pkt_exchange_mode = VSS_IPKTEXG_MODE_IN_BAND;
        }
        else
        {
          /* The VSM packet exchange mode will be set to OOB if CVS's packet
           * exchange mode is either OOB or mailbox.
           */
          set_pkt_exchange_mode.pkt_exchange_mode = VSS_IPKTEXG_MODE_OUT_OF_BAND;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_set_mode_cmd_ctrl(): " \
               "CVS mode is 0x%08X, VSM mode is 0x%08X.",
               seqjob_obj->use_case.set_pktexg_mode.mode,
               set_pkt_exchange_mode.pkt_exchange_mode );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_PKT_EXCHANGE_MODE,
               &set_pkt_exchange_mode, sizeof( set_pkt_exchange_mode ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for set packet exchange mode on VSM to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );

        if ( seqjob_obj->status != APR_EOK )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAR_OOB_PKTEXG_CONFIG;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_CLEAR_OOB_PKTEXG_CONFIG:
      {
        seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_PKTEXG_CONFIG;

        /* Clear the CVS OOB packet exchange config if the packet exchange mode
         * was VSS_IPKTEXG_MODE_OUT_OF_BAND and the client is setting the 
         * packet exchange mode to something other than 
         * VSS_IPKTEXG_MODE_OUT_OF_BAND.
         */
        if ( ( session_obj->packet_exchange_info.mode == 
               VSS_IPKTEXG_MODE_OUT_OF_BAND ) &&
             ( seqjob_obj->use_case.set_pktexg_mode.mode != 
               VSS_IPKTEXG_MODE_OUT_OF_BAND ) )
        {
          /* Write access to packet_exchange_info.oob_info. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->packet_exchange_info.oob_info.is_configured = FALSE;
          session_obj->packet_exchange_info.oob_info.config.mem_handle = 0;
          session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw = 0;
          session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw = 0;
          session_obj->packet_exchange_info.oob_info.config.enc_buf_size = 0;
          session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw = 0;
          session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_msw = 0;
          session_obj->packet_exchange_info.oob_info.config.dec_buf_size = 0;
          ( void ) apr_lock_leave( session_obj->lock );
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_CLEAR_MAILBOX_PKTEXG_CONFIG:
      {
        /* Clear the CVS mailbox packet exchange config if the packet exchange 
         * mode was VSS_IPKTEXG_MODE_MAILBOX and the client is setting the 
         * packet exchange mode to something other than 
         * VSS_IPKTEXG_MODE_MAILBOX.
         */
        if ( ( session_obj->packet_exchange_info.mode ==
               VSS_IPKTEXG_MODE_MAILBOX ) &&
             ( seqjob_obj->use_case.set_pktexg_mode.mode !=
               VSS_IPKTEXG_MODE_MAILBOX ) &&
             ( session_obj->packet_exchange_info.mailbox_info.is_configured ==
               TRUE ) )
        { /* Unmap the CVS heap memory used for OOB packet exchange with VSM. */
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;

          rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
          CVS_PANIC_ON_ERROR( rc );
          seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

          voice_unmap_memory.mem_map_handle = 
            session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle;

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_set_mode_cmd_ctrl(): " \
                 "Unmap memory with VSM, mem_handle: 0x%08X.",
                 session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, APR_NULL_V,
                 subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
                 &voice_unmap_memory, sizeof( voice_unmap_memory ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for memory unmap with VSM to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        if ( seqjob_obj->status == APR_EOK )
        {
          /* Read/write access to packet_exchange_info.mailbox_info. */
          ( void ) apr_lock_enter( session_obj->lock );

          /* Free the CVS heap memory used for OOB packet exchange with VSM and
           * clear the mailbox configurations.
           */
          apr_memmgr_free(
            &cvs_heapmgr,
            session_obj->packet_exchange_info.mailbox_info.config.oob_enc_buf );

          session_obj->packet_exchange_info.mailbox_info.is_configured = FALSE;
          session_obj->packet_exchange_info.mailbox_info.config.cvs_mem_handle = 0;
          session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf = NULL;
          session_obj->packet_exchange_info.mailbox_info.config.tx_circ_buf_mem_size = 0;
          session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf = NULL;
          session_obj->packet_exchange_info.mailbox_info.config.rx_circ_buf_mem_size = 0;
          session_obj->packet_exchange_info.mailbox_info.config.vsm_mem_handle = 0;
          session_obj->packet_exchange_info.mailbox_info.config.max_enc_pkt_size = 0;
          session_obj->packet_exchange_info.mailbox_info.config.oob_enc_buf = NULL;
          session_obj->packet_exchange_info.mailbox_info.config.oob_dec_buf = NULL;
          session_obj->packet_exchange_info.mailbox_info.config.tx_ref_timestamp_us = 0;
          session_obj->packet_exchange_info.mailbox_info.config.rx_ref_timestamp_us = 0;
          session_obj->packet_exchange_info.mailbox_info.config.request_to_start_timestamp_us = 0;
          session_obj->packet_exchange_info.mailbox_info.config.rx_req_and_dequeue_time_diff_us = 
            CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
          session_obj->packet_exchange_info.mailbox_info.config.is_start_requested = FALSE;
          session_obj->packet_exchange_info.mailbox_info.config.is_reseted = FALSE;
          session_obj->packet_exchange_info.mailbox_info.config.is_time_ref_received = FALSE;
          session_obj->packet_exchange_info.mailbox_info.config.is_rx_expiry_proc_disabled = FALSE;
          session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timestamp_us = 0;
          if ( session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle != APR_NULL_V )
          {
            ( void ) cvs_mailbox_timer_destroy(
                       session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle );
            session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
          }

          ( void )mmstd_memset(
                    &session_obj->packet_exchange_info.mailbox_info.stats, 0,
                    sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) );

          ( void ) apr_lock_leave( session_obj->lock );
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        if ( seqjob_obj->status == APR_EOK )
        {
          /* Write access to packet_exchange_info.mode. */
          ( void ) apr_lock_enter( session_obj->lock );
          session_obj->packet_exchange_info.mode =
            seqjob_obj->use_case.set_pktexg_mode.mode;
          ( void ) apr_lock_leave( session_obj->lock );
        }

        rc = __aprv2_cmd_end_command(
               cvs_apr_handle, ctrl->packet, seqjob_obj->status );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvs_pktexg_oob_set_config_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  vss_ipktexg_cmd_oob_set_config_t* payload;
  uint64_t enc_buf_virt_addr;
  uint64_t dec_buf_virt_addr;
  uint32_t vsm_mem_handle;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      rc = cvs_helper_validate_payload_size_control(
             ctrl, sizeof( vss_ipktexg_cmd_oob_set_config_t ) );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Invalid payload size." );
        break;
      }

      if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Client is passive." );
        break;
      }

      if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
      { /* Stream must not be running when setting the oob packet exchange config. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Stream is running." );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      if ( session_obj->packet_exchange_info.mode != VSS_ISTREAM_PACKET_EXCHANGE_MODE_OUT_OF_BAND )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Packet exchange mode is not OOB." );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_cmd_oob_set_config_t,
                                       ctrl->packet );

      if ( ( payload->enc_buf_size < CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE ) ||
           ( payload->dec_buf_size < CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Invalid enc (%d) or dec (%d) buffer size.",
                                                payload->enc_buf_size,
                                                payload->dec_buf_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Invalid mem_handle: 0x%08X.",
                                                payload->mem_handle );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->enc_buf_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Mis-aligned enc_buf_addr: 0x%016X.",
                                                payload->enc_buf_addr );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->dec_buf_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Mis-aligned dec_buf_addr: 0x%016X.",
                                                payload->dec_buf_addr );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->enc_buf_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Mis-aligned enc_buf_size: %d.",
                                                payload->enc_buf_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->dec_buf_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Mis-aligned dec_buf_size: %d.",
                                                payload->dec_buf_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->enc_buf_addr,
                                                     payload->enc_buf_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Memory is not within range, mem_handle: 0x%08X, enc_buf_addr: 0x%016X, enc_buf_size: %d.",
                                                payload->mem_handle,
                                                payload->enc_buf_addr,
                                                payload->enc_buf_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->dec_buf_addr,
                                                     payload->dec_buf_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Memory is not within range, mem_handle: 0x%08X, dec_buf_addr: 0x%016X, dec_buf_size: %d.",
                                                payload->mem_handle,
                                                payload->dec_buf_addr,
                                                payload->dec_buf_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( payload->mem_handle,
                                            payload->enc_buf_addr,
                                            &enc_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Cannot get virtual address, mem_handle: 0x%08X, enc_buf_addr: 0x%016X.",
                                                payload->mem_handle,
                                                payload->enc_buf_addr );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( payload->mem_handle,
                                            payload->dec_buf_addr,
                                            &dec_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Cannot get virtual address, mem_handle: 0x%08X, dec_buf_addr: 0x%016X.",
                                                payload->mem_handle,
                                                payload->dec_buf_addr );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_get_vsm_mem_handle( payload->mem_handle, &vsm_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_oob_set_config_cmd_ctrl(): Cannot get vsm_mem_handle for cvd_mem_handle: 0x%08X.",
                                                payload->mem_handle );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvs_create_simple_job_object(
             session_obj->header.handle,
             ( ( cvs_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVS_PANIC_ON_ERROR( rc );

      /* Write access to packet_exchange_info.oob_info.*/
      ( void ) apr_lock_enter( session_obj->lock );
      session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw = ( ( uint32_t ) enc_buf_virt_addr );;
      session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw = ( ( uint32_t ) ( enc_buf_virt_addr >> 32 ) );
      session_obj->packet_exchange_info.oob_info.config.enc_buf_size = payload->enc_buf_size;
      session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw = ( ( uint32_t ) dec_buf_virt_addr );
      session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_msw = ( ( uint32_t ) ( dec_buf_virt_addr >> 32 ) );
      session_obj->packet_exchange_info.oob_info.config.dec_buf_size = payload->dec_buf_size;
      session_obj->packet_exchange_info.oob_info.config.mem_handle = vsm_mem_handle;
      session_obj->packet_exchange_info.oob_info.is_configured = TRUE;
      ( void ) apr_lock_leave( session_obj->lock );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_oob_set_config_cmd_ctrl(): enc_buf_addr_lsw: 0x%08X, enc_buf_addr_msw: 0x%08X, enc_buf_size: 0x%08X",
                                            session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_lsw,
                                            session_obj->packet_exchange_info.oob_info.config.enc_buf_addr_msw,
                                            session_obj->packet_exchange_info.oob_info.config.enc_buf_size );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_oob_set_config_cmd_ctrl(): dec_buf_addr_lsw: 0x%08X, dec_buf_addr_msw: 0x%08X, dec_buf_size: 0x%08X",
                                            session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_lsw,
                                            session_obj->packet_exchange_info.oob_info.config.dec_buf_addr_msw,
                                            session_obj->packet_exchange_info.oob_info.config.dec_buf_size );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_oob_set_config_cmd_ctrl(): vsm_mem_handle: 0x%08X.",
                                            vsm_mem_handle );

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             ctrl->pendjob_obj->header.handle, VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG,
             &session_obj->packet_exchange_info.oob_info.config,
             sizeof( session_obj->packet_exchange_info.oob_info.config ) );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );
    }

    if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
    {
      uint32_t sts = ctrl->pendjob_obj->simple_job.status;
      ( void ) cvs_free_object( ctrl->pendjob_obj );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, sts );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    return APR_EPENDING;
  }

  return APR_EOK;
}

static int32_t cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_eval_cal_indexing_mem_size_t* payload;
  uint32_t payload_len;
  cvd_virt_addr_t cal_virt_addr;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  vss_istream_rsp_eval_cal_indexing_mem_size_t eval_cal_indexing_rsp;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    payload = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_eval_cal_indexing_mem_size_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_istream_cmd_eval_cal_indexing_mem_size_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Unexpected payload size, %d !< %d.",
                                              payload_len,
                                              sizeof( vss_istream_cmd_eval_cal_indexing_mem_size_t ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Invalid mem_handle: 0x%08X.",
                                              payload->mem_handle );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Mis-aligned mem_address: 0x%016X.",
                                              payload->mem_address );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                   payload->mem_size );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Memory is not within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d.",
                                              payload->mem_handle,
                                              payload->mem_address,
                                              payload->mem_size );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle,
                                          payload->mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Cannot get virtual address, mem_handle: 0x%08X, addr: 0x%016X.",
                                              payload->mem_handle,
                                              payload->mem_address );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->mem_size;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_istream_cmd_eval_cal_indexing_mem_size_t ) ) );

    rc = cvd_cal_eval_indexing_mem_size( &cal_table_descriptor,
                                         &session_obj->common_cal.required_index_mem_size );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl(): Error in evaluating the cal table." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->common_cal.is_evaluated = TRUE;

    eval_cal_indexing_rsp.mem_size = session_obj->common_cal.required_index_mem_size;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvs_my_addr, ctrl->packet->dst_port,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_ISTREAM_RSP_EVAL_CAL_INDEXING_MEM_SIZE,
           &eval_cal_indexing_rsp, sizeof( eval_cal_indexing_rsp ) );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvs_stream_register_calibration_data_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_register_calibration_data_v2_t* payload;
  uint32_t payload_len;
  cvd_virt_addr_t cal_virt_addr;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Calibration already registered." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Calibration already registered via static cal registration command." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_register_calibration_data_v2_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_istream_cmd_register_calibration_data_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Unexpected payload size, %d !< %d.",
                                              payload_len,
                                              sizeof( vss_istream_cmd_register_calibration_data_v2_t ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X.",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                          payload->cal_mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    ( void ) cvd_mem_mapper_get_vsm_mem_handle( payload->cal_mem_handle,
                                                &session_obj->common_cal.vsm_mem_handle );

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_istream_cmd_register_calibration_data_v2_t ) ) );

    rc = cvd_cal_parse_table( 0, 0, &cal_table_descriptor,
                              &session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v2_cmd_ctrl(): Error in parsing the cal table." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->common_cal.is_calibrate_needed = TRUE;
    session_obj->common_cal.is_registered = TRUE;

    session_obj->is_stream_config_changed = TRUE;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Stream Calibration table successfully. cvs_stream_register_calibration_data_v2_cmd_ctrl(): Table size reported: %d",
                                          cal_table_descriptor.size );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_register_calibration_data_v3_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_register_calibration_data_v3_t* payload;
  uint32_t payload_len;
  cvd_virt_addr_t cal_virt_addr;
  cvd_virt_addr_t index_mem_virt_addr;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Calibration already registered." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Calibration already registered via static cal registration command." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_register_calibration_data_v3_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_istream_cmd_register_calibration_data_v3_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Unexpected payload size, %d !< %d.",
                                              payload_len,
                                              sizeof( vss_istream_cmd_register_calibration_data_v3_t ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( payload->cal_indexing_mem_handle != 0 )
    {
      if ( session_obj->common_cal.is_evaluated == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Client has not evaluated the indexing mem size. " );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      if ( payload->cal_indexing_mem_size < session_obj->common_cal.required_index_mem_size )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Unexpected cal index mem size %d < %d. ",
                                                payload->cal_indexing_mem_size,
                                                session_obj->common_cal.required_index_mem_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_handle( payload->cal_indexing_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Invalid cal_indexing_mem_handle: 0x%08X.",
                                                payload->cal_indexing_mem_handle );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_indexing_mem_address: 0x%016X.",
                                                payload->cal_indexing_mem_address );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_indexing_mem_size: %d.",
                                                payload->cal_indexing_mem_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address,
                                                     payload->cal_indexing_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Memory is not within range, cal_indexing_mem_handle: 0x%08X, cal_indexing_mem_address: 0x%016X, size: %d.",
                                                payload->cal_indexing_mem_handle,
                                                payload->cal_indexing_mem_address,
                                                payload->cal_indexing_mem_size );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_indexing_mem_handle,
                                            payload->cal_indexing_mem_address,
                                            &index_mem_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Cannot get virtual address, cal_indexing_mem_handle: 0x%08X, cal_indexing_mem_address: 0x%016X.",
                                                payload->cal_indexing_mem_handle,
                                                payload->cal_indexing_mem_address );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );
        break;
      }
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X.",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                          payload->cal_mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    ( void ) cvd_mem_mapper_get_vsm_mem_handle( payload->cal_mem_handle,
                                                &session_obj->common_cal.vsm_mem_handle );

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = payload->cal_indexing_mem_handle;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_istream_cmd_register_calibration_data_v2_t ) ) );

    rc = cvd_cal_parse_table( index_mem_virt_addr.ptr, payload->cal_indexing_mem_size,
                              &cal_table_descriptor,
                              &session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_calibration_data_v3_cmd_ctrl(): Error in parsing the cal table." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->common_cal.is_calibrate_needed = TRUE;
    session_obj->common_cal.is_registered = TRUE;

    session_obj->is_stream_config_changed = TRUE;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Stream Calibration table successfully. cvs_stream_register_calibration_data_v3_cmd_ctrl(): Table size reported: %d",
                                          cal_table_descriptor.size );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvs_stream_deregister_calibration_data_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_deregister_calibration_data_cmd_ctrl(): Invalid session handle: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_deregister_calibration_data_cmd_ctrl(): Calibration not registered." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table( session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_deregister_calibration_data_cmd_ctrl(): Failed to deregister calibration data." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }
    else
    {
      session_obj->common_cal.is_registered = FALSE;
      session_obj->common_cal.is_evaluated = FALSE;
      session_obj->common_cal.is_calibrate_needed = FALSE;
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_register_static_calibration_data_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_register_static_calibration_data_t* payload;
  uint32_t payload_len;
  cvd_virt_addr_t cal_virt_addr;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  uint16_t client_addr;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_table_header_t log_info_table;

  bool_t is_mapped_memory_in_range = FALSE;
  /* Flag to indicate if memory range of mem_map command
   * and cal registration are in sync
   */
  
  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_static_calibration_data_cmd_ctrl(): Calibration data already registered." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_static_calibration_data_cmd_ctrl(): Calibration data already registered via deprecated registration command." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_register_static_calibration_data_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_istream_cmd_register_static_calibration_data_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_static_calibration_data_cmd_ctrl(): Unexpected payload size, %d < %d.",
                                              payload_len,
                                              sizeof( vss_istream_cmd_register_static_calibration_data_t ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_static_calibration_data_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X.",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_static_calibration_data_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_static_calibration_data_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                             payload->cal_mem_address,
                                             &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvs_stream_register_static_calibration_data_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    is_mapped_memory_in_range = TRUE;

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = ( uint32_t ) NULL;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_istream_cmd_register_static_calibration_data_t ) ) );

    rc = cvd_cal_parse_table_v2( &cal_table_descriptor,
                                 &session_obj->static_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_register_static_calibration_data_cmd_ctrl(): Error in parsing the cal table." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->static_cal.is_calibrate_needed = TRUE;
    session_obj->static_cal.is_registered = TRUE;

    session_obj->is_stream_config_changed = TRUE;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  /* Log stream static cal table. */
  if (is_mapped_memory_in_range == TRUE)
  {
    log_info_table.table_handle = session_obj->static_cal.table_handle;

    log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) | 
                          (  session_obj->header.handle ) );
    log_info.call_num = session_obj->target_set.system_config.call_num;
    log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_TABLE;
    log_info.data_container_header_size = sizeof ( log_info_table );
    log_info.data_container_header = &log_info_table;
    log_info.payload_size = cal_table_descriptor.size;
    log_info.payload_buf = cal_table_descriptor.start_ptr;

    ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_STREAM_STATIC_TABLE,
                              ( void* )&log_info, sizeof( log_info ) );
  }

  return APR_EOK;
}

static int32_t cvs_stream_deregister_static_calibration_data_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->static_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_deregister_static_calibration_data_cmd_ctrl(): Calibration not registered." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table_v2( session_obj->static_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_deregister_static_calibration_data_cmd_ctrl(): Failed to deregister calibration data." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }
    else
    {
      session_obj->static_cal.is_registered = FALSE;
      session_obj->static_cal.is_calibrate_needed = FALSE;
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvs_set_param_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc = APR_EOK;
  cvs_session_object_t* session_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_icommon_cmd_set_param_v2_t* payload;
  uint32_t payload_size;
  uint32_t expected_payload_size;
  uint8_t* param_data = NULL;
  cvd_virt_addr_t param_data_virt_addr;
  voice_set_param_v2_t set_param;
  aprv2_packet_t* dst_packet;
  uint8_t* dst_payload;
  uint32_t dst_payload_size;
  uint32_t dst_payload_left;
  uint32_t vsm_mem_handle;
  uint16_t client_addr;
  uint32_t param_id;
  uint16_t param_size;
  uint8_t* param_payload_data;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_GET_KPPS,
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_RECONFIG,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };


  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_param_v2_t, ctrl->packet );
      payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_size < sizeof( vss_icommon_cmd_set_param_v2_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_param_v2_cmd_ctrl(): Unexpected payload size, %d < %d",
                                                payload_size,
                                                sizeof( vss_icommon_cmd_set_param_v2_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvs_verify_param_data_size( payload->mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_param_v2_cmd_ctrl(): Invalid param data size, %d",
                                                payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      dst_payload_size = sizeof( voice_set_param_v2_t );

      if ( payload->mem_handle == 0 )
      { /* Parameter data is in-band. */
        expected_payload_size = ( sizeof( vss_icommon_cmd_set_param_v2_t ) + payload->mem_size );
        if ( payload_size != expected_payload_size )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_param_v2_cmd_ctrl(): Unexpected payload size, %d != %d",
                                                  payload_size,
                                                  expected_payload_size );
          rc = APR_EBADPARAM;
          break;
        }

        param_data = ( ( uint8_t* ) payload );
        param_data += sizeof( vss_icommon_cmd_set_param_v2_t );
        vsm_mem_handle = 0;
        dst_payload_size += payload->mem_size;

        param_id = *( uint32_t* ) ( param_data +  sizeof( uint32_t ) );
        param_size = *( uint32_t* ) ( param_data + ( 2 * ( sizeof( uint32_t ) ) ) );
        param_payload_data = ( param_data + ( 3 * ( sizeof( uint32_t ) ) ) );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Param ID[0]=0x%08x",
                                              param_id );

        if ( param_size >= 16 )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[0-2]=0x%08x, " \
                                                "0x%08x, 0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ), 
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ), 
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[3]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
        }
        else if ( param_size >= 4 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[0]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ) );
        }
      }
      else
      { /* Parameter data is out-of-band. */
        rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_param_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X.",
                                                  payload->mem_handle );
          rc = APR_EHANDLE;
          break;
        }

        ( void ) cvd_mem_mapper_get_vsm_mem_handle( payload->mem_handle, &vsm_mem_handle );

        rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvs_set_param_v2_cmd_ctrl(): Mis-aligned mem address:"
            " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
            ( uint32_t )( payload->mem_address >> 32 ) );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                       payload->mem_size );
        if ( rc )
        {
          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvs_set_param_v2_cmd_ctrl(): Memory is not within range,"
            " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
            payload->mem_handle, ( uint32_t )payload->mem_address,
            ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle, payload->mem_address,
                                                 &param_data_virt_addr );
        if ( rc )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvs_set_param_v2_cmd_ctrl(): Cannot get virtual address,"
            " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
            ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
          rc = APR_EFAILED;
          break;
        }

        param_id = *( uint32_t* ) ( ( uint8_t* ) param_data_virt_addr.ptr +  sizeof( uint32_t ) );
        param_size = *( uint32_t* ) ( ( uint8_t* ) param_data_virt_addr.ptr + ( 2 * ( sizeof( uint32_t ) ) ) );
        param_payload_data = ( ( uint8_t* ) param_data_virt_addr.ptr + ( 3 * ( sizeof( uint32_t ) ) ) );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Param ID[0]=0x%08x",
                                              param_id );

        if ( param_size >= 16 )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[0-2]=0x%08x, " \
                                                "0x%08x, 0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ), 
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ), 
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[3]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
        }
        else if ( param_size >= 4 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvs_set_param_v2_cmd_ctrl(): Payload[0]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ) );
        }
      }

 
      rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
      CVS_PANIC_ON_ERROR( rc );

      ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_WAIT_1,

      rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
      CVS_PANIC_ON_ERROR( rc );
    
      seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
      seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

      cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                              param_data_virt_addr.ptr );

      set_param.payload_size = payload->mem_size;
      set_param.mem_map_handle = vsm_mem_handle;

      rc = __aprv2_cmd_alloc_ext(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_vsm_addr, session_obj->vsm_stream_handle,
             subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             dst_payload_size, &dst_packet );
      CVS_PANIC_ON_ERROR( rc );

      dst_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, dst_packet );
      dst_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( dst_packet->header );

      ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &set_param, sizeof( set_param ) );
      dst_payload_left -= sizeof( set_param );

      if ( param_data != NULL )
      {
        ( void ) mmstd_memcpy(
                   ( dst_payload + sizeof( set_param ) ), dst_payload_left,
                   param_data, payload->mem_size );
      }

      rc = __aprv2_cmd_forward( cvs_apr_handle, dst_packet );
      CVS_COMM_ERROR( rc, cvs_vsm_addr );

      return APR_EPENDING;
   }

   else
   {
     seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
     subjob_obj = &seqjob_obj->subjob_obj->simple_job;

      for ( ;; )
      {
        switch ( seqjob_obj->state )
        {
          case CVS_SEQUENCER_ENUM_WAIT_1:
           {
              if ( subjob_obj->is_completed == FALSE )
              {
                return APR_EPENDING;
              }
              else
              {
                ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
                if ( ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN ) &&
                     ( session_obj->attached_mvm_handle != APR_NULL_V ) )
                {
                  ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_GET_KPPS;
                }
                else
                {
                  ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_COMPLETE;
                  continue;
                }
              }
            }
            /*-fallthru */

          case CVS_SEQUENCER_ENUM_GET_KPPS:
            { /* Retrieve the VSM's KPPS requirement. */
              seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;

              rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              CVS_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_KPPS ] = cvs_get_kpps_rsp_fn;

              seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

              rc = __aprv2_cmd_alloc_send(
                   cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                   session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
                   subjob_obj->header.handle, VSM_CMD_GET_KPPS,
                   NULL, 0 );
              CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
            }
            /*-fallthru */

          case CVS_SEQUENCER_ENUM_WAIT_2:
            { /* Wait for get KPPS to complete. */
              subjob_obj = &seqjob_obj->subjob_obj->simple_job;

              if ( subjob_obj->is_completed == FALSE )
              {
                return APR_EPENDING;
              }

              if ( session_obj->is_kpps_changed == TRUE )
              {
                seqjob_obj->state = CVS_SEQUENCER_ENUM_RECONFIG;
                ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
              }
              else
              {
                seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
                ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
                continue;
              }
            } /* fallthru */

          case CVS_SEQUENCER_ENUM_RECONFIG:
            {
              seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;

              rc = __aprv2_cmd_alloc_send(
                     cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                     cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                     cvs_mvm_addr, session_obj->attached_mvm_handle,
                     0, VSS_ISTREAM_EVT_RECONFIG,
                     NULL, 0 );
              CVS_COMM_ERROR( rc, cvs_mvm_addr );

              session_obj->is_kpps_changed = FALSE;
            }
            /* fallthru */

          case CVS_SEQUENCER_ENUM_COMPLETE:
            { /* End the sequencer. */
              ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
               ctrl->pendjob_obj = NULL;
               rc = APR_EOK;
            }
            break;

          default:
            CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
            break;
        }
        break;
      }
    }
    break;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_get_param_v2_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_track_job_object_t* track_job_obj;
  vss_icommon_cmd_get_param_v2_t* payload;
  uint32_t payload_size;
  uint64_t param_data_virt_addr = 0;
  voice_get_param_v2_t get_param;
  uint32_t vsm_mem_handle;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_get_param_v2_t, ctrl->packet );
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_size != sizeof( vss_icommon_cmd_get_param_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_param_v2_cmd_ctrl(): Unexpected payload size, %d != %d",
                                              payload_size,
                                              sizeof( vss_icommon_cmd_get_param_v2_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvs_verify_param_data_size( payload->mem_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_param_v2_cmd_ctrl(): Invalid param data size, %d",
                                              payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload->mem_handle == 0 )
    { /* Parameter data is in-band. */
      vsm_mem_handle = 0;
    }
    else
    { /* Parameter data is out-of-band. */
      rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_param_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X.",
                                                payload->mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      ( void ) cvd_mem_mapper_get_vsm_mem_handle( payload->mem_handle, &vsm_mem_handle );

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvs_get_param_v2_cmd_ctrl(): Mis-aligned mem address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
     }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_get_param_v2_cmd_ctrl(): Mis-aligend mem size: 0x%08X",
                                                payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                     payload->mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvs_get_param_v2_cmd_ctrl(): Memory is not within range,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->mem_handle, ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( payload->mem_handle, payload->mem_address,
                                            &param_data_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvs_get_param_v2_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
          ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }
    }

    get_param.payload_address_lsw = ( ( uint32_t ) param_data_virt_addr );
    get_param.payload_address_msw = ( ( uint32_t ) ( param_data_virt_addr >> 32 ) );
    get_param.module_id = payload->module_id;
    get_param.param_id = payload->param_id;
    get_param.param_max_size = payload->mem_size;
    get_param.reserved = 0;
    get_param.mem_map_handle = vsm_mem_handle;

    rc = cvs_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVS_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_RESULT ] = cvs_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_PARAM ] = cvs_forward_command_get_param_rsp_fn;

    /* Store the original destination and opcode info from the packet */
    track_job_obj->orig_src_service = ctrl->packet->src_addr;
    track_job_obj->orig_src_port = ctrl->packet->src_port;
    track_job_obj->orig_dst_port = ctrl->packet->dst_port;
    track_job_obj->orig_opcode = ctrl->packet->opcode;
    track_job_obj->orig_token = ctrl->packet->token;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: Sending GET_PARAM_V2 to VCP. cvs_get_param_v2_cmd_ctrl(): Token=%d Param ID=0x%08x",
                                          ctrl->packet->token, payload->param_id );

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_vsm_addr, session_obj->vsm_stream_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVS_COMM_ERROR( rc, cvs_vsm_addr );

    ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

    break;
  }

  if ( rc )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
    CVS_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t cvs_stream_mvm_attach_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_evt_voc_operating_mode_update_t voc_op_mode_update;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->attached_mvm_handle == ctrl->packet->src_port )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_mvm_attach_cmd_ctrl(): Stream has already been " \
                                              "attached to the MVM session handle 0x%04X.",
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->attached_mvm_handle != APR_NULL_V )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_mvm_attach_cmd_ctrl(): Stream is currently " \
                                              "attached to the MVM session handle 0x%04X, and cannot be " \
                                              "attached to the new MVM session handle 0x%04X",
                                              session_obj->attached_mvm_handle,
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    /* Write access to attached_mvm_handle. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->attached_mvm_handle = ctrl->packet->src_port;
    ( void ) apr_lock_leave( session_obj->lock );

    /* Send VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event to MVM. */
    voc_op_mode_update.rx_mode = session_obj->voc_operating_mode_info.rx_mode;
    voc_op_mode_update.tx_mode = session_obj->voc_operating_mode_info.tx_mode;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvs_mvm_addr, session_obj->attached_mvm_handle,
           0, VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE,
           &voc_op_mode_update, sizeof( voc_op_mode_update ) );
    CVS_COMM_ERROR( rc, cvs_mvm_addr );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_mvm_detach_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->attached_mvm_handle == APR_NULL_V )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_mvm_detach_cmd_ctrl(): Stream is currently not " \
                                            "attached to any MVM session handle." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->attached_mvm_handle != ctrl->packet->src_port )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_mvm_detach_cmd_ctrl(): Stream is currently " \
                                              "attached to the MVM session handle 0x%04X, and the caller " \
                                              "MVM session's handle is 0x%04X.",
                                              session_obj->attached_mvm_handle,
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    /* Write access to attached_mvm_handle. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->attached_mvm_handle = APR_NULL_V;
    ( void ) apr_lock_leave( session_obj->lock );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_set_voice_timing_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_set_voice_timing_t* in_args;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_istream_cmd_set_voice_timing_t ) );
    if ( rc ) break;

    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Setting voice timing when the stream is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_voice_timing_cmd_ctrl(): Stream is running." );
      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_voice_timing_t,
                                     ctrl->packet );

    /* Save the timing parameters in target_set to be picked up by the state
     * machine.
     */
    session_obj->target_set.voice_timing = *in_args;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_listen_for_event_class_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_inotify_cmd_listen_for_event_class_t* in_args;
  vss_istream_evt_eamr_mode_changed_t eamr_mode_changed;
  vss_istream_evt_evs_rx_bandwidth_changed_t evs_rx_bandwidth_changed;
  vss_iavsync_evt_rx_path_delay_t rx_path_delay;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_inotify_cmd_listen_for_event_class_t ) );
    if ( rc ) break;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_inotify_cmd_listen_for_event_class_t,
                                     ctrl->packet );

    switch ( in_args->class_id )
    {
    case VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE:
      {
        if ( session_obj->eamr_mode_change_notification_info.is_enabled == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_listen_for_event_class_cmd_ctrl(): Client has already listened." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to eamr_mode_change_notification_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->eamr_mode_change_notification_info.is_enabled = TRUE;
        session_obj->eamr_mode_change_notification_info.client_addr = ctrl->packet->src_addr;
        session_obj->eamr_mode_change_notification_info.client_port = ctrl->packet->src_port;
        session_obj->eamr_mode_change_notification_info.mode = VSS_ISTREAM_EAMR_MODE_NARROWBAND;
        ( void ) apr_lock_leave( session_obj->lock );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );

       /* Send eAMR mode change event to registered client if the mode is
        * different from default mode. Refer to the comments under
        * eamr_mode_change_notification_info in cvs_session_object_t for
        * details.
        */
        if ( ( session_obj->active_set.media_id == VSS_MEDIA_ID_EAMR ) &&
             ( session_obj->voc_operating_mode_info.rx_mode ==
               VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB ) )
        {
          session_obj->eamr_mode_change_notification_info.mode =
            VSS_ISTREAM_EAMR_MODE_WIDEBAND;

          eamr_mode_changed.mode = VSS_ISTREAM_EAMR_MODE_WIDEBAND;

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 session_obj->eamr_mode_change_notification_info.client_addr,
                 session_obj->eamr_mode_change_notification_info.client_port,
                 0, VSS_ISTREAM_EVT_EAMR_MODE_CHANGED,
                 &eamr_mode_changed, sizeof( eamr_mode_changed ) );
          CVS_COMM_ERROR(
            rc, session_obj->eamr_mode_change_notification_info.client_addr );
        }
      }
      break;

    case VSS_ISTREAM_EVENT_CLASS_EVS_BANDWIDTH_CHANGE:
      {
        if ( session_obj->evs_bandwidth_change_notification_info.is_enabled == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_listen_for_event_class_cmd_ctrl(): Client has already listened." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to evs_bandwidth_change_notification_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->evs_bandwidth_change_notification_info.is_enabled = TRUE;
        session_obj->evs_bandwidth_change_notification_info.client_addr = ctrl->packet->src_addr;
        session_obj->evs_bandwidth_change_notification_info.client_port = ctrl->packet->src_port;
        ( void ) apr_lock_leave( session_obj->lock );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
        
       /* Send EVS Rx Bandwidth if registering during a call. Refer to the comments under
        * evs_bandwidth_change_notification_info in cvs_session_object_t for
        * details.
        */       
        if ( ( session_obj->evs_bandwidth_change_notification_info.is_enabled == TRUE ) &&
             ( session_obj->active_set.media_id == VSS_MEDIA_ID_EVS ) &&
             ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN ) )   
        {
          if ( session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth == CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET )
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_listen_for_event_class_cmd_ctrl(): " \
                                                  "EVS Bandwidth not yet set by vocoder while stream is running." );
            break;
          }
          
          evs_rx_bandwidth_changed.bandwidth = session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 session_obj->evs_bandwidth_change_notification_info.client_addr,
                 session_obj->evs_bandwidth_change_notification_info.client_port,
                 0, VSS_ISTREAM_EVT_EVS_RX_BANDWIDTH_CHANGED,
                 &evs_rx_bandwidth_changed, sizeof( evs_rx_bandwidth_changed ) );
          CVS_COMM_ERROR(
            rc, session_obj->evs_bandwidth_change_notification_info.client_addr );
            
          /* Updated last sent Rx Bandwidth */
          session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = 
            session_obj->evs_bandwidth_change_notification_info.last_received_rx_bandwidth;            
        }
      }
      break;      
      
    case VSS_IAVSYNC_EVENT_CLASS_RX:
      { /*  Need to Cache the client addr/port info . */
        if ( session_obj->avsync_client_rx_info.is_enabled == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
               "cvs_listen_for_event_class_cmd_ctrl(): Client has already listened for RX." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to avsync_client_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->avsync_client_rx_info.is_enabled = TRUE;
        session_obj->avsync_client_rx_info.client_addr = ctrl->packet->src_addr;
        session_obj->avsync_client_rx_info.client_port = ctrl->packet->src_port;
        ( void ) apr_lock_leave( session_obj->lock );

        /* If stream is already running then send the rx_delay event. */
        if( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
        {
          rx_path_delay.delay_us = session_obj->avsync_delay_info.total_rx_delay;

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 client_addr, session_obj->avsync_client_rx_info.client_port,
                 APR_NULL_V, VSS_IAVSYNC_EVT_RX_PATH_DELAY,
                 &rx_path_delay, sizeof( rx_path_delay ) );
          CVS_COMM_ERROR( rc, client_addr );

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_listen_for_event_class_cmd_ctrl(): IAVSYNC RX Path Delay: %d",
                                                 rx_path_delay.delay_us );

          MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, 
               "cvs_listen_for_event_class_cmd_ctrl(): CVS in RUN state send IAVSYNC RX Path Delay." );
        }

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_listen_for_event_class_cmd_ctrl(): IAVSYNC RX Client Addr: 0x%04X",
                                              session_obj->avsync_client_rx_info.client_addr );
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_listen_for_event_class_cmd_ctrl(): IAVSYNC RX Client Port: 0x%04X",
                                              session_obj->avsync_client_rx_info.client_port );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;

    case VSS_IAVSYNC_EVENT_CLASS_TX:
      { /*  Need to Cache the client addr/port info . */
        if ( session_obj->avsync_client_tx_info.is_enabled == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvs_listen_for_event_class_cmd_ctrl(): Client has already listened for TX." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to avsync_client_tx_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->avsync_client_tx_info.is_enabled = TRUE;
        session_obj->avsync_client_tx_info.client_addr = ctrl->packet->src_addr;
        session_obj->avsync_client_tx_info.client_port = ctrl->packet->src_port;
        ( void ) apr_lock_leave( session_obj->lock );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_listen_for_event_class_cmd_ctrl(): IAVSYNC TX Client Addr: 0x%04X",
                                              session_obj->avsync_client_tx_info.client_addr );
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_listen_for_event_class_cmd_ctrl(): IAVSYNC TX Client Port: 0x%04X",
                                              session_obj->avsync_client_tx_info.client_port );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );

      }
      break;     
      
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_listen_for_event_class_cmd_ctrl(): Unsupported event class: 0x%08X",
                                                in_args->class_id );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvs_cancel_event_class_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_inotify_cmd_cancel_event_class_t* in_args;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_inotify_cmd_cancel_event_class_t ) );
    if ( rc ) break;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_inotify_cmd_cancel_event_class_t,
                                     ctrl->packet );

    switch ( in_args->class_id )
    {
      case VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE:
      {
        if ( session_obj->eamr_mode_change_notification_info.is_enabled == FALSE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_cancel_event_class_cmd_ctrl(): Client has not listened." );
          
          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to eamr_mode_change_notification_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->eamr_mode_change_notification_info.is_enabled = FALSE;
        session_obj->eamr_mode_change_notification_info.client_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->eamr_mode_change_notification_info.client_port = APR_NULL_V;   
        ( void ) apr_lock_leave( session_obj->lock );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;

      case VSS_ISTREAM_EVENT_CLASS_EVS_BANDWIDTH_CHANGE:
      {
        if ( session_obj->evs_bandwidth_change_notification_info.is_enabled == FALSE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_cancel_event_class_cmd_ctrl(): Client has not listened." );
          
          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        /* Write access to evs_bandwidth_change_notification_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->evs_bandwidth_change_notification_info.is_enabled = FALSE;
        session_obj->evs_bandwidth_change_notification_info.client_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->evs_bandwidth_change_notification_info.client_port = APR_NULL_V;
		session_obj->evs_bandwidth_change_notification_info.last_sent_rx_bandwidth = CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET;
        ( void ) apr_lock_leave( session_obj->lock );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;      
      
      case VSS_IAVSYNC_EVENT_CLASS_RX:
      { /*  Need to store the Cache the client addr/port info. */
        if ( session_obj->avsync_client_rx_info.is_enabled == FALSE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_cancel_event_class_cmd_ctrl(): Client has not listened to IAVSYNC in RX." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): IAVSYNC RX Client Addr: 0x%04X",
                                              session_obj->avsync_client_rx_info.client_addr );
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): IAVSYNC RX Client Port: 0x%04X",
                                              session_obj->avsync_client_rx_info.client_port );

        /* Write access to avsync_client_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->avsync_client_rx_info.is_enabled = FALSE;
        session_obj->avsync_client_rx_info.client_addr = APR_NULL_V;
        session_obj->avsync_client_rx_info.client_port = APR_NULL_V;
        ( void ) apr_lock_leave( session_obj->lock );

        MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): Client cancelled IAVSYNC on RX." );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;

      case VSS_IAVSYNC_EVENT_CLASS_TX:
      {
        if ( session_obj->avsync_client_tx_info.is_enabled == FALSE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_cancel_event_class_cmd_ctrl(): Client has not listened to IAVSYNC in TX." );

          rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
          CVS_COMM_ERROR( rc, client_addr );

          break;
        }

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): IAVSYNC TX Client Addr: 0x%04X",
                                              session_obj->avsync_client_tx_info.client_addr );
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): IAVSYNC TX Client Port: 0x%04X",
                                              session_obj->avsync_client_tx_info.client_port );

        /* Write access to avsync_client_tx_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->avsync_client_tx_info.is_enabled = FALSE;
        session_obj->avsync_client_tx_info.client_addr = APR_NULL_V;
        session_obj->avsync_client_tx_info.client_port = APR_NULL_V;
        ( void ) apr_lock_leave( session_obj->lock );

        MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_cancel_event_class_cmd_ctrl(): Client cancelled IAVSYNC on TX." );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;
      
      default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_cancel_event_class_cmd_ctrl(): Unsupported event class: 0x%08X",
                                                in_args->class_id );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
        CVS_COMM_ERROR( rc, client_addr );
      }
      break;
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvs_stream_set_vocproc_avsync_delays_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  vss_istream_cmd_set_vocproc_avsync_delays_t *avsync_vp_delay;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  session_obj->avsync_delay_info.total_rx_delay = 0;
  session_obj->avsync_delay_info.total_tx_delay = 0;

  client_addr = ctrl->packet->src_addr;

  avsync_vp_delay = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_vocproc_avsync_delays_t, ctrl->packet );

  /* Add the normalized< processing + algorithmic > vocproc delay. */
  session_obj->avsync_delay_info.total_rx_delay =
    avsync_vp_delay->vp_rx_normalized_total_delay;
  session_obj->avsync_delay_info.total_tx_delay =
    avsync_vp_delay->vp_tx_normalized_total_delay;

  /* The encoder/decoder processing delays were already reverse calculated
   * during mvm_calculate_avsync_delays() with in MVM. Thus adding of algorithmic
   * delays is only left.
   */

  /* Add the stream algoritmic delay. */
  session_obj->avsync_delay_info.total_rx_delay +=
    session_obj->avsync_delay_info.stream_rx_algorithmic_delay ;
  session_obj->avsync_delay_info.total_tx_delay +=
    session_obj->avsync_delay_info.stream_tx_algorithmic_delay ;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_stream_set_vocproc_avsync_delays_cmd_ctrl(): total_rx_delay = %d "
                                        "total_tx_delay = %d ",
                                        session_obj->avsync_delay_info.total_rx_delay,
                                        session_obj->avsync_delay_info.total_tx_delay );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_ittyoob_cmd_register (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  /* Registration supported for Single Client  */
  if( session_obj->tty_info.ittyoob_client_addr != APR_NULL_V )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_ittyoob_cmd_register(): ITTYOOB is already Registered!!!" );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY);
    CVS_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  /* Write access to tty_info. */
  ( void ) apr_lock_enter( session_obj->lock );

  if( session_obj->tty_info.is_ittyoob_registered == FALSE )
  {
    session_obj->is_stream_config_changed = TRUE;
  }

  session_obj->tty_info.is_ittyoob_registered = TRUE;
  /* Caching the Source Addr/Port  for routing the OOBTTY EVT for Tx. */
  session_obj->tty_info.ittyoob_client_addr = ctrl->packet->src_addr;
  session_obj->tty_info.ittyoob_client_port = ctrl->packet->src_port;
  ( void ) apr_lock_leave( session_obj->lock );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ittyoob_cmd_register(): cvs_ittyoob_cmd_register(): " \
                                      "ITTYOOB Registered" );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ittyoob_cmd_register(): ITTYOOB Client Addr: %04x",
                                        session_obj->tty_info.ittyoob_client_addr );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ittyoob_cmd_register(): ITTYOOB Client Port: %04x",
                                        session_obj->tty_info.ittyoob_client_port );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_ittyoob_cmd_deregister (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  /* Allow only the following entities to deregister:
   * 1. Client who registered for OOB TTY.
   * 2. CVS on behalf of its client to deregister, in case of the client's
   *    subsystem is being restarted.
   */
  if ( ( ctrl->packet->src_addr != session_obj->tty_info.ittyoob_client_addr ) &&
       ( ctrl->packet->src_addr != cvs_my_addr ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_ittyoob_cmd_deregister(): Deregistration by invalid client!!!" );

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED);
    CVS_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  /* Write access to tty_info. */
  ( void ) apr_lock_enter( session_obj->lock );

  if( session_obj->tty_info.is_ittyoob_registered == TRUE )
  {
    session_obj->is_stream_config_changed = TRUE;
  }

  session_obj->tty_info.is_ittyoob_registered = FALSE;
  /* Clearing the Cleint Addr/Port  for routing the OOBTTY EVT for Tx. */
  session_obj->tty_info.ittyoob_client_addr = APR_NULL_V;
  session_obj->tty_info.ittyoob_client_port = APR_NULL_V;
  ( void ) apr_lock_leave( session_obj->lock );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_ittyoob_cmd_deregister(): ITTYOOB Deregistered" );

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK);
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

/* Note that for mailbox packet exchange, all the client side errors detected
 * are logged with "Client error:" in the message. Errors that are not expected
 * to happen often are logged with HIGH message, errors that can happen during
 * steady state are logged with MED message. The errors are not logged using
 * ERROR message because such messages are usually intepreted by others to mean
 * CVD issues (results in CVD support overhead). In the future, CVD should
 * request for a new log code, e.g. CVD_CLIENT_ERROR_LOG_CODE to log all client
 * side errors.
 */
static int32_t cvs_pktexg_mailbox_set_config_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvd_virt_addr_t tx_circ_buf_virt_addr;
  cvd_virt_addr_t rx_circ_buf_virt_addr;
  uint32_t tx_circ_buf_data_size;
  uint32_t rx_circ_buf_data_size;
  cvs_session_object_t* session_obj;
  cvs_indirection_object_t* indirect_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_ipktexg_cmd_mailbox_set_config_t* payload;
  uint8_t* oob_packet_mem;
  vsm_config_packet_exchange_t vsm_set_oob_config;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;
  struct voice_map_args_t {
    voice_cmd_shared_mem_map_regions_t map_memory;
    voice_shared_map_region_payload_t mem_region;
  } voice_map_args;
  voice_cmd_shared_mem_unmap_regions_t voice_unmap_memory;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_ALLOC_AND_MAP_OOB_PKTEXG_MEM,
      /**< Allocate and map CVS heap memory for OOB packet exchange with VSM. */
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_SET_OOB_PKTEXG_CONFIG,
      /**< Configure VSM for OOB packet exchange. */
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR,
      /**<
        * Unmap and free CVS heap memory if there is an error for setting OOB
        * packet exchange config on VSM. This sequencer state and 
        * CVS_SEQUENCER_ENUM_WAIT_3 are not entered if there is no error.
        */
    CVS_SEQUENCER_ENUM_WAIT_3,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;
  mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ipktexg_cmd_mailbox_set_config_t ) );
    if ( rc ) return APR_EOK;

    if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
    {
      return APR_EOK;
    }

    if ( session_obj->session_ctrl.state == CVS_STATE_ENUM_RUN )
    { /* Stream must be disabled when setting the maibox pktexg config. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
           "Client error: Stream is running." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    if ( session_obj->packet_exchange_info.mode != VSS_IPKTEXG_MODE_MAILBOX )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
           "Client error: Packet exchange mode is not mailbox." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    if ( cvs_media_id_is_valid( session_obj->active_set.media_id ) == FALSE )
    { /* If no media type has been set, reject this command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
           "Client error: Media ID has not been set yet." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_cmd_mailbox_set_config_t, 
                                     ctrl->packet );

    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
             "Client error: Invalid mem_handle: 0x%08X.", payload->mem_handle );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EHANDLE );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    if ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) ||
         ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
    {
      rc = cvd_mem_mapper_validate_attributes_align(
             payload->mem_handle, payload->tx_circ_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Mis-aligned tx_circ_buf_mem_address: 0x%016X.", payload->tx_circ_buf_mem_address );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region(
             payload->mem_handle, payload->tx_circ_buf_mem_address, 
             payload->tx_circ_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Memory is not within range, mem_handle: 0x%08X, " \
               "tx_circ_buf_mem_address: 0x%016X, tx_circ_buf_mem_size: %d.",
               payload->mem_handle, payload->tx_circ_buf_mem_address, 
               payload->tx_circ_buf_mem_size );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      tx_circ_buf_data_size = 
        ( payload->tx_circ_buf_mem_size - sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );

      if ( ( tx_circ_buf_data_size % sizeof( vss_ipktexg_mailbox_enc_request_t ) ) != 0 )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Tx circular buffer data size %d is not a multiple of an encoding request size.",
               tx_circ_buf_data_size );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( 
             payload->mem_handle, payload->tx_circ_buf_mem_address,
             &tx_circ_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Cannot get virtual address, mem_handle: 0x%08X, tx_circ_buf_mem_address: 0x%016X.",
               payload->mem_handle, payload->tx_circ_buf_mem_address );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }
    }

    if ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) ||
         ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
    {
      rc = cvd_mem_mapper_validate_attributes_align(
             payload->mem_handle, payload->rx_circ_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Mis-aligned rx_circ_buf_mem_address: 0x%016X.", payload->rx_circ_buf_mem_address );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( 
             payload->mem_handle, payload->rx_circ_buf_mem_address,
             payload->rx_circ_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Memory is not within range, mem_handle: 0x%08X, " \
               "rx_circ_buf_mem_address: 0x%016X, rx_circ_buf_mem_size: %d.",
               payload->mem_handle, payload->rx_circ_buf_mem_address, 
               payload->rx_circ_buf_mem_size );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      rx_circ_buf_data_size = 
        ( payload->rx_circ_buf_mem_size - sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );

      if ( ( rx_circ_buf_data_size % sizeof( vss_ipktexg_mailbox_dec_request_t ) ) != 0 )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Client error: Rx circular buffer data size %d is not a multiple of a decoding request size.",
               rx_circ_buf_data_size );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( 
             payload->mem_handle, payload->rx_circ_buf_mem_address,
             &rx_circ_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Cannot get virtual address, mem_handle: 0x%08X, rx_circ_buf_mem_address: 0x%016X.",
               payload->mem_handle, payload->rx_circ_buf_mem_address );

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
        CVS_COMM_ERROR( rc, client_addr );

        return APR_EOK;
      }
    }

    mailbox_config->cvs_mem_handle = payload->mem_handle;

    if ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) ||
         ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
    {
      mailbox_config->tx_circ_buf = 
        ( ( vss_ipktexg_mailbox_voc_req_circ_buffer_t* ) tx_circ_buf_virt_addr.ptr );
      mailbox_config->tx_circ_buf_mem_size = payload->tx_circ_buf_mem_size;
    }

    if ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) ||
         ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
    {
      mailbox_config->rx_circ_buf = 
        ( ( vss_ipktexg_mailbox_voc_req_circ_buffer_t* ) rx_circ_buf_virt_addr.ptr );
      mailbox_config->rx_circ_buf_mem_size = payload->rx_circ_buf_mem_size;
    }

    if ( session_obj->packet_exchange_info.mailbox_info.is_configured == TRUE )
    {
      /* If the mailbox packet exchange was already configured before, the
       * client is re-configuring the mailbox packet exchange. There is no need
       * to enter the sequencer for configuring the OOB packet exchange with
       * VSM. Simply reset the book keeping information and send a response to
       * the client.
       */
      mailbox_config->tx_ref_timestamp_us = 0;
      mailbox_config->rx_ref_timestamp_us = 0;
      mailbox_config->request_to_start_timestamp_us = 0;
      mailbox_config->rx_req_and_dequeue_time_diff_us =
        CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
      mailbox_config->is_start_requested = FALSE;
      mailbox_config->is_reseted = FALSE;
      mailbox_config->is_time_ref_received = FALSE;
      mailbox_config->is_rx_expiry_proc_disabled = FALSE;
      mailbox_config->rx_expiry_timestamp_us = 0;
      if ( session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle != 
           APR_NULL_V )
      {
        ( void ) cvs_mailbox_timer_destroy(
                   session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle );
        session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
      }

      ( void )mmstd_memset(
                &session_obj->packet_exchange_info.mailbox_info.stats, 0,
                sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
      CVS_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_ALLOC_AND_MAP_OOB_PKTEXG_MEM;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVS_SEQUENCER_ENUM_ALLOC_AND_MAP_OOB_PKTEXG_MEM:
      { /* Allocate and map CVS heap memory for OOB packet exchange with VSM. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;

        { /* Allocate encoder and decoder packet memory from CVS heap. */
          oob_packet_mem = ( ( uint8_t* ) apr_memmgr_malloc(
                               &cvs_heapmgr, CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE * 2 ) );
          if ( oob_packet_mem == NULL )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
                   "Failed to allocate OOB packet buffers, requestd size( %d ).",
                   CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE * 2 );

            seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = APR_ENORESOURCE;
            continue;
          }

          mailbox_config->oob_enc_buf = oob_packet_mem;
          mailbox_config->oob_dec_buf = 
            ( oob_packet_mem + CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE );
        }

        { /* Map shared memory with VSM. */
          rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
          CVS_PANIC_ON_ERROR( rc );
          subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_MAP_MEMORY ] = cvs_map_oob_pktexg_mem_rsp_fn;
          seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

          voice_map_args.map_memory.mem_pool_id = VOICE_MAP_MEMORY_SHMEM8_4K_POOL;
          voice_map_args.map_memory.num_regions = 1;
          voice_map_args.map_memory.property_flag = 2; /* Indicates mapping heap memory. */
          voice_map_args.mem_region.shm_addr_lsw = ( ( uint32_t ) oob_packet_mem );
          voice_map_args.mem_region.shm_addr_msw = 0;
          voice_map_args.mem_region.mem_size_bytes = ( CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE * 2 );

          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
                 "Map memory with VSM, mem_pool_id: 0x%08X, num_regions: %d, property_flag: 0x%08X.",
                 voice_map_args.map_memory.mem_pool_id,
                 voice_map_args.map_memory.num_regions,
                 voice_map_args.map_memory.property_flag );

          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
                 "shm_addr_lsw: 0x%08X, shm_addr_msw: 0x%08X, mem_size_bytes: %d.",
                 voice_map_args.mem_region.shm_addr_lsw,
                 voice_map_args.mem_region.shm_addr_msw,
                 voice_map_args.mem_region.mem_size_bytes );

          rc = __aprv2_cmd_alloc_send(
                 cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvs_vsm_addr, APR_NULL_V,
                 subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_MAP_REGIONS,
                 &voice_map_args, sizeof( voice_map_args ) );
          CVS_COMM_ERROR( rc, cvs_vsm_addr );
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for VSM memory map to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );

        if ( seqjob_obj->status != APR_EOK )
        {
          apr_memmgr_free( &cvs_heapmgr, mailbox_config->oob_enc_buf );
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_SET_OOB_PKTEXG_CONFIG;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_SET_OOB_PKTEXG_CONFIG:
      { /* Sets the OOB packet exchange config on VSM. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        vsm_set_oob_config.mem_handle = mailbox_config->vsm_mem_handle;
        vsm_set_oob_config.enc_buf_addr_lsw = ( ( uint32_t ) mailbox_config->oob_enc_buf );
        vsm_set_oob_config.enc_buf_addr_msw = 0;
        vsm_set_oob_config.enc_buf_size = CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE;
        vsm_set_oob_config.dec_buf_addr_lsw = ( ( uint32_t ) mailbox_config->oob_dec_buf );
        vsm_set_oob_config.dec_buf_addr_msw = 0;
        vsm_set_oob_config.dec_buf_size = CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE;

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Set OOB on VSM, enc_buf_addr_lsw: 0x%08X, enc_buf_addr_msw: 0x%08X, enc_buf_size: 0x%08X.",
               vsm_set_oob_config.enc_buf_addr_lsw,
               vsm_set_oob_config.enc_buf_addr_msw,
               vsm_set_oob_config.enc_buf_size );

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "dec_buf_addr_lsw: 0x%08X, dec_buf_addr_msw: 0x%08X, dec_buf_size: 0x%08X.",
               vsm_set_oob_config.dec_buf_addr_lsw,
               vsm_set_oob_config.dec_buf_addr_msw,
               vsm_set_oob_config.dec_buf_size );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "vsm_mem_handle: 0x%08X.", vsm_set_oob_config.mem_handle );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG,
               &vsm_set_oob_config, sizeof( vsm_set_oob_config ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for set OOB pktexg config on VSM to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );

        if ( seqjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR;
        }
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR:
      { /* Clean up on error. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_3;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        voice_unmap_memory.mem_map_handle = mailbox_config->vsm_mem_handle;

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
               "Unmap memory with VSM, mem_handle: 0x%08X.", mailbox_config->vsm_mem_handle );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
               &voice_unmap_memory, sizeof( voice_unmap_memory ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for memory unmap with VSM to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;

        if ( subjob_obj->status != APR_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_set_config_cmd_ctrl(): " \
                 "Failed to unmap memory, error=0x%08X.", subjob_obj->status );
        }

        apr_memmgr_free( &cvs_heapmgr, mailbox_config->oob_enc_buf );

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        if ( seqjob_obj->status == APR_EOK )
        {
          if ( cvs_is_mailbox_timer_initialized == FALSE )
          {
            ( void ) cvs_mailbox_timer_init( );
            cvs_is_mailbox_timer_initialized = TRUE;
          }
          session_obj->packet_exchange_info.mailbox_info.is_configured = TRUE;
          mailbox_config->tx_ref_timestamp_us = 0;
          mailbox_config->rx_ref_timestamp_us = 0;
          mailbox_config->request_to_start_timestamp_us = 0;
          mailbox_config->rx_req_and_dequeue_time_diff_us =
            CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
          mailbox_config->is_start_requested = FALSE;
          mailbox_config->is_reseted = FALSE;
          mailbox_config->is_time_ref_received = FALSE;
          mailbox_config->is_rx_expiry_proc_disabled = FALSE;
          mailbox_config->rx_expiry_timestamp_us = 0;
          if ( session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle != 
               APR_NULL_V )
          {
            ( void ) cvs_mailbox_timer_destroy(
                      session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle );
            session_obj->packet_exchange_info.mailbox_info.config.rx_expiry_timer_handle = APR_NULL_V;
          }

          ( void )mmstd_memset(
                    &session_obj->packet_exchange_info.mailbox_info.stats, 0,
                    sizeof( session_obj->packet_exchange_info.mailbox_info.stats ) );
        }
        else
        { /* Clear the mailbox configurations. */
          mailbox_config->cvs_mem_handle = 0;
          mailbox_config->tx_circ_buf = NULL;
          mailbox_config->tx_circ_buf_mem_size = 0;
          mailbox_config->rx_circ_buf = NULL;
          mailbox_config->rx_circ_buf_mem_size = 0;
          mailbox_config->vsm_mem_handle = 0;
          mailbox_config->oob_enc_buf = NULL;
          mailbox_config->oob_dec_buf = NULL;
        }

        rc = __aprv2_cmd_end_command( 
               cvs_apr_handle, ctrl->packet, seqjob_obj->status );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvs_pktexg_mailbox_reset_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_indirection_object_t* indirect_obj;
  cvs_session_object_t* session_obj;
  cvs_mailbox_packet_exchange_info_t* mailbox_info;
  cvd_virt_addr_t circ_buf_virt_addr;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    /* This command is allowed only with full control handle. */
    if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
      break;

    mailbox_info = &session_obj->packet_exchange_info.mailbox_info;

    if ( mailbox_info->is_configured == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_reset_cmd_ctrl(): " \
           "Client error: Mailbox packet exchange has not been configured." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_start_requested == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_reset_cmd_ctrl(): " \
           "Client error: The stream has not requested the client to start mailbox." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_time_ref_received == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_reset_cmd_ctrl(): " \
           "Client error: Mailbox has already been started." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    { /* Clear the mailbox vocoding request circular buffer. */
      if ( ( session_obj->active_set.direction == CVS_DIRECTION_TX ) ||
           ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
      {
        mailbox_info->config.tx_circ_buf->read_offset = 0;
        mailbox_info->config.tx_circ_buf->write_offset = 0;

        circ_buf_virt_addr.ptr = mailbox_info->config.tx_circ_buf;

        ( void ) cvd_mem_mapper_cache_flush_v2(
                   &circ_buf_virt_addr, sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );
      }

      if ( ( session_obj->active_set.direction == CVS_DIRECTION_RX ) ||
           ( session_obj->active_set.direction == CVS_DIRECTION_RX_TX ) )
      {
        mailbox_info->config.rx_circ_buf->read_offset = 0;
        mailbox_info->config.rx_circ_buf->write_offset = 0;

        circ_buf_virt_addr.ptr = mailbox_info->config.rx_circ_buf;

        ( void ) cvd_mem_mapper_cache_flush_v2(
                   &circ_buf_virt_addr, sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) );
      }
    }

    mailbox_info->config.is_reseted = TRUE;

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_start_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_indirection_object_t* indirect_obj;
  cvs_session_object_t* session_obj;
  cvs_mailbox_packet_exchange_info_t* mailbox_info;
  vss_ipktexg_cmd_mailbox_start_t* in_args;
  uint64_t current_timestamp_us = 0;
  uint64_t rx_expiry_timestamp_us;
  uint64_t client_rx_offset;
  uint64_t current_time_offset;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ipktexg_cmd_mailbox_start_t ) );
    if ( rc ) break;

    /* This command is allowed only with full control handle. */
    if ( cvs_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
      break;

    mailbox_info = &session_obj->packet_exchange_info.mailbox_info;

    if ( mailbox_info->is_configured == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_start_cmd_ctrl(): \
           Client error: Mailbox packet exchange has not been configured." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_start_requested == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
           "Client error: The stream has not requested the client to start mailbox." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_time_ref_received == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
           "Client error: Time reference has already been provided." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EALREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_reseted == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
           "Client error: Mailbox has not been reseted yet." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_rx_expiry_proc_disabled == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
           "Client error: Mailbox rx expiry handling disabled due to call stopped " \
           "by client. Mailbox is not allowed to start after client stopped the " \
           "call." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ipktexg_cmd_mailbox_start_t,
                                     ctrl->packet );

    { /* Save the mailbox timing reference and request MVM to reconfig. */
      mailbox_info->config.is_time_ref_received = TRUE;
      mailbox_info->config.tx_ref_timestamp_us = in_args->tx_ref_timstamp_us;
      mailbox_info->config.rx_ref_timestamp_us = in_args->rx_ref_timstamp_us;

      rc = __aprv2_cmd_alloc_send(
             cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
             cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvs_mvm_addr, session_obj->attached_mvm_handle,
             0, VSS_IPKTEXG_EVT_MAILBOX_TIMING_RECONFIG,
             NULL, 0 );
      CVS_COMM_ERROR( rc, cvs_mvm_addr );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
             "Tx reference time %d, Rx reference time %d.",
             mailbox_info->config.tx_ref_timestamp_us,
             mailbox_info->config.rx_ref_timestamp_us );
    }

    rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
    CVS_COMM_ERROR( rc, client_addr );

    /* Create a timer to process Rx packets before the decoder becomes ready. */
    if ( mailbox_info->config.rx_expiry_timer_handle == APR_NULL_V )
    {
      rc = cvs_mailbox_timer_create(
             cvs_mailbox_pktexg_rx_expiry_timer_cb,
             &session_obj->master_cvs_port,
             &mailbox_info->config.rx_expiry_timer_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_start_cmd_ctrl(): " \
               "Failed to create Rx expiry timer, rc = 0x%08X.", rc );

        break;
      }
    }

    /* Get the current timestamp. */
    rc = cvs_mailbox_timer_get_time( &current_timestamp_us );

    client_rx_offset =
      ( mailbox_info->config.rx_ref_timestamp_us % CVS_VOC_PACKET_DURATION_US );
    current_time_offset = ( current_timestamp_us % CVS_VOC_PACKET_DURATION_US );

    if ( client_rx_offset >= current_time_offset )
    {
      rx_expiry_timestamp_us =
        ( ( client_rx_offset - current_time_offset ) + current_timestamp_us +
          CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN +
          CVS_MAILBOX_DEC_EXPIRY_HANDLING_SAFETY_MARGIN );
    }
    else
    {
      rx_expiry_timestamp_us =
        ( ( ( CVS_VOC_PACKET_DURATION_US + client_rx_offset ) - current_time_offset ) +
          current_timestamp_us + CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN +
          CVS_MAILBOX_DEC_EXPIRY_HANDLING_SAFETY_MARGIN );
    }

    /* Set the expiry time to the previous expected expiry time. */
    if ( rx_expiry_timestamp_us > CVS_VOC_PACKET_DURATION_US )
    {
      mailbox_info->config.rx_expiry_timestamp_us =
        ( rx_expiry_timestamp_us - CVS_VOC_PACKET_DURATION_US );
    }

    ( void ) cvs_mailbox_timer_start_absolute(
               mailbox_info->config.rx_expiry_timer_handle,
               mailbox_info->config.rx_expiry_timestamp_us );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_get_voc_packet_properties_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  vss_ipktexg_rsp_mailbox_get_voc_packet_properties_t get_voc_packet_prop_rsp;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( cvs_media_id_is_valid( session_obj->active_set.media_id ) == FALSE )
    { /* If no media type has been set, reject this command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_get_voc_packet_properties_cmd_ctrl(): " \
           "Client error: Media type has not been set yet." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvs_pktexg_mailbox_get_max_pkt_size(
           session_obj->active_set.media_id,
           &get_voc_packet_prop_rsp.tx_max_size,
           &get_voc_packet_prop_rsp.rx_max_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_pktexg_mailbox_get_voc_packet_properties_cmd_ctrl(): " \
             "Client error: Failed to get vocoder packet size, rc = 0x%08X", rc );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, rc );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    get_voc_packet_prop_rsp.tx_duration_us = CVS_VOC_PACKET_DURATION_US;
    get_voc_packet_prop_rsp.rx_duration_us = CVS_VOC_PACKET_DURATION_US;

    switch ( session_obj->active_set.direction )
    {
    case CVS_DIRECTION_RX:
      {
        get_voc_packet_prop_rsp.tx_duration_us = 0;
        get_voc_packet_prop_rsp.tx_max_size = 0;
      }
      break;

    case CVS_DIRECTION_TX:
      {
        get_voc_packet_prop_rsp.rx_duration_us = 0;
        get_voc_packet_prop_rsp.rx_max_size = 0;
      }
      break;

    default:
      break;
    }

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvs_my_addr, ctrl->packet->dst_port,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_IPKTEXG_RSP_MAILBOX_GET_VOC_PACKET_PROPERTIES,
           &get_voc_packet_prop_rsp, sizeof( get_voc_packet_prop_rsp ) );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_pktexg_get_mode_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  vss_ipktexg_rsp_get_mode_t rsp_get_mode;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rsp_get_mode.mode = session_obj->packet_exchange_info.mode;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvs_my_addr, ctrl->packet->dst_port,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_IPKTEXG_RSP_GET_MODE,
           &rsp_get_mode, sizeof( rsp_get_mode ) );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_clear_time_reference_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_mailbox_packet_exchange_info_t* mailbox_info;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    mailbox_info = &session_obj->packet_exchange_info.mailbox_info;

    if ( mailbox_info->is_configured == FALSE )
    {
      /* If mailbox packet exchange mode has not been configured, this command
       * is a no-op.
       */
      break;
    }

    mailbox_info->config.is_start_requested = FALSE;
    mailbox_info->config.is_time_ref_received = FALSE;

    if ( mailbox_info->config.rx_expiry_timer_handle != APR_NULL_V )
    {
      ( void ) cvs_mailbox_timer_destroy( mailbox_info->config.rx_expiry_timer_handle );
      mailbox_info->config.rx_expiry_timer_handle = APR_NULL_V;
    }

    break;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_get_time_reference_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  vss_ipktexg_rsp_mailbox_get_time_reference_t get_time_ref_rsp;
  cvs_mailbox_packet_exchange_info_t* mailbox_info;

  for ( ;; )
  {
    rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    mailbox_info = &session_obj->packet_exchange_info.mailbox_info;

    if ( mailbox_info->is_configured == FALSE )
    { /* If mailbox packet exchange mode has not been configured, fail this command. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_pktexg_mailbox_get_time_reference_cmd_ctrl(): " \
           "mailbox is not configured." );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EFAILED );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( mailbox_info->config.is_time_ref_received == FALSE )
    {
      if ( mailbox_info->config.is_start_requested == FALSE )
      {
        rc = cvs_mailbox_timer_get_time(
               &mailbox_info->config.request_to_start_timestamp_us );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_get_time_reference_cmd_ctrl(): " \
               "Mailbox is requested to start at timestamp %d.", mailbox_info->config.request_to_start_timestamp_us );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               cvs_my_addr, session_obj->master_cvs_port,
               session_obj->master_client_addr, session_obj->master_client_port,
               0, VSS_IPKTEXG_EVT_MAILBOX_REQUEST_TO_START,
               NULL, 0 );
        CVS_COMM_ERROR( rc, session_obj->master_client_addr );

        mailbox_info->config.is_start_requested = TRUE;
        mailbox_info->config.is_reseted = FALSE;
        mailbox_info->config.is_rx_expiry_proc_disabled = FALSE;
      }

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_ENOTREADY );
      CVS_COMM_ERROR( rc, client_addr );
      break;
    }

    get_time_ref_rsp.tx_timstamp_us = mailbox_info->config.tx_ref_timestamp_us;
    get_time_ref_rsp.rx_timstamp_us = mailbox_info->config.rx_ref_timestamp_us;
    get_time_ref_rsp.enc_offset_margin_us = CVS_MAILBOX_ENC_OFFSET_SAFETY_MARGIN;
    get_time_ref_rsp.dec_req_offset_margin_us = CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN;
    get_time_ref_rsp.dec_offset_margin_us = CVS_MAILBOX_DEC_OFFSET_SAFETY_MARGIN;

    rc = __aprv2_cmd_alloc_send(
           cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvs_my_addr, ctrl->packet->dst_port,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_IPKTEXG_RSP_MAILBOX_GET_TIME_REFERENCE,
           &get_time_ref_rsp, sizeof( get_time_ref_rsp ) );
    CVS_COMM_ERROR( rc, client_addr );

    ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

    break;
  }

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_process_rx_expiry_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_mailbox_packet_exchange_config_t* mailbox_config;
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* rx_circ_buf;
  cvd_virt_addr_t rx_circ_buf_virt_addr;
  vss_ipktexg_mailbox_dec_request_t* dec_req;
  uint64_t current_timestamp_us = 0;

  /* Get the current timestamp. */
  rc = cvs_mailbox_timer_get_time( &current_timestamp_us );

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  mailbox_config = &session_obj->packet_exchange_info.mailbox_info.config;

  for ( ;; )
  {
    if ( mailbox_config->rx_expiry_timer_handle == APR_NULL_V )
    {
      break;
    }

    rx_circ_buf = mailbox_config->rx_circ_buf;
    rx_circ_buf_virt_addr.ptr = rx_circ_buf;

    ( void ) cvd_mem_mapper_cache_invalidate_v2(
      &rx_circ_buf_virt_addr, mailbox_config->rx_circ_buf_mem_size );

    for ( ;; )
    { /* Handle expired decoding requests by completing them. */
      rc = cvs_pktexg_mailbox_is_voc_req_exist(
             rx_circ_buf, mailbox_config->rx_circ_buf_mem_size,
             sizeof( vss_ipktexg_mailbox_dec_request_t ) );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_process_rx_expiry_cmd_ctrl(): " \
             "No pending request." );

        break;
      }

      dec_req = ( ( vss_ipktexg_mailbox_dec_request_t* )
                  ( ( ( uint8_t* ) rx_circ_buf ) + 
                    sizeof( vss_ipktexg_mailbox_voc_req_circ_buffer_t ) +
                    rx_circ_buf->read_offset ) );

      if ( mailbox_config->rx_expiry_timestamp_us > dec_req->timestamp_us )
      { /* Complete the decoding request if the request has expired. */
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvs_pktexg_mailbox_process_rx_expiry_cmd_ctrl(): " \
               "Decoding request timestamp %d is expired. Current time is %d.",
               dec_req->timestamp_us, current_timestamp_us );

        ( void ) cvs_pktexg_mailbox_complete_voc_request(
                   rx_circ_buf, mailbox_config->rx_circ_buf_mem_size,
                   sizeof( vss_ipktexg_mailbox_dec_request_t ) );
      }
      else
      {
        break;
      }
    }

    mailbox_config->rx_expiry_timestamp_us =
      ( mailbox_config->rx_expiry_timestamp_us + CVS_VOC_PACKET_DURATION_US );

    ( void ) cvs_mailbox_timer_start_absolute(
               mailbox_config->rx_expiry_timer_handle,
               mailbox_config->rx_expiry_timestamp_us );

    break;
  }

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_pktexg_mailbox_disable_rx_expiry_processing_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_mailbox_packet_exchange_info_t* mailbox_info;

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  mailbox_info = &session_obj->packet_exchange_info.mailbox_info;

  if ( mailbox_info->config.rx_expiry_timer_handle != APR_NULL_V )
  {
    ( void ) cvs_mailbox_timer_destroy( mailbox_info->config.rx_expiry_timer_handle );
    mailbox_info->config.rx_expiry_timer_handle = APR_NULL_V;
  }

  if ( mailbox_info->is_configured == TRUE )
  {
    mailbox_info->config.is_rx_expiry_proc_disabled = TRUE;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EOK );
  CVS_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvs_ssr_cleanup_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_issr_cmd_cleanup_t* in_args;
  cvs_sequencer_job_object_t* seqjob_obj;
  uint16_t client_addr;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_STOP_VPCM,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_DEREGISTER_OOB_TTY,
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_CANCEL_EAMR_MODE_CHANGE_NOTIFICATION,
    CVS_SEQUENCER_ENUM_WAIT_3,
    CVS_SEQUENCER_ENUM_CANCEL_EVS_BANDWIDTH_CHANGE_NOTIFICATION,
    CVS_SEQUENCER_ENUM_WAIT_4,
    CVS_SEQUENCER_ENUM_RESET_PACKET_EXCHANGE_MODE,
    CVS_SEQUENCER_ENUM_WAIT_5,
    CVS_SEQUENCER_ENUM_DEREGISTER_AVSYNC,
    CVS_SEQUENCER_ENUM_WAIT_6,
    CVS_SEQUENCER_ENUM_DESTROY_SESSION,
    CVS_SEQUENCER_ENUM_WAIT_7,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_issr_cmd_cleanup_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_issr_cmd_cleanup_t, ctrl->packet );

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.ssr_cleanup.domain_id = in_args->domain_id;

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_STOP_VPCM;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case CVS_SEQUENCER_ENUM_STOP_VPCM:
      {
        /* Stop each of the VPCM sessions on behalf of CVS clients who reside
         * in a subsystem that is being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;

        ( void ) cvs_ssr_stop_vpcm(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for CVS to respond for each vpcm stop command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_DEREGISTER_OOB_TTY;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_DEREGISTER_OOB_TTY:
      {
        /* For each of the CVS sessions, deregister OOB TTY on behalf of CVS
         * clients who reside in a subsystem that is being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;

        ( void ) cvs_ssr_deregister_oob_tty(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for CVS to respond for each OOB TTY deregister command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_CANCEL_EAMR_MODE_CHANGE_NOTIFICATION;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_CANCEL_EAMR_MODE_CHANGE_NOTIFICATION:
      {
        /* For each of the CVS sessions, cancel the eAMR mode change
         * notifications on behalf of CVS clients who reside in a subsystem
         * that is being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_3;

        ( void ) cvs_ssr_cancel_event_class_registration(
                   seqjob_obj->use_case.ssr_cleanup.domain_id,
                   VSS_ISTREAM_EVENT_CLASS_EAMR_MODE_CHANGE );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for CVS to respond for each cancel event class command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_CANCEL_EVS_BANDWIDTH_CHANGE_NOTIFICATION;
      }
      /*-fallthru */
      
    case CVS_SEQUENCER_ENUM_CANCEL_EVS_BANDWIDTH_CHANGE_NOTIFICATION:
      {
        /* For each of the CVS sessions, cancel the EVS bandwidth change
         * notifications on behalf of CVS clients who reside in a subsystem
         * that is being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_4;

        ( void ) cvs_ssr_cancel_event_class_registration(
                   seqjob_obj->use_case.ssr_cleanup.domain_id,
                   VSS_ISTREAM_EVENT_CLASS_EVS_BANDWIDTH_CHANGE );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for CVS to respond for each cancel event class command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_RESET_PACKET_EXCHANGE_MODE;
      }
      /*-fallthru */      

    case CVS_SEQUENCER_ENUM_RESET_PACKET_EXCHANGE_MODE:
      {
        /* For each of the CVS sessions, reset the packet exchange mode to
         * in-band on behalf of CVS clients who reside in a subsystem that is
         * being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_5;

        ( void ) cvs_ssr_reset_packet_exchange_mode(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for CVS to respond for each set packet exchange mode command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_DEREGISTER_AVSYNC;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_DEREGISTER_AVSYNC:
      {
        /* For each of the CVS sessions, cancel the AVSYNC rx delay and tx timestamp
         * notifications on behalf of CVS clients who reside in a subsystem
         * that is being restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_6;

        ( void ) cvs_ssr_deregister_avsync(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_6:
      { /* Wait for CVS to respond for each set packet exchange mode command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_DESTROY_SESSION;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_DESTROY_SESSION:
      {
        /* Destroy the CVS session control handles (indirection handles) on
         * behalf of CVS clients who reside in a subsystem that is being
         * restarted.
         */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_7;

        ( void ) cvs_ssr_destroy_session(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_7:
      { /* Wait for CVS to respond for each destroy session command. */
        if ( cvs_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvs_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command(
               cvs_apr_handle, ctrl->packet, seqjob_obj->status );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvs_set_hdvoice_config_cmd_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  cvs_session_object_t* session_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_ihdvoice_cmd_set_config_t* in_args;
  aprv2_ibasic_rsp_result_t basic_result;
  voice_set_param_v2_t set_param;
  aprv2_packet_t* packet;
  uint8_t* packet_payload = NULL;
  uint32_t payload_size = 0;
  uint32_t packet_payload_left = 0;
  cvs_hdvoice_enable_param_info_t hdvoice_enable_param_info;
  uint16_t client_addr;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_HDVOICE_DISABLE,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_HDVOICE_ENABLE,  /** dynamic: doesn't need to calibrate **/
    CVS_SEQUENCER_ENUM_WAIT_2,
    CVS_SEQUENCER_ENUM_GET_KPPS,
    CVS_SEQUENCER_ENUM_WAIT_3,
    CVS_SEQUENCER_ENUM_RECONFIG,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
    if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ihdvoice_cmd_set_config_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ihdvoice_cmd_set_config_t, ctrl->packet );

    rc = cvs_create_sequencer_job_object(
           ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    session_obj->target_set.system_config.feature = in_args->feature_id;

    if ( session_obj->target_set.system_config.feature == session_obj->active_set.system_config.feature )
      return APR_EOK;


    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_HDVOICE_DISABLE;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case CVS_SEQUENCER_ENUM_HDVOICE_DISABLE:
      { 
        if (session_obj->active_set.system_config.feature == VSS_ICOMMON_CAL_FEATURE_NONE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_HDVOICE_ENABLE;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        switch ( session_obj->active_set.system_config.feature )
        {
          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV;
            break;

          case VSS_ICOMMON_CAL_FEATURE_BEAMR:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_BEAMR;
            break;

          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV_V2;
            break;

          default:
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_hdvoice_config_cmd_ctrl(): Undefined " \
                                                    "Feature ID to disable 0x%08X",
                                                    session_obj->active_set.system_config.feature );
            break;
        }

        hdvoice_enable_param_info.param_hdr.param_id = VOICE_PARAM_MOD_ENABLE;
        hdvoice_enable_param_info.param_hdr.param_size = sizeof(uint32_t);
        hdvoice_enable_param_info.param_hdr.reserved = 0;
        hdvoice_enable_param_info.enable = FALSE;
        hdvoice_enable_param_info.reserved = 0;

        set_param.payload_address_lsw = 0;
        set_param.payload_address_msw = 0;
        set_param.payload_size = sizeof(hdvoice_enable_param_info);
        set_param.mem_map_handle = 0;

        payload_size = sizeof(set_param) + sizeof(hdvoice_enable_param_info);

        rc = __aprv2_cmd_alloc_ext(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               payload_size, &packet );
        CVS_PANIC_ON_ERROR( rc );

        packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
        packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

        ( void ) mmstd_memcpy( packet_payload, packet_payload_left,
                               &set_param, sizeof (voice_set_param_v2_t) );
        packet_payload_left -= sizeof(voice_set_param_v2_t);
        ( void ) mmstd_memcpy( packet_payload + sizeof(voice_set_param_v2_t), packet_payload_left,
                               &hdvoice_enable_param_info, sizeof (cvs_hdvoice_enable_param_info_t) );

        rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the feature to be disabled by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS disabling Feature ID 0x%08X, VCP returned status 0x%08X. cvs_stream_set_system_config_cmd_ctrl()",
                                              session_obj->active_set.system_config.feature,
                                              subjob_obj->status );

        session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_HDVOICE_ENABLE;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_HDVOICE_ENABLE:
      { /* Enable/Disable HD Voice feature if feature to be enabled has changed */
        if ( session_obj->target_set.system_config.feature == VSS_ICOMMON_CAL_FEATURE_NONE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_GET_KPPS;
          continue;
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_2;
        }

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        switch ( session_obj->target_set.system_config.feature )
        {
          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV;
            break;

          case VSS_ICOMMON_CAL_FEATURE_BEAMR:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_BEAMR;
            break;

          case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
            hdvoice_enable_param_info.param_hdr.module_id = VOICE_MODULE_WV_V2;
            break;

          default:
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_hdvoice_config_cmd_ctrl(): Undefined " \
                                                    "Feature ID to enable 0x%08X",
                                                    session_obj->target_set.system_config.feature );
            break;
        }

        hdvoice_enable_param_info.param_hdr.param_id = VOICE_PARAM_MOD_ENABLE;
        hdvoice_enable_param_info.param_hdr.param_size = sizeof(uint32_t);
        hdvoice_enable_param_info.param_hdr.reserved = 0;
        hdvoice_enable_param_info.enable = TRUE;
        hdvoice_enable_param_info.reserved = 0;

        set_param.payload_address_lsw = 0;
        set_param.payload_address_msw = 0;
        set_param.payload_size = sizeof(hdvoice_enable_param_info);
        set_param.mem_map_handle = 0;

        payload_size = sizeof(set_param) + sizeof(hdvoice_enable_param_info);

        rc = __aprv2_cmd_alloc_ext(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               payload_size, &packet );
        CVS_PANIC_ON_ERROR( rc );

        packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
        packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

        ( void ) mmstd_memcpy( packet_payload, packet_payload_left,
                               &set_param, sizeof (voice_set_param_v2_t) );
        packet_payload_left -= sizeof(voice_set_param_v2_t);
        ( void ) mmstd_memcpy( packet_payload + sizeof(voice_set_param_v2_t), packet_payload_left,
                               &hdvoice_enable_param_info, sizeof (cvs_hdvoice_enable_param_info_t) );

        rc = __aprv2_cmd_forward( cvs_apr_handle, packet );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the feature to be enabled by VSM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVS enabling Feature ID 0x%08X, VCP returned status 0x%08X. cvs_stream_set_system_config_cmd_ctrl()",
                                              session_obj->target_set.system_config.feature,
                                              subjob_obj->status );

        session_obj->active_set.system_config.feature =
          session_obj->target_set.system_config.feature;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_GET_KPPS;

        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_GET_KPPS:
      { /* Retrieve the VSM's KPPS requirement. */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_3;

        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ CVS_RESPONSE_FN_ENUM_GET_KPPS ] = cvs_get_kpps_rsp_fn;

        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vsm_stream_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_GET_KPPS,
               NULL, 0 );
        CVS_COMM_ERROR( rc, session_obj->vsm_stream_addr );
      }
      /*-fallthru */

    case CVS_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for get KPPS to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        if ( session_obj->is_kpps_changed == TRUE )
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_RECONFIG;
          ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
        }
        else
        {
          seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
          ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
          continue;
        }
      } /* fallthru */

    case CVS_SEQUENCER_ENUM_RECONFIG:
      {
        /* This should only be called for WB Sampling Rate Vocoder */
        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;

        rc = __aprv2_cmd_alloc_send(
                cvs_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                cvs_mvm_addr, session_obj->attached_mvm_handle,
                0, VSS_ISTREAM_EVT_RECONFIG,
                NULL, 0 );
        CVS_COMM_ERROR( rc, cvs_mvm_addr );

        session_obj->is_kpps_changed = FALSE;
      }
      /* fallthru */
      
    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;
        basic_result.opcode = ctrl->packet->opcode;
        basic_result.status = APR_EOK;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
               ctrl->packet->dst_addr, ctrl->packet->dst_port,
               ctrl->packet->src_addr, ctrl->packet->src_port,
               ctrl->packet->token, APRV2_IBASIC_RSP_RESULT,
               &basic_result, sizeof( basic_result ) );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

}

static int32_t cvs_set_var_voc_sampling_rate_ctrl (
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_icommon_cmd_set_var_voc_sampling_rate_t* in_args;
  vsm_set_samp_rate_t set_samp_rate;
  aprv2_ibasic_rsp_result_t basic_result;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_SET_VSM_SAMP_RATE,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
            ctrl, sizeof( vss_icommon_cmd_set_var_voc_sampling_rate_t ) );
    if ( rc ) return APR_EOK;

    rc = cvs_create_sequencer_job_object(
            ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_SET_VSM_SAMP_RATE;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_var_voc_sampling_rate_t, ctrl->packet );
  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case CVS_SEQUENCER_ENUM_SET_VSM_SAMP_RATE:
      { 
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        session_obj->requested_var_voc_rx_sampling_rate = in_args->rx_pp_sr;
        session_obj->requested_var_voc_tx_sampling_rate = in_args->tx_pp_sr;

        set_samp_rate.rx_samp_rate = session_obj->requested_var_voc_rx_sampling_rate;
        set_samp_rate.tx_samp_rate = session_obj->requested_var_voc_tx_sampling_rate;
        
        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_STREAM_PP_SAMP_RATE,
               &set_samp_rate, sizeof( set_samp_rate ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;

      case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for job to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      continue;

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;
        basic_result.opcode = ctrl->packet->opcode;
        basic_result.status = APR_EOK;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
               ctrl->packet->dst_addr, ctrl->packet->dst_port,
               ctrl->packet->src_addr, ctrl->packet->src_port,
               ctrl->packet->token, APRV2_IBASIC_RSP_RESULT,
               &basic_result, sizeof( basic_result ) );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) __aprv2_cmd_free( cvs_apr_handle, ctrl->packet );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

  return APR_EFAILED;
}

static int32_t cvs_set_evs_voc_operating_mode_ctrl(
  cvs_pending_control_t* ctrl
)
{
  int32_t rc;
  int32_t status;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_istream_cmd_set_evs_voc_enc_operating_mode_t* in_args;
  vsm_set_evs_enc_operating_mode_t set_evs_enc_operating_mode;
  enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_SET_EVS_ENC_MODE,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  status = APR_EOK;
  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvs_helper_validate_payload_size_control(
            ctrl, sizeof( vss_istream_cmd_set_evs_voc_enc_operating_mode_t ) );
    if ( rc ) return APR_EOK;
	
    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_evs_voc_enc_operating_mode_t, ctrl->packet );

    if ( !cvs_vocoder_is_variable( session_obj->active_set.media_id ) )
    {
      status = APR_EFAILED;
    }

    if ( !cvs_evs_validate_enc_operating_mode ( in_args->mode, in_args->bandwidth, session_obj->requested_var_voc_tx_sampling_rate ) )
    {
      status = APR_EBADPARAM;
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_evs_voc_operating_mode_ctrl(): Invalid payload " \
                                              "Mode=0x%08X, Bandwidth=0x%08X, Tx PP Sr=0x%08X",
                                              in_args->mode, in_args->bandwidth,
                                              session_obj->requested_var_voc_tx_sampling_rate );

    }

    if ( APR_EOK != status )
    {
      client_addr = ctrl->packet->src_addr;

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Cache voc property. */
    cvs_cache_voc_property( session_obj,
      ( void* )in_args, CVS_VOC_PROPERTY_ENUM_ENC_OPERATING_MODE );

    rc = cvs_create_sequencer_job_object(
            ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_SET_EVS_ENC_MODE;
  }
  
  in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_evs_voc_enc_operating_mode_t, ctrl->packet );
  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      /* Check if the arguments violate EVS spec restrictions for bit-rates and bandwidths */
      case CVS_SEQUENCER_ENUM_SET_EVS_ENC_MODE:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        set_evs_enc_operating_mode.mode = in_args->mode;
        set_evs_enc_operating_mode.bandwidth = in_args->bandwidth;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_EVS_ENC_OPERATING_MODE,
               &set_evs_enc_operating_mode, sizeof( vsm_set_evs_enc_operating_mode_t ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;

      case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for get job to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      continue;

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        if ( status != APR_EOK )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "CVP_EVS: Bad parameter in VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE");
        }

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

  return APR_EFAILED;
}

static int32_t cvs_set_evs_enc_channel_aware_mode_ctrl(
  cvs_pending_control_t* ctrl,
  bool_t enable
)
{
  int32_t rc;
  int32_t status;
  uint16_t client_addr;
  cvs_session_object_t* session_obj;
  cvs_sequencer_job_object_t* seqjob_obj;
  cvs_simple_job_object_t* subjob_obj;
  vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t* in_args;
  vsm_set_evs_enc_channel_aware_mode_enable_t set_evs_enc_channel_aware_mode_enable;
  cvs_enc_channel_aware_mode_params_t cvs_channel_aware_mode_params;
    enum {
    CVS_SEQUENCER_ENUM_UNINITIALIZED,
    CVS_SEQUENCER_ENUM_CACHE_CHANNEL_AWARE_MODE,
    CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_ENABLE,
    CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_DISABLE,
    CVS_SEQUENCER_ENUM_WAIT_1,
    CVS_SEQUENCER_ENUM_COMPLETE,
    CVS_SEQUENCER_ENUM_INVALID
  };

  status = APR_EOK;
  rc = cvs_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVS_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( enable )
    {
      rc = cvs_helper_validate_payload_size_control(
              ctrl, sizeof( vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t ) );
      if ( rc ) return APR_EOK;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t, ctrl->packet );	
	
    if ( !cvs_vocoder_is_variable( session_obj->active_set.media_id ) )
    {
      client_addr = ctrl->packet->src_addr;
      status = APR_EFAILED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_evs_enc_channel_aware_mode_ctrl(): Active Media ID not " \
                                              "a variable vocoder: 0x%08X",
                                              session_obj->active_set.media_id );

      rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
      CVS_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvs_create_sequencer_job_object(
            ( cvs_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVS_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    if ( enable )
    {
      /* Check if the command payload is valid */
      if ( !cvs_evs_validate_enc_channel_aware_mode( in_args->fec_offset, in_args->fer_rate ) )
      {
        client_addr = ctrl->packet->src_addr;
        status = APR_EBADPARAM;
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_set_evs_enc_channel_aware_mode_ctrl(): Invalid payload " \
                                                "FEC Offset=0x%08X, FER Rate=0x%08X",
                                                in_args->fec_offset, in_args->fer_rate );
        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
        CVS_COMM_ERROR( rc, client_addr );
        return APR_EOK;
      }
    }
    ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_CACHE_CHANNEL_AWARE_MODE;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t, ctrl->packet );
  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVS_SEQUENCER_ENUM_CACHE_CHANNEL_AWARE_MODE:
      {
        /* Cache voc property. */
        cvs_channel_aware_mode_params.channel_aware_enabled = enable;
        if ( enable )
        {
          cvs_channel_aware_mode_params.fec_offset = in_args->fec_offset;
          cvs_channel_aware_mode_params.fer_rate = in_args->fer_rate;
          ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_ENABLE;
        }
        else
        {
          ( void ) apr_lock_enter( session_obj->lock );
          cvs_channel_aware_mode_params.fec_offset =
            session_obj->cached_voc_properties_evs.fec_offset;
          cvs_channel_aware_mode_params.fer_rate =
            session_obj->cached_voc_properties_evs.fer_rate;
          ( void ) apr_lock_leave( session_obj->lock );
          ctrl->pendjob_obj->sequencer_job.state = CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_DISABLE;
        }
        cvs_cache_voc_property( session_obj,
          ( void* )&cvs_channel_aware_mode_params, CVS_VOC_PROPERTY_ENUM_CHANNEL_AWARE_MODE );
      }
      continue;

    case CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_ENABLE:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        set_evs_enc_channel_aware_mode_enable.fec_offset = in_args->fec_offset;
        set_evs_enc_channel_aware_mode_enable.fer_rate = in_args->fer_rate;

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE,
               &set_evs_enc_channel_aware_mode_enable, sizeof( vsm_set_evs_enc_channel_aware_mode_enable_t ) );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;

    case CVS_SEQUENCER_ENUM_SET_EVS_CHANNEL_AWARE_DISABLE:
      {
        rc = cvs_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVS_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvs_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               cvs_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvs_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvs_vsm_addr, session_obj->vsm_stream_handle,
               subjob_obj->header.handle, VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE,
               NULL, 0 );
        CVS_COMM_ERROR( rc, cvs_vsm_addr );

        seqjob_obj->state = CVS_SEQUENCER_ENUM_WAIT_1;
      }
      continue;

    case CVS_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for get job to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        status = subjob_obj->status;

        seqjob_obj->state = CVS_SEQUENCER_ENUM_COMPLETE;
        ( void ) cvs_free_object( ( cvs_object_t* ) subjob_obj );
      }
      continue;

    case CVS_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, status );
        CVS_COMM_ERROR( rc, client_addr );

        ( void ) cvs_free_object( ( cvs_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }
  }

  return APR_EFAILED;
}

/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/

/* Dispatch the incoming packet in-context (assume in ISR). */
static int32_t cvs_isr_dispatch_fn (
  aprv2_packet_t* packet,
  void* dispatch_data
)
{
  cvs_queue_incoming_packet( CVS_THREAD_PRIORITY_ENUM_HIGH, packet );

  return APR_EOK;
}

static void cvs_response_fn_trampoline (
  uint32_t fn_index,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvs_object_t* object;
  uint32_t msg_type;

  msg_type = APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header );

  if ( msg_type == APRV2_PKT_MSGTYPE_EVENT_V )
  {
    rc = cvs_get_object( packet->dst_port, &object );
  }
  else
  {
    rc = cvs_get_object( packet->token, &object );
  }

  if ( rc == APR_EOK )
  {
    switch ( object->header.type )
    {
    case CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB:
      object->simple_job.fn_table[ fn_index ]( packet );
      return;

    default:
      break;
    }
  }

  rc = __aprv2_cmd_free( cvs_apr_handle, packet );
}

/* Process high priority events, responses, and non-gating commands in task context. */
static void cvs_high_task_process_nongating_commands ( void )
{
  cvs_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &cvs_high_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &cvs_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    /* Currently high priority task just dispatch commands to 
       medium priority task. */
    default:
      cvs_queue_incoming_packet( CVS_THREAD_PRIORITY_ENUM_MED, packet );
      break;
    }
  }
}

static void cvs_high_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void cvs_med_task_process_nongating_commands ( void )
{
  cvs_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &cvs_med_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &cvs_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    /* Accepted and response events are handled in low priority thread. */
/*    case APRV2_IBASIC_EVT_ACCEPTED:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_ACCEPTED, packet );
      break;

    case APRV2_IBASIC_RSP_RESULT:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_RESULT, packet );
      break;
*/
    case VSM_EVT_SEND_ENC_PACKET:
    case VSM_EVT_REQ_DEC_PACKET:
      ( void ) cvs_stream_forward_pkt_exchange_evt_to_client_processing( packet, packet->opcode );
      break;

    case VSM_EVT_OOB_ENC_BUF_READY:
      ( void ) cvs_stream_oob_enc_buf_ready_evt_processing( packet );
      break;

    case VSM_EVT_OOB_DEC_BUF_REQUEST:
      ( void ) cvs_stream_oob_dec_buf_request_evt_processing( packet );
      break;

    case VSS_IVPCM_EVT_PUSH_BUFFER_V2:
      ( void ) cvs_vpcm_evt_push_buffer_to_vsm_processing( packet );
      break;

    case VOICE_EVT_HOST_BUF_AVAILABLE_V2:
      ( void ) cvs_vpcm_evt_notify_to_client_processing( packet );
      break;

    case VSS_ISTREAM_CMD_VOC_QCELP13K_SET_RATE:
    case VSS_ISTREAM_CMD_VOC_4GVNB_SET_RATE:
    case VSS_ISTREAM_CMD_VOC_4GVWB_SET_RATE:
    case VSS_ISTREAM_CMD_VOC_4GVNW_SET_RATE:
    case VSS_ISTREAM_CMD_VOC_4GVNW2K_SET_RATE:
    case VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE:
    case VSS_ISTREAM_CMD_VOC_AMRWB_SET_ENC_RATE:
      ( void ) cvs_stream_set_enc_rate_cmd_processing( packet );
      break;

    case VSS_ISTREAM_CMD_CDMA_SET_ENC_MINMAX_RATE:
      ( void ) cvs_stream_cdma_set_enc_minmax_rate_cmd_processing( packet );
      break;

    case VSS_ISTREAM_CMD_CDMA_SET_ENC_RATE_MODULATION:
      ( void ) cvs_delegate_to_vsm_stream_cmd_processing( packet, VSM_CMD_ENC_SET_RATE_MOD );
      break;

    case VSS_ISTREAM_CMD_SET_ENC_DTX_MODE:
      ( void ) cvs_delegate_to_vsm_stream_cmd_processing( packet, VSM_CMD_ENC_SET_DTX_MODE );
      break;

    case VSS_ISTREAM_CMD_SET_DEJITTER_MODE:
      ( void ) cvs_delegate_to_vsm_stream_cmd_processing( packet, VSS_ISTREAM_CMD_SET_DEJITTER_MODE );
      break;

    case VSS_ISTREAM_CMD_SET_DTMF_GENERATION:
      ( void ) cvs_set_dtmf_generation_cmd_processing( packet );
      break;

    case VSS_ISTREAM_CMD_SET_RX_DTMF_DETECTION:
      ( void ) cvs_set_rx_dtmf_detection_cmd_processing( packet );
      break;

    case VSS_ISTREAM_EVT_DTMF_GENERATION_ENDED:
      ( void )cvs_forward_evt_dtmf_generation_ended_processing( packet );
      break;

    case VSS_ISTREAM_EVT_RX_DTMF_DETECTED:
      ( void )cvs_forward_evt_rx_dtmf_detected_processing( packet );
      break;

    case VSM_EVT_OUTOFBAND_TTY_TX_CHAR_DETECTED:
      ( void ) cvs_forward_ittyoob_evt_tx_char_to_client( packet );
      break;

    case VSS_ITTYOOB_CMD_SEND_RX_CHAR:
      ( void ) cvs_forward_ittyoob_cmd_rx_char_to_vsm( packet );
      break;

    case VSM_EVT_OUTOFBAND_TTY_RX_CHAR_ACCEPTED:
      ( void ) cvs_forward_ittyoob_rx_char_done_cmd_response_to_client( packet );
      break;

    case VSM_EVT_OUTOFBAND_TTY_ERROR:
      ( void ) cvs_forward_ittyoob_evt_error_cmd_response_to_client( packet );
      break;

    case VSS_IPKTEXG_EVT_IN_BAND_SEND_DEC_BUFFER:
      ( void ) cvs_stream_forward_pkt_exchange_evt_to_vsm_processing( packet, packet->opcode );
      break;

    case VSS_IPKTEXG_EVT_OOB_NOTIFY_ENC_BUFFER_CONSUMED:
      ( void ) cvs_stream_forward_pkt_exchange_evt_to_vsm_processing( packet, VSM_EVT_OOB_ENC_BUF_CONSUMED );
      break;

    case VSS_IPKTEXG_EVT_OOB_NOTIFY_DEC_BUFFER_READY:
      ( void ) cvs_stream_forward_pkt_exchange_evt_to_vsm_processing( packet, VSM_EVT_OOB_DEC_BUF_READY );
      break;

    case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
      ( void ) cvs_voc_operating_mode_update_evt_processing( packet );
      break;

    default:
      cvs_queue_incoming_packet( CVS_THREAD_PRIORITY_ENUM_LOW, packet );
      break;
    }
  }
}

static void cvs_med_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void cvs_low_task_process_nongating_commands ( void )
{
  int32_t rc;
  cvs_work_item_t* p_work_item;
  aprv2_packet_t* p_packet;
  uint16_t client_addr;

  while ( apr_list_remove_head( &cvs_low_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &p_work_item ) )
          == APR_EOK )
  {
    p_packet = ( ( aprv2_packet_t* ) p_work_item->packet );
    ( void ) apr_list_add_tail( &cvs_free_cmd_q, &p_work_item->link );

    switch ( p_packet->opcode )
    {
    /* NON GATING COMMANDS
       - These can be done right away ahead of the gating commands
     */
    case APRV2_IBASIC_EVT_ACCEPTED:
      /* TODO: Events use dst_port, but trampoline use token for results. */
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_ACCEPTED, p_packet );
      break;

    case VSS_IVOCPROC_RSP_ATTACH_STREAM:
    case VSS_IVOCPROC_RSP_DETACH_STREAM:
    case APRV2_IBASIC_RSP_RESULT:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_RESULT, p_packet );
      break;

    case VSS_ICOMMON_RSP_GET_PARAM:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_GET_PARAM, p_packet );
      break;

    case VOICE_RSP_SHARED_MEM_MAP_REGIONS:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_MAP_MEMORY, p_packet );
      break;

    case VSM_RSP_GET_KPPS_ACK:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_GET_KPPS, p_packet );
      break;

    case VSM_RSP_GET_DELAY_ACK:
      cvs_response_fn_trampoline( CVS_RESPONSE_FN_ENUM_GET_AVSYNC_DELAY, p_packet );
      break;

    case VSS_ITTY_CMD_SET_TTY_MODE:
      ( void ) cvs_set_tty_mode_cmd_processing( p_packet );
      break;

    case VSS_ISTREAM_CMD_RESYNC_CTM:
      ( void ) cvs_delegate_to_vsm_stream_cmd_processing( p_packet, VSM_CMD_RESYNC_CTM );
      break;

    case VSS_ICOMMON_CMD_SET_UI_PROPERTY:
      ( void ) cvs_set_ui_property_cmd_processing( p_packet );
      break;

    case VSS_ICOMMON_CMD_GET_UI_PROPERTY:
      ( void ) cvs_get_ui_property_cmd_processing( p_packet );
      break;

    case VSS_IVOCPROC_EVT_READY:
    case VSS_IVOCPROC_EVT_NOT_READY:
      cvs_queue_pending_packet( &cvs_low_task_pending_ctrl.cmd_q, p_packet );
      break;

    case VSS_ISSR_CMD_CLEANUP:
      cvs_queue_pending_packet( &cvs_ssr_pending_ctrl.cmd_q, p_packet );
      break;

    default:
      if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, p_packet->header ) ==
           APRV2_PKT_MSGTYPE_SEQCMD_V )
      {
        /* Sequential command. Put it on the pending command queue. */
        cvs_queue_pending_packet( &cvs_low_task_pending_ctrl.cmd_q, p_packet );
        break;
      }
      else if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, p_packet->header ) ==
           APRV2_PKT_MSGTYPE_NSEQCMD_V )
      { /* Unsupported non-sequential command. Ack back with unsupported. */
        client_addr = p_packet->src_addr;

        rc = __aprv2_cmd_end_command( cvs_apr_handle, p_packet, APR_EUNSUPPORTED );
        CVS_COMM_ERROR( rc, client_addr );
      }
      else
      { /* Unsupported event. Drop it. */
        rc = __aprv2_cmd_free( cvs_apr_handle, p_packet );
        CVS_PANIC_ON_ERROR( rc );
      }
      break;
    }
  }
}

/*TODO: Move control commands to MED thread.*/
static void cvs_low_task_process_gating_commands ( void )
{
  int32_t rc;
  cvs_pending_control_t* ctrl = &cvs_low_task_pending_ctrl;
  cvs_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case CVS_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &cvs_free_cmd_q, &work_item->link );
        }
        ctrl->state = CVS_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case CVS_PENDING_CMD_STATE_ENUM_EXECUTE:
    case CVS_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION:
          rc = cvs_core_create_session_cmd_ctrl( ctrl, TRUE );
          break;

        case VSS_ISTREAM_CMD_CREATE_PASSIVE_CONTROL_SESSION:
          rc = cvs_core_create_session_cmd_ctrl( ctrl, FALSE );
          break;

        case VSS_ISTREAM_CMD_ATTACH_VOCPROC:
          rc = cvs_stream_update_attached_vocproc_cmd_ctrl( ctrl, CVS_GOAL_ENUM_ATTACH_VOCPROC );
          break;

        case VSS_ISTREAM_CMD_DETACH_VOCPROC:
          rc = cvs_stream_update_attached_vocproc_cmd_ctrl( ctrl, CVS_GOAL_ENUM_DETACH_VOCPROC );
          break;

        case VSS_ISTREAM_CMD_ENABLE:
          rc = cvs_stream_enable_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_DISABLE:
          rc = cvs_stream_disable_cmd_ctrl( ctrl );
          break;

        case APRV2_IBASIC_CMD_DESTROY_SESSION:
          rc = cvs_destroy_session_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_MEDIA_TYPE:
          rc = cvs_stream_set_media_type_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_VAR_VOC_SAMPLING_RATE:
          rc = cvs_stream_set_var_voc_sampling_rate_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG:
          rc = cvs_stream_set_system_config_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_EVT_NOT_READY:
          rc = cvs_stream_vocproc_evt_not_ready_processing( ctrl );
          break;

        case VSS_IVOCPROC_EVT_READY:
          rc = cvs_stream_vocproc_evt_ready_processing( ctrl );
          break;

        case VSS_ISTREAM_CMD_REINIT:
          rc = cvs_stream_reinit_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_START_RECORD:
          /* Handles recording via pseudo port. */
          rc = cvs_stream_start_record_cmd_ctrl( ctrl );
          break;

        case VSS_IRECORD_CMD_START:
          /* Handles recording via any port, pseudo or real. */
          rc = cvs_stream_start_record_v2_cmd_ctrl( ctrl );
          break;

        /* case VSS_ISTREAM_CMD_STOP_RECORD: */
        case VSS_IRECORD_CMD_STOP:
          /* VSS_ISTREAM_CMD_STOP_RECORD is deprecated and
           * VSS_IRECORD_CMD_STOP is introduced. The two
           * commands have the same GUID. They behave the same
           * and share the same command handling function.
           */
          rc = cvs_stream_stop_record_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_START_PLAYBACK:
          /* Handles playback via pseudo port. */
          rc = cvs_stream_start_playback_cmd_ctrl( ctrl );
          break;

        case VSS_IPLAYBACK_CMD_START:
          /* Handles playback via any port, pseudo or real. */
          rc = cvs_stream_start_playback_v2_cmd_ctrl( ctrl );
          break;

        /* case VSS_ISTREAM_CMD_STOP_PLAYBACK: */
        case VSS_IPLAYBACK_CMD_STOP:
          /* VSS_ISTREAM_CMD_STOP_PLAYBACK is deprecated and
           * VSS_IPLAYBACK_CMD_STOP is introduced. The two
           * commands have the same GUID. They behave the same
           * and share the same command handling function.
           */
          rc = cvs_stream_stop_playback_cmd_ctrl( ctrl );
          break;

        case VSS_IVPCM_CMD_START_V2:
          rc = cvs_vpcm_start_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVPCM_CMD_STOP:
          rc = cvs_vpcm_stop_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_SET_MODE:
          rc = cvs_pktexg_set_mode_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_OOB_SET_CONFIG:
          rc = cvs_pktexg_oob_set_config_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_REGISTER_CALIBRATION_DATA_V2: /* BACKWARD COMPATIBILITY */
          rc = cvs_stream_register_calibration_data_v2_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_EVAL_CAL_INDEXING_MEM_SIZE:
          rc = cvs_stream_eval_cal_indexing_mem_size_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_REGISTER_CALIBRATION_DATA_V3:
          rc = cvs_stream_register_calibration_data_v3_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_DEREGISTER_CALIBRATION_DATA: /* BACKWARD COMPATIBILITY */
          rc = cvs_stream_deregister_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_REGISTER_STATIC_CALIBRATION_DATA:
          rc = cvs_stream_register_static_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_DEREGISTER_STATIC_CALIBRATION_DATA:
          rc = cvs_stream_deregister_static_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_PARAM_V2:
          rc = cvs_set_param_v2_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_GET_PARAM_V2:
          rc = cvs_get_param_v2_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_MVM_ATTACH:
          rc = cvs_stream_mvm_attach_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_MVM_DETACH:
          rc = cvs_stream_mvm_detach_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_VOICE_TIMING:
          rc = cvs_stream_set_voice_timing_cmd_ctrl( ctrl );
          break;

        case VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS:
          rc = cvs_listen_for_event_class_cmd_ctrl( ctrl );
          break;

        case VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS:
          rc = cvs_cancel_event_class_cmd_ctrl( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_VOCPROC_AVSYNC_DELAYS:
          rc = cvs_stream_set_vocproc_avsync_delays_cmd_ctrl( ctrl );
          break;

        case VSS_ITTYOOB_CMD_REGISTER:
          rc = cvs_ittyoob_cmd_register( ctrl );
          break;

        case VSS_ITTYOOB_CMD_DEREGISTER:
          rc = cvs_ittyoob_cmd_deregister( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_GET_VOC_PACKET_PROPERTIES:
          rc = cvs_pktexg_mailbox_get_voc_packet_properties_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_SET_CONFIG:
          rc = cvs_pktexg_mailbox_set_config_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_RESET:
          rc = cvs_pktexg_mailbox_reset_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_START:
          rc = cvs_pktexg_mailbox_start_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_GET_MODE:
          rc = cvs_pktexg_get_mode_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_CLEAR_TIME_REFERENCE:
          rc = cvs_pktexg_mailbox_clear_time_reference_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_GET_TIME_REFERENCE:
          rc = cvs_pktexg_mailbox_get_time_reference_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_PROCESS_RX_EXPIRY:
          rc = cvs_pktexg_mailbox_process_rx_expiry_cmd_ctrl( ctrl );
          break;

        case VSS_IPKTEXG_CMD_MAILBOX_DISABLE_RX_EXPIRY_PROCESSING:
          rc = cvs_pktexg_mailbox_disable_rx_expiry_processing_cmd_ctrl( ctrl );
          break;

        case VSS_IHDVOICE_CMD_SET_CONFIG:
          rc = cvs_set_hdvoice_config_cmd_ctrl ( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_VAR_VOC_SAMPLING_RATE:
          rc = cvs_set_var_voc_sampling_rate_ctrl ( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE:
          rc = cvs_set_evs_voc_operating_mode_ctrl ( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE:
          rc = cvs_set_evs_enc_channel_aware_mode_ctrl ( ctrl, TRUE );
          break;

        case VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE:
          rc = cvs_set_evs_enc_channel_aware_mode_ctrl ( ctrl, FALSE );
          break;

		case VSS_IVOLUME_CMD_MUTE_V2:
		  rc = cvs_volume_mute_v2_cmd_processing( ctrl->packet );
		  break;

        default:
          { /* Usupported sequential command. Ack back with unsupported. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            CVS_COMM_ERROR( rc, client_addr );
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = CVS_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = CVS_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

static void cvs_thread_process_ssr_gating_commands ( void )
{
  int32_t rc;
  cvs_pending_control_t* ctrl = &cvs_ssr_pending_ctrl;
  cvs_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case CVS_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &cvs_free_cmd_q, &work_item->link );
        }
        ctrl->state = CVS_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case CVS_PENDING_CMD_STATE_ENUM_EXECUTE:
    case CVS_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_ISSR_CMD_CLEANUP:
          rc = cvs_ssr_cleanup_cmd_ctrl( ctrl );
          break;

        default:
          { /* Usupported sequential command. Ack back with unsupported. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( cvs_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            CVS_COMM_ERROR( rc, client_addr );
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = CVS_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = CVS_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      CVS_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

#if 0
static void cvs_thread_process_state_control ( void )
{
  /* TODO: Run through all CVD sessions in a linked list instead of having
           the sessions executed in every pending command controls.
  */
}
#endif //0

/****************************************************************************
 * IST ROUTINES                                                             *
 ****************************************************************************/

static int32_t cvs_run_high_task ( void )
{
  cvs_high_task_process_nongating_commands( );
  cvs_high_task_process_gating_commands( );

  return APR_EOK;
}

static int32_t cvs_high_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvs_high_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvs_thread_event );

  do
  {
    rc = apr_event_wait( cvs_high_task_event );
   ( void ) cvs_run_high_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvs_high_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t cvs_run_med_task ( void )
{
  cvs_med_task_process_nongating_commands( );
  cvs_med_task_process_gating_commands( );

  return APR_EOK;
}

static int32_t cvs_med_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvs_med_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvs_thread_event );

  do
  {
    rc = apr_event_wait( cvs_med_task_event );
   ( void ) cvs_run_med_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvs_med_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t cvs_run_low_task ( void )
{
  cvs_low_task_process_nongating_commands( );
  cvs_low_task_process_gating_commands( );
  cvs_thread_process_ssr_gating_commands( );

  return APR_EOK;
}

static int32_t cvs_low_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvs_low_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvs_thread_event );

  do
  {
    rc = apr_event_wait( cvs_low_task_event );
   ( void ) cvs_run_low_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvs_low_task_event );
  CVS_PANIC_ON_ERROR( rc );

  cvs_task_state = CVS_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static uint32_t cvs_run ( void )
{
  cvs_run_high_task( );
  cvs_run_med_task( );
  cvs_run_low_task( );

  return APR_EOK;
}

/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static int32_t cvs_init ( void )
{
  uint32_t i;
  int32_t rc;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvs_init()======" );

  { /* Initialize the locks. */
    ( void ) apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &cvs_int_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvs_med_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvs_low_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvs_ref_cnt_lock );
  }
  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &cvs_heapmgr, ( ( void* ) &cvs_heap_pool ),
                          sizeof( cvs_heap_pool ), NULL, NULL );
      /* memheap mustn't be called from interrupt context. No locking is
       * required in task context because all commands are serialized.
       */
  }
  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = cvs_object_table;
    params.total_bits = CVS_HANDLE_TOTAL_BITS_V;
    params.index_bits = CVS_HANDLE_INDEX_BITS_V;
    params.lock_fn = cvs_int_lock_fn;
    params.unlock_fn = cvs_int_unlock_fn;
    ( void ) apr_objmgr_construct( &cvs_objmgr, &params );
      /* TODO: When the ISR and task context contentions becomes a problem
       *       the objmgr could be split into ISR/task and task-only stores
       *       to reduce the bottleneck.
       */
  }
  { /* Initialize the session management. */
    ( void ) cvs_pending_control_init( &cvs_high_task_pending_ctrl );
    ( void ) cvs_pending_control_init( &cvs_med_task_pending_ctrl );
    ( void ) cvs_pending_control_init( &cvs_low_task_pending_ctrl );
  }
  { /* Initialize the SSR management. */
    ( void ) cvs_pending_control_init( &cvs_ssr_pending_ctrl );

    cvs_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
    cvs_ssr_cleanup_cmd_tracking.rsp_cnt = 0;
  }
  { /* Initialize the command queue management. */
    { /* Populate the free command structures. */
      ( void ) apr_list_init_v2( &cvs_free_cmd_q,
                                 cvs_int_lock_fn, cvs_int_unlock_fn );
      for ( i = 0; i < CVS_NUM_COMMANDS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &cvs_cmd_pool[ i ] );
        ( void ) apr_list_add_tail( &cvs_free_cmd_q,
                                    ( ( apr_list_node_t* ) &cvs_cmd_pool[ i ] ) );
      }
    }
    ( void ) apr_list_init_v2(
               &cvs_high_task_incoming_cmd_q,
               cvs_int_lock_fn, cvs_int_unlock_fn
             );
    ( void ) apr_list_init_v2(
               &cvs_med_task_incoming_cmd_q,
               cvs_med_task_lock_fn, cvs_med_task_unlock_fn
             );
    ( void ) apr_list_init_v2(
               &cvs_low_task_incoming_cmd_q,
               cvs_low_task_lock_fn, cvs_low_task_unlock_fn
             );
  }
  { /* Initialize the CVS session tracking list. */
    { /* Populate the free session list. */
      ( void ) apr_list_init_v2( &cvs_session_list_free_q, NULL, NULL );
      for ( i = 0; i < CVS_MAX_NUM_SESSIONS; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &cvs_session_list_pool[ i ] );
        ( void ) apr_list_add_tail(
                   &cvs_session_list_free_q,
                   ( ( apr_list_node_t* ) &cvs_session_list_pool[ i ] ) );
      }
    }

    ( void ) apr_list_init_v2( &cvs_session_q, NULL, NULL );
  }
  { /* Create the CVS IST. */
    rc = apr_event_create( &cvs_thread_event );
    CVS_PANIC_ON_ERROR( rc );

    {
      ( void ) apr_thread_create( &cvs_high_task_handle, CVS_HIGH_TASK_NAME,
                                CVS_HIGH_TASK_PRIORITY, cvs_high_task_stack, CVS_HIGH_TASK_STACK_SIZE,
                                cvs_high_task, NULL );
      ( void ) apr_event_wait( cvs_thread_event );
    }
    {
      ( void ) apr_thread_create( &cvs_med_task_handle, CVS_MED_TASK_NAME,
                                  CVS_MED_TASK_PRIORITY, cvs_med_task_stack, CVS_MED_TASK_STACK_SIZE,
                                  cvs_med_task, NULL );
      ( void ) apr_event_wait( cvs_thread_event );
    }
    {
      ( void ) apr_thread_create( &cvs_low_task_handle, CVS_LOW_TASK_NAME,
                                  CVS_LOW_TASK_PRIORITY, cvs_low_task_stack, CVS_LOW_TASK_STACK_SIZE,
                                  cvs_low_task, NULL );
      ( void ) apr_event_wait( cvs_thread_event );
    }

    rc = apr_event_destroy( cvs_thread_event );
    CVS_PANIC_ON_ERROR( rc );
  }
  { /* Initialize the APR resource (registration is performed last). */
    rc = __aprv2_cmd_register2(
           &cvs_apr_handle, cvs_my_dns, sizeof( cvs_my_dns ), 0,
           cvs_isr_dispatch_fn, NULL, &cvs_my_addr );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvs_init(): registered with APR rc=0x%08X",
                                           rc );
  }

  ( void ) vccm_mmpm_register ( VCCM_CLIENT_ID_CVS );

  return APR_EOK;
}

static int32_t cvs_post_init ( void )
{
  { /* Perform DNS look-ups now after services have registered in init. */
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvs_cvp_dns, sizeof( cvs_cvp_dns ), &cvs_cvp_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvs_vsm_dns, sizeof( cvs_vsm_dns ), &cvs_vsm_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvs_mvm_dns, sizeof( cvs_mvm_dns ), &cvs_mvm_addr );
  }

  return APR_EOK;
}

static int32_t cvs_pre_deinit ( void )
{
  return APR_EOK;
}

static int32_t cvs_deinit ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvs_deinit()======" );

  ( void ) vccm_mmpm_deregister ( VCCM_CLIENT_ID_CVS );

  /* TODO: Ensure no race conditions on deregister. */

  { /* Release the APR resource. */
    ( void ) __aprv2_cmd_deregister( cvs_apr_handle );
  }
  { /* Deinitialize the mailbox timer driver. */
    if ( cvs_is_mailbox_timer_initialized == TRUE )
    {
      ( void ) cvs_mailbox_timer_deinit( );
      cvs_is_mailbox_timer_initialized = FALSE;
    }
  }
  { /* Destroy the CVS IST. */
    {
      ( void ) apr_event_signal_abortall( cvs_low_task_event );

      while ( cvs_task_state != CVS_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvs_low_task_handle );
    }
    {
      ( void ) apr_event_signal_abortall( cvs_med_task_event );

      while ( cvs_task_state != CVS_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvs_med_task_handle );
    }
    {
      ( void ) apr_event_signal_abortall( cvs_high_task_event );

      while ( cvs_task_state != CVS_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvs_high_task_handle );
    }
  }
  { /* Release the session queue management. */
    ( void ) apr_list_destroy( &cvs_session_q );
    ( void ) apr_list_destroy( &cvs_session_list_free_q );
  }
  { /* Release the command queue management. */
    ( void ) apr_list_destroy( &cvs_low_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvs_med_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvs_high_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvs_free_cmd_q );
  }
  { /* Release the SSR management. */
    ( void ) cvs_pending_control_destroy( &cvs_ssr_pending_ctrl );
  }
  { /* Release the session management. */
    ( void ) cvs_pending_control_destroy( &cvs_low_task_pending_ctrl );
    ( void ) cvs_pending_control_destroy( &cvs_med_task_pending_ctrl );
    ( void ) cvs_pending_control_destroy( &cvs_high_task_pending_ctrl );
  }
  { /* Release the object management. */
    ( void ) apr_objmgr_destruct( &cvs_objmgr );
  }
  { /* Release the locks. */
    ( void ) apr_lock_destroy( cvs_ref_cnt_lock );
    ( void ) apr_lock_destroy( cvs_low_task_lock );
    ( void ) apr_lock_destroy( cvs_med_task_lock );
    ( void ) apr_lock_destroy( cvs_int_lock );
  }

  { /* Print out debug info. on object sizes. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "apr_objmgr_object_t size = %d",
                                          sizeof( apr_objmgr_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvs_object_t size = %d",
                                          sizeof( cvs_object_t ) );
  }

  return APR_EOK;
}

APR_EXTERNAL int32_t cvs_call (
  cvs_callindex_enum_t index,
  void* params,
  uint32_t size
)
{
  int32_t rc;

  switch ( index )
  {
  case CVS_CALLINDEX_ENUM_INIT:
    rc = cvs_init( );
    break;

  case CVS_CALLINDEX_ENUM_POSTINIT:
    rc = cvs_post_init( );
    break;

  case CVS_CALLINDEX_ENUM_PREDEINIT:
    rc = cvs_pre_deinit( );
    break;

  case CVS_CALLINDEX_ENUM_DEINIT:
    rc = cvs_deinit( );
    break;

  case CVS_CALLINDEX_ENUM_RUN:
    rc = cvs_run( );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unsupported callindex (%d)",
                                            index );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

