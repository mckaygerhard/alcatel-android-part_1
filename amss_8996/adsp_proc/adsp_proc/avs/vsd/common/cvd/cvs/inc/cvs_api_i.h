#ifndef __CVS_API_I_H__
#define __CVS_API_I_H__

/*
  Copyright (C) 2009-2016 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/cvs/inc/cvs_api_i.h#1 $
  $Author: pwbldsvc $
*/

#include "apr_comdef.h"
#include "adsp_vsm_api.h"
#include "vss_public_if.h"
#include "vss_private_if.h"
#include "cvd_task.h"

/****************************************************************************
 * CVS DEFINES                                                              *
 ****************************************************************************/

#define CVS_VERSION_V ( 0x00000000 )
  /**<
   * TODO: Need to come up with a versioning scheme that will sustain for
   * generations.
   */

#define CVS_HEAP_SIZE_V ( 40 * 1024 )

#define CVS_NUM_COMMANDS_V ( 100 )

#define CVS_HANDLE_TOTAL_BITS_V ( 12 )
#define CVS_HANDLE_INDEX_BITS_V ( 6 ) /* 6 bits = 64 handles. */

#define CVS_MAX_OBJECTS_V ( 1 << CVS_HANDLE_INDEX_BITS_V )

#define CVS_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

/* This macro is to be used to inspect the APR return code whenever CVS
 * calls APR to send a packet.
 *
 * When CVS encounters an APR communication error for sending a packet to an
 * APR service who resides in a different domain than CVS, an error message
 * will be printed and the error is ignored. This is the phase one SSR
 * implementation for handling APR communication errors due to that CVS's
 * client's subsystem encounters SSR. In the future, CVS may need to use a
 * timer based retry mechanism to keep on trying to communicate with the remote
 * client until CVS knows that the remote client is no longer available.
 *
 * When CVS encounters an APR communication error for sending a packet to an
 * APR service who resides in the same domain as CVS, CVS_PANIC_ON_ERROR will
 * be called.
 */
#define CVS_COMM_ERROR( rc, dst_addr ) \
  { if ( ( rc ) && \
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, dst_addr ) != \
           APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, cvs_my_addr ) ) ) \
    { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,"CVS APR comm error 0x%08X, \
                                dst_addr = 0x%04X", rc, dst_addr ); } \
    else \
    { CVS_PANIC_ON_ERROR( rc ); } \
  }

#define CVS_MAX_GENERIC_OBJECTS_PER_SESSION_V ( 32 )

#define CVS_CTRL_TEMP_DATA_CACHE_MAX ( 4 ) /* TODO: clean this up. */

#define CVS_MAX_ATTACHED_VOCPROC_CNT ( 16 )
#define CVS_MAX_SESSION_NAME_SIZE ( 31 )
#define CVS_MAX_NUM_SESSIONS ( 16 )

#define CVS_SESSION_INVALID ( 0xFFFFFFFF )
#define CVS_ATTACHED_VOCPROC_HANDLE_INVALID ( 0xFFFF )

#define CVS_DIRECTION_TX ( 0 )
#define CVS_DIRECTION_RX ( 1 )
#define CVS_DIRECTION_RX_TX ( 2 )

#define CVS_MAX_UI_PROP ( 20 )
#define CVS_MAX_UI_PROP_DATA_LEN ( 12 )
  /**<
   * UI prop data can hold only enable/disable params
   * If UI properties intend to hold more params,
   * this size need to be increased
   */

#define CVS_MAX_OOB_VOC_PACKET_BUFFER_SIZE ( 1932 )
  /**< Buffer size in bytes required to hold the maximum size vocoder packet
       used in out-of-band packet exchange. The size includes 12 bytes of
       header (i.e. 4 bytes timestamp, 4 bytes media type, 4 bytes data size)
       plus the actual data size for the maximum vocoder packet, i.e. PCM_48KHZ
       (1920 bytes). */

#define CVS_NUM_COMMON_CAL_COLUMNS ( 3 ) /* BACKWARD COMPATIBILITY */
  /**< Number of common cal columns is 3: network ID, Tx PP sampling rate, and
       Rx PP sampling rate. If more columns are added, this value has to be
       increased. */

#define CVS_MAX_NUM_MATCHING_COMMON_CAL_ENTRIES ( 1 << CVS_NUM_COMMON_CAL_COLUMNS ) /* BACKWARD COMPATIBILITY */

#define CVS_NUM_STATIC_CAL_COLUMNS ( 7 )
  /**< Number of static cal columns is 7: network ID, Tx PP sampling rate, Rx
       PP sampling rate, Tx vocoder operating mode, Rx vocoder operating mode,
       media ID, and feature. If more columns are added, this value has to be
       increased. */

#define CVS_DEFAULT_DEC_SR ( 8000 )
  /**< Default decoder sample rate. */
#define CVS_DEFAULT_ENC_SR ( 8000 )
  /**< Default encoder sample rate. */

#define CVS_DEFAULT_VAR_VOC_DEC_SAMPLING_RATE ( 48000 )
  /**< Default variable vocoder decoder sample rate. */
#define CVS_DEFAULT_VAR_VOC_ENC_SAMPLING_RATE ( 48000 )
  /**< Default variable vocoder encoder sample rate. */

#define CVS_DEFAULT_RX_PP_SR ( 8000 )
  /**< Default post-proc sample rate. */
#define CVS_DEFAULT_TX_PP_SR ( 8000 )
  /**< Default pre-proc sample rate. */

#define CVS_CACHE_LINE_SIZE ( 128 )
  /**< Cache line size in bytes. */

#define CVS_VOC_PACKET_DURATION_US ( 20000 )
  /**< Duration in microseconds of the media data contained in a single vocoder
       packet. */

#define CVS_MAILBOX_AVSYNC_RX_DELAY_UPDATE_THRESHOLD_US ( 2000 )
  /**< The threshold in microseconds of change in Rx path AVSync delay detected
       during mailbox vocoder packet exchange operation that triggers sending
       the updated Rx path delay to the client (if the client registered for
       AVSync Rx event class). */

#define CVS_MAILBOX_ENC_OFFSET_SAFETY_MARGIN ( 2000 )
  /**< Time in microseconds ahead of the client's mailbox Tx timeline for
       producing an encoder packet. The safety margin is to ensure that there
       is sufficient time for the stream to process an encoder packet such that
       the client's encoding request is completed on time. */

#define CVS_MAILBOX_DEC_REQUEST_OFFSET_SAFETY_MARGIN ( 2000 )
  /**< Time in microseconds behind of the client's mailbox Rx timeline for
       picking up a decoder packet. The safety margin is to ensure that the
       stream picks up a decoder packet at some time after the client
       provides a packet, to avoid read and write race condition due to
       timing jitters. */

#define CVS_MAILBOX_DEC_OFFSET_SAFETY_MARGIN ( 2000 )
  /**< Time in microseconds between when VSM request for a decoder packet to
       when VSM must start decoding a packet. The safety margin is to ensure
       that there is sufficient time for CVS to process (such as error
       checking) a decoder packet and deliver (such as mem_cpy and sending an
       OOB packet exchange event via APR) a packet to VSM. */

#define CVS_MAILBOX_DEC_EXPIRY_HANDLING_SAFETY_MARGIN ( 1000 )
  /**< Time in microseconds between the expected time of receiving a decoder
       packet request from VSM to when CVS begins dropping expired decoding
       request during call setup time. This is used for dropping expired
       decoding request during call setup time before the decoder becomes
       active. */

#define CVS_MAILBOX_DEC_WINDOW ( CVS_VOC_PACKET_DURATION_US + 1000 )
  /**< The decoding window in microseconds. A decoding request with a timestamp
       of more than a decoding window in the past is considered as an expired
       request and will be dropped. The decoding window is
       CVS_VOC_PACKET_DURATION_US plus a 1ms safety margin. */

#define CVS_MAILBOX_VOC_PACKET_ERROR_STATUS_MASK ( 1 )
  /**< Mailbox vocoder packet error status bit mask. */

  /**< Default values for any stream property as long as 
       the property is not set by the full control client. */
#define CVS_STREAM_PROPERTY_NOT_SET_UINT32 ( 0xFDFDFDFD )

#define CVS_STREAM_PROPERTY_NOT_SET_UINT8 ( 0xFD )

#define CVS_STREAM_VOC_BANDWIDTH_EVT_NOT_SET ( 0xFF )
  /**< Vocoder Bandwidth Event sent by VSM_EVT_VOC_OPERATING_MODE_UPDATE
       is not yet received. */

/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

typedef enum cvs_thread_state_enum_t
{
  CVS_THREAD_STATE_ENUM_INIT,
  CVS_THREAD_STATE_ENUM_READY,
  CVS_THREAD_STATE_ENUM_EXIT
}
  cvs_thread_state_enum_t;

typedef enum cvs_thread_priority_enum_t
{
  CVS_THREAD_PRIORITY_ENUM_HIGH,
  CVS_THREAD_PRIORITY_ENUM_MED,
  CVS_THREAD_PRIORITY_ENUM_LOW
}
  cvs_thread_priority_enum_t;

typedef enum cvs_voc_property_enum_t
{
  CVS_VOC_PROPERTY_ENUM_DTX,
  CVS_VOC_PROPERTY_ENUM_RATE_MODULATION,
  CVS_VOC_PROPERTY_ENUM_MINMAX_RATE,
  CVS_VOC_PROPERTY_ENUM_ENC_RATE,
  CVS_VOC_PROPERTY_ENUM_CHANNEL_AWARE_MODE,
  CVS_VOC_PROPERTY_ENUM_ENC_OPERATING_MODE
}
  cvs_voc_property_enum_t;

/****************************************************************************
 * CVS WORK QUEUE DEFINITIONS                                               *
 ****************************************************************************/

typedef struct cvs_work_item_t cvs_work_item_t;

struct cvs_work_item_t
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
};

/****************************************************************************
 * COMMAND RESPONSE FUNCTION TABLE                                          *
 ****************************************************************************/

typedef void ( *cvs_event_handler_fn_t ) ( aprv2_packet_t* packet );

typedef enum cvs_response_fn_enum_t
{
  CVS_RESPONSE_FN_ENUM_ACCEPTED,
  CVS_RESPONSE_FN_ENUM_RESULT,
  CVS_RESPONSE_FN_ENUM_GET_PARAM,
  CVS_RESPONSE_FN_ENUM_MAP_MEMORY,
  CVS_RESPONSE_FN_ENUM_GET_KPPS,
  CVS_RESPONSE_FN_ENUM_GET_AVSYNC_DELAY,
  CVS_RESPONSE_FN_ENUM_INVALID,
  CVS_RESPONSE_FN_ENUM_MAX = CVS_RESPONSE_FN_ENUM_INVALID
}
  cvs_response_fn_enum_t;

/**
 * Pending commands may load different sets of response and event handlers to
 * complete each job. The response function table is equivalent to the state
 * design pattern. The state context is stored in the pending command control.
 * Pending commands can be as simple or as complex as required.
 */
typedef cvs_event_handler_fn_t cvs_response_fn_table_t[ CVS_RESPONSE_FN_ENUM_MAX ];

/****************************************************************************
 * SESSION CONTROL DEFINITIONS                                              *
 ****************************************************************************/

typedef enum cvs_state_enum_t
{
  CVS_STATE_ENUM_UNINITIALIZED,
    /**< Reserved. */
  CVS_STATE_ENUM_RESET_ENTRY,
    /**< Move into or out of reset. */
  CVS_STATE_ENUM_RESET,
    /**< The session resource is not acquired. */
  CVS_STATE_ENUM_IDLE_ENTRY,
    /**< Move into or out of idle. */
  CVS_STATE_ENUM_IDLE,
    /**< The session resource is ready to run. */
  CVS_STATE_ENUM_RUN_ENTRY,
    /**< Move into or out of run. */
  CVS_STATE_ENUM_RUN,
    /**< The session resource is running. */
  CVS_STATE_ENUM_ERROR_ENTRY,
    /**< Performing error recovery. */
  CVS_STATE_ENUM_ERROR,
    /**< The session resource is unusable and should be destroyed. */
  CVS_STATE_ENUM_INVALID
    /**< Reserved. */
}
  cvs_state_enum_t;

typedef enum cvs_goal_enum_t
{
  CVS_GOAL_ENUM_UNINITIALIZED,
  CVS_GOAL_ENUM_NONE,
  CVS_GOAL_ENUM_CREATE,
  CVS_GOAL_ENUM_REINIT,
  CVS_GOAL_ENUM_DESTROY,
  CVS_GOAL_ENUM_DISABLE,
  CVS_GOAL_ENUM_STANDBY,
  CVS_GOAL_ENUM_ENABLE,
  CVS_GOAL_ENUM_ATTACH_VOCPROC,
  CVS_GOAL_ENUM_DETACH_VOCPROC,
  CVS_GOAL_ENUM_INVALID,
}
  cvs_goal_enum_t;

typedef enum cvs_action_enum_t
{
  CVS_ACTION_ENUM_UNINITIALIZED,
    /**< Common actions. */
  CVS_ACTION_ENUM_NONE,
    /**< The first action has not started for a goal from any state. */
  CVS_ACTION_ENUM_COMPLETE,
    /**<
     * Reached the last action for a goal from a state. A multi-action goal
     * that starts from and ends in the same state may require a COMPLETE
     * action to properly differentiate a terminate signal.
     */
  CVS_ACTION_ENUM_CONTINUE,
    /**<
     * For multi-state goals, the last action from each state should set to
     * CONINTUE. This indicates to the next state that a goal is continuing
     * its operation from a previous state. Usually the previous state is
     * known given the current state and the continued goal. New actions can
     * be created to help discriminate the direction from where goals come
     * from as required.
     */
  CVS_ACTION_ENUM_UPDATE_VSM_ATTACH_TABLE,
  CVS_ACTION_ENUM_SET_CLOCKS,
  CVS_ACTION_ENUM_REVERT_CLOCKS,
  CVS_ACTION_ENUM_CREATE_SESSION,
  CVS_ACTION_ENUM_REINIT,
  CVS_ACTION_ENUM_SET_MEDIA_TYPE,
  CVS_ACTION_ENUM_SET_VAR_VOC_SAMPLING_RATE,
  CVS_ACTION_ENUM_SET_CACHED_DTX_MODE,
  CVS_ACTION_ENUM_SET_CACHED_RATE_MODULATION,
  CVS_ACTION_ENUM_SET_CACHED_MINMAX_RATE,
  CVS_ACTION_ENUM_SET_CACHED_ENC_RATE,
  CVS_ACTION_ENUM_SET_CACHED_CHANNEL_AWARE_MODE,
  CVS_ACTION_ENUM_SET_CACHED_ENC_OPERATING_MODE,
  CVS_ACTION_ENUM_REGISTER_VOC_OP_MODE_UPDATE_EVENT,
  CVS_ACTION_ENUM_WAIT_FOR_VOC_OP_MODE_UPDATE,
  CVS_ACTION_ENUM_OPEN_AVTIMER,
  CVS_ACTION_ENUM_SET_PACKET_EXCHANGE_MODE,
  CVS_ACTION_ENUM_SET_OOB_PACKET_EXCHANGE_CONFIG,
  CVS_ACTION_ENUM_RECONFIG_MAILBOX_TIMING,
  CVS_ACTION_ENUM_SET_VOICE_TIMING,
  CVS_ACTION_ENUM_SET_TX_MUTE,
  CVS_ACTION_ENUM_SET_RX_MUTE,  
  CVS_ACTION_ENUM_SET_UI_PROPERTIES,
  CVS_ACTION_ENUM_SEND_AVSYNC_RX_DELAY_NOTIFICATION,
  CVS_ACTION_ENUM_SEND_EVS_BANDWIDTH_NOTIFICATION,
  CVS_ACTION_ENUM_SEND_READY_NOTIFICATION,
  CVS_ACTION_ENUM_GET_MAX_MAILBOX_PKT_SIZE,
  CVS_ACTION_ENUM_RUN,
  CVS_ACTION_ENUM_STOP,
  CVS_ACTION_ENUM_RESET_MAILBOX_BOOK_KEEPING,
  CVS_ACTION_ENUM_SEND_NOT_READY_NOTIFICATION,
  CVS_ACTION_ENUM_DESTROY_SESSION,
  CVS_ACTION_ENUM_MAP_SHARED_MEMORY,
  CVS_ACTION_ENUM_CLOSE_AVTIMER,
  CVS_ACTION_ENUM_UNMAP_SHARED_MEMORY,
  CVS_ACTION_ENUM_START_RECORD,
  CVS_ACTION_ENUM_STOP_RECORD,
  CVS_ACTION_ENUM_START_PLAYBACK,
  CVS_ACTION_ENUM_STOP_PLAYBACK,
  CVS_ACTION_ENUM_RX_DTMF_DETECT,
  CVS_ACTION_ENUM_INVALID
}
  cvs_action_enum_t;

typedef struct cvs_control_t cvs_control_t;
struct cvs_control_t
{
  uint32_t transition_job_handle;
  uint32_t pendjob_handle;

  cvs_goal_enum_t goal;
    /**<
     * The goal field is the current goal for the state machine. Goals are
     * defined to complete one at a time.
     *
     * CAUTION: The goal field is intended for the user of the state machine
     *          to indicate the goal. Any attempt by the state machine to
     *          manipulate the goal will lead to live locking and
     *          unmaintainable code.
     */
  cvs_state_enum_t state;
    /**<
     * The state field is the current state of the state machine.
     *
     * CAUTION: The state field is intended for the state machine to keep
     *          track of its current state. Any attempt to manipulate the
     *          state field from outside the state machine will lead to
     *          live locking and unmaintainable code.
     */
  cvs_action_enum_t action;
    /**<
     * The action field is the next action to perform by the state machine.
     *
     * CAUTION: The action field is intended by the state machine to keep
     *          track of the series of steps to transition out of the current
     *          state or to complete a goal. Any attempt to manipulate the
     *          action field from outside the state machine will lead to
     *          live locking, system panics due to unhandled actions in the
     *          state, and unmaintainable code.
     */
  uint32_t status;
    /**<
     * The status field reports the status of the last action.
     */
};

/****************************************************************************
 * CVD OBJECT DEFINITIONS                                                   *
 ****************************************************************************/

typedef struct cvs_object_header_t cvs_object_header_t;
typedef struct cvs_session_object_t cvs_session_object_t;
typedef struct cvs_indirection_object_t cvs_indirection_object_t;
typedef struct cvs_simple_job_object_t cvs_simple_job_object_t;
typedef struct cvs_track_job_object_t cvs_track_job_object_t;
typedef struct cvs_sequencer_job_object_t cvs_sequencer_job_object_t;
typedef union cvs_object_t cvs_object_t;

/****************************************************************************
 * PENDING COMMAND CONTROL DEFINITIONS                                      *
 ****************************************************************************/

typedef enum cvs_pending_cmd_state_enum_t
{
  CVS_PENDING_CMD_STATE_ENUM_FETCH,
    /**< Fetch the next pending command to execute. */
  CVS_PENDING_CMD_STATE_ENUM_EXECUTE,
    /**< Execute the current pending command for the first time. */
  CVS_PENDING_CMD_STATE_ENUM_CONTINUE
    /**< Continue executing the current pending command. */
}
  cvs_pending_cmd_state_enum_t;

/**
 * The pending command control structure stores information used to process
 * pending commands serially.
 *
 * Pending commands have state and are executed one at a time until
 * completion. A gated pending command gates all the pending commands that
 * follows it. A gated pending command should employ abort timers to allow the
 * system to make progress.
 *
 * Aborting critical pending commands can leave the system in a bad state. It
 * is recommended that (at the very least) the configurations of the aborted
 * critical pending commands be saved so that on error recovery the proper
 * configurations could be restored.
 */
typedef struct cvs_pending_control_t
{
  apr_list_t cmd_q;
    /**< The pending (cvs_work_item_t) command queue. */
  cvs_pending_cmd_state_enum_t state;
    /**<
     * The current state of the pending command control.
     *
     * This variable is managed by the pending command processor. The
     * individual pending command controls indicates to the pending command
     * processor to complete or to delay the completion of the current
     * pending command.
     */
  aprv2_packet_t* packet;
    /**<
     * The current (command) packet being processed.
     */
  cvs_object_t* pendjob_obj;
    /**<
     * The pendjob_obj is a temporary storage for the current pending
     * command.
     */
  uint32_t temp_data[CVS_CTRL_TEMP_DATA_CACHE_MAX];
}
  cvs_pending_control_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE COMMON OBJECT DEFINITIONS                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef enum cvs_object_type_enum_t
{
  CVS_OBJECT_TYPE_ENUM_UNINITIALIZED,
  CVS_OBJECT_TYPE_ENUM_SESSION,
  CVS_OBJECT_TYPE_ENUM_INDIRECTION,
  CVS_OBJECT_TYPE_ENUM_SIMPLE_JOB,
  CVS_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
  CVS_OBJECT_TYPE_ENUM_INVALID
}
  cvs_object_type_enum_t;

struct cvs_object_header_t
{
  uint32_t handle;
    /**< The handle to the associated apr_objmgr_object_t instance. */
  cvs_object_type_enum_t type;
    /**<
     * The object type defines the actual derived object.
     *
     * The derived object can be any custom object type. A session or a
     * command are two such custom object types. A free object entry is set
     * to CVS_OBJECT_TYPE_ENUM_FREE.
     */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SESSION OBJECT                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* TODO: The generic item is now only used for indirection obj. It should
 * be used for storing the vpm handles that are attached to the vsm.
 */

/**
 * cvs_generic_item_t is a generic linked list node data structure whose
 * intent is to maximize recycling of linked list node storage space.
 * cvs_generic_item_t linked list nodes contain a handle. The handle is
 * dynamically re-purposed based on the use case.
 *
 * An example of this is in the session object where there is a free_items_q
 * of generic items. The free_item_q are dequeued and enqueued into the
 * indirection_q (storing MVM indirection objects) or the vpm_handle_q
 * (storing vpm session handles TODO: TBD on the name once the attach/detach vocproc
 * is cleaned up.). Once the item storage is no longer needed in the respective
 * list, the free item is moved back into the free_item_q.
 */
typedef struct cvs_generic_item_t
{
  apr_list_node_t link;

  uint32_t handle;
    /**< Any custom handle. */
}
  cvs_generic_item_t;

typedef struct cvs_common_cal_info_t /* BACKWARD COMPATIBILITY. */
{
  bool_t is_registered;
  bool_t is_evaluated;
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the stream calibration data need to be applied.
      * Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of calibration data being registered with the stream.
      *
      *   - upon the stream has been re-initialized and there is calibration
      *     data currently being registered with the stream.
      *
      *   - upon the stream receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command
      *     and any of the following parameters is different between the stream
      *     active_set and target_set: network_id, tx_pp_sr, rx_pp_sr, and
      *     there is calibration data currently registered.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon calibration being deregistered.
      */
  uint32_t required_index_mem_size;
  cvd_cal_table_handle_t* table_handle;
  uint32_t vsm_mem_handle;
  cvd_cal_entry_t matching_entries[ ( CVS_MAX_NUM_MATCHING_COMMON_CAL_ENTRIES *
                                      sizeof( cvd_cal_entry_t ) ) ];
  uint32_t num_matching_entries;
  uint32_t set_param_rsp_cnt;
  uint32_t set_param_failed_rsp_cnt;
}
  cvs_common_cal_info_t;

typedef struct cvs_static_cal_info_t
{
  bool_t is_registered;
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the stream static calibration data need to be
      * applied. Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of static calibration data being registered with the
      *     stream.
      *
      *   - upon the stream has been re-initialized and there is static
      *     calibration data currently being registered with the stream.
      *
      *   - upon the stream receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command
      *     and any of the following parameters is different between the stream
      *     active_set and target_set: network_id, tx_pp_sr, rx_pp_sr,
      *     tx_voc_op_mode, rx_voc_op_mode, media ID, feature, and there is
      *     static calibration data currently registered.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon static calibration being deregistered.
      */
  uint32_t table_handle;
  uint32_t query_handle;
  uint8_t matching_entries[ CVD_CAL_GET_REQUIRED_MATCHING_ENTRIES_BYTE_SIZE( CVS_NUM_STATIC_CAL_COLUMNS ) ];
    /**<
      * Book-keeping buffer to be provided to CVD CAL utility for maintaining
      * the location (address or offset) of the matching calibration entries in
      * the calibration table; so that multiple calls of cvd_cal_query with the
      * the same query_handle can simply return the calibration values based on
      * the cached matching entries rather than performing a fresh search on
      * every cvd_cal_query call.
      */
  cvd_cal_column_t query_key_columns[ CVS_NUM_STATIC_CAL_COLUMNS ];
}
  cvs_static_cal_info_t;

typedef struct cvs_vocproc_table_t
{
  /* Replicate setting from VSM for this session. */
  uint16_t tx_device_cnt;
  uint16_t rx_device_cnt;
  uint16_t tx_device_handle[CVS_MAX_ATTACHED_VOCPROC_CNT];
  uint16_t rx_device_handle[CVS_MAX_ATTACHED_VOCPROC_CNT];

  uint16_t cvp_handles[CVS_MAX_ATTACHED_VOCPROC_CNT];
  bool_t   cvp_isready[CVS_MAX_ATTACHED_VOCPROC_CNT];

  uint16_t cvp_ready_cnt;

}
  cvs_session_vocproc_table_t;

typedef struct cvs_dtmf_settings_t
{
  uint16_t direction;
  int16_t  mixing;
  uint16_t tone1;
  uint16_t tone2;
  int32_t  duration;
}
  cvs_session_dtmf_settings_t;

typedef enum cvs_enable_state_enum_t
{
    CVS_DISABLED = 0,
    CVS_ENABLED = 1
}
  cvs_enable_state_enum_t;

typedef struct cvs_recording_settings_t
{
  uint32_t rx_tap_point;
  uint32_t tx_tap_point;
  uint16_t port_id;
  uint32_t mode;
  cvs_enable_state_enum_t enable_flag;
}
  cvs_recording_settings_t;

typedef struct cvs_playback_settings_t
{
  uint16_t port_id;
  cvs_enable_state_enum_t  enable_flag;
}
  cvs_playback_settings_t;

typedef struct cvs_mute_settings_t
{
  uint16_t tx_mute_flag;
  uint16_t rx_mute_flag;
  uint32_t tx_ramp_duration;
  uint32_t rx_ramp_duration;  
}
  cvs_mute_settings_t;
  
typedef struct cvs_session_settings_t
{
  uint16_t direction;
  uint32_t media_id;
  vss_icommon_cmd_set_system_config_t system_config;
  cvs_session_vocproc_table_t attach_table;
  cvs_recording_settings_t record;
  cvs_playback_settings_t playback;
  cvs_mute_settings_t mute;
  vss_istream_cmd_set_voice_timing_t voice_timing;
  /* TODO: The following need to be cleaned up along with the clean-up for
   * vocproc attachment handling. The following are used to store the vocproc
   * handle and the stream handle (indirection handle) that are needed to be
   * attached together.
   */
  uint16_t attach_detach_vocproc_handle;
  uint16_t attach_detach_stream_handle;
}
  cvs_session_settings_t;

typedef struct cvs_set_dtmf_gen_settings_t
{
  uint16_t client_addr;
  uint16_t client_port;
  uint16_t cvs_port;
}
  cvs_set_dtmf_gen_settings_t;

typedef struct cvs_set_rx_dtmf_detect_settings_t
{
  uint16_t client_addr;
  uint16_t client_port;
  uint16_t cvs_port;
  cvs_enable_state_enum_t enable_flag;
}
  cvs_set_rx_dtmf_detect_settings_t;

typedef struct cvs_ui_prop_cache_t
{
  uint32_t data_len;
  uint32_t num_ui_prop;
  uint8_t data [ ( ( CVS_MAX_UI_PROP_DATA_LEN +
                     sizeof( vss_icommon_param_data_t ) ) *
                   CVS_MAX_UI_PROP ) ];
    /**<
     * The size of the data is
     * ( UI prop data length + header ) * max number UI props.
     */
}
  cvs_ui_prop_cache_t;

/* CVS heap shared with VSM. This heap stores the cached data:
 * UI property.
 */
typedef struct cvs_shared_heap_t
{
  uint32_t vsm_mem_handle;
  cvs_ui_prop_cache_t ui_prop_cache;
}
  cvs_shared_heap_t;

typedef struct cvs_vpcm_info_t
{
  bool_t is_enabled;
    /**< Flag to indicate whether vpcm is enabled or not. */
  uint16_t client_addr;
    /**< Address of VPCM client. */
  uint16_t client_handle;
    /**< Handle for VPCM client. */
  uint16_t cvs_handle;
    /**< Handle (indirection object) for CVS. */
  uint32_t mem_handle;
    /**< VPCM Client's memory handle. */
}
  cvs_vpcm_info_t;

typedef struct cvs_oob_packet_exchange_info_t
{
  bool_t is_configured;
    /**< Flag indicates if OOB packet exchange is properly configured. */
  vsm_config_packet_exchange_t config;
    /**< Packet exchange configuration set on VSM. */
}
  cvs_oob_packet_exchange_info_t;

typedef struct cvs_mailbox_packet_exchange_config_t
{
  uint32_t cvs_mem_handle;
    /**< Handle to the shared memory used for mailbox packet exchange between
         CVS and its client. */
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* tx_circ_buf;
    /**< Tx path vocoding request circular buffer. */
  uint32_t tx_circ_buf_mem_size;
    /**< Tx path vocoding request circular buffer memory size in bytes. */
  vss_ipktexg_mailbox_voc_req_circ_buffer_t* rx_circ_buf;
    /**< Rx path vocoding request circular buffer. */
  uint32_t rx_circ_buf_mem_size;
    /**< Rx path vocoding request circular buffer memory size in bytes. */
  uint32_t vsm_mem_handle;
    /**< Handle to the shared memory used for OOB packet exchange between CVS
         and VSM. CVS uses OOB packet exchange with VSM, when CVS operates in
         mailbox packet exchange mode with its client. */
  uint8_t* oob_enc_buf;
    /**< Encoder buffer used for OOB packet exchange between CVS and VSM. */
  uint8_t* oob_dec_buf;
    /**< Decoder buffer used for OOB packet exchange between CVS and VSM. */
  uint32_t max_enc_pkt_size;
    /**< The maximum possible encoder packet size for the media ID currently
         configured on the stream. */
  uint64_t tx_ref_timestamp_us;
    /**< The client's Tx path packet exchange time reference in microseconds. */
  uint64_t rx_ref_timestamp_us;
    /**< The client's Rx path packet exchange time reference in microseconds. */
  uint64_t request_to_start_timestamp_us;
    /**< The timestamp in microseconds of when the mailbox is requested to be
         started. This is treated as time zero of the packet exchange timeline
         and will be used to handle timer wrap around (which is possible even
         for 64 bit timers, since the timer may not necessarily start at 0 at
         boot up, but at some random value). */
  uint32_t rx_req_and_dequeue_time_diff_us;
    /**< Time difference in microseconds between the client's decoding request
         timestamp and the time when the request is dequeued from the Rx
         circular buffer. A change in this value results in a change in the Rx
         path AVSync delay. */
  bool_t is_start_requested;
    /**< Flag indicates whether CVS has requested the client to start packet
         exchange. */
  bool_t is_reseted;
    /**< Flag indicates whether the client has reseted the packet exchange. */
  bool_t is_time_ref_received;
    /**< Flag indicates whether the client's packet exchange time reference has
         been received. */
  bool_t is_rx_expiry_proc_disabled;
    /**< Flag indicates whether Rx expiry processing is disabled. */
  uint32_t rx_expiry_timer_handle;
    /**< Timer for processing the expired decoding requests during call setup
         time before the decoder becomes active. */
  uint64_t rx_expiry_timestamp_us;
    /**< Expiry timestamp in microseconds for decoding requests. Any decoding
         request with a timestamp that is in the past than the expiry timestamp
         will be dropped. This is used for dropping expired decoding request
         during call setup time before the decoder becomes active. */
}
  cvs_mailbox_packet_exchange_config_t;

/* Mailbox packet exchange statistics for debugging purpose. */
typedef struct cvs_mailbox_packet_exchange_stats_t
{
  uint32_t num_no_rx_req_in_buf;
    /**< Number of times that no decoding request found in the Rx circular
         buffer upon decoder requesting for a packet, since the decoder is
         started. */
  uint32_t num_expired_rx_req_dropped;
    /**< Number of times that expired decoding request found in the Rx circular
         buffer upon decoder requesting for a packet, since the decoder is
         started. */
  uint32_t num_future_rx_req_skipped;
    /**< Number of times that future decoding request found in the Rx circular
         buffer upon decoder requesting for a packet, since the decoder is
         started. */
  uint32_t num_invalid_rx_req_dropped;
    /**< Number of times that invalid decoding request found in the Rx circular
         buffer upon decoder requesting for a packet, since the decoder is
         started. */
  uint32_t num_error_status_rx_req_dropped;
    /**< Number of times that an error status (i.e. error status bit is set)
         decoding request found in the Rx circular buffer upon decoder
         requesting for a packet, since the decoder is started. */
  uint32_t num_no_tx_req_in_buf;
    /**< Number of times that no encoding request found in the Tx circular
         buffer upon encoder generating a packet, since the encoder is
         started. */
  uint32_t num_invalid_tx_req_dropped;
    /**< Number of times that invalid encoding request found in the Tx circular
         buffer upon encoder generating a packet, since the encoder is
         started. */
}
  cvs_mailbox_packet_exchange_stats_t;

typedef struct cvs_mailbox_packet_exchange_info_t
{
  bool_t is_configured;
    /**< Flag indicates if mailbox packet exchange is properly configured. */
  cvs_mailbox_packet_exchange_config_t config;
    /**< Mailbox packet exchange configuration. */
  cvs_mailbox_packet_exchange_stats_t stats;
    /**< Mailbox packet exchange statistics for debugging purpose. */
}
  cvs_mailbox_packet_exchange_info_t;

typedef struct cvs_packet_exchange_info_t
{
  uint32_t mode;
    /**< Vocoder packet exchange mode.
         In band = VSS_IPKTEXG_MODE_IN_BAND.
         OOB = VSS_IPKTEXG_MODE_OUT_OF_BAND.
         Mailbox = VSS_IPKTEXG_MODE_MAILBOX */
  cvs_oob_packet_exchange_info_t oob_info;
    /**< OOB packet exchange info. Applicable only if the mode is set to
         VSS_IPKTEXG_MODE_OUT_OF_BAND. */
  cvs_mailbox_packet_exchange_info_t mailbox_info;
    /**< Mailbox packet exchange info. Applicable only if the mode is set to
         VSS_IPKTEXG_MODE_MAILBOX. */
}
  cvs_packet_exchange_info_t;

typedef struct cvs_kpps_info_t
{
  uint32_t enc;
  uint32_t dec;
  uint32_t dec_pp;
}
  cvs_kpps_info_t;

typedef struct cvs_tty_info_t
{
  uint16_t ittyoob_client_addr;
    /**< Address of cleint who registers for ITTYOOB. */
  uint16_t ittyoob_client_port;
    /**< Port of cleint who registers for ITTYOOB. */
  uint32_t tty_mode;
    /**< Represent TTY mode set for TTY device. */
  bool_t is_ittyoob_registered;
    /**< flag = FALSE is unregistered indicating In band TTY
         and flag = TRUE is registered indicating Out of band TTY. */
}
  cvs_tty_info_t;

typedef struct cvs_eamr_mode_change_notification_info_t
{
  bool_t is_enabled;
    /**< Flag to indicate whether eAMR mode change notification is enabled. */
  uint16_t client_addr;
    /**< Address of the client who listens to the eAMR mode change
         notification. */
  uint16_t client_port;
    /**< Port of the client who listens to the eAMR mode change
         notification. */
  uint32_t mode;
    /**< Last known eAMR mode. */
}
  cvs_eamr_mode_change_notification_info_t;

typedef struct cvs_iavsync_client_info_t
{
  bool_t is_enabled;
    /**< Flag to indicate whether client has registered for
         VSS_IAVSYNC_EVENT_CLASS_RX. */
  uint16_t client_addr;
    /**< Address of the client who listens to the IAVSYNC RX Delay
         notification. */
  uint16_t client_port;
    /**< Port of the client who listens to the IAVSYNC RX Delay
         notification. */
}
  cvs_iavsync_client_info_t;
  
typedef struct cvs_evs_bandwidth_change_notification_info_t
{
  bool_t is_enabled;
    /**< Flag to indicate whether EVS BW change notification is enabled. */
  uint16_t client_addr;
    /**< Address of the client who listens to the EVS bandwidth
         change notification. */
  uint16_t client_port;
    /**< Port of the client who listens to the EVS bandwidth change
         notification. */
  uint8_t last_received_rx_bandwidth;
    /**< Last known EVS Rx bandwidth received from VSM. */
  uint8_t last_sent_rx_bandwidth;
    /**< Last known EVS Rx bandwidth sent to registered client. */    
}
  cvs_evs_bandwidth_change_notification_info_t;  

typedef struct cvs_iavsync_delays_info_t
{
  uint32_t stream_rx_algorithmic_delay;
    /**< Stream algorithmic RX delay. */
  uint32_t stream_tx_algorithmic_delay;
    /**< Stream algorithmic TX delay. */
  uint32_t total_rx_delay;
    /**< Total normalized vocproc + stream rx delay. */
  uint32_t total_tx_delay;
    /**< Total normalized vocproc + stream rx delay. */
}
  cvs_iavsync_delays_info_t;

typedef struct cvs_packet_logging_info_t
{
  uint32_t voice_call_num;
  uint32_t rx_packet_seq_num;
  uint32_t tx_packet_seq_num;
}
  cvs_packet_logging_info_t;

typedef struct cvs_voc_operating_mode_info_t
{
  bool_t is_rx_mode_received;
    /**<
      * Indicates whether the VSM_EVT_VOC_OPERATING_MODE_UPDATE event
      * containing the initial Rx path vocoder mode has been received from VSM.
      */
  bool_t is_tx_mode_received;
    /**<
      * Indicates whether the VSM_EVT_VOC_OPERATING_MODE_UPDATE event
      * containing the initial Tx path vocoder mode has been received from VSM.
      */
  uint32_t rx_mode;
    /**< VSM reported Rx vocoder operating mode. */
  uint32_t tx_mode;
    /**< VSM reported Tx vocoder operating mode. */
}
  cvs_voc_operating_mode_info_t;

typedef struct cvs_hdvoice_enable_param_info_t
{
  voice_param_data_t param_hdr;
    /**< VSM reported Tx vocoder operating mode. */
  uint16_t enable;
    /**< Enablement value. Supported values: \n
         - 0 -- Disabled
         - 1 -- Enabled */
  uint16_t reserved;
}
  cvs_hdvoice_enable_param_info_t;

typedef struct cvs_voc_property_qcelp13k_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
  uint32_t reduced_rate_mode;
    /**< Reduced_rate_mode of the encoder rate. */
}
  cvs_voc_property_qcelp13k_t;

typedef struct cvs_voc_property_evrc_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
  uint32_t reduced_rate_mode;
    /**< Reduced_rate_mode of the encoder rate. */
}
  cvs_voc_property_evrc_t;

typedef struct cvs_voc_property_4gvnb_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
}
  cvs_voc_property_4gvnb_t;

typedef struct cvs_voc_property_4gvwb_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
}
  cvs_voc_property_4gvwb_t;

typedef struct cvs_voc_property_4gvnw_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_4gvnw_t;

typedef struct cvs_voc_property_4gvnw2k_t
{
  uint32_t rate;
    /**< Average vocoder rate. */
  uint32_t min_rate;
    /**< Lower boundary of the encoder rate. */
  uint32_t max_rate;
    /**< Upper boundary of the encoder rate. */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_4gvnw2k_t;

typedef struct cvs_voc_property_amr_t
{
  uint32_t rate;
    /**< AMR encoder rate. */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_amr_t;

typedef struct cvs_voc_property_amrwb_t
{
  uint32_t rate;
    /**< AMRWB encoder rate. */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_amrwb_t;

typedef struct cvs_voc_property_eamr_t
{
  uint32_t rate;
    /**< EAMR encoder rate. */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_eamr_t;

typedef struct cvs_voc_property_efr_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_efr_t;

typedef struct cvs_voc_property_fr_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_fr_t;

typedef struct cvs_voc_property_hr_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_hr_t;

typedef struct cvs_voc_property_g711_alaw_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_g711_alaw_t;

typedef struct cvs_voc_property_g711_mulaw_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_g711_mulaw_t;

typedef struct cvs_voc_property_g711_linear_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_g711_linear_t;

typedef struct cvs_voc_property_g729_t
{
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_g729_t;

typedef struct cvs_voc_property_evs_t
{
  uint8_t mode;
    /**< Operating bit-rate of the vocoder encoder. */
  uint8_t bandwidth;
    /**<  Operating audio bandwidth of the vocoder encoder. */
  uint8_t channel_aware_enabled;
    /**< Inicates whether channel aware mode is enabled.
       1 - Enabled,
       0 - Disabled.
       CVS_STREAM_PROPERTY_NOT_SET_UINT8 - value not set. */
  uint8_t fec_offset;
    /**< Specifies the forward-error correction offset.
         @values 2, 3, 5, 7 */
  uint8_t fer_rate;
    /**< Specifies FER rate threshold to LOW (0) or HIGH (1). */
  uint32_t dtx_mode;
    /**< DTX mode value. */
}
  cvs_voc_property_evs_t;

typedef struct cvs_enc_channel_aware_mode_params_t
{
  uint8_t channel_aware_enabled;
    /**< Inicates whether channel aware mode is enabled.
       1 - Enabled,
       0 - Disabled.
       CVS_STREAM_PROPERTY_NOT_SET_UINT8 - value not set. */
  uint8_t fec_offset;
    /**< Specifies the forward-error correction offset.
         @values 2, 3, 5, 7  */
  uint8_t fer_rate;
    /**< Specifies FER rate threshold to LOW (0) or HIGH (1). */
}
  cvs_enc_channel_aware_mode_params_t;

struct cvs_session_object_t
{
  cvs_object_header_t header;

  /* Private housekeeping variables. */
  cvs_generic_item_t generic_pool[ CVS_MAX_GENERIC_OBJECTS_PER_SESSION_V ];
  apr_list_t free_item_q;
    /**< A list of free cvs_generic_item_t entries. The handle is undefined. */

  /* Indirection queue. */
  apr_list_t indirection_q;
    /**<
     * A list of cvs_generic_item_t entries. The handle is an CVS indirection
     * object handle.
     */

  char_t  session_name[ CVS_MAX_SESSION_NAME_SIZE ];

  uint16_t master_client_addr;
    /**< Full control client's address. */
  uint16_t master_client_port;
    /**< Full control client's port. */
  uint16_t master_cvs_port;
    /**< CVS's full control port. */
  uint16_t vsm_stream_addr;
    /**< VSM's address. */
  uint16_t vsm_stream_handle;
    /**< VSM's port. */
  uint16_t attached_mvm_handle;
    /**<
      * The MVM session handle that this stream is attached to. A handle with
      * a value of APR_NULL_V indicates that this stream is not attached to an
      * MVM session.
      */

  bool_t is_stream_config_changed;
    /**<
      * Indicates whether the stream configurations have changed since the
      * last time when VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      * Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon the stream receiving a VSS_ISTREAM_CMD_SET_MEDIA_TYPE command
      *     with a different media type from that currently configured on the
      *     stream.
      *
      *   - upon the stream receiving a VSS_ISTREAM_CMD_SET_VAR_VOC_SAMPLING_RATE
      *     command such that it results in the stream to have different sample
      *     rates from those currently configured on the stream.
      *
      *   - upon a new set of calibration data being registered with the stream.
      *
      * All of the above events have the effect of potentially changing the
      * current KPPS requirements of the stream, and the stream must be
      * reconfigured.
      *
      * Upon the stream receiving a VSS_ISTREAM_CMD_ENABLE command, if
      * is_stream_config_changed is TRUE, the stream will send a
      * VSS_ISTREAM_EVT_RECONFIG event to MVM in order to notify MVM to
      * reconfigure this stream.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      */

  cvs_voc_operating_mode_info_t voc_operating_mode_info;

  cvs_control_t session_ctrl;
  cvs_session_settings_t active_set;
  cvs_session_settings_t target_set;

  cvs_tty_info_t tty_info;
  cvs_iavsync_client_info_t avsync_client_tx_info;
  cvs_iavsync_client_info_t avsync_client_rx_info;  
  cvs_iavsync_delays_info_t avsync_delay_info;

  bool_t is_var_voc_sr_requested;
  uint32_t requested_var_voc_rx_sampling_rate;
  uint32_t requested_var_voc_tx_sampling_rate;
    /**<
     * Variable vocoder sampling rates requested by client. Upon
     * VSS_IMVM_CMD_SET_CAL_VAR_VOC_SAMPLING_RATE we cache them here. On
     * every VSS_IMVM_CMD_SET_CAL_VAR_VOC_SAMPLING_RATE or
     * VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE, we determine the target_set enc,
     * dec and pp sample rates based on the media type and the requested
     * sample rates.
     */

  cvs_shared_heap_t shared_heap;
    /**< Cached UI property data that to be shared with VSM. */

  cvs_common_cal_info_t common_cal; /* BACKWARD COMPATIBILITY. */
    /**< Stream common calibration information. */

  cvs_static_cal_info_t static_cal;
    /**< Stream static calibration information. */

  uint8_t is_enable_received_from_mvm;
    /**< TRUE (1): CVS has received VSS_ISTREAM_CMD_ENABLE from MVM
     *   FALSE (0): CVS has not received VSS_ISTREAM_CMD_ENABLE from MVM (default)
     */
  cvs_set_dtmf_gen_settings_t set_dtmf_gen;
  cvs_set_rx_dtmf_detect_settings_t set_rx_dtmf_detect;
  cvs_vpcm_info_t vpcm_info;
  cvs_packet_exchange_info_t packet_exchange_info;
  cvs_kpps_info_t kpps_info;
  bool_t is_kpps_changed;

  cvs_eamr_mode_change_notification_info_t eamr_mode_change_notification_info;
    /**<
     * Note: The existing VSM_CMD_SET_EAMR_MODE_CHANGE_DETECTION API is
     * deleted from VSM as part of the introduction of the new
     * VSM_EVT_VOC_OPERATING_MODE_UPDATE event. CVS will utilize the new event
     * to detect eAMR mode change and notify the registered client. The CVS
     * implementation maintains the same behavior as before in terms of when
     * the eAMR mode changed event is sent to the client. Specifically, the
     * behavior is as follows:
     * 1. Upon the client listens to the eAMR mode change event, the initial
     *    default eAMR mode is assumed to be NB.
     * 2. Subsequently, whenever the eAMR mode changes, the eAMR mode changed
     *    event is sent to the client.
     * 3. Upon VSM is re-initialized, the eAMR mode is reset back to default
     *    (i.e. NB).
     *
     * TODO: Current implementation allows only one client to listen to the
     * eAMR mode change notification. When there is a need to support multiple
     * listeners, we must store all the listeners' information and broadcast
     * the eAMR mode change notification event to all listeners.
     */
     
  cvs_evs_bandwidth_change_notification_info_t evs_bandwidth_change_notification_info;
     /**<
     * EVS bandwidth change notification information.  CVS uses the  
     * VSM_EVT_VOC_OPERATING_MODE_UPDATE event to detect EVS bandwidth changes and
     * will notify the registered client.  The VSM_EVT_VOC_OPERATING_MODE_UPDATE
     * event is sent before a call starts after VSM_CMD_SET_MEDIA_TYPE with EVS is 
     * sent to VSM. Also, if EVS is being used, VSM_CMD_SET_STREAM_PP_SAMP_RATE will
     * also trigger a mode update from VSM.  The EVS bandwidth event notification 
     * behavior is as follows:
     * 1. Upon client registration, if CVS is in the RUN state, the EVS bandwidth
     *    change event will be sent.
     * 2. Whenever the EVS bandwidth changes during a call, the EVS bandwidth change
     *    event is sent to the client.
     * 3. During CVS's state transition from IDLE -> RUN, the EVS bandwidth change 
     *    event is sent if the client is registered and if the previously last sent
     *    bandwidth is not the same as the current bandwidth.
     *
     * TODO: Current implementation allows only one client to listen to the
     * EVS bandwidth change notification. When there is a need to support multiple
     * listeners, we must store all the listeners' information and broadcast
     * the EVS bandwidth change notification event to all listeners.
     */  
     
  cvs_packet_logging_info_t packet_logging_info;

  apr_lock_t lock;
    /* Per session object lock. To guard multi-thread access of session object. */

  uint32_t ref_cnt;
    /* Reference counter. To safely free session object and associated resources. */

    /* Cached vocoder properties required for each media id. */
  cvs_voc_property_qcelp13k_t cached_voc_properties_qcelp13k;
  cvs_voc_property_evrc_t cached_voc_properties_evrc;
  cvs_voc_property_4gvnb_t cached_voc_properties_4gvnb;
  cvs_voc_property_4gvwb_t cached_voc_properties_4gvwb;
  cvs_voc_property_4gvnw_t cached_voc_properties_4gvnw;
  cvs_voc_property_4gvnw2k_t cached_voc_properties_4gvnw2k;
  cvs_voc_property_amr_t cached_voc_properties_amr;
  cvs_voc_property_amrwb_t cached_voc_properties_amrwb;
  cvs_voc_property_eamr_t cached_voc_properties_eamr;
  cvs_voc_property_efr_t cached_voc_properties_efr;
  cvs_voc_property_fr_t cached_voc_properties_fr;
  cvs_voc_property_hr_t cached_voc_properties_hr;
  cvs_voc_property_g711_alaw_t cached_voc_properties_g711_alaw;
  cvs_voc_property_g711_mulaw_t cached_voc_properties_g711_mulaw;
  cvs_voc_property_g711_linear_t cached_voc_properties_g711_linear;
  cvs_voc_property_g729_t cached_voc_properties_g729;
  cvs_voc_property_evs_t cached_voc_properties_evs;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SESSION INDIRECTION OBJECT                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define CVS_INDIRECTION_ACCESS_FULL_CONTROL_MASK ( 0x00000001 )
#define CVS_INDIRECTION_ACCESS_FULL_CONTROL_SHFT ( 0 )

struct cvs_indirection_object_t
{
  cvs_object_header_t header;

  uint16_t client_addr;
    /**<
     * The address of the CVS client that this indirection object is created
     * for. When CVS client creates either a full or passive session control,
     * CVS creates a session indirection object whose handle is provided
     * to the client for controlling the session.
     */

  uint32_t session_handle;
    /**< The handle to the named session cvs_session_object_t. */

  uint32_t access_bits;
    /**<
     * The indirection access bit-flags:
     *
     * Bit 0: 0: Passive control.
     *        1: Full control.
     */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SIMPLE JOB OBJECT                                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

struct cvs_simple_job_object_t
{
  cvs_object_header_t header;
  uint32_t context_handle;
    /**<
     * The context handle provides additional information or storage for the
     * simple job to complete.
     *
     * The context can be anything; a session, a parent-job, etc. The
     * context is especially useful to applications that override the
     * default simple job response function table to perform additional
     * processing such as state transitions.
     *
     * Set this value to -1 when it is unused or when there is no parent.
     */
  cvs_response_fn_table_t fn_table;
    /**<
     * This is the response function v-table. The response table can store
     * custom response routines for all possible responses directed to this
     * specific job.
     */
  bool_t is_accepted;
    /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
    /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
    /**< The status returned by the command completion. */
};

struct cvs_track_job_object_t
{
  cvs_object_header_t header;
  uint32_t context_handle;
    /**<
     * The context handle provides additional information or storage for the
     * simple job to complete.
     *
     * The context can be anything; a session, a parent-job, etc. The
     * context is especially useful to applications that override the
     * default simple job response function table to perform additional
     * processing such as state transitions.
     *
     * Set this value to -1 when it is unused or when there is no parent.
     */
  cvs_response_fn_table_t fn_table;
    /**<
     * This is the response function v-table. The response table can store
     * custom response routines for all possible responses directed to this
     * specific job.
     */
  bool_t is_accepted;
    /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
    /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
    /**< The status returned by the command completion. */

    /**< Optional params for forwarding. */
  uint16_t orig_src_service;
    /**< The original client service id.. */
  uint16_t orig_src_port;
    /**< The original client port id. */
  uint16_t orig_dst_port;
    /**< The original cvs port id (indirection handle). */
  uint32_t orig_opcode;
    /**< The original opcode used before this comamnd was forwarded. */
  uint32_t orig_token;
    /**< The original token used before this comamnd was forwarded. */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SEQUENCER JOB OBJECT                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct cvs_sequencer_ssr_cleanup_struct_t
{
  uint8_t domain_id;
}
  cvs_sequencer_ssr_cleanup_struct_t;

typedef struct cvs_sequencer_set_pktexg_mode_struct_t
{
  uint32_t mode;
}
  cvs_sequencer_set_pktexg_mode_struct_t;

typedef union cvs_sequencer_use_case_union_t
{
  cvs_sequencer_ssr_cleanup_struct_t ssr_cleanup;
  cvs_sequencer_set_pktexg_mode_struct_t set_pktexg_mode;
}
  cvs_sequencer_use_case_union_t;

struct cvs_sequencer_job_object_t
{
  cvs_object_header_t header;

  uint32_t state;
    /**< The generic state variable. */
  union cvs_object_t* subjob_obj;
    /**< The current sub-job object. */
  uint32_t status;
    /**< A status value. */

  cvs_sequencer_use_case_union_t use_case;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE GENERIC CVD OBJECT                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

union cvs_object_t
{
  cvs_object_header_t header;
  cvs_session_object_t session;
  cvs_indirection_object_t indirection;
  cvs_simple_job_object_t simple_job;
  cvs_sequencer_job_object_t sequencer_job;
};

/****************************************************************************
 * SUBSYSTEM RESTART (SSR) RELATED TRACKING                                 *
 ****************************************************************************/

typedef struct cvs_ssr_cleanup_cmd_tracking_t
{
  uint32_t num_cmd_issued;
    /**<
     * Number of the commands issued by CVS to itself in each of the cleanup
     * sequencer state, when doing cleanup due to the subsystem where CVS
     * clients reside is being restarted.
     */
  uint32_t rsp_cnt;
    /**<
     * Response counter for the commands issued by CVS to itself in each of the
     * cleanup sequencer state, when doing cleanup due to the subsystem where
     * CVS clients reside is being restarted.
     */
}
  cvs_ssr_cleanup_cmd_tracking_t;

/****************************************************************************
 * EVS VOCODER DEFINITIONS                                                  *
 ****************************************************************************/

typedef enum cvs_evs_operating_mode_enum_t
{
  CVS_EVS_OPERATING_MODE_AMRWB_IO_6_60_KBPS = 0,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_8_85_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_12_65_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_14_25_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_15_85_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_18_25_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_19_85_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_23_05_KBPS,
  CVS_EVS_OPERATING_MODE_AMRWB_IO_23_85_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_5_90_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_7_20_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_8_00_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_9_60_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_13_20_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_16_40_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_24_40_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_32_00_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_48_00_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_64_00_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_96_00_KBPS,
  CVS_EVS_OPERATING_MODE_EVS_128_00_KBPS,
  CVS_EVS_OPERATING_MODE_MAX
}
  cvs_evs_operating_mode_enum_t;

typedef enum cvs_evs_fec_offset_enum_t
{
  CVS_EVS_FEC_OFFSET_2 = 2,
  CVS_EVS_FEC_OFFSET_3 = 3,
  CVS_EVS_FEC_OFFSET_5 = 5,
  CVS_EVS_FEC_OFFSET_7 = 7,
  CVS_EVS_FEC_OFFSET_MAX
}
  cvs_evs_fec_offset_enum_t;

typedef enum cvs_evs_fer_rate_enum_t
{
  CVS_EVS_FER_RATE_LOW = 0,
  CVS_EVS_FER_RATE_HIGH,
  CVS_EVS_FER_RATE_MAX
}
  cvs_evs_fer_rate_enum_t;

#endif /* __CVS_API_I_H__ */

