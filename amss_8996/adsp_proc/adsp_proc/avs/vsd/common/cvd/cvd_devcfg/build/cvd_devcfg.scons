#===============================================================================
#
# CVD DEVCFG SCONS
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (C) 2013-2014 QUALCOMM Technologies Incorporated.
# All rights reserved.
# Qualcomm Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/cvd_devcfg/build/cvd_devcfg.scons#1 $
#  $DateTime: 2016/06/05 22:53:09 $
#  $Author: pwbldsvc $
#  $Change: 10625169 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')

env.Append(CFLAGS = '-DMSG_BT_SSID_DFLT=MSG_SSID_AVS')

core_public_apis = [
   'DEBUGTOOLS',
   'KERNEL',
   'POWER',
   'SERVICES',
   ]

env.RequireRestrictedApi('AVS')
env.RequireProtectedApi('AVS')
env.RequirePublicApi(core_public_apis, area='core')

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${AVS_ROOT}/vsd/common/cvd/cvd_devcfg"
LIBNAME_CVD = 'cvd_devcfg'

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)
                
srcpath = env.RealPath(SRCPATH, posix=True)
cvd_sources = env.GlobFiles([srcpath+'/build/*.cpp', srcpath+'/build/*.c'], posix=True)
# Since there is no multi-devcfg support for AUDIO and SENSOR for ADSP MPD env, can't add .c file to devcfg into user pd.
# multi-devcfg support for user pd on ADSP is under discussion. On 8994 audio pd is not POR, used for development on target while 8996 is pre-silicon
if 'AUDIO_IN_USERPD' in env:
    if env['MSM_ID'] in ['8994']:
        cvd_config = [
            '${BUILDPATH}/msm8994/cvd_devcfg.c',
        ]
        cvd_sources.extend(cvd_config)
    cvd_sources = [source.replace(srcpath, '${BUILDPATH}') for source in cvd_sources]

    env.AddLibrary(['AVS_ADSP_USER'], '${BUILDPATH}/'+LIBNAME_CVD, cvd_sources)
else:
    env.AddLibrary(['AVS_ADSP'], '${BUILDPATH}/'+LIBNAME_CVD, cvd_sources)

#-------------------------------------------------------------------------------
# DEVCFG
#-------------------------------------------------------------------------------
   
if 'USES_DEVCFG' in env:
   if 'AUDIO_IN_USERPD' in env:
      DEVCFG_IMG = ['DEVCFG_CORE_QDSP6_AUDIO_SW']
      env.AddDevCfgInfo(DEVCFG_IMG, 
      {      
      '8994_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8994/cvd_devcfg.xml'],
      '8996_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8996/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8996/cvd_devcfg.c'],
      '8998_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8998/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8998/cvd_devcfg.c']					
      })
   else:
      DEVCFG_IMG = ['DAL_DEVCFG_IMG']
      env.AddDevCfgInfo(DEVCFG_IMG, 
      {
      '8974_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.c'],
      '8x26_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.c'],
      '8x10_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.c'],
      '8084_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.c'],
      '8962_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8974/cvd_devcfg.c'],
      '9x25_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/mdm9x25/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/mdm9x25/cvd_devcfg.c'],
      '8994_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8994/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8994/cvd_devcfg.c'],
      '8996_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8996/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8996/cvd_devcfg.c'],
      '8092_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/mpq8092/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/mpq8092/cvd_devcfg.c'],
      '8952_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.c'],
      '8976_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8976/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8976/cvd_devcfg.c'],
      '8953_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.c'],
      '8937_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.c'],
      '8917_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8952/cvd_devcfg.c'],
      '8998_xml' : ['${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8998/cvd_devcfg.xml',
                    '${AVS_ROOT}/vsd/common/cvd/cvd_devcfg/msm8998/cvd_devcfg.c']										
      })

if env.CheckAlias('devcfg_img'):
   env.Append(CFLAGS = ' -Wno-unused-function')

