/*
  Copyright (C) 2013 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/cvd_utils/src/cvd_log.c#1 $
  $Author: pwbldsvc $
*/

#include "msg.h"
#include "mmstd.h"
#include "apr_errcodes.h"
#include "cvd_log_i.h"


APR_INTERNAL int32_t cvd_log_commit_data (
  log_code_type log_code,
  uint32_t sequence_num,
  uint32_t total_size,
  uint32_t instance,
  uint32_t call_num,
  uint64_t timestamp,
  uint32_t tap_id,
  uint32_t media_id,
  uint8_t* data
)
{
  cvd_log_voice_t* log_ptr;
  uint16_t payload_size = ( uint16_t )total_size; /* No fragmentation support yet. */
  uint8_t* dst_data_ptr;
  log_ptr = ( ( cvd_log_voice_t* )log_alloc( log_code, 
                                             payload_size + sizeof( cvd_log_voice_t ) ) );

  
  
  if( log_ptr != NULL )
  {
    dst_data_ptr = ( ( uint8_t* )log_ptr ) + sizeof( cvd_log_voice_t );
  
    log_ptr->voice.cointainer_size = sizeof( cvd_log_voice_container_t ) + payload_size;
    log_ptr->voice.container_id = CVD_LOG_VOICE_CONTAINER_ID;
    log_ptr->voice.minor_version = 0;

    log_ptr->voice.segment_header.container_size = sizeof( cvd_log_segment_header_t );
    log_ptr->voice.segment_header.container_id = CVD_LOG_SEGMENT_HEADER_CONTAINER_ID;
    log_ptr->voice.segment_header.minor_version = 0;
    log_ptr->voice.segment_header.sequence_num = sequence_num;
    log_ptr->voice.segment_header.total_size = total_size;
    log_ptr->voice.segment_header.offset = 0; /* No fragmentation support yet. */

    log_ptr->voice.aux_info.container_size = sizeof( cvd_log_aux_info_t );
    log_ptr->voice.aux_info.container_id = CVD_LOG_AUX_INFO_CONTAINER_ID;
    log_ptr->voice.aux_info.minor_version = 0;
    log_ptr->voice.aux_info.instance = instance;
    log_ptr->voice.aux_info.call_num = call_num;
    log_ptr->voice.aux_info.timestamp = timestamp;
    log_ptr->voice.aux_info.tap_id = tap_id;

    log_ptr->voice.data.container_size = sizeof( cvd_log_raw_data_t ) + payload_size;
    log_ptr->voice.data.container_id = media_id;

    
	
    ( void ) mmstd_memcpy( dst_data_ptr, payload_size, data, payload_size );

	

    log_commit( log_ptr );
  }
  else
  {
    return APR_EFAILED;
  }

  return APR_EOK;
}

