/*==============================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/slimbus/src/AFESlimbusDriverUtils.cpp#1 $
$DateTime: 2016/06/05 22:53:09 $
$Author: pwbldsvc $
$Change: 10625169 $
$Revision: #1 $

FILE:     afe_slimbus_driver_utils.cpp

DESCRIPTION: AFE Slimbus driver utility functions

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright 2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/slimbus/src/AFESlimbusDriverUtils.cpp#1 $ $DateTime: 2016/06/05 22:53:09 $ $Author: pwbldsvc $

when        who  what, where, why
----------  ---  ----------------------------------------------------------
08-08-2010  mspk  Initial Draft


============================================================================*/
#include "AFESlimbusDriverUtils_i.h"
#include "ddislimbus.h"
#include "AFEInternal.h"
#include "AFEDevService.h"
#include "qurt_elite.h"
#include "adsp_afe_service_commands.h"
#include "AFESlimbusTimerUtils_i.h"
#include "ddislimbus.h"
#include "AFEDeviceDriver.h"
#include "AFESlimbusSlaveCfg_i.h"
#include "AFEInternal.h"

/*=====================================================================
 Functions
 ======================================================================*/
void*  afe_dal_bam_callback (void *cbCtxt, uint32 uSize1, void *pBuf2, uint32 uSize2)
{
    afe_dev_port_t* afe_port_ptr = (afe_dev_port_t *)cbCtxt;

	/* Skip the first interrupt processing (needed for multi-channel case),
	   This is mitigating the HW issue with generating the interrupt when first channel copy is done. */
	/* For 8Khz and mono case, first interrupt will come before port state changes to RUN. So, skipping
	   first interrupt should be independent of port state */
	if (afe_port_ptr->is_first_interrupt)
	{
		afe_port_ptr->is_first_interrupt = FALSE;
		return NULL;
	}

	if (AFE_PORT_STATE_RUN == afe_port_ptr->port_state)
	{
		/*if software latched read the progress counter and av-timer*/
		afe_avt_drift_params_t *avt_drift_params_ptr = (afe_avt_drift_params_t *)&afe_port_ptr->avt_drift_params;
		if(TIMESTAMP_LATCH_TYPE_SOFTWARE == avt_drift_params_ptr->avtimer_latch_type)
		{
			afe_read_avt_and_sb_counters_atomically(afe_port_ptr, &avt_drift_params_ptr->sw_latched_dev_time,
					&afe_port_ptr->port_av_timestamp);
		}

		// signal to the port worker loop about the finishing of the data transfer
      afe_port_ptr->dynamic_thread_ptr->isr_context_counter++;
		qurt_elite_signal_send(&afe_port_ptr->dma_signal);
	}

    return NULL;
}

void*  afe_dal_port_callback (void *cbCtxt, uint32 uSize1, void *pBuf2, uint32 uSize2)
{
    ADSPResult result=ADSP_EOK;
	afe_dev_port_t* afe_port_ptr = (afe_dev_port_t *)cbCtxt;

	if (AFE_PORT_STATE_RUN == afe_port_ptr->port_state)
	{
		afe_slimbus_state_struct_t* p_dev_state = (afe_slimbus_state_struct_t *)afe_port_ptr->afe_drv;
		afe_sb_stream_info_t *p_stream_info = &p_dev_state->a_stream_info[p_dev_state->afe_sb_cfg.no_of_streams-1];
		SlimBusEventNotifyType portEvent;

		// get the port event information
		result = DalSlimBus_GetPortEvent(p_dev_state->p_sb_driver->p_core_driver,p_stream_info->h_master_port, &portEvent);

		// if it port disconnect event, then signal to the sw thread which will be waiting for device closure
		if((portEvent.eEvent & (SLIMBUS_EVENT_O_FIFO_TRANSMIT_UNDERRUN|SLIMBUS_EVENT_O_FIFO_RECEIVE_OVERRUN)) || (result != DAL_SUCCESS))
		{
			// increment the counter in case of overflow or underflow events or any errors
			p_dev_state->port_error_cnt++;
		}
	}
    return NULL;
}

ADSPResult afe_sb_open_stream_ports(afe_slimbus_state_struct_t *p_dev_state)
{
   ADSPResult result = ADSP_EOK;
   DalDeviceHandle *p_core_driver = p_dev_state->p_sb_driver->p_core_driver;
   afe_sb_stream_info_t *p_stream_info = p_dev_state->a_stream_info;
   uint16_t stream_idx, desc_idx, no_of_streams = p_dev_state->afe_sb_cfg.no_of_streams;
   SlimBusPortConfigType master_cfg;
   SlimBusResourceHandle h_master_port[AFE_PORT_MAX_AUDIO_CHAN_CNT];
   uint32_t virt_addr;
   uint32_t phy_addr = 0;
   uint16_t pipe_size;
   SlimBusPipeConfigType *pipe_cfg_info_ptr;
   uint32_t aligned_buf_size;
   uint32_t total_aligned_buf_size;

   memset(&master_cfg, 0, sizeof(SlimBusPortConfigType));

   //SLIMbus port will assert block_end after uBlockSize bytes,
   // only valid for producer data pipe in BAM-to-BAM mode.
   master_cfg.uBlockSize = 0;
   // SLIMbus port will assert trans_end after uTransSize bytes,
   // closing the current FIFO descriptor early; only valid for producer data pipe.
   master_cfg.uTransSize = 0;
   master_cfg.eOptions = p_dev_state->pack_mode;

   // choose the descriptors for the given device
   p_dev_state->desc_mem_type = AFE_DESC_MEM_LPM;
   p_dev_state->num_descptrs = MAX_DESCRIPTORS_PER_STREAM;
   master_cfg.uWaterMark = AFE_SB_PORT_CFG_WATERMARK_SIZE_2;
   if((AFE_DESC_MEM_LPM == p_dev_state->desc_mem_type)
         && (p_dev_state->afe_sb_cfg.sample_rate <= AFE_PORT_SAMPLE_RATE_48K))
   {
      master_cfg.uWaterMark = AFE_SB_PORT_CFG_WATERMARK_SIZE_1;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "num_descptrs: 0x%x, watermark: %lx", p_dev_state->num_descptrs, master_cfg.uWaterMark);

   // configure the BAM with default settings
   SlimBusBamRegisterEventType *bam_cfg_ptr = &p_dev_state->bam_cfg;
   bam_cfg_ptr->eOptions = SLIMBUS_BAM_O_DESC_DONE;
   bam_cfg_ptr->pUser = NULL;

   // create the BAM event. Single BAM event is used for all streams.
   // Only last stream gets registered for the BAM interrupt after data transfer
   bam_cfg_ptr->hEvent = NULL;
   result = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT, &bam_cfg_ptr->hEvent, NULL);
   if(DAL_SUCCESS != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create BAM event, 0x%x", result);
      return ADSP_EFAILED;
   }

   // register the call back function for the events
   result = DALSYS_SetupCallbackEvent(bam_cfg_ptr->hEvent, afe_dal_bam_callback,
                                      (void*)p_dev_state->afe_port_ptr);
   if(DAL_SUCCESS != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to register call back function to BAM events, 0x%x", result);
      return ADSP_EFAILED;
   }

   // create the master port event for capturing over-flow, underflow, port disconnect events
   p_dev_state->master_port_event = NULL;
   result = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT, &p_dev_state->master_port_event,
                               NULL);
   if(DAL_SUCCESS != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create master port event, 0x%x", result);
      return ADSP_EFAILED;
   }

   // register the call back function for the events
   result = DALSYS_SetupCallbackEvent(p_dev_state->master_port_event, afe_dal_port_callback,
                                      (void*)p_dev_state->afe_port_ptr);
   if(DAL_SUCCESS != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to register master port callback func, 0x%x", result);
      return ADSP_EFAILED;
   }

   // allocate master ports for the whole group once
   result = DalSlimBus_AllocMasterPorts(p_core_driver, SLIMBUS_PORT_REQ_O_MULTI_CHAN_GROUP,
                                        MIN_FIFO_SIZE_IN_BYTES, h_master_port,
                                        no_of_streams * sizeof(h_master_port[0]));
   if(DAL_SUCCESS != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate master port, 0x%x", result);
      return ADSP_EFAILED;
   }


   // init the pipe cfg info, size of pipe, etc
   // Due to cicular nature of the FIFO, the additional +1 is used.
   // It is used to distinguish empty and full status ..
   pipe_size = (p_dev_state->num_descptrs+1)*sizeof(SlimBusBamIOVecType);
   aligned_buf_size = SLIMBUS_ALIGN(pipe_size, SLIMBUS_ALIGN_32_BYTES_MASK);
   total_aligned_buf_size = aligned_buf_size * no_of_streams;
   
   pipe_cfg_info_ptr = p_dev_state->pipe_cfg_info;
   memset(pipe_cfg_info_ptr, 0, no_of_streams*sizeof(SlimBusPipeConfigType));

   // Pipe memory allocation pass
   virt_addr = (uint32_t) afe_dev_lpa_aligned_malloc(total_aligned_buf_size, 32);
   if(0 == virt_addr)
   {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unable to get LPM memory for pipe cfg");
       return ADSP_ENOMEMORY;
   }
   // Convert to physical start address
   phy_addr =(uint32_t) qurt_elite_memorymap_get_physical_addr((uint32_t)virt_addr);
   if(0 == phy_addr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unable to get physical address for virtual address0x%x", virt_addr);
      afe_dev_lpa_aligned_free( (void *) virt_addr);
      return ADSP_EFAILED;
   }

   // Assign the allocated memory for individual pipe cfg
   pipe_cfg_info_ptr = p_dev_state->pipe_cfg_info;
   for(stream_idx = 0; stream_idx < no_of_streams; stream_idx++)
   {
      pipe_cfg_info_ptr->DescFifo.uMinSize = pipe_size;
      pipe_cfg_info_ptr->eBamDev = SLIMBUS_BAM_DEV_HANDLE_MEM;
      pipe_cfg_info_ptr->DescFifo.pBase    = (void *) virt_addr;
      pipe_cfg_info_ptr->DescFifo.uPhysBase = phy_addr;
      pipe_cfg_info_ptr->DescFifo.uSize = pipe_cfg_info_ptr->DescFifo.uMinSize;
      MSG_3(MSG_SSID_QDSP6,DBG_HIGH_PRIO, " virt_addr :0x%x, phy_addr:0x%x, pipe size: %d,", virt_addr,phy_addr,pipe_cfg_info_ptr->DescFifo.uMinSize);
      virt_addr += aligned_buf_size;
      phy_addr += aligned_buf_size;
      pipe_cfg_info_ptr++;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LPM Pipe memory allocted size %ld for num stream %d", 
     total_aligned_buf_size, no_of_streams);

   // Transfer buffer allocation phase
   aligned_buf_size = SLIMBUS_ALIGN(p_dev_state->bam_trfr_size, SLIMBUS_ALIGN_32_BYTES_MASK);
   total_aligned_buf_size = aligned_buf_size * p_dev_state->num_descptrs * no_of_streams;
   if(AFE_DESC_MEM_LPM == p_dev_state->desc_mem_type)
   {
      virt_addr = (uint32_t)afe_dev_lpa_aligned_malloc(total_aligned_buf_size, 32);
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LPM Des memory allocted size %ld", 
      total_aligned_buf_size);

    }
    else
    {
      virt_addr = (uint32_t)qurt_elite_memory_aligned_malloc(total_aligned_buf_size, 32, QURT_ELITE_HEAP_DEFAULT);
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Default Des memory allocted size %ld",
        total_aligned_buf_size);
    }


   if(0 == virt_addr)
    {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate memory for descriptors");
       return ADSP_ENOMEMORY;
    }


   // initialize the BAM buffers with all zeros
   memset((char *)virt_addr, 0, total_aligned_buf_size);

   phy_addr =(uint32_t) qurt_lookup_physaddr((uint32_t)virt_addr);
   if(0 == phy_addr)
   {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unable to get physical address for virtual address0x%x", virt_addr);
       if (AFE_DESC_MEM_LPM == p_dev_state->desc_mem_type)
       {
         afe_dev_lpa_aligned_free((void *)virt_addr);
       }
       else
       {
         qurt_elite_memory_aligned_free((void *)virt_addr);
       }
       return ADSP_EFAILED;
   }

   

   pipe_cfg_info_ptr = p_dev_state->pipe_cfg_info;
   // initialize stream specific parameters (DMA buffer allocation and interrupt mode etc)
   for(stream_idx = 0; stream_idx < no_of_streams; stream_idx++, p_stream_info++, pipe_cfg_info_ptr++)
   {
      // allocate & configure the descriptors for every stream
      for(desc_idx = 0; desc_idx < (p_dev_state->num_descptrs); desc_idx++)
      {
         p_stream_info->cust_desc_info[desc_idx].virt_addr = virt_addr;
         p_stream_info->IOVec[desc_idx].uAddr = phy_addr;
         p_stream_info->IOVec[desc_idx].uSize = p_dev_state->bam_trfr_size;
         p_stream_info->IOVec[desc_idx].uFlags = SLIMBUS_BAM_IOVEC_FLAG_INT;
         phy_addr += aligned_buf_size;
         virt_addr += aligned_buf_size;
      }

      // assign the master port
      p_stream_info->h_master_port = h_master_port[stream_idx];

      // register with port event handler to get under-run\over-flow\port-disconnect events
      // register for last stream in multi channels
      if(stream_idx == (no_of_streams - 1))
      {
         result = DalSlimBus_RegisterPortEvent(p_core_driver, p_stream_info->h_master_port,
                                               p_dev_state->master_port_event, NULL);
         if(DAL_SUCCESS != result)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to register master port event, 0x%x", result);
            return ADSP_EFAILED;
         }

         // initialize over-flow or under-flow count to 0
         p_dev_state->port_error_cnt = 0;
      }

      // configure the masterport
      if(DAL_SUCCESS
            != (result = DalSlimBus_ConfigMasterPort(p_core_driver, p_stream_info->h_master_port,
                                                     &master_cfg)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to configure master port, 0x%x", result);
         return ADSP_EFAILED;
      }

      if(stream_idx == (no_of_streams - 1))
      {
         pipe_cfg_info_ptr->eOptions = (SlimBusBamOptionType)(
               SLIMBUS_BAM_O_DESC_DONE | SLIMBUS_BAM_O_ERROR | SLIMBUS_BAM_O_OUT_OF_DESC);
      }
      else
      {
         pipe_cfg_info_ptr->eOptions = (SlimBusBamOptionType)(
               SLIMBUS_BAM_O_ERROR | SLIMBUS_BAM_O_OUT_OF_DESC);
      }

      // configure master pipe which will be used to get data from memory to FIFO
      if(DAL_SUCCESS
            != (result = DalSlimBus_ConfigMasterPipe(p_core_driver, p_stream_info->h_master_port,
                                                     (SlimBusPortFlowType)p_dev_state->direction,
                                                     pipe_cfg_info_ptr)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to configure master pipe, 0x%x", result);
         return ADSP_EFAILED;
      }

      // register with BAM event handler to get data transfer events
      if(stream_idx == (no_of_streams - 1))
      {
         result = DalSlimBus_RegisterBamEvent(p_core_driver, p_stream_info->h_master_port,
                                              SLIMBUS_BAM_DEFAULT, bam_cfg_ptr);
         if(DAL_SUCCESS != result)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to register BAM event, 0x%x", result);
            return ADSP_EFAILED;
         }
      }
   }

   // initialize progress counter handle to NULL by default
   // and then allocate one progress counter per device
   // TBD: do we need to allocate progress counters based
   //      on need instead of default per device?
   p_dev_state->h_progress_ctr = 0;
   return (afe_sb_alloc_progress_counter(p_dev_state));
}

ADSPResult afe_sb_close_stream_ports(afe_slimbus_state_struct_t *p_dev_state)
{
    ADSPResult result=ADSP_EOK, curr_result;
    DalDeviceHandle *p_core_driver = p_dev_state->p_sb_driver->p_core_driver;
    afe_sb_stream_info_t *p_stream_info = p_dev_state->a_stream_info;
    uint16_t stream_idx, desc_idx;
    SlimBusPipeConfigType *pipe_cfg_info_ptr = p_dev_state->pipe_cfg_info;

    if (0 != p_dev_state->h_progress_ctr)
    {
        if(ADSP_EOK != (curr_result = afe_sb_dealloc_progress_counter(p_dev_state)) )
        {
           MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to Dealloc progress counter, result: 0x%x", curr_result);
           result |= curr_result;
        }
    }

    // Delllocate pipe cfg mem, first location point to all memory
   // and reset the pip cfg info
    if (pipe_cfg_info_ptr->DescFifo.pBase != NULL)
    {
      afe_dev_lpa_aligned_free((void *)pipe_cfg_info_ptr->DescFifo.pBase );
      for(stream_idx = 0; stream_idx < p_dev_state->afe_sb_cfg.no_of_streams; stream_idx++)
      {
         pipe_cfg_info_ptr->DescFifo.pBase = NULL;
         pipe_cfg_info_ptr->DescFifo.uPhysBase = NULL;           
         pipe_cfg_info_ptr++;
      }
    }
  
   // Deallocate mem for all desc
   // all mem is at the first location, i.e. first stream, first des
    if(0 != p_stream_info->cust_desc_info[0].virt_addr)
    {
      if (AFE_DESC_MEM_LPM == p_dev_state->desc_mem_type)
      {
         afe_dev_lpa_aligned_free((void *)p_stream_info->cust_desc_info[0].virt_addr);
      }
      else
      {
         qurt_elite_memory_aligned_free((void *)p_stream_info->cust_desc_info[0].virt_addr);
      }
    }

    // in case of valid stream, de-allocate the allocated memory for descriptors
	for(stream_idx = 0; stream_idx < p_dev_state->afe_sb_cfg.no_of_streams; stream_idx++, p_stream_info++)
	{
		// de-allocate the master ports associated with this device
		if (p_stream_info->h_master_port)
		{
			if (DAL_SUCCESS != (curr_result = DalSlimBus_DeallocMasterPorts(p_core_driver, &(p_stream_info->h_master_port), sizeof(p_stream_info->h_master_port))))
			{
				MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to Dealloc master port, 0x%x", curr_result);
            result |= curr_result;
			}
			p_stream_info->h_master_port = 0;
		}

		// reset the descriptors for every stream
		for (desc_idx = 0; desc_idx < (p_dev_state->num_descptrs); desc_idx++)
		{
				p_stream_info->cust_desc_info[desc_idx].virt_addr = 0;
				p_stream_info->IOVec[desc_idx].uAddr = 0;
			}
		}

	if (NULL != p_dev_state->master_port_event)
	{
		DALSYS_DestroyObject(p_dev_state->master_port_event);
		p_dev_state->master_port_event = NULL;
	}

	if (NULL != p_dev_state->bam_cfg.hEvent)
	{
		DALSYS_DestroyObject(p_dev_state->bam_cfg.hEvent);
		p_dev_state->bam_cfg.hEvent = NULL;
	}

	return result;
}

ADSPResult afe_sb_open_data_channels(afe_slimbus_state_struct_t *p_dev_state)
{
    ADSPResult result;
    uint16_t stream_idx;
    DalDeviceHandle *p_core_driver = p_dev_state->p_sb_driver->p_core_driver;
    afe_sb_stream_info_t *p_stream_info =  p_dev_state->a_stream_info;

    // create resource group for the requested device\interface
    if (DAL_SUCCESS != (result = DalSlimBus_AllocResourceGroup(p_core_driver, &p_dev_state->h_chan_group)))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate resource group, 0x%x", result);
        return ADSP_EFAILED;
    }

    for(stream_idx = 0; stream_idx < p_dev_state->afe_sb_cfg.no_of_streams; stream_idx++, p_stream_info++)
    {
		if (FALSE == p_dev_state->afe_sb_cfg.is_it_share_chan)
		{
			// allocate the data channel between master-port and slave-port
			if (DAL_SUCCESS != (result = DalSlimBus_AllocDataChannel(p_core_driver, &(p_stream_info->h_data_channel))))
			{
				MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate the channel, 0x%x", result);
				return ADSP_EFAILED;
			}
		}
		else
		{
			// allocate the data channel between master-port and slave-port
			if (DAL_SUCCESS != (result = DalSlimBus_AllocSharedDataChannel(p_core_driver, p_dev_state->afe_sb_cfg.shared_channel_mapping[stream_idx], &(p_stream_info->h_data_channel))))
			{
				MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate the channel, 0x%x", result);
				return ADSP_EFAILED;
			}
		}

		if (DAL_SUCCESS != (result = DalSlimBus_AddResourceToGroup(p_core_driver, p_dev_state->h_chan_group, p_stream_info->h_data_channel)))
		{
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add channel to resource group, 0x%x", result);
            return ADSP_EFAILED;
		}

		if (DAL_SUCCESS != (result = DalSlimBus_ConnectPortToChannel(p_core_driver, p_stream_info->h_data_channel, (SlimBusPortFlowType)p_dev_state->direction, p_stream_info->h_master_port)))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to connect master port to the channel, 0x%x", result);
			return ADSP_EFAILED;
		}
    }

    return ADSP_EOK;
}

ADSPResult afe_sb_close_data_channels(afe_slimbus_state_struct_t *p_dev_state)
{
    ADSPResult result = ADSP_EOK, curr_result = ADSP_EOK;
    DalDeviceHandle *p_core_driver = p_dev_state->p_sb_driver->p_core_driver;
    afe_sb_stream_info_t *p_stream_info =  p_dev_state->a_stream_info;
    uint16_t stream_idx;

	// de-activate the data channel first
	if((DAL_SUCCESS != (curr_result |= DalSlimBus_NextDataChannelControl(p_core_driver, p_dev_state->h_chan_group, SLIMBUS_CHAN_CTRL_REMOVE)))||
		  (DAL_SUCCESS != (curr_result |= DalSlimBus_DoReconfigureNow(p_core_driver))))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to remove the channel with ReConfigNow(), 0x%x", curr_result);
		// return; -> continue  with closing sequence and core driver should take care of the needed dependencies.

      result |= curr_result;
	}

	// de-alloc all the data channels
	for(stream_idx = 0; stream_idx < p_dev_state->afe_sb_cfg.no_of_streams; stream_idx++, p_stream_info++)
	{
		// de-allocate data channels associated with this device
		if (p_stream_info->h_data_channel)
		{
			 curr_result = DalSlimBus_DeallocDataChannel(p_core_driver, p_stream_info->h_data_channel);
			 if (DAL_SUCCESS != curr_result)
			 {
				 MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to de-allocate the data channel, 0x%x", curr_result);
             result |= curr_result;
			 }
			 p_stream_info->h_data_channel = 0;
		}
	}

	 // de-alloc the resource group
	 curr_result = DalSlimBus_DeallocResourceGroup(p_core_driver, p_dev_state->h_chan_group);
	 if (DAL_SUCCESS != curr_result)
	 {
		 MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to de-allocate data channel resource group, 0x%x", curr_result);
       result |= curr_result;
	 }

	p_dev_state->h_chan_group = 0;

    return result;
}
