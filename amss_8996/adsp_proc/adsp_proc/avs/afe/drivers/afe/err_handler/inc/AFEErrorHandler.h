/*========================================================================
  This file contains AFE Error handler related API's

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: 
 ====================================================================== */
#ifndef _AFE_ERR_HDLR_H_
#define _AFE_ERR_HDLR_H_

/*==========================================================================
  Include Files
  ========================================================================== */

#include "qurt_elite.h"

/*==========================================================================
  Macro\Constant definitions
  ========================================================================== */


/*==========================================================================
  Function declarations
  ========================================================================== */

/**
  AFE Error handler

  @param[in]  None

  @result
  None

  @dependencies
  None.
*/
ADSPResult afe_err_handler();


#endif /* _AFE_ERR_HDLR_H_ */
