/*==============================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/tdmif/stub_src/AFETdmDriver_stub.cpp#1 $
$DateTime: 2016/06/10 11:57:20 $
$Author: pwbldsvc $
$Change: 10672791 $
$Revision: #1 $

FILE:     hdmi_input_drv.cpp

DESCRIPTION: Main Interface to the AFE Slimbus driver

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright 2013 Qualcomm Technologies, Inc. (QTI).
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/tdmif/stub_src/AFETdmDriver_stub.cpp#1 $ $DateTime: 2016/06/10 11:57:20 $ $Author: pwbldsvc $

when        who  what, where, why
----------  ---  ----------------------------------------------------------
7/30/2012   RP   Created


============================================================================*/

/*=====================================================================
 Includes
 ======================================================================*/
#include "qurt_elite.h"
#include "qurt_error.h"
#include "AFEDeviceDriver.h"
#include "AFEInternal.h"
#include "AFETdmDriver.h"


ADSPResult afe_tdm_driver_init(void)
{
   return ADSP_EUNSUPPORTED;
}

ADSPResult afe_tdm_driver_deinit(void)
{
   return ADSP_EUNSUPPORTED;
}

ADSPResult afe_tdm_port_init(aud_stat_afe_svc_t* afe_svc_ptr)
{
   return ADSP_EUNSUPPORTED;
}

ADSPResult afe_tdm_port_deinit(aud_stat_afe_svc_t* afe_svc_ptr)
{
   return ADSP_EUNSUPPORTED;
}

void afe_tdm_get_dev_port(uint16 unPortId, afe_dev_port_t **pDevPort)
{
  *pDevPort = NULL;
}

