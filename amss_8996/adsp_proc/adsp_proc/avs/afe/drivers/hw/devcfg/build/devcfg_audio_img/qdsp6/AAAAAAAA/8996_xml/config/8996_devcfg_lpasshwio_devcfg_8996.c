#include "../config/devcfg_lpasshwio.h"
typedef signed char int8;
typedef unsigned char uint8;
typedef signed short int int16;
typedef unsigned short int uint16;
typedef signed int long int32;
typedef unsigned long int uint32;
typedef signed long long int int64;
typedef unsigned long long int uint64;
typedef unsigned char boolean;
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,
        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,
        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,
        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,
        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;
typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;
typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;
typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;
typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;
typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;
struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
typedef struct {
    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;
} qurt_mem_region_attr_t;
typedef struct {
    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];
} qurt_mem_pool_attr_t;
typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;
typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
enum
{
   TIMESTAMP_LATCH_TYPE_SOFTWARE = 0,
   TIMESTAMP_LATCH_TYPE_HARDWARE
};
enum
{
   PRIMARY_MI2S = 0,
   PRIMARY_PCM = 0,
   PRIMARY_TDM = 0,
   PRIMARY_MUX = 0,
   SECONDARY_MI2S = 1,
   SECONDARY_PCM = 1,
   SECONDARY_TDM = 1,
   SECONDARY_MUX = 1,
   TERTIARY_MI2S = 2,
   TERTIARY_PCM = 2,
   TERTIARY_TDM = 2,
   TERTIARY_MUX = 2,
   QUATERNARY_MI2S = 3,
   QUATERNARY_PCM = 3,
   QUATERNARY_TDM = 3,
   QUATERNARY_MUX = 3,
   SPEAKER_I2S = 4,
   QUINARY_MI2S = 5,
   SENARY_MI2S = 6
};
enum
{
   DATA_LINE_SINK = 1,
   DATA_LINE_SOURCE = 2,
   DATA_LINE_BIDIRECT = 3,
};
enum
{
   NOT_SUPPORTED = 0,
   SUPPORTED = 1,
};
typedef struct
{
   uint32 hw_ver_reg_base;
}HwdLpassPropertyType;
typedef struct
{
   uint32 hdmi_reg_base;
   uint32 hdmi_reg_size;
   uint32 is_supported;
} lpasshwio_prop_hdmi14_struct_t;
typedef struct
{
   uint32 i2s_reg_offset_addr;
   uint32 inf_type_cnt;
   uint32 inf_type_list[7];
   uint32 inf_type_data_line_caps[7];
   uint32 inf_type_direction[7];
   uint32 is_supported;
} lpasshwio_prop_i2s_struct_t;
typedef struct
{
   uint32 pcm_reg_offset_addr;
   uint32 inf_type_cnt;
   uint32 inf_type_list[4];
   uint32 inf_type_data_line_caps[4];
   uint32 inf_type_direction[4];
   uint32 is_supported;
} lpasshwio_prop_pcm_struct_t;
typedef struct
{
   uint32 tdm_reg_offset_addr;
   uint32 inf_type_cnt;
   uint32 inf_type_list[4];
   uint32 inf_type_data_line_caps[4];
   uint32 inf_type_direction[4];
   uint32 is_supported;
} lpasshwio_prop_tdm_struct_t;
typedef struct
{
   uint32 mux_reg_offset_addr;
   uint32 mux_inf_type_cnt;
   uint32 shared_inf_type_list_map[4];
   uint32 is_supported;
} lpasshwio_prop_lpaif_mux_struct_t;
typedef struct
{
   uint32 lpaif_reg_base;
   uint32 lpaif_reg_size;
   lpasshwio_prop_i2s_struct_t i2s_prop;
   lpasshwio_prop_pcm_struct_t pcm_prop;
   lpasshwio_prop_tdm_struct_t tdm_prop;
   lpasshwio_prop_hdmi14_struct_t hdmi_prop;
   lpasshwio_prop_lpaif_mux_struct_t lpaif_mux_prop;
   uint32 hw_revision;
} lpasshwio_prop_lpaif_struct_t;
typedef struct
{
   uint32 dma_reg_base;
   uint32 dma_reg_size;
   uint32 dma_int_reg_offset_addr;
   uint32 rddma_reg_offset_addr;
   uint32 wrdma_reg_offset_addr;
   uint32 stc_rddma_reg_offset_addr;
   uint32 stc_wrdma_reg_offset_addr;
   uint32 sink_dma_cnt;
   uint32 source_dma_cnt;
   uint32 int_irq_no;
   uint32 int_output_index;
   uint32 hw_revision;
} lpasshwio_prop_afe_dma_struct_t;
typedef struct
{
   uint32 tcsrRegBase;
   uint32 tcsrRegSize;
   uint32 hw_revision;
   uint32 supportedClkIdCnt;
   uint32 supportedClkId[36];
   uint32 infPcmOeCnt;
   uint32 infPcmOeSupportedClkId[8];
   uint32 infPcmOeClkIdToUse[8];
   uint32 infPcmOeClkFreq[8];
} HwdGenericClkPropertyType;
typedef struct
{
   uint32 baseRegAddr;
   uint32 regSize;
   uint32 rootClk;
   uint32 avt_offset_addr;
   uint32 atime1_offset_addr;
   uint32 atime2_offset_addr;
   uint32 audsync_offset_addr;
   uint32 isrHookPinNum;
   uint32 hw_revision;
} HwdAvtimerPropertyType;
typedef struct
{
   uint32 baseRegAddr;
   uint32 regSize;
   uint32 isrHookPinNum;
   uint32 hw_revision;
} HwdResamplerPropertyType;
typedef struct vfr_src_prop
{
   uint32 irq_mux_ctl_sel;
   uint32 is_supported;
} vfr_src_prop_t;
typedef struct
{
   uint32 reg_base_addr;
   uint32 reg_size;
   uint32 vfr_ts_mux_offset1_addr;
   uint32 gp_ts_mux_offset0_addr;
   uint32 gp_ts_mux_offset1_addr;
   uint32 hw_revision;
   uint32 hw_latch_ver;
   uint32 num_vfr_ts_mux;
   uint32 num_gp_ts_mux;
   uint32 l2vic_num[2];
   vfr_src_prop_t vfr_src_prop[2];
}lpasshwio_vfr_prop_t;
typedef struct
{
    uint32 hw_revision;
    uint32 ccu_mb0_ctrl_reg_phy_addr;
    uint32 q6ss_ipc_reg_phy_addr;
    uint32 q6ss_ipc_reg_ack_bit_pos;
    uint16 ipc_send_mail_box_offset;
    uint16 internal_bt_tx_int;
    uint16 internal_bt_rx_int;
    uint16 internal_fm_tx_rx_int;
    uint16 internal_bt_fm_ipc_int;
    uint16 hw_timestamp_available;
}lpasshwio_prop_riva_struct_t;
typedef struct
{
    uint32 hw_revision;
    uint32 avtimer_latch_type;
}lpasshwio_prop_slimbus_struct_t;
typedef struct
{
   uint32 baseRegAddr;
   uint32 baseRegVirtAddr;
   uint32 virtOffset;
   uint32 regSize;
   uint32 intPinNo;
   uint32 hw_revision;
} HwdMidiPropertyType;
typedef struct
{
   uint32 base_reg_phy_addr;
   uint32 reg_size;
   uint32 reset_reg_phy_addr;
   uint32 reset_reg_size;
   uint32 int_irq_no;
   uint32 num_hw_buf;
   uint32 hw_revision;
} lpasshwio_prop_spdif_struct_t;
typedef struct
{
   uint32 lpass_reg_base_addr;
   uint32 lpass_reg_size;
   uint32 lpass_reset_reg_base_addr;
   uint32 lpass_reset_reg_size;
   uint32 hdmi_core_reg_base_addr;
   uint32 hdmi_core_reg_size;
   uint32 hw_revision;
} lpasshwio_prop_hdmi_output_struct_t;
typedef struct {
    uint64 base_phy_addr;
    uint32 size;
    uint32 size_afe_dma_buf;
    uint32 size_afe_working_buf;
    uint32 cache_attribute;
    uint32 ahb_voting_scale_factor;
} lpm_prop_struct_t;
HwdLpassPropertyType lpass_prop = { 0x09080000 };
lpasshwio_prop_lpaif_struct_t lpaif_prop = {
   0x09100000,
   (128*1024),
   {0x09101000,
    5,
   {PRIMARY_MI2S,SECONDARY_MI2S,TERTIARY_MI2S,QUATERNARY_MI2S,SPEAKER_I2S},
   {4,4,4,8,2},
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_SINK},
   SUPPORTED},
   {0x09101500,
    4,
   {PRIMARY_PCM,SECONDARY_PCM,TERTIARY_PCM,QUATERNARY_PCM},
   {4,4,4,4},
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT},
   SUPPORTED},
   {0x09101500,
    4,
   {PRIMARY_TDM,SECONDARY_TDM,TERTIARY_TDM,QUATERNARY_TDM},
   {8,8,8,8},
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT},
   SUPPORTED},
   {0x009A0000,
    (4*1024),
    SUPPORTED},
   {0x0911A000,
    4,
   {PRIMARY_MUX,SECONDARY_MUX,TERTIARY_MUX,QUATERNARY_MUX},
    SUPPORTED},
    0x40000000,
};
lpasshwio_prop_afe_dma_struct_t audioif_dma_prop = {
   0x09100000,
   (128*1024),
   0x0910A000,
   0x0910D000,
   0x09113000,
   0x0910D028,
   0x09113028,
   5,
   4,
   19,
   2,
   0x40000000
};
lpasshwio_prop_afe_dma_struct_t hdmiout_dma_prop = {
   0x090F8000,
   0x8000,
   0,
   0,
   0,
   0,
   0,
   4,
   0,
   94,
   0,
   0x10000000
};
HwdAvtimerPropertyType avtimer_prop = {
   0x090F7000,
   0x1000,
   19200000,
   0x090F7000,
   0,
   0,
   0,
   50,
   0x30040000
   };
HwdResamplerPropertyType resampler_prop = {
  0x09138000,
  0x8000,
  53,
  0x30000000
  };
lpasshwio_vfr_prop_t vfr_prop = {
  0x0908F000,
  0x9000,
  0x09092000,
  0x09094000,
  0x09095000,
  0x10000000,
  0x1,
  0x2,
  2,
  {51, 124},
  {{0x0, 0x1},
  {0x7, 0x1}}
};
lpasshwio_prop_riva_struct_t afe_riva_prop = {0, 0, 0, 0, 0, 0, 0, 0};
lpasshwio_prop_slimbus_struct_t afe_slimbus_prop = {
  1,
  1
};
HwdMidiPropertyType midi_prop = {0,0, 0, 0, 0,0 };
lpasshwio_prop_spdif_struct_t spdiftx_prop = {0, 0, 0, 0, 0, 0, 0};
lpasshwio_prop_hdmi_output_struct_t hdmi_output_prop = {
  0x090F8000,
  0x8000,
  0x090C8000,
  0x1000,
  0x009A0000,
  0x1000,
  0x10000000
};
lpm_prop_struct_t lpm_prop = {
                              0x9120000,
                              64*1024,
                              64*1024,
                              0,
                              QURT_MEM_CACHE_NONE_SHARED,
                              1
                              };
HwdGenericClkPropertyType genericclk_prop =
{
  0x9080000,
 (120*1024),
  0x40000000,
  23,
  {
    0x100, 0x101, 0x102, 0x103, 0x104, 0x105, 0x106, 0x107, 0x108, 0x109, 0x10A,
    0x200, 0x201, 0x202, 0x203, 0x204, 0x205, 0x206, 0x207,
    0x300, 0x301, 0x302,
    0x20000
  },
  8,
  { 0x200, 0x201, 0x202, 0x203, 0x204, 0x205, 0x206, 0x207},
  { 0x20000, 0x20000, 0x20000, 0x20000, 0x20000, 0x20000, 0x20000, 0x20000},
  { 61440000, 61440000, 61440000, 61440000, 61440000, 61440000, 61440000, 61440000},
};
