/*========================================================================
   Implements wrapper functions for resamplers.

  Copyright (c) 2009-2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: //components/rel/avs.adsp/2.7.1.c4/afe/services/static/src/AFESampRateConv.cpp#1 $
 ====================================================================== */

/*----------------------------------------------------------------------------
* Include Files
* -------------------------------------------------------------------------*/
#include "AFESampRateConv.h"
#include "AFEPortManager.h"
#include "AFEInternal.h"
#include "AFEMmpm_i.h"

#define NUM_IIR_FREQUENCIES 4

static const uint32_t freq_array[] = { 8000, 16000, 32000, 48000 };

static const uint32_t iir_resampler_kpps_table[NUM_IIR_FREQUENCIES][NUM_IIR_FREQUENCIES] =
{
    { 0,                              VOICE_RESAMPLER_KPPS_8K_TO_16K,  VOICE_RESAMPLER_KPPS_8K_TO_32K,  VOICE_RESAMPLER_KPPS_8K_TO_48K  },
    { VOICE_RESAMPLER_KPPS_16K_TO_8K, 0,                               VOICE_RESAMPLER_KPPS_16K_TO_32K, VOICE_RESAMPLER_KPPS_16K_TO_48K },
    { VOICE_RESAMPLER_KPPS_32K_TO_8K, VOICE_RESAMPLER_KPPS_32K_TO_16K, 0,                               VOICE_RESAMPLER_KPPS_32K_TO_48K },
    { VOICE_RESAMPLER_KPPS_48K_TO_8K, VOICE_RESAMPLER_KPPS_48K_TO_16K, VOICE_RESAMPLER_KPPS_48K_TO_32K, 0                               }
};

static inline uint16_t get_frequency_index(uint32_t freq)
{
  uint16_t index;
  for(index = 0; index < NUM_IIR_FREQUENCIES; index++)
  {
    if(freq == freq_array[index]) return index;
  }
  return index;
}

/*==========================================================================
Function Definitions
========================================================================== */
capi_v2_err_t capi_v2_event_callback_fir_rs(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_ptr);
/**
* This function is to select the resampler type for the given 
* case 
*
 * @param[in] resamp_type, requests the sampling rate converter type to be used
* @param[in] in_freq, input frequency for the resampler. 
* @param[in] out_freq, output frequency for the resampler. 
* @param[in] bytes_per_sample, number of bytes per sample (Min.
*  		of client and port bytes per sample)
 * @return samp_rate_conv_type_t, returns resampler type
* */ 

samp_rate_conv_type_t get_samp_rate_conv_type(afe_client_resampler_type resamp_type, uint32_t in_freq, uint32_t out_freq, uint32_t  bytes_per_sample)
{
	samp_rate_conv_type_t resampler_type = FIR_BASED_RESAMPLER;

	if(((IIR_BASED_SRC == resamp_type))&&  (TWO_BYTES_PER_SAMPLE == bytes_per_sample) &&
        ((AFE_PORT_SAMPLE_RATE_8K == in_freq) || (AFE_PORT_SAMPLE_RATE_16K == in_freq) || (AFE_PORT_SAMPLE_RATE_48K == in_freq) || (AFE_PORT_SAMPLE_RATE_32K == in_freq)) &&
        ((AFE_PORT_SAMPLE_RATE_8K == out_freq) || (AFE_PORT_SAMPLE_RATE_16K == out_freq) || (AFE_PORT_SAMPLE_RATE_48K == out_freq) || (AFE_PORT_SAMPLE_RATE_32K == out_freq)))
	{
		resampler_type = IIR_BASED_RESAMPLER;
	}
	return resampler_type; 
}

/**
* This function is the wrapper function for the initialization 
* of IIR resampler (Sample rate converters)
*
* @param[in] pClientInfo, pointer to the client instance
* @param[in] bytes_per_sample, number of bytes per sample (Min.
*  		of client and port bytes per sample)
* @param[in] in_freq, input frequency for the resampler.
* @param[in] out_freq, output frequency for the resampler.
* @param[in] samp_rate_conv, pointer to sample rate
*  		converter (resampler) instance.
* @return ADSPResult, returns success or failure of the
*  		  initialization
*  		  Caller should take care of deallocating any memory
*  		  created for this API call in case of failure
* */
ADSPResult sample_rate_conv_init_iir(samp_rate_conv_t *samp_rate_conv, uint16_t num_channels,  uint32_t  bytes_per_sample,
	uint32_t in_freq, uint32_t out_freq)
{
	uint16_t						unNum;
	void           					*iResample = NULL, *iResample_inst = NULL;
	uint32_t 						resampDefaultChannelSize,resampDefaultInstanceSize;
	uint32_t						voc_resampler_result;
	uint32_t						frame_size_out;

	/* For IIR Based Resampler. */
	/* Getting the buffer size for resampler instance memory*/
	voice_resampler_get_config_mem(&resampDefaultInstanceSize);

	/* Checking if the instance memory is already allocated \
	If so de-allocating the instance memory. If FIR resampler is already present, it has to be deleted
	using CAPIv2 interface. IIR resampler would not have CAPIv2 interface*/
	
	if(NULL != samp_rate_conv->resample_instance_ptr)
	{
		if (FIR_BASED_RESAMPLER == samp_rate_conv->resamp_type)
		{
			capi_v2_t *resampler_ptr = (capi_v2_t *)samp_rate_conv->resample_instance_ptr;
			if (NULL != resampler_ptr->vtbl_ptr)
			{
				resampler_ptr->vtbl_ptr->end(resampler_ptr);
			}
		}
		qurt_elite_memory_aligned_free(samp_rate_conv->resample_instance_ptr);
		samp_rate_conv->resample_instance_ptr = NULL;
		samp_rate_conv->resamp_type = INVALID_RESAMP_TYPE;	
	}

	/* Allocating the 8 byte aligned memory for resampler instance memory*/
	iResample_inst = qurt_elite_memory_aligned_malloc(resampDefaultInstanceSize, 8, QURT_ELITE_HEAP_DEFAULT);
	if (NULL == iResample_inst)
	{
		MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate memory for voc proc resampler instance memory");
		return ADSP_ENOMEMORY;
	}
	samp_rate_conv->resample_instance_ptr = iResample_inst;

	frame_size_out = samp_rate_conv->frame_size_out;
	/* Initializing the resampler instance */
	voc_resampler_result = voice_resampler_set_config(samp_rate_conv->resample_instance_ptr, in_freq, out_freq, bytes_per_sample << 3,samp_rate_conv->frame_size_in, &samp_rate_conv->frame_size_out);
	if(VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
	{
		return ADSP_EFAILED;
	}

	/* IIR based resampler updates the frame_size_out. Checking whether IIR based resampler returned the correct frame_size_out*/
	if(samp_rate_conv->frame_size_out != frame_size_out)
	{
	   MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " frame_size_out %lu, samp_rate_conv->frame_size_out %lu", frame_size_out, samp_rate_conv->frame_size_out);
	   MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " IIR based Resampler init failed");
	   return ADSP_EFAILED;
	}

	/* Getting the buffer size for resampler channel memory*/
	voc_resampler_result = voice_resampler_get_channel_mem(samp_rate_conv->resample_instance_ptr, &resampDefaultChannelSize);
	if(VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
	{
		return ADSP_EFAILED;
	}

	/* Initialize resampler lib obj for each of the channel */
	for (unNum = 0; unNum < num_channels; unNum++)
	{
		/* De-allocating the pre allocated channel memory*/
		if(NULL != samp_rate_conv->resample_chan_ptr[unNum])
		{
			qurt_elite_memory_aligned_free(samp_rate_conv->resample_chan_ptr[unNum]);
			samp_rate_conv->resample_chan_ptr[unNum] = NULL;
		}

		iResample = NULL;
		iResample = qurt_elite_memory_aligned_malloc(resampDefaultChannelSize, 8, QURT_ELITE_HEAP_DEFAULT);
		if (NULL == iResample)
		{
			MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate memory for voc proc resampler channel object");
			return ADSP_ENOMEMORY;
		}

	#ifdef DBG_BUFFER_ADDRESSES
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "BUFFERADDR AFE port resampler start : 0x%8x, size %d", iResample, resampDefaultChannelSize);
	#endif /* DBG_BUFFER_ADDRESSES */

		samp_rate_conv->resample_chan_ptr[unNum] = iResample;
		voc_resampler_result = voice_resampler_channel_init(samp_rate_conv->resample_instance_ptr, samp_rate_conv->resample_chan_ptr[unNum], resampDefaultChannelSize);
		if(VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
		{
			return ADSP_EFAILED;
		}
	}
	samp_rate_conv->delay_in_us = 0; // for IIR since no API/callback is available, delay_in_us is set to zero.
	return ADSP_EOK;
}


/**
* This function is the wrapper function for the initialization
* of various resamplers (Sample rate converters) 
*
* @param[in] pClientInfo, pointer to the client instance
* @param[in] bytes_per_sample, number of bytes per sample (Min.
*  		of client and port bytes per sample)
* @param[in] in_freq, input frequency for the resampler. 
* @param[in] out_freq, output frequency for the resampler. 
* @param[in] samp_rate_conv, pointer to sample rate 
*  		converter (resampler) instance.
* @return ADSPResult, retuns success or failure of the 
*  		  initialization
*  		  Caller should take care of deallocating any memory
*  		  created for this API call in case of failure
* */ 
ADSPResult sample_rate_conv_init(samp_rate_conv_t *samp_rate_conv, uint16_t num_channels,  uint32_t  bytes_per_sample, 
	uint32_t in_freq, uint32_t out_freq, samp_rate_conv_type_t resamp_type)
{	
	ADSPResult 						result = ADSP_EOK;
	capi_v2_err_t 					result2 = CAPI_V2_EOK;
	void                            *iResample_inst = NULL;

	switch(resamp_type)
	{
		case FIR_BASED_RESAMPLER:
		{
			capi_v2_init_memory_requirement_t memory_req = { 0 };
			capi_v2_prop_t req_props[] = {
					{CAPI_V2_INIT_MEMORY_REQUIREMENT, {reinterpret_cast<int8_t*>(&memory_req), 0, sizeof(memory_req)}, {FALSE, FALSE, 0}},
			};
			capi_v2_proplist_t req_proplist = {SIZE_OF_ARRAY(req_props), req_props};
			result2 = capi_v2_dynamic_resampler_get_static_properties(NULL, &req_proplist);
			if (result2 != ADSP_EOK)
				{
				MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"\n FIR type dynamic resampler get static properties failed. checking for IIR type");
					goto __generic_resampler_error_bailout;
				}

			/* Checking if the instance memory is already allocated \
			If so de-allocating the instance memory. If FIR resampler is already present, it has to be deleted
			using CAPIv2 interface. IIR resampler would not have CAPIv2 interface*/
			if(NULL != samp_rate_conv->resample_instance_ptr)
			{
				sample_rate_conv_deinit(samp_rate_conv);				
			}
	
			/*Allocating resampler module memory with the starting address is 8-byte aligned*/
			iResample_inst = qurt_elite_memory_aligned_malloc(memory_req.size_in_bytes, 8, QURT_ELITE_HEAP_DEFAULT);
			if (NULL == iResample_inst)
			{
				MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate memory for Generic resampler instance memory");
				return ADSP_ENOMEMORY;
			}
			samp_rate_conv->resample_instance_ptr = iResample_inst;
			memset(iResample_inst,0,memory_req.size_in_bytes);			
	
			// Set up init properties

			capi_v2_heap_id_t heap_id = { 0 };
			capi_v2_event_callback_info_t cb_info = { capi_v2_event_callback_fir_rs, (void *)samp_rate_conv };
	
			capi_v2_prop_t init_props[] = {
					{ CAPI_V2_HEAP_ID,             { reinterpret_cast<int8_t*>(&heap_id),    sizeof(heap_id),  sizeof(heap_id)  }, { FALSE, FALSE, 0} },
					{ CAPI_V2_EVENT_CALLBACK_INFO, { reinterpret_cast<int8_t*>(&cb_info),     sizeof(cb_info),   sizeof(cb_info)   }, { FALSE, FALSE, 0} }
			};
			capi_v2_proplist_t init_proplist = { SIZE_OF_ARRAY(init_props), init_props };

			result = capi_v2_dynamic_resampler_init((capi_v2_t *)samp_rate_conv->resample_instance_ptr, &init_proplist);
			if (result2 != ADSP_EOK)
			{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "FIR dynamic resampler init failed");
					result = ADSP_EFAILED;
					goto __generic_resampler_error_bailout;
			}
	
			capi_v2_t		*resampler_ptr;
			resampler_ptr 	= (capi_v2_t *)samp_rate_conv->resample_instance_ptr;
	
			//DISABLE USING HARDWARE RS FOR BOTH THE RESAMPLERS
			capi_v2_port_info_t capiv2_rs_port_info;
			capiv2_rs_port_info.is_valid = FALSE;

			param_id_dynamic_rs_force_disable_hw_rs_t capi_v2_rs_disable;
			capi_v2_rs_disable.force_disable = TRUE;

			capi_v2_buf_t rs_buf;
			rs_buf.data_ptr = (int8_t*)&capi_v2_rs_disable;
			rs_buf.actual_data_len = sizeof(param_id_dynamic_rs_force_disable_hw_rs_t);
			rs_buf.max_data_len = sizeof(param_id_dynamic_rs_force_disable_hw_rs_t);
	
	
			if (NULL != resampler_ptr->vtbl_ptr)
			{
				result2 = resampler_ptr->vtbl_ptr->set_param(resampler_ptr,
						AUDPROC_PARAM_ID_RESAMPLER_FORCE_DISABLE_HW_RS,
						&capiv2_rs_port_info,
						&rs_buf);

				if (CAPI_V2_FAILED(result2))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AFE sample rate conv: FIR resampler failed setParam to disable HW.");
					result = ADSP_EFAILED;
					goto __capiv2_resampler_error_bailout;
				}
			}
			struct
			{
				capi_v2_data_format_header_t d;
				capi_v2_standard_data_format_t f;
			}media_format;

			media_format.d.data_format = CAPI_V2_FIXED_POINT;

			media_format.f.bits_per_sample = (bytes_per_sample << 3);
			media_format.f.num_channels = num_channels;
			media_format.f.bitstream_format = CAPI_V2_FIXED_POINT;
			media_format.f.data_interleaving = CAPI_V2_DEINTERLEAVED_UNPACKED;			
			media_format.f.q_factor = (media_format.f.bits_per_sample == 16) ? PCM_16BIT_Q_FORMAT : ELITE_32BIT_PCM_Q_FORMAT;
			media_format.f.data_is_signed = TRUE;
			media_format.f.sampling_rate = in_freq;

			capi_v2_prop_t prop = {
					CAPI_V2_INPUT_MEDIA_FORMAT,	/* capi_v2_property_id_t */
					/* capi_v2_buf_t */
					{
							reinterpret_cast<int8_t*>(&media_format),
							sizeof(media_format),
							sizeof(media_format)
					},
					/* capi_v2_port_info_t*/
					{
							TRUE,
							TRUE,
							0,
					}
			};
			capi_v2_proplist_t proplist = { 1, &prop };
			if (NULL != resampler_ptr->vtbl_ptr)
			{
				result2 = resampler_ptr->vtbl_ptr->set_properties(resampler_ptr,&proplist);
				if (CAPI_V2_FAILED(result2))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AFE sample rate conv: failed to set property for input media format");
					result = ADSP_EFAILED;
					goto __capiv2_resampler_error_bailout;
				}
			
				media_format.f.sampling_rate = out_freq;

				prop.id = CAPI_V2_OUTPUT_MEDIA_FORMAT;
				prop.port_info.is_input_port = FALSE;
				prop.port_info.is_valid = TRUE;
				prop.port_info.port_index = 0;

				result2 = resampler_ptr->vtbl_ptr->set_properties(resampler_ptr,&proplist);
				if (CAPI_V2_FAILED(result2))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AFE sample rate conv: failed to set property for output media format");
					result = ADSP_EFAILED;
					goto __capiv2_resampler_error_bailout;
				}			
				samp_rate_conv->resamp_type = FIR_BASED_RESAMPLER;
			}
		}
		break;
		case IIR_BASED_RESAMPLER:
		{		
			result = sample_rate_conv_init_iir(samp_rate_conv, num_channels, bytes_per_sample, in_freq, out_freq);
			if (ADSP_EOK != result)
			{
				return result;
			}
			samp_rate_conv->resamp_type = IIR_BASED_RESAMPLER;
		}
		break;
		default:
		{
			MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported resampler type");
			return ADSP_EUNSUPPORTED;
		}
	}

	samp_rate_conv->bytes_per_sample = bytes_per_sample;

	return result;

__generic_resampler_error_bailout:
	/* Come to bailout if FIR resampler is not present. Resampler is changed to IIR only if the below conditions are met. */	
	if (IIR_BASED_RESAMPLER == get_samp_rate_conv_type(IIR_BASED_SRC,in_freq,out_freq,bytes_per_sample))
	{
		result = sample_rate_conv_init_iir(samp_rate_conv, num_channels,  bytes_per_sample, in_freq, out_freq);
		if (result != ADSP_EOK)
		{
			goto __capiv2_resampler_error_bailout;
		}
		samp_rate_conv->resamp_type = IIR_BASED_RESAMPLER;
		return result;
	}
	else
	{
		return ADSP_EFAILED;
	}
	
__capiv2_resampler_error_bailout:
	/* release the capiv2 resampler memory and return error code */
	sample_rate_conv_deinit(samp_rate_conv);
	return result;
}

/**
 * THis function is to clear the data memory of voice resampler
 * @param[in] resampler, pointer to instance of the resampler
 * @param[in] num_channels, number of channels
 * return ADSPResult
 */
ADSPResult sample_rate_conv_memory_init(samp_rate_conv_t *samp_rate_conv, uint16_t num_channels)
{
   uint16_t                unNum;
   uint32_t                   resampDefaultChannelSize;
   uint32_t                voc_resampler_result;


   voc_resampler_result = voice_resampler_get_channel_mem(samp_rate_conv->resample_instance_ptr, &resampDefaultChannelSize);
   if(VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
   {
      return ADSP_EFAILED;
   }
    for (unNum = 0; unNum < num_channels; unNum++)
    {
        voc_resampler_result = voice_resampler_channel_init(samp_rate_conv->resample_instance_ptr, samp_rate_conv->resample_chan_ptr[unNum], resampDefaultChannelSize);
        if(VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
        {
           return ADSP_EFAILED;
        }
    }
    return ADSP_EOK;
}
/**
* This function is for freeing the memory created for the 
* resampler. 
*
* @param[in] resampler, pointer to instance of the resampler
* @param[in] resampler_memory, pointer to instance voice 
*  		resampler memory.
* @param[in] resamplerType, information about the type of the 
*  		resampler.
* @return none
*/
void sample_rate_conv_deinit(samp_rate_conv_t *samp_rate_conv)
{
   capi_v2_t *resampler_ptr = NULL;
   if(!samp_rate_conv)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed cannot deinit sample rate conv, NULL ptr!");
      return;
   }

   for(uint32_t i = 0; i < MAX_AUDIO_CHANNELS; i++)
   {
      if(NULL != samp_rate_conv->resample_chan_ptr[i])
      {
         qurt_elite_memory_aligned_free(samp_rate_conv->resample_chan_ptr[i]);
         samp_rate_conv->resample_chan_ptr[i] = NULL;
      }
   }

   if(NULL != samp_rate_conv->resample_instance_ptr)
   {
	   if (FIR_BASED_RESAMPLER == samp_rate_conv->resamp_type)
	   {
		   resampler_ptr = (capi_v2_t *)samp_rate_conv->resample_instance_ptr;
			if(NULL != resampler_ptr->vtbl_ptr)
			{
				resampler_ptr->vtbl_ptr->end(resampler_ptr);
			}
	   }
	   
      qurt_elite_memory_aligned_free(samp_rate_conv->resample_instance_ptr);
      samp_rate_conv->resample_instance_ptr = NULL;
	  samp_rate_conv->resamp_type = INVALID_RESAMP_TYPE;
   }
}

/**
* This function is the wrapper function for the various 
* resamplers (Sample rate converters) 
*
* @param[in] samp_rate_conv, resampler strcture
* @param[in] in_buf, input buffer for the resampler. 
* @param[in] out_buf, output buffer for the resampler. 
* @param[in] nchan, number of channels.
* @param[out] samples_processed, actual number of samples 
*  		processed by resampler.
* @param[in] mode, resampler mode. * 
* @return ADSPResult 
*/
ADSPResult sample_rate_conv_process(samp_rate_conv_t *samp_rate_conv, int8 *in_buf, int8 *out_buf, uint32 *samples_processed, uint16_t nChan)
{
   capi_v2_err_t 					resampler_result = CAPI_V2_EOK;
   uint32_t						voc_resampler_result = VOICE_RESAMPLE_SUCCESS;
   capi_v2_t 						*resampler_ptr = NULL;
   int8_t							i = 0;
   uint32_t							bytes_per_sample = samp_rate_conv->bytes_per_sample;
   uint32_t						input_chan_spacing = (samp_rate_conv->in_ch_spacing_in_bytes);
   uint32_t						output_chan_spacing = (samp_rate_conv->out_ch_spacing_in_bytes);

   switch (samp_rate_conv->resamp_type)
   {
      case FIR_BASED_RESAMPLER:
      {
         resampler_ptr = (capi_v2_t *)samp_rate_conv->resample_instance_ptr;
         capi_v2_stream_data_t			inp_buf_stream;
         capi_v2_stream_data_t			out_buf_stream;
         capi_v2_stream_data_t			*inp_buf_stream_ptr;
         capi_v2_stream_data_t			*out_buf_stream_ptr;

         memset(&inp_buf_stream, 0, sizeof(capi_v2_stream_data_t));
         memset(&out_buf_stream, 0, sizeof(capi_v2_stream_data_t));

         capi_v2_buf_t	input_buf[nChan];
         capi_v2_buf_t	output_buf[nChan];
         memset(&input_buf, 0, nChan * sizeof(capi_v2_buf_t));
         memset(&output_buf, 0, nChan * sizeof(capi_v2_buf_t));

         inp_buf_stream_ptr = &inp_buf_stream;
         inp_buf_stream.bufs_num = nChan;

         for (i = 0; i < nChan; i++)
         {
            input_buf[i].data_ptr = in_buf + (i * input_chan_spacing);
            input_buf[i].actual_data_len = (samp_rate_conv->frame_size_in * bytes_per_sample);
            input_buf[i].max_data_len = (samp_rate_conv->frame_size_in * bytes_per_sample);
         }
         inp_buf_stream.buf_ptr = &input_buf[0];

         out_buf_stream_ptr = &out_buf_stream;
         out_buf_stream.bufs_num = nChan;

         for (i = 0; i < nChan; i++)
         {
            output_buf[i].data_ptr = out_buf + (i * output_chan_spacing);
            output_buf[i].actual_data_len = 0;
            output_buf[i].max_data_len = (samp_rate_conv->frame_size_out * bytes_per_sample);
         }
         out_buf_stream.buf_ptr = &output_buf[0];

         resampler_result = resampler_ptr->vtbl_ptr->process(resampler_ptr, &inp_buf_stream_ptr, &out_buf_stream_ptr);
         if (CAPI_V2_EOK != resampler_result)
         {
            return ADSP_EFAILED;
         }

         *samples_processed = (output_buf[0].actual_data_len) / bytes_per_sample;
      }
      break;
      case IIR_BASED_RESAMPLER:
      {
         int8_t   * ptr_curr_input_channel, *ptr_curr_output_channel;

         for (i = 0; i < nChan; i++)
         {
            ptr_curr_input_channel = in_buf + (i * input_chan_spacing);
            ptr_curr_output_channel = out_buf + (i * output_chan_spacing);

            voc_resampler_result = voice_resampler_process(samp_rate_conv->resample_instance_ptr, samp_rate_conv->resample_chan_ptr[i], (int8 *)ptr_curr_input_channel, (int32)samp_rate_conv->frame_size_in, (int8 *)ptr_curr_output_channel, (int32)samp_rate_conv->frame_size_out);
            *samples_processed = samp_rate_conv->frame_size_out;

            if (VOICE_RESAMPLE_SUCCESS != voc_resampler_result)
            {
               return ADSP_EFAILED;
            }
         }
      }
      break;
      default:
         /* return error*/
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported resampler type: %lu", samp_rate_conv->resamp_type);
         return ADSP_EFAILED;
   }
   return ADSP_EOK;
}

/**
* This function is the wrapper function for getting the 
* algorithm delay of various resamplers (Sample rate converters)
*  
* @param[in] samp_rate_conv, pointer to sample rate 
*  		converter (resampler) instance.
* @param[out] delay_us, delay of the resampler in micro sec.  
* */ 
void sample_rate_conv_get_delay(samp_rate_conv_t *samp_rate_conv, uint64_t *delay_us)
{	
	*delay_us = samp_rate_conv->delay_in_us;
	return;
}


capi_v2_err_t capi_v2_event_callback_fir_rs(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_ptr)
{

	samp_rate_conv_t* samp_rate_conv = (samp_rate_conv_t*)(context_ptr);

	switch(id)
	{
		case CAPI_V2_EVENT_KPPS:
		{
			if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_KPPS_t))
			{
				return CAPI_V2_ENEEDMORE;
			}
			samp_rate_conv->fir_rs_kpps = reinterpret_cast<capi_v2_event_KPPS_t*>(event_ptr->payload.data_ptr)->KPPS;
			break;
		}

		case CAPI_V2_EVENT_BANDWIDTH:
		{
			if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_KPPS_t))
			{
				return CAPI_V2_ENEEDMORE;
			}         
			capi_v2_event_bandwidth_t *bw=reinterpret_cast<capi_v2_event_bandwidth_t*>(event_ptr->payload.data_ptr);
			samp_rate_conv->fir_rs_bw = bw->code_bandwidth + bw->data_bandwidth;
			break;
		}
		
		case CAPI_V2_EVENT_ALGORITHMIC_DELAY:
		{
			if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_algorithmic_delay_t))
			{
				return CAPI_V2_ENEEDMORE;
			}
			samp_rate_conv->delay_in_us = reinterpret_cast<capi_v2_event_algorithmic_delay_t*>(event_ptr->payload.data_ptr)->delay_in_us;
			break;
		}
		
		default:
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AFESampleRateconv: callback_fir_rs: Event id %lu is not supported.", static_cast<uint32_t>(id));
			return CAPI_V2_EUNSUPPORTED;
	}
	return CAPI_V2_EOK;
}

/*This function should be called by connecting afe client, when re-sampler is needed.
 * If FIR re-sampler, gets client re-sampler kpps and BW through CAPIv2.
 * If IIR re-sampler, gets client re-sampler kpps from IIR re-sampler predefined table.
 *
 *pDevPort          : Used pDevPort to access mmpm_info_ptr, from which aggregated client kpps/bw is updated/stored
 *psNewClient       : Used psNewClient to store individual client kpps/bw
 * */
void afe_port_aggregate_client_resampler_kpps_bw(void *dev_ptr, void *client_info_ptr)
{
  afe_dev_port_t    *pDevPort    = (afe_dev_port_t *)dev_ptr;
  afe_client_info_t *psNewClient = (afe_client_info_t *)client_info_ptr;

  if( NULL == pDevPort || NULL == psNewClient )
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Port or/and client info pointer is NULL, port pointer:0x%x, client info pointer: 0x%x",\
          pDevPort, psNewClient);
    return;
  }

  uint32_t           inFreq = 0, outFreq = 0, client_resamp_kpps = 0, client_resamp_bw = 0;
  uint16_t           inFreq_index = 0, outFreq_index = 0;

  samp_rate_conv_t   *samp_rate_conv  = &psNewClient->samp_rate_conv;
  afe_mmpm_info_t    *mmpm_info_ptr   = (afe_mmpm_info_t *)pDevPort->mmpm_info_ptr;

  if( (NULL == mmpm_info_ptr) || (NULL == samp_rate_conv) )
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "mmpm_info_ptr/samp_rate_conv pointer is/are NULL, mmpm_info_ptr: 0x%x, samp_rate_conv ptr: 0x%x",\
          mmpm_info_ptr, samp_rate_conv );
    return;
  }

  if(FIR_BASED_RESAMPLER == samp_rate_conv->resamp_type)
  {
    client_resamp_kpps   = samp_rate_conv->fir_rs_kpps;
    client_resamp_bw     = samp_rate_conv->fir_rs_bw;
  }
  else if(IIR_BASED_RESAMPLER == samp_rate_conv->resamp_type)
  {
    if ((psNewClient->afe_client.data_path == AFE_TX_OUT) ||
        (psNewClient->afe_client.data_path == AFE_PP_OUT) ||
        (psNewClient->afe_client.data_path == AFE_TX_PP_AUDIO_SENSE_OUT))
    {
      inFreq            = pDevPort->sample_rate;
      outFreq           = psNewClient->afe_client.sample_rate;
    }
    else
    {
      inFreq            = psNewClient->afe_client.sample_rate;
      outFreq           = pDevPort->sample_rate;
    }

    inFreq_index        = get_frequency_index(inFreq);
    outFreq_index       = get_frequency_index(outFreq);

    /*afe_port_aggregate_client_resampler_kpps_bw() is called after afe port client resample init(), where supported frequencies (8k, 16k and 48k) for
     * IIR re-sampling is verified. Therefore, ( samp_rate_conv->resamp_type = IIR_BASED_RESAMPLER ) indicates, inFreq and outFreq were supported frequencies
     * for IIR re-sampling. Hence, below error condition will never happens*/
    if(NUM_IIR_FREQUENCIES <= inFreq_index || NUM_IIR_FREQUENCIES <= outFreq_index)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe unsupported IIR resampler frequency, inFreq: %lu, outFreq: %lu",\
            inFreq, outFreq);
        return;
    }

    client_resamp_kpps  = iir_resampler_kpps_table[inFreq_index][outFreq_index];

    /*client_resamp_bw  = scaling_factor*common_bw_factor
     *          200000  = scaling_factor*(48000*2*1)           => scaling factor as 25/12
     * */
    client_resamp_bw    = (25*afe_port_get_common_bw_factor(pDevPort))/12;
  }

  psNewClient->client_kpps                     += client_resamp_kpps;
  mmpm_info_ptr->dyn_vot_info.agr_client_kpps  += client_resamp_kpps;

  psNewClient->client_bw                       += client_resamp_bw;
  mmpm_info_ptr->dyn_vot_info.agr_client_bw    += client_resamp_bw;

  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AFE MPPS:: client_resamp_kpps: %lu, client_resamp_bw: %lu",\
        client_resamp_kpps, client_resamp_bw );
}

