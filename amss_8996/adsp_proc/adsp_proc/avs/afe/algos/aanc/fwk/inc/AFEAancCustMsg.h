/**
@file AFEAancCustMsg.h

@brief For custom message passing to the CAPI V2 AANC lib

*/

/*========================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/afe/algos/aanc/fwk/inc/AFEAancCustMsg.h#1 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
4/4/2014   rv       Created

==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2015 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/


#ifndef AFEAANCCUSTMSG_H_
#define AFEAANCCUSTMSG_H_

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/

/* System */
#include "qurt_elite.h"
#include "Elite.h"


/** Macro for custom param id from CAPI V2 modules */
#define CAPI_V2_AFE_AANC_PARAM_ID_GAIN_1            0x00001001
#define CAPI_V2_AFE_AANC_PARAM_ID_GAIN_2            0x00001002
#define CAPI_V2_AFE_AANC_PARAM_ID_LIB_SIZE          0x00001003
#define CAPI_V2_AFE_AANC_PARAM_ID_LIB_PTR           0x00001004
#define CAPI_V2_AFE_AANC_PARAM_ID_LIB_SAMPLING_RATE 0x00001005
#define CAPI_V2_AFE_AANC_PARAM_ID_ACTUAL_FRAME_SIZE 0x00001006
#define CAPI_V2_AFE_AANC_PARAM_ID_LIB_ALL_CFG_RCVD  0x00001009
#define CAPI_V2_AFE_AANC_PARAM_ID_LIB_VALID_CFG     0x00001010


/** Macro for custom property id from CAPI V2 modules */
#define CAPI_V2_AFE_AANC_PROP_ID_INIT_FRAME_BUFFER_TX 0x00002001
#define CAPI_V2_AFE_AANC_PROP_ID_INIT_FRAME_BUFFER_RX 0x00002002
#define CAPI_V2_AFE_AANC_PROP_ID_INIT_LIB_CFG         0x00002003
#define CAPI_V2_AFE_AANC_PROP_ID_INIT_LOG             0x00002004


#ifdef __cplusplus
}
#endif //__cplusplus

#endif /* AFEAANCCUSTMSG_H_ */
