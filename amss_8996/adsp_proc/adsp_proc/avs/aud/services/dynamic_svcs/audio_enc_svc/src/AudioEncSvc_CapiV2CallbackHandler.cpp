/** @file AudioEncSvc_CapiV2CallbackHandler.cpp
This file contains functions for Elite Encoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_enc_svc/src/AudioEncSvc_CapiV2CallbackHandler.cpp#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
07/22/14    rbhatnk      Created file.

========================================================================== */

#include "AudioEncSvc_CapiV2CallbackHandler.h"
#include "EliteMsg_Custom.h"
#include "AudioEncSvc_Includes.h"
#include "AudioEncSvc_CapiV2Util.h"

static capi_v2_err_t audio_enc_svc_capi_v2_callback(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_buf_t *payload = &event_info_ptr->payload;

   enc_CAPI_callback_obj_t *cb_obj = reinterpret_cast<enc_CAPI_callback_obj_t*>(context_ptr);

   AudioEncSvc_t *pMe = cb_obj->pMe;
   if (!( (cb_obj->capi_index < ENC_SVC_MAX_CAPI) && (NULL != pMe->capiContainer[cb_obj->capi_index])))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error in callback function. Data possibly corrupted.");
      return CAPI_V2_EFAILED;
   }

   enc_CAPI_container_t *capi_container = pMe->capiContainer[cb_obj->capi_index];

   if (payload->actual_data_len > payload->max_data_len)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is greater than the max size %lu for id %lu.",
            payload->actual_data_len, payload->max_data_len, static_cast<uint32_t>(id));
      return CAPI_V2_EBADPARAM;
   }

   switch(id)
   {
   case CAPI_V2_EVENT_KPPS:
   {
      if (payload->actual_data_len < sizeof(capi_v2_event_KPPS_t))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
               payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
         return CAPI_V2_ENEEDMORE;
      }

      capi_v2_event_KPPS_t *kpps = reinterpret_cast<capi_v2_event_KPPS_t*>(payload->data_ptr);

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module KPPS  %lu", static_cast<uint32_t>(kpps->KPPS));

      if (capi_container->kpps != kpps->KPPS)
      {
         capi_container->kpps = kpps->KPPS;
         pMe->event_mask = (pMe->event_mask | AUD_ENC_SVC_EVENT__KPPS_MASK);
      }

      return CAPI_V2_EOK;
   }
   case CAPI_V2_EVENT_BANDWIDTH:
   {
      if (payload->actual_data_len < sizeof(capi_v2_event_bandwidth_t))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
               payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
         return CAPI_V2_ENEEDMORE;
      }

      capi_v2_event_bandwidth_t *bw = reinterpret_cast<capi_v2_event_bandwidth_t*>(payload->data_ptr);
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module BW (code,data) %lu %lu.", bw->code_bandwidth, bw->data_bandwidth);

      if (capi_container->code_bw != bw->code_bandwidth)
      {
         capi_container->code_bw = bw->code_bandwidth;
         pMe->event_mask = (pMe->event_mask | AUD_ENC_SVC_EVENT__BW_MASK);
      }
      if (capi_container->data_bw != bw->data_bandwidth)
      {
         capi_container->data_bw = bw->data_bandwidth;
         pMe->event_mask = (pMe->event_mask | AUD_ENC_SVC_EVENT__BW_MASK);
      }
      return CAPI_V2_EOK;
   }
   case CAPI_V2_EVENT_PROCESS_STATE:
   {
      if (payload->actual_data_len < sizeof(capi_v2_event_process_state_t))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
               payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
         return CAPI_V2_ENEEDMORE;
      }
      capi_v2_event_process_state_t *pData = (capi_v2_event_process_state_t*)payload->data_ptr;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module process check set to %lu", static_cast<uint32_t>(pData->is_enabled));

      return CAPI_V2_EOK;
   }
   case CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED:
   {
      if (payload->actual_data_len < sizeof(capi_v2_set_get_media_format_t))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
               payload->actual_data_len, sizeof(capi_v2_set_get_media_format_t), static_cast<uint32_t>(id));
         return CAPI_V2_ENEEDMORE;
      }

      capi_v2_set_get_media_format_t *pData = reinterpret_cast<capi_v2_set_get_media_format_t*>(payload->data_ptr);
      bool_t changed = false;

      switch(pData->format_header.data_format)
      {
      case CAPI_V2_FIXED_POINT:
      case CAPI_V2_IEC61937_PACKETIZED:
      {
         if (payload->actual_data_len < (sizeof(capi_v2_set_get_media_format_t)+sizeof(capi_v2_standard_data_format_t)))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size for id %lu.",
                  payload->actual_data_len, static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }

         if (!event_info_ptr->port_info.is_valid || event_info_ptr->port_info.is_input_port || (event_info_ptr->port_info.port_index > 0))
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. port is not valid or is an input port or not zero.");
            return CAPI_V2_EBADPARAM;
         }

         enc_CAPI_container_t *last_capi = AudioEncSvc_GetLastCapi(pMe);
         enc_CAPI_container_t *capi_cont = pMe->capiContainer[cb_obj->capi_index];

         if (last_capi != capi_cont)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Ignoring media format event from non-last CAPI");
            return CAPI_V2_EOK;
         }

         EncMediaFmt_t *out_med_fmt = &(pMe->out_med_fmt);

         capi_v2_standard_data_format_t *pPcmData = (capi_v2_standard_data_format_t*) (payload->data_ptr+sizeof(capi_v2_set_get_media_format_t));

         //no need to raise event for bps change because the encoder always outputs at the bps needed by the PP (set during asm open)
         out_med_fmt->bits_per_sample = pPcmData->bits_per_sample;

         if (out_med_fmt->num_channels != pPcmData->num_channels)
         {
            out_med_fmt->num_channels = pPcmData->num_channels;
            capi_cont->media_fmt_event = true;
            changed=true;
         }
         if (out_med_fmt->sample_rate != pPcmData->sampling_rate)
         {
            out_med_fmt->sample_rate = pPcmData->sampling_rate;
            capi_cont->media_fmt_event = true;
            changed=true;
         }

         for (uint32_t i=0;((i<sizeof(out_med_fmt->chan_map)) && (i<pPcmData->num_channels)); i++)
         {
            if (out_med_fmt->chan_map[i] != pPcmData->channel_type[i])
            {
               out_med_fmt->chan_map[i] = pPcmData->channel_type[i];
               capi_cont->media_fmt_event = true;
               changed=true;
            }
         }
         if (pData->format_header.data_format == CAPI_V2_FIXED_POINT)
         {
            if (pPcmData->data_interleaving != CAPI_V2_DEINTERLEAVED_PACKED)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Unsupported data interleaving %u for fixed point data ", pPcmData->data_interleaving);
            }
         }
         break;
      }
      case CAPI_V2_RAW_COMPRESSED:
      {
         capi_v2_raw_compressed_data_format_t *pRawData = (capi_v2_raw_compressed_data_format_t*) (payload->data_ptr+sizeof(capi_v2_set_get_media_format_t));
         //depacketizer and encoder
         if (pMe->out_med_fmt.fmt_id != pRawData->bitstream_format)
         {
            pMe->out_med_fmt.fmt_id = pRawData->bitstream_format;
            changed=true;
            //no need to raise flag
         }
         break;
      }
      default:
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. Changing media type not supported for format %d",
               pData->format_header.data_format);
         return CAPI_V2_EUNSUPPORTED;
      }

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module output format updated for capi_index %lu. changed? = %u", cb_obj->capi_index, changed);

      return CAPI_V2_EOK;
   }
   case CAPI_V2_EVENT_PORT_DATA_THRESHOLD_CHANGE:
   {
      if (payload->actual_data_len < sizeof(capi_v2_port_data_threshold_change_t))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.", payload->actual_data_len, sizeof(capi_v2_port_data_threshold_change_t), static_cast<uint32_t>(id));
         return CAPI_V2_ENEEDMORE;
      }

      if (!event_info_ptr->port_info.is_valid )
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. port is not valid ");
         return CAPI_V2_EBADPARAM;
      }

      capi_v2_port_data_threshold_change_t *pData = reinterpret_cast<capi_v2_port_data_threshold_change_t*>(payload->data_ptr);
      if (event_info_ptr->port_info.is_input_port)
      {
         if (event_info_ptr->port_info.port_index > 0)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Input port %lu more than supported value", event_info_ptr->port_info.port_index);
            return CAPI_V2_EFAILED;
         }
         capi_container->in_port_event_new_size = pData->new_threshold_in_bytes;
         pMe->event_mask |= AUD_ENC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK;

         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " CAPI index %lu Input port %lu event with new buf len = %lu.", cb_obj->capi_index, event_info_ptr->port_info.port_index, pData->new_threshold_in_bytes);
      }
      else
      {
         if (event_info_ptr->port_info.port_index > 0)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Output port %lu more than supported value", event_info_ptr->port_info.port_index);
            return CAPI_V2_EFAILED;
         }

         capi_container->out_port_event_new_size = pData->new_threshold_in_bytes;
         pMe->event_mask |= AUD_ENC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK;

         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " CAPI index %lu  Output port %lu event with new buf len = %lu.", cb_obj->capi_index, event_info_ptr->port_info.port_index, pData->new_threshold_in_bytes);
      }

      return CAPI_V2_EOK;
   }
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. ID %lu not supported.", static_cast<uint32_t>(id));
      return CAPI_V2_EUNSUPPORTED;
   }
   }
   return result;
}

capi_v2_event_callback_info_t audio_enc_svc_get_capi_v2_callback_handler(AudioEncSvc_t *pMe, uint32_t capi_index)
{
   capi_v2_event_callback_info_t cb_info;
   cb_info.event_cb = audio_enc_svc_capi_v2_callback;

   pMe->capiContainer[capi_index]->capi_callback_obj.capi_index = capi_index;
   pMe->capiContainer[capi_index]->capi_callback_obj.pMe = pMe;

   cb_info.event_context = (void*)&pMe->capiContainer[capi_index]->capi_callback_obj;

   return cb_info;
}

#ifdef ELITE_CAPI_H
#error "Do not include CAPI V1 in this file"
#endif
