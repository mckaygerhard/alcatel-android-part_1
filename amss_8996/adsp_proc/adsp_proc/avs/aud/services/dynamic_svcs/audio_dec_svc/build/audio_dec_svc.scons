#===============================================================================
#
# AVS AU
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2009-2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: $
#  $DateTime: $
#  $Author:  $
#  $Change:  $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')

if 'USES_ENABLE_FUNCTION_SECTIONS' in env:
   env.Append(CFLAGS = '-ffunction-sections')

core_public_apis = [
   'DAL',
   'DEBUGTOOLS',
   'SERVICES',
   'KERNEL',
   ]

env.RequirePublicApi(core_public_apis, area='core')
env.RequireRestrictedApi(['AVS', 'MODULE_INTERFACES_UTILS', 'SHARED_LIBRARY_INC_PATHS'])
env.RequireProtectedApi('AVS')
env.RequirePublicApi([
  'ADSP_AMDB',
])
env.Append(CFLAGS = ' -Werror ')


platform_public_apis = [
    'QAIC',
    'STDDEF',
    'HAPSDK'
    ]

env.RequirePublicApi(platform_public_apis, area='platform') # This has to be the last include command, so that the Elite_CAPI.h and Elite_APPI.h are not overridden

env.PublishPrivateApi('AVS',[
   '../inc',
   '../build',
   '${AVS_ROOT}/aud/algorithms/utils/dtshd/packetizer/CDtshdPacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/dts/dts_broadcast/CDTS_BCDecLib/inc',  
   '${AVS_ROOT}/aud/algorithms/dts/dts_broadcast/CDTS_BCDecLib/inc',  
   '${AVS_ROOT}/aud/algorithms/dts/dts_lbr/CDTS_LBRDecLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/heaac/dec/CHeaacDec/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/adpcm/qcom/dec/CADPCMDecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/amrwbplus/etsi/dec/CEtsiAmrWbPlusDecLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/flac/tataelxsi/dec/CFlacDecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/mp3/common/CComboMp3DecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/mp2/Allgo/dec/CHpMp2DecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/vorbis/tataelxsi/dec/CVorbisDecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/wmapro/ms/dec/CWmaProDecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/wmastd/ms/dec/CWmaStdDecoderLib/inc',   
   '${AVS_ROOT}/aud/algorithms/dolby/dolby_mobile/dec/eAC3/CeAc3DecoderLib/inc',
   '${AVS_ROOT}/aud/algorithms/dolby/dolby_mobile/dec/eAC3/CComboDDPDecPacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/generic/depacketizer/CDepacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/dtmf/dec/CDtmfGeneratorLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/ac3/packetizer/CAc3PacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/eac3/packetizer/CeAc3PacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/aac/packetizer/CAacPacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/dts/packetizer/CDtsPacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/mat/packetizer/CMatPacketizerLib/inc',
   '${AVS_ROOT}/aud/algorithms/utils/passthru/formatter/CPassthruFormatterLib/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/alac/dec/CAlacDecoder/inc',
   '${AVS_ROOT}/aud/algorithms/audencdec/ape/dec/CApeDecoder/inc'
   ])

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = '..'
LIBNAME = 'audio_dec_svc'
#import pdb; pdb.set_trace()

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

avs_sources = env.GlobSourceFiles(['src/*.cpp', 'src/*.c'], SRCPATH,posix=True)

# Dependency on the header files for amdb. These files are generated as part of the build
env.AddQaicHeaderDep(avs_sources, "adsp_amdb")


env.AddLibrary(['AVS_ADSP','AVS_ADSP_USER'], '${BUILDPATH}/'+LIBNAME, avs_sources) 


