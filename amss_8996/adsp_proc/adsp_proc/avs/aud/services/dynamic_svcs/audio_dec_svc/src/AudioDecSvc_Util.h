
/*========================================================================

This file contains declarations of Audio DecoderService utility functions.

Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_Util.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
07/12/10   Wen Jin   Created file.

========================================================================== */
#ifndef AUDIODECSVC_UTIL_H
#define AUDIODECSVC_UTIL_H

#include "EliteLoggingUtils_i.h"

#ifndef MIN
#define MIN(m,n)  ((m < n) ? m : n)
#endif

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "Elite.h"
#include "qurt_elite.h"
#include "adsp_media_fmt.h"
#include "adsp_asm_data_commands.h"

#include "AudioDecSvc.h"
#include "AudioDecSvc_Structs.h"
#include "byte_conversion.h"

/* -----------------------------------------------------------------------
** Global definitions/forward declarations
** ----------------------------------------------------------------------- */
#define CHECK_ERR_BITS_PER_SAMPLE(bits_per_sample)                                              \
      if ((bits_per_sample != 16) && (bits_per_sample != 24))                                   \
      {                                                                                         \
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Invalid bits per sample %u",bits_per_sample);   \
         return ADSP_EBADPARAM;                                                                 \
      }

ADSPResult AudioDecSvc_CreateCAPIs(AudioDecSvc_t *pMe, AudioDecSvcInitParams_t *pInitParams, dec_init_time_get_params_t *dec_params);
ADSPResult AudioDecSvc_CreateInternalInputBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container,
      uint32_t inBufSize, dec_capi_port_index_t in_capi_port_index);
ADSPResult AudioDecSvc_CreateInternalOutputBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container,
      uint32_t outBufSize, dec_capi_port_index_t out_capi_port_index);
ADSPResult AudioDecSvc_CheckCreateScratchBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container, uint32_t outBufSize);
ADSPResult AudioDecSvc_CreateCirBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t* pOutStrm, uint32_t BufSize, uint32_t numBufs);
ADSPResult AudioDecSvc_CreateExternalBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStream, dec_CAPI_container_t *capi_container, uint32_t outBufSize, uint32_t numOutBufs);
void AudioDecSvc_DestroyBuffers(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_UpdateMediaFmt(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, uint8_t *pFmtBlk, uint32_t ulFmtBlkSize);
ADSPResult AudioDecSvc_UpdateMediaFmt_PeerService(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm);

void AudioDecSvc_InitOutDataBuf(elite_msg_data_buffer_t *pBuf, AudioDecSvc_OutStream_t *pOutStrm);

ADSPResult AudioDecSvc_GetInputDataCmd(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm);
ADSPResult AudioDecSvc_FreeInputDataCmd(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, ADSPResult status);
ADSPResult AudioDecSvc_ResetDecoder(AudioDecSvc_t *pMe);
ADSPResult AudioDecSvc_FlushInputDataQ(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm);
void AudioDecSvc_setThreadStackSize( uint32_t *threadStackSize, uint32_t formatID);

ADSPResult AudioDecSvc_NotifyPeerSvcWithMediaFmtUpdate (AudioDecSvc_t* pMe,  AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, int32_t nSampleRateAfter,
                                                       int32_t nChannelsAfter, uint8_t * pucChannelMappingAfter,
                                                       uint16_t bits_per_sample);

ADSPResult AudioDecSvc_SendPcmToPeerSvc (AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, elite_msg_data_buffer_t *pOutBuf );
ADSPResult AudioDecSvc_InitTsState(DecTimeStampState_t *pTsState);
ADSPResult AudioDecSvc_FillTsStateFromInBuf(DecTimeStampState_t *pTsState,
      uint32_t dec_fmt,      uint64_t ts,      bool_t ts_valid,      bool_t ts_continue,      bool_t eof_flag, bool_t WasPrevDecResNeedMore);
ADSPResult AudioDecSvc_UpdateNextOutbufTS(uint32_t ulOutbufSize,
                                          elite_multi_channel_pcm_fmt_blk_t* pOutputFmt,
                                          DecTimeStampState_t* pTsState,
                                          uint32_t dec_fmt);

ADSPResult AudioDecSvc_GetDecoderParams(AudioDecSvc_t *pMe,
                                        uint32_t *SampleRateAfter,
                                        DecChannelMap_t **channelCfgPtr);
ADSPResult AudioDecSvc_SendSrCmEvent(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm,
                                     uint32_t sample_rate,
                                     DecChannelMap_t *chan_map);
void AudioDecSvc_RemoveInitialSamples(uint32_t *samples_to_remove,
                                      uint32_t *valid_bytes_in_buffer,
                                      uint8_t * buffer_start,
                                      uint16_t num_channels,
                                      uint32_t sample_rate,
                                      uint64_t *next_buffer_ts,
                                      uint64_t *next_out_buf_sample_count,
                                      uint32_t bytes_per_sample);

void AudioDecSvc_RemoveTrailingSamples(uint32_t samples_to_remove,
                                       uint32_t *valid_bytes_in_buffer,
                                       uint8_t * buffer_start,
                                       uint16_t num_channels,
                                       uint32_t bytes_per_sample);

void AudioDecSvc_UpdatePrevFmtBlk(AudioDecSvc_t* pMe,AudioDecSvc_InpStream_t *pInpStrm,
                                  dec_CAPI_container_t *capi_cont,
                                  int32_t nSampleRateAfter,
                                  int32_t nChannelsAfter,
                                  uint8_t *ucChannelMappingAfter,
                                  uint16_t bits_per_sample,uint8_t capi_out_index);


ADSPResult AudioDecSvc_HandlePortDataThreshChangeEvent(AudioDecSvc_t *pMe);
ADSPResult AudioDecSvc_RaiseKCPSUpdateEvent(AudioDecSvc_t* pMe,uint32_t eventToken);
ADSPResult AudioDecSvc_RaiseBwUpdateEvent(AudioDecSvc_t* pMe, uint32_t pcm_bw, uint32_t enc_bw, uint32_t eventToken);

ADSPResult AudioDecSvc_CopyPcmOr61937MediaFmt(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, void *pFmtBlk );

#define AUDIOLOG_AUD_DEC_PCM_IN_TAP_ID      0x0001 /* Tap ID for logging PCM input      */
#define AUDIOLOG_AUD_DEC_BS_IN_TAP_ID           0x0002 /* Tap ID for logging bitstream input */

ADSPResult AudioDecSvc_LogBitStreamData(int8_t *buf_addr, uint32_t buf_size,
                                        uint32_t session_id, uint32_t ulDecFormatId);
ADSPResult AudioDecSvc_LogPcmData(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, int8_t * buf_addr, uint32_t buf_size);
ADSPResult AudioDecSvc_DiscardMarkBuffer(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm);

/*Function to check if decoding has begun*/
static inline bool_t decoding_begun(AudioDecSvc_t *pMe)
{
   if (pMe->capiContainer[0]->PrevFmtBlk[0].num_channels == 0)
      return false;
   else
      return true;
}

static inline ADSPResult AudioDecSvc_PopOutBuf(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   ADSPResult nResult;
   elite_msg_data_buffer_t *pOutBuf = NULL;

   // if a buf is already popped, return
   if (NULL != pOutStrm->outDataBufferNode.pBuffer)
   {
      return ADSP_EOK;
   }

   // Take next buffer off the q
   if(ADSP_FAILED(nResult = qurt_elite_queue_pop_front(pOutStrm->pOutBufQ, (uint64_t*)&(pOutStrm->outDataBufferNode))))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:PBQ:Failure on Out Buf Pop ");
      return nResult;
   }
   pOutBuf = (elite_msg_data_buffer_t*) (pOutStrm->outDataBufferNode.pBuffer);
   pOutBuf->nActualSize = 0;

   return nResult;
}

static inline void AudioDecSvc_ReturnOutBuf(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   (void) elite_msg_push_payload_to_returnq(pOutStrm->pOutBufQ, (elite_msg_any_payload_t*)(pOutStrm->outDataBufferNode.pBuffer));
   pOutStrm->outDataBufferNode.pBuffer = NULL;
}

static inline ADSPResult AudioDecSvc_PushOutBuf(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm, uint64_t *pPayload)
{
   ADSPResult nResult;

   nResult= qurt_elite_queue_push_back(pOutStrm->pDownStreamSvc->dataQ, pPayload);

   // send pOutputBuf to downstream service.
   if(ADSP_FAILED(nResult))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to deliver buffer dowstream. Dropping!\n");
      (void) elite_msg_push_payload_to_returnq(pOutStrm->pOutBufQ, (elite_msg_any_payload_t*)(pOutStrm->outDataBufferNode.pBuffer));
   }

#ifdef DBG_AUDIO_DECODE
   MSG_2(MSG_SSID_QDSP6,  DBG_HIGH_PRIO, "Sent(%d) bytes to PP, TS=%llu\n", pOutBuf->nActualSize,pOutBuf->ullTimeStamp);
#endif

   pOutStrm->outDataBufferNode.pBuffer = NULL;

   return nResult;
}

// Low latency decoder sessions will have higher priority than legacy sessions.
static inline int32_t AudioDecSvc_GetThreadPriority(AudioDecSvc_t *pMe, uint32_t perf_mode)
{
   int32_t nPriority = ELITETHREAD_DYNA_DEC_SVC_PRIO;
   if ((ASM_LOW_LATENCY_STREAM_SESSION == perf_mode ) ||
       (ASM_ULTRA_LOW_LATENCY_STREAM_SESSION == perf_mode ))
   {
      nPriority = ELITETHREAD_DYNA_LL_DEC_SVC_PRIO;
   }
   return nPriority;
}

/**
 * outBufSize should be initialized
 */
static inline void AudioDecSvc_GetRequiredOutBufSizeNum(AudioDecSvc_t *pMe, uint32_t *numOutBufs, uint32_t *reqOutBufSize, uint32_t specifiedOutBufSize, uint32_t metadata_size)
{
   *numOutBufs = DEFAULT_NUM_OUTPUT_BUFFERS;
   if (AudioDecSvc_IsPcmFmt(pMe->capiContainer[0]->dec_fmt_id))
       *numOutBufs = DEFAULT_NUM_PCM_DEC_OUTPUT_BUFFERS;

   *reqOutBufSize = specifiedOutBufSize;
   if ((metadata_size > 0))
   {
      *numOutBufs = NUM_OUTPUT_BUFFERS_WITH_METADATA;
      if (*reqOutBufSize < (metadata_size + sizeof(elite_msg_data_set_param_t)))
      {
         *reqOutBufSize = (metadata_size + sizeof(elite_msg_data_set_param_t));
      }
   }
}

ADSPResult AudioDecSvc_ProcessKppsBw(AudioDecSvc_t *pMe, bool_t is_release, bool_t force_vote);
ADSPResult AudioDecSvc_GetRequiredThreadStackSize(AudioDecSvc_t *pMe, uint32_t *threadStackSize);
ADSPResult AudioDecSvc_AggregateKcpsRequired(AudioDecSvc_t *pMe, uint32_t *decoder_kcps);

bool_t AudioDecSvc_IsLastCapi(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container);
bool_t AudioDecSvc_IsLastCapiIndex(AudioDecSvc_t *pMe, uint32_t ind);
dec_CAPI_container_t *AudioDecSvc_GetFirstCapi(AudioDecSvc_t *pMe);
dec_CAPI_container_t *AudioDecSvc_GetLastCapi(AudioDecSvc_t *pMe);

void AudioDecSvc_DecErrorEvent_CheckRaise(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, dec_err_event_t *dec_err_event);
void AudioDecSvc_DecErrorEvent_RecordError(dec_err_event_t *dec_err_event);
void AudioDecSvc_DecErrorEvent_Reset(dec_err_event_t *dec_err_event);
void AudioDecSvc_DecErrorEvent_Clear(dec_err_event_t *dec_err_event);
void AudioDecSvc_DecErrEvent_SetParam(dec_err_event_t *dec_err_event, uint8_t *pSetParamPayload);

ADSPResult AudioDecSvc_RegisterWithAdsppm(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_DeregisterWithAdsppm(AudioDecSvc_t* pMe);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef AUDCMNUTIL_H
