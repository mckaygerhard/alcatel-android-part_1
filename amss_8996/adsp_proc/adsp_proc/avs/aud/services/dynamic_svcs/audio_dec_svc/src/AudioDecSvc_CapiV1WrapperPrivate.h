/*
 * CAPIV1DECWRAPPER.h
 *
 *  Created on: Jan 20, 2014
 *      Author: rbhatnk
 */
/** @file AudioDecSvc_CapiV1Wrapper.h
This file is header file to be used between capi v1 util and capi v1 wrapper.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/**
========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_CapiV1WrapperPrivate.h#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/04/2014   rbhatnk     Created file.

==========================================================================
*/
#ifndef CAPIV1DECWRAPPERPRIVATE_H_
#define CAPIV1DECWRAPPERPRIVATE_H_

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

#include "Elite_CAPI_V2.h"
#include "adsp_error_codes.h"
#include "mmdefs.h"
#include "Elite_CAPI.h"
#include "adsp_amdb_static.h"

#define AUD_DEC_SVC_SIZE_OF_STD_MEDIA_FMT (sizeof(capi_v2_set_get_media_format_t) + sizeof(capi_v2_standard_data_format_t))
#define AUD_DEC_SVC_MIN_SIZE_OF_RAW_MEDIA_FMT (sizeof(capi_v2_set_get_media_format_t) + sizeof(capi_v2_raw_compressed_data_format_t))

static inline uint32_t aud_dec_svc_media_fmt_size (data_format_t fmt_header)
{
   switch(fmt_header)
   {
   case CAPI_V2_FIXED_POINT:
   case CAPI_V2_IEC61937_PACKETIZED:
         return AUD_DEC_SVC_SIZE_OF_STD_MEDIA_FMT;
   case CAPI_V2_RAW_COMPRESSED:
         return AUD_DEC_SVC_MIN_SIZE_OF_RAW_MEDIA_FMT;
   default:
      return 0;
   }
   return 0;
}

typedef struct audio_dec_svc_capi_v2_media_fmt_t
{
   capi_v2_set_get_media_format_t main;
   union
   {
      capi_v2_standard_data_format_t std;
      capi_v2_raw_compressed_data_format_t raw_fmt;
   };
} audio_dec_svc_capi_v2_media_fmt_t;

//CAPI V1 will never support more than 1 in and output ports. If more than 1 in/out port is required use capi v2.
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_IN_PORTS 1
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_OUT_PORTS 1

typedef struct audio_dec_svc_capi_v1_wrapper_t
{
   capi_v2_t vtbl;
   capi_v2_event_callback_info_t cb_info;

   audio_dec_svc_capi_v2_media_fmt_t out_media_fmt[AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_OUT_PORTS];

   ICAPI                            *capi_v1_ptr;
   adsp_amdb_capi_t                 *amdb_capi_ptr;
   uint32_t                         media_fmt;

   uint32_t                kpps;
   uint32_t                code_bw;
   uint32_t                data_bw;

   //bool_t         wait_for_media_fmt_blk;
   bool_t         capi_init_done;  //wait_for_media_fmt_blk enforced waiting for media fmt (if no media fmt, then data was dropped),
   //but with init_done, if a CAPI needs to wait for media fmt, then init is postponed till media fmt or the first process call.
   bool_t         prev_eos_flag;

   uint32_t       bytes_logged;

   bool_t         prev_eof_flag;

   //in general, CAPI doesn't have to know its in & out fmt. But in case of wrapper, since it has to work with several CAPIs of diff types, this is needed.
   data_format_t  in_data_fmt;
   data_format_t  out_data_fmt;

   bool_t       wait_for_media_fmt; //some capis rely heavily on the logic of wait_for_media_fmt. if they are waiting for media fmt,
      //then they cannot be inited until media fmt comes. capi v2 doesn't support wait_for_media_fmt. when implementing CAPI V2 for such libs,
      //proper code has to be written.

} audio_dec_svc_capi_v1_wrapper_t;


#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPIV1DECWRAPPERPRIVATE_H_ */
