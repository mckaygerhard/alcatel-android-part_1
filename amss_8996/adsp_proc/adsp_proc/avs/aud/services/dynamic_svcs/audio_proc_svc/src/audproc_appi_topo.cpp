/*==========================================================================
ELite Source File

This file implements a generic topology which uses APPI compliant modules.

Copyright (c) 2009-2015 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/*===========================================================================
Edit History

when       who     what, where, why
--------   ---     ---------------------------------------------------------
03/28/11   ss      Introducing PROCESS_CHECK for Resampler and Downmix (APPI Rev D)
01/12/11   ss      Delay accumulation mechanism for EOS, so that all the
                   intermediate buffers are flushed out before processing EOS
10/19/10   DG      Created file.
=========================================================================== */

/*---------------------------------------------------------------------------
* Include Files
* -------------------------------------------------------------------------*/
#include "audproc_appi_topo.h"
#include "Elite_CAPI_V2.h"
#include <stddef.h>
#ifndef TST_TOPO_LAYER
#include "audpp_util.h"
#include "adsp_mtmx_strtr_api.h"
#include "capi_v2_appi_wrapper.h"
#include "adsp_ds1ap_api.h"
#include "adsp_dts_hpx_api.h"
#include "adsp_adm_api.h"
#include "adsp_asm_api.h"
#include "audio_basic_op_ext.h"
#include "capi_v2_virtualizer.h"
#include "appi_softvolumecontrols.h"
#include "capi_v2_library_factory.h"
#else
#include "Elite_APPI.h"
#endif
#include "capi_v2_adsp_error_code_converter.h"
/*---------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Definitions and Constants
 * -------------------------------------------------------------------------*/
#define SIZE_OF_ARRAY(a) (sizeof(a)/sizeof((a)[0]))

#define IS_ALIGN_4_BYTE(a) (!(uint32_t(a) & 0x3))

static const uint32_t NUM_CHANNELS_STEREO = 2;
static const uint32_t NUM_CHANNELS_7_1 = 8;
static const uint32_t MAX_INPUT_CHANNELS = NUM_CHANNELS_7_1;

// KCPS requirements
static const uint32_t KCPS_AUDPROC_SVC __attribute__ ((unused)) = 1000;
static const uint32_t KCPS_DS2AP_2CH __attribute__ ((unused))  = 60000;  // TODO : actual numbers will be profiled and updated
static const uint32_t KCPS_DS2AP_M_CH __attribute__ ((unused)) = 120000; // TODO : actual numbers will be profiled and updated
static const uint32_t KCPS_VOL_CTRL    = 1200;
static const uint32_t KCPS_COMBOPP     = 16200;
static const uint32_t KCPS_IIR         = 7200;
static const uint32_t KCPS_CHANNEL_IIR = KCPS_IIR / NUM_CHANNELS_STEREO;
static const uint32_t KCPS_QENSEMBLE __attribute__ ((unused))  = 1400;
static const uint32_t KCPS_SPA         = 2000; // Not profiled yet, rough value
static const uint32_t KCPS_CODEC_GAIN  = 200;
static const uint32_t KCPS_DOWNMIX     = 20000;
static const uint32_t KCPS_RESAMPLER   = 5000; // Not profiled yet, rough value
static const uint32_t KCPS_RESAMPLER_MULTICHANNEL __attribute__ ((unused))  = KCPS_RESAMPLER * MAX_INPUT_CHANNELS / NUM_CHANNELS_STEREO;
static const uint32_t KCPS_PEAKMETER   = 600;  // Not profiled yet, rough value
static const uint32_t KCPS_SRS_TRUMEDIA =  120000; //max 75 MPPS
static const uint32_t KCPS_SRS_SS3D __attribute__ ((unused)) = 166000; //Not profiled yet, request max clock
static const uint32_t KCPS_DTS_DRC  =5000 ;  //DTS DRC rough value
static const uint32_t KCPS_DTS_DIALNORM  =5000 ;  //DTS DIALNORM rough value
static const uint32_t KCPS_COMPRESSED_LATENCY __attribute__ ((unused)) =5000 ;  //Compressed Latency Module rough value
static const uint32_t KCPS_COMPRESSED_MUTE __attribute__ ((unused)) =5000 ;  //Compressed Mute Module rough value
static const uint32_t KCPS_DELAY  = 1000 ;  // Value for 32 bit stereo data
static const uint32_t KCPS_DTS_HPX_PREMIX  = 50000;
static const uint32_t KCPS_DTS_HPX_POSTMIX = 130000; // based on 8974 hardware
static const uint32_t KCPS_REVERB = 46000; // based on APPI Unit Test profile, 192KHz, stereo, 32-bit
static const uint32_t KCPS_VIRTUALIZER = 18000; // based on APPI Unit Test profile, desktop speaker, 48K/stereo/16-bit
static const uint32_t KCPS_BASS_BOOST = 70000;  // based on APPI Unit Test profile, 192KHz, stereo, 32-bit
static const uint32_t KCPS_POPLESS_EQUALIZER = 21000;  // based on APPI Unit Test profile, 192KHz, stereo, 32-bit
static const uint32_t KCPS_VISUALIZER __attribute__ ((unused)) = 119000;  // based on APPI Unit Test profile, 4096-FFT, stereo, 32-bit
static const uint32_t KCPS_DYNAMIC_RESAMPLER __attribute__ ((unused)) = 10000;
static const uint32_t KCPS_MONO2STEREO_UPMIX = 200;


struct topo_media_fmt_t;
struct topo_buf_t;
struct topo_graph_t;
struct event_handle_t;
struct topo_media_event_t;
struct topo_port_index_t;
struct module_input_port_t;
struct module_output_port_t;
struct _module_info;
struct topo_buf_manager;
struct topo_struct;

// Define the only media format type supported.
struct topo_media_fmt_t
{
	capi_v2_data_format_header_t h;
	capi_v2_standard_data_format_t f;
};

static const topo_media_fmt_t invalid_media_format = {
		//.h
		{
				CAPI_V2_FIXED_POINT
		},
		//.f
		{
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_DATA_FORMAT_INVALID_VAL,
				CAPI_V2_INVALID_INTERLEAVING,
				{ 0 }
		}
};

static const AdspAudioMediaFormatInfo_t invalid_svc_media_format = {
      0,
      0,
      0,
      0,
      0,
      { 0 },
      0
};

static const capi_v2_buf_t empty_buf = {
      NULL, 0, 0
};

static const capi_v2_stream_data_t init_stream_data = {
      { 0, 0, 0, 0, 0, 0, 0},
      0,
      NULL,
      0
};
// Buffer structure and its methods
struct topo_buf_t
{

	capi_v2_stream_flags_t flags;
	int64_t timestamp;
	int8_t *buf_ptrs[CAPI_V2_MAX_CHANNELS];
	uint32_t num_bufs;
	uint32_t size_per_buffer;
	uint32_t valid_data_start; // Index of the first valid byte
	uint32_t valid_data_end;   // Index just beyond the last valid byte
	uint32_t bytes_to_samples_factor; // Num_samples = num_bytes / bytes_to_samples_factor
};
static void topo_buffer_initialize(topo_buf_t *buf_ptr);
static bool_t topo_buffer_is_buf_empty(const topo_buf_t *buf_ptr);
static bool_t topo_buffer_is_buf_full(const topo_buf_t *buf_ptr);
static void topo_buffer_convert_from_ext_input_buffer(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr);
static void topo_buffer_convert_from_ext_output_buffer(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr);
static void topo_buffer_update_external_input_buf_sizes(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr);
static void topo_buffer_update_external_output_buf_sizes(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr);
static uint32_t topo_buffer_bytes_to_samples(const topo_buf_t *buf_ptr, uint32_t bytes);
static uint32_t topo_buffer_samples_to_bytes(const topo_buf_t *buf_ptr, uint32_t samples);
static ADSPResult topo_init_single_module(topo_struct *topo_ptr, const uint32_t index, const audproc_module_info_t *module_info_ptr, const adsp_amdb_module_handle_info_t *handle_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, bool_t initialize_with_compressed_format);
static ADSPResult common_topo_init_single_module(topo_struct *topo_ptr, const uint32_t index, avcs_module_info_t *module_info_ptr, const adsp_amdb_module_handle_info_t *handle_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, bool_t initialize_with_compressed_format);
static ADSPResult topo_create_capi_v2_module(topo_struct *topo_ptr, adsp_amdb_capi_v2_t *handle, const uint32_t index, const audproc_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr);
static ADSPResult topo_create_appi_module(topo_struct *topo_ptr, adsp_amdb_appi_t *handle, const uint32_t index, const audproc_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr, bool_t initialize_with_compressed_format);
static ADSPResult common_topo_create_capi_v2_module(topo_struct *topo_ptr, adsp_amdb_capi_v2_t *handle, const uint32_t index, avcs_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr);
static ADSPResult common_topo_create_appi_module(topo_struct *topo_ptr, adsp_amdb_appi_t *handle, const uint32_t index, avcs_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr, bool_t initialize_with_compressed_format);
static const uint32_t TOPO_INVALID_MODULE_INDEX = UINT32_MAX;

// Topology graph structure and its methods
static const uint8_t GRAPH_MAIN_INPUT_PORT_INDEX = 0xC0;
static const uint8_t GRAPH_MAIN_OUTPUT_PORT_INDEX = 0xE0;

struct topo_def_t
{
   uint32_t topology_id;
   uint32_t num_modules;
   uint32_t *module_id_list;
};

struct topo_graph_t
{
   uint32_t num_modules;

   struct topo_graph_port_t
   {
      uint8_t module_index;
      uint8_t port_index;
   };

   struct topo_graph_node_t
   {
      uint32_t module_id;
      uint8_t num_inputs;
      topo_graph_port_t *inputs_array;
      uint8_t num_outputs;
      topo_graph_port_t *outputs_array;
   } *nodes;
   topo_graph_port_t *connections_array;
};
static void topo_graph_init(topo_graph_t *topo_graph_ptr);
static void topo_graph_deinit(topo_graph_t *topo_graph_ptr);
static ADSPResult topo_graph_set_graph(topo_graph_t *topo_graph_ptr, const topo_def_t *topo_def_ptr);
static ADSPResult common_topo_graph_set_graph(topo_graph_t *topo_graph_ptr, topo_def_t *topo_def_ptr);
static void topo_graph_free_graph(topo_graph_t *topo_graph_ptr);
static topo_port_index_t topo_graph_get_prev_module(topo_graph_t *topo_graph_ptr, topo_port_index_t current_index);
static topo_port_index_t topo_graph_get_next_module(topo_graph_t *topo_graph_ptr, topo_port_index_t current_index);
static topo_port_index_t topo_graph_get_main_input(topo_graph_t *topo_graph_ptr);
static topo_port_index_t topo_graph_get_main_output(topo_graph_t *topo_graph_ptr);
static uint32_t topo_graph_num_modules(const topo_graph_t *topo_graph_ptr);
#ifdef TST_TOPO_LAYER
static void topo_graph_print_graph(const topo_graph_t *topo_graph_ptr, uint16_t instance_id);
#endif
static void topo_graph_print_linear_topo(const topo_graph_t *topo_graph_ptr, uint16_t instance_id);
static capi_v2_port_num_info_t topo_graph_get_port_num_info(const topo_graph_t *topo_graph_ptr, uint32_t module_index);

// Event handle structure and its methods
struct event_handle_t
{
   topo_struct *topo_struct_ptr;
   uint32_t module_index;
};
static void topo_event_initialize_handle(topo_struct *topo_ptr, uint32_t module_index);
static uint32_t topo_event_get_module_index(void *handle);
static topo_struct *topo_event_get_struct_ptr(void *handle);


struct topo_media_event_t
{
   bool_t event_is_pending;
   topo_media_fmt_t event;
};
static void topo_set_media_event(topo_media_event_t *downstream_event_ptr, const topo_media_fmt_t *format_to_be_set);
// Module port structure and its methods
// Module port structure and its methods
struct topo_port_index_t
{
   uint32_t module_index;
   uint32_t port_index;
   enum port_type
{
      INPUT_PORT,
      OUTPUT_PORT
   } type;
};
static module_input_port_t *topo_in_port_from_index(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static module_output_port_t *topo_out_port_from_index(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
#define ASSERT_PORTS_EQUAL(a, b) QURT_ELITE_ASSERT(((a)->module_index == (b)->module_index) && ((a)->port_index == (b)->port_index) &&((a)->type == (b)->type))


// Module input port structure and its methods
struct module_input_port_t
{
   topo_port_index_t connected_port;
   topo_media_event_t media_event;
};
static void topo_module_input_port_initialize(module_input_port_t *port_ptr);

// Module output port structure and its methods
struct module_output_port_t
{
   topo_port_index_t connected_port;
   topo_media_event_t media_event;
   topo_buf_t topo_buf;
};
static void topo_module_output_port_initialize(module_output_port_t *port_ptr);

// The module structure and its methods
typedef struct _module_info
{
   uint32_t        module_id;
   bool_t          is_in_place;
   bool_t          requires_data_buffering;
   bool_t          is_enabled;
   capi_v2_t*      module_ptr;
   uint32_t        KPPS;
   uint32_t        bandwidth;
   uint32_t        delay_in_us;
   uint32_t        headroom;
#ifdef AUDPPSVC_TEST
   uint64_t        count;  // The very first iteration of processing is ignored
   uint64_t        prof_total_cycles;
   uint64_t        prof_num_samples;
   float           prof_peak_mips;
#endif // AUDPPSVC_TEST

   uint32_t num_input_ports;
   module_input_port_t *input_ports_ptr;
   uint32_t num_output_ports;
   module_output_port_t *output_ports_ptr;
   void *port_mem;

   bool_t is_in_chain;
   bool_t disabled_till_next_media_event;

   event_handle_t event_handle;
} module_info;
static topo_buf_t *topo_module_get_input_buffer(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static topo_buf_t *topo_module_get_output_buffer(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static topo_port_index_t topo_module_next_port(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static topo_port_index_t topo_module_prev_port(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static ADSPResult topo_module_allocate_ports(module_info *module_ptr);
struct topo_buf_manager;
static void topo_module_free_ports(module_info *module_ptr, topo_buf_manager *buf_mgr_ptr);
static bool_t topo_module_is_propagate_for_disabled_module(const module_info *module_ptr);
static bool_t topo_module_is_disablement_possible(const module_info *module_ptr);
static void topo_module_set_pending_input_media(module_info *module_ptr, uint16_t instance_id);
static void topo_module_destroy(module_info *module_struct_ptr, topo_buf_manager *buf_mgr_ptr);

// Buffer manager structure and methods
static const uint32_t TOPO_BUF_MANAGER_MAX_ELEMENTS = 32;
struct topo_buf_manager
{
	uint32_t max_memory_allocated;
	uint32_t current_memory_allocated;

	struct topo_buf_manager_element
	{
		bool_t present;
		uint32_t size;
		int8_t *ptr;
	} bufs[TOPO_BUF_MANAGER_MAX_ELEMENTS];
};
static void topo_buf_manager_init(topo_buf_manager *buf_mgr_ptr);
static void topo_buf_manager_deinit(topo_buf_manager *buf_mgr_ptr);
static ADSPResult topo_buf_manager_get_bufs(topo_buf_manager *buf_mgr_ptr, topo_buf_t *buf_ptr);
static void topo_buf_manager_return_bufs(topo_buf_manager *buf_mgr_ptr, topo_buf_t *buf_ptr);
static void topo_buf_manager_clear_all_bufs(topo_buf_manager *buf_mgr_ptr);
static void topo_set_headroom(topo_struct *topo_ptr);
static void topo_set_ramp_period_to_virtualizer(topo_struct *topo_ptr, const uint32_t index);
static void topo_set_delay_ptr_virtualizer(topo_struct *topo_ptr);
static void topo_check_empty_last_buffer(topo_struct *topo_ptr);

static uint32_t INVALID_KPPS_VALUE = UINT32_MAX;
static uint32_t INVALID_BANDWIDTH_VALUE = UINT32_MAX;

struct topo_struct
{
   uint16_t        instance_id;
   uint32_t        ouput_buffer_num_unit_frames;  // size of one unit is returned by elite_svc_get_frame_size function
   uint32_t        compress_frame_size;

   module_info     *module;  // Array of modules (libraries)
   topo_buf_t    main_input_buffer;
   topo_buf_t    main_output_buffer;
   topo_port_index_t first_port;
   topo_port_index_t last_port;

   bool_t          module_list_locked;
   bool_t          currently_handling_events;

   // Data format related variables
   void *stream_struct_mem;
   uint32_t max_in_streams;
   capi_v2_stream_data_t *in_streams;
   capi_v2_stream_data_t **in_stream_ptrs;
   uint32_t max_out_streams;
   capi_v2_stream_data_t *out_streams;
   capi_v2_stream_data_t **out_stream_ptrs;

   // Data format related variables
   bool_t data_format_received;
   AdspAudioMediaFormatInfo_t  in_format;
   AdspAudioMediaFormatInfo_t  out_format;

   // EOS variables
   AudPP_BufInfo_t eos_buf;

   // Status variables shared externally
   uint32_t KPPS;
   uint32_t bandwidth;
   uint32_t delay;

   uint32_t        topo_headroom;                   // Specifies the current headroom requirement of the topology in milli-bels.

   // Buffer manager
   topo_buf_manager buf_mgr;

   // Client for notification of events
   topo_event_client_t *event_client;

   // The topology graph
   topo_graph_t graph;
};

static const uint32_t NUM_US_IN_SEC = 1000000;
static const uint32_t TOPO_KPPS = 1000;
static const uint32_t TOPO_BANDWIDTH = 0; // The upper layer calculates bandwidth for the input and output
static const uint32_t TOPO_BW_FUDGE_FACTOR_NUM = 4; //use fudge factor of 2.0
static const uint32_t TOPO_BW_FUDGE_FACTOR_DEN = 2;
static const uint32_t TOPO_BW_FUDGE_FACTOR_NUM_90_10 = 3; //use fudge factor of 1.5 when any clients vote is 90% of sum.
static const uint32_t TOPO_BW_FUDGE_FACTOR_DEN_90_10 = 2;
static const uint32_t TOPO_BW_FUDGE_FACTOR_THRESHOLD = 90;

static ADSPResult topo_find_module(const topo_struct *topo_ptr,
      const uint32_t module_id,
      uint32_t *index_ptr);
static ADSPResult create_delay_buffer(topo_struct* topo_ptr);
static bool_t topo_is_ds1ap_module(const uint32_t module_id);
static uint32_t topo_convert_us_to_samples(const uint32_t val_in_us, const uint32_t sample_rate);
static uint64_t div_round(const uint64_t x, const uint32_t y);
static bool_t topo_is_compressed(uint32_t bitstream_format);
static uint32_t topo_calculate_q_factor(const AdspAudioMediaFormatInfo_t *media_fmt_ptr);
static void topo_adsp_media_fmt_to_topo(const AdspAudioMediaFormatInfo_t *adsp_media_fmt_ptr, topo_media_fmt_t *topo_media_fmt_ptr);
static void topo_topo_media_fmt_to_adsp(const topo_media_fmt_t *topo_media_fmt_ptr, AdspAudioMediaFormatInfo_t *adsp_media_fmt_ptr);
static void topo_print_media_format(uint16_t obj_id, const topo_media_fmt_t *media_fmt_ptr);
static void topo_print_module_output_media_format(uint16_t obj_id, uint32_t module_id, const topo_media_fmt_t *media_fmt_ptr);
static void topo_loop_thru_zero_modules(topo_struct *topo_ptr);
static bool_t topo_zero_modules_enabled(const topo_struct *topo_ptr);
static bool_t topo_zero_modules_created(const topo_struct *topo_ptr);
static bool_t topo_module_list_locked(const topo_struct *topo_ptr);
static bool_t topo_module_pending_reconfig(const topo_struct *topo_ptr, uint32_t start_index);
static void topo_lock_module_list(topo_struct *topo_ptr);
static void topo_unlock_module_list(topo_struct *topo_ptr);
static bool_t topo_handle_pending_events(topo_struct *topo_ptr, uint32_t start_module_index);
static void topo_handle_module_enable_events(topo_struct *topo_ptr, uint32_t module_index);
static void topo_add_to_module_list(topo_struct *topo_ptr, uint32_t module_index, const topo_port_index_t *prev_port_ptr);
static void topo_remove_from_module_list(topo_struct *topo_ptr, uint32_t module_index);
static void topo_module_setup_output_buffers(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr);
static ADSPResult topo_process_module(topo_struct *topo_ptr, uint32_t module_index);
static ADSPResult topo_process_module_inplace(topo_struct *topo_ptr, uint32_t module_index);
static bool_t topo_inplace_processing_possible(topo_struct *topo_ptr, uint32_t module_index);
static void topo_update_chain_KPPS(topo_struct *topo_ptr);
static void topo_update_chain_bandwidth(topo_struct *topo_ptr);
static void topo_update_chain_delay(topo_struct *topo_ptr);
static void topo_update_chain_headroom(topo_struct *topo_ptr);
static void topo_raise_output_media_type_event(topo_struct *topo_ptr);
static bool_t topo_is_equal_media_format(const topo_media_fmt_t *f1, const topo_media_fmt_t *f2);
static capi_v2_err_t topo_event_callback(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event);
static event_handle_t *topo_event_get_handle(topo_struct *topo_ptr, uint32_t module_index);
static bool_t topo_module_can_process(topo_struct *topo_ptr, uint32_t module_index);
static bool_t topo_check_update_module_enablement(topo_struct *topo_ptr, uint32_t module_index);
static bool_t topo_check_update_module_process_state(topo_struct *topo_ptr, uint32_t module_index);
static void topo_destroy_modules(uint32_t num_modules, module_info modules[], topo_buf_manager *buf_mgr_ptr);
static void topo_setup_connections(topo_struct *topo_ptr);
static ADSPResult topo_allocate_stream_structs(topo_struct *topo_ptr);
static void topo_deallocate_stream_structs(topo_struct *topo_ptr);

#ifdef AUDPPSVC_TEST
static float convert_cycles_to_mips(const uint32_t sample_rate, const uint32_t num_samples, const uint64_t cycles);
#endif

/*---------------------------------------------------------------------------
 * Globals
 * -------------------------------------------------------------------------*/
static const uint32_t AUDPROC_TOPO_DEFAULT_STACK_SIZE = 7*1024;

/*---------------------------------------------------------------------------
 * Function Definitions
 * -------------------------------------------------------------------------*/

static ADSPResult topo_create_obj(const uint16_t instance_id, topo_struct **topo_obj_ptr, const uint32_t ouput_buffer_num_unit_frames, topo_event_client_t *event_client)
{
   *topo_obj_ptr = NULL;

   // Creating the topology object
   topo_struct *topo_ptr = (topo_struct*)(qurt_elite_memory_malloc(sizeof (topo_struct), QURT_ELITE_HEAP_DEFAULT));
   if(NULL == topo_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to allocate memory for topology structure", instance_id);
      return ADSP_ENOMEMORY;
   }
   // Initialize the topology object variables
   topo_ptr->instance_id = instance_id;
   topo_ptr->ouput_buffer_num_unit_frames = ouput_buffer_num_unit_frames;
   topo_ptr->compress_frame_size = 0;
   topo_ptr->module = NULL;
   topo_buffer_initialize(&topo_ptr->main_input_buffer);
   topo_buffer_initialize(&topo_ptr->main_output_buffer);
   topo_ptr->first_port.module_index = TOPO_INVALID_MODULE_INDEX;
   topo_ptr->first_port.port_index = 0;
   topo_ptr->first_port.type = topo_port_index_t::INPUT_PORT;
   topo_ptr->last_port.module_index = TOPO_INVALID_MODULE_INDEX;
   topo_ptr->last_port.port_index = 0;
   topo_ptr->last_port.type = topo_port_index_t::OUTPUT_PORT;
   topo_ptr->module_list_locked = FALSE;
   topo_ptr->currently_handling_events = FALSE;
   topo_ptr->stream_struct_mem = NULL;
   topo_ptr->max_in_streams = 0;
   topo_ptr->in_streams = NULL;
   topo_ptr->in_stream_ptrs = NULL;
   topo_ptr->max_out_streams = 0;
   topo_ptr->out_streams = NULL;
   topo_ptr->out_stream_ptrs = NULL;
   topo_ptr->data_format_received = FALSE;
   topo_ptr->in_format = invalid_svc_media_format;
   topo_ptr->out_format = invalid_svc_media_format;
   topo_ptr->eos_buf.pBuf = NULL;
   topo_ptr->eos_buf.sizeInSamples = 0;
   topo_ptr->eos_buf.offsetInSamples = 0;
   topo_ptr->KPPS = INVALID_KPPS_VALUE;
   topo_ptr->bandwidth = INVALID_BANDWIDTH_VALUE;
   topo_ptr->delay = 0;
   topo_ptr->topo_headroom = 0;
   topo_buf_manager_init(&topo_ptr->buf_mgr);
   topo_ptr->event_client = event_client;
   topo_graph_init(&topo_ptr->graph);

   *topo_obj_ptr = topo_ptr;

   return ADSP_EOK;
}

static QURT_ELITE_HEAP_ID topo_get_heap_id(bool_t use_lpa, AudPP_AudProcType pp_type)
{
   QURT_ELITE_HEAP_ID heap_id = QURT_ELITE_HEAP_DEFAULT;
   if (use_lpa)
   {
      switch (pp_type)
      {
      case DYNA_SVC_PP_TYPE_POPP:
         heap_id = PP_SVC_POPP_ALWAYS_ENABLED_ALGS;
         break;
      case DYNA_SVC_PP_TYPE_COPP:
         heap_id = PP_SVC_COPP_ALWAYS_ENABLED_ALGS;
         break;
      default:
         break;
      }
   }
   return heap_id;
}

static void *topo_allocate_module_memory(topo_struct *topo_ptr, const uint32_t malloc_size,
                                             bool_t use_lpa, AudPP_AudProcType pp_type)
{
   QURT_ELITE_HEAP_ID heap_id = topo_get_heap_id(use_lpa, pp_type);
	return qurt_elite_memory_malloc(malloc_size, heap_id);
}

/** Generally kcps numbers are obtained by assuming cpp of 1.6 = 8/5*/
#define CONVERT_KCPS_TO_KPPS(kcps) ((kcps)*5/8)

// Since we don't support KPPS query yet, this is a workaround. Ideally we should
// be able to query the module for its KPPS.
static uint32_t topo_init_get_KPPS(const uint32_t module_id)
{
   static const uint32_t default_KPPS = 166000; // MAX clock
   struct KCPS_LUT_t
   {
      uint32_t module_id;
      uint32_t KCPS;
   };
   static const KCPS_LUT_t KCPS_info[] =
   {
         { AUDPROC_MODULE_ID_VOL_CTRL,                       KCPS_VOL_CTRL           },
         { AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2,        KCPS_VOL_CTRL           },
         { AUDPROC_MODULE_ID_RESAMPLER,                  KCPS_RESAMPLER          },
         { AUDPROC_MODULE_ID_EQUALIZER,                    KCPS_COMBOPP            },
         { AUDPROC_MODULE_ID_IIR_TUNING_FILTER,              KCPS_IIR                },
         { AUDPROC_MODULE_ID_SPA,                            KCPS_SPA                },
         { AUDPROC_MODULE_ID_RX_CODEC_GAIN_CTRL,             KCPS_CODEC_GAIN         },
         { AUDPROC_MODULE_ID_PEAK_METER,                 KCPS_PEAKMETER          },
         { AUDPROC_MODULE_ID_LEFT_IIR_TUNING_FILTER,         KCPS_CHANNEL_IIR        },
         { AUDPROC_MODULE_ID_RIGHT_IIR_TUNING_FILTER,        KCPS_CHANNEL_IIR        },
         { APPI_SRS_TRUMEDIA_MODULE_ID,                  KCPS_SRS_TRUMEDIA       },
         { AUDPROC_MODULE_ID_TX_MIC_GAIN_CTRL,               KCPS_CODEC_GAIN         },
         { AUDPROC_MODULE_ID_TX_IIR_FILTER,                  KCPS_IIR                },
         { AUDPROC_MODULE_ID_DOWNMIX,                    KCPS_DOWNMIX            },
         { AUDPROC_MODULE_ID_DTS_DIALNORM,               KCPS_DTS_DIALNORM       },
         { AUDPROC_MODULE_ID_DTS_DRC,                    KCPS_DTS_DRC            },
         { AUDPROC_MODULE_ID_RESAMPLER_DS1AP,            KCPS_RESAMPLER          },
         { AUDPROC_MODULE_ID_DELAY,                      KCPS_DELAY              },
         { AUDPROC_MODULE_ID_BASS_BOOST,                 KCPS_BASS_BOOST         },
         { AUDPROC_MODULE_ID_POPLESS_EQUALIZER,          KCPS_POPLESS_EQUALIZER  },
         { AUDPROC_MODULE_ID_VIRTUALIZER,                KCPS_VIRTUALIZER        },
         { AUDPROC_MODULE_ID_REVERB,                     KCPS_REVERB             },
         { AUDPROC_MODULE_ID_DTS_HPX_PREMIX,             KCPS_DTS_HPX_PREMIX     },
         { AUDPROC_MODULE_ID_DTS_HPX_POSTMIX,            KCPS_DTS_HPX_POSTMIX    },
         { AUDPROC_MODULE_ID_MONO2STEREO_UPMIX,          KCPS_MONO2STEREO_UPMIX  }
   };

   for (uint32_t i = 0; i < SIZE_OF_ARRAY(KCPS_info); i++)
   {
      if (module_id == KCPS_info[i].module_id)
      {
         return CONVERT_KCPS_TO_KPPS(KCPS_info[i].KCPS);
      }
   }

   return default_KPPS;
}

static bool_t topo_lookup_appi_buffering(uint32_t module_id)
{
   struct buffering_LUT_t
   {
      uint32_t module_id;
      bool_t requires_buffering;
   };
   static const buffering_LUT_t buffering_info[] =
   {
         { AUDPROC_MODULE_ID_VOL_CTRL,                   FALSE   },
		 { AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2,    FALSE   },
         { AUDPROC_MODULE_ID_RESAMPLER,              TRUE    },
         { AUDPROC_MODULE_ID_EQUALIZER,                FALSE   },
         { AUDPROC_MODULE_ID_IIR_TUNING_FILTER,          FALSE   },
         { AUDPROC_MODULE_ID_SPA,                        FALSE   },
         { AUDPROC_MODULE_ID_RX_CODEC_GAIN_CTRL,         FALSE   },
         { AUDPROC_MODULE_ID_MCHAN_IIR_TUNING_FILTER,FALSE   },
         { AUDPROC_MODULE_ID_PEAK_METER,             FALSE   },
         { AUDPROC_MODULE_ID_LEFT_IIR_TUNING_FILTER,     FALSE   },
         { AUDPROC_MODULE_ID_RIGHT_IIR_TUNING_FILTER,    FALSE   },
         { APPI_SRS_TRUMEDIA_MODULE_ID,              FALSE   },
         { AUDPROC_MODULE_ID_TX_MIC_GAIN_CTRL,           FALSE   },
         { AUDPROC_MODULE_ID_HPF_IIR_TX_FILTER,          FALSE   },
         { AUDPROC_MODULE_ID_TX_IIR_FILTER,              FALSE   },
         { AUDPROC_MODULE_ID_DOWNMIX,                FALSE   },
         { AUDPROC_MODULE_ID_DTS_DIALNORM,           FALSE   },
         { AUDPROC_MODULE_ID_DTS_DRC,                FALSE   },
         { AUDPROC_MODULE_ID_DS1AP,		             TRUE    },
         { AUDPROC_MODULE_ID_RESAMPLER_DS1AP,        TRUE    },
         { AUDPROC_MODULE_ID_DELAY,                  FALSE   },
         { AUDPROC_MODULE_ID_POPLESS_EQUALIZER,      FALSE   },
         { AUDPROC_MODULE_ID_VIRTUALIZER,            FALSE   },
         { AUDPROC_MODULE_ID_BASS_BOOST,             FALSE   },
         { AUDPROC_MODULE_ID_REVERB,                 FALSE   },
         { AUDPROC_MODULE_ID_VISUALIZER,             FALSE   },
         { AUDPROC_MODULE_ID_DTS_HPX_PREMIX,         FALSE   },
         { AUDPROC_MODULE_ID_DTS_HPX_POSTMIX,        FALSE   },
   };

   for (uint32_t i = 0; i < SIZE_OF_ARRAY(buffering_info); i++)
   {
      if (module_id == buffering_info[i].module_id)
      {
         return buffering_info[i].requires_buffering;
      }
   }

   return TRUE; // Assume that a module requires buffering by default
}

static ADSPResult topo_create_capi_v2_module(topo_struct *topo_ptr, adsp_amdb_capi_v2_t *handle, const uint32_t index, const audproc_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr)
{
	ADSPResult result = ADSP_EOK;
	capi_v2_err_t err_code = CAPI_V2_EOK;

   // Query static properties

	// First query for the properties which are not optional.
   capi_v2_init_memory_requirement_t memory_req = { 0 };
   capi_v2_prop_t req_props[] = {
		   { CAPI_V2_INIT_MEMORY_REQUIREMENT, { reinterpret_cast<int8_t*>(&memory_req),   0, sizeof(memory_req)   }, { FALSE, FALSE, 0} },
   };
   capi_v2_proplist_t req_proplist = { SIZE_OF_ARRAY(req_props), req_props };
   err_code = adsp_amdb_capi_v2_get_static_properties_f(handle, init_proplist_ptr, &req_proplist);
   if(CAPI_V2_FAILED(err_code))
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in getting some required static properties with %d.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
	   return capi_v2_err_to_adsp_result(err_code);
   }

   // Query for the optional properties now.
   capi_v2_init_memory_requirement_t dummy_memory_req = { 0 };
   capi_v2_is_inplace_t is_inplace = { FALSE };
   capi_v2_requires_data_buffering_t req_data_buf = { TRUE };
   capi_v2_stack_size_t stack_size = { 0 };
   capi_v2_num_needed_framework_extensions_t num_extensions = { 0 };
   capi_v2_prop_t optional_props[] = {
		   { CAPI_V2_IS_INPLACE,              { reinterpret_cast<int8_t*>(&is_inplace),   0, sizeof(is_inplace)   }, { FALSE, FALSE, 0} },
		   { CAPI_V2_REQUIRES_DATA_BUFFERING, { reinterpret_cast<int8_t*>(&req_data_buf), 0, sizeof(req_data_buf) }, { FALSE, FALSE, 0} },
		   { CAPI_V2_STACK_SIZE,                       { reinterpret_cast<int8_t*>(&stack_size),     0, sizeof(stack_size)       }, { FALSE, FALSE, 0} },
		   { CAPI_V2_NUM_NEEDED_FRAMEWORK_EXTENSIONS,  { reinterpret_cast<int8_t*>(&num_extensions), 0, sizeof(num_extensions)   }, { FALSE, FALSE, 0} },
		   // We still need to query for the memory requirement here since amdb returns an error if it does not find it. This value is ignored.
		   { CAPI_V2_INIT_MEMORY_REQUIREMENT, { reinterpret_cast<int8_t*>(&dummy_memory_req),   0, sizeof(dummy_memory_req)   }, { FALSE, FALSE, 0} },
   };
   capi_v2_proplist_t optional_proplist = { SIZE_OF_ARRAY(optional_props), optional_props };
   err_code = adsp_amdb_capi_v2_get_static_properties_f(handle, init_proplist_ptr, &optional_proplist);
   if(CAPI_V2_FAILED(err_code))
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX Module with ID 0x%lX failed in getting some optional static properties with %d. Using default values.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
   }

   topo_ptr->module[index].requires_data_buffering = req_data_buf.requires_data_buffering;
   if (topo_ptr->module[index].requires_data_buffering)
   {
	   topo_ptr->module[index].is_in_place = FALSE;
   }
   else
   {
	   topo_ptr->module[index].is_in_place = is_inplace.is_inplace;
   }

   *stack_size_ptr = (*stack_size_ptr > stack_size.size_in_bytes) ? *stack_size_ptr : stack_size.size_in_bytes;

   if (num_extensions.num_extensions != 0)
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX requires %lu framework extensions. Currently no extensions are supported.",  topo_ptr->instance_id, module_info_ptr->module_id, num_extensions.num_extensions);
	   return ADSP_EUNSUPPORTED;
   }

   // Allocate memory
   void *module_mem = topo_allocate_module_memory(topo_ptr, memory_req.size_in_bytes, module_info_ptr->use_lpm, pp_type);
   if (NULL == module_mem)
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed to allocate memory. The module requested for %lu bytes.", topo_ptr->instance_id, module_info_ptr->module_id, memory_req.size_in_bytes);
	   return ADSP_ENOMEMORY;
   }
   MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Module with ID 0x%lX allocated for %lu bytes of memory at location 0x%p. Use LPM = %lu", topo_ptr->instance_id, module_info_ptr->module_id, memory_req.size_in_bytes, module_mem, (uint32_t)(module_info_ptr->use_lpm));

   topo_ptr->module[index].module_ptr = reinterpret_cast<capi_v2_t*>(module_mem);

   // Initialize
   err_code = adsp_amdb_capi_v2_init_f(handle, topo_ptr->module[index].module_ptr, init_proplist_ptr);
   if(CAPI_V2_FAILED(err_code))
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in init with %d.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
	   qurt_elite_memory_free(topo_ptr->module[index].module_ptr);
	   topo_ptr->module[index].module_ptr = NULL;
	   return capi_v2_err_to_adsp_result(err_code);
   }

   return result;
}
static ADSPResult topo_create_appi_module(topo_struct *topo_ptr, adsp_amdb_appi_t *handle, const uint32_t index, const audproc_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr, bool_t initialize_with_compressed_format)
{
	ADSPResult result = ADSP_EOK;

   uint32_t curr_module_size = 0;
   appi_buf_t init_params;
   init_params.actual_data_len = sizeof(module_info_ptr->init_params);
   init_params.max_data_len = sizeof(module_info_ptr->init_params);
   init_params.data_ptr = (int8_t*)module_info_ptr->init_params;

   result = adsp_amdb_appi_getsize_f(handle, &init_params, &curr_module_size);
   if(ADSP_FAILED(result))
   {
	   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in getsize with %d.", topo_ptr->instance_id, module_info_ptr->module_id, result);
	   return result;
   }

   uint32_t malloc_size = align_to_8_byte(capi_v2_appi_wrapper_getsize()) + curr_module_size;

   // Allocate memory
   void *malloc_ptr = topo_allocate_module_memory(topo_ptr, malloc_size, module_info_ptr->use_lpm, pp_type);
   if (NULL == malloc_ptr)
   {
	   MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed to allocate memory. The module requested for %lu bytes. Total size required including the wrapper: %lu bytes.", topo_ptr->instance_id, module_info_ptr->module_id, curr_module_size, malloc_size);
	   return ADSP_ENOMEMORY;
   }
   MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Module with ID 0x%lX allocated for %lu bytes of memory at location 0x%p. Use LPM = %lu", topo_ptr->instance_id, module_info_ptr->module_id, malloc_size, malloc_ptr, (uint32_t)(module_info_ptr->use_lpm));

   capi_v2_t *wrapper_mem_ptr = reinterpret_cast<capi_v2_t*>(malloc_ptr);
   appi_t *module_mem_ptr = reinterpret_cast<appi_t*>((uint8_t*)(malloc_ptr) + align_to_8_byte(capi_v2_appi_wrapper_getsize()));

   // Call initialize function
   {
	   // The media format is irrelevant here. It will be overwritten when the main media format comes.
	   const appi_format_t default_in_format =
	   {
			   NUM_CHANNELS_STEREO,                          // num_channels
			   16,                                           // bits_per_sample
			   48000,                                        // sampling_rate
			   1,                                            // data_is_signed
			   0,                                            // data_is_interleaved
			   { PCM_CHANNEL_L, PCM_CHANNEL_R }              // Default channel mapping
	   };

	   const appi_format_t default_compressed_in_format =
	   {
			   NUM_CHANNELS_STEREO,                          // num_channels
			   16,                                           // bits_per_sample
			   48000,                                        // sampling_rate
			   1,                                            // data_is_signed
			   1,                                            // data_is_interleaved
			   { PCM_CHANNEL_L, PCM_CHANNEL_R }              // Default channel mapping
	   };

	   const appi_format_t *in_format_ptr = initialize_with_compressed_format ? &default_compressed_in_format : &default_in_format;
	   appi_format_t out_format;

	   ADSPResult result = adsp_amdb_appi_init_f(handle, module_mem_ptr, &topo_ptr->module[index].is_in_place, in_format_ptr, &out_format, &init_params);
	   if (ADSP_FAILED(result))
	   {
		   MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID 0x%lX failed to init.", topo_ptr->instance_id, topo_ptr->module[index].module_id);
		   qurt_elite_memory_free(malloc_ptr);
		   return result;
	   }

	   // Initialize the wrapper
	   result = capi_v2_appi_wrapper_init(wrapper_mem_ptr, module_mem_ptr, init_proplist_ptr, &out_format,module_info_ptr->module_id);
	   if (ADSP_FAILED(result))
	   {
		   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in initializing the capi v2 appi wrapper with %d.", topo_ptr->instance_id, module_info_ptr->module_id, result);
		   (void) module_mem_ptr->vtbl_ptr->end(module_mem_ptr);
		   qurt_elite_memory_free(malloc_ptr);
		   return result;
	   }

   }

   topo_ptr->module[index].module_ptr = wrapper_mem_ptr;
   topo_ptr->module[index].requires_data_buffering = initialize_with_compressed_format ? FALSE : topo_lookup_appi_buffering(module_info_ptr->module_id); // Compressed APPIs always have the same input and output size.
   if(topo_ptr->module[index].KPPS==0)
   {
   topo_ptr->module[index].KPPS       = topo_init_get_KPPS(module_info_ptr->module_id);
   }

   return result;
}

static ADSPResult common_topo_create_capi_v2_module(topo_struct *topo_ptr, adsp_amdb_capi_v2_t *handle, const uint32_t index, avcs_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr)
{
	ADSPResult result = ADSP_EOK;
	capi_v2_err_t err_code = CAPI_V2_EOK;

	// Query static properties

	// First query for the properties which are not optional.
	capi_v2_init_memory_requirement_t memory_req = { 0 };
	capi_v2_prop_t req_props[] = {
			{ CAPI_V2_INIT_MEMORY_REQUIREMENT, { reinterpret_cast<int8_t*>(&memory_req),   0, sizeof(memory_req)   }, { FALSE, FALSE, 0} },
	};
	capi_v2_proplist_t req_proplist = { SIZE_OF_ARRAY(req_props), req_props };
	err_code = adsp_amdb_capi_v2_get_static_properties_f(handle, init_proplist_ptr, &req_proplist);
	if(CAPI_V2_FAILED(err_code))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in getting some required static properties with %d.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
		return capi_v2_err_to_adsp_result(err_code);
	}

	// Query for the optional properties now.
	capi_v2_init_memory_requirement_t dummy_memory_req = { 0 };
	capi_v2_is_inplace_t is_inplace = { FALSE };
	capi_v2_requires_data_buffering_t req_data_buf = { TRUE };
	capi_v2_stack_size_t stack_size = { 0 };
	capi_v2_num_needed_framework_extensions_t num_extensions = { 0 };
	capi_v2_prop_t optional_props[] = {
			{ CAPI_V2_IS_INPLACE,              { reinterpret_cast<int8_t*>(&is_inplace),   0, sizeof(is_inplace)   }, { FALSE, FALSE, 0} },
			{ CAPI_V2_REQUIRES_DATA_BUFFERING, { reinterpret_cast<int8_t*>(&req_data_buf), 0, sizeof(req_data_buf) }, { FALSE, FALSE, 0} },
			{ CAPI_V2_STACK_SIZE,                       { reinterpret_cast<int8_t*>(&stack_size),     0, sizeof(stack_size)       }, { FALSE, FALSE, 0} },
			{ CAPI_V2_NUM_NEEDED_FRAMEWORK_EXTENSIONS,  { reinterpret_cast<int8_t*>(&num_extensions), 0, sizeof(num_extensions)   }, { FALSE, FALSE, 0} },
			// We still need to query for the memory requirement here since amdb returns an error if it does not find it. This value is ignored.
			{ CAPI_V2_INIT_MEMORY_REQUIREMENT, { reinterpret_cast<int8_t*>(&dummy_memory_req),   0, sizeof(dummy_memory_req)   }, { FALSE, FALSE, 0} },
	};
	capi_v2_proplist_t optional_proplist = { SIZE_OF_ARRAY(optional_props), optional_props };
	err_code = adsp_amdb_capi_v2_get_static_properties_f(handle, init_proplist_ptr, &optional_proplist);
	if(CAPI_V2_FAILED(err_code))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX Module with ID 0x%lX failed in getting some optional static properties with %d. Using default values.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
	}

	topo_ptr->module[index].requires_data_buffering = req_data_buf.requires_data_buffering;
	if (topo_ptr->module[index].requires_data_buffering)
	{
		topo_ptr->module[index].is_in_place = FALSE;
	}
	else
	{
		topo_ptr->module[index].is_in_place = is_inplace.is_inplace;
	}

	*stack_size_ptr = (*stack_size_ptr > stack_size.size_in_bytes) ? *stack_size_ptr : stack_size.size_in_bytes;

	if (num_extensions.num_extensions != 0)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX requires %lu framework extensions. Currently no extensions are supported.",  topo_ptr->instance_id, module_info_ptr->module_id, num_extensions.num_extensions);
		return ADSP_EUNSUPPORTED;
	}

	// Allocate memory
	void *module_mem = topo_allocate_module_memory(topo_ptr, memory_req.size_in_bytes, FALSE, pp_type);
	if (NULL == module_mem)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed to allocate memory. The module requested for %lu bytes.", topo_ptr->instance_id, module_info_ptr->module_id, memory_req.size_in_bytes);
		return ADSP_ENOMEMORY;
	}
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Module with ID 0x%lX allocated for %lu bytes of memory at location 0x%p.", topo_ptr->instance_id, module_info_ptr->module_id, memory_req.size_in_bytes, module_mem);

	topo_ptr->module[index].module_ptr = reinterpret_cast<capi_v2_t*>(module_mem);

	// Initialize
	err_code = adsp_amdb_capi_v2_init_f(handle, topo_ptr->module[index].module_ptr, init_proplist_ptr);
	if(CAPI_V2_FAILED(err_code))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in init with %d.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
		qurt_elite_memory_free(topo_ptr->module[index].module_ptr);
		topo_ptr->module[index].module_ptr = NULL;
		return capi_v2_err_to_adsp_result(err_code);
	}

	return result;
}

static ADSPResult common_topo_create_appi_module(topo_struct *topo_ptr, adsp_amdb_appi_t *handle, const uint32_t index, avcs_module_info_t *module_info_ptr, AudPP_AudProcType pp_type, uint32_t *stack_size_ptr, capi_v2_proplist_t *init_proplist_ptr, bool_t initialize_with_compressed_format)
{
	ADSPResult result = ADSP_EOK;

	uint32_t curr_module_size = 0;
	appi_buf_t init_params;
	init_params.actual_data_len = 0;
	init_params.max_data_len = 0;
	init_params.data_ptr = NULL;

	result = adsp_amdb_appi_getsize_f(handle, &init_params, &curr_module_size);
	if(ADSP_FAILED(result))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in getsize with %d.", topo_ptr->instance_id, module_info_ptr->module_id, result);
		return result;
	}

	uint32_t malloc_size = align_to_8_byte(capi_v2_appi_wrapper_getsize()) + curr_module_size;

	// Allocate memory
	void *malloc_ptr = topo_allocate_module_memory(topo_ptr, malloc_size, FALSE, pp_type);
	if (NULL == malloc_ptr)
	{
		MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed to allocate memory. The module requested for %lu bytes. Total size required including the wrapper: %lu bytes.", topo_ptr->instance_id, module_info_ptr->module_id, curr_module_size, malloc_size);
		return ADSP_ENOMEMORY;
	}
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Module with ID 0x%lX allocated for %lu bytes of memory at location 0x%p.", topo_ptr->instance_id, module_info_ptr->module_id, malloc_size, malloc_ptr);

	capi_v2_t *wrapper_mem_ptr = reinterpret_cast<capi_v2_t*>(malloc_ptr);
	appi_t *module_mem_ptr = reinterpret_cast<appi_t*>((uint8_t*)(malloc_ptr) + align_to_8_byte(capi_v2_appi_wrapper_getsize()));

	// Call initialize function
	{
		// The media format is irrelevant here. It will be overwritten when the main media format comes.
		const appi_format_t default_in_format =
		{
				NUM_CHANNELS_STEREO,                          // num_channels
				16,                                           // bits_per_sample
				48000,                                        // sampling_rate
				1,                                            // data_is_signed
				0,                                            // data_is_interleaved
				{ PCM_CHANNEL_L, PCM_CHANNEL_R }              // Default channel mapping
		};

		const appi_format_t default_compressed_in_format =
		{
				NUM_CHANNELS_STEREO,                          // num_channels
				16,                                           // bits_per_sample
				48000,                                        // sampling_rate
				1,                                            // data_is_signed
				1,                                            // data_is_interleaved
				{ PCM_CHANNEL_L, PCM_CHANNEL_R }              // Default channel mapping
		};

		const appi_format_t *in_format_ptr = initialize_with_compressed_format ? &default_compressed_in_format : &default_in_format;
		appi_format_t out_format;

		ADSPResult result = adsp_amdb_appi_init_f(handle, module_mem_ptr, &topo_ptr->module[index].is_in_place, in_format_ptr, &out_format, &init_params);
		if (ADSP_FAILED(result))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID 0x%lX failed to init.", topo_ptr->instance_id, topo_ptr->module[index].module_id);
			qurt_elite_memory_free(malloc_ptr);
			return result;
		}

		// Initialize the wrapper
		result = capi_v2_appi_wrapper_init(wrapper_mem_ptr, module_mem_ptr, init_proplist_ptr, &out_format,module_info_ptr->module_id);
		if (ADSP_FAILED(result))
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX failed in initializing the capi v2 appi wrapper with %d.", topo_ptr->instance_id, module_info_ptr->module_id, result);
			(void) module_mem_ptr->vtbl_ptr->end(module_mem_ptr);
			qurt_elite_memory_free(malloc_ptr);
			return result;
		}

	}

	topo_ptr->module[index].module_ptr = wrapper_mem_ptr;
	topo_ptr->module[index].requires_data_buffering = initialize_with_compressed_format ? FALSE : topo_lookup_appi_buffering(module_info_ptr->module_id); // Compressed APPIs always have the same input and output size.
	if(topo_ptr->module[index].KPPS==0)
	{
		topo_ptr->module[index].KPPS       = topo_init_get_KPPS(module_info_ptr->module_id);
	}

	return result;
}

static ADSPResult topo_init_single_module(topo_struct *topo_ptr,
      const uint32_t index,
      const audproc_module_info_t *module_info_ptr,
      const adsp_amdb_module_handle_info_t *handle_info_ptr,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      bool_t initialize_with_compressed_format)
{
   capi_v2_port_num_info_t port_info = topo_graph_get_port_num_info(&topo_ptr->graph, index);

   // Set up default values
   topo_ptr->module[index].module_id  = module_info_ptr->module_id;
   topo_ptr->module[index].is_in_place = FALSE;
   topo_ptr->module[index].requires_data_buffering = TRUE;
   topo_ptr->module[index].is_enabled = TRUE;
   topo_ptr->module[index].module_ptr = NULL;
   topo_ptr->module[index].KPPS       = 0;
   topo_ptr->module[index].bandwidth  = 0;
   topo_ptr->module[index].delay_in_us       = 0;
   topo_ptr->module[index].is_in_chain = FALSE;
   topo_ptr->module[index].disabled_till_next_media_event = FALSE;
   topo_ptr->module[index].headroom = 0;
   topo_ptr->module[index].num_input_ports = port_info.num_input_ports;
   topo_ptr->module[index].num_output_ports = port_info.num_output_ports;
   topo_ptr->module[index].input_ports_ptr = NULL;
   topo_ptr->module[index].output_ports_ptr = NULL;
   topo_event_initialize_handle(topo_ptr, index);

   {
      ADSPResult result = topo_module_allocate_ports(&topo_ptr->module[index]);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed to allocate memory for ports for module %lX.", topo_ptr->instance_id, module_info_ptr->module_id);
         return result;
      }
   }

   // Set up init properties
   capi_v2_heap_id_t heap_id = { topo_get_heap_id(module_info_ptr->use_lpm, pp_type) };
   capi_v2_event_callback_info_t cb_info = { topo_event_callback, topo_event_get_handle(topo_ptr, index) };
   capi_v2_prop_t init_props[] = {
		   { CAPI_V2_PORT_NUM_INFO,            { reinterpret_cast<int8_t*>(&port_info),   sizeof(port_info), sizeof(port_info) }, { FALSE, FALSE, 0} },
		   { CAPI_V2_HEAP_ID,             { reinterpret_cast<int8_t*>(&heap_id),    sizeof(heap_id),  sizeof(heap_id)  }, { FALSE, FALSE, 0} },
		   { CAPI_V2_EVENT_CALLBACK_INFO,  { reinterpret_cast<int8_t*>(&cb_info),     sizeof(cb_info),   sizeof(cb_info)   }, { FALSE, FALSE, 0} },
		   { CAPI_V2_CUSTOM_INIT_DATA,  { (int8_t*)(module_info_ptr->init_params), sizeof(module_info_ptr->init_params), sizeof(module_info_ptr->init_params) }, { FALSE, FALSE, 0 } }
   };
   capi_v2_proplist_t init_proplist = { SIZE_OF_ARRAY(init_props), init_props };

   // Create the module
   if(ADSP_SUCCEEDED(handle_info_ptr->result) && (NULL != handle_info_ptr->h.capi_v2_handle))
   {
      ADSPResult result = ADSP_EOK;
      switch (handle_info_ptr->interface_type)
      {
      case CAPI_V2:
         result = topo_create_capi_v2_module(topo_ptr, handle_info_ptr->h.capi_v2_handle, index, module_info_ptr, pp_type, stack_size_ptr, &init_proplist);
         break;
      case APPI:
         result = topo_create_appi_module(topo_ptr, handle_info_ptr->h.appi_handle, index, module_info_ptr, pp_type, stack_size_ptr, &init_proplist, initialize_with_compressed_format);
         break;
      default:
         MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module ID 0x%lX type is neither APPI nor CAPIv2. Type is %d.", topo_ptr->instance_id, module_info_ptr->module_id, handle_info_ptr->interface_type);
         result = ADSP_EFAILED;
         break;
      }

      if (ADSP_FAILED(result))
      {
         topo_module_free_ports(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return result;
      }
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module ID 0x%lX is not supported.", topo_ptr->instance_id, module_info_ptr->module_id);
      topo_module_free_ports(&topo_ptr->module[index], &topo_ptr->buf_mgr);
      return ADSP_EUNSUPPORTED;
   }


   if (topo_ptr->module[index].requires_data_buffering)
   {
	   capi_v2_port_data_threshold_t input, output;

	   // Set up the thresholds
	   capi_v2_prop_t threshold_props[] = {
		   { CAPI_V2_PORT_DATA_THRESHOLD,  { reinterpret_cast<int8_t*>(&input),   0, sizeof(input)   }, { TRUE, TRUE, 0} },
		   { CAPI_V2_PORT_DATA_THRESHOLD,  { reinterpret_cast<int8_t*>(&output),   0, sizeof(output)   }, { TRUE, FALSE, 0} },
	   };
	   capi_v2_proplist_t threshold_proplist = {
			   SIZE_OF_ARRAY(threshold_props),
			   threshold_props
	   };

	   capi_v2_err_t err_code = topo_ptr->module[index].module_ptr->vtbl_ptr->get_properties(topo_ptr->module[index].module_ptr, &threshold_proplist);

	   if (CAPI_V2_FAILED(err_code))
	   {
		   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned error %d when getting thresholds.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
		   topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
		   return capi_v2_err_to_adsp_result(err_code);
	   }

	   if (input.threshold_in_bytes != 1)
	   {
		   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned input threshold %lu. Only a threshold of 1 is supported.", topo_ptr->instance_id, module_info_ptr->module_id, input.threshold_in_bytes);
		   topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
		   return ADSP_EUNSUPPORTED;
	   }

	   if (output.threshold_in_bytes != 1)
	   {
		   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned output threshold %lu. Only a threshold of 1 is supported.", topo_ptr->instance_id, module_info_ptr->module_id, output.threshold_in_bytes);
		   topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
		   return ADSP_EUNSUPPORTED;
	   }
   }

   return ADSP_EOK;
}

static ADSPResult common_topo_init_single_module(topo_struct *topo_ptr,
      const uint32_t index,
      avcs_module_info_t *module_info_ptr,
      const adsp_amdb_module_handle_info_t *handle_info_ptr,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      bool_t initialize_with_compressed_format)
{
   capi_v2_port_num_info_t port_info = topo_graph_get_port_num_info(&topo_ptr->graph, index);

   // Set up default values
   topo_ptr->module[index].module_id  = module_info_ptr->module_id;
   topo_ptr->module[index].is_in_place = FALSE;
   topo_ptr->module[index].requires_data_buffering = TRUE;
   topo_ptr->module[index].is_enabled = TRUE;
   topo_ptr->module[index].module_ptr = NULL;
   topo_ptr->module[index].KPPS       = 0;
   topo_ptr->module[index].bandwidth  = 0;
   topo_ptr->module[index].delay_in_us       = 0;
   topo_ptr->module[index].is_in_chain = FALSE;
   topo_ptr->module[index].disabled_till_next_media_event = FALSE;
   topo_ptr->module[index].headroom = 0;
   topo_ptr->module[index].num_input_ports = port_info.num_input_ports;
   topo_ptr->module[index].num_output_ports = port_info.num_output_ports;
   topo_ptr->module[index].input_ports_ptr = NULL;
   topo_ptr->module[index].output_ports_ptr = NULL;
   topo_event_initialize_handle(topo_ptr, index);

   {
      ADSPResult result = topo_module_allocate_ports(&topo_ptr->module[index]);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed to allocate memory for ports for module %lX.", topo_ptr->instance_id, module_info_ptr->module_id);
         return result;
      }
   }

   // Set up init properties
   capi_v2_heap_id_t heap_id = { topo_get_heap_id(FALSE, pp_type) };
   capi_v2_event_callback_info_t cb_info = { topo_event_callback, topo_event_get_handle(topo_ptr, index) };
   capi_v2_prop_t init_props[] = {
         { CAPI_V2_PORT_NUM_INFO,            { reinterpret_cast<int8_t*>(&port_info),   sizeof(port_info), sizeof(port_info) }, { FALSE, FALSE, 0} },
         { CAPI_V2_HEAP_ID,             { reinterpret_cast<int8_t*>(&heap_id),    sizeof(heap_id),  sizeof(heap_id)  }, { FALSE, FALSE, 0} },
         { CAPI_V2_EVENT_CALLBACK_INFO,  { reinterpret_cast<int8_t*>(&cb_info),     sizeof(cb_info),   sizeof(cb_info)   }, { FALSE, FALSE, 0} }
   };
   capi_v2_proplist_t init_proplist = { SIZE_OF_ARRAY(init_props), init_props };

   // Create the module
   if (ADSP_SUCCEEDED(handle_info_ptr->result) && (NULL != handle_info_ptr->h.capi_v2_handle))
   {
      ADSPResult result = ADSP_EOK;
      switch (handle_info_ptr->interface_type)
      {
      case CAPI_V2:
         result = common_topo_create_capi_v2_module(topo_ptr, handle_info_ptr->h.capi_v2_handle, index, module_info_ptr, pp_type, stack_size_ptr, &init_proplist);
         break;
      case APPI:
         result = common_topo_create_appi_module(topo_ptr, handle_info_ptr->h.appi_handle, index, module_info_ptr, pp_type, stack_size_ptr, &init_proplist, initialize_with_compressed_format);
         break;
      default:
         MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module ID 0x%lX type is neither APPI nor CAPIv2. Type is %d.", topo_ptr->instance_id, module_info_ptr->module_id, handle_info_ptr->interface_type);
         result = ADSP_EFAILED;
         break;
      }

      if (ADSP_FAILED(result))
      {
         topo_module_free_ports(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return result;
      }
   }
   else
   {
         MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module ID 0x%lX is not supported.", topo_ptr->instance_id, module_info_ptr->module_id);
         topo_module_free_ports(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return ADSP_EUNSUPPORTED;
   }

   if (topo_ptr->module[index].requires_data_buffering)
   {
      capi_v2_port_data_threshold_t input, output;

      // Set up the thresholds
      capi_v2_prop_t threshold_props[] = {
            { CAPI_V2_PORT_DATA_THRESHOLD,  { reinterpret_cast<int8_t*>(&input),   0, sizeof(input)   }, { TRUE, TRUE, 0} },
            { CAPI_V2_PORT_DATA_THRESHOLD,  { reinterpret_cast<int8_t*>(&output),   0, sizeof(output)   }, { TRUE, FALSE, 0} },
      };
      capi_v2_proplist_t threshold_proplist = {
            SIZE_OF_ARRAY(threshold_props),
            threshold_props
      };

      capi_v2_err_t err_code = topo_ptr->module[index].module_ptr->vtbl_ptr->get_properties(topo_ptr->module[index].module_ptr, &threshold_proplist);

      if (CAPI_V2_FAILED(err_code))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned error %d when getting thresholds.", topo_ptr->instance_id, module_info_ptr->module_id, static_cast<int>(err_code));
         topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return capi_v2_err_to_adsp_result(err_code);
      }

      if (input.threshold_in_bytes != 1)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned input threshold %lu. Only a threshold of 1 is supported.", topo_ptr->instance_id, module_info_ptr->module_id, input.threshold_in_bytes);
         topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return ADSP_EUNSUPPORTED;
      }

      if (output.threshold_in_bytes != 1)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned output threshold %lu. Only a threshold of 1 is supported.", topo_ptr->instance_id, module_info_ptr->module_id, output.threshold_in_bytes);
         topo_module_destroy(&topo_ptr->module[index], &topo_ptr->buf_mgr);
         return ADSP_EUNSUPPORTED;
      }
   }

   return ADSP_EOK;
}

ADSPResult topo_init(const audproc_topology_definition_t *def_ptr,
      const uint16_t instance_id,
      const uint32_t ouput_buffer_num_unit_frames,
      topo_struct **topo_obj,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      topo_event_client_t *event_client_ptr,
      bool_t initialize_with_compressed_format)
{
   ADSPResult result = ADSP_EOK;
   topo_struct *topo_ptr = NULL;
   uint32_t num_modules = 0;
   uint32_t num_created_modules = 0;
   uint32_t actual_num_modules = 0;
   uint32_t num_modules_filled = 0;

   const audproc_module_info_t *module_info_ptr = &(def_ptr->module_info[0]);
   adsp_amdb_module_handle_info_t *handle_info_ptr = NULL;
   topo_def_t active_topo_def;

   *topo_obj = NULL;
   active_topo_def.module_id_list = NULL;

   if (NULL == stack_size_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Stack size pointer is NULL.", instance_id);
      result = ADSP_EBADPARAM;
      goto cleanup;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology begin", instance_id);

   result = topo_create_obj(instance_id, &topo_ptr, ouput_buffer_num_unit_frames, event_client_ptr);
   if (ADSP_FAILED(result))
   {
      goto cleanup;
   }
   num_modules = def_ptr->num_modules;
   if(0 != num_modules)
   {
       handle_info_ptr = (adsp_amdb_module_handle_info_t *)qurt_elite_memory_malloc(num_modules * sizeof(adsp_amdb_module_handle_info_t), QURT_ELITE_HEAP_DEFAULT);
       if (handle_info_ptr == NULL)
       {
           MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: "
                 "Failed to allocate memory for the module handler", instance_id);
           result = ADSP_ENOMEMORY;
           goto cleanup;
       }

       while (num_modules_filled < num_modules)
       {
           adsp_amdb_module_handle_info_t *handle_info;
           handle_info = handle_info_ptr + num_modules_filled;

           handle_info->interface_type = CAPI_V2;
           handle_info->type = AMDB_MODULE_TYPE_GENERIC;
           handle_info->id1 = module_info_ptr[num_modules_filled].module_id;
           handle_info->id2 = 0;
           handle_info->h.capi_v2_handle = NULL;
           handle_info->result = ADSP_EFAILED;

           num_modules_filled++;
       }
       /*
        * Note: This call will block till all modules with 'preload = 0' are loaded by the AMDB. This loading
        * happens using a thread pool using threads of very low priority. This can cause the current thread
        * to be blocked because of a low priority thread. If this is not desired, a callback function
        * should be provided that can be used by the AMDB to signal when the modules are loaded. The current
        * thread can then handle other tasks in parallel.
        */
       adsp_amdb_get_modules_request(handle_info_ptr, num_modules_filled, NULL, NULL);

       active_topo_def.module_id_list = (uint32_t *)qurt_elite_memory_malloc(num_modules * sizeof(uint32_t), QURT_ELITE_HEAP_DEFAULT);
       if (active_topo_def.module_id_list == NULL)
       {
           MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: "
                 "Failed to allocate memory for the module list", instance_id);
           result = ADSP_ENOMEMORY;
           goto cleanup;
       }
       for (uint32_t j = 0; j < num_modules; j++ )
       {
           if(handle_info_ptr[j].interface_type != STUB)
           {
               active_topo_def.module_id_list[actual_num_modules] = def_ptr->module_info[j].module_id;
               actual_num_modules++;
           }
           else
           {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
                     "P%hX audproc_svc: Module ID 0x%lx is stubbed and removed from the topology ",
                     topo_ptr->instance_id, handle_info_ptr[j].id1);
           }
       }
   }

   active_topo_def.num_modules = actual_num_modules;
   active_topo_def.topology_id = def_ptr->topology_id;

   result = topo_graph_set_graph(&topo_ptr->graph, &active_topo_def);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the graph representation.", topo_ptr->instance_id);
      goto cleanup;
   }

   topo_graph_print_linear_topo(&topo_ptr->graph, instance_id);

    result = topo_allocate_stream_structs(topo_ptr);
    if (ADSP_FAILED(result))
    {
       MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the stream structures.", topo_ptr->instance_id);
       goto cleanup;
    }

    *stack_size_ptr = AUDPROC_TOPO_DEFAULT_STACK_SIZE;

    num_modules = topo_graph_num_modules(&topo_ptr->graph);

    topo_lock_module_list(topo_ptr);
    if((0 != num_modules) && (handle_info_ptr!=NULL))
    {
       topo_ptr->module = (module_info*)qurt_elite_memory_malloc(sizeof(module_info) * num_modules, QURT_ELITE_HEAP_DEFAULT);
       if (NULL == topo_ptr->module)
       {
          MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to allocate modules array", topo_ptr->instance_id);
          topo_destroy(topo_ptr);
          result = ADSP_ENOMEMORY;
          goto cleanup;
       }

	   memset(topo_ptr->module, 0, (sizeof(module_info) * num_modules));
	   /* CRID  948586
		# customer topology is having Popless EQ module before Volume control module. so, even if PoplessEQ raises and headroom even, it shouldn't be honoured since Volume control module is not created yet.
		#As part of topo init, due to previous allocated values in the same memory, PoplessEQ is raising a headroom event (as part of init)
		#the soft volume control library is being set for headroom requirements. But, there is no real requirement for headroom and 
		#volume control library is not yet created. The key here is the previous memory block (with old topo details) being reallocated to the new
		#topology. This scenario is not reproducible with current test framework. Hence memset with zero to flush out old values*/

       const audproc_module_info_t *module_info_ptr = &(def_ptr->module_info[0]);
       uint32_t stub_module_count = 0;

       /* Create the modules */
       for (uint32_t j = 0; j < num_modules; j++ )
       {

          while(handle_info_ptr[j+stub_module_count].interface_type == STUB)
          {
             stub_module_count++;
          }

          result = topo_init_single_module(topo_ptr, j, &module_info_ptr[j+stub_module_count], handle_info_ptr + j + stub_module_count, pp_type, stack_size_ptr, initialize_with_compressed_format);

          if (ADSP_FAILED(result))
          {
             MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module with ID 0x%lX failed in init.", topo_ptr->instance_id, module_info_ptr[stub_module_count + j].module_id);
             break;
          }
          num_created_modules++;
       }
       if (ADSP_FAILED(result))
       {
          goto cleanup;
       }
    }
    topo_set_delay_ptr_virtualizer(topo_ptr);
    topo_unlock_module_list(topo_ptr);

    *topo_obj = topo_ptr;
    topo_update_chain_KPPS(topo_ptr);
    topo_update_chain_bandwidth(topo_ptr);
    topo_update_chain_delay(topo_ptr);
    topo_update_chain_headroom(topo_ptr);

   cleanup:

   if (ADSP_FAILED(result))
   {
      if (NULL != topo_ptr)
      {
         if (NULL != topo_ptr->module)
         {
            topo_destroy_modules(num_created_modules, topo_ptr->module, &topo_ptr->buf_mgr);
            qurt_elite_memory_free(topo_ptr->module);
            topo_ptr->module = NULL;
         }
         topo_destroy(topo_ptr);
         topo_ptr = NULL;
      }
   }

   // No need to keep the handles now since the modules are created.
   if(handle_info_ptr!=NULL)
   {
      adsp_amdb_release_handles(handle_info_ptr, def_ptr->num_modules);
      qurt_elite_memory_free(handle_info_ptr);
   }
   if (NULL != active_topo_def.module_id_list)
   {
      qurt_elite_memory_free(active_topo_def.module_id_list);
      active_topo_def.module_id_list = NULL;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology End", instance_id);

   return result;
}

ADSPResult common_topo_init(elite_cmn_topo_db_entry_t *def_ptr,
      const uint16_t instance_id,
      const uint32_t ouput_buffer_num_unit_frames,
      topo_struct **topo_obj,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      topo_event_client_t *event_client_ptr,
      bool_t initialize_with_compressed_format)
{
   ADSPResult result = ADSP_EOK;
   topo_struct *topo_ptr = NULL;
   uint32_t num_modules = 0;
   uint32_t num_created_modules = 0;
   uint32_t num_modules_filled = 0;
   uint32_t actual_num_modules = 0;
   uint32_t stub_module_count = 0;

   adsp_amdb_module_handle_info_t *handle_info_ptr = NULL;
   topo_def_t active_topo_def;

   memset((void *)&active_topo_def, 0, sizeof(active_topo_def));

   *topo_obj = NULL;

   if (NULL == stack_size_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Stack size pointer is NULL.", instance_id);
      result = ADSP_EBADPARAM;
      goto cleanup;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology begin", instance_id);

   result = topo_create_obj(instance_id, &topo_ptr, ouput_buffer_num_unit_frames, event_client_ptr);
   if (ADSP_FAILED(result))
   {
      goto cleanup;
   }

   num_modules = def_ptr->topo_def.num_modules;
   if(0 != num_modules)
   {
       handle_info_ptr = (adsp_amdb_module_handle_info_t *)qurt_elite_memory_malloc(num_modules * sizeof(adsp_amdb_module_handle_info_t), QURT_ELITE_HEAP_DEFAULT);
       if (handle_info_ptr == NULL)
       {
           MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: "
                 "Failed to allocate memory for the module handler", instance_id);
           result = ADSP_ENOMEMORY;
           goto cleanup;
       }
       while (num_modules_filled < num_modules)
       {
           adsp_amdb_module_handle_info_t *handle_info;
           handle_info = handle_info_ptr + num_modules_filled;

           handle_info->interface_type = CAPI_V2;
           handle_info->type = AMDB_MODULE_TYPE_GENERIC;
           handle_info->id1 = def_ptr->module_info[num_modules_filled].module_id;
           handle_info->id2 = 0;
           handle_info->h.capi_v2_handle = NULL;
           handle_info->result = ADSP_EFAILED;

           num_modules_filled++;
       }

       /*
        * Note: This call will block till all modules with 'preload = 0' are loaded by the AMDB. This loading
        * happens using a thread pool using threads of very low priority. This can cause the current thread
        * to be blocked because of a low priority thread. If this is not desired, a callback function
        * should be provided that can be used by the AMDB to signal when the modules are loaded. The current
        * thread can then handle other tasks in parallel.
        */
       adsp_amdb_get_modules_request(handle_info_ptr, num_modules, NULL, NULL);

       active_topo_def.module_id_list = NULL;
       active_topo_def.module_id_list = (uint32_t *)qurt_elite_memory_malloc(num_modules * sizeof(uint32_t), QURT_ELITE_HEAP_DEFAULT);
       if (active_topo_def.module_id_list == NULL)
       {
           MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: "
                 "Failed to allocate memory for the module list", instance_id);
           result = ADSP_ENOMEMORY;
           goto cleanup;
       }
       for (uint32_t j = 0; j < num_modules; j++ )
       {
           if(handle_info_ptr[j].interface_type != STUB)
           {
               active_topo_def.module_id_list[actual_num_modules] = def_ptr->module_info[j].module_id;
               actual_num_modules++;
           }
           else
           {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
                     "P%hX audproc_svc: Module ID 0x%lx is stubbed and removed from the topology ",
                     topo_ptr->instance_id, handle_info_ptr[j].id1);
           }
       }
   }

   active_topo_def.num_modules = actual_num_modules;
   active_topo_def.topology_id = def_ptr->topo_def.topology_id;

   result = common_topo_graph_set_graph(&topo_ptr->graph, &active_topo_def);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the graph representation.", topo_ptr->instance_id);
      goto cleanup;
   }

   topo_graph_print_linear_topo(&topo_ptr->graph, instance_id);

   result = topo_allocate_stream_structs(topo_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the stream structures.", topo_ptr->instance_id);
      goto cleanup;
   }

   *stack_size_ptr = AUDPROC_TOPO_DEFAULT_STACK_SIZE;

   num_modules = topo_graph_num_modules(&topo_ptr->graph);

   topo_lock_module_list(topo_ptr);
   if((0 != num_modules) && (handle_info_ptr!=NULL))
   {
      topo_ptr->module = (module_info*)qurt_elite_memory_malloc(sizeof(module_info) * num_modules, QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_ptr->module)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to allocate modules array", topo_ptr->instance_id);
         topo_destroy(topo_ptr);
         result = ADSP_ENOMEMORY;
         goto cleanup;
      }
	  
		memset(topo_ptr->module, 0, (sizeof(module_info) * num_modules));
	   /* CRID  948586
		# customer topology is having Popless EQ module before Volume control module. so, even if PoplessEQ raises and headroom even, it shouldn't be honoured since Volume control module is not created yet.
		#As part of topo init, due to previous allocated values in the same memory, PoplessEQ is raising a headroom event (as part of init)
		#the soft volume control library is being set for headroom requirements. But, there is no real requirement for headroom and 
		#volume control library is not yet created. The key here is the previous memory block (with old topo details) being reallocated to the new
		#topology. This scenario is not reproducible with current test framework. Hence memset with zero to flush out old values*/

      avcs_module_info_t *module_info_ptr = &(def_ptr->module_info[0]);

      /* Create the modules */
      for (uint32_t j = 0; j < num_modules; j++ )
      {
         while(handle_info_ptr[j+stub_module_count].interface_type == STUB)
         {
            stub_module_count++;
         }
         result = common_topo_init_single_module(topo_ptr, j, &module_info_ptr[stub_module_count + j], handle_info_ptr + stub_module_count + j, pp_type, stack_size_ptr, initialize_with_compressed_format);
         if (ADSP_FAILED(result))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module with ID 0x%lX failed in init.",
                  topo_ptr->instance_id, module_info_ptr[stub_module_count + j].module_id);
            break;
         }
         num_created_modules++;
      }
      if (ADSP_FAILED(result))
      {
         goto cleanup;
      }

   }

   topo_set_delay_ptr_virtualizer(topo_ptr);
   topo_unlock_module_list(topo_ptr);

   *topo_obj = topo_ptr;
   topo_update_chain_KPPS(topo_ptr);
   topo_update_chain_bandwidth(topo_ptr);
   topo_update_chain_delay(topo_ptr);
   topo_update_chain_headroom(topo_ptr);

cleanup:

   if (ADSP_FAILED(result))
   {
      if (NULL != topo_ptr)
      {
         if (NULL != topo_ptr->module)
         {
            topo_destroy_modules(num_created_modules, topo_ptr->module, &topo_ptr->buf_mgr);
            qurt_elite_memory_free(topo_ptr->module);
            topo_ptr->module = NULL;
         }
         topo_destroy(topo_ptr);
         topo_ptr = NULL;
      }
   }


   // No need to keep the handles now since the modules are created.
   if(handle_info_ptr!=NULL)
   {
      adsp_amdb_release_handles(handle_info_ptr, def_ptr->topo_def.num_modules);
      qurt_elite_memory_free(handle_info_ptr);
   }
   if (NULL != active_topo_def.module_id_list)
   {
      qurt_elite_memory_free(active_topo_def.module_id_list);
      active_topo_def.module_id_list = NULL;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology End", instance_id);

   return result;
}
/**
 * Outputs data from any intermediate buffers if present. This
 * can be done at end of stream or before a change in media type
 * or parameters.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] obj_ptr
 *   Pointer to the topology object.
 * @param[out] outbuf_ptr
 *   The output buffer to which the data is to be written.
 * @param[out] buffer_reset_flag_ptr
 *   Indicates if there is still any data left in the
 *   intermediate buffers.
 *
 */
void topo_buffer_reset (topo_struct *topo_ptr,
      AudPP_BufInfo_t* outbuf_ptr,
      AudPP_BufInfo_t* prev_inbuf_ptr,
      bool_t* buffer_reset_flag_ptr)
{
   ADSPResult result;

   *buffer_reset_flag_ptr = FALSE;

   if (NULL == topo_ptr->eos_buf.pBuf)
   {
      result = create_delay_buffer(topo_ptr);
      if (ADSP_FAILED(result))
      {
         topo_ptr->eos_buf.pBuf = NULL;
         topo_ptr->eos_buf.sizeInSamples = 0;
         topo_ptr->eos_buf.offsetInSamples = 0;
         *buffer_reset_flag_ptr = TRUE;
         return;
      }
   }

   topo_ptr->eos_buf.flags = prev_inbuf_ptr->flags;
   if((topo_ptr->eos_buf.flags.is_timestamp_valid) && (topo_ptr->in_format.samplingRate!=0))
   {
      topo_ptr->eos_buf.timestamp = prev_inbuf_ptr->timestamp +(prev_inbuf_ptr->sizeInSamples *1000000)/topo_ptr->in_format.samplingRate;

   }

   // If there is delay associated with this chain
   // then flush out intermediate buffers into
   // the output buffers, before setting flag
   topo_process(topo_ptr,
         &(topo_ptr->eos_buf),
         outbuf_ptr);

}


void topo_free_eos_buffer (topo_struct *topo_ptr,
      bool_t* buffer_reset_flag_ptr)
{
   *buffer_reset_flag_ptr = FALSE;
    if (NULL != topo_ptr->eos_buf.pBuf)
    {
     qurt_elite_memory_free(topo_ptr->eos_buf.pBuf);
     topo_ptr->eos_buf.pBuf= NULL;
    }
    topo_ptr->eos_buf.sizeInSamples = 0;
    topo_ptr->eos_buf.offsetInSamples = 0;
    *buffer_reset_flag_ptr = TRUE;
}


/**
 * Get the samples buffered at the input of each module in the
 * topology chain. This value is used to correct the offset value
 * during the time-stamp calculation
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] obj_ptr
 *   Pointer to the object structure.
 * @return uint64_t
 *   returns the samples buffered in the topology.
 */
uint64_t topo_get_internal_buffer_delay(topo_struct *topo_ptr)
{
	uint64_t total_buffer_delay = 0;
	uint32_t current_module_index = topo_ptr->last_port.module_index;

	while((topo_ptr->first_port.module_index != current_module_index) && (current_module_index!=TOPO_INVALID_MODULE_INDEX))
	{
		// The implementation assumes there is only one input port
		// for each module in present use-cases.
		QURT_ELITE_ASSERT(1 == topo_ptr->module[current_module_index].num_input_ports);
		uint32_t buffered_samples_at_module_input = 0;
		uint32_t module_input_port_index = 0;
		const topo_port_index_t port_index = { current_module_index, module_input_port_index, topo_port_index_t::INPUT_PORT };

		if(topo_ptr->module[current_module_index].is_in_chain)
		{
			topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);
			uint32_t input_length = in_buf_ptr->valid_data_end - in_buf_ptr->valid_data_start;
			buffered_samples_at_module_input = topo_buffer_bytes_to_samples(in_buf_ptr, input_length);
			total_buffer_delay += ((uint64_t)buffered_samples_at_module_input * 1000000)/
					topo_ptr->module[current_module_index].input_ports_ptr[module_input_port_index].media_event.event.f.sampling_rate;
		}

		topo_port_index_t prev_port_index = { TOPO_INVALID_MODULE_INDEX, 0, topo_port_index_t::OUTPUT_PORT };
		prev_port_index = topo_graph_get_prev_module(&topo_ptr->graph, port_index);
		current_module_index = prev_port_index.module_index;
	}

	/*MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: total buffer_duration %llu",
			topo_ptr->instance_id, total_buffer_delay);*/

	return total_buffer_delay;
}
/**
 *  This function post-processes input data to output buffer
 *  It also correctly sets the input/output buffer offset after operation.
 * @param[in, out] obj_ptr This points to the current topology
 *          instance.
 * @param[in] inbuf_ptr  This describes the input buffer.
 * @param[in] outbuf_ptr   This describes the output buffer.
 *
 * @return ADSPResult ADSP error code
 */
ADSPResult topo_process(topo_struct *topo_ptr,
      AudPP_BufInfo_t *inbuf_ptr,
      AudPP_BufInfo_t *outbuf_ptr)
{
	if (!topo_ptr->data_format_received)
	{
		return ADSP_ENOTREADY;
	}


	topo_buf_t *main_input_buffer_ptr = &topo_ptr->main_input_buffer;
	topo_buf_t *main_output_buffer_ptr = &topo_ptr->main_output_buffer;

	// Convert the input and output buffers to the appi_buflist_t form.
	topo_buffer_convert_from_ext_input_buffer(inbuf_ptr,
			main_input_buffer_ptr,
			&topo_ptr->in_format);

	topo_buffer_convert_from_ext_output_buffer(outbuf_ptr,
			main_output_buffer_ptr,
			&topo_ptr->out_format);

	// Since there can be intermediate buffering, there is no guarantee of the number
	// of samples consumed and produced. So loop till either input buffer
	// is empty or the output buffer is full.
	// consume any more input as this will just increase the data in the internal buffers.
	topo_lock_module_list(topo_ptr);

	bool_t topo_output_media_changed = FALSE;
	if (!topo_zero_modules_enabled(topo_ptr))
	{
		uint32_t current_module_index = topo_ptr->last_port.module_index;

		while (!topo_buffer_is_buf_full(main_output_buffer_ptr))
		{
            QURT_ELITE_ASSERT(TOPO_INVALID_MODULE_INDEX != current_module_index);
            if(current_module_index == topo_ptr->last_port.module_index)
            {
                topo_check_empty_last_buffer(topo_ptr);
            }
            if(topo_buffer_is_buf_full(main_output_buffer_ptr))
            {
                break;
            }

			if (topo_module_pending_reconfig(topo_ptr, current_module_index))
			{
				topo_output_media_changed = topo_handle_pending_events(topo_ptr, current_module_index);
				if (topo_output_media_changed || topo_zero_modules_enabled(topo_ptr))
				{
					break;
				}
				else
				{
					current_module_index = topo_ptr->last_port.module_index;
				}
			}
			else
			{
			   module_info *current_module = &topo_ptr->module[current_module_index];
			   if (topo_module_can_process(topo_ptr, current_module_index))
			   {
			      // Process data
			      QURT_ELITE_ASSERT(current_module->is_in_chain);

			      ADSPResult err_code = topo_process_module(topo_ptr, current_module_index);
			      if (ADSP_FAILED(err_code))
			      {
			         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed while processing module with id %lu. Disabling it.", topo_ptr->instance_id, current_module->module_id);
			         current_module->disabled_till_next_media_event = TRUE;
			         continue;
			      }

			      for (uint32_t i = 0; i < current_module->num_output_ports; i++)
				{
			         uint32_t connected_module_index = current_module->output_ports_ptr[i].connected_port.module_index;
			         if ((TOPO_INVALID_MODULE_INDEX != connected_module_index)
			               &&(connected_module_index > current_module_index)
			               && topo_module_can_process(topo_ptr, connected_module_index))
					{
			            current_module_index = connected_module_index;
					}
					}
				}
				else
				{
			      if (0 == current_module_index)
					{

			         QURT_ELITE_ASSERT(topo_buffer_is_buf_empty(main_input_buffer_ptr));
			         break;
						}
			      else
						{
			         current_module_index--;
					}
				}
			}
		}
	}

   if (!topo_output_media_changed) // If the output media changed, the output buffer must not be written to.
   {
      if (topo_zero_modules_enabled(topo_ptr))
      {
         topo_loop_thru_zero_modules(topo_ptr);
      }
   }

	QURT_ELITE_ASSERT(topo_output_media_changed || topo_buffer_is_buf_empty(main_input_buffer_ptr) || topo_buffer_is_buf_full(main_output_buffer_ptr));

   topo_unlock_module_list(topo_ptr);

	topo_buffer_update_external_input_buf_sizes(inbuf_ptr, main_input_buffer_ptr, &topo_ptr->in_format);
	topo_buffer_update_external_output_buf_sizes(outbuf_ptr, main_output_buffer_ptr, &topo_ptr->out_format);

	return ADSP_EOK;
}

static ADSPResult topo_process_module(topo_struct *topo_ptr, uint32_t module_index)
{

	if (topo_inplace_processing_possible(topo_ptr, module_index))
	{
		return topo_process_module_inplace(topo_ptr, module_index);
	}

   module_info *module_struct_ptr = &topo_ptr->module[module_index];

   uint32_t min_samples = UINT32_MAX;

   // Set up the structures for the input streams for every port.
   QURT_ELITE_ASSERT(module_struct_ptr->num_input_ports <= topo_ptr->max_in_streams);
   for (uint32_t i = 0; i < module_struct_ptr->num_input_ports; i++)
   {
      const topo_port_index_t port_index = { module_index, i, topo_port_index_t::INPUT_PORT };

      topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);

      uint32_t input_length = in_buf_ptr->valid_data_end - in_buf_ptr->valid_data_start;
      uint32_t samples_in_input = topo_buffer_bytes_to_samples(in_buf_ptr, input_length);
      if (samples_in_input < min_samples)
      {
         min_samples = samples_in_input;
      }

      // Set up the input buffers
      topo_ptr->in_streams[i].flags = in_buf_ptr->flags;
      topo_ptr->in_streams[i].timestamp = in_buf_ptr->timestamp;

      if(in_buf_ptr->valid_data_start!=0)
      {
         uint64_t timestamp_offset = ((in_buf_ptr->valid_data_start/in_buf_ptr->bytes_to_samples_factor) * 1000000);
         timestamp_offset/=(module_struct_ptr->input_ports_ptr->media_event.event.f.sampling_rate);
         topo_ptr->in_streams[i].timestamp += timestamp_offset;
      }

      topo_ptr->in_streams[i].bufs_num = in_buf_ptr->num_bufs;
      QURT_ELITE_ASSERT(in_buf_ptr->num_bufs <= CAPI_V2_MAX_CHANNELS);
      capi_v2_buf_t *in_bufs = topo_ptr->in_streams[i].buf_ptr;
      for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
      {
         in_bufs[j].actual_data_len = input_length;
         in_bufs[j].max_data_len = in_buf_ptr->size_per_buffer - in_buf_ptr->valid_data_start;
         in_bufs[j].data_ptr = in_buf_ptr->buf_ptrs[j] + in_buf_ptr->valid_data_start;
      }
   }


   for (uint32_t i = 0; i < module_struct_ptr->num_output_ports; i++)
   {
      const topo_port_index_t port_index = { module_index, i, topo_port_index_t::OUTPUT_PORT } ;

      topo_buf_t *out_buf_ptr = topo_module_get_output_buffer(topo_ptr, &port_index);

	// Rewind the output buffer if it is empty. Also get the buffers from the buffer manager.
	if (topo_buffer_is_buf_empty(out_buf_ptr))
	{
		out_buf_ptr->valid_data_start = out_buf_ptr->valid_data_end = 0;

         if (module_index != topo_ptr->last_port.module_index)
		{
			ADSPResult err_code = topo_buf_manager_get_bufs(&topo_ptr->buf_mgr, out_buf_ptr);
			if (ADSP_FAILED(err_code))
			{
               MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu failed to allocate output buffers for port %lu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, i);
				return err_code;
			}
		}
	}

      uint32_t output_length = out_buf_ptr->size_per_buffer - out_buf_ptr->valid_data_end;
      uint32_t samples_in_output = topo_buffer_bytes_to_samples(out_buf_ptr, output_length);
      if (samples_in_output < min_samples)
      {
         min_samples = samples_in_output;
      }
	// Calculate the input and output lengths. The input length is the number of bytes provided to the module. The output
	// length is the number of bytes available for the module to fill in the output buffer.


	// Set up the input buffers
      if ((1 == module_struct_ptr->num_input_ports) && (1 == module_struct_ptr->num_output_ports))
      {
         topo_ptr->out_streams[i].flags = topo_ptr->in_streams[0].flags; // Default value
         topo_ptr->out_streams[i].timestamp = topo_ptr->in_streams[0].timestamp; // Default value
      }
      else
	{
         topo_ptr->out_streams[i].flags = init_stream_data.flags; // Default value
         topo_ptr->out_streams[i].timestamp = init_stream_data.timestamp; // Default value
	}
      topo_ptr->out_streams[i].bufs_num = out_buf_ptr->num_bufs;
      QURT_ELITE_ASSERT(out_buf_ptr->num_bufs <= CAPI_V2_MAX_CHANNELS);
      capi_v2_buf_t *out_bufs = topo_ptr->out_streams[i].buf_ptr;
      for(uint32_t j = 0; j < out_buf_ptr->num_bufs; j++)
	{
		out_bufs[j].actual_data_len = 0;
		out_bufs[j].max_data_len = output_length;
		out_bufs[j].data_ptr = out_buf_ptr->buf_ptrs[j] + out_buf_ptr->valid_data_end;
	}
   }

   // Adjust the input buffer length if the module requires data buffering. All input buffers must have the
   // same number of samples. Also all output buffers should have enough space for that many samples. So we
   // set the number of samples in each input buffer to be the minimum of the number of valid samples over
   // all input buffers and the number of samples that can be stored over all output buffers.
   if (!module_struct_ptr->requires_data_buffering)
   {
      for (uint32_t i = 0; i < module_struct_ptr->num_input_ports; i++)
      {
         const topo_port_index_t port_index = { module_index, i, topo_port_index_t::INPUT_PORT };

         topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);
         uint32_t	input_length = topo_buffer_samples_to_bytes(in_buf_ptr, min_samples);
         capi_v2_buf_t *in_bufs = topo_ptr->in_streams[i].buf_ptr;
         for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
         {
            in_bufs[j].actual_data_len = input_length;
         }
      }
   }

	// Process data
	capi_v2_err_t err_code = CAPI_V2_EOK;
	capi_v2_t *m_ptr = module_struct_ptr->module_ptr;

#ifdef AUDPPSVC_TEST
      uint64_t prof_cycles = hexagon_sim_read_pcycles();
#endif // AUDPPSVC_TEST

   err_code = m_ptr->vtbl_ptr->process(m_ptr, topo_ptr->in_stream_ptrs, topo_ptr->out_stream_ptrs);

#ifdef AUDPPSVC_TEST
	{

		// Diff Current Cycle State against previously acquired to
		// check Cycles elapsed
		prof_cycles = hexagon_sim_read_pcycles() - prof_cycles;
		prof_cycles /= QURT_ELITE_MAX_HW_THREADS;

      const topo_port_index_t port_index = { module_index, 0, topo_port_index_t::OUTPUT_PORT } ;

      topo_buf_t *out_buf_ptr = topo_module_get_output_buffer(topo_ptr, &port_index);

		uint32_t sample_count = topo_ptr->out_stream_ptrs[0]->buf_ptr[0].actual_data_len / out_buf_ptr->bytes_to_samples_factor;

		// Add to Total Cycles
		// Ignoring the very first iteration of the processing
		if (module_struct_ptr->count > 0)
		{
			module_struct_ptr->prof_total_cycles += prof_cycles;
			module_struct_ptr->prof_num_samples += sample_count;

			// Maximum mips
			float mips_this_iteration = convert_cycles_to_mips(48000, sample_count, prof_cycles);
			if (mips_this_iteration > module_struct_ptr->prof_peak_mips)
			{
				module_struct_ptr->prof_peak_mips = mips_this_iteration;
			}
		}
		module_struct_ptr->count++;
	}
#endif// AUDPPSVC_TEST

	if(CAPI_V2_FAILED(err_code))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu failed in process with code %d. Dropping data.", topo_ptr->instance_id, module_struct_ptr->module_id, static_cast<int>(err_code));
      for (uint32_t i = 0; i < module_struct_ptr->num_input_ports; i++)
		{
         const topo_port_index_t port_index = { module_index, i, topo_port_index_t::INPUT_PORT };

         topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);
         uint32_t	input_length = topo_buffer_samples_to_bytes(in_buf_ptr, min_samples);
         capi_v2_buf_t *in_bufs = topo_ptr->in_streams[i].buf_ptr;
         for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
         {
            in_bufs[j].actual_data_len = input_length;
         }
		}
	}

   bool_t fully_used_buffer_found = FALSE;

   // Update the offsets in the input buffers.
   for (uint32_t i = 0; i < module_struct_ptr->num_input_ports; i++)
	{
      const topo_port_index_t port_index = { module_index, i, topo_port_index_t::INPUT_PORT };

      topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);
      capi_v2_buf_t *in_bufs = topo_ptr->in_streams[i].buf_ptr;

	in_buf_ptr->valid_data_start += in_bufs[0].actual_data_len;
	if (in_buf_ptr->valid_data_start > in_buf_ptr->valid_data_end)
	{
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Input buffer read beyond the boundary from module id %lu, port %lu.", topo_ptr->instance_id, module_struct_ptr->module_id, i);
		in_buf_ptr->valid_data_start = in_buf_ptr->valid_data_end;
	}
	if (topo_buffer_is_buf_empty(in_buf_ptr))
	{
         fully_used_buffer_found = TRUE;
		in_buf_ptr->valid_data_start = in_buf_ptr->valid_data_end = 0;
         if (
               !(
                     (module_index == topo_ptr->first_port.module_index)
                     && (i == topo_ptr->first_port.port_index)
               )
         )
		{
			topo_buf_manager_return_bufs(&topo_ptr->buf_mgr, in_buf_ptr);
		}
	}

      // There should not be any buffering between modules that don't require buffering except at the input of the last module.
      QURT_ELITE_ASSERT(module_struct_ptr->requires_data_buffering
            || ((topo_ptr->first_port.module_index == module_index) && (topo_ptr->first_port.port_index == i))
            || ((topo_ptr->last_port.module_index == module_index) && (topo_ptr->last_port.port_index == i))
            || topo_buffer_is_buf_empty(in_buf_ptr));
   }

   // Update the offsets in the output buffers.
   for (uint32_t i = 0; i < module_struct_ptr->num_output_ports; i++)
   {
      const topo_port_index_t port_index = { module_index, i, topo_port_index_t::OUTPUT_PORT };

      topo_buf_t *out_buf_ptr = topo_module_get_output_buffer(topo_ptr, &port_index);

      capi_v2_buf_t *out_bufs = topo_ptr->out_streams[i].buf_ptr;

      out_buf_ptr->flags = topo_ptr->out_streams[i].flags;

      if((out_buf_ptr->flags.is_timestamp_valid) && (out_buf_ptr->valid_data_end == 0))
      {
         if(topo_ptr->in_streams[0].timestamp == topo_ptr->out_streams[i].timestamp)
         {
           if(out_buf_ptr->valid_data_end == 0)
           {
            topo_ptr->out_streams[i].timestamp = topo_ptr->in_streams[0].timestamp
                                                      - topo_ptr->module[module_index].delay_in_us;
           }
         }
         out_buf_ptr->timestamp = topo_ptr->out_streams[i].timestamp;
      }

      out_buf_ptr->valid_data_end += out_bufs[0].actual_data_len;
      if (out_buf_ptr->valid_data_end > out_buf_ptr->size_per_buffer)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Output buffer overflowed from module id %lu.", topo_ptr->instance_id, module_struct_ptr->module_id);
         out_buf_ptr->valid_data_end = out_buf_ptr->size_per_buffer;
      }
      if (topo_buffer_is_buf_full(out_buf_ptr))
      {
         fully_used_buffer_found = TRUE;
      }
	if (topo_buffer_is_buf_empty(out_buf_ptr))
	{
         if (
               !(
                     (module_index == topo_ptr->last_port.module_index)
                     && (i == topo_ptr->last_port.port_index)
               )
         )
		{
			topo_buf_manager_return_bufs(&topo_ptr->buf_mgr, out_buf_ptr);
		}
	}
   }

	// There should not be any buffering between modules that don't require buffering except at the input of the last module.
   if (!fully_used_buffer_found)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: No fully used buffer found after calling process for module id %lu.", topo_ptr->instance_id, module_struct_ptr->module_id);
      //QURT_ELITE_ASSERT(0);
   }

	return ADSP_EOK;
}

static bool_t topo_inplace_processing_possible(topo_struct *topo_ptr, uint32_t module_index)
{
	module_info *module_struct_ptr = &topo_ptr->module[module_index];

	if (!module_struct_ptr->is_in_place)
	{
		return FALSE;
	}

   if (module_struct_ptr->num_input_ports != 1)
   {
      return FALSE;
   }

   if (module_struct_ptr->num_output_ports != 1)
   {
      return FALSE;
   }

   const topo_port_index_t port_index = { module_index, 0, topo_port_index_t::OUTPUT_PORT};
   topo_buf_t *out_buf_ptr = topo_module_get_output_buffer(topo_ptr, &port_index);

	if (!topo_buffer_is_buf_empty(out_buf_ptr))
	{
		return FALSE;
	}

   if (module_index == topo_ptr->first_port.module_index)
	{
		return FALSE;
	}

   if (module_index == topo_ptr->last_port.module_index)
	{
		return FALSE;
	}

	return TRUE;
}

static ADSPResult topo_process_module_inplace(topo_struct *topo_ptr, uint32_t module_index)
{
   QURT_ELITE_ASSERT(1 == topo_ptr->module[module_index].num_input_ports);
   QURT_ELITE_ASSERT(1 == topo_ptr->module[module_index].num_output_ports);

   topo_port_index_t port_index = { module_index, 0, topo_port_index_t::INPUT_PORT };
   topo_buf_t *in_buf_ptr = topo_module_get_input_buffer(topo_ptr, &port_index);
   port_index.type = topo_port_index_t::OUTPUT_PORT;
   topo_buf_t *out_buf_ptr = topo_module_get_output_buffer(topo_ptr, &port_index);

	module_info *module_struct_ptr = &topo_ptr->module[module_index];

	// Copy the buffer data from the input buffers to the output buffers.
	*out_buf_ptr = *in_buf_ptr;
	out_buf_ptr->valid_data_end = out_buf_ptr->valid_data_start; // Output buffer is empty at this point.

	// Calculate the input and output length. The input length is the number of bytes provided to the module. The output
	// length is the number of bytes available for the module to fill in the output buffer.
	uint32_t process_length = in_buf_ptr->valid_data_end - in_buf_ptr->valid_data_start;

	// Set up the input buffers
	QURT_ELITE_ASSERT(1 <= topo_ptr->max_in_streams);
   topo_ptr->in_streams[0].flags = in_buf_ptr->flags;
   topo_ptr->in_streams[0].timestamp = in_buf_ptr->timestamp;
   topo_ptr->in_streams[0].bufs_num = in_buf_ptr->num_bufs;
   QURT_ELITE_ASSERT(in_buf_ptr->num_bufs <= CAPI_V2_MAX_CHANNELS);
   capi_v2_buf_t *in_bufs = topo_ptr->in_streams[0].buf_ptr;
   for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
	{
		in_bufs[j].actual_data_len = process_length;
		in_bufs[j].max_data_len = in_buf_ptr->size_per_buffer - in_buf_ptr->valid_data_start;
		in_bufs[j].data_ptr = in_buf_ptr->buf_ptrs[j] + in_buf_ptr->valid_data_start;
	}

	// Set up the output buffers
   topo_ptr->out_streams[0].flags = topo_ptr->in_streams[0].flags; // Default value
   topo_ptr->out_streams[0].timestamp = topo_ptr->in_streams[0].timestamp; // Default value
   topo_ptr->out_streams[0].bufs_num = out_buf_ptr->num_bufs;
   QURT_ELITE_ASSERT(out_buf_ptr->num_bufs <= CAPI_V2_MAX_CHANNELS);
   capi_v2_buf_t *out_bufs = topo_ptr->out_streams[0].buf_ptr;
   for(uint32_t j = 0; j < out_buf_ptr->num_bufs; j++)
	{
		out_bufs[j].actual_data_len = 0;
		out_bufs[j].max_data_len = process_length;
		out_bufs[j].data_ptr = out_buf_ptr->buf_ptrs[j] + out_buf_ptr->valid_data_end;
	}

	// Process data
	capi_v2_err_t err_code = CAPI_V2_EOK;
	capi_v2_t *m_ptr = module_struct_ptr->module_ptr;
   err_code = m_ptr->vtbl_ptr->process(m_ptr, topo_ptr->in_stream_ptrs, topo_ptr->out_stream_ptrs);
	if(CAPI_V2_FAILED(err_code))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu failed in process with code %d. Passing data through.", topo_ptr->instance_id, module_struct_ptr->module_id, static_cast<int>(err_code));

      capi_v2_buf_t *in_bufs = topo_ptr->in_streams[0].buf_ptr;
      for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
		{
         in_bufs[j].actual_data_len = process_length;
		}
      capi_v2_buf_t *out_bufs = topo_ptr->out_streams[0].buf_ptr;
      for(uint32_t j = 0; j < in_buf_ptr->num_bufs; j++)
		{
         out_bufs[j].actual_data_len = process_length;
		}
	}

	if ((in_buf_ptr->valid_data_start + in_bufs[0].actual_data_len) > in_buf_ptr->valid_data_end)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Input data buffer overflowed for the inplace module id %lu.", topo_ptr->instance_id, module_struct_ptr->module_id);
	}
	if ((in_buf_ptr->valid_data_start + in_bufs[0].actual_data_len) < in_buf_ptr->valid_data_end)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Input data is not fully consumed for the inplace module id %lu. The rest of the data will be dropped.", topo_ptr->instance_id, module_struct_ptr->module_id);
	}

   out_buf_ptr->flags = topo_ptr->out_streams[0].flags;
   out_buf_ptr->timestamp = topo_ptr->out_streams[0].timestamp;
   out_buf_ptr->valid_data_end += out_bufs[0].actual_data_len;
	if (out_buf_ptr->valid_data_end > out_buf_ptr->size_per_buffer)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Output buffer overflowed from module id %lu.", topo_ptr->instance_id, module_struct_ptr->module_id);
		out_buf_ptr->valid_data_end = out_buf_ptr->size_per_buffer;
	}
	if (out_buf_ptr->valid_data_end != in_buf_ptr->valid_data_end)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Output data is not equal to input for the inplace module id %lu.", topo_ptr->instance_id, module_struct_ptr->module_id);
		out_buf_ptr->valid_data_end = in_buf_ptr->valid_data_end;
	}

	// Set all the input buffers to NULL since the pointer have now been moved to the output buffer. Also the input buffer is now empty.
	in_buf_ptr->valid_data_start = in_buf_ptr->valid_data_end = 0;
	for (uint32_t i = 0; i < in_buf_ptr->num_bufs; i++)
	{
		in_buf_ptr->buf_ptrs[i] = NULL;
	}

	return ADSP_EOK;
}

/**
 * Function to change the input media type.
 *   - Dependencies: None
 *   - Side Effects: Will cause some modules to reinitialize
 *     their internal states.
 *   - Re-entrant: Yes
 *
 * @param[in, out] obj_ptr
 *   Pointer to the object structure.
 * @param[in] pInMediaFormat
 *   The input media format.
 *
 */
void topo_change_input_media_type (topo_struct *topo_ptr, const AdspAudioMediaFormatInfo_t *pInMediaFormat)
{
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Change Input Mediatype Begin", topo_ptr->instance_id);
#endif

   if (!topo_ptr->data_format_received)
   {
      topo_setup_connections(topo_ptr);
	topo_ptr->data_format_received = TRUE;
   }

	topo_ptr->in_format = *pInMediaFormat;

		// Passing TOPO_INVALID_MODULE_INDEX to this function causes it to set the input media type of the first module.
		// Media format events are automatically propagated, so this will cause the media format to go down the list of modules.
		topo_handle_pending_events(topo_ptr, TOPO_INVALID_MODULE_INDEX);
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Change Input Mediatype End", topo_ptr->instance_id);
#endif
}

void topo_set_output_sample_rate (topo_struct *topo_ptr, uint32_t module_id, uint32_t output_sample_rate, uint32_t port_id)
{
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Set Output Sample Rate", topo_ptr->instance_id);
#endif

   ADSPResult result;
   capi_v2_err_t err_code;
   uint32_t index = 0;
   result = topo_find_module(topo_ptr, module_id, &index);
   if(result!=ADSP_EOK)
   {
	   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology attempt to set Output Sample Rate to unsupported module ID 0x%8lx!", topo_ptr->instance_id, module_id);
	  return;
   }

   topo_media_fmt_t media_format;
   media_format = invalid_media_format;
   media_format.f.sampling_rate = output_sample_rate;

   capi_v2_prop_t prop = {
         CAPI_V2_OUTPUT_MEDIA_FORMAT,
         {
               reinterpret_cast<int8_t*>(&media_format),
               sizeof(media_format),
               sizeof(media_format)
         },
         {
               TRUE,
               FALSE,
               port_id
         }
   };
   capi_v2_proplist_t proplist = { 1, &prop };
   capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
   err_code = module_ptr->vtbl_ptr->set_properties(module_ptr, &proplist);
   if (CAPI_V2_FAILED(err_code))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID %lX failed to set property for output sampling rate.", topo_ptr->instance_id, module_id);
   }
}


/**
 * Function to clear the internal history for all modules. This
 * does not reset the module parameters to their defaults.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] obj_ptr
 *   Pointer to the object structure.
 *
 * @return ADSPResult
 *   Error code returning status.
 */
void topo_reset (topo_struct *topo_ptr)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Reset Begin", topo_ptr->instance_id);

   // The reset set property does not need any parameters.
   capi_v2_prop_t prop;
   prop.id = CAPI_V2_ALGORITHMIC_RESET;
   prop.payload.actual_data_len = 0;
   prop.payload.max_data_len = 0;
   prop.payload.data_ptr = NULL;
   prop.port_info.is_valid = FALSE;
   capi_v2_proplist_t proplist;
   proplist.props_num = 1;
   proplist.prop_ptr = &prop;

   // Loop through all the modules and reset each.
   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
      capi_v2_err_t err_code;
      capi_v2_t *module_ptr = topo_ptr->module[i].module_ptr;

      err_code = module_ptr->vtbl_ptr->set_properties(module_ptr, &proplist);
      if (CAPI_V2_FAILED(err_code))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID %lX failed to reset.", topo_ptr->instance_id, topo_ptr->module[i].module_id);
      }

      for (uint32_t j = 0; j < topo_ptr->module[i].num_output_ports; j++)
      {
         topo_buf_t *buf_ptr = &topo_ptr->module[i].output_ports_ptr[j].topo_buf;
	   if (!topo_buffer_is_buf_empty(buf_ptr))
	   {
		   topo_buf_manager_return_bufs(&topo_ptr->buf_mgr, buf_ptr);
		   buf_ptr->valid_data_start = buf_ptr->valid_data_end = 0;
	   }
   }
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Reset End", topo_ptr->instance_id);
}

/**
 * Set the params for the modules in this topology.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] obj_ptr
 *   Pointer to the object structure.
 * @param[in] pPayload
 *   Pointer to the payload of the set params data.
 * @param[in] payloadSize
 *   Size of the payload of the set params data, in bytes.
 *
 * @return ADSPResult
 *   Error code returning status.
 */
ADSPResult topo_set_param (topo_struct *topo_ptr, void *pPayload, uint32_t payloadSize)
{
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Set params Begin.", topo_ptr->instance_id);
#endif
   ADSPResult result = ADSP_EOK;
   uint8_t *pPacket = (uint8_t *)pPayload;

   if(!IS_ALIGN_4_BYTE(pPacket))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Set param pointer is not aligned to 4 bytes. Aborting.", topo_ptr->instance_id);
      return ADSP_EFAILED;
   }

   while (payloadSize > 0)
   {
      if (payloadSize < sizeof(asm_stream_param_data_v2_t))
      {
         break;
      }

      asm_stream_param_data_v2_t *pDataPayload = (asm_stream_param_data_v2_t *)(pPacket);

      if (payloadSize < (sizeof(asm_stream_param_data_v2_t) + pDataPayload->param_size))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Insufficient size, payload size=%lu, Param size=%hu.", topo_ptr->instance_id, payloadSize, pDataPayload->param_size);
         result = ADSP_ENEEDMORE;
         break;
      }

      uint32_t index;
      ADSPResult errCode = topo_find_module(topo_ptr, pDataPayload->module_id, &index);
      if (ADSP_SUCCEEDED(errCode))
      {
    	 capi_v2_err_t err_code = CAPI_V2_EOK;
         capi_v2_buf_t buf;
         buf.data_ptr = (int8_t *) (pPacket + sizeof(asm_stream_param_data_v2_t));
         buf.max_data_len = pDataPayload->param_size;
         buf.actual_data_len = pDataPayload->param_size;
         capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
         capi_v2_port_info_t port_info;
         port_info.is_valid = FALSE;

         err_code = module_ptr->vtbl_ptr->set_param(module_ptr, pDataPayload->param_id, &port_info, &buf);
         if (CAPI_V2_FAILED(err_code))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID %lX failed setParam.", topo_ptr->instance_id, topo_ptr->module[index].module_id);
            result = capi_v2_err_to_adsp_result(err_code);
         }
         if (pDataPayload->module_id ==  AUDPROC_MODULE_ID_VOL_CTRL)
         {
             if (pDataPayload->param_id ==  AUDPROC_PARAM_ID_SOFT_VOL_STEPPING_PARAMETERS)
             {
                 topo_set_ramp_period_to_virtualizer(topo_ptr, index);
             }
         }
      }
      else
      {
        if(pDataPayload->module_id != AUDPROC_MODULE_ID_DTS_DOWNMIXER &&
           pDataPayload->module_id != ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC)
        {
           MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Attempt to set param for unsupported module ID 0x%8lx! Ignoring.", topo_ptr->instance_id, pDataPayload->module_id);
        }
      }

      pPacket += (sizeof(asm_stream_param_data_v2_t) + pDataPayload->param_size);
      payloadSize -= (sizeof(asm_stream_param_data_v2_t) + pDataPayload->param_size);

      if (((pDataPayload->param_size % 4) != 0) && (payloadSize > 0))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Param size for set param module ID 0x%lx, param ID %lx is %lu which is not a multiple of 4. Aborting further processing.", topo_ptr->instance_id, pDataPayload->module_id, pDataPayload->param_id, pDataPayload->param_size);
         result = ADSP_EFAILED;
         break;
      }
   }
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Set params End.", topo_ptr->instance_id);
#endif
   return result;
}

/**
 * Get the params for one module in this topology.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] obj_ptr
 *   Pointer to the object structure.
 * @param[in] pPayload
 *   Pointer to the payload of the get params data.
 * @param[in] payloadSize
 *   Size of the payload of the get params data, in bytes.
 * @param[in] module_id
 *   The module id for the module whose paramters are to be
 *   read.
 * @param[in] paramId
 *   The param id for the parameters to be read.
 * @param[out] pParamSize
 *   The amount of data filled in the payload buffer, in bytes.
 *
 * @return ADSPResult
 *   Error code returning status.
 */
ADSPResult topo_get_param (topo_struct *topo_ptr, void *pPayload, const uint16_t payloadSize, const uint32_t module_id, const uint32_t paramId, uint32_t *pParamSize)
{
   ADSPResult result = ADSP_EOK;

   // The PP service calls this function during steady state for the sample slip module. Don't print the message in that case.
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   if(paramId != AUDPROC_PARAM_ID_SAMPLESLIP_SAMPLE_ADJUST)
   {
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Get params Begin.", topo_ptr->instance_id);
   }
#endif

   uint32_t index;
   result = topo_find_module(topo_ptr, module_id, &index);
   if (ADSP_SUCCEEDED(result))
   {
	   capi_v2_err_t err_code;
      capi_v2_buf_t buf;
      buf.data_ptr = (int8_t *) pPayload;
      buf.max_data_len = payloadSize;
      capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
      capi_v2_port_info_t port_info;
      port_info.is_valid = FALSE;

      err_code = module_ptr->vtbl_ptr->get_param(module_ptr, paramId, &port_info, &buf);
      if (CAPI_V2_FAILED(err_code))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID %lX failed getParam.", topo_ptr->instance_id, topo_ptr->module[index].module_id);
      }

      *pParamSize = buf.actual_data_len;

      result = capi_v2_err_to_adsp_result(err_code);
   }
   else
   {
      if(paramId != AUDPROC_PARAM_ID_SAMPLESLIP_SAMPLE_ADJUST)
      {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Attempt to get unsupported module ID 0x%8lx!", topo_ptr->instance_id, module_id);
      }
      result = ADSP_EBADPARAM;
   }

   // The PP service calls this function during steady state for the sample slip module. Don't print the message in that case.
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   if(paramId != AUDPROC_PARAM_ID_SAMPLESLIP_SAMPLE_ADJUST)
   {
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Get params End.", topo_ptr->instance_id);
   }
#endif
   return result;
}

/**
 * Destroys the topology object. After this function is called,
 * the topology object pointer and the vtable pointer become
 * invalid.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] obj_ptr
 *   Pointer to the object structure.
 */
void topo_destroy (topo_struct *topo_ptr)
{
   if (NULL != topo_ptr)
   {
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Destroy Begin.", topo_ptr->instance_id);
#endif


#ifdef AUDPPSVC_TEST
      {
         uint32_t num_modules = topo_graph_num_modules(&topo_ptr->graph);
    	  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"*** MIPS per MODULE: Peak MIPS indicates only MIPS for the iteration with max cycles ***");
    	  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"*** MIPS per MODULE: These numbers might not be gold standard, meant for quick ref only ***");
    	  for (uint32_t i = 0; i < num_modules; i++)
    	  {
    		  module_info* module_info_ptr = &topo_ptr->module[i];
    		  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Module ID: 0x%lx, Average MIPS: %.3f", module_info_ptr->module_id, convert_cycles_to_mips(48000, module_info_ptr->prof_num_samples, module_info_ptr->prof_total_cycles));
    		  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Module ID: 0x%lx, Peak MIPS   : %.3f", module_info_ptr->module_id, module_info_ptr->prof_peak_mips);
    	  }
      }
#endif // AUDPPSVC_TEST

      if(NULL != topo_ptr->module)
      {
         topo_destroy_modules(topo_graph_num_modules(&topo_ptr->graph), topo_ptr->module, &topo_ptr->buf_mgr);
         qurt_elite_memory_free(topo_ptr->module);
      }

      topo_deallocate_stream_structs(topo_ptr);
      topo_graph_deinit(&topo_ptr->graph);
      topo_buf_manager_deinit(&topo_ptr->buf_mgr);

      if (NULL != topo_ptr->eos_buf.pBuf)
      {
         qurt_elite_memory_free(topo_ptr->eos_buf.pBuf);
         topo_ptr->eos_buf.pBuf = NULL;
         topo_ptr->eos_buf.sizeInSamples = 0;
         topo_ptr->eos_buf.offsetInSamples = 0;
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology Destroy End, now freeing Topology.", topo_ptr->instance_id);
      qurt_elite_memory_free(topo_ptr);
   }
}

/**
 * Returns the index of a module in the modules structure based
 * on module ID.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] topo_ptr
 *   Pointer to the topology structure.
 * @param[in] module_id
 *   The module ID of the module to be found.
 * @param[out] index_ptr
 *   The index of the module in the modules array.
 *
 * @return ADSPResult
 *   ADSP_EOK if the module was found.
 *   ADSP_ENOTEXIST if the module was not found.
 */
static ADSPResult topo_find_module(const topo_struct *topo_ptr, const uint32_t module_id, uint32_t *index_ptr)
{
   ADSPResult result = ADSP_ENOTEXIST;
   uint32_t search_id = module_id;

   uint32_t num_modules = topo_graph_num_modules(&topo_ptr->graph);
   for (uint32_t i = 0; i < num_modules; i++)
   {
      bool_t module_found = FALSE;

      module_found = module_found || (search_id == topo_ptr->module[i].module_id);
      module_found = module_found || ((AUDPROC_MODULE_ID_DS1AP == topo_ptr->module[i].module_id) && topo_is_ds1ap_module(search_id));

      if (module_found)
      {
         *index_ptr = i;
         result = ADSP_EOK;
         break;
      }
   }

   return result;
}

static ADSPResult create_delay_buffer(topo_struct* topo_ptr)
{
   uint32_t bytes_per_sample = topo_ptr->in_format.bitsPerSample / 8;

   // Calculate the total delay of all the modules in the chain. This is different from the value in delay_ptr, which also includes
   // the buffering delay of the PP service.
   uint32_t alg_delay = 0;
   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
	   if (topo_ptr->module[i].is_in_chain)
	   {
		   alg_delay += topo_ptr->module[i].delay_in_us;
	   }
   }


   if(0 != alg_delay)
   {
      uint32_t delay_in_samples = topo_convert_us_to_samples(alg_delay, topo_ptr->in_format.samplingRate);
      uint32_t bytes_to_alloc = delay_in_samples * bytes_per_sample * topo_ptr->in_format.channels;

      int8_t* ptr = (int8_t *)qurt_elite_memory_malloc(bytes_to_alloc, QURT_ELITE_HEAP_DEFAULT);
      if (!ptr)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"audproc_svc: Out of Memory while creating delay buffer!");
         return ADSP_ENOMEMORY;
      }
      memset(ptr, 0, bytes_to_alloc);

      topo_ptr->eos_buf.pBuf= ptr;
      topo_ptr->eos_buf.sizeInSamples = delay_in_samples; // Per channel
      topo_ptr->eos_buf.offsetInSamples = 0;
   }
   else
   {
      topo_ptr->eos_buf.pBuf = NULL;
      topo_ptr->eos_buf.sizeInSamples = 0;
      topo_ptr->eos_buf.offsetInSamples = 0;
   }

   return ADSP_EOK;
}

static bool_t topo_is_ds1ap_module(const uint32_t module_id)
{
   const uint32_t ds1ap_module_ids[] = {
             AUDPROC_MODULE_ID_DS1AP,
             AUDPROC_MODULE_ID_DS1AP_BUNDLE,
             AUDPROC_MODULE_ID_DS1AP_VIS,
             AUDPROC_MODULE_ID_DS1AP_NGS,
             AUDPROC_MODULE_ID_DS1AP_DE ,
             AUDPROC_MODULE_ID_DS1AP_DV ,
             AUDPROC_MODULE_ID_DS1AP_IEQ ,
             AUDPROC_MODULE_ID_DS1AP_GEQ ,
             AUDPROC_MODULE_ID_DS1AP_VIRTUALIZER,
             AUDPROC_MODULE_ID_DS1AP_VM ,
             AUDPROC_MODULE_ID_DS1AP_AO ,
             AUDPROC_MODULE_ID_DS1AP_ARE ,
             AUDPROC_MODULE_ID_DS1AP_PLIM
           };

   for(uint32_t i = 0; i < SIZE_OF_ARRAY(ds1ap_module_ids); i++)
   {
      if (module_id == ds1ap_module_ids[i])
      {
         return TRUE;
      }
   }

   return FALSE;
}



#ifdef AUDPPSVC_TEST
static float convert_cycles_to_mips(const uint32_t sample_rate, const uint32_t num_samples, const uint64_t cycles)
{
   const uint32_t MIN_SAMPLE_COUNT = 50; // If the samples are less than this, the calculation may not be accurate
   if (num_samples < MIN_SAMPLE_COUNT)
   {
      return 0;
   }

   float rate = sample_rate;
   float num_seconds = num_samples / rate;
   float cycles_per_second = cycles / num_seconds;
   float mips = cycles_per_second / 1e6;

   return mips;
}
#endif // AUDPPSVC_TEST

static uint32_t topo_convert_us_to_samples(const uint32_t val_in_us, const uint32_t sample_rate)
{
   uint64_t val = val_in_us;
   val *= sample_rate;

   return uint32_t(div_round(val, NUM_US_IN_SEC));
}

static uint64_t div_round(const uint64_t x, const uint32_t y)
{
   return (x + y/2) / y;
}

bool_t topo_is_module_present(topo_struct *topo_ptr, const uint32_t module_id)
{
   uint32_t index;
   ADSPResult result = topo_find_module(topo_ptr, module_id, &index);

   return ADSP_SUCCEEDED(result);
}

bool_t topo_buffer_is_buf_empty(const topo_buf_t *buf_ptr)
{
	return (buf_ptr->valid_data_end == buf_ptr->valid_data_start);
}

bool_t topo_buffer_is_buf_full(const topo_buf_t *buf_ptr)
{
	return (buf_ptr->valid_data_end == buf_ptr->size_per_buffer);
}

static void topo_buffer_convert_from_ext_input_buffer(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr)
{

	 int_buf_ptr->flags = (ext_buf_ptr->flags);
	 int_buf_ptr->timestamp = ext_buf_ptr->timestamp;

	switch (format_ptr->bitstreamFormat)
	{
	case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
	case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
	{
		if (1 == format_ptr->isInterleaved)
		{
			int_buf_ptr->num_bufs = 1;
			int_buf_ptr->buf_ptrs[0] = ext_buf_ptr->pBuf;
			int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
			int_buf_ptr->valid_data_start = ext_buf_ptr->offsetInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
			int_buf_ptr->valid_data_end = int_buf_ptr->size_per_buffer;
			int_buf_ptr->bytes_to_samples_factor = format_ptr->channels * format_ptr->bitsPerSample / 8;
		}
		else
		{
			int_buf_ptr->num_bufs = format_ptr->channels;
			int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->bitsPerSample / 8;
			int_buf_ptr->bytes_to_samples_factor = format_ptr->bitsPerSample / 8;
			int8_t *ptr = ext_buf_ptr->pBuf;
			for (uint32_t i = 0; i < format_ptr->channels; i++)
			{
				int_buf_ptr->buf_ptrs[i] = ptr;
				ptr += int_buf_ptr->size_per_buffer;
			}

			int_buf_ptr->valid_data_start = ext_buf_ptr->offsetInSamples * format_ptr->bitsPerSample / 8;
			int_buf_ptr->valid_data_end = int_buf_ptr->size_per_buffer;
		}
		break;
	}
	default:
	{
		int_buf_ptr->num_bufs = 1;
		int_buf_ptr->buf_ptrs[0] = ext_buf_ptr->pBuf;
		int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
		int_buf_ptr->valid_data_start = ext_buf_ptr->offsetInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
		int_buf_ptr->valid_data_end = int_buf_ptr->size_per_buffer;
		int_buf_ptr->bytes_to_samples_factor = 1;
		break;
	}
	}
}

static void topo_buffer_convert_from_ext_output_buffer(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr)
{
	switch (format_ptr->bitstreamFormat)
	{
	case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
	case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
	{
		if (1 == format_ptr->isInterleaved)
		{
			int_buf_ptr->num_bufs = 1;
			int_buf_ptr->buf_ptrs[0] = ext_buf_ptr->pBuf;
			int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
			int_buf_ptr->valid_data_start = 0;
			int_buf_ptr->valid_data_end = ext_buf_ptr->offsetInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
			int_buf_ptr->bytes_to_samples_factor = format_ptr->channels * format_ptr->bitsPerSample / 8;
		}
		else
		{
			int_buf_ptr->num_bufs = format_ptr->channels;
			int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->bitsPerSample / 8;
			int_buf_ptr->bytes_to_samples_factor = format_ptr->bitsPerSample / 8;
			int8_t *ptr = ext_buf_ptr->pBuf;
			for (uint32_t i = 0; i < format_ptr->channels; i++)
			{
				int_buf_ptr->buf_ptrs[i] = ptr;
				ptr += int_buf_ptr->size_per_buffer;
			}

			int_buf_ptr->valid_data_start = 0;
			int_buf_ptr->valid_data_end = ext_buf_ptr->offsetInSamples * format_ptr->bitsPerSample / 8;
		}
		break;
	}
	default:
	{
		int_buf_ptr->num_bufs = 1;
		int_buf_ptr->buf_ptrs[0] = ext_buf_ptr->pBuf;
		int_buf_ptr->size_per_buffer = ext_buf_ptr->sizeInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
		int_buf_ptr->valid_data_start = 0;
		int_buf_ptr->valid_data_end = ext_buf_ptr->offsetInSamples * format_ptr->channels * format_ptr->bitsPerSample / 8;
		int_buf_ptr->bytes_to_samples_factor = 1;
		break;
	}
	}
}

static bool_t data_is_interleaved(const AdspAudioMediaFormatInfo_t *format_ptr)
{
	if((ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2 == format_ptr->bitstreamFormat) || (ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3 == format_ptr->bitstreamFormat))
	{
		if (1 == format_ptr->isInterleaved)
		{
			return TRUE;
		}
	}

	return FALSE;
}

static void topo_buffer_update_external_input_buf_sizes(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr)
{
	if (topo_buffer_is_buf_empty(int_buf_ptr))
	{
		ext_buf_ptr->offsetInSamples = ext_buf_ptr->sizeInSamples;
	}
	else
	{
		ext_buf_ptr->offsetInSamples = int_buf_ptr->valid_data_start * 8 / format_ptr->bitsPerSample;
		if (data_is_interleaved(format_ptr) || topo_is_compressed(format_ptr->bitstreamFormat))
		{
			ext_buf_ptr->offsetInSamples /= format_ptr->channels;
		}
	}
}
static void topo_buffer_update_external_output_buf_sizes(AudPP_BufInfo_t *ext_buf_ptr, topo_buf_t *int_buf_ptr, const AdspAudioMediaFormatInfo_t *format_ptr)
{
   ext_buf_ptr->flags = int_buf_ptr->flags;
   ext_buf_ptr->timestamp = int_buf_ptr->timestamp;
	ext_buf_ptr->offsetInSamples = int_buf_ptr->valid_data_end * 8 / format_ptr->bitsPerSample;
	if (data_is_interleaved(format_ptr) || topo_is_compressed(format_ptr->bitstreamFormat))
	{
		ext_buf_ptr->offsetInSamples /= format_ptr->channels;
	}
}

topo_buf_t *topo_module_get_input_buffer(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(TOPO_INVALID_MODULE_INDEX != port_index_ptr->module_index);
   QURT_ELITE_ASSERT(topo_port_index_t::INPUT_PORT == port_index_ptr->type);

   topo_port_index_t prev_port = topo_module_prev_port(topo_ptr, port_index_ptr);
   if (TOPO_INVALID_MODULE_INDEX == prev_port.module_index)
	{
		return &topo_ptr->main_input_buffer;
	}

   return topo_module_get_output_buffer(topo_ptr, &prev_port);
}

topo_buf_t *topo_module_get_output_buffer(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(TOPO_INVALID_MODULE_INDEX != port_index_ptr->module_index);
   QURT_ELITE_ASSERT(topo_port_index_t::OUTPUT_PORT == port_index_ptr->type);

   topo_port_index_t next_port = topo_module_next_port(topo_ptr, port_index_ptr);
   if (TOPO_INVALID_MODULE_INDEX == next_port.module_index)
	{
		return &topo_ptr->main_output_buffer;
	}

   return &(topo_out_port_from_index(topo_ptr, port_index_ptr)->topo_buf);
}

static void topo_adsp_media_fmt_to_topo(const AdspAudioMediaFormatInfo_t *adsp_media_fmt_ptr, topo_media_fmt_t *topo_media_fmt_ptr)
{
   topo_media_fmt_ptr->f.bitstream_format    = adsp_media_fmt_ptr->bitstreamFormat;
   topo_media_fmt_ptr->h.data_format         = topo_is_compressed(adsp_media_fmt_ptr->bitstreamFormat) ? CAPI_V2_IEC61937_PACKETIZED : CAPI_V2_FIXED_POINT;
   topo_media_fmt_ptr->f.num_channels        = adsp_media_fmt_ptr->channels;
   topo_media_fmt_ptr->f.bits_per_sample     = adsp_media_fmt_ptr->bitsPerSample;
   topo_media_fmt_ptr->f.q_factor            = topo_calculate_q_factor(adsp_media_fmt_ptr);
   topo_media_fmt_ptr->f.sampling_rate       = adsp_media_fmt_ptr->samplingRate;
   topo_media_fmt_ptr->f.data_is_signed      = adsp_media_fmt_ptr->isSigned;
   topo_media_fmt_ptr->f.data_interleaving = adsp_media_fmt_ptr->isInterleaved ? CAPI_V2_INTERLEAVED : CAPI_V2_DEINTERLEAVED_UNPACKED;
   for (uint32_t i = 0; i < topo_media_fmt_ptr->f.num_channels; i++)
   {
	   topo_media_fmt_ptr->f.channel_type[i] = adsp_media_fmt_ptr->channelMapping[i]; // Conversion from 8 bit to 16 bit here.
   }
}

static bool_t topo_is_compressed(uint32_t bitstream_format)
{
	return ((bitstream_format != ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2) && (bitstream_format != ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3));

}

static uint32_t topo_calculate_q_factor(const AdspAudioMediaFormatInfo_t *media_fmt_ptr)
{
	if (topo_is_compressed(media_fmt_ptr->bitstreamFormat))
	{
		return CAPI_V2_DATA_FORMAT_INVALID_VAL;
	}

	switch(media_fmt_ptr->bitsPerSample)
	{
	case 16:
		return PCM_16BIT_Q_FORMAT;
	case 32:
		return ELITE_32BIT_PCM_Q_FORMAT;
	default:
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: Topo received invalid bits per sample %lu", media_fmt_ptr->bitsPerSample);
		return CAPI_V2_DATA_FORMAT_INVALID_VAL;
	}
}

static void topo_topo_media_fmt_to_adsp(const topo_media_fmt_t *topo_media_fmt_ptr, AdspAudioMediaFormatInfo_t *adsp_media_fmt_ptr)
{
	switch(topo_media_fmt_ptr->h.data_format)
	{
	case CAPI_V2_FIXED_POINT:
		QURT_ELITE_ASSERT(topo_media_fmt_ptr->f.num_channels != CAPI_V2_DATA_FORMAT_INVALID_VAL);
		adsp_media_fmt_ptr->channels = topo_media_fmt_ptr->f.num_channels;

		if (adsp_media_fmt_ptr->channels > SIZE_OF_ARRAY(adsp_media_fmt_ptr->channelMapping))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: %lu channels at topo output, limiting to %d.", adsp_media_fmt_ptr->channels, SIZE_OF_ARRAY(adsp_media_fmt_ptr->channelMapping));
			adsp_media_fmt_ptr->channels = SIZE_OF_ARRAY(adsp_media_fmt_ptr->channelMapping);
		}
		for (uint32_t i = 0; i < topo_media_fmt_ptr->f.num_channels; i++)
		{
			adsp_media_fmt_ptr->channelMapping[i] = topo_media_fmt_ptr->f.channel_type[i]; // Conversion from 16 bit to 8 bit here.
		}

		QURT_ELITE_ASSERT ((topo_media_fmt_ptr->f.bits_per_sample == 16) || (topo_media_fmt_ptr->f.bits_per_sample == 32));
		adsp_media_fmt_ptr->bitsPerSample = topo_media_fmt_ptr->f.bits_per_sample;

		if (topo_media_fmt_ptr->f.sampling_rate != CAPI_V2_DATA_FORMAT_INVALID_VAL)
		{
			adsp_media_fmt_ptr->samplingRate = topo_media_fmt_ptr->f.sampling_rate;
		}
		else
		{
			MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: Invalid sampling rate at topo output, setting to 48000.");
			adsp_media_fmt_ptr->samplingRate = 48000;
		}
		if (topo_media_fmt_ptr->f.data_is_signed != CAPI_V2_DATA_FORMAT_INVALID_VAL)
		{
			adsp_media_fmt_ptr->isSigned = topo_media_fmt_ptr->f.data_is_signed;
		}
		else
		{
			MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: Invalid is_signed at topo output, setting to TRUE.");
			adsp_media_fmt_ptr->isSigned = 1;
		}
		switch(topo_media_fmt_ptr->f.data_interleaving)
		{
		case CAPI_V2_INTERLEAVED:
			adsp_media_fmt_ptr->isInterleaved = 1;
			break;
		case CAPI_V2_DEINTERLEAVED_UNPACKED:
			adsp_media_fmt_ptr->isInterleaved = 0;
			break;
		default:
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: Invalid interleaving at topo output. Only INTERLEAVED(0) and DEINTERLEAVED_UNPACKED(2) supported. Got %lu", static_cast<uint32_t>(topo_media_fmt_ptr->f.data_interleaving));
			adsp_media_fmt_ptr->isInterleaved = 0;
			break;
		}
		if (topo_media_fmt_ptr->f.bitstream_format != CAPI_V2_DATA_FORMAT_INVALID_VAL)
		{
			adsp_media_fmt_ptr->bitstreamFormat = topo_media_fmt_ptr->f.bitstream_format;
		}
		else
		{
			MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "audproc_svc: Invalid bitstream format at topo output, setting to PCM.");
			adsp_media_fmt_ptr->bitstreamFormat = ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3;
		}
		break;
	default:
		QURT_ELITE_ASSERT(CAPI_V2_IEC61937_PACKETIZED == topo_media_fmt_ptr->h.data_format);
		adsp_media_fmt_ptr->bitsPerSample = (CAPI_V2_DATA_FORMAT_INVALID_VAL != topo_media_fmt_ptr->f.bits_per_sample) ? topo_media_fmt_ptr->f.bits_per_sample : 16;
		adsp_media_fmt_ptr->bitstreamFormat = (CAPI_V2_DATA_FORMAT_INVALID_VAL != topo_media_fmt_ptr->f.bitstream_format) ?topo_media_fmt_ptr->f.bitstream_format : ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3;
		adsp_media_fmt_ptr->isInterleaved = 1; // IEC61937 has to be interleaved for the PP service
		adsp_media_fmt_ptr->isSigned = (CAPI_V2_DATA_FORMAT_INVALID_VAL != topo_media_fmt_ptr->f.data_is_signed) ? topo_media_fmt_ptr->f.data_is_signed : 1;
		adsp_media_fmt_ptr->samplingRate = (CAPI_V2_DATA_FORMAT_INVALID_VAL != topo_media_fmt_ptr->f.sampling_rate) ? topo_media_fmt_ptr->f.sampling_rate : 48000;
		if (CAPI_V2_DATA_FORMAT_INVALID_VAL != topo_media_fmt_ptr->f.num_channels)
		{
			adsp_media_fmt_ptr->channels = topo_media_fmt_ptr->f.num_channels;
			if (adsp_media_fmt_ptr->channels > SIZE_OF_ARRAY(adsp_media_fmt_ptr->channelMapping))
			{
				adsp_media_fmt_ptr->channels = SIZE_OF_ARRAY(adsp_media_fmt_ptr->channelMapping);
			}
			for (uint32_t i = 0; i < adsp_media_fmt_ptr->channels; i++)
			{
				adsp_media_fmt_ptr->channelMapping[i] = topo_media_fmt_ptr->f.channel_type[i]; // Conversion from 16 bit to 8 bit here.
			}
		}
		else
		{
			adsp_media_fmt_ptr->channels = 1;
			adsp_media_fmt_ptr->channelMapping[0] = PCM_CHANNEL_C;

		}
		break;
	}
}


static void topo_print_module_output_media_format(uint16_t obj_id, uint32_t module_id, const topo_media_fmt_t *media_fmt_ptr)
{
   MSG_6(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX raised output media format. The new format is"
         "num_channels: %lu, "
         "bits_per_sample: %lu,"
         "sampling_rate: %lu,"
         " bitstream_format: 0x%lx, ",
         obj_id,
         module_id,
         media_fmt_ptr->f.num_channels,
         media_fmt_ptr->f.bits_per_sample,
         media_fmt_ptr->f.sampling_rate ,
         media_fmt_ptr->f.bitstream_format);
}

static void topo_print_media_format(uint16_t obj_id, const topo_media_fmt_t *media_fmt_ptr)
{
	MSG_5(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: reinit done, final media type is as follows:"
	      "num_channels: %lu, "
	      "bits_per_sample: %lu,"
	      "sampling_rate: %lu,"
	      " bitstream_format: 0x%lx, ",
	      obj_id,
	      media_fmt_ptr->f.num_channels,
	      media_fmt_ptr->f.bits_per_sample,
	      media_fmt_ptr->f.sampling_rate ,
	      media_fmt_ptr->f.bitstream_format);

	MSG_7(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Channel mapping for channels 1 to 6: %hu, %hu, %hu, %hu, %hu, %hu", obj_id,
			media_fmt_ptr->f.channel_type[0], media_fmt_ptr->f.channel_type[1], media_fmt_ptr->f.channel_type[2], media_fmt_ptr->f.channel_type[3],
			media_fmt_ptr->f.channel_type[4], media_fmt_ptr->f.channel_type[5]);

	if(media_fmt_ptr->f.num_channels > 6)
	{
	   MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc for channels 7 and 8:: Channel mapping: %hu, %hu", obj_id,
	         media_fmt_ptr->f.channel_type[6], media_fmt_ptr->f.channel_type[7]);
	}
}

static uint32_t topo_buffer_bytes_to_samples(const topo_buf_t *buf_ptr, uint32_t bytes)
{
	return bytes / buf_ptr->bytes_to_samples_factor;
}
static uint32_t topo_buffer_samples_to_bytes(const topo_buf_t *buf_ptr, uint32_t samples)
{
	return samples * buf_ptr->bytes_to_samples_factor;
}

/**
 * Copies the input buffer to the output buffer. To be called
 * when there are no modules in the topology.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] topo_ptr
 *   Pointer to the topology structure.
 */
static void topo_loop_thru_zero_modules(topo_struct *topo_ptr)
{
	topo_buf_t *input_buf, *output_buf;
	input_buf = &topo_ptr->main_input_buffer;
	output_buf = &topo_ptr->main_output_buffer;

	QURT_ELITE_ASSERT(input_buf->bytes_to_samples_factor == output_buf->bytes_to_samples_factor);

	if ((output_buf->valid_data_end == 0) && (topo_ptr->in_format.samplingRate!=0))
	{
	   uint64_t timestamp_offset = ((input_buf->valid_data_start/input_buf->bytes_to_samples_factor) * 1000000);
	   timestamp_offset/=(topo_ptr->in_format.samplingRate);
	   output_buf->flags = input_buf->flags;
	   output_buf->timestamp = input_buf->timestamp + timestamp_offset;
	}

	uint32_t input_data_size = input_buf->valid_data_end - input_buf->valid_data_start;
	uint32_t output_data_size = output_buf->size_per_buffer - output_buf->valid_data_end;
	uint32_t num_bytes_copied = 0;

	QURT_ELITE_ASSERT(input_buf->num_bufs == output_buf->num_bufs);
	for (uint32_t i = 0; i < input_buf->num_bufs; i++)
	{
		num_bytes_copied = memscpy(output_buf->buf_ptrs[i] + output_buf->valid_data_end, output_data_size, input_buf->buf_ptrs[i] + input_buf->valid_data_start, input_data_size);
	}

	input_buf->valid_data_start += num_bytes_copied;
	QURT_ELITE_ASSERT(input_buf->valid_data_start <= input_buf->valid_data_end);
	if (input_buf->valid_data_start == input_buf->valid_data_end)
	{
		input_buf->valid_data_start = input_buf->valid_data_end = 0;
	}
	output_buf->valid_data_end += num_bytes_copied;

	QURT_ELITE_ASSERT(output_buf->valid_data_end <= output_buf->size_per_buffer);
}

static bool_t topo_is_media_format_valid(const topo_media_fmt_t *f, uint16_t instance_id, uint32_t module_id)
{
	QURT_ELITE_ASSERT ((CAPI_V2_FIXED_POINT == f->h.data_format) || (CAPI_V2_IEC61937_PACKETIZED == f->h.data_format));

	if (CAPI_V2_FIXED_POINT == f->h.data_format)
	{
		if ((f->f.bits_per_sample != 16) && (f->f.bits_per_sample != 32))
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu provided bits_per_sample %lu for data format event. Only 16 and 32 are supported.", instance_id, module_id, f->f.bits_per_sample);
			return FALSE;
		}

		if ((f->f.num_channels < 1) || (f->f.num_channels > CAPI_V2_MAX_CHANNELS))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu provided num_channels %lu for data format event. Only values from %d to %d are supported.", instance_id, module_id, f->f.num_channels, 1, CAPI_V2_MAX_CHANNELS);
			return FALSE;
		}
	}

	return TRUE;
}

static capi_v2_err_t topo_event_callback(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_ptr)
{
	topo_struct *topo_ptr = topo_event_get_struct_ptr(context_ptr);
	uint32_t module_index = topo_event_get_module_index(context_ptr);

#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
	MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX raised event id %lu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id));
#endif

	switch(id)
	{
	case CAPI_V2_EVENT_KPPS:
	{
		if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_KPPS_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_KPPS_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}


		uint32_t new_KPPS = reinterpret_cast<capi_v2_event_KPPS_t*>(event_ptr->payload.data_ptr)->KPPS;
		uint32_t old_KPPS = topo_ptr->module[module_index].KPPS;
		if(new_KPPS!=old_KPPS)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX changed KPPS from %lu to %lu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, old_KPPS, new_KPPS);
			topo_ptr->module[module_index].KPPS = new_KPPS;
			topo_update_chain_KPPS(topo_ptr);
		}
		break;
	}
	case CAPI_V2_EVENT_BANDWIDTH:
	{
		if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_bandwidth_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_bandwidth_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}

		capi_v2_event_bandwidth_t *bandwidth_info_ptr = reinterpret_cast<capi_v2_event_bandwidth_t*>(event_ptr->payload.data_ptr);
		uint32_t new_bandwidth = bandwidth_info_ptr->code_bandwidth + bandwidth_info_ptr->data_bandwidth;
		uint32_t old_bandwidth = topo_ptr->module[module_index].bandwidth;
		if(new_bandwidth!=old_bandwidth)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX changed bandwidth from %lu to %lu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, old_bandwidth, new_bandwidth);
			topo_ptr->module[module_index].bandwidth = new_bandwidth;
			topo_update_chain_bandwidth(topo_ptr);
		}
		break;
	}
	case CAPI_V2_EVENT_ALGORITHMIC_DELAY:
	{
		if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_algorithmic_delay_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_algorithmic_delay_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}

		uint32_t new_delay = reinterpret_cast<capi_v2_event_algorithmic_delay_t*>(event_ptr->payload.data_ptr)->delay_in_us;
		uint32_t old_delay = topo_ptr->module[module_index].delay_in_us;
		if(new_delay!=old_delay)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX changed delay from %lu us to %lu us.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, old_delay, new_delay);
			topo_ptr->module[module_index].delay_in_us = new_delay;
			topo_update_chain_delay(topo_ptr);
		}
		break;
	}
	case CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED:
	{
		if(event_ptr->payload.actual_data_len < sizeof(topo_media_fmt_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu provided insufficient size for header for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(topo_media_fmt_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}

		topo_media_fmt_t *format_ptr = reinterpret_cast<topo_media_fmt_t*>(event_ptr->payload.data_ptr);

		if (!event_ptr->port_info.is_valid)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided invalid port for event id %lu. Only output port 0 is supported.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id));
			return CAPI_V2_EUNSUPPORTED;
		}
		if (event_ptr->port_info.is_input_port)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided input port for event id %lu. Only output port 0 is supported.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id));
			return CAPI_V2_EUNSUPPORTED;
		}
      if(event_ptr->port_info.port_index >= topo_ptr->module[module_index].num_output_ports)
		{
         MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided port %lu for event id %lu. It has only %lu output ports.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, event_ptr->port_info.port_index, static_cast<uint32_t>(id), topo_ptr->module[module_index].num_output_ports);
			return CAPI_V2_EUNSUPPORTED;
		}

		if ((CAPI_V2_FIXED_POINT != format_ptr->h.data_format) && (CAPI_V2_IEC61937_PACKETIZED != format_ptr->h.data_format))
		{
			MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided data_format %lu for event id %lu. Only port FIXED_POINT(0) and IEC61937_PACKETIZED(3) are supported.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(format_ptr->h.data_format), static_cast<uint32_t>(id));
			return CAPI_V2_EUNSUPPORTED;
		}

		if (!topo_is_media_format_valid(format_ptr, topo_ptr->instance_id, topo_ptr->module[module_index].module_id))
		{
			return CAPI_V2_EUNSUPPORTED;
		}

		topo_ptr->module[module_index].output_ports_ptr[event_ptr->port_info.port_index].media_event.event_is_pending = TRUE;
		topo_ptr->module[module_index].output_ports_ptr[event_ptr->port_info.port_index].media_event.event = *format_ptr;
		// Media format events raised by the module will be handled during the process
	    topo_print_module_output_media_format(topo_ptr->instance_id, topo_ptr->module[module_index].module_id, format_ptr);
		break;
	}
	case CAPI_V2_EVENT_PROCESS_STATE:
	{
		if(event_ptr->payload.actual_data_len < sizeof(capi_v2_event_process_state_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for header for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_process_state_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}

		capi_v2_event_process_state_t *payload_ptr = reinterpret_cast<capi_v2_event_process_state_t*>(event_ptr->payload.data_ptr);

      if (!payload_ptr->is_enabled)
      {
         if (!topo_module_is_disablement_possible(&topo_ptr->module[module_index]))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Cannot disable modules of type module id 0x%lX.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id);
            return CAPI_V2_EUNSUPPORTED;
         }
      }

		bool_t old_process_state = topo_ptr->module[module_index].is_enabled;
		topo_ptr->module[module_index].is_enabled = payload_ptr->is_enabled;

		MSG_4(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with id 0x%lX changed process state from %hu to %hu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, old_process_state, topo_ptr->module[module_index].is_enabled);
	   if(payload_ptr->is_enabled)
	   {
	      if (!topo_module_list_locked(topo_ptr))
	      {
	         topo_handle_module_enable_events(topo_ptr, module_index);
	      }
	   }
	   // Disable events raised by the module will be handled in the process

	   break;
	}
	case CAPI_V2_EVENT_PORT_DATA_THRESHOLD_CHANGE:
	{
		if(event_ptr->payload.actual_data_len < sizeof(capi_v2_port_data_threshold_change_t))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id %lu provided insufficient size for header for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_port_data_threshold_change_t), event_ptr->payload.actual_data_len);
			return CAPI_V2_ENEEDMORE;
		}

		capi_v2_port_data_threshold_change_t *payload_ptr = reinterpret_cast<capi_v2_port_data_threshold_change_t*>(event_ptr->payload.data_ptr);

		if (payload_ptr->new_threshold_in_bytes != 1)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX Module with ID 0x%lX returned input threshold %lu. Only a threshold of 1 is supported.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, payload_ptr->new_threshold_in_bytes);
			return CAPI_V2_EUNSUPPORTED;
		}
		break;
	}
    case CAPI_V2_EVENT_HEADROOM:
    {
        if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_headroom_t))
        {
            MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_headroom_t), event_ptr->payload.actual_data_len);
            return CAPI_V2_ENEEDMORE;
        }

        uint32_t new_headroom = reinterpret_cast<capi_v2_event_headroom_t*>(event_ptr->payload.data_ptr)->headroom_in_millibels;
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
        uint32_t old_headroom = topo_ptr->module[module_index].headroom;
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Module with id 0x%lX changed headroom "
                    "from %lu to %lu.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id,
                    old_headroom, new_headroom);
#endif
      topo_ptr->module[module_index].headroom = new_headroom;
      topo_update_chain_headroom(topo_ptr);

        break;
    }
    case CAPI_V2_EVENT_GET_LIBRARY_INSTANCE:
    {
        if (event_ptr->payload.actual_data_len < sizeof(capi_v2_event_get_library_instance_t))
        {
            MSG_5(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with id 0x%lX provided insufficient size for event id %lu. Required: %d bytes, provided: %lu bytes.", topo_ptr->instance_id, topo_ptr->module[module_index].module_id, static_cast<uint32_t>(id), sizeof(capi_v2_event_get_library_instance_t), event_ptr->payload.actual_data_len);
            return CAPI_V2_ENEEDMORE;
        }

        capi_v2_event_get_library_instance_t *payload_ptr = reinterpret_cast<capi_v2_event_get_library_instance_t*>(event_ptr->payload.data_ptr);

        return capi_v2_library_factory_get_instance(payload_ptr->id, &payload_ptr->ptr);

        break;
    }
	default:
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Event id %lu raised by module %lu is not supported.", topo_ptr->instance_id, static_cast<uint32_t>(id), topo_ptr->module[module_index].module_id);
      return CAPI_V2_EUNSUPPORTED;
	}

	return CAPI_V2_EOK;
}

static void topo_event_initialize_handle(topo_struct *topo_ptr, uint32_t module_index)
{
   event_handle_t *handle = topo_event_get_handle(topo_ptr, module_index);
   handle->module_index = module_index;
   handle->topo_struct_ptr = topo_ptr;
}

static event_handle_t *topo_event_get_handle(topo_struct *topo_ptr, uint32_t module_index)
{
	return &topo_ptr->module[module_index].event_handle;
}

static uint32_t topo_event_get_module_index(void *handle)
{
	return reinterpret_cast<event_handle_t*>(handle)->module_index;
}

static topo_struct *topo_event_get_struct_ptr(void *handle)
{
	return reinterpret_cast<event_handle_t*>(handle)->topo_struct_ptr;
}

static bool_t topo_zero_modules_enabled(const topo_struct *topo_ptr)
{
   if (TOPO_INVALID_MODULE_INDEX == topo_ptr->first_port.module_index)
	{
      QURT_ELITE_ASSERT(TOPO_INVALID_MODULE_INDEX == topo_ptr->last_port.module_index);
		return TRUE;
	}
	else
	{
      QURT_ELITE_ASSERT(TOPO_INVALID_MODULE_INDEX != topo_ptr->last_port.module_index);
		return FALSE;
	}
}

static bool_t topo_module_list_locked(const topo_struct *topo_ptr)
{
	return topo_ptr->module_list_locked;
}

static bool_t topo_module_pending_reconfig(const topo_struct *topo_ptr, uint32_t start_index)
{
   if ((topo_ptr->module[start_index].is_enabled && !topo_ptr->module[start_index].disabled_till_next_media_event) != topo_ptr->module[start_index].is_in_chain)
	{
		return TRUE;
	}

   if (topo_ptr->module[start_index].is_in_chain)
   {
      for (uint32_t i = 0; i < topo_ptr->module[start_index].num_output_ports; i++)
      {
         if (topo_ptr->module[start_index].output_ports_ptr[i].media_event.event_is_pending)
	{
		return TRUE;
	}
      }
   }
	return FALSE;
}

static void topo_lock_module_list(topo_struct *topo_ptr)
{
	QURT_ELITE_ASSERT(!topo_module_list_locked(topo_ptr));

	topo_ptr->module_list_locked = TRUE;
}

static void topo_unlock_module_list(topo_struct *topo_ptr)
{
	QURT_ELITE_ASSERT(topo_module_list_locked(topo_ptr));

    if (!topo_zero_modules_created(topo_ptr))
    {
        for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
        {
            topo_handle_module_enable_events(topo_ptr, i);
        }
    }

	topo_ptr->module_list_locked = FALSE;
}

static void topo_handle_module_enable_events(topo_struct *topo_ptr, uint32_t module_index)
{
   if (!topo_ptr->data_format_received)
   {
      return;
   }

   if (topo_ptr->currently_handling_events)
   {
      return;
   }

   topo_ptr->currently_handling_events = TRUE;


   module_info *m = topo_ptr->module;
   bool_t module_state_changed = topo_check_update_module_enablement(topo_ptr, module_index);

   if(m[module_index].is_in_chain)
   {
      for (uint32_t j = 0; j < m[module_index].num_output_ports; j++)
      {
         if(module_state_changed)
         {
            m[module_index].output_ports_ptr[j].media_event.event_is_pending = TRUE;
         }
      }
   }
   topo_ptr->currently_handling_events = FALSE;

   topo_update_chain_KPPS(topo_ptr);
   topo_update_chain_bandwidth(topo_ptr);
   topo_update_chain_delay(topo_ptr);
   topo_update_chain_headroom(topo_ptr);
}

static bool_t topo_handle_pending_events(topo_struct *topo_ptr, uint32_t start_module_index)
{
   if (!topo_ptr->data_format_received)
   {
	   return FALSE;
   }

   if (topo_ptr->currently_handling_events)
   {
	   return FALSE;
   }

   topo_ptr->currently_handling_events = TRUE;

   topo_media_event_t output_media_event;
   output_media_event.event_is_pending = FALSE;

	if (TOPO_INVALID_MODULE_INDEX == start_module_index)
	{
   topo_media_fmt_t in_format;
   topo_adsp_media_fmt_to_topo(&topo_ptr->in_format, &in_format);

      topo_port_index_t main_input = topo_graph_get_main_input(&topo_ptr->graph);
      topo_media_event_t *downstream_media_event = (TOPO_INVALID_MODULE_INDEX == main_input.module_index) ? &output_media_event : &topo_ptr->module[main_input.module_index].input_ports_ptr[main_input.port_index].media_event;
      start_module_index = main_input.module_index;
      topo_media_fmt_t new_in_format;
      topo_adsp_media_fmt_to_topo(&topo_ptr->in_format, &new_in_format);
      topo_set_media_event(downstream_media_event, &new_in_format);
	   }

   module_info *m = topo_ptr->module;
	   // The media type must be passed to disabled modules also since some modules get enabled/disabled based on media type.
   for (uint32_t i = start_module_index; i < topo_graph_num_modules(&topo_ptr->graph); i++)
		{
      bool_t propagate_for_disabled_module = topo_module_is_propagate_for_disabled_module(&topo_ptr->module[i]);

      topo_module_set_pending_input_media(&m[i], topo_ptr->instance_id);
      bool_t module_state_changed = topo_check_update_module_process_state(topo_ptr, i);

      if(m[i].is_in_chain)
      {
         for (uint32_t j = 0; j < m[i].num_output_ports; j++)
			{
            if(module_state_changed || m[i].output_ports_ptr[j].media_event.event_is_pending)
			{
               topo_port_index_t current_port = { i, j, topo_port_index_t::OUTPUT_PORT };
               topo_port_index_t connected_port = topo_graph_get_next_module(&topo_ptr->graph, current_port);

               topo_media_event_t *downstream_media_event = (TOPO_INVALID_MODULE_INDEX == connected_port.module_index) ? &output_media_event : &m[connected_port.module_index].input_ports_ptr[connected_port.port_index].media_event;
               topo_set_media_event(downstream_media_event, &m[i].output_ports_ptr[j].media_event.event);
		// Handle enable/disable status
               m[i].output_ports_ptr[j].media_event.event_is_pending = FALSE;
		   }
            }
        }
		else
		{
         QURT_ELITE_ASSERT(topo_module_is_disablement_possible(&m[i]));
         if (module_state_changed || propagate_for_disabled_module)
		{
            topo_port_index_t current_port = { i, 0, topo_port_index_t::OUTPUT_PORT };
            topo_port_index_t connected_port = topo_graph_get_next_module(&topo_ptr->graph, current_port);

            topo_media_event_t *downstream_media_event = (TOPO_INVALID_MODULE_INDEX == connected_port.module_index) ? &output_media_event : &m[connected_port.module_index].input_ports_ptr[connected_port.port_index].media_event;
            topo_set_media_event(downstream_media_event, &m[i].input_ports_ptr[0].media_event.event);
			}
		}
	}

	bool_t output_media_format_changed = FALSE;
	{
	   topo_media_fmt_t out_format;
	   topo_adsp_media_fmt_to_topo(&topo_ptr->out_format, &out_format);
			// Set the media format from the last module.
      if (output_media_event.event_is_pending && !topo_is_equal_media_format(&output_media_event.event, &out_format))
	   {
         topo_topo_media_fmt_to_adsp(&output_media_event.event, &topo_ptr->out_format);
         topo_print_media_format(topo_ptr->instance_id, &output_media_event.event);

	      topo_raise_output_media_type_event(topo_ptr);
	      output_media_format_changed = TRUE;
		}
      output_media_event.event_is_pending = FALSE;
	}

   topo_ptr->currently_handling_events = FALSE;

	topo_update_chain_KPPS(topo_ptr);
	topo_update_chain_bandwidth(topo_ptr);
	topo_update_chain_delay(topo_ptr);
   topo_update_chain_headroom(topo_ptr);

   return output_media_format_changed;
}

static void topo_add_to_module_list(topo_struct *topo_ptr, uint32_t module_index, const topo_port_index_t *prev_port_ptr)
{
   QURT_ELITE_ASSERT(topo_module_is_disablement_possible(&topo_ptr->module[module_index]));
   QURT_ELITE_ASSERT(topo_port_index_t::OUTPUT_PORT == prev_port_ptr->type);

	module_info* m = topo_ptr->module;
   uint32_t prev_module_index = prev_port_ptr->module_index;
   const topo_port_index_t next_port = (TOPO_INVALID_MODULE_INDEX == prev_module_index) ? topo_ptr->first_port : topo_module_next_port(topo_ptr, prev_port_ptr);

   const topo_port_index_t current_in_port = { module_index, 0, topo_port_index_t::INPUT_PORT };
   const topo_port_index_t current_out_port = { module_index, 0, topo_port_index_t::OUTPUT_PORT };

	if (!m[module_index].is_in_chain)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Adding module %lX to the processing chain.", topo_ptr->instance_id, m[module_index].module_id);

      m[module_index].input_ports_ptr[0].connected_port = *prev_port_ptr;
      m[module_index].output_ports_ptr[0].connected_port = next_port;

		if(TOPO_INVALID_MODULE_INDEX == prev_module_index)
		{
         topo_ptr->first_port = current_in_port;
		}
		else
		{
         topo_out_port_from_index(topo_ptr, prev_port_ptr)->connected_port = current_in_port;
		}

      if (TOPO_INVALID_MODULE_INDEX == next_port.module_index)
		{
         topo_ptr->last_port = current_out_port;
		}
		else
		{
         topo_in_port_from_index(topo_ptr, &next_port)->connected_port = current_out_port;
		}

		m[module_index].is_in_chain = TRUE;
	}
	else
	{
		if(TOPO_INVALID_MODULE_INDEX != prev_module_index)
		{
         ASSERT_PORTS_EQUAL(prev_port_ptr, &current_in_port);
		}
      else
      {
         ASSERT_PORTS_EQUAL(&topo_ptr->first_port, &current_in_port);
		}
      ASSERT_PORTS_EQUAL(&m[module_index].input_ports_ptr[0].connected_port, prev_port_ptr);
	}
}

static void topo_remove_from_module_list(topo_struct *topo_ptr, uint32_t module_index)
{
   QURT_ELITE_ASSERT(topo_module_is_disablement_possible(&topo_ptr->module[module_index]));
	module_info* m = topo_ptr->module;

	if(m[module_index].is_in_chain)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Removing module %lX from the processing chain.", topo_ptr->instance_id, m[module_index].module_id);

      const topo_port_index_t current_in_port = { module_index, 0, topo_port_index_t::INPUT_PORT };
      const topo_port_index_t current_out_port = { module_index, 0, topo_port_index_t::OUTPUT_PORT };

      if (topo_ptr->first_port.module_index == module_index)
		{
         topo_ptr->first_port = topo_module_next_port(topo_ptr, &current_out_port);
		}
		else
		{
         topo_port_index_t prev_port = topo_module_prev_port(topo_ptr, &current_in_port);

         m[prev_port.module_index].output_ports_ptr[prev_port.port_index].connected_port = topo_module_next_port(topo_ptr, &current_out_port);
		}

      if (topo_ptr->last_port.module_index == module_index)
		{
         topo_ptr->last_port = topo_module_prev_port(topo_ptr, &current_in_port);
		}
		else
		{
         topo_port_index_t next_port = topo_module_next_port(topo_ptr, &current_out_port);

         m[next_port.module_index].input_ports_ptr[next_port.port_index].connected_port = topo_module_prev_port(topo_ptr, &current_in_port);
		}

		m[module_index].is_in_chain = FALSE;
	}
}

static void topo_module_setup_output_buffers(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   topo_buf_t *out_buf = &topo_ptr->module[port_index_ptr->module_index].output_ports_ptr[port_index_ptr->port_index].topo_buf;

   // Free any previously allocated output buffers
   topo_buf_manager_return_bufs(&topo_ptr->buf_mgr, out_buf);
   topo_buf_manager_clear_all_bufs(&topo_ptr->buf_mgr);
   out_buf->num_bufs = 0;
   uint32_t frame_size_in_samples = 0;
   uint32_t size_per_buffer = 0;
   uint32_t module_out_freq = topo_ptr->module[port_index_ptr->module_index].output_ports_ptr[port_index_ptr->port_index].media_event.event.f.sampling_rate;

   // Set up output buffers
   {
      uint32_t num_bufs_to_alloc = 0;
      topo_media_fmt_t *out_format_ptr = &(topo_out_port_from_index(topo_ptr, port_index_ptr)->media_event.event);

      if ((CAPI_V2_IEC61937_PACKETIZED == out_format_ptr->h.data_format) || (CAPI_V2_RAW_COMPRESSED == out_format_ptr->h.data_format) || (CAPI_V2_INTERLEAVED == out_format_ptr->f.data_interleaving))
      {
         num_bufs_to_alloc = 1;
      }
      else
      {
         QURT_ELITE_ASSERT(out_format_ptr->f.num_channels != CAPI_V2_DATA_FORMAT_INVALID_VAL);
         num_bufs_to_alloc = out_format_ptr->f.num_channels;

      }

      QURT_ELITE_ASSERT (num_bufs_to_alloc <= SIZE_OF_ARRAY(out_buf->buf_ptrs));
      if ((CAPI_V2_IEC61937_PACKETIZED == out_format_ptr->h.data_format) || (CAPI_V2_RAW_COMPRESSED == out_format_ptr->h.data_format))
      {
         out_buf->bytes_to_samples_factor = 1;
         size_per_buffer =  topo_ptr->compress_frame_size * out_buf->bytes_to_samples_factor;
      }
      else
      {
         if (CAPI_V2_INTERLEAVED == out_format_ptr->f.data_interleaving)
         {
            out_buf->bytes_to_samples_factor = out_format_ptr->f.bits_per_sample / 8 * out_format_ptr->f.num_channels;
         }
         else
         {
            out_buf->bytes_to_samples_factor = out_format_ptr->f.bits_per_sample / 8;
         }
         elite_svc_get_frame_size(module_out_freq,&frame_size_in_samples);
         size_per_buffer =  topo_ptr->ouput_buffer_num_unit_frames * frame_size_in_samples * out_buf->bytes_to_samples_factor;
      }

      out_buf->size_per_buffer = size_per_buffer;
      out_buf->valid_data_start = out_buf->valid_data_end = 0;
      out_buf->num_bufs = num_bufs_to_alloc;
   }
}

static bool_t topo_zero_modules_created(const topo_struct *topo_ptr)
{
   return (0 == topo_graph_num_modules(&topo_ptr->graph));
}

static void topo_buf_manager_init(topo_buf_manager *buf_mgr_ptr)
{
	buf_mgr_ptr->current_memory_allocated = buf_mgr_ptr->max_memory_allocated = 0;

	for (uint32_t i = 0; i < SIZE_OF_ARRAY(buf_mgr_ptr->bufs); i++)
	{
		buf_mgr_ptr->bufs[i].present = FALSE;
		buf_mgr_ptr->bufs[i].size = 0;
		buf_mgr_ptr->bufs[i].ptr = NULL;
	}
}

static void topo_buf_manager_deinit(topo_buf_manager *buf_mgr_ptr)
{
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "audproc_svc: topo topo_buf manager max size allocated was: %lu", buf_mgr_ptr->max_memory_allocated);
#endif
	buf_mgr_ptr->max_memory_allocated = 0;

	for (uint32_t i = 0; i < SIZE_OF_ARRAY(buf_mgr_ptr->bufs); i++)
	{
		if (NULL != buf_mgr_ptr->bufs[i].ptr)
		{
			QURT_ELITE_ASSERT(buf_mgr_ptr->bufs[i].present);

			buf_mgr_ptr->current_memory_allocated -= buf_mgr_ptr->bufs[i].size;
			qurt_elite_memory_free(buf_mgr_ptr->bufs[i].ptr);
			buf_mgr_ptr->bufs[i].size = 0;
			buf_mgr_ptr->bufs[i].ptr = NULL;
			buf_mgr_ptr->bufs[i].present = FALSE;
		}
	}

	QURT_ELITE_ASSERT(0 == buf_mgr_ptr->current_memory_allocated);
}

static ADSPResult topo_buf_manager_get_bufs(topo_buf_manager *buf_mgr_ptr, topo_buf_t *buf_ptr)
{
	bool_t allocating_buffers = FALSE;
	uint32_t current_index = 0;
	for (uint32_t i = 0; i < buf_ptr->num_bufs; i++)
	{
		QURT_ELITE_ASSERT(NULL == buf_ptr->buf_ptrs[i]);

		if (!allocating_buffers)
		{
			while(current_index < SIZE_OF_ARRAY(buf_mgr_ptr->bufs))
			{
				if (buf_mgr_ptr->bufs[current_index].present && (buf_ptr->size_per_buffer <= buf_mgr_ptr->bufs[current_index].size))
				{
					buf_ptr->buf_ptrs[i] = buf_mgr_ptr->bufs[current_index].ptr;
					buf_mgr_ptr->bufs[current_index].present = FALSE;
					break;
				}
				else
				{
					current_index++;
				}
			}

			if (current_index >= SIZE_OF_ARRAY(buf_mgr_ptr->bufs))
			{
				allocating_buffers = TRUE;
				topo_buf_manager_clear_all_bufs(buf_mgr_ptr); // This will clear out any smaller buffers that are still present.
				current_index = 0;
			}
		}

		if (allocating_buffers)
		{
			void *ptr = qurt_elite_memory_malloc(buf_ptr->size_per_buffer, QURT_ELITE_HEAP_DEFAULT);
			if (NULL != ptr)
			{
				buf_ptr->buf_ptrs[i] = (int8_t*)ptr;

				// Search for an empty slot
				while(current_index < SIZE_OF_ARRAY(buf_mgr_ptr->bufs))
				{
					if (NULL == buf_mgr_ptr->bufs[current_index].ptr)
					{
						buf_mgr_ptr->bufs[current_index].size = buf_ptr->size_per_buffer;
						buf_mgr_ptr->bufs[current_index].ptr = (int8_t*)ptr;
						buf_mgr_ptr->bufs[current_index].present = FALSE;
						break;
					}
					else
					{
						current_index++;
					}
				}
				// If no empty slot found, don't store this buffer

				buf_mgr_ptr->current_memory_allocated += buf_ptr->size_per_buffer;
				if (buf_mgr_ptr->current_memory_allocated > buf_mgr_ptr->max_memory_allocated)
				{
					buf_mgr_ptr->max_memory_allocated = buf_mgr_ptr->current_memory_allocated;
				}
			}
			else
			{
				// Cleanup
				topo_buf_manager_return_bufs(buf_mgr_ptr, buf_ptr);
				return ADSP_ENOMEMORY;
			}
		}
	}

	return ADSP_EOK;
}

static void topo_buf_manager_return_bufs(topo_buf_manager *buf_mgr_ptr, topo_buf_t *buf_ptr)
{
	for (uint32_t i = 0; i < buf_ptr->num_bufs; i++)
	{
		if (NULL != buf_ptr->buf_ptrs[i])
		{
			for (uint32_t j = 0; j < SIZE_OF_ARRAY(buf_mgr_ptr->bufs); j++)
			{
				if (buf_ptr->buf_ptrs[i] == buf_mgr_ptr->bufs[j].ptr)
				{
					QURT_ELITE_ASSERT(!buf_mgr_ptr->bufs[j].present);
					QURT_ELITE_ASSERT(buf_mgr_ptr->bufs[j].size >= buf_ptr->size_per_buffer);
					buf_mgr_ptr->bufs[j].present = TRUE;
					buf_ptr->buf_ptrs[i] = NULL;
					break;
				}
			}

			if (NULL != buf_ptr->buf_ptrs[i])
			{
				qurt_elite_memory_free(buf_ptr->buf_ptrs[i]);
				buf_ptr->buf_ptrs[i] = NULL;
				buf_mgr_ptr->current_memory_allocated -= buf_ptr->size_per_buffer;
			}
		}
	}
}

static void topo_buf_manager_clear_all_bufs(topo_buf_manager *buf_mgr_ptr)
{
	if (buf_mgr_ptr->current_memory_allocated > 0)
	{
		for (uint32_t i = 0; i < SIZE_OF_ARRAY(buf_mgr_ptr->bufs); i++)
		{
			if ((NULL != buf_mgr_ptr->bufs[i].ptr) && buf_mgr_ptr->bufs[i].present)
			{
				qurt_elite_memory_free(buf_mgr_ptr->bufs[i].ptr);
				buf_mgr_ptr->current_memory_allocated -= buf_mgr_ptr->bufs[i].size;
				buf_mgr_ptr->bufs[i].ptr = NULL;
				buf_mgr_ptr->bufs[i].size = 0;
				buf_mgr_ptr->bufs[i].present = FALSE;
			}
		}
	}
}

bool_t topo_check_if_module_is_enabled (topo_struct *topo_ptr, uint32_t module_id)
{
   bool_t enabled_module_found = false;
   uint32_t num_modules = topo_graph_num_modules(&topo_ptr->graph);

   for (uint32_t i = 0; i < num_modules; i++)
   {
      if (topo_ptr->module[i].module_id == module_id)
      {
         enabled_module_found = topo_ptr->module[i].is_in_chain;
         break;
      }
   }

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: check module (0x%lx) enabled Ended with enabled_module_found %u.",
         topo_ptr->instance_id, module_id, enabled_module_found);

   return enabled_module_found;
}

void topo_compressed_reinit_with_output_sample_size(topo_struct *topo_ptr, uint16_t compress_frame_samples)
{
    if (topo_is_compressed(topo_ptr->in_format.bitstreamFormat))
    {
        uint32_t compress_frame_size = compress_frame_samples * (topo_ptr->in_format.bitsPerSample / 8) * topo_ptr->in_format.channels;
        if (compress_frame_size == topo_ptr->compress_frame_size)
        {
            return;
        }
        topo_ptr->compress_frame_size = compress_frame_size;


        for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
        {
            if (topo_ptr->module[i].is_in_chain)
            {
                for (uint32_t j = 0; j < topo_ptr->module[i].num_output_ports; j++)
                {
                    const topo_port_index_t port_index = {i, j, topo_port_index_t::OUTPUT_PORT};
                    topo_module_setup_output_buffers(topo_ptr, &port_index);
                }
            }
        }
    }
    return ;
}

static void topo_set_headroom(topo_struct *topo_ptr)
{
    ADSPResult errCode;
    uint32_t module_id,param_id,index;
    module_id = AUDPROC_MODULE_ID_VOL_CTRL;
    param_id  = APPI_PARAM_ID_ALGORITHMIC_HEADROOM;
    errCode = topo_find_module(topo_ptr, module_id , &index);
    if (ADSP_SUCCEEDED(errCode))
    {
        capi_v2_buf_t buf;
        buf.data_ptr = (int8_t *) (&topo_ptr->topo_headroom);
        buf.max_data_len = sizeof(topo_ptr->topo_headroom);
        buf.actual_data_len = sizeof(topo_ptr->topo_headroom);
        capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
        capi_v2_port_info_t port_info;
        port_info.is_valid = FALSE;

        errCode = module_ptr->vtbl_ptr->set_param(module_ptr,param_id,&port_info,&buf);
        if (ADSP_FAILED(errCode))
        {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Failed to set Headroom Value");
        }
    }
}

static void topo_set_ramp_period_to_virtualizer(topo_struct *topo_ptr, const uint32_t index)
{
	capi_v2_buf_t buf;
	uint32_t virtualizer_index;
	audproc_soft_step_volume_params_t volume_info;
	ADSPResult errCode;
	capi_v2_port_info_t port_info;

	buf.data_ptr = (int8_t *) (&volume_info);
	buf.max_data_len = sizeof(volume_info);
	buf.actual_data_len = sizeof(volume_info);
	capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
	errCode = module_ptr->vtbl_ptr->get_param(module_ptr, AUDPROC_PARAM_ID_SOFT_VOL_STEPPING_PARAMETERS,&port_info, &buf);
	if (ADSP_SUCCEEDED(errCode))
	{
		errCode = topo_find_module(topo_ptr, AUDPROC_MODULE_ID_VIRTUALIZER, &virtualizer_index);
		if (ADSP_SUCCEEDED(errCode))
		{
			buf.data_ptr = (int8_t *) (&volume_info.period);
			buf.max_data_len = sizeof(uint32_t);
			buf.actual_data_len = sizeof(uint32_t);
			capi_v2_t *module_ptr1 = topo_ptr->module[virtualizer_index].module_ptr;
			errCode = module_ptr1->vtbl_ptr->set_param(module_ptr1, AUDPROC_PARAM_ID_VIRTUALIZER_VOLUME_RAMP,&port_info, &buf);
		}
	}

}

static void topo_set_delay_ptr_virtualizer(topo_struct *topo_ptr)
{
    uint32_t index;
    ADSPResult errCode;
    errCode = topo_find_module(topo_ptr, AUDPROC_MODULE_ID_VIRTUALIZER, &index);

    if (ADSP_SUCCEEDED(errCode))
    {
        capi_v2_buf_t buf;
        capi_v2_port_info_t port_info;
        uint32_t *delay_ptr = &topo_ptr->delay;
        buf.data_ptr = (int8_t *) (delay_ptr);
        buf.max_data_len = sizeof(delay_ptr);
        buf.actual_data_len = sizeof(delay_ptr);
        capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
        errCode = module_ptr->vtbl_ptr->set_param(module_ptr, AUDPROC_PARAM_ID_VIRTUALIZER_DELAY,&port_info,&buf);
    }
}

static void topo_check_empty_last_buffer(topo_struct *topo_ptr)
{
   uint32_t copy_size = 0;

   // associated with this port must be copied into the final output buffer.
   topo_buf_t *port_out_buf_ptr = &topo_ptr->module[topo_ptr->last_port.module_index].output_ports_ptr[topo_ptr->last_port.port_index].topo_buf;

   topo_buf_t *main_out_buf_ptr = &topo_ptr->main_output_buffer;

   if (!topo_buffer_is_buf_empty(port_out_buf_ptr))
   {
      uint32_t output_buf_len = main_out_buf_ptr->size_per_buffer - main_out_buf_ptr->valid_data_end;
      uint32_t port_output_buf_len = port_out_buf_ptr->valid_data_end - port_out_buf_ptr->valid_data_start;

      for (uint32_t ch = 0; ch < port_out_buf_ptr->num_bufs; ch++)
      {
         copy_size = memscpy(&main_out_buf_ptr->buf_ptrs[ch][main_out_buf_ptr->valid_data_end],
               output_buf_len,
               &port_out_buf_ptr->buf_ptrs[ch][port_out_buf_ptr->valid_data_start],
               port_output_buf_len);
      }

      if(main_out_buf_ptr->valid_data_end == 0)
      {
         main_out_buf_ptr->flags =  port_out_buf_ptr->flags;
         module_info *module_struct_ptr = &topo_ptr->module[topo_ptr->last_port.module_index];
         uint64_t timestamp_offset = 0;
         if(main_out_buf_ptr->flags.is_timestamp_valid)
         {
            timestamp_offset = ((port_out_buf_ptr->valid_data_start/port_out_buf_ptr->bytes_to_samples_factor) * 1000000);
            timestamp_offset/=(module_struct_ptr->output_ports_ptr->media_event.event.f.sampling_rate);
            main_out_buf_ptr->timestamp = port_out_buf_ptr->timestamp + timestamp_offset;
         }

      }

      main_out_buf_ptr->valid_data_end += copy_size;
      port_out_buf_ptr->valid_data_start +=copy_size;

      if (copy_size == port_output_buf_len)
      {
         QURT_ELITE_ASSERT(topo_buffer_is_buf_empty(port_out_buf_ptr));
         port_out_buf_ptr->valid_data_start = port_out_buf_ptr->valid_data_end = 0;
         topo_buf_manager_return_bufs(&topo_ptr->buf_mgr, port_out_buf_ptr);
      }
   }
}

static void topo_update_chain_KPPS(topo_struct *topo_ptr)
{
   if (topo_ptr->currently_handling_events)
{
      return;
   }

   uint32_t old_KPPS = topo_ptr->KPPS;
   uint32_t new_KPPS = 0;

   if (topo_ptr->data_format_received)
   {
      new_KPPS = TOPO_KPPS; // Base number
      for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
      {
         if (topo_ptr->module[i].is_in_chain)
      {
            new_KPPS += topo_ptr->module[i].KPPS;
         }
      }
   }

   if (new_KPPS != old_KPPS)
      {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: PP KPPS vote changed from %lu to %lu.", topo_ptr->instance_id, old_KPPS, new_KPPS);
      topo_ptr->KPPS = new_KPPS;
      if (NULL != topo_ptr->event_client)
      {
         topo_ptr->event_client->vtbl->KPPS(topo_ptr->event_client, new_KPPS);
   }
   }
}

static void topo_update_chain_bandwidth(topo_struct *topo_ptr)
{
   if (topo_ptr->currently_handling_events)
   {
      return;
   }

   uint32_t ff_num = TOPO_BW_FUDGE_FACTOR_NUM;
   uint32_t ff_den = TOPO_BW_FUDGE_FACTOR_DEN;

   uint32_t old_bandwidth = topo_ptr->bandwidth;
   uint32_t new_bandwidth = TOPO_BANDWIDTH; // Base number
   uint32_t max_bandwidth = new_bandwidth;

   uint32_t num_mod_present = 0;
   
   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
      if (topo_ptr->module[i].is_in_chain)
      {
    	  uint32_t bw=topo_ptr->module[i].bandwidth;
        new_bandwidth += bw;
        if (bw>max_bandwidth) max_bandwidth=bw;
        
        num_mod_present++;
      }
   }

   //check if any modules contribution is 90% of the new_bandwidth.
   if ( (((uint64_t)max_bandwidth*100)/new_bandwidth) >= TOPO_BW_FUDGE_FACTOR_THRESHOLD)
      {
         ff_num = TOPO_BW_FUDGE_FACTOR_NUM_90_10;
         ff_den = TOPO_BW_FUDGE_FACTOR_DEN_90_10;
   }

   if(num_mod_present <= 1)
   {
      ff_num = 1;
      ff_den = 1;
   }
   
   new_bandwidth = new_bandwidth*ff_num/ff_den;

   if (new_bandwidth != old_bandwidth)
   {
      MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: PP bandwidth vote changed from %lu to %lu. Fudge Factor %lu/%lu",
            topo_ptr->instance_id, old_bandwidth, new_bandwidth,ff_num,ff_den);
      topo_ptr->bandwidth = new_bandwidth;
      if (NULL != topo_ptr->event_client)
      {
         topo_ptr->event_client->vtbl->bandwidth(topo_ptr->event_client, new_bandwidth);
      }
   }
}


static void topo_update_chain_delay(topo_struct *topo_ptr)
{
   if (topo_ptr->currently_handling_events)
{
      return;
   }

   uint32_t old_delay = topo_ptr->delay;
   uint32_t new_delay = 0; // Base number
   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
      if (topo_ptr->module[i].is_in_chain)
   {
         new_delay += topo_ptr->module[i].delay_in_us;
      }
   }

   if (new_delay != old_delay)
      {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: PP algorithmic delay changed from %lu us to %lu us .", topo_ptr->instance_id, old_delay, new_delay);
      topo_ptr->delay = new_delay;
      if (NULL != topo_ptr->event_client)
         {
         topo_ptr->event_client->vtbl->algorithmic_delay(topo_ptr->event_client, new_delay);
      }
   }
}

static void topo_update_chain_headroom(topo_struct *topo_ptr)
{
   if (topo_ptr->currently_handling_events)
   {
      return;
   }

   uint32_t old_headroom = topo_ptr->topo_headroom;
   uint32_t new_headroom = 0; // Base number
   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
      if (topo_ptr->module[i].is_in_chain)
      {
         new_headroom += topo_ptr->module[i].headroom;
      }
   }

   if (new_headroom != old_headroom)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: PP headroom changed from %lu mB to %lu mB .", topo_ptr->instance_id, old_headroom, new_headroom);
      topo_ptr->topo_headroom = new_headroom;
      topo_set_headroom(topo_ptr);
   }
}

static void topo_raise_output_media_type_event(topo_struct *topo_ptr)
{
   if (NULL != topo_ptr->event_client)
    {
      topo_ptr->event_client->vtbl->output_media_format(topo_ptr->event_client, &topo_ptr->out_format);
   }
    }

static bool_t topo_is_equal_media_format(const topo_media_fmt_t *f1, const topo_media_fmt_t *f2)
    {
   bool_t is_equal = TRUE;

   is_equal = is_equal && (f1->h.data_format == f2->h.data_format);
   is_equal = is_equal && (f1->f.bits_per_sample == f2->f.bits_per_sample);
   is_equal = is_equal && (f1->f.bitstream_format == f2->f.bitstream_format);
   is_equal = is_equal && (f1->f.data_interleaving == f2->f.data_interleaving);
   is_equal = is_equal && (f1->f.data_is_signed == f2->f.data_is_signed);
   is_equal = is_equal && (f1->f.num_channels == f2->f.num_channels);
   is_equal = is_equal && (f1->f.q_factor == f2->f.q_factor);
   is_equal = is_equal && (f1->f.sampling_rate == f2->f.sampling_rate);

   if (is_equal)
        {
      if (CAPI_V2_DATA_FORMAT_INVALID_VAL != f1->f.num_channels)
      {
      for (uint32_t i = 0; i < f1->f.num_channels; i++)
        {
         is_equal = is_equal && (f1->f.channel_type[i] == f2->f.channel_type[i]);
    }
}
   }

   return is_equal;
}

/**
 * Function to set the start pause command to the volume modules in the topology, during the pause resume.
 * If the topology has one volume module, then that volume module will be set to ramp down during the pause.
 * If the topology has two volume modules, then volume2 module will be set for the soft ramp down during the pause
 * and volume1 will be set to pause state after the ramp down is complete.
 *
 * @param[in, out]
 * topo_ptr : Pointer to the topology instance structure.
 * softPauseParamPayload : pointer to the soft pause parameters of volume 1 module.
 */

void  topo_set_start_pause(topo_struct *topo_ptr , audproc_soft_pause_params_t *soft_pause_param_payload)
{
   uint32_t index;
   ADSPResult err_code;
   asm_stream_param_data_v2_t param_payload;
   err_code = topo_find_module(topo_ptr, AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2, &index);


   if (ADSP_SUCCEEDED(err_code))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Pause Processing begin, Volume 2 present in topology",topo_ptr->instance_id);

      struct volume2_soft_pause_params_t
      {
         asm_stream_param_data_v2_t param_payload;
         audproc_soft_pause_params_t soft_pause_param;
      } volume2_soft_pause_params;

      audproc_soft_pause_params_t *soft_pause_param = (audproc_soft_pause_params_t *)soft_pause_param_payload;

      volume2_soft_pause_params.param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2;
      volume2_soft_pause_params.param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS;
      volume2_soft_pause_params.param_payload.param_size = sizeof(audproc_soft_pause_params_t);
      volume2_soft_pause_params.param_payload.reserved = 0;
      volume2_soft_pause_params.soft_pause_param = *soft_pause_param;

      topo_set_param(topo_ptr, &volume2_soft_pause_params, sizeof(volume2_soft_pause_params));

      param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2;
      param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_START;
      param_payload.param_size = 0;
      param_payload.reserved = 0;

      topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));
   }
   else
   {
	  param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
	  param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_START;
	  param_payload.param_size = 0;
	  param_payload.reserved = 0;

      topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));
   }
}

/**
 * Function to change the volume1 module state to pause after the pause ramp is done in the
 * two volume topology. The function also resets all the modules in the topology to flush out the
 * internal buffers of the modules in the topology.
 *
 * @param[in, out]
 * topo_ptr : Pointer to the topology instance structure.
 */

void topo_set_volume1_state_to_pause(topo_struct *topo_ptr)
{
   uint32_t index;
   ADSPResult err_code;
   uint32_t   us_actual_param_size = 0;
   asm_stream_param_data_v2_t param_payload;
   audproc_soft_pause_params_t soft_pause_param_payload;

   err_code = topo_find_module(topo_ptr, AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2, &index);

   if (ADSP_SUCCEEDED(err_code))
   {
      err_code = topo_get_param(topo_ptr,
                               &soft_pause_param_payload,
                               sizeof(audproc_soft_pause_params_t), /* ParamMaxSize */
                               AUDPROC_MODULE_ID_VOL_CTRL, /* ModuleId: SoftPause is done via SoftVolumeControls */
                               AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS, /* ParamId */
                               &us_actual_param_size);


      if (soft_pause_param_payload.enable_flag)
      {
         struct volume_soft_pause_params_t
         {
            asm_stream_param_data_v2_t param_payload;
            audproc_soft_pause_params_t soft_pause_param;
         } volume_soft_pause_params;

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Pause ramp done, change Volume 1 to Pause state and then reset modules",topo_ptr->instance_id);

         volume_soft_pause_params.param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
         volume_soft_pause_params.param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS;
         volume_soft_pause_params.param_payload.param_size = sizeof(audproc_soft_pause_params_t);
         volume_soft_pause_params.param_payload.reserved = 0;
         volume_soft_pause_params.soft_pause_param = soft_pause_param_payload;
         volume_soft_pause_params.soft_pause_param.enable_flag = 0; // disable soft pause. set pause immediately

         topo_set_param(topo_ptr, &volume_soft_pause_params, sizeof(volume_soft_pause_params));

         param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
         param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_START;
         param_payload.param_size = 0;
         param_payload.reserved = 0;

         topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));

         volume_soft_pause_params.param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
         volume_soft_pause_params.param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS;
         volume_soft_pause_params.param_payload.param_size = sizeof(audproc_soft_pause_params_t);
         volume_soft_pause_params.param_payload.reserved = 0;
         volume_soft_pause_params.soft_pause_param = soft_pause_param_payload;

         topo_set_param(topo_ptr, &volume_soft_pause_params, sizeof(volume_soft_pause_params));

         topo_reset (topo_ptr);
      }
    }
}

/**
 * Function to set the ramp on resume for the volumes module in the topology, during the pause resume.
 * If the topology has one volume module, then that volume module will be set to ramp up during the resume.
 * If the topology has two volume modules, then volume1 module will be set for the soft ramp up and volume2
 * will ramped up immediately.
 *
 * @param[in, out]
 * topo_ptr : Pointer to the topology instance structure.
 *
 */

void  topo_set_ramp_on_resume(topo_struct *topo_ptr)
{
   uint32_t index;
   ADSPResult err_code;
   uint32_t   us_actual_param_size = 0;
   asm_stream_param_data_v2_t param_payload;
   audproc_soft_pause_params_t soft_pause_param_payload;

   err_code = topo_find_module(topo_ptr, AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2, &index);

   if (ADSP_SUCCEEDED(err_code))
   {

	   err_code = topo_get_param(topo_ptr,
                               &soft_pause_param_payload,
                               sizeof(audproc_soft_pause_params_t), /* ParamMaxSize */
                               AUDPROC_MODULE_ID_VOL_CTRL, /* ModuleId: SoftPause is done via SoftVolumeControls */
                               AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS, /* ParamId */
                               &us_actual_param_size);


      if (soft_pause_param_payload.enable_flag)
      {
         struct volume_soft_pause_params_t
         {
            asm_stream_param_data_v2_t param_payload;
            audproc_soft_pause_params_t soft_pause_param;
         } volume_soft_pause_params;

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Set Ramp on Resume to volume 2 with zero period/Set Soft Ramp on Resume to volume 1 ",topo_ptr->instance_id);

         volume_soft_pause_params.param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2;
         volume_soft_pause_params.param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS;
         volume_soft_pause_params.param_payload.param_size = sizeof(audproc_soft_pause_params_t);
         volume_soft_pause_params.param_payload.reserved = 0;
         volume_soft_pause_params.soft_pause_param = soft_pause_param_payload;
         volume_soft_pause_params.soft_pause_param.enable_flag = 0; // disable soft pause. set pause immediately

         topo_set_param(topo_ptr, &volume_soft_pause_params, sizeof(volume_soft_pause_params));
      }

      //setup volume control lib to ramp up after resume
      param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL_INSTANCE_2;
      param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_SET_RAMP_ON_RESUME;
      param_payload.param_size = 0;
      param_payload.reserved = 0;

      topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));

      param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
      param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_SET_RAMP_ON_RESUME;
      param_payload.param_size = 0;
      param_payload.reserved = 0;

      topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));
   }
   else
   {
	  param_payload.module_id = AUDPROC_MODULE_ID_VOL_CTRL;
	  param_payload.param_id = AUDPROC_PARAM_ID_SOFT_PAUSE_SET_RAMP_ON_RESUME;
	  param_payload.param_size = 0;
	  param_payload.reserved = 0;

      topo_set_param(topo_ptr, &param_payload, sizeof(asm_stream_param_data_v2_t));
   }
}

ADSPResult topo_set_output_media_format (topo_struct *topo_ptr, uint32_t module_id , const AdspAudioMediaFormatInfo_t *output_media_format, uint32_t port_index)
{
#ifdef PP_TOPO_ENABLE_LOW_LEVEL_MSG
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Set Module Output Media Format", topo_ptr->instance_id);
#endif

    ADSPResult result;
    capi_v2_err_t err_code;
    uint32_t index = 0;
    result = topo_find_module(topo_ptr, module_id, &index);
    if(result!=ADSP_EOK)
    {
        MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Set output media format, Module with ID %lX  is not there in the Topology", topo_ptr->instance_id,module_id);
        return result;
    }

    topo_media_fmt_t media_format;

    media_format.f.bitstream_format = output_media_format->bitstreamFormat;
    media_format.f.num_channels = output_media_format->channels;
    media_format.f.bits_per_sample = output_media_format->bitsPerSample;
    media_format.f.sampling_rate = output_media_format->samplingRate;
    media_format.f.data_is_signed = output_media_format->isSigned;

    if(output_media_format->isInterleaved)
    	media_format.f.data_interleaving = CAPI_V2_INTERLEAVED;
    else
    	media_format.f.data_interleaving = CAPI_V2_DEINTERLEAVED_UNPACKED;

	switch(output_media_format->bitsPerSample)
	{
	case 16:
		media_format.f.q_factor = PCM_16BIT_Q_FORMAT;
		break;
	case 32:
		media_format.f.q_factor = ELITE_32BIT_PCM_Q_FORMAT;
		break;
	default:
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Unsupported bits per sample: %lu. Only 16 and 32 bits supported.", topo_ptr->instance_id, output_media_format->bitsPerSample);
		return ADSP_EUNSUPPORTED;
	}

    uint8_t max_num_channels = 8;
    for (uint8_t ch = 0 ; ch < max_num_channels ; ch++)
    {
        media_format.f.channel_type[ch] = (uint16_t)output_media_format->channelMapping[ch];
    }

    capi_v2_prop_t prop = {
        CAPI_V2_OUTPUT_MEDIA_FORMAT,
        {
            reinterpret_cast<int8_t*>(&media_format),
            sizeof(media_format),
            sizeof(media_format)
        },
        {
            TRUE,
            FALSE,
               port_index
        }
    };
    capi_v2_proplist_t proplist = { 1, &prop };
    capi_v2_t *module_ptr = topo_ptr->module[index].module_ptr;
    err_code = module_ptr->vtbl_ptr->set_properties(module_ptr, &proplist);
    if (CAPI_V2_FAILED(err_code))
    {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Module with ID %lX failed to set property for output media format.", topo_ptr->instance_id, module_id);
        return capi_v2_err_to_adsp_result(err_code);
    }
    return ADSP_EOK;
}

static ADSPResult topo_module_allocate_ports(module_info *module_ptr)
{
   // Initialize everything to NULL
   module_ptr->input_ports_ptr = NULL;
   module_ptr->output_ports_ptr = NULL;

   // Calculate memory requirement
   uint32_t inport_mem_size = align_to_8_byte(sizeof(module_input_port_t) * module_ptr->num_input_ports);
   uint32_t outport_mem_size = align_to_8_byte(sizeof(module_output_port_t) * module_ptr->num_output_ports);
   uint32_t total_mem = inport_mem_size + outport_mem_size;
   QURT_ELITE_ASSERT(total_mem > 0);

   // Allocate memory
   module_ptr->port_mem = qurt_elite_memory_malloc(total_mem, QURT_ELITE_HEAP_DEFAULT);
   if (NULL == module_ptr->port_mem)
   {
      return ADSP_ENOMEMORY;
   }

   // Divide up the memory
   uint8_t *mem_ptr = (uint8_t*)module_ptr->port_mem;
   if (0 == module_ptr->num_input_ports)
   {
      module_ptr->input_ports_ptr = NULL;
   }
   else
   {
      module_ptr->input_ports_ptr = reinterpret_cast<module_input_port_t*>(mem_ptr);
      mem_ptr += inport_mem_size;
   }

   if (0 == module_ptr->num_output_ports)
   {
      module_ptr->output_ports_ptr = NULL;
   }
   else
   {
      module_ptr->output_ports_ptr = reinterpret_cast<module_output_port_t*>(mem_ptr);
      mem_ptr += outport_mem_size;
   }

   // Initialize all the variables
   for (uint32_t i = 0; i < module_ptr->num_input_ports; i++)
   {
      topo_module_input_port_initialize(&module_ptr->input_ports_ptr[i]);
   }

   for (uint32_t i = 0; i < module_ptr->num_output_ports; i++)
   {
      topo_module_output_port_initialize(&module_ptr->output_ports_ptr[i]);
   }

   return ADSP_EOK;
}

static void topo_module_free_ports(module_info *module_ptr, topo_buf_manager *buf_mgr_ptr)
{
   if (NULL != module_ptr->output_ports_ptr)
   {
      for (uint32_t i = 0; i < module_ptr->num_output_ports; i++)
      {
         if (NULL != module_ptr->output_ports_ptr->topo_buf.buf_ptrs[0])
         {
            topo_buf_manager_return_bufs(buf_mgr_ptr, &module_ptr->output_ports_ptr->topo_buf);
         }
      }
   }
   if (NULL != module_ptr->port_mem)
   {
      qurt_elite_memory_free(module_ptr->port_mem);
   }
}

static void topo_graph_init(topo_graph_t *topo_graph_ptr)
{
   topo_graph_ptr->num_modules = 0;
   topo_graph_ptr->connections_array = NULL;
   topo_graph_ptr->nodes = NULL;
}

static ADSPResult topo_graph_set_graph(topo_graph_t *topo_graph_ptr, const topo_def_t *topo_def_ptr)
{
   topo_graph_free_graph(topo_graph_ptr);

   if (topo_def_ptr->num_modules > 0)
   {
      topo_graph_ptr->num_modules = topo_def_ptr->num_modules;

      const uint8_t NUM_INPUT_PORTS_PER_MODULE = 1;
      const uint8_t NUM_OUTPUT_PORTS_PER_MODULE = 1;

      uint32_t connections_array_size = topo_def_ptr->num_modules * (NUM_INPUT_PORTS_PER_MODULE + NUM_OUTPUT_PORTS_PER_MODULE);

      topo_graph_ptr->connections_array = (topo_graph_t::topo_graph_port_t*)qurt_elite_memory_malloc(connections_array_size * sizeof(*(topo_graph_ptr->connections_array)), QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_graph_ptr->connections_array)
      {
         topo_graph_ptr->num_modules = 0;
         return ADSP_ENOMEMORY;
      }

      topo_graph_ptr->nodes = (topo_graph_t::topo_graph_node_t*)qurt_elite_memory_malloc(topo_def_ptr->num_modules * sizeof(*(topo_graph_ptr->nodes)), QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_graph_ptr->nodes)
      {
         qurt_elite_memory_free(topo_graph_ptr->connections_array);
         topo_graph_ptr->connections_array = NULL;
         topo_graph_ptr->num_modules = 0;
         return ADSP_ENOMEMORY;
      }

      {
         uint32_t current_connections_index = 0;
         for (uint32_t i = 0; i < topo_def_ptr->num_modules; i++)
         {
            topo_graph_t::topo_graph_node_t *n = &topo_graph_ptr->nodes[i];

            n->module_id = topo_def_ptr->module_id_list[i];
            n->num_inputs = NUM_INPUT_PORTS_PER_MODULE;
            n->inputs_array = &topo_graph_ptr->connections_array[current_connections_index];
            current_connections_index += n->num_inputs;
            n->inputs_array[0].module_index = (i > 0) ? (i - 1) : GRAPH_MAIN_INPUT_PORT_INDEX;
            n->inputs_array[0].port_index = 0;

            n->num_outputs = NUM_OUTPUT_PORTS_PER_MODULE;
            n->outputs_array = &topo_graph_ptr->connections_array[current_connections_index];
            current_connections_index += n->num_outputs;
            n->outputs_array[0].module_index = ((i + 1) < topo_def_ptr->num_modules) ? (i + 1) : GRAPH_MAIN_OUTPUT_PORT_INDEX;
            n->outputs_array[0].port_index = 0;
         }
      }
   }

   return ADSP_EOK;
}

ADSPResult topo_get_module_list(const topo_struct *topo_obj, uint32_t* topo_module_list_ptr, uint32_t payload_size)
{
   const topo_graph_t *topo_graph_ptr = &(topo_obj->graph);

   uint32_t req_payload_size = sizeof(int32_t) + (sizeof(int32_t) * topo_graph_num_modules(topo_graph_ptr));

   if(payload_size < req_payload_size)
   {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Insufficient size error in topo_get_module_list.", topo_obj->instance_id);
       return ADSP_ENOMEMORY;
   }
   else
   {
      memset(topo_module_list_ptr, 0, payload_size);

      if(topo_graph_ptr->nodes != NULL)
      {
         *topo_module_list_ptr = topo_graph_num_modules(topo_graph_ptr);
         topo_module_list_ptr++;

         for (uint32_t i = 0; i < topo_graph_num_modules(topo_graph_ptr); i++)
         {
             const topo_graph_t::topo_graph_node_t *n = &topo_graph_ptr->nodes[i];
             *topo_module_list_ptr =  n->module_id;
             topo_module_list_ptr++;
         }
      }
   }

   return ADSP_EOK;
}

static ADSPResult common_topo_graph_set_graph(topo_graph_t *topo_graph_ptr, topo_def_t *topo_def_ptr)
{
	topo_graph_free_graph(topo_graph_ptr);

	if (topo_def_ptr->num_modules > 0)
	{
		topo_graph_ptr->num_modules = topo_def_ptr->num_modules;

		const uint8_t NUM_INPUT_PORTS_PER_MODULE = 1;
		const uint8_t NUM_OUTPUT_PORTS_PER_MODULE = 1;

		uint32_t connections_array_size = topo_def_ptr->num_modules * (NUM_INPUT_PORTS_PER_MODULE + NUM_OUTPUT_PORTS_PER_MODULE);

		topo_graph_ptr->connections_array = (topo_graph_t::topo_graph_port_t*)qurt_elite_memory_malloc(connections_array_size * sizeof(*(topo_graph_ptr->connections_array)), QURT_ELITE_HEAP_DEFAULT);
		if (NULL == topo_graph_ptr->connections_array)
		{
			topo_graph_ptr->num_modules = 0;
			return ADSP_ENOMEMORY;
		}

		topo_graph_ptr->nodes = (topo_graph_t::topo_graph_node_t*)qurt_elite_memory_malloc(topo_def_ptr->num_modules * sizeof(*(topo_graph_ptr->nodes)), QURT_ELITE_HEAP_DEFAULT);
		if (NULL == topo_graph_ptr->nodes)
		{
			qurt_elite_memory_free(topo_graph_ptr->connections_array);
			topo_graph_ptr->connections_array = NULL;
			topo_graph_ptr->num_modules = 0;
			return ADSP_ENOMEMORY;
		}

		{
			uint32_t current_connections_index = 0;
			for (uint32_t i = 0; i < topo_def_ptr->num_modules; i++)
			{
				topo_graph_t::topo_graph_node_t *n = &topo_graph_ptr->nodes[i];

				n->module_id = topo_def_ptr->module_id_list[i];
				n->num_inputs = NUM_INPUT_PORTS_PER_MODULE;
				n->inputs_array = &topo_graph_ptr->connections_array[current_connections_index];
				current_connections_index += n->num_inputs;
				n->inputs_array[0].module_index = (i > 0) ? (i - 1) : GRAPH_MAIN_INPUT_PORT_INDEX;
				n->inputs_array[0].port_index = 0;

				n->num_outputs = NUM_OUTPUT_PORTS_PER_MODULE;
				n->outputs_array = &topo_graph_ptr->connections_array[current_connections_index];
				current_connections_index += n->num_outputs;
				n->outputs_array[0].module_index = ((i + 1) < topo_def_ptr->num_modules) ? (i + 1) : GRAPH_MAIN_OUTPUT_PORT_INDEX;
				n->outputs_array[0].port_index = 0;
			}
		}
	}

	return ADSP_EOK;
}

static void topo_graph_free_graph(topo_graph_t *topo_graph_ptr)
{
   if (NULL != topo_graph_ptr->connections_array)
   {
      qurt_elite_memory_free(topo_graph_ptr->connections_array);
   }
   if (NULL != topo_graph_ptr->nodes)
   {
      qurt_elite_memory_free(topo_graph_ptr->nodes);
   }
   topo_graph_ptr->num_modules = 0;
}

static void topo_graph_deinit(topo_graph_t *topo_graph_ptr)
{
   topo_graph_free_graph(topo_graph_ptr);
}

static topo_port_index_t topo_graph_get_prev_module(topo_graph_t *topo_graph_ptr, topo_port_index_t current_index)
{
   QURT_ELITE_ASSERT(topo_port_index_t::INPUT_PORT == current_index.type);
   QURT_ELITE_ASSERT(current_index.module_index < topo_graph_num_modules(topo_graph_ptr));
   QURT_ELITE_ASSERT(current_index.port_index < topo_graph_ptr->nodes[current_index.module_index].num_inputs);

   topo_port_index_t prev_index;
   prev_index.module_index = topo_graph_ptr->nodes[current_index.module_index].inputs_array[current_index.port_index].module_index;
   if (GRAPH_MAIN_INPUT_PORT_INDEX == prev_index.module_index)
   {
      prev_index.module_index = TOPO_INVALID_MODULE_INDEX;
   }

   prev_index.port_index = topo_graph_ptr->nodes[current_index.module_index].inputs_array[current_index.port_index].port_index;
   prev_index.type = topo_port_index_t::OUTPUT_PORT;

   return prev_index;
}
static topo_port_index_t topo_graph_get_next_module(topo_graph_t *topo_graph_ptr, topo_port_index_t current_index)
{
   QURT_ELITE_ASSERT(topo_port_index_t::OUTPUT_PORT == current_index.type);
   QURT_ELITE_ASSERT(current_index.module_index < topo_graph_num_modules(topo_graph_ptr));
   QURT_ELITE_ASSERT(current_index.port_index < topo_graph_ptr->nodes[current_index.module_index].num_outputs);

   topo_port_index_t next_index;
   next_index.module_index = topo_graph_ptr->nodes[current_index.module_index].outputs_array[current_index.port_index].module_index;
   if (GRAPH_MAIN_OUTPUT_PORT_INDEX == next_index.module_index)
   {
      next_index.module_index = TOPO_INVALID_MODULE_INDEX;
   }

   next_index.port_index = topo_graph_ptr->nodes[current_index.module_index].outputs_array[current_index.port_index].port_index;
   next_index.type = topo_port_index_t::INPUT_PORT;

   return next_index;
}

static topo_port_index_t topo_graph_get_main_input(topo_graph_t *topo_graph_ptr)
{
   topo_port_index_t main_input;
   main_input.module_index = TOPO_INVALID_MODULE_INDEX;
   main_input.port_index = 0;
   main_input.type = topo_port_index_t::INPUT_PORT;

   for (uint32_t i = 0; i < topo_graph_num_modules(topo_graph_ptr); i++)
   {
      for (uint32_t j = 0; j < topo_graph_ptr->nodes[i].num_inputs; j++)
      {
         if (GRAPH_MAIN_INPUT_PORT_INDEX == topo_graph_ptr->nodes[i].inputs_array[j].module_index)
         {
            main_input.module_index = i;
            main_input.port_index = j;
            return main_input;
         }
      }
   }

   QURT_ELITE_ASSERT(0 == topo_graph_num_modules(topo_graph_ptr));

   return main_input;
}

static topo_port_index_t topo_graph_get_main_output(topo_graph_t *topo_graph_ptr)
{
   topo_port_index_t main_output;
   main_output.module_index = TOPO_INVALID_MODULE_INDEX;
   main_output.port_index = 0;
   main_output.type = topo_port_index_t::OUTPUT_PORT;

   for (uint32_t i = 0; i < topo_graph_num_modules(topo_graph_ptr); i++)
   {
      for (uint32_t j = 0; j < topo_graph_ptr->nodes[i].num_outputs; j++)
      {
         if (GRAPH_MAIN_OUTPUT_PORT_INDEX == topo_graph_ptr->nodes[i].outputs_array[j].module_index)
         {
            main_output.module_index = i;
            main_output.port_index = j;
            return main_output;
         }
      }
   }

   QURT_ELITE_ASSERT(0 == topo_graph_num_modules(topo_graph_ptr));

   return main_output;
}

static uint32_t topo_graph_num_modules(const topo_graph_t *topo_graph_ptr)
{
   return topo_graph_ptr->num_modules;
   }


static void topo_graph_print_linear_topo(const topo_graph_t *topo_graph_ptr, uint16_t instance_id)
{
   uint32_t num_module = topo_graph_num_modules(topo_graph_ptr);
   uint32_t module_list[6];
   uint32_t array_size = sizeof(module_list)/sizeof(uint32_t);
   uint32_t j = array_size;

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: The topology is as follows, Number of modules: %lu", instance_id, num_module);

   if(num_module >= array_size)
   {
      for (j = array_size; j < num_module; j+=array_size)
      {
         for (uint32_t i = 0; i < array_size; i++)
         {
            module_list[i] = topo_graph_ptr->nodes[(j - array_size) + i].module_id;
         }

         MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology modules ids "
               "0x%lx --> 0x%lx --> "
               "0x%lx --> 0x%lx --> "
               "0x%lx --> 0x%lx --> ",
               instance_id,
               module_list[0], module_list[1],
               module_list[2], module_list[3],
               module_list[4], module_list[5]);
      }
   }

   uint32_t num_modules_remaining = num_module % array_size;

   if (num_modules_remaining == 0)
      return;

   for (uint32_t i = 0; i < num_modules_remaining; i++)
   {
      module_list[i] = topo_graph_ptr->nodes[(j - array_size) + i].module_id;
   }
   for (uint32_t i = num_modules_remaining; i < array_size; i++)
   {
      module_list[i] = 0;
   }
   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Topology modules ids "
         "0x%lx --> 0x%lx --> "
         "0x%lx --> 0x%lx --> "
         "0x%lx --> 0x%lx --> ",
         instance_id,
         module_list[0], module_list[1],
         module_list[2], module_list[3],
         module_list[4], module_list[5]);

}
#ifdef TST_TOPO_LAYER
static void topo_graph_print_graph(const topo_graph_t *topo_graph_ptr, uint16_t instance_id)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph:The topology graph is as follows:", instance_id);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph:Number of modules: %lu", instance_id, topo_graph_num_modules(topo_graph_ptr));

   for (uint32_t i = 0; i < topo_graph_num_modules(topo_graph_ptr); i++)
   {
      MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: Module %lu: id 0x%lx num_inputs %hhu num_outputs %hhu.", instance_id, i, topo_graph_ptr->nodes[i].module_id, topo_graph_ptr->nodes[i].num_inputs, topo_graph_ptr->nodes[i].num_outputs);
      for (uint32_t j = 0; j < topo_graph_ptr->nodes[i].num_inputs; j++)
      {
         uint8_t connected_module_index = topo_graph_ptr->nodes[i].inputs_array[j].module_index;

         if (GRAPH_MAIN_INPUT_PORT_INDEX == connected_module_index)
         {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: module 0x%lx port %lu <-- main input port", instance_id, topo_graph_ptr->nodes[i].module_id, j);
         }
         else
         {
            MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: module 0x%lx port %lu <-- module 0x%lx port %hhu ", instance_id, topo_graph_ptr->nodes[i].module_id, j, topo_graph_ptr->nodes[connected_module_index].module_id, topo_graph_ptr->nodes[i].inputs_array[j].port_index);
         }
   }
      for (uint32_t j = 0; j < topo_graph_ptr->nodes[i].num_outputs; j++)
      {
         uint8_t connected_module_index = topo_graph_ptr->nodes[i].outputs_array[j].module_index;

         if (GRAPH_MAIN_OUTPUT_PORT_INDEX == connected_module_index)
   {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: module 0x%lx port %lu --> main output port", instance_id, topo_graph_ptr->nodes[i].module_id, j);
   }
   else
   {
            MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Graph: module 0x%lx port %lu --> module 0x%lx port %hhu ", instance_id, topo_graph_ptr->nodes[i].module_id, j, topo_graph_ptr->nodes[connected_module_index].module_id, topo_graph_ptr->nodes[i].outputs_array[j].port_index);
         }
      }
   }
}
#endif
static capi_v2_port_num_info_t topo_graph_get_port_num_info(const topo_graph_t *topo_graph_ptr, uint32_t module_index)
      {
   QURT_ELITE_ASSERT(module_index < topo_graph_num_modules(topo_graph_ptr));
   capi_v2_port_num_info_t port_num_info;
   port_num_info.num_input_ports = topo_graph_ptr->nodes[module_index].num_inputs;
   port_num_info.num_output_ports = topo_graph_ptr->nodes[module_index].num_outputs;
   return port_num_info;
}

static void topo_buffer_initialize(topo_buf_t *buf_ptr)
         {
   memset(buf_ptr, 0, sizeof(*buf_ptr));
   buf_ptr->bytes_to_samples_factor = 1;
}

static bool_t topo_module_can_process(topo_struct *topo_ptr, uint32_t module_index)
{
   if (!topo_ptr->module[module_index].is_in_chain)
   {
      return FALSE;
   }

   module_info *m = &topo_ptr->module[module_index];
   for (uint32_t i = 0; i < m->num_input_ports; i++)
   {
      const topo_port_index_t current_in_port = { module_index, i, topo_port_index_t::INPUT_PORT };
      if(topo_buffer_is_buf_empty(topo_module_get_input_buffer(topo_ptr, &current_in_port)))
      {
         return FALSE;
      }
         }

   // We only need to check input ports. Since we go from the end of the chain to the beginning, the output ports will always be empty.
   for (uint32_t i = 0; i < m->num_output_ports; i++)
   {
      const topo_port_index_t current_out_port = { module_index, i, topo_port_index_t::OUTPUT_PORT };

      QURT_ELITE_ASSERT(!topo_buffer_is_buf_full(topo_module_get_output_buffer(topo_ptr, &current_out_port)));
   }

   return TRUE;
}

static bool_t topo_module_is_propagate_for_disabled_module(const module_info *module_ptr)
{
   return topo_module_is_disablement_possible(module_ptr) && module_ptr->input_ports_ptr[0].media_event.event_is_pending;
      }

static bool_t topo_module_is_disablement_possible(const module_info *module_ptr)
{
   return ((1 == module_ptr->num_input_ports) && (1 == module_ptr->num_output_ports));
   }

static void topo_module_set_pending_input_media(module_info *module_ptr, uint16_t instance_id)
{
   const uint32_t TYPICAL_MAX_INPUT_PORTS = 2;
   capi_v2_prop_t props[TYPICAL_MAX_INPUT_PORTS];
   uint32_t current_prop_index = 0;

   capi_v2_err_t err_code = CAPI_V2_EOK;
   bool_t media_format_changed = FALSE;

   for (uint32_t i = 0; i < module_ptr->num_input_ports; i++)
   {
      if (module_ptr->input_ports_ptr[i].media_event.event_is_pending)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Setting input media format for module %lX, port %lu.", instance_id, module_ptr->module_id, i);
         media_format_changed = TRUE;

         topo_media_fmt_t *new_format_ptr = &module_ptr->input_ports_ptr[i].media_event.event;

         props[current_prop_index].id = CAPI_V2_INPUT_MEDIA_FORMAT;
         props[current_prop_index].port_info.is_input_port = TRUE;
         props[current_prop_index].port_info.is_valid = TRUE;
         props[current_prop_index].port_info.port_index = i;
         props[current_prop_index].payload.actual_data_len = sizeof(*new_format_ptr);
         props[current_prop_index].payload.max_data_len = sizeof(*new_format_ptr);
         props[current_prop_index].payload.data_ptr = reinterpret_cast<int8_t*>(new_format_ptr);

         module_ptr->input_ports_ptr[i].media_event.event_is_pending = FALSE;

         current_prop_index++;

         if (TYPICAL_MAX_INPUT_PORTS == current_prop_index)
         {
            capi_v2_proplist_t proplist = { current_prop_index, props };

            CAPI_V2_SET_ERROR(err_code, module_ptr->module_ptr->vtbl_ptr->set_properties(module_ptr->module_ptr, &proplist));
            current_prop_index = 0;
         }
      }
   }

   if (current_prop_index > 0)
   {
      capi_v2_proplist_t proplist = { current_prop_index, props };

      CAPI_V2_SET_ERROR(err_code, module_ptr->module_ptr->vtbl_ptr->set_properties(module_ptr->module_ptr, &proplist));
   }

   // Change the disabled_till_next_media_format flag only if some media format changed.
   if (media_format_changed)
   {
      if (CAPI_V2_FAILED(err_code))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO, "P%hX audproc_svc: Module with ID %lX failed to accept the current media type. Disabling it.", instance_id, module_ptr->module_id);
         module_ptr->disabled_till_next_media_event = TRUE;
      }
      else
      {
         module_ptr->disabled_till_next_media_event = FALSE;
      }
   }
}


static bool_t topo_check_update_module_enablement(topo_struct *topo_ptr, uint32_t module_index)
{
   bool_t module_state_changed = FALSE;
   module_info *module_ptr = &topo_ptr->module[module_index];
   // Handle enable status
   if(!module_ptr->is_in_chain)
   {
      if (module_ptr->is_enabled && (!module_ptr->disabled_till_next_media_event))
      {
         // Find the first module before this module in the graph that is enabled.
         topo_port_index_t prev_port_index = { TOPO_INVALID_MODULE_INDEX, 0, topo_port_index_t::OUTPUT_PORT };
         {
            topo_port_index_t current_port_index = { module_index, 0, topo_port_index_t::INPUT_PORT };
            for (prev_port_index = topo_graph_get_prev_module(&topo_ptr->graph, current_port_index);
                  prev_port_index.module_index != TOPO_INVALID_MODULE_INDEX;
                  prev_port_index = topo_graph_get_prev_module(&topo_ptr->graph, current_port_index))
            {
               if (topo_ptr->module[prev_port_index.module_index].is_in_chain)
               {
                  break;
               }
               else
               {
                  current_port_index.module_index = prev_port_index.module_index;
               }
            }
         }

         topo_add_to_module_list(topo_ptr, module_index, &prev_port_index);
         module_state_changed = TRUE;
      }
   }

   // Set up the output buffers if needed.
   if (module_ptr->is_in_chain)
   {
   for (uint32_t i = 0; i < module_ptr->num_output_ports; i++)
   {
         if(module_ptr->output_ports_ptr[i].media_event.event_is_pending)
         {
            const topo_port_index_t current_port_index = {module_index, i, topo_port_index_t::OUTPUT_PORT };
            topo_module_setup_output_buffers(topo_ptr, &current_port_index);
         }
      }
   }

   return module_state_changed;
}

static bool_t topo_check_update_module_process_state(topo_struct *topo_ptr, uint32_t module_index)
{
   bool_t module_state_changed = FALSE;
   module_info *module_ptr = &topo_ptr->module[module_index];
   // Handle enable/disable status
   if(module_ptr->is_in_chain)
   {
      if (!module_ptr->is_enabled || module_ptr->disabled_till_next_media_event) // TODO: Save this condition in a variable.
      {
         topo_remove_from_module_list(topo_ptr, module_index);
         topo_buf_manager_clear_all_bufs(&topo_ptr->buf_mgr);
         module_state_changed = TRUE;
      }
   }
   else
   {
      if (!(!module_ptr->is_enabled || module_ptr->disabled_till_next_media_event))
      {
         // Find the first module before this module in the graph that is enabled.
         topo_port_index_t prev_port_index = { TOPO_INVALID_MODULE_INDEX, 0, topo_port_index_t::OUTPUT_PORT };
         {
            topo_port_index_t current_port_index = { module_index, 0, topo_port_index_t::INPUT_PORT };
            for (prev_port_index = topo_graph_get_prev_module(&topo_ptr->graph, current_port_index);
                  prev_port_index.module_index != TOPO_INVALID_MODULE_INDEX;
                  prev_port_index = topo_graph_get_prev_module(&topo_ptr->graph, current_port_index))
            {
               if (topo_ptr->module[prev_port_index.module_index].is_in_chain)
               {
                  break;
               }
               else
               {
                  current_port_index.module_index = prev_port_index.module_index;
               }
            }
         }

         topo_add_to_module_list(topo_ptr, module_index, &prev_port_index);
         module_state_changed = TRUE;
      }
   }

   // Set up the output buffers if needed.
   if (module_ptr->is_in_chain)
   {
   for (uint32_t i = 0; i < module_ptr->num_output_ports; i++)
   {
         if(module_ptr->output_ports_ptr[i].media_event.event_is_pending)
         {
            const topo_port_index_t current_port_index = {module_index, i, topo_port_index_t::OUTPUT_PORT };
            topo_module_setup_output_buffers(topo_ptr, &current_port_index);
         }
      }
   }

   return module_state_changed;
}

static void topo_module_input_port_initialize(module_input_port_t *port_ptr)
{
   port_ptr->connected_port.module_index = TOPO_INVALID_MODULE_INDEX;
   port_ptr->connected_port.port_index = 0;
   port_ptr->connected_port.module_index = topo_port_index_t::OUTPUT_PORT;
   port_ptr->media_event.event_is_pending = FALSE;
   port_ptr->media_event.event = invalid_media_format;
}

static void topo_module_output_port_initialize(module_output_port_t *port_ptr)
{
   port_ptr->connected_port.module_index = TOPO_INVALID_MODULE_INDEX;
   port_ptr->connected_port.port_index = 0;
   port_ptr->connected_port.module_index = topo_port_index_t::INPUT_PORT;
   port_ptr->media_event.event_is_pending = FALSE;
   port_ptr->media_event.event = invalid_media_format;
   topo_buffer_initialize(&port_ptr->topo_buf);
}

static module_input_port_t *topo_in_port_from_index(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(topo_port_index_t::INPUT_PORT == port_index_ptr->type);
   QURT_ELITE_ASSERT(topo_graph_num_modules(&topo_ptr->graph) > port_index_ptr->module_index);
   QURT_ELITE_ASSERT(topo_ptr->module[port_index_ptr->module_index].num_input_ports > port_index_ptr->port_index);

   return &topo_ptr->module[port_index_ptr->module_index].input_ports_ptr[port_index_ptr->port_index];
}

static module_output_port_t *topo_out_port_from_index(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(topo_port_index_t::OUTPUT_PORT == port_index_ptr->type);
   QURT_ELITE_ASSERT(topo_graph_num_modules(&topo_ptr->graph) > port_index_ptr->module_index);
   QURT_ELITE_ASSERT(topo_ptr->module[port_index_ptr->module_index].num_output_ports > port_index_ptr->port_index);

   return &topo_ptr->module[port_index_ptr->module_index].output_ports_ptr[port_index_ptr->port_index];
}

static topo_port_index_t topo_module_next_port(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(topo_port_index_t::OUTPUT_PORT == port_index_ptr->type);

   if (TOPO_INVALID_MODULE_INDEX == port_index_ptr->module_index)
   {
      return topo_ptr->first_port;
   }
   else
      {
      QURT_ELITE_ASSERT(port_index_ptr->module_index < topo_graph_num_modules(&topo_ptr->graph));
      QURT_ELITE_ASSERT(port_index_ptr->port_index < topo_ptr->module[port_index_ptr->module_index].num_output_ports);
      return topo_ptr->module[port_index_ptr->module_index].output_ports_ptr[port_index_ptr->port_index].connected_port;
      }
   }

static topo_port_index_t topo_module_prev_port(topo_struct *topo_ptr, const topo_port_index_t *port_index_ptr)
{
   QURT_ELITE_ASSERT(topo_port_index_t::INPUT_PORT == port_index_ptr->type);

   if (TOPO_INVALID_MODULE_INDEX == port_index_ptr->module_index)
   {
      return topo_ptr->last_port;
}
   else
{
      QURT_ELITE_ASSERT(port_index_ptr->module_index < topo_graph_num_modules(&topo_ptr->graph));
      QURT_ELITE_ASSERT(port_index_ptr->port_index < topo_ptr->module[port_index_ptr->module_index].num_input_ports);
      return topo_ptr->module[port_index_ptr->module_index].input_ports_ptr[port_index_ptr->port_index].connected_port;
   }
}

static void topo_destroy_modules(uint32_t num_modules, module_info modules[], topo_buf_manager *buf_mgr_ptr)
   {
   for (uint32_t i = 0; i < num_modules; i++)
      {
      topo_module_destroy(&modules[i], buf_mgr_ptr);
      }
   }

static void topo_module_destroy(module_info *module_struct_ptr, topo_buf_manager *buf_mgr_ptr)
{
   capi_v2_t *module_ptr = module_struct_ptr->module_ptr;

   (void) module_ptr->vtbl_ptr->end(module_ptr);

   qurt_elite_memory_free(module_struct_ptr->module_ptr);

   module_struct_ptr->module_ptr = NULL;
   module_struct_ptr->module_id = 0;
   topo_module_free_ports(module_struct_ptr, buf_mgr_ptr);
}

static void topo_setup_connections(topo_struct *topo_ptr)
{
   // This function sets up all the connections from scratch. This should be done only at init time, before the media format is received.
   QURT_ELITE_ASSERT(!topo_ptr->data_format_received);

   for (uint32_t i = 0; i < topo_graph_num_modules(&topo_ptr->graph); i++)
   {
      module_info *m = &topo_ptr->module[i];

      for (uint32_t j = 0; j < m->num_input_ports; j++)
      {
         topo_port_index_t current_index = { i, j, topo_port_index_t::INPUT_PORT };
         m->input_ports_ptr[j].connected_port = topo_graph_get_prev_module(&topo_ptr->graph, current_index);
      }

      for (uint32_t j = 0; j < m->num_output_ports; j++)
      {
         topo_port_index_t current_index = { i, j, topo_port_index_t::OUTPUT_PORT };
         m->output_ports_ptr[j].connected_port = topo_graph_get_next_module(&topo_ptr->graph, current_index);
      }

      m->is_in_chain = TRUE;
   }

   topo_ptr->first_port = topo_graph_get_main_input(&topo_ptr->graph);
   topo_ptr->last_port = topo_graph_get_main_output(&topo_ptr->graph);
}

#ifdef TST_TOPO_LAYER

static ADSPResult topo_graph_set_graph_dag(topo_graph_t *topo_graph_ptr, const topo_tst_graph_helper *topo_def_ptr)
{
   topo_graph_free_graph(topo_graph_ptr);

   if (topo_def_ptr->num_modules() > 0)
   {
      topo_graph_ptr->num_modules = topo_def_ptr->num_modules();

      uint32_t connections_array_size = topo_def_ptr->num_modules() * topo_def_ptr->get_total_ports();

      topo_graph_ptr->connections_array = (topo_graph_t::topo_graph_port_t*)qurt_elite_memory_malloc(connections_array_size * sizeof(*(topo_graph_ptr->connections_array)), QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_graph_ptr->connections_array)
      {
         topo_graph_ptr->num_modules = 0;
         return ADSP_ENOMEMORY;
      }

      topo_graph_ptr->nodes = (topo_graph_t::topo_graph_node_t*)qurt_elite_memory_malloc(topo_def_ptr->num_modules() * sizeof(*(topo_graph_ptr->nodes)), QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_graph_ptr->nodes)
      {
         qurt_elite_memory_free(topo_graph_ptr->connections_array);
         topo_graph_ptr->connections_array = NULL;
         topo_graph_ptr->num_modules = 0;
         return ADSP_ENOMEMORY;
      }

      {
         uint32_t current_connections_index = 0;
         for (uint32_t i = 0; i < topo_def_ptr->num_modules(); i++)
         {
            topo_graph_t::topo_graph_node_t *n = &topo_graph_ptr->nodes[i];

            n->module_id = topo_def_ptr->get_module_info(i).module_id;
            n->num_inputs = topo_def_ptr->get_module_info(i).inputs.size();
            n->inputs_array = &topo_graph_ptr->connections_array[current_connections_index];
            current_connections_index += n->num_inputs;
            for (uint32_t j = 0; j < n->num_inputs; j++)
            {
               n->inputs_array[j].module_index = topo_def_ptr->get_module_info(i).inputs.at(j).module_index;
               n->inputs_array[j].port_index = topo_def_ptr->get_module_info(i).inputs.at(j).port_index;
            }

            n->num_outputs = topo_def_ptr->get_module_info(i).outputs.size();
            n->outputs_array = &topo_graph_ptr->connections_array[current_connections_index];
            current_connections_index += n->num_outputs;
            for (uint32_t j = 0; j < n->num_outputs; j++)
            {
               n->outputs_array[j].module_index = topo_def_ptr->get_module_info(i).outputs.at(j).module_index;
               n->outputs_array[j].port_index = topo_def_ptr->get_module_info(i).outputs.at(j).port_index;
            }
         }
      }
   }

   return ADSP_EOK;
}

ADSPResult topo_init_dag(const topo_tst_graph_helper &def,
      const uint16_t instance_id,
      const uint32_t max_output_samples,
      topo_struct **topo_obj,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      topo_event_client_t *event_client_ptr,
      bool_t initialize_with_compressed_format)
{
   ADSPResult result = ADSP_EOK;
   topo_struct *topo_ptr = NULL;
   uint32_t num_modules = 0;
   uint32_t num_created_modules = 0;

   *topo_obj = NULL;

   if (NULL == stack_size_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Stack size pointer is NULL.", instance_id);
      result = ADSP_EBADPARAM;
      goto cleanup;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology begin", instance_id);

   result = topo_create_obj(instance_id, &topo_ptr, max_output_samples, event_client_ptr);
   if (ADSP_FAILED(result))
   {
      goto cleanup;
   }

   result = topo_graph_set_graph_dag(&topo_ptr->graph, &def);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the graph representation.", topo_ptr->instance_id);
      goto cleanup;
   }

   topo_graph_print_graph(&topo_ptr->graph, instance_id);

   result = topo_allocate_stream_structs(topo_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to create the stream structures.", topo_ptr->instance_id);
      goto cleanup;
   }

   *stack_size_ptr = AUDPROC_TOPO_DEFAULT_STACK_SIZE;

   num_modules = topo_graph_num_modules(&topo_ptr->graph);

   if(0 != num_modules)
   {
      topo_ptr->module = (module_info*)qurt_elite_memory_malloc(sizeof(module_info) * num_modules, QURT_ELITE_HEAP_DEFAULT);
      if (NULL == topo_ptr->module)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to allocate modules array", topo_ptr->instance_id);
         topo_destroy(topo_ptr);
         return ADSP_ENOMEMORY;
      }

      topo_lock_module_list(topo_ptr);

      static const uint32_t MAX_HANDLES_IN_ONE_CALL = 16;
      uint32_t first_module_index = 0;

      while (first_module_index < num_modules)
      {
         adsp_amdb_module_handle_info_t handle_info[MAX_HANDLES_IN_ONE_CALL];
         uint32_t num_modules_filled = 0;

         while ((num_modules_filled < MAX_HANDLES_IN_ONE_CALL) && ((first_module_index + num_modules_filled) < num_modules))
         {
            handle_info[num_modules_filled].interface_type = CAPI_V2;
            handle_info[num_modules_filled].type = AMDB_MODULE_TYPE_GENERIC;
            handle_info[num_modules_filled].id1 = def.get_module_info(first_module_index + num_modules_filled).module_id;
            handle_info[num_modules_filled].id2 = 0;
            handle_info[num_modules_filled].h.capi_v2_handle = NULL;
            handle_info[num_modules_filled].result = ADSP_EFAILED;

            num_modules_filled++;
         }

         adsp_amdb_get_modules_request(handle_info, num_modules_filled, NULL, NULL);

         /* Create the modules */
         for (uint32_t j = 0; j < num_modules_filled; j++ )
         {
            audproc_module_info_t m_info;
            m_info.module_id = def.get_module_info(first_module_index + j).module_id;
            m_info.use_lpm = FALSE;
            memset(m_info.init_params, 0, sizeof(m_info.init_params));

            result = topo_init_single_module(topo_ptr, first_module_index + j, &m_info, &handle_info[j], pp_type, stack_size_ptr, initialize_with_compressed_format);
            if (ADSP_FAILED(result))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Module with ID 0x%lX failed in init.", topo_ptr->instance_id, def.get_module_info(first_module_index + j).module_id);
               break;
            }
            num_created_modules++;
         }

         // No need to keep the handles now since the modules are created.
         adsp_amdb_release_handles(handle_info, num_modules_filled);

         first_module_index += num_modules_filled;

         if (ADSP_FAILED(result))
         {
            goto cleanup;
         }
      }

      topo_set_delay_ptr_virtualizer(topo_ptr);
      topo_unlock_module_list(topo_ptr);
   }

   *topo_obj = topo_ptr;
   topo_update_chain_KPPS(topo_ptr);
   topo_update_chain_bandwidth(topo_ptr);
   topo_update_chain_delay(topo_ptr);
   topo_update_chain_headroom(topo_ptr);

   cleanup:

   if (ADSP_FAILED(result))
   {
      if (NULL != topo_ptr)
      {
         if (NULL != topo_ptr->module)
         {
            topo_destroy_modules(num_created_modules, topo_ptr->module, &topo_ptr->buf_mgr);
            qurt_elite_memory_free(topo_ptr->module);
            topo_ptr->module = NULL;
         }
         topo_destroy(topo_ptr);
         topo_ptr = NULL;
      }
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Initializing topology End", instance_id);

   return result;
}
#endif

static ADSPResult topo_allocate_stream_structs(topo_struct *topo_ptr)
{
   QURT_ELITE_ASSERT(NULL == topo_ptr->stream_struct_mem);
   QURT_ELITE_ASSERT(0 == topo_ptr->max_in_streams);
   QURT_ELITE_ASSERT(NULL == topo_ptr->in_streams);
   QURT_ELITE_ASSERT(NULL == topo_ptr->in_stream_ptrs);


   topo_ptr->max_in_streams = 0;
   topo_ptr->max_out_streams = 0;
   uint32_t num_modules = topo_graph_num_modules(&topo_ptr->graph);
   for (uint32_t i = 0; i < num_modules; i++ )
   {
      capi_v2_port_num_info_t port_info = topo_graph_get_port_num_info(&topo_ptr->graph, i);
      topo_ptr->max_in_streams = (topo_ptr->max_in_streams < port_info.num_input_ports) ? port_info.num_input_ports : topo_ptr->max_in_streams;
      topo_ptr->max_out_streams = (topo_ptr->max_out_streams < port_info.num_output_ports) ? port_info.num_output_ports : topo_ptr->max_out_streams;
   }

   // Allocate memory for all the stream structures
   uint32_t num_ports = topo_ptr->max_in_streams + topo_ptr->max_out_streams;
   uint32_t stream_struct_size = align_to_8_byte(sizeof(capi_v2_stream_data_t) * num_ports);
   uint32_t stream_struct_ptrs_size = align_to_8_byte(sizeof(capi_v2_stream_data_t*) * num_ports);
   uint32_t bufs_size = align_to_8_byte(sizeof(capi_v2_buf_t) * num_ports * CAPI_V2_MAX_CHANNELS);
   uint32_t total_size = stream_struct_size + stream_struct_ptrs_size + bufs_size;
   if (0 == total_size)
      {
      return ADSP_EOK;
      }
   topo_ptr->stream_struct_mem = qurt_elite_memory_malloc(total_size, QURT_ELITE_HEAP_DEFAULT);
   if (NULL == topo_ptr->stream_struct_mem)
   {
      return ADSP_ENOMEMORY;
   }

   // Divide up the memory
   uint8_t *ptr = reinterpret_cast<uint8_t*>(topo_ptr->stream_struct_mem);
   topo_ptr->in_streams = reinterpret_cast<capi_v2_stream_data_t*>(ptr);
   topo_ptr->out_streams = &topo_ptr->in_streams[topo_ptr->max_in_streams];
   ptr += stream_struct_size;
   topo_ptr->in_stream_ptrs = reinterpret_cast<capi_v2_stream_data_t**>(ptr);
   topo_ptr->out_stream_ptrs = &topo_ptr->in_stream_ptrs[topo_ptr->max_in_streams];
   ptr += stream_struct_ptrs_size;
   for (uint32_t i = 0; i < topo_ptr->max_in_streams; i++)
   {
      topo_ptr->in_stream_ptrs[i] = &topo_ptr->in_streams[i];
      topo_ptr->in_streams[i] = init_stream_data;
      topo_ptr->in_streams[i].buf_ptr = reinterpret_cast<capi_v2_buf_t*>(ptr);
      for (uint32_t j = 0; j < CAPI_V2_MAX_CHANNELS; j++)
      {
         topo_ptr->in_streams[i].buf_ptr[j] = empty_buf;
      }
      ptr += sizeof(capi_v2_buf_t) * CAPI_V2_MAX_CHANNELS;
}
   for (uint32_t i = 0; i < topo_ptr->max_out_streams; i++)
{
      topo_ptr->out_stream_ptrs[i] = &topo_ptr->out_streams[i];
      topo_ptr->out_streams[i] = init_stream_data;
      topo_ptr->out_streams[i].buf_ptr = reinterpret_cast<capi_v2_buf_t*>(ptr);
      for (uint32_t j = 0; j < CAPI_V2_MAX_CHANNELS; j++)
      {
         topo_ptr->out_streams[i].buf_ptr[j] = empty_buf;
      }
      ptr += sizeof(capi_v2_buf_t) * CAPI_V2_MAX_CHANNELS;
   }

   return ADSP_EOK;
}

static void topo_deallocate_stream_structs(topo_struct *topo_ptr)
   {
   if (NULL != topo_ptr->stream_struct_mem)
      {
      qurt_elite_memory_free(topo_ptr->stream_struct_mem);
   }
   topo_ptr->stream_struct_mem = NULL;
   topo_ptr->max_in_streams = 0;
   topo_ptr->in_streams = NULL;
   topo_ptr->in_stream_ptrs = NULL;
   topo_ptr->max_out_streams = 0;
   topo_ptr->out_streams = NULL;
   topo_ptr->out_stream_ptrs = NULL;
}

static void topo_set_media_event(topo_media_event_t *downstream_event_ptr, const topo_media_fmt_t *format_to_be_set)
{
   downstream_event_ptr->event_is_pending = !topo_is_equal_media_format(&downstream_event_ptr->event, format_to_be_set);
   if (downstream_event_ptr->event_is_pending)
   {
      downstream_event_ptr->event = *format_to_be_set;
   }
}
