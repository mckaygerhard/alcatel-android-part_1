/**
@file MixerSvc_OutPortHandler.cpp
@brief This file defines functions that handle the arrival of a
       buffer at an output port of the audio matrix mixer.

 */

/*========================================================================
Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_OutPortHandler.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/04/2010 AAA     Created file.
========================================================================== */

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"
#include "adsp_media_fmt.h"
#include "adsp_adm_api.h"
#include "adsp_asm_session_commands.h"
#include "MixerSvc_OutPortHandler.h"
#include "MixerSvc_InPortHandler.h"
#include "MixerSvc_Util.h"
#include "avsync_lib.h"
#include "adsp_private_api_ext.h"
#include "MixerSvc_ChanMixUtils.h"
#include "MixerSvc_LatencyModuleUtils.h"

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */
static void MxAr_OutPortToCheckForUnderflowEvents(This_t *me, uint32_t unOutPortID);
static void MxAr_CommonOutPortProcessingRoutine(This_t *me, uint32_t unOutPortID);
static bool_t MxAr_OutPortToCheckIfItIsOkToProceed(This_t *me, uint32_t unOutPortID);
static void MxArUpdateInputPortSessionTime(This_t *me,
		uint32_t unInPortID,
		MatrixInPortInfoType *pCurrentInPort,
		MatrixOutPortInfoType *pCurrentOutPort,
		uint32_t ullCurrentDevicePathDelay);

/* =======================================================================
Function Definitions
========================================================================== */

void MxAr_ProcessBufQ(This_t *me, uint32_t unOutPortID)
{
	elite_msg_any_t            *pPeerDataQMsg;
	qurt_elite_bufmgr_node_t   outBufMgrNode;
	int16_t                    *ptr_data_buf_16;
	int32_t                    *ptr_acc_buf_32, *ptr_data_buf_32;
	uint32_t                   i;
	ADSPResult                 result;
	MatrixOutPortInfoType      *pCurrentOutPort;

	MtMx_ClearOutputPortChannelStatus(me, unOutPortID);

	pCurrentOutPort = me->outPortParams[unOutPortID];

	//Output ports are always in PULL mode
	//Check if this o/p port is a secondary port and if any accumulation has happened on this port.
	//If yes, proceed normally (Yes means either that this is a top prio port or a secondary port with atleast one acc.)
	//If no, wait (for primary port to trigger accumulation).
	//This will accommodate for jitters in the returned buffers from COPP(AFE).
	if(FALSE == MxAr_OutPortToCheckIfItIsOkToProceed(me, unOutPortID))
	{
		//We do not need to set the o/p channel status bit again. Buffer is already available.
		//Since no accumulation has happened yet, simply exit (dont pop the buffer yet).
		//When accumulation does happen, this entire function will be called again and
		//at that time, it will be OK to proceed further normally.
		pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = TRUE;
		MtMx_RemoveOutputPortFromWaitMask(me, unOutPortID);

		// Set the corresponding bit in outputReqPendingMask of all the active i/p ports feeding to this o/p port.
		MtMx_OutPortToUpdateOutputReqPendingMask(me, unOutPortID);

#ifdef MT_MX_EXTRA_DEBUG
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu reg o/p port %lu is non top prio port and has no acc yet. It will wait.", me->mtMxID, unOutPortID);
#endif

		return;
	}

	pCurrentOutPort->numBufRcvd++;

	result = qurt_elite_queue_pop_front(pCurrentOutPort->bufQ, (uint64_t*)&outBufMgrNode);

	elite_msg_data_buffer_t* pOutputBuf = (elite_msg_data_buffer_t*)outBufMgrNode.pBuffer;

	if(TRUE == pCurrentOutPort->bIsOutPortPendingReleaseBuffers && pCurrentOutPort->unNumOutputBufsPendingRelease > 0)
	{
		//If this port is pending release buffers, check to make sure that this is indeed the right buffer to release
		uint32_t unCurrentOpBufSize = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;

		//If incoming new o/p buffer on this port has a different size , then release it.
		//If the size does match, then DO NOT release it, and proceed normally.
		if((pOutputBuf->nActualSize) != (int32_t)(unCurrentOpBufSize))
		{
			MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu Pop&Destroy Buf#%lu, 0x%p, buf act size %lu, o/p port curr size %lu ",
					me->mtMxID, unOutPortID, pCurrentOutPort->unNumOutputBufsPendingRelease, outBufMgrNode.pBuffer, pOutputBuf->nActualSize, unCurrentOpBufSize);

			MTMX_FREE  (outBufMgrNode.pBuffer);
			pCurrentOutPort->unNumOutputBufsPendingRelease --;

			if (0 == pCurrentOutPort->unNumOutputBufsPendingRelease)
			{
				pCurrentOutPort->bIsOutPortPendingReleaseBuffers = FALSE;
			}

			return;
		}
	}

	//This port is no longer waiting on resources to carry on. Clear this flag
	pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = FALSE;

	ptr_acc_buf_32 = pCurrentOutPort->pAccBuf;

	if(MT_MX_BYTES_PER_SAMPLE_TWO == pCurrentOutPort->unBytesPerSample)
	{
		ptr_data_buf_16 = (int16_t*)(&(pOutputBuf->nDataBuf));

		// Copy samples from o/p port's AccBuf to OutputBuf and clear AccBuf
		for (i = 0; i < pCurrentOutPort->unBufSize; i++)
		{

			// Saturate 32-bit data to 16-bit and sign extend again to 32-bit word
			//  and truncate again back to 16-bit word
			*ptr_data_buf_16++ = (int16_t)Q6_R_sath_R(*ptr_acc_buf_32++);
		}
	}
	else // 4 bytes per sample
	{
		ptr_data_buf_32 = (int32_t*)(&(pOutputBuf->nDataBuf));

		// Copy samples from o/p port's AccBuf to OutputBuf and clear AccBuf
		for (i = 0; i < pCurrentOutPort->unBufSize; i++)
		{

			// Saturate Q28 result
			*ptr_acc_buf_32 = Q6_R_asl_RI_sat(*ptr_acc_buf_32, QFORMAT_SHIFT_FACTOR);

			// Scale it back to Q28
			*ptr_data_buf_32 = (*ptr_acc_buf_32 >> QFORMAT_SHIFT_FACTOR);

			// Increment read/write pointers
			ptr_acc_buf_32++;
			ptr_data_buf_32++;
		}
	}
	memset(pCurrentOutPort->pAccBuf, 0, pCurrentOutPort->unBufSize * sizeof(int32_t));

	pOutputBuf->nFlag          = 0;

	// If we have an input with a valid TS, and the input port ID is within bounds, copy the timestamp to the output buffer.
	MtMx_SetOutputBufferTimestamp(me, unOutPortID, pOutputBuf);
	pOutputBuf->nOffset        = 0;
	pOutputBuf->nActualSize    = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;
	pOutputBuf->nMaxSize       = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;
	pCurrentOutPort->numBufSent++;

	// Send OutputBuf downstream.
	pPeerDataQMsg = elite_msg_convt_buf_node_to_msg(
			&outBufMgrNode,
			ELITE_DATA_BUFFER,
			NULL,
			0,
			0
	);

#ifdef MT_MX_EXTRA_DEBUG
	MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu, Sending Buf#%lu: 0x%p Act Size: %lu", me->mtMxID, unOutPortID, pCurrentOutPort->numBufSent, pOutputBuf, pOutputBuf->nActualSize);
#endif

	result = qurt_elite_queue_push_back(pCurrentOutPort->pDownstreamPeerHandle->dataQ, (uint64_t*)pPeerDataQMsg );

	if (ADSP_FAILED(result))
	{
		MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to deliver buffer dowstream. Dropping");
		(void)elite_msg_push_payload_to_returnq(
				pCurrentOutPort->bufQ, (elite_msg_any_payload_t*) outBufMgrNode.pBuffer);
	}

	//Check if output port is operating in native mode and needs to be re-initialized
	if ((pCurrentOutPort->unNativeModeFlags) && pCurrentOutPort->bIsOutPortInNativeModePendingReInit)
	{
		if (ADSP_FAILED(result = MtMx_ReInitNativeModeOutport(me, unOutPortID)))
		{
			// If re-init failed, stop processing on this o/p port
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MxAr_ProcessBufQ o/p port %lu MtMx_ReInitNativeModeOutport failed. Res = %d.", unOutPortID, result);
			return;
		}
	}

	// Update session time for i/p's feeding to this o/p port
	MxAr_OutPortToHonorInPortsTimestamps(me, unOutPortID);

	// Check if any input port feeding to this output port registered for underflow event
	// If it did, and couldn't deliver samples to the output port in time, raise an underflow event
	MxAr_OutPortToCheckForUnderflowEvents(me, unOutPortID);

	// Check if EOS msg is pending on any of the i/p ports feeding to this o/p port.
	// If yes, send the EOS msg downstream.
	MtMx_OutPortToCheckForPendingEOS(me, unOutPortID);

	// In accInPortsMask, clear the bits of the input ports that accumulated into accBuf
	pCurrentOutPort->accInPortsMask = 0;


	// Set the corresponding bit in outputReqPendingMask of all the active i/p ports feeding to this o/p port.
	MtMx_OutPortToUpdateOutputReqPendingMask(me, unOutPortID);

	//check if there is accumulation in secondary acc buffer. 
	if (0 != pCurrentOutPort->secondaryAccInPortsMask)
	{
		memcpy(pCurrentOutPort->pAccBuf, pCurrentOutPort->pSecondaryAccBuf, pCurrentOutPort->unBufSize * sizeof(int32_t));

		//for input ports that have accumulated to secondary acc buffer, reset outputReqPendingMask.		
		MatrixInPortInfoType    *pCurrentInPort;
		uint32_t unInPortID;		
		while (pCurrentOutPort->secondaryAccInPortsMask)
		{
			unInPortID = Q6_R_ct0_R(pCurrentOutPort->secondaryAccInPortsMask);
			pCurrentOutPort->accInPortsMask |= 1 << unInPortID;
			pCurrentInPort = me->inPortParams[unInPortID];
			pCurrentInPort->outputReqPendingMask &= ~(1 << unOutPortID);
			pCurrentOutPort->secondaryAccInPortsMask &= ~(1 << unInPortID);			
		}

		//Zero out the secondary accBuf
		memset(pCurrentOutPort->pSecondaryAccBuf, 0, pCurrentOutPort->unBufSize * sizeof(int32_t));		
	}

	// Called every time an output buffer is received
	MxAr_CommonOutPortProcessingRoutine(me, unOutPortID);
}

void MxAt_ProcessOutputPortWakeUp(This_t *me, uint32_t unOutPortID)
{
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	MatrixInPortInfoType    *pCurrentInPort;
	uint32_t unInPortID, iMask;

	if(TRUE == pCurrentOutPort->bIsOutPortPendingReleaseBuffers)
	{
		//This may also be called if the TX matrix o/p port is pending to release some buffers
		MxAt_ProcessBufQ(me, unOutPortID);
		return;
	}

	if(TRUE == pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut)
	{
		//If o/p port is waiting for resources to free up to send buffer downstream, it would have
		//been added to the wait mask. In this case, go ahead and process the buffer immediately.
		pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = FALSE;
		MxAt_ProcessBufQ(me, unOutPortID);
		return;
	}

	if(FALSE == me->bIsMxAtOperatingInBurstMode)
	{
		if(FALSE == pCurrentOutPort->bIsTopPriorityInputPortLive)
		{
			iMask = pCurrentOutPort->inPortsMask;
			while(iMask)
			{
				unInPortID = Q6_R_ct0_R(iMask);
				iMask ^= (1 << unInPortID);
				pCurrentInPort = me->inPortParams[unInPortID];
				if (((pCurrentOutPort->inPortsTopPriorityMask) & (1 << unInPortID)) &&
						(pCurrentInPort->bIsNonLiveInputPortWaitingToAcc))
				{
					MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "WARNING! Calling MxAt_OutPortToProcessIfNonLiveInputPortsNeedToAcc from MxAt_ProcessBufQWrapper!");
					MxAt_OutPortToProcessIfNonLiveInputPortsNeedToAcc(me, unOutPortID);
				}
			}
		}
	}
}

void MxAt_ProcessBufQ(This_t *me, uint32_t unOutPortID)
{
	elite_msg_any_t               *pPeerDataQMsg;
	qurt_elite_bufmgr_node_t      outBufMgrNode;
	int16_t                       *ptr_data_buf_16;
	int32_t                       *ptr_acc_buf_32, *ptr_data_buf_32;
	uint32_t                      i;
	ADSPResult                    result;
	MatrixOutPortInfoType         *pCurrentOutPort;
	elite_msg_data_buffer_t       *pOutputBuf;

	MtMx_ClearOutputPortChannelStatus(me, unOutPortID);
	pCurrentOutPort = me->outPortParams[unOutPortID];

	// Take next buffer off the queue
	result = qurt_elite_queue_pop_front(pCurrentOutPort->bufQ, (uint64_t*)&outBufMgrNode);
	if(ADSP_FAILED(result) && (TRUE == pCurrentOutPort->bIsTopPriorityInputPortLive))
	{
		pCurrentOutPort->numBufRcvd++;
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MxAt_ProcessBufQ o/p port %lu qurt_elite_queue_pop_front returned %x... Top prio i/p port is live! Dropping o/p buf", unOutPortID, result);

		//Raise an overflow event
		MxAt_OutPortToCheckForOverflowEvents(me, unOutPortID);
		goto __commonroutine;
	}
	if((ADSP_EOK != result) && (ADSP_ENEEDMORE != result) && (FALSE == pCurrentOutPort->bIsTopPriorityInputPortLive))
	{
		pCurrentOutPort->numBufRcvd++;
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MxAt_ProcessBufQ o/p port %lu qurt_elite_queue_pop_front returned %x... Top prio i/p port is non-live! Dropping o/p buf", unOutPortID, result);

		//Raise an overflow event
		MxAt_OutPortToCheckForOverflowEvents(me, unOutPortID);
		goto __commonroutine;
	}
	if((ADSP_ENEEDMORE == result) && (FALSE == pCurrentOutPort->bIsTopPriorityInputPortLive))
	{

#ifdef MT_MX_EXTRA_DEBUG
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MxAt_ProcessBufQ o/p port %lu qurt_elite_queue_pop_front returned %x... Top prio i/p port is non-live! Waiting for resources...", unOutPortID, result);
#endif

		pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = TRUE;
		MtMx_AddOutputPortToWaitMask(me, unOutPortID);
		return;
	}

	pCurrentOutPort->numBufRcvd++;

	pOutputBuf = (elite_msg_data_buffer_t*)outBufMgrNode.pBuffer;

	if(TRUE == pCurrentOutPort->bIsOutPortPendingReleaseBuffers && pCurrentOutPort->unNumOutputBufsPendingRelease > 0)
	{
		//If this port is pending release buffers, check to make sure that this is indeed the right buffer to release
		uint32_t unCurrentOpBufSize = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;

		//If incoming new o/p buffer on this port has a different size , then release it.
		//If the size does match, then DO NOT release it, and proceed normally.
		if((pOutputBuf->nActualSize) != (int32_t)(unCurrentOpBufSize))
		{
			MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu Pop&Destroy Buf#%lu, 0x%p, buf act size %lu, o/p port curr size %lu ",
					me->mtMxID, unOutPortID, pCurrentOutPort->unNumOutputBufsPendingRelease, outBufMgrNode.pBuffer, pOutputBuf->nActualSize, unCurrentOpBufSize);

			MTMX_FREE  (outBufMgrNode.pBuffer);
			pCurrentOutPort->unNumOutputBufsPendingRelease --;

			if (0 == pCurrentOutPort->unNumOutputBufsPendingRelease)
			{
				pCurrentOutPort->bIsOutPortPendingReleaseBuffers = FALSE;
			}

			pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = TRUE;
			MtMx_AddOutputPortToWaitMask(me, unOutPortID);

			return;
		}
	}

	//This port is no longer waiting on resources to carry on. Clear this flag
	pCurrentOutPort->bIsOutPortWaitingForRscsToSendOut = FALSE;

	ptr_acc_buf_32 = pCurrentOutPort->pAccBuf;

	// Copy samples from o/p port's AccBuf to OutputBuf and clear AccBuf
	if(MT_MX_BYTES_PER_SAMPLE_TWO == pCurrentOutPort->unBytesPerSample)
	{
		ptr_data_buf_16 = (int16_t *)(&(pOutputBuf->nDataBuf));

		for (i = 0; i < pCurrentOutPort->unBufSize; i++)
		{

			// Saturate 32-bit sample to 16-bit and sign extend again to 32-bits
			// and truncate the result back to 16-bits
			*ptr_data_buf_16++ = (int16_t)Q6_R_sath_R(*ptr_acc_buf_32++);
		}
	}
	else
	{
		ptr_data_buf_32 = (int32_t *)(&(pOutputBuf->nDataBuf));

		for (i = 0; i < pCurrentOutPort->unBufSize; i++)
		{

			// Saturate Q28 result
			*ptr_acc_buf_32 = Q6_R_asl_RI_sat(*ptr_acc_buf_32, QFORMAT_SHIFT_FACTOR);

			// Scale it back to Q28
			*ptr_data_buf_32 = (*ptr_acc_buf_32 >> QFORMAT_SHIFT_FACTOR);

			// Increment read/write pointers
			ptr_acc_buf_32++;
			ptr_data_buf_32++;
		}
	}

	pOutputBuf->nFlag         = 0;

	pOutputBuf->nOffset         = 0;
	pOutputBuf->nActualSize     = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;
	pOutputBuf->nMaxSize       = pCurrentOutPort->unBufSize * pCurrentOutPort->unBytesPerSample;

	// Apply timestamp to the buffer
	MtMx_ApplyOutputBufferTS(me, unOutPortID, pOutputBuf);

	pCurrentOutPort->numBufSent++;

	// Send OutputBuf downstream
	pPeerDataQMsg = elite_msg_convt_buf_node_to_msg(
			&outBufMgrNode,
			ELITE_DATA_BUFFER,
			NULL,
			0,
			0
	);

#ifdef MT_MX_EXTRA_DEBUG
	MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu, Sending Buf#%lu: 0x%p Act Size: %lu", me->mtMxID, unOutPortID, pCurrentOutPort->numBufSent, pOutputBuf, pOutputBuf->nActualSize);
#endif

	result = qurt_elite_queue_push_back(pCurrentOutPort->pDownstreamPeerHandle->dataQ, (uint64_t*)pPeerDataQMsg );

	if (ADSP_FAILED(result))
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MxAt_ProcessBufQ o/p port %lu failed to deliver bfr DS! Error: %x...Dropping o/p buffer!", unOutPortID, result);
		(void)elite_msg_push_payload_to_returnq(pCurrentOutPort->bufQ, (elite_msg_any_payload_t*) outBufMgrNode.pBuffer);

		//Raise an overflow event
		MxAt_OutPortToCheckForOverflowEvents(me, unOutPortID);
	}
	else
	{
		//If it reaches this point, atleast one buffer was sent successfully. Therefore, it is safe to reset the overflow status of this o/p port
		if(TRUE == pCurrentOutPort->bIsRegisteredForOverflowEvents)
		{
			pCurrentOutPort->unOverflowReportStatus = MT_MX_OUTPUT_PORT_OVERFLOW_REPORT_STATUS_REPORT;
		}
	}

	__commonroutine:
	//Now, repeat the standard procedure after a buffer has been sent down stream.

	//For MXAT, almost never should the o/p port be added to the wait mask. Only very specific situations demand this.
	//Remove the o/p from the wait mask
	MtMx_RemoveOutputPortFromWaitMask(me,unOutPortID);

	//Zero out the accBuf
	memset(pCurrentOutPort->pAccBuf, 0, pCurrentOutPort->unBufSize * sizeof(int32_t));

	//Clear some masks
	pCurrentOutPort->accInPortsMask = 0;

	// Set the accBufAvailabilityMask to 1 for all the active i/p ports feeding to this o/p port: a mask that indicates
	// which i/p port can accumulate into this output port's accBuf. At this point, since the o/p buffer and acc. buffer
	// have been sent downstream, all the i/p ports that feed this o/p port can accumulate their samples, if they wish.
	MxAt_OutPortToUpdateAccBufAvailabilityMask(me, unOutPortID);

	// Set the corresponding bit in outputReqPendingMask of all the active i/p ports feeding to this o/p port.
	MtMx_OutPortToUpdateOutputReqPendingMask(me, unOutPortID);

	//check if there is accumulation in secondary acc buffer.
	if (0 != pCurrentOutPort->secondaryAccInPortsMask)
	{
		memcpy(pCurrentOutPort->pAccBuf, pCurrentOutPort->pSecondaryAccBuf, pCurrentOutPort->unBufSize * sizeof(int32_t));

		//for input ports that have accumulated to secondary acc buffer, reset outputReqPendingMask.
		MatrixInPortInfoType    *pCurrentInPort;
		uint32_t unInPortID;
		while (pCurrentOutPort->secondaryAccInPortsMask)
		{
			unInPortID = Q6_R_ct0_R(pCurrentOutPort->secondaryAccInPortsMask);
			pCurrentOutPort->accInPortsMask |= 1 << unInPortID;
			pCurrentOutPort->accBufAvailabilityMask  &= ~(1 << unInPortID);
			pCurrentInPort = me->inPortParams[unInPortID];
			pCurrentInPort->outputReqPendingMask &= ~(1 << unOutPortID);
			pCurrentOutPort->secondaryAccInPortsMask &= ~(1 << unInPortID);
		}

		//Zero out the secondary accBuf
		memset(pCurrentOutPort->pSecondaryAccBuf, 0, pCurrentOutPort->unBufSize * sizeof(int32_t));
	}

	//Check if output port is operating in native mode and needs to be re-initialized
	if ((pCurrentOutPort->unNativeModeFlags) && pCurrentOutPort->bIsOutPortInNativeModePendingReInit)
	{
		if (ADSP_FAILED(result = MtMx_ReInitNativeModeOutport(me, unOutPortID)))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MxAt_ProcessBufQ o/p port %lu MxAt_ReInitNativeModeOutport failed. Res = %d.", unOutPortID, result);
			return;
		}
	}

	// Check if EOS msg is pending on any of the i/p ports feeding to this o/p port. If yes, send the EOS msg downstream.
	MtMx_OutPortToCheckForPendingEOS(me, unOutPortID);

	// Now, Check and process if any non-live i/p ports needs to accumulate to this port's acc buffer.
	MxAt_OutPortToProcessIfNonLiveInputPortsNeedToAcc(me, unOutPortID);
}

static void MxAr_CommonOutPortProcessingRoutine(This_t *me, uint32_t unOutPortID)
{
	uint32_t                  steadyStateInPortsMask, unInPortID;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType    *pCurrentOutPort = me->outPortParams[unOutPortID];
	mt_mx_sampleslip_t	    *pSampleSlip;

	steadyStateInPortsMask = me->steadyStateInPortsMask;
	while(steadyStateInPortsMask)
	{
		unInPortID = Q6_R_ct0_R(steadyStateInPortsMask);
		steadyStateInPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];
		pSampleSlip     = &(pCurrentInPort->structSampleSlip);

		// If this input port's local buffer is full AND its top priority output port has request pending
		if((pCurrentInPort->bIsLocalBufFull || pCurrentInPort->bIsAccumulationPending) && ((1 << pCurrentInPort->unTopPrioOutPort) & (pCurrentInPort->outputReqPendingMask)))
		{
			//Normal operation
			MxAr_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);

			//Once the contents of an input port that was just transitioned to ACTIVE have been accumulated, move it to FALSE.
			pCurrentInPort->bInputPortJustMovedFromHeldToActive = FALSE;

			if (pCurrentInPort->bIsAccumulationPending)
			{
				pCurrentInPort->bIsAccumulationPending = FALSE;

				if (TRUE == pCurrentInPort->bShouldLocalBufBeReInit)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu triggering re-init of i/p port %lu local buf",
							me->mtMxID, unOutPortID, unInPortID);
					ADSPResult result = MtMx_ReInitLocalBuffer(me, unInPortID);

					//Check if output port is operating in native mode and needs to be re-initialized
					if (pCurrentOutPort->unNativeModeFlags && pCurrentOutPort->bIsOutPortInNativeModePendingReInit)
					{
						if (ADSP_FAILED(result = MtMx_ReInitNativeModeOutport(me, unOutPortID)))
						{
							MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu o/p port %lu MtMx_ReInitNativeModeOutport failed. Res = %d.", me->mtMxID, unOutPortID, result);
							return;
						}
					}

					pCurrentInPort->bShouldLocalBufBeReInit = FALSE;

					//Check if PSPD libraries needs to be re-init.
					MtMx_InPortToCheckReInitPspdLibraries(me, unInPortID);
				}
			}

			if(TRUE == pCurrentInPort->bShouldActiveInputPortBeReProcessed &&  NULL != pSampleSlip->pSampleSlipAppi)
			{
				//Special case handling when SSLib was introduced
				//MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBuffer from o/p port %lu", me->mtMxID, unInPortID, unOutPortID);
				MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);
				pCurrentInPort->bShouldActiveInputPortBeReProcessed = FALSE;
			}
		}
	}

	//Check if any pull ports are waiting to send data downstream
	MxAr_ProcessPendingPullOutPorts(me);
}

static void MxAr_OutPortToCheckForUnderflowEvents(This_t *me, uint32_t unOutPortID)
{
	ADSPResult              result;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	uint32_t                unInPortID, inPortsMask = pCurrentOutPort->inPortsMask;
	while (inPortsMask)
	{
		unInPortID = Q6_R_ct0_R(inPortsMask);
		inPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];

		//Report the underflow event if the client has registered for it.
		if ((TRUE == pCurrentInPort->bIsRegisteredForUnderflowEvents))
		{
			if(
					((pCurrentInPort->outputReqPendingMask) & (1 << unOutPortID)) &&
					(FALSE == pCurrentInPort->bIsLastMsgEOS) &&
					(unOutPortID == pCurrentInPort->unTopPrioOutPort) &&
					(TRUE == pCurrentInPort->bHasFirstSampleAccumulated) &&
					(INPUT_PORT_STATE_ACTIVE == pCurrentInPort->inPortState) &&
					(FALSE == pCurrentInPort->bInputPortJustMovedFromHeldToActive)
			)
			{
				if(MT_MX_INPUT_PORT_UNDERFLOW_REPORT_STATUS_REPORT == pCurrentInPort->unUnderflowReportStatus)
				{
					pCurrentInPort->unUnderflowReportStatus = MT_MX_INPUT_PORT_UNDERFLOW_REPORT_STATUS_DO_NOT_REPORT;
					Elite_CbType *pEliteCb = &(pCurrentInPort->eliteCb);
					MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"MtMx #%lu: generating underflow event for input port %lu", me->mtMxID, unInPortID);
					result = pEliteCb->pCbFct(pEliteCb->pCbHandle,
							ASM_SESSION_EVENT_RX_UNDERFLOW,
							0, NULL, 0);
					if (ADSP_FAILED(result))
					{
						MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Error - MtMx #%lu: callback ftn for generating underflow event for i/p port %lu returned with status 0x%x",
								me->mtMxID, unInPortID, result);
					}
				}
			}
			else
			{
				pCurrentInPort->unUnderflowReportStatus = MT_MX_INPUT_PORT_UNDERFLOW_REPORT_STATUS_REPORT;
			}
		}

		//During an underflow, session clock will advance in the case that the i/p buffer has a valid TS.
		//In that case, the expected TS has to be adjusted based on the current session clock
		if(
				((pCurrentInPort->outputReqPendingMask) & (1 << unOutPortID)) &&
				(FALSE == pCurrentInPort->bIsLastMsgEOS) &&
				(unOutPortID == pCurrentInPort->unTopPrioOutPort) &&
				(TRUE == pCurrentInPort->bHasFirstSampleAccumulated) &&
				(INPUT_PORT_STATE_ACTIVE == pCurrentInPort->inPortState) &&
				(FALSE == pCurrentInPort->bInputPortJustMovedFromHeldToActive)
		)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"MtMx #%lu: i/p port %lu o/p port %lu underflow occurred", me->mtMxID, unInPortID, unOutPortID);

			uint64_t ullSessionTime = 0;
			avsync_lib_get_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&ullSessionTime);
			// JV-TODO: need to fix error accumulation here.
			uint64_t proposed_expected_session_clock = ullSessionTime + pCurrentInPort->unFrameDurationInUsec.int_part;
			avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));
		}
	}
}

void MtMx_OutPortToCheckForPendingEOS(This_t *me, uint32 unOutPortID)
{
	ADSPResult              result;
	uint32_t                  unInPortID;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	uint32_t                inPortsMask = pCurrentOutPort->inPortsMask;
	while (inPortsMask)
	{
		unInPortID = Q6_R_ct0_R(inPortsMask);
		inPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];
		if (
				((pCurrentInPort->eosPendingMask) & (1 << unOutPortID)) &&
				(!pCurrentInPort->bIsAccumulationPending) &&
				(FALSE == pCurrentInPort->bInputPortJustMovedFromHeldToActive)
		)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO, "MtMx #%lu o/p port %lu sending EOS from i/p port %lu downstream, eos pending flag = %lu",
					me->mtMxID, unOutPortID, unInPortID, pCurrentInPort->eosPendingMask);

			elite_msg_any_t msg;

			if(FALSE == pCurrentInPort->bIsGaplessEOSMsgWaitingToBeSentDS)
			{
				if (ADSP_FAILED(result = MtMx_DuplicateEosMsg( &(pCurrentInPort->myDataQMsg), &msg)))
				{
					MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu o/p port %lu: couldn't duplicate regular EOS from i/p port %lu",
							me->mtMxID, unOutPortID, unInPortID);
					goto __EOScleanup;
				}
			}
			else
			{
				if (ADSP_FAILED(result = MtMx_DuplicateEosMsg( &(pCurrentInPort->gaplessEOSmsg), &msg)))
				{
					MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu o/p port %lu: couldn't duplicate gap-less EOS from i/p port %lu",
							me->mtMxID, unOutPortID, unInPortID);
					goto __EOScleanup;
				}
			}

			MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu: Sending EOS from i/p port %lu DS. EOSFormat: %lu",
					me->mtMxID, unOutPortID, unInPortID, (((elite_msg_data_eos_apr_t*)(msg.pPayload))->unEosFormat));

			if (ADSP_FAILED(result = qurt_elite_queue_push_back(pCurrentOutPort->pDownstreamPeerHandle->dataQ, (uint64_t*)&(msg))))
			{
				MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu o/p port %lu: couldn't send EOS from i/p port %lu downstream",
						me->mtMxID, unOutPortID, unInPortID);
				elite_msg_finish_msg(&msg, ADSP_EOK);
			}

			__EOScleanup:
			//Clear the pending EOS flag on this output port
			pCurrentInPort->eosPendingMask &=  (~(1 << unOutPortID));

			if(pCurrentInPort->bIsEOSHeld)
			{
				//Stop incrementing session clock for this input port
				pCurrentInPort->bIsSessionUnderEOS = TRUE;
			}

			if ( !( pCurrentInPort->eosPendingMask) )
			{
				if(pCurrentInPort->bIsEOSHeld)
				{
					//For regular EOS, release the EOS message from upstream if all EOS has been sent.
					elite_msg_finish_msg( &(pCurrentInPort->myDataQMsg), ADSP_EOK );
					pCurrentInPort->bIsEOSHeld = FALSE;
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu adding i/p port %lu to wait mask", me->mtMxID, unOutPortID, unInPortID);
					MtMx_AddInputPortToWaitMask(me,unInPortID);
				}
				else if(pCurrentInPort->bIsGaplessEOSMsgWaitingToBeSentDS)
				{
					//For Gap-less EOS, release the gapless EOS msg, mark this i/p port as not pending EOS if all EOS has been sent.
					elite_msg_finish_msg( &(pCurrentInPort->gaplessEOSmsg), ADSP_EOK );
					pCurrentInPort->bIsGaplessEOSMsgWaitingToBeSentDS = FALSE;
				}
			}
		}
	}
}

void MxAr_OutPortToHonorInPortsTimestamps(This_t *me, uint32_t unOutPortID)
{
	int32_t                 unInPortID;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	uint32_t                inPortsMask = pCurrentOutPort->inPortsMask;
	elite_msg_data_buffer_t*   pInputBuf;

	uint32_t bufDelay = pCurrentOutPort->unFrameDurationInUsec.int_part * pCurrentOutPort->unNumOutputBufs;
	uint32_t ullCurrentDownstreamDelay = *(pCurrentOutPort->punAFEDelay) + *(pCurrentOutPort->punCoppBufDelay) + *(pCurrentOutPort->punCoppAlgDelay) + bufDelay;

	while (inPortsMask)
	{
		unInPortID = Q6_R_ct0_R(inPortsMask);
		inPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];

		if (unOutPortID == pCurrentInPort->unTopPrioOutPort)
		{
			avsync_lib_update_stc_clock(pCurrentInPort->pAVSyncLib);

			if((FALSE == pCurrentInPort->bHasFirstSampleAccumulated) &&
					(INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER != pCurrentInPort->inPortState) &&
					(INPUT_PORT_STATE_WAITING_TO_BE_READY != pCurrentInPort->inPortState) &&
					(INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE != pCurrentInPort->inPortState))
			{
				continue;
			}

			avsync_rendering_decision_t rendering_decision = RENDER;
			avsync_lib_make_simple_rendering_decision(pCurrentInPort->pAVSyncLib, pCurrentInPort->ullInBufHoldDurationInUsec.int_part, &rendering_decision);

			switch (pCurrentInPort->inPortState)
			{
			case INPUT_PORT_STATE_ACTIVE:
			{
				// if i/p buffer timestamp is not valid, and if i/p port is starving for data, do not
				// increment the session clock when silence is inserted
				if (!pCurrentInPort->bIsTimeStampValid && !(pCurrentOutPort->accInPortsMask & (1 << unInPortID)))
				{
					break;
				}
				// if an EOS was sent down, no longer increment i/p port's session time
				if (FALSE == pCurrentInPort->bIsSessionUnderEOS)
				{
					//Update session time and absolute time
					MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

					if(pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown)
					{
						pCurrentInPort->unNumRemSamplesAdj--;
						if (0 == pCurrentInPort->unNumRemSamplesAdj)
						{
							pCurrentInPort->bIsSampleAddDropEnabled = FALSE;
							pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown = FALSE;
						}
					}

					//Update timestamps
					if(pCurrentInPort->bIsTimeStampValid)
					{
						mt_mx_copy_time_value(&pCurrentInPort->ullTimeStampProcessed, pCurrentInPort->ullTimeStampAtAccumulation);
					}

#ifdef MT_MX_EXTRA_DEBUG
					MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu ProcessedTS [%lu, %lu] samples, TSValidity: %d", me->mtMxID, unInPortID,
							(uint32_t)(pCurrentInPort->ullTimeStampProcessed.int_part>>32),(uint32_t)(pCurrentInPort->ullTimeStampProcessed.int_part), pCurrentInPort->bIsTimeStampValid);
#endif

					//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
					ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pAVSyncLib);
					if (ADSP_FAILED(commit_result) && (ADSP_EBADPARAM != commit_result))
					{
						MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
					}
				}

				//For cases where stream clk is not yet sync'd with session clk, and i/p buffers are being dropped because their TS's are in the past,
				//increment the expected str_time by the amount of samples rendered down in the form of silence
				// if session time > expected stream time, expected stream time = session time
				if (!pCurrentInPort->bIsStrClkSyncdWithSessClk)
				{
					uint64_t ullStaleEOSSamplesuSec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->ullStaleEosSamples, pCurrentInPort->unSampleRate);
					// JV-TODO: need to fix error accumulation here.
					avsync_lib_increment_expected_session_clock(pCurrentInPort->pAVSyncLib, (pCurrentInPort->unFrameDurationInUsec.int_part - ullStaleEOSSamplesuSec));
				}

				pCurrentInPort->ullStaleEosSamples = 0;
				break;
			}
			case INPUT_PORT_STATE_WAITING_TO_BE_READY:
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

				if (RENDER == rendering_decision)
				{
					pCurrentInPort->inPortState = INPUT_PORT_STATE_READY;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_READY;
					MtMx_AddInputPortToWaitMask(me, unInPortID);
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: o/p port %lu moving i/p port %lu to ready state", me->mtMxID, unOutPortID, unInPortID);
				}
				else
				{
					// Need to use output port frame duration as input port media type is not received yet
					mt_mx_decrement_time(&pCurrentInPort->ullInBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

				break;
			}

			case INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE:
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

				if (RENDER == rendering_decision)
				{
					pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
					MtMx_InPortToUpdateWaitMask(me, unInPortID);
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: o/p port %lu moving i/p port %lu to active state", me->mtMxID, unOutPortID, unInPortID);
				}
				else
				{
					mt_mx_decrement_time(&pCurrentInPort->ullInBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

				break;
			}

			case INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER:
			{
				//Update session time and absolute time
				MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

				pInputBuf = pCurrentInPort->pInputBuf;
				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu],frame duration %lu usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part),
						(uint32_t)pCurrentInPort->unFrameDurationInUsec.int_part);

				//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
				ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pAVSyncLib);
				if (ADSP_FAILED(commit_result) && NULL!=pCurrentInPort->pAVSyncLib)
				{
					MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
				}

				//Adjust ullInBufHoldDuration if less than 1ms
				if(pCurrentInPort->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
				}

				if (pCurrentInPort->ullInBufHoldDurationInUsec.int_part <= pCurrentInPort->unFrameDurationInUsec.int_part)
				{
					//Update session time and expected stream time
					avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
					avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));

					//I/p port transition back to ACTIVE
					pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
					MtMx_InPortToUpdateWaitMask(me, unInPortID);

					//Trigger i/p port processing by honoring i/p TS
					MtMx_InPortToHonorTimestamp(me, unInPortID);

					if(pCurrentInPort->ullInBufHoldDurationInUsec.int_part > 0)
					{
						MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBufferHold from o/p port", me->mtMxID, unInPortID);
						MtMx_MsgDataBufferHold(me, unInPortID, pCurrentInPort);
					}
					else
					{
						//Explictly set the hold duration to 0
						mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
						MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBuffer from o/p port", me->mtMxID, unInPortID);
						MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);
					}

					//Mark this input port as just moved to ACTIVE (to skip the EOS and Underflow handling, that follow this function on the outporthandler)
					pCurrentInPort->bInputPortJustMovedFromHeldToActive = TRUE;

					//Now, sync is complete
					pCurrentInPort->bIsStrClkSyncdWithSessClk = TRUE;
				}
				else if(FALSE == pCurrentInPort->bIsLocalBufEmpty)
				{
					//if(pCurrentInPort->ullInBufHoldDuration >= pCurrentInPort->unFrameDurationInUsec) && (FALSE == pCurrentInPort->bIsLocalBufEmpty)

					//In the normal hold case, there maybe some residual data from previous buffer sitting in the internal buffer
					//of the audio matrix input port. In such a case, it would be better to send the rest of the previous data
					//out before proceeding with the hold duration for the current buffer, to avoid an "island" type glitch.
					MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);
				}
				else //if(pCurrentInPort->ullInBufHoldDurationInSamples >= pCurrentInPort->unInPortPerChBufSize)
				{
					mt_mx_decrement_time(&pCurrentInPort->ullInBufHoldDurationInUsec, pCurrentInPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

				break;
			}
			case INPUT_PORT_STATE_PAUSED:
			{
				// During paused state, increment session time ONLY for as long as i/p port receives i/p buffers.
				// When it doesn't receive i/p buffer anymore, stop incrementing session clock.
				if (pCurrentOutPort->accInPortsMask & (1 << unInPortID))
				{
					//Update session time and absolute time
					MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

					if(pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown)
					{
						pCurrentInPort->unNumRemSamplesAdj--;
						if (0 == pCurrentInPort->unNumRemSamplesAdj)
						{
							pCurrentInPort->bIsSampleAddDropEnabled = FALSE;
							// session clock adjustment has completed, output port can go back to incrementing session clock normally
							pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown = FALSE;
						}
					}

					//Update timestamps
					if(pCurrentInPort->bIsTimeStampValid)
					{
						mt_mx_copy_time_value(&pCurrentInPort->ullTimeStampProcessed, pCurrentInPort->ullTimeStampAtAccumulation);
					}

					//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
					ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pAVSyncLib);
					if (ADSP_FAILED(commit_result) && NULL!=pCurrentInPort->pAVSyncLib)
					{
						MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
					}
				}
				pCurrentInPort->ullStaleEosSamples = 0;
				break;
			}
			}
		}
	}
}

void MtMx_OutPortToUpdateOutputReqPendingMask(This_t *me, uint32_t unOutPortID)
{
	MatrixInPortInfoType    *pCurrentInPort;
	uint32_t unInPortID;

	for (unInPortID = 0; unInPortID <= me->maxInPortID; unInPortID++)
	{
		pCurrentInPort = me->inPortParams[unInPortID];
		if (NULL != pCurrentInPort)
		{
			if (INPUT_PORT_STATE_INACTIVE != pCurrentInPort->inPortState)
			{
				pCurrentInPort->outputReqPendingMask |= pCurrentInPort->strMask & (1 << unOutPortID);
			}
		}
	}
}

void MxAt_OutPortToProcessIfNonLiveInputPortsNeedToAcc(This_t *me, uint32_t unOutPortID)
{
	uint32_t               steadyStateInPortsMask, unInPortID;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	ADSPResult result = ADSP_EOK;

	steadyStateInPortsMask = me->steadyStateInPortsMask;
	while(steadyStateInPortsMask)
	{
		unInPortID = Q6_R_ct0_R(steadyStateInPortsMask);
		steadyStateInPortsMask ^= (1 << unInPortID);
		pCurrentInPort = me->inPortParams[unInPortID];

		// If this input port is Non-Live and is waiting to accumulate to this o/p port's acc buf, trigger accumulation
		if(((FALSE == pCurrentInPort->bIsPortLive) && ((pCurrentOutPort->inPortsWaitingToAccMask) & (1<<unInPortID))) ||
				((FALSE == pCurrentInPort->bIsPortLive) && (pCurrentInPort->bIsAccumulationPending)))
		{
			MxAt_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);
			if (pCurrentInPort->bIsAccumulationPending)
			{
				pCurrentInPort->bIsAccumulationPending = FALSE;

				if (TRUE == pCurrentInPort->bShouldLocalBufBeReInit)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MxAt #%lu o/p port %lu triggering re-init of i/p port %lu local buf",
							me->mtMxID, unOutPortID, unInPortID);
					(void) MtMx_ReInitLocalBuffer(me, unInPortID);

					//Check if output port is operating in native mode and needs to be re-initialized
					if (pCurrentOutPort->unNativeModeFlags && pCurrentOutPort->bIsOutPortInNativeModePendingReInit)
					{
						if (ADSP_FAILED(result = MtMx_ReInitNativeModeOutport(me, unOutPortID)))
						{
							MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MxAt_ProcessBufQ o/p port %lu MxAt_ReInitNativeModeOutport failed. Res = %d.", unOutPortID, result);
							return;
						}
					}
					pCurrentInPort->bShouldLocalBufBeReInit = FALSE;

					//Check if the PSPD libraries needs to be re-init.
					MtMx_InPortToCheckReInitPspdLibraries(me, unInPortID);
				}
			}
		}
	} // while(steadyStateInPortsMask)
}

static bool_t MxAr_OutPortToCheckIfItIsOkToProceed(This_t *me, uint32_t unOutPortID)
{
	//This function checks for 3 things:
	//1) Is this o/p port connected to any i/p port. If not, return TRUE (OK to proceed).
	//2) Is this o/p port a top priority port to any i/p port it is connected to. If yes, return TRUE (OK to proceed).
	//3) If this o/p port is not a top priority port, then check if at least one connected i/p port is Active.
	//       If not, then all of its i/p ports may be paused/holding in which case return TRUE (OK to proceed).
	//       if yes, then at least one connected i/p port is active, in which case check if at least one accumulation has happened.
	//           If yes, then proceed and return TRUE
	//           if not, then wait for atleast one accumulation to happen prior to sending zeros. (return FALSE).

	bool_t bIsItOKToProceed = TRUE;
	bool_t bIsAnyConnectedPortActive = FALSE;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	uint32_t                inPortsMask = pCurrentOutPort->inPortsMask;
	uint32_t                unInPortID;
	MatrixInPortInfoType   *pCurrentInPort;

	//Check if this o/p port has anything connected yet. If not, send zeros as normal and OK to proceed
	if(0 == inPortsMask)
	{
		return bIsItOKToProceed;
	}

	//If this port is pending a release buffer, it is OK to proceed (the current existing steady state buffers do not count)
	if (TRUE == pCurrentOutPort->bIsOutPortPendingReleaseBuffers && pCurrentOutPort->unNumOutputBufsPendingRelease > 0
			&& pCurrentOutPort->unNumOutputBufsPendingRelease != pCurrentOutPort->unNumOutputBufs)
	{
		return bIsItOKToProceed;
	}

	while (inPortsMask)
	{
		unInPortID = Q6_R_ct0_R(inPortsMask);
		inPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];
		if(unOutPortID == pCurrentInPort->unTopPrioOutPort)
		{
			return bIsItOKToProceed;
		}

		if (INPUT_PORT_STATE_ACTIVE == pCurrentInPort->inPortState)
		{
			bIsAnyConnectedPortActive = TRUE;
		}
	}

	//Only if all connected i/p ports are active will the waiting for resource logic apply on this o/p port
	if (FALSE == bIsAnyConnectedPortActive)
	{
		return bIsItOKToProceed;
	}

	//If code execution has reached this point, it means this o/p port is not a top priority o/p port to any connected i/p port
	//And at least one of its connected i/p ports are ACTIVE.
	//It is OK to proceed if atleast one accumulation has happened into this o/p port's acc. buffer
	bIsItOKToProceed = (0 == pCurrentOutPort->accInPortsMask) ? FALSE:TRUE;

	return bIsItOKToProceed;
}

ADSPResult MtMx_OpPortPopBuffer(This_t *me, uint32_t unOutPortID)
{
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	ADSPResult                 result = ADSP_EOK;
	qurt_elite_bufmgr_node_t bufNode;

	if (0 < pCurrentOutPort->unNumOutputBufsPendingRelease)
	{
		//Pop the buffer and destroy it
		result = qurt_elite_queue_pop_front(pCurrentOutPort->bufQ, (uint64_t*)&bufNode);

		if (ADSP_EOK == result)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu Pop&Destroy Buf#%lu, 0x%p",
					me->mtMxID, unOutPortID, pCurrentOutPort->unNumOutputBufsPendingRelease, bufNode.pBuffer);

			MTMX_FREE  (bufNode.pBuffer);
			pCurrentOutPort->unNumOutputBufsPendingRelease --;
		}
	}

	return result;
}

ADSPResult MtMx_OpPortReCfg(This_t *me, uint32_t unOutPortID)
{
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	ADSPResult                 result = ADSP_EOK;

	//Free up the Acc. Buf. using aligned free
	if (pCurrentOutPort->pAccBuf)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu o/p port %lu Freeing acc. buffer: 0x%p", me->mtMxID, unOutPortID,  pCurrentOutPort->pAccBuf);
		MTMX_ALIGNED_FREE(pCurrentOutPort->pAccBuf);
	}

	//Free up the secondary Acc. Buf. using aligned free
	if (pCurrentOutPort->pSecondaryAccBuf)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu o/p port %lu Freeing secondary acc. buffer: 0x%p", me->mtMxID, unOutPortID,  pCurrentOutPort->pSecondaryAccBuf);
		MTMX_ALIGNED_FREE(pCurrentOutPort->pSecondaryAccBuf);
	}	

	//Alloc new buffers
	if (ADSP_FAILED(result = MtMx_OutportAllocBuffers(me, unOutPortID, pCurrentOutPort)))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: O/p port %lu Error in alloc buffers! Result = %d",
				me->mtMxID, unOutPortID, result);
		return result;
	}

	//O/p port (if in native mode) has successfully re-inited.
	pCurrentOutPort->bIsOutPortInNativeModePendingReInit = FALSE;

	//Check and try to send the MT down
	if (ADSP_FAILED(result = MtMx_OpPortReCfg_CheckAndSendMT(me, unOutPortID, pCurrentOutPort)))
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: O/p port %lu Error in MT check and send step! Result = %d",
				me->mtMxID, unOutPortID, result);
		return result;
	}

	MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu output port %lu new settings", me->mtMxID, unOutPortID);
	MtMx_PrintDebugMediaTypeInformationOutputPort(me, unOutPortID);

	//Setup masks etc after recfg
	if(ADM_MATRIX_ID_AUDIO_RX == me->mtMxID)
	{
		MxAr_OpPortReCfg_PostSteps(me, unOutPortID, pCurrentOutPort);
	}
	else
	{
		MxAt_OpPortReCfg_PostSteps(me, unOutPortID, pCurrentOutPort);
	}

	MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Reconfiguring output port %lu End. Current state %d.",
			me->mtMxID, unOutPortID, pCurrentOutPort->outPortState);

	return result;
}

static void MxArUpdateInputPortSessionTime(This_t *me,
		uint32_t unInPortID,
		MatrixInPortInfoType *pCurrentInPort,
		MatrixOutPortInfoType *pCurrentOutPort,
		uint32_t ullCurrentDevicePathDelay)
{
	uint32_t unSampleRate = pCurrentInPort->unSampleRate;
	uint32_t unPortPerChBufSize = pCurrentInPort->unInPortPerChBufSize;
	uint64_t ullNumSamplesForStcUpdate = pCurrentInPort->ullNumSamplesForStcUpdate;
	uint32_t unSampleIncrement = 0;

	// When adding an additional device (running at a different rate) to an existing session it is
	// possible that that output port will send data downstream before reconfiguring to the new rate.
	// During this transition the input port may have already reconfigured itself and it's state
	// will reflect the new sampling rate; however, the data being sent down is at the old rate.
	// In this case we must use the old rate information to update the STC. The old rate information
	// is the same as the output port's rate.
	if (pCurrentInPort->unSampleRate != pCurrentOutPort->unSampleRate)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port fs %lu does not match o/p port fs %lu. Using old rate for ST update!",
				me->mtMxID, pCurrentInPort->unSampleRate, pCurrentOutPort->unSampleRate);
		unSampleRate = pCurrentOutPort->unSampleRate;
		unPortPerChBufSize = pCurrentOutPort->unOutPortPerChBufSize;

		// Convert the sample count to the old base using the previous sampling rate.
		// sample_base_old = sample_base_new * (fs_prev / fs_new)
		ullNumSamplesForStcUpdate = (pCurrentInPort->ullNumSamplesForStcUpdate * pCurrentOutPort->unSampleRate) / pCurrentInPort->unSampleRate;
	}

	//Default rendering mode
	if(pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown)
	{
		if (MT_MX_INPUT_PORT_ADD_SAMPLES == pCurrentInPort->samplesAddOrDropMask)
		{
			unSampleIncrement += (unPortPerChBufSize - MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER - pCurrentInPort->ullStaleEosSamples);
		}
		else
		{
			unSampleIncrement += (unPortPerChBufSize + MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER - pCurrentInPort->ullStaleEosSamples);
		}
	}
	else
	{
		unSampleIncrement += unPortPerChBufSize - pCurrentInPort->ullStaleEosSamples;
	}
	ullNumSamplesForStcUpdate += unSampleIncrement;
	pCurrentInPort->ullNumSamplesForStcUpdate += unSampleIncrement;


	// overflow protection logic is unnecessary since the calculation below will overflow after ~1000 days
	// The Proposed Session Clock = Equivalent time for the STC Update + The Base Time for STC
	uint64_t proposed_session_clock = (MT_MX_SAMPLES_TO_USEC(ullNumSamplesForStcUpdate, unSampleRate))+ (pCurrentInPort->ullTimeStampForStcBaseInUsec.int_part);

#ifdef MT_MX_EXTRA_DEBUG
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu updating STC to [%lu, %lu]",
			me->mtMxID, unInPortID, (uint32_t)(proposed_session_clock>>32), (uint32_t)proposed_session_clock);
#endif

	avsync_lib_update_session_clock(pCurrentInPort->pAVSyncLib,proposed_session_clock);
	uint64_t ullStaleEosSamplesuSec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->ullStaleEosSamples, unSampleRate);
	avsync_lib_update_absolute_time(pCurrentInPort->pAVSyncLib, ullCurrentDevicePathDelay-ullStaleEosSamplesuSec, FALSE);
}
