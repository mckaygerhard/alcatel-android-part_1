/**
@file MixerSvc_LatencyModuleUtils.h
@brief This file declares latency module functions that 
       the audio matrix mixer uses.
*/

/*========================================================================
Copyright (c) 2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_LatencyModuleUtils.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/10/2014 aprasad  Created file.
========================================================================== */

#ifndef MIXER_SVC_LATENCY_MODULE_UTILS_H
#define MIXER_SVC_LATENCY_MODULE_UTILS_H

#include "MixerSvc.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void MtMx_DestroyPspdLatencyModule(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);
void MtMx_InOutPortsToCheckReInitLatencyModuleLibrary(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);
ADSPResult MtMx_SetLatency(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //MIXER_SVC_LATENCY_MODULE_UTILS_H

