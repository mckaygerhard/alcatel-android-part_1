/**
@file MixerSvc_ChanMixUtils.cpp
@brief This file defines channel mixer utility functions that 
       the audio matrix mixer uses.
 */

/*========================================================================
Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_ChanMixUtils.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
03/10/2014  kr      Re-factoring, De-coupling PSPD and ChannelMixer.
11/25/2013 rkc     Created file.
========================================================================== */

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"
#include "MixerSvc_MsgHandlers.h"
#include "MixerSvc_OutPortHandler.h"
#include "MixerSvc_Util.h"
#include "adsp_media_fmt.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "MixerSvc_InPortHandler.h"
#include "MixerSvc_ChanMixUtils.h"
#include "AudDevMgr_PrivateDefs.h"
#include "MixerSvc_Pspd.h"

static ADSPResult MtMx_CreatePspdChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, eChMixerType eChannelMixerType);
static ADSPResult MtMx_CreateInitChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, eChMixerType eChannelMixerType);
static void MtMx_DestroyChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);

void MtMx_InOutPortsToCheckReInitChannelMixerLibrary(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, bool_t bOptzCheck)
{
	MatrixInPortInfoType    *pCurrentInPort  = me->inPortParams[unInPortID];
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	mt_mx_struct_pspd_t                *pCurrentPspd                         = &(pCurrentInPort->structPspd[unOutPortID]);
	mt_mx_struct_channel_mixer_t *pCurrentInputOutputChMixer = &(pCurrentInPort->structChanMixer[unOutPortID]);
	ADSPResult                               result                                       = ADSP_EOK;

	if(!MtMx_PspdSvcCreated(pCurrentPspd))
	{
		//PSPD thread needs to be created for this input-output pair.
		memset(&pCurrentPspd->thread_param, 0, sizeof(pCurrentPspd->thread_param));
		result = MtMx_CreatePspdSvc(&pCurrentPspd->thread_param, unInPortID, unOutPortID, &(pCurrentPspd->unPspdKpps), &(pCurrentPspd->unPspdBw));

		if (ADSP_EOK != result)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, created PSPD svc. failed, result = %d", me->mtMxID, unInPortID, unOutPortID, result);
			return;
		}
		else
		{
			(void)MtMx_PspdReInitBuffers(me, unInPortID, unOutPortID);
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, created PSPD svc. success", me->mtMxID, unInPortID, unOutPortID);
		}
	}

	//If the o/p port is a native mode port, the ChMixer lib will not be init/used.
	if(FALSE == MtMx_ChannelNativityIsEnabled(pCurrentOutPort->unNativeModeFlags))
	{
		if((FALSE == pCurrentInputOutputChMixer->bIsDTSMixerLibCreated) && (TRUE == pCurrentInPort->bIsDTSStream))
		{
			//Destroy current PSPD channel mixer (if it exists)
			MtMx_DestroyPspdChannelMixer(me, unInPortID, unOutPortID);

			//Create new PSPD DTS channel mixer
			result = MtMx_CreatePspdChannelMixer(me, unInPortID, unOutPortID, PSPD_CHMIXER_DTS);
			if (ADSP_EOK != result)
			{
				MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, DTS channel mixer creation failed, result=%d. Cont w/o lib.",
						me->mtMxID, unInPortID, unOutPortID, result);
				goto __bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary;
			}
			else
			{
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, created DTS channel mixer", me->mtMxID, unInPortID, unOutPortID);
			}
		}
		else if(FALSE == pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated)
		{
			//Destroy current PSPD channel mixer (if it exists)
			MtMx_DestroyPspdChannelMixer(me, unInPortID, unOutPortID);

			//Create new PSPD QCOM channel mixer
			result = MtMx_CreatePspdChannelMixer(me, unInPortID, unOutPortID, PSPD_CHMIXER_QCOM);
			if (ADSP_EOK != result)
			{
				MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, QCOM channel mixer creation failed, result=%d. Cont w/o lib.",
						me->mtMxID, unInPortID, unOutPortID, result);
				goto __bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary;
			}
			else
			{
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, created QCOM channel mixer", me->mtMxID, unInPortID, unOutPortID);
			}
		}

		if(TRUE == bOptzCheck && TRUE == pCurrentInputOutputChMixer->bCanChMixerBeOptimizedOut)
		{
			//As an optimization step, the library will not be called for the following scenarios
			//1) M->M
			//2) M->S
			//3) S->M
			//4) S->S with the same channel maps on the i/p and o/p
			if (
					(((NUM_CHANNELS_MONO == pCurrentInPort->unNumChannels) && (NUM_CHANNELS_MONO == pCurrentOutPort->unNumChannels)) ||
							((NUM_CHANNELS_MONO == pCurrentInPort->unNumChannels) && (NUM_CHANNELS_STEREO == pCurrentOutPort->unNumChannels)) ||
							((NUM_CHANNELS_STEREO == pCurrentInPort->unNumChannels) && (NUM_CHANNELS_MONO == pCurrentOutPort->unNumChannels)) ||
							((NUM_CHANNELS_STEREO == pCurrentInPort->unNumChannels) && (NUM_CHANNELS_STEREO == pCurrentOutPort->unNumChannels) &&
									((pCurrentInPort->unChannelMapping[0] == pCurrentOutPort->unChannelMapping[0] &&
											pCurrentInPort->unChannelMapping[1] == pCurrentOutPort->unChannelMapping[1]) ||
											(pCurrentInPort->unChannelMapping[0] == pCurrentOutPort->unChannelMapping[1] &&
													pCurrentInPort->unChannelMapping[1] == pCurrentOutPort->unChannelMapping[0])))) &&
													(FALSE == pCurrentInPort->bIsDTSStream)
			)
			{
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, ChannelMixerLib will not be used (optimization). Cont w/o lib.",
						me->mtMxID, unInPortID, unOutPortID);
				MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx i/p #ch %lu, o/p #ch %lu, i/p (ch[0],ch[1]) = (%d,%d), o/p (ch[0],ch[1]) = (%d,%d)",
						pCurrentInPort->unNumChannels, pCurrentOutPort->unNumChannels, pCurrentInPort->unChannelMapping[0],
						pCurrentInPort->unChannelMapping[1], pCurrentOutPort->unChannelMapping[0], pCurrentOutPort->unChannelMapping[1]);
				goto __bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary;
			}

			// channel mixer library is not required if number of channels, channel map are same
			if (pCurrentInPort->unNumChannels == pCurrentOutPort->unNumChannels)
			{
				// check if channel mapping is same. If same, delete channel mixer.
				uint32_t unChannelNum = 0;
				for (unChannelNum = 0; unChannelNum < pCurrentInPort->unNumChannels; unChannelNum++)
				{ 
					if (pCurrentInPort->unChannelMapping[unChannelNum] != pCurrentOutPort->unChannelMapping[unChannelNum])
					{
						break;
					}
				}
				if (unChannelNum == pCurrentInPort->unNumChannels)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, ChannelMixerLib will not be used (optimization). Cont w/o lib.",
							me->mtMxID, unInPortID, unOutPortID);
					goto __bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary;				
				}
			}
		}

		//Set the output channel configuration through set param
		ADSPResult result = ADSP_EOK;
		result = MtMx_SetChannelMixerCfg(me, unInPortID, unOutPortID);
		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, MtMx_SetChannelMixerCfg returned failure. Cont w/o lib.",
					me->mtMxID, unInPortID, unOutPortID);
			goto __bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary;
		}

		//Successful init
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, MtMx_SetChannelMixerCfg returned success.",
				me->mtMxID, unInPortID, unOutPortID);

		return;
	}

	__bailoutMtMxInOutPortsToCheckReInitChannelMixerLibrary:
	//Destroy current PSPD channel mixer (if it exists)
	MtMx_DestroyPspdChannelMixer(me, unInPortID, unOutPortID);
	return;
}

void MtMx_SetDtsDownmixerParams(This_t *me, uint32_t unInPortID, uint32_t paramID, uint32_t paramSize, const uint8_t *pParams)
{
	MatrixInPortInfoType  *pCurrentInPort  = me->inPortParams[unInPortID];
	uint32_t strMask = pCurrentInPort->strMask;
	uint32_t  unOutPortID;
	MatrixOutPortInfoType   *pCurrentOutPort;

	//Delete the previously stored coefficients
	if(pCurrentInPort->punSetParamPayloadCache)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Freeing punSetParamPayloadCache 0x%p", me->mtMxID, pCurrentInPort->punSetParamPayloadCache);
		MTMX_FREE (pCurrentInPort->punSetParamPayloadCache);
	}

	//Allocate the memory for Coefficients
	pCurrentInPort->punSetParamPayloadCache = (void*)qurt_elite_memory_malloc(paramSize, QURT_ELITE_HEAP_DEFAULT);
	if (!pCurrentInPort->punSetParamPayloadCache)
	{
		MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error: Failed to allocate memory for DTS SetParams");
		return;
	}
	else
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: punSetParamPayloadCache: 0x%p, Size: %lu malloc success",
				me->mtMxID, pCurrentInPort->punSetParamPayloadCache, paramSize);
	}

	//Copy the coefficients
	memscpy(pCurrentInPort->punSetParamPayloadCache, paramSize, (void*)(pParams),paramSize );

	//Update the port type
	pCurrentInPort->bIsDTSStream = TRUE;

	//For each connected output port, set the DTS params on the DTS Channel Mixer
	while (strMask)
	{
		unOutPortID = Q6_R_ct0_R(strMask);
		strMask ^= (1 << unOutPortID);
		pCurrentOutPort = me->outPortParams[unOutPortID];

		if(OUTPUT_PORT_STATE_INACTIVE != pCurrentOutPort->outPortState)
		{
			mt_mx_struct_channel_mixer_t *pCurrentInputOutputChMixer = &(pCurrentInPort->structChanMixer[unOutPortID]);
			if(FALSE == pCurrentInputOutputChMixer->bIsDTSMixerLibCreated)
			{
				MtMx_InOutPortsToCheckReInitChannelMixerLibrary(me, unInPortID, unOutPortID, FALSE);
			}
			if(TRUE == pCurrentInputOutputChMixer->bIsDTSMixerLibCreated)
			{
				pCurrentInputOutputChMixer->bCanChMixerBeOptimizedOut = FALSE;

				if(NULL != pCurrentInPort->punSetParamPayloadCache)
				{
					pCurrentInPort->nParamID = paramID;
					(void) MtMx_SetDtsDownmixerCfg(me, unInPortID, unOutPortID, paramID, pCurrentInPort->punSetParamPayloadCache);
				}

				if(NULL != pCurrentInputOutputChMixer->punCachedPsPdSetParam)
				{
					(void) MtMx_SetDtsDownmixerCfg(me, unInPortID, unOutPortID, pCurrentInputOutputChMixer->unCachedPsPdSetParamID, pCurrentInputOutputChMixer->punCachedPsPdSetParam);
				}
			}
			else
			{
				MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu o/p port %lu DTS Channelmixer library failed",
						me->mtMxID, unInPortID, unOutPortID);
			}
		}
	}
}

static ADSPResult MtMx_CreatePspdChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, eChMixerType eChannelMixerType)
{
	//This function creates the channel mixer. It assumes that the PSPD thread is already created.
	MatrixInPortInfoType                *pCurrentInPort                        = me->inPortParams[unInPortID];
	mt_mx_struct_channel_mixer_t *pCurrentInputOutputChMixer = &(pCurrentInPort->structChanMixer[unOutPortID]);
	ADSPResult result = ADSP_EOK;

	result = MtMx_CreateInitChannelMixer(me, unInPortID, unOutPortID, eChannelMixerType);
	if(ADSP_EOK != result)
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed MtMx_CreateInitChannelMixer; result = %d", result);
		goto __bailoutCreatePspdChannelMixer;
	}

	switch(eChannelMixerType)
	{
	case PSPD_CHMIXER_QCOM:
	{
		pCurrentInputOutputChMixer->bIsDTSMixerLibCreated = FALSE;
		pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated = TRUE;
		break;
	}
	case PSPD_CHMIXER_DTS:
	{
		pCurrentInputOutputChMixer->bIsDTSMixerLibCreated = TRUE;
		pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated = FALSE;
		break;
	}
	case PSPD_CHMIXER_NONE:
	{
		pCurrentInputOutputChMixer->bIsDTSMixerLibCreated = FALSE;
		pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated = FALSE;
		result = ADSP_EBADPARAM;
		break;
	}
	}

	pCurrentInputOutputChMixer->bCanChMixerBeOptimizedOut = TRUE;
	return result;

	__bailoutCreatePspdChannelMixer:
	pCurrentInputOutputChMixer->unCachedPsPdSetParamID = 0;
	pCurrentInputOutputChMixer->bIsDTSMixerLibCreated = FALSE;
	pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated = FALSE;
	pCurrentInputOutputChMixer->bCanChMixerBeOptimizedOut = TRUE;
	return result;
}

static ADSPResult MtMx_CreateInitChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, eChMixerType eChannelMixerType)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgChmixInout);

	MatrixInPortInfoType    *pCurrentInPort     = me->inPortParams[unInPortID];
	mt_mx_struct_pspd_t    *pCurrentPspd      = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdCfgChmixInout *payload = (EliteMsg_CustomPspdCfgChmixInout *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_CREATE_CHMIXER;

	//Fill up params.
	payload->param.bit_width =16;
	payload->param.num_in_ch = 2;
	payload->param.in_ch_map[0] = PCM_CHANNEL_L;
	payload->param.in_ch_map[1] = PCM_CHANNEL_R;
	payload->param.num_out_ch = 2;
	payload->param.out_ch_map[0] = PCM_CHANNEL_L;
	payload->param.out_ch_map[1] = PCM_CHANNEL_R;
	payload->param.eChannelMixerType = eChannelMixerType;

	//Send command to PSPD thread and wait for response.
	return MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
}

void MtMx_DestroyPspdChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	//This function destroys the channel mixer. It assumes that the PSPD thread is already created and not joined yet.
	MatrixInPortInfoType                *pCurrentInPort                        = me->inPortParams[unInPortID];
	mt_mx_struct_channel_mixer_t *pCurrentInputOutputChMixer = &(pCurrentInPort->structChanMixer[unOutPortID]);

	//Free up the APPI pointer
	if(pCurrentInputOutputChMixer->bIsDTSMixerLibCreated || pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated)
	{
		MtMx_DestroyChannelMixer(me, unInPortID, unOutPortID);
	}

	//Free up any malloced PSPD SetParam and reset the channel mixer structure params
	if(pCurrentInputOutputChMixer->punCachedPsPdSetParam)
	{
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Freeing punCachedPsPdSetParam 0x%p", me->mtMxID, pCurrentInputOutputChMixer->punCachedPsPdSetParam);
		MTMX_FREE(pCurrentInputOutputChMixer->punCachedPsPdSetParam);
	}
	pCurrentInputOutputChMixer->unCachedPsPdSetParamID = 0;
	pCurrentInputOutputChMixer->bIsDTSMixerLibCreated = FALSE;
	pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated = FALSE;
	pCurrentInputOutputChMixer->bCanChMixerBeOptimizedOut = TRUE;
}

static void MtMx_DestroyChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgChmixInout);

	MatrixInPortInfoType    *pCurrentInPort     = me->inPortParams[unInPortID];
	mt_mx_struct_pspd_t    *pCurrentPspd      = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return;
	}
	EliteMsg_CustomPspdCfgChmixInout *payload = (EliteMsg_CustomPspdCfgChmixInout *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_DESTROY_CHMIXER;

	//No params to send.

	//Send command to PSPD thread and wait for response.
	(void)MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
}

ADSPResult MtMx_SetDtsDownmixerCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, int32_t nCfgID, void* pData)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgDtsChmix);

	MatrixInPortInfoType  *pCurrentInPort  = me->inPortParams[unInPortID];
	mt_mx_struct_pspd_t  *pCurrentPspd   = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdCfgDtsChmix *payload = (EliteMsg_CustomPspdCfgDtsChmix *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_CFG_DTS_CHMIXER;

	//Fill up params.
	payload->cfg.config_id = nCfgID;
	payload->cfg.payload_ptr = pData;

	//Send command to PSPD thread and wait for response.
	return MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
}

ADSPResult MtMx_SetChannelMixerCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgChmixInout);

	MatrixInPortInfoType    *pCurrentInPort     = me->inPortParams[unInPortID];
	MatrixOutPortInfoType *pCurrentOutPort  = me->outPortParams[unOutPortID];
	mt_mx_struct_pspd_t    *pCurrentPspd      = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdCfgChmixInout *payload = (EliteMsg_CustomPspdCfgChmixInout *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_CFG_CHMIXER_INOUT;

	//Fill up params.
	payload->param.bit_width = (pCurrentInPort->unBitwidth)?(pCurrentInPort->unBitwidth):(16);
	payload->param.num_in_ch = (pCurrentInPort->unNumChannels)?(pCurrentInPort->unNumChannels):(2);
	payload->param.num_out_ch = (pCurrentOutPort->unNumChannels)?(pCurrentOutPort->unNumChannels):(2);

	if (0 == pCurrentInPort->unNumChannels)
	{
		payload->param.in_ch_map[0] = PCM_CHANNEL_L;
		payload->param.in_ch_map[1] = PCM_CHANNEL_R;
	}
	else
	{
		MtMx_CopyChannelMap(pCurrentInPort->unChannelMapping, payload->param.in_ch_map, pCurrentInPort->unNumChannels);
	}

	if (0 == pCurrentOutPort->unNumChannels)
	{
		payload->param.out_ch_map[0] = PCM_CHANNEL_L;
		payload->param.out_ch_map[1] = PCM_CHANNEL_R;
	}
	else
	{
		MtMx_CopyChannelMap(pCurrentOutPort->unChannelMapping, payload->param.out_ch_map, pCurrentOutPort->unNumChannels);
	}
	//This command does not set the channel mixer type. Therefore, payload->param.eChannelMixerType is not set here.

	//Send command to PSPD thread and wait for response.
	if(ADSP_EOK != (result = MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "PSPD channel mixer config command failed with error = %d", result);
		return result;	
	}

	//Update matrix KPPS and/or BW and raise ADSPPM event if needed.
	return(MtMx_SetReqKppsAndBW(me));
}

ADSPResult MtMx_SetChannelMixerCoef(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, audproc_chmixer_param_id_coeff_t *pCoefPayload)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdSetChmixCoef);

	MatrixInPortInfoType  *pCurrentInPort  = me->inPortParams[unInPortID];
	mt_mx_struct_pspd_t  *pCurrentPspd   = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdSetChmixCoef *payload = (EliteMsg_CustomPspdSetChmixCoef *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_SET_CHMIXER_COEF;

	//Fill up params.
	payload->coef = pCoefPayload;

	//Send command to PSPD thread and wait for response.
	return MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
}
