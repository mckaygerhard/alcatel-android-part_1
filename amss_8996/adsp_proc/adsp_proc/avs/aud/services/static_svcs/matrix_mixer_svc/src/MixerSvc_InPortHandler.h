/**
@file MixerSvc_InPortHandler.h
@brief This file declares functions that the audio matrix mixer
       uses to handle the arrival of a buffer at its input
       ports.

*/

/*========================================================================
Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_InPortHandler.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/04/2010 AAA     Created file.
========================================================================== */

#ifndef MIXER_SVC_IN_PORT_HANDLER_H
#define MIXER_SVC_IN_PORT_HANDLER_H


/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/

#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/*---------------------------------------------------------------------------
Function Declarations and Documentation
----------------------------------------------------------------------------*/
void MtMx_ProcessDataQ(This_t *me, uint32_t unInPortID);
void MtMx_MsgDataBuffer(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort);
void MtMx_MsgDataBufferHold(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort);
ADSPResult MtMx_InPortToHonorTimestamp(This_t *me, uint32 unInPortID);
void MtMx_HoldCommonRoutine(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort);
void MtMx_AddInterpolatedSamples(This_t *me, uint32 unInPortID);
void MtMx_DropSamples(This_t *me, uint32_t unInPortID);
void MtMx_CheckIfInportMovesAnyOutportToActiveState(This_t *ne, uint32_t unInPortID);
void MtMx_InPortUpdateMediaFmtParams(This_t *me, uint32_t unInPortID, uint32_t unSampleRate,
                                     uint32_t nNumChannels, uint32_t unBytesPerSample);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif //MIXER_SVC_IN_PORT_HANDLER_H
