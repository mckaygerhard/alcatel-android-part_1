/**
@file MixerSvc_ChanMixUtils.h
@brief This file declares channel mixer utility functions that 
       the audio matrix mixer uses.
*/

/*========================================================================
Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_ChanMixUtils.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
03/10/2014  kr      Re-factoring, De-coupling PSPD and ChannelMixer.
11/25/2013 rkc     Created file.
========================================================================== */

#ifndef MIXER_SVC_CHANMIX_UTILS_H
#define MIXER_SVC_CHANMIX_UTILS_H

#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"
#include "audio_basic_op.h"
#include "audio_basic_op_ext.h"
#include "MixerSvc_Util.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void MtMx_InOutPortsToCheckReInitChannelMixerLibrary(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, bool_t bOptzCheck);
void MtMx_SetDtsDownmixerParams(This_t *me, uint32_t unInPortID, uint32_t paramID, uint32_t paramSize, const uint8_t *pParams);
void MtMx_DestroyPspdChannelMixer(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);
ADSPResult MtMx_SetDtsDownmixerCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, int32_t nCfgID, void* pData);
ADSPResult MtMx_SetChannelMixerCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);
ADSPResult MtMx_SetChannelMixerCoef(This_t *me, uint32_t unInPortID, uint32_t unOutPortID, audproc_chmixer_param_id_coeff_t *pCoefPayload);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //MIXER_SVC_CHANMIX_UTILS_H

