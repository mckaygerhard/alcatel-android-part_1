/* ======================================================================== */
/**
@file appi_dts_dialnorm.h

   Header file to implement the Audio Post Processor Interface for DTS
   DialNorm algorithm.
*/

/* =========================================================================
  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   06/11/12   wjin      created file
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DTS_DIALNORM_H
#define __APPI_DTS_DIALNORM_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define APPI_PARAM_ID_DTS_DIALNORM_METADATA     0x12345678
#define APPI_PARAM_ID_DTS_DIALNORM_LBR_METADATA     0x12345679

ADSPResult appi_dts_dialnorm_getsize(
      /*[in]*/ const appi_buf_t*    params_ptr,
      /*[out]*/ uint32_t*           size_ptr);

ADSPResult appi_dts_dialnorm_init(
      /*[in]*/ appi_t*                _pif,
      /*[out]*/ bool_t*               pfInPlace,
      /*[in]*/ const appi_format_t*   pInFormat,
      /*[in]*/ appi_format_t*         pOutFormat,
      /*[in]*/ appi_buf_t*            pInfo);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //__APPI_DTS_DIALNORM_H

