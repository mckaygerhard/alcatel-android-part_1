#ifndef C_DTS_LBR_DECODER_LIB_H
#define C_DTS_LBR_DECODER_LIB_H

/* ========================================================================
  DTS Low Bit Rate decoder CAPI

 *//** @file CDTS_LBRDecoderLib.h
  This is a wrapper code for DTS Low Bit Rate decoder library.

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who            what, where, why
   --------   --------     -- ----------------------------------------------
   4/12/2012  ryalaman		CAPI for DTS Low BIt Rate decoder.

   ========================================================================= */


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
#include "Elite_CAPI.h"


class CDTS_LBRDecoderLib : public ICAPI
{

private:
	void* m_pDecInstance;

	/*Default constructor private*/
	CDTS_LBRDecoderLib ( );

	/*
	 * function to perform memory frees
	 */
	void CleanUp();

	/*
	 * initialize the decoder
	 */
	int InitDec();

    /*
     * Create, destroy and process functions for Downmix
     */
    int CreateDownMix();
    int DownMixProcess();
    int DestroyDownMix();

    /*
     * Copies the Post processing (downmix, dialnorm) config from decoder config to PP configs and indicates if there was a change.
     */
     void CopyPpConfig();

    /*
     * populates meta-data from internal structs into the format needed by decoder service. Memory for populating is given by dec svc.
     */
     void PopulateMetadata(uint8_t *metaDataPtr);

public:
	/* =======================================================================
	 *                          Public Function Declarations
	 * ======================================================================= */

	/**
	 * Constructor of CDTS_LBRDecoderLib that creates an instance of decoder lib
	 */
	CDTS_LBRDecoderLib ( ADSPResult    &nRes );

	/**
	 * Destructor of CDTS_LBRDecoderLib
	 */
	virtual ~CDTS_LBRDecoderLib ( );

	/*************************************************************************
	 * CAudioProcLib Methods
	 *************************************************************************/

	/**
	 * Initialize the core decoder library
	 *
	 * @return     success/failure is returned
	 */
	virtual int CDECL Init ( CAPI_Buf_t* pParams );

	/**
	 * Reinitialize the core decoder library in the case of repositioning or
	 * when full initialization is not required
	 *
	 * @return     success/failure is returned
	 */
	virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

	/**
	 * Gracefully exit the core decoder library
	 *
	 * @return     success/failure is returned
	 */
	virtual int CDECL End ( void );

	/**
	 * Get the value of the DTS broadcast decoder parameters
	 *
	 * @param[in]   nParamIdx      Enum value of the parameter of interest
	 * @param[out]  pnParamVal     Desired value of the parameter of interest
	 *
	 * @return   Success/fail
	 */
	virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

	/**
	 * Get the value of the DTS broadcast  decoder parameters
	 *
	 * @param[in]   nParamIdx      Enum value of the parameter of interest
	 * @param[out]  nPrarmVal      Desired value of the parameter of interest
	 *
	 * @return   Success/fail
	 */
	virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

	/**
	 * Decode audio bitstream and produce one frame worth of samples
	 *
	 * @param[in]   pInBitStream     Pointer to input bit stream
	 * @param[out]  pOutSamples      Pointer to output samples
	 * @param[out]  pOutParams       Pointer to output parameters
	 *
	 * @return     Success/failure
	 */
	virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
			CAPI_BufList_t*       pOutSamples,
			CAPI_Buf_t*       pOutParams );
};

#endif /* C_DTS_LBR_DECODER_LIB_H */

