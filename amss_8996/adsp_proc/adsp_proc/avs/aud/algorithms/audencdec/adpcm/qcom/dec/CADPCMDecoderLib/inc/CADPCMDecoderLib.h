#ifndef C_CAPI_ADPCM_DECODER_LIB_H
#define C_CAPI_ADPCM_DECODER_LIB_H

/* ========================================================================
   ADPCM decoder library wrapper header file

  *//** @file CADPCMDecoderLib.h
  This is a CAPI wrapper code for ADPCM Core decoder library.
  the function in this file are called by the CCapiADPCMDecoder media module.
  It is derived from CAudioProcLib class

  Copyright (c) 2008 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who            what, where, why
   --------   --------     -- ----------------------------------------------
   10/07/08   AP           Created this file for CAPI ADPCM Decoder

   ========================================================================= */


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
#include "Elite_CAPI.h"

   class CADPCMDecoderLib : public ICAPI
   {
public:
      enum ADPCMParamIdx
      {
         eADPCMNumBlockSize  = eIcapiMaxParamIndex,
         eADPCMBitsPerSample,
         _QIDL_PLACEHOLDER_ADPCMParamIdx = 0x7fffffff
      };

   private:

     void *m_pDecState;  

       /// number of bytes in data buffer passed to ADPCMDecoderLib DecodeFrame

   public:
   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Default Constructor of CADPCMDecoderLib
    */
   CADPCMDecoderLib ( );

   /**
    * Constructor of CADPCMDecoderLib that creats an instance of decoder lib
    */
   CADPCMDecoderLib ( ADSPResult    &nRes );

   /**
    * Destructor of CADPCMDecoderLib
    */
   virtual ~CADPCMDecoderLib ( );

   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core decoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core decoder library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core decoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the ADPCM decoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  pnParamVal      Desired value of the parameter of interest
    *
    * @return  Success/fail
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pParamVal );

   /**
    * Get the value of the ADPCM decoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nParamVal      Desired value of the parameter of interest
    *
    * @return  Success/fail
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pIndata,
                               CAPI_BufList_t*       pOutData,
                               CAPI_Buf_t*       pOutParams );
};

#endif /* C_CAPI_ADPCM_DECODER_LIB_H */

