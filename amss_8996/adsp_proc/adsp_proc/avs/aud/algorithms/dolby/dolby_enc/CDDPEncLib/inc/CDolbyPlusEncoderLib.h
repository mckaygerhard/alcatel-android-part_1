#ifndef C_DOLBYPLUS_ENCODER_LIB_H
#define C_DOLBYPLUS_ENCODER_LIB_H

/* ========================================================================
      DolbyPlus encoder library wrapper header file

 *//** @file CDolbyPlusEncoderLib.h
  This is a wrapper code for Dolbyplus Core decoder library.

  Copyright (c) 2011 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who          what, where, why
   --------   ---         ------------------------------------------------------
   06/24/11    WJ        Created file.
  ========================================================================= */

#include "Elite_CAPI.h"
#define  DDPENC_MAX_CHANNELS     6
#define  DDPENC_FRAME_SIZE       1536

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CDolbyplusEncoderLib : public ICAPI
{
public:

	void *m_pEncState;

	enum ddpEncParamIdx
	{
		ddpEncMode = eIcapiMaxParamIndex,
		ddpEncAcmod,
		ddpEncDataRate ,
		ddpEncLfeEnable,
		ddpEncPh90Filt,
		ddpEncGblDrcProf,
		ddpEncDailNorm,
		ddpEncEos

	};

	void *pm_ui32InpBuffer[ DDPENC_MAX_CHANNELS ];
	void *pm_ui32OutBuffer[1];                                  // TODO change it to double ptr
	uint32_t * m_ui32InpBuffer;
	uint32_t * m_ui32OutBuffer;


private:
	/**
	 * Default Constructor of CDolbyPulseDecoderLib
	 */
	CDolbyplusEncoderLib ( );


public:
	/* =======================================================================
	 *                          Public Function Declarations
	 * ======================================================================= */

	/**
	 * Constructor of CDolbyPulseDecoderLib that creats an instance
	 * of decoder lib and returns the error code
	 */

	CDolbyplusEncoderLib (uint16_t inputBitsPerSample, ADSPResult    &nResult );


	/**
	 * Destructor of CDolbyPulseDecoderLib
	 */
	~CDolbyplusEncoderLib ( );

	/*************************************************************************
	 * CDolbyPulseDecoderLib Methods
	 *************************************************************************/

	/**
	 * Initialize the core decoder library
	 *
	 * @return     success/failure is returned
	 */
	virtual int Init ( CAPI_Buf_t* pParams );

	/**
	 * Re initialize the core decoder library in the case of repositioning or
	 * when full initialization is not required
	 *
	 * @return     success/failure is returned
	 */
	virtual int ReInit ( CAPI_Buf_t* pParams );

	/**
	 * Gracefully exit the core decoder library
	 *
	 * @return     success/failure is returned
	 */
	virtual int End ( void );

	/**
	 * Get the value of the EaacPLus decoder parameters
	 *
	 * @param[in]   nParamIdx      Enum value of the parameter of interest
	 * @param[out]  pnParamVal     Desired value of the parameter of interest
	 *
	 * @return   Success/fail
	 */
	virtual int GetParam ( int nParamIdx, int *pnParamVal );

	/**
	 * Get the value of the EaacPLus decoder parameters
	 *
	 * @param[in]   nParamIdx      Enum value of the parameter of interest
	 * @param[out]  nPrarmVal      Desired value of the parameter of interest
	 *
	 * @return   Success/fail
	 */
	virtual int SetParam ( int nParamIdx, int nParamVal );

	virtual int Process ( const CAPI_BufList_t* pInBitStream,
			CAPI_BufList_t*       pOutSamples,
			CAPI_Buf_t*       pOutParams );

};


#endif /* C_DOLBYPLUS_ENCODER_LIB_H */

