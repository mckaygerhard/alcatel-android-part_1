/* ======================================================================== */
/**
   @file capi_v2_ds1ap.h

   Header file to implement the Common Audio Processor Interface v2 for DS1AP
*/

/* =========================================================================
   Copyright (c) 2014 QUALCOMM Technologies Incorporated.
   All rights reserved. Qualcomm Technologies Proprietary and Confidential.
   ========================================================================== */

/* =========================================================================
   Edit History

   when         who        what, where, why
   --------     ---        --------------------------------------------------
   11/19/14   adeepak      Initial creation
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef CAPI_V2_DS1AP_H
#define CAPI_V2_DS1AP_H

#include "Elite_CAPI_V2.h"

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

capi_v2_err_t __attribute__ ((visibility ("default"))) capi_v2_ds1ap_get_static_properties (
        capi_v2_proplist_t *init_set_properties,
        capi_v2_proplist_t *static_properties);



capi_v2_err_t __attribute__ ((visibility ("default"))) capi_v2_ds1ap_init (
        capi_v2_t*              _pif,
        capi_v2_proplist_t      *init_set_properties);
        
#ifdef __cplusplus
}
#endif

#endif // CAPI_V2_DS1AP_H
