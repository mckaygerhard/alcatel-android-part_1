#ifndef C_DTMF_GENERATOR_LIB_H
#define C_DTMF_GENERATOR_LIB_H

/* ========================================================================
   DTMF Generator library wrapper header file

  *//** @file CDtmfGeneratorLib.h
  This is a wrapper code for Dtmf Core Generator library.
  the function in this file are called by the CDtmfGenerator media module.
  

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who            what, where, why
   --------   --------     -- ----------------------------------------------
   3/27/10   Sudhir         Created file 

   ========================================================================= */


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
#include "Elite_CAPI.h"
#define  AUD_DEC_DTMF_OUT_BUF_SIZE  960  //60 msec of data at 8Khz
#define  AUD_DEC_ZEROS_AT_DTMF_TONE_END 32 //2msec of zeros at 8 Khz


class CDtmfGeneratorLib : public ICAPI
{

   private:
      void* m_pDecState;

      /*Default constructor private*/
      CDtmfGeneratorLib ( );


   public:
   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

      enum DtmfParamIdx
      {
         eDtmfHighTone = eIcapiMaxParamIndex,
         eDtmfLowTone,
         eDtmfDuration,
         eDtmfGain,
         eDtmfSet,
         eDtmfRunning
      };

      /**
       * Constructor of CDtmfGeneratorLib that creats an instance of Generator lib
       */
      CDtmfGeneratorLib ( ADSPResult    &nRes );

      /**
       * Destructor of CDtmfGeneratorLib
       */
      virtual ~CDtmfGeneratorLib ( );

      /*************************************************************************
       * CAudioProcLib Methods
       *************************************************************************/

      /**
       * Initialize the core Generator library
       *
       * @return     success/failure is returned
       */
      virtual int CDECL Init ( CAPI_Buf_t* pParams );

      /**
       * Re initialize the core Generator library in the case of repositioning or
       * when full initialization is not required
       *
       * @return     success/failure is returned
       */
      virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

      /**
       * Gracefully exit the core Generator library
       *
       * @return     success/failure is returned
       */
      virtual int CDECL End ( void );

      /**
       * Get the value of the DTMF Generator parameters
       *
       * @param[in]   nParamIdx      Enum value of the parameter of interest
       * @param[out]  pnParamVal     Desired value of the parameter of interest
       *
       * @return   Success/fail
       */
      virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

      /**
       * Get the value of the DTMF Generator parameters
       *
       * @param[in]   nParamIdx      Enum value of the parameter of interest
       * @param[out]  nPrarmVal      Desired value of the parameter of interest
       *
       * @return   Success/fail
       */
      virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

      /**
       * Decode audio bitstream and produce one frame worth of samples
       *
       * @param[in]   pInBitStream     Pointer to input bit stream
       * @param[out]  pOutSamples      Pointer to output samples
       * @param[out]  pOutParams       Pointer to output parameters
       *
       * @return     Success/failure
       */
      virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                                  CAPI_BufList_t*       pOutSamples,
                                  CAPI_Buf_t*       pOutParams );

};

#endif /* C_DTMF_GENERATOR_LIB_H */

