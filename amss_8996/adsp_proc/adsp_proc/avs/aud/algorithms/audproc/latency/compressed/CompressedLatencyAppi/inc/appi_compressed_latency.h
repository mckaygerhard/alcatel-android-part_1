/* ======================================================================== */
/**
@file appi_compressed_latency.h

   Header file to implement the Audio Post Processor Interface for 
   the latency module in compressed data path.
*/

/* =========================================================================
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   04/03/13   rbhatnk      Created file.
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_COMP_LATENCY_H
#define __APPI_COMP_LATENCY_H
#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_compressed_latency_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_compressed_latency_init(
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_COMP_LATENCY_H
