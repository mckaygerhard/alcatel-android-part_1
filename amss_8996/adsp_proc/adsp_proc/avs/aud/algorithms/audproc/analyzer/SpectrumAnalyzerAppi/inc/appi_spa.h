
/**
@file audproc_appi_spa.h

   Header file to implement the Audio Post Processor Interface
   for Spectrum Analyzer
*/

/*========================================================================

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/*========================================================================
Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
11/15/10   DG      Created file.

========================================================================== */

#ifndef AUDPROC_APPI_SPA_H
#define AUDPROC_APPI_SPA_H


/*----------------------------------------------------------------------------
 * Include files
 * -------------------------------------------------------------------------*/

#include "qurt_elite.h"
#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
 #define AUDPROC_PARAM_ID_SPA_QUEUE 0x1
 typedef struct _spa_queue_params_t
 {
    qurt_elite_queue_t *spa_input_queue_ptr;
    uint32_t mem_map_client;          // The memory map client required for physical to virtual mapping.
 } spa_queue_params_t;

/*----------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/

ADSPResult appi_spa_getsize( const appi_buf_t* params_ptr,
                           uint32_t* size_ptr);

ADSPResult appi_spa_init(
   appi_t* _pif,
   bool_t*                    is_in_place_ptr,
   const appi_format_t*       in_format_ptr,
   appi_format_t*             out_format_ptr,
   appi_buf_t*                info_ptr);


#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif // #ifndef AUDPROC_APPI_SPA_H

