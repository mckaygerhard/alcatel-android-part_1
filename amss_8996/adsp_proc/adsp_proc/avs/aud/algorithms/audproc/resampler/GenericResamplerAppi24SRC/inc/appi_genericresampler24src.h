/* ======================================================================== */
/**
@file appi_genericresampler24src.h

   Header file to implement the Audio Post Processor Interface for Generic
   Resampler
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   11/17/10   ss      Introducing APPI Rev B
   10/29/10   ss      Introducing Audio Post Processor Interface for Generic
                      Resampler
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_GENERICRESAMPLER24SRC_H
#define __APPI_GENERICRESAMPLER24SRC_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */
/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
#define NUM_RESAMPLERS_NEEDED(numChannels) (numChannels)

#define MULTI_RESAMP_MAX_CHANNELS 8
#define MAX_RESAMPLERS 8

#define AUDPROC_PARAM_ID_RESAMP_OUT_FREQ  1
#define AUDPROC_PARAM_ID_RESAMP_HEAP_ID  2

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_genericresampler24src_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_genericresampler24src_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif //__APPI_GENERICRESAMPLER24SRC_H

