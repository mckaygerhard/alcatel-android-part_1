/* ======================================================================== */
/**
@file appi_byteshifter.h

   Header file to implement the Audio Post Processor Interface for 
   shifting between 16 bit and 24 bit representations
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   11/03/11   RP      APPI wrapper
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_BYTESHIFTER_H
#define __APPI_BYTESHIFTER_H

#include "Elite_APPI.h"
#include "adsp_error_codes.h"

#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_byteshifter_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_byteshifter_init( 
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif

#endif //__APPI_BYTESHIFTER_H

