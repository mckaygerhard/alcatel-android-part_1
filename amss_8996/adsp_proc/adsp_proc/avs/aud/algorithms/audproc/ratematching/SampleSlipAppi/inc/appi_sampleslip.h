/* ======================================================================== */
/**
@file appi_sampleslip.h

   Header file to implement the Audio Post Processor Interface for 
   the sample slipping library.
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   10/27/11   dg      Created file.
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_SAMPLESLIP_H
#define __APPI_SAMPLESLIP_H

#include "Elite_APPI.h"
#include "AFEAvtDrift.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
enum ss_port_direction
{
   Playback,
   Record
};

const uint32_t AUDPROC_PARAM_ID_SAMPLESLIP_ENABLE = 1;
struct audproc_sampleslip_enable_t
{
   uint32_t enable; // 0 = disable, else enable
};

const uint32_t AUDPROC_PARAM_ID_SAMPLESLIP_DRIFT_POINTER = 2;
struct audproc_sampleslip_drift_pointer_t
{
	volatile const afe_drift_info_t *primary_drift_info_ptr;
	volatile const afe_drift_info_t *current_drift_info_ptr;
    volatile const int32_t *stream_to_device_drift_info_ptr; //This pointer to be set to NULL for regular drift correction on COPP path
};

const uint32_t AUDPROC_PARAM_ID_SAMPLESLIP_SAMPLE_ADJUST = 3;
struct audproc_sampleslip_sample_adjust_t
{
	int64_t  compensated_drift;		//compensated_drift value in us.
};
//	internal param id to communicate the compensated drift value to PP service
const uint32_t AUDPROC_PARAM_ID_SAMPLESLIP_DIRECTION = 3;
struct audproc_sampleslip_direction_t
{
   ss_port_direction direction;
};


/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_ss_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_ss_init(
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#endif //__APPI_SAMPLESLIP_H
