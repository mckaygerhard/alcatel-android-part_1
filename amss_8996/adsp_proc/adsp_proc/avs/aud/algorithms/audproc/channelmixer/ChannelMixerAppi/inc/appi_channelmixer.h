/* ======================================================================== */
/**
@file appi_channelmixer.h

   Header file for the channel mixer APPI module.
*/

/* =========================================================================
  Copyright (c) 2013-2014 QUALCOMM Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   08/09/13   rkc      Created the channel mixer APPI
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_CHANNELMIXER_H
#define __APPI_CHANNELMIXER_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/** Param ID for configuring the input/output of the channel mixer*/
#define CHANNELMIXER_PARAM_ID_CHAN_CONFIG   (0)

/** Param ID for setting mixer coefficients for a particular
 *  input-output channel configuration. */
#define CHANNELMIXER_PARAM_ID_COEF_SET      (1)

/** Param ID for Enabling/Disabling the channel mixer internally. */
#define CHANNELMIXER_PARAM_ID_ENABLE        (2)

/** Max num channels supported by channel mixer (input or
 *  output) */
#define APPI_CHANNELMIXER_MAX_CHAN          (8)

/** Payload for CHANNELMIXER_PARAM_ID_CONFIG */
typedef struct _appi_channel_mixer_chan_cfg_
{
   uint32_t      num_in_ch;                               /**< Num input channels. */
   uint8_t       in_ch_map[APPI_CHANNELMIXER_MAX_CHAN];   /**< Input channel mappings. */
   uint32_t      num_out_ch;                              /**< Num output channels. */
   uint8_t       out_ch_map[APPI_CHANNELMIXER_MAX_CHAN];  /**< Output channel mapping. */
   uint32_t      bit_width;                               /**< Bits per sample for input/output. */
} appi_channel_mixer_chan_cfg_t;

/** Definition of mixer coefficient set. */
typedef struct _appi_channel_mixer_coef_set_
{
   uint16_t      num_in_ch;                               /**< Num input channels. */
   uint16_t      *in_ch_map;                              /**< Input channel mappings. */
   uint16_t      num_out_ch;                              /**< Num output channels. */
   uint16_t      *out_ch_map;                             /**< Output channel mapping. */
   int16_t       *coef_ptr;                               /**< Mixing coefficients. */
} appi_channel_mixer_coef_set_t;

/** Payload for CHANNELMIXER_PARAM_ID_COEF_SET. */
typedef struct _appi_channel_mixer_coef_payload_
{
   uint16_t                          index;               /**< Index of the coef set in a mx of 8 sets. */
   appi_channel_mixer_coef_set_t     coef_set;            /**< Coefficient set. */
} appi_channel_mixer_coef_payload_t;

/** Payload for CHANNELMIXER_PARAM_ID_ENABLE. */
typedef struct _appi_channel_mixer_enable_payload_
{
   uint32_t                          enable;               /**< Defined the state of the module. */
} appi_channel_mixer_enable_payload_t;

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
  Function name: appi_channelmixer_getsize
  Returns the memory required by this algorithm.
 * -----------------------------------------------------------------------*/
ADSPResult appi_channelmixer_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

/*------------------------------------------------------------------------
  Function name: appi_channelmixer_init
  Initializes the channel mixer library as a pass through module.
 * -----------------------------------------------------------------------*/
ADSPResult appi_channelmixer_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif //__cplusplus
       
#endif //__appi_channelmixer_H


