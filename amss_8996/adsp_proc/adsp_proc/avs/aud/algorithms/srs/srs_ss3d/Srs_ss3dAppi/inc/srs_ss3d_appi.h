/********************************************************************************
 *    SRS Labs CONFIDENTIAL
 *    @Copyright 2012 by SRS Labs.
 *    All rights reserved.
 *
 *  Description:
 *  Project internal header file
 *
 *    Author: Darren Mohle
 *
 *  RCS keywords:
 *    $Id: //components/rel/avs.adsp/2.7.1.c4/aud/algorithms/srs/srs_ss3d/Srs_ss3dAppi/inc/srs_ss3d_appi.h#1 $
 *  $Author: pwbldsvc $
 *  $Date: 2016/06/05 $
 *    
********************************************************************************/

#ifndef __SRS_SS3D_APPI_H__
#define __SRS_SS3D_APPI_H__

#include "Elite_APPI.h"

#define SRS_SS3D_FILTER_MAX_SIZE 2752 
#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/
ADSPResult appi_srs_ss3d_getsize(const appi_buf_t *params_ptr, uint32_t* size_ptr);
ADSPResult appi_srs_ss3d_init(appi_t *_pif, bool_t *is_inplace_ptr, const appi_format_t *in_format_ptr, appi_format_t *out_format_ptr, appi_buf_t *info_ptr);
#ifdef __cplusplus
}
#endif /*__cplusplus*/


#endif /*__SRS_SS3D_APPI_H__*/
