
/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/Voice_CtmTx.cpp#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
08/05/2010     dp      Created
========================================================================== */
#include "Voice_CtmTx.h"
#include "VoiceLoggingUtils.h"
#include "VoiceCmnUtils.h"

#define VOICE_CTMTX_NB_KPPS (5180)
#define VOICE_CTMTX_WB_KPPS (6180)
#define VOICE_CTMTX_SWB_KPPS (VOICE_CTMTX_NB_KPPS + VOICE_RESAMPLER_KPPS_32K_TO_8K + VOICE_RESAMPLER_KPPS_8K_TO_32K)
#define VOICE_CTMTX_FB_KPPS (7180)
static const vcmn_sampling_kpps_t VOICE_CTMTX_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_CTMTX_NB_KPPS },
                                                                       {VOICE_WB_SAMPLING_RATE, VOICE_CTMTX_WB_KPPS },
                                                                       {VOICE_SWB_SAMPLING_RATE, VOICE_CTMTX_SWB_KPPS },
                                                                       {VOICE_FB_SAMPLING_RATE, VOICE_CTMTX_FB_KPPS },
                                                                       {VOICE_INVALID_SAMPLING_RATE, 0}};

static void voice_ctm_tx_free_local_memory(ctm_tx_struct_t* ctm_tx_struct_ptr);

extern "C" {
    #include "ctm_tx_api.h"
}

/* initialize Ctm module, allocate memory */
ADSPResult voice_ctm_tx_init(ctm_tx_struct_t* ctm_tx_struct_ptr, uint32_t session_id)
{
   ADSPResult result = ADSP_EOK;

   voice_ctm_tx_set_enable( ctm_tx_struct_ptr, FALSE);

   ctm_tx_struct_ptr->ctm_character_transmitted        = FALSE;
   ctm_tx_struct_ptr->enquiry_from_far_end_detected      = FALSE;
   ctm_tx_struct_ptr->ctm_from_far_end_detected          = FALSE;
   ctm_tx_struct_ptr->sync_recover_tx                  = FALSE;
   ctm_tx_struct_ptr->session_id                       = session_id;

   init_ctmTx(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr);

   return result;
}

ADSPResult voice_ctm_tx_resampler_init(ctm_tx_struct_t* ctm_tx_struct_ptr)
{
   // Freeup existing resampler memory, if any
   voice_ctm_tx_free_local_memory(ctm_tx_struct_ptr);
   // Resampler config for CTM Tx
   if(VOICE_NB_SAMPLING_RATE !=ctm_tx_struct_ptr->sampling_rate)
   {
     uint32_t out_frame_samples,in_frame_samples, sampling_rate;
     sampling_rate=(uint32_t)ctm_tx_struct_ptr->sampling_rate;
     in_frame_samples=(ctm_tx_struct_ptr->sampling_rate)*VOICE_FRAME_SIZE_WB/VOICE_WB_SAMPLING_RATE;
     voice_resampler_set_config(&(ctm_tx_struct_ptr->down_samp_config),sampling_rate,VOICE_NB_SAMPLING_RATE,NO_OF_BITS_PER_SAMPLE,in_frame_samples,&out_frame_samples);
     voice_resampler_set_config(&(ctm_tx_struct_ptr->up_samp_config),VOICE_NB_SAMPLING_RATE,sampling_rate,NO_OF_BITS_PER_SAMPLE,(VOICE_FRAME_SIZE_NB/2),&out_frame_samples);
     ctm_tx_struct_ptr->down_samp_mem_ptr = qurt_elite_memory_malloc( (ctm_tx_struct_ptr->down_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     ctm_tx_struct_ptr->up_samp_mem_ptr = qurt_elite_memory_malloc( (ctm_tx_struct_ptr->up_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     if ( NULL == ctm_tx_struct_ptr->down_samp_mem_ptr
           || NULL == ctm_tx_struct_ptr->up_samp_mem_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: Tx CTM init failed, out of memory");
      voice_ctm_tx_free_local_memory(ctm_tx_struct_ptr);
      return ADSP_ENOMEMORY;
   }
     // Initialize upsampler and downsampler needed for CTM
     int32_t resample_result;
     resample_result = voice_resampler_channel_init(
                         &(ctm_tx_struct_ptr->down_samp_config),
                         ctm_tx_struct_ptr->down_samp_mem_ptr,
                         (ctm_tx_struct_ptr->down_samp_config).total_channel_mem_size
                         );
     if(VOICE_RESAMPLE_SUCCESS != resample_result)
     {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Tx CTM down sampler init failed");
        voice_ctm_tx_free_local_memory(ctm_tx_struct_ptr);
        return ADSP_EFAILED;
     }
     resample_result = voice_resampler_channel_init(
                        &(ctm_tx_struct_ptr->up_samp_config),
                        ctm_tx_struct_ptr->up_samp_mem_ptr,
                        (ctm_tx_struct_ptr->up_samp_config).total_channel_mem_size
                        );
     if(VOICE_RESAMPLE_SUCCESS != resample_result)
     {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Tx CTM up sampler init failed");
        voice_ctm_tx_free_local_memory(ctm_tx_struct_ptr);
        return ADSP_EFAILED;
     }
   }

   return ADSP_EOK;

}

/* process 20 ms buffer, assume in place */
ADSPResult voice_ctm_tx_process(ctm_tx_struct_t* ctm_tx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples, uint16_t out_buff_size)
{
   ADSPResult result = ADSP_EOK;
   uint16_t input_samples_10ms = samples>>1;
   if( ctm_tx_struct_ptr->enable )
   {

      int8_t i;
      int16_t *in_curr_ptr = NULL;
      int16_t *out_curr_ptr = NULL;
      int16_t *det_curr_ptr = NULL;
      int16_t det_buf[VOICE_FRAME_SIZE_FB];
      int32_t bypassed;

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Tx CTM processing samples(%d)",samples);

      // boost the input by 12 dB to be consistent with CDMA TTY case
      // this level boosting is intended for detection only.
      for (int32_t j=0; j < samples; j++)
      {
         det_buf[j] = in_ptr[j];
      }

      // downsample if Wideband/Fullband operation
      if((VOICE_NB_SAMPLING_RATE != ctm_tx_struct_ptr->sampling_rate)&&( NULL != ctm_tx_struct_ptr->down_samp_mem_ptr ))
      {
         int32_t resample_result;
         resample_result = voice_resampler_process(
                            &(ctm_tx_struct_ptr->down_samp_config),
                            ctm_tx_struct_ptr->down_samp_mem_ptr,
                            (int8 *)det_buf,
                            (uint32)samples,
                            (int8 *)det_buf,
                            VOICE_FRAME_SIZE_NB
                            );
         if(VOICE_RESAMPLE_SUCCESS != resample_result)
         {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Tx CTM down sampler process failed");
            return ADSP_EFAILED;
         }
         samples = VOICE_FRAME_SIZE_NB;
      }


      in_curr_ptr  = in_ptr;            // may point to NB or WB data
      out_curr_ptr = out_ptr;            // may point to NB or WB data
      det_curr_ptr = det_buf;         // always points to NB data (orig NB or downsampled WB)

      assert( samples == VOICE_FRAME_SIZE_NB);  // should be 20 ms worth of data (considering NB and downsampled WB/FB case)

      // process CTM Tx in two 10 ms chunks.  CTM Tx algorithm doesn't allow more than 10 ms of NB data.
      // Input buffer is given to ctmtx_modulate since might do Audio bypass.  If modulator is operating it
      // will modulate NB CTM.  For WB this NB output needs to be upsampled.  If in bypass mode (bypassed returns true)
      // then ctmtx_modulate handles this for NB (copies 80 samples from in_curr_ptr to out_curr_ptr).  However for WB
      // case when bypass, it really needs to copy 160 samples.  So handle this with voice_memsmove.

      for( i = 0; i < 2; i++)
      {

         int16_t tty_code;

         MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: Tx CTM TTY pre-det/mod, ctm_char_tx(%ld),fEnqFromFarEndDet(%ld),fCtmFarEndDet(%ld)",
               ctm_tx_struct_ptr->ctm_character_transmitted,
               ctm_tx_struct_ptr->enquiry_from_far_end_detected,
               ctm_tx_struct_ptr->ctm_from_far_end_detected);

         // do TTY detection
         tty_code = ctmtx_ttydet(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr, det_curr_ptr, 80);

         if( tty_code != -1)
         {

#if defined(__qdsp6__) && !defined(SIM)

            // 5 bit TTY code doesn't have FIGS/LTRS info.  Add the info here.  full_tty_code[5:5] = figs_flag.  full_tty_code[4:0] = tty_code
            int16_t full_tty_code = ( (ctmtx_is_in_figs_mode(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr) << 5) | tty_code);

            // log tty character if available
            int8_t *bufptr[4] = { (int8_t *) &full_tty_code, NULL, NULL, NULL };
            voice_log_buffer( bufptr,
                  VOICE_LOG_CHAN_VSM_TX_CTM_CHAR,
                  (((VOICE_NB_SAMPLING_RATE != ctm_tx_struct_ptr->sampling_rate) << 3) | ctm_tx_struct_ptr->session_id),
                  qurt_elite_timer_get_time(),
                  VOICE_LOG_DATA_FORMAT_PCM_MONO,
                  (uint32_t)(ctm_tx_struct_ptr->sampling_rate),
                  sizeof(int8_t),
                  NULL);
#endif

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx CTM detected figs_flag(%d) tty_code(%d)", ctmtx_is_in_figs_mode(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr), tty_code);
         }

         (void) ctmtx_modulate(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr,
               in_curr_ptr,
               out_curr_ptr,
               80,
               &ctm_tx_struct_ptr->enquiry_from_far_end_detected,
               &ctm_tx_struct_ptr->ctm_character_transmitted,
               &bypassed,
               ctm_tx_struct_ptr->ctm_from_far_end_detected,
               FALSE,
               ctm_tx_struct_ptr->sync_recover_tx);

         ctm_tx_struct_ptr->sync_recover_tx = FALSE;

         if((VOICE_NB_SAMPLING_RATE != ctm_tx_struct_ptr->sampling_rate)&&( NULL != ctm_tx_struct_ptr->up_samp_mem_ptr ))
         {
            /* both cases should write 160/480 samples to out_curr_ptr[] = 10 ms WB/FB */
            if( FALSE == bypassed )
            {
               int32_t resample_result;
               resample_result = voice_resampler_process(
                                  &(ctm_tx_struct_ptr->up_samp_config),
                                  ctm_tx_struct_ptr->up_samp_mem_ptr,
                                  (int8 *)out_curr_ptr,
                                  80,
                                  (int8 *)out_curr_ptr,
                                  (uint32)input_samples_10ms
                                  );
                if(VOICE_RESAMPLE_SUCCESS !=resample_result)
                {
           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Tx CTM up sampler process failed");
                   return ADSP_EFAILED;
        }
            }
            else
            {
               memsmove( out_curr_ptr, out_buff_size, in_curr_ptr, input_samples_10ms*sizeof(int16_t));
            }
            in_curr_ptr  += input_samples_10ms;
            out_curr_ptr += input_samples_10ms;
         }
         else
         {
            /* NB case handled by ctmtx_modulate completely, will write 80 samples to out_curr_ptr[] = 10 ms NB */
            in_curr_ptr  += 80;
            out_curr_ptr += 80;
         }
         det_curr_ptr += 80;
      }
   }


   return result;
}

/* destroy Ctm module, free memory */
ADSPResult voice_ctm_tx_end(ctm_tx_struct_t* ctm_tx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   deinit_ctmTx(ctm_tx_struct_ptr->ctm_tx_struct_instance_ptr);

   voice_ctm_tx_free_local_memory(ctm_tx_struct_ptr);

   return result;
}

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_ctm_tx_set_enable(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8_t enable)
{
   ADSPResult result = ADSP_EOK;

   ctm_tx_struct_ptr->enable = enable;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx setting enable(%d)",enable);

   return result;
}

uint8_t voice_ctm_tx_get_enable(ctm_tx_struct_t* ctm_tx_struct_ptr)
{
   return ctm_tx_struct_ptr->enable;
}

/* set WB vs NB */
ADSPResult voice_ctm_tx_commit_format(ctm_tx_struct_t* ctm_tx_struct_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   ctm_tx_struct_ptr->sampling_rate = sampling_rate;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx setting sampling_rate(%d)",ctm_tx_struct_ptr->sampling_rate);
   return result;
}

/* set when CTM char_t transmitted */
ADSPResult voice_ctm_tx_get_ctm_char_transmitted(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8_t *ctm_char_tx)
{
   ADSPResult result = ADSP_EOK;

   *ctm_char_tx = ctm_tx_struct_ptr->ctm_character_transmitted;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx getting ctm_character_transmitted(%ld)",ctm_tx_struct_ptr->ctm_character_transmitted);
   return result;
}

/* set status of CTM detect on far end */
ADSPResult voice_ctm_tx_set_ctm_from_far_end_detected(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8_t ctm_from_far_end_detected)
{
   ADSPResult result = ADSP_EOK;

   ctm_tx_struct_ptr->ctm_from_far_end_detected = ctm_from_far_end_detected;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx setting ctm_from_far_end_detected(%d)",ctm_from_far_end_detected);
   return result;
}

/* set status of Enquiry detect from far end */
ADSPResult voice_ctm_tx_set_enquiry_from_far_end_detected(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8_t enquiry_from_far_end_detected)
{
   ADSPResult result = ADSP_EOK;

   ctm_tx_struct_ptr->enquiry_from_far_end_detected = enquiry_from_far_end_detected;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx setting enquiry_from_far_end_detected(%d)",enquiry_from_far_end_detected);
   return result;
}

/* resync CTM during handover */
ADSPResult voice_ctm_tx_resync(ctm_tx_struct_t* ctm_tx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   ctm_tx_struct_ptr->sync_recover_tx = TRUE;
   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMTx Resync");
   return result;
}

static void voice_ctm_tx_free_local_memory(ctm_tx_struct_t* ctm_tx_struct_ptr)
{

   // Destroy Up/Downsamplers
   if(NULL != ctm_tx_struct_ptr->down_samp_mem_ptr)
   {
      qurt_elite_memory_free(ctm_tx_struct_ptr->down_samp_mem_ptr);
      ctm_tx_struct_ptr->down_samp_mem_ptr = NULL;
   }
   if(NULL != ctm_tx_struct_ptr->up_samp_mem_ptr)
   {
      qurt_elite_memory_free(ctm_tx_struct_ptr->up_samp_mem_ptr );
      ctm_tx_struct_ptr->up_samp_mem_ptr = NULL;
   }
}

ADSPResult voice_ctm_tx_get_kpps(ctm_tx_struct_t* ctm_tx_struct_ptr,uint32_t* kpps_ptr, uint8_t enable, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= ctm_tx_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if( enable )
      {
         result = vcmn_find_kpps_table(VOICE_CTMTX_SAMPLING_KPPS_TABLE, sampling_rate, kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

