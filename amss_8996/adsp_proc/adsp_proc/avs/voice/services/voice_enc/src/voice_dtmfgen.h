#ifndef DTMFGENERATION_H
#define DTMFGENERATION_H

/*========================================================================
ELite

This file contains class belonging to the DTMF generation.

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
                             Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/08/10   ss      C++ to C wrapper changes
========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */

#include "qurt_elite.h"
#include "voice_resampler.h"

extern "C" {
   #include "dtmf_gen_api.h"
   #include "VolumeControl_api.h"
}
#if  defined(__qdsp6__)
#define  voice_dtmf_gen_add_sat(x, y)   ((int16)Q6_R_add_RlRl_sat( (x), (y) ))
#define  voice_dtmf_gen_out_shr(x,n)    ((int16)Q6_R_sath_R(Q6_R_asr_RR_sat( (x),(n) )))
#else

#endif

#define  VOICE_DTMF_GEN_CONTINUOUS   -1
#define  VOICE_DTMF_GEN_STOP          0

/** @brief Structure for the wrapper of DTMF generation
*/
typedef struct
{
   int32_t current_dtmf_state;
   /**< To evaluate state of DTMF engine */
   DTMFGenStruct dtmf_generator;
   /**< structure for DTMF gen lib */
   SetOfSamplingFreqs sampling_freq;
   /**< Sampling frequencies supported. */
   bool_t second_dtmf_command;
   /**< A Second DTMF Command issued, while one is already running.
        The first one will have to be ramped down before the
        second one is executed (ramped up)
   */
   int32_t dtmf_gen_last_block;
   /**< Indicator for last block of samples. */
   uint16_t dtmf_gen_mix_flag;
   /**< To enable mixing Speech and DTMF. */
   int16_t second_dtmf_samples_in_block1;
   /**< Variable Required in case of Second DTMF Command issued*/
   uint16_t dtmf_gen_gain;
   /**< DTMF gen gain */
   uint16_t dtmf_gen_tone1;
   /**< First tone of DTMF. */
   uint16_t dtmf_gen_tone2;
   /**< Second tone of DTMF */
   MicroSecs duration_in_micro_secs;
   /**< Duration of DTMF tone */
   voice_resampler_config_struct_t dtmf_resampler_cfg_str;
   /**< Adding voice resampler for dtmf tone generation at 48kHz*/
   void *dtmf_resampler_channel_mem_ptr;
   /**<pointer to channel memory*/
} dtmf_gen_struct_t;

/** The function that allocates memory(for 8->48) for resampler and default init for DTMF generation
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @return An indication of success or failure
*/
ADSPResult voice_dtmf_resampler_init(dtmf_gen_struct_t *dtmf_gen_ptr, uint32 sampling_rate);
/** The function that free up memory allocated for resampler
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @return An indication of success or failure
*/
void voice_dtmf_resampler_mem_free(dtmf_gen_struct_t *dtmf_gen_ptr);
/** The function that initializes the default values for DTMF generation
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @return An indication of success or failure
*/
ADSPResult voice_dtmf_gen_default_init(dtmf_gen_struct_t *dtmf_gen_ptr);
/** The function that process for DTMF generation
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @param [in] in_ptr pointer to the input buffer.
    @param [out] out_ptr pointer to the input buffer.
    @param [in] in_size frame size.
    @return An indication of success or failure
*/
ADSPResult voice_dtmf_gen_process(dtmf_gen_struct_t *dtmf_gen_ptr,int16_t *in_ptr,int16_t *out_ptr, int16_t in_size);

/** The function that process for DTMF generation
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @param [in] tone1 first tone of DTMF gen.
    @param [in] tone2 first tone of DTMF gen.
    @param [in] duration: lenght of the DTMF gen tone in msecs.
    @param [in] dtmf_mix_enable: enable or disable mixing with speech.
    @param [in] dtmf_gain Gain for DTMF generation tone.
    @return An indication of success or failure
*/
ADSPResult voice_dtmf_gen_start(dtmf_gen_struct_t *dtmf_gen_ptr,uint16_t tone1,uint16_t tone2,int32_t duration,
                                uint16_t dtmf_mix_enable,uint16_t dtmf_gain);
/** The function that resets the default values for DTMF generation
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @return An indication of success or failure
*/
ADSPResult voice_dtmf_gen_process_end(dtmf_gen_struct_t *dtmf_gen_ptr);

/** The function that returns the KPPS required for DTMF functionality.
    @param [in/out] dtmf_gen_ptr pointer to the DTMF generation wrapper structure.
    @param [in/out] KPPS.
*/
ADSPResult voice_dtmf_gen_get_kpps(dtmf_gen_struct_t *dtmf_gen_ptr,uint32_t* kpps_ptr, uint16_t sampling_rate);

#endif /* DTMFGENERATION_H */

