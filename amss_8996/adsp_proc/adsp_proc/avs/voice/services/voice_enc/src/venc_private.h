/*========================================================================

*//** @file VoiceEnc.cpp

Copyright (c) 2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

This file contains the code for Voice Encoder (Venc) Dynamic service. This service
has one thread. It receives commands from Voice Stream Manager (VSM).
Venc initializes the vocoder algorithms and processes the data. Multiple Venc can be
instantiated and each of them is identified by a session number provided
while creating.
*//*====================================================================== */

/*========================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/venc_private.h#1 $

  when       who     what, where, why
  --------   ---     -------------------------------------------------------
  10/30/12  ss   Created file.
  ========================================================================== */
#include "venc_svc.h"

#include "Elite.h"
#include "VoiceTimerMsgs.h"
#include "EliteMsg_Util.h"
#include "voice_cng.h"
#include "voice_dtmfgen.h"
#include "VoiceCmnUtils.h"
#include "VoiceGenResampler.h"
#include "Voice_CtmTx.h"
#include "Voice_OOBTTYTx.h"
#include "Voice_SampleSlip.h"
#include "AFEInterface.h"
#include "Voice_HostPcm.h"
#if defined(__qdsp6__) && !defined(SIM)
#include "VoiceLoggingUtils.h"
#endif
#include "VoiceTimer.h"
#include "voice_delivery.h"

/* APR/API related */
#include "EliteAprIf.h"
#include "apr_comdef.h"
#include "adsp_vsm_api.h"
#include "adsp_vcmn_api.h"
#include "adsp_vparams_api.h"
#include "adsp_media_fmt.h"
#include "fourgv_enc_api.h"

extern "C" {
#include "evrc_enc_api.h"
#include "v13k_enc_api.h"
#include "tty_api.h"
#include "efr_enc_api.h"
#include "fr_enc_api.h"
#include "hr_enc_api.h"
#include "ctm_tx_api.h"
#include "amrwb_enc_api.h"
#include "g711_api.h"
#include "voice_resampler.h"
#include "g729a_enc_api.h"
#include "voice_cross_fade_api.h"
#include "eamr_enc_api.h"
#include "limiter24_api.h"
#include "evs_enc_api.h"
}

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

// This contains all the required data for a service instance. Like the data members of a class...
static const uint32_t NAME_LEN = QURT_ELITE_DEFAULT_NAME_LEN;

#define LIMITER_PARAM_SIZE 8   // Limter configuration parameter size

// Vocoder delays in microseconds
#define VOICE_DELAY_AMR_NB      (5000)
#define VOICE_DELAY_AMR_WB      (6000)
#define VOICE_DELAY_EFR         (0)
#define VOICE_DELAY_HR          (4000)
#define VOICE_DELAY_FR          (0)
#define VOICE_DELAY_EVRC        (10000)
#define VOICE_DELAY_V13K        (7500)
#define VOICE_DELAY_4GV_NB      (11125)
#define VOICE_DELAY_4GV_WB      (15313)
#define VOICE_DELAY_4GV_NW      (15313)
#define VOICE_DELAY_4GV_NW2K    (18000)
#define VOICE_DELAY_EAMR        (15125)
#define VOICE_DELAY_EVS         (12000)

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/
typedef struct
{
   int8_t                   *start_addr_ptr;
   int8_t                   *usage_addr_ptr;
   uint32_t                 size;
} venc_memory_t;

// Structure for managing Record
typedef struct venc_rec_info_t
{
   // Book keeping
   bool_t                   enable;                       // flag to indicate whether recording is enabled or not
   uint32_t                 mode;                         // in call recording mode
   uint32_t                 bufs_allocated;               // Keep track of number of output bufs
   int8_t                   first_frame;                  // to indicate recording first frame

   // Port Handles
   elite_svc_handle_t       *afe_handle_ptr;              // handle to push the data for recording
   void                     *afe_drift_ptr;               // for afe drift in record
   uint16_t                 aud_ref_port;

   // Queues
   qurt_elite_queue_t       *buf_q_ptr;                   // recording buffer queue
   char_t                   buf_q_name[NAME_LEN];         // output buffer queue name

   // Buffers
   circbuf_struct           circ_struct_encout;           // circular buffer for encoder input
   int8_t                   *circ_encout_buf_ptr;         // pointer to cicular buffer

   // Drift handling
   voice_cmn_drift_info_t   voice_cmn_drift_info;         // structure for av/hp timer vs device drift tracking
   int8_t                   *ss_struct_ptr;               // pointer to sample slip structure
   ss_struct_type_t         ss_struct;                    // Structure for Sample Slip algorithm
   uint8_t                  ss_multiframe_counter;        // Indicate number of 10 msec frames left before present drift correction is done
   int32_t                  ss_info_counter;              // Indicates the number of samples to be slipped or stuffed

} venc_rec_info_t;

// Structure for managing Out of band processing
typedef struct venc_oob_info_t
{
   int32_t                  *pkt_exchange_ptr;            // Out of band packet exchange pointer
   bool                     enc_pkt_consumed;             // flag to know enc pkt consumed by client or not
   uint32_t                 pkt_miss_ctr;                 // stores the count of un succesful OOB pkt delivery due to previous pkt not read by client
   uint32_t                 pkt_consumed_ctr;             // stores the count of pkt consumed by client
   elite_mem_shared_memory_map_t
                            memmap;                       // stores memory address used by VENC for outof band pkt exchange handling
} venc_oob_info_t;

// Structure for managing Vocoders
typedef struct venc_voc_info_t
{
   FourGVEncode             *four_gv_encode_ptr;
   int16_t                  g711_frame;
   int16_t                  g711_frame_size;              // g711 frame size; 10ms or 20ms
   int8_t                   *g729a_encoder_ptr;           // G729A enc static mem ptr
   int8_t                   *g729a_common_ptr;            // G729A enc common static mem ptr
   uint16_t                 min_rate;                     // min rate for 1x vocoders
   uint16_t                 max_rate;                     // max rate for 1x vocoders
   uint32_t                 redu_rate;                    // reduced rate level used in v13k only
   uint32_t                 rate_mod;                     // rate modulation cmd used in v13k and evrc only
   uint32_t                 fgv_nb_avg_rate;              // cop used in 4gv-nb
   uint32_t                 fgv_wb_avg_rate;              // cop used in 4gv-wb
   uint32_t                 fgv_nw_avg_rate;              // cop used in 4gv-nw
   uint32_t                 fgv_nw_2k_avg_rate;           //cop used in 4gv-nw-2k/evrc-nw-2k
   uint32_t                 fgv_avg_rate_control;         // avg_rate_control for 4gv
   uint32_t                 amr_nb_mode;                  // encoding rate used in amr-nb
   uint32_t                 amr_wb_mode;                  // encoding rate used in amr-wb
   uint32_t                 dtx_mode;                     // DTX mode for AMR-NB, AMR-WB, FR, HR and EFR
   uint32_t                 prev_amr_nb_mode;             // caching to the rate to fix the CMI CMC rate change issue
   uint32_t                 prev_amr_wb_mode;             // caching to the rate to fix the CMI CMC rate change issue
   uint32_t                 vocoder_op_mode;              // Current operating mode of vocoder
   uint32_t                 vocoder_op_detection;         // Enable/disable for vocoder mode event
   evs_enc_param_struct         evs_param_struct;
} venc_voc_info_t;

// Structure for managing TTY
typedef struct venc_tty_info_t
{
   /* 1x TTY related variables */
   TTYEncStruct             enc_struct;                   // TTY encoder structure
   bool_t                   option;                       // TTY enable, disable flag
   void                     *onex_downby2_mem_ptr;        // downsampler for WB mode to input to tty_enc
   voice_strm_tty_state_t   *state_ptr;                   // pointer to cmn struct used by tx and rx
   voice_resampler_config_struct_t
                            resamp_config;                // TTY encoder resampler config structure
} venc_tty_info_t;

// Structure for managing TTY
typedef struct venc_ctm_info_t
{
   ctm_tx_struct_t          state;                        // Ctm structure
   uint16_t                 resync_delay_cnt;             // wait for these many counts before resyncing ctm, each count is 20ms
   uint16_t                 resync_afe;                   // wait for these many counts before resyncing ctm, each count is 20ms
   uint16_t                 resync_modem_afe;             // unify modem resync and afe resync for HO cases
} venc_ctm_info_t;

// Structure for managing Pre-processing blocks
typedef struct venc_proc_info_t
{
   CComfortNoise            *comfort_noise_ptr;
   CLimiterLib*             limiter_ptr;
   int32_t                  limiter_params[LIMITER_PARAM_SIZE];
   dtmf_gen_struct_t        *dtmf_gen_ptr;
   voice_strm_apr_info_t    apr_info_dtmf_gen;            // info to send DTMF gen ended event
   CrossFadeCfgType         cross_fade_cfg_struct;
   CrossFadeDataType        cross_fade_data_struct;
   voice_host_pcm_context_t host_pcm_context;             // Structure for Host PCM processing
   int16_t                  *mute_inp_ptr;                //temp memory required for soft mute
   voice_generic_resampler_t
                            codec_resampler;              // used for rate conversion of the sampling rate of the vocoder to that of the stream PP(before widevoice).
} venc_proc_info_t;

// Structure for managing Pre-processing blocks
typedef struct venc_io_info_t
{
   /* Input buffer */
   int16_t                  *in_buf;                      // local input buffer, todo: remove hardcoding
   uint32_t                 in_buf_size;                  // input buffer size
   /* Output buffer */
   int16_t                  *out_buf;                     // local output buffer, todo: remove hardcoding
   uint32_t                 out_buf_size;                 // output buffer size
   uint16_t                 frame_samples;                // number of samples in a frame
} venc_io_info_t;

// Structure for Managing Voice Encoder
typedef struct venc_t
{
   // Voice Encoder thread specifics
   elite_svc_handle_t       svc_handle;                   // handle to give out to others - first element
   elite_svc_handle_t       *downstream_peer_ptr;         // This service only supports 1 downstream peer
   elite_svc_handle_t       *loopback_handle;             // handle to rx session for loopback mode
   elite_svc_handle_t       *vds_handle_ptr;              // pointer to  VDS handle
   qurt_elite_queue_t       *buf_q_ptr;                   // output buffer queue
   qurt_elite_queue_t       *vtm_cmd_q_ptr;               // pointer to  voice timer cmd Q
   qurt_elite_queue_t       *resp_q_ptr;                  // Queue for handling responses
   qurt_elite_channel_t     qurt_elite_channel;           // Channel for the queues
   char_t                   data_q_name[NAME_LEN];        // input data queue name
   char_t                   cmd_q_name[NAME_LEN];         // input command queue name
   char_t                   buf_q_name[NAME_LEN];         // output buffer queue name
   uint32_t                 wait_mask;                    // Current wait mask
   uint32_t                 rcvd_mask;                    // Received mask
   int32_t                  bit_pos;                      // Bit Position
   uint32_t                 session_num;                  // Session Number

   // Voice Encoder feature structures
   venc_memory_t            memory;                       // Structure for handling memory
   venc_rec_info_t          record;                       // Structure for handling Record
   venc_oob_info_t          oob;                          // Structure for handling Out of Band data
   venc_voc_info_t          voc;                          // Structure for handling Vocoders
   venc_tty_info_t          tty;                          // Structure for handling TTY
   oobtty_tx_struct_t  oobtty_tx_struct;                  // Structure for LTE TTY 
   venc_ctm_info_t          ctm;                          // Structure for handling CTM
   venc_proc_info_t         proc;                         // Structure for handling all processing blocks
   venc_io_info_t           io;                           // Structure for handling In/Out

   // Voice Encoder states
   voice_cmn_time_profile_t time_profile_struct;          // collect timing stats
   int64_t*                 enc_state_ptr;
   int8_t                   process_data;                 // flag indicating the run and running state
   int32_t                  bufs_allocated;               // Keep track of number of output bufs in circulation
   int8_t                   loopback_enable;              // flag to indicate if enc packets should be
   uint16_t                 samp_rate;                    // stores current sampling rate from MediaType message
   uint8_t                  samp_rate_factor;
   uint32_t                 voc_type;                     // vocoder type
   uint16_t                 mute;                         // mute command (0 - Unmute, 1 - Mute, 2 - CNG enable)
   int8_t                   reset_voc_flag;               // flag to indicate if vocoder should be reset next Run()
   uint32_t                 pkt_exchange_mode;            // pkt_exchange_mode
   int8_t                   pkt_ready;                    // flag used to check if pkt is ready for delivery
   uint32_t                 pkt_ctr;                      // count packets, used as client token when sending Enc packets
   uint32_t                 pkt_miss_ctr;                 // counter to track pkt misses
   uint32_t                 vfr_mode;                     // VFR mode (VFR_NONE = 0,
   uint8_t                  vfr_source;
   bool_t                   send_media_type;
   uint32_t                 aggregate_kpps;               // KPPS of all the enabled modules combined.
   uint32_t                 aggregate_delay;              // Delay of all the enabled modules combined.

   // Others
   voice_strm_apr_info_t    *apr_info_ptr;                // info to send/process apr messages with client
   Vtm_SubUnsubMsg_t        vtm_sub_unsub_data;           // msg payload used to subscribe/unsubscribe timer
   uint32_t                 vds_client_id;                // client id returned by VDS
   uint32_t                 vds_client_token;             // client token sent to VDS
   char_t                   respq_name[NAME_LEN];         // response queue name
   bool                     pkt_delivery_pending;         // Flag is used to send pkt to VDS after pkt is ready and host consumed prev pkt
   qurt_elite_signal_t      *vds_error_signal_ptr;        // Error signal for VDS to respond back when delivery fails
   uint32_t                 vsid;                         // voice system id for hard vfr timing //PGtodo: refactor further
} venc_t;

