/*========================================================================

*//** @file VoiceEnc.cpp

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

This file contains the code for Voice Encoder (Venc) Dynamic service. This service
has one thread. It receives commands from Voice Stream Manager (VSM).
Venc initializes the vocoder algorithms and processes the data. Multiple Venc can be
instantiated and each of them is identified by a session number provided
while creating.
*//*====================================================================== */

/*========================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/venc_svc.cpp#1 $

  when       who     what, where, why
  --------   ---     -------------------------------------------------------
  10/26/10  pg    use VAD1
  06/25/10  pg   Major cleanup
  06/03/10  pg   Merge features from main to 8660DevAprV2
  EFR, FR, HR, PCM_NB, 4GV_NB, V13k, G711 A/MLAW, 4GV_WB,
  ENC-DEC pkt loopback
  11/16/09  pg   Created file.
  ========================================================================== */

/* =======================================================================
   INCLUDE FILES FOR MODULE
   ========================================================================== */
#include "venc_private.h"
/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

// Masks for the signals and queues
#define VENC_RESYNC_MASK            0x80000000
#define VENC_TICK_MASK              0x40000000
#define VENC_CMD_MASK               0x20000000
#define VENC_BUF_MASK               0X10000000
#define VENC_REC_BUF_MASK           0x08000000
#define VENC_DATA_MASK              0x04000000
#define VENC_TICK_END_MASK          0x02000000
#define VENC_RESPONSE_MASK          0x01000000
#define VENC_VDS_RESPONSE_MASK      0x00800000
#define VENC_CMD_DATA_TICK_MASK     (VENC_TICK_MASK | VENC_CMD_MASK | VENC_DATA_MASK | VENC_RESYNC_MASK | VENC_VDS_RESPONSE_MASK)
#define VENC_ALL_MASK               (VENC_TICK_MASK | VENC_CMD_MASK | VENC_DATA_MASK | VENC_TICK_END_MASK | VENC_REC_BUF_MASK | VENC_RESYNC_MASK | VENC_VDS_RESPONSE_MASK)
#define VENC_CMD_DATA_MASK          (VENC_CMD_MASK | VENC_DATA_MASK )
#define FULL_RATE                   4
#define HALF_RATE                   3
#define QUARTER_RATE                2
#define SILENCE_RATE                1
#define ENC_STATE_MEMORY_IN_BYTES   100*1024 // worst case for EVS encoder

#define NO_OF_CHANNELS  1       // MONO. It is used to allocate memory for Limiter buffers.
#define VENC_SS_MULTIFRAME   VOICE_SS_MULTI_FRAME_4


// TODO: Remove when API updated
/**  Parameter for enabling internal loopback feature for a particular session.
  Fixed size of LOOPBACK_ENABLE_PARAM_BYTES = 4 bytes.
 */
#define VOICE_PARAM_LOOPBACK_ENABLE (0x00010E18)


// externs
extern uint32_t vsm_memory_map_client;


/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */
// Maximum data messages in data queue. Expectation is upstream svc will send 20ms buffers.
static const uint32_t MAX_DATA_Q_ELEMENTS = 2;

// Maximum number of commands expected ever in command queue.
static const uint32_t MAX_CMD_Q_ELEMENTS = 8;

// How many buffers in recording buffer queue? May need to make this configurable...
static const uint32_t MAX_REC_BUF_Q_ELEMENTS = 3;

// maximum number of commands expected ever in response queue.
static const uint32_t MAX_RESP_Q_ELEMENTS = 2;

// Buffer size. Assume service knows what size buffer it needs to deliver.
// If it needs to be told, have to add a command to be informed.
static const uint32_t REC_BUFFER_SIZE = 2*(1920 + 192);
//22 ms for worst case for each set in stereo mode

// Thread name
static char_t aTHREAD_NAME[6] = {'V','E','C','$','\0'};

// Thread stack size
static const uint32_t THREAD_STACK_SIZE = 75*1024;

// todo: move these to api hdr
static const uint32_t G729AB_ENC_PKT_SIZE_IN_WORDS=11;
static const uint32_t G729AB_ENC_PKT_SIZE_IN_WORDS_10MSEC=6;
static const uint32_t fgv_nb_cop_avg_rate_table[] = {10000, 8500, 7500, 7000, 6600, 6200, 5800, 4800};
static const uint32_t fgv_wb_cop_avg_rate_table[] = {8500, 0, 0, 0, 10000, 0, 0, 4800}; //only 0,4,7 COP values are defined
// average rate table for EVRC-NW modem
static const uint32_t fgv_nw_cop_avg_rate_table[] = {8500, 10000, 7500, 7000, 6600, 6200, 5800, 4800};
static const uint32_t fgv_nw_2k_cop_avg_rate_table[] = {8500, 10000, 7500, 2400, 6600, 6200, 5800, 4800};   // SO77 table with COP3 replaced with 2400 kbps
static uint32_t evs_find_mode_rate(uint32_t  mode_rate);

static const uint32_t evs_mode_bandwidth_table[][2] = {
   {VSM_EVS_VOC_BANDWIDTH_NB             , VSM_VOC_OPERATING_MODE_NB},
   {VSM_EVS_VOC_BANDWIDTH_WB             , VSM_VOC_OPERATING_MODE_WB},
   {VSM_EVS_VOC_BANDWIDTH_SWB            , VSM_VOC_OPERATING_MODE_SWB},
   {VSM_EVS_VOC_BANDWIDTH_FB             , VSM_VOC_OPERATING_MODE_FB},
   {4                                    , VSM_VOC_OPERATING_MODE_NONE}
};

static const uint32_t evs_mode_rate_table[][2] = {
   {0             ,6600 },
   {1             ,8850 },
   {2             ,12650 },
   {3             ,14250 },
   {4             ,15850 },
   {5             ,18250 },
   {6             ,19850 },
   {7             ,23050 },
   {8             ,23850 },
   {9             ,5900 },
   {10            ,7200 },
   {11            ,8000 },
   {12            ,9600 },
   {13            ,13200},
   {14            ,16400},
   {15            ,24400},
   {16            ,32000},
   {17            ,48000},
   {18            ,64000},
   {19            ,96000},
   {20            ,128000},
   {21           , NULL}
};

static const uint32_t evs_pkt_size_rate_table[][2] = {
   {0             ,17 },
   {1             ,23 },
   {2             ,32 },
   {3             ,36 },
   {4             ,40 },
   {5             ,46 },
   {6             ,50 },
   {7             ,58 },
   {8             ,60 },
   {9             ,5 },
   {14            ,0},
   {15            ,0},
   {16            ,7},
   {17            ,18},
   {18            ,20},
   {19            ,24},
   {20            ,33},
   {21           , 41},
   {22            ,61},
   {23            ,80},
   {24            ,120},
   {25           , 160},
   {26            ,240},
   {27            ,320},
   {28           , 6},
   {29           , -1}
};


static uint32_t evs_find_pkt_size(uint32_t  header_payload)
{
   int16_t i = 0;
   while( 29 != evs_pkt_size_rate_table[i][0])
   {
      if (evs_pkt_size_rate_table[i][0] == header_payload)
      {
         return(evs_pkt_size_rate_table[i][1]);
      }
      i++;
   }
   return 0;
}

static uint32_t evs_find_operating_bandwidth(uint32_t  mode_bandwidth)
{
   int16_t i = 0;
   while( 4 != evs_mode_bandwidth_table[i][0])
   {
      if (evs_mode_bandwidth_table[i][0] == mode_bandwidth)
      {
         return(evs_mode_bandwidth_table[i][1]);
      }
      i++;
   }
   return 0;
}

static uint32_t evs_find_mode_rate(uint32_t  mode_rate)
{
   int16_t i = 0;
   while(21 != evs_mode_rate_table[i][0])
   {
      if (evs_mode_rate_table[i][0] == mode_rate)
      {
         return(evs_mode_rate_table[i][1]);
      }
      i++;
   }
   return 0;
}



// Each count is 20ms, maximum delay after which ctm resync should be set
static const uint32_t MAX_RESYNC_DELAY = 4;

/* flag to differentiate of packing of bitstream for FR encoder starting from LSB or MSB*/
#define FR_ENC_FROM_VOICE_PATH         0

#define SS_DELAY_RECORD_PATH            16   // 2msec delay in NB : 1ms for pre-buffering and 1ms for system jitter

#define MAX_SAMP_RATE_FACTOR 6          //(FB/NB)

#define SS_DELAY_REC_PATH_NB 16 //2msec

// NB default 20msec + 4msec for delay for sample slipping 2ms for pre buffering for slipping & 2 ms to prevent overflow due to stuffing
#define ENC_OUT_CIRC_BUF_SIZE_REC      (160+SS_DELAY_RECORD_PATH)



/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */
// destructor
static void venc_destroy(venc_t* venc_ptr);

// Main work loop for service thread. Pulls msgs off of queues and processes them.
static ADSPResult venc_thread(void* instance_ptr);

// Main processing function for converting input buffer to output buffer
static ADSPResult venc_process(venc_t* venc_ptr);

// message handler functions
static ADSPResult venc_connect_dwn_stream_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_disconnect_dwn_stream_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_run_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_stop_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_param_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_get_param_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_media_type(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_destroy_yourself_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static void venc_limiter_process_wrapper(venc_t* venc_ptr,int16_t *in_ptr);
/*
   static ADSPResult Venc_ConnectRxPeerCmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
   static ADSPResult Venc_DisconnectRxPeerCmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
 */

static ADSPResult venc_custom_msg( void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_apr_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_mute_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_pp_samp_rate(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_timing_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_timingv2_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_timingv3_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_reinit_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_start_record_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_stop_record_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_send_rec_buf_to_afe(venc_t* venc_ptr,qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr);
static ADSPResult venc_send_afe_media_type(venc_t *venc_ptr);
static ADSPResult venc_config_host_pcm(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_pkt_exchange_mode(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_oob_pkt_exchange_config(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_register_event(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_unregister_event(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_param_v3(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_enc_rate(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_set_param_int(venc_t* venc_ptr, uint32_t module_id, uint32_t param_id, void* payload_address_in, uint32_t param_size);
// state handler functions
static ADSPResult venc_vfr_handler(void* instance_ptr);
static ADSPResult venc_cmd_handler(void* instance_ptr);
static ADSPResult venc_buf_handler(void* instance_ptr);
static ADSPResult venc_rec_buf_handler(void* instance_ptr);
static ADSPResult venc_data_handler(void* instance_ptr);
static ADSPResult venc_vfr_end_handler(void* instance_ptr);
static ADSPResult venc_resync_handler(void *instance_ptr);
static ADSPResult venc_response_handler(void* instance_ptr);

// Memory management related function
/*
   static ADSPResult Venc_AllocateMem(venc_t* venc_ptr, uint32_t nNearCircBufSamples, uint32_t nFarCircBufSamples, uint32_t nOutCircBufSamples);
   static ADSPResult Venc_AllocateMemFree(venc_t* venc_ptr); */
static ADSPResult venc_allocate_mem(venc_t * venc_ptr);
static void venc_allocate_mem_free(venc_t * venc_ptr);
static ADSPResult venc_modules_init(venc_t * venc_ptr);


// static ADSPResult venc_out_buf_allocator(venc_t* venc_ptr, uint32_t nOutBufSamples);

// Initialization and End functions
static ADSPResult venc_init(venc_t *venc_ptr);
static ADSPResult venc_enc_params_init(venc_t *venc_ptr);
static ADSPResult venc_ctrl_params_init(venc_t *venc_ptr);

/*
   static void Venc_End(venc_t *venc_ptr);
   static void Venc_CircBufInit(venc_t* venc_ptr, uint32_t nNearCircBufSamples, uint32_t nFarCircBufSamples, uint32_t nOutCircBufSamples);
   static void Venc_ModulesInit(venc_t* venc_ptr);
   static void Venc_ModulesEnd(venc_t* venc_ptr);
 */


// Error check functions
static void voice_result_check(ADSPResult result, uint32_t session_num);

// Helper functions
static ADSPResult venc_g711_process(venc_t* venc_ptr, int16_t *in_ptr,int16_t *out_ptr, int16_t mode);
static ADSPResult venc_process_ctm_tx(venc_t* venc_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t nSamples, uint16_t out_buff_size);
static int32_t venc_send_dtmf_gen_ended(venc_t *venc_ptr);
static ADSPResult venc_send_oobtty_tx_char_detected(venc_t *venc_ptr);
static ADSPResult venc_check_enc_struct_size(uint32_t enc_struct_size, uint32_t sess_num);
static void venc_set_default_op_mode(void* instance_ptr);
static ADSPResult venc_send_mode_notification_v2(void* instance_ptr);

// APR buffer delivery
static ADSPResult venc_deliver_pkt(venc_t* venc_ptr, void *buffer_ptr, uint32_t nBufSize);


// wrapper for logging DSP format vocoder packet
static ADSPResult  venc_log_vocoder_packet( uint32_t voc_type, uint32_t log_session_id, void *out_ptr, uint16_t size, uint16_t sampling_rate);

// function to init the record circular buffer with 2ms pre-buffering
static void venc_init_rec_circbuf(venc_t* venc_ptr);

// calculate drift with respect to av/hp timer
static void inline venc_cal_drift(venc_t* venc_ptr);
// subscribe functions to voice timer for hard vfr
static void venc_vtm_subscribe(venc_t* venc_ptr,Vtm_SubUnsubMsg_t *data_ptr);
static void venc_vtm_unsubscribe(venc_t* venc_ptr,Vtm_SubUnsubMsg_t *data_ptr,uint32_t mask);
static uint32_t venc_get_enc_static_size( );
static void populate_limiter_params(void* instance_ptr);
void voice_saturate_to_14_bits(int16_t* speech, int16_t samples);

//voice delivery functions
static ADSPResult venc_vds_response_handler(void* instance_ptr);
static ADSPResult venc_get_kpps_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_aggregate_modules_kpps(void* instance_ptr, uint32_t* kpps_changed);
static ADSPResult venc_get_delay_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult venc_get_vocoder_delay(uint32_t voc_type, uint32_t* delay_ptr);
static ADSPResult venc_aggregate_modules_delay(void* instance_ptr);

// calibration callback function
static void venc_calibration_cb_func(cvd_cal_param_t* cal_params_ptr, void* cb_data);

// ECALL TX wrapper function
static void voice_ecall_tx (venc_t *venc_ptr, int16_t *in_out_ptr);

/* -----------------------------------------------------------------------
 ** Message handler
 ** ----------------------------------------------------------------------- */
/* Venc custom msg handler */
static elite_svc_msg_handler_func pHandler[VENC_NUM_MSGS] =
{
   venc_get_kpps_cmd,                // VENC_GET_KPPS_CMD
   venc_set_mute_cmd,                // VENC_SET_MUTE_CMD
   venc_set_timing_cmd,              // VENC_SET_TIMING_CMD
   venc_set_timingv2_cmd,            // VENC_SET_TIMINGV2_CMD
   venc_reinit_cmd,                  // VENC_REINIT_CMD
   venc_stop_record_cmd,             // VENC_STOP_RECORD_CMD
   venc_start_record_cmd,            // VENC_START_RECORD_CMD_V2
   venc_config_host_pcm,             // VENC_CONFIG_HOST_PCM
   venc_set_pp_samp_rate,            // VENC_SET_STREAM_PP_SAMP_RATE
   venc_set_pkt_exchange_mode,       // VENC_SET_PKT_EXCHANGE_MODE
   venc_set_oob_pkt_exchange_config, // VENC_SET_OOB_PKT_EXCHANGE_CONFIG
   venc_set_timingv3_cmd,            // VENC_SET_TIMINGV3_CMD
   venc_get_delay_cmd,               // VENC_GET_DELAY_CMD
   venc_register_event,              // VENC_REGISTER_EVENT
   venc_unregister_event,            // VENC_UNREGISTER_EVENT
   venc_set_param_v3,                // VENC_SET_PARAM_V3
   venc_set_enc_rate,                // VENC_SET_ENC_RATE
};

/* Venc FADD msg handler */
static elite_svc_msg_handler_func pEliteHandler[] =
{
   venc_custom_msg,              //0  - ELITE_CUSTOM_MSG
   elite_svc_unsupported,          //1  - ELITE_CMD_START_SERVICE
   venc_destroy_yourself_cmd,     //2  - ELITE_CMD_DESTROY_SERVICE
   venc_connect_dwn_stream_cmd,    //3  - ELITE_CMD_CONNECT
   venc_disconnect_dwn_stream_cmd, //4  - ELITE_CMD_DISCONNECT
   venc_stop_cmd,                //5  - ELITE_CMD_PAUSE
   venc_run_cmd,                 //6  - ELITE_CMD_RESUME
   elite_svc_unsupported,          //7  - ELITE_CMD_FLUSH
   venc_set_param_cmd,            //8  - ELITE_CMD_SET_PARAM
   venc_get_param_cmd,            //9  - ELITE_CMD_GET_PARAM
   elite_svc_unsupported,          //10 - ELITE_DATA_BUFFER
   venc_set_media_type,           //11 - ELITE_DATA_MEDIA_TYPE
   elite_svc_unsupported,          //12 - ELITE_DATA_EOS
   elite_svc_unsupported,          //13 - ELITE_DATA_RAW_BUFFER
   elite_svc_unsupported,          //14 - ELITE_CMD_STOP_SERVICE
   venc_apr_cmd                  //15 - ELITE_APR_PACKET_OPCODE
};

/* Venc channel handler */
static ADSPResult (*pVencHandler[])(void *) =
{
   venc_resync_handler, //handles resync
   venc_vfr_handler, // handles vfr signal
   venc_cmd_handler, // handles cmds
   venc_buf_handler, // handles output buffers
   venc_rec_buf_handler,
   venc_data_handler,// handles input data buffers
   venc_vfr_end_handler, // handles hand shake signal from voicetimer for unsubscribe
   venc_response_handler,     // should never get invoked because of sync wait
   venc_vds_response_handler //VDS response handler for failed delivery
};

// global variables for enc-dec pkt loopback
int32_t gVoiceEncDecLB = 0;
int8_t gaVoiceEncDecLBBuf[640] = {0}; // accounts for wb also

/* =======================================================================
 **                          Function Definitions
 ** ======================================================================= */

/*
** Wrapper is created to avoid stack increment for venc_process function.
** Earlier the stack was contributing to worst case path, having a
** seperate function will avoid it.
*/
static __attribute__((noinline)) void venc_limiter_process_wrapper(venc_t* venc_ptr,int16_t *in_ptr)
{
   int32_t lim_temp[VOICE_FRAME_SIZE_FB];
   uint32_t i = 0;
   if (venc_ptr->proc.limiter_params[LIM_ENABLE])
   {
      // Limiter processing needs a 32-bit copy of the input.
      for (i=0 ; i < venc_ptr->io.in_buf_size>>1 ; i++)
      {
         lim_temp[i] = in_ptr[i];
      }

      Lim_process(venc_ptr->proc.limiter_ptr,(void*)in_ptr, lim_temp, venc_ptr->io.in_buf_size>>1, 0/*No BYPASS*/);
   }
}

ADSPResult venc_create (uint32_t nInputParam,
      voice_strm_apr_info_t  *apr_info_ptr,
      voice_strm_tty_state_t *tty_state_ptr,
      elite_svc_handle_t *loopback_handle,
      void **ppHandle,uint32_t session_num)
{
   ADSPResult result=ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_create enter session(%x)",(int)session_num);

   // allocate instance struct
   venc_t *venc_ptr = (venc_t*) qurt_elite_memory_malloc( sizeof(venc_t), QURT_ELITE_HEAP_DEFAULT);
   if (!venc_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to allocate memory for venc struct !!");
      return ADSP_ENOMEMORY;
   }

   // zero out all the fields
   memset(venc_ptr, 0, sizeof(venc_t));

   // Initialize instance variables
   venc_ptr->svc_handle.unSvcId = ELITE_VOICE_ENC_SVCID;
   venc_ptr->wait_mask = VENC_CMD_DATA_MASK;
   venc_ptr->apr_info_ptr = apr_info_ptr;
   venc_ptr->loopback_handle = loopback_handle;
   venc_ptr->tty.state_ptr       = tty_state_ptr;
   venc_ptr->session_num = session_num;

   // initialze qurt_elite channel
   qurt_elite_channel_init(&venc_ptr->qurt_elite_channel);

   venc_ptr->ctm.resync_modem_afe = FALSE;
   venc_ptr->record.enable = FALSE; // should not be reset in init to support handover cases
   venc_ptr->record.mode = 0; // init with 0
   (void) venc_init(venc_ptr);
   // directly copy generic handle -> should be same for any VSM client
   venc_ptr->proc.apr_info_dtmf_gen.apr_handle = venc_ptr->apr_info_ptr->apr_handle;

   if(qurt_elite_globalstate.pVoiceTimerCmdQ == NULL)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to get Vtm cmdQ session(%x)",(int)session_num);
      venc_ptr->vtm_cmd_q_ptr = NULL;
   }
   venc_ptr->vtm_cmd_q_ptr = qurt_elite_globalstate.pVoiceTimerCmdQ;
   venc_ptr->voc.four_gv_encode_ptr = NULL;

   venc_ptr->vds_handle_ptr = vds_handle;
   venc_ptr->vds_client_token = VDS_CREATE_TOKEN(venc_ptr->session_num, VDS_CLIENT_VENC_DELIVERY);

   // Allocate memory for CNG
   if (NULL == venc_ptr->proc.comfort_noise_ptr)
   {
      qurt_elite_memory_new (venc_ptr->proc.comfort_noise_ptr, CComfortNoise, QURT_ELITE_HEAP_DEFAULT, result);
      if (result != ADSP_EOK)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: CNG module not created session(%x)",(int)session_num);
         venc_destroy(venc_ptr);
         return result;
      }
   }

   // for recording
   venc_ptr->record.afe_drift_ptr = NULL;
   venc_ptr->record.ss_info_counter = 0;
   venc_ptr->record.aud_ref_port = 0;
   //Set config for 1x TTY downby2 resampler
   {
      uint32_t out_frame_samples;
      voice_resampler_set_config(&(venc_ptr->tty.resamp_config),
            VOICE_WB_SAMPLING_RATE,VOICE_NB_SAMPLING_RATE,NO_OF_BITS_PER_SAMPLE,VOICE_FRAME_SIZE_WB,&out_frame_samples);
   }

   result = venc_allocate_mem(venc_ptr);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed Venc Allocate Memory function (%lx)",session_num);
      venc_destroy(venc_ptr);
      return result;
   }

   // initialize CTM transmitter here
   // so NOT to re-initialize when STOP->RUN, eg, handover happens
   if( ADSP_FAILED( result = voice_ctm_tx_init( &venc_ptr->ctm.state, session_num)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: CTM Tx not created session(%x)",(int)session_num);
      venc_destroy(venc_ptr);
      return result;
   }
   // Allocate memory for LTE TTY Tx

   if( ADSP_FAILED( result = voice_oobtty_tx_mem_alloc( &venc_ptr->oobtty_tx_struct)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: LTE TTY Tx not created session(%x)",(int)session_num);
      venc_destroy(venc_ptr);
      return result;
   }
   //default initialize soft-mute module
   cross_fade_init_default ( &(venc_ptr->proc.cross_fade_cfg_struct) );
   venc_ptr->proc.cross_fade_cfg_struct.iConvergeNumSamples = 0;
   venc_ptr->proc.cross_fade_cfg_struct.iTotalPeriodMsec = 0;
   cross_fade_init ( &(venc_ptr->proc.cross_fade_cfg_struct),&(venc_ptr->proc.cross_fade_data_struct),8000/*default sampling rate*/);

   // initialize host pcm module
   {
      result = voice_host_pcm_init( &venc_ptr->proc.host_pcm_context,
            8000 /*default sampling rate*/,
            1 /*init for 1 channel by default*/ ,
            FALSE /* disable sample slip/stuff */);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed Host Pcm Init in tx stream!! session(%lx)",session_num);
         venc_destroy(venc_ptr);
         return result;
      }
   }


   result= venc_modules_init(venc_ptr);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Venc init failed (%lx)",session_num);
      venc_destroy(venc_ptr);
      return result;
   }

   // make Venc Q names unique per instance
   snprintf(venc_ptr->data_q_name    , QURT_ELITE_DEFAULT_NAME_LEN, "Venc_DataQ%2x",(int)session_num);
   snprintf(venc_ptr->cmd_q_name     , QURT_ELITE_DEFAULT_NAME_LEN, "Venc_CmdQ%2x" ,(int)session_num);
   snprintf(venc_ptr->buf_q_name     , QURT_ELITE_DEFAULT_NAME_LEN, "Venc_BufQ%2x" ,(int)session_num);
   snprintf(venc_ptr->record.buf_q_name , QURT_ELITE_DEFAULT_NAME_LEN, "Venc_Rec_BufQ%2x" ,(int)session_num);
   snprintf(venc_ptr->respq_name     , QURT_ELITE_DEFAULT_NAME_LEN, "Venc_RespQ%2lx",session_num);

   // Create the queues. Use non-blocking queues, since pselect is always used.
   // pselect blocks on any non-masked queue to receive, then can do non-blocking checks.
   // DataQ for inputs
   // CmdQ for cmd inputs
   // BufQ for output buffers - no need of a Q, instead use local buffer, todo: need bufQ for enc-dec loopback
   // signal_ptr for 20ms tick
   // signal_end_ptr for handshaking unsubscribe with VoiceTimer

   /* Allocate memory for queues/signals */
   venc_ptr->svc_handle.dataQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_DATA_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT );
   venc_ptr->svc_handle.cmdQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT );
   venc_ptr->record.buf_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT );
   venc_ptr->resp_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_RESP_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT );
   /* This tick signal will be allocated by VDS, don't allocate/free/add to channel here*/
   venc_ptr->vtm_sub_unsub_data.signal_ptr = NULL;
   venc_ptr->vtm_sub_unsub_data.signal_end_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc(sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   venc_ptr->vtm_sub_unsub_data.resync_signal_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc(sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   venc_ptr->vds_error_signal_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc(sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   if (NULL == venc_ptr->svc_handle.dataQ
         || NULL == venc_ptr->svc_handle.cmdQ
         || NULL == venc_ptr->record.buf_q_ptr
         || NULL == venc_ptr->resp_q_ptr
         || NULL == venc_ptr->vtm_sub_unsub_data.signal_end_ptr
         || NULL == venc_ptr->vtm_sub_unsub_data.resync_signal_ptr
         || NULL == venc_ptr->vds_error_signal_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to allocate memory for Venc Queues/Signals session(%#lx)",session_num);
      venc_destroy(venc_ptr);
      return ADSP_ENOMEMORY;
   }

   if(   ADSP_FAILED(result = qurt_elite_queue_init(venc_ptr->data_q_name, MAX_DATA_Q_ELEMENTS, venc_ptr->svc_handle.dataQ))
         || ADSP_FAILED(result = qurt_elite_queue_init(venc_ptr->cmd_q_name, MAX_CMD_Q_ELEMENTS, venc_ptr->svc_handle.cmdQ))
         || ADSP_FAILED(result = qurt_elite_queue_init(venc_ptr->respq_name, MAX_RESP_Q_ELEMENTS, venc_ptr->resp_q_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(venc_ptr->vtm_sub_unsub_data.signal_end_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(venc_ptr->vtm_sub_unsub_data.resync_signal_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(venc_ptr->vds_error_signal_ptr))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&venc_ptr->qurt_elite_channel, (venc_ptr->svc_handle.cmdQ), VENC_CMD_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&venc_ptr->qurt_elite_channel, (venc_ptr->svc_handle.dataQ), VENC_DATA_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&venc_ptr->qurt_elite_channel, (venc_ptr->resp_q_ptr), VENC_RESPONSE_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&venc_ptr->qurt_elite_channel, (venc_ptr->vtm_sub_unsub_data.signal_end_ptr), VENC_TICK_END_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&venc_ptr->qurt_elite_channel, (venc_ptr->vtm_sub_unsub_data.resync_signal_ptr), VENC_RESYNC_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&venc_ptr->qurt_elite_channel, (venc_ptr->vds_error_signal_ptr), VENC_VDS_RESPONSE_MASK))
         || ADSP_FAILED(result = qurt_elite_queue_init(venc_ptr->record.buf_q_name, MAX_CMD_Q_ELEMENTS,
               venc_ptr->record.buf_q_ptr))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&venc_ptr->qurt_elite_channel, (venc_ptr->record.buf_q_ptr),
               VENC_REC_BUF_MASK))

     )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed create Venc MsgQs or Signals!! session(%x)",(int)session_num);
      venc_destroy(venc_ptr);
      return result;
   }
   // Allocate and queue up the  recording output buffers
   for (uint32_t i = 0; i < MAX_REC_BUF_Q_ELEMENTS-1; i++)
   {
      //allocate the databuffer payload (metadata + pcm buffer size)
      int32_t req_size = GET_ELITEMSG_DATABUF_REQ_SIZE(REC_BUFFER_SIZE);
      elite_msg_data_buffer_t* data_payload_ptr = (elite_msg_data_buffer_t*) qurt_elite_memory_malloc(req_size, QURT_ELITE_HEAP_DEFAULT);

      if (!data_payload_ptr)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to allocate memory for recording output buffer, session(%lx)",venc_ptr->session_num);
         venc_destroy(venc_ptr);
         return ADSP_ENOMEMORY;
      }

      data_payload_ptr->nActualSize = REC_BUFFER_SIZE;
      data_payload_ptr->nMaxSize    = REC_BUFFER_SIZE;

      venc_ptr->record.bufs_allocated++;

      if (ADSP_FAILED(result = elite_msg_push_payload_to_returnq(venc_ptr->record.buf_q_ptr, (elite_msg_any_payload_t*) data_payload_ptr)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to fill Venc Rec buf queue session(%lx)",venc_ptr->session_num);
         qurt_elite_memory_free(data_payload_ptr);
         venc_destroy(venc_ptr);
         return result;
      }
   }

   // Launch the thread
   aTHREAD_NAME[3] = ((session_num + 48) & 0xff); // int32_t to ascii conversion
   if (ADSP_FAILED(result = qurt_elite_thread_launch(&(venc_ptr->svc_handle.threadId), aTHREAD_NAME, NULL,
               THREAD_STACK_SIZE, ELITETHREAD_DYNA_VOICE_ENC_PRIO, venc_thread, (void*)venc_ptr, QURT_ELITE_HEAP_DEFAULT)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed create Venc thread!! session(%x)",(int)session_num);
      venc_destroy(venc_ptr);
      return result;
   }

   *ppHandle = &(venc_ptr->svc_handle);
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_create end %d, wait_mask=%x, session(%x)",(int)result,(int)venc_ptr->wait_mask,(int)session_num);
   return ADSP_EOK;
}

// destructor
static void venc_destroy (venc_t* venc_ptr)
{
   if (venc_ptr)
   {
      uint32_t session_num = venc_ptr->session_num;

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_destroy begin session(%x)",(int)session_num);

      // destroy the Host pcm module first
      voice_host_pcm_end( &venc_ptr->proc.host_pcm_context);

      // call utility function to destroy data Q
      if(NULL != venc_ptr->svc_handle.dataQ)
      {
         elite_svc_deinit_data_queue(venc_ptr->svc_handle.dataQ);
         qurt_elite_memory_free(venc_ptr->svc_handle.dataQ);
         venc_ptr->svc_handle.dataQ = NULL;
      }

      // call utility function to destroy cmd Q
      if(NULL != venc_ptr->svc_handle.cmdQ)
      {
         elite_svc_deinit_cmd_queue(venc_ptr->svc_handle.cmdQ);
         qurt_elite_memory_free(venc_ptr->svc_handle.cmdQ);
         venc_ptr->svc_handle.cmdQ = NULL;
      }


      // call utility function to destroy response Q
      if(NULL != venc_ptr->resp_q_ptr)
      {
         elite_svc_deinit_cmd_queue(venc_ptr->resp_q_ptr);
         qurt_elite_memory_free(venc_ptr->resp_q_ptr);
         venc_ptr->resp_q_ptr = NULL;
      }

      // call utility function to destroy signal
      if(NULL != venc_ptr->vtm_sub_unsub_data.signal_end_ptr)
      {
         qurt_elite_signal_deinit(venc_ptr->vtm_sub_unsub_data.signal_end_ptr);
         qurt_elite_memory_free(venc_ptr->vtm_sub_unsub_data.signal_end_ptr);
         venc_ptr->vtm_sub_unsub_data.signal_end_ptr = NULL;
      }


      if(NULL != venc_ptr->vtm_sub_unsub_data.resync_signal_ptr)
      {
         qurt_elite_signal_deinit(venc_ptr->vtm_sub_unsub_data.resync_signal_ptr);
         qurt_elite_memory_free(venc_ptr->vtm_sub_unsub_data.resync_signal_ptr);
         venc_ptr->vtm_sub_unsub_data.resync_signal_ptr = NULL;
      }

      if(NULL != venc_ptr->vds_error_signal_ptr)
      {
         qurt_elite_signal_deinit(venc_ptr->vds_error_signal_ptr);
         qurt_elite_memory_free(venc_ptr->vds_error_signal_ptr);
         venc_ptr->vds_error_signal_ptr = NULL;
      }

      if(NULL != venc_ptr->record.buf_q_ptr)
      {
         elite_svc_deinit_buf_queue(venc_ptr->record.buf_q_ptr, venc_ptr->record.bufs_allocated);
         qurt_elite_memory_free(venc_ptr->record.buf_q_ptr);
         venc_ptr->record.bufs_allocated = 0;
         venc_ptr->record.buf_q_ptr = NULL;
      }

      // Destroy the qurt_elite_channel
      qurt_elite_channel_destroy(&venc_ptr->qurt_elite_channel);

      // Destroy CTM Tx
      if (NULL != venc_ptr->ctm.state.ctm_tx_struct_instance_ptr)
      {
         voice_ctm_tx_end( &venc_ptr->ctm.state);
         venc_ptr->ctm.state.ctm_tx_struct_instance_ptr=NULL;
      }

      // Destroy LTE TTY Tx
      voice_oobtty_tx_end( &venc_ptr->oobtty_tx_struct);

      venc_allocate_mem_free(venc_ptr);

      // Destroy CNG
      if (NULL != venc_ptr->proc.comfort_noise_ptr)
      {
         qurt_elite_memory_delete(venc_ptr->proc.comfort_noise_ptr, CComfortNoise);
         venc_ptr->proc.comfort_noise_ptr = NULL;
      }

      // Destroy 1x TTY downsampler

      qurt_elite_memory_free (venc_ptr);

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_destroy end session(%x)",(int)session_num);
   }
   venc_ptr = NULL;
}

/**
 * This function is the main work loop for the service. VFR signals and Commands
 * are handled with the highest priority. Data processing is handled only when command
 * queue is empty.
 *
 * The thread listens for command and data queues simultaneously (commands always
 * prioritized). When a data buffer is received, it is stored and processed when enough
 * data and the thread switches to listening for command queue and output buffer
 * queue. As soon as an output buffer is received, the data is copied  and delivered
 * downstream.
 */

static ADSPResult venc_thread(void* instance_ptr)
{
   ADSPResult result;                    // general result value
   venc_t *venc_ptr = (venc_t*)instance_ptr;    // instance structure
   uint32_t session_num = venc_ptr->session_num;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_thread begin session(%lx)",session_num);
   // Enter forever loop
   for(;;)
   {
      // ***************** Wait for the MASK
      // block on any one or more of selected queues and signals
      venc_ptr->rcvd_mask = qurt_elite_channel_wait(&venc_ptr->qurt_elite_channel, venc_ptr->wait_mask);

      while(venc_ptr->rcvd_mask)
      {
         venc_ptr->bit_pos = voice_get_signal_pos(venc_ptr->rcvd_mask);

         // De queue and signal clear done in the handler functions.
         result = pVencHandler[venc_ptr->bit_pos](venc_ptr);

         if(result != ADSP_EOK)
         {
            if (ADSP_ETERMINATED == result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_thread end session(%lx)",session_num);
               return ADSP_EOK;
            }
            MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Venc thread Handler Error bit_pos=%d,res=%d, session(%lx)",(int)venc_ptr->bit_pos, (int)result, session_num );
         }

         venc_ptr->rcvd_mask = qurt_elite_channel_poll(&venc_ptr->qurt_elite_channel, venc_ptr->wait_mask);
      } // end of while loop
   } // end of for loop
   return 0;
}

static ADSPResult venc_vfr_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_vfr_handler begin session(%x)",(int)venc_ptr->session_num);
   //qurt_elite_signal_clear(venc_ptr->vtm_sub_unsub_data.signal_ptr);
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_vfr_handler end session(%x)",(int)venc_ptr->session_num);

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error! venc_vfr_handler should not get invoked, session(%x)",(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_buf_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_buf_handler begin session(%x)",(int)venc_ptr->session_num);

   MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_buf_handler end session(%x)",(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_rec_buf_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   qurt_elite_bufmgr_node_t out_buf_mgr_node;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_buf_handler begin session(%x)",venc_ptr->session_num);

   if (FALSE == venc_ptr->process_data)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Not in RUN state, this should not happen!! session(%lx)",venc_ptr->session_num);
      return ADSP_ENOTREADY;
   }
   // Take next buffer off the Q
   if ( TRUE == venc_ptr->record.enable )
   {
      if (TRUE == venc_ptr->send_media_type)
      {
         result = venc_send_afe_media_type(venc_ptr);
      }
      else
      {
         result = qurt_elite_queue_pop_front(venc_ptr->record.buf_q_ptr, (uint64_t*)&out_buf_mgr_node );
         if (ADSP_EOK != result)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error reading buf queue, result(%d) session(%lx)",result,venc_ptr->session_num);
         }
         else
         {
            result = venc_send_rec_buf_to_afe(venc_ptr, &out_buf_mgr_node);
            if (!ADSP_FAILED(result))
            {  // make the flag false if the result was success
               venc_ptr->record.first_frame = FALSE;
            }
         }
      }
   }
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_buf_handler end session(%x)",venc_ptr->session_num);
   return result;
}

static ADSPResult venc_vfr_end_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_vfr_end_handler begin session(%x)",(int)venc_ptr->session_num);

   qurt_elite_signal_clear(venc_ptr->vtm_sub_unsub_data.signal_end_ptr);

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_vfr_end_handler end session(%x)",(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_resync_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   qurt_elite_signal_clear(venc_ptr->vtm_sub_unsub_data.resync_signal_ptr);
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc resync from AFE, session(%x)", (int)venc_ptr->session_num);
   // Start couting for resync. If already counting, this will reset the count
   venc_ptr->ctm.resync_delay_cnt = 1;
   // Clear recording ss_info if applicable
   if(venc_ptr->record.enable)
   {
      venc_ptr->record.ss_info_counter  = 0;
      venc_ptr->record.ss_multiframe_counter = 0;
      // reset device drift counter
      memset(&venc_ptr->record.voice_cmn_drift_info, 0, sizeof(venc_ptr->record.voice_cmn_drift_info));
      //update nb_sampling rate factor..
      venc_ptr->record.voice_cmn_drift_info.nb_sampling_rate_factor = venc_ptr->samp_rate_factor;
   }
   return result;
}

/* Since we always wait synchronously for responses, this should never get invoked */
static ADSPResult venc_response_handler(void* instance_ptr)
{
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! venc_response_handler invoked, session(%x)",(int)venc_ptr->session_num);
   return ADSP_EFAILED;
}

static ADSPResult venc_vds_response_handler(void* instance_ptr)
{
   venc_t *venc_ptr = (venc_t*)instance_ptr;

   venc_ptr->wait_mask &= ~(VENC_VDS_RESPONSE_MASK);  // Stop listening to errors from VDS.
   qurt_elite_signal_clear(venc_ptr->vds_error_signal_ptr); //clear the signal

   if(OUT_OF_BAND == venc_ptr->pkt_exchange_mode)
   {
      if(FALSE == venc_ptr->oob.enc_pkt_consumed)
      {
         //venc_ptr->oob.enc_pkt_consumed = TRUE;    // dont set the flag to true to avoid sending OOB packets, even when no OOB consumed event has come from client.
         venc_ptr->oob.pkt_miss_ctr++;
         venc_ptr->pkt_delivery_pending = FALSE;
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! unexpected response from VDS failed delivery, session(%x)",(int)venc_ptr->session_num);
      }
   }
   else
   {
      venc_ptr->pkt_miss_ctr++;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VDS Failed to deliver Venc pkt, session(%x)",
         (int)venc_ptr->session_num);
   return ADSP_EOK;
}

static void venc_cal_sample_slip_stuff(venc_t* venc_ptr, int16_t* slip_stuff_samples)
{
   //Check if any samples needs to be corrected
   if(0 != venc_ptr->record.ss_info_counter)
   {
      //Check if we are done with previous correction
      if(0 == venc_ptr->record.ss_multiframe_counter)
      {
         //reset multi frame counter
         venc_ptr->record.ss_multiframe_counter = VENC_SS_MULTIFRAME;

         if(0 > venc_ptr->record.ss_info_counter)
         {
            // This means DMS is faster, then stuff one samples
            *slip_stuff_samples = -1;

            //decrease (magniture) of ss_info counter
            venc_ptr->record.ss_info_counter += (int32_t)1; // arithmetic to make the slip stuff counter zero
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR:: venc sample stuff !!  count (%d) session(%x)",*slip_stuff_samples,(int)venc_ptr->session_num);
         }
         else  // venc_ptr->record.ss_info_counter>0
         {
            // This means DMS is slow, then slip one samples
            *slip_stuff_samples = 1;
            //decrease (magniture) of ss_info counter
            venc_ptr->record.ss_info_counter -= (int32_t)1; // arithmetic to make the slip counter zero
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR:: venc sample slip !!  count (%d) session(%x)",*slip_stuff_samples,(int)venc_ptr->session_num);
         }
      }
   }//  if(0 != venc_ptr->record.ss_info_counter)
}

static void inline venc_cal_drift(venc_t* venc_ptr)
{
   voice_cmn_accu_drift(&venc_ptr->record.ss_info_counter,
         &venc_ptr->record.voice_cmn_drift_info,
         venc_ptr->record.afe_drift_ptr,
         venc_ptr->vfr_source,
         VOICE_ENC_REC,
         venc_ptr->vfr_mode,
         venc_ptr->session_num,
         venc_ptr->vtm_sub_unsub_data.timing_ver,
         venc_ptr->vsid);
}
static ADSPResult venc_cmd_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   uint32_t session_num = venc_ptr->session_num;
   elite_msg_any_t msg;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_cmd_handler begin session(%lx)",session_num);
   result = qurt_elite_queue_pop_front(venc_ptr->svc_handle.cmdQ, (uint64_t*) &msg);
   // ***************** Process the msg
   if (ADSP_EOK == result)
   {
      const uint32_t cmdTableSize = sizeof(pEliteHandler)/sizeof(pEliteHandler[0]);
      if (msg.unOpCode >= cmdTableSize)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unsupported message ID 0x%8lx!! session(%lx)", msg.unOpCode,session_num);
         return ADSP_EUNSUPPORTED;
      }
      // table lookup to call handling function, with FALSE to indicate processing of msg
      result = pEliteHandler[msg.unOpCode](instance_ptr, &msg);
   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_cmd_handler end result=%x session(%lx)",result,session_num);
   return result;
}

static ADSPResult venc_custom_msg( void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*) instance_ptr;
   elite_msg_custom_header_t *pCustom = (elite_msg_custom_header_t *) msg_ptr->pPayload;

   if( pCustom->unSecOpCode < VENC_NUM_MSGS)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc_Cmd, SecOpcode: %ld session(%x)",pCustom->unSecOpCode ,(int)venc_ptr->session_num);
      result = pHandler[pCustom->unSecOpCode](instance_ptr, msg_ptr);
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unsupported SecOpcode message ID 0x%8lx!! session(%x)", pCustom->unSecOpCode,(int)venc_ptr->session_num);
      result = ADSP_EFAILED;
   }
   return result;
}

static ADSPResult venc_run_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   ADSPResult result = ADSP_EOK;
   uint8_t wideband = FALSE;
   uint8_t tty_enable = FALSE;
   uint8_t oobtty_enable = FALSE;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_run_cmd begin session(%x)",(int)venc_ptr->session_num);

   // if here,it session is already in STOP state.(RUN or STOP is already is checked in VSM)

   /* Must handle all vocoder init, post proc init, TTY init, sample rate dependent init here!! */
   /* TODO:  appears that some post proc not setup via CommitFormat! */

   // handle Host Pcm resampler (re)init here if enabled (keep Host Pcm active thru handover if not previously disabled ),
   // this reinit is mainly to latch stream parameters such as sampling rate and frame size.
   // if hpcm is disabled, hpcm start on the fly will take care of all the settings
   if( TRUE == venc_ptr->proc.host_pcm_context.read_config.enable || TRUE == venc_ptr->proc.host_pcm_context.write_config.enable)
   {
      result = voice_host_pcm_reinit( &venc_ptr->proc.host_pcm_context, 1 /*num of channels*/, venc_ptr->samp_rate, venc_ptr->io.frame_samples);
      if(ADSP_FAILED(result))
      {
         voice_host_pcm_end(&venc_ptr->proc.host_pcm_context);
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_host_pcm_reinit failed in venc_run_cmd, session(%lx)",venc_ptr->session_num);
         elite_svc_send_ack(msg_ptr, result);
         return result;
      }
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_host_pcm_reinit done in venc_run_cmd, session(%x)",(int)venc_ptr->session_num);
   }

   if (OUT_OF_BAND == venc_ptr->pkt_exchange_mode)
   {
      venc_ptr->oob.enc_pkt_consumed = TRUE;    // force the flag to true to send first packet
   }
   // Initializing CNG
   result = venc_ptr->proc.comfort_noise_ptr->CommitFormat(voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate));
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ComfortNoise Commit Format failed session(%x)",(int)venc_ptr->session_num);
   }
   result = venc_ptr->proc.comfort_noise_ptr->ProcessInit();
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ComfortNoise Process Init failed session(%x)",(int)venc_ptr->session_num);
   }
   venc_ptr->proc.comfort_noise_ptr->SetMute( (venc_ptr->mute == VOICE_MUTE));

   /* todo: Need to find an ISOD supported way to disable the muting (or CNG) for TTY loopback SIM testing */
#ifdef SIM
   venc_ptr->proc.comfort_noise_ptr->SetCNGOnRxTTYDetected(FALSE);
#else
   /* enable Tx CNG muting when Rx TTY detected only for TTY FULL mode */
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx CNG Mute on Rx TTY Det: Enable(%d)",(venc_ptr->tty.state_ptr->m_etty_mode == VSM_TTY_MODE_FULL) ? TRUE : FALSE);
   venc_ptr->proc.comfort_noise_ptr->SetCNGOnRxTTYDetected((venc_ptr->tty.state_ptr->m_etty_mode == VSM_TTY_MODE_FULL) ? TRUE : FALSE );
#endif

   // init sample slipping algorithm
   voice_sample_slip_init(&(venc_ptr->record.ss_struct),((venc_ptr->samp_rate_factor)*VOICE_FRAME_SIZE_NB_10MS),VENC_SS_MULTIFRAME);

   //update nb sampling factor for drift detection
   venc_init_rec_circbuf(venc_ptr);
   venc_ptr->record.voice_cmn_drift_info.nb_sampling_rate_factor = venc_ptr->samp_rate_factor;
   uint16_t venc_samp_rate = voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate);
   wideband = ( venc_samp_rate == VOICE_WB_SAMPLING_RATE);

   //Update dtmf_gen_ptr->sampling_freq.
   uint16_t dtmf_gen_samp_freq_old = venc_ptr->proc.dtmf_gen_ptr->sampling_freq;

   venc_ptr->proc.dtmf_gen_ptr->sampling_freq = ((VOICE_NB_SAMPLING_RATE == venc_samp_rate)?EIGHT:((VOICE_WB_SAMPLING_RATE == venc_samp_rate)?SIXTEEN:FORTYEIGHT));

   // extra check for 32k
   if( VOICE_SWB_SAMPLING_RATE == venc_samp_rate )
   {
       venc_ptr->proc.dtmf_gen_ptr->sampling_freq = THIRTYTWO;
   }

   if( venc_ptr->proc.dtmf_gen_ptr->sampling_freq > SIXTEEN )
   {
      result = voice_dtmf_resampler_init(venc_ptr->proc.dtmf_gen_ptr, venc_samp_rate);

      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc DTMF Gen resampler init failed %d: session (%x)",(int) result, (int)venc_ptr->session_num);
      }
   }


   //If dtmf generation is in DTMR_RUN state and Handover to media type of different frequency happens, Then duration need to be adjusted accordingly.
   //Reinit dtmf tone generation if already in DTMF_RUN state before STOP command and new media type with different sampling rate has come.
   if (DTMF_RUN == venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state && dtmf_gen_samp_freq_old != venc_ptr->proc.dtmf_gen_ptr->sampling_freq)
   {
      MicroSecs duration  =  venc_ptr->proc.dtmf_gen_ptr->duration_in_micro_secs;

      /* Calcualte new duration based on the tone duration in the command and duration elapsed before stop command*/
      if(VOICE_DTMF_GEN_CONTINUOUS != venc_ptr->proc.dtmf_gen_ptr->duration_in_micro_secs && VOICE_DTMF_GEN_STOP != venc_ptr->proc.dtmf_gen_ptr->duration_in_micro_secs)
      {
         //get remaining samples to be generated
         uint32_t samples_remaining = venc_ptr->proc.dtmf_gen_ptr->dtmf_generator.totalSamples -  venc_ptr->proc.dtmf_gen_ptr->dtmf_generator.currentSamples;

         //Previous tone generation frequency. For FB previos tone generation frequency is NB
         uint16_t venc_samp_rate_old = (dtmf_gen_samp_freq_old==SIXTEEN)?VOICE_WB_SAMPLING_RATE:VOICE_NB_SAMPLING_RATE;

         //new duration in milli seconds
         //    duration_in_millisec = (samples_remaining*1000)/(sampling_rate)
         duration = (int32_t)(samples_remaining*1000)/venc_samp_rate_old ;

         //DTMF Gen library has 5 millisecond resolution
         duration = (int32_t)((duration/5)*5);
      }

      //REINIT dtmf generation
      result = voice_dtmf_gen_start(venc_ptr->proc.dtmf_gen_ptr,venc_ptr->proc.dtmf_gen_ptr->dtmf_gen_tone1,venc_ptr->proc.dtmf_gen_ptr->dtmf_gen_tone2,
            (int32_t)duration,venc_ptr->proc.dtmf_gen_ptr->dtmf_gen_mix_flag,venc_ptr->proc.dtmf_gen_ptr->dtmf_gen_gain);

      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc DTMF Gen start from run cmd failed %d: session (%x). Stopping DTMF Gen",(int) result, (int)venc_ptr->session_num);
         venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state = DTMF_STOP;
         result = (ADSPResult) venc_send_dtmf_gen_ended(venc_ptr);
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  VSM_EVT_DTMF_GENERATION_ENDED failed session(%x)", \
                  (int)venc_ptr->session_num);
         }
      }
   }

   // Initialize Limiter
   result = Lim_init(venc_ptr->proc.limiter_ptr,venc_ptr->proc.limiter_params,0/*channel index*/,voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate),NO_OF_BITS_PER_SAMPLE);
   if(ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc Limter initialization error %d: session (%x)",(int) result, (int)venc_ptr->session_num);
   }


   /* latch all settings when going into Run state */
   oobtty_enable = (venc_ptr->tty.state_ptr->m_oobtty_mode == VSM_TTY_MODE_HCO ||
         venc_ptr->tty.state_ptr->m_oobtty_mode == VSM_TTY_MODE_FULL);
   if ( (( venc_ptr->reset_voc_flag )) ||(oobtty_enable ^ voice_oobtty_tx_get_enable(&venc_ptr->oobtty_tx_struct)))
   {
      // Perform set config for VoLTE TTY
      voice_oobtty_tx_set_config( &venc_ptr->oobtty_tx_struct, oobtty_enable, venc_ptr->samp_rate);
      // init OOB TTY Tx
      voice_oobtty_tx_init(&venc_ptr->oobtty_tx_struct, venc_ptr->session_num);
   }

   /* handle CDMA TTY/CTM init */
   tty_enable = (venc_ptr->tty.state_ptr->m_etty_mode == VSM_TTY_MODE_HCO ||
         venc_ptr->tty.state_ptr->m_etty_mode == VSM_TTY_MODE_FULL);

   /* For CTM (GSM/UMTS voc), if we are in VCO mode, we still need to enable TTY algorithm.  This
    * is because when the far end sends CTM, and Rx TTY algorithm detects, we need to acknowledge this
    * with a CTM ack.  If not the far end could revert to bypass mode.
    */
   switch(venc_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
      case VSM_MEDIA_TYPE_EFR_MODEM:
      case VSM_MEDIA_TYPE_FR_MODEM:
      case VSM_MEDIA_TYPE_HR_MODEM:
      case VSM_MEDIA_TYPE_EAMR:
      case VSM_MEDIA_TYPE_EVS:
         {
            // also enable TTY algorithm for CTM VCO  case, in addition to normal HCO and FULL cases
            if( venc_ptr->tty.state_ptr->m_etty_mode == VSM_TTY_MODE_VCO)
            {
               tty_enable = TRUE;
            }
            if (( venc_ptr->reset_voc_flag ) || ( tty_enable ^ voice_ctm_tx_get_enable(&venc_ptr->ctm.state) ))
            {
               voice_ctm_tx_set_enable(&venc_ptr->ctm.state, (uint8_t)tty_enable);
               voice_ctm_tx_commit_format( &venc_ptr->ctm.state, venc_samp_rate);
               result = voice_ctm_tx_resampler_init( &venc_ptr->ctm.state);
               if(ADSP_FAILED(result))
               {
                  elite_svc_send_ack(msg_ptr, result);
                  MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: CTM Tx resampler init failed in VENC, result(%x) session(%x) ",(int)result,(int)venc_ptr->session_num);
                  return result;
               }
            }
            break;
         }
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            if (( venc_ptr->reset_voc_flag ) || ( tty_enable ^ venc_ptr->tty.option ))
            {
               venc_ptr->tty.option = tty_enable;
               init_tty_enc(&venc_ptr->tty.enc_struct, (int16_t)venc_ptr->tty.option, CDMA_V13K);
            }
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            if (( venc_ptr->reset_voc_flag ) || ( tty_enable ^ venc_ptr->tty.option ))
            {
               venc_ptr->tty.option = tty_enable;
               init_tty_enc(&venc_ptr->tty.enc_struct, (int16_t)venc_ptr->tty.option, CDMA_EVRC);
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            if (( venc_ptr->reset_voc_flag ) || ( tty_enable ^ venc_ptr->tty.option ))
            {
               venc_ptr->tty.option = tty_enable;
               init_tty_enc(&venc_ptr->tty.enc_struct, (int16_t)venc_ptr->tty.option, CDMA_FOURGV);

            }
            break;
         }
      default:
         {
            break; // do nothing
         }
   }
   // Init downsampler memory (used for 1x TTY)
   if(wideband)
   {
      int32_t resample_result;
      resample_result = voice_resampler_channel_init(
            &(venc_ptr->tty.resamp_config),
            venc_ptr->tty.onex_downby2_mem_ptr,
            venc_ptr->tty.resamp_config.total_channel_mem_size
            );
      if(VOICE_RESAMPLE_SUCCESS != resample_result)
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: 1x TTY down sampler init failed");
      }
   }
   // TTY should not be enable for both CS and PS mode
   if ((tty_enable)&&(oobtty_enable))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error ! Both CS and PS TTY enabled ! : session(%lx)",venc_ptr->session_num);
      elite_svc_send_ack(msg_ptr, ADSP_EFAILED);
      return ADSP_EFAILED;
   }
   // initialize vocoder only when necessary (handover to different media type, or after reinit command)
   if( venc_ptr->reset_voc_flag )
   {

      venc_ptr->reset_voc_flag = FALSE;

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Init venc in run cmd : session(%lx)",venc_ptr->session_num);
      venc_ptr->send_media_type = TRUE;

      /* perform vocoder encoder initialization here */
      switch(venc_ptr->voc_type)
      {
         case VSM_MEDIA_TYPE_EVRC_MODEM:
            {
               result = venc_check_enc_struct_size(get_sizeof_evrc_enc_struct(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,get_sizeof_evrc_enc_struct());
               evrc_init_enc((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_AMR_NB_MODEM:
         case VSM_MEDIA_TYPE_AMR_NB_IF2:
            {
               result = venc_check_enc_struct_size(get_eamr_enc_struct_size(),venc_ptr->session_num); // similar api for get_struct_size for eAMR and AMR-NB
               if (ADSP_FAILED(result))
               {
                  return result;
               }

               result = (eamr_encoder_frame_init((void*)venc_ptr->enc_state_ptr, static_cast<int16_t> (0),
                        static_cast<int16_t> (1), 8000) || eamr_sid_sync_init((void*)venc_ptr->enc_state_ptr));     // AMR-NB init (eAMR init with SR 8000)
               break;
            }
         case VSM_MEDIA_TYPE_AMR_WB_MODEM:
            {
               /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
               result = venc_check_enc_struct_size(get_amr_wb_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,get_amr_wb_enc_struct_size());
               amrwb_init_encoder((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_EAMR:
            {
               /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
               result = venc_check_enc_struct_size(get_eamr_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               result = (eamr_encoder_frame_init((void*)venc_ptr->enc_state_ptr, static_cast<int16_t> (0),
                        static_cast<int16_t> (1), 16000) || eamr_sid_sync_init((void*)venc_ptr->enc_state_ptr)); // eAMR init with SR 16000
               break;
            }

         case VSM_MEDIA_TYPE_EFR_MODEM:
            {
               result = venc_check_enc_struct_size(get_efr_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,get_efr_enc_struct_size());
               efr_reset_enc((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_FR_MODEM:
            {
               result = venc_check_enc_struct_size(get_fr_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,get_fr_enc_struct_size());
               gsm_fr_vad_reset((void*)venc_ptr->enc_state_ptr);
               gsm_fr_encoderInit((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_HR_MODEM:
            {
               result = venc_check_enc_struct_size(gsmhr_get_hr_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,gsmhr_get_hr_enc_struct_size());
               gsmhr_resetEnc((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_PCM_8_KHZ:
         case VSM_MEDIA_TYPE_PCM_16_KHZ:
         case VSM_MEDIA_TYPE_PCM_32_KHZ:
         case VSM_MEDIA_TYPE_PCM_48_KHZ:
            {
               break;
            }
         case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
            {
               int16_t voice_resamp_mode_16_bit = (GEN_RESAMP_LP_LPH << 6)|GEN_RESAMP_FIXED_OUTPUT_MODE; /* Resampler Quality: Low Power Linear Phase, FIXED OUTPUT mode*/
               ADSPResult resampler_result = voice_gen_resamp_init(&(venc_ptr->proc.codec_resampler),(int32)VOICE_FB_SAMPLING_RATE ,(int32)VOICE_44_1_SAMPLING_RATE, voice_resamp_mode_16_bit);
               if( ADSP_EOK != resampler_result)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Generic Resampler initialization failed for encoder session(%x)",(int)venc_ptr->session_num);
                  return ADSP_EFAILED;
               }
               break;
            }
         case VSM_MEDIA_TYPE_4GV_NB_MODEM:
         case VSM_MEDIA_TYPE_4GV_WB_MODEM:
         case VSM_MEDIA_TYPE_4GV_NW_MODEM:
         case VSM_MEDIA_TYPE_4GV_NW:
         case VSM_MEDIA_TYPE_EVRC_NW_2K:
            {
               //some initializations used in init_encoder function.
               FourGVEncode* four_gv_encode_ptr = (FourGVEncode*)venc_ptr->voc.four_gv_encode_ptr;
               four_gv_encode_ptr->max_rate        =4; //default to full-rate
               four_gv_encode_ptr->min_rate        =1; //default to eight -rate
               four_gv_encode_ptr->dtx             =0; //default DTX OFF
               four_gv_encode_ptr->Fsop            =8000;
               four_gv_encode_ptr->Fsinp           =8000;
               four_gv_encode_ptr->operating_point =0;
               four_gv_encode_ptr->avg_rate_target =10000; //default to max rate
               four_gv_encode_ptr->joint_source_modem_dtx = 0;
               if(VSM_MEDIA_TYPE_4GV_WB_MODEM == venc_ptr->voc_type)
               {
                  four_gv_encode_ptr->Fsop            =16000;
                  four_gv_encode_ptr->Fsinp           =16000;
                  four_gv_encode_ptr->avg_rate_target =8500; //default to max rate
                  four_gv_encode_ptr->operating_point =3;
               }
               else if((VSM_MEDIA_TYPE_4GV_NW_MODEM == venc_ptr->voc_type) || (VSM_MEDIA_TYPE_4GV_NW == venc_ptr->voc_type))
               {
                  four_gv_encode_ptr->Fsop            =16000;
                  four_gv_encode_ptr->Fsinp           =16000;
                  four_gv_encode_ptr->avg_rate_target =6600; //default COP4
                  four_gv_encode_ptr->operating_point =1;    //for EVRC-NW

                  if(VSM_MEDIA_TYPE_4GV_NW_MODEM == venc_ptr->voc_type)
                  {
                     four_gv_encode_ptr->dtx             =1;         // always enable DTX for EVRC-NW CS calls
                     four_gv_encode_ptr->joint_source_modem_dtx = 1; // enable for CS calls for non critical 1/8th rate encoding
                  }
               }
               else if(VSM_MEDIA_TYPE_EVRC_NW_2K == venc_ptr->voc_type)
               {
                  four_gv_encode_ptr->Fsop                   = 16000;
                  four_gv_encode_ptr->Fsinp                  = 16000;
                  four_gv_encode_ptr->avg_rate_target        = 2400;
                  four_gv_encode_ptr->operating_point        = 2;
                  four_gv_encode_ptr->dtx                    = 1; // always enable DTX for SO77
                  four_gv_encode_ptr->joint_source_modem_dtx = 1;
                  venc_ptr->voc.fgv_avg_rate_control = 1;
               }
               four_gv_encode_ptr->init_encoder();
               break;
            }
         case VSM_MEDIA_TYPE_13K_MODEM:
            {
               result = venc_check_enc_struct_size(v13k_get_enc_struct_size(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset(venc_ptr->enc_state_ptr,0,v13k_get_enc_struct_size());
               v13k_init_encoder13((void*)venc_ptr->enc_state_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_G711_ALAW:
         case VSM_MEDIA_TYPE_G711_MLAW:
         case VSM_MEDIA_TYPE_G711_ALAW_V2:
         case VSM_MEDIA_TYPE_G711_MLAW_V2:
            {
               venc_ptr->voc.g711_frame_size = 80;
               if ( VSM_MEDIA_TYPE_G711_ALAW_V2 == venc_ptr->voc_type || VSM_MEDIA_TYPE_G711_MLAW_V2 == venc_ptr->voc_type )
               {
                  venc_ptr->voc.g711_frame_size = 160;
               }
               venc_ptr->voc.g711_frame = 0;
               result = venc_check_enc_struct_size(G711EncoderGetSize(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               memset(venc_ptr->enc_state_ptr,0,G711EncoderGetSize());
               G711A2EncoderInit((void *) venc_ptr->enc_state_ptr,venc_ptr->voc.g711_frame_size);
               break;
            }
         case VSM_MEDIA_TYPE_G729AB:
            {
               result = venc_check_enc_struct_size(( g729a_get_g729a_enc_struct_size() + g729a_get_g729a_enc_com_struct_size()),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset(venc_ptr->enc_state_ptr,0,( g729a_get_g729a_enc_struct_size() +
                        g729a_get_g729a_enc_com_struct_size()));
               venc_ptr->voc.g729a_encoder_ptr = (int8_t *) venc_ptr->enc_state_ptr;
               venc_ptr->voc.g729a_common_ptr = venc_ptr->voc.g729a_encoder_ptr + g729a_get_g729a_enc_struct_size();
               (void) g729_initEnc((void *) venc_ptr->voc.g729a_encoder_ptr, (void *)venc_ptr->voc.g729a_common_ptr);
               break;
            }
         case VSM_MEDIA_TYPE_EVS:
            {
               /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
               result = venc_check_enc_struct_size(get_sizeof_evs_enc_struct(),venc_ptr->session_num);
               if (ADSP_FAILED(result))
               {
                  return result;
               }
               // todo: need this?
               memset((void*)venc_ptr->enc_state_ptr,0,get_sizeof_evs_enc_struct());

               if (evs_voc_init_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct)))
               {
                  MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Invalid parameters set on EVS: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                        venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                        venc_ptr->voc.evs_param_struct.enc_bit_rate,
                        venc_ptr->voc.evs_param_struct.enc_bandwidth,
                        venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                        venc_ptr->voc.evs_param_struct.fec_offset,
                        venc_ptr->voc.evs_param_struct.fer_rate,
                        venc_ptr->voc.evs_param_struct.dtx_enable);

                  elite_svc_send_ack(msg_ptr, ADSP_EFAILED);

                  return ADSP_EFAILED;
               }
               break;
            }
         default:
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid encoder type: 0x%x",(int)venc_ptr->voc_type);
            venc_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
            result = ADSP_EBADPARAM;
            /** terminate processing here itself if vocoder type was wrong **/
            elite_svc_send_ack(msg_ptr, result);
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_run_cmd end result=%x  session(%x)",(int)result,(int)venc_ptr->session_num);
            return result;
      }
   }
   // Subscribe to VDS. Should be done before VTM subscribe so that signal is created first
   result = voice_cmn_send_vds_command(&venc_ptr->vds_client_id, venc_ptr->vds_client_token, venc_ptr->resp_q_ptr, VDS_SUBSCRIBE,
         &venc_ptr->vtm_sub_unsub_data.signal_ptr, venc_ptr->vds_error_signal_ptr, venc_ptr->vds_handle_ptr, venc_ptr->session_num);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed Venc subscribe to VDS, session(%x)!",(int)venc_ptr->session_num);
      return result;
   }

   // Subscribe to VoiceTimer. todo: move to SetVfrMode and sub only for hard vfr and handle != NULL
   venc_ptr->vtm_sub_unsub_data.signal_enable  = 1;
   venc_vtm_subscribe(venc_ptr,&(venc_ptr->vtm_sub_unsub_data));

   if (VFR_NONE == venc_ptr->vfr_mode)
   {
      (void) qurt_elite_channel_wait(&(venc_ptr->qurt_elite_channel), VENC_TICK_END_MASK);
      qurt_elite_signal_clear(venc_ptr->vtm_sub_unsub_data.signal_end_ptr); //clear the signal
   }

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VoiceEnc::Run mediaType(%lx),ttyEnable(%d)",venc_ptr->voc_type,tty_enable);

   if ( ADSP_EOK == result )
   {
      venc_ptr->wait_mask |= (VENC_TICK_MASK | VENC_RESYNC_MASK);     // Begin listening for ticks/resync
      venc_ptr->process_data    = 1; // Start processing - RUN state
   }

   voice_cmn_time_profile_init( &venc_ptr->time_profile_struct);

   elite_svc_send_ack(msg_ptr, result);
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_run_cmd end result=%x, wait_mask=%x, session(%x)",(int)result,(int)venc_ptr->wait_mask,(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_destroy_yourself_cmd (void *instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_destroy((venc_t*)instance_ptr);

   // send ADSP_ETERMINATED so calling routine knows the destroyer has been invoked.
   return ADSP_ETERMINATED;
}

static ADSPResult venc_set_media_type(void *instance_ptr, elite_msg_any_t* msg_ptr)
{

   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;
   elite_msg_data_media_type_header_t* media_type_msg_payload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_media_type begin session(%x)",(int)venc_ptr->session_num);

   /* only handle VocType changes in stop state.  Latch all settings perform init/TTY init/postproc init/postproc
    * sample rate dependent init when transitioning to Run state
    */
   if( TRUE == venc_ptr->process_data)
   {
      result = ADSP_EBUSY;
   }
   else
   {
      media_type_msg_payload = (elite_msg_data_media_type_header_t*) msg_ptr->pPayload;

      // set reset flag if we need to reset vocoder.  Latch flag high, only reset when actually processed (Run)
      if( venc_ptr->voc_type != media_type_msg_payload->unMediaTypeFormat)
      {
         venc_ptr->reset_voc_flag = TRUE;
         if(venc_ptr->voc_type == VSM_MEDIA_TYPE_EVS)
         {
            evs_enc_destroy((void *)venc_ptr->enc_state_ptr);
         }
         // Media type change and hence reset encoder config params
         (void) venc_enc_params_init(venc_ptr);
         if( (VSM_MEDIA_TYPE_4GV_NB_MODEM == venc_ptr->voc_type ) ||
               (VSM_MEDIA_TYPE_4GV_WB_MODEM == venc_ptr->voc_type ) ||
               (VSM_MEDIA_TYPE_4GV_NW_MODEM == venc_ptr->voc_type ) ||
               (VSM_MEDIA_TYPE_4GV_NW == venc_ptr->voc_type ) ||
               (VSM_MEDIA_TYPE_EVRC_NW_2K == venc_ptr->voc_type ) )
         {
            if ( NULL != venc_ptr->voc.four_gv_encode_ptr)
            {
               venc_ptr->voc.four_gv_encode_ptr->FGVEncodeDestructor();
               venc_ptr->voc.four_gv_encode_ptr = NULL;
            }
         }

         switch(media_type_msg_payload->unMediaTypeFormat)
         {
            case VSM_MEDIA_TYPE_EVRC_MODEM:
               {
                  if(0==get_sizeof_evrc_enc_struct())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_AMR_NB_MODEM:
            case VSM_MEDIA_TYPE_AMR_NB_IF2:
               {
                  if(0==get_eamr_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_EAMR:
               {
                  if(0==get_eamr_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }

            case VSM_MEDIA_TYPE_AMR_WB_MODEM:
               {
                  if(0==get_amr_wb_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_EFR_MODEM:
               {
                  if(0==get_efr_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_FR_MODEM:
               {
                  if(0==get_fr_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_HR_MODEM:
               {
                  if(0==gsmhr_get_hr_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_PCM_8_KHZ:
            case VSM_MEDIA_TYPE_PCM_16_KHZ:
            case VSM_MEDIA_TYPE_PCM_32_KHZ:
            case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
            case VSM_MEDIA_TYPE_PCM_48_KHZ:
               {
                  venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  break;
               }
            case VSM_MEDIA_TYPE_4GV_NB_MODEM:
            case VSM_MEDIA_TYPE_4GV_WB_MODEM:
            case VSM_MEDIA_TYPE_4GV_NW_MODEM:
            case VSM_MEDIA_TYPE_4GV_NW:
            case VSM_MEDIA_TYPE_EVRC_NW_2K:

               {
                  if(NULL == venc_ptr->enc_state_ptr)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     init_fourgv_enc_static_mem(venc_ptr->enc_state_ptr,&venc_ptr->voc.four_gv_encode_ptr);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     if((VSM_MEDIA_TYPE_4GV_NW_MODEM == venc_ptr->voc_type) || (VSM_MEDIA_TYPE_EVRC_NW_2K == venc_ptr->voc_type))
                     {
                        venc_ptr->voc.four_gv_encode_ptr->dtx = 1;
                        venc_ptr->voc.four_gv_encode_ptr->joint_source_modem_dtx = 1;
                     }
                     else
                     {
                        venc_ptr->voc.four_gv_encode_ptr->joint_source_modem_dtx = 0;
                     }

                  }
                  break;
               }

            case VSM_MEDIA_TYPE_13K_MODEM:
               {
                  if(0==v13k_get_enc_struct_size())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }

            case VSM_MEDIA_TYPE_G711_ALAW:
            case VSM_MEDIA_TYPE_G711_MLAW:
            case VSM_MEDIA_TYPE_G711_ALAW_V2:
            case VSM_MEDIA_TYPE_G711_MLAW_V2:
               {
                  if(0==G711EncoderGetSize())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_G729AB:
               {
                  if(0==( g729a_get_g729a_enc_struct_size() +
                           g729a_get_g729a_enc_com_struct_size()))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            case VSM_MEDIA_TYPE_EVS:
               {
                  if(0==get_sizeof_evs_enc_struct())
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%x. library structure Size 0",
                           (int)media_type_msg_payload->unMediaTypeFormat);
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                     result = ADSP_EOK;
                  }
                  else
                  {
                     venc_ptr->voc_type = media_type_msg_payload->unMediaTypeFormat;
                  }
                  break;
               }
            default:
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid encoder type: 0x%x",(int)media_type_msg_payload->unMediaTypeFormat);
               venc_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
               result = ADSP_EBADPARAM;
               break;
         }

         //Update default operating mode
         venc_set_default_op_mode(instance_ptr);
      }
   }

   // always issue event notification for set_media_type
   venc_send_mode_notification_v2(instance_ptr);

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_media_type end  session(%x)",(int)venc_ptr->session_num);
   elite_svc_send_ack(msg_ptr, result);
   return result;
}

static ADSPResult venc_set_pp_samp_rate(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_voc_stream_set_sampling_type *set_samp_rate_cmd_ptr = (elite_msg_custom_voc_stream_set_sampling_type *) msg_ptr->pPayload;
   switch(venc_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_EVS:
         if(venc_ptr->voc.evs_param_struct.enc_sampling_rate != set_samp_rate_cmd_ptr->tx_samp_rate)
         {
            venc_ptr->reset_voc_flag = TRUE;
            evs_enc_destroy((void *)venc_ptr->enc_state_ptr);
            venc_ptr->voc.evs_param_struct.enc_sampling_rate = set_samp_rate_cmd_ptr->tx_samp_rate;
         }

         break;
      default:
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Invalid Vocoder(0x%lx) for set pp samp rate cmd!", venc_ptr->voc_type);
         result = ADSP_EUNSUPPORTED;
         break;
   }
   elite_svc_send_ack(msg_ptr, result);
   return result;
}

static ADSPResult venc_stop_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_stop_cmd begin session(%x)",(int)venc_ptr->session_num);

   venc_ptr->process_data    = 0; // Stop processing

   venc_vtm_unsubscribe(venc_ptr,&(venc_ptr->vtm_sub_unsub_data),VENC_TICK_END_MASK);

   // Unsubscribe from VDS. Should be done after timer unsubscribe
   result = voice_cmn_send_vds_command(&venc_ptr->vds_client_id, venc_ptr->vds_client_token, venc_ptr->resp_q_ptr, VDS_UNSUBSCRIBE,
         &venc_ptr->vtm_sub_unsub_data.signal_ptr, venc_ptr->vds_error_signal_ptr, venc_ptr->vds_handle_ptr, venc_ptr->session_num);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed Venc unsubscribe to VDS session(%x)!",(int)venc_ptr->session_num);
      return result;
   }

   venc_ptr->wait_mask &= ~(VENC_TICK_MASK | VENC_RESYNC_MASK | VENC_REC_BUF_MASK | VENC_VDS_RESPONSE_MASK) ; // Stop listening to Output buffers, ticks and errors from VDS.

   // Reset control code params
   (void) venc_ctrl_params_init(venc_ptr);

   result = venc_ptr->proc.comfort_noise_ptr->ProcessEnd();
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ComfortNoise Process End failed  session(%x)",(int)venc_ptr->session_num);
   }

   //De-alloc memory allocated for dtmf gen resampler
   voice_dtmf_resampler_mem_free(venc_ptr->proc.dtmf_gen_ptr);
   // OOB statistics

   if (venc_ptr->pkt_exchange_mode == OUT_OF_BAND)
   {
      MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc OOB Pkt miss count from call start %ld, miss due previous pkt not consumed (%d) total pkts sent %ld, "
            "total packets consumed by client %ld session(%lx)",venc_ptr->pkt_miss_ctr, (int)venc_ptr->oob.pkt_miss_ctr, venc_ptr->pkt_ctr, venc_ptr->oob.pkt_consumed_ctr,venc_ptr->session_num);
   }
   // print out profiling stats
   if( venc_ptr->time_profile_struct.num_samples > 0)
   {
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_stop session(%x) voc_type(%x): profiler microsec: num_samples(%d) max_time(%d) min_time(%d) avg_time(%d) max_cycles(%d)",
            (int)venc_ptr->session_num,
            (int)venc_ptr->voc_type,
            (int)venc_ptr->time_profile_struct.num_samples,
            (int)venc_ptr->time_profile_struct.max_time,
            (int)venc_ptr->time_profile_struct.min_time,
            (int)(venc_ptr->time_profile_struct.total_time / venc_ptr->time_profile_struct.num_samples),
            (int) venc_ptr->time_profile_struct.max_cycles  );
   }

   //if soft mute is in ramping state, it should be disabled in stop command and bring into
   //the target state (either MUTE/UNMUTE) immediately.Because we dont know after how much
   //time run issued and it makes no sense to carry the ramp state after run.
   CrossFadeDataType *mute_data_ptr = &(venc_ptr->proc.cross_fade_data_struct);

   mute_data_ptr->state = STEADY_STATE;

   // ack back to msg sender
   elite_svc_send_ack(msg_ptr, result);

   if (TRUE == venc_ptr->record.enable)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_stop_cmd, In call conversation Record has not been stopped");
   }

   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_stop_cmd end result=%x, wait_mask=%x, session(%x)",(int)result,(int)venc_ptr->wait_mask,(int)venc_ptr->session_num);
   return result;
}

// todo: use fadd msg ?
static ADSPResult venc_connect_dwn_stream_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_cmd_connect_t* connect_msg_payload_ptr;
   elite_svc_handle_t *svc2_connect_ptr;
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_connect_dwn_stream_cmd begin  session(%x)",(int)venc_ptr->session_num);

   if (venc_ptr->process_data)
   {
      result = ADSP_ENOTREADY; // Cannot reconnect in RUN state. todo: needed?
      elite_svc_send_ack(msg_ptr, result);
      return result;
   }

   connect_msg_payload_ptr = (elite_msg_cmd_connect_t*) (msg_ptr->pPayload);
   svc2_connect_ptr = connect_msg_payload_ptr->pSvcHandle;
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc connecting to SvcID 0x%8x session(%x)",(int)svc2_connect_ptr->unSvcId,(int)venc_ptr->session_num);

   // This service only allows one downstream
   if (NULL != venc_ptr->downstream_peer_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc peer connection failed, already connected  session(%x)",(int)venc_ptr->session_num);
      result = ADSP_EUNSUPPORTED;
   }
   else //else accept the connection
   {
      venc_ptr->downstream_peer_ptr = svc2_connect_ptr;
   }
   elite_svc_send_ack(msg_ptr, result);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_connect_dwn_stream_cmd end result=%x  session(%x)",(int)result,(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_disconnect_dwn_stream_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_cmd_connect_t* disconnect_msg_payload_ptr;
   elite_svc_handle_t *svc2_disconnect_ptr;
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc_DisConnectDwnStreamCmd begin  session(%x)",(int)venc_ptr->session_num);

   disconnect_msg_payload_ptr = (elite_msg_cmd_connect_t*) (msg_ptr->pPayload);
   svc2_disconnect_ptr = disconnect_msg_payload_ptr->pSvcHandle ;
   if (svc2_disconnect_ptr != venc_ptr->downstream_peer_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED, not connected to that service. session(%x)",(int)venc_ptr->session_num);
      result = ADSP_EBADPARAM;
   }
   else //else accept the dis-connection
   {
      venc_ptr->downstream_peer_ptr = NULL;
   }

   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc_DisConnectDwnStreamCmd end result=%x  session(%x)",(int)result,(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_set_param_int(venc_t* venc_ptr, uint32_t module_id, uint32_t param_id, void* payload_address_in, uint32_t param_size)
{
   ADSPResult result = ADSP_EOK;
   int8_t* calibration_data_pay_load_ptr = (int8_t*)payload_address_in;
   switch (module_id)
   {
      case VOICESTREAM_MODULE_TX:
         {
            /* handle loopback param */
            if( VOICE_PARAM_LOOPBACK_ENABLE == param_id)
            {
               int16_t enable_flag = *((int16_t*)calibration_data_pay_load_ptr);
               if (sizeof(int32_t) != param_size)
               {
                  result = ADSP_EBADPARAM;
               }
               else
               {
                  if( TRUE == enable_flag)
                  {
                     venc_ptr->loopback_enable           = TRUE;
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_set_param_cmd Venc->Vdec Loopback Enable session(%x)",(int)venc_ptr->session_num);
                  }
                  else
                  {
                     venc_ptr->loopback_enable           = FALSE;
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_set_param_cmd Venc->Vdec Loopback Disable session(%x)",(int)venc_ptr->session_num);
                  }
                  result = ADSP_EOK;
               }
            }
         }
         break;
      case VOICE_MODULE_ENC_CNG:
         {
            result = venc_ptr->proc.comfort_noise_ptr->SetParam(calibration_data_pay_load_ptr,param_id,param_size);
         }
         break;
      case VOICE_MODULE_TX_STREAM_LIMITER:
         {
            if (VOICE_PARAM_MOD_ENABLE == param_id)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: SetParam Tx Stream Limiter enable %x", *calibration_data_pay_load_ptr);
               venc_ptr->proc.limiter_params[LIM_ENABLE]      = *((int16_t*)calibration_data_pay_load_ptr);      // Q0 Limiter enable word

               // Initialize Limiter
               result = Lim_init(venc_ptr->proc.limiter_ptr,venc_ptr->proc.limiter_params,0/*channel index*/,venc_ptr->samp_rate,NO_OF_BITS_PER_SAMPLE);
            }
            else if (VOICE_PARAM_TX_STREAM_LIMITER == param_id)
            {
               tx_stream_limiter_t *lim_param_ptr = (tx_stream_limiter_t*)calibration_data_pay_load_ptr;

               venc_ptr->proc.limiter_params[LIM_MODE]        = lim_param_ptr->sLimMode;      // Q0 Limiter mode word
               venc_ptr->proc.limiter_params[LIM_MAKEUP_GAIN] = lim_param_ptr->sLimMakeUpGain;    // Q7.8 Limiter make-up gain
               venc_ptr->proc.limiter_params[LIM_GC]          = lim_param_ptr->sLimGc;   // Q15 Limiter gain recovery coefficient
               venc_ptr->proc.limiter_params[LIM_DELAY]       = lim_param_ptr->sLimDelay;      // Q15 Limiter waiting time in seconds 1ms
               venc_ptr->proc.limiter_params[LIM_MAX_WAIT]    = lim_param_ptr->sLimMaxWait;    // Q15 Limiter delay in seconds
               venc_ptr->proc.limiter_params[LIM_THRESH]      = lim_param_ptr->nLimThreshold;   // -12 dB in Q1.15 for limiting to 14 bits on tx path

               MSG_6(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: SetParam Tx Stream Limiter params, sLimMode=%x, sLimMakeUpGain=%x, sLimGc=%x, sLimDelay=%x, sLimMaxWait=%x, nLimThreshold=%x",
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_MODE],
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_MAKEUP_GAIN],
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_GC],
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_DELAY],
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_MAX_WAIT],
                     (unsigned int)venc_ptr->proc.limiter_params[LIM_THRESH]);

               if (LIM_MAX_DELAY < venc_ptr->proc.limiter_params[LIM_DELAY])
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Tx Stream Limiter delay too high, defaulting to %d, session (%lx)", LIM_MAX_DELAY,venc_ptr->session_num);
                  venc_ptr->proc.limiter_params[LIM_DELAY] = LIM_MAX_DELAY;
               }

               // Initialize Limiter
               result = Lim_init(venc_ptr->proc.limiter_ptr,venc_ptr->proc.limiter_params,0/*channel index*/,venc_ptr->samp_rate,NO_OF_BITS_PER_SAMPLE);
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: SetParam Tx Stream Limiter invalid PID %x", (unsigned int)param_id);
               result = ADSP_EUNSUPPORTED;
            }
         }

         break;
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported set enc module 0x%lx", module_id);
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }
   return result;
}

static ADSPResult venc_set_param_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_param_cmd begin  session(%x)",(int)venc_ptr->session_num);

   if (ELITE_CMD_SET_PARAM == msg_ptr->unOpCode)
   {
      elite_msg_param_cal_t* fadd_payload_ptr = (elite_msg_param_cal_t*) msg_ptr->pPayload;
      if (ELITEMSG_PARAM_ID_CAL == fadd_payload_ptr->unParamId)
      {
         uint32_t byte_size_counter=0;
         int8_t *calibration_data_pay_load_ptr;
         voice_param_data_t *ptr = (voice_param_data_t *) fadd_payload_ptr->pnParamData;
         uint32_t nPayloadSize = fadd_payload_ptr->unSize;
         uint32_t payload_address = (uint32_t) ptr;

         {
            ADSPResult local_result = ADSP_EUNSUPPORTED;
            /* initialize to unsupported in case no valid module_id is found */

            // Iterate through the entire payload size and copy all updated parameters
            do
            {
               ptr = (voice_param_data_t *)payload_address;
               calibration_data_pay_load_ptr = (int8_t*)ptr + sizeof(voice_param_data_t);
               local_result = venc_set_param_int(venc_ptr, ptr->module_id, ptr->param_id, calibration_data_pay_load_ptr, ptr->param_size);
               if (!((ADSP_EOK == local_result) || (ADSP_EUNSUPPORTED == local_result)))
               {

                  break; // in case of any error dont go forward with parsing
               }
               byte_size_counter += (sizeof(voice_param_data_t) + ptr->param_size);
               payload_address += (sizeof(voice_param_data_t) + ptr->param_size);
            } while(byte_size_counter < nPayloadSize);
            elite_svc_send_ack(msg_ptr, local_result);
         }
      }
      else
      {
         elite_svc_send_ack(msg_ptr, ADSP_EFAILED);
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Invalid param id session(%lx)",venc_ptr->session_num);
      }
   }
   else
   {
      result = ADSP_EFAILED;
      elite_svc_send_ack(msg_ptr, result);
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_param_cmd secondary opcode not supported session(%lx)",venc_ptr->session_num);

   }

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_param_cmd end result=%x  session(%x)",(int)result,(int)venc_ptr->session_num);
   return ADSP_EOK;
}
// todo: add meat
static ADSPResult venc_get_param_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_get_param_cmd begin session(%lx)",venc_ptr->session_num);
   elite_msg_param_cal_t* fadd_payload_ptr = (elite_msg_param_cal_t*) msg_ptr->pPayload;
   voice_param_data_t* voice_data_param_ptr;

   if (ELITE_CMD_GET_PARAM == msg_ptr->unOpCode)
   {
      ADSPResult local_result = ADSP_EUNSUPPORTED;
      if (ELITEMSG_PARAM_ID_CAL == fadd_payload_ptr->unParamId)
      {
         // extract Voice params payload pointer
         voice_data_param_ptr = (voice_param_data_t*) fadd_payload_ptr->pnParamData;

         // Extract address of Cal data and size info
         // param_max_size in the interface includes the size of the param data as well as the header
         // so need to subtract header size to ensure param fits in allocated memory
         uint32_t param_size_max = voice_data_param_ptr->param_size - sizeof(voice_param_data_t);
         int8_t* calibration_data_pay_load_ptr = (int8_t*)(((int8_t*)fadd_payload_ptr->pnParamData) + sizeof(voice_param_data_t));

         {  // TODO: put the following in a big function which includes all the mods and parameters function

            // Iterate through the entire payload size and copy all updated parameters
            switch (voice_data_param_ptr->module_id)
            {

               case VOICESTREAM_MODULE_TX:
                  {
                     if( VOICE_PARAM_LOOPBACK_ENABLE == voice_data_param_ptr->param_id)
                     {
                        voice_data_param_ptr->param_size = sizeof(int32_t);

                        if( voice_data_param_ptr->param_size > param_size_max)
                        {
                           local_result = ADSP_ENOMEMORY;
                        }
                        else
                        {
                           *((int16_t*)calibration_data_pay_load_ptr) = (int16_t) venc_ptr->loopback_enable;
                           // Clear reserved word
                           *((int16_t*)calibration_data_pay_load_ptr+1) = 0;
                           local_result = ADSP_EOK;
                        }
                     }
                     break;
                  }

               case VOICE_MODULE_ENC_CNG:
                  {
                     local_result = venc_ptr->proc.comfort_noise_ptr->GetParam(calibration_data_pay_load_ptr,voice_data_param_ptr->param_id,param_size_max, &voice_data_param_ptr->param_size);
                  }
                  break;
               case VOICE_MODULE_TX_STREAM_LIMITER:
                  {
                     if (VOICE_PARAM_MOD_ENABLE == voice_data_param_ptr->param_id)
                     {
                        voice_data_param_ptr->param_size = sizeof(int32_t);
                        if(voice_data_param_ptr->param_size > param_size_max)
                        {
                           MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Tx Stream Limiter Get enable required size = %d, given size = %ld",voice_data_param_ptr->param_size, param_size_max);
                           local_result = ADSP_ENOMEMORY;
                        }
                        else
                        {
                           *((int16_t*)calibration_data_pay_load_ptr) = (int16_t) venc_ptr->proc.limiter_params[LIM_ENABLE];
                           MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: GetParam Tx Stream Limiter enable %x", *((int16_t*)calibration_data_pay_load_ptr));
                           local_result = ADSP_EOK;
                        }
                     }
                     else if (VOICE_PARAM_TX_STREAM_LIMITER == voice_data_param_ptr->param_id)
                     {
                        voice_data_param_ptr->param_size = sizeof(tx_stream_limiter_t);
                        if(voice_data_param_ptr->param_size > param_size_max)
                        {
                           MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Tx Stream Limiter Get param required size = %d, given size = %ld",voice_data_param_ptr->param_size, param_size_max);
                           local_result = ADSP_ENOMEMORY;
                        }
                        else
                        {
                           tx_stream_limiter_t *lim_param_ptr = (tx_stream_limiter_t*)calibration_data_pay_load_ptr;

                           lim_param_ptr->sLimMode = venc_ptr->proc.limiter_params[LIM_MODE];    // Q0 Limiter mode word
                           lim_param_ptr->sLimMakeUpGain = venc_ptr->proc.limiter_params[LIM_MAKEUP_GAIN];    // Q7.8 Limiter make-up gain
                           lim_param_ptr->sLimGc = venc_ptr->proc.limiter_params[LIM_GC];    // Q15 Limiter gain recovery coefficient
                           lim_param_ptr->sLimDelay = venc_ptr->proc.limiter_params[LIM_DELAY];   // Q15 Limiter waiting time in seconds 1ms
                           lim_param_ptr->sLimMaxWait = venc_ptr->proc.limiter_params[LIM_MAX_WAIT];    // Q15 Limiter delay in seconds
                           lim_param_ptr->nLimThreshold = venc_ptr->proc.limiter_params[LIM_THRESH];  // -12 dB in Q1.15 for limiting to 14 bits on tx path

                           MSG_6(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: GetParam Tx Stream Limiter params, sLimMode=%x, sLimMakeUpGain=%x, sLimGc=%x, sLimDelay=%x, sLimMaxWait=%x, nLimThreshold=%x",
                                 (unsigned int)lim_param_ptr->sLimMode,
                                 (unsigned int)lim_param_ptr->sLimMakeUpGain,
                                 (unsigned int)lim_param_ptr->sLimGc,
                                 (unsigned int)lim_param_ptr->sLimDelay,
                                 (unsigned int)lim_param_ptr->sLimMaxWait,
                                 (unsigned int)lim_param_ptr->nLimThreshold);
                           local_result = ADSP_EOK;
                        }
                     }
                     else
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: GetParam Tx Stream Limiter invalid PID %x", (unsigned int)voice_data_param_ptr->param_id);
                        local_result = ADSP_EUNSUPPORTED;
                     }
                     break;
                  }

               default:
                  {
                     MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported Venc module");
                     local_result = ADSP_EUNSUPPORTED;
                     break;
                  }
            }
            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc get param, mod id(0x%lx), param id (0x%lx), result(0x%x) session(0x%lx)\n",
                  voice_data_param_ptr->module_id, voice_data_param_ptr->param_id, local_result, venc_ptr->session_num);
            result = local_result;
         } // Local scope


      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc get param bad fadd id,  result(0x%x) session(0x%lx)\n",
               ADSP_EFAILED, venc_ptr->session_num);
         result = ADSP_EFAILED;
      }
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_get_param_cmd secondary opcode not supported :(%lx)\n",venc_ptr->session_num);
      result = ADSP_EFAILED;
   }

   if(ADSP_SUCCEEDED(result))
   {
      fadd_payload_ptr->unSize = voice_data_param_ptr->param_size;
   }
   else
   {
      fadd_payload_ptr->unSize = 0;
   }
   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_get_param_cmd end handler result(0x%x) session(0x%x)",result,(int)venc_ptr->session_num);
   return ADSP_EOK;
}

static ADSPResult venc_get_kpps_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_kpps_type *kpps_msg_ptr= (elite_msg_custom_kpps_type*) msg_ptr->pPayload;
   vsm_get_kpps_ack_t* kpps_ptr = (vsm_get_kpps_ack_t*)kpps_msg_ptr->pnKpps;
   uint32_t kpps_changed=FALSE;

   if( kpps_ptr )
   {
      kpps_ptr->venc_kpps = 0;  // Initialization
      result = venc_aggregate_modules_kpps(instance_ptr, &kpps_changed);

      if(ADSP_SUCCEEDED(result))
      {
         kpps_ptr->venc_kpps = venc_ptr->aggregate_kpps;
      }
   }
   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_get_kpps_cmd end result(%d) session(%x)",(int)result,(int)venc_ptr->session_num);
   return ADSP_EOK;
}

static ADSPResult venc_get_delay_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_delay_type *delay_msg_ptr= (elite_msg_custom_delay_type*) msg_ptr->pPayload;
   vsm_get_delay_ack_t* delay_ptr = (vsm_get_delay_ack_t*)delay_msg_ptr->delay_ptr;

   if( delay_ptr )
   {
      delay_ptr->venc_delay = 0;  // Initialization
      result = venc_aggregate_modules_delay(instance_ptr);

      if(ADSP_SUCCEEDED(result))
      {
         delay_ptr->venc_delay = venc_ptr->aggregate_delay;
      }
   }
   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_get_delay_cmd end result(%d) session(%x)",(int)result,(int)venc_ptr->session_num);
   return ADSP_EOK;
}


static ADSPResult venc_set_mute_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_voc_set_soft_mute_type *set_mute_cmd_ptr = (elite_msg_custom_voc_set_soft_mute_type *) msg_ptr->pPayload;
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_soft_mute begin mute(%d),ramp_duration(%d), session(%x)",
         (int)set_mute_cmd_ptr->mute,(int)set_mute_cmd_ptr->ramp_duration,(int)venc_ptr->session_num);

   if( set_mute_cmd_ptr->mute > VOICE_MUTE)
   {
      result = ADSP_EBADPARAM;
   }
   else if (venc_ptr->mute != set_mute_cmd_ptr->mute)
   {
      venc_ptr->proc.cross_fade_cfg_struct.iConvergeNumSamples = 0;
      venc_ptr->proc.cross_fade_cfg_struct.iTotalPeriodMsec = set_mute_cmd_ptr->ramp_duration;

      //handling soft mute-init upon receiving new command
      CrossFadeCfgType *mute_cfg_ptr = &(venc_ptr->proc.cross_fade_cfg_struct);
      CrossFadeDataType *mute_data_ptr = &(venc_ptr->proc.cross_fade_data_struct);
      if ( 0 != mute_cfg_ptr->iTotalPeriodMsec )
      {
         cross_fade_init( mute_cfg_ptr,mute_data_ptr,voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate) );
         mute_data_ptr->state = CROSSFADE_STATE;
      }
      else
      {
         mute_data_ptr->state = STEADY_STATE;
      }

      // Set this state on Comfort Noise Generation
      venc_ptr->mute = set_mute_cmd_ptr->mute;
   }
   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_soft_mute end result(%d) session(%x)",(int)result,(int)venc_ptr->session_num);
   return result;
}


static ADSPResult venc_set_timing_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timing_cmd begin session(%lx)",venc_ptr->session_num);

   if (FALSE == venc_ptr->process_data)
   {
      voice_set_timing_params_t* vfr_cmd_ptr = (voice_set_timing_params_t*)set_timing_cmd_ptr->param_data_ptr;
      MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_SET_TIMING_PARAMS,enc vfr_mode(%x), enc offset(%x), process_data(%x)",vfr_cmd_ptr->mode, vfr_cmd_ptr->enc_offset, venc_ptr->process_data);

      if ((VFR_HARD_EXT >= vfr_cmd_ptr->mode) )
      {
         venc_ptr->vfr_mode = vfr_cmd_ptr->mode;
      }
      else
      {
         venc_ptr->vfr_mode = VFR_NONE;
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VOICE_SET_TIMING_PARAMS, Invalid enc vfr_mode(%x),setting vfr_mode to VFR_NONE",vfr_cmd_ptr->mode);
         result = ADSP_EBADPARAM;
      }
      venc_ptr->vtm_sub_unsub_data.vfr_mode = (uint8_t)venc_ptr->vfr_mode;

      venc_ptr->vfr_source = voice_cmn_get_vfr_source(venc_ptr->vfr_mode);

      // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min
      if (MAX_TIMER_OFFSET < vfr_cmd_ptr->enc_offset)
      {
         venc_ptr->vtm_sub_unsub_data.offset = 8000; // default 8ms
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VOICE_SET_TIMING_PARAMS, Invalid enc_offset(%x), Defaulting to 8msec",vfr_cmd_ptr->enc_offset);
         result = ADSP_EBADPARAM;
      }
      else
      {
         venc_ptr->vtm_sub_unsub_data.offset = vfr_cmd_ptr->enc_offset;
      }

      //update version of timing
      venc_ptr->vtm_sub_unsub_data.timing_ver = VFR_CLIENT_INFO_VER_1;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VFR mode can't be changed in RUN session(%lx)",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }

   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timing_cmd end result(%d) session(%lx)",result,venc_ptr->session_num);
   return result;
}
static ADSPResult venc_set_timingv2_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timingv2_cmd begin session(%lx)",venc_ptr->session_num);

   // Verify Stop state of thread before updating timing values.
   if (FALSE == venc_ptr->process_data)
   {
      vsm_set_timing_params_t* vfr_cmd_ptr = (vsm_set_timing_params_t*)set_timing_cmd_ptr->param_data_ptr;
      MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS,enc vfr_mode(%x), enc offset(%x), process_data(%x)",vfr_cmd_ptr->mode, vfr_cmd_ptr->enc_offset, venc_ptr->process_data);

      // Verify validity of VFR mode
      if ((VFR_HARD_EXT >= vfr_cmd_ptr->mode) )
      {
         venc_ptr->vfr_mode = vfr_cmd_ptr->mode;
      }
      else
      {
         venc_ptr->vfr_mode = VFR_NONE;
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS, Invalid enc vfr_mode(%x),setting vfr_mode to VFR_NONE",vfr_cmd_ptr->mode);
         result = ADSP_EBADPARAM;
      }
      venc_ptr->vtm_sub_unsub_data.vfr_mode = (uint8_t)venc_ptr->vfr_mode;

      // Select VFR source
      venc_ptr->vfr_source = voice_cmn_get_vfr_source(venc_ptr->vfr_mode);

      // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min
      if (MAX_TIMER_OFFSET < vfr_cmd_ptr->enc_offset)
      {
         venc_ptr->vtm_sub_unsub_data.offset = 8000; // default 8ms
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS, Invalid enc_offset(%x), Defaulting to 8msec",vfr_cmd_ptr->enc_offset);
         result = ADSP_EBADPARAM;
      }
      else
      {
         venc_ptr->vtm_sub_unsub_data.offset = vfr_cmd_ptr->enc_offset;
      }

      //update version of timing
      venc_ptr->vtm_sub_unsub_data.timing_ver = VFR_CLIENT_INFO_VER_1;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VFR mode can't be changed in RUN session(%lx)",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }

   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS end result(%d) session(%lx)",result,venc_ptr->session_num);
   return ADSP_EOK;
}

static ADSPResult venc_set_pkt_exchange_mode(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_pkt_exchange_mode_type *set_pkt_exchange_mode_ptr = (elite_msg_custom_pkt_exchange_mode_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_pkt_exchange_mode begin session(%lx)",venc_ptr->session_num);

   if (FALSE == venc_ptr->process_data)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VENC_SET_PKT_EXCHANGE_MODE ,pkt_exchange_mode(%d) session(%lx)",
            (int)set_pkt_exchange_mode_ptr->pkt_exchange_mode,venc_ptr->session_num);
      venc_ptr->pkt_exchange_mode = set_pkt_exchange_mode_ptr-> pkt_exchange_mode;
      /* make packet exchange pointer NULL as config command with new addresses must follow a mode command */
      venc_ptr->oob.pkt_exchange_ptr = NULL;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: pkt_exchange_mode mode can't be changed in RUN session(%lx)",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }
   elite_svc_send_ack(msg_ptr, result);
   return result;


}

static ADSPResult venc_set_oob_pkt_exchange_config(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_oob_pkt_exchange_config_type *set_oob_pkt_exchange_config_ptr = (elite_msg_custom_oob_pkt_exchange_config_type *) msg_ptr->pPayload;
   vsm_config_packet_exchange_t* pkt_exchange_config_ptr = (vsm_config_packet_exchange_t*)set_oob_pkt_exchange_config_ptr->param_data_ptr;


   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_pkt_exchange_config begin session(%lx)",venc_ptr->session_num);

   if (venc_ptr->process_data == TRUE)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: oob_pkt_exchange_config can't be come in RUN session(%lx) ",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }
   else
   {
      venc_ptr->oob.memmap.unMemMapClient = vsm_memory_map_client;
      venc_ptr->oob.memmap.unMemMapHandle = pkt_exchange_config_ptr->mem_handle;
      result = elite_mem_map_get_shm_attrib(pkt_exchange_config_ptr->enc_buf_addr_lsw,
            pkt_exchange_config_ptr->enc_buf_addr_msw,
            pkt_exchange_config_ptr->enc_buf_size,
            &venc_ptr->oob.memmap);
      if( ADSP_FAILED( result ))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Mapping to virtual address for OOB pkt exchange failed for ENCODER result %d! session(%lx)",
               result,venc_ptr->session_num);
      }
      else
      {
         venc_ptr->oob.pkt_exchange_ptr = (int32_t *)venc_ptr->oob.memmap.unVirtAddr;
         MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VENC_SET_OOB_PKT_EXCHANGE_CONFIG ,venc OOB address(%#x)  size (%d) session(%lx) ",
               (unsigned int)venc_ptr->oob.pkt_exchange_ptr, (int)pkt_exchange_config_ptr->enc_buf_size,venc_ptr->session_num);
      }
   }
   elite_svc_send_ack(msg_ptr, result);
   return result;

}

static ADSPResult venc_reinit_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_reinit_cmd begin session(%lx)",venc_ptr->session_num);

   if (FALSE == venc_ptr->process_data)
   {

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_reinit_cmd process session(%lx)",venc_ptr->session_num);

      /* reinit CTM */
      voice_ctm_tx_end( &venc_ptr->ctm.state );
      voice_ctm_tx_init( &venc_ptr->ctm.state, venc_ptr->session_num );

      // Must reset vocoder on reinit command
      venc_ptr->reset_voc_flag = TRUE;
      voice_oobtty_tx_set_config( &venc_ptr->oobtty_tx_struct, FALSE, 8000);

      (void) venc_init(venc_ptr);

      if (NULL != venc_ptr->voc.four_gv_encode_ptr)
      {
         venc_ptr->voc.four_gv_encode_ptr->FGVEncodeDestructor();
         venc_ptr->voc.four_gv_encode_ptr = NULL;
      }
      venc_ptr->wait_mask = VENC_CMD_DATA_MASK;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc Reinit can't be done in RUN session(%lx)",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }

   elite_svc_send_ack(msg_ptr, result);
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_reinit_cmd end result(%d), wait_mask=%lx, session(%lx)",result,venc_ptr->wait_mask,venc_ptr->session_num);
   return result;
}

static ADSPResult venc_start_record_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_custom_voc_svc_connect_record_type* connect_msg_payload_ptr;
   elite_svc_handle_t *svc_2_connect_ptr;
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   connect_msg_payload_ptr = (elite_msg_custom_voc_svc_connect_record_type*) (msg_ptr->pPayload);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_start_record_cmd opcode(%#lx)session(%#lx)",connect_msg_payload_ptr->sec_opcode, venc_ptr->session_num);

   svc_2_connect_ptr = connect_msg_payload_ptr->svc_handle_ptr;


   // This service only allows one downstream
   if (NULL != venc_ptr->record.afe_handle_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED, only 1 peer allowed session(%lx)",
            venc_ptr->session_num);
      result = ADSP_EUNSUPPORTED;
   }
   else //else accept the connection
   {
      venc_ptr->record.afe_handle_ptr = svc_2_connect_ptr;
      venc_ptr->record.enable = TRUE;
      venc_ptr->record.first_frame = TRUE;
      venc_ptr->record.ss_info_counter = 0; // reset drift info counter
      venc_ptr->record.ss_multiframe_counter = 0; //reset multiframe counter
      venc_ptr->record.mode = connect_msg_payload_ptr->record_mode;
      venc_ptr->record.aud_ref_port = connect_msg_payload_ptr->aud_ref_port;  // VSM abstracts out the v1 and v2 version of the api
      // vtm subscribe parameters
      venc_ptr->record.afe_drift_ptr  = connect_msg_payload_ptr->afe_drift_ptr; // afe drift pointer is stored only at 1 location, hence not in the if condition of vfr_hard

      venc_init_rec_circbuf(venc_ptr);

      // reset the hptimer vs device drift counter
      memset(&venc_ptr->record.voice_cmn_drift_info, 0, sizeof(venc_ptr->record.voice_cmn_drift_info));
      //update nb_sampling rate factor..
      venc_ptr->record.voice_cmn_drift_info.nb_sampling_rate_factor = venc_ptr->samp_rate_factor;
   }
   elite_svc_send_ack(msg_ptr, result);

   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_start_record_cmd_v2 end result=%x, wait_mask=%lx, session(%lx)",result,venc_ptr->wait_mask,venc_ptr->session_num);
   return result;
}

static ADSPResult venc_stop_record_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_custom_voc_svc_connect_type* connect_msg_payload_ptr;
   elite_svc_handle_t *svc_2_connect_ptr;
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_stop_record_cmd session(%lx)",venc_ptr->session_num);

   connect_msg_payload_ptr = (elite_msg_custom_voc_svc_connect_type*) (msg_ptr->pPayload);
   if (VENC_STOP_RECORD_CMD != connect_msg_payload_ptr->sec_opcode)
   {
      result = ADSP_EBADPARAM;
      elite_svc_send_ack(msg_ptr, result);
      return result;
   }

   svc_2_connect_ptr = NULL;

   // This service only allows one downstream
   venc_ptr->record.afe_handle_ptr = svc_2_connect_ptr;
   venc_ptr->record.enable = FALSE;
   venc_ptr->record.mode = 0;
   voice_circbuf_reset(&(venc_ptr->record.circ_struct_encout));

   venc_ptr->record.afe_drift_ptr = NULL;

   venc_ptr->wait_mask &= ~(VENC_REC_BUF_MASK ) ; // Stop listening to Rec Output buffers and Ticks

   elite_svc_send_ack(msg_ptr, result);

   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_stop_record_cmd end result=%x, wait_mask=%lx, session(%lx)",result,venc_ptr->wait_mask,venc_ptr->session_num);
   return result;
}


static ADSPResult venc_data_handler(void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t *venc_ptr = (venc_t*)instance_ptr;       // instance structure
   elite_msg_any_t in_buf_msg;

   // ***************** Pop Input buffer
   memset(&in_buf_msg, 0, sizeof(elite_msg_any_t));

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_data_handler begin  session(%x)",(int)venc_ptr->session_num); //TODO: not needed

   // ***************** Read the input data
   result = qurt_elite_queue_pop_front(venc_ptr->svc_handle.dataQ, (uint64_t*)&in_buf_msg);
   voice_result_check(result,(venc_ptr->session_num));

   // ***************** Check Media Type
   if (NULL != in_buf_msg.pPayload)
   {
      if (ELITE_DATA_MEDIA_TYPE == in_buf_msg.unOpCode)// If input data  payload is valid & if data is a mediatype
      {
         // Check Media Type
         elite_msg_data_media_type_apr_t *media_type_payload_ptr =(elite_msg_data_media_type_apr_t *)in_buf_msg.pPayload;
         if( ELITEMSG_DATA_MEDIA_TYPE_APR == media_type_payload_ptr->unMediaTypeFormat &&
               ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM   == media_type_payload_ptr->unMediaFormatID)
         {

            elite_multi_channel_pcm_fmt_blk_t *media_format_blk_ptr =
               (elite_multi_channel_pcm_fmt_blk_t*)elite_msg_get_media_fmt_blk(media_type_payload_ptr);

            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Received MEDIA TYPE sampRate(%ld)  session(%x)", \
                  media_format_blk_ptr->sample_rate,(int)venc_ptr->session_num);
            venc_ptr->samp_rate = media_format_blk_ptr->sample_rate;
            venc_ptr->samp_rate_factor = ((venc_ptr->samp_rate)/VOICE_NB_SAMPLING_RATE);
            venc_ptr->io.frame_samples = VOICE_FRAME_SIZE_NB_20MS*(venc_ptr->samp_rate_factor);//((8000 == venc_ptr->samp_rate)?160:320);

            if( media_format_blk_ptr->num_channels      != 1     ||
                  media_format_blk_ptr->is_interleaved != FALSE ||
                  media_format_blk_ptr->bits_per_sample != 16    ||
                  media_format_blk_ptr->is_signed      != TRUE  ||
                  ((media_format_blk_ptr->sample_rate != VOICE_NB_SAMPLING_RATE) &&
                   (media_format_blk_ptr->sample_rate != VOICE_WB_SAMPLING_RATE) &&
                   (media_format_blk_ptr->sample_rate != VOICE_SWB_SAMPLING_RATE) &&
                   (media_format_blk_ptr->sample_rate != VOICE_FB_SAMPLING_RATE))
              )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Received Invalid MT message: (%x)",(int)venc_ptr->session_num);
               venc_ptr->samp_rate = VOICE_NB_SAMPLING_RATE;  // fall back to 8k
               venc_ptr->io.frame_samples = VOICE_FRAME_SIZE_NB_20MS;
            }

         }
      }

      // ***************** Copy data to local buffer
      // If RUN state and if data payload is valid & if data is pcm samples
      if ((venc_ptr->process_data) &&
            (ELITE_DATA_BUFFER == in_buf_msg.unOpCode))
      {
         // Copy data if any to local buffer
         elite_msg_data_buffer_t* in_data_payload_ptr = (elite_msg_data_buffer_t*) in_buf_msg.pPayload;
         venc_ptr->io.in_buf_size = in_data_payload_ptr->nActualSize;
         memsmove(&(venc_ptr->io.in_buf[0]), VOICE_ROUNDTO8((VOICE_FRAME_SIZE_NB_20MS*VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE)<<1), &(in_data_payload_ptr->nDataBuf),in_data_payload_ptr->nActualSize);
         //dbg: MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: Copy Data in_buf_size(%d) to local buf  session(%x)",(int)venc_ptr->io.in_buf_size,(int)venc_ptr->session_num);
      }
      //else Dont read any data and ignore all the input buffers

      // ***************** Return buffers
      result = elite_msg_return_payload_buffer ( &in_buf_msg );
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed return buffer session(%x)",(int)venc_ptr->session_num);
      }
      in_buf_msg.pPayload = NULL;
   }

   // ***************** Check sanity and call venc_process
   // check if RUN state
   if (venc_ptr->process_data)
   {
      if((venc_ptr->samp_rate == voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate)) &&
            (((uint32_t)((venc_ptr->samp_rate_factor)*VOICE_FRAME_SIZE_NB_20MS)<<1) == venc_ptr->io.in_buf_size))
      {

         uint64_t time = qurt_elite_timer_get_time();
         uint64_t cycles = qurt_elite_profile_get_pcycles();

         // Count the delay
         if ((venc_ptr->ctm.resync_delay_cnt) || venc_ptr->tty.state_ptr->m_sync_recover_tx)
         {
            venc_ptr->ctm.resync_delay_cnt++;
         }
         if (venc_ptr->ctm.resync_delay_cnt >= MAX_RESYNC_DELAY)
         {
            venc_ptr->ctm.resync_modem_afe = TRUE;
            venc_ptr->ctm.resync_delay_cnt = 0;
         }

         venc_process(venc_ptr);

         // Latch the rate when pkt is delivered only for CS calls to take care of CMI/CMC issue
         // Idea is to apply rate change immediately for PS calls
         if (VFR_NONE != venc_ptr->vfr_mode)
         {
            venc_ptr->voc.amr_nb_mode = venc_ptr->voc.prev_amr_nb_mode;
            venc_ptr->voc.amr_wb_mode = venc_ptr->voc.prev_amr_wb_mode;
         }

         if(venc_ptr->pkt_ready && (0 != venc_ptr->io.out_buf_size))
         {
            venc_ptr->pkt_ready = FALSE;
            result = venc_deliver_pkt(venc_ptr, &(venc_ptr->io.out_buf[0]), venc_ptr->io.out_buf_size);//call deliver packet
            if(ADSP_FAILED(result))
            {
               venc_ptr->pkt_miss_ctr++;
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to send Venc pkt req to VDS, result=%d session(%x)",
                     (int)result,(int)venc_ptr->session_num);
            } // pkt_ctr already counts pkt delivered
            if (gVoiceEncDecLB != 0)
            {
               //move to global shared buffer for encoder to decoder loopback
               memsmove(gaVoiceEncDecLBBuf, sizeof(gaVoiceEncDecLBBuf), &(venc_ptr->io.out_buf[0]),venc_ptr->io.out_buf_size);
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: LB mode on - move enc pkt to global buf, bytes = %d session(%x)", (int)venc_ptr->io.out_buf_size,(int)venc_ptr->session_num);
            }
         }
         time = qurt_elite_timer_get_time() - time;
         cycles = qurt_elite_profile_get_pcycles() - cycles;

         voice_cmn_time_profile_add_data( &venc_ptr->time_profile_struct, (uint32_t) time, cycles);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Invalid data buffer  session(%x)",(int)venc_ptr->session_num);
      }
   }

   return result;
}

static void venc_do_sample_slip_struff(venc_t* venc_ptr, int16_t *input_ptr, int16_t *output_ptr, int16_t slip_stuff_samples)
{
   uint16_t frame_size_10msec = venc_ptr->io.in_buf_size >> 2;   //10 msec data.
   uint16_t offset_10msec = 0, output_samples;
   uint16_t input_samples = frame_size_10msec;   //input samples = 10 msec frame samples

   uint16_t num_10msec_frames = 2;

   // Process 10 msec frames. adjust samples in second 10 msec frames
   // when num_10msec_frame=1, adjust drift in same frame i.e for nb do 79->79(stuff) or 81->81(slip) correction
   // when num_10msec_frame=2, adjust drift in second 10 msec frame i.e for nb do 80->80 and 79->79(stuff) or 81->81(slip) in second 10 msec frame.
   for(uint16_t i=0; i < num_10msec_frames ; i++)
   {
      output_samples = input_samples - (i*slip_stuff_samples);
      offset_10msec = frame_size_10msec*i;

      //dbg: MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Voice SS venc: voice_sample_slip_process-in_frame_size=%d,out_frame_size=%d,i=%d",input_samples,output_samples,i);

      (void)voice_sample_slip_process(&(venc_ptr->record.ss_struct),&output_ptr[offset_10msec],&input_ptr[offset_10msec],input_samples,output_samples);

      if((int8_t)venc_ptr->record.ss_multiframe_counter > 0)
      {
         venc_ptr->record.ss_multiframe_counter -= 1;
      }
   }
}

static ADSPResult venc_process(venc_t* venc_ptr)
{
   ADSPResult result;
   int16_t *in_ptr = &(venc_ptr->io.in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = &(venc_ptr->io.out_buf[0]); // pointer to local output buffer
   int16_t tty_enc_flag = 0;
   uint32_t i;
   uint32_t out_buf_size = 0;
   CrossFadeCfgType *mute_cfg_ptr = &(venc_ptr->proc.cross_fade_cfg_struct);
   CrossFadeDataType *mute_data_ptr = &(venc_ptr->proc.cross_fade_data_struct);

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_process begin I(%d) session(%x)",(int)venc_ptr->io.in_buf_size,(int)venc_ptr->session_num);

#if defined(__qdsp6__) && !defined(SIM)
   int16_t enc_in_temp[VOICE_FRAME_SIZE_FB];
   memscpy( enc_in_temp, sizeof(enc_in_temp), in_ptr, venc_ptr->io.in_buf_size);
#endif

   // Apply Tx stream Limiter
   venc_limiter_process_wrapper(venc_ptr,in_ptr);

   // Ensure data is within 14-bit dynamic range
   voice_saturate_to_14_bits(in_ptr, venc_ptr->io.in_buf_size>>1);

   // Convert speech samples from 14-bit to 16-bit
   for (i = 0; i < venc_ptr->io.in_buf_size>>1; i++)
   {
      in_ptr[i] = in_ptr[i] << 2;
   }

   venc_ptr->pkt_ready = TRUE;

   /* for any TTY call, print debug of flag used for muting */
   if( (venc_ptr->tty.state_ptr->m_etty_mode != VSM_TTY_MODE_OFF)||(venc_ptr->tty.state_ptr->m_oobtty_mode != VSM_TTY_MODE_OFF))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_process RxTTYDet(%d)",(int)(venc_ptr->tty.state_ptr->m_rx_tty_detected ||venc_ptr->tty.state_ptr->m_rx_oobtty_detected));
   }

   /* update comfort noise with the updated state of Rx TTY from common TTY structure */
   venc_ptr->proc.comfort_noise_ptr->SetRxTTYDetected(venc_ptr->tty.state_ptr->m_rx_tty_detected);

   // Comfort Noise wrapper to handle generation of zeroes or CNG based on Mute/CNGLVB settings
   // Create a copy for Crossfading
   memscpy(venc_ptr->proc.mute_inp_ptr, VOICE_FRAME_SIZE_FB << 1, in_ptr, venc_ptr->io.in_buf_size);
   result = venc_ptr->proc.comfort_noise_ptr->Process(in_ptr,in_ptr,venc_ptr->io.in_buf_size, VOICE_ROUNDTO8((VOICE_FRAME_SIZE_NB_20MS*VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE)<<1));
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ComfortNoise Processing Error  session(%x)",(int)venc_ptr->session_num);
   }
   //handling soft mute
   if ( VOICE_MUTE == venc_ptr->mute )
   {
      // If in Mute state, send setting to Comfort Noise wrapper, so that
      // zeroes or CNG can be generated from next frame onwards.
      venc_ptr->proc.comfort_noise_ptr->SetMute(VOICE_MUTE);
      cross_fade_process (mute_cfg_ptr,mute_data_ptr, venc_ptr->proc.mute_inp_ptr,in_ptr, in_ptr,(venc_ptr->io.in_buf_size)/2 );
   }
   else  // Unmute
   {
      // If in Unmute state, send setting to Comfort Noise wrapper after reaching steady state (of crossfade algorithm)
      // so that zeroes or CNG can be mixed until completely unmuted.
      if( STEADY_STATE == mute_data_ptr->state )
      {
         venc_ptr->proc.comfort_noise_ptr->SetMute(VOICE_UNMUTE);
      }
      cross_fade_process (mute_cfg_ptr,mute_data_ptr, in_ptr, venc_ptr->proc.mute_inp_ptr, in_ptr,(venc_ptr->io.in_buf_size)/2 );
   }

   // half duplex muting forced after crossfading/CNG
   if( (TRUE == venc_ptr->tty.state_ptr->m_rx_tty_detected &&
            VSM_TTY_MODE_FULL == venc_ptr->tty.state_ptr->m_etty_mode)||
         ( TRUE == venc_ptr->tty.state_ptr->m_rx_oobtty_detected &&
           VSM_TTY_MODE_FULL == venc_ptr->tty.state_ptr->m_oobtty_mode))
   {
      memset( in_ptr, 0, venc_ptr->io.in_buf_size);
   }

   /* DTMF Gen*/
   uint16_t dtmf_gen_ended_flag;
   dtmf_gen_ended_flag = ((DTMF_RUN == venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state)?TRUE:FALSE);
   result = voice_dtmf_gen_process(venc_ptr->proc.dtmf_gen_ptr,in_ptr,in_ptr,venc_ptr->io.in_buf_size);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: DTMF Gen Processing Error  session(%x)",(int)venc_ptr->session_num);
   }
   else if((DTMF_STOP == venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state) && dtmf_gen_ended_flag)
   {
      result = (ADSPResult) venc_send_dtmf_gen_ended(venc_ptr);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  VSM_EVT_DTMF_GENERATION_ENDED failed session(%lx)", \
               venc_ptr->session_num);
      }
   }

   /* CTM */
   result = venc_process_ctm_tx(venc_ptr,in_ptr,in_ptr,(venc_ptr->io.in_buf_size >> 1),VOICE_ROUNDTO8((VOICE_FRAME_SIZE_NB_20MS*VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE)<<1));

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: CtmTx Processing Error  session(%x)",(int)venc_ptr->session_num);
   }

   voice_process_oobtty_tx(&(venc_ptr->oobtty_tx_struct),in_ptr,in_ptr,(venc_ptr->io.in_buf_size >> 1),(int)venc_ptr->tty.state_ptr->m_rx_oobtty_detected);

   if(TRUE == voice_oobtty_tx_char_detected(&(venc_ptr->oobtty_tx_struct)))
   {
      result = venc_send_oobtty_tx_char_detected(venc_ptr);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  VSM_EVT_OOBTTY_TX_CHAR_DETECTED failed session(%lx)", \
               venc_ptr->session_num);
      }
   }
   /* Host PCM processing */
   /* This point is chosen so that e-call data will be captured in the log and in-call rec */
   /* e-call data from client -> qxdm log -> TTY detection -> in-call rec -> encoder       */
   voice_ecall_tx (venc_ptr, in_ptr);


#if defined(__qdsp6__) && !defined(SIM)
   // log saved input buf and preproc output prior to encode
   int8_t *bufptr[4] = { (int8_t *) enc_in_temp, (int8_t *) in_ptr, NULL, NULL };
   uint32_t sampling_rate_log_id;
   sampling_rate_log_id = voice_get_sampling_rate_log_id(voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate));
   voice_log_buffer( bufptr,
         VOICE_LOG_TAP_POINT_VENC_IN,
         (sampling_rate_log_id << 3) | venc_ptr->session_num,
         qurt_elite_timer_get_time(),
         VOICE_LOG_DATA_FORMAT_PCM_STEREO_NON_INTLV,
         voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate),
         venc_ptr->io.in_buf_size,
         NULL);
#endif

   /* 1x TTY processing */
   if(TRUE == venc_ptr->tty.option )
   {
      // buffer to hold downsampled data in WB case
      int16_t  tty_ds_buf[160];

      if(( voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate)== 16000 )&&( NULL != venc_ptr->tty.onex_downby2_mem_ptr))
      {
         int32_t resample_result;
         resample_result = voice_resampler_process(
               &(venc_ptr->tty.resamp_config),
               venc_ptr->tty.onex_downby2_mem_ptr,
               (int8 *)in_ptr,
               VOICE_FRAME_SIZE_WB,
               (int8 *)tty_ds_buf,
               VOICE_FRAME_SIZE_NB
               );
         if(VOICE_RESAMPLE_SUCCESS != resample_result)
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: 1x TTY down sampler process failed");
         }
         tty_enc_flag = tty_enc(tty_ds_buf, /*160,*/ &venc_ptr->tty.enc_struct);
      }
      else
      {
         tty_enc_flag = tty_enc(in_ptr, /*160,*/ &venc_ptr->tty.enc_struct);
      }
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: 1x TX TTY Process, ttyEncFlag(%d)  session(%x)", \
            tty_enc_flag,(int)venc_ptr->session_num);
   }
   if (TRUE == venc_ptr->record.enable )
   {
      int16_t slip_stuff_samples = 0;
      int16_t local_ss_buf[966];            // local input buffer, todo: remove hardcoding

      // calculate av/hp timer drift
      venc_cal_drift(venc_ptr);
      // Calculate how many samples to slip/stuff in this frame
      venc_cal_sample_slip_stuff(venc_ptr, &(slip_stuff_samples));
      // apply sample slipping on the encoder input to match rate with record afe port
      venc_do_sample_slip_struff(venc_ptr,in_ptr,&local_ss_buf[0],slip_stuff_samples);
      // store in the circular buffer to be sent out
      voice_circbuf_write(&venc_ptr->record.circ_struct_encout,(int8_t *)&local_ss_buf[0],(venc_ptr->io.in_buf_size/2 - slip_stuff_samples));
   }

   // vocoder encoder processing
   switch (venc_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            evrc_set_enc_params((void*)venc_ptr->enc_state_ptr,venc_ptr->voc.max_rate,venc_ptr->voc.min_rate,
                  venc_ptr->voc.rate_mod,0 /*sending 0 doesn't force to eigth rate*/,0,0);
            evrc_encoder((void*)venc_ptr->enc_state_ptr,in_ptr,out_ptr,tty_enc_flag,&venc_ptr->tty.enc_struct);
            out_buf_size = (int16_t) evrc_repacking_dsp_to_mvs(reinterpret_cast<char_t*> (out_ptr));
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
         {
            int16_t amr_nb_pkt_out[18];
            (void) eamr_encoder((void*)venc_ptr->enc_state_ptr,
                  in_ptr,
                  amr_nb_pkt_out,
                  venc_ptr->voc.amr_nb_mode /*using amrnb mode as modes are same for amr and eamr */,
                  venc_ptr->voc.dtx_mode,
                  0 /*disable Water Marking for AMR-NB */);  // AMR-NB encoder call with WM 0


            out_buf_size = eamr_modem_if1_framing_main((uint8_t *)amr_nb_pkt_out,(uint8_t *)out_ptr);

            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
         {
            int16_t amr_nb_pkt_out[18];
            (void) eamr_encoder((void*)venc_ptr->enc_state_ptr,
                  in_ptr,
                  amr_nb_pkt_out,
                  venc_ptr->voc.amr_nb_mode /*using amrnb mode as modes are same for amr and eamr */,
                  venc_ptr->voc.dtx_mode,
                  0 /*disable Water Marking for AMR-NB */);  // AMR-NB encoder call with WM 0

            out_buf_size = eamr_if2_framing_main((uint8_t *)amr_nb_pkt_out,(uint8_t *)out_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            int16_t eamr_pkt_out[18];
            (void) eamr_encoder((void*)venc_ptr->enc_state_ptr,
                  in_ptr,
                  eamr_pkt_out,
                  venc_ptr->voc.amr_nb_mode /*using amrnb mode as modes are same for amr and eamr */,
                  venc_ptr->voc.dtx_mode,
                  1 /*enable Water Marking*/);
            out_buf_size = eamr_modem_if1_framing_main((uint8_t *)eamr_pkt_out,(uint8_t *)out_ptr);
            break;
         }

      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            int16_t amr_wb_pkt_out[32];
            amrwb_enc_api(in_ptr,amr_wb_pkt_out,(void*)venc_ptr->enc_state_ptr,venc_ptr->voc.dtx_mode,
                  static_cast<AmrWBModeRate> (venc_ptr->voc.amr_wb_mode));
            out_buf_size = amrwb_modem_if1_framing_main((uint8_t *)amr_wb_pkt_out,(uint8_t *)out_ptr,1);
            break;
         }
      case VSM_MEDIA_TYPE_EFR_MODEM:
         {
            // todo: remove unnecessary vars from api
            int16_t vad,sp;
            efr_encoder((void*)venc_ptr->enc_state_ptr,in_ptr,out_ptr,&vad,&sp,static_cast<int16_t> (venc_ptr->voc.dtx_mode));
            gsm_efr_repacking_dsp_to_mvs(reinterpret_cast<int8_t*> (out_ptr));
            out_buf_size = 32; // fixed as per std
            break;
         }
      case VSM_MEDIA_TYPE_FR_MODEM:
         {
            int16_t fr_pkt_out[18];
            gsm_fr_encoder((void*)venc_ptr->enc_state_ptr,in_ptr,fr_pkt_out,static_cast<int16_t> (venc_ptr->voc.dtx_mode),FR_ENC_FROM_VOICE_PATH);
            gsm_fr_repacking_dsp_to_mvs(fr_pkt_out,reinterpret_cast<int8_t*> (out_ptr));
            out_buf_size = 34; // fixed as per std
            break;
         }
      case VSM_MEDIA_TYPE_HR_MODEM:
         {
            gsmhr_encode((void*)venc_ptr->enc_state_ptr,in_ptr,out_ptr,static_cast<int16_t> (venc_ptr->voc.dtx_mode));
            gsm_hr_repacking_dsp_to_mvs(reinterpret_cast<int8_t*> (out_ptr));
            out_buf_size = 15; // fixed as per std;
            break;
         }
      case VSM_MEDIA_TYPE_PCM_8_KHZ:
         {
            for ( i=0; i<VOICE_FRAME_SIZE_NB; i++ )
            {
               out_ptr[i] = in_ptr[i];
            }
            out_buf_size = VOICE_FRAME_SIZE_NB<<1;
            break;
         }
      case VSM_MEDIA_TYPE_PCM_16_KHZ:
         {
            for ( i=0; i<VOICE_FRAME_SIZE_WB; i++ )
            {
               out_ptr[i] = in_ptr[i];
            }
            out_buf_size = VOICE_FRAME_SIZE_WB<<1;
            break;
         }
      case VSM_MEDIA_TYPE_PCM_32_KHZ:
         {
            for ( i=0; i<VOICE_FRAME_SIZE_SWB; i++ )
            {
               out_ptr[i] = in_ptr[i];
            }
            out_buf_size = VOICE_FRAME_SIZE_SWB<<1;
            break;
         }
      case VSM_MEDIA_TYPE_PCM_48_KHZ:
         {
            for ( i=0; i<VOICE_FRAME_SIZE_FB; i++ ) //Size needs to be changed and made based on sampling rate variable
            {
               out_ptr[i] = in_ptr[i];
            }
            out_buf_size = VOICE_FRAME_SIZE_FB<<1;//Size needs to be changed and made based on sampling rate variable
            break;
         }
      case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
         {
            ADSPResult resampler_result = voice_gen_resamp_process(&(venc_ptr->proc.codec_resampler), (int32 *)in_ptr, (int32 *)out_ptr, (int32)VOICE_FRAME_SIZE_FB, (int32)VOICE_FRAME_SIZE_44_1, (int16)GEN_RESAMP_FIXED_OUTPUT_MODE);
            if(ADSP_EOK != resampler_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ERROR: Generic Resampler failed for encoder session(%x)",(int)venc_ptr->session_num);
               memset((void*)out_ptr,0,VOICE_FRAME_SIZE_44_1<<1); //silence;
               venc_ptr->io.out_buf_size = VOICE_FRAME_SIZE_44_1<<1;
               return ADSP_EFAILED;
            }
            for ( i=0; i<VOICE_FRAME_SIZE_44_1; i++ )
            {
               out_ptr[i] = out_ptr[i];
            }
            out_buf_size = VOICE_FRAME_SIZE_44_1<<1;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            memset(out_ptr, 0, 24); // todo: need this?
            FourGVEncode* four_gv_encode_ptr = (FourGVEncode*)venc_ptr->voc.four_gv_encode_ptr;
            four_gv_encode_ptr->min_rate = venc_ptr->voc.min_rate;
            four_gv_encode_ptr->max_rate = venc_ptr->voc.max_rate;
            four_gv_encode_ptr->avg_rate_target = venc_ptr->voc.fgv_nb_avg_rate;
            four_gv_encode_ptr->avg_rate_control = venc_ptr->voc.fgv_avg_rate_control;

            if ( VSM_MEDIA_TYPE_EVRC_NW_2K == venc_ptr->voc_type )
            {
               four_gv_encode_ptr->avg_rate_target = venc_ptr->voc.fgv_nw_2k_avg_rate;
            }
            /*
            // For NW_MODEM, internal DTX is always ON hence ignore SW DTX setting.
            // But, SW DTX value is used to decide joint_source_modem flag.
            // SW DTX = OFF => j = 1 : default case in CS call which generates
            //                         critical and non-critical frames.
            // SW DTX = ON => j = 0 : this case is supported for consistency
            //                     with 6k/7k behavior.
             */

            if((VSM_MEDIA_TYPE_4GV_NW_MODEM != venc_ptr->voc_type) && (VSM_MEDIA_TYPE_EVRC_NW_2K != venc_ptr->voc_type))
            {
               four_gv_encode_ptr->dtx = venc_ptr->voc.dtx_mode;
            }
            else if(1 == venc_ptr->voc.dtx_mode)
            {
               four_gv_encode_ptr->joint_source_modem_dtx = 0;
            }
            else if(0 == venc_ptr->voc.dtx_mode)
            {
               four_gv_encode_ptr->joint_source_modem_dtx = 1;
            }

            if (VSM_MEDIA_TYPE_4GV_WB_MODEM == venc_ptr->voc_type)
            {
               four_gv_encode_ptr->avg_rate_target = venc_ptr->voc.fgv_wb_avg_rate;
            }
            if ((VSM_MEDIA_TYPE_4GV_NW_MODEM == venc_ptr->voc_type) || (VSM_MEDIA_TYPE_4GV_NW == venc_ptr->voc_type))
            {
               four_gv_encode_ptr->avg_rate_target = venc_ptr->voc.fgv_nw_avg_rate;
            }

            four_gv_encode_ptr->set_encoder_params(); //todo: implement fully with interface to change param
            four_gv_encode_ptr->encoder(in_ptr,out_ptr,((VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE)*VOICE_FRAME_SIZE_NB_20MS)<<1,tty_enc_flag,&(venc_ptr->tty.enc_struct));
            out_buf_size = fourgv_repacking_dsp_to_mvs(reinterpret_cast<char_t*> (out_ptr));
            break;
         }
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            v13k_set_enc_params((void*)venc_ptr->enc_state_ptr,venc_ptr->voc.max_rate,venc_ptr->voc.min_rate,
                  venc_ptr->voc.rate_mod,0 /*sending 0 doesnt force to eigth rate*/,
                  0,venc_ptr->voc.redu_rate,(int16_t) 0);
            v13k_lvb_enc((void*)venc_ptr->enc_state_ptr,in_ptr,out_ptr,tty_enc_flag,&(venc_ptr->tty.enc_struct));
            out_buf_size = v13k_repacking_dsp_to_mvs(reinterpret_cast<int8_t *> (out_ptr));
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW:
         {
            venc_g711_process(venc_ptr, in_ptr, out_ptr, 1 /*A-LAW*/ );
            out_buf_size = 322;
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW_V2:
         {
            venc_g711_process(venc_ptr, in_ptr, out_ptr, 1 /*A-LAW V2*/ );
            out_buf_size = 161;
            break;
         }
      case VSM_MEDIA_TYPE_G711_MLAW:
         {
            venc_g711_process(venc_ptr, in_ptr, out_ptr,  0 /*M-LAW*/ );
            out_buf_size = 322;
            break;
         }
      case VSM_MEDIA_TYPE_G711_MLAW_V2:
         {
            venc_g711_process(venc_ptr, in_ptr, out_ptr,  0 /*M-LAW V2*/ );
            out_buf_size = 161;
            break;
         }
      case VSM_MEDIA_TYPE_G729AB:
         {
            // todo: move to a helper function
            int16_t enc_buf1[G729AB_ENC_PKT_SIZE_IN_WORDS_10MSEC];
            int16_t enc_buf2[G729AB_ENC_PKT_SIZE_IN_WORDS_10MSEC];

            //encode 1st 80 samples (10 ms) into 6 output words
            g729a_encode((void *)venc_ptr->voc.g729a_encoder_ptr, (void *)venc_ptr->voc.g729a_common_ptr, 1,
                  venc_ptr->voc.dtx_mode, in_ptr, enc_buf1);
            // frame 1st frame into fixed frame of 1 header followed by 10 payload bytes
            g729_enc_framing(enc_buf1, (int8_t *)(out_ptr));

            //encode 2nd 80 samples (10 ms) into 5 output words
            g729a_encode((void *)venc_ptr->voc.g729a_encoder_ptr,(void *)venc_ptr->voc.g729a_common_ptr,
                  1, venc_ptr->voc.dtx_mode, (in_ptr + G729_ENC_L_FRAME), enc_buf2);
            // frame 2nd frame into fixed frame of 1 header followed by 10 payload bytes
            g729_enc_framing(enc_buf2, (int8_t *)(out_ptr) + G729AB_ENC_PKT_SIZE_IN_WORDS);

            out_buf_size = G729AB_ENC_PKT_SIZE_IN_WORDS<<1;  //updating output size in bytes

            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VENC: EVS meta data: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                  venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                  venc_ptr->voc.evs_param_struct.enc_bit_rate,
                  venc_ptr->voc.evs_param_struct.enc_bandwidth,
                  venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                  venc_ptr->voc.evs_param_struct.fec_offset,
                  venc_ptr->voc.evs_param_struct.fer_rate,
                  venc_ptr->voc.evs_param_struct.dtx_enable);

            evs_reinit_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct));  // call this every frame to update rate and BW parameters
            evs_enc_process((void*)venc_ptr->enc_state_ptr,in_ptr,out_ptr);
            out_buf_size = 1 + evs_find_pkt_size((uint32_t )(((uint8_t )out_ptr[0])& 0x1F));
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VENC: EVS pkt size of the frame (%d) ",
                  (int)out_buf_size);
            break;
         }
      default:
         {
            out_buf_size = 0;
            venc_ptr->pkt_ready = FALSE;
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc not supported  session(%x)",(int)venc_ptr->session_num);
         }
   }
   venc_ptr->io.out_buf_size = out_buf_size;

   /* log the MVS payload if not a PCM/G711 media type */
   if( venc_ptr->voc_type != VSM_MEDIA_TYPE_PCM_8_KHZ &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_PCM_16_KHZ &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_PCM_32_KHZ &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_PCM_44_1_KHZ &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_PCM_48_KHZ &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_G711_ALAW &&
         venc_ptr->voc_type != VSM_MEDIA_TYPE_G711_MLAW)
   {
      venc_log_vocoder_packet( venc_ptr->voc_type, ((voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate)== 16000) << 3) | venc_ptr->session_num, out_ptr, out_buf_size, (voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate)));
   }

   if (TRUE == venc_ptr->record.enable )
   {
      venc_ptr->wait_mask |= VENC_REC_BUF_MASK;
   }
   //dbg: MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_process end EncType(%x) I(%d) O(%d) session(%x)",(int)venc_ptr->voc_type,venc_ptr->io.in_buf_size,venc_ptr->io.out_buf_size,(int)venc_ptr->session_num);
   return ADSP_EOK;
}



static ADSPResult venc_g711_process(venc_t* venc_ptr, int16_t *in_ptr,int16_t *out_ptr,int16_t mode )
{

   int16_t   serial[162];
   uint8_t  framed_buf[161];
   uint8_t  *coded_buf_ptr;

   memset(framed_buf,0,sizeof(framed_buf));

   //nothing but a counter needed by g711 lib
   if (venc_ptr->voc.g711_frame == 32767) {
      venc_ptr->voc.g711_frame = 256;
   }
   else {
      venc_ptr->voc.g711_frame++;
   }

   memset( serial, 0, sizeof( serial));
   G711A2Encoder((void *)venc_ptr->enc_state_ptr, in_ptr, serial, (int16_t)venc_ptr->voc.dtx_mode, mode, &venc_ptr->voc.g711_frame, sizeof(serial));
   /* Use FIXED frame sizes of 161 bytes, regardless of actual packet length.  Pre fill with all zeros as padding */
   for( int16_t i = 0; i < G711_L_TOTALFRAME/venc_ptr->voc.g711_frame_size; i++)
   {
      coded_buf_ptr = (uint8_t *) &serial[i*(G711_L_FRAME_10MS+1)];
      /* will write proper header and use fixed payload size of 160 bytes (representing max payload size for PCM NB */
      G711Framing( coded_buf_ptr, framed_buf, VOICE_FRAME_SIZE_NB, sizeof(serial));
      memsmove( reinterpret_cast<int8_t *>(out_ptr)+(i*(G711_L_TOTALFRAME+1)), VOICE_ROUNDTO8((VOICE_FRAME_SIZE_NB_20MS*VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE)<<1)-(i*(G711_L_TOTALFRAME+1)), framed_buf, 161);

   }

   return ADSP_EOK;
}

static ADSPResult venc_apr_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = APR_EOK;
   venc_t* venc_ptr = (venc_t*) instance_ptr;
   elite_apr_packet_t * apr_packet_ptr = (elite_apr_packet_t*) msg_ptr->pPayload;
   int32_t rc;

   MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_apr_cmd begin, opCode(%lx) token(%lx) session(%x)",
         elite_apr_if_get_opcode(apr_packet_ptr),elite_apr_if_get_client_token(apr_packet_ptr),(int)venc_ptr->session_num);

   /* parse out the received pakcet.  Note in this current framework we are not prioritizing commands
      that can be completed immediately.  We are simply processing command in order they are received */
   switch(elite_apr_if_get_opcode(apr_packet_ptr))
   {

      case VSM_EVT_OOB_ENC_BUF_CONSUMED:
         {
            if ( venc_ptr->oob.enc_pkt_consumed == TRUE)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP:  UNEXPECTED EVENT as VENC did not deliver the previous pkt  for session (%d)",
                     (int)venc_ptr->session_num);
            }
            // set flag to indicate OOB pkt is consumed so as to send next pkt
            venc_ptr->oob.enc_pkt_consumed= TRUE;
            if(venc_ptr->pkt_delivery_pending == TRUE)
            { //Since Packet was ready before VSM_EVT_OOB_ENC_BUF_CONSUMED event, queue the packet to VDS now.
               //venc_ptr->pkt_delivery_pending is set to FALSE inside venc_deliver_pkt
               result = venc_deliver_pkt(venc_ptr, &(venc_ptr->io.out_buf[0]), venc_ptr->io.out_buf_size);
               if(ADSP_FAILED(result))
               {
                  venc_ptr->pkt_miss_ctr++;
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to send Venc pkt to VDS, result=%d session(%x)",
                        (int)result,(int)venc_ptr->session_num);
               } // pkt_ctr already counts pkt delivered
            }
            venc_ptr->oob.pkt_consumed_ctr++;
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VENC: VSM_EVT_OOB_ENC_BUF_CONSUMED number (%d) for session (%d)",
                  (int)venc_ptr->oob.pkt_consumed_ctr,(int)venc_ptr->session_num);
            // free the event since not done internally
            rc = elite_apr_if_free(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr);
            break;
         }


      case VSM_CMD_ENC_SET_MINMAX_RATE:
         {
            vsm_enc_set_minmax_rate_t *min_max_cmd_ptr = (vsm_enc_set_minmax_rate_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);
            MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_ENC_SET_MINMAX_RATE, mediaType(%lx), \
                  min_rate(%lx), max_rate(%lx), process_data(%x) session(%x)",min_max_cmd_ptr->media_type, \
                  min_max_cmd_ptr->min_rate, min_max_cmd_ptr->max_rate, venc_ptr->process_data,(int)venc_ptr->session_num);

            //check min_rate/max_rate within valid range or not (1 to 4 )
            if (((min_max_cmd_ptr->min_rate <= FULL_RATE) && (min_max_cmd_ptr->min_rate >= SILENCE_RATE )) &&
                  ((min_max_cmd_ptr->max_rate <= FULL_RATE) && (min_max_cmd_ptr->max_rate >= SILENCE_RATE )))
            {
               //EVRC doesnt support Quarter rate encoding.
               if ( VSM_MEDIA_TYPE_EVRC_MODEM == venc_ptr->voc_type )
               {
                  if (( min_max_cmd_ptr->min_rate == QUARTER_RATE ) || ( min_max_cmd_ptr->max_rate == QUARTER_RATE ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid Min or Max rates  session(%x)",(int)venc_ptr->session_num);
                  }
                  else
                  {
                     venc_ptr->voc.min_rate = min_max_cmd_ptr->min_rate;
                     venc_ptr->voc.max_rate = min_max_cmd_ptr->max_rate;
                  }
               }
               else
               {
                  venc_ptr->voc.min_rate = min_max_cmd_ptr->min_rate;
                  venc_ptr->voc.max_rate = min_max_cmd_ptr->max_rate;
               }
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid Min or Max rates  session(%x)",(int)venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }

            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, result );
            break;
         }
      case VSM_CMD_ENC_SET_RATE_MOD:
         {
            vsm_enc_set_rate_mod_t *enc_rate_mod_cmd_ptr = (vsm_enc_set_rate_mod_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);
            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_ENC_SET_RATE_MOD, enc_rate_mod(%lx), \
                  process_data(%x) session(%x)",enc_rate_mod_cmd_ptr->rate_modulation_param, \
                  venc_ptr->process_data,(int)venc_ptr->session_num);
            venc_ptr->voc.rate_mod = enc_rate_mod_cmd_ptr->rate_modulation_param;
            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, result );
            break;
         }
      case VSM_CMD_ENC_SET_DTX_MODE:
         {
            vsm_enc_set_dtx_mode_t *dtx_mode_cmd_ptr = (vsm_enc_set_dtx_mode_t*) elite_apr_if_get_payload_ptr( apr_packet_ptr );
            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_ENC_SET_DTX_MODE, dtx_mode(%lx), \
                  process_data(%x) session(%x)", dtx_mode_cmd_ptr->dtx_mode, \
                  venc_ptr->process_data,(int)venc_ptr->session_num);
            if(dtx_mode_cmd_ptr->dtx_mode > 1)
            {
               MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc received invalid DTX mode %ld, process_data(%x) session(%lx)",
                     dtx_mode_cmd_ptr->dtx_mode, venc_ptr->process_data, venc_ptr->session_num);
               rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM );
               break;
            }
            switch(venc_ptr->voc_type)
            {
               // Print a message if you receive on unsupported vocoder, but cache anyway
               case VSM_MEDIA_TYPE_AMR_NB_MODEM:
               case VSM_MEDIA_TYPE_AMR_NB_IF2:
               case VSM_MEDIA_TYPE_EAMR:
               case VSM_MEDIA_TYPE_AMR_WB_MODEM:
               case VSM_MEDIA_TYPE_EFR_MODEM:
               case VSM_MEDIA_TYPE_FR_MODEM:
               case VSM_MEDIA_TYPE_HR_MODEM:
               case VSM_MEDIA_TYPE_G711_ALAW:
               case VSM_MEDIA_TYPE_G711_MLAW:
               case VSM_MEDIA_TYPE_G711_ALAW_V2:
               case VSM_MEDIA_TYPE_G711_MLAW_V2:
               case VSM_MEDIA_TYPE_G729AB:
               case VSM_MEDIA_TYPE_4GV_NW_MODEM:
               case VSM_MEDIA_TYPE_4GV_NW:
               case VSM_MEDIA_TYPE_EVRC_NW_2K:
                  {
                     break;
                  }
               case VSM_MEDIA_TYPE_EVS:
                  {
                     if (venc_ptr->voc.evs_param_struct.dtx_enable != dtx_mode_cmd_ptr->dtx_mode)
                     {
                        Word16 dtx_enable = venc_ptr->voc.evs_param_struct.dtx_enable;

                        venc_ptr->voc.evs_param_struct.dtx_enable = dtx_mode_cmd_ptr->dtx_mode;

                        // Reinitialize vocoder if in the middle of processing.
                        if(venc_ptr->process_data)
                        {
                           if (evs_reinit_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct)))
                           {
                              MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Invalid parameters set on EVS: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                                    venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                                    venc_ptr->voc.evs_param_struct.enc_bit_rate,
                                    venc_ptr->voc.evs_param_struct.enc_bandwidth,
                                    venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                                    venc_ptr->voc.evs_param_struct.fec_offset,
                                    venc_ptr->voc.evs_param_struct.fer_rate,
                                    venc_ptr->voc.evs_param_struct.dtx_enable);

                              // Restore previous state.
                              venc_ptr->voc.evs_param_struct.dtx_enable = dtx_enable;

                              // Report error.
                              (void)elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
                              return ADSP_EBADPARAM;
                           }
                        }
                     }
                     break;
                  }
               default:
                  {
                     MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Caching DTX mode on vocoder %#lx, process_data(%x) session(%lx)",
                           venc_ptr->voc_type, venc_ptr->process_data, venc_ptr->session_num);
                     break;
                  }

            }
            venc_ptr->voc.dtx_mode = dtx_mode_cmd_ptr->dtx_mode;
            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EOK );
            break;
         }
      case VSM_CMD_SET_DTMF_GENERATION:
         {
            /* check if DTMF generation is set in correct direction */
            vsm_set_dtmf_generation_t *dtmf_gen_ptr = (vsm_set_dtmf_generation_t *)elite_apr_if_get_payload_ptr( apr_packet_ptr );
            MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_DTMF_GENERATION, direction(%x), mixing(%x), \
                  tone1(%x) tone2(%x) gain(%x) duration(%x)msecs :session(%lx)",dtmf_gen_ptr->direction, dtmf_gen_ptr->mixing, \
                  dtmf_gen_ptr->tone1,dtmf_gen_ptr->tone2,dtmf_gen_ptr->gain,dtmf_gen_ptr->duration,venc_ptr->session_num);

            if( VSM_SET_DTMF_GEN_TX == dtmf_gen_ptr->direction)
            {
               /* save self/client info for DTMF Gen ended event.  Client is source address from incoming packet */
               venc_ptr->proc.apr_info_dtmf_gen.self_addr = elite_apr_if_get_dst_addr( apr_packet_ptr);
               venc_ptr->proc.apr_info_dtmf_gen.self_port = elite_apr_if_get_dst_port( apr_packet_ptr);
               venc_ptr->proc.apr_info_dtmf_gen.client_addr = elite_apr_if_get_src_addr( apr_packet_ptr);
               venc_ptr->proc.apr_info_dtmf_gen.client_port = elite_apr_if_get_src_port( apr_packet_ptr);
               MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_DTMF_GENERATION, saving clientAddr(%x) clientPort(%x) :session(%lx)",
                     venc_ptr->proc.apr_info_dtmf_gen.client_addr, venc_ptr->proc.apr_info_dtmf_gen.client_port,venc_ptr->session_num);

               //Should not process DTMF Stop command if already in DTMF_STOP state.
               if( (DTMF_STOP == venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state) && ( VOICE_DTMF_GEN_STOP == dtmf_gen_ptr->duration))
               {
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_SET_DTMF_GENERATION with duration 0, Already in DTMF_STOP");

                  //Send VSM_EVT_DTMF_GENERATION_ENDED event
                  result = (ADSPResult) venc_send_dtmf_gen_ended(venc_ptr);
                  if (ADSP_FAILED(result))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  VSM_EVT_DTMF_GENERATION_ENDED failed session(%x)", \
                           (unsigned int)venc_ptr->session_num);
                  }
               }
               else
               {
                  result = voice_dtmf_gen_start(venc_ptr->proc.dtmf_gen_ptr,dtmf_gen_ptr->tone1,dtmf_gen_ptr->tone2,
                        dtmf_gen_ptr->duration,dtmf_gen_ptr->mixing,dtmf_gen_ptr->gain);

                  if(ADSP_FAILED(result))
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Venc DTMF Gen start from gen start cmd failed %d: session (%x)",(int) result, (int)venc_ptr->session_num);
                     venc_ptr->proc.dtmf_gen_ptr->current_dtmf_state = DTMF_STOP;
                     result = (ADSPResult) venc_send_dtmf_gen_ended(venc_ptr);
                     if (ADSP_FAILED(result))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  VSM_EVT_DTMF_GENERATION_ENDED failed session(%x)", \
                              (int)venc_ptr->session_num);
                     }
                  }
               }

               rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EOK );
            }
            else
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM_CMD_SET_DTMF_GENERATION: Invalid direction = %d, session(%x)",dtmf_gen_ptr->direction,(int)venc_ptr->session_num);
               rc = elite_apr_if_end_cmd( venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
            }
            break;
         }
      case VOICE_EVT_PUSH_HOST_BUF_V2:
         {
            ADSPResult local_result = APR_EOK;

            // handle the event, message info will get copied and queued.  No policy on write buffers for multichannel topology,
            // can mark all as valid
            if ((VOICE_NB_SAMPLING_RATE == venc_ptr->samp_rate) || (VOICE_WB_SAMPLING_RATE == venc_ptr->samp_rate) ||
                                                                     (VOICE_SWB_SAMPLING_RATE == venc_ptr->samp_rate) ||(VOICE_FB_SAMPLING_RATE == venc_ptr->samp_rate))
            {
               local_result = voice_host_pcm_buffer_rd_wr( &venc_ptr->proc.host_pcm_context, msg_ptr, BUF_VALID);
            }
            else
            {
               local_result = ADSP_EUNSUPPORTED;
            }

            if (ADSP_FAILED(local_result))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc-error-cannot process voice_evt_push_host_pcm_buf_v2_t, result(%#x) session(%#lx)", \
                     local_result,venc_ptr->session_num);
            }

            // free the event since not done internally
            rc = elite_apr_if_free(venc_ptr->proc.host_pcm_context.apr_handle, apr_packet_ptr);
            break;
         }
      case VSM_CMD_SET_EVS_ENC_OPERATING_MODE:
         {
            vsm_set_evs_enc_operating_mode_t *evs_enc_operating_mode_cmd_ptr = (vsm_set_evs_enc_operating_mode_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);
            if( venc_ptr->voc_type != VSM_MEDIA_TYPE_EVS )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_EVS_ENC_OPERATING_MODE cmd is not valid for other vocoders");
               rc = elite_apr_if_end_cmd( venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM );
            }
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VSM_CMD_SET_EVS_ENC_OPERATING_MODE, enc_rate_mod(%x), bandwidth (%x), process_data(%x) session(%x)", \
                  (int)evs_enc_operating_mode_cmd_ptr->mode,
                  (int)evs_enc_operating_mode_cmd_ptr->bandwidth,
                  venc_ptr->process_data,(int)venc_ptr->session_num);

            if(venc_ptr->voc.evs_param_struct.enc_bandwidth != evs_enc_operating_mode_cmd_ptr->bandwidth
                  || venc_ptr->voc.evs_param_struct.enc_bit_rate != evs_find_mode_rate(evs_enc_operating_mode_cmd_ptr->mode))
            {

               Word16 enc_bit_rate = venc_ptr->voc.evs_param_struct.enc_bit_rate;
               Word16 enc_bandwidth = venc_ptr->voc.evs_param_struct.enc_bandwidth;
               venc_ptr->voc.evs_param_struct.enc_bit_rate = evs_find_mode_rate(evs_enc_operating_mode_cmd_ptr->mode);
               venc_ptr->voc.evs_param_struct.enc_bandwidth = (uint32_t )evs_enc_operating_mode_cmd_ptr->bandwidth;

               // Reinitialize vocoder if in the middle of processing.
               if(venc_ptr->process_data)
               {
                  if (evs_reinit_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct)))
                  {
                     MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Invalid parameters set on EVS: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                           venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                           venc_ptr->voc.evs_param_struct.enc_bit_rate,
                           venc_ptr->voc.evs_param_struct.enc_bandwidth,
                           venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                           venc_ptr->voc.evs_param_struct.fec_offset,
                           venc_ptr->voc.evs_param_struct.fer_rate,
                           venc_ptr->voc.evs_param_struct.dtx_enable);

                     // Restore previous mode.
                     venc_ptr->voc.evs_param_struct.enc_bit_rate = enc_bit_rate;
                     venc_ptr->voc.evs_param_struct.enc_bandwidth = enc_bandwidth;

                     // Report error.
                     (void)elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
                     return ADSP_EBADPARAM;
                  }
               }

               // Once confirmed, send mode notification to client.
               if((evs_find_operating_bandwidth(venc_ptr->voc.evs_param_struct.enc_bandwidth)!= venc_ptr->voc.vocoder_op_mode))
               {
                  venc_ptr->voc.vocoder_op_mode = evs_find_operating_bandwidth(venc_ptr->voc.evs_param_struct.enc_bandwidth);
                  venc_send_mode_notification_v2(venc_ptr);
               }
            }
            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, result );
            break;
         }
      case VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE:
         {
            vsm_set_evs_enc_channel_aware_mode_enable_t *evs_enc_hannel_aware_mode_enable_cmd_ptr = (vsm_set_evs_enc_channel_aware_mode_enable_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);
            if( venc_ptr->voc_type != VSM_MEDIA_TYPE_EVS )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE cmd is not valid for other vocoders");
               rc = elite_apr_if_end_cmd( venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM );
            }
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE, fec_offset(%x), fec_rate (%x), process_data(%x) session(%x)", \
                  (int)evs_enc_hannel_aware_mode_enable_cmd_ptr->fec_offset,
                  (int)evs_enc_hannel_aware_mode_enable_cmd_ptr->fer_rate,
                  venc_ptr->process_data,(int)venc_ptr->session_num);

            if( 1 != venc_ptr->voc.evs_param_struct.evs_channel_aware_mode
                  || venc_ptr->voc.evs_param_struct.fec_offset != evs_enc_hannel_aware_mode_enable_cmd_ptr->fec_offset
                  || venc_ptr->voc.evs_param_struct.fer_rate != evs_enc_hannel_aware_mode_enable_cmd_ptr->fer_rate)
            {

               Word16 evs_channel_aware_mode = venc_ptr->voc.evs_param_struct.evs_channel_aware_mode;
               Word16 fec_offset = venc_ptr->voc.evs_param_struct.fec_offset;
               Word16 fer_rate = venc_ptr->voc.evs_param_struct.fer_rate;

               venc_ptr->voc.evs_param_struct.evs_channel_aware_mode = 1;
               venc_ptr->voc.evs_param_struct.fec_offset = (uint32_t )evs_enc_hannel_aware_mode_enable_cmd_ptr->fec_offset;
               venc_ptr->voc.evs_param_struct.fer_rate = (uint32_t )evs_enc_hannel_aware_mode_enable_cmd_ptr->fer_rate;

               // Reinitialize vocoder if in the middle of processing.
               if(venc_ptr->process_data)
               {
                  if (evs_reinit_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct)))
                  {
                     MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Invalid parameters set on EVS: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                           venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                           venc_ptr->voc.evs_param_struct.enc_bit_rate,
                           venc_ptr->voc.evs_param_struct.enc_bandwidth,
                           venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                           venc_ptr->voc.evs_param_struct.fec_offset,
                           venc_ptr->voc.evs_param_struct.fer_rate,
                           venc_ptr->voc.evs_param_struct.dtx_enable);

                     // Restore previous state.
                     venc_ptr->voc.evs_param_struct.evs_channel_aware_mode = evs_channel_aware_mode;
                     venc_ptr->voc.evs_param_struct.fec_offset = fec_offset;
                     venc_ptr->voc.evs_param_struct.fer_rate = fer_rate;

                     // Report error.
                     (void)elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
                     return ADSP_EBADPARAM;
                  }
               }
            }

            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, result );
            break;
         }
      case VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE:
         {
            if( venc_ptr->voc_type != VSM_MEDIA_TYPE_EVS )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE cmd is not valid for other vocoders");
               rc = elite_apr_if_end_cmd( venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM );
            }

            if( 0 != venc_ptr->voc.evs_param_struct.evs_channel_aware_mode)
            {
               Word16 evs_channel_aware_mode = venc_ptr->voc.evs_param_struct.evs_channel_aware_mode;

               venc_ptr->voc.evs_param_struct.evs_channel_aware_mode = 0;

               // Reinitialize vocoder if in the middle of processing.
               if(venc_ptr->process_data)
               {
                  if (evs_reinit_enc((void*)venc_ptr->enc_state_ptr,&(venc_ptr->voc.evs_param_struct)))
                  {
                     MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Invalid parameters set on EVS: (%d), (%d), (%d), (%d), (%d) (%d), (%d) ",
                           venc_ptr->voc.evs_param_struct.enc_sampling_rate,
                           venc_ptr->voc.evs_param_struct.enc_bit_rate,
                           venc_ptr->voc.evs_param_struct.enc_bandwidth,
                           venc_ptr->voc.evs_param_struct.evs_channel_aware_mode,
                           venc_ptr->voc.evs_param_struct.fec_offset,
                           venc_ptr->voc.evs_param_struct.fer_rate,
                           venc_ptr->voc.evs_param_struct.dtx_enable);

                     // Restore previous state.
                     venc_ptr->voc.evs_param_struct.evs_channel_aware_mode = evs_channel_aware_mode;

                     // Report error.
                     (void)elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
                     return ADSP_EBADPARAM;
                  }
               }
            }

            rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, result );
            break;
         }
      default:
         {
            /* Handle error. */
            if (elite_apr_if_msg_type_is_cmd(apr_packet_ptr))
            {  /* Complete unsupported commands. */
               rc = elite_apr_if_end_cmd(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr, APR_EUNSUPPORTED);
            }
            else
            {  /* Drop unsupported events. */
               rc = elite_apr_if_free(venc_ptr->apr_info_ptr->apr_handle, apr_packet_ptr);
            }
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }

   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }
   MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_apr_cmd end, result=%d session(%x)", \
         result,(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_deliver_pkt(venc_t *venc_ptr, void *buffer, uint32_t buf_size)
{
   int32_t rc = 0;
   uint8_t* content;
   elite_apr_packet_t* packet=NULL;
   vsm_send_enc_packet_t* enc_buffer;
   ADSPResult result=ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_deliver_pkt begin session(%x)",(int)venc_ptr->session_num);

   if (IN_BAND == venc_ptr->pkt_exchange_mode)
   {
      rc = elite_apr_if_alloc_event( venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->apr_info_ptr->self_addr,
            venc_ptr->apr_info_ptr->self_port,
            venc_ptr->apr_info_ptr->client_addr,
            venc_ptr->apr_info_ptr->client_port,
            venc_ptr->pkt_ctr++,
            VSM_EVT_SEND_ENC_PACKET,
            ( sizeof( vsm_send_enc_packet_t ) + buf_size ),
            &packet );

      if(rc || (NULL == packet))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet to deliver Venc pkt 0x%8x session(%lx)",(int)rc,
               venc_ptr->session_num);
         return ADSP_EFAILED;
      }

      enc_buffer = (vsm_send_enc_packet_t *) elite_apr_if_get_payload_ptr( packet );
      enc_buffer->media_type = venc_ptr->voc_type;
      content = APR_PTR_END_OF( enc_buffer, sizeof( vsm_send_enc_packet_t ) );
      memscpy( content,buf_size, buffer, buf_size );

      result = voice_cmn_send_vds_apr_request(venc_ptr->vds_client_id, venc_ptr->vds_client_token, &venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->vds_handle_ptr, packet, venc_ptr->session_num);
      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to send APR packet to deliver Venc pkt to VDS result(0x%x) session(%x)",
               (unsigned int)result,(int)venc_ptr->session_num);
         elite_apr_if_free( venc_ptr->apr_info_ptr->apr_handle, packet );
         return result;
      }
   }
   else if (venc_ptr->oob.pkt_exchange_ptr == NULL)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ERROR!OOB packet exchange pointer to shared memory for ENCODER is NULL, session(%x)",
            (int)venc_ptr->session_num);
      rc = ADSP_EFAILED;
   }
   else if (venc_ptr->oob.enc_pkt_consumed == TRUE)
   {
      vsm_oob_pkt_exchange_header_t* enc_buffer_header;

      enc_buffer_header= (vsm_oob_pkt_exchange_header_t *) venc_ptr->oob.pkt_exchange_ptr;
      enc_buffer_header->media_type = venc_ptr->voc_type;
      enc_buffer_header->size = buf_size;
      enc_buffer_header->time_stamp=(uint32_t)qurt_elite_timer_get_time_in_msec();
      content = APR_PTR_END_OF( enc_buffer_header, sizeof( vsm_oob_pkt_exchange_header_t ) );
      memscpy( content, (venc_ptr->oob.memmap.unMemSize)*sizeof(int32_t) - sizeof( vsm_oob_pkt_exchange_header_t ), buffer, buf_size );
      result = elite_mem_flush_cache(&venc_ptr->oob.memmap);        // flushing cache
      if( ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: cache flush failed, result=%x session(%x)",(int)result,(int)venc_ptr->session_num);
      }

      rc = elite_apr_if_alloc_event( venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->apr_info_ptr->self_addr,
            venc_ptr->apr_info_ptr->self_port,
            venc_ptr->apr_info_ptr->client_addr,
            venc_ptr->apr_info_ptr->client_port,
            venc_ptr->pkt_ctr++,
            VSM_EVT_OOB_ENC_BUF_READY,
            0,
            &packet );

      if(rc || (NULL == packet))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet to deliver Venc pkt 0x%8x session(%lx)",(int)rc,
               venc_ptr->session_num);
         return ADSP_EFAILED;
      }

      result = voice_cmn_send_vds_apr_request(venc_ptr->vds_client_id, venc_ptr->vds_client_token, &venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->vds_handle_ptr, packet, venc_ptr->session_num);
      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to queue Venc pkt delivery request to VDS result 0x%x session(%x)",
               (unsigned int)result,(int)venc_ptr->session_num);
         elite_apr_if_free( venc_ptr->apr_info_ptr->apr_handle, packet );
         return result;
      }

      venc_ptr->oob.enc_pkt_consumed = FALSE;
      venc_ptr->pkt_delivery_pending = FALSE;

      venc_ptr->wait_mask &= ~(VENC_VDS_RESPONSE_MASK);  // No need to listen to errors from VDS.
      qurt_elite_signal_clear(venc_ptr->vds_error_signal_ptr); //clear the signal (just for sanity)
   }
   else
   {
      // Start listening to errors from VDS. If VDS reports an error it implies that
      // client took way too long. If that is the case, instead of holding on to the
      // stale packet  be sent, drop it and prepare to send the next current packet.
      // As a side effect, packets will continue to be dropped if client continues to
      // delay its processing (and thereby its CONSUMED event to the DSP).
      venc_ptr->wait_mask |= VENC_VDS_RESPONSE_MASK;

      venc_ptr->pkt_delivery_pending = TRUE;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Previous Encoder Pkt not yet consumed by HOST :session(%x)",(int)venc_ptr->session_num);
   }

   /* If we are in a loopback with valid loopback handle, generate a new APR packet,
    * then queue the apr pointer to the peer (rx decoder) gpQ for servicing.  If operation fails, free the APR packet here.
    */

   if( TRUE == venc_ptr->loopback_enable && NULL != venc_ptr->loopback_handle)
   {

      ADSPResult       result;
      elite_msg_any_t  msg;

      rc = elite_apr_if_alloc_event( venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->apr_info_ptr->self_addr,
            venc_ptr->apr_info_ptr->self_port,
            venc_ptr->apr_info_ptr->client_addr,
            venc_ptr->apr_info_ptr->client_port,
            venc_ptr->pkt_ctr++,
            VSM_EVT_SEND_ENC_PACKET,
            ( sizeof( vsm_send_enc_packet_t ) + buf_size ),
            &packet );

      if ( rc || NULL == packet)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet to deliver Venc pkt 0x%8x session(%lx)",(int)rc,
               venc_ptr->session_num);
         return ADSP_EFAILED;
      }

      enc_buffer = (vsm_send_enc_packet_t *) elite_apr_if_get_payload_ptr( packet );
      enc_buffer->media_type = venc_ptr->voc_type;
      content = APR_PTR_END_OF( enc_buffer, sizeof( vsm_send_enc_packet_t ) );
      memscpy( content, buf_size, buffer, buf_size );


      msg.unOpCode = ELITE_APR_PACKET;
      msg.pPayload = (void *) packet;

      result = qurt_elite_queue_push_back(venc_ptr->loopback_handle->gpQ,(uint64_t*)&msg);

      if( ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_deliver_pkt to Dec gpQ failed, result=%x session(%x)",(int)rc,(int)venc_ptr->session_num);
         rc = elite_apr_if_free( venc_ptr->apr_info_ptr->apr_handle, packet );
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_deliver_pkt to Dec gpQ rc(%x) session(%x)",(int)rc,(int)venc_ptr->session_num);
      }
   }

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: venc_deliver_pkt end, result=%x session(%x)",(int)rc,(int)venc_ptr->session_num);
   return rc;
}

static void voice_result_check(ADSPResult result, uint32_t session_num)
{
   if (ADSP_EOK != result)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc operation error result %d, session(%lx)",(int)result,session_num);
   }
}

static ADSPResult venc_process_ctm_tx(venc_t* venc_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t nSamples, uint16_t out_buff_size)
{
   ADSPResult result = ADSP_EOK;

   // check enable before CTM processing or state variable update
   if( ! venc_ptr->ctm.state.enable)
   {
      return result;
   }

   // TODO: Use Rx<->Tx messaging for peer connect
   // Set state variables
   if ((TRUE == venc_ptr->ctm.resync_modem_afe))
   {
      voice_ctm_tx_resync( &venc_ptr->ctm.state );
      venc_ptr->tty.state_ptr->m_sync_recover_tx = FALSE;
      venc_ptr->ctm.resync_modem_afe = FALSE;
   }

   voice_ctm_tx_set_ctm_from_far_end_detected( &venc_ptr->ctm.state, venc_ptr->tty.state_ptr->m_ctm_from_far_end_detected);

   if( TRUE == venc_ptr->tty.state_ptr->m_enquiry_from_far_end_detected )
   {
      voice_ctm_tx_set_enquiry_from_far_end_detected( &venc_ptr->ctm.state, TRUE );
      venc_ptr->tty.state_ptr->m_enquiry_from_far_end_detected = FALSE;
   }

   result = voice_ctm_tx_process( &venc_ptr->ctm.state, in_ptr, out_ptr, nSamples, out_buff_size);

   uint8_t ctm_char_tx;
   voice_ctm_tx_get_ctm_char_transmitted( &venc_ptr->ctm.state, &ctm_char_tx);

   // set CTM char Tx flag if high
   if( TRUE == ctm_char_tx)
   {
      venc_ptr->tty.state_ptr->m_ctm_character_transmitted = (int32_t) ctm_char_tx;
   }
   return result;

}

static ADSPResult venc_init(venc_t *venc_ptr)
{
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_init begin session(%x)",(int)venc_ptr->session_num);
   // Reset encoder config params
   (void) venc_enc_params_init(venc_ptr);
   // Reset control code params
   (void) venc_ctrl_params_init(venc_ptr);
   // Reset params for a new call
   venc_ptr->vtm_sub_unsub_data.offset = 8000;
   venc_ptr->vtm_sub_unsub_data.signal_enable = 1;
   venc_ptr->vtm_sub_unsub_data.client_id = VOICE_ENC;
   venc_ptr->vtm_sub_unsub_data.vfr_mode = VFR_NONE;
   venc_ptr->vtm_sub_unsub_data.vsid = 0;
   venc_ptr->vtm_sub_unsub_data.timing_ver = VFR_CLIENT_INFO_VER_1;
   venc_ptr->pkt_exchange_mode = IN_BAND;
   venc_ptr->oob.pkt_exchange_ptr = NULL;
   venc_ptr->oob.enc_pkt_consumed = FALSE;
   venc_ptr->pkt_delivery_pending = FALSE;
   venc_ptr->pkt_ctr = 0;
   venc_ptr->pkt_miss_ctr = 0;
   venc_ptr->oob.pkt_consumed_ctr = 0;
   venc_ptr->oob.pkt_miss_ctr = 0;
   venc_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
   venc_ptr->vfr_mode = VFR_NONE;
   venc_ptr->mute    = VOICE_UNMUTE;  // don't cache mute flag for a fresh call
   venc_ptr->loopback_enable = FALSE;  //reset on init/reinit
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_init end session(%x)",(int)venc_ptr->session_num);
   venc_ptr->proc.cross_fade_cfg_struct.iTotalPeriodMsec = 0;
   venc_ptr->proc.cross_fade_cfg_struct.iConvergeNumSamples = 0;
   venc_ptr->samp_rate = VOICE_NB_SAMPLING_RATE;
   venc_ptr->io.frame_samples = VOICE_FRAME_SIZE_NB_20MS;
   venc_ptr->samp_rate_factor = 1;
   venc_ptr->vsid = 0;
   venc_ptr->tty.option = FALSE;
   venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NONE;


   // Populate default parameters for limiter (with max delay)
   populate_limiter_params(venc_ptr);

   return result;
}

// Reset encoder config params, should be called for a new call and HO to another vocoder
static ADSPResult venc_enc_params_init(venc_t *venc_ptr)
{
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_enc_params_init begin session(%x)",(int)venc_ptr->session_num);
   venc_ptr->voc.min_rate = 0;
   venc_ptr->voc.max_rate = 4;
   venc_ptr->voc.redu_rate = 0;
   venc_ptr->voc.rate_mod = 0;
   venc_ptr->voc.fgv_nb_avg_rate = 10000;
   venc_ptr->voc.fgv_wb_avg_rate = 8500;
   venc_ptr->voc.fgv_nw_avg_rate = 6600;
   venc_ptr->voc.fgv_nw_2k_avg_rate = 2400;
   venc_ptr->voc.fgv_avg_rate_control = 0;
   venc_ptr->voc.amr_nb_mode = 7;
   venc_ptr->voc.prev_amr_nb_mode = 7;
   venc_ptr->voc.amr_wb_mode = 8;
   venc_ptr->voc.prev_amr_wb_mode = 8;
   venc_ptr->voc.dtx_mode = 0;
   venc_ptr->voc.evs_param_struct.enc_sampling_rate = 48000;
   venc_ptr->voc.evs_param_struct.enc_bit_rate = 13200;
   venc_ptr->voc.evs_param_struct.enc_bandwidth = VSM_EVS_VOC_BANDWIDTH_SWB;
   venc_ptr->voc.evs_param_struct.evs_channel_aware_mode = 0;
   venc_ptr->voc.evs_param_struct.fec_offset = 3;
   venc_ptr->voc.evs_param_struct.fer_rate = 0;
   venc_ptr->voc.evs_param_struct.dtx_enable = 0;
   venc_ptr->voc.evs_param_struct.dtx_sid_interval = 8;  // Based on default configuration from vocoder.
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_enc_params_init end session(%x)",(int)venc_ptr->session_num);
   return result;
}

// Reset control code params, should be called for a new call and STOP state
static ADSPResult venc_ctrl_params_init(venc_t *venc_ptr)
{
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_ctrl_params_init begin session(%x)",(int)venc_ptr->session_num);
   venc_ptr->io.in_buf_size = 0;
   venc_ptr->io.out_buf_size = 0;
   venc_ptr->pkt_ready = 0;
   venc_ptr->ctm.resync_delay_cnt = 0;
   venc_ptr->ctm.resync_afe = FALSE;
   venc_ptr->send_media_type = FALSE;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_ctrl_params_init end session(%x)",(int)venc_ptr->session_num);
   return result;
}

static ADSPResult venc_send_rec_buf_to_afe(venc_t* venc_ptr,qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t* peer_data_Q_msg_ptr;
   uint8_t samp_rate_factor = venc_ptr->samp_rate_factor;
   int32_t sample_rate_ms_samples = samp_rate_factor * 8;
   int32_t output_buf_size = voice_cmn_int_div(venc_ptr->record.circ_struct_encout.unread_samples, sample_rate_ms_samples); // finding the number of 1ms samples

   output_buf_size = (output_buf_size * sample_rate_ms_samples) << 1; // covert to num samples, then to bytes

   elite_msg_data_buffer_t* out_data_payload_ptr = (elite_msg_data_buffer_t*) out_buf_mgr_node_ptr->pBuffer;
   out_data_payload_ptr->pResponseQ           = NULL;
   out_data_payload_ptr->unClientToken        = NULL;
   out_data_payload_ptr->pBufferReturnQ       = (venc_ptr->record.buf_q_ptr);
   out_data_payload_ptr->nOffset              = 0;
   out_data_payload_ptr->nActualSize          = output_buf_size;

   if(VSM_RECORD_TX_RX_STEREO == venc_ptr->record.mode)
   {
      out_data_payload_ptr->nActualSize          = out_data_payload_ptr->nActualSize<<1;
   }
   int16_t *out_ptr = (int16 *)(&(out_data_payload_ptr->nDataBuf));
   int16_t recording_input[ENC_OUT_CIRC_BUF_SIZE_REC*MAX_SAMP_RATE_FACTOR]; //20msec data for WB case
   int16_t *in_ptr = &recording_input[0];// pointer to local input buffer
   int16_t i;

   if(TRUE == venc_ptr->record.first_frame) // TODO: not sure why this is needed
   {
      // send 20ms of zeros, which will act as pre-buffering to take care of processing jitters
      // pre-buffering for processing jitters should be atleast "max. processing time - min. processing time"
      // since delay is not a concern in recording, we are considering  "max. processing time - min. processing time = 20ms"
      memset(in_ptr,0,output_buf_size);
   }
   else
   {
      voice_circbuf_read(&venc_ptr->record.circ_struct_encout,(int8_t*)in_ptr,(int32_t) output_buf_size/2, sizeof(recording_input));
      venc_ptr->wait_mask ^= VENC_REC_BUF_MASK; // don't listen to output buf

   }


   if(VSM_RECORD_TX_RX_STEREO == venc_ptr->record.mode)
   {
      for ( i=0;i<(out_data_payload_ptr->nActualSize>>2);i++)
      {
         out_ptr[i] = in_ptr[i];
         out_ptr[i+(out_data_payload_ptr->nActualSize>>2)] = 0;
      }
   }
   else
   {
      for ( i=0;i<(out_data_payload_ptr->nActualSize)/2;i++)
      {
         out_ptr[i] = in_ptr[i];
      }
   }

   // send output buf to downstream service
   peer_data_Q_msg_ptr = elite_msg_convt_buf_node_to_msg(
         out_buf_mgr_node_ptr,
         ELITE_DATA_BUFFER,
         NULL, /* do not need response */
         0,    /* token */
         0     /* do not care response result*/
         );
   // todo: check for downstreampeer null
   result = qurt_elite_queue_push_back(venc_ptr->record.afe_handle_ptr->dataQ, (uint64_t*)peer_data_Q_msg_ptr);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to deliver buffer dowstream. Dropping session(%lx)",venc_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq(venc_ptr->record.buf_q_ptr, (elite_msg_any_payload_t*) out_buf_mgr_node_ptr->pBuffer);
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: VENC_RECORD Delivered buffer dowstream, muteCmd(%d) bytes(%ld) session(%lx)",venc_ptr->mute,output_buf_size,venc_ptr->session_num);
   }
   return result;
}

static ADSPResult venc_send_afe_media_type(venc_t *venc_ptr)
{
   ADSPResult result = ADSP_EOK;
   qurt_elite_bufmgr_node_t buf_mgr_node;
   int32_t actual_size;
   elite_msg_any_t media_type_msg;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_send_afe_media_type begin session(%lx)",venc_ptr->session_num);

   if (ADSP_FAILED(elite_mem_get_buffer(sizeof(elite_msg_data_media_type_apr_t) + sizeof( elite_multi_channel_pcm_fmt_blk_t), &buf_mgr_node, (int *)&actual_size)))
   {
      return ADSP_ENEEDMORE;
   }

   elite_msg_data_media_type_apr_t* pMediaTypePayload =
      (elite_msg_data_media_type_apr_t*)buf_mgr_node.pBuffer;

   pMediaTypePayload->pBufferReturnQ = buf_mgr_node.pReturnQ;
   pMediaTypePayload->pResponseQ = NULL;
   pMediaTypePayload->unClientToken = NULL;

   pMediaTypePayload->unMediaTypeFormat = ELITEMSG_DATA_MEDIA_TYPE_APR;
   pMediaTypePayload->unMediaFormatID   = ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM;

   elite_multi_channel_pcm_fmt_blk_t *pMediaFormatBlk =
      (elite_multi_channel_pcm_fmt_blk_t*)elite_msg_get_media_fmt_blk(pMediaTypePayload);
   memset(pMediaFormatBlk,0,sizeof(elite_multi_channel_pcm_fmt_blk_t));

   pMediaFormatBlk->num_channels           = 1;
   pMediaFormatBlk->channel_mapping[0]     = PCM_CHANNEL_C;
   if(VSM_RECORD_TX_RX_STEREO == venc_ptr->record.mode)
   {
      pMediaFormatBlk->num_channels           = 2; // stereo mode recording
      pMediaFormatBlk->channel_mapping[0]     = PCM_CHANNEL_L;
      pMediaFormatBlk->channel_mapping[1]     = PCM_CHANNEL_R;
   }
   pMediaFormatBlk->bits_per_sample      = 16;
   pMediaFormatBlk->sample_rate         = voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate);

   pMediaFormatBlk->is_signed           = TRUE;
   pMediaFormatBlk->is_interleaved      = FALSE;

   media_type_msg.unOpCode = ELITE_DATA_MEDIA_TYPE;
   media_type_msg.pPayload = (void*) pMediaTypePayload;

   result = qurt_elite_queue_push_back(venc_ptr->record.afe_handle_ptr->dataQ, (uint64_t*)&media_type_msg );
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed deliver buf dwnstrm Dropping! session(%lx)",venc_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq(buf_mgr_node.pReturnQ, (elite_msg_any_payload_t*) buf_mgr_node.pBuffer);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc delivered media type to AFE session(%lx)",venc_ptr->session_num);
      venc_ptr->send_media_type = FALSE;
   }
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_send_afe_media_type end, result(%d) session(%lx)",result,venc_ptr->session_num);
   return result;
}

static int32_t venc_send_dtmf_gen_ended(venc_t *venc_ptr)
{
   vsm_evt_dtmf_generation_ended_t dtmf_gen_ended; // direction
   int32_t rc;
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_DTMF_GENERATION_ENDED APR pkt sent from Q6 - A9 session(%lx)",venc_ptr->session_num);
      dtmf_gen_ended.direction = VSM_SET_DTMF_GEN_TX;
      rc = elite_apr_if_alloc_send_event( venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->proc.apr_info_dtmf_gen.self_addr,
            venc_ptr->proc.apr_info_dtmf_gen.self_port,
            venc_ptr->proc.apr_info_dtmf_gen.client_addr,
            venc_ptr->proc.apr_info_dtmf_gen.client_port,
            NULL,
            VSM_EVT_DTMF_GENERATION_ENDED,
            &dtmf_gen_ended,
            sizeof(vsm_evt_dtmf_generation_ended_t));

      if (ADSP_FAILED(rc))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR request for VSM_EVT_DTMF_GENERATION_ENDED  0x%08lx :session(%lx)",rc,venc_ptr->session_num);
      }
   }
   return rc;
}

static ADSPResult venc_send_oobtty_tx_char_detected(venc_t *venc_ptr)
{
   vsm_evt_outofband_tty_tx_char_detected_t tx_char_detected;
   int32_t rc;
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_OUTOFBAND_TTY_TX_CHAR_DETECTED APR pkt sent from VSM to client, session(%lx)",venc_ptr->session_num);
      tx_char_detected.tty_char = voice_oobtty_tx_get_char(&venc_ptr->oobtty_tx_struct);
      rc = (ADSPResult) elite_apr_if_alloc_send_event( venc_ptr->apr_info_ptr->apr_handle,
            venc_ptr->apr_info_ptr->self_addr,
            venc_ptr->apr_info_ptr->self_port,
            venc_ptr->apr_info_ptr->client_addr,
            venc_ptr->apr_info_ptr->client_port,
            NULL, /* token */
            VSM_EVT_OUTOFBAND_TTY_TX_CHAR_DETECTED,
            &tx_char_detected,
            sizeof(vsm_evt_outofband_tty_tx_char_detected_t));

      if (ADSP_FAILED(rc))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR request for VSM_EVT_OUTOFBAND_TTY_TX_CHAR_DETECTED  0x%08lx :session(%lx)",rc,venc_ptr->session_num);
      }
   }
   return rc;
}

static ADSPResult venc_check_enc_struct_size(uint32_t enc_struct_size, uint32_t sess_num)
{
   ADSPResult result = ADSP_EOK;
   if (enc_struct_size > ENC_STATE_MEMORY_IN_BYTES)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Error - Enc struct size(%ld) exceeds static size allocated, \
            session(%lx)",enc_struct_size, sess_num);
      result = ADSP_ENOMEMORY;
   }
   return result;
}

static ADSPResult  venc_log_vocoder_packet( uint32_t voc_type, uint32_t log_session_id, void *voc_packet, uint16_t size, uint16_t sampling_rate)
{
#if defined(__qdsp6__) && !defined(SIM)
   int8_t *bufptr[4] = { (int8_t *) voc_packet, NULL, NULL, NULL };
   // log saved input buf and preproc output prior to encode
   voice_log_buffer( bufptr,
         VOICE_LOG_CHAN_PKT_TX_STREAM,
         log_session_id,
         qurt_elite_timer_get_time(),
         VOICE_LOG_DATA_FORMAT_PCM_MONO,
         sampling_rate,
         size,
         &voc_type);   // custom data, to send media type info
#endif

   return ADSP_EOK;
}

static void venc_init_rec_circbuf(venc_t* venc_ptr)
{
   if (NULL != venc_ptr->record.circ_encout_buf_ptr)
   {
      uint8_t samp_rate_factor;
      int16_t temp_buf[SS_DELAY_RECORD_PATH*MAX_SAMP_RATE_FACTOR]; // temp 2msec buffer for worst case - fb number of samples
      memset(temp_buf,0,SS_DELAY_RECORD_PATH*MAX_SAMP_RATE_FACTOR*sizeof(int16_t)); // clear the buffer

      samp_rate_factor=venc_ptr->samp_rate_factor;

      voice_circbuf_init(&(venc_ptr->record.circ_struct_encout), (int8_t*)(venc_ptr->record.circ_encout_buf_ptr),
            (int32_t)samp_rate_factor*ENC_OUT_CIRC_BUF_SIZE_REC /* 20ms + 4msec delay*/,
            MONO_VOICE,
            (int32_t)16 /*bitperchannel*/
            );
      voice_circbuf_write(&(venc_ptr->record.circ_struct_encout),
            (int8_t *)&(temp_buf[0]),
            samp_rate_factor*SS_DELAY_RECORD_PATH // add 2ms delay for pre buffering
            );
   }
}

static void venc_vtm_subscribe(venc_t* venc_ptr,Vtm_SubUnsubMsg_t *data_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   if(VFR_MODE_END > venc_ptr->vfr_mode)
   {
      if (ADSP_FAILED(result = voice_custom_vt_sub_unsub_msg_send(NULL,venc_ptr->vtm_cmd_q_ptr,VOICE_TIMER_SUBSCRIBE,NULL,FALSE,data_ptr)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed Venc subscribe with vtm memory(%p) Vtm session(%#x) ",data_ptr, (int)venc_ptr->session_num);
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc subscribed with vtm memory(%p) Vtm session(%#x) ",data_ptr, (int)venc_ptr->session_num);
      }
   }
}

static void venc_vtm_unsubscribe(venc_t* venc_ptr,Vtm_SubUnsubMsg_t *data_ptr,uint32_t mask)
{
   ADSPResult result = ADSP_EFAILED;
   if(VFR_MODE_END > venc_ptr->vfr_mode)
   {
      if (ADSP_FAILED(result = voice_custom_vt_sub_unsub_msg_send(NULL,venc_ptr->vtm_cmd_q_ptr,VOICE_TIMER_UNSUBSCRIBE,NULL,FALSE,data_ptr)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed Venc unsubscribe with vtm memory(%p) Vtm session(%#x) ",data_ptr, (int)venc_ptr->session_num);
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc unsubscribed with vtm memory(%p) Vtm session(%#x) ",data_ptr, (int)venc_ptr->session_num);
      }

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc waiting for unsubscribe to finish - vtm memory(%p) Vtm session(%#x) ",data_ptr, (int)venc_ptr->session_num);
      (void)qurt_elite_channel_wait(&venc_ptr->qurt_elite_channel, mask);
      qurt_elite_signal_clear(data_ptr->signal_end_ptr);
   }
}

static ADSPResult venc_config_host_pcm(void* pInstance, elite_msg_any_t* pMsg)
{
   ADSPResult nResult = ADSP_EOK;
   ADSPResult apr_pkt_result = APR_EOK;
   venc_t* venc_ptr = (venc_t*) pInstance;
   elite_msg_custom_voc_config_host_pcm_type *pConfig = (elite_msg_custom_voc_config_host_pcm_type *) pMsg->pPayload;
   elite_apr_packet_t *apr_packet_ptr = pConfig->apr_packet_ptr;
   uint16_t read_enable, write_enable;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_config_host_pcm begin session(%x)\n",(int)venc_ptr->session_num);

   if( (VOICE_CMD_START_HOST_PCM_V2 == elite_apr_if_get_opcode( apr_packet_ptr)) &&
         ((VOICE_NB_SAMPLING_RATE == venc_ptr->samp_rate) || (VOICE_WB_SAMPLING_RATE == venc_ptr->samp_rate) ||
                                                                  (VOICE_SWB_SAMPLING_RATE == venc_ptr->samp_rate) || (VOICE_FB_SAMPLING_RATE == venc_ptr->samp_rate))
     )
   {
      voice_start_host_pcm_v2_t    *start_host_pcm_ptr = (voice_start_host_pcm_v2_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      uint8 index;

      // check if default Tx stream tap point in the list
      for( index = 0; index < start_host_pcm_ptr->num_tap_points; index++)
      {
         if( VOICESTREAM_MODULE_TX == start_host_pcm_ptr->tap_points[index].tap_point )
         {
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_config_host_pcm: tap point being enabled is (%lx), session(%x)\n",(uint32_t)VOICESTREAM_MODULE_TX,(int)venc_ptr->session_num);
            break;
         }
      }

      voice_host_pcm_get_enable( &venc_ptr->proc.host_pcm_context, &read_enable, VOICE_HOST_PCM_READ);
      voice_host_pcm_get_enable( &venc_ptr->proc.host_pcm_context, &write_enable, VOICE_HOST_PCM_WRITE);

      /* make sMode match definition of VOICE_HOST_PCM_* => 0,1,2,3 = {OFF,R,W,RW} */
      uint16_t sMode = (write_enable << 1) | read_enable;

      // check if valid index found.  TODO: check if tap point already enabled
      if( index < start_host_pcm_ptr->num_tap_points &&
            VOICE_HOST_PCM_OFF == sMode)
      {

         voice_tap_point_v2_t *voice_tap_point_ptr = &start_host_pcm_ptr->tap_points[index];

         if(NULL == voice_tap_point_ptr->mem_map_handle)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Venc received NULL mem map handle!");
            apr_pkt_result = APR_EBADPARAM;
         }
         else
         { // scope begining
            /* copy Host Pcm APR info */
            venc_ptr->proc.host_pcm_context.apr_handle        = pConfig->apr_handle;
            venc_ptr->proc.host_pcm_context.shared_mem_client = vsm_memory_map_client;
            venc_ptr->proc.host_pcm_context.mem_map_handle    = voice_tap_point_ptr->mem_map_handle;
            venc_ptr->proc.host_pcm_context.self_addr         = elite_apr_if_get_dst_addr( apr_packet_ptr);
            venc_ptr->proc.host_pcm_context.self_port         = elite_apr_if_get_dst_port( apr_packet_ptr);
            venc_ptr->proc.host_pcm_context.client_addr       = elite_apr_if_get_src_addr( apr_packet_ptr);
            venc_ptr->proc.host_pcm_context.client_port       = elite_apr_if_get_src_port( apr_packet_ptr);
            venc_ptr->proc.host_pcm_context.tap_point         = VOICESTREAM_MODULE_TX;
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc setting hpcm context, memmmap handle (%x), client addr (%x), client port (%x)",
                  (unsigned int)venc_ptr->proc.host_pcm_context.mem_map_handle, (unsigned int)venc_ptr->proc.host_pcm_context.client_addr, (unsigned int)venc_ptr->proc.host_pcm_context.client_port);

            /* enable Host pcm, R only,W only,or RW */

            /* enable read session if enabled */
            if( voice_tap_point_ptr->direction & VOICE_HOST_PCM_READ)
            {
               voice_host_pcm_set_enable( &venc_ptr->proc.host_pcm_context,
                     TRUE,
                     VOICE_HOST_PCM_READ,
                     1 /*num of chan*/,
                     voice_tap_point_ptr->sampling_rate,
                     venc_ptr->samp_rate,
                     venc_ptr->io.frame_samples);
            }

            /* enable write session if enabled */
            if( voice_tap_point_ptr->direction & VOICE_HOST_PCM_WRITE)
            {
               voice_host_pcm_set_enable( &venc_ptr->proc.host_pcm_context,
                     TRUE,
                     VOICE_HOST_PCM_WRITE,
                     1 /*num of chan*/,
                     voice_tap_point_ptr->sampling_rate,
                     venc_ptr->samp_rate,
                     venc_ptr->io.frame_samples);
            } // end of if( voice_tap_point_ptr->direction & VOICE_HOST_PCM_WRITE)
         } // scope ending
      } // end of if valid index found
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_config_host_pcm START command NOP - tappoint not found or already enabled, session(%lx)\n",venc_ptr->session_num);
      }
   }
   else if( VOICE_CMD_STOP_HOST_PCM == elite_apr_if_get_opcode( apr_packet_ptr))
   {
      voice_host_pcm_set_enable( &venc_ptr->proc.host_pcm_context,
            FALSE,
            VOICE_HOST_PCM_READ,
            1 /*num of chan*/,
            0,   /* dummy sampling rate */
            venc_ptr->samp_rate,
            venc_ptr->io.frame_samples);

      voice_host_pcm_set_enable( &venc_ptr->proc.host_pcm_context,
            FALSE,
            VOICE_HOST_PCM_WRITE,
            1 /*num of chan*/,
            0,   /* dummy sampling rate */
            venc_ptr->samp_rate,
            venc_ptr->io.frame_samples);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_config_host_pcm failed - opcode or sampling rate unsupported, session(%lx)\n",venc_ptr->session_num);
      apr_pkt_result = APR_EUNSUPPORTED;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_config_host_pcm end apr pkt result(%d) session(%x)\n",apr_pkt_result, (int)venc_ptr->session_num);

   elite_svc_send_ack(pMsg, apr_pkt_result);

   return nResult;
}
static uint32_t venc_get_enc_static_size( )
{
   uint32_t mem_size = 0;
   if (ENC_STATE_MEMORY_IN_BYTES > get_sizeof_evs_enc_struct())
   {
      mem_size = ENC_STATE_MEMORY_IN_BYTES;
   }
   else
   {
      mem_size = get_sizeof_evs_enc_struct();
   }
   return (mem_size);
}

static ADSPResult venc_allocate_mem(venc_t * venc_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint32_t total_size;
   uint32_t ss_size, enc_size,dtmf_gen_size,limiter_lib_size,rec_buf_size,downby2_size,inp_buf_size,out_buf_size,mute_inp_size,resampler_size,ctm_tx_size;
   uint16_t sampling_rate_factor = VOICE_FB_SAMPLING_RATE/VOICE_NB_SAMPLING_RATE; //considering VOICE_FB_SAMPLING_RATE for max mem allocation
   int8_t* limiter_ptr;

   inp_buf_size = VOICE_ROUNDTO8((VOICE_FRAME_SIZE_NB_20MS*sampling_rate_factor)<<1);
   out_buf_size = VOICE_ROUNDTO8(( (VOICE_FRAME_SIZE_NB_20MS*sampling_rate_factor)<<1) + 2) ;  // adding 2 for EVS WB mode can have packet size of 322 bytes
   enc_size     = venc_get_enc_static_size();
   enc_size     = VOICE_ROUNDTO8(enc_size);
   dtmf_gen_size = VOICE_ROUNDTO8(sizeof(dtmf_gen_struct_t));
   limiter_lib_size = VOICE_ROUNDTO8(Lim_get_lib_size(venc_ptr->proc.limiter_params[LIM_DELAY],VOICE_FB_SAMPLING_RATE,NO_OF_CHANNELS));
   rec_buf_size = VOICE_ROUNDTO8(inp_buf_size+((2*SS_DELAY_REC_PATH_NB*sampling_rate_factor)<<1));
   voice_ss_get_size(&ss_size);
   ss_size = VOICE_ROUNDTO8(ss_size);
   downby2_size = VOICE_ROUNDTO8(venc_ptr->tty.resamp_config.total_channel_mem_size);
   mute_inp_size= VOICE_FRAME_SIZE_FB << 1;   // worst case allocation
   resampler_size = VOICE_ROUNDTO8(voice_gen_resamp_get_size((int16)GEN_RESAMP_LP_LPH,(uint8)TWO_BYTES_PER_SAMPLE,&(venc_ptr->proc.codec_resampler)));
   ctm_tx_size = VOICE_ROUNDTO8(sizeof(int16_t)*ctmtx_get_struct_size());

   total_size = inp_buf_size
      + out_buf_size
      + enc_size
      + dtmf_gen_size
      + limiter_lib_size
      + rec_buf_size
      + ss_size
      + downby2_size
      + mute_inp_size
      + resampler_size
      + ctm_tx_size;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Computed memory requirement%d",(int)total_size);



   venc_ptr->memory.start_addr_ptr = (int8_t *)qurt_elite_memory_malloc(total_size, QURT_ELITE_HEAP_DEFAULT);
   if (NULL == venc_ptr->memory.start_addr_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to allocate memory for venc modules!! session(%lx)",venc_ptr->session_num);
      return ADSP_ENOMEMORY;
   }
   memset( venc_ptr->memory.start_addr_ptr, 0, total_size); // clear the memory before use

   venc_ptr->memory.size = total_size; // size allocated

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Allocated memory %d", (int)venc_ptr->memory.size);

   venc_ptr->memory.usage_addr_ptr = venc_ptr->memory.start_addr_ptr; // Usage level pointer

   venc_ptr->proc.mute_inp_ptr = (int16_t *)venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(mute_inp_size); // UsageAddr is a byte pointer

   venc_ptr->io.in_buf = (int16_t *)venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(inp_buf_size); // UsageAddr is a byte pointer

   venc_ptr->io.out_buf = (int16_t *)venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(out_buf_size); // UsageAddr is a byte pointer

   venc_ptr->enc_state_ptr = (int64_t *)venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(enc_size); // UsageAddr is a byte pointer

   venc_ptr->proc.dtmf_gen_ptr = (dtmf_gen_struct_t*)venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(dtmf_gen_size); // UsageAddr is a byte pointer

   limiter_ptr = venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(limiter_lib_size); // UsageAddr is a byte pointer

   venc_ptr->record.circ_encout_buf_ptr = venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(rec_buf_size); // UsageAddr is a byte pointer

   venc_ptr->record.ss_struct_ptr = venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(ss_size); // UsageAddr is a byte pointer

   venc_ptr->tty.onex_downby2_mem_ptr = venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(downby2_size); // UsageAddr is a byte pointer

   voice_gen_resamp_set_mem(&(venc_ptr->proc.codec_resampler),(int8_t*)venc_ptr->memory.usage_addr_ptr, resampler_size);
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(resampler_size); // UsageAddr is a byte pointer

   venc_ptr->ctm.state.ctm_tx_struct_instance_ptr = venc_ptr->memory.usage_addr_ptr;
   venc_ptr->memory.usage_addr_ptr += VOICE_ROUNDTO8(ctm_tx_size); // UsageAddr is a byte pointer

   // Sanity check to see the allocated memory was enough
   if ((uint32_t)(venc_ptr->memory.usage_addr_ptr - venc_ptr->memory.start_addr_ptr) <= venc_ptr->memory.size)
   {
      result = ADSP_EOK;
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error: allocated memory %d, memory consumed %d session(%x)",(int)venc_ptr->memory.size,
            (int)(venc_ptr->memory.usage_addr_ptr - venc_ptr->memory.start_addr_ptr),
            (int)venc_ptr->session_num);
      result = ADSP_ENOMEMORY;
      return result;
   }

   venc_ptr->proc.limiter_ptr = (CLimiterLib*)limiter_ptr;
   Lim_set_lib_memory(limiter_ptr,(CLimiterLib**)(&(venc_ptr->proc.limiter_ptr)),venc_ptr->proc.limiter_params[LIM_DELAY],VOICE_FB_SAMPLING_RATE,0/*channel index*/);

   voice_ss_set_mem(&(venc_ptr->record.ss_struct),venc_ptr->record.ss_struct_ptr,ss_size);

   return result;
}

static void venc_allocate_mem_free(venc_t * venc_ptr)
{

   venc_ptr->io.in_buf = NULL;
   venc_ptr->io.out_buf = NULL;
   venc_ptr->record.circ_encout_buf_ptr = NULL;
   if(NULL != venc_ptr->proc.dtmf_gen_ptr)
   {
      voice_dtmf_resampler_mem_free(venc_ptr->proc.dtmf_gen_ptr);
      venc_ptr->proc.dtmf_gen_ptr = NULL;
   }
   venc_ptr->proc.limiter_ptr = NULL;
   venc_ptr->tty.onex_downby2_mem_ptr = NULL;
   venc_ptr->ctm.state.ctm_tx_struct_instance_ptr=NULL;
   if(NULL !=  venc_ptr->record.ss_struct_ptr)
   {
      voice_ss_end(&(venc_ptr->record.ss_struct));
      venc_ptr->record.ss_struct_ptr = NULL;
   }
   if (NULL != venc_ptr->voc.four_gv_encode_ptr)
   {
      venc_ptr->voc.four_gv_encode_ptr->FGVEncodeDestructor();
      venc_ptr->voc.four_gv_encode_ptr = NULL;
   }
   if(venc_ptr->voc_type == VSM_MEDIA_TYPE_EVS)
   {
      evs_enc_destroy((void *)venc_ptr->enc_state_ptr);
   }
   venc_ptr->enc_state_ptr = NULL;
   venc_ptr->proc.mute_inp_ptr = NULL;

   voice_gen_resamp_end(&venc_ptr->proc.codec_resampler);

   qurt_elite_memory_free(venc_ptr->memory.start_addr_ptr);
   venc_ptr->memory.start_addr_ptr = NULL;
   venc_ptr->memory.usage_addr_ptr = NULL;
   venc_ptr->memory.size = 0;


}



ADSPResult venc_modules_init(venc_t* venc_ptr)
{


   ADSPResult result = ADSP_EOK;
   result = voice_dtmf_gen_default_init(venc_ptr->proc.dtmf_gen_ptr);
   if(result != ADSP_EOK)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: DTMF Gen default init failed ");
      return result;
   }

   venc_init_rec_circbuf(venc_ptr);

   return result;


}
static void populate_limiter_params(void* instance_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   venc_ptr->proc.limiter_params[LIM_ENABLE]      = 0x0;      // Q0 Limiter enable word (default Disable)
   venc_ptr->proc.limiter_params[LIM_MODE]        = 0x1;      // Q0 Limiter mode word
   venc_ptr->proc.limiter_params[LIM_MAKEUP_GAIN] = 0x100;    // Q7.8 Limiter make-up gain
   venc_ptr->proc.limiter_params[LIM_GC]          = 0x7EB8;   // Q15 Limiter gain recovery coefficient
   venc_ptr->proc.limiter_params[LIM_DELAY]       = LIM_MAX_DELAY;  // Q15 Limiter waiting time in seconds 10ms
   venc_ptr->proc.limiter_params[LIM_MAX_WAIT]    = 0x147;    // Q15 Limiter delay in seconds
   venc_ptr->proc.limiter_params[LIM_THRESH]      = 0x1FF8;   // -12 dB in Q1.15 for limiting to 14 bits on tx path
}

void voice_saturate_to_14_bits(int16_t* speech, int16_t samples)
{
   int16_t i;

   for (i=0; i<samples; i++)
   {
      if (speech[i] >  VOICE_MAX_14)
      {
         speech[i] = VOICE_MAX_14;
      }
      else if (speech[i] < VOICE_MIN_14)
      {
         speech[i] = VOICE_MIN_14;
      }
   }
}

static ADSPResult venc_aggregate_modules_kpps(void* instance_ptr, uint32_t* kpps_changed)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   if ((NULL == venc_ptr) || (NULL == kpps_changed))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Voice Encoder KPPS aggregator received bad pointers");
      return ADSP_EBADPARAM;
   }

   uint32_t kpps;
   uint32_t aggregate_kpps=0;
   uint8_t  ctm_tx_enable = FALSE;
   uint16_t venc_samp_rate = 0;
   venc_samp_rate = voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate);
   bool_t tty_enable  = (venc_ptr->tty.state_ptr->m_etty_mode != VSM_TTY_MODE_OFF);
   bool_t oobtty_enable  = (venc_ptr->tty.state_ptr->m_oobtty_mode != VSM_TTY_MODE_OFF);

   switch(venc_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
      case VSM_MEDIA_TYPE_EFR_MODEM:
      case VSM_MEDIA_TYPE_FR_MODEM:
      case VSM_MEDIA_TYPE_HR_MODEM:
      case VSM_MEDIA_TYPE_EAMR:
      case VSM_MEDIA_TYPE_EVS:
         {
            ctm_tx_enable = tty_enable;
            break;
         }
   }
   // Comfort Noise
   kpps = 0;

   venc_ptr->proc.comfort_noise_ptr->GetKpps(&kpps, venc_samp_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: CNGLVB kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // DTMF PCM
   kpps = 0;
   voice_dtmf_gen_get_kpps(venc_ptr->proc.dtmf_gen_ptr, &kpps, venc_samp_rate );
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: DTMF Gen kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // Host PCM
   kpps = 0;
   voice_host_pcm_get_kpps( &venc_ptr->proc.host_pcm_context, &kpps);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Host PCM kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // CTM Tx
   kpps = 0;
   voice_ctm_tx_get_kpps( &venc_ptr->ctm.state, &kpps, ctm_tx_enable, venc_samp_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: CTM Tx kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // OOBTTY Tx
   kpps = 0;
   voice_oobtty_tx_get_kpps(&venc_ptr->oobtty_tx_struct, &kpps, oobtty_enable, venc_samp_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "OOBTTY Tx kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // Voice Encoder KPPS
   kpps = 0;
   vcmn_find_vockpps_table(VOICE_ENCODE_KPPS_TABLE, venc_ptr->voc_type,0/*BeAMR flag for encoder*/, &kpps, venc_ptr->voc.evs_param_struct.enc_sampling_rate, 1);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Encoder kpps (%d), session number (%lx)", (int)kpps, venc_ptr->session_num);
   aggregate_kpps += kpps;

   // TODO: Retaining this margin for safety, will need to be reassessed
   aggregate_kpps += 1000; // adding extra 1000 kpps to ceil the number to mpps (to include Limiter)

   // Retaining behavior as it is as of today. That is to report an increase in KPPS only.
   // Once the VOICE_CMD_SET_TIMING_PARAMS commands are removed,
   // this can be modified to (venc_ptr->aggregate_kpps != aggregate_kpps)
   if (venc_ptr->aggregate_kpps >= aggregate_kpps)
   {
      *kpps_changed = FALSE;
   }
   else
   {
      *kpps_changed = TRUE;
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VEnc KPPS changed from (%lu) to (%lu), session(%lx)", venc_ptr->aggregate_kpps, aggregate_kpps, venc_ptr->session_num);
   }

   // Update state
   venc_ptr->aggregate_kpps = aggregate_kpps;

   return ADSP_EOK;
}

static ADSPResult venc_aggregate_modules_delay(void* instance_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   if ((NULL == venc_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Voice Encoder delay aggregator received bad pointers");
      return ADSP_EBADPARAM;
   }

   uint32_t delay;
   uint32_t aggregate_delay=0;

   //Stream limiter delay
   delay = 0;
   if(venc_ptr->proc.limiter_params[LIM_ENABLE])
   {
      // max lim delay is 327, so multiplication by 1000000 is safe, since it easily fits into uint32
      // lim delay is in q15 seconds, so multiply by 1e6 and then right shift by 15 to get in q0 us
      delay = (((uint32_t)venc_ptr->proc.limiter_params[LIM_DELAY]) * 1000000) >> 15;
   }
   aggregate_delay += delay;

   // Voice Encoder Delay
   delay = 0;
   (void)venc_get_vocoder_delay(venc_ptr->voc_type, &delay);
   aggregate_delay += delay;

   // Update state
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Encoder delay (%d), session number (%lx)", (int)aggregate_delay, venc_ptr->session_num);
   venc_ptr->aggregate_delay = aggregate_delay;

   return ADSP_EOK;
}

static ADSPResult venc_set_timingv3_cmd(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timingv3_cmd begin session(%lx)",venc_ptr->session_num);

   // Verify Stop state of thread before updating timing values.
   if (FALSE == venc_ptr->process_data)
   {
      vsm_set_timing_params_v2_t* vfr_cmd_ptr = (vsm_set_timing_params_v2_t*)set_timing_cmd_ptr->param_data_ptr;

      //dbg msg is printed in VSM with all params
      /*MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timingv3_cmd, mode(%d), VSID(0x%lx), \
        enc_offset(%d), decreq_offset(%d), dec_offset(%d),decpp_offset(%d),session(%x)",
        vfr_cmd_ptr->mode,vfr_cmd_ptr->vsid,vfr_cmd_ptr->enc_offset,vfr_cmd_ptr->dec_req_offset,
        vfr_cmd_ptr->dec_offset,vfr_cmd_ptr->decpp_offset,venc_ptr->session_num); */

      // Verify validity of VFR mode. In this version of timing cmd, mode supports only two values - VFR_NONE and VFR_HARD
      // If VFR_HARD, further information is derived from VSID
      if ((VFR_HARD >= vfr_cmd_ptr->mode) )
      {
         venc_ptr->vfr_mode = vfr_cmd_ptr->mode;
      }
      else
      {
         venc_ptr->vfr_mode = VFR_NONE;
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_set_timingv3_cmd, invalid mode(%x),setting to VFR_NONE,session(%lx)",vfr_cmd_ptr->mode,venc_ptr->session_num);
         result = ADSP_EBADPARAM;
      }

      // Verify validity of VSID. In VFR_HARD case, VSID should be non-zero. In VFR_NONE case, VSID is don't care.
      if ((VFR_HARD == venc_ptr->vfr_mode) && (0 == vfr_cmd_ptr->vsid))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_set_timingv3_cmd, invalid VSID(%lx), session(%lx)",vfr_cmd_ptr->vsid,venc_ptr->session_num);
         result = ADSP_EBADPARAM;
      }
      else
      {
         venc_ptr->vsid = vfr_cmd_ptr->vsid;
      }

      // Verify validity of enc_offset
      // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min because it's unsigned
      if (MAX_TIMER_OFFSET < vfr_cmd_ptr->enc_offset)
      {
         venc_ptr->vtm_sub_unsub_data.offset = 8000; // default 8ms
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc_set_timingv3_cmd, Invalid enc_offset(%x), defaulting to 8ms, session(%lx)", \
               vfr_cmd_ptr->enc_offset,venc_ptr->session_num);
         result = ADSP_EBADPARAM;
      }
      else
      {
         venc_ptr->vtm_sub_unsub_data.offset = vfr_cmd_ptr->enc_offset;
      }

      //write into vtm subscribe struct
      venc_ptr->vtm_sub_unsub_data.vfr_mode = (uint8_t)venc_ptr->vfr_mode;
      venc_ptr->vtm_sub_unsub_data.vsid = venc_ptr->vsid;
      venc_ptr->vtm_sub_unsub_data.timing_ver = VFR_CLIENT_INFO_VER_2; //update version of timing used
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Timing can't be changed in RUN, session(%lx)",venc_ptr->session_num);
      result = ADSP_EBUSY;
   }

   elite_svc_send_ack(msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: venc_set_timingv3_cmd end result(%d) session(%lx)",result,venc_ptr->session_num);
   return ADSP_EOK;
}


static ADSPResult venc_get_vocoder_delay(uint32_t voc_type, uint32_t* delay_ptr)
{
   //delay_ptr assumed valid
   *delay_ptr = 0;
   switch(voc_type)
   {
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            *delay_ptr = VOICE_DELAY_V13K;
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            *delay_ptr = VOICE_DELAY_EVRC;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
         {
            *delay_ptr = VOICE_DELAY_4GV_NB;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
         {
            *delay_ptr = VOICE_DELAY_4GV_WB;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
         {
            *delay_ptr = VOICE_DELAY_4GV_NW;
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
         {
            *delay_ptr = VOICE_DELAY_AMR_NB;
            break;
         }
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            *delay_ptr = VOICE_DELAY_AMR_WB;
            break;
         }
      case VSM_MEDIA_TYPE_EFR_MODEM:
         {
            *delay_ptr = VOICE_DELAY_EFR;
            break;
         }
      case VSM_MEDIA_TYPE_FR_MODEM:
         {
            *delay_ptr = VOICE_DELAY_FR;
            break;
         }
      case VSM_MEDIA_TYPE_HR_MODEM:
         {
            *delay_ptr = VOICE_DELAY_HR;
            break;
         }
      case VSM_MEDIA_TYPE_PCM_8_KHZ:
      case VSM_MEDIA_TYPE_PCM_16_KHZ:
      case VSM_MEDIA_TYPE_PCM_32_KHZ:
      case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
      case VSM_MEDIA_TYPE_PCM_48_KHZ:
         {
            //no delay
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW:
      case VSM_MEDIA_TYPE_G711_MLAW:
      case VSM_MEDIA_TYPE_G711_ALAW_V2:
      case VSM_MEDIA_TYPE_G711_MLAW_V2:
         {
            //no delay
            break;
         }
      case VSM_MEDIA_TYPE_G729AB:
         {
            //to be determined, zero for now
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            *delay_ptr = VOICE_DELAY_EAMR;
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            *delay_ptr = VOICE_DELAY_4GV_NW2K;
            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            *delay_ptr = VOICE_DELAY_EVS;
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Invalid vocoder type for delay (%lx), returning zero", voc_type);
         }
   }
   return ADSP_EOK;
}

static ADSPResult venc_register_event(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_event_reg_type *payload_ptr = (elite_msg_custom_event_reg_type*) msg_ptr->pPayload;
   uint32_t event_id = payload_ptr->event_id;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc received registration for event %lx", event_id);

   switch(event_id)
   {
      case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
         {
            // set mode detection to true
            venc_ptr->voc.vocoder_op_detection = 1;
            // Issue default mode event
            venc_send_mode_notification_v2(instance_ptr);
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Venc received registration for invalid event %lx", event_id);
         }
   }
   elite_msg_return_payload_buffer( msg_ptr );
   return ADSP_EOK;
}

static ADSPResult venc_unregister_event(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_event_reg_type *payload_ptr = (elite_msg_custom_event_reg_type*) msg_ptr->pPayload;
   uint32_t event_id = payload_ptr->event_id;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc received deregistration for event %lx", event_id);

   switch(event_id)
   {
      case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
         {
            // set mode detection to true
            venc_ptr->voc.vocoder_op_detection = 0;
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Venc received deregistration for invalid event %lx", event_id);
         }
   }
   elite_msg_return_payload_buffer( msg_ptr );
   return ADSP_EOK;
}

static ADSPResult venc_send_mode_notification_v2(void* instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;

   if(0 == venc_ptr->voc.vocoder_op_detection)
   {
      return ADSP_EOK;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: venc_send_mode_notification_v2 begin session(%lx)",venc_ptr->session_num);
   vsm_evt_voc_operating_mode_update_t mode_update;

   // EAMR is always sent by decoder
   if(VSM_MEDIA_TYPE_EAMR == venc_ptr->voc_type)
   {
      return ADSP_EOK;
   }

   mode_update.direction = VSM_VOC_OPERATING_MODE_DIRECTION_TX;
   mode_update.reserved = 0;
   mode_update.mode = venc_ptr->voc.vocoder_op_mode;

   result = elite_apr_if_alloc_send_event( venc_ptr->apr_info_ptr->apr_handle,
         venc_ptr->apr_info_ptr->self_addr,
         venc_ptr->apr_info_ptr->self_port,
         venc_ptr->apr_info_ptr->client_addr,
         venc_ptr->apr_info_ptr->client_port,
         0,
         VSM_EVT_VOC_OPERATING_MODE_UPDATE,
         &mode_update,
         (sizeof(mode_update)));

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR request for VSM_EVT_VOC_OPERATING_MODE_UPDATE :session(%lx)",venc_ptr->session_num);
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Venc sent the vocoder mode change notification, session(%lx), mode(%lx)",venc_ptr->session_num, mode_update.mode);
   }
   return result;
}

static void venc_set_default_op_mode(void* instance_ptr)
{
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   switch(venc_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_NONE:
         {
            venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NONE;
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_WB;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NB;
            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            venc_ptr->voc.vocoder_op_mode = evs_find_operating_bandwidth(venc_ptr->voc.evs_param_struct.enc_bandwidth);
            break;
         }
      default:
         {
            uint32_t sampling_rate = voice_get_sampling_rate(venc_ptr->voc_type,0/*BeAMR flag for enacoder*/,venc_ptr->voc.evs_param_struct.enc_sampling_rate);
            if(VOICE_NB_SAMPLING_RATE == sampling_rate)
            {
               venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NB;
            }
            else if(VOICE_WB_SAMPLING_RATE == sampling_rate)
            {
               venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_WB;
            }
            else if(VOICE_SWB_SAMPLING_RATE == sampling_rate)
            {
               venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_SWB;
            }
            else
            {
               venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_FB;
            }
            break;
         }
   }
   return;
}

static ADSPResult venc_set_param_v3(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_set_param_v3_type *payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_ptr->pPayload;
   uint32_t cal_handle = payload_ptr->cal_handle;
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Venc received set_param_v3, handle %lx, session(%lx)", cal_handle, venc_ptr->session_num);

   //call into mvm using mvm_call as an entry point to cvd_cal_query
   vss_imvm_cmd_cal_query_t mvm_payload;
   mvm_payload.query_handle = payload_ptr->cal_handle;
   mvm_payload.cb_fn = venc_calibration_cb_func;
   mvm_payload.client_data = instance_ptr;

   result = mvm_call(MVM_CMDID_CAL_QUERY, &mvm_payload, sizeof(mvm_payload));

   elite_svc_send_ack(msg_ptr, result);
   return result;
}

void venc_calibration_cb_func(cvd_cal_param_t* cal_params_ptr, void* cb_data)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)cb_data;
   uint32_t mod_id, param_id, param_size;
   mod_id = cal_params_ptr->module_id;
   param_id = cal_params_ptr->param_id;
   param_size = cal_params_ptr->param_data_size;

   result = venc_set_param_int(venc_ptr, mod_id, param_id, cal_params_ptr->param_data, param_size);
   if(ADSP_EOK != result && ADSP_EUNSUPPORTED != result)
   {
      MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: venc set param error %u, mod %lx, param %lx, session(%lx)",result, mod_id, param_id, venc_ptr->session_num);
   }
   return;
}

static ADSPResult venc_set_enc_rate(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   venc_t* venc_ptr = (venc_t*)instance_ptr;
   elite_msg_custom_set_enc_rate_type *payload_ptr = (elite_msg_custom_set_enc_rate_type*)msg_ptr->pPayload;
   uint32_t media_type, encoder_rate;

   media_type = payload_ptr->media_type;
   encoder_rate = payload_ptr->encoder_rate;

   switch (media_type)
   {
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            if (encoder_rate <= 4)
            {
               venc_ptr->voc.redu_rate = encoder_rate;
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid v13k reduced rate level  session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
         {
            if (encoder_rate <= 7)
            {
               venc_ptr->voc.fgv_nb_avg_rate = fgv_nb_cop_avg_rate_table[encoder_rate];
               //avg rate control shd be set only when we get a RATE
               //command and cannot be set every time in the process
               //function as its updated in lib also.
               venc_ptr->voc.fgv_avg_rate_control = 1;
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid 4gv-nb cop session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
         {
            if ((0 == encoder_rate) ||
                  (4 == encoder_rate) ||
                  (7 == encoder_rate))
            {                        //check if we need to issue a mode change event
               if((0 == encoder_rate) &&
                     (venc_ptr->voc.fgv_wb_avg_rate != fgv_wb_cop_avg_rate_table[encoder_rate]))
               {
                  //nb to wb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_WB;
                  venc_send_mode_notification_v2(instance_ptr);
               }
               else if((0 != encoder_rate) &&
                     (venc_ptr->voc.fgv_wb_avg_rate == fgv_wb_cop_avg_rate_table[0]))
               {
                  //wb to nb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NB;
                  venc_send_mode_notification_v2(instance_ptr);
               }

               venc_ptr->voc.fgv_wb_avg_rate = fgv_wb_cop_avg_rate_table[encoder_rate];
               //avg rate control shd be set only when we get a RATE
               //command and cannot be set every time in the process
               //function as its updated in lib also.
               venc_ptr->voc.fgv_avg_rate_control =1;
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid 4gv-wb cop session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
         {
            if (encoder_rate <= 7)
            {
               //check if we need to issue a mode change event
               if((0 == encoder_rate) &&
                     (venc_ptr->voc.fgv_nw_avg_rate != fgv_nw_cop_avg_rate_table[encoder_rate]))
               {
                  //nb to wb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_WB;
                  venc_send_mode_notification_v2(instance_ptr);
               }
               else if((0 != encoder_rate) &&
                     (venc_ptr->voc.fgv_nw_avg_rate == fgv_nw_cop_avg_rate_table[0]))
               {
                  //wb to nb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NB;
                  venc_send_mode_notification_v2(instance_ptr);
               }

               venc_ptr->voc.fgv_nw_avg_rate = fgv_nw_cop_avg_rate_table[encoder_rate];
               //avg rate control shd be set only when we get a RATE
               //command and cannot be set every time in the process
               //function as its updated in lib also.
               venc_ptr->voc.fgv_avg_rate_control = 1;

            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid 4gv-nw cop session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            if (encoder_rate <= 7)
            {
               //check if we need to issue a mode change event
               if((0 == encoder_rate) &&
                     (venc_ptr->voc.fgv_nw_2k_avg_rate != fgv_nw_2k_cop_avg_rate_table[encoder_rate]))
               {
                  //nb to wb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_WB;
                  venc_send_mode_notification_v2(instance_ptr);
               }
               else if((0 != encoder_rate) &&
                     (venc_ptr->voc.fgv_nw_2k_avg_rate == fgv_nw_2k_cop_avg_rate_table[0]))
               {
                  //wb to nb transition
                  venc_ptr->voc.vocoder_op_mode = VSM_VOC_OPERATING_MODE_NB;
                  venc_send_mode_notification_v2(instance_ptr);
               }


               venc_ptr->voc.fgv_nw_2k_avg_rate = fgv_nw_2k_cop_avg_rate_table[encoder_rate];
               //avg rate control shd be set only when we get a RATE
               //command and cannot be set every time in the process
               //function as its updated in lib also.
               venc_ptr->voc.fgv_avg_rate_control = 1;
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid EVRC-NW-2K cop session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }

      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
      case VSM_MEDIA_TYPE_EAMR:
         {
            if (encoder_rate <= 7)
            {
               // Cache the rate always. Requirements are,
               // CS calls: If rate comes in STOP, apply immediately
               //           If rate comes in RUN, apply it after current
               //           pkt is delivered
               // PS calls: Apply immediately in any case
               venc_ptr->voc.prev_amr_nb_mode = encoder_rate;
               if ((VFR_NONE == venc_ptr->vfr_mode) ||
                     (FALSE == venc_ptr->process_data))
               {
                  venc_ptr->voc.amr_nb_mode = encoder_rate;
               }
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid amr-nb mode session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            if (encoder_rate <= 8)
            {
               // Cache the rate always. Requirements are,
               // CS calls: If rate comes in STOP, apply immediately
               //           If rate comes in RUN, apply it after current
               //           pkt is delivered
               // PS calls: Apply immediately in any case
               venc_ptr->voc.prev_amr_wb_mode = encoder_rate;
               if ((VFR_NONE == venc_ptr->vfr_mode) ||
                     (FALSE == venc_ptr->process_data))
               {
                  venc_ptr->voc.amr_wb_mode = encoder_rate;
               }
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Invalid amr-wb mode session(%lx)",
                     venc_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_ENC_SET_RATE: Invalid mediatype session(%lx)",
                  venc_ptr->session_num);
            result = ADSP_EUNSUPPORTED;
         }
   }
   elite_svc_send_ack(msg_ptr, result);
   return result;
}

static void voice_ecall_tx (venc_t *venc_ptr, int16_t *in_out_ptr)
{
   int16_t ecall_temp_buffer[960];   // worst case size: 20ms samples at 48KHz

   // Clear ecall_temp_buffer
   memset (&ecall_temp_buffer[0], 0, sizeof(ecall_temp_buffer));

   // If hpcm read enable, client wants to read data from fw.
   // Ecall interface API's are defined for 14-bit input/output
   // Input is in 16-bit format so convert input from 16-bit to 14 bit before giving it to client.
   if (TRUE == venc_ptr->proc.host_pcm_context.read_config.enable)
   {
      for( uint32_t i=0; i<venc_ptr->io.frame_samples; i++)
      {
         ecall_temp_buffer[i] = in_out_ptr[i] >> 2;   // convert input 16-bit to 14-bit
      }
   }

   // call host pcm processing
   int16_t *pHostPcmBuf[MAX_NUM_HOST_PCM_CHANNELS] = { (int16_t *) &ecall_temp_buffer[0], NULL, NULL, NULL};
   voice_host_pcm_process( &venc_ptr->proc.host_pcm_context, 1, pHostPcmBuf, venc_ptr->io.frame_samples, 0, TRUE);

   // If hpcm write enable, client has wwritten data to fw.
   // Ecall interface API's are defined for 14-bit input/output
   // Ecall output is in 14-bit format so convert output from 14-bit to 16 bit before processing in fw
   if (TRUE == venc_ptr->proc.host_pcm_context.write_config.enable)
   {
      for( uint32_t i=0; i<venc_ptr->io.frame_samples; i++)
      {
         in_out_ptr[i] = ecall_temp_buffer[i] << 2;   // convert output 14-bit to 16-bit
      }
   }

}
