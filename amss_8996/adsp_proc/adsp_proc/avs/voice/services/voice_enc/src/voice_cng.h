#ifndef CCOMFORTNOISE_H
#define CCOMFORTNOISE_H

/****************************************************************************
Copyright (c) 2007-2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/voice_cng.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
09/25/09   ss      Comfort Noise Generation class for Collapsed Voice Tx Chain.

========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */


#include "qurt_elite.h"
#include "VoiceCmnUtils.h"
#include "voice_resampler.h"

extern "C" {
    #include "lvb_api.h"
    #include "cng_api.h"
}

/* =======================================================================

                        DATA DECLARATIONS

========================================================================== */
/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */

class CComfortNoise;

/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */
/**
 * This is the implementation of an OpenMM media module, which represents
 * an example of a class derived from the RawAudioConversion base class.
 */
class CComfortNoise
{
   /*************************************************************************
   ** Constructors/destructor
   *************************************************************************/
   /*************************************************************************
   FUNCTION: CComfortNoise
   *************************************************************************/
   /**
   This function is the component constructor, meant to be called by the
   class factory of the component.

   @return None.
   */
   CComfortNoise ();

public:
    /************************************************************************
    FUNCTION: CComfortNoise
    ************************************************************************/
    /**
    This is the component constructor, meant to be called by the
    class factory of the component.

    @return None.
     */
    CComfortNoise (ADSPResult &result
                   /**< This output parameter is the location of an
                   indication of whether the component was initialized
                   successfully.*/
                   );

    /************************************************************************
    FUNCTION: ~CComfortNoise
    ************************************************************************/
    /**
    This is the destructor for the class.

    @return None.
     */
    virtual ~CComfortNoise ();

private:
    /************************************************************************
    FUNCTION: FreeLocalMemory
    ************************************************************************/
    /**
    This function is used to free the local memory.

    @return None.
     */
    ADSPResult FreeLocalMemory(void);

    /**************************************************************************
     ** IComfortNoiseGen Methods
     *************************************************************************/
public:

    /************************************************************************
    FUNCTION: GetMute
    ************************************************************************/
    /**
    This method requests the mute status of Comfort Noise Generator

    @return
      - ADSP_EBADPARAM - The pointer passed in was NULL.
      - ADSP_EOK - The cng mute state was returned
        successfully.
     */
    virtual int32_t CDECL GetMute(bool_t* pfMute
                                 /**< This output parameter is the
                                 Mute flag (TRUE==MUTE,FALSE==UNMUTE).*/
                                 );

    /************************************************************************
    FUNCTION: SetMute
    ************************************************************************/
    /**
    This method sets mute status for Comfort Noise Generator (cng).

    @return
      - ADSP_EOK - The mute flag was set successfully.
     */
    virtual int32_t CDECL SetMute (bool_t fMute
                                 /**< This input parameter is the
                                 Mute flag (TRUE==MUTE,FALSE==UNMUTE).*/
                                 );

    /************************************************************************
    FUNCTION: GetCNGOnRxTTYDetected
    ************************************************************************/
    /**
    This method requests the audio device CNGOnRxTTYDetected status.

    @return
      - ADSP_EBADPARAM - The pointer passed in was NULL.
      - ADSP_EOK - The CNGOnRxTTYDetected flag state was returned
        successfully.
     */
    virtual int32_t CDECL GetCNGOnRxTTYDetected (bool_t* pfCNGOnRxTTYDetected
                                             /**< This output parameter is the
                                             CNGOnRxTTYDetected flag (TRUE==
                                             CNGOnRxTTYDetected, FALSE==
                                             CNGOnRxTTY not Detected).*/
                                             );

    /************************************************************************
    FUNCTION: SetCNGOnRxTTYDetected
    ************************************************************************/
    /**
    This method sets audio device CNGOnRxTTYDetected.

    @return
      - ADSP_EOK - The CNGOnRxTTYDetected was set successfully.
     */
    virtual int32_t CDECL SetCNGOnRxTTYDetected (bool_t fCNGOnRxTTYDetected
                                             /**< This input parameter is
                                             the CNGOnRxTTYDetected flag
                                             (TRUE==CNGOnRxTTYDetected,
                                             FALSE==CNGOnRxTTY not
                                             Detected).*/
                                             );

    /************************************************************************
    FUNCTION: GetRxTTYDetected
    ************************************************************************/
    /**
    This method requests the audio device RxTTYDetected status.**

    @return
    * - ADSP_EOK - RxTTYDetected was got successfully.
     */
    virtual int32_t CDECL GetRxTTYDetected (bool_t* pfRxTTYDetected
                                        /**< pointer to RxTTYDetected flag
                                         (TRUE==RxTTYDetected,
                                         FALSE==RxTTY not Detected) */
                                        );

    /************************************************************************
    FUNCTION: SetRxTTYDetected
    ************************************************************************/
    /**
    This method sets the audio device RxTTYDetected status.******

    @return
    * - ADSP_EOK - RxTTYDetected was set successfully.
     */
    virtual int32_t CDECL SetRxTTYDetected (bool_t fRxTTYDetected
                                        /**< RxTTYDetected flag
                                         (TRUE==RxTTYDetected,
                                         FALSE==RxTTY not Detected) */
                                        );

public:
    /************************************************************************
    FUNCTION: EnhancedConvert
    ************************************************************************/
    /**
    This is a pure virtual method declared in CEnhancedConversion. It must
    be implemented by every module derived from CEnhancedConversion.
    It performs the core processing on the PCM data.

    @return
      - ADSP_EBADPARAM - The PCM format is not supported.
      - ADSP_EOK - The conversion was successful.
     */
   ADSPResult Process(int16_t *pOut,
                /**< This output parameter is a
                pointer to output Buffer.*/
                int16_t *pIn,
                /**< This input parameter is a
                pointer to input Buffer.*/
                uint32_t inSize,
                /**< This input parameter is the
                pointer to the size of the Buffer.*/
                uint32_t OutSize
                /* size of the output buffer */
                );

    /************************************************************************
    FUNCTION: EnhancedConvertInit
    ************************************************************************/
    /**
    This is a pure virtual method declared in CEnhancedConversion.
    Depending upon initialization requirements for a given media
    module, this function may be used. Any memory allocation
    requirements for the media module could also be initiated here.

    @return
      - ADSP_EBADPARAM - The PCM format is not supported.
      - ADSP_EOK - The conversion was successful.
     */
    virtual ADSPResult ProcessInit(void);

    /************************************************************************
    FUNCTION: EnhancedConvertEnd
    ************************************************************************/
    /**
    This is a pure virtual method declared in CEnhancedConversion.
    Depending upon termination requirements for a given media
    module, this function may be used. An example would be freeing
    allocated memory.

    @return
      - ADSP_EBADPARAM - The PCM format is not supported.
      - ADSP_EOK - The conversion was successful.
     */
    virtual ADSPResult ProcessEnd(void);

   /*************************************************************************
   FUNCTION: CommitFormatRawAudio
   *************************************************************************/
  /**
   This method is declared virtual in CRawAudioConversion. The
   deriving class may implement this method if it has special
   checks or actions needed at the point of committing to a media
   type.

   @return
     - ADSP_EOK - The initialization was successful.
     - OMM_E_MTUNSUPPORTED - Unsupported Media type.
   */
   ADSPResult CommitFormat(uint16_t nSamplingRate
                     /**< This input parameter identifies
                     the input band (sampling rate). */
                     );

   /*************************************************************************
   FUNCTION: SetParam
   *************************************************************************/
  /**
    This method is used to set calibration parameters for Comfort 
    Noise Generation

   @return
     - ADSP_EOK - The initialization was successful.
     - ADSP_EBADPARAM - Unsupported parameter.
     - ADSP_EFAILED - General failure
   */
   ADSPResult SetParam (int8_t *pParamsBuffer,
         uint32_t nParamId,
         uint16_t nParamSize);

   /*************************************************************************
   FUNCTION: GetParam
   *************************************************************************/
  /**
    This method is used to set calibration parameters for Comfort 
    Noise Generation

   @return
     - ADSP_EOK - The initialization was successful.
     - ADSP_EBADPARAM - Unsupported parameter.
     - ADSP_EFAILED - General failure
   */
   ADSPResult GetParam (int8_t *pParamsBuffer,
         uint32_t nParamId,
         int32_t nBufferSize, 
         uint16_t *nParamSize);

   /*************************************************************************
   FUNCTION: GetKpps
   *************************************************************************/
  /**
    This method is used to get Kpps

   @return
     - ADSP_EOK - The initialization was successful.
     - ADSP_EBADPARAM - Null pointers.
   */
   ADSPResult GetKpps(uint32_t* kpps_ptr, uint16_t sampling_rate);

   /**************************************************************************
     ** Variables
     *************************************************************************/
private:

    int32_t *m_pComfortNoise; ///< Pointer being passed into Comfort
                            /// Noise Generator (cng) routines.

    int32_t *m_pLVB; ///< Pointer being passed into LPC, VAD and
                   /// background noise estimation (lvb) routines.

    int16_t *m_anBnLpc; ///< Background noise Linear Predictive Coding (LPC).

    int16_t m_nBgNoiseScaleValue; ///< Background noise scale value.

    int16_t *m_anInput; ///< Local buffer for input speech samples.

    int16_t m_nNumSamples; ///<160 if narrow band, 320 if wide band.

    int16_t m_nLpcOrder; ///< This value is <10 if narrow band, 16 if wide
                       /// band.

    int16_t m_nNrOccuredVocoder; ///< Updates inside cng, depending on cng
                               /// Enable and Tone Mute Enable.

    int16_t m_nStoredSamples; ///< Local buffer  managing variable to count
                            /// stored samples.

    int16_t m_nBufIndex; ///< Local buffer  managing variable to keep track
                       /// of local pointer.

    bool_t m_fIsWideBand; ///< False if narrow band, true if wide band.

    bool_t m_fMute; ///< Tx mute, true for enable, false for
                                     /// disable.

    bool_t m_fPrevMute; ///< Previous Tx mute,
                                         /// true for enable, false for
                                     /// disable.

    bool_t m_fTxToneMuteEnable; ///< False if Tone is present, true if
                                 /// Mute is present.

    bool_t m_fRxTTYDetected; ///< The flag which indicates TTY has been
                              /// detected on the Rx path.

    bool_t m_fCNGOnRxTTYDetected;  ///< The flag used to control whether
                                    /// to do CNG or not when TTY is detected
                                    /// on the Rx path.
                                    ///< If this flag is enabled, TTY
                                    /// communication will become half-duplex.

   void *m_pDownSampler;  // Channel mem pointer for down sampler instance

   void *m_pUpSampler;  // Channel mem pointer for up sampler instance

   bool_t m_fResamplingRequired;      ///< False by default, true when a the media format is other than 8KHz or 16KHz
   uint16_t m_nSamplingRate;          ///< Prevailing sampling rate (input rate = output rate for CNG)
   uint16_t m_nProcessNumSamples;    ///< Number of samples that CNG/LVB will effectively be done on.
   int16_t* m_anProcessBuf;
   bool_t m_nProcessEnable;

   voice_resampler_config_struct_t m_DownSampConfig;  //Instance of voice resampler for support over 16KHz
   voice_resampler_config_struct_t m_UpSampConfig;  //Instance of voice resampler for support over 16KHz
};

/* -----------------------------------------------------------------------
** Global Object Definitions
** ----------------------------------------------------------------------- */

/* =======================================================================
**                          Macro Definitions
** ======================================================================= */

/* =======================================================================
**                          Function Declarations
** ======================================================================= */


#endif /* CTUNINGFILTER_H */
