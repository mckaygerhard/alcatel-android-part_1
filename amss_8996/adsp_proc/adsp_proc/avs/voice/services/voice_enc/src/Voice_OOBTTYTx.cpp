
/****************************************************************************
Copyright (c) 2013-2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/Voice_OOBTTYTx.cpp#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
03/14/2013     rojha      Created
========================================================================== */
#include "Voice_OOBTTYTx.h"
#include "VoiceLoggingUtils.h"
#include "VoiceCmnUtils.h"

static void voice_oobtty_tx_free_local_memory(oobtty_tx_struct_t* obtty_tx_struct_ptr);
static void voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_t* obtty_tx_struct_ptr);

extern "C" {

    #include "lte_tty_api.h"
}

#define VOICE_OOBTTYTX_NB_KPPS (2332)
#define VOICE_OOBTTYTX_WB_KPPS (2832)
#define VOICE_OOBTTYTX_SWB_KPPS (VOICE_OOBTTYTX_NB_KPPS + VOICE_RESAMPLER_KPPS_32K_TO_8K)
#define VOICE_OOBTTYTX_FB_KPPS (3332)
static const vcmn_sampling_kpps_t VOICE_OOBTTYTX_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_OOBTTYTX_NB_KPPS },
                                                                       {VOICE_WB_SAMPLING_RATE, VOICE_OOBTTYTX_WB_KPPS },
                                                                       {VOICE_SWB_SAMPLING_RATE, VOICE_OOBTTYTX_SWB_KPPS },
                                                                       {VOICE_FB_SAMPLING_RATE, VOICE_OOBTTYTX_FB_KPPS },
                                                                       {VOICE_INVALID_SAMPLING_RATE, 0}};

ADSPResult voice_oobtty_tx_mem_alloc(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr = qurt_elite_memory_malloc(ltetty_tx_get_struct_size(), QURT_ELITE_HEAP_DEFAULT);

   if (NULL == oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: Tx oobtty init failed, out of memory");
      voice_oobtty_tx_free_local_memory(oobtty_tx_struct_ptr);
      return ADSP_ENOMEMORY;
   }

   return result;

}

/* initialize OOB TTY Tx module, allocate memory */
void voice_oobtty_tx_init(oobtty_tx_struct_t* oobtty_tx_struct_ptr, uint32_t session_id)
{

   oobtty_tx_struct_ptr->session_id                       = session_id;

   init_ltetty_tx(oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr);

   voice_oobtty_tx_resampler_init(oobtty_tx_struct_ptr);

   return;
}

/* process 20 ms buffer, assume in place */
ADSPResult voice_process_oobtty_tx(oobtty_tx_struct_t* oobtty_tx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples, int16_t rxActivity)
{
   ADSPResult result = ADSP_EOK;

   if( oobtty_tx_struct_ptr->enable )
   {

      int16_t det_buf[VOICE_FRAME_SIZE_FB],nb_samples;

      nb_samples = (samples*VOICE_NB_SAMPLING_RATE)/oobtty_tx_struct_ptr->sampling_rate;

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Tx OOBTTY processing samples(%d)",samples);

      // boost the input by 12 dB to be consistent with CDMA TTY case
      // this level boosting is intended for detection only.
      for (int32_t j=0; j < samples; j++)
      {
         det_buf[j] = in_ptr[j];
      }

      // downsample if not Narrowband operation
      if((VOICE_NB_SAMPLING_RATE != oobtty_tx_struct_ptr->sampling_rate) && (NULL != oobtty_tx_struct_ptr->down_samp_mem_ptr))
      {
         int32_t resample_result;
         resample_result = voice_resampler_process(
                             &(oobtty_tx_struct_ptr->down_samp_config),
                             oobtty_tx_struct_ptr->down_samp_mem_ptr,
                             (int8 *)det_buf,
                             (uint32)samples,
                             (int8 *)det_buf,
                             VOICE_FRAME_SIZE_NB
                             );
         if(VOICE_RESAMPLE_SUCCESS != resample_result)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Tx OOB TTY down sampler process failed");
            return ADSP_EFAILED;
         }
      }

      assert( nb_samples == 160);  // should be 20 ms worth of data (considering NB and downsampled WB case)

      {

         int16_t txChar, baudotCode;
         // do TTY detection
         txChar = lte_tty_tx_processing(det_buf, det_buf, (uint16_t)nb_samples, &baudotCode, rxActivity, (oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr));
         oobtty_tx_struct_ptr->txChar = txChar;
         if( txChar > 0)
         {

#if defined(__qdsp6__) && !defined(SIM)

            // log tty character if available
            int8_t *bufptr[4] = { (int8_t *) &txChar, NULL, NULL, NULL };
            voice_log_buffer( bufptr,
                  VOICE_LOG_CHAN_VSM_TX_OOBTTY_CHAR,
                  (((oobtty_tx_struct_ptr->sampling_rate == 16000) << 3) | oobtty_tx_struct_ptr->session_id),
                  qurt_elite_timer_get_time(),
                  VOICE_LOG_DATA_FORMAT_PCM_MONO,
                  (oobtty_tx_struct_ptr->sampling_rate),
                  sizeof(int8_t),
                  NULL);
#endif

            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx OOB TTY detected ttyChar(%d)", txChar);
         }
         /* Check if we need to bypass samples, otherwise mute */
         if (lte_tty_get_tx_bypass_flag(oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr) != TRUE)
         {
            memset(in_ptr, 0, sizeof(int16_t)*samples);
         }
      }
   }


   return result;
}


ADSPResult voice_oobtty_tx_resampler_init(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
   //freeup existing local memory for resampler, if any
   voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_ptr);
   // Resampler config for OOB TTY Tx
   if(VOICE_NB_SAMPLING_RATE != oobtty_tx_struct_ptr->sampling_rate)
   {
     uint32_t out_frame_samples,in_frame_samples, sampling_rate;
     sampling_rate=(uint32_t)oobtty_tx_struct_ptr->sampling_rate;
     in_frame_samples= (VOICE_FRAME_SIZE_WB*oobtty_tx_struct_ptr->sampling_rate)/VOICE_WB_SAMPLING_RATE;              //((oobtty_tx_struct_ptr->sampling_rate == VOICE_WB_SAMPLING_RATE) ? VOICE_FRAME_SIZE_WB : VOICE_FRAME_SIZE_FB );
     voice_resampler_set_config(&(oobtty_tx_struct_ptr->down_samp_config),sampling_rate,VOICE_NB_SAMPLING_RATE,NO_OF_BITS_PER_SAMPLE,in_frame_samples,&out_frame_samples);
     oobtty_tx_struct_ptr->down_samp_mem_ptr = qurt_elite_memory_malloc( (oobtty_tx_struct_ptr->down_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     if (NULL == oobtty_tx_struct_ptr->down_samp_mem_ptr)
     {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_oobtty_tx_resampler_init: Tx OOB TTY resampler init failed, out of memory");
        voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_ptr);
        return ADSP_ENOMEMORY;
     }

     // Initialize downsampler needed for OOB TTY Tx
     int32_t resample_result;
     resample_result = voice_resampler_channel_init(
                         &(oobtty_tx_struct_ptr->down_samp_config),
                         oobtty_tx_struct_ptr->down_samp_mem_ptr,
                         (oobtty_tx_struct_ptr->down_samp_config).total_channel_mem_size
                         );
     if(VOICE_RESAMPLE_SUCCESS != resample_result)
     {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Tx OOB TTY up sampler init failed");
        voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_ptr);
        return ADSP_EFAILED;
     }
   }
   return ADSP_EOK;
}


uint8_t voice_oobtty_tx_char_detected(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
    return (oobtty_tx_struct_ptr->txChar > 0);
}
int16_t voice_oobtty_tx_get_char(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
    return oobtty_tx_struct_ptr->txChar;
}
uint8_t voice_oobtty_tx_get_enable(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
    return oobtty_tx_struct_ptr->enable;
}
/* destroy LTE TTY module, free memory */
ADSPResult voice_oobtty_tx_end(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   voice_oobtty_tx_free_local_memory(oobtty_tx_struct_ptr);
   voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_ptr);

   return result;
}

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_oobtty_tx_set_config(oobtty_tx_struct_t* oobtty_tx_struct_ptr, uint8_t enable, uint32_t samp_rate)
{
   ADSPResult result = ADSP_EOK;

   oobtty_tx_struct_ptr->enable = enable;
   oobtty_tx_struct_ptr->sampling_rate = samp_rate;
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: OOB TTY Tx setting enable flag(%d) and sampling rate(%d)",enable, (int)samp_rate);

   return result;
}


static void voice_oobtty_tx_free_local_memory(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
{
   // Destroy LTE structure
   if(NULL != oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr)
   {
      qurt_elite_memory_free(oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr);
      oobtty_tx_struct_ptr->oobtty_tx_struct_instance_ptr = NULL;
   }
}

static void voice_oobtty_tx_free_resamp_memory(oobtty_tx_struct_t* oobtty_tx_struct_ptr)
   {
   // Destroy down sampler memory
   if(NULL != oobtty_tx_struct_ptr->down_samp_mem_ptr)
   {
      qurt_elite_memory_free(oobtty_tx_struct_ptr->down_samp_mem_ptr );
      oobtty_tx_struct_ptr->down_samp_mem_ptr = NULL;
   }
   return;
}

ADSPResult voice_oobtty_tx_get_kpps(oobtty_tx_struct_t* oobtty_tx_struct_ptr,uint32_t* kpps_ptr,uint8_t enable, uint16_t sampling_rate)
   {
   ADSPResult result = ADSP_EOK;

   if ((NULL!= oobtty_tx_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(enable)
      {
         result = vcmn_find_kpps_table(VOICE_OOBTTYTX_SAMPLING_KPPS_TABLE,sampling_rate,kpps_ptr);
   }
}
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}
