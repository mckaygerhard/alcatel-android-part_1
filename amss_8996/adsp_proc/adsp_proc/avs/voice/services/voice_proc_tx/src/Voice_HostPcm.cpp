/*========================================================================

*//** @file Voice_HostPcm.cpp

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header:

when       who     what, where, why
--------   ---     -------------------------------------------------------
4/5/2011    DP     created file

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "Voice_HostPcm.h"
#include "Elite.h"
#include "EliteMsg_Util.h"
#include "adsp_vcmn_api.h"
#include "Voice_SampleSlip.h"


/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/
#define ROUNDTO8(x) ((((uint32_t)(x) + 7) >> 3) << 3);
#define VOICE_HOST_PCM_MARGIN_KPPS         500    // 0.5 mpps margin at 8k for copies and other overhead
                                                  //this is currently empirical and may need to be revisited

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
/* -----------------------------------------------------------------------
** Function prototypes
** ----------------------------------------------------------------------- */

/* =======================================================================
**                          Function Definitions
** ======================================================================= */


static ADSPResult voice_reply_host_pcm_data_evt( voice_host_pcm_context_t *context_ptr, voice_evt_notify_host_pcm_buf_v2_t *pNotifyEvt)
{
   ADSPResult result = ADSP_EOK;

   result = elite_apr_if_alloc_send_event( context_ptr->apr_handle,
      context_ptr->self_addr,
      context_ptr->self_port,
      context_ptr->client_addr,
      context_ptr->client_port,
      0,   /* token */
      VOICE_EVT_HOST_BUF_AVAILABLE_V2,
      pNotifyEvt,
      sizeof(voice_evt_notify_host_pcm_buf_v2_t));

   return result;
}


ADSPResult voice_host_pcm_init(voice_host_pcm_context_t *context_ptr, uint16 sampling_rate, int32_t in_num_channels, uint8_t ss_enable)
{

   ADSPResult result = ADSP_EOK;
   uint8_t index;

   context_ptr->sampling_rate            = sampling_rate;
   context_ptr->ss_enable                = ss_enable;        // save sample slip enable flag
   context_ptr->read_config.num_channels = in_num_channels;
   context_ptr->write_config.num_channels = in_num_channels;
   context_ptr->sampling_rate_factor     = sampling_rate/VOICE_NB_SAMPLING_RATE;
   context_ptr->read_config.host_sampling_rate_factor=context_ptr->read_config.host_sampling_rate/VOICE_NB_SAMPLING_RATE;
   context_ptr->write_config.host_sampling_rate_factor=context_ptr->write_config.host_sampling_rate/VOICE_NB_SAMPLING_RATE;

   // init circ buffers for read/write dataQ.  Overloading usage of circ buffer to push voice_host_pcm_buffer_element_t
   // structures, each of which is of size VOICE_HOST_PCM_CIRC_BUFFER_SIZE, in terms of 16bit sample units

   voice_circbuf_init(&(context_ptr->read_config.circ_struct_client_buffers), (int8_t*)(context_ptr->read_config.elements),
      (int32_t)VOICE_HOST_PCM_CIRC_BUFFER_SIZE /* number of "samples" */,MONO_VOICE,
      (int32_t)16 /*bitperchannel*/);

   voice_circbuf_init(&(context_ptr->write_config.circ_struct_client_buffers), (int8_t*)(context_ptr->write_config.elements),
      (int32_t)VOICE_HOST_PCM_CIRC_BUFFER_SIZE /* number of "samples" */,MONO_VOICE,
      (int32_t)16 /*bitperchannel*/);

   for (int32_t j = 0; j < MAX_NUM_HOST_PCM_CHANNELS; j++)
   {
       context_ptr->read_config.circ_struct_local_buf.buf_ptr[j] = NULL;
       context_ptr->read_config.hpcm_resamp_channel_mem_ptr[j]=NULL;
       context_ptr->write_config.circ_struct_local_buf.buf_ptr[j] = NULL;
       context_ptr->write_config.hpcm_resamp_channel_mem_ptr[j]=NULL;
       context_ptr->scratch_resamp_ptr[j] = NULL;
   }

  // voice_multi_ch_circ_buf_init(&(context_ptr->read_config.circ_struct_local_buf),  in_num_channels, (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor)), (int32_t) 16);
  // voice_multi_ch_circ_buf_init(&(context_ptr->write_config.circ_struct_local_buf), in_num_channels, (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor)), (int32_t) 16);

   context_ptr->read_config.circ_struct_local_buf.num_channels = in_num_channels;
   context_ptr->write_config.circ_struct_local_buf.num_channels = in_num_channels;

   // config sample slip if enabled (do before the enable calls)
   if( TRUE == ss_enable)
   {

      uint32_t nSsSize = 0, nTotalSize = 0;

      // size for one sample slip module
      voice_ss_get_size(&nSsSize);
      nSsSize = ROUNDTO8(nSsSize);

      // size for one direction, read or write
      nTotalSize = MAX_NUM_HOST_PCM_CHANNELS * nSsSize;

      context_ptr->read_config.ss_memory = (int8_t *) qurt_elite_memory_malloc(nTotalSize, QURT_ELITE_HEAP_DEFAULT);
      if( NULL == context_ptr->read_config.ss_memory)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
         return ADSP_ENOMEMORY;
      }
      context_ptr->write_config.ss_memory = (int8_t *) qurt_elite_memory_malloc(nTotalSize, QURT_ELITE_HEAP_DEFAULT);
      if( NULL == context_ptr->write_config.ss_memory)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
         return ADSP_ENOMEMORY;
      }

      int8_t *read_ss_ptr  = context_ptr->read_config.ss_memory;
      int8_t *write_ss_ptr = context_ptr->write_config.ss_memory;

      for( index = 0; index < MAX_NUM_HOST_PCM_CHANNELS; index++)
      {
         // TODO: only need 1 array of sample slip structs that operate on each channel, at input to host pcm process.  No need for one in each direction
         // SampleSlip mem alloc
         voice_ss_set_mem(&(context_ptr->read_config.ss_struct[index]),read_ss_ptr,nSsSize);
         read_ss_ptr += nSsSize;
         read_ss_ptr  = (int8_t*)ROUNDTO8(read_ss_ptr);

         // SampleSlip mem alloc
         voice_ss_set_mem(&(context_ptr->write_config.ss_struct[index]),write_ss_ptr,nSsSize);
         write_ss_ptr += nSsSize;
         write_ss_ptr  = (int8_t*)ROUNDTO8(write_ss_ptr);
      }
   }


   // sampling rate and num channels and num samples passed in dummy param
   if(ADSP_FAILED(result = voice_host_pcm_set_enable( context_ptr, FALSE, VOICE_HOST_PCM_READ, 1,  sampling_rate, context_ptr->sampling_rate, 80)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Read HPCM enable failed, result %d", (int)result);
   }
   if(ADSP_FAILED(result = voice_host_pcm_set_enable( context_ptr, FALSE, VOICE_HOST_PCM_WRITE, 1, sampling_rate, context_ptr->sampling_rate, 80)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Write HPCM enable failed, result %d", (int)result);
   }

   return result;
}

// Reinitialize the host pcm service (without affecting enable/disable state)
// Note, this function gets called even in default pcm_init, hence dynamic memalloc are being done in this function
ADSPResult voice_host_pcm_reinit(voice_host_pcm_context_t *context_ptr, uint16_t num_channels, uint16_t sampling_rate,  uint16_t num_samples)
{
   ADSPResult result = ADSP_EOK;
   uint8_t index;

   // save topology sampling rate
   context_ptr->sampling_rate = sampling_rate;
   context_ptr->sampling_rate_factor = sampling_rate/VOICE_NB_SAMPLING_RATE;
   context_ptr->read_config.host_sampling_rate_factor=context_ptr->read_config.host_sampling_rate/VOICE_NB_SAMPLING_RATE;
   context_ptr->write_config.host_sampling_rate_factor=context_ptr->write_config.host_sampling_rate/VOICE_NB_SAMPLING_RATE;

   MSG_6(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM reinit context(%p) numChannels(%d) num_samples(%d) topologySamplingRate(%d) readEnable(%d) writeEnable(%d)", context_ptr, num_channels, num_samples, sampling_rate,
      context_ptr->read_config.enable, context_ptr->write_config.enable );
   context_ptr->read_config.num_channels = num_channels;
   context_ptr->write_config.num_channels = num_channels;

   if(TRUE == context_ptr->read_config.enable )
   {
     for (int32_t j = 0; j < MAX_NUM_HOST_PCM_CHANNELS; j++)
     {
        if( NULL != context_ptr->read_config.circ_struct_local_buf.buf_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->read_config.circ_struct_local_buf.buf_ptr[j]);
            context_ptr->read_config.circ_struct_local_buf.buf_ptr[j] = NULL;
        }
        if( NULL != context_ptr->scratch_resamp_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->scratch_resamp_ptr[j]);
            context_ptr->scratch_resamp_ptr[j] = NULL;
        }
        if( NULL != context_ptr->read_config.hpcm_resamp_channel_mem_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->read_config.hpcm_resamp_channel_mem_ptr[j]);
            context_ptr->read_config.hpcm_resamp_channel_mem_ptr[j] = NULL;
        }
     }
     // init multi channel circular buffers
     for (int32_t j = 0; j < num_channels; j++)
     {
        context_ptr->read_config.circ_struct_local_buf.buf_ptr[j] = (int8_t *) qurt_elite_memory_malloc((NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor))<<1, QURT_ELITE_HEAP_DEFAULT);
        if( NULL == context_ptr->read_config.circ_struct_local_buf.buf_ptr[j])
        {
           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
           return ADSP_ENOMEMORY;
        }
        context_ptr->scratch_resamp_ptr[j] = (int16_t *) qurt_elite_memory_malloc((NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor))<<1, QURT_ELITE_HEAP_DEFAULT);
        if( NULL == context_ptr->scratch_resamp_ptr[j])
        {
           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
           return ADSP_ENOMEMORY;
        }
     }
     voice_multi_ch_circ_buf_init(&(context_ptr->read_config.circ_struct_local_buf), num_channels,  (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor)), (int32_t) 16);
   }
   if(TRUE == context_ptr->write_config.enable)
   {
     for (int32_t j = 0; j < MAX_NUM_HOST_PCM_CHANNELS; j++)
     {
        if( NULL != context_ptr->write_config.circ_struct_local_buf.buf_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->write_config.circ_struct_local_buf.buf_ptr[j]);
            context_ptr->write_config.circ_struct_local_buf.buf_ptr[j] = NULL;
        }
        if( NULL != context_ptr->scratch_resamp_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->scratch_resamp_ptr[j]);
            context_ptr->scratch_resamp_ptr[j] = NULL;
        }
        if( NULL != context_ptr->write_config.hpcm_resamp_channel_mem_ptr[j])
        {
            qurt_elite_memory_free(context_ptr->write_config.hpcm_resamp_channel_mem_ptr[j]);
            context_ptr->write_config.hpcm_resamp_channel_mem_ptr[j] = NULL;
        }
     }
     for (int32_t j = 0; j < num_channels; j++)
     {
        context_ptr->write_config.circ_struct_local_buf.buf_ptr[j] = (int8_t *)qurt_elite_memory_malloc((NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor))<<1, QURT_ELITE_HEAP_DEFAULT);
        if( NULL == context_ptr->write_config.circ_struct_local_buf.buf_ptr[j])
        {
           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
           return ADSP_ENOMEMORY;
        }
        context_ptr->scratch_resamp_ptr[j] = (int16_t *) qurt_elite_memory_malloc((NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor))<<1, QURT_ELITE_HEAP_DEFAULT);
        if( NULL == context_ptr->scratch_resamp_ptr[j])
        {
           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM Out of Memory!!");
           return ADSP_ENOMEMORY;
        }
      }
      voice_multi_ch_circ_buf_init(&(context_ptr->write_config.circ_struct_local_buf), num_channels, (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor)), (int32_t) 16);
   }
   // init the ss structures (assumes memory for ss_struct has been setup
   {
      //call sample slip process for 10msec frame size
      uint16_t frame_size_10msec = VOICE_FRAME_SIZE_NB_10MS*context_ptr->sampling_rate_factor;
      if( context_ptr->ss_enable)
      {
         for( index = 0; index < num_channels; index++)
         {
            voice_sample_slip_init( &context_ptr->read_config.ss_struct[index], frame_size_10msec,VOICE_HPCM_SS_MULTIFRAME);
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: read HPCM ss init (%#lx)", context_ptr->tap_point);
         }
      }

      // init the ss structures
      if( context_ptr->ss_enable)
      {
         for( index = 0; index < num_channels; index++)
         {
            voice_sample_slip_init( &context_ptr->write_config.ss_struct[index], frame_size_10msec,VOICE_HPCM_SS_MULTIFRAME);
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: write HPCM ss init (%#lx)", context_ptr->tap_point);
         }
      }
   }


   if( TRUE == context_ptr->read_config.enable )
   {

      /* read direction: src rate is sampling_rate and destination rate is context_ptr->read_config.host_sampling_rate */

      if(context_ptr->read_config.host_sampling_rate != sampling_rate)
      {
         uint32 num_in_samples,num_out_samples;
         num_in_samples=NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor);
         result = voice_resampler_set_config(
            &(context_ptr->read_config.hpcm_resamp_config),
            sampling_rate,
            context_ptr->read_config.host_sampling_rate,
            NO_OF_BITS_PER_SAMPLE,
            num_in_samples,
            &num_out_samples
            );
         if (VOICE_RESAMPLE_FAILURE == result)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to set config for resampler in voice_host_pcm_reinit HPCM-read ");
            return ADSP_EFAILED;
         }
         for( index = 0; index < num_channels; index++)
         {
            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM read resampler init tap point(%#lx) srcRate(%d) clientRate(%d)\n",
               context_ptr->tap_point, sampling_rate, context_ptr->read_config.host_sampling_rate);
            context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index] = qurt_elite_memory_malloc((context_ptr->read_config.hpcm_resamp_config).total_channel_mem_size,
                                                                                                            QURT_ELITE_HEAP_DEFAULT);
            if (NULL == context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index])
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed allocating channel mem for Host Pcm Read Resampler !! \n");
               return ADSP_ENOMEMORY;
            }
           // configure for mono
            {
               result =voice_resampler_channel_init(
                  &(context_ptr->read_config.hpcm_resamp_config),
                  context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index],
                  (context_ptr->read_config.hpcm_resamp_config).total_channel_mem_size
                  );
               if (VOICE_RESAMPLE_FAILURE == result)
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to channel init for resampler in voice_host_pcm_reinit HPCM-read ");
                  voice_host_pcm_end(context_ptr);
                  return ADSP_EFAILED;
               }
            }
         }
      }
   }


   /* handle the write direction  */
   if( TRUE == context_ptr->write_config.enable)
   {

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM writ reinit init circBufElements(%ld)",
         context_ptr->write_config.circ_struct_client_buffers.unread_samples / VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);

      // mark any queued write buffers as invalid, so they are not consumed (client will get notified). Reinit will happen during handover/device switch
      // this isn't necessary for read direction. In read direction, any queued buffers may be used if they pass the sanity checks on size, etc...if not client will get notified that
      // they weren't used.

      uint8_t index = 0;
      uint32_t available_read_samples = 0;
      voice_circbuf_read_request(&(context_ptr->write_config.circ_struct_client_buffers), VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, &available_read_samples);

      while (available_read_samples >= VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE)
      {
         voice_host_pcm_buffer_element_t   host_pcm_write_element;

         if( CIRCBUF_SUCCESS == voice_circbuf_read( &context_ptr->write_config.circ_struct_client_buffers, (int8_t*) &host_pcm_write_element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, sizeof(voice_host_pcm_buffer_element_t) ))
         {
            host_pcm_write_element.valid = BUF_INVALID;
            voice_circbuf_write( &context_ptr->write_config.circ_struct_client_buffers, (int8_t*) &host_pcm_write_element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);
         }
         available_read_samples -= VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE;
      }

      /* write direction: src rate is context_ptr->write_config.host_sampling_rate and destination rate is sampling_rate */
      if(context_ptr->write_config.host_sampling_rate != context_ptr->sampling_rate)
      {
         uint32 num_in_samples,num_out_samples;
         num_in_samples=(context_ptr->write_config.host_sampling_rate_factor)*NB_VOICE_HPCM_FRAME_SAMPLES;
         result = voice_resampler_set_config(
            &(context_ptr->write_config.hpcm_resamp_config),
            context_ptr->write_config.host_sampling_rate,
            sampling_rate,
            NO_OF_BITS_PER_SAMPLE,
            num_in_samples,
            &num_out_samples
            );
         if (VOICE_RESAMPLE_FAILURE == result)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to set config for resampler in voice_host_pcm_reinit HPCM-write ");
            return ADSP_EFAILED;
         }
         for( index = 0; index < num_channels; index++)
         {
            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM read resampler init tap point(%#lx) clientRate(%d) dstRate(%d)\n",
               context_ptr->tap_point, context_ptr->write_config.host_sampling_rate, sampling_rate);
            context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index] = qurt_elite_memory_malloc(
                                                                            (context_ptr->write_config.hpcm_resamp_config).total_channel_mem_size,
                                                                            QURT_ELITE_HEAP_DEFAULT);
            if(NULL == context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index])
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed allocating channel mem for Host Pcm Write Resampler !! \n");
               return (ADSP_ENOMEMORY);
            }
            {
               result =voice_resampler_channel_init(
                  &(context_ptr->write_config.hpcm_resamp_config),
                  context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index],
                  (context_ptr->write_config.hpcm_resamp_config).total_channel_mem_size
                  );
               if (VOICE_RESAMPLE_FAILURE == result)
               {
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to channel init for resampler in voice_host_pcm_reinit HPCM-write ");
                  voice_host_pcm_end(context_ptr);
                  return ADSP_EFAILED;
               }
            }
         }
      }
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host Pcm Reinit done");

   return result;
}

// processes incoming APR buffers sent.
ADSPResult voice_host_pcm_buffer_rd_wr(voice_host_pcm_context_t *context_ptr, void *pMsg, uint16_t write_direction_valid)
{
   ADSPResult result = ADSP_EOK;
   ADSPResult read_result = ADSP_EFAILED;
   ADSPResult write_result = ADSP_EFAILED;
   elite_msg_any_t *pEliteMsg = (elite_msg_any_t *) pMsg;

   voice_evt_push_host_pcm_buf_v2_t    *pHostPcmData = (voice_evt_push_host_pcm_buf_v2_t *) elite_apr_if_get_payload_ptr( (elite_apr_packet_t *) pEliteMsg->pPayload);

   /*
   //dbg:   MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM buffer ready to Q, tapPoint(%x) mask(%d) sampling_rate(%d) hostReadEn(%d) hosttWriteEn(%d)",pHostPcmData->tap_point, pHostPcmData->mask, \
   //dbg:                                     pHostPcmData->sampling_rate, context_ptr->read_config.enable, context_ptr->write_config.enable);
   */

   /* flags to check is being attempted */

   uint8_t read_flag  = (pHostPcmData->mask & VOICE_HOST_PCM_READ);
   uint8_t write_flag = (pHostPcmData->mask & VOICE_HOST_PCM_WRITE);

   /* try to push the message to the proper host pcm Q
   Do this only if either direction is enabled and mask is non zero.
   If neither direction is enabled, return buffer with corresponding errors set immediately.
   If neither mask is set, do the same, but don't set any error bits
   */

   if((read_flag || write_flag) && (context_ptr->read_config.enable || context_ptr->write_config.enable))
   {
      do
      {
         if( read_flag)
         {
            //TODO: Validate that the buffer is aligned to 32 bytes and has a size that is a multiple of 32 bytes
            uint32_t available_read_samples = 0;
            if (CIRCBUF_SUCCESS != voice_circbuf_write_request(&(context_ptr->read_config.circ_struct_client_buffers), VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, &available_read_samples))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM overflow error - client pushing too much. available samples for writing (%#lx), required (%#x)", available_read_samples,VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);
               read_result = ADSP_EFAILED; // skip rest of the code and do error handling
               //break;
            }
            else
            {
               read_result = ADSP_EOK;
            }
         } // end of if( read_flag)

         if( write_flag)
         {
            //TODO: Validate that the buffer is aligned to 4 bytes and has a size that is a multiple of 4 bytes
            uint32_t available_write_samples = 0;
            if (CIRCBUF_SUCCESS != voice_circbuf_write_request(&(context_ptr->write_config.circ_struct_client_buffers), VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, &available_write_samples))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Host PCM overflow error - client pushing too much. available samples for writing (%#lx), required (%#x)", available_write_samples,VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);
               write_result = ADSP_EFAILED;
               //break; // skip rest of the code and do error handling
            }
            else
            {
               write_result = ADSP_EOK;
            }

         } // end of if( write_flag)

         /* note read and write operate on the same tap point and sampling rate */
         if( read_flag && (ADSP_EOK == read_result))
         {
            /* push the buffer onto the internal read q */
            voice_host_pcm_buffer_element_t element;

            element.tap_point     = pHostPcmData->tap_point;
            element.buff_addr_lsw = pHostPcmData->rd_buff_addr_lsw;
            element.buff_addr_msw = pHostPcmData->rd_buff_addr_msw;
            element.buff_size     = pHostPcmData->rd_buff_size;
            element.sampling_rate = pHostPcmData->sampling_rate;
            element.num_chan      = 0;    /* dummy data */
            element.valid         = BUF_VALID;

            /* push one element onto the read direction circ buffer queue.  Since the circ buffer write is in terms of number of 16bit samples, give the
            * element size in these units */
            if( CIRCBUF_SUCCESS != voice_circbuf_write(&(context_ptr->read_config.circ_struct_client_buffers), (int8_t*)&element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE))
            {
               MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Host PCM read buffer Queuing error");
               read_result = ADSP_EFAILED;  //TODO: need to notify client buffer is free since not going to be processed.  mark buff_size = 0
               //break; // skip rest of the code and do error handling
            }
            else
            {
               MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM read buffer queued: tapPoint(0x%lx) size(%d) addr(0x%lx) q count (%ld)",pHostPcmData->tap_point,
                  pHostPcmData->rd_buff_size, pHostPcmData->rd_buff_addr_lsw,
                  context_ptr->read_config.circ_struct_client_buffers.unread_samples / VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);
            }
         } // if( read_flag)

         if( write_flag && (ADSP_EOK == write_result))
         {
            /* push the buffer onto the internal write q */
            voice_host_pcm_buffer_element_t element;

            element.tap_point     = pHostPcmData->tap_point;
            element.buff_addr_lsw = pHostPcmData->wr_buff_addr_lsw;
            element.buff_addr_msw = pHostPcmData->wr_buff_addr_msw;
            element.buff_size     = pHostPcmData->wr_buff_size;
            element.sampling_rate = pHostPcmData->sampling_rate;
            element.num_chan      = pHostPcmData->wr_num_chan;
            element.valid         = write_direction_valid;

            /* push one element onto the write direction circ buffer queue.  Since the circ buffer write is in terms of number of 16bit samples, give the
            * element size in these units */
            if( CIRCBUF_SUCCESS != voice_circbuf_write(&(context_ptr->write_config.circ_struct_client_buffers), (int8_t*)&element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE))
            {
               MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Host PCM write buffer Queuing error");
               write_result = ADSP_EFAILED;
            }
            else
            {
               MSG_6(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write buffer queued: tapPoint(0x%lx) size(%d) numChan(%d) addr(0x%lx) valid(%d) q count (%ld)",pHostPcmData->tap_point,
                  pHostPcmData->wr_buff_size, pHostPcmData->wr_num_chan, pHostPcmData->wr_buff_addr_lsw, write_direction_valid,
                  context_ptr->write_config.circ_struct_client_buffers.unread_samples / VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);
            }
         } //if( write_flag)
      } while(0);
   }

   //Handle error cases - write/read failing, or client sent a buffer without any mask
   //if one path fails, but another succeeds, don't return here, return only in process
   if (ADSP_FAILED(write_result) && ADSP_FAILED(read_result))
   {
      // In case of read/write overflow failure, send the buffer back so that client can free these buffers
      voice_evt_notify_host_pcm_buf_v2_t    host_pcm_notify_evt = {0};

      host_pcm_notify_evt.tap_point = pHostPcmData->tap_point;
      host_pcm_notify_evt.mask = 0;

      if (read_flag)
      {
         host_pcm_notify_evt.mask |= (VOICE_HOST_PCM_READ | VOICE_HOST_PCM_READ_ERROR);
      }

      if (write_flag)
      {
         host_pcm_notify_evt.mask |= (VOICE_HOST_PCM_WRITE | VOICE_HOST_PCM_WRITE_ERROR);
      }

      host_pcm_notify_evt.rd_buff_addr_lsw = pHostPcmData->rd_buff_addr_lsw;
      host_pcm_notify_evt.wr_buff_addr_lsw = pHostPcmData->wr_buff_addr_lsw;
      host_pcm_notify_evt.rd_buff_size = pHostPcmData->rd_buff_size;
      host_pcm_notify_evt.wr_buff_size = pHostPcmData->wr_buff_size;
      host_pcm_notify_evt.rd_num_chan = context_ptr->write_config.circ_struct_local_buf.num_channels;
      host_pcm_notify_evt.wr_num_chan = pHostPcmData->wr_num_chan;
      voice_reply_host_pcm_data_evt( context_ptr, &host_pcm_notify_evt); // send the evt notify

      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Host PCM client error, notify back - rd result(%#x), wr result(%#x)",read_result, write_result);
   }

   return result;
}

ADSPResult voice_host_pcm_get_enable( voice_host_pcm_context_t *context_ptr, uint16_t *enable, uint16_t direction)
{

   ADSPResult result = ADSP_EOK;

   if( VOICE_HOST_PCM_READ == direction)
   {
      *enable = context_ptr->read_config.enable;
   }
   else if( VOICE_HOST_PCM_WRITE == direction)
   {
      *enable = context_ptr->write_config.enable;
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

// enable/disable the service.
ADSPResult voice_host_pcm_set_enable(voice_host_pcm_context_t *context_ptr, uint16_t enable, uint16_t direction, uint16_t num_channels, uint16 hostSamplingRate, uint16_t dsp_sampling_rate, uint16_t num_samples)
{

   ADSPResult result = ADSP_EOK;

   // Validate client sampling rate when enabling
   if(TRUE == enable)
   {
      if((VOICE_NB_SAMPLING_RATE != hostSamplingRate) &&
            (VOICE_WB_SAMPLING_RATE != hostSamplingRate) &&
            (VOICE_SWB_SAMPLING_RATE != hostSamplingRate) &&
            (VOICE_FB_SAMPLING_RATE != hostSamplingRate))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Invalid host pcm client sampling rate %d", (int)hostSamplingRate);
         return ADSP_EBADPARAM;
      }
   }

   MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Host PCM setEnable direction(%d) enable(%d) num_channels(%d) clientRate(%d)\n",
      direction, enable, num_channels, hostSamplingRate);

   if( VOICE_HOST_PCM_READ == direction)
   {
      if (FALSE == enable)
      {
         context_ptr->read_config.enable = FALSE;
         // reset the read direction circ buffers -> effectively drops all pushed buffers
         voice_circbuf_reset( &(context_ptr->read_config.circ_struct_client_buffers));
         voice_multi_ch_circ_buf_reset(&(context_ptr->read_config.circ_struct_local_buf));
         /* consistency, as we reset circ buffers */
      }
      else if (TRUE == enable)
      {
         //enable read direction
         context_ptr->read_config.enable        = TRUE;
         context_ptr->read_config.host_sampling_rate = hostSamplingRate;

      } // end of inner else if
   }
   else if( VOICE_HOST_PCM_WRITE == direction)
   {
      if(FALSE == enable)
      {
         context_ptr->write_config.enable = FALSE;
         // reset the read direction circ buffers -> effectively drops all pushed buffers
         voice_circbuf_reset( &(context_ptr->write_config.circ_struct_client_buffers));
         voice_multi_ch_circ_buf_reset(&(context_ptr->write_config.circ_struct_local_buf));
         /* consistency, as we reset circ buffers */
      }
      else if (TRUE == enable)
      {
         //enable write direction
         context_ptr->write_config.enable             = TRUE;
         context_ptr->write_config.host_sampling_rate = hostSamplingRate;
      } // end of else if
   } // end of else if( VOICE_HOST_PCM_WRITE == direction)
   else
   {
      result = APR_EBADPARAM;
   }
   result = voice_host_pcm_reinit(context_ptr, num_channels, dsp_sampling_rate,  num_samples);
   return result;
}

// destroy the host pcm service
ADSPResult voice_host_pcm_end(voice_host_pcm_context_t *context_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint8_t index;

   for( index = 0; index < MAX_NUM_HOST_PCM_CHANNELS; index++)
   {
      // free up channel mem of Host Pcm Resamplers
      if(NULL != context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index])
      {
         qurt_elite_memory_free(context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index]);
         context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index] = NULL;
      }
      if(NULL != context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index])
      {
         qurt_elite_memory_free(context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index]);
         context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index] = NULL;
      }
   }
   if( context_ptr->ss_enable)
   {
      /* ss_end not required here, since memory being freed */
      if( NULL != context_ptr->read_config.ss_memory)
      {
         qurt_elite_memory_free( context_ptr->read_config.ss_memory);
         context_ptr->read_config.ss_memory = NULL;
      }
      if( NULL != context_ptr->write_config.ss_memory)
      {
         qurt_elite_memory_free( context_ptr->write_config.ss_memory);
         context_ptr->write_config.ss_memory = NULL;
      }
   }
   for (int32_t j = 0; j < MAX_NUM_HOST_PCM_CHANNELS; j++)
   {
      if( NULL != context_ptr->read_config.circ_struct_local_buf.buf_ptr[j])
      {
         qurt_elite_memory_free(context_ptr->read_config.circ_struct_local_buf.buf_ptr[j]);
         context_ptr->read_config.circ_struct_local_buf.buf_ptr[j] = NULL;
      }
      if( NULL != context_ptr->write_config.circ_struct_local_buf.buf_ptr[j])
      {
         qurt_elite_memory_free(context_ptr->write_config.circ_struct_local_buf.buf_ptr[j]);
         context_ptr->write_config.circ_struct_local_buf.buf_ptr[j] = NULL;
      }
      if( NULL != context_ptr->scratch_resamp_ptr[j])
      {
         qurt_elite_memory_free(context_ptr->scratch_resamp_ptr[j]);
         context_ptr->scratch_resamp_ptr[j] = NULL;
      }

   }
   return result;
}


ADSPResult voice_host_pcm_process(voice_host_pcm_context_t *context_ptr, uint16_t num_channels, int16 *pBuf[MAX_NUM_HOST_PCM_CHANNELS], uint16_t num_samples, int16_t numSampSlip, int16_t client_rd_wr_signal)
{

   ADSPResult result = ADSP_EOK;
   elite_mem_shared_memory_map_t memmap_read = {0};
   elite_mem_shared_memory_map_t memmap_write = {0};

   // structure to contain notification information.  Begin by initializing known values -> start with mask = 0
   voice_evt_notify_host_pcm_buf_v2_t    host_pcm_notify_evt = {0};

   uint32_t topology_rate_frame_samples     = (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor));
   uint32_t hpcm_read_frame_samples         = (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->read_config.host_sampling_rate_factor));
   uint32_t hpcm_write_frame_samples        = (NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->write_config.host_sampling_rate_factor));

   if( TRUE == context_ptr->write_config.enable ||
       TRUE == context_ptr->read_config.enable)
   {
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: tap_point(%#lx) num_samples(%d) hpcm samples(%ld) sampling rate(%d) rd_enable(%d) wr_enable(%d) rd_wr_signal(%d)",
                                             context_ptr->tap_point,
                                             num_samples,
                                             topology_rate_frame_samples,
                                             context_ptr->sampling_rate,
                                             context_ptr->read_config.enable,
                                             context_ptr->write_config.enable,
                                             client_rd_wr_signal);
   }

   if( num_samples > VOICE_HOST_PCM_LOCALBUF_SIZE)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: HPCM process, input size too big: num_samples(%d) tap_point(%#lx)",num_samples,context_ptr->tap_point);
      return ADSP_EFAILED;
   }

   // indicate no buffers ready for notification.  If after processing read and write direction we haven't indicated buffers are ready, then
   // client will use the event to drive timing and read size information from the notification.  API definition of voice_evt_notify_host_pcm_buf_v2_t
   // contains details on how mask = 0 case works.

   host_pcm_notify_evt.mask      = 0;
   host_pcm_notify_evt.tap_point = context_ptr->tap_point;   //  one tap point per context

   if( TRUE == context_ptr->read_config.enable)
   {
#if defined(VOICE_DBG_MSG)
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: HPCM read processing, context(%#x)",context_ptr);
#endif
      host_pcm_notify_evt.sampling_rate = context_ptr->read_config.host_sampling_rate;
      host_pcm_notify_evt.rd_buff_size  = (hpcm_read_frame_samples) *
         sizeof(int16_t) *
         num_channels;
   }
   // this also fills sampling rate info, which is assumed to be the same given common sampling rate field in interface.  Fill in here since read enable may be false
   if( TRUE == context_ptr->write_config.enable)
   {
#if defined(VOICE_DBG_MSG)
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: HPCM write processing, context(%#x)",context_ptr);
#endif
      host_pcm_notify_evt.sampling_rate = context_ptr->write_config.host_sampling_rate;
      host_pcm_notify_evt.wr_buff_size  = (hpcm_write_frame_samples) *
         sizeof(int16_t) *
         num_channels;
      host_pcm_notify_evt.wr_num_chan   = num_channels;
   }

   //Number of 10msec frame samples.
   uint16_t frame_size_10msec = context_ptr->sampling_rate_factor*VOICE_FRAME_SIZE_NB_10MS;

   // process the read direction
   {
      voice_host_pcm_buffer_element_t   host_pcm_read_element;
      if( context_ptr->ss_enable && (TRUE == context_ptr->read_config.enable || TRUE == context_ptr->write_config.enable))
      {
         //Find number of 10msec frames need to be processed
         uint16 input_size, output_size,offset_10msec;
         int16_t current_sample_slip;
         uint16_t num_10msec_frames = (num_samples == frame_size_10msec)?1:2;    // = num_samples/frame_size_10msec;

         //Output is fixed 10msec frame size.
         output_size = frame_size_10msec;

         for(int i=0; i < num_10msec_frames; i++)
         {
            //Adjust sample in second 10 msec frame
            current_sample_slip = (1 == num_10msec_frames)?numSampSlip:(numSampSlip*i); //+ve for slip and negative for stuff
            input_size = frame_size_10msec + current_sample_slip;
            offset_10msec = frame_size_10msec*i;

            //dbg: MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Voice SS HPCM READ:input_frame=%d, output_size=%d, i=%d ",input_size,output_size,i);

            for (int j = 0; j < num_channels; j++)
            {
               voice_sample_slip_process(&(context_ptr->read_config.ss_struct[j]),(int16_t *)&pBuf[j][offset_10msec],(int16_t *)&pBuf[j][offset_10msec],input_size, output_size);
            }
         }
      }

      if( TRUE == context_ptr->read_config.enable )
      { // store the data from svc in the local rd circular buffer
         circbuf_status_enum result_circ_buf = CIRCBUF_SUCCESS ;
         int8_t *local_buf_ptr[MAX_CIRCBUF_CHANNELS];
         for (int j = 0; j < MAX_CIRCBUF_CHANNELS; j++)
         {
            local_buf_ptr[j] = (int8_t*)pBuf[j];
         }


         result_circ_buf = voice_multi_ch_circ_buf_write(&(context_ptr->read_config.circ_struct_local_buf),
            &local_buf_ptr, //(int8_t* (*)[MAX_CIRCBUF_CHANNELS])pBuf,
            num_samples);

         if (CIRCBUF_SUCCESS != result_circ_buf)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM read direction tap_point(%#lx), circbuf write error(%#x) ",context_ptr->tap_point,result_circ_buf);
         }
      }//if( TRUE == context_ptr->read_config.enable )

      // get a buffer element from the read buffer Q.  Since using circ buf, indicate size of element in terms of "samples" units
      if (client_rd_wr_signal)
      {

         uint32_t available_read_samples = 0;
         if( CIRCBUF_SUCCESS == voice_circbuf_read_request(&(context_ptr->read_config.circ_struct_client_buffers), VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, &available_read_samples) &&
             CIRCBUF_SUCCESS == voice_circbuf_read( &context_ptr->read_config.circ_struct_client_buffers, (int8_t*) &host_pcm_read_element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, sizeof(voice_host_pcm_buffer_element_t) ))
         {

            uint16_t actual_size   = 0;

            // calculate the required read buffer size to fill the input data with to send to Host
            uint32_t required_host_size =  host_pcm_notify_evt.rd_buff_size;

//dbg:            MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM read buffer processing tapPoint(0x%x) context_ptr(%p) num_channels(%d) numSampSlip(%d) qcount(%d)",
//dbg:                  host_pcm_read_element.tap_point, context_ptr, num_channels,numSampSlip,
//dbg:                  context_ptr->read_config.circ_struct_client_buffers.unread_samples / VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);

            //get the virtual address of the read buffer

            memmap_read.unMemMapClient = context_ptr->shared_mem_client;
            memmap_read.unMemMapHandle = context_ptr->mem_map_handle;
            result = elite_mem_map_get_shm_attrib(host_pcm_read_element.buff_addr_lsw,
                                                                            host_pcm_read_element.buff_addr_msw,
                                                  host_pcm_read_element.buff_size,
                                                  &memmap_read);

            // validate the virtual address translation, and check params
            if( ADSP_SUCCEEDED( result) &&
               host_pcm_read_element.buff_size  >= required_host_size &&
               host_pcm_read_element.tap_point == context_ptr->tap_point &&
               host_pcm_read_element.valid     == BUF_VALID &&
               TRUE == context_ptr->read_config.enable)
            {
               uint8_t index;
               int16_t *pHostPcmReadBuf = (int16_t *) memmap_read.unVirtAddr;
               circbuf_status_enum result_circ_buf = CIRCBUF_SUCCESS;
              // int16_t resampling_buf[MAX_CIRCBUF_CHANNELS][MAX_VOICE_HOST_PCM_FRAME_SAMPLES]; // used for resampling if any required
               int8_t *local_buf_ptr[MAX_CIRCBUF_CHANNELS];
               for (int j = 0; j < MAX_CIRCBUF_CHANNELS; j++)
               {
                  local_buf_ptr[j] = (int8_t*)context_ptr->scratch_resamp_ptr[j];
               }

               // read data from circular buffer to the resampling buffers;
               result_circ_buf = voice_multi_ch_circ_buf_read(&(context_ptr->read_config.circ_struct_local_buf),&local_buf_ptr, topology_rate_frame_samples );

               // process each channel separately (and resample if necessary)

               if (CIRCBUF_SUCCESS == result_circ_buf )
               {
                  for( index = 0; index < num_channels; index++)
                  {
                     uint32_t numActualSamples = hpcm_read_frame_samples;
                     int16_t *pInput = (int16_t*)(context_ptr->scratch_resamp_ptr[index]);

                     // now send samples to the read buffer, resample if necessary

                     if( context_ptr->sampling_rate != context_ptr->read_config.host_sampling_rate)
                     {
                        /* topology_rate_frame_samples is input size, at topology rate.
                        * we resample this into the pHostPcmReadBuf, and numActualSamples after the
                        * call represents how many samples were written out, which is at the host pcm client rate.   */

                        if( NULL != context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index] )
                        {
                           int32_t resamp_result;
                           resamp_result = voice_resampler_process(&(context_ptr->read_config.hpcm_resamp_config),
                                                                   context_ptr->read_config.hpcm_resamp_channel_mem_ptr[index],
                                                                   (int8 *)pInput,topology_rate_frame_samples,(int8 *)pHostPcmReadBuf,numActualSamples);
                           if (VOICE_RESAMPLE_FAILURE != resamp_result)
                           {
                              MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM read buffer resampling: from (%ld) to (%ld), tap point(%#lx)",topology_rate_frame_samples, numActualSamples, context_ptr->tap_point);
                           }
                           else
                           {
                             MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM read resampling failure: from (%ld) to (%ld), tap point(%#lx)",topology_rate_frame_samples, numActualSamples, context_ptr->tap_point);
                             return ADSP_EFAILED;
                           }
                        }
                        else
                        {
                            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM read resampler NULL pointer error !! for tap point(%#lx)", context_ptr->tap_point);
                        }
                     }
                     else
                     {
                        memscpy( pHostPcmReadBuf,memmap_read.unMemSize - actual_size, pInput, numActualSamples << 1);
                     }

                     // advance to the next channel data to write to, update the total size written out
                     pHostPcmReadBuf += numActualSamples;
                     actual_size     += (numActualSamples << 1);
                  }
                  // Flush after writing the data into the rd buffer

                  result = elite_mem_flush_cache_v2(&memmap_read);

                  if (ADSP_FAILED(result))
                  {
                     MSG_6(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: HPCM read direction CACHE FLUSH ERROR(ignoring) tap_point(%#lx) context_ptr(%p) map_result(%d) required_host_size(%ld) buf_size(%d) popped tap point(0x%lx)!",
                        context_ptr->tap_point, context_ptr,result,required_host_size,host_pcm_read_element.buff_size,host_pcm_read_element.tap_point);
                  }
                  else
                  {
                     MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: HPCM Read direction Flush: Memory Map Phy low(%lx) high(%lx) virtual address(%lx)",
                                                          host_pcm_read_element.buff_addr_lsw,
                                                          host_pcm_read_element.buff_addr_msw,
                                                          memmap_read.unVirtAddr);
                  }

               }
               else
               {
                  MSG_5(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: HPCM read direction data not yet available tap_point(%#lx), context_ptr(%p) required_host_size(%ld), buf_size(%d) popped tap point(0x%lx)!",
                     context_ptr->tap_point, context_ptr,required_host_size,host_pcm_notify_evt.rd_buff_size, host_pcm_read_element.tap_point);
               }
            }
            else
            {
               MSG_8(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM read direction error tap_point(%#lx) context_ptr(%p) map_result(%d) handle(%#lx) required_host_size(%ld) buf_size(%d) popped tap point(0x%lx) enable(%d)!",
                  context_ptr->tap_point, context_ptr,result,context_ptr->mem_map_handle, required_host_size,host_pcm_read_element.buff_size,host_pcm_read_element.tap_point,context_ptr->read_config.enable);
               host_pcm_notify_evt.mask |= VOICE_HOST_PCM_READ_ERROR;
            }

            // after processing, update the read fields
            host_pcm_notify_evt.rd_buff_size = actual_size;                      // total size read out (if an error occurred, this is 0)
            host_pcm_notify_evt.rd_num_chan  = num_channels;
            host_pcm_notify_evt.rd_buff_addr_lsw = host_pcm_read_element.buff_addr_lsw;  // physical address
            host_pcm_notify_evt.rd_buff_addr_msw = host_pcm_read_element.buff_addr_msw;  // physical address
            host_pcm_notify_evt.mask        |= VOICE_HOST_PCM_READ;

         }
         else
         {
            if( TRUE == context_ptr->read_config.enable )
            {
               // if no buffer available, host will simply not get the data for this interval
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM read direction, Client buffer queue empty, tap_point(%#lx), context_ptr(%p)", context_ptr->tap_point, context_ptr);
            }
         }
      } //if (client_rd_wr_signal)
   }


   // process the write direction
   {
      voice_host_pcm_buffer_element_t   host_pcm_write_element;

      if (client_rd_wr_signal)
      {
         uint32_t available_read_samples = 0;
         if( (CIRCBUF_SUCCESS == voice_circbuf_read_request(&(context_ptr->write_config.circ_struct_client_buffers), VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, &available_read_samples)) &&
            CIRCBUF_SUCCESS == voice_circbuf_read( &context_ptr->write_config.circ_struct_client_buffers, (int8_t*) &host_pcm_write_element, VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE, sizeof(voice_host_pcm_buffer_element_t) ))
         {

            uint16_t actual_size   = 0;

            // calculate the required write buffer size to get from Host in order to send downstream
            uint32_t required_host_size = host_pcm_notify_evt.wr_buff_size;

//dbg:           MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write buffer processing context_ptr(%p) tapPoint(0x%x) valid(%ld) num_wr_channels(%d) qcount (%d)",
//dbg:                 context_ptr,host_pcm_write_element.tap_point, host_pcm_write_element.valid, num_wr_channels,
//dbg:                 context_ptr->write_config.circ_struct_client_buffers.unread_samples / VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE);

            //get the virtual address of the write buffer

            memmap_write.unMemMapClient = context_ptr->shared_mem_client;
            memmap_write.unMemMapHandle = context_ptr->mem_map_handle;
            result = elite_mem_map_get_shm_attrib( host_pcm_write_element.buff_addr_lsw,
                                                                            host_pcm_write_element.buff_addr_msw,
                                                   host_pcm_write_element.buff_size,
                                                   &memmap_write);

            //validate virtual address translation and check params.  Since we are injecting into downstream processing the buffer size should match the requirements. Any more
            //data will effectively be dropped.  Since buffer is non interleaved, results of missized buffers may not be as expected as far as consuming oversized buffer channel
            //data equally.
            //valid is checked here, since we mark any pushed buffers as invalid during a VPM Stop/Reinit/Start sequence (vocproc, not host pcm).  Buffers across a device switch are
            //stale and should be dropped.  The size reported will indicate that it was consumed, however (silently dropped)

            if( ADSP_SUCCEEDED( result) &&
               host_pcm_write_element.buff_size     == required_host_size &&
               host_pcm_write_element.sampling_rate == context_ptr->write_config.host_sampling_rate &&
               host_pcm_write_element.tap_point     == context_ptr->tap_point &&
               host_pcm_write_element.valid         == BUF_VALID  &&
               TRUE                                 == context_ptr->write_config.enable )
            {

               uint8_t index;
               int16_t *pHostPcmWriteBuf = (int16_t *) memmap_write.unVirtAddr;
               // int16_t resampled_buf[MAX_CIRCBUF_CHANNELS][MAX_VOICE_HOST_PCM_FRAME_SAMPLES];

               // Invalidate cache before reading the wr buffer

               result = elite_mem_invalidate_cache_v2(&memmap_write);

               if (ADSP_FAILED (result))
               {
                  MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: HPCM write direction CACHE INVALIDATION ERROR(ignoring) context_ptr(%p) map_result(%d) required_host_size(%ld) buf_size(%d) popped tap point(0x%lx) valid(%d)!",
                     context_ptr,result,required_host_size,host_pcm_write_element.buff_size,host_pcm_write_element.tap_point,host_pcm_write_element.valid);
               }
               else
               {
                   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: HPCM Write direction Inavalidate: Memory Map Phy low(%lx) high(%lx) virtual address(%lx)",
                                      host_pcm_write_element.buff_addr_lsw,
                                      host_pcm_write_element.buff_addr_msw,
                                      memmap_write.unVirtAddr);
               }

               // process each channel separately (and resample if necessary)
               for( index = 0; index < num_channels; index++)
               {
                  uint32_t numActualSamples = hpcm_write_frame_samples;

                  if( context_ptr->sampling_rate != context_ptr->write_config.host_sampling_rate)
                  {
                     /* topology_rate_frame_samples is the number of samples at the topology rate.  We need to resample the host pcm buffer at pHostPcmWriteBuf so
                     * each channel has num_samples after resampling.  numActualSamples will represent how many input samples were consumed (at host pcm client rate)
                     * to produce num_samples of output. */
                     if( NULL != context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index] )
                     {
                        int32_t resamp_result;
                        resamp_result = voice_resampler_process(&(context_ptr->write_config.hpcm_resamp_config),
                                                                context_ptr->write_config.hpcm_resamp_channel_mem_ptr[index],
                                                                (int8 *)pHostPcmWriteBuf,numActualSamples,
                                                                (int8 *)(context_ptr->scratch_resamp_ptr[index]),
                                                                topology_rate_frame_samples);
                        if( VOICE_RESAMPLE_FAILURE != resamp_result )
                        {
                           MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write buffer resampling: from (%ld) to (%ld) tap point(%#lx)",hpcm_write_frame_samples, topology_rate_frame_samples,context_ptr->tap_point);
                        }
                        else
                        {
                           MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM write resampling failure: from (%ld) to (%ld), tap point(%#lx)",hpcm_write_frame_samples, topology_rate_frame_samples, context_ptr->tap_point);
                           return ADSP_EFAILED;
                        }
                     }
                     else
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM write resampler NULL pointer error !! for tap point(%#lx)", context_ptr->tap_point);
                     }
                  }
                  else
                  {
                     memscpy( (context_ptr->scratch_resamp_ptr[index]),(NB_VOICE_HPCM_FRAME_SAMPLES*(context_ptr->sampling_rate_factor))<<1, pHostPcmWriteBuf, topology_rate_frame_samples << 1);
                  }

                  pHostPcmWriteBuf += numActualSamples;
                  actual_size      += (numActualSamples << 1);
               } // end for loop for num wr channels

               { // scope for local circ buf operations, store client data into circular buffer
                  circbuf_status_enum result_circ_buf = CIRCBUF_SUCCESS ;
                  int8_t *local_buf_ptr[MAX_CIRCBUF_CHANNELS];
                  for (int j = 0; j < MAX_CIRCBUF_CHANNELS; j++)
                  {
                     local_buf_ptr[j] = (int8_t*)context_ptr->scratch_resamp_ptr[j];
                  }

                  result_circ_buf = voice_multi_ch_circ_buf_write(&(context_ptr->write_config.circ_struct_local_buf),
                     &local_buf_ptr, //(int8_t* (*)[MAX_CIRCBUF_CHANNELS])(&resampled_buf[0]),
                     topology_rate_frame_samples);
                  if (CIRCBUF_SUCCESS != result_circ_buf)
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM read direction tap_point(%#lx), circbuf write error(%#x) ",context_ptr->tap_point,result_circ_buf);
                  }

                  //dbg: MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM tap point(%#lx) wrote samples (%d) into local circbuf", context_ptr->tap_point,topology_rate_frame_samples );

               } // scope for local circ buf operations
            }
            else
            {
               /* buffer error occurred (possibly because of size mismatch).   */
               MSG_8(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: HPCM write direction error context_ptr(%p) map_result(%d) handle(0x%lx) required_host_size(%ld) buf_size(%d) popped tap point(0x%lx) valid(%d), enable(%d)!",
                  context_ptr,result,context_ptr->mem_map_handle,required_host_size,host_pcm_write_element.buff_size,host_pcm_write_element.tap_point,host_pcm_write_element.valid, context_ptr->write_config.enable);
               host_pcm_notify_evt.mask |= VOICE_HOST_PCM_WRITE_ERROR;
            }

            // if the buffer was marked invalid, we "fake" consumption by indicated what the required size was, rather
            // than send 0 bytes consumed. The buffer could have been marked invalid because it was stale from a device handover
            // or because the write buffer was pushed during a write session on a multichannel topology.  Since no processing was done on
            // the write buffer, the input simply passes thru to the output in this case. (silent drop).  In all cases, mark wr_buff_size as
            // the required size of the next buffer for this topology

            host_pcm_notify_evt.wr_buff_size = required_host_size;
            host_pcm_notify_evt.wr_num_chan  = context_ptr->write_config.circ_struct_local_buf.num_channels;
            host_pcm_notify_evt.wr_buff_addr_lsw = host_pcm_write_element.buff_addr_lsw;  // physical address
            host_pcm_notify_evt.wr_buff_addr_msw = host_pcm_write_element.buff_addr_msw;  // physical address
            host_pcm_notify_evt.mask        |= VOICE_HOST_PCM_WRITE;

         } //popping hpcm write buffer in circular buffer Q
         else
         {
            if( TRUE == context_ptr->write_config.enable )
            {
               /* no buffer available, since in-place assumed the input will pass thru to output in this scenario */
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write direction, Client buffer queue empty, pass thru, tap_point(%#lx), context_ptr(%p)", context_ptr->tap_point, context_ptr);
            }
         }
      }//if (client_rd_wr_signal)

      if (TRUE == context_ptr->write_config.enable )
      {
         // local circ buf operation's scope, read num samples from circular buffer
         int8_t *local_buf_ptr[MAX_CIRCBUF_CHANNELS];
         for (int j = 0; j < MAX_CIRCBUF_CHANNELS; j++)
         {
            local_buf_ptr[j] = (int8_t*)pBuf[j];
         }

         circbuf_status_enum result_circ_buf = voice_multi_ch_circ_buf_read(&(context_ptr->write_config.circ_struct_local_buf),&local_buf_ptr, num_samples);

         //dbg: MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM tap point(%#lx) read write samples (%d) from local circbuf", context_ptr->tap_point,num_samples); //DC:
         if (CIRCBUF_SUCCESS != result_circ_buf)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write direction, circ buffer error reading out samples to local buffer -- pass thru, tap_point(%#lx) context_ptr(%p)", context_ptr->tap_point,context_ptr);
         }
      }

      if( context_ptr->ss_enable && (TRUE == context_ptr->read_config.enable || TRUE == context_ptr->write_config.enable))
      {
         //Find number of 10msec frames need to be processed
         uint16 input_size, output_size,offset_10msec;
         int16_t current_sample_slip;
         uint16_t num_10msec_frames = (num_samples == frame_size_10msec)?1:2;    // = num_samples/frame_size_10msec;

         //calculate input frame samples for 10msec processing. Input is always fixed to topology rate
         input_size = frame_size_10msec;

         for(int i=0; i < num_10msec_frames; i++)
         {
            //Adjust sample in second 10 msec frame
            current_sample_slip = (1 == num_10msec_frames)?numSampSlip:(numSampSlip*i); //numSampSlip is +ve for stuff and negative for slip
            output_size = frame_size_10msec + current_sample_slip;
            offset_10msec = frame_size_10msec*i;

            //dbg: MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Voice SS HPCM WRITE:input_frame=%d, output_size=%d, i=%d ",input_size,output_size,i);

            for (int j = 0; j < num_channels ; j++)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM SS tap point(%#lx) slip/stuff samples(%d) ", context_ptr->tap_point,numSampSlip);
               voice_sample_slip_process(&(context_ptr->write_config.ss_struct[j]),(int16_t*)&pBuf[j][offset_10msec],(int16_t*)&pBuf[j][offset_10msec], input_size, output_size);
            }
         }
      }
   }

   // now send the notification
   if (client_rd_wr_signal)
   {
      if (( TRUE == context_ptr->write_config.enable ) || ( TRUE == context_ptr->read_config.enable ))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM read direction notification: tap_point(%#lx) rd buffsize(%d) num_channels(%d) addr(%#lx)", context_ptr->tap_point, host_pcm_notify_evt.rd_buff_size, num_channels,host_pcm_notify_evt.rd_buff_addr_lsw);
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: HPCM write direction notification: tap_point(%#lx) wr buffsize(%d) num_wr_channels(%d) addr(%#lx) ", context_ptr->tap_point, host_pcm_notify_evt.wr_buff_size, num_channels,host_pcm_notify_evt.wr_buff_addr_lsw);
         //Timetick should always be sent when sending a notify via host_pcm_process
         host_pcm_notify_evt.mask |= VOICE_HOST_PCM_TIMETICK;
         voice_reply_host_pcm_data_evt( context_ptr, &host_pcm_notify_evt);
      }
   }

   return result;
}


ADSPResult voice_host_pcm_get_kpps(voice_host_pcm_context_t *context_ptr, uint32_t* kpps_ptr)
{
   /* Need to account for mcps from resampler and sample slip primarily,
      along with some margin for copies and control overhead
   */
   uint32_t resamp_kpps, ss_kpps = 0, margin_kpps = VOICE_HOST_PCM_MARGIN_KPPS, channel_count;
   ADSPResult result;
   if(NULL == context_ptr || NULL == kpps_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! HPCM wrapper received NULL pointer");
      return ADSP_EBADPARAM;
   }
   *kpps_ptr = 0;

   if(VOICE_FB_SAMPLING_RATE == context_ptr->sampling_rate)
   {
      resamp_kpps = VOICE_RESAMPLER_KPPS_48K_MAX;
      margin_kpps *= 6;
   }
   else if(VOICE_SWB_SAMPLING_RATE == context_ptr->sampling_rate)
   {
      resamp_kpps = VOICE_RESAMPLER_KPPS_32K_MAX;
      margin_kpps *= 4;
   }
   else if(VOICE_WB_SAMPLING_RATE == context_ptr->sampling_rate)
   {
      resamp_kpps = VOICE_RESAMPLER_KPPS_16K_MAX;
      margin_kpps *= 2;
   }
   else
   {
      resamp_kpps = VOICE_RESAMPLER_KPPS_8K_MAX;
   }

   /* Account for ss_kpps. ss algo should be inited to correct number of
    nominal samples and sampling rate, so the value we read should be good.
    All ss instances are initialized the same way, so it doesn't really matter
    which one we pick for the call below
  */
   if(context_ptr->ss_enable)
   {
      //configure sample slip structure
      (void)voice_ss_config(&context_ptr->read_config.ss_struct[0],context_ptr->sampling_rate);
      result = voice_ss_get_kpps(&context_ptr->read_config.ss_struct[0], &ss_kpps);
      if(ADSP_FAILED(result))
      {
         return result;
      }
   }

   /* The policy is to allow writes when single channel read is enabled,
      but no writes on multichannel reads. The below count takes care of scaling
      the mpps correctly for multiple channels */
   channel_count = (context_ptr->read_config.num_channels > 1) ? context_ptr->read_config.num_channels : 2;
   resamp_kpps *= channel_count;
   ss_kpps *= channel_count;

   *kpps_ptr = (resamp_kpps + ss_kpps + margin_kpps);
   return ADSP_EOK;
}
