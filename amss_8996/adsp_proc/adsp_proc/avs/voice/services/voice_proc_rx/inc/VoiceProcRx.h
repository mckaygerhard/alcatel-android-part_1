
/*========================================================================

@file VoiceProcRx.h
@brief This file provides interfaces to create and control an instance of the
voice proc rx dynamic service

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_proc_rx/inc/VoiceProcRx.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/11/10   ksa     Added doxygen markup
10/29/09   dc      Created file.

========================================================================== */
#ifndef VOICEPROCRX_H
#define VOICEPROCRX_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus


/* -----------------------------------------------------------------------
** Global definitions/forward declarations
** ----------------------------------------------------------------------- */

// 20ms input frame length
#define VOICE_RX_INPUT_FRAME_LEN  20

//SA TODO: Use same structure for Tx and Rx
typedef struct vprx_create_params_t
{
   uint16_t afe_port_id;         // afe_port_id ID of Rx AFE port to connect to
   uint32_t topology_id;         // topology_id Topology ID to be used
   uint32_t sampling_rate;       // sampling_rate Sampling rate to run the voice proc at
   uint32_t session_num;         // session_num VPM session number that this instance belongs to
   uint16_t num_channels;        // topology_id Topology ID to be used
   uint32_t shared_mem_client;   // shared_mem_client Client to use when converting physical
} vprx_create_params_t;

typedef struct vprx_appi_fptr_t
{
   appi_getsize_v2_f so_getsize_fptr;
   appi_init_v2_f    so_init_fptr;
}vprx_appi_fptr_t;


/** Create an instance of the voiceproc rx service
    @param [out] handle Return handle to the created instance
    @param [in] vprx_create_params_t - create params
    @param [in] vprx_appi_fptr_t - create params for shared object for VDL v1 topos
    @return An indication of success/failure
*/
ADSPResult vprx_create (void **handle,
      vprx_create_params_t *create_param_ptr,
      vprx_appi_fptr_t *vprx_appi_fptr_ptr);


#ifdef __cplusplus
}
#endif //__cplusplus

#endif//#ifndef VOICEPROCRX_H

