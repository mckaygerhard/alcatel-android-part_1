
/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_CtmRx.cpp#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
08/05/2010     dp      Created
========================================================================== */
#include "Voice_CtmRx.h"
#include "VoiceLoggingUtils.h"
#include "VoiceCmnUtils.h"

#define VOICE_CTMRX_NB_KPPS (7860)
#define VOICE_CTMRX_WB_KPPS (8860)
#define VOICE_CTMRX_SWB_KPPS (VOICE_CTMRX_NB_KPPS + VOICE_RESAMPLER_KPPS_8K_TO_32K + VOICE_RESAMPLER_KPPS_32K_TO_8K)
#define VOICE_CTMRX_FB_KPPS (9860) // TODO: Needs update
static const vcmn_sampling_kpps_t VOICE_CTMRX_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_CTMRX_NB_KPPS },
                                                                       {VOICE_WB_SAMPLING_RATE, VOICE_CTMRX_WB_KPPS },
                                                                       {VOICE_SWB_SAMPLING_RATE, VOICE_CTMRX_SWB_KPPS },
                                                                       {VOICE_FB_SAMPLING_RATE, VOICE_CTMRX_FB_KPPS },
                                                                       {VOICE_INVALID_SAMPLING_RATE, 0}};

static void voice_ctm_rx_free_local_memory(ctm_rx_struct_t* ctm_rx_struct_ptr);

extern "C" {
    #include "ctm_rx_api.h"
}

/* initialize Ctm module, allocate memory */
ADSPResult voice_ctm_rx_init(ctm_rx_struct_t* ctm_rx_struct_ptr, uint32_t session_id)
{
   ADSPResult result = ADSP_EOK;

   voice_ctm_rx_set_enable( ctm_rx_struct_ptr, FALSE);

   ctm_rx_struct_ptr->ctm_character_transmitted        = FALSE;
   ctm_rx_struct_ptr->enquiry_from_far_end_detected      = FALSE;
   ctm_rx_struct_ptr->ctm_from_far_end_detected          = FALSE;
   ctm_rx_struct_ptr->sync_recover_rx                  = FALSE;
   ctm_rx_struct_ptr->rx_tty_detected                  = FALSE;
   ctm_rx_struct_ptr->session_id                       = session_id;

   init_ctmRx(ctm_rx_struct_ptr->ctm_rx_struct_instance_ptr);

   return result;
}

ADSPResult voice_ctm_rx_resampler_init(ctm_rx_struct_t* ctm_rx_struct_ptr)
{
   //freeup existing local memory for resampler, if any
   voice_ctm_rx_free_local_memory(ctm_rx_struct_ptr);
   // Resampler config for CTM Rx
   if(VOICE_NB_SAMPLING_RATE != ctm_rx_struct_ptr->sampling_rate)
   {
     uint32_t out_frame_samples,in_frame_samples, sampling_rate;
     sampling_rate=(uint32_t)ctm_rx_struct_ptr->sampling_rate;
     in_frame_samples=((ctm_rx_struct_ptr->sampling_rate)*VOICE_FRAME_SIZE_WB/VOICE_WB_SAMPLING_RATE);
     voice_resampler_set_config(&(ctm_rx_struct_ptr->down_samp_config),sampling_rate,VOICE_NB_SAMPLING_RATE,NO_OF_BITS_PER_SAMPLE,in_frame_samples,&out_frame_samples);
     voice_resampler_set_config(&(ctm_rx_struct_ptr->up_samp_config),VOICE_NB_SAMPLING_RATE,sampling_rate,NO_OF_BITS_PER_SAMPLE,VOICE_FRAME_SIZE_NB,&out_frame_samples);
     ctm_rx_struct_ptr->down_samp_mem_ptr = qurt_elite_memory_malloc( (ctm_rx_struct_ptr->down_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     ctm_rx_struct_ptr->up_samp_mem_ptr = qurt_elite_memory_malloc( (ctm_rx_struct_ptr->up_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     if ( NULL == ctm_rx_struct_ptr->down_samp_mem_ptr
         || NULL == ctm_rx_struct_ptr->up_samp_mem_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: Rx CTM init failed, out of memory");
      voice_ctm_rx_free_local_memory(ctm_rx_struct_ptr);
      return ADSP_ENOMEMORY;
   }
     // Initialize upsampler and downsampler needed for CTM
     int32_t resample_result;
     resample_result = voice_resampler_channel_init(
                         &(ctm_rx_struct_ptr->down_samp_config),
                         ctm_rx_struct_ptr->down_samp_mem_ptr,
                         (ctm_rx_struct_ptr->down_samp_config).total_channel_mem_size
                         );
     if(VOICE_RESAMPLE_SUCCESS !=resample_result)
     {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Rx CTM down sampler init failed");
        voice_ctm_rx_free_local_memory(ctm_rx_struct_ptr);
        return ADSP_EFAILED;
     }
     resample_result = voice_resampler_channel_init(
                         &(ctm_rx_struct_ptr->up_samp_config),
                         ctm_rx_struct_ptr->up_samp_mem_ptr,
                         (ctm_rx_struct_ptr->up_samp_config).total_channel_mem_size
                         );
     if(VOICE_RESAMPLE_SUCCESS !=resample_result)
     {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Rx CTM up sampler init failed");
        voice_ctm_rx_free_local_memory(ctm_rx_struct_ptr);
        return ADSP_EFAILED;
     }
   }
   return ADSP_EOK;
}

/* process 20 ms buffer, assume in place */
ADSPResult voice_ctm_rx_process(ctm_rx_struct_t* ctm_rx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples)
{
   ADSPResult result = ADSP_EOK;

   if( TRUE == ctm_rx_struct_ptr->enable )
   {
      int16_t demodBuf[VOICE_FRAME_SIZE_FB];  // fullband input vector size in word.
      int32_t cng_control = 0;
      int32_t fBypassed = FALSE;
      char_t  character;
      int16_t  tty_code = -1;

      if (VOICE_FRAME_SIZE_FB >= samples)
      {
         for (int32_t j=0; j<samples; j++)
         {
            demodBuf[j]= in_ptr[j];
         }
      }
      // downsample if Wideband/Fullband operation
      if ((VOICE_NB_SAMPLING_RATE != ctm_rx_struct_ptr->sampling_rate)&&( NULL != ctm_rx_struct_ptr->down_samp_mem_ptr))
      {
         int32_t resample_result;
         resample_result = voice_resampler_process(
                            &(ctm_rx_struct_ptr->down_samp_config),
                            ctm_rx_struct_ptr->down_samp_mem_ptr,
                            (int8 *)demodBuf,
                            (uint32)samples,
                            (int8 *)demodBuf,
                            VOICE_FRAME_SIZE_NB
                            );
         if(VOICE_RESAMPLE_SUCCESS != resample_result)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rx CTM down sampler process failed");
            return ADSP_EFAILED;
     }
      }
      // do CTM demodulate

      MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Rx CTM processing pre-demod, EnqDet(%ld),CtmDet(%ld),CtmTxd(%ld)",
                                         ctm_rx_struct_ptr->enquiry_from_far_end_detected, ctm_rx_struct_ptr->ctm_from_far_end_detected,
                                         ctm_rx_struct_ptr->ctm_character_transmitted);

      character = ctmrx_demodulate(ctm_rx_struct_ptr->ctm_rx_struct_instance_ptr,
                                   demodBuf,
                                   VOICE_FRAME_SIZE_NB,
                                   &tty_code,
                                   &ctm_rx_struct_ptr->enquiry_from_far_end_detected, &ctm_rx_struct_ptr->ctm_from_far_end_detected,
                                   ctm_rx_struct_ptr->ctm_character_transmitted, ctm_rx_struct_ptr->sync_recover_rx);

      ctm_rx_struct_ptr->sync_recover_rx = FALSE;

      if( (int8_t) character != -1)
      {
#if defined(__qdsp6__) && !defined(SIM)
           // log tty character if available
           int8_t *bufptr[4] = { (int8_t *) &character, NULL, NULL, NULL };
           voice_log_buffer( bufptr,
                  VOICE_LOG_CHAN_VSM_RX_CTM_CHAR,
                  (((VOICE_NB_SAMPLING_RATE != ctm_rx_struct_ptr->sampling_rate) << 3) | ctm_rx_struct_ptr->session_id),
                  qurt_elite_timer_get_time(),
                  VOICE_LOG_DATA_FORMAT_PCM_MONO,
                  (uint32_t)(ctm_rx_struct_ptr->sampling_rate),
                  sizeof(int8_t),
                  NULL);
#endif
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Rx CTM Regen, Char(%d)",character);
      }
      // do TTY regeneration
      cng_control = ctmrx_ttyregen(ctm_rx_struct_ptr->ctm_rx_struct_instance_ptr,
                                  in_ptr,
                                  VOICE_FRAME_SIZE_NB,
                                  out_ptr,
                                  &tty_code,
                                  &fBypassed,
                                  FALSE);

      // set Rx TTY detection (this is actually a flag for TTY baudot regeneration which controls muting).
      ctm_rx_struct_ptr->rx_tty_detected = cng_control;

      if ((VOICE_NB_SAMPLING_RATE != ctm_rx_struct_ptr->sampling_rate)&&( NULL != ctm_rx_struct_ptr->up_samp_mem_ptr))
      {
         if(FALSE ==fBypassed)
         {
            int32_t resample_result;
            resample_result = voice_resampler_process(
                               &(ctm_rx_struct_ptr->up_samp_config),
                               ctm_rx_struct_ptr->up_samp_mem_ptr,
                               (int8 *)in_ptr,
                               VOICE_FRAME_SIZE_NB,
                               (int8 *)out_ptr,
                               (uint32)samples
                               );
            if(VOICE_RESAMPLE_SUCCESS !=resample_result)
            {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rx CTM up sampler process failed");
            return ADSP_EFAILED;
        }
         }
         // Since in-place operation is assumed here, no need to copy samples through
         // if byPassed is TRUE
      }
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Rx CTM Regen, tty_code(%d), cng_control(%ld)", \
                                           tty_code, cng_control);
   }

   return result;
}

/* destroy Ctm module, free memory */
ADSPResult voice_ctm_rx_end(ctm_rx_struct_t* ctm_rx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   deinit_ctmRx( ctm_rx_struct_ptr->ctm_rx_struct_instance_ptr);

   voice_ctm_rx_free_local_memory(ctm_rx_struct_ptr);

   return result;
}

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_ctm_rx_set_enable(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8_t enable)
{
   ADSPResult result = ADSP_EOK;

   ctm_rx_struct_ptr->enable = enable;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMRx setting enable(%d)",enable);

   return result;
}

/* set WB vs NB */
ADSPResult voice_ctm_rx_commit_format(ctm_rx_struct_t* ctm_rx_struct_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   ctm_rx_struct_ptr->sampling_rate = sampling_rate;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: CTMRx setting sampling_rate(%d)",ctm_rx_struct_ptr->sampling_rate);
   return result;
}

/* set when CTM char_t transmitted */
ADSPResult voice_ctm_rx_set_ctm_char_transmitted(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8_t ctm_char_tx)
{
   ADSPResult result = ADSP_EOK;

   ctm_rx_struct_ptr->ctm_character_transmitted = ctm_char_tx;
   MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: CTMRx setting ctm_character_transmitted(%d)",ctm_char_tx);
   return result;
}

/* get status of CTM detect on far end  */
ADSPResult voice_ctm_rx_get_ctm_from_far_endDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8_t *ctm_from_far_end_detected)
{
   ADSPResult result = ADSP_EOK;

   *ctm_from_far_end_detected         = ctm_rx_struct_ptr->ctm_from_far_end_detected;

   return result;
}

/* get status of Enquiry detect from far end */
ADSPResult voice_ctm_rx_get_enquiry_from_far_endDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8_t *fEnquiryFromFarEnd)
{
   ADSPResult result = ADSP_EOK;

   *fEnquiryFromFarEnd = ctm_rx_struct_ptr->enquiry_from_far_end_detected;
   return result;
}


/* get status of Rx TTY detected end */
ADSPResult voice_ctm_rx_get_rxTTYDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8_t *rxTTYDetected)
{
   ADSPResult result = ADSP_EOK;

   *rxTTYDetected = ctm_rx_struct_ptr->rx_tty_detected;
   return result;
}

/* resync CTM during handover */
ADSPResult voice_ctm_rx_resync(ctm_rx_struct_t* ctm_rx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   ctm_rx_struct_ptr->sync_recover_rx = TRUE;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: CTMRx Resync");
   return result;
}

static void voice_ctm_rx_free_local_memory(ctm_rx_struct_t* ctm_rx_struct_ptr)
{
   // Destroy Up/Downsamplers
   if(NULL != ctm_rx_struct_ptr->down_samp_mem_ptr)
   {
      qurt_elite_memory_free(ctm_rx_struct_ptr->down_samp_mem_ptr);
      ctm_rx_struct_ptr->down_samp_mem_ptr = NULL;
   }
   if(NULL != ctm_rx_struct_ptr->up_samp_mem_ptr)
   {
      qurt_elite_memory_free(ctm_rx_struct_ptr->up_samp_mem_ptr );
      ctm_rx_struct_ptr->up_samp_mem_ptr = NULL;
   }
}

ADSPResult voice_ctm_rx_get_kpps(ctm_rx_struct_t* ctm_rx_struct_ptr,uint32_t* kpps_ptr, uint8_t enable, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= ctm_rx_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if( enable )
      {
         result = vcmn_find_kpps_table(VOICE_CTMRX_SAMPLING_KPPS_TABLE, sampling_rate ,kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}
