/*============================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_FNS.cpp#1 $

when        who   what, where, why
---------   ---   -------------------------------------------------------
07/15/10    sb    setparam,getparam added.
05/13/10    sb    Added changes for C++ to C conversion
10/12/09    cp    Created file.

============================================================================*/

/*============================================================================
 *                       INCLUDE FILES FOR MODULE
 *==========================================================================*/

#include "Voice_FNS.h"
#include "adsp_vparams_api.h"
#include "VoiceCmnUtils.h"

extern "C" {
    #include "eans_api.h"
 }

/* =============================================================*/
/*                                                              */
/*         DEFINITIONS AND DECLARATIONS FOR MODULE              */
/*                                                              */
/* This section contains definitions for constants,macros,types,*/
/* variables and other items needed by this module.             */
/*==============================================================*/

/* -------------------------------------------------------------*/
/* Constant / Define Declarations                               */
/* -------------------------------------------------------------*/
#define EANS_STATIC_MEM_SIZE   4500
#define EANS_SCRATCH_MEM_SIZE_FB   9500
#define SUBFRAME_SIZE        80      /* Nominal subframe size */

#define ROUNDTO8(x) ((((x) + 7) >> 3) << 3);

// Parameters for single-mic VAD
#define  AVAD_THRESH            16384   // (0.8) Q0.15 threshold for VAD detection
#define  AVAD_PWR_SCALE          512   // (0.0156) Q0.15 Multiplication factor for speech power
#define  AVAD_HANGOVER_MAX         20   // Length of hangover window
#define  AVAD_ALPHA_SNR         3278   // (0.75) Q0.15 averaging factor for smoothing SNR estimate
#define  AVAD_SNR_DIFF_MAX       4608   // (18 dB) Q7.8 max limit for log SNR difference
#define  AVAD_SNR_DIFF_MIN       2560   // (10 dB) Q7.8 min limit for log SNR difference
#define  AVAD_INIT_LENGTH         100   // Number of initial frames for min bound control
#define  AVAD_MAX_VAL             648   // (0.0198) Q0.15 square-root of upper bound for noise power estimate

// AVAD_CHANGES - added three new tuning parameters
#define  AVAD_INIT_BOUND          648   // (0.019) Q0.15 square-root of initial value for minimum bound
#define  AVAD_RESET_BOUND         290   // (0.088) Q0.15 square-root of reset value for minimum bound
#define  AVAD_VAR                8192   // (1) Q2.13 Multiplication factor for speech standard deviation
//

// IS_CHANGES: JAN_09 Two tuning parameters brought back
#define  AVAD_SUB_NC               12   // Sub-window length for single-mic VAD
#define  AVAD_SPOW_MIN            410   // (1.5-4) Q0.15 square root of minimum speech power threshold


// TODO : size should come as macro defintion from EANS library
//         will update in the next EANS algorithm update
typedef enum
{
   // Tuning parameters
   EANS_MODE_VAL               = 0x00FF,     // Q0 Default Mode word
   EANS_INPUT_GAIN_VAL         = 0x2000,     // Q2.13 (0 dB) Input gain
   EANS_OUTPUT_GAIN_VAL        = 0x2000,     // Q2.13 (0 dB) Output gain
   EANS_TARGET_NS_VAL          = 5120,       // Q7.8 (20 dB) Target noise suppression in dB
   EANS_SALPHA_VAL             = 5120,       // Q3.12 (1.25) Stationary NS over-subtraction factor
   EANS_NALPHA_VAL             = 4096,       // Q3.12 (1) Non-stationary NS over-subtraction factor
   EANS_NALPHA_MAX_VAL         = 8192,      // Q3.12 (3) Non-stationary NS max over-subtraction factor
   EANS_EALPHA_VAL             = 16384,      // Q15 (0.5) Excess NS subtraction factor
   EANS_NSNR_MAX_VAL           = 5120,       // Q7.8 (20 dB) Non-stationary NS SNR max
   EANS_SN_BLOCK_VAL           = 25,         // Q0 (25) Block size for stationary NS estimation
   EANS_NI_VAL                 = 400,        // Q0 (400) Initial block length for non-stationary NS initialization
   EANS_NPSCALE_VAL            = 2560,       // Q7.8 (10) SNR scale factor for non-stationary NS estimation
   EANS_NLAMBDA_VAL            = 32440,      // Q15 (0.99) Smoothing factor for non-stationary NS estimation
   EANS_NLAMBDAF_VAL           = 32736,      // Q15 (0.999) Higher smoothing factor for non-stationary NS estimation
   EANS_GSBIAS_VAL             = 0,          // Q7.8 (0 dB) SNR bias for Gain calculation
   EANS_GSMAX_VAL              = 2560,       // Q7.8 (10 dB) SNR limit for gain shaping
   EANS_SALPHA_HB_VAL          = 5120,       // Q3.12 (1.25) HB Stationary NS over-subtraction factor
   EANS_NALPHA_MAX_HB_VAL      = 4096,       // Q3.12 (1) HB Max non-stationary NS over-subtraction factor
   EANS_EALPHA_HB_VAL          = 16384,      // Q15 (0.5) HB Excess NS subtraction factor
   EANS_NLAMBDA0_VAL           = 32767,      // Q15 (0.9999) Non-stationary noise smoothing factor during active periods
    // Hardcoded parameters
   EANS_PLAMBDA0_VAL           = 16384,      // Q15 (0.5) Speech smoothing factor during active periods
   EANS_PLAMBDA_VAL            = 22938,      // Q15 (0.7) Speech smoothing factor during noise only periods
   EANS_SLAMBDA_VAL            = 22938,      // Q15 (0.7) Stationary noise floor smoothing factor
   EANS_SUB_BLOCK_VAL          = 4,          // Q0 (4) Number of sub-windows used in stationary NS estimation
   EANS_ALAMBDA0_VAL           = 16384,      // Q15 (0.5) SNR smoothing factor during active periods
   EANS_ALAMBDA_VAL            = 22938,      // Q15 (0.7) SNR smoothing factor during noise only periods
   EANS_NALAMBDA0_VAL          = 16384,      // Q15 (0.5) Nalpha smoothing factor during active periods
   EANS_NALAMBDA_VAL           = 22938,      // Q15 (0.7) Nalpha smoothing factor during noise only periods
   EANS_GLAMBDA0_VAL           = 9830,       // Q15 (0.3) Gain smoothing factor during active periods
   EANS_GLAMBDA_VAL            = 19660,      // Q15 (0.6) Gain smoothing factor during noise only periods
   EANS_NLAMBDA_INIT_VAL       = 32736,      // Q15 (0.999) Initial smoothing factor for non-stationary noise power
   EANS_NLAMBDA_SUB_VAL        = 8,          // Q15 subtraction factor for non-stationary smoothing constant
   EANS_PTHRESH_VAL            = 4,          // Q15 (2^-13) Threshold limit for power level
   EANS_STHRESH_VAL            = 4,          // Q15 (2^-13) Initial threshold for stationary noise floor estimate
   EANS_NTHRESH_VAL            = 16,         // Q15 (2^-13) Initial threshold for non-stationary noise power estimate
   EANS_GSM_FAST_VAL           = 9830,       // Q0.15 (0.3) Fast gain smoothing factor
   EANS_GSM_MED_VAL            = 22938,      // Q0.15 (0.7) Medium gain smoothing factor
   EANS_GSM_SLOW_VAL           = 29490,      // Q0.15 (0.9) Slow gain smoothing factor
   EANS_SWB_SALPHA_VAL         = 0x1000,     // Q3.12 stationary NS over-subtraction factor in SWB HB
   EANS_SWB_NALPHA_VAL         = 0x1000,     // Q3.12 non-stationary NS over-subtraction factor in SWB HB
   EANS_TARGET_NOISEFLOOR_VAL  = 0x01,       // Q0.15 Target noise floor level
   EANS_SLOPE_NOISEFLOOR_VAL   = 0x0         // Q0.15 Spectral slope of the target noise floor

} FnsParamsDefault;

#define VOICE_EANS_NB_KPPS (1700)
#define VOICE_EANS_WB_KPPS (3150)
#define VOICE_EANS_SWB_KPPS (5150) // approx
#define VOICE_EANS_FB_KPPS (9530) 
static const vcmn_sampling_kpps_t VOICE_EANS_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_EANS_NB_KPPS },
                                                                      {VOICE_WB_SAMPLING_RATE, VOICE_EANS_WB_KPPS },
                                                                      {VOICE_SWB_SAMPLING_RATE, VOICE_EANS_SWB_KPPS },
                                                                      {VOICE_FB_SAMPLING_RATE, VOICE_EANS_FB_KPPS },
                                                                      {VOICE_INVALID_SAMPLING_RATE, 0}};
/* Algorithmic delay values in microseconds */
#define VOICE_EANS_NB_DELAY  (6000) // 48 samples
#define VOICE_EANS_WB_DELAY  (6375) // 102 samples
#define VOICE_EANS_SWB_DELAY  (7200) // 102 samples
#define VOICE_EANS_FB_DELAY  (7200) // 340 samples


/*--------------------------------------------------------------*/
/* Local Object Definitions                                     */
/* -------------------------------------------------------------*/

static void eans_set_params_default ( int16_t *params );

uint32_t voice_eans_get_size( )
{
   uint32_t eans_struct_size =  ROUNDTO8(sizeof(eans_struct_t));
   uint32_t eans_static_mem_size =  ROUNDTO8(EANS_STATIC_MEM_SIZE);
   return( eans_struct_size + eans_static_mem_size );
}

ADSPResult voice_eans_init_default(eans_struct_t  *eans_struct_ptr)
{
   uint32_t eans_mem_consumed = 0;
   uint32_t eans_mem_allocated = ROUNDTO8(EANS_STATIC_MEM_SIZE);
   ADSPResult result = ADSP_EOK;
   eans_struct_ptr->enable_eans = FALSE;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_init_default() :: EANS init begin");
   // Allocate the EANS Static Memory
   //initialize the eans parameters
   eans_struct_ptr->sampling_rate = VOICE_NB_SAMPLING_RATE;
   eans_struct_ptr->params_modified = FALSE;
   // EANS_FRAME_SIZE will be initialized to NB block size by default
   eans_set_params_default(eans_struct_ptr->eans_params);
   eans_mem_consumed = eans_init(eans_struct_ptr->eans_params, eans_struct_ptr->p_eans_static_mem );

   if ( eans_mem_allocated < eans_mem_consumed )
   {
	   MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: RX-path EANS static memory allocated is insufficient");
   }
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_init_default() :: EANS init end ");
   return result;
}

void voice_eans_tx_init_default(eans_struct_t  *eans_struct_ptr, uint32_t sampling_rate)
{
   uint32_t eans_mem_consumed = 0;
   uint32_t eans_mem_allocated = ROUNDTO8(EANS_STATIC_MEM_SIZE);
   uint32_t eans_struct_size = ROUNDTO8(sizeof(eans_struct_t));
   uint16_t samp_rate_factor = sampling_rate/VOICE_NB_SAMPLING_RATE;
   eans_struct_ptr->enable_eans = FALSE;
   eans_struct_ptr->p_eans_static_mem = ((int8_t*)eans_struct_ptr + eans_struct_size );
   //initialize the eans parameters
   eans_struct_ptr->sampling_rate = sampling_rate;
   eans_struct_ptr->params_modified = FALSE;
   eans_struct_ptr->eans_params[EANS_FRAME_SIZE] = FRAME_SIZE_EANS_NB*samp_rate_factor;
   eans_set_params_default(eans_struct_ptr->eans_params);
   eans_struct_ptr->eans_params[EANS_MODE] = 0x00;
   eans_mem_consumed = eans_init(eans_struct_ptr->eans_params, eans_struct_ptr->p_eans_static_mem );

   if ( eans_mem_allocated < eans_mem_consumed )
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: TX-path EANS static memory allocated is insufficient");
   }
   MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: voice_eans_tx_init_default() :: EANS init end reached");

}


void voice_eans_init(eans_struct_t  *eans_struct_ptr, uint16_t sampling_rate)
{
   uint32_t eans_mem_consumed = 0;
   uint32_t eans_mem_allocated = ROUNDTO8(EANS_STATIC_MEM_SIZE);
   uint16_t samp_rate_factor = sampling_rate/VOICE_NB_SAMPLING_RATE;
   if ( sampling_rate != eans_struct_ptr->sampling_rate )
      {
      eans_struct_ptr->eans_params[EANS_FRAME_SIZE] = FRAME_SIZE_EANS_NB*samp_rate_factor;
      eans_mem_consumed = eans_init(eans_struct_ptr->eans_params, eans_struct_ptr->p_eans_static_mem );
      if ( eans_mem_allocated < eans_mem_consumed )
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: EANS static memory allocated is insufficient");
      }
      eans_struct_ptr->sampling_rate = sampling_rate;
   }

   MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: voice_eans_init() end");
}


ADSPResult voice_eans_end(eans_struct_t  *eans_struct_ptr)
{
   ADSPResult result = ADSP_EOK;
   eans_struct_ptr->p_eans_static_mem = NULL;
   MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: voice_eans_end() Success");
   return result;
}

void voice_eans_tx_end(eans_struct_t  *eans_struct_ptr)
{
   if(eans_struct_ptr != NULL)
   {
      eans_struct_ptr->p_eans_static_mem = NULL;
   }
   return;
}


ADSPResult voice_eans_process (eans_struct_t  *eans_struct_ptr,int16_t *out_ptr,int16_t *in_ptr, uint16_t block_size, int16_t out_buff_size)
{

   int16_t scratch_mem[EANS_SCRATCH_MEM_SIZE_FB];
   uint32_t scratch_mem_consumed = 0;
   uint32_t eans_mem_consumed = 0;
   uint32_t eans_mem_allocated = ROUNDTO8(EANS_STATIC_MEM_SIZE);

   if(NULL == in_ptr || NULL == out_ptr || NULL == eans_struct_ptr->p_eans_static_mem )
   {
		MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_process():: NULL Pointer error");
      return ADSP_EFAILED;
   }
   else
   {
      if(TRUE == eans_struct_ptr->enable_eans)
      {
         if ( TRUE == eans_struct_ptr->params_modified )
         {
            eans_mem_consumed = eans_init(eans_struct_ptr->eans_params, eans_struct_ptr->p_eans_static_mem );
            if ( eans_mem_allocated < eans_mem_consumed )
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: EANS static memory allocated is insufficient");
            }
            eans_struct_ptr->params_modified = FALSE;
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: eans params initialized");
         }

#if defined(VOICE_DBG_MSG)
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: FENS processing instance(%#x)",eans_struct_ptr);
#endif
         //eans_process returns amount of scratch memory used
         scratch_mem_consumed = eans_process(in_ptr, out_ptr, eans_struct_ptr->p_eans_static_mem,scratch_mem );

         //since there is no direct way to ascertain the scratch memory requirement
         // Checking whether scratch memory is allocted sufficiently or not
         if (scratch_mem_consumed > sizeof (scratch_mem))
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: EANS scratch memory allocated is insufficient");
         }

         if ( block_size > eans_struct_ptr->eans_params[EANS_FRAME_SIZE] ) //eans algo inp block size is 10msec
         {

           eans_process(in_ptr+eans_struct_ptr->eans_params[EANS_FRAME_SIZE], out_ptr+eans_struct_ptr->eans_params[EANS_FRAME_SIZE],
                      eans_struct_ptr->p_eans_static_mem, scratch_mem );
         }
         //dbg:MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Entered eans process called");
      }
      else if ( in_ptr != out_ptr )
      {
         memscpy(out_ptr, out_buff_size, in_ptr, block_size<<1 /* blocksize in bytes*/ );
      }
   }
   return ADSP_EOK;
}

ADSPResult voice_eans_set_param(eans_struct_t  *eans_struct_ptr,int8_t *params_buffer_ptr,uint32_t param_id,uint16_t param_size)
{
   ADSPResult result = ADSP_EOK;
   int16_t lib_result = 0;
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: set_param on eans, param_id=%x param_size=%d",(int)param_id,(int)param_size );
   if(NULL == params_buffer_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_set_param():: NULL Pointer error");
      return ADSP_EFAILED;
   }
   switch(param_id)
   {
      case VOICE_PARAM_FNS:
      case VOICE_PARAM_FNS_V2:
         {
            eans_interface_params_struct *eans_if_params_ptr;
            if(VOICE_PARAM_FNS == param_id)                       // if param id is for older version than compare with older interface structure
            {
               eans_if_params_ptr = (eans_interface_params_struct*)params_buffer_ptr;
               if (sizeof(eans_interface_params_struct) != param_size)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_set_param():: Bad Param Size %d for VOICE_PARAM_FNS param", (int32_t)param_size);
                  result = ADSP_EBADPARAM;
                  break;
               }

            }
            else
            {
               eans_interface_params_v2_struct *eans_if_params_ptr_v2 = (eans_interface_params_v2_struct*)params_buffer_ptr;
               eans_if_params_ptr = (eans_interface_params_struct*)(&eans_if_params_ptr_v2->eans_v1_params);
               if(VOICE_PARAM_FNS_V2_VERSION_0 == eans_if_params_ptr_v2->version)
               {
                  if (VOICE_PARAM_FNS_V2_VER_ZERO_SIZE != param_size)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_set_param():: Bad Param Size %d for VOICE_PARAM_FNS_V2 param", (int32_t)param_size);
                     result = ADSP_EBADPARAM;
                     break;
                  }
                  eans_struct_ptr->eans_params[EANS_TARGET_NOISEFLOOR]    = eans_if_params_ptr_v2->eansTargetNoiseFloor;
                  eans_struct_ptr->eans_params[EANS_SLOPE_NOISEFLOOR]    = eans_if_params_ptr_v2->eansSlopeNoiseFloor;
               }
               else
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_set_param():: unsupported version for VOICE_PARAM_FNS_V2 param");
                  result = ADSP_EBADPARAM;
                  break;
               }
            }
            eans_struct_ptr->eans_params[EANS_MODE]          = eans_if_params_ptr->eansMode;
            eans_struct_ptr->eans_params[EANS_INPUT_GAIN]    = eans_if_params_ptr->eansInputGain;
            eans_struct_ptr->eans_params[EANS_OUTPUT_GAIN]   = eans_if_params_ptr->eansOutputGain;
            eans_struct_ptr->eans_params[EANS_TARGET_NS]     = eans_if_params_ptr->eansTargetNS;
            eans_struct_ptr->eans_params[EANS_SALPHA]        = eans_if_params_ptr->eansSalpha;
            eans_struct_ptr->eans_params[EANS_NALPHA]        = eans_if_params_ptr->eansNalpha;
            eans_struct_ptr->eans_params[EANS_NALPHA_MAX]    = eans_if_params_ptr->eansNalphaMax;
            eans_struct_ptr->eans_params[EANS_EALPHA]        = eans_if_params_ptr->eansEalpha;
            eans_struct_ptr->eans_params[EANS_NSNR_MAX]      = eans_if_params_ptr->eansNSNRmax;
            eans_struct_ptr->eans_params[EANS_SN_BLOCK]      = eans_if_params_ptr->eansSNblock;
            eans_struct_ptr->eans_params[EANS_NI]            = eans_if_params_ptr->eansNi;
            eans_struct_ptr->eans_params[EANS_NPSCALE]       = eans_if_params_ptr->eansNPscale;
            eans_struct_ptr->eans_params[EANS_NLAMBDA]       = eans_if_params_ptr->eansNLambda;
            eans_struct_ptr->eans_params[EANS_NLAMBDAF]      = eans_if_params_ptr->eansNLambdaf;
            eans_struct_ptr->eans_params[EANS_GSBIAS]        = eans_if_params_ptr->eansGsBias;
            eans_struct_ptr->eans_params[EANS_GSMAX]         = eans_if_params_ptr->eansGsMax;
            eans_struct_ptr->eans_params[EANS_SALPHA_HB]     = eans_if_params_ptr->eansSalphaHB;
            eans_struct_ptr->eans_params[EANS_NALPHA_MAX_HB] = eans_if_params_ptr->eansNalphaMaxHB;
            eans_struct_ptr->eans_params[EANS_EALPHA_HB]     = eans_if_params_ptr->eansEalphaHB;
            eans_struct_ptr->eans_params[EANS_NLAMBDA0]      = eans_if_params_ptr->eansNLambda0;
            eans_struct_ptr->eans_params[EANS_AVAD_TH]       = eans_if_params_ptr->thresh;
            eans_struct_ptr->eans_params[EANS_AVAD_PSCALE]   = eans_if_params_ptr->pwrScale;
            eans_struct_ptr->eans_params[EANS_AVAD_HOVER]    = eans_if_params_ptr->hangoverMax;
            eans_struct_ptr->eans_params[EANS_AVAD_ALPHAS]   = eans_if_params_ptr->alphaSNR;
            eans_struct_ptr->eans_params[EANS_AVAD_SDMAX]    = eans_if_params_ptr->snrDiffMax;
            eans_struct_ptr->eans_params[EANS_AVAD_SDMIN]    = eans_if_params_ptr->snrDiffMin;
            eans_struct_ptr->eans_params[EANS_AVAD_INITL]    = eans_if_params_ptr->initLength;
            eans_struct_ptr->eans_params[EANS_AVAD_MAXVAL]   = eans_if_params_ptr->maxVal;
            eans_struct_ptr->eans_params[EANS_AVAD_INITB]    = eans_if_params_ptr->initBound;
            eans_struct_ptr->eans_params[EANS_AVAD_RESETB]   = eans_if_params_ptr->resetBound;
            eans_struct_ptr->eans_params[EANS_AVAD_AVAR]     = eans_if_params_ptr->avarScale;
            eans_struct_ptr->eans_params[EANS_AVAD_NC]       = eans_if_params_ptr->sub_Nc;
            eans_struct_ptr->eans_params[EANS_AVAD_SPMIN]    = eans_if_params_ptr->spowMin;
            eans_struct_ptr->eans_params[EANS_GSM_FAST]    = eans_if_params_ptr->eansGsFast;
            eans_struct_ptr->eans_params[EANS_GSM_MED]    = eans_if_params_ptr->eansGsMed;
            eans_struct_ptr->eans_params[EANS_GSM_SLOW]    = eans_if_params_ptr->eansGsSlow;
            eans_struct_ptr->eans_params[EANS_SWB_SALPHA]    = eans_if_params_ptr->eansSwbSalpha;
            eans_struct_ptr->eans_params[EANS_SWB_NALPHA]    = eans_if_params_ptr->eansSwbNalpha;

            //eans_struct_ptr->params_modified = TRUE;       // since only reinit is required here no need to set it true (it will prevent init of data structure when cal is changed during run cmnd)
            lib_result = eans_reinit(eans_struct_ptr->eans_params, eans_struct_ptr->p_eans_static_mem );
            if(lib_result != 0)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_set_param():: eans_reinit failed with result(%d) for param ID =  %lu", (int)lib_result,(uint32_t)param_id);
               return ADSP_EFAILED;
            }
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: EANS configuration structure re-initialized");

            result = ADSP_EOK;
         }
         break;
      case VOICE_PARAM_MOD_ENABLE:
         {
            int16_t enable_flag = *((int16_t*)params_buffer_ptr);
            if (sizeof(int32_t) != param_size)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_set_param():: Bad Param Size");
               result = ADSP_EBADPARAM;
               break;
            }
            if(eans_struct_ptr->enable_eans != enable_flag)
            {
               eans_struct_ptr->enable_eans = (int32_t)enable_flag;
               if(TRUE == enable_flag)
               {
                  eans_struct_ptr->params_modified = TRUE;
               }
            }
            result = ADSP_EOK;
         }
         break;
      default:
         result = ADSP_EBADPARAM;
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_set_param() :: Module ID not support \n");
   }
   return result;
}

ADSPResult voice_eans_get_param(eans_struct_t  *eans_struct_ptr,int8_t *params_buffer_ptr,uint32_t param_id,int32_t buffer_size, uint16_t *param_size_ptr)
{
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: get_param on eans, param_id=%x param_size=%d",(int)param_id,(int)buffer_size );
   if (NULL == params_buffer_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_get_param():: NULL Pointer error");
      return ADSP_EBADPARAM;
   }

   switch(param_id)
   {
      case VOICE_PARAM_FNS:
      case VOICE_PARAM_FNS_V2:
         {
            eans_interface_params_struct *eans_if_params_ptr;

            if(VOICE_PARAM_FNS == param_id)
            {
               *param_size_ptr = sizeof(eans_interface_params_struct);
               if(*param_size_ptr > buffer_size )
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_get_param required size = %d, given size = %ld",*param_size_ptr, buffer_size);
                  return ADSP_ENOMEMORY;
               }
               eans_if_params_ptr = (eans_interface_params_struct*)params_buffer_ptr;
            }
            else
            {
               eans_interface_params_v2_struct *eans_if_params_ptr_v2 = (eans_interface_params_v2_struct*)params_buffer_ptr;
               eans_if_params_ptr = (eans_interface_params_struct*)(&eans_if_params_ptr_v2->eans_v1_params);
               if(buffer_size >= (int32_t)VOICE_PARAM_FNS_V2_VER_ZERO_SIZE)
               {
                  *param_size_ptr = VOICE_PARAM_FNS_V2_VER_ZERO_SIZE;
                  eans_if_params_ptr_v2->version = VOICE_PARAM_FNS_V2_VERSION_0;
                  eans_if_params_ptr_v2->reserved = 0;
                  eans_if_params_ptr_v2->eansTargetNoiseFloor = eans_struct_ptr->eans_params[EANS_TARGET_NOISEFLOOR];   // copy new param id version 0 params
                  eans_if_params_ptr_v2->eansSlopeNoiseFloor  =	eans_struct_ptr->eans_params[EANS_SLOPE_NOISEFLOOR];
               }
               else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_eans_get_param required size = %d, given size = %ld",*param_size_ptr, buffer_size);
                  return ADSP_ENOMEMORY;
               }
            }

            eans_if_params_ptr->eansMode          = eans_struct_ptr->eans_params[EANS_MODE]         ;
            eans_if_params_ptr->eansInputGain     = eans_struct_ptr->eans_params[EANS_INPUT_GAIN]   ;
            eans_if_params_ptr->eansOutputGain    = eans_struct_ptr->eans_params[EANS_OUTPUT_GAIN]  ;
            eans_if_params_ptr->eansTargetNS      = eans_struct_ptr->eans_params[EANS_TARGET_NS]    ;
            eans_if_params_ptr->eansSalpha        = eans_struct_ptr->eans_params[EANS_SALPHA]       ;
            eans_if_params_ptr->eansNalpha        = eans_struct_ptr->eans_params[EANS_NALPHA]       ;
            eans_if_params_ptr->eansNalphaMax     = eans_struct_ptr->eans_params[EANS_NALPHA_MAX]   ;
            eans_if_params_ptr->eansEalpha        = eans_struct_ptr->eans_params[EANS_EALPHA]       ;
            eans_if_params_ptr->eansNSNRmax       = eans_struct_ptr->eans_params[EANS_NSNR_MAX]     ;
            eans_if_params_ptr->eansSNblock       = eans_struct_ptr->eans_params[EANS_SN_BLOCK]     ;
            eans_if_params_ptr->eansNi            = eans_struct_ptr->eans_params[EANS_NI]           ;
            eans_if_params_ptr->eansNPscale       = eans_struct_ptr->eans_params[EANS_NPSCALE]      ;
            eans_if_params_ptr->eansNLambda       = eans_struct_ptr->eans_params[EANS_NLAMBDA]      ;
            eans_if_params_ptr->eansNLambdaf      = eans_struct_ptr->eans_params[EANS_NLAMBDAF]     ;
            eans_if_params_ptr->eansGsBias        = eans_struct_ptr->eans_params[EANS_GSBIAS]       ;
            eans_if_params_ptr->eansGsMax         = eans_struct_ptr->eans_params[EANS_GSMAX]        ;
            eans_if_params_ptr->eansSalphaHB      = eans_struct_ptr->eans_params[EANS_SALPHA_HB]    ;
            eans_if_params_ptr->eansNalphaMaxHB   = eans_struct_ptr->eans_params[EANS_NALPHA_MAX_HB];
            eans_if_params_ptr->eansEalphaHB      = eans_struct_ptr->eans_params[EANS_EALPHA_HB]    ;
            eans_if_params_ptr->eansNLambda0      = eans_struct_ptr->eans_params[EANS_NLAMBDA0]     ;
            eans_if_params_ptr->thresh           = eans_struct_ptr->eans_params[EANS_AVAD_TH]      ;
            eans_if_params_ptr->pwrScale         = eans_struct_ptr->eans_params[EANS_AVAD_PSCALE]  ;
            eans_if_params_ptr->hangoverMax      = eans_struct_ptr->eans_params[EANS_AVAD_HOVER]   ;
            eans_if_params_ptr->alphaSNR         = eans_struct_ptr->eans_params[EANS_AVAD_ALPHAS]  ;
            eans_if_params_ptr->snrDiffMax       = eans_struct_ptr->eans_params[EANS_AVAD_SDMAX]   ;
            eans_if_params_ptr->snrDiffMin       = eans_struct_ptr->eans_params[EANS_AVAD_SDMIN]   ;
            eans_if_params_ptr->initLength       = eans_struct_ptr->eans_params[EANS_AVAD_INITL]   ;
            eans_if_params_ptr->maxVal           = eans_struct_ptr->eans_params[EANS_AVAD_MAXVAL]  ;
            eans_if_params_ptr->initBound        = eans_struct_ptr->eans_params[EANS_AVAD_INITB]   ;
            eans_if_params_ptr->resetBound       = eans_struct_ptr->eans_params[EANS_AVAD_RESETB]  ;
            eans_if_params_ptr->avarScale        = eans_struct_ptr->eans_params[EANS_AVAD_AVAR]    ;
            eans_if_params_ptr->sub_Nc           = eans_struct_ptr->eans_params[EANS_AVAD_NC]      ;
            eans_if_params_ptr->spowMin          = eans_struct_ptr->eans_params[EANS_AVAD_SPMIN]   ;
            eans_if_params_ptr->eansGsFast        = eans_struct_ptr->eans_params[EANS_GSM_FAST]     ;
            eans_if_params_ptr->eansGsMed         = eans_struct_ptr->eans_params[EANS_GSM_MED]      ;
            eans_if_params_ptr->eansGsSlow        = eans_struct_ptr->eans_params[EANS_GSM_SLOW]     ;
            eans_if_params_ptr->eansSwbSalpha     = eans_struct_ptr->eans_params[EANS_SWB_SALPHA]    ;
            eans_if_params_ptr->eansSwbNalpha     = eans_struct_ptr->eans_params[EANS_SWB_NALPHA]    ;

         }
         break;
      case  VOICE_PARAM_MOD_ENABLE:
         {
            *param_size_ptr = sizeof(int32_t);
            if(*param_size_ptr > buffer_size )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_eans_get_param required size = %d, given size = %ld",*param_size_ptr, buffer_size);
               return ADSP_ENOMEMORY;
            }
            *((int32_t*)params_buffer_ptr) = 0;	// Clearing the whole buffer
            *((int16_t*)params_buffer_ptr) = eans_struct_ptr->enable_eans;
         }
         break;
      default:
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_eans_set_param() :: Module ID not support \n");
         return ADSP_EUNSUPPORTED;
   }

   return ADSP_EOK;
}


void eans_set_params_default ( int16_t *params )
{

    // Set default values for the tuning parameters
   params[EANS_FRAME_SIZE]         = SUBFRAME_SIZE;          // Frame size
   params[EANS_MODE]               = EANS_MODE_VAL;           // Mode word
   params[EANS_INPUT_GAIN]         = EANS_INPUT_GAIN_VAL;     // Input gain
   params[EANS_OUTPUT_GAIN]        = EANS_OUTPUT_GAIN_VAL;    // Output gain
   params[EANS_TARGET_NS]          = EANS_TARGET_NS_VAL;      // Target noise suppression limit
   params[EANS_SALPHA]             = EANS_SALPHA_VAL;         // Stationary NS over-subtraction factor
   params[EANS_NALPHA]             = EANS_NALPHA_VAL;         // Non-stationary NS over-subtraction factor
   params[EANS_NALPHA_MAX]         = EANS_NALPHA_MAX_VAL;     // Non-stationary NS max over-subtraction factor
   params[EANS_EALPHA]             = EANS_EALPHA_VAL;         // Excess NS subtraction factor
   params[EANS_NSNR_MAX]           = EANS_NSNR_MAX_VAL;       // Non-stationary NS SNR max
   params[EANS_SN_BLOCK]           = EANS_SN_BLOCK_VAL;       // Block size for stationary NS estimation
   params[EANS_NI]                 = EANS_NI_VAL;             // Initial block length for non-stationary NS initialization
   params[EANS_NPSCALE]            = EANS_NPSCALE_VAL;        // SNR scale factor for non-stationary NS estimation
   params[EANS_NLAMBDA]            = EANS_NLAMBDA_VAL;        // Smoothing factor for non-stationary NS estimation
   params[EANS_NLAMBDAF]           = EANS_NLAMBDAF_VAL;       // Higher smoothing factor for non-stationary NS estimation
   params[EANS_GSBIAS]             = EANS_GSBIAS_VAL;         // Q7.8 SNR bias for gain calculation
   params[EANS_GSMAX]              = EANS_GSMAX_VAL;          // Q7.8 SNR limit for gain calculation
   params[EANS_SALPHA_HB]          = EANS_SALPHA_HB_VAL;      // HB Stationary NS over-subtraction factor
   params[EANS_NALPHA_MAX_HB]      = EANS_NALPHA_MAX_HB_VAL;  // HB Non-stationary NS max over-subtraction factor
   params[EANS_EALPHA_HB]          = EANS_EALPHA_HB_VAL;      // HB Excess NS subtraction factor
   params[EANS_NLAMBDA0]           = EANS_NLAMBDA0_VAL;       // Highest smoothing factor for non-stationary NS estimation
   params[EANS_AVAD_TH]            = AVAD_THRESH;            // Single-mic VAD threshold
   params[EANS_AVAD_PSCALE]        = AVAD_PWR_SCALE;         // Scale factor for speech power level
   params[EANS_AVAD_HOVER]         = AVAD_HANGOVER_MAX;      // Hangover limit for VAD decision
   params[EANS_AVAD_ALPHAS]        = AVAD_ALPHA_SNR;         // Averaging factor for SNR smoothing
   params[EANS_AVAD_SDMAX]         = AVAD_SNR_DIFF_MAX;      // Max limit for log SNR difference
   params[EANS_AVAD_SDMIN]         = AVAD_SNR_DIFF_MIN;      // Min limit for log SNR difference
   params[EANS_AVAD_INITL]         = AVAD_INIT_LENGTH;       // Single-mic vad initialization block length
   params[EANS_AVAD_MAXVAL]        = AVAD_MAX_VAL;           // Max value for noise power estimate
   params[EANS_AVAD_INITB]         = AVAD_INIT_BOUND;        // Bound value for initial noise power estimate
   params[EANS_AVAD_RESETB]        = AVAD_RESET_BOUND;       // Bound value for re-setting noise power estimation
   params[EANS_AVAD_AVAR]          = AVAD_VAR;               // Scale factor for noise power estimation
   params[EANS_AVAD_NC]            = AVAD_SUB_NC;            // Window length for stationary Noise floor estimation
   params[EANS_AVAD_SPMIN]         = AVAD_SPOW_MIN;          // Minimum limit for speech power level
   params[EANS_GSM_FAST]           = EANS_GSM_FAST_VAL;       // Q0.15 (0.3) Fast gain smoothing factor
   params[EANS_GSM_MED]            = EANS_GSM_MED_VAL;        // Q0.15 (0.7) Medium gain smoothing factor
   params[EANS_GSM_SLOW]           = EANS_GSM_SLOW_VAL;       // Q0.15 (0.9) Slow gain smoothing factor
   params[EANS_SWB_SALPHA]         = EANS_SWB_SALPHA_VAL;     // Q3.12 stationary NS over-subtraction factor in SWB HB
   params[EANS_SWB_NALPHA]         = EANS_SWB_NALPHA_VAL;     // Q3.12 non-stationary NS over-subtraction factor in SWB HB
   params[EANS_TARGET_NOISEFLOOR]  = EANS_TARGET_NOISEFLOOR_VAL;  // Target noise floor value
   params[EANS_SLOPE_NOISEFLOOR]   = EANS_SLOPE_NOISEFLOOR_VAL;   // Spectral Slope of the noise floor
}

ADSPResult voice_eans_set_mem(eans_struct_t *eans_struct_ptr,int8 *pMemAddr,uint32_t nSize)
{
   uint32_t eans_size = eans_get_size();

   if (nSize < eans_size)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  EANS static memory size insufficient - allocated (%ld) required (%ld)", nSize,eans_size);
      return ADSP_EFAILED;
    }

   eans_struct_ptr->p_eans_static_mem = (void *)pMemAddr;
   return ADSP_EOK;
}

uint32_t eans_get_size()
{

   return EANS_STATIC_MEM_SIZE;
}

ADSPResult voice_eans_get_kpps(eans_struct_t* eans_struct_ptr,uint32_t* kpps_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= eans_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(TRUE == eans_struct_ptr->enable_eans)
      {
         result = vcmn_find_kpps_table(VOICE_EANS_SAMPLING_KPPS_TABLE, sampling_rate, kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

ADSPResult voice_eans_get_delay(eans_struct_t* eans_struct_ptr,uint32_t* delay_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint32_t delay;

   if ((NULL!= eans_struct_ptr) && (NULL!= delay_ptr))
   {
      *delay_ptr = 0;
      if(TRUE == eans_struct_ptr->enable_eans)
      {
         if(VOICE_NB_SAMPLING_RATE == eans_struct_ptr->sampling_rate)
         {
            delay = VOICE_EANS_NB_DELAY;
         }
         else if(VOICE_WB_SAMPLING_RATE == eans_struct_ptr->sampling_rate)
         {
            delay = VOICE_EANS_WB_DELAY;
         }
         else if(VOICE_SWB_SAMPLING_RATE == eans_struct_ptr->sampling_rate)
         {
            delay = VOICE_EANS_SWB_DELAY;
         }
         else
         {
            delay = VOICE_EANS_FB_DELAY;
         }
         *delay_ptr = delay;
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}
