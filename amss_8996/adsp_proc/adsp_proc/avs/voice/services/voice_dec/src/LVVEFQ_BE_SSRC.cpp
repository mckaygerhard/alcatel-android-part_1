/****************************************************************************************/
/*  Copyright (c) 2004-2013 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: $                                                                       */
/*     $Revision: $                                                                     */
/*     $Date: $                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/

#ifdef LVVE

#include "adsp_vparams_api.h"
#include "EliteMsg_Util.h"
#include "LVVEFQ_BE_SSRC.h"

/****************************************************************************************/
/*                                                                                      */
/*  Macro definitions Constant / Define Declarations                                    */
/*                                                                                      */
/****************************************************************************************/
#define VOICE_LVVEFQ_BE_SSRC_KPPS (7725)


/****************************************************************************************/
/*                                                                                      */
/*  Function Definitions                                                                */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_process(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                  int16_t *out_ptr,
                                  int16_t *in_ptr,
                                  int32_t in_size)
{
  ADSPResult result = ADSP_EOK;
  SSRC_ReturnStatus_en ReturnStatus = SSRC_OK;

  if(in_size == LVVEFQ_BE_SSRC_BLOCK_SIZE)
  {
    ReturnStatus = SSRC_Process(&(pInstance->SSRC_Instance), 
                                (LVM_INT16 *)in_ptr,
                                (LVM_INT16 *)out_ptr);

    if(ReturnStatus != SSRC_OK)
    {
      MSG_1(MSG_SSID_QDSP6, 
            DBG_FATAL_PRIO, 
            "LVVEFQ_BE_SSRC_process - ERROR - SSRC_Process: %d", 
            (int32_t)ReturnStatus);
      return ADSP_EFAILED;
    }
  }
  else
    MSG_1(MSG_SSID_QDSP6, 
          DBG_FATAL_PRIO, 
          "LVVEFQ_BE_SSRC_process - ERROR - block size: %d", 
          in_size);
  
  return result;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_init(LVVEFQ_BE_SSRC_Instance_st *pInstance)
{
  ADSPResult result = ADSP_EOK;
  SSRC_ReturnStatus_en ReturnStatus = SSRC_OK;

  /* Call the init function */
  ReturnStatus = SSRC_Init(&(pInstance->SSRC_Instance), 
                           pInstance->pScratch, 
                           &(pInstance->SSRC_Params),
                           &(pInstance->pInputInScratch), 
                           &(pInstance->pOutputInScratch));
  if(ReturnStatus != SSRC_OK)
  {
    MSG_1(MSG_SSID_QDSP6, 
          DBG_FATAL_PRIO, 
          "LVVEFQ_BE_SSRC_init - ERROR - SSRC_Init: %d", 
          (int32_t)ReturnStatus);
    return ADSP_EFAILED;
  }

  /* Disable output gain */
  ReturnStatus = SSRC_SetGains(&(pInstance->SSRC_Instance),
                               LVM_MODE_OFF,
                               LVM_MODE_OFF,
                               0x7fff);
  if(ReturnStatus != SSRC_OK)
  {
    MSG_1(MSG_SSID_QDSP6, 
          DBG_FATAL_PRIO, 
          "LVVEFQ_BE_SSRC_init - ERROR - SSRC_SetGains: %d", 
          (int32_t)ReturnStatus);
    return ADSP_EFAILED;
  }

  pInstance->enable_wide_voice = 0;
  
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_end(LVVEFQ_BE_SSRC_Instance_st *pInstance)
{
  ADSPResult result = ADSP_EOK;
  
  if(pInstance->pScratch)
    pInstance->pScratch = NULL;

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
uint32_t LVVEFQ_BE_SSRC_get_size(LVVEFQ_BE_SSRC_Instance_st *pInstance)
{
  SSRC_ReturnStatus_en ReturnStatus = SSRC_OK;

#if defined(LVVEFQ_DEBUG_MSG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_BE_SSRC_get_size");
#endif  
 
  pInstance->SSRC_Params.SSRC_Fs_In = LVM_FS_8000;
  pInstance->SSRC_Params.SSRC_Fs_Out = LVM_FS_16000;
  pInstance->SSRC_Params.SSRC_NrOfChannels = LVM_MONO;
  pInstance->SSRC_Params.Quality = SSRC_QUALITY_VERY_HIGH;
  
  /* Get samples to process */
  ReturnStatus = SSRC_GetNrSamples(SSRC_NR_SAMPLES_MIN, &(pInstance->SSRC_Params));
  if(ReturnStatus != SSRC_OK)
  {
    MSG_1(MSG_SSID_QDSP6, 
          DBG_FATAL_PRIO, 
          "LVVEFQ_BE_SSRC_init - ERROR - SSRC_GetNrSamples: %d", 
          (int32_t)ReturnStatus);
    return ADSP_EFAILED;
  }
    
  pInstance->SSRC_Params.NrSamplesIn = LVVEFQ_BE_SSRC_BLOCK_SIZE/2;
  pInstance->SSRC_Params.NrSamplesOut = LVVEFQ_BE_SSRC_BLOCK_SIZE;
  
  /* Allocate scratch memory */
  ReturnStatus = SSRC_GetScratchSize(&(pInstance->SSRC_Params), &(pInstance->ScratchSize));
  if(ReturnStatus != SSRC_OK)
  {
    MSG_1(MSG_SSID_QDSP6, 
          DBG_FATAL_PRIO, 
          "LVVEFQ_BE_SSRC_init - ERROR - SSRC_GetScratchSize: %d", 
          (int32_t)ReturnStatus);
    return ADSP_EFAILED;
  }

#if defined(LVVEFQ_DEBUG_MSG)
  MSG_1(MSG_SSID_QDSP6, 
        DBG_FATAL_PRIO, 
        "LVVEFQ_BE_SSRC_init - Scratch memory allocated :%d", 
        (int32_t)pInstance->ScratchSize);  
#endif  
  
  return (uint32_t)pInstance->ScratchSize;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_set_mem(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                  int8 *pMemAddr)
{
  ADSPResult result = ADSP_EOK;

  pInstance->pScratch = (SSRC_Scratch_t *)pMemAddr;

  return result;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_get_kpps(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                   uint32_t* kpps_ptr)
{
  ADSPResult result = ADSP_EOK;
  
  if((NULL != pInstance) && (NULL != kpps_ptr))
  {
    *kpps_ptr = VOICE_LVVEFQ_BE_SSRC_KPPS; 
  }
  else
  {
    result = ADSP_EBADPARAM;
  }
  return result;  
}

#endif // LVVE


