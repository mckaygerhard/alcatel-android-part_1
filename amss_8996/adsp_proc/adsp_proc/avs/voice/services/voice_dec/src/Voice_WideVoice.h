#ifndef CWIDEVOICE_H
#define CWIDEVOICE_H
/*========================================================================
** @file Voice_WideVoice.h
  This file contains internal declarations common to all parts of the media
  module.  It is meant to be used only by the media module implementation.
  Nothing else should include it.

  This media module does not expose a clock.
*/
/****************************************************************************
Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_WideVoice.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/25/09   cp       Created file for WideVoice

========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */


#include "wve_api.h"
#include "qurt_elite.h"

//Structure as per ISOD (not as per c-sim)
struct wide_voice_param
{
   uint16_t adaptive_bass_boost_voiced;
   uint16_t fixed_bass_boost_9dB;
   int16_t high_band_contrib;
   int16_t low_band_contrib;
   int16_t nfact;
   int16_t noise_mix_factor_alpha;
   int16_t noise_mix_factor_beta;
   int16_t nReserved1;
   uint32_t silence_hb_egy_attn;
   int16_t smf_sens_cntrl;
   int16_t speech_buffer_len;
   int16_t tilt_scal_fac;
   int16_t nReserved2;
   int32_t unvoiced_highband_gain_cap;
   int16_t voiced_egy_map_alpha;
   int16_t nReserved3;
   int32_t voiced_egy_map_beta;
   int16_t voiced_pitchgain_limit;
   int16_t voiced_tilt_limit;
};



/* =======================================================================

   DATA DECLARATIONS

   ========================================================================== */
typedef struct {
   void*               wve_data_struct_ptr;
   wvConfigStruct      wve_config_struct_t;
   wide_voice_param      wve_config_intf_struct_t;
   //int16_t                enable_wide_voice;         // not required now as there is common BBWE flag
} voice_wve_struct_t;

/* =======================================================================
 **                          Function Declarations
 ** ======================================================================= */

ADSPResult voice_wve_process (voice_wve_struct_t *wve_struct_ptr,
      /**< This is ptr to WVE state
        structure. */
      int16_t *out_ptr,
      /**< This output parameter is a
        pointer to output Buffer.*/
      int16_t *in_ptr,
      /**< This input parameter is a
        pointer to input Buffer.*/
      int32_t in_size,
      /**< This inputparameter is the
        pointer to the size of the Buffer.*/
      int16_t out_buff_size,
      /* size of output buffer */
      int8_t wv_v1_enable
      /*  wv_v1_enable : wv v1 enable flag*/
      );
ADSPResult voice_wve_init (voice_wve_struct_t *wve_struct_ptr);

ADSPResult voice_wve_set_param (voice_wve_struct_t *wve_struct_ptr,uint32_t param_id,
      char* param_buffer_ptr, int32_t param_buf_len);

ADSPResult voice_wve_get_param (voice_wve_struct_t *wve_struct_ptr,uint32_t param_id,
      char* param_buffer_ptr, int32_t param_buf_len, int32_t* params_buffer_len_req_ptr, int8_t wv_v1_enable);

ADSPResult voice_wve_end (voice_wve_struct_t *wve_struct_ptr);

ADSPResult voice_wve_set_mem(voice_wve_struct_t *wve_struct_ptr,int8 *pMemAddr,uint32_t nSize);

ADSPResult voice_wve_get_kpps(voice_wve_struct_t* wve_struct_ptr, uint32_t* kpps_ptr, int8_t wv_v1_enable);

ADSPResult voice_wve_get_delay(voice_wve_struct_t* wve_struct_ptr, uint32_t* delay_ptr, int8_t wv_v1_enable);

#endif /* CWideVoice_H */

