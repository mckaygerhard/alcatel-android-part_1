#ifndef VOICE_CTMRX_H
#define VOICE_CTMRX_H
/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_CtmRx.h#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
08/05/2010     dp      Created
========================================================================== */
#include "Elite.h"
#include "qurt_elite.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus
#include "voice_resampler.h"

typedef struct {

  uint8  enable;                        // Enable/Disable CTM Processing
  int32_t  sync_recover_rx;                 // flag to Resync CTM
  int32_t  ctm_character_transmitted;       // flag to indicate CTM Tx was sent
  int32_t  enquiry_from_far_end_detected;     // detect flag of Enquiry burst at Rx
  int32_t  ctm_from_far_end_detected;         // detect flag of CTM from far end
  uint8  rx_tty_detected;                 // flag if Rx TTY was received
  voice_resampler_config_struct_t down_samp_config; //CTM downsampler config struct for Rx
  voice_resampler_config_struct_t up_samp_config; //CTM upsampler config struct for Rx
  void   *down_samp_mem_ptr;             // CTM downsampler memory ptr for Rx;
  void   *up_samp_mem_ptr;               // CTM upsampler memory ptr for Rx
  void   *ctm_rx_struct_instance_ptr;          // structure to hold CTM lib state
  uint32_t session_id;
  uint16_t sampling_rate;
} ctm_rx_struct_t;

/* initialize Ctm module, allocate memory */
ADSPResult voice_ctm_rx_init(ctm_rx_struct_t* ctm_rx_struct_ptr, uint32_t session_id);

/* process 20 ms buffer, assume in place */
ADSPResult voice_ctm_rx_process(ctm_rx_struct_t* ctm_rx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples);

/* destroy Ctm module, free memory */
ADSPResult voice_ctm_rx_end(ctm_rx_struct_t* ctm_rx_struct_ptr);

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_ctm_rx_set_enable(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8 enable);

/* set WB vs NB */
ADSPResult voice_ctm_rx_commit_format(ctm_rx_struct_t* ctm_rx_struct_ptr, uint16 sampling_rate);

/* resync CTM during handover */
ADSPResult voice_ctm_rx_resync(ctm_rx_struct_t* ctm_rx_struct_ptr);

/* set when CTM char transmitted */
ADSPResult voice_ctm_rx_set_ctm_char_transmitted(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8 ctm_char_tx);

/* get status of CTM detect on far end, and whether status has been notified */
ADSPResult voice_ctm_rx_get_ctm_from_far_endDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8 *ctm_from_far_end_detected);

/* get status of Enquiry detect from far end */
ADSPResult voice_ctm_rx_get_enquiry_from_far_endDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8 *enquiry_from_far_end_detected);

/* get status of Rx TTY detected end */
ADSPResult voice_ctm_rx_get_rxTTYDetected(ctm_rx_struct_t* ctm_rx_struct_ptr, uint8 *rxTTYDetected);

/* initialize Ctm resampler, allocate memory */
ADSPResult voice_ctm_rx_resampler_init(ctm_rx_struct_t* ctm_rx_struct_ptr);

/* Return KPPS for CTMRx */
ADSPResult voice_ctm_rx_get_kpps(ctm_rx_struct_t* ctm_rx_struct_ptr,uint32_t* kpps_ptr, uint8_t enable, uint16_t sampling_rate);
#ifdef __cplusplus
}
#endif //__cplusplus
#endif /* VOICE_DTMF_DETECT_H */

