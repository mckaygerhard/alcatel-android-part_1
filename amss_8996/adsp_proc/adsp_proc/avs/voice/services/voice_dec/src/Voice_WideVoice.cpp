/*============================================================================

                          W I D E V O I C E    W R A P P E R
*//** @file CWideVoice.cpp
  This file contains the definitions of the CWideVoice class methods.

@par EXTERNALIZED FUNCTIONS
  (none)

@par INITIALIZATION AND SEQUENCING REQUIREMENTS
  (none)

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*========================================================================*/

/*============================================================================
                             Edit History

$Header:

when       who     what, where, why
--------   ---     -------------------------------------------------------
30/06/10   pc       Created file.
============================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "Voice_WideVoice.h"
#include "adsp_vparams_api.h"
#include "VoiceCmnUtils.h"

#define VOICE_PARAM_MOD_ENABLE_SIZE sizeof(int32_t)

/* Peak kpps taken */
#define VOICE_WVE_NB_KPPS (8120) 

/* Algorithmic delay in microseconds */
#define VOICE_WVE_NB_DELAY  (0)  //(4000)

ADSPResult static voice_wve_int_param_to_lib_param (voice_wve_struct_t *wve_struct_ptr);
ADSPResult static voice_wve_lib_param_to_int_param (voice_wve_struct_t *wve_struct_ptr);

ADSPResult voice_wve_init (voice_wve_struct_t *wve_struct_ptr)
{
   int32_t lib_result = 0;
   lib_result = wve_init(wve_struct_ptr->wve_data_struct_ptr);
   if (ADSP_FAILED(lib_result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: WVE initialization failed, %d", (int)lib_result);
      return lib_result;
   }

   wve_config_default_init(&(wve_struct_ptr->wve_config_struct_t));

   //if getparam is called before setparam
   voice_wve_lib_param_to_int_param(wve_struct_ptr);

   return ADSP_EOK;
}


ADSPResult voice_wve_process (voice_wve_struct_t *wve_struct_ptr,
      int16_t *out_ptr, int16_t *in_ptr, int32_t in_size, int16_t out_buff_size, int8_t wv_v1_enable)
{
   if(TRUE == wv_v1_enable)
   {
#if defined(VOICE_DBG_MSG)
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Rx WVE processing");
#endif
      wve_process(in_ptr, out_ptr, wve_struct_ptr->wve_data_struct_ptr,
            &(wve_struct_ptr->wve_config_struct_t));
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: WVE Process Called");

   }
   else
   {
      if (in_ptr != out_ptr)
      {
         memscpy(out_ptr, out_buff_size, in_ptr, in_size);
      }
   }

   return ADSP_EOK;
}



ADSPResult voice_wve_set_param (voice_wve_struct_t *wve_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len)
{
   switch(param_id)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
            int32_t lib_result = 0;
            if (VOICE_PARAM_MOD_ENABLE_SIZE != param_buf_len)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: wve: mod_enable required size = 4, given size = %ld",param_buf_len);
               return ADSP_EBADPARAM;
            }

            if ( TRUE == *param_buffer_ptr )
            {
               lib_result = wve_init(wve_struct_ptr->wve_data_struct_ptr);
	       if (0 != lib_result)
	       {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wve_init : Failed with result (%d)",lib_result);
                  return ADSP_EUNSUPPORTED;
                }
            }
            break;
         }
      case VOICE_PARAM_WV:
         {
            if(param_buf_len != sizeof(wide_voice_param))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: wide_voice_param required size = %d, given size = %ld",sizeof(wide_voice_param), param_buf_len);
               return ADSP_EBADPARAM;
            }
            //memcpy from buffer you got to Interface structure
            memscpy(&wve_struct_ptr->wve_config_intf_struct_t, sizeof(wide_voice_param), param_buffer_ptr,sizeof(wide_voice_param));
            //copy from interface structure to c-sim structure
            voice_wve_int_param_to_lib_param(wve_struct_ptr);
            break;
         }
      default:
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_wve_set_param:Bad Parameter ID");
         return ADSP_EBADPARAM;
   }
   return ADSP_EOK;
}


ADSPResult voice_wve_get_param (voice_wve_struct_t *wve_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len, int32_t* params_buffer_len_req_ptr, int8_t wv_v1_enable)
{
   switch(param_id)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {

            *params_buffer_len_req_ptr = sizeof(uint32_t); //2 bytes. Enable is a uint32_t in ISOD

            if(*params_buffer_len_req_ptr > param_buf_len )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_wve_get_param required size = %ld, given size = %ld",*params_buffer_len_req_ptr, param_buf_len);
               return ADSP_ENOMEMORY;
            }
            *((int32_t*)param_buffer_ptr) = 0;       // Clearing the whole buffer

            *((int16_t*)param_buffer_ptr) = wv_v1_enable;

            break;
         }
      case VOICE_PARAM_WV:
         {

            *params_buffer_len_req_ptr = sizeof(wide_voice_param);

            if(*params_buffer_len_req_ptr > param_buf_len )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_wve_get_param required size = %ld, given size = %ld",*params_buffer_len_req_ptr, param_buf_len);
               return ADSP_ENOMEMORY;
            }

            //memcpy from Interface structure to buffer you got
            memscpy(param_buffer_ptr, param_buf_len, &wve_struct_ptr->wve_config_intf_struct_t,sizeof(wide_voice_param));
            wide_voice_param *wv_cal_param_buf_ptr=(wide_voice_param *)param_buffer_ptr;
            // Clearing Reserved Field
            wv_cal_param_buf_ptr->nReserved1=0;
            wv_cal_param_buf_ptr->nReserved2=0;
            wv_cal_param_buf_ptr->nReserved3=0;

            break;
         }
      default:
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_wve_get_param:Bad Parameter ID");
         return ADSP_EBADPARAM;
   }
   return ADSP_EOK;
}

//copy interface provided values to c-sim configuration structure
ADSPResult static voice_wve_int_param_to_lib_param (voice_wve_struct_t *wve_struct_ptr)
{

   wve_struct_ptr->wve_config_struct_t.adaptive_bass_boost_voiced = wve_struct_ptr->wve_config_intf_struct_t.adaptive_bass_boost_voiced;
   wve_struct_ptr->wve_config_struct_t.fixed_bass_boost_9dB       = wve_struct_ptr->wve_config_intf_struct_t.fixed_bass_boost_9dB;
   wve_struct_ptr->wve_config_struct_t.high_band_contrib          = wve_struct_ptr->wve_config_intf_struct_t.high_band_contrib;
   wve_struct_ptr->wve_config_struct_t.low_band_contrib           = wve_struct_ptr->wve_config_intf_struct_t.low_band_contrib;
   wve_struct_ptr->wve_config_struct_t.nfact                      = wve_struct_ptr->wve_config_intf_struct_t.nfact;
   wve_struct_ptr->wve_config_struct_t.noise_mix_factor_alpha     = wve_struct_ptr->wve_config_intf_struct_t.noise_mix_factor_alpha;
   wve_struct_ptr->wve_config_struct_t.noise_mix_factor_beta      = wve_struct_ptr->wve_config_intf_struct_t.noise_mix_factor_beta;
   wve_struct_ptr->wve_config_struct_t.silence_hb_egy_attn        = wve_struct_ptr->wve_config_intf_struct_t.silence_hb_egy_attn;
   wve_struct_ptr->wve_config_struct_t.smf_sens_cntrl             = wve_struct_ptr->wve_config_intf_struct_t.smf_sens_cntrl;
   wve_struct_ptr->wve_config_struct_t.speech_buffer_len          = wve_struct_ptr->wve_config_intf_struct_t.speech_buffer_len;
   wve_struct_ptr->wve_config_struct_t.tilt_scal_fac              = wve_struct_ptr->wve_config_intf_struct_t.tilt_scal_fac;
   wve_struct_ptr->wve_config_struct_t.unvoiced_highband_gain_cap = wve_struct_ptr->wve_config_intf_struct_t.unvoiced_highband_gain_cap;
   wve_struct_ptr->wve_config_struct_t.voiced_egy_map_alpha       = wve_struct_ptr->wve_config_intf_struct_t.voiced_egy_map_alpha;
   wve_struct_ptr->wve_config_struct_t.voiced_egy_map_beta        = wve_struct_ptr->wve_config_intf_struct_t.voiced_egy_map_beta;
   wve_struct_ptr->wve_config_struct_t.voiced_pitchgain_limit     = wve_struct_ptr->wve_config_intf_struct_t.voiced_pitchgain_limit;
   wve_struct_ptr->wve_config_struct_t.voiced_tilt_limit          = wve_struct_ptr->wve_config_intf_struct_t.voiced_tilt_limit;

   return ADSP_EOK;
}


//copy interface provided values to c-sim configuration structure
ADSPResult static voice_wve_lib_param_to_int_param (voice_wve_struct_t *wve_struct_ptr)
{

   wve_struct_ptr->wve_config_intf_struct_t.adaptive_bass_boost_voiced = wve_struct_ptr->wve_config_struct_t.adaptive_bass_boost_voiced;
   wve_struct_ptr->wve_config_intf_struct_t.fixed_bass_boost_9dB       = wve_struct_ptr->wve_config_struct_t.fixed_bass_boost_9dB;
   wve_struct_ptr->wve_config_intf_struct_t.high_band_contrib          = wve_struct_ptr->wve_config_struct_t.high_band_contrib;
   wve_struct_ptr->wve_config_intf_struct_t.low_band_contrib           = wve_struct_ptr->wve_config_struct_t.low_band_contrib;
   wve_struct_ptr->wve_config_intf_struct_t.nfact                      = wve_struct_ptr->wve_config_struct_t.nfact;
   wve_struct_ptr->wve_config_intf_struct_t.noise_mix_factor_alpha     = wve_struct_ptr->wve_config_struct_t.noise_mix_factor_alpha;
   wve_struct_ptr->wve_config_intf_struct_t.noise_mix_factor_beta      = wve_struct_ptr->wve_config_struct_t.noise_mix_factor_beta;
   wve_struct_ptr->wve_config_intf_struct_t.silence_hb_egy_attn        = wve_struct_ptr->wve_config_struct_t.silence_hb_egy_attn;
   wve_struct_ptr->wve_config_intf_struct_t.smf_sens_cntrl             = wve_struct_ptr->wve_config_struct_t.smf_sens_cntrl;
   wve_struct_ptr->wve_config_intf_struct_t.speech_buffer_len          = wve_struct_ptr->wve_config_struct_t.speech_buffer_len;
   wve_struct_ptr->wve_config_intf_struct_t.tilt_scal_fac              = wve_struct_ptr->wve_config_struct_t.tilt_scal_fac;
   wve_struct_ptr->wve_config_intf_struct_t.unvoiced_highband_gain_cap = wve_struct_ptr->wve_config_struct_t.unvoiced_highband_gain_cap;
   wve_struct_ptr->wve_config_intf_struct_t.voiced_egy_map_alpha       = wve_struct_ptr->wve_config_struct_t.voiced_egy_map_alpha;
   wve_struct_ptr->wve_config_intf_struct_t.voiced_egy_map_beta        = wve_struct_ptr->wve_config_struct_t.voiced_egy_map_beta;
   wve_struct_ptr->wve_config_intf_struct_t.voiced_pitchgain_limit     = wve_struct_ptr->wve_config_struct_t.voiced_pitchgain_limit;
   wve_struct_ptr->wve_config_intf_struct_t.voiced_tilt_limit          = wve_struct_ptr->wve_config_struct_t.voiced_tilt_limit;

   return ADSP_EOK;
}

ADSPResult voice_wve_end (voice_wve_struct_t *wve_struct_ptr)
{
   wve_struct_ptr->wve_data_struct_ptr = NULL;
   return ADSP_EOK;
}

ADSPResult voice_wve_set_mem(voice_wve_struct_t *wve_struct_ptr,int8 *pMemAddr,uint32_t nSize)
{
   uint32_t wveSize = wve_compute_data_struct_size();
   if (nSize < wveSize)
   {
	   MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  WVE memory size insufficient - allocated (%ld) required (%ld)", nSize,wveSize);
	   return ADSP_EFAILED;
   }

   wve_struct_ptr->wve_data_struct_ptr = (void *) pMemAddr;
   return ADSP_EOK;

}

ADSPResult voice_wve_get_kpps(voice_wve_struct_t* wve_struct_ptr, uint32_t* kpps_ptr,int8_t wv_v1_enable)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= wve_struct_ptr) && (NULL!= kpps_ptr))
   {  
      *kpps_ptr = 0;
      if(TRUE == wv_v1_enable)
      {
         // Not using sampling rate tables here, as Widevoice enablement 
         // is a sufficient check for KPPS
         *kpps_ptr = VOICE_WVE_NB_KPPS; 
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

ADSPResult voice_wve_get_delay(voice_wve_struct_t* wve_struct_ptr, uint32_t* delay_ptr,int8_t wv_v1_enable)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= wve_struct_ptr) && (NULL!= delay_ptr))
   {  
      *delay_ptr = 0;
      if(TRUE == wv_v1_enable)
      {
         *delay_ptr = VOICE_WVE_NB_DELAY; 
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

