/****************************************************************************************/
/*  Copyright (c) 2004-2013 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: $                                                                       */
/*     $Revision: $                                                                     */
/*     $Date: $                                                                         */
/*                                                                                      */
/****************************************************************************************/

#include "qurt_elite.h"

#ifndef __LVVEFQ_BE_SSRC_H__
#define __LVVEFQ_BE_SSRC_H__

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/

#include "SSRC.h"

/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/

#define LVVEFQ_BE_SSRC_BLOCK_SIZE 320

/****************************************************************************************/
/*                                                                                      */
/*  Types                                                                               */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/

typedef struct 
{
  SSRC_Params_t SSRC_Params;      /* Memory for init parameters                   */
  LVM_INT32 ScratchSize;          /* The size of the scratch memory               */
  SSRC_Scratch_t *pScratch;       /* Pointer to scratch memory                    */
  SSRC_Instance_t SSRC_Instance;  /* Allocate memory for the instance             */
  LVM_INT16 *pInputInScratch;     /* Pointer to input in the scratch buffer       */
  LVM_INT16 *pOutputInScratch;    /* Pointer to the output in the scratch buffer  */
  int16_t enable_wide_voice;
} LVVEFQ_BE_SSRC_Instance_st;


/****************************************************************************************/
/*                                                                                      */
/*  Function Prototypes                                                                 */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_process(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                  int16_t *out_ptr,
                                  int16_t *in_ptr,
                                  int32_t in_size);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_init(LVVEFQ_BE_SSRC_Instance_st *pInstance);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_end(LVVEFQ_BE_SSRC_Instance_st *pInstance);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
uint32_t LVVEFQ_BE_SSRC_get_size(LVVEFQ_BE_SSRC_Instance_st *pInstance);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_set_mem(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                  int8 *pMemAddr);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_BE_SSRC_get_kpps(LVVEFQ_BE_SSRC_Instance_st *pInstance, 
                                   uint32_t* kpps_ptr);


#ifdef __cplusplus
}
#endif // __cplusplus

#endif  //#ifndef __LVVEFQ_BE_SSRC_H__

