#ifndef VOICE_OOBTTYRX_H
#define VOICE_OOBTTYRX_H
/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_OOBTTYRx.h#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
03/08/2013     rojha      Created
========================================================================== */
#include "Elite.h"
#include "qurt_elite.h"
#include "voice_resampler.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#define LTE_TTY_CHAR_FIFO_LENGTH 6

typedef struct {

  uint8  enable;                        // Enable/Disable Rx TTY Processing for VoLTE
  uint32  sampling_rate;                      // sampling_rate
  uint8  rxTTYActive;                 // flag if Rx TTY is active
  void   *lte_tty_fifo_ptr;            // structure pointer for FIFO
  voice_resampler_config_struct_t up_samp_config; //OOB TTY upsampler config struct for Rx
  void   *up_samp_mem_ptr;               // OOB TTY upsampler memory ptr for Rx
  void   *oobtty_rx_struct_instance_ptr;          // structure to hold LTE TTY lib state
  int16  fifoBuffer[LTE_TTY_CHAR_FIFO_LENGTH];
  uint32_t session_id;

} oobtty_rx_struct_t;

/* allocate memory for VoLTE TTY Rx module */
ADSPResult voice_oobtty_rx_mem_alloc(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

/* initialize VoLTE TTY Rx module */
void voice_oobtty_rx_init(oobtty_rx_struct_t* oobtty_rx_struct_ptr, uint32_t session_id);

/* process 20 ms buffer, assume in place */
ADSPResult voice_oobtty_rx_process(oobtty_rx_struct_t* oobtty_rx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples);

/* destroy VoLTE TTY Rx module, free memory */
ADSPResult voice_oobtty_rx_end(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_oobtty_rx_set_config(oobtty_rx_struct_t* oobtty_rx_struct_ptr, uint8 enable, uint32 samp_rate);

/* get rx activity status */
uint8_t voice_oobtty_get_rx_detected(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

/* push the new rx character in fifo */
int32_t voice_oobtty_push_char(oobtty_rx_struct_t* oobtty_rx_struct_ptr,int16_t* rxChar);

/* reset fifo for LTE TTY */
void voice_oobtty_reset_fifo(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

ADSPResult voice_oobtty_rx_get_kpps(oobtty_rx_struct_t* oobtty_rx_struct_ptr,uint32_t* kpps_ptr,uint8_t enable,uint16_t sampling_rate);

/* resampler init for OOB TTY */
ADSPResult voice_oobtty_rx_resampler_init(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

#ifdef __cplusplus
}
#endif //__cplusplus
#endif /* VOICE_DTMF_DETECT_H */

