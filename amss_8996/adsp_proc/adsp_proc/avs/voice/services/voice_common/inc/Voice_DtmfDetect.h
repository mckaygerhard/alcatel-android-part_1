#ifndef VOICE_DTMF_DETECT_H
#define VOICE_DTMF_DETECT_H

/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_common/inc/Voice_DtmfDetect.h#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
15/07/2010  sb         calibration added.
08/07/2010  sb         converted from C++ to C wrapper
03/04/2010  cpoliset   created the file.
========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */

#include "Elite.h"
#include "VoiceCmnUtils.h"
#include "voice_resampler.h"

/* =======================================================================
                        DATA DECLARATIONS
========================================================================== */
typedef struct dtmf_detect_struct_t{
    int16_t   dtmf_detect_static_mem[42];
    int16_t   tone_status_buf[8];       // two DTMF tone status payload of size 8 bytes each
    int16_t   enable_dtmf_detect;
    int16_t   prev_tone_status;         //required for deciding change in tone
    int16_t   dtmf_tone_status;
    int16_t   circ_buf[160*2];          // Local Circular Data Buffer Pointer.
                                      // narrow band vocoder frame size * two frames.
    circbuf_struct circ_struct_dtmf_det;  // Circular buffer structure for DTMF detection
    circbuf_struct tone_status_circbuf_struct;  // Circular buffer structure for Tone status
    voice_resampler_config_struct_t      dtmf_det_config_rs;   // Downsampler WB/FB DTMF Detection;
    void      *dtmf_det_resamp_channel_mem_ptr;   // Downsampler WB/FB DTMF Detection;
    int16_t   dtmf_det_resamp_channel_size;       //Channel size required for resampler channel memory.
    uint32_t sampling_rate;
} dtmf_detect_struct_t;

typedef enum
{
	MAGNITUDE_THRESH       =  0,
	FW_TWIST_FACTOR        =  1,
	REV_TWIST_FACTOR       =  2,
	FREQ_RATIO_1           =  3,
	FREQ_RATIO_2           =  4,
	FREQ_RATIO_3           =  5,
	FREQ_RATIO_4           =  6,
	FREQ_RATIO_5           =  7,
	FREQ_RATIO_6           =  8,
	FREQ_RATIO_7           =  9,
	FREQ_RATIO_8           =  10,
	DETECTION_THRESH1      =  11,
	DETECTION_THRESH2      =  12,
	DETECTION_THRESH31     =  13,
	DETECTION_THRESH32     =  14,
	PARAM_L                =  15,
	PARAM_M                =  16,
	PARAM_N                =  17

} dtmf_detect_params_list;

typedef struct {
   int16_t    MagnitudeThresh;
   int16_t	   FwTwistFactor;
   int16_t	   RevTwistFactor;
   int16_t	   FreqRatio1;
   int16_t	   FreqRatio2;
   int16_t	   FreqRatio3;
   int16_t	   FreqRatio4;
   int16_t	   FreqRatio5;
   int16_t	   FreqRatio6;
   int16_t	   FreqRatio7;
   int16_t	   FreqRatio8;
   int16_t	   DetectionThresh1;
   int16_t	   DetectionThresh2;
   int16_t	   DetectionThresh31;
   int16_t	   DetectionThresh32;
   int16_t	   ParamL;
   int16_t	   ParamM;
   int16_t	   ParamN;
} dtmf_detect_interface_params;

/* =======================================================================
**                          Function Declarations
** ======================================================================= */

ADSPResult voice_dtmf_detect_init(dtmf_detect_struct_t *dtmf_detect_ptr,uint32_t sampling_rate,uint32_t input_frame_samples);
void voice_dtmf_detect_end(dtmf_detect_struct_t *dtmf_detect_ptr);
void voice_dtmf_detect_process(dtmf_detect_struct_t *dtmf_detect_ptr,
                               int16_t *out_ptr,
                               int16_t *in_ptr,
                               uint16_t no_of_samples,
                               uint16_t sampling_rate);
uint8 voice_dtmf_detect_get_tone_status(dtmf_detect_struct_t *dtmf_detect_ptr);
ADSPResult voice_dtmf_detect_set_param(dtmf_detect_struct_t *dtmf_detect_ptr,
                                       int8_t *params_buffer_ptr,
                                       uint32_t param_id,
                                       uint16_t param_size);
ADSPResult voice_dtmf_detect_get_param(dtmf_detect_struct_t *dtmf_detect_ptr,
                                       int8_t *params_buffer_ptr,
                                       uint32_t param_id,
                                       int32_t buffer_size,
                                       uint16_t *param_size_ptr);
//Note following API should be called only for resampler reconfiguration when memory is already allocated.
ADSPResult dtmf_det_resampler_init(dtmf_detect_struct_t *dtmf_detect_ptr,uint32_t sampling_rate);

/* Returns KPPS */
ADSPResult voice_dtmf_det_get_kpps(dtmf_detect_struct_t* dtmf_detect_ptr, uint32_t* kpps_ptr, uint16_t sampling_rate);

#endif /* VOICE_DTMF_DETECT_H */

