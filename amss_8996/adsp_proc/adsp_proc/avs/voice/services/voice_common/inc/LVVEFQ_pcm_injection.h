/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp34663 $                                                              */
/*     $Revision:  $                                                               */
/*     $Date:  $                            */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Header file for the application layer interface of the PCM injection module         */
/*                                                                                      */
/*  This files includes all definitions, types, structures and function prototypes      */
/*  required by the calling layer. All other types, structures and functions are        */
/*  private.                                                                            */
/*                                                                                      */
/****************************************************************************************/
#ifdef LVVE
#ifndef __LVVEFQ_PCM_INJECTION_H__
#define __LVVEFQ_PCM_INJECTION_H__

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/

#include "qurt_elite.h"
#include "LVC_Types.h"
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Types                                                                               */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/

typedef enum
{
    VEFQ_INJECT_WHITENOISE = 0,
    VEFQ_INJECT_WHITEBURST,
    VEFQ_INJECT_SINE_1K,
    VEFQ_INJECT_SINE_1K_RAMPUP,
    VEFQ_INJECT_SINE_1K_RAMPDOWN,
    VEFQ_INJECT_TONE_PLUS_SILENCE,
	VEFQ_INJECT_SPEECH,
	VEFQ_INJECT_SPEECH_PLUS_SILENCE,
    VEFQ_INJECT_TYPE_MAX
}LVVEFQ_injection_type;

typedef struct {
    uint16_t enable;
    uint16_t mode;
    int16_t *ptr;
    uint32_t size;
    uint32_t offset;
    uint32_t processedsamples;
    uint16_t stepindex;
    int16_t  shift_bit;
} LVVEFQ_injection_mode_st;

typedef struct {
    LVVEFQ_injection_mode_st in_st;
    LVVEFQ_injection_mode_st out_st;
    uint32_t sample_rate;
} LVVEFQ_injection_st;

typedef void (*elite_vefq_injection_processing_func) (LVVEFQ_injection_mode_st* /*pInjectionInstance*/, int16_t* /*IOBuffer*/, uint32_t /*ProcessBlockSize*/);

/****************************************************************************************/
#define SAMPLES_FACTOR_FS8000          8         //samples count
#define SAMPLES_FACTOR_FS16000         16        //samples count
#define SAMPLES_FACTOR_FS48000         48        //samples count

#define RAMP_STEP_MAX                      12
#define RAMP_STEP_DURATION                 2000     //2S
#define TONE_DURATION                      2000     //2S  
#define INTERVAL_DURATION                  450      //450ms

#define SAMPLERATE_8K     8000
#define SAMPLERATE_16K    16000
#define SAMPLERATE_48K    48000

const float RampDownRatio[RAMP_STEP_MAX] =
{
    0.00,
    0.794,   //-2db
    0.631,   //-4db
    0.501,   //-6db
    0.398,   //-8db
    0.316,   //-10db 
    0.251,   //-12db
    0.200,   //-14db
    0.158,   //-16db
    0.126,   //-18db
    0.100,    //-20db
    0.00  
};

/****************************************************************************************/
/*                                                                                      */
/*  Function Prototypes                                                                 */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_config(LVVEFQ_injection_st *pInstance, uint32_t sample_rate);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_init(LVVEFQ_injection_st *pInstance);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_process(LVVEFQ_injection_st *pInstance,
                              int16_t *IOBuffer,
                              uint32_t ProcessBlockSize);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_injection_set_param(LVVEFQ_injection_st *pInstance,
                                      int8_t *params_buffer_ptr,
                                      uint32_t param_id,
                                      uint16_t param_size);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_injection_get_param(LVVEFQ_injection_st *pInstance,
                                      int8_t *params_buffer_ptr,
                                      uint32_t param_id,
                                      int32_t inp_param_buf_size,
                                      uint16_t *param_size_ptr);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __LVVEFQ_PCM_INJECTION_H__
#endif //LVVE
