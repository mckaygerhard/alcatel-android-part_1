/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp34663 $                                                              */
/*     $Revision:  $                                                               */
/*     $Date:  $                            */
/*                                                                                      */
/****************************************************************************************/
const uint16_t Sine_1khz_Fs8000_12db[] =
{
    0x0000, 0x16BC, 0x2027, 0x16BC, 0x0000, 0xE944, 0xDFD9, 0xE944,
};

const uint16_t Sine_1khz_Fs16000_12db[] =
{
    0x0000,0x0C4E,0x16BC,0x1DB4,0x2027,0x1DB4,0x16BC,0x0C4E, 
    0x0000, 0xF3B2, 0xE944, 0xE24C, 0xDFD9, 0xE24C, 0xE944, 0xF3B2,
};

const uint16_t Sine_1khz_Fs48000_12db[] =
{
    0x0000, 0x0432, 0x0852, 0x0c4e, 0x1014, 0x1393, 0x16bd, 0x1982, 
    0x1bd8, 0x1db4, 0x1f0e, 0x1fe1, 0x2027, 0x1fe0, 0x1f0e, 0x1db5,
    0x1bd7, 0x1982, 0x16bc, 0x1393, 0x1014, 0x0c4d, 0x0851, 0x0432, 
    0x0000, 0xfbcd, 0xf7ae, 0xf3b3, 0xefed, 0xec6e, 0xe944, 0xe67e,
    0xe428, 0xe24b, 0xe0f2, 0xe01f, 0xdfda, 0xe020, 0xe0f1, 0xe24c, 
    0xe428, 0xe67e, 0xe944, 0xec6d, 0xefec, 0xf3b2, 0xf7ad, 0xfbce,
};

const uint16_t Sine_1khz_Fs8000_1db[] =
{
    0x0000, 0x50AB, 0x7214, 0x50AA, 0x0000, 0xAf56, 0x8DED, 0xAF56,
};

const uint16_t Sine_1khz_Fs16000_1db[] =
{
    0x0000,0x306C,0x5979,0x74E6,0x7E88,0x74E6,0x5979,0x306C,
    0x0000, 0xCF94, 0xA687, 0x8B1A, 0x8178, 0x8B1A, 0xA687, 0xCF94,
};

const uint16_t Sine_1khz_Fs48000_1db[] =
{
    0x0000, 0x0ee4, 0x1d87, 0x2ba8, 0x390a, 0x4572, 0x50aa, 0x5a80,
    0x62cb, 0x6964, 0x6e31, 0x7119, 0x7214, 0x711a, 0x6e30, 0x6965,
    0x62cb, 0x5a81, 0x50a9, 0x4572, 0x390a, 0x2ba7, 0x1d87, 0x0ee3,
    0x0000, 0xf11c, 0xe279, 0xd459, 0xc6f7, 0xba8e, 0xaf56, 0xa57f,
    0x9d35, 0x969c, 0x91d0, 0x8ee6, 0x8ded, 0x8ee6, 0x91cf, 0x969b,
    0x9d35, 0xa580, 0xaf56, 0xba8e, 0xc6f6, 0xd458, 0xe27a, 0xf11d,
};
