/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp34663 $                                                              */
/*     $Revision:  $                                                               */
/*     $Date:  $                            */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#ifdef LVVE
#include "EliteMsg_Util.h"
#include "adsp_vparams_api.h"
#include "LVVEFQ_pcm_injection.h"

/****************************************************************************************/
/*                                                                                      */
/*  define debug feature and function feature                                           */
/*                                                                                      */
/****************************************************************************************/
//#define NXP_INJECT_DEBUG_FTR
//#define NXP_INJECT_SPEECH_FTR

#include "LVVEFQ_1k_sine.h"
#include "LVVEFQ_white_noise_burst_8000.h"
#include "LVVEFQ_white_noise_burst_16000.h"
#include "LVVEFQ_white_noise_burst_48000.h"

#ifdef NXP_INJECT_SPEECH_FTR
#include "LVVEFQ_french_male_speech_8000.h"
#include "LVVEFQ_french_male_speech_16000.h"
#endif


void LVVEFQ_injection_WhiteNoise(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_WhiteBurst(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_RampDown_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_RampUp_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_TonePlusSilence(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_Speech(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);
void LVVEFQ_injection_SpeechPlusSilence(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize);

static elite_vefq_injection_processing_func pVEFQ_InjectionProcessHandler[VEFQ_INJECT_TYPE_MAX] =
{
    LVVEFQ_injection_WhiteNoise,             //VEFQ_INJECT_WHITENOISE
    LVVEFQ_injection_WhiteBurst,             //VEFQ_INJECT_WHITEBURST
    LVVEFQ_injection_Sine_1000Hz,            //VEFQ_INJECT_SINE_1K
    LVVEFQ_injection_RampUp_Sine_1000Hz,     //VEFQ_INJECT_SINE_1K_RAMPUP
    LVVEFQ_injection_RampDown_Sine_1000Hz,   //VEFQ_INJECT_SINE_1K_RAMPDOWN
    LVVEFQ_injection_TonePlusSilence,        //VEFQ_INJECT_TONE_PLUS_SILENCE
    LVVEFQ_injection_Speech,                 //VEFQ_INJECT_SPEECH  
    LVVEFQ_injection_SpeechPlusSilence,      //VEFQ_INJECT_SPEECH_PLUS_SILENCE
};

/****************************************************************************************/
/*                                                                                      */
/*  Macro definitions Constant / Define Declarations                                    */
/*                                                                                      */
/****************************************************************************************/
static const uint32_t LVVEFQ_INJECTION_PARAM_SIZE = sizeof(uint16_t)  /* enable */
                                                                                        + sizeof(uint16_t) /* mode */
                                                                                        + sizeof(uint16_t) /* shift_bit */
                                                                                        + sizeof(uint16_t); /* reserved */

/****************************************************************************************/
/*                                                                                      */
/*  Function Definitions                                                                */
/*                                                                                      */
/****************************************************************************************/

/*************************************************************************************************************************************************/
/****************************************************** private function begin ************************************************************************/
/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_injection_set_datasource(LVVEFQ_injection_mode_st *pInstance, uint32_t sample_rate, uint16_t mode)
{
    ADSPResult result = ADSP_EOK;
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_set_datasource   pInstance=0x%x    sample_rate=%d  mode=%d", pInstance, sample_rate, mode);

    if ((SAMPLERATE_8K != sample_rate) && (SAMPLERATE_16K != sample_rate) && (SAMPLERATE_48K != sample_rate))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_set_datasource   un-support sample_rate=%d", sample_rate);
        return ADSP_EUNSUPPORTED;
    }

    switch(mode)
    {
        case VEFQ_INJECT_WHITENOISE:
        case VEFQ_INJECT_WHITEBURST:
        {
            if (SAMPLERATE_8K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&white_noise_8k[0];
                pInstance->size = sizeof(white_noise_8k)/sizeof(int16_t);
            }
            else if (SAMPLERATE_16K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&white_noise_16k[0];
                pInstance->size = sizeof(white_noise_16k)/sizeof(int16_t);
            }
            else if (SAMPLERATE_48K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&white_noise_48k[0];
                pInstance->size = sizeof(white_noise_48k)/sizeof(int16_t);
            }
            
            break;
        }
        
        case VEFQ_INJECT_SINE_1K:
        case VEFQ_INJECT_TONE_PLUS_SILENCE:
        {
            if (SAMPLERATE_8K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs8000_12db[0];
                pInstance->size = sizeof(Sine_1khz_Fs8000_12db)/sizeof(int16_t);
            }
            else if (SAMPLERATE_16K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs16000_12db[0];
                pInstance->size = sizeof(Sine_1khz_Fs16000_12db)/sizeof(int16_t);
            }
            else if (SAMPLERATE_48K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs48000_12db[0];
                pInstance->size = sizeof(Sine_1khz_Fs48000_12db)/sizeof(int16_t);
            }

            break;
        }

        case VEFQ_INJECT_SINE_1K_RAMPUP:
        case VEFQ_INJECT_SINE_1K_RAMPDOWN:
        {
            if (VEFQ_INJECT_SINE_1K_RAMPUP == mode)
            {
                pInstance->stepindex = RAMP_STEP_MAX - 1;
            }
            else
            {
                pInstance->stepindex = 0;
            }

            if (SAMPLERATE_8K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs8000_1db[0];
                pInstance->size = sizeof(Sine_1khz_Fs8000_1db)/sizeof(int16_t);
            }
            else if (SAMPLERATE_16K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs16000_1db[0];
                pInstance->size = sizeof(Sine_1khz_Fs16000_1db)/sizeof(int16_t);
            }
            else if (SAMPLERATE_48K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&Sine_1khz_Fs48000_1db[0];
                pInstance->size = sizeof(Sine_1khz_Fs48000_1db)/sizeof(int16_t);
            }

            break;
        }
        
        case VEFQ_INJECT_SPEECH:
        case VEFQ_INJECT_SPEECH_PLUS_SILENCE:
        {
#ifdef NXP_INJECT_SPEECH_FTR
            if (SAMPLERATE_8K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&french_speech_8k[0];
                pInstance->size = sizeof(french_speech_8k)/sizeof(uint16_t);
            }
            else if (SAMPLERATE_16K == sample_rate)
            {
                pInstance->ptr = (int16_t*)&french_speech_16k[0];
                pInstance->size = sizeof(french_speech_16k)/sizeof(uint16_t);
            }
            else if (SAMPLERATE_48K == sample_rate)
            {
                pInstance->ptr = LVM_NULL;
                pInstance->size = 0;
            }
#else
            pInstance->ptr = LVM_NULL;
            pInstance->size = 0;
#endif
            break;
        }
        default:
        {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_set_datasource   un-support mode=%d", mode);
            result = ADSP_EUNSUPPORTED;
            break;
        }
    }

    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_set_datasource   pInstance->ptr=0x%x  pInstance->size=%d", pInstance->ptr, pInstance->size);
    return result;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_WhiteNoise(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t *pData = IOBuffer;
    uint32_t UnprocessedSamples = ProcessBlockSize;
    uint32_t Preset_ResidueSamples = pInjectionInstance->size - pInjectionInstance->offset;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_WhiteNoise   ProcessBlockSize=%d     size=%d     offset=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }

    if (Preset_ResidueSamples >= ProcessBlockSize)
    {
        memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), ProcessBlockSize * sizeof(int16_t));
        pInjectionInstance->offset += (uint16_t)ProcessBlockSize;
        if (pInjectionInstance->offset == pInjectionInstance->size)
        {
            pInjectionInstance->offset = 0;
        }
    }
    else
    {
        /*step1: copy preset buffer residue samples to injection buffer.*/
        memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), Preset_ResidueSamples * sizeof(int16_t));
        pData += Preset_ResidueSamples;

        /*step2: calculate un-processed samples and copy data from preset buffer beginning*/
        UnprocessedSamples -= Preset_ResidueSamples;
        memcpy((void*)pData, (void*)pInjectionInstance->ptr, UnprocessedSamples * sizeof(int16_t));
        /*update offset*/
        pInjectionInstance->offset = UnprocessedSamples;
    }
    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_WhiteBurst(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t *pData = IOBuffer;
    uint32_t UnprocessedSamples = ProcessBlockSize;
    uint32_t Preset_ResidueSamples = pInjectionInstance->size - pInjectionInstance->offset;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_WhiteBurst   ProcessBlockSize=%d     size=%d     offset=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }

    /*insert whitenoise.*/ 
    if (0 != Preset_ResidueSamples)
    {
        /*insert whitenoise.*/
        if (Preset_ResidueSamples >= UnprocessedSamples)
        {
            memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->offset += (uint16_t)UnprocessedSamples;
        }
        else
        {
            /*step1: copy residue sample to injection buffer.*/
            memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), Preset_ResidueSamples * sizeof(int16_t));
            pData += Preset_ResidueSamples;
            pInjectionInstance->offset += Preset_ResidueSamples;

            /*step2: insert slience to injection buffer.*/
            UnprocessedSamples -= Preset_ResidueSamples;
            memset((void*)pData, 0x00, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->processedsamples = UnprocessedSamples;
        }
    }
    else
    {
        /*insert slience.*/
        /*calculate slience samples*/
        uint32_t interval_samples = pInjectionInstance->size * INTERVAL_DURATION / 1000;   /*450ms samples = */
        if (pInjectionInstance->processedsamples + UnprocessedSamples < interval_samples)
        {
            memset((void*)pData, 0x00, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->processedsamples += UnprocessedSamples;
        }
        else
        {
            memset((void*)pData, 0x00, (interval_samples - pInjectionInstance->processedsamples)* sizeof(int16_t));
            pData += (interval_samples - pInjectionInstance->processedsamples);
            UnprocessedSamples -= (interval_samples - pInjectionInstance->processedsamples);

            /*insert whitenoise*/
            memcpy((void*)pData,(void*) pInjectionInstance->ptr, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->offset = UnprocessedSamples;
            pInjectionInstance->processedsamples = 0;
        }
    }

    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int32_t index = 0;
    uint32_t temp;
    int16_t *pData = IOBuffer;
    uint32_t UnprocessedSamples = ProcessBlockSize;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_Sine_1000Hz   ProcessBlockSize=%d     size=%d     offset=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }

#if 0
    for (index = 0; index<ProcessBlockSize; index++)
    {
        if (pInjectionInstance->size == pInjectionInstance->offset)
        {
            pInjectionInstance->offset = 0;
        }
        *(IOBuffer + index) = *(pInjectionInstance->ptr + pInjectionInstance->offset);
        pInjectionInstance->offset++;
    }
#else 
    /*alignment prest buffer*/
    if (0 != pInjectionInstance->offset)
    {
        temp = pInjectionInstance->size - pInjectionInstance->offset;
        memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), temp * sizeof(int16_t));
        pData += temp;
        UnprocessedSamples -= temp;
        pInjectionInstance->offset = 0;
    }

    for (index = (UnprocessedSamples / pInjectionInstance->size); index>0; index--)
    {
        memcpy((void*)pData, (void*)pInjectionInstance->ptr, pInjectionInstance->size * sizeof(int16_t));
        pData += pInjectionInstance->size;
    }

    if (0 != (UnprocessedSamples%pInjectionInstance->size))
    {
        memcpy((void*)pData, (void*)pInjectionInstance->ptr, (UnprocessedSamples % pInjectionInstance->size) * sizeof(int16_t));
        pInjectionInstance->offset = UnprocessedSamples % pInjectionInstance->size;
    }
#endif

    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_RampDown_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t Sine_1kHz_new[SAMPLES_FACTOR_FS48000] = {0};
    uint32_t index = 0;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_RampDown_Sine_1000Hz   ProcessBlockSize=%d     size=%d     offset=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }

    /*generate new reference data*/
    for (index = 0; index<pInjectionInstance->size; index++)
    {
        Sine_1kHz_new[index] = (int16_t)((*(pInjectionInstance->ptr + index)) * RampDownRatio[pInjectionInstance->stepindex]);
    }

    for (index = 0; index<ProcessBlockSize; index++)
    {
        *(IOBuffer + index) = Sine_1kHz_new[pInjectionInstance->offset];
        pInjectionInstance->offset++;
        pInjectionInstance->processedsamples++;

        if (pInjectionInstance->size == pInjectionInstance->offset)
        {
            pInjectionInstance->offset = 0;   //reset offset
            /*check step duration*/
            if ((pInjectionInstance->processedsamples) >= (uint32_t)(RAMP_STEP_DURATION* pInjectionInstance->size))
            {
                uint32_t i = 0;
                pInjectionInstance->processedsamples = 0;   //reset processed counter.
                /*update step index.*/
                if ((RAMP_STEP_MAX - 1) == pInjectionInstance->stepindex)
                {
                    pInjectionInstance->stepindex = 0;
                }
                else
                {
                    pInjectionInstance->stepindex++;
                }

                /*update reference data*/
                for (i = 0; i<pInjectionInstance->size; i++)
                {
                    Sine_1kHz_new[i] = (int16_t)((*(pInjectionInstance->ptr + i)) * RampDownRatio[pInjectionInstance->stepindex]);
                }
            }
        }
    }
    
    return;    
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_RampUp_Sine_1000Hz(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t Sine_1kHz_new[SAMPLES_FACTOR_FS48000] = { 0 };
    uint32_t index = 0;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_RampUp_Sine_1000Hz   ProcessBlockSize=%d     size=%d     offset=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }

    /*generate new reference data*/
    for (index = 0; index<pInjectionInstance->size; index++)
    {
        Sine_1kHz_new[index] = (int16_t)((*(pInjectionInstance->ptr + index)) * RampDownRatio[pInjectionInstance->stepindex]);
    }

    for (index = 0; index<ProcessBlockSize; index++)
    {
        *(IOBuffer + index) = Sine_1kHz_new[pInjectionInstance->offset];
        pInjectionInstance->offset++;
        pInjectionInstance->processedsamples++;

        if (pInjectionInstance->size == pInjectionInstance->offset)
        {
            pInjectionInstance->offset = 0;   //reset offset
            /*check step duration*/
            if ((pInjectionInstance->processedsamples) >= (uint32_t)(RAMP_STEP_DURATION* pInjectionInstance->size))
            {
                uint32_t i = 0;
                pInjectionInstance->processedsamples = 0;   //reset processed counter.
                /*update step index.*/
                if (0 == pInjectionInstance->stepindex)
                {
                    pInjectionInstance->stepindex = RAMP_STEP_MAX - 1;
                }
                else
                {
                    pInjectionInstance->stepindex--;
                }
                /*update reference data*/
                for (i = 0; i<pInjectionInstance->size; i++)
                {
                    Sine_1kHz_new[i] = (int16_t)((*(pInjectionInstance->ptr + i)) * RampDownRatio[pInjectionInstance->stepindex]);
                }
            }
        }
    }
    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_TonePlusSilence(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    uint32_t index = 0;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_TonePlusSilence   ProcessBlockSize=%d     size=%d     offset=%d    processedsamples=%d", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset, pInjectionInstance->processedsamples);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_WhiteNoise error!!");
        return;
    }


    if (pInjectionInstance->processedsamples >= (uint32_t)(TONE_DURATION* pInjectionInstance->size))
    {
        memset((void*)IOBuffer, 0x00, ProcessBlockSize * sizeof(int16_t));
        if (0 != pInjectionInstance->offset)
        {
            for (index=0; index<(pInjectionInstance->size - pInjectionInstance->offset); index++)
            {
                 *(IOBuffer + index) = pInjectionInstance->ptr[pInjectionInstance->offset];
                 pInjectionInstance->offset++;
                if (pInjectionInstance->size == pInjectionInstance->offset)
                {
                    pInjectionInstance->offset = 0;   //reset offset
                }
            }
        }
    }
    else
    {
        for (index=0; index<(uint32_t)ProcessBlockSize; index++)
        {
            *(IOBuffer + index) = pInjectionInstance->ptr[pInjectionInstance->offset];
            pInjectionInstance->offset++;
            if (pInjectionInstance->size == pInjectionInstance->offset)
            {
                pInjectionInstance->offset = 0;   //reset offset
            }
        }
        pInjectionInstance->processedsamples += ProcessBlockSize;
    }
    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_Speech(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t *pData = IOBuffer;
    uint32_t UnprocessedSamples = ProcessBlockSize;
    uint32_t Preset_ResidueSamples = pInjectionInstance->size - pInjectionInstance->offset;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_Speech   ProcessBlockSize=%d     size=%d     offset=%d\r\n", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
#ifdef NXP_INJECT_SPEECH_FTR
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_Speech  Error.");
#else
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_Speech   un-support inject speech.");
#endif
        return;
    }

    if (Preset_ResidueSamples >= (uint32_t)ProcessBlockSize)
    {
        memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), ProcessBlockSize * sizeof(int16_t));
        pInjectionInstance->offset += (uint16_t)ProcessBlockSize;
        if (pInjectionInstance->offset == pInjectionInstance->size)
        {
            pInjectionInstance->offset = 0;
        }
    }
    else
    {
        /*step1: copy preset buffer residue samples to injection buffer.*/
        memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), Preset_ResidueSamples * sizeof(int16_t));
        pData += Preset_ResidueSamples;

        /*step2: calculate un-processed samples and copy data from preset buffer beginning*/
        UnprocessedSamples -= Preset_ResidueSamples;
        memcpy((void*)pData, (void*)pInjectionInstance->ptr, UnprocessedSamples * sizeof(int16_t));
        /*update offset*/
        pInjectionInstance->offset = UnprocessedSamples;
    }
    return;
}

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_SpeechPlusSilence(LVVEFQ_injection_mode_st *pInjectionInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{
    int16_t *pData = IOBuffer;
    uint32_t UnprocessedSamples = ProcessBlockSize;
    uint32_t Preset_ResidueSamples = pInjectionInstance->size - pInjectionInstance->offset;

#ifdef NXP_INJECT_DEBUG_FTR
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_SpeechPlusSilence   ProcessBlockSize=%d     size=%d     offset=%d\r\n", ProcessBlockSize, pInjectionInstance->size, pInjectionInstance->offset);
#endif

    if ((LVM_NULL == pInjectionInstance->ptr) || (LVM_NULL == IOBuffer))
    {
#ifdef NXP_INJECT_SPEECH_FTR
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_SpeechPlusSilence   Error.");
#else
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_SpeechPlusSilence   un-support inject speech.");
#endif
        return;
    }

    /*insert speech.*/ 
    if (0 != Preset_ResidueSamples)
    {
        if (Preset_ResidueSamples >= UnprocessedSamples)
        {
            memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->offset += (uint16_t)UnprocessedSamples;
        }
        else
        {
            /*step1: copy residue sample to injection buffer.*/
            memcpy((void*)pData, (void*)(pInjectionInstance->ptr + pInjectionInstance->offset), Preset_ResidueSamples * sizeof(int16_t));
            pData += Preset_ResidueSamples;
            pInjectionInstance->offset += Preset_ResidueSamples;

            /*step2: insert slience to injection buffer.*/
            UnprocessedSamples -= Preset_ResidueSamples;
            memset((void*)pData, 0x00, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->processedsamples = UnprocessedSamples;
        }
    }
    else
    {
        /*insert slience.*/
        /*calculate slience samples*/
        uint32_t interval_samples = pInjectionInstance->size;
        if (pInjectionInstance->processedsamples + UnprocessedSamples < interval_samples)
        {
            memset((void*)pData, 0x00, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->processedsamples += UnprocessedSamples;
        }
        else
        {
            memset((void*)pData, 0x00, (interval_samples - pInjectionInstance->processedsamples)* sizeof(int16_t));
            pData += (interval_samples - pInjectionInstance->processedsamples);
            UnprocessedSamples -= (interval_samples - pInjectionInstance->processedsamples);

            /*insert speech*/
            memcpy((void*)pData,(void*) pInjectionInstance->ptr, UnprocessedSamples * sizeof(int16_t));
            pInjectionInstance->offset = (int16_t)UnprocessedSamples;
            pInjectionInstance->processedsamples = 0;
        }
    }

    return;
}

/****************************************************** private function end **************************************************************************/
/*************************************************************************************************************************************************/



/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_config(LVVEFQ_injection_st *pInstance, uint32_t sample_rate)
{
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_config   sample_rate=%d", sample_rate);

    if ((SAMPLERATE_8K != sample_rate) && (SAMPLERATE_16K != sample_rate) && (SAMPLERATE_48K != sample_rate))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LVVEFQ_injection_config   un-support sample_rate=%d", sample_rate);
        return;
    }

    if (LVM_NULL != pInstance)
    {
        pInstance->sample_rate = sample_rate;
    }
    else
    {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_config: return error.");
    }
    
    return;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_init(LVVEFQ_injection_st *pInstance)
{
    MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_init");
    memset(&pInstance->in_st, 0x00, sizeof(LVVEFQ_injection_mode_st));
    memset(&pInstance->out_st, 0x00, sizeof(LVVEFQ_injection_mode_st));
    return;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_injection_process(LVVEFQ_injection_st *pInstance, int16_t *IOBuffer, uint32_t ProcessBlockSize)
{

    LVVEFQ_injection_mode_st* pPrivateInstance = LVM_NULL; 

    if (pInstance->in_st.enable)
    {
        pPrivateInstance = &pInstance->in_st;
    }
    else if (pInstance->out_st.enable)
    {
        pPrivateInstance = &pInstance->out_st;
    }
    else
    {
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_process  process is disabled!");
        return;
    }
    
    /*check injection mode and call processing function.*/
    if (pPrivateInstance->mode < VEFQ_INJECT_TYPE_MAX)
    {
        pVEFQ_InjectionProcessHandler[pPrivateInstance->mode](pPrivateInstance, IOBuffer, ProcessBlockSize);
        
        if (pPrivateInstance->shift_bit > 0)
        {
            uint32_t index = 0;
            for (index = 0; index <ProcessBlockSize; index++)
            {
                *(IOBuffer+index) = (*(IOBuffer+index)) << pPrivateInstance->shift_bit;
            }
        }
        else if (pPrivateInstance->shift_bit < 0)
        {
            uint32_t index = 0;
            for (index = 0; index <ProcessBlockSize; index++)
            {
                *(IOBuffer+index) = (*(IOBuffer+index)) >> (abs(pPrivateInstance->shift_bit));
            }
        }
    }
    else
    {
        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_process  un-support mode %d", pPrivateInstance->mode);
    }

    return;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_injection_set_param(LVVEFQ_injection_st *pInstance,
                                      int8_t *params_buffer_ptr,
                                      uint32_t param_id,
                                      uint16_t param_size)
{
    ADSPResult result = ADSP_EOK;
    uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;

    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_set_param   param_id=0x%x  param_size=%d", param_id, param_size);
    switch (param_id)
    {
        case VOICE_PARAM_LVVEFQ_INJECTION_IN:
        {
            if (param_size != LVVEFQ_INJECTION_PARAM_SIZE)
            {
                result = ADSP_EBADPARAM;
                MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_set_param: Parameter error - VOICE_PARAM_LVVEFQ_INJECTION_IN for PCM injection: %d", param_size);
                return result;
            }
            memset(&pInstance->in_st, 0x00, sizeof(LVVEFQ_injection_mode_st));
            pInstance->in_st.enable = param_ptr[0];
            pInstance->in_st.mode = param_ptr[1];
            pInstance->in_st.shift_bit = param_ptr[2];
            LVVEFQ_injection_set_datasource(&pInstance->in_st, pInstance->sample_rate, pInstance->in_st.mode);
            break;
        }

        case VOICE_PARAM_LVVEFQ_INJECTION_OUT:
        {
            if (param_size != LVVEFQ_INJECTION_PARAM_SIZE)
            {
                result = ADSP_EBADPARAM;
                MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_set_param: Parameter error - VOICE_PARAM_LVVEFQ_INJECTION_OUT for PCM injection: %d", param_size);
                return result;
            }
            memset(&pInstance->out_st, 0x00, sizeof(LVVEFQ_injection_mode_st));
            pInstance->out_st.enable = param_ptr[0];
            pInstance->out_st.mode = param_ptr[1];
            pInstance->out_st.shift_bit = param_ptr[2];
            LVVEFQ_injection_set_datasource(&pInstance->out_st, pInstance->sample_rate, pInstance->out_st.mode);
            break;
        }

        default:
        {
            result = ADSP_EBADPARAM;
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_set_param: Invalid Parameter %d", param_id);
            break;
        }
    }

    return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_injection_get_param(LVVEFQ_injection_st *pInstance,
                                      int8_t *params_buffer_ptr,
                                      uint32_t param_id,
                                      int32_t inp_param_buf_size,
                                      uint16_t *param_size_ptr)

{
    ADSPResult result = ADSP_EOK;
    uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;

    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ_injection_set_param   param_id=0x%x  inp_param_buf_size=%d", param_id, inp_param_buf_size);
    switch (param_id)
    {
        case VOICE_PARAM_LVVEFQ_INJECTION_IN:
        {
            if (inp_param_buf_size < (int32_t)LVVEFQ_INJECTION_PARAM_SIZE)
            {
                result = ADSP_EBADPARAM;
                MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_get_param: Parameter error - VOICE_PARAM_LVVEFQ_INJECTION_IN for PCM injection: %d", inp_param_buf_size);
                return result;
            }
            
            *(param_ptr+0) = pInstance->in_st.enable;
            *(param_ptr+1) = pInstance->in_st.mode;
            *(param_ptr+2) = pInstance->in_st.shift_bit;

            *param_size_ptr = LVVEFQ_INJECTION_PARAM_SIZE;
            break;
        }

        case VOICE_PARAM_LVVEFQ_INJECTION_OUT:
        {
            if (inp_param_buf_size < (int32_t)LVVEFQ_INJECTION_PARAM_SIZE)
            {
                result = ADSP_EBADPARAM;
                MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_get_param: Parameter error - VOICE_PARAM_LVVEFQ_INJECTION_OUT for PCM injection: %d", inp_param_buf_size);
                return result;
            }
            
            *(param_ptr+0) = pInstance->out_st.enable;
            *(param_ptr+1) = pInstance->out_st.mode;
            *(param_ptr+2) = pInstance->out_st.shift_bit;

            *param_size_ptr = LVVEFQ_INJECTION_PARAM_SIZE;
            break;
        }

        default:
        {
            result = ADSP_EBADPARAM;
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_injection_set_param: Invalid Parameter %d", param_id);
            break;
        }
    }
MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "TJLVVEFQ_injection_set_param   param_id=0x%x  out_param_buf_size=%d", param_id, *param_size_ptr);
    return result;
}

#endif
