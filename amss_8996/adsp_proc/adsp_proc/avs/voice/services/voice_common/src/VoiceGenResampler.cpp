/*========================================================================

*//** @file VoiceGenResampler.cpp
This file abstracts out common utilities for voice.

Copyright (c) 2010-2011, 2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_common/src/VoiceGenResampler.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
08/27/12   pb    Initial version

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "VoiceGenResampler.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Function prototypes
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Message handler
** ----------------------------------------------------------------------- */

/* =======================================================================
**                          Function Definitions
** ======================================================================= */

/**
 Given the resampling parameters, this API returns the size of channel memory and instance memory required.
*/
uint32_t voice_gen_resamp_get_size(int16_t quality, uint8_t num_bytes_per_sample, voice_generic_resampler_t *gen_resampler_ptr)
{
   uint32_t total_size;
   resample_getLibSize(quality, num_bytes_per_sample, &(gen_resampler_ptr->channel.channel_size),(uint8_t)CHANNEL_MEM);
   resample_getLibSize(quality, num_bytes_per_sample, &(gen_resampler_ptr->instance.instance_size),(uint8_t)INSTANCE_MEM);
   total_size = VOICE_ROUNDTO8(gen_resampler_ptr->channel.channel_size) + VOICE_ROUNDTO8(gen_resampler_ptr->instance.instance_size);
   return (total_size);
}

/**
 Given the memory pointers and size, this API initializes the memory for the channel and instance memories.
*/
ADSPResult voice_gen_resamp_set_mem(voice_generic_resampler_t *gen_resampler_ptr, int8_t* mem_ptr, uint32_t size)
{
	uint32_t resampler_size = VOICE_ROUNDTO8(gen_resampler_ptr->channel.channel_size) + VOICE_ROUNDTO8(gen_resampler_ptr->instance.instance_size);
	if(size < resampler_size)
	{
	   MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  ERROR: resampler memory size insufficient:allocated (%ld) required (%ld)", size, resampler_size);
	   return ADSP_ENOMEMORY;
    }

	gen_resampler_ptr->channel.channel_memory_ptr = mem_ptr;
    gen_resampler_ptr->instance.instance_memory_ptr = (void *)(mem_ptr+VOICE_ROUNDTO8(gen_resampler_ptr->channel.channel_size));

    return ADSP_EOK;
}

/**
 Given the resampler instance, this API makes the channel and instance memory pointers to NULL.
*/
void voice_gen_resamp_end(voice_generic_resampler_t *gen_resampler_ptr)
{
   gen_resampler_ptr->channel.channel_memory_ptr = NULL;
   gen_resampler_ptr->instance.instance_memory_ptr = NULL;
   return;
}

/**
 Given the resampler instance pointer, input-output buffer pointers, sizes and the mode of resampler operation,
 this API resamples the input data to generate the output data based on the mode of operation specified
*/
ADSPResult voice_gen_resamp_process(voice_generic_resampler_t *gen_resampler_ptr, int32_t *in_ptr, int32_t *out_ptr, int32_t in_cnt, int32_t out_cnt, int16_t mode)
{
	ResampReturnType result;
	uint32_t samples_processed;
	result = resample((GenericResamplerLib24SRC*)gen_resampler_ptr->channel.channel_memory_ptr, in_ptr, out_ptr, in_cnt, out_cnt, &samples_processed, mode);
    if(GEN_RESAMP_SUCCESS != result)
    {
      return ADSP_EFAILED;
    }
	return ADSP_EOK;
}
/**
 Given the resampler instance pointer, input-output sampling rate and the mode of resampler operation,
 this API initializes the resampler instance and its internal structures
*/
ADSPResult voice_gen_resamp_init(voice_generic_resampler_t *gen_resampler_ptr, int32_t in_samp_rate, int32_t out_samp_rate, int16_t mode)
{
	ResampReturnType result ;
	result = resampler_initialize((GenericResamplerLib24SRC*)(gen_resampler_ptr->channel.channel_memory_ptr),in_samp_rate ,out_samp_rate , mode, (int8*)gen_resampler_ptr->instance.instance_memory_ptr);
    if( GEN_RESAMP_SUCCESS != result)
    {
       return ADSP_EFAILED;
    }
	return ADSP_EOK;
}