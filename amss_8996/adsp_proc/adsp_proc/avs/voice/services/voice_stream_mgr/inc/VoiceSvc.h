
/*========================================================================
Elite

This file contains an example service belonging to the Elite framework.

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_stream_mgr/inc/VoiceSvc.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
9/27/09    vn       Created file.
10/05/10   pc       Added doxy comments

========================================================================== */
#ifndef VOICESVC_H
#define VOICESVC_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"
#include "VoiceMsgs.h"
#include "apr_api.h"
#include "VoiceMixerSvc.h"
#include "VoiceCmnUtils.h"
#include "mmpm.h"
#include "mvm_api_i.h" //for calibration utility for set param

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus


/** Enumeration for maximum number of stream sessions */
#define VSM_MAX_SESSIONS   5

//TODO: <katitkar> Where to put them and whether to assign them GUIDs
/** Enumeration for RUN state of stream sessions */
#define VSM_RUN_STATE     0xABCD1111
/** Enumeration for STOP state of stream sessions */
#define VSM_STOP_STATE    0xABCD2222
/* -----------------------------------------------------------------------
** Global definitions/forward declarations
** ----------------------------------------------------------------------- */
/** Redefinition of voice_strm_tty_state_t */
#define voice_strm_tty_state_t VoiceStrmTtyState_t
/** Redefinition of voice_strm_apr_info_t */
#define voice_strm_apr_info_t  VoiceStrmAprInfo_t
/** Redefinition of voice_strm_session_t */
#define voice_strm_session_t   VoiceStrmSession_t
/** Redefinition of voice_strm_mgr_t */
#define voice_strm_mgr_t       VoiceStrmMgr_t
/** Redefinition of voice_strm_mgr_create */
#define voice_strm_mgr_create  VoiceStrmMgrCreate


typedef struct voice_strm_mgr_t voice_strm_mgr_t;
typedef struct voice_strm_session_t voice_strm_session_t;
typedef struct voice_strm_tty_state_t voice_strm_tty_state_t;
typedef struct voice_strm_apr_info_t  voice_strm_apr_info_t;

/** @brief This structure is used to maitain the state of GSM-TTY.*/
struct voice_strm_tty_state_t
{
   uint16_t m_etty_mode;
   /**< TTY operation mode - OFF/HCO/VCO/FULL, set by VSM in stop state, read by CTM Rx/Tx (Dec/Enc) >*/
   int32_t m_ctm_from_far_end_detected;
   /**< variable that CTM Rx (Dec) writes, CTM Tx (Enc) reads */
   int32_t m_enquiry_from_far_end_detected;
   /**< variable that CTM Rx (Dec) writes, CTM Tx (Enc) reads/clears */
   int32_t m_rx_tty_detected;
   /**< variable that CTM Rx (Dec) writes, Tx CNG (Enc) reads */
   int32_t m_ctm_character_transmitted;
   /**< variable that CTM Tx (Enc) writes, CTM Rx (Dec) reads */
   int32_t m_sync_recover_rx;
   /**< variable that VSM writes based on RESYNC_CTM, CTM Rx (Dec) reads/resets */
   int32_t m_sync_recover_tx;
   /**< variable that VSM writes based on RESYNC_CTM, CTM Tx (Dec) reads/resets */
   uint16_t m_oobtty_mode;
   /**< VoLTE TTY operation mode - OFF/HCO/VCO/FULL, set by VSM in stop state >*/
   int32_t m_rx_oobtty_detected;
   /**< variable that VoLTE TTY Rx (Dec) writes, Tx VoLTE (Enc) reads */
};

/** @brief This strucutre is used for communication with client.
This structure is typically used as an argument passing to dynamic service for
   1. For service to directly raise an event with client if so required.
   2. For service to send information about the commands processing result.
*/

struct voice_strm_apr_info_t
{
   /*The following is necessary information for direct communication with client
    and for talking with APR*/
   uint32_t    apr_handle;
   /**< This is the apr handle that is required in sending ack. */
   uint16_t    self_addr;
   /**< self's address (16bit) */
   uint16_t    self_port;
   /**< slef's port (16bit) */
   uint16_t    client_addr;
   /**< Client's address (16bit)*/
   uint16_t    client_port;
   /**< Client's port (16bit) */
};

/** @brief This structure holds information for a stream session.
This structure is used to create a dynamic stream session.It contains all the
information that a dynamic sessions needs. Dynamic stream sessions are created
in pairs, although the configuration allows for TX, RX or TX+RX.
Assumption is that only one Tx can be paired with one Rx.
*/
struct voice_strm_session_t
{
   VoiceStrmMgr_t        *vsm_ptr;
   /**< Pointer to the VSM manager */
   elite_svc_handle_t    *tx;
   /**< Voice Enc handle */
   elite_svc_handle_t    *rx;
   /**< Voice Dec handle */
   vmx_inport_handle_t   *mixer_in_port_ptr;
   /**< Input port of mixer for Voice Dec */
   vmx_outport_handle_t  *mixer_out_port_ptr;
   /**< Output port of mixer for Voice Enc */
   uint32_t              input_mapping_mask;
   /**< indicates mapping from the voice decoder modules to vocproc rx modules */
   uint32_t              output_mapping_mask;
   /**< indicates mapping from the vocproc tx modules to encoders */
   int32_t               tx_port;
   /**< port mask corresponding to tx device */
   int32_t               rx_port;
   /**< port mask corresponding to rx device */
   uint32_t              state;
   /**< session state  */
   uint8_t               pending;
   /**< pending APR control command to manage async processing of commands */
   uint16_t              my_handle;
   /**< handle to give to client */
   voice_strm_tty_state_t tty_state;
   /**< TTY state info */
   voice_strm_apr_info_t  apr_info;
   /**< APR info for dynamic services to reference */
   uint32_t               media_type_format;
   /**< store current media type */
   uint32_t               tx_stream_pp_samp_rate;
   /**< stores the current sampling rate set on the stream PP */
   async_struct_rxtx_t    async_struct;
   /**< structure to manage asynchronous responses coming back from dynamic services.
        This structure will collect responses from the dynamic services, and match them
        to the original APR command, so that the APR response is sent when all the appropriate
        messages have been collected, and an appropriate response result has been generated*/
   uint16_t               pb_session;
   /**< Handle to voiceproc session used for in call audio delivery */
   elite_svc_handle_t     *tx_afe_handle_ptr;
   /**< saved handle to send pcm samples to be recorded from VENC to AFE service (returned from AFE connect) */
   elite_svc_handle_t     *rx_afe_handle_ptr;
   /**< saved handle to send pcm samples to be recorded from VDEC to AFE service (returned from AFE connect) */
   uint32_t               rx_tap_point;
   /**< Tap point to use on rx path. Values are:
        - ::VSM_TAP_POINT_NONE -- Do not record rx path   
        - ::VSM_TAP_POINT_STREAM_END -- Rx tap point is at the end of the
	                                stream*/
   uint32_t               tx_tap_point;
   /**< Tap point to use on tx path. Values are:
        - ::VSM_TAP_POINT_NONE -- Do not record tx path   
        - ::VSM_TAP_POINT_STREAM_END -- Tx tap point is at the end of the
	                                stream*/
   uint32_t               record_mode;
   /**< recording mode that can be choosen by the user. Values are:
      - ::VSM_RECORD_TX_ONLY -- record only tx path   
		- ::VSM_RECORD_TX_ONLY -- record only rx path   
		- ::VSM_RECORD_TX_RX_STEREO -- record tx and rx paths in stereo mode   
      - ::VSM_RECORD_TX_RX_MIXING -- record mixed data of tx and rx paths*/
   uint16_t               aud_ref_port_rx;
   /**< Audio Front End (AFE) port ID for sending the Vdec to AFE.
        This can be real port or psuedo port.*/
   uint16_t               aud_ref_port_tx;
   /**< Audio Front End (AFE) port ID for sending the Venc samples to AFE.
        This can be real port or psuedo port.*/
   uint16_t               record_start_on;
   /**< flag to indicate record start command is on already or not*/
   uint16_t               vfr_mode;
   /**< Sets the stream VFR mode. Values are:
        - 0 -- None
        - 1 -- Hard internal
        - 2 -- Hard external*/
   uint16_t               enc_offset;
   /**< 0 to 20000 microseconds with 1-microsecond granularity
     offset from VFR to send the encoded packet to the modem.*/
   uint16_t               host_pcm_num_active_tap_points;     
   /**< used to track number of active host pcm tap points per session 
       (can stay enabled across RUN->STOP->RUN state transitions) */
   void                   *tx_afe_drift_ptr;
   /**< saved ptr to drift info struct (returned from Tx near-end AFE connect)  */
   void                   *rx_afe_drift_ptr;
   /**< saved ptr to drift info struct (returned from Rx AFE connect)  */   
   uint32_t               pkt_exchange_mode;
   /**< Sets the packet exchange mode. Values are:
         - 0 -- Inband
         - 1 -- Outband*/
   bool                   oob_config_received;
   /**< oob config flag to manage proper sequencing  of commands */
   uint16_t               timing_cmd_version;
   /**< keeps track of which version of timing command is used.
        needed for in-call music delivery use case where VSM need to 
        send right version of timing command to VPM as part of setup sequence.
        This is also being extended to identify clock requirements for ADSP Test fwk.
    */
   uint32_t               vsid;
   /**< voice systems id for hard vfr timing*/
   uint16_t               vptx_delivery_offset;
   /**< offset from vfr for vptx to deliver processed data to venc.
        this offset is needed for in-call music delivery use case
        where VSM need to send timing command to VPM as part of setup sequence*/
   uint32_t               vocoder_mode_event_registered;
   /**< Indicates whether the client has registered for vocoder mode detection on this session*/
};

/** @brief Structure to define the state of a the Voice Stream Manager.

    This contains information about the manager and the active voice stream sessions under control.  There
    may be 0 to VSM_MAX_SESSIONS active at any given time.
*/

struct voice_strm_mgr_t
{
   elite_svc_handle_t     svc_handle;
   /**< handle to give out to others */
   voice_strm_session_t    *session[VSM_MAX_SESSIONS];
   /**< assume max of 4 concurrent sessions */
   uint32_t  session_run_mask;
   /**< Session mask holds the sessions that are enabled (Populated in the order of creation LSB->MSB). */
   qurt_elite_queue_t       *vsm_tx_resp_q_ptr;
   /**< queue for Tx acks */
   qurt_elite_queue_t       *vsm_rx_resp_q_ptr;
   /**< queue for Rx acks */
   qurt_elite_channel_t     channel;
   /**< channel for wait signals */
   elite_apr_handle_t     apr_handle;
   /**< saved handle obtained during APR registration, required for processing APR packets */
   elite_apr_addr_t       apr_addr;
   /**< saved address obtained during APR registration, equal to VPM's APR address (domain/id pair) */
   qurt_elite_queue_t           *vsm_afe_cmd_q_ptr ;
   /**< saved AFE command Queue for sending AFE requests */

   voice_commom_mmpm_t          mmpm;           
   /**< MMPM data for VSM, LPASS ADSP core Id */
   /**< TBD: Need another instance for LPASS LPM core ID if using LPM for vocoder??  If so, should 
     add power request structure within voice_common_mmpm_t to vote for LPM power/BW requirements, but not clock  */
   uint32_t  mmpm_min_mips_per_thread;
   /**< This stores the last procured per thread mips, required for clock requests. */
   /**< Caching this is not currently required, as we do not have kpps based mmpm requests . */
   uint32_t  mmpm_mips_request;
   /**< This stores the last total mips request used for clock requests. */
   /**< Caching this is not currently required, as we do not have kpps based mmpm requests . */
   bool_t req_clock;                     // If any client is the Second Application Domain (APPS), then assume
                                         // end application to be ADSP Test framework on Target. If this flag is 
                                         // TRUE, then clock requests logic will be pursued.

};

/**
* This function creates an instance of the Voice Stream Manager service.
* Usually this service is created at boot time.
*
* @param[in] inputParam This can be any value or pointer to structure required
*    by service help set up the instance. In the example service, this input is
*    not used.
* @param[out] handle This will be the service entry handle returned to the caller.
* @return Success/failure of instance creation.
*/
ADSPResult voice_strm_mgr_create(uint32_t input_param, void **handle);


#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef VOICESVC_H

