/*========================================================================

 *//** @file VoiceSvc.cpp
     This file Voice service - control logic for voice call/voip calls.
     and in call features.

     Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
     QUALCOMM Proprietary.  Export of this technology or software is regulated
     by the U.S. Government, Diversion contrary to U.S. law prohibited.
    *//*====================================================================== */

/*========================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_stream_mgr/src/VoiceSvc.cpp#1 $

  when  who     what, where, why
  --------   ----------------------------------------------------------
  10/06/09   vn Created file.

  ========================================================================== */


/* =======================================================================
   INCLUDE FILES FOR MODULE
   ========================================================================== */
#include "Elite.h"
#include "EliteSvc.h"
#include "EliteSvc_Util.h"
#include "VoiceSvc.h"
#include "VoiceMsgs.h"
#include "venc_svc.h"
#include "VoiceDec.h"
#include "VoiceDevSvc.h"
#include "EliteMsg_Util.h"
#include "VoiceMixerSvc.h"
#include "VoiceCmnUtils.h"
#include "adsp_vcmn_api.h"
#include "AFEDevService.h"
#include "EliteMsg_AfeCustom.h"
#if defined(__qdsp6__) && !defined(SIM)
#include "VoiceLoggingUtils.h"
#endif

/* APR/API related */
#include "EliteAprIf.h"
#include "apr_comdef.h"
#include "aprv2_ids_domains.h"
#include "adsp_vsm_api.h"
#include "adsp_vpm_api.h"
#include "adsp_vparams_api.h"

// This is the pointer to VPM, will come from main.c
extern VoiceProcMgr_t* voiceProcMgr;
extern elite_svc_handle_t *txVoiceMatrix;
extern elite_svc_handle_t *rxVoiceMatrix;

/*--------------------------------------------------------------*/
/* Global definitions                                       */
/* -------------------------------------------------------------*/
//TODO: Make this a VSM struct member
uint32_t vsm_memory_map_client = 0;

/*--------------------------------------------------------------*/
/* Macro definitions                                       */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */
/* used to indicate to RX path or Tx path */
enum
{
   VSM_RX_PATH = 0,
   VSM_TX_PATH
};

/* used to indicate to ack handler where the ack came from */
enum
{
   VSM_RX_RESP_Q = 0,
   VSM_TX_RESP_Q,
   VSM_CMD_Q
};

/* list of possible actions/states that can be tracked on Rx and Tx for ack handling and command sequencing.  When state machine is
 * finished for either Rx or Tx it will be in VSM_ACTION_NONE state.  Ack handler can check when both Rx and Tx are in VSM_ACTION_NONE state
 * before sending the APR ack.  In addition if session is unpaired (example Rx only), then Tx state can be set to VSM_ACTION_NONE
 * and behave in the same way */
enum
{
   VSM_ACTION_NONE = 0,   /* Must be 0 to match common utils Async handler state variable init state_rx/state_tx */
   VSM_ACTION_STOP_SERVICE,
   VSM_ACTION_RUN_SERVICE,
   VSM_ACTION_SET_MEDIATYPE,
   VSM_ACTION_CREATE_PORT,     /* currently done synchronously */
   VSM_ACTION_STOP_PORT,
   VSM_ACTION_RUN_PORT,
   VSM_ACTION_REASSIGN_PORT,   /* changes mapping */
   VSM_ACTION_RECONFIG_PORT,   /* changes sampling rate */
   VSM_ACTION_CLOSE_PORT,
   VSM_ACTION_CONNECT_DOWNSTREAM,     /* currently done synchronously */
   VSM_ACTION_SET_PARAM,
   VSM_ACTION_SET_VFR,          /*changes VFR mode*/
   VSM_ACTION_SET_SOFT_MUTE,
   VSM_ACTION_GET_PARAM,
   VSM_ACTION_REINIT_DYNAMIC_SVC,   /* reinit dynamic service state and algorithms */
   VSM_ACTION_START_RECORD,
   VSM_ACTION_STOP_RECORD,
   VSM_ACTION_DISCONNECT_AFE,
   VSM_ACTION_CONNECT_AFE,
   VSM_ACTION_START_HPCM,
   VSM_ACTION_STOP_HPCM,
   VSM_ACTION_SET_PP_SAMP_RATE,
   VSM_ACTION_SET_PKT_EXCHANGE_MODE,
   VSM_ACTION_SET_OOB_PKT_EXCHANGE_CONFIG,
   VSM_ACTION_GET_KPPS,
   VSM_ACTION_GET_DELAY,
   VSM_ACTION_SET_ENC_RATE
};

// maximum number of commands expected ever in command queue.
static const uint32_t MAX_CMD_Q_ITEMS = 16; // power of 2


// thread settings.
static char_t VSM_THREAD_NAME[]="VSM";
static const uint32_t VSM_THREAD_STACK_SIZE = 4*1024;

// thread settings. Make configurable? Probably this is good enough.

static char_t cmdQName[]="VssCmdQ";


static voice_strm_mgr_t *vsm_ptr = NULL;

// MIN_OOB_BUFFER_SIZE = worst case payload for PCM_48K (1920 bytes) + OOB pkt exchange header (12 bytes)
#define MIN_OOB_BUFFER_SIZE (1920+12)


/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */
// no destructor needed for static service

// Main work loop for service thread. Pulls msgs off of queues and processes them.
static int vsm_thread(void* instance_ptr);

// message handler functions (TBD REPLACE ALL THESE with APR compatible versions...no ELite ack)


static ADSPResult vsm_run_rx(void* instance_ptr, uint16_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_stop_tx(void* instance_ptr, uint16_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_run_tx(void* instance_ptr, uint16_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_stop_rx(void* instance_ptr, uint16_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_process_apr_msg(void* instance_ptr, elite_msg_any_t* pMsg);
static ADSPResult vsm_reinit_session( uint16_t session_index, uint16_t mode,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_create_session( elite_apr_packet_t *apr_packet_ptr,
      uint16_t *session_index_ptr);
static ADSPResult vsm_destroy_session( uint16_t session_index);
static ADSPResult vsm_send_tx_vfr_mode( void* instance_ptr, uint32_t session_index,
      int32_t* vfr_cmd_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token, uint32_t sec_opcode);
static ADSPResult vsm_send_rx_vfr_mode( void* instance_ptr, uint32_t session_index,
      int32_t* vfr_cmd_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token, uint32_t sec_opcode);
static ADSPResult vsm_send_tx_soft_mute( void* instance_ptr, uint32_t session_index,
      voice_set_soft_mute_v2_t *cmd_ptr, async_status_rxtx_t
      *async_status_ptr, uint32_t token, uint32_t opcode);
static ADSPResult vsm_send_rx_soft_mute( void* instance_ptr, uint32_t session_index,
      voice_set_soft_mute_v2_t *cmd_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token, uint32_t opcode);

static ADSPResult vsm_reinit_rx_session( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_reinit_tx_session( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static uint32_t vsm_get_str_mask(int32_t count, uint16_t* handles_ptr);
static uint32_t vsm_get_in_ports_mask(int32_t count, uint16_t* handles_ptr);
static int32_t  vsm_convert_to_apr_result( void *instance_ptr, uint32_t session_index,
      ADSPResult rx_result, ADSPResult tx_result);

static ADSPResult vsm_process_start_hpcm_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_stop_hpcm_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid);
static ADSPResult vsm_process_create_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_run_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_stop_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_reinit_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_media_type_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_stream_pp_samp_rate_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_destroy_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_attach_detach_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_tty_mode_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_oob_set_tty_mode_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
                                                uint8_t qid);
static ADSPResult vsm_process_ctm_resync_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_param_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_get_param_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_vfr( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_soft_mute( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);

static ADSPResult vsm_process_start_record_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_stop_record_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_pkt_exchange_mode( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_oob_pkt_exchange_config( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);
static ADSPResult vsm_process_set_enc_rate( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid);

static ADSPResult vsm_send_tx_media_type( void* instance_ptr, uint32_t session_index,
      uint32_t media_type,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_tx_samp_rate( void* instance_ptr, uint32_t session_index,
      vsm_set_samp_rate_t   *samp_rate_cmd_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_rx_media_type( void* instance_ptr, uint32_t session_index,
      uint32_t media_type,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_rx_samp_rate( void* instance_ptr, uint32_t session_index,
      vsm_set_samp_rate_t   *samp_rate_cmd_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_connect_rx_downstream( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);

/* Rx port control */
static ADSPResult vsm_control_rx_mixer_input(void* instance_ptr, uint32_t session_index,
      uint32_t command,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_create_rx_mixer_input( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_close_rx_mixer_input(void* instance_ptr, int32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_reassign_rx_mixer_input(void* instance_ptr, int32_t session_index,
      uint32_t input_mapping_mask,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);

/* Tx port control */
static ADSPResult vsm_create_tx_mixer_output(void* instance_ptr, uint32_t session_index,
      uint16_t samplingRate,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_reconfigure_tx_mixer_output( void* instance_ptr, uint32_t session_index,
      uint16_t samplingRate,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_control_tx_mixer_output(void* instance_ptr, uint32_t session_index,
      uint32_t command,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_close_tx_mixer_output(void* instance_ptr, int32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_reassign_tx_mixer_output(void* instance_ptr, int32_t session_index,
      uint32_t output_mapping_mask,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token);
static ADSPResult vsm_start_record_tx( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_start_record_rx( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_connect_afe(void* instance_ptr, uint32_t session_index,
      uint16_t sampling_rate, uint16_t tx_session,
      uint16_t port, async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_stop_record_tx( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token );


static ADSPResult vsm_stop_record_rx( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr, uint32_t token);


static ADSPResult vsm_disconnect_afe(void* instance_ptr, uint32_t session_index,
      uint16_t port, async_status_rxtx_t *async_status_ptr, uint32_t token,
      uint16_t direction);

static ADSPResult vsm_send_tx_pkt_exchange_mode( void* instance_ptr, uint32_t session_index,
      uint32_t pkt_exchange_mode,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_send_rx_pkt_exchange_mode( void* instance_ptr, uint32_t session_index,
      uint32_t pkt_exchange_mode,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_send_tx_oob_pkt_exchange_config( void* instance_ptr, uint32_t session_index,
      vsm_config_packet_exchange_t *vsm_config_oob_pkt_exchange_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_send_rx_oob_pkt_exchange_config( void* instance_ptr, uint32_t session_index,
      vsm_config_packet_exchange_t *vsm_config_oob_pkt_exchange_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult voice_check_oob_buf_size(uint32_t payload_size);

static ADSPResult vsm_send_tx_enc_rate( void* instance_ptr, uint32_t session_index,
      vsm_enc_set_rate_t* enc_rate_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);

static ADSPResult vsm_send_rx_enc_rate( void* instance_ptr, uint32_t session_index,
      vsm_enc_set_rate_t* enc_rate_ptr,
      async_status_rxtx_t *async_status_ptr, uint32_t token);


static ADSPResult vsm_send_start_tx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *packet_ptr,async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_start_rx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *packet_ptr,async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_stop_tx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *packet_ptr,async_status_rxtx_t *async_status_ptr, uint32_t token);
static ADSPResult vsm_send_stop_rx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *packet_ptr,async_status_rxtx_t *async_status_ptr, uint32_t token);
static void vsm_set_mmpm_clocks(void* instance_ptr);
static ADSPResult vsm_process_get_kpps_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid);
static ADSPResult vsm_process_get_delay_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid);
static ADSPResult vsm_send_event_reg_unreg( void* instance_ptr, uint16_t session_index, uint32_t event_id, uint32_t opcode);
/* -----------------------------------------------------------------------
 ** Message handler
 ** ----------------------------------------------------------------------- */
// when RUN or STOP is commanded.


static elite_svc_msg_handler_func handler_ptr[] =
{
   elite_svc_unsupported,          //0 - ELITE_CMD_CUSTOM
   elite_svc_unsupported,          //1 - ELITE_CMD_START_SERVICE
   elite_svc_unsupported,          //2 - ELITE_CMD_DESTROY_SERVICE
   elite_svc_unsupported,          //3 - ELITE_CMD_CONNECT
   elite_svc_unsupported,          //4 - ELITE_CMD_DISCONNECT
   elite_svc_unsupported,          //5 - ELITE_CMD_PAUSE
   elite_svc_unsupported,          //6 - ELITE_CMD_RESUME
   elite_svc_unsupported,          //7 - ELITE_CMD_FLUSH
   elite_svc_unsupported,          //8 - ELITE_CMD_SET_PARAM
   elite_svc_unsupported,          //9 - ELITE_CMD_GET_PARAM
   elite_svc_unsupported,          //10 - ELITE_DATA_BUFFER
   elite_svc_unsupported,          //11 - ELITE_DATA_MEDIA_TYPE
   elite_svc_unsupported,          //12 - ELITE_DATA_EOS
   elite_svc_unsupported,          //13 - ELITE_DATA_RAW_BUFFER
   elite_svc_unsupported,          //14 - ELITE_CMD_STOP_SERVICE
   vsm_process_apr_msg                 //15 - ELITE_APR_PACKET

};


/* =======================================================================
 **                     Function Definitions
 ** ======================================================================= */

static int32_t vsm_isr_recv_fn ( elite_apr_packet_t* apr_packet_ptr, void* dispatch_data )
{
   uint32_t       payload_size;
   int32_t        rc = APR_EOK;
   elite_msg_any_t     sMsg;
   ADSPResult result;

   if ( apr_packet_ptr == NULL )
   {
      rc = APR_EBADPARAM;
      return rc;
   }

   payload_size = elite_apr_if_get_payload_size( apr_packet_ptr);

   /* debug info.  Destination address should be VSM domain/service ID to get to this ISR, DstPort
    * should be the session handle that this is directed to, in case a session has already been established
    */
   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM apr_packet_ptr(%x): src_addr(%x) src_port(%x) dst_port(%x) op_code(0x%x) token(0x%x) size(%d)",
         (int) apr_packet_ptr,
         (int)elite_apr_if_get_src_addr(apr_packet_ptr),
         (int)elite_apr_if_get_src_port(apr_packet_ptr),
         (int)elite_apr_if_get_dst_port(apr_packet_ptr),
         (int)elite_apr_if_get_opcode(apr_packet_ptr),
         (int)elite_apr_if_get_client_token(apr_packet_ptr),
         (int)payload_size);

   /* dispatch the packets to either the VSM manager or the endpoint dynamic service depending on the opcode.  Assuming
    * queue onto Elite service succeeds, the receiving service is expected to respond that the command was accepted,
    * to process, and free the APR packet when the requested operation is complete.  Current design is to use simple 8 byte
    * elite header to manage APR packet delivery, so no additional copies of APR payload is required, and FADD message
    * doesn't need to be obtained from buffer manager to handle delivery to the Elite service.  This assumes it is OK to hold
    * onto the incoming apr_packet_t *apr_packet_ptr pointer until the packet is processed by the destination service.
    */

   /* Report command acceptance when requested. */
   if ( elite_apr_if_msg_type_is_cmd( apr_packet_ptr) )
   {
      rc = elite_apr_if_send_event_accepted( vsm_ptr->apr_handle, apr_packet_ptr);

      if( APR_EOK != rc)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM Error(%x) sending APR_IBASIC_RSP_ACCEPTED",(int) rc);
         rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
         return APR_EOK;     // command freed, need to return EOK
      }
   }


   /* simple delivery...copy pointer.  receiver must free packet if queue'd successfully */
   sMsg.unOpCode = ELITE_APR_PACKET;
   sMsg.pPayload = (void *) apr_packet_ptr;

   uint16_t handle = elite_apr_if_get_dst_port(apr_packet_ptr);
   uint16_t session_index = voice_map_handle_to_session_index( handle);
   uint32_t cmd_opcode = elite_apr_if_get_opcode( apr_packet_ptr);

   //For everything other than create/memory map/unmap, check for NULL session
   if(  (VSM_CMD_CREATE_SESSION != cmd_opcode)             &&
         (VOICE_CMD_SHARED_MEM_MAP_REGIONS != cmd_opcode)   &&
         (VOICE_CMD_SHARED_MEM_UNMAP_REGIONS != cmd_opcode)
     )
   {
      if( session_index >= VSM_MAX_SESSIONS)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rcvd opCode(0x%x) on invalid session_index(%x)!!",
               (int) elite_apr_if_get_opcode( apr_packet_ptr),(int) session_index);
         if ( elite_apr_if_msg_type_is_cmd( apr_packet_ptr) )
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
         }
         else
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
         }
         return APR_EOK;   // command freed/ended, need to return EOK
      }
      if( NULL == vsm_ptr->session[session_index])
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rcvd opCode(0x%x) on non-existent session_index(%x)!!",
               (int) elite_apr_if_get_opcode( apr_packet_ptr),(int) session_index);
         if ( elite_apr_if_msg_type_is_cmd( apr_packet_ptr) )
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
         }
         else
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
         }
         return APR_EOK;   // command freed/ended, need to return EOK
      }
   }

   switch( elite_apr_if_get_opcode( apr_packet_ptr) )
   {
      case APRV2_IBASIC_EVT_ACCEPTED:
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            break;
         }

         /* test: route these to VSM manager */
      case VSM_CMD_CREATE_SESSION:
      case VSM_CMD_DESTROY_SESSION:
      case VSM_CMD_ATTACH_VOICE_PROC:
      case VSM_CMD_DETACH_VOICE_PROC:
      case VSM_CMD_START_SESSION:
      case VSM_CMD_STOP_SESSION:
      case VSM_CMD_SET_TTY_MODE:
      case VSM_CMD_OUTOFBAND_SET_TTY_MODE:
      case VSM_CMD_RESYNC_CTM:
      case VSM_CMD_SET_MEDIA_TYPE:  /* TBD mediatype goes directly to Enc/Dec? */
      case VSM_CMD_REINIT_SESSION:
      case VOICE_CMD_SET_PARAM_V2:
      case VOICE_CMD_SET_PARAM_V3:
      case VOICE_CMD_GET_PARAM_V2:
      case VSM_CMD_GET_KPPS:
      case VSM_CMD_GET_DELAY:
      case VOICE_CMD_SET_TIMING_PARAMS:
      case VSM_CMD_SET_TIMING_PARAMS:
      case VSM_CMD_SET_TIMING_PARAMS_V2:
      case VOICE_CMD_SET_SOFT_MUTE_V2:
      case VSM_CMD_SET_STREAM_PP_SAMP_RATE:
      case VOICE_CMD_SHARED_MEM_MAP_REGIONS:
      case VOICE_CMD_SHARED_MEM_UNMAP_REGIONS:
      case VSM_CMD_STOP_PLAYBACK:
      case VSM_CMD_STOP_RECORD:
      case VSM_CMD_START_RECORD_V2:
      case VSM_CMD_START_PLAYBACK_V2:
      case APRV2_IBASIC_RSP_RESULT:
      case VOICE_CMD_START_HOST_PCM_V2:
      case VOICE_CMD_STOP_HOST_PCM:
      case VOICE_EVT_PUSH_HOST_BUF_V2:
      case VSM_CMD_SET_PKT_EXCHANGE_MODE:
      case VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG:
      case VSM_CMD_REGISTER_EVENT:
      case VSM_CMD_UNREGISTER_EVENT:
      case VSM_CMD_ENC_SET_RATE:
         {

            /* Queue packet onto VSM command Q via standard Elite op/payload format */
            if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->svc_handle.cmdQ,(uint64_t*)&sMsg)))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM APR isr Q to manager: qurt_elite_queue_push_back error = %d!!\n",(int) result);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY );
            }
            break;
         }

      case VSM_EVT_SEND_DEC_PACKET:
         /* route this to dec endpoint */
         /* VSM_SEND_DEC/ENC_PACKET is an event so don't respond, just return error code if necessary */
         /* Queue packet onto VSM decoder Q session pointed by dst_port via standard Elite op/payload format */

         /* TODO: Map dst_port to array index...now just use directly */


         /* if we've already stopped the session, complete this request by finishing with BAD_STATE response */
         if( VSM_RUN_STATE != vsm_ptr->session[session_index]->state)
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM APR isr Q to endpoint: VSM_STOP_STATE!!");
         }
         else if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->dataQ,(uint64_t*)&sMsg)))
         {
            //if queue is overflowed, empty the queue first and then push back the pkt
            //the idea is keep dataQ depth 1 and have the latest pkt in case of overflow
            if (ADSP_ENEEDMORE == result)
            {
               elite_msg_any_t dataQMsg;
               elite_apr_packet_t    *data_msg_payload_ptr;
               result = qurt_elite_queue_pop_front(vsm_ptr->session[session_index]->rx->dataQ, (uint64_t*)&dataQMsg);
               // return the buffer to its rightful q.
               if (ADSP_EOK == result)
               {
                  data_msg_payload_ptr = (elite_apr_packet_t *) dataQMsg.pPayload;
                  if (NULL != data_msg_payload_ptr)
                  {
                     elite_apr_if_free(vsm_ptr->apr_handle, data_msg_payload_ptr);
                  }
                  //now push back the latest pkt to dec dataq
                  if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->dataQ,(uint64_t*)&sMsg)))
                  {
                     rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
                     MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
                  }
                  else
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM APR isr Q to endpoint: dec dataq is overwritten with latest pkt\n");
                  }
               }
               else
               {
                  rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM APR isr Q to endpoint: dec dataq pop failed, result(%d)",result);
               }
            }
            else
            {
               rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
            }
         }
         break;

      case VSM_EVT_OOB_DEC_BUF_READY:

         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM received VSM_EVT_OOB_DEC_BUF_READY ");
         if( VSM_RUN_STATE != vsm_ptr->session[session_index]->state)
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_OOB_DEC_BUF_READY received in STOP STATE :INVALID EVENT");
         }
         else if (vsm_ptr->session[session_index]->pkt_exchange_mode == IN_BAND)
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_OOB_DEC_BUF_READY received for IN_BAND mode :INVALID EVENT");
         }
         else if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&sMsg)))
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
         }

         break;

      case VSM_EVT_OOB_ENC_BUF_CONSUMED:
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM received VSM_EVT_OOB_ENC_BUF_CONSUMED ");
         if( VSM_RUN_STATE != vsm_ptr->session[session_index]->state)
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_OOB_ENC_BUF_CONSUMED received in STOP STATE :INVALID EVENT");
         }
         else if (vsm_ptr->session[session_index]->pkt_exchange_mode == IN_BAND)
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_EVT_OOB_ENC_BUF_CONSUMED received for IN_BAND mode :INVALID EVENT");
         }
         else if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)&sMsg)))
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
         }
         break;
      case VSM_EVT_OUTOFBAND_TTY_RX_CHAR_PUSH:
         /* route this to dec endpoint */
         if ((NULL != vsm_ptr->session[session_index]->rx)&&( VSM_RUN_STATE == vsm_ptr->session[session_index]->state))
         {
            if( ADSP_FAILED(result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&sMsg)))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr , APR_EBUSY);
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q qurt_elite_queue_push_back error for rx char push, session (%d)!!\n",(int) session_index);
            }
         }
         else
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: rx session doesn't exist or not in run state, session (%d)!!\n",(int) session_index);
         }
         break;

     case VSM_CMD_SET_EAMR_MODE_CHANGE_DETECTION:
         /* route this to dec endpoint */
         if (NULL != vsm_ptr->session[session_index]->rx)
         {
            if( ADSP_FAILED(result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&sMsg)))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr , APR_EBUSY);
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
            }
         }
         else
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: rx session (%x) doesn't exist\n",(int) session_index);
         }
         break;

      case VSM_CMD_ENC_SET_MINMAX_RATE:
      case VSM_CMD_ENC_SET_RATE_MOD:
      case VSM_CMD_ENC_SET_DTX_MODE:
      case VSM_CMD_SET_EVS_ENC_OPERATING_MODE:
      case VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE:
      case VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE:
         /* route this to enc endpoint */
         /* these are cmds so need to respond*/
         /* Queue packet onto VSM encoder Q session pointed by dst_port via standard Elite op/payload format */

         /* TODO: Map dst_port to array index...now just use directly */

         if (NULL != vsm_ptr->session[session_index]->tx)
         {
            if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)&sMsg)))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY );
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
            }
         }
         else
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: vsm_ptr->session[%d]->tx->cmdQ is NULL!!\n",
                  (int) session_index);
         }
         break;
      case VSM_CMD_SET_RX_DTMF_DETECTION:
         {
            /* check if valid rx session exists */
            if (NULL != vsm_ptr->session[session_index]->rx)
            {
               if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&sMsg)))
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY );
                  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: vsm_ptr->session[%d]->rx->cmdQ is NULL!!\n",(int) session_index);
            }
            break;
         }
      case VSM_CMD_SET_DTMF_GENERATION:
         {
            vsm_set_dtmf_generation_t *dtmf_gen_ptr = (vsm_set_dtmf_generation_t *) elite_apr_if_get_payload_ptr(apr_packet_ptr);
            /* check for valid dtmf generation direction */
            if(VSM_SET_DTMF_GEN_TX != dtmf_gen_ptr->direction)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP:  DTMF generation not supported in RX direction : session[%d] \n",(int) session_index);
            }
            else
            {
               /* now check if valid Tx session exists */
               if (NULL != vsm_ptr->session[session_index]->tx)
               {
                  if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)&sMsg)))
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY );
                     MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
                  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: vsm_ptr->session[%d]->tx->cmdQ is NULL!!\n",
                        (int) session_index);
               }
            }
            break;
         }
      default:
         if ( elite_apr_if_msg_type_is_cmd( apr_packet_ptr) )
         {
            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EUNSUPPORTED );
         }
         else
         {
            rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
         }
         break;
   }

   return APR_EOK;   // incoming command freed/ended/processed...need to return EOK
}

// TODO: can most of this be moved to a utility function, shared by multiple services? Probably...
ADSPResult voice_strm_mgr_create (uint32_t inputParam, void **handle)
{
   // static variable counter to make queue and thread name strings unique
   // limit to 16 bits so it will roll over and and only cost 4 characters in hexadecimal.
   // Queues in existence likely to have unique names, but not required...
   int32_t result;
   int32_t  rc;

   // allocate instance struct
   vsm_ptr = (VoiceStrmMgr_t*) qurt_elite_memory_malloc( sizeof(voice_strm_mgr_t), QURT_ELITE_HEAP_DEFAULT);
   if (!vsm_ptr)
   {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Memory allocation failed for VSM structure !!");
       return ADSP_ENOMEMORY;
   }

   // zero out all the fields.
   memset(vsm_ptr, 0, sizeof(voice_strm_mgr_t));

   // Create the queues. Use non-blocking queues, since pselect is always used.
   // pselect blocks on any non-masked queue to receive, then can do non-blocking
   // checks.

   vsm_ptr->svc_handle.cmdQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );
   vsm_ptr->vsm_tx_resp_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );
   vsm_ptr->vsm_rx_resp_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );

   if ( ( NULL == vsm_ptr->svc_handle.cmdQ ) || ( NULL == vsm_ptr->vsm_tx_resp_q_ptr ) || ( NULL == vsm_ptr->vsm_rx_resp_q_ptr ) )
   {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Memory allocation failed for VSM cmd q or tx/rx resp q !!");
       return ADSP_ENOMEMORY;
   }
   if (ADSP_FAILED(result = qurt_elite_queue_init(
               cmdQName, MAX_CMD_Q_ITEMS, vsm_ptr->svc_handle.cmdQ)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: MQ creation failed %d\n", (int)result);
      return result;
   }

   if (ADSP_FAILED(result = qurt_elite_queue_init((char*)"vsm_tx_resp_q_ptr",
               MAX_CMD_Q_ITEMS, vsm_ptr->vsm_tx_resp_q_ptr)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Tx RespQ creation failed\n");
      return result;
   }

   if (ADSP_FAILED(result = qurt_elite_queue_init((char*)"vsm_rx_resp_q_ptr",
               MAX_CMD_Q_ITEMS, vsm_ptr->vsm_rx_resp_q_ptr)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Rx RespQ creation failed\n");
      return result;
   }

   // Initialize the channel
   qurt_elite_channel_init(&vsm_ptr->channel);

   if (ADSP_FAILED(result = qurt_elite_channel_addq(&vsm_ptr->channel, (vsm_ptr->svc_handle.cmdQ),NULL)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: MQ add to channel failed = %d\n", (int)result);
      return result;
   }

   if (ADSP_FAILED(result = qurt_elite_channel_addq(&vsm_ptr->channel, (vsm_ptr->vsm_tx_resp_q_ptr),NULL)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx RespQ add to channel failed = %d\n", (int)result);
      return result;
   }

   if (ADSP_FAILED(result = qurt_elite_channel_addq(&vsm_ptr->channel, (vsm_ptr->vsm_rx_resp_q_ptr),NULL)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Rx RespQ add to channel failed = %d\n", (int)result);
      return result;
   }
   // populate me
   vsm_ptr->svc_handle.unSvcId = ELITE_VOICE_STREAM_SVCID;

   if( ADSP_FAILED( result = voice_mmpm_register( &vsm_ptr->mmpm,
               "VSM_ADSP",
               MMPM_CORE_ID_LPASS_ADSP)))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: MMPM_Register_Ext for VPM failed\n");
      return ADSP_EFAILED;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: MMPM Registration done for VSM, received client_id(%lu)\n", vsm_ptr->mmpm.client_id);

   // Launch the thread
   if (ADSP_FAILED(result = qurt_elite_thread_launch(&(vsm_ptr->svc_handle.threadId), VSM_THREAD_NAME, NULL,
               VSM_THREAD_STACK_SIZE, ELITETHREAD_STAT_VOICE_STREAM_SVC_PRIO, vsm_thread, (void*)vsm_ptr,QURT_ELITE_HEAP_DEFAULT)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Thread creation failed\n");
      return result;
   }

   rc = elite_apr_if_register_by_name(
         &vsm_ptr->apr_handle, &vsm_ptr->apr_addr, (char*)"qcom.adsp.vsm", sizeof("qcom.adsp.vsm"), vsm_isr_recv_fn,NULL);

   if (rc)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR register error %d, continuing...", (int)rc);
   }

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM APR register apr_handle(0x%x) apr_addr(%x), fn(%x)",
         (int) vsm_ptr->apr_handle,(int) vsm_ptr->apr_addr,(int) vsm_isr_recv_fn);

   // register with qurt_elite memory map.
   qurt_elite_memorymap_register(&vsm_memory_map_client);
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Registered as memory map client");

   *handle = vsm_ptr;
   return ADSP_EOK;
}

/**
 * Main Thread for all commands
 */

static int vsm_thread(void* instance_ptr)
{
   // You tell it which MQ's to wait on, but you have to also pass in the 1 + the maximum
   // MQ descriptor of all the MQ's in the mask. That is why all this weirdness.
   uint32_t cmd_mask, wait_mask, output_mask, rx_resp_mask, tx_resp_mask;
   int32_t     rc = APR_EOK;

   ADSPResult result = ADSP_EOK;                            // general result value
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_msg_any_t msg;

   // get the bitfield for the cmdQ from channel;
   cmd_mask = qurt_elite_queue_get_channel_bit(vsm_ptr->svc_handle.cmdQ);
   rx_resp_mask = qurt_elite_queue_get_channel_bit(vsm_ptr->vsm_rx_resp_q_ptr);
   tx_resp_mask = qurt_elite_queue_get_channel_bit(vsm_ptr->vsm_tx_resp_q_ptr);

   wait_mask = cmd_mask|rx_resp_mask|tx_resp_mask;

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Entering VSM workloop...");

   // Enter forever loop
   for(;;)
   {
      // block on any one or more of selected queues to get a msg
      output_mask = qurt_elite_channel_wait(&vsm_ptr->channel, wait_mask);

      // Check if commands came in
      if (output_mask & cmd_mask)
      {
         // call common utility to process command queue in standard way.
         const uint32_t cmd_table_size = sizeof(handler_ptr)/sizeof(handler_ptr[0]);
         result = elite_svc_process_cmd_queue(instance_ptr, &(vsm_ptr->svc_handle), handler_ptr, cmd_table_size);

         // If service has been destroyed, exit.
         if (ADSP_ETERMINATED == result) return ADSP_EOK;
      }
      else if (output_mask & rx_resp_mask)
      {
         /* drive all async VSM actions from acks */
         if (ADSP_FAILED(result = qurt_elite_queue_pop_front(vsm_ptr->vsm_rx_resp_q_ptr, (uint64_t*)&msg)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to get Rx response !!!");
         }
         else
         {
            /* handle all acks from FADD messages here.  This can drive various state machines for pending APR commands. */
            rc = vsm_process_msg( vsm_ptr, &msg, VSM_RX_RESP_Q);
            if (ADSP_FAILED(rc))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! VSM Message processing has returned error %d, continuing...", (int)rc);
            }

            /* free the incoming response */
            elite_msg_return_payload_buffer( &msg );
         }
      } //else if (outputMask & rxRespMask)
      else if (output_mask & tx_resp_mask)
      {
         /* drive all async VSM actions from acks */
         if (ADSP_FAILED(result = qurt_elite_queue_pop_front(vsm_ptr->vsm_tx_resp_q_ptr, (uint64_t*)&msg)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to get Tx response !!!");
         }
         else
         {
            /* handle all acks from FADD messages here.  This can drive various state machines for pending APR commands. */
            rc = vsm_process_msg( vsm_ptr, &msg, VSM_TX_RESP_Q);
            if (ADSP_FAILED(rc))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! VSM Message processing has returned error %d, continuing...", (int)rc);
            }

            /* free the incoming response */
            elite_msg_return_payload_buffer( &msg );
         }
      } //else if (outputMask & txRespMask)
      // there was error reading queue.
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Error %d reading cmd queue!!\n",(int) result);
      }
   }

   // close mq
   qurt_elite_queue_deinit(vsm_ptr->svc_handle.cmdQ);
   qurt_elite_memory_free(vsm_ptr->svc_handle.cmdQ);

   // close rxRespQ
   qurt_elite_queue_deinit(vsm_ptr->vsm_rx_resp_q_ptr);
   qurt_elite_memory_free(vsm_ptr->vsm_rx_resp_q_ptr);

   // close txRespQ
   qurt_elite_queue_deinit(vsm_ptr->vsm_tx_resp_q_ptr);
   qurt_elite_memory_free(vsm_ptr->vsm_tx_resp_q_ptr);

   /* Release the APR resource...doesn't occur since we don't destroy the static service */
   rc = elite_apr_if_deregister( vsm_ptr->apr_handle );
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }


   // free alloc
   qurt_elite_memory_free(vsm_ptr);

   return 0;
}

/* =======================================================================
 **                     Message handler functions
 ** ======================================================================= */

/* This function processes all acks that come back from Enc/Dec/Mixer for FADD requests sent
 * from VSM.  An Ack can be used to drive different events...ie the next FADD message in the
 * sequence, or if the sequence is done then the APR command ack can be sent, and the APR packet freed.
 * The client token of an FADD command/ack embeds an index to the tracking structure for all pending APR
 * commands.  This function can directly look up the APR command that the FADD ack is tied to, and proceed with the
 * proper sequence.
 */
static ADSPResult vsm_process_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure

   elite_apr_packet_t* apr_packet_ptr;
   /* Cmd Q -> TBD use common function for all Msgs */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      /* Rx/Tx Ack Q */
      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = param_payload_ptr->unResponseResult;

      if( ADSP_FAILED( response_result))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Ack on qId(%d) for apr_packet_ptr(%x) on session_index(%x) : unResponseResult(%d)",
               (int)qid,(int)apr_packet_ptr,(int)session_index,(int)response_result);
      }
   }

   if (NULL != apr_packet_ptr)
   {
      switch( elite_apr_if_get_opcode(apr_packet_ptr))
      {

         case VSM_CMD_CREATE_SESSION:
            {
               // Associate who is creating the session based on the Domain ID of the client.
               // If the client is the Second Application Domain (APPS), then this supercedes every other setup
               // and assumes the application to be Test client running on Target. Meaning, a single instance
               // of VSM communicating with APPS directly implies that VSM will request for clocks.
               uint16_t client_domain_id = ((elite_apr_if_get_src_addr(apr_packet_ptr)>>8) & 0xFF);
               if (APRV2_IDS_DOMAIN_ID_APPS2_V == client_domain_id)
               {
                  vsm_ptr->req_clock = TRUE;
               }
               result = vsm_process_create_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_REINIT_SESSION:
            {
               result = vsm_process_reinit_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_DESTROY_SESSION:
            {
               result = vsm_process_destroy_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_START_SESSION:
            {
               result = vsm_process_run_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_STOP_SESSION:
            {
               result = vsm_process_stop_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_SET_MEDIA_TYPE:
            {
               result = vsm_process_set_media_type_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_SET_STREAM_PP_SAMP_RATE:
            {
               result = vsm_process_set_stream_pp_samp_rate_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
            /* common function handles attach/detach */
         case VSM_CMD_ATTACH_VOICE_PROC:
         case VSM_CMD_DETACH_VOICE_PROC:
            {
               result = vsm_process_attach_detach_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_SET_TTY_MODE:
            {
               result = vsm_process_set_tty_mode_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_OUTOFBAND_SET_TTY_MODE:
         {
            result = vsm_process_oob_set_tty_mode_msg( vsm_ptr, msg_ptr, qid);
            break;
         }
         case VSM_CMD_RESYNC_CTM:
            {
               result = vsm_process_ctm_resync_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_SET_PARAM_V2:
         case VOICE_CMD_SET_PARAM_V3:
            {
               result = vsm_process_set_param_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_GET_PARAM_V2:
         case VOICE_RSP_GET_PARAM_ACK:
            {
               result = vsm_process_get_param_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_RSP_GET_KPPS_ACK:
            {
               result = vsm_process_get_kpps_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_RSP_GET_DELAY_ACK:
            {
               result = vsm_process_get_delay_msg( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_SET_TIMING_PARAMS:
         case VSM_CMD_SET_TIMING_PARAMS:
         case VSM_CMD_SET_TIMING_PARAMS_V2:
            {
               result = vsm_process_set_vfr ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_SET_SOFT_MUTE_V2:
            {
               result = vsm_process_set_soft_mute ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_START_RECORD_V2:
            {
               result = vsm_process_start_record_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_STOP_RECORD:
            {
               result = vsm_process_stop_record_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_START_HOST_PCM_V2:
            {
               result = vsm_process_start_hpcm_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VOICE_CMD_STOP_HOST_PCM:
            {
               result = vsm_process_stop_hpcm_msg ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_SET_PKT_EXCHANGE_MODE:
            {
               result = vsm_process_set_pkt_exchange_mode ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG:
            {
               result = vsm_process_set_oob_pkt_exchange_config ( vsm_ptr, msg_ptr, qid);
               break;
            }
         case VSM_CMD_ENC_SET_RATE:
            {
               result = vsm_process_set_enc_rate ( vsm_ptr, msg_ptr, qid);
               break;
            }
      }
   }
   else
   {
      /* the ack was linked to an incorrect or expired APR packet...should never happen */
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: NULL APR packet");
   }
   return result;
}



static ADSPResult vsm_process_create_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      /* Rx/Tx Ack Q */
      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         /* create sequence here */
         else if( VSM_ACTION_CREATE_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            result = vsm_connect_rx_downstream( vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_CONNECT_DOWNSTREAM == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_CREATE_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         // TODO: check ResponseResult before sending APR status back
         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         uint16_t handle = voice_map_session_index_to_handle(session_index);

         rc = elite_apr_if_alloc_send_ack_result(
               vsm_ptr->apr_handle,
               elite_apr_if_get_dst_addr( apr_packet_ptr),
               (apr_result == APR_EOK) ? handle : 0,      /* give back new handle if valid */
               elite_apr_if_get_src_addr( apr_packet_ptr),
               elite_apr_if_get_src_port( apr_packet_ptr),
               elite_apr_if_get_client_token( apr_packet_ptr),
               elite_apr_if_get_opcode( apr_packet_ptr),
               apr_result);

         vsm_ptr->session[session_index]->my_handle = handle;
         /* free the original packet */
         rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);

         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM session_index(%x) Create Complete, giving handle(%x)",
               (int)session_index, (int)handle);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_run_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      /* Rx/Tx Ack Q */

      /* Start sequence is VSM_ACTION_RUN_PORT -> VSM_ACTION_RUN_SERVICE -> VSM_ACTION_NONE on both Rx and Tx */

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_RUN_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            /* Rx Mixer port finished running, so start Dec */
            /* kick off asynchronous commands to Dec --> keep same token for proper indexing */
            result = vsm_run_rx(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_RUN_SERVICE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_RUN_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            /* Tx Mixer port finished running, so start Enc */
            /* kick off asynchronous commands to Enc --> keep same token for proper indexing */
            /* this will change state_rx to RUN_SERVICE */
            result = vsm_run_tx(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_RUN_SERVICE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         uint16_t src_addr = elite_apr_if_get_src_addr(apr_packet_ptr);
         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         if( APR_EOK == apr_result)
         {
            vsm_ptr->session[session_index]->state   = VSM_RUN_STATE;
         }

         /* Don't clear pending flag if this command came from VSM itself */
         if(src_addr != vsm_ptr->apr_addr)
         {
            vsm_ptr->session[session_index]->pending = FALSE;
         }

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Transitioned to Run state",(int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}
static ADSPResult vsm_process_stop_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_STOP_SERVICE == async_struct_ptr->async_status[free_index].state_rx)
         {
            /* this will change state_rx to STOP_PORT */
            result = vsm_control_rx_mixer_input(vsm_ptr, session_index,
                  ELITEMSG_CUSTOM_VMX_STOP,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_STOP_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_STOP_SERVICE == async_struct_ptr->async_status[free_index].state_tx)
         {
            /* this will change state_tx to STOP_PORT */
            result = vsm_control_tx_mixer_output(vsm_ptr, session_index,
                  ELITEMSG_CUSTOM_VMX_STOP,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_STOP_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {

         /* clear the structure out and finish the APR response */

         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         uint16_t src_addr = elite_apr_if_get_src_addr(apr_packet_ptr);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         if( APR_EOK == apr_result)
         {
            vsm_ptr->session[session_index]->state   = VSM_STOP_STATE;
         }
         /* Don't clear pending flag if this command came from VSM itself */
         if(src_addr != vsm_ptr->apr_addr)
         {
            vsm_ptr->session[session_index]->pending = FALSE;
         }

         /* TODO: convert into messages? to Enc/Dec */
         /* clear flags since RESYNC_CTM handler doesn't check if we are using a vocoder that supports CTM */
         vsm_ptr->session[session_index]->tty_state.m_sync_recover_rx = FALSE;
         vsm_ptr->session[session_index]->tty_state.m_sync_recover_tx = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         // Clear mask to indicate session has stopped.
         vsm_ptr->session_run_mask = voice_clr_bit (vsm_ptr->session_run_mask, session_index);
         /* Assess clock requests. */
         vsm_set_mmpm_clocks(vsm_ptr);

         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Transitioned to STOP state",(int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_reinit_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      /* Rx/Tx Ack Q */

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         /* reinit dynamic service sequence here */
         else if( VSM_ACTION_REINIT_DYNAMIC_SVC == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx Reinit dyn svc
         }
         /* create sequence here */
         else if( VSM_ACTION_CREATE_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            result = vsm_connect_rx_downstream( vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_CONNECT_DOWNSTREAM == async_struct_ptr->async_status[free_index].state_rx)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM connected to rx matrix! PortID = %d!!\n",
                  (int) vsm_ptr->session[session_index]->mixer_in_port_ptr->inport_id);
            /* send current mediaType to Rx...must be valid */
            result = vsm_send_rx_media_type( vsm_ptr, session_index,
                  vsm_ptr->session[session_index]->media_type_format,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_SET_MEDIATYPE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx MT handling
         }
         /* Destroy seqeunce here */
         else if( VSM_ACTION_REASSIGN_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            /* in reinit we are hardcoding mask to 0 to automatically detach all voiceprocs connected */
            vsm_ptr->session[session_index]->input_mapping_mask = 0;
            result = vsm_close_rx_mixer_input(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_CLOSE_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            vsm_ptr->session[session_index]->mixer_in_port_ptr = NULL;

            /* destroy Rx session */
            qurt_elite_thread_t rx_thread_t = vsm_ptr->session[session_index]->rx->threadId;

            elite_msg_any_t rx_destroy_msg_t;
            uint32_t size = sizeof(elite_msg_cmd_destroy_svc_t);
            result = elite_msg_create_msg( &rx_destroy_msg_t,
                  &size,
                  ELITE_CMD_DESTROY_SERVICE,
                  vsm_ptr->vsm_rx_resp_q_ptr,
                  NULL,
                  ADSP_EOK);
            /* if action attempt fails, kill state machine and save fail code */
            if( ADSP_FAILED( result))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg rx_destroy_msg_t, session_index(%x)",(int) session_index);
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
            else
            {
               result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&rx_destroy_msg_t);
               /* if action attempt fails, kill state machine and save fail code */
               if (ADSP_FAILED(result))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push to dec cmdQ, error = %d, , session_index(%x)",(int) result,(int) session_index);
                  async_struct_ptr->async_status[free_index].result_rx = result;
                  async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
                  elite_msg_return_payload_buffer( &rx_destroy_msg_t);
               }
               else
               {
                  qurt_elite_thread_join(rx_thread_t, &result);
                  vsm_ptr->session[session_index] = NULL;

                  elite_msg_return_payload_buffer( &rx_destroy_msg_t );
                  /* finish sequence */
                  async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
               }
            }
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         /* reinit dynamic service sequence here */
         else if( VSM_ACTION_REINIT_DYNAMIC_SVC == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;   // done with Tx Reinit dyn svc
         }
         /* create sequence here */
         else if( VSM_ACTION_CREATE_PORT   == async_struct_ptr->async_status[free_index].state_tx)
         {
            /* send current mediaType to Tx....must be valid */
            result = vsm_send_tx_media_type( vsm_ptr, session_index,
                  vsm_ptr->session[session_index]->media_type_format,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_SET_MEDIATYPE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;   // done with Rx MT handling
         }
         /* Destroy seqeunce here */
         else if( VSM_ACTION_REASSIGN_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            vsm_ptr->session[session_index]->output_mapping_mask = 0;
            result = vsm_close_tx_mixer_output(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_CLOSE_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            vsm_ptr->session[session_index]->mixer_out_port_ptr = NULL;

            /* destroy tx session */
            qurt_elite_thread_t tx_thread_t = vsm_ptr->session[session_index]->tx->threadId;

            elite_msg_any_t tx_destroy_msg_t;
            uint32_t size = sizeof(elite_msg_cmd_destroy_svc_t);
            result = elite_msg_create_msg( &tx_destroy_msg_t,
                  &size,
                  ELITE_CMD_DESTROY_SERVICE,
                  vsm_ptr->vsm_tx_resp_q_ptr,
                  NULL,
                  ADSP_EOK);
            /* if action attempt fails, kill state machine and save fail code */
            if( ADSP_FAILED( result))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg tx_destroy_msg_t, session_index(%x)",(int) session_index);
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
            else
            {
               result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)&tx_destroy_msg_t);
               /* if action attempt fails, kill state machine and save fail code */
               if (ADSP_FAILED(result))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push to enc cmdQ, error = %d, session_index(%x)",(int) result,(int) session_index);
                  async_struct_ptr->async_status[free_index].result_tx = result;
                  async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
                  elite_msg_return_payload_buffer( &tx_destroy_msg_t);
               }
               else
               {
                  qurt_elite_thread_join(tx_thread_t, &result);
                  vsm_ptr->session[session_index]->tx = NULL;

                  elite_msg_return_payload_buffer( &tx_destroy_msg_t );

                  /* finish sequence */
                  async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
               }
            }
         }
      }
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         vsm_ptr->session[session_index]->pending = FALSE;
         /* clearing the VSM media type upon receiving the response from venc/vdec reinit commands
            in which media type gets cleard */
         vsm_ptr->session[session_index]->media_type_format = VSM_MEDIA_TYPE_NONE;
         vsm_ptr->session[session_index]->pkt_exchange_mode = IN_BAND;
         vsm_ptr->session[session_index]->oob_config_received = FALSE;


         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Reinit Complete",(int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_stream_pp_samp_rate_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      vsm_set_samp_rate_t   *vsm_set_samp_rate_cmd_ptr = (vsm_set_samp_rate_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      /* skip some checks since Enc/Dec are sent MEDIATYPE FADD messages, and Tx mixer is sent Custom msgs
         Sequence for Rx is VSM_ACTION_SET_PP_SAMP_RATE->VSM_ACTION_NONE.
         Sequence for Tx is VSM_ACTION_SET_PP_SAMP_RATE -> VSM_ACTION_RECONFIG_PORT -> VSM_ACTION_NONE(if SampRate change) */
      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_PP_SAMP_RATE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx MT handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_PP_SAMP_RATE == async_struct_ptr->async_status[free_index].state_tx)
         {

            /* if SR changes, then need to reconfigure Tx mixer output, else we are done with Tx sequence */
            if( vsm_ptr->session[session_index]->tx_stream_pp_samp_rate !=
                  vsm_set_samp_rate_cmd_ptr->tx_samp_rate)
            {

               if( NULL == vsm_ptr->session[session_index]->mixer_out_port_ptr)
               {
                  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Set Stream PP Samp Rate response received from encoder: Attempting to reconfigure MixerOut before creating port\n");
                  async_struct_ptr->async_status[free_index].result_tx = ADSP_EUNEXPECTED;
                  async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
               }
               else
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Set Stream PP Samp Rate response from Encoder: VSM sending reconfigure tx port sampRate(%d)\n",
                        (int)vsm_set_samp_rate_cmd_ptr->tx_samp_rate);

                  result = vsm_reconfigure_tx_mixer_output( vsm_ptr,
                        session_index,
                        vsm_set_samp_rate_cmd_ptr->tx_samp_rate,   // use new session samp rate
                        &async_struct_ptr->async_status[free_index],
                        param_payload_ptr->unClientToken);
                  if(ADSP_FAILED( result))
                  {
                     async_struct_ptr->async_status[free_index].result_tx = result;
                     async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
                  }
               }
            }
            else
            {
               /* Mediatype ack and no matrix reconfig required -> done */
               async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
               vsm_ptr->session[session_index]->tx_stream_pp_samp_rate = vsm_set_samp_rate_cmd_ptr->tx_samp_rate;
            }
         }
         else if( VSM_ACTION_RECONFIG_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM ack from reconfigure tx port to sampRate(%d)\n",
                  (int)vsm_set_samp_rate_cmd_ptr->tx_samp_rate);

            /* port is reconfigured, now we are done */
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
            vsm_ptr->session[session_index]->tx_stream_pp_samp_rate = vsm_set_samp_rate_cmd_ptr->tx_samp_rate;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         /*if( APR_EOK == apr_result)
           {
           vsm_ptr->session[session_index]->session_samp_rate = vsm_set_samp_rate_cmd_ptr->session_samp_rate;
           }*/
         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) changed to stream pp sampling rate : tx(0x%x) rx(0x%x)",
               (int)session_index,(int)vsm_set_samp_rate_cmd_ptr->tx_samp_rate,(int)vsm_set_samp_rate_cmd_ptr->rx_samp_rate);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_media_type_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      vsm_set_media_type_t   *media_type_cmd_ptr = (vsm_set_media_type_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      /* skip some checks since Enc/Dec are sent MEDIATYPE FADD messages, and Tx mixer is sent Custom msgs */
      /* Sequence is VSM_ACTION_SET_MEDIATYPE on Rx.  Sequence for Tx is VSM_ACTION_SET_MEDIATYPE -> VSM_ACTION_RECONFIG_PORT (if SampRate change) */
      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_MEDIATYPE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx MT handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_MEDIATYPE == async_struct_ptr->async_status[free_index].state_tx)
         {

            /* if SR changes, then add this step in, else we are done with Tx sequence */
            if( voice_get_sampling_rate( vsm_ptr->session[session_index]->media_type_format,0/*BeAMR flag for encoder*/, (uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/) !=
                  voice_get_sampling_rate( media_type_cmd_ptr->media_type,0/*BeAMR flag for encoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/))
            {

               if( NULL == vsm_ptr->session[session_index]->mixer_out_port_ptr)
               {
                  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Set MT: Attempting to reconfigure MixerOut before creating port\n");
                  async_struct_ptr->async_status[free_index].result_tx = ADSP_EUNEXPECTED;
                  async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
               }
               else
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM sending reconfigure tx port sampRate(%d)\n",
                        (int)voice_get_sampling_rate( media_type_cmd_ptr->media_type,0/*BeAMR flag for enacoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/));

                  result = vsm_reconfigure_tx_mixer_output( vsm_ptr,
                        session_index,
                        voice_get_sampling_rate( media_type_cmd_ptr->media_type,0/*BeAMR flag for encoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/),   // use new MT
                        &async_struct_ptr->async_status[free_index],
                        param_payload_ptr->unClientToken);
                  if(ADSP_FAILED( result))
                  {
                     async_struct_ptr->async_status[free_index].result_tx = result;
                     async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
                  }
               }
            }
            else
            {
               /* Mediatype ack and no matrix reconfig required -> done */
               async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_RECONFIG_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM ack from reconfigure tx port to sampRate(%d)\n",
                  (int)voice_get_sampling_rate( media_type_cmd_ptr->media_type,0/*BeAMR flag for enacoder*/, (uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/));
             vsm_ptr->session[session_index]->tx_stream_pp_samp_rate = voice_get_sampling_rate( media_type_cmd_ptr->media_type,0/*BeAMR flag for enacoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/);


            /* port is reconfigured, now we are done */
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         if( APR_EOK == apr_result)
         {
            vsm_ptr->session[session_index]->media_type_format = media_type_cmd_ptr->media_type;
         }
         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) changed to media_type(0x%x)",
               (int)session_index,(int)media_type_cmd_ptr->media_type);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_destroy_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      /* Rx/Tx Ack Q */

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_CLOSE_PORT == async_struct_ptr->async_status[free_index].state_rx)
         {
            vsm_ptr->session[session_index]->mixer_in_port_ptr = NULL;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_CLOSE_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            vsm_ptr->session[session_index]->mixer_out_port_ptr = NULL;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         /* do this last, since me->session[nSessionIndex] becomes invalid --> TODO: do this if aprResult != APR_EOK ?? */
         result = vsm_destroy_session(session_index);

         // make apr_result a generic failure if the above result failed
         if (ADSP_FAILED(result))
         {
            apr_result = APR_EFAILED;
         }
         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Destroy Complete",(int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_attach_detach_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      vsm_routing_config_t   *route_cmd_ptr = (vsm_routing_config_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         /* reassign sequence here */
         else if( VSM_ACTION_REASSIGN_PORT == async_struct_ptr->async_status[free_index].state_rx )
         {
            uint32_t new_str_mask = vsm_get_str_mask(route_cmd_ptr->rx_device_count,
                  &route_cmd_ptr->handles[route_cmd_ptr->tx_device_count]);

            if( VSM_CMD_ATTACH_VOICE_PROC == elite_apr_if_get_opcode( apr_packet_ptr))
            {
               vsm_ptr->session[session_index]->input_mapping_mask |= new_str_mask;
            }
            else
            {
               vsm_ptr->session[session_index]->input_mapping_mask &= ~new_str_mask;
            }
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: session_index(%x) Reassign Rx updated mask(%x)",
                  (int)session_index,(int)vsm_ptr->session[session_index]->input_mapping_mask);
            async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
         }
         else
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }

      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         /* Reassign sequence here */
         else if( VSM_ACTION_REASSIGN_PORT == async_struct_ptr->async_status[free_index].state_tx)
         {
            uint32_t new_in_ports_mask = vsm_get_in_ports_mask(route_cmd_ptr->tx_device_count,
                  &route_cmd_ptr->handles[0]);
            if( VSM_CMD_ATTACH_VOICE_PROC == elite_apr_if_get_opcode( apr_packet_ptr))
            {
               vsm_ptr->session[session_index]->output_mapping_mask |= new_in_ports_mask;
            }
            else
            {
               vsm_ptr->session[session_index]->output_mapping_mask &= ~new_in_ports_mask;
            }

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: session_index(%x) Reassign Tx updated mask(%x)",
                  (int)session_index,(int)vsm_ptr->session[session_index]->output_mapping_mask);
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;

            /* if reassign done (not with _RUN) then we have completed Rx sequence.  If _RUN, blindly go
             * to run state, assuming dyn service/mixer can handle operating in Run state with no devices
             * attached, as will happen during a device switch
             */

         }
      }
      /* Run sequence here */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         uint16_t src_addr = elite_apr_if_get_src_addr(apr_packet_ptr);
         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         /* Don't clear pending flag if this command came from VSM itself */
         if(src_addr != vsm_ptr->apr_addr)
         {
            vsm_ptr->session[session_index]->pending = FALSE;
         }

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Attach/Detach Complete",(int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_tty_mode_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   //elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q */
   if( VSM_CMD_Q == qid)
   {
      //apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   return result;
}
static ADSPResult vsm_process_oob_set_tty_mode_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   //elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q */
   if( VSM_CMD_Q == qid)
   {
      //apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   return result;
}
static ADSPResult vsm_process_ctm_resync_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   //elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      //apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   return result;
}

static ADSPResult vsm_process_set_vfr( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_VFR == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_VFR == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_pkt_exchange_mode( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      vsm_set_pkt_exchange_mode_t *pkt_exchange_mode_ptr = (vsm_set_pkt_exchange_mode_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_PKT_EXCHANGE_MODE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_PKT_EXCHANGE_MODE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         if( APR_EOK == apr_result)
         {
            vsm_ptr->session[session_index]->pkt_exchange_mode = pkt_exchange_mode_ptr->pkt_exchange_mode;
         }
         vsm_ptr->session[session_index]->pending = FALSE;

         /* reset  oob_config_received flag as OOB config command must follow mode command */
         vsm_ptr->session[session_index]->oob_config_received = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: RESPONSE for pkt_exchange_mode session_index(%x) pkt_exchange_mode(%d)",
               (int)session_index,(int)pkt_exchange_mode_ptr->pkt_exchange_mode);
      }

   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_oob_pkt_exchange_config( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_OOB_PKT_EXCHANGE_CONFIG == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_OOB_PKT_EXCHANGE_CONFIG == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         if( APR_EOK == apr_result)
         {
            vsm_ptr->session[session_index]->oob_config_received = TRUE;
         }
         vsm_ptr->session[session_index]->pending = FALSE;
         voice_end_async_control_rx_tx( async_struct_ptr, free_index);

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: RESPONSE for pkt exchange config end session_index(%x) ",(int)session_index);
      }

   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_set_soft_mute( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      voice_set_soft_mute_v2_t *set_mute_cmd_ptr = (voice_set_soft_mute_v2_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_SOFT_MUTE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_SOFT_MUTE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Set Soft Mute for direction(%d) to mute(%d),ramp_duration(%d)",
               (int)session_index,(int)set_mute_cmd_ptr->direction,(int)set_mute_cmd_ptr->mute,(int)set_mute_cmd_ptr->ramp_duration);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}


static ADSPResult vsm_process_set_param_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_SET_PARAM ack, qId(%d) session_index(%x),index(%d),result(%d)",
            qid, (int)session_index,(int)free_index,(int)response_result );

      if( VSM_RX_RESP_Q == qid)
      {

         if( VSM_ACTION_SET_PARAM == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
         }
      }
      else
      {
         if( VSM_ACTION_SET_PARAM == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }


      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         uint32_t result_rx = async_struct_ptr->async_status[free_index].result_rx;
         uint32_t result_tx = async_struct_ptr->async_status[free_index].result_tx;
         int32_t apr_result;

         /* decide on apr_result to send to client _*/
         if (   ((ADSP_EOK == result_tx) && (ADSP_EOK == result_rx || ADSP_EUNSUPPORTED == result_rx)) ||
               ((ADSP_EOK == result_rx) && (ADSP_EOK == result_tx || ADSP_EUNSUPPORTED == result_tx))
            )
         {  // if result was success from both tx and rx return success result
            apr_result = APR_EOK;
         }
         else
         {  // return failure
            apr_result = APR_EBADPARAM;
         }

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );
         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_SET_PARAM complete session_index(%x),index(%d) result(%d)",
               (int) session_index,(int) free_index, (int) apr_result);

      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_get_param_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_param_cal_t *param_payload_ptr = (elite_msg_param_cal_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;
      // opcode determines in band or out of band, since for in band, we overwrite original packet with ack
      uint32_t opcode = elite_apr_if_get_opcode(apr_packet_ptr);

      MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_PARAM ack, qId(%d) session_index(%x),index(%d),result(%d)",
            qid, (int)session_index,(int)free_index,(int)response_result );

      if( VSM_RX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_PARAM == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      if( VSM_TX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_PARAM == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }

      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {

         voice_get_param_v2_t* voice_get_params_ptr=NULL;
#if defined(__qdsp6__) && !defined(SIM)
         uint32_t payload_address = 0;
#endif
         if(VOICE_CMD_GET_PARAM_V2 == opcode )
         {
            // Out of band data, so flush cache
            if (NULL != vsm_memory_map_client)
            {
               elite_mem_shared_memory_map_t memmap = {0};
               voice_get_params_ptr = (voice_get_param_v2_t*) elite_apr_if_get_payload_ptr(apr_packet_ptr);
               memmap.unMemMapClient = vsm_memory_map_client;
               memmap.unMemMapHandle = voice_get_params_ptr->mem_map_handle;
               result = elite_mem_map_get_shm_attrib(voice_get_params_ptr->payload_address_lsw,
                     voice_get_params_ptr->payload_address_msw,
                     voice_get_params_ptr->param_max_size,
                     &memmap);

               if( ADSP_SUCCEEDED(result))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM Memory Map Phy(%x) virtual address(%x)",
                        (int)voice_get_params_ptr->payload_address_lsw,(int) memmap.unVirtAddr);
#if defined(__qdsp6__) && !defined(SIM)
                  payload_address = memmap.unVirtAddr;
#endif
                  result = elite_mem_flush_cache_v2(&memmap);
               }
            }
            else
            {
               result = ADSP_EFAILED;
            }
            { // sending out of band result
               voice_get_param_ack_t get_param_ack;
               if (NULL != voice_get_params_ptr &&
                     (ADSP_EOK == async_struct_ptr->async_status[free_index].result_tx ||
                      ADSP_EOK == async_struct_ptr->async_status[free_index].result_rx))
               {
                  get_param_ack.status = APR_EOK;


#if defined(__qdsp6__) && !defined(SIM)
                  int8_t *bufptr[4] = { (int8_t *) payload_address, NULL, NULL, NULL };
                  voice_log_buffer( bufptr,
                        VOICE_LOG_CHAN_VSM_GET_PARAM,
                        session_index,
                        qurt_elite_timer_get_time(),
                        VOICE_LOG_DATA_FORMAT_PCM_MONO,
                        1,   /* dummy value */
                        voice_get_params_ptr->param_max_size,
                        NULL);
#endif

               }
               else
               {
                  get_param_ack.status = APR_EBADPARAM;
               }
               elite_apr_if_alloc_send_ack(vsm_ptr->apr_handle,
                     apr_packet_ptr->dst_addr,
                     apr_packet_ptr->dst_port,
                     apr_packet_ptr->src_addr,
                     apr_packet_ptr->src_port,
                     apr_packet_ptr->token,
                     VOICE_RSP_GET_PARAM_ACK,
                     &get_param_ack,
                     sizeof(voice_get_param_ack_t)
                     );

               elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);

               if( ADSP_FAILED(result))
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM Get Param get virtual addr or flush error");
               }
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Get Param ack apr_packet_ptr(0x%p)",apr_packet_ptr);
               //break;
            }
         }
         else
         {
            voice_get_param_ack_t* get_param_ack_ptr = (voice_get_param_ack_t*) elite_apr_if_get_payload_ptr(apr_packet_ptr);

            // fill the response status
            if (ADSP_EOK == async_struct_ptr->async_status[free_index].result_tx ||
                  ADSP_EOK == async_struct_ptr->async_status[free_index].result_rx)
            {
               get_param_ack_ptr->status = APR_EOK;
            }
            else
            {
               get_param_ack_ptr->status = APR_EBADPARAM;
            }
            // send get param ack back
            elite_apr_if_async_send( vsm_ptr->apr_handle, apr_packet_ptr);

            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM Get Param In-Band end ack packet(0x%p)",apr_packet_ptr);
         }
         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_PARAM complete session_index(%x),index(%d)",
               (int)session_index,(int) free_index);
      }
   }

   return result;
}

static ADSPResult vsm_process_get_kpps_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t* param_payload_ptr = (elite_msg_any_payload_t*) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = param_payload_ptr->unResponseResult;

      MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_KPPS ack, qId(%d) session_index(%x),index(%d),result(%d)",
            qid, (int)session_index,(int)free_index,(int)response_result );

      if( VSM_RX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_KPPS == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      if( VSM_TX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_KPPS == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }

      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         {
            // send get param ack back
            elite_apr_if_async_send( vsm_ptr->apr_handle, apr_packet_ptr);

            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM Get KPPS In-Band end ack packet(0x%p)",apr_packet_ptr);
         }
         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_KPPS complete session_index(%x),index(%d)",
               (int)session_index,(int) free_index);
      }
   }

   return result;
}

static ADSPResult vsm_process_get_delay_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t* param_payload_ptr = (elite_msg_any_payload_t*) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = param_payload_ptr->unResponseResult;

      MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_DELAY ack, qId(%d) session_index(%x),index(%d),result(%d)",
            qid, (int)session_index,(int)free_index,(int)response_result );

      if( VSM_RX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_DELAY == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
      }
      if( VSM_TX_RESP_Q == qid)
      {
         if( VSM_ACTION_GET_DELAY == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }

      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         {
            // send delay ack back
            elite_apr_if_async_send( vsm_ptr->apr_handle, apr_packet_ptr);

            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM Get delay end ack packet(0x%p)",apr_packet_ptr);
         }
         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_GET_DELAY complete session_index(%x),index(%d)",
               (int)session_index,(int) free_index);
      }
   }

   return result;
}

static ADSPResult vsm_run_tx(void* instance_ptr, uint16_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_tx_t;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_RunTx Invoked, session_index(%x)", session_index);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   if(vsm_ptr->session[session_index]->tx)
   {
      size = sizeof(elite_msg_cmd_run_t);
      result = elite_msg_create_msg( &msg_tx_t,
            &size,
            ELITE_CMD_RUN,
            vsm_ptr->vsm_tx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
         elite_msg_return_payload_buffer( &msg_tx_t);
         return result;
      }
      async_status_ptr->state_tx = VSM_ACTION_RUN_SERVICE;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_RunTx Pending");
   return result;
}


static ADSPResult vsm_run_rx(void* instance_ptr, uint16_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_rx_t;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_RunRx Invoked, session_index(%x)",(int) session_index);

   if(vsm_ptr->session[session_index]->rx)
   {
      size = sizeof(elite_msg_cmd_run_t);
      result = elite_msg_create_msg( &msg_rx_t,
            &size,
            ELITE_CMD_RUN,
            vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
         elite_msg_return_payload_buffer( &msg_rx_t);
         return result;
      }
      async_status_ptr->state_rx = VSM_ACTION_RUN_SERVICE;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_RunRx Pending");
   return result;
}

static ADSPResult vsm_stop_tx(void* instance_ptr, uint16_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_tx_t;

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_StopTx Invoked session_index(%x)",(int)session_index);

   if(vsm_ptr->session[session_index]->tx)
   {
      size = sizeof(elite_msg_cmd_run_t);
      result = elite_msg_create_msg( &msg_tx_t,
            &size,
            ELITE_CMD_PAUSE,
            vsm_ptr->vsm_tx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
         elite_msg_return_payload_buffer( &msg_tx_t);
         return result;
      }
      async_status_ptr->state_tx = VSM_ACTION_STOP_SERVICE;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_StopTx Pending");
   return result;

}


static ADSPResult vsm_stop_rx(void* instance_ptr, uint16_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_rx_t;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_StopRx Invoked session_index(%x)",session_index);

   if(vsm_ptr->session[session_index]->rx)
   {
      size = sizeof(elite_msg_cmd_stop_t);
      result = elite_msg_create_msg( &msg_rx_t,
            &size,
            ELITE_CMD_PAUSE,
            vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
         elite_msg_return_payload_buffer( &msg_rx_t);
         return result;
      }
      async_status_ptr->state_rx = VSM_ACTION_STOP_SERVICE;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_StopRx Pending");
   return result;

}

static ADSPResult vsm_reinit_session( uint16_t session_index, uint16_t mode, async_status_rxtx_t *async_status_ptr, uint32_t token)
{

   ADSPResult   result = ADSP_EOK;

   if( vsm_ptr->session[session_index] != NULL )
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM Reinit session_index(%x) psessionIndex(%x), myHandle(0x%x)",
            (int) session_index,(int) vsm_ptr->session[session_index],vsm_ptr->session[session_index]->my_handle);
   }
   else
   {
      return ADSP_EHANDLE;
   }

   if( mode > TX_AND_RX)
   {
      return ADSP_EBADPARAM;
   }
   // TODO: This needs to be split into Rx reinit and Tx reinit for efficiency and proper error handling

   // on reinit, reset common TTY state info except TTY mode (keep mode until explicitly changed with SET_TTY_MODE)
   // -> dynamic service will go thru init/reinit to fully reset CTM algorithms
   uint16_t tty_mode = vsm_ptr->session[session_index]->tty_state.m_etty_mode;
   memset( &vsm_ptr->session[session_index]->tty_state, 0, sizeof( voice_strm_tty_state_t));
   vsm_ptr->session[session_index]->tty_state.m_etty_mode = tty_mode;

   // reset event registrations on reinit
   vsm_ptr->session[session_index]->vocoder_mode_event_registered = 0;

   /* check what is enabled in new mode */
   uint8_t Tx = ( mode == TX_ONLY   ||
         mode == TX_AND_RX);

   uint8_t Rx = ( mode == RX_ONLY   ||
         mode == TX_AND_RX );

   /* check if need to reinit active Rx session (including CTM) */
   if(  vsm_ptr->session[session_index]->rx && Rx)
   {

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Sending Vdec reinit, nSessionIndex(%x)",(int) session_index);
      if( ADSP_FAILED( result = vsm_reinit_rx_session( vsm_ptr, session_index, async_status_ptr, token)))
      {
         return result;
      }
   }
   /* check if need to destroy active Rx module */
   else if( vsm_ptr->session[session_index]->rx && !Rx)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Destroying Rx session, nSessionIndex(%x)",(int) session_index);

      /* reconfig input port to detach -> start process, driven by acks */
      if( ADSP_FAILED( result = vsm_reassign_rx_mixer_input( vsm_ptr, session_index, 0, async_status_ptr, token)))
      {
         return result;
      }
   }
   /* check if need to create Rx module */
   else if( !vsm_ptr->session[session_index]->rx && Rx)
   {
      if (ADSP_FAILED(result = vdec_create (NULL,
                  &vsm_ptr->session[session_index]->apr_info,
                  &vsm_ptr->session[session_index]->tty_state,        /* shared ttyState info */
                  (void**)&vsm_ptr->session[session_index]->rx,
                  (uint32_t)voice_map_session_index_to_handle(session_index)
                  )))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: voice_dec Creation Failed");
         return result;
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Created Rx module session_index(%x)",(int) session_index);
      }
      /* kick off create sequence -> port create */
      if( ADSP_FAILED( result = vsm_create_rx_mixer_input( vsm_ptr, session_index, async_status_ptr, token)))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Rx mixer port creation failed");
         return result;
      }
   }

   /* check if need to reinit active Tx session (including CTM) */
   if(  vsm_ptr->session[session_index]->tx && Tx)
   {

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Sending Venc reinit, session_index(%x)",(int) session_index);
      if( ADSP_FAILED( result = vsm_reinit_tx_session( vsm_ptr, session_index, async_status_ptr, token)))
      {
         return result;
      }
   }
   /* check if need to destroy active Tx module -> start process..driven by acks */
   else if( vsm_ptr->session[session_index]->tx && !Tx)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Destroying Tx session, session_index(%x)",(int) session_index);

      /* kick off sequence -> reconfig output port to detach */
      if( ADSP_FAILED( result = vsm_reassign_tx_mixer_output( vsm_ptr, session_index, 0, async_status_ptr, token)))
      {
         return result;
      }
   }
   /* check if need to create Tx module */
   else if( !vsm_ptr->session[session_index]->tx && Tx)
   {
      if (ADSP_FAILED(result = venc_create (NULL,
                  &vsm_ptr->session[session_index]->apr_info,
                  &vsm_ptr->session[session_index]->tty_state,        /* shared ttyState info */
                  vsm_ptr->session[session_index]->rx,                /* always give Rx handle to Tx, for possible loopback.  NULL if no Rx.  Loopback is dynamically enabled/disabled*/
                  (void**)&vsm_ptr->session[session_index]->tx,
                  (uint32_t) voice_map_session_index_to_handle(session_index)
                  )))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: voice_enc Creation Failed");
         return result;
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Created Tx module session_index(%x)",(int) session_index);
      }
      //kick off create sequence with Port creation
      if( ADSP_FAILED( result = vsm_create_tx_mixer_output( vsm_ptr,
                  session_index,
                  voice_get_sampling_rate( vsm_ptr->session[session_index]->media_type_format,0/*BeAMR flag for encoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/),
                  async_status_ptr,
                  token)))
      {
         return result;
      }
   }
   return result;
}
static ADSPResult vsm_destroy_session( uint16_t session_index)
{
   ADSPResult   result = ADSP_EOK;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_DESTROY_SESSION, session_index(%x)",(int) session_index);

   if( vsm_ptr->session[session_index] != NULL )
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM Destroy session_index(%x) session_index_ptr(%x), my_handle(0x%x)",
            (int) session_index,(int) vsm_ptr->session[session_index],vsm_ptr->session[session_index]->my_handle);
   }
   else
   {
      return ADSP_EHANDLE;
   }

   qurt_elite_thread_t tx_thread_t;
   qurt_elite_thread_t rx_thread_t;

   // Cache the thread id as context will be lost once destroy message is sent
   // destroy enc first so tx loopback stops before decoder is destroyed */
   if( vsm_ptr->session[session_index]->tx)
   {

      // TODO: error handling
      tx_thread_t = vsm_ptr->session[session_index]->tx->threadId;

      elite_msg_any_t tx_destroy_msg_t;
      uint32_t size = sizeof(elite_msg_cmd_destroy_svc_t);
      result = elite_msg_create_msg( &tx_destroy_msg_t,
            &size,
            ELITE_CMD_DESTROY_SERVICE,
            vsm_ptr->vsm_tx_resp_q_ptr,
            NULL,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg tx_destroy_msg_t, session_index(%x)",(int) session_index);
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)&tx_destroy_msg_t);

      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push destroy msg to enc cmdQ, session_index(%x)",(int) session_index);
         elite_msg_return_payload_buffer( &tx_destroy_msg_t);
         return result;
      }

      // Wait for the threads to finish
      qurt_elite_thread_join(tx_thread_t, &result);
      vsm_ptr->session[session_index]->tx = NULL;

      elite_msg_return_payload_buffer( &tx_destroy_msg_t );
   }
   if( vsm_ptr->session[session_index]->rx)
   {
      // TODO: error handling
      rx_thread_t = vsm_ptr->session[session_index]->rx->threadId;

      elite_msg_any_t rx_destroy_msg_t;
      uint32_t size = sizeof(elite_msg_cmd_destroy_svc_t);
      result = elite_msg_create_msg( &rx_destroy_msg_t,
            &size,
            ELITE_CMD_DESTROY_SERVICE,
            vsm_ptr->vsm_rx_resp_q_ptr,
            NULL,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg rx_destroy_msg_t, session_index(%x)",(int) session_index);
         return result;
      }

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&rx_destroy_msg_t);

      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to send destroy msg to dec cmdQ, session_index(%x)",(int) session_index);
         elite_msg_return_payload_buffer( &rx_destroy_msg_t);
         return result;
      }

      qurt_elite_thread_join(rx_thread_t, &result);
      vsm_ptr->session[session_index]->rx = NULL;

      elite_msg_return_payload_buffer( &rx_destroy_msg_t );
   }

   vsm_ptr->session[session_index]->vsm_ptr = NULL;

   // reset the VSM handle
   qurt_elite_memory_free(vsm_ptr->session[session_index]);

   vsm_ptr->session[session_index] = NULL;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_DESTROY_SESSION complete, session_index(%x)",(int) session_index);

   return result;
}

static ADSPResult vsm_create_session( elite_apr_packet_t *apr_packet_ptr, uint16_t *session_index_ptr)
{

   vsm_create_session_t *vsm_create_session_cmd_ptr;
   ADSPResult       result = ADSP_EOK;
   int32_t i;

   *session_index_ptr = 0;

   vsm_create_session_cmd_ptr = (vsm_create_session_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

   /* Do create session work and get status --> TBD index properly */
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_CREATE_SESSION, mode(%d)",
         (int) vsm_create_session_cmd_ptr->mode);

   if( TX_AND_RX < vsm_create_session_cmd_ptr->mode)
   {
      return ADSP_EBADPARAM;
   }

   for( i = 0; i < VSM_MAX_SESSIONS; i++)
   {
      /* find unused session */
      if( NULL == vsm_ptr->session[i])
      {
         break;
      }
   }
   if( i == VSM_MAX_SESSIONS)
   {
      /* no sessions available */
      return ADSP_ENORESOURCE;
   }

   //Create a new session
   vsm_ptr->session[i] =  (voice_strm_session_t*) qurt_elite_memory_malloc
      ( sizeof(voice_strm_session_t), QURT_ELITE_HEAP_DEFAULT);

   if( NULL == vsm_ptr->session[i])
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM_CMD_CREATE_SESSION,Failed to allocate memory for new session !");
      return ADSP_ENOMEMORY;
   }
   memset( vsm_ptr->session[i], 0, sizeof( voice_strm_session_t));

   // init the state
   vsm_ptr->session[i]->state = VSM_STOP_STATE;
   vsm_ptr->session[i]->media_type_format = VSM_MEDIA_TYPE_NONE;   // assumes default creation of 8k sampRate ports
   vsm_ptr->session[i]->rx_tap_point = VSM_TAP_POINT_NONE;
   vsm_ptr->session[i]->tx_tap_point = VSM_TAP_POINT_NONE;

   // Store handle to the VSM for debugging
   vsm_ptr->session[i]->vsm_ptr = vsm_ptr;

   /* initialize rx/tx */
   vsm_ptr->session[i]->tx = NULL;
   vsm_ptr->session[i]->rx = NULL;
   vsm_ptr->session[i]->pkt_exchange_mode = IN_BAND;
   vsm_ptr->session[i]->oob_config_received=FALSE;
   *session_index_ptr = i;

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Creating VSM session_index(%lx) session_ptr(%p)",i,vsm_ptr->session[i]);

   /* pass APR messaging info to Enc/Dec services, including src_port which is our session handle */
   /* Note the input params are SelfAddr/SelfPort/ClientAddr/ClientPort in that order.  "Self" is the Q6 VSM APR, so it's actually  */
   /* the destination address of the original APR packet that triggered this VSM create, referenced as pkt here.  ClientAddr is the */
   /* src_addr/port of the original APR packet that triggered this VSM create (CVS APR Address) */


   vsm_ptr->session[i]->apr_info.self_addr   = elite_apr_if_get_dst_addr(apr_packet_ptr);
   vsm_ptr->session[i]->apr_info.self_port   = voice_map_session_index_to_handle(i);  /* Give dyn service actual handle for packet exchange  */
   vsm_ptr->session[i]->apr_info.client_addr = elite_apr_if_get_src_addr(apr_packet_ptr);
   vsm_ptr->session[i]->apr_info.client_port = elite_apr_if_get_src_port(apr_packet_ptr);
   vsm_ptr->session[i]->apr_info.apr_handle  = vsm_ptr->apr_handle;

   /* currently create decoder first, so we can pass the Rx handle to Encoder for loopback mode. */
   switch( vsm_create_session_cmd_ptr->mode)
   {
      case RX_ONLY:
      case TX_AND_RX:
         if (ADSP_FAILED(result = vdec_create (NULL,
                     &vsm_ptr->session[i]->apr_info,
                     &vsm_ptr->session[i]->tty_state,        /* shared ttyState info */
                     (void**)&vsm_ptr->session[i]->rx, (uint32_t) voice_map_session_index_to_handle(i)
                     )))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: voice_dec Creation Failed");
            return result;
         }
         break;
      default:
         break;
   }
   switch( vsm_create_session_cmd_ptr->mode)
   {
      case TX_ONLY:
      case TX_AND_RX:
         if (ADSP_FAILED(result = venc_create (NULL,
                     &vsm_ptr->session[i]->apr_info,
                     &vsm_ptr->session[i]->tty_state,        /* shared ttyState info */
                     vsm_ptr->session[i]->rx,                /* always give Rx handle to Tx for possible loopback.  NULL if no Rx session.  Loopback enable is dynamic */
                     (void**)&vsm_ptr->session[i]->tx,(uint32_t) voice_map_session_index_to_handle(i)
                     )))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: voice_enc Creation Failed");
            return result;
         }
         break;
      default:
         break;
   }

   // Connect To AFE
   if (qurt_elite_globalstate.pAfeStatSvcCmdQ == NULL)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to get Afe cmdQ session");
      return ADSP_EUNSUPPORTED;
   }
   vsm_ptr->vsm_afe_cmd_q_ptr = qurt_elite_globalstate.pAfeStatSvcCmdQ;


   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_CREATE_SESSION pending session_num(%d)",*session_index_ptr);
   return result;
}

static uint32_t vsm_get_str_mask(int32_t rxcount, uint16_t* handles_ptr)
{
   uint32_t result = 0;
   int32_t count;
   for(count = 0; count < rxcount; count++)
   {
      uint16_t index = voice_map_handle_to_session_index( handles_ptr[count]);
      if(voiceProcMgr->session[index])
      {
         if(voiceProcMgr->session[index]->mixer_out_port)
         {
            result |= 1<<(voiceProcMgr->session[index]->mixer_out_port->outport_id);
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: NULL output port %u, ignoring!",(int) index);
         }
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: NULL session %u, ignoring!",(int) index);
      }
   }
   return result;
}

static uint32_t vsm_get_in_ports_mask(int32_t txcount, uint16_t* handles_ptr)
{
   uint32_t result = 0;
   int32_t count;
   for(count = 0; count < txcount; count++)
   {
      uint16_t index = voice_map_handle_to_session_index( handles_ptr[count]);
      if(voiceProcMgr->session[index])
      {
         if(voiceProcMgr->session[index]->mixer_in_port)
         {
            result |= 1<<(voiceProcMgr->session[index]->mixer_in_port->inport_id);
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: NULL input port %u, ignoring!",(int) index);
         }
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: NULL session %u, ignoring!",(int) index);
      }
   }
   return result;
}



static ADSPResult vsm_process_apr_msg(void* instance_ptr, elite_msg_any_t* msg_ptr)
{

   ADSPResult result = ADSP_EOK;
   int32_t rc = APR_EOK;
   ADSPResult rc_rx=ADSP_EOK;
   ADSPResult rc_tx=ADSP_EOK;
   uint32_t free_index;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   assert(msg_ptr);

   elite_apr_packet_t *apr_packet_ptr = (elite_apr_packet_t *) msg_ptr->pPayload;
   uint16_t handle = elite_apr_if_get_dst_port( apr_packet_ptr);
   uint16_t src_addr = elite_apr_if_get_src_addr( apr_packet_ptr);
   uint16_t session_index = voice_map_handle_to_session_index( handle );

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_ProcessAprMsg Message handler Invoked, op_code(0x%x),Token(0x%x),session_index(%x)",
         (int)elite_apr_if_get_opcode(apr_packet_ptr),
         (int)elite_apr_if_get_client_token(apr_packet_ptr),
         (int)session_index);

   /* parse out the received pakcet.  Note in this current framework we are not prioritizing commands
    * that can be completed immediately (non-gating).  We are simply processing command in order they are received, need a separate
    * Elite Q to push to for command which can't be handled directly from this main dispatcher */

   switch( elite_apr_if_get_opcode( apr_packet_ptr) )
   {

      case VSM_CMD_CREATE_SESSION:
         {
            uint16_t  create_index;

            /* tbd -> should dyn services get handle instead of session index */
            result = vsm_create_session( apr_packet_ptr, &create_index);

            if( ADSP_SUCCEEDED(result))
            {
               /* special case, use newly created session_index here, not session_index */

               /* check if we have a place to hold this pending packet */
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[create_index]->async_struct, apr_packet_ptr, &free_index);

               if( vsm_ptr->session[create_index]->rx)
               {
                  rc_rx = vsm_create_rx_mixer_input( vsm_ptr, create_index,
                        &vsm_ptr->session[create_index]->async_struct.async_status[free_index],
                        VOICE_ASYNC_STUFF_TOKEN(create_index,free_index));
                  /* initialize results */
                  vsm_ptr->session[create_index]->async_struct.async_status[free_index].result_rx = rc_rx;
               }
               if( vsm_ptr->session[create_index]->tx)
               {
                  rc_tx = vsm_create_tx_mixer_output( vsm_ptr,
                        create_index,
                        voice_get_sampling_rate( vsm_ptr->session[create_index]->media_type_format,0/*BeAMR flag for encoder*/,(uint16_t )VOICE_FB_SAMPLING_RATE/*evs default sampling rate*/),   /* should be MEDIATYPE_NONE at create -> default 8K */
                        &vsm_ptr->session[create_index]->async_struct.async_status[free_index],
                        VOICE_ASYNC_STUFF_TOKEN(create_index,free_index));
                  vsm_ptr->session[create_index]->async_struct.async_status[free_index].result_tx = rc_tx;
               }
               /* check if any action attempt succeeded...if so need to handle an ack */
               if( (vsm_ptr->session[create_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                     (vsm_ptr->session[create_index]->tx && ADSP_SUCCEEDED(rc_tx)))
               {
                  vsm_ptr->session[create_index]->pending = TRUE;
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                  voice_end_async_control_rx_tx( &vsm_ptr->session[create_index]->async_struct, free_index);
               }
            }
            else
            {
               /* mapping is same from ADSP <-> APR error codes */
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, (int32_t) result);
            }

            break;

         }
      case VSM_CMD_REINIT_SESSION:
         {

            vsm_create_session_t *vsm_create_session_cmd_ptr;

            if( vsm_ptr->session[session_index]->state != VSM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {

               /* check if we have a place to hold this pending packet */
               result = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               if( ADSP_SUCCEEDED(result))
               {
                  vsm_create_session_cmd_ptr = (vsm_create_session_t *)
                     elite_apr_if_get_payload_ptr( apr_packet_ptr);

                  /* Do create session work and get status --> TBD index properly */
                  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM_CMD_REINIT_SESSION, session_index(%x),mode(%d)",
                        (int) session_index,(int)vsm_create_session_cmd_ptr->mode);

                  result = vsm_reinit_session( session_index, vsm_create_session_cmd_ptr->mode,
                        &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                        VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                  /* initialize results */
                  vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = result;
                  vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = result;

                  /* set to pending, since we need to handle an ack */
                  if( ADSP_SUCCEEDED(result))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }
      case VSM_CMD_DESTROY_SESSION:
         {

            if( vsm_ptr->session[session_index]->state != VSM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {

               /* check if we have a place to hold this pending packet */
               result = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if( ADSP_SUCCEEDED(result))
               {
                  //Destroy mixer ports created
                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_close_rx_mixer_input( vsm_ptr, session_index,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_close_tx_mixer_output( vsm_ptr, session_index,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }

         /* handle Attach and Detach -> same operation except different masking */
      case VSM_CMD_ATTACH_VOICE_PROC:
      case VSM_CMD_DETACH_VOICE_PROC:
         {
            MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Attach/Detach voice proc received in VSM APR handler");
            vsm_routing_config_t *route_command_ptr = (vsm_routing_config_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            if( route_command_ptr->rx_device_count  != 0    &&
                  vsm_ptr->session[session_index]->rx == NULL)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM );
            }
            else if( route_command_ptr->tx_device_count  != 0    &&
                  vsm_ptr->session[session_index]->tx == NULL)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM );
            }
            /* Allow command in pending state if it comes from VSM itself
               This is necessary in situations where a single APR command ends up mapping into multiple APR commands
             */
            else if( (vsm_ptr->session[session_index]->pending == TRUE) && (vsm_ptr->apr_addr != src_addr))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {

               /* This command is now allowed in Run state.  When in run state, the sequence will (behind the scenes)
                  stop the port/service, perform the attach or detach, then run the port/service.
                */

               /* check if we have a place to hold this pending packet */
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               if( ADSP_SUCCEEDED(rc))
               {
                  if(route_command_ptr->rx_device_count != 0)
                  {
                     uint32_t new_str_mask = vsm_get_str_mask(route_command_ptr->rx_device_count,
                           &route_command_ptr->handles[route_command_ptr->tx_device_count]);
                     if( VSM_CMD_ATTACH_VOICE_PROC == elite_apr_if_get_opcode( apr_packet_ptr))
                     {
                        new_str_mask = vsm_ptr->session[session_index]->input_mapping_mask | new_str_mask;
                     }
                     else
                     {
                        new_str_mask = vsm_ptr->session[session_index]->input_mapping_mask & ~new_str_mask;
                     }
                     rc_rx = vsm_reassign_rx_mixer_input(vsm_ptr, session_index,
                           new_str_mask,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if(route_command_ptr->tx_device_count != 0)
                  {

                     //Need to send a VMX_REASSIGN_OUTPUT with output_mapping_mask updated
                     uint32_t new_in_ports_mask = vsm_get_in_ports_mask(
                           route_command_ptr->tx_device_count,
                           &route_command_ptr->handles[0]);
                     if( VSM_CMD_ATTACH_VOICE_PROC == elite_apr_if_get_opcode( apr_packet_ptr))
                     {
                        new_in_ports_mask = vsm_ptr->session[session_index]->output_mapping_mask |
                           new_in_ports_mask;
                     }
                     else
                     {
                        new_in_ports_mask = vsm_ptr->session[session_index]->output_mapping_mask & ~new_in_ports_mask;
                     }
                     rc_tx = vsm_reassign_tx_mixer_output(vsm_ptr, session_index,
                           new_in_ports_mask,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( ((route_command_ptr->rx_device_count != 0) && ADSP_SUCCEEDED(rc_rx)) ||
                        ((route_command_ptr->tx_device_count != 0) && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
         }
         break;

      case VSM_CMD_START_SESSION:
         {

            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            /* Allow command in pending state if it comes from VSM itself
               This is necessary in situations where a single APR command ends up mapping into multiple APR commands
             */
            else if(( vsm_ptr->session[session_index]->pending == TRUE) && (vsm_ptr->apr_addr != src_addr))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else if ((vsm_ptr->session[session_index]->pkt_exchange_mode == OUT_OF_BAND)&&!(vsm_ptr->session[session_index]->oob_config_received))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ERROR : OOB_CONFIG not received in OOB mode can't go to RUN STATE  session(%x)",(int)session_index);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, ADSP_EUNEXPECTED);
            }
            else
            {

               /* check if we have a place to hold this pending packet */
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if( ADSP_SUCCEEDED(rc))
               {
                  /* setup token for proper indexing to asyncStruct */
                  if( vsm_ptr->session[session_index]->rx)
                  {
                     rc_rx = vsm_control_rx_mixer_input(vsm_ptr, session_index,
                           ELITEMSG_CUSTOM_VMX_RUN,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if( vsm_ptr->session[session_index]->tx)
                  {
                     rc_tx = vsm_control_tx_mixer_output(vsm_ptr, session_index,
                           ELITEMSG_CUSTOM_VMX_RUN,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;

                     // Set mask to indicate session has started.
                     vsm_ptr->session_run_mask = voice_set_bit (vsm_ptr->session_run_mask, session_index);

                     /* Assess clock requests. */
                     /* TODO: Would placing the call so late cause a frame or two to be
                        processed at an undesired clock setting? */
                     vsm_set_mmpm_clocks(vsm_ptr);
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
         }
         break;

      case VSM_CMD_STOP_SESSION:
         {
            if( vsm_ptr->session[session_index]->state == VSM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            /* Allow command in pending state if it comes from VSM itself
               This is necessary in situations where a single APR command ends up mapping into multiple APR commands
             */
            else if((vsm_ptr->session[session_index]->pending == TRUE) && (vsm_ptr->apr_addr != src_addr))
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {

               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* kick off Enc/Dec Stop.  setup token for proper indexing to asyncStruct.  Acks from Enc/Dec will trigger mixer stop */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->rx)
                  {
                     rc_rx = vsm_stop_rx(vsm_ptr, session_index,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if( vsm_ptr->session[session_index]->tx)
                  {
                     rc_tx = vsm_stop_tx(vsm_ptr, session_index,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
         }
         break;

      case VSM_CMD_SET_TTY_MODE:
         {

            vsm_set_tty_mode_t   *tty_mode_cmd_ptr = (vsm_set_tty_mode_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_TTY_MODE, mode(%d), session_state(0x%x)",
                  (int)tty_mode_cmd_ptr->mode,
                  (int)vsm_ptr->session[session_index]->state );

            if( vsm_ptr->session[session_index]->state != VSM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else
            {
               /* setup TTY mode */
               vsm_ptr->session[session_index]->tty_state.m_etty_mode = tty_mode_cmd_ptr->mode;
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EOK );
            }
         }
         break;

      case VSM_CMD_OUTOFBAND_SET_TTY_MODE:
         {

            vsm_outofband_set_tty_mode_t   *tty_mode_cmd_ptr = (vsm_outofband_set_tty_mode_t *)
                                  elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_OOB_SET_TTY_MODE, mode(%d), session_state(0x%x)",
                                      (int)tty_mode_cmd_ptr->mode,
                                      (int)vsm_ptr->session[session_index]->state );

            if( vsm_ptr->session[session_index]->state != VSM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else
            {
               /* setup TTY mode */
               vsm_ptr->session[session_index]->tty_state.m_oobtty_mode = tty_mode_cmd_ptr->mode;
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EOK );
            }
         }
         break;

      case VSM_CMD_SET_MEDIA_TYPE:
         {

            vsm_set_media_type_t   *media_type_cmd_ptr = (vsm_set_media_type_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_MEDIA_TYPE, media_type(0x%x), session_state(0x%x)",
                  (int)media_type_cmd_ptr->media_type,
                  (int)vsm_ptr->session[session_index]->state );

            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change media_type(0x%x->0x%x) in RUN state",
                     (int)vsm_ptr->session[session_index]->media_type_format,
                     (int)media_type_cmd_ptr->media_type);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* kick off Enc/Dec MT config. */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_media_type( vsm_ptr, session_index,
                           media_type_cmd_ptr->media_type,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_media_type( vsm_ptr, session_index,
                           media_type_cmd_ptr->media_type,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }
      case VSM_CMD_SET_STREAM_PP_SAMP_RATE:
         {

            vsm_set_samp_rate_t   *samp_rate_cmd_ptr = (vsm_set_samp_rate_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: SET_STREAM_PP_SAMP_RATE_CMD(0x%x), uplink sampling rate(0x%x), dnlink sampling rate(0x%x), session_state(0x%x)",
                  (int)elite_apr_if_get_opcode(apr_packet_ptr),
                  (int)samp_rate_cmd_ptr->tx_samp_rate,
                  (int)samp_rate_cmd_ptr->rx_samp_rate,
                  (int)vsm_ptr->session[session_index]->state );
            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change sampling rate in RUN state");
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_samp_rate( vsm_ptr, session_index,
                           samp_rate_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_samp_rate( vsm_ptr, session_index,
                           samp_rate_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: failed to push stream_pp_samp_rate_cmd to the dynamic services");
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: async state initialisation failed for stream_pp_samp_rate_cmd");
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
         }
         break;

      case VSM_CMD_RESYNC_CTM:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_RESYNC_CTM, session_state(0x%x)",
                  (int)vsm_ptr->session[session_index]->state );

            /* TODO: convert into messages? to Enc/Dec */
            vsm_ptr->session[session_index]->tty_state.m_sync_recover_rx = TRUE;
            vsm_ptr->session[session_index]->tty_state.m_sync_recover_tx = TRUE;

            rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EOK );
         }
         break;
      case VOICE_CMD_SET_PARAM_V2:
         {
            //SetParam is implemented only for Rx-Path as there is no calibration requiremnt
            //for Tx-Path.It can be extended to Tx-Path when required.
            uint32_t size = 0;
            elite_msg_any_t msg_rx_t;
            elite_msg_any_t msg_tx_t;
            uint32_t free_index;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            uint32_t payload_address = 0;
            elite_mem_shared_memory_map_t mem_shared_memory_map = {0};
            voice_set_param_v2_t* voice_set_params_ptr = (voice_set_param_v2_t*) elite_apr_if_get_payload_ptr(apr_packet_ptr);

            mem_shared_memory_map.unMemMapClient = vsm_memory_map_client;
            mem_shared_memory_map.unMemMapHandle = voice_set_params_ptr->mem_map_handle;
            if(NULL != voice_set_params_ptr->mem_map_handle)
            {  // Out of band data, so invalidate cache
               if (NULL != vsm_memory_map_client)
               {
                  result = voice_cmn_check_align_size(voice_set_params_ptr->payload_address_lsw, voice_set_params_ptr->payload_address_msw, voice_set_params_ptr->payload_size, 4);
                  if( ADSP_FAILED(result))
                  {
                     MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM set param: address has to be 4-byte aligned and size has to be multiple of 4 bytes, lsw(%lx) msw(%lx) size(%lx)",voice_set_params_ptr->payload_address_lsw, voice_set_params_ptr->payload_address_msw, voice_set_params_ptr->payload_size);
                  }
                  else
                  {
                     result = elite_mem_map_get_shm_attrib(voice_set_params_ptr->payload_address_lsw, voice_set_params_ptr->payload_address_msw, voice_set_params_ptr->payload_size, &mem_shared_memory_map);
                     if( ADSP_SUCCEEDED(result))
                     {
                        MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM Memory Map lsw(%lx) msw(%lx) virtual address(%lx)",voice_set_params_ptr->payload_address_lsw, voice_set_params_ptr->payload_address_msw, mem_shared_memory_map.unVirtAddr);
                        result = elite_mem_invalidate_cache_v2(&mem_shared_memory_map);

#if defined(__qdsp6__) && !defined(SIM)
                        int8_t *bufptr[4] = { (int8_t *) mem_shared_memory_map.unVirtAddr, NULL, NULL, NULL };
                        voice_log_buffer( bufptr,
                              VOICE_LOG_CHAN_VSM_SET_PARAM,
                              session_index,
                              qurt_elite_timer_get_time(),
                              VOICE_LOG_DATA_FORMAT_PCM_MONO,
                              1,   /* dummy value */
                              voice_set_params_ptr->payload_size,
                              NULL);
#endif
                     }
                  }
               }
               else
               {
                  result = ADSP_EFAILED;
               }
               if( ADSP_FAILED(result))
               {
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM Memory Map Err");
                  elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
                  break;
               }
               payload_address = mem_shared_memory_map.unVirtAddr;
            }
            else
            {  // for in-band
               payload_address = (uint32_t)voice_set_params_ptr + sizeof(voice_set_param_v2_t);
            }

            /* check if we have a place to hold this pending packet */
            rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                  apr_packet_ptr, &free_index);
            if( ADSP_SUCCEEDED(rc))
            {
               if( vsm_ptr->session[session_index]->rx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_param_cal_t);
                  //using rc_rx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_rx = elite_msg_create_msg( &msg_rx_t,
                        &size,
                        ELITE_CMD_SET_PARAM,
                        vsm_ptr->vsm_rx_resp_q_ptr,
                        VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_rx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create rx set_param EliteMsg, session_index(%x)",(int) session_index);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  else
                  {
                     elite_msg_param_cal_t* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_param_cal_t*) msg_rx_t.pPayload;
                     fadd_payload_ptr->unSize = voice_set_params_ptr->payload_size;
                     fadd_payload_ptr->pnParamData = (int32_t*)payload_address;
                     fadd_payload_ptr->unParamId = ELITEMSG_PARAM_ID_CAL;

                     rc_rx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
                     if (ADSP_FAILED(rc_rx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on rx, session_index(%x)",(int) session_index);
                        elite_msg_return_payload_buffer( &msg_rx_t);
                     }
                     else
                     {
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_rx = VSM_ACTION_SET_PARAM;
                     }

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
               }
               if( vsm_ptr->session[session_index]->tx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_param_cal_t);
                  //using rc_tx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_tx = elite_msg_create_msg( &msg_tx_t,
                        &size,
                        ELITE_CMD_SET_PARAM,
                        vsm_ptr->vsm_tx_resp_q_ptr,
                        VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_tx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create tx set_param EliteMsg, session_index(%x)",(int) session_index);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  else
                  {
                     elite_msg_param_cal_t* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_param_cal_t*) msg_tx_t.pPayload;
                     fadd_payload_ptr->unSize = voice_set_params_ptr->payload_size;
                     fadd_payload_ptr->pnParamData = (int32_t*)payload_address;
                     fadd_payload_ptr->unParamId = ELITEMSG_PARAM_ID_CAL;

                     rc_tx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
                     if (ADSP_FAILED(rc_tx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on tx, session_index(%x)",(int) session_index);
                        elite_msg_return_payload_buffer( &msg_tx_t);
                     }
                     else
                     {
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_tx = VSM_ACTION_SET_PARAM;
                     }

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
               }

               /* initialize results */
               /*update result as this is what is returned outside*/
               result = rc_tx | rc_rx;

               /* if both Rx and Tx failed, send result now, else we process async */
               if( !((vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) || (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx))) )
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                  voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            break;
         }
      case VOICE_CMD_SET_PARAM_V3:
         {
            elite_msg_any_t msg_rx_t;
            elite_msg_any_t msg_tx_t;
            uint32_t free_index, size;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            voice_cmd_set_param_v3_t* voice_set_params_ptr = (voice_cmd_set_param_v3_t*) elite_apr_if_get_payload_ptr(apr_packet_ptr);

#if defined(__qdsp6__) && !defined(SIM)
        //log the buffer
        voice_log_cal_data(voice_set_params_ptr->cal_handle, VOICE_LOG_CHAN_VSM_SET_PARAM, session_index);
#endif

            /* check if we have a place to hold this pending packet */
            rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                  apr_packet_ptr, &free_index);
            if( ADSP_SUCCEEDED(rc))
            {
               if( vsm_ptr->session[session_index]->rx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_custom_set_param_v3_type);
                  //using rc_rx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_rx = elite_msg_create_msg( &msg_rx_t,
                        &size,
                        ELITE_CUSTOM_MSG,
                        vsm_ptr->vsm_rx_resp_q_ptr,
                        VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_rx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create rx set_param EliteMsg, session_index(%x)",(int) session_index);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  else
                  {
                     elite_msg_custom_set_param_v3_type* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_rx_t.pPayload;
                     fadd_payload_ptr->sec_opcode = VDEC_SET_PARAM_V3;
                     fadd_payload_ptr->cal_handle = voice_set_params_ptr->cal_handle;

                     rc_rx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
                     if (ADSP_FAILED(rc_rx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on rx, session_index(%x)",(int) session_index);
                        elite_msg_return_payload_buffer( &msg_rx_t);
                     }
                     else
                     {
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_rx = VSM_ACTION_SET_PARAM;
                     }

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
               }
               if( vsm_ptr->session[session_index]->tx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_custom_set_param_v3_type);
                  //using rc_tx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_tx = elite_msg_create_msg( &msg_tx_t,
                        &size,
                        ELITE_CUSTOM_MSG,
                        vsm_ptr->vsm_tx_resp_q_ptr,
                        VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_tx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create tx set_param EliteMsg, session_index(%x)",(int) session_index);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  else
                  {
                     elite_msg_custom_set_param_v3_type* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_tx_t.pPayload;
                     fadd_payload_ptr->cal_handle = voice_set_params_ptr->cal_handle;
                     fadd_payload_ptr->sec_opcode = VENC_SET_PARAM_V3;

                     rc_tx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
                     if (ADSP_FAILED(rc_tx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on tx, session_index(%x)",(int) session_index);
                        elite_msg_return_payload_buffer( &msg_tx_t);
                     }
                     else
                     {
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_tx = VSM_ACTION_SET_PARAM;
                     }

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
               }

               /* initialize results */
               /*update result as this is what is returned outside*/
               result = rc_tx | rc_rx;

               /* if both Rx and Tx failed, send result now, else we process async */
               if( !((vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) || (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx))) )
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                  voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            break;
         }


      case VOICE_CMD_GET_PARAM_V2:
         {
            uint32_t size = 0;
            elite_msg_any_t msg_rx_t;
            elite_msg_any_t msg_tx_t;
            uint32_t free_index;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            async_struct_rxtx_t *async_struct_ptr;
            voice_get_param_v2_t* voice_get_params_ptr = (voice_get_param_v2_t*)
               elite_apr_if_get_payload_ptr(apr_packet_ptr);
            int32_t *get_param_ptr;
            elite_apr_packet_t * apr_in_band_packet_ptr;
            qurt_elite_memorymap_mapping_mode_t mapping_mode;
            uint32_t alignment_check;

            //Validate memory for out of band before doing any further processing
            if(NULL != voice_get_params_ptr->mem_map_handle)
            {
               result = qurt_elite_memorymap_get_mapping_mode(vsm_memory_map_client,
                     voice_get_params_ptr->mem_map_handle, &mapping_mode);
               if(ADSP_FAILED(result))
               {
                  rc = elite_apr_if_free(vsm_ptr->apr_handle, apr_packet_ptr);
                  return ADSP_EBADPARAM;
               }

               if(QURT_ELITE_MEMORYMAP_HEAP_ADDR_MAPPING == mapping_mode)
               {
                  alignment_check = 4;
               }
               else
               {
                  alignment_check = 32;
               }

               result = voice_cmn_check_align_size(voice_get_params_ptr->payload_address_lsw, voice_get_params_ptr->payload_address_msw, voice_get_params_ptr->param_max_size, alignment_check);
               if(ADSP_FAILED(result))
               {
                  rc = elite_apr_if_free(vsm_ptr->apr_handle, apr_packet_ptr);
                  return ADSP_EBADPARAM;
               }
            }

            /* check if we have a place to hold this pending packet */
            async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
            rc = voice_init_async_control_rx_tx( async_struct_ptr, apr_packet_ptr, &free_index);

            if( ADSP_SUCCEEDED(rc))
            {
               if( vsm_ptr->session[session_index]->rx || vsm_ptr->session[session_index]->tx )
               {
                  if ( NULL == voice_get_params_ptr->mem_map_handle )
                  {
                     // In-Band processing
                     // allocate response apr packet and use that for get param
                     int32_t rc;
                     rc = elite_apr_if_alloc_cmd_rsp(
                           vsm_ptr->apr_handle,
                           apr_packet_ptr->dst_addr,
                           apr_packet_ptr->dst_port,
                           apr_packet_ptr->src_addr,
                           apr_packet_ptr->src_port,
                           apr_packet_ptr->token,
                           VOICE_RSP_GET_PARAM_ACK,
                           voice_get_params_ptr->param_max_size + sizeof(voice_get_param_ack_t),
                           &apr_in_band_packet_ptr
                           );
                     if ( rc )
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet for GetParamAck in VSM 0x%8x ",(int)rc);
                        return ADSP_EFAILED;
                     }
                     // overwrite the get param apr packet with the response pkt in the async structure tracking
                     async_struct_ptr->async_status[free_index].apr_packet_ptr = apr_in_band_packet_ptr;
                     get_param_ptr = (int32_t*)elite_apr_if_get_payload_ptr(apr_in_band_packet_ptr);
                     get_param_ptr = (int32_t*)((int8_t*)get_param_ptr + sizeof(voice_get_param_ack_t));
                     memscpy (get_param_ptr, voice_get_params_ptr->param_max_size, (int32_t *)(&voice_get_params_ptr->module_id), (sizeof(voice_param_data_t)));
                     // free the apr getparam pkt and use the apr getparam_ack pkt
                     // TODO: Probably can't free packet here. need to check if this is the right behavior
                     rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);
                  }
                  else
                  {
                     // out of band processing
                     elite_mem_shared_memory_map_t memmap = {0};
                     memmap.unMemMapClient = vsm_memory_map_client;
                     memmap.unMemMapHandle = voice_get_params_ptr->mem_map_handle;
                     result = elite_mem_map_get_shm_attrib(voice_get_params_ptr->payload_address_lsw,
                           voice_get_params_ptr->payload_address_msw,
                           voice_get_params_ptr->param_max_size,
                           &memmap);
                     if( ADSP_FAILED( result ))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Failed to get virtual address with result %d!", result);
                        return result;
                     }
                     get_param_ptr = (int32_t*)memmap.unVirtAddr;
                     memscpy (get_param_ptr, voice_get_params_ptr->param_max_size, (int32_t *)(&voice_get_params_ptr->module_id), (sizeof(voice_param_data_t)));
                  }

                  // size of the FADD message
                  size = sizeof(elite_msg_param_cal_t);
                  //*************** Compose FADD Message *****************
                  if(  vsm_ptr->session[session_index]->rx )
                  {
                     //using rc_rx for result as this is used for immediate termination of command if both Tx and Rx failed
                     rc_rx = elite_msg_create_msg( &msg_rx_t,
                           &size,
                           ELITE_CMD_GET_PARAM,
                           vsm_ptr->vsm_rx_resp_q_ptr,
                           VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                           ADSP_EOK);
                     if( ADSP_FAILED( rc_rx ))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create get_param rx EliteMsg, session_index(%x)",(int) session_index);
                        //state is VSM_ACTION_NONE (0). So no need to update
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                     else
                     {
                        elite_msg_param_cal_t* fadd_payload_ptr;
                        fadd_payload_ptr = (elite_msg_param_cal_t*) msg_rx_t.pPayload;
                        fadd_payload_ptr->unParamId = ELITEMSG_PARAM_ID_CAL;
                        fadd_payload_ptr->pnParamData = (int32_t*)get_param_ptr;

                        // MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch GetParams to Rx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                        rc_rx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
                        if (ADSP_FAILED(rc_rx))
                        {
                           MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push get params on rx, session_index(%x)",(int) session_index);
                           elite_msg_return_payload_buffer( &msg_rx_t);
                        }
                        else
                        {
                           vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_rx = VSM_ACTION_GET_PARAM;
                        }

                        /* initialize results */
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                  }
                  if(  vsm_ptr->session[session_index]->tx )
                  {
                     //using rc_tx for result as this is used for immediate termination of command if both Tx and Rx failed
                     rc_tx = elite_msg_create_msg( &msg_tx_t,
                           &size,
                           ELITE_CMD_GET_PARAM,
                           vsm_ptr->vsm_tx_resp_q_ptr,
                           VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                           ADSP_EOK);
                     if( ADSP_FAILED( rc_tx ))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create get_param tx EliteMsg, session_index(%x)",(int) session_index);
                        //state is VSM_ACTION_NONE (0). So no need to update
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                     else
                     {
                        elite_msg_param_cal_t* fadd_payload_ptr;
                        fadd_payload_ptr = (elite_msg_param_cal_t*) msg_tx_t.pPayload;
                        fadd_payload_ptr->unParamId = ELITEMSG_PARAM_ID_CAL;
                        fadd_payload_ptr->pnParamData = (int32_t*)get_param_ptr;

                        // MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch GetParams to Tx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                        rc_tx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
                        if (ADSP_FAILED(rc_tx))
                        {
                           MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push get params on tx, session_index(%x)",(int) session_index);
                           elite_msg_return_payload_buffer( &msg_tx_t);
                        }
                        else
                        {
                           vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_tx = VSM_ACTION_GET_PARAM;
                        }

                        /* initialize results */
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                  }
               }
               /*update result as this is what is returned outside*/
               result = rc_tx | rc_rx;


               if( !( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)) ))
               {
                  /* Immediate error case: free the APR packet pointer in use -- could be original or ACK pointer depending on in band or out of band */
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, async_struct_ptr->async_status[free_index].apr_packet_ptr, APR_EFAILED);
                  voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            break;
         }
      case VSM_CMD_GET_KPPS:
      case VSM_CMD_GET_DELAY:
         {
            uint32_t size = 0;
            elite_msg_any_t msg_rx_t;
            elite_msg_any_t msg_tx_t;
            uint32_t free_index;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            async_struct_rxtx_t *async_struct_ptr;
            void*  apr_payload_ptr =NULL;
            elite_apr_packet_t * apr_in_band_packet_ptr=NULL;
            uint32_t opcode = elite_apr_if_get_opcode( apr_packet_ptr);
            uint32_t resp_opcode, resp_payload_size, action;
            if(VSM_CMD_GET_KPPS == opcode)
            {
               resp_opcode = VSM_RSP_GET_KPPS_ACK;
               resp_payload_size = sizeof(vsm_get_kpps_ack_t);
               size = sizeof(elite_msg_custom_kpps_type);
               action = VSM_ACTION_GET_KPPS;
            }
            else
            {
               resp_opcode = VSM_RSP_GET_DELAY_ACK;
               resp_payload_size = sizeof(vsm_get_delay_ack_t);
               size = sizeof(elite_msg_custom_delay_type);
               action = VSM_ACTION_GET_DELAY;
            }

            /* check if we have a place to hold this pending packet */
            async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
            rc = voice_init_async_control_rx_tx( async_struct_ptr, apr_packet_ptr, &free_index);

            if( ADSP_SUCCEEDED(rc))
            {
               if( vsm_ptr->session[session_index]->rx || vsm_ptr->session[session_index]->tx )
               {
                  {
                     // allocate response apr packet and use that for get param
                     int32_t rc;
                     rc = elite_apr_if_alloc_cmd_rsp(
                           vsm_ptr->apr_handle,
                           apr_packet_ptr->dst_addr,
                           apr_packet_ptr->dst_port,
                           apr_packet_ptr->src_addr,
                           apr_packet_ptr->src_port,
                           apr_packet_ptr->token,
                           resp_opcode,
                           resp_payload_size,
                           &apr_in_band_packet_ptr
                           );
                     if ( rc )
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet for ack in VSM 0x%8x ",(int)rc);
                        return ADSP_EFAILED;
                     }
                     // overwrite the get param apr packet with the response pkt in the async structure tracking
                     async_struct_ptr->async_status[free_index].apr_packet_ptr = apr_in_band_packet_ptr;
                     apr_payload_ptr = elite_apr_if_get_payload_ptr(apr_in_band_packet_ptr);
                     // free the apr msg pkt and use the apr ack pkt
                     rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);
                  }

                  //*************** Compose FADD Message *****************
                  if(  vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = elite_msg_create_msg( &msg_rx_t,
                           &size,
                           ELITE_CUSTOM_MSG,
                           vsm_ptr->vsm_rx_resp_q_ptr,
                           VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                           ADSP_EOK);
                     if( ADSP_FAILED( rc_rx ))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create rx EliteMsg, session_index(%x)",(int) session_index);
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;

                     }
                     else
                     {
                        if(VSM_CMD_GET_KPPS == opcode)
                        {
                           elite_msg_custom_kpps_type* fadd_payload_ptr;
                           fadd_payload_ptr = (elite_msg_custom_kpps_type*) msg_rx_t.pPayload;
                           fadd_payload_ptr->pnKpps = apr_payload_ptr;
                           fadd_payload_ptr->sec_opcode = VDEC_GET_KPPS_CMD;
                        }
                        else
                        {
                           elite_msg_custom_delay_type* fadd_payload_ptr;
                           fadd_payload_ptr = (elite_msg_custom_delay_type*) msg_rx_t.pPayload;
                           fadd_payload_ptr->delay_ptr = apr_payload_ptr;
                           fadd_payload_ptr->sec_opcode = VDEC_GET_DELAY_CMD;
                        }

                        rc_rx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
                        if (ADSP_FAILED(rc_rx))
                        {
                           MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push get kpps/delay on rx, session_index(%x)",(int) session_index);
                           elite_msg_return_payload_buffer( &msg_rx_t);
                        }
                        else
                        {
                           vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_rx = action;
                        }

                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                  }
                  if(  vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = elite_msg_create_msg( &msg_tx_t,
                           &size,
                           ELITE_CUSTOM_MSG,
                           vsm_ptr->vsm_tx_resp_q_ptr,
                           VOICE_ASYNC_STUFF_TOKEN(session_index, free_index),
                           ADSP_EOK);
                     if( ADSP_FAILED( rc_tx ))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create tx get_kpps EliteMsg, session_index(%x)",(int) session_index);
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                     else
                     {
                        if(VSM_CMD_GET_KPPS == opcode)
                        {
                           elite_msg_custom_kpps_type* fadd_payload_ptr;
                           fadd_payload_ptr = (elite_msg_custom_kpps_type*) msg_tx_t.pPayload;
                           fadd_payload_ptr->pnKpps = apr_payload_ptr;
                           fadd_payload_ptr->sec_opcode = VENC_GET_KPPS_CMD;
                        }
                        else
                        {
                           elite_msg_custom_delay_type* fadd_payload_ptr;
                           fadd_payload_ptr = (elite_msg_custom_delay_type*) msg_tx_t.pPayload;
                           fadd_payload_ptr->delay_ptr = apr_payload_ptr;
                           fadd_payload_ptr->sec_opcode = VENC_GET_DELAY_CMD;
                        }

                        rc_tx = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
                        if (ADSP_FAILED(rc_tx))
                        {
                           MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push get kpps/delay on tx, session_index(%x)",(int) session_index);
                           elite_msg_return_payload_buffer( &msg_tx_t);
                        }
                        else
                        {
                           vsm_ptr->session[session_index]->async_struct.async_status[free_index].state_tx = action;
                        }

                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                  }
               }

               if( !( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)) ))
               {
                  /* Immediate error case: free the APR packet pointer in use -- could be original or ACK pointer depending on in band or out of band */
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, async_struct_ptr->async_status[free_index].apr_packet_ptr, APR_EFAILED);
                  voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            break;
         }

      case VSM_CMD_SET_TIMING_PARAMS:
         {

            vsm_set_timing_params_t* timing_cmd_ptr = (vsm_set_timing_params_t*)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS, mode(%d), enc_offset(%d), \
                  decreq_offset(%d), dec_offset(%d),decpp_offset(%d),vptx_deli_offset(%d),session_state(0x%x)",
                  timing_cmd_ptr->mode,timing_cmd_ptr->enc_offset,timing_cmd_ptr->dec_req_offset,
                  timing_cmd_ptr->dec_offset,timing_cmd_ptr->decpp_offset,timing_cmd_ptr->vptx_delivery_offset,
                  (int)vsm_ptr->session[session_index]->state );

            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change VFR in RUN state");

               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* store vfr_mode and enc_offset in VSM to send it to VPM for in call music delivery case
                  to get the drift info in the case of external audio reference port for sample slip/stuff correction
                */
               vsm_ptr->session[session_index]->vfr_mode = timing_cmd_ptr->mode;
               vsm_ptr->session[session_index]->enc_offset = timing_cmd_ptr->enc_offset;
               vsm_ptr->session[session_index]->vptx_delivery_offset = timing_cmd_ptr->vptx_delivery_offset;
               vsm_ptr->session[session_index]->timing_cmd_version = VOICE_TIMING_VERSION_2;

               /* kick off Enc/Dec MT config. */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*)timing_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VENC_SET_TIMINGV2_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*) timing_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VDEC_SET_TIMINGV2_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }
      case VSM_CMD_SET_TIMING_PARAMS_V2:
         {

            vsm_set_timing_params_v2_t* timing_cmd_ptr = (vsm_set_timing_params_v2_t*)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_8(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_TIMING_PARAMS_V2, mode(%d), VSID(0x%lx), \
                  enc_offset(%d), decreq_offset(%d), dec_offset(%d),decpp_offset(%d),vptx_delivery_offset(%d),session_index(%x)",
                  timing_cmd_ptr->mode,timing_cmd_ptr->vsid,timing_cmd_ptr->enc_offset,timing_cmd_ptr->dec_req_offset,
                  timing_cmd_ptr->dec_offset,timing_cmd_ptr->decpp_offset,timing_cmd_ptr->vptx_delivery_offset,session_index);

            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: cannot change timing in RUN state, session_index(%x)",session_index);

               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: cmd status is pending cannot process VSM_CMD_SET_TIMING_PARAMS_V2, \
                   return APR_EBUSY, session_index(%x)",session_index);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* store vfr_mode and enc_offset in VSM to send it to VPM for in call music delivery case
                  to get the drift info in the case of external audio reference port for sample slip/stuff correction
                */
               vsm_ptr->session[session_index]->vfr_mode = timing_cmd_ptr->mode;
               vsm_ptr->session[session_index]->enc_offset = timing_cmd_ptr->enc_offset;
               vsm_ptr->session[session_index]->vsid = timing_cmd_ptr->vsid;
               vsm_ptr->session[session_index]->vptx_delivery_offset = timing_cmd_ptr->vptx_delivery_offset;
               vsm_ptr->session[session_index]->timing_cmd_version = VOICE_TIMING_VERSION_3;

               /* kick off Enc/Dec timing config */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*)timing_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VENC_SET_TIMINGV3_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*) timing_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VDEC_SET_TIMINGV3_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }
      case VOICE_CMD_SET_TIMING_PARAMS:
         {

            voice_set_timing_params_t  *vfr_mode_cmd_Ptr = (voice_set_timing_params_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_SET_TIMING_PARAMS, VFR(0x%x), session_state(0x%x)",
                  (int) vfr_mode_cmd_Ptr->mode,(int)vsm_ptr->session[session_index]->state );


            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change VFR in RUN state");

               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY );
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* store vfr_mode and enc_offset in VSM to send it to VPM for in call music delivery case
                  to get the drift info in the case of external audio reference port for sample slip/stuff correction
                */
               vsm_ptr->session[session_index]->vfr_mode = vfr_mode_cmd_Ptr->mode;
               vsm_ptr->session[session_index]->enc_offset = vfr_mode_cmd_Ptr->enc_offset;
               vsm_ptr->session[session_index]->timing_cmd_version = VOICE_TIMING_VERSION_1;

               /* kick off Enc/Dec MT config. */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*) vfr_mode_cmd_Ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VENC_SET_TIMING_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_vfr_mode( vsm_ptr, session_index,
                           (int32_t*) vfr_mode_cmd_Ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VDEC_SET_TIMING_CMD);

                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }
      case VOICE_CMD_SET_SOFT_MUTE_V2:
         {
            voice_set_soft_mute_v2_t    *set_mute_cmd_ptr = (voice_set_soft_mute_v2_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_SET_SOFT_MUTE_V2, direction(0x%x), mute_cmd(%d), ramp_duration(%d), session_state(0x%x)",
                  (int) set_mute_cmd_ptr->direction,
                  (int) set_mute_cmd_ptr->mute,
                  (int) set_mute_cmd_ptr->ramp_duration,
                  (int) vsm_ptr->session[session_index]->state );

            //validate params.  Can do SET_MUTE in run state and parallel to other commands (TODO: should not process this or any other commands with destroy pending)
            if( set_mute_cmd_ptr->mute      > VOICE_MUTE               ||
                  set_mute_cmd_ptr->direction > VOICE_SET_MUTE_TX_AND_RX ||
                  set_mute_cmd_ptr->ramp_duration > MAX_MUTE_RAMP_DURATION_MSEC
              )
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               /* kick off Enc/Dec MT config. */
               if( ADSP_SUCCEEDED(rc))
               {
                  if( set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_ONLY ||
                        set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_AND_RX)

                  {
                     if( vsm_ptr->session[session_index]->tx )
                     {
                        rc_tx = vsm_send_tx_soft_mute( vsm_ptr, session_index,
                              set_mute_cmd_ptr,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index),VOICE_CMD_SET_SOFT_MUTE_V2 );
                     }
                     else
                     {
                        rc_tx = ADSP_EBADPARAM;
                     }
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  if( set_mute_cmd_ptr->direction == VOICE_SET_MUTE_RX_ONLY ||
                        set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_AND_RX)
                  {

                     if( vsm_ptr->session[session_index]->rx )
                     {
                        rc_rx = vsm_send_rx_soft_mute( vsm_ptr, session_index,
                              set_mute_cmd_ptr,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index), VOICE_CMD_SET_SOFT_MUTE_V2 );
                     }
                     else
                     {
                        rc_rx = ADSP_EBADPARAM;
                     }
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( ((set_mute_cmd_ptr->direction == VOICE_SET_MUTE_RX_ONLY || set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_AND_RX) && ADSP_SUCCEEDED(rc_rx)) ||
                        ((set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_ONLY || set_mute_cmd_ptr->direction == VOICE_SET_MUTE_TX_AND_RX) && ADSP_SUCCEEDED(rc_tx)))
                  {
                     //me->session[nSessionIndex]->pending = TRUE;  /* don't need to serialize MUTE, can handle other commands */
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }

      case VOICE_CMD_SHARED_MEM_MAP_REGIONS:
         {
            result = voice_shared_memory_map_regions_process(vsm_memory_map_client, vsm_ptr->apr_handle, apr_packet_ptr);
            break;
         }
      case VOICE_CMD_SHARED_MEM_UNMAP_REGIONS:
         {
            result = voice_shared_memory_un_map_regions_process(vsm_memory_map_client, vsm_ptr->apr_handle, apr_packet_ptr);
            break;
         }
      case VSM_CMD_START_RECORD_V2:
         {
            vsm_start_record_v2_t   *start_record2_cmd_ptr=NULL;
            uint16_t sampling_rate;

            start_record2_cmd_ptr = (vsm_start_record_v2_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);
            vsm_ptr->session[session_index]->rx_tap_point = start_record2_cmd_ptr->rx_tap_point;
            vsm_ptr->session[session_index]->tx_tap_point = start_record2_cmd_ptr->tx_tap_point;
            vsm_ptr->session[session_index]->record_mode = start_record2_cmd_ptr->record_mode;
            if(AFE_PORT_ID_INVALID == start_record2_cmd_ptr->aud_ref_port)
            {
               //ignore the record_mode in case of recording through default ports.
               vsm_ptr->session[session_index]->record_mode = 0;

            }
            MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_CMD_START_RECORD_V2,on session_index(0x%hx): rx tap (0x%x), tx tap (0x%x)\
                  record mode (0x%lx), aud_ref_port (0x%x)",
                  session_index,(int)start_record2_cmd_ptr->rx_tap_point,(int)start_record2_cmd_ptr->tx_tap_point,
                  start_record2_cmd_ptr->record_mode,start_record2_cmd_ptr->aud_ref_port);

            if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               if(TRUE == vsm_ptr->session[session_index]->record_start_on)
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, ADSP_EALREADY);
                  MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_START_RECORD command already in process/started for session (0x%x) ",session_index);
                  break;
               }
               vsm_ptr->session[session_index]->record_start_on = TRUE;

               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);

               //Assume here BeAMR is disabled for AMR-NB. sampling rate will be 8KHz. set_media_type will send proper sampling rate when BeAMR is enabled.
               sampling_rate = voice_get_sampling_rate ( vsm_ptr->session[session_index]->media_type_format, 0, vsm_ptr->session[session_index]->tx_stream_pp_samp_rate);
               rc_tx = APR_EFAILED;
               rc_rx = APR_EFAILED;
               if( ADSP_SUCCEEDED(rc))
               {
                  if ( VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->tx_tap_point)
                  {
                     if( vsm_ptr->session[session_index]->tx )
                     {
                        uint16_t afe_port;
                        if(AFE_PORT_ID_INVALID == start_record2_cmd_ptr->aud_ref_port)
                        {
                           afe_port =  AFE_PORT_ID_VOICE_RECORD_TX;
                           MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::aud_ref_port is invalid,defaulting port to Tx pseudo port for the session (0x%x)",
                                 session_index);
                        }
                        else
                        {
                           afe_port = start_record2_cmd_ptr->aud_ref_port;
                        }
                        vsm_ptr->session[session_index]->aud_ref_port_tx = afe_port;

                        rc_tx = vsm_connect_afe ( vsm_ptr,session_index,sampling_rate, TRUE, afe_port,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                  }

                  if ( VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->rx_tap_point)
                  {
                     if( vsm_ptr->session[session_index]->rx )
                     {
                        uint16_t afe_port;
                        if(AFE_PORT_ID_INVALID == start_record2_cmd_ptr->aud_ref_port)
                        {
                           afe_port =  AFE_PORT_ID_VOICE_RECORD_RX;
                           MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::aud_ref_port is invalid,defaulting port to Rx pseudo port for the session (0x%x)",
                                 session_index);
                        }
                        else
                        {
                           afe_port = start_record2_cmd_ptr->aud_ref_port;
                        }
                        vsm_ptr->session[session_index]->aud_ref_port_rx = afe_port;
                        rc_rx = vsm_connect_afe ( vsm_ptr,session_index,sampling_rate , FALSE, afe_port,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }

      case VSM_CMD_STOP_RECORD:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_CMD_STOP_RECORD,on session_index(0x%x)",
                  session_index );

            vsm_ptr->session[session_index]->record_start_on = FALSE;
            if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               rc_tx = APR_EFAILED;
               rc_rx = APR_EFAILED;

               if( ADSP_SUCCEEDED(rc))
               {
                  if ( VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->tx_tap_point)
                  {
                     if( vsm_ptr->session[session_index]->tx )
                     {
                        rc_tx = vsm_stop_record_tx( vsm_ptr, session_index,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                     }
                  }
                  if ( VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->rx_tap_point)
                  {
                     if( vsm_ptr->session[session_index]->rx )
                     {
                        rc_rx = vsm_stop_record_rx( vsm_ptr, session_index,
                              &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));

                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                  }

                  /* check if any action attempt succeeded...if so need to handle an ack */
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }
            }
            break;
         }

      case VSM_CMD_START_PLAYBACK_V2:
         {
            /* Check if session already running */
            if(vsm_ptr->session[session_index]->pb_session != 0)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD::Received playback start on existing playback session (0x%x)", (int)session_index);
               result = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY );
               break;
            }
            /* Send create session command to vpm, use pointer to the original apr packet as token */
            elite_apr_packet_t* create_session_pkt;

            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::create VPM session for music delivery session (0x%x)", (int)session_index);
            result = elite_apr_if_alloc_cmd( vsm_ptr->apr_handle,
                  vsm_ptr->apr_addr,
                  handle,
                  voiceProcMgr->apr_addr,
                  0,
                  (uint32_t)apr_packet_ptr,
                  VPM_CMD_CREATE_SESSION_V2,
                  sizeof(vpm_create_session_v2_t),
                  &create_session_pkt);

            vpm_create_session_v2_t* session_payload = (vpm_create_session_v2_t*)elite_apr_if_get_payload_ptr(create_session_pkt);
            session_payload->rx_topology_id = VPM_RX_DEFAULT;
            session_payload->tx_topology_id = VPM_TX_NONE;
            /* The next 2 parameters are not used at all, but are set for sake of completeness */
            session_payload->ec_mode = VPM_TX_INT_MIX_MODE;
            session_payload->audio_ref_port = AFE_PORT_ID_INVALID;
            /* get the sampling rate of tx based on the media type format*/
            session_payload->tx_sampling_rate = voice_get_sampling_rate( vsm_ptr->session[session_index]->media_type_format,0/*BeAMR flag for encoder*/,vsm_ptr->session[session_index]->tx_stream_pp_samp_rate);
            //Rx sampling rate wont be used as VPRx is not created.
            session_payload->rx_sampling_rate = session_payload->tx_sampling_rate; // rx sampling rate same as tx.
            session_payload->tx_port = AFE_PORT_ID_VOICE_PLAYBACK_TX; //default port in case afe port is set to invalid
            session_payload->rx_port = AFE_PORT_ID_INVALID;
            vsm_start_playback_t   *start_playback_cmd_ptr;
            start_playback_cmd_ptr = (vsm_start_playback_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);
            /* check for the valid port id in the command payload. if the port id is valid then pass the port
               id to VPM. if the port id is invalid, then audio samples are read from pseudo port*/
            if(AFE_PORT_ID_INVALID != start_playback_cmd_ptr->aud_ref_port)
            {
               session_payload->tx_port = start_playback_cmd_ptr->aud_ref_port;
            }
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::Music samples are from Tx pseudo/real port id (0x%x) session (0x%x)", session_payload->tx_port,(int)session_index);
            result = elite_apr_if_async_send(vsm_ptr->apr_handle, create_session_pkt);

            if(ADSP_FAILED(result))
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to send vpm create!");
               result = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, result );
            }
            else
            {
               /* Mark as pending till playback session handling can be completed */
               vsm_ptr->session[session_index]->pending = TRUE;
            }

            break;
         }
      case VSM_CMD_STOP_PLAYBACK:
         {
            /* Check if session exists */
            if(vsm_ptr->session[session_index]->pb_session == 0)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Received playback stop on non-existent playback session %#x", (int)session_index);
               result = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EHANDLE );
               break;
            }
            elite_apr_packet_t *detach_packet;
            result = elite_apr_if_alloc_cmd(vsm_ptr->apr_handle,
                  vsm_ptr->apr_addr,
                  handle,
                  vsm_ptr->apr_addr,
                  handle,
                  (uint32_t)apr_packet_ptr,
                  VSM_CMD_DETACH_VOICE_PROC,
                  sizeof(vsm_routing_config_t),
                  &detach_packet);
            vsm_routing_config_t* pb_detach_payload = (vsm_routing_config_t*)elite_apr_if_get_payload_ptr(detach_packet);
            pb_detach_payload->tx_device_count = 1;
            pb_detach_payload->rx_device_count = 0;
            pb_detach_payload->handles[0] = vsm_ptr->session[session_index]->pb_session;
            result = elite_apr_if_async_send(vsm_ptr->apr_handle, detach_packet);
            if(ADSP_FAILED(result))
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to send vsm detach!");
               result = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, result );
            }
            else
            {
               /* Mark as pending till playback session handling can be completed */
               vsm_ptr->session[session_index]->pending = TRUE;
            }
            break;
         }
         /* Handle responses coming from APR commands sent by VSM. These commands can be sent to itself or to VPM.
            When sending these commands, the original APR packet that triggered the sequence is stored as the token.
            The opcode can be retrieved from the response, and this controls the state machine. For example, for
            VSM_CMD_START_PLAYBACK_V2, sequence is vpm_create->vsm_attach->vpm_start. For stop_playback, it is vsm_detach
            ->vpm_stop->vpm_destroy */
      case APRV2_IBASIC_RSP_RESULT:
         {
            /* Treatment depends on opcode */
            uint32_t token = elite_apr_if_get_client_token(apr_packet_ptr);
            aprv2_ibasic_rsp_result_t* apr_result = (aprv2_ibasic_rsp_result_t*)elite_apr_if_get_payload_ptr(apr_packet_ptr);
            uint32_t opcode = apr_result->opcode;
            uint32_t prev_result = apr_result->status;
            if(ADSP_FAILED(prev_result))
            {
               /* In failure case, return ack for command */
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed while handling opcode %#lx with result %ld!!!", opcode, prev_result);
               elite_apr_packet_t* pb_packet_ptr = (elite_apr_packet_t*)token;
               elite_apr_if_end_cmd(vsm_ptr->apr_handle, pb_packet_ptr, prev_result);
               vsm_ptr->session[session_index]->pending = FALSE;
               vsm_ptr->session[session_index]->pb_session = NULL;
               break;
            }
            switch(opcode)
            {
               case VPM_CMD_CREATE_SESSION_V2:
                  {
                     /* Step 2 of start playback sequence */
                     /* Next step is to find session number of created vpm session */
                     vsm_ptr->session[session_index]->pb_session = elite_apr_if_get_src_port(apr_packet_ptr);
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VSM_CMD_ATTACH_VOICE_PROC session %x", vsm_ptr->session[session_index]->pb_session);
                     /* Attach the vsm session to above vpm session */
                     elite_apr_packet_t *attach_packet;
                     result = elite_apr_if_alloc_cmd(vsm_ptr->apr_handle,
                           vsm_ptr->apr_addr,
                           handle,
                           vsm_ptr->apr_addr,
                           handle,
                           token,
                           VSM_CMD_ATTACH_VOICE_PROC,
                           sizeof(vsm_routing_config_t),
                           &attach_packet);
                     vsm_routing_config_t* pb_attach_payload = (vsm_routing_config_t*)elite_apr_if_get_payload_ptr(attach_packet);
                     pb_attach_payload->tx_device_count = 1;
                     pb_attach_payload->rx_device_count = 0;
                     pb_attach_payload->handles[0] = vsm_ptr->session[session_index]->pb_session;
                     result = elite_apr_if_async_send(vsm_ptr->apr_handle, attach_packet);
                     break;
                  }
               case VSM_CMD_ATTACH_VOICE_PROC:
                  {
                     void* payload_ptr;
                     uint32_t payload_size,opcode;
                     voice_set_timing_params_t set_vfr1;
                     vpm_set_timing_params_t set_vfr2;
                     vpm_set_timing_params_v2_t set_vfr3;
                     uint16_t session = vsm_ptr->session[session_index]->pb_session, version=vsm_ptr->session[session_index]->timing_cmd_version;
                     switch (version)
                     {
                        case VOICE_TIMING_VERSION_1:
                        {
                           opcode = VOICE_CMD_SET_TIMING_PARAMS;
                           payload_size = sizeof(voice_set_timing_params_t);
                           set_vfr1.mode = vsm_ptr->session[session_index]->vfr_mode;
                           set_vfr1.enc_offset = vsm_ptr->session[session_index]->enc_offset;
                           /* Don't care values */
                           set_vfr1.dec_offset = 0;
                           set_vfr1.dec_req_offset = 0;
                           payload_ptr = (void *) &set_vfr1;
                           MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VOICE_CMD_SET_TIMING_PARAMS vfr_mode (0x%x), enc_offset(%d), session (0x%x)", (int)set_vfr1.mode,set_vfr1.enc_offset,session);

                           break;
                        }
                        case VOICE_TIMING_VERSION_2:
                        {
                           opcode = VPM_CMD_SET_TIMING_PARAMS;
                           payload_size = sizeof(vpm_set_timing_params_t);
                           // Hard coding 5 ms processing time for Vptx NONE topology to be very safe
                           int32_t start_offset = vsm_ptr->session[session_index]->vptx_delivery_offset - 5000;
                           start_offset = ((0 > start_offset) ? (20000 + start_offset) : start_offset); // Mod20 operation for 20ms timeline

                           set_vfr2.mode = vsm_ptr->session[session_index]->vfr_mode;
                           set_vfr2.vptx_delivery_offset = vsm_ptr->session[session_index]->vptx_delivery_offset;
                           set_vfr2.vptx_start_offset = (uint16_t) start_offset;
                           /* Don't care values */
                           set_vfr2.vprx_delivery_offset = 0;
                           payload_ptr = (void *) &set_vfr2;
                           MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VPM_CMD_SET_TIMING_PARAMS vfr_mode (0x%x), vptx_deli_offset(%d), vptx_start_offset(%d),session (0x%x)", set_vfr2.mode,set_vfr2.vptx_delivery_offset,set_vfr2.vptx_start_offset,session);
                           break;
                        }
                        case VOICE_TIMING_VERSION_3:
                        {
                           opcode = VPM_CMD_SET_TIMING_PARAMS_V2;
                           payload_size = sizeof(vpm_set_timing_params_v2_t);
                           // Hard coding 5 ms processing time for Vptx NONE topology to be very safe
                           int32_t start_offset = vsm_ptr->session[session_index]->vptx_delivery_offset - 5000;
                           start_offset = ((0 > start_offset) ? (20000 + start_offset) : start_offset); // Mod20 operation for 20ms timeline

                           set_vfr3.mode = vsm_ptr->session[session_index]->vfr_mode;
                           set_vfr3.vptx_delivery_offset = vsm_ptr->session[session_index]->vptx_delivery_offset;
                           set_vfr3.vptx_start_offset = (uint16_t) start_offset;
                           set_vfr3.vsid = vsm_ptr->session[session_index]->vsid;
                           /* Don't care values */
                           set_vfr3.vprx_delivery_offset = 0;
                           payload_ptr = (void *) &set_vfr3;
                           MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VPM_CMD_SET_TIMING_PARAMS_V2 vfr_mode (0x%x),vsid(%lx),vptx_deli_offset(%d), vptx_start_offset(%d),session (0x%x)", set_vfr3.mode,set_vfr3.vsid,set_vfr3.vptx_delivery_offset,set_vfr3.vptx_start_offset,session);
                           break;
                        }
                        default:
                        {
                           MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD:: Error - Unknown timing cmd version(%d),session(0x%x)",version,session);
                           result = ADSP_EUNSUPPORTED;
                        }
                     }

                     if (ADSP_SUCCEEDED(result))
                     {
                        result = elite_apr_if_alloc_send_cmd(vsm_ptr->apr_handle,
                              vsm_ptr->apr_addr,
                              handle,
                              voiceProcMgr->apr_addr,
                              session,
                              token,
                              opcode,
                              payload_ptr,
                              payload_size);
                        if(ADSP_FAILED(result))
                        {
                           MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD::Failed to send vfr mode and timing params to VPM!");
                        }
                     }

                     break;
                  }
                  // TODO: Need to update the state machine here to use new timing command here,
                  // once the old one has been deprecated. For now, the old sequence will do.
               case VOICE_CMD_SET_TIMING_PARAMS:
               case VPM_CMD_SET_TIMING_PARAMS:
               case VPM_CMD_SET_TIMING_PARAMS_V2:
                  {
                     /* Step 3 of start playback sequence */
                     /* This is when you start the voice proc session */
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VPM_CMD_START_SESSION session (0x%x)", vsm_ptr->session[session_index]->pb_session);
                     result = elite_apr_if_alloc_send_cmd( vsm_ptr->apr_handle,
                           vsm_ptr->apr_addr,
                           handle,
                           voiceProcMgr->apr_addr,
                           vsm_ptr->session[session_index]->pb_session,
                           token,
                           VPM_CMD_START_SESSION,
                           NULL,
                           0);
                     if(ADSP_FAILED(result))
                     {
                        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD::Failed to send vpm run!");
                     }
                     break;
                  }
               case VSM_CMD_DETACH_VOICE_PROC:
                  {
                     /* Step 2 of stop playback sequence */
                     /* Stop the vpm session */
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VPM_CMD_STOP_SESSION session (0x%x)", vsm_ptr->session[session_index]->pb_session);
                     result = elite_apr_if_alloc_send_cmd( vsm_ptr->apr_handle,
                           vsm_ptr->apr_addr,
                           handle,
                           voiceProcMgr->apr_addr,
                           vsm_ptr->session[session_index]->pb_session,
                           token,
                           VPM_CMD_STOP_SESSION,
                           NULL,
                           0);
                     if(ADSP_FAILED(result))
                     {
                        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD::Failed to send vpm stop!");
                     }
                     break;
                  }
               case VPM_CMD_START_SESSION:
                  {
                     /* Step 4 of start playback sequence */
                     /* We're done with the start playback command, now send out response to original playback apr packet */
                     elite_apr_packet_t* pb_packet_ptr = (elite_apr_packet_t*)token;
                     elite_apr_if_end_cmd(vsm_ptr->apr_handle, pb_packet_ptr, APR_EOK);
                     vsm_ptr->session[session_index]->pending = FALSE;
                     break;
                  }
               case VPM_CMD_STOP_SESSION:
                  {
                     /* Step 3 of stop playback sequence */
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICMD::issuing VPM_CMD_DESTROY_SESSION session %x", vsm_ptr->session[session_index]->pb_session);
                     /* The last step of stop playback is to destroy the vp session */
                     result = elite_apr_if_alloc_send_cmd( vsm_ptr->apr_handle,
                           vsm_ptr->apr_addr,
                           handle,
                           voiceProcMgr->apr_addr,
                           vsm_ptr->session[session_index]->pb_session,
                           token,
                           VPM_CMD_DESTROY_SESSION,
                           NULL,
                           0);
                     if(ADSP_FAILED(result))
                     {
                        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ICMD::Failed to send vpm destroy!");
                     }
                     break;
                  }
               case VPM_CMD_DESTROY_SESSION:
                  {
                     /* Step 4 of stop playback sequence */
                     /* Now an ack can be sent for stop_playback */
                     elite_apr_packet_t* pb_packet_ptr = (elite_apr_packet_t*)token;
                     elite_apr_if_end_cmd(vsm_ptr->apr_handle, pb_packet_ptr, APR_EOK);
                     vsm_ptr->session[session_index]->pending = FALSE;
                     vsm_ptr->session[session_index]->pb_session = 0;
                     break;
                  }
               default:
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Something bad happened, should not have ended up here %#lx", opcode);
                  }
            }
            elite_apr_if_free(vsm_ptr->apr_handle, apr_packet_ptr);
            break;
         }
      case VOICE_CMD_START_HOST_PCM_V2:
         {
            voice_start_host_pcm_v2_t    *start_host_pcm_payload_ptr = (voice_start_host_pcm_v2_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM VOICE_CMD_START_HOST_PCM_V2, numTapPoints(%ld) numActivePoints(%d) sessionState(0x%lx)", start_host_pcm_payload_ptr->num_tap_points,
                  vsm_ptr->session[session_index]->host_pcm_num_active_tap_points,
                  vsm_ptr->session[session_index]->state );

            // currently can support 1 tx and 1 rx tap point per session. More comprehensive error checks can be done, if session keeps list of all active tap points.
            // it's not valid to start a tap point that has already been started, need to first stop the tap point. If the command attempts
            // to start a tap point that is already started, the whole command is NOP. This is done inside dynamic svcs.
            if( start_host_pcm_payload_ptr->num_tap_points > 2 || start_host_pcm_payload_ptr->num_tap_points == 0)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_CMD_START_HOST_PCM_V2: Incorrect number of tap points, session(%x)",session_index);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBADPARAM);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if (ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_start_tx_host_pcm( vsm_ptr, session_index, apr_packet_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_start_rx_host_pcm( vsm_ptr, session_index, apr_packet_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  /* check if any action attempt failed...if so send error response */
                  if( ADSP_SUCCEEDED(rc_rx) || ADSP_SUCCEEDED(rc_tx))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE; //PG: not required i think
                  }
                  else
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_CMD_START_HOST_PCM_V2 failed - error in send cmd to dyn svcs, session(%x)",session_index);
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_CMD_START_HOST_PCM_V2 failed - async control error, session(%x)",session_index);
               }
            }
            break;
         }
      case VOICE_CMD_STOP_HOST_PCM:
         {
            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM VOICE_CMD_STOP_HOST_PCM, numActivePoints(%d) sessionState(0x%lx), session(%x)", vsm_ptr->session[session_index]->host_pcm_num_active_tap_points,
                  vsm_ptr->session[session_index]->state,session_index );
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if (ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_stop_tx_host_pcm( vsm_ptr, session_index, apr_packet_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }

                  if( vsm_ptr->session[session_index]->rx)
                  {
                     rc_rx = vsm_send_stop_rx_host_pcm( vsm_ptr, session_index, apr_packet_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }

                  if( ADSP_SUCCEEDED(rc_rx) || ADSP_SUCCEEDED(rc_tx))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE; //PG: not required i think
                  }
                  else
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_CMD_STOP_HOST_PCM failed - error in send cmd to dyn svcs, session(%x)",session_index);
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE_CMD_STOP_HOST_PCM failed - async control error, session(%x)",session_index);
               }
            }
            break;
         }
      case VOICE_EVT_PUSH_HOST_BUF_V2:
         {
            voice_evt_push_host_pcm_buf_v2_t    *pHostPcmData = (voice_evt_push_host_pcm_buf_v2_t *) elite_apr_if_get_payload_ptr( apr_packet_ptr);

            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM VOICE_EVT_PUSH_HOST_BUF_V2, tap_point(0x%lx) mask(%ld) session(%x)",
                  pHostPcmData->tap_point, pHostPcmData->mask, session_index );

            if( NULL == vsm_memory_map_client)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_EVT_PUSH_HOST_BUF_V2, no mem map client, Event dropped, session(%x)",session_index);
               rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);
            }
            else if( NULL == vsm_ptr->session[session_index])
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_EVT_PUSH_HOST_BUF_V2, Inactive Session, Event dropped, session(%x)",session_index);
               rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);
            }
            else
            {
               ADSPResult result = ADSP_EFAILED;

               /* push directly on to CMD Q (need to check which Q appropriate based on tap point -- assume default tap points only).  treat as a command, since dataQ already used */
               if( VOICESTREAM_MODULE_TX == pHostPcmData->tap_point)
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ,(uint64_t*)msg_ptr);
                  }
               }
               else if( VOICESTREAM_MODULE_RX == pHostPcmData->tap_point)
               {
                  if( vsm_ptr->session[session_index]->rx )
                  {
                     result =qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)msg_ptr);
                  }
               }
               if( ADSP_FAILED(result))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM VOICE_EVT_PUSH_HOST_BUF_V2 to tap-point(%lx) error, Event dropped, session(%x)",pHostPcmData->tap_point,session_index);
                  rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr);
               }
            }
            break;
         }
      case VSM_CMD_SET_PKT_EXCHANGE_MODE:
         {
            vsm_set_pkt_exchange_mode_t   *pkt_exchange_mode_ptr = (vsm_set_pkt_exchange_mode_t *)
               elite_apr_if_get_payload_ptr( apr_packet_ptr);
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_PKT_EXCHANGE_MODE, pkt_exchange_mode(%d), session_state(0x%x)",
                  (int)pkt_exchange_mode_ptr->pkt_exchange_mode,
                  (int)vsm_ptr->session[session_index]->state );
            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change pkt exchange mode(0x%x->0x%x) in RUN state",
                     (int)vsm_ptr->session[session_index]->pkt_exchange_mode,
                     (int)pkt_exchange_mode_ptr->pkt_exchange_mode);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else
            {
               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {

                     rc_tx = vsm_send_tx_pkt_exchange_mode( vsm_ptr, session_index,
                           pkt_exchange_mode_ptr->pkt_exchange_mode,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_pkt_exchange_mode( vsm_ptr, session_index,
                           pkt_exchange_mode_ptr->pkt_exchange_mode,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                        (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                  {
                     vsm_ptr->session[session_index]->pending = TRUE;
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }

            }
            break;


         }
      case VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG :
         {
            uint32_t free_index;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            vsm_config_packet_exchange_t *vsm_config_oob_pkt_exchange_ptr = (vsm_config_packet_exchange_t *)
               elite_apr_if_get_payload_ptr(apr_packet_ptr);

            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG, session_state(0x%x)",
                  (int)vsm_ptr->session[session_index]->state );
            MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: OOB CONFIG PARAMS : mem_handle(%#x) enc_lsw(%#x) enc_msw(%#x) enc_size(%d) dec_lsw(%#x) dec_msw(%#x) dec_size(%d)",
                  (unsigned int) vsm_config_oob_pkt_exchange_ptr->mem_handle, (unsigned int) vsm_config_oob_pkt_exchange_ptr->enc_buf_addr_lsw,
                  (unsigned int) vsm_config_oob_pkt_exchange_ptr->enc_buf_addr_msw, (int) vsm_config_oob_pkt_exchange_ptr->enc_buf_size,
                  (unsigned int) vsm_config_oob_pkt_exchange_ptr->dec_buf_addr_lsw, (unsigned int) vsm_config_oob_pkt_exchange_ptr->dec_buf_addr_msw,
                  (int) vsm_config_oob_pkt_exchange_ptr->dec_buf_size);

            if( vsm_ptr->session[session_index]->state == VSM_RUN_STATE )
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: cannot change pkt exchange config  in RUN state");
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_ENOTREADY);
            }
            else if( vsm_ptr->session[session_index]->pending == TRUE)
            {
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
            }
            else if (vsm_ptr->session[session_index]-> pkt_exchange_mode == IN_BAND)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR ! VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG is received for IN_BAND mode!!, current mode (%d)",
                     (int) vsm_ptr->session[session_index]-> pkt_exchange_mode);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, ADSP_EUNSUPPORTED);

            }

            else if (NULL != vsm_config_oob_pkt_exchange_ptr->mem_handle)
            {
               ADSPResult result ADSP_EOK;
               uint32_t alignment_check;
               qurt_elite_memorymap_mapping_mode_t mapping_mode;

               result = qurt_elite_memorymap_get_mapping_mode(vsm_memory_map_client,
                            vsm_config_oob_pkt_exchange_ptr->mem_handle, &mapping_mode);

               if(ADSP_FAILED(result))
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! VSM_CMD_SET_OOB_PKT_EXCHANGE_CONFIG: get mapping mode failied");
                  rc = elite_apr_if_free(vsm_ptr->apr_handle, apr_packet_ptr);
                  return ADSP_EBADPARAM;
               }
               //get alignment check
               if(QURT_ELITE_MEMORYMAP_HEAP_ADDR_MAPPING == mapping_mode)
               {
                  alignment_check = 4;
               }
               else
               {
                  alignment_check = 32;
               }

               if(  vsm_ptr->session[session_index]->rx )
               {
                  // dec address should be 4 byte aligned
                  rc_rx = voice_cmn_check_align_size(vsm_config_oob_pkt_exchange_ptr->dec_buf_addr_lsw, vsm_config_oob_pkt_exchange_ptr->dec_buf_addr_msw, vsm_config_oob_pkt_exchange_ptr->dec_buf_size,4);
                  rc_rx |= voice_check_oob_buf_size(vsm_config_oob_pkt_exchange_ptr->dec_buf_size);
               }
               if(  vsm_ptr->session[session_index]->tx )
               {
                  //enc address should be 32 byte aligned for shared memory. heap does not have this limitation.
                  rc_tx = voice_cmn_check_align_size(vsm_config_oob_pkt_exchange_ptr->enc_buf_addr_lsw, vsm_config_oob_pkt_exchange_ptr->enc_buf_addr_msw, vsm_config_oob_pkt_exchange_ptr->enc_buf_size,alignment_check);
                  rc_tx |= voice_check_oob_buf_size( vsm_config_oob_pkt_exchange_ptr->enc_buf_size);
               }
               if ((ADSP_FAILED(rc_rx))||(ADSP_FAILED(rc_tx)))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ERROR! VALIDATION of OOB config parameters FAILED  rc_rx(%d) rc_tx(%d)",
                        rc_rx,rc_tx);
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
               }
               else
               {
                  rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                        apr_packet_ptr, &free_index);
                  if( ADSP_SUCCEEDED(rc))
                  {
                     if(  vsm_ptr->session[session_index]->rx )
                     {
                        rc_rx = vsm_send_rx_oob_pkt_exchange_config( vsm_ptr, session_index,
                              vsm_config_oob_pkt_exchange_ptr, &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                     }
                     if(  vsm_ptr->session[session_index]->tx )
                     {
                        rc_tx = vsm_send_tx_oob_pkt_exchange_config( vsm_ptr, session_index,
                              vsm_config_oob_pkt_exchange_ptr, &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                              VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                        vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_tx;
                     }
                     if( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                           (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx)))
                     {
                        vsm_ptr->session[session_index]->pending = TRUE;
                     }
                     else
                     {
                        rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                        voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                     }
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
                  }
               }
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR !invalid  mem_handle for pkt_exchange_config , mem_handle(%#x)",
                     (unsigned int) vsm_config_oob_pkt_exchange_ptr->mem_handle);
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, ADSP_EBADPARAM);
            }

            break;
         }
      case VSM_CMD_REGISTER_EVENT:
         {
            vsm_cmd_register_event_t   *event_reg_ptr = (vsm_cmd_register_event_t *)elite_apr_if_get_payload_ptr( apr_packet_ptr);
            uint32_t event_id = event_reg_ptr->event_id;
            switch(event_id)
            {
               case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
                  {
                     if(0 == vsm_ptr->session[session_index]->vocoder_mode_event_registered)
                     {
                        //return result first. vocoder mode registration will always succeed, plus CVD waits synchronously for the event
                        //once registration succeeds, so registration has to finish first before enc/dec issue events
                        rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EOK);

                        vsm_ptr->session[session_index]->vocoder_mode_event_registered = 1;

                        //No need for response since we already assume this will succeed. So no async handling either
                        rc = vsm_send_event_reg_unreg( vsm_ptr, session_index, event_id, VSM_CMD_REGISTER_EVENT);
                     }
                     else
                     {
                        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM client has already registered for event 0x%x, session(%x)", (int)event_id, session_index);
                        rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EALREADY);
                     }
                     break;
                  }
               default:
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM received invalid event 0x%x, session(%x)", (int)event_id, session_index);
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                  }
            }
            break;
         }
      case VSM_CMD_UNREGISTER_EVENT:
         {
            vsm_cmd_register_event_t   *event_reg_ptr = (vsm_cmd_register_event_t *)elite_apr_if_get_payload_ptr( apr_packet_ptr);
            uint32_t event_id = event_reg_ptr->event_id;
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM received unregister event 0x%x, session(%x)", (int)event_id, session_index);
            switch(event_id)
            {
               case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
                  {
                     if(1 == vsm_ptr->session[session_index]->vocoder_mode_event_registered)
                     {
                        rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EOK);

                        vsm_ptr->session[session_index]->vocoder_mode_event_registered = 0;

                        //No need for response since we already assume this will succeed. So no async handling either
                        rc = vsm_send_event_reg_unreg( vsm_ptr, session_index, event_id, VSM_CMD_UNREGISTER_EVENT);
                     }
                     else
                     {
                        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM client has already unregistered for event 0x%x, session(%x)", (int)event_id, session_index);
                        rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EALREADY);
                     }
                     break;
                  }
               default:
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM received invalid event 0x%x, session(%x)", (int)event_id, session_index);
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                  }
              }
              break;
         }
      case VSM_CMD_ENC_SET_RATE:
         {
            vsm_enc_set_rate_t *enc_rate_cmd_ptr = (vsm_enc_set_rate_t *) elite_apr_if_get_payload_ptr(apr_packet_ptr);
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM_CMD_ENC_SET_RATE, enc_rate(%lx), session(%x)",
                  enc_rate_cmd_ptr->encoder_rate, (int)session_index);

               rc = voice_init_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct,
                     apr_packet_ptr, &free_index);
               if( ADSP_SUCCEEDED(rc))
               {
                  if( vsm_ptr->session[session_index]->tx )
                  {
                     rc_tx = vsm_send_tx_enc_rate( vsm_ptr, session_index,
                           enc_rate_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_tx = rc_tx;
                  }
                  if( vsm_ptr->session[session_index]->rx )
                  {
                     rc_rx = vsm_send_rx_enc_rate( vsm_ptr, session_index,
                           enc_rate_cmd_ptr,
                           &vsm_ptr->session[session_index]->async_struct.async_status[free_index],
                           VOICE_ASYNC_STUFF_TOKEN( session_index, free_index));
                     vsm_ptr->session[session_index]->async_struct.async_status[free_index].result_rx = rc_rx;
                  }
                  if(!( (vsm_ptr->session[session_index]->rx && ADSP_SUCCEEDED(rc_rx)) ||
                      (vsm_ptr->session[session_index]->tx && ADSP_SUCCEEDED(rc_tx))))

                  {
                     rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EFAILED);
                     voice_end_async_control_rx_tx( &vsm_ptr->session[session_index]->async_struct, free_index);
                  }
               }
               else
               {
                  rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EBUSY);
               }

            break;
         }
      default:
         {
            /* Handle error. */
            if ( elite_apr_if_msg_type_is_cmd( apr_packet_ptr) )
            {  /* Complete unsupported commands. */
               rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, APR_EUNSUPPORTED );
            }
            else
            {  /* Drop unsupported events. */
               rc = elite_apr_if_free( vsm_ptr->apr_handle, apr_packet_ptr );
            }
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }

   return result;

}


static ADSPResult vsm_reassign_rx_mixer_input(void* instance_ptr, int32_t session_index,
      uint32_t input_mapping_mask,
      async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t matrix_connect_msg_t;
   uint32_t size;
#ifdef VOICE_MATRIX_DEBUG
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Reassigning rx mixer input, session_index %d, new mask %#x",
         (int) session_index,(int) input_mapping_mask);
#endif

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   size = sizeof(elite_msg_custom_vmx_cfg_inports_t);
   result = elite_msg_create_msg( &matrix_connect_msg_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create matrix_connect_msg_t EliteMsg, session_index(%x)",(int) session_index);
      return result;
   }

   elite_msg_custom_vmx_cfg_inports_t* inports_payload_ptr =
      (elite_msg_custom_vmx_cfg_inports_t*)matrix_connect_msg_t.pPayload;
   inports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
   inports_payload_ptr->cfgInPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_in_port_ptr;
   inports_payload_ptr->cfgInPortsPayload.index = vsm_ptr->session[session_index]->mixer_in_port_ptr->inport_id;
   inports_payload_ptr->cfgInPortsPayload.configuration = VMX_REASSIGN_INPUT;
   inports_payload_ptr->cfgInPortsPayload.input_mapping_mask = input_mapping_mask;
   if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n",(int) result);
      elite_msg_return_payload_buffer( &matrix_connect_msg_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_REASSIGN_PORT;
   return result;
}

static ADSPResult vsm_close_rx_mixer_input(void* instance_ptr, int32_t session_index,
      async_status_rxtx_t* async_status_ptr, uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t matrix_connect_msg_t;
   uint32_t size;
#if 1//def VOICE_MATRIX_DEBUG
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Closing rx mixer input port, session (%lx)", session_index);
#endif

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   //Input port
   if(vsm_ptr->session[session_index]->mixer_in_port_ptr != 0)
   {
      size = sizeof(elite_msg_custom_vmx_cfg_inports_t);
      result = elite_msg_create_msg( &matrix_connect_msg_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create matrix_connect_msg_t EliteMsg, session_index(%x)",(int) session_index);
         return result;
      }

      elite_msg_custom_vmx_cfg_inports_t* inports_payload_ptr =
         (elite_msg_custom_vmx_cfg_inports_t*)matrix_connect_msg_t.pPayload;
      inports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
      inports_payload_ptr->cfgInPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_in_port_ptr;
      inports_payload_ptr->cfgInPortsPayload.index = vsm_ptr->session[session_index]->mixer_in_port_ptr->inport_id;
      inports_payload_ptr->cfgInPortsPayload.configuration = VMX_CLOSE_INPUT;
      if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n",(int) result);
         elite_msg_return_payload_buffer( &matrix_connect_msg_t);
         return result;
      }
      async_status_ptr->state_rx = VSM_ACTION_CLOSE_PORT;
   }
   return result;
}

static ADSPResult vsm_reassign_tx_mixer_output(void* instance_ptr, int32_t session_index,
      uint32_t output_mapping_mask,
      async_status_rxtx_t* async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t matrix_connect_msg_t;
   uint32_t size;
#ifdef VOICE_MATRIX_DEBUG
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Reassigning tx mixer output, session %d, new mask %#x",
         (int) session_index,(int) output_mapping_mask);
#endif

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   size = sizeof(elite_msg_custom_vmx_cfg_outports_t);
   result = elite_msg_create_msg( &matrix_connect_msg_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create matrix_connect_msg_t EliteMsg, session_index(%x)",(int) session_index);
      return result;
   }

   elite_msg_custom_vmx_cfg_outports_t* outports_payload_ptr = (elite_msg_custom_vmx_cfg_outports_t*)
      matrix_connect_msg_t.pPayload;
   outports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
   outports_payload_ptr->cfgOutPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_out_port_ptr;
   outports_payload_ptr->cfgOutPortsPayload.index = vsm_ptr->session[session_index]->mixer_out_port_ptr->outport_id;
   outports_payload_ptr->cfgOutPortsPayload.configuration = VMX_REASSIGN_OUTPUT;
   outports_payload_ptr->cfgOutPortsPayload.output_mapping_mask = output_mapping_mask;
   if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n",(int) result);
      elite_msg_return_payload_buffer( &matrix_connect_msg_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_REASSIGN_PORT;
   return result;
}

static ADSPResult vsm_close_tx_mixer_output(void* instance_ptr, int32_t session_index,
      async_status_rxtx_t* async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t matrix_connect_msg_t;
   uint32_t size;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Closing tx mixer output, session (%lx)", session_index);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   if(vsm_ptr->session[session_index]->mixer_out_port_ptr != NULL)
   {
      size = sizeof(elite_msg_custom_vmx_cfg_outports_t);
      result = elite_msg_create_msg( &matrix_connect_msg_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_tx_resp_q_ptr,
            token,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create matrix_connect_msg_t EliteMsg, session_index(%x)",(int) session_index);
         return result;
      }

      elite_msg_custom_vmx_cfg_outports_t* outports_payload_ptr = (elite_msg_custom_vmx_cfg_outports_t*)matrix_connect_msg_t.pPayload;
      outports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
      outports_payload_ptr->cfgOutPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_out_port_ptr;
      outports_payload_ptr->cfgOutPortsPayload.index = vsm_ptr->session[session_index]->mixer_out_port_ptr->outport_id;
      outports_payload_ptr->cfgOutPortsPayload.configuration = VMX_CLOSE_OUTPUT;

      if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n",(int) result);
         elite_msg_return_payload_buffer( &matrix_connect_msg_t);
         return result;
      }
      async_status_ptr->state_tx = VSM_ACTION_CLOSE_PORT;
   }
   return result;
}



static ADSPResult vsm_create_rx_mixer_input( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t* async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM creating mixer input port ...");
   elite_msg_any_t matrix_connect_msg_t;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   uint32_t size = sizeof(elite_msg_custom_vmx_cfg_inports_t);
   result = elite_msg_create_msg( &matrix_connect_msg_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   elite_msg_custom_vmx_cfg_inports_t* inports_payload_ptr = (elite_msg_custom_vmx_cfg_inports_t*)
      matrix_connect_msg_t.pPayload;
   inports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
   inports_payload_ptr->cfgInPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_in_port_ptr;
   inports_payload_ptr->cfgInPortsPayload.index = -1;
   inports_payload_ptr->cfgInPortsPayload.configuration = VMX_NEW_INPUT;
   inports_payload_ptr->cfgInPortsPayload.priority = VMX_INPUT_HIGH_PRIO;
   //Map input_mapping_mask using tx_handles[] array and portIDs from VPM structures
   //rx_handles begin after tx_device_count amount of handles
   inports_payload_ptr->cfgInPortsPayload.input_mapping_mask = vsm_ptr->session[session_index]->input_mapping_mask = 0;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM got input_mapping_mask %#lx for rx path", inports_payload_ptr->cfgInPortsPayload.input_mapping_mask);
   if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
   {
      elite_msg_return_payload_buffer( &matrix_connect_msg_t);
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n",(int) result);
      return(result);
   }
   async_status_ptr->state_rx = VSM_ACTION_CREATE_PORT;
   return result;
}

static ADSPResult vsm_reconfigure_tx_mixer_output( void* instance_ptr, uint32_t session_index,
      uint16_t sampling_rate,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t matrix_connect_msg_t;

   async_status_ptr->state_tx = VSM_ACTION_NONE; //init

   uint32_t size = sizeof(elite_msg_custom_vmx_cfg_outports_t);
   result = elite_msg_create_msg( &matrix_connect_msg_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   elite_msg_custom_vmx_cfg_outports_t* outports_payload_ptr = (elite_msg_custom_vmx_cfg_outports_t*)matrix_connect_msg_t.pPayload;
   outports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
   outports_payload_ptr->cfgOutPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_out_port_ptr;
   outports_payload_ptr->cfgOutPortsPayload.index = vsm_ptr->session[session_index]->mixer_out_port_ptr->outport_id;
   outports_payload_ptr->cfgOutPortsPayload.configuration = VMX_RECONFIGURE_OUTPUT;
   outports_payload_ptr->cfgOutPortsPayload.out_sample_rate = sampling_rate / 1000;
   outports_payload_ptr->cfgOutPortsPayload.svc_handle_ptr = vsm_ptr->session[session_index]->tx;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n",(int) result);
      elite_msg_return_payload_buffer( &matrix_connect_msg_t);
      return(result);
   }

   async_status_ptr->state_tx = VSM_ACTION_RECONFIG_PORT;

   return result;
}

static ADSPResult vsm_create_tx_mixer_output(void* instance_ptr, uint32_t session_index,
      uint16_t sampling_rate,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{

   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM creating mixer input port ...");
   elite_msg_any_t matrix_connect_msg_t;

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   uint32_t size = sizeof(elite_msg_custom_vmx_cfg_outports_t);
   result = elite_msg_create_msg( &matrix_connect_msg_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   matrix_connect_msg_t.unOpCode = ELITE_CUSTOM_MSG;
   elite_msg_custom_vmx_cfg_outports_t* outports_payload_ptr = (elite_msg_custom_vmx_cfg_outports_t*)matrix_connect_msg_t.pPayload;
   outports_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
   outports_payload_ptr->cfgOutPortsPayload.ack_ptr = &vsm_ptr->session[session_index]->mixer_out_port_ptr;
   outports_payload_ptr->cfgOutPortsPayload.index = -1;
   outports_payload_ptr->cfgOutPortsPayload.configuration = VMX_NEW_OUTPUT;
   outports_payload_ptr->cfgOutPortsPayload.num_output_bufs = 2;
   outports_payload_ptr->cfgOutPortsPayload.max_bufq_capacity = 4;
   outports_payload_ptr->cfgOutPortsPayload.out_sample_rate = sampling_rate / 1000;
   //tx handles are listed first
   outports_payload_ptr->cfgOutPortsPayload.output_mapping_mask = vsm_ptr->session[session_index]->output_mapping_mask = 0;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM got output_mapping_mask %#lx for tx path",
         outports_payload_ptr->cfgOutPortsPayload.output_mapping_mask);
   outports_payload_ptr->cfgOutPortsPayload.svc_handle_ptr = vsm_ptr->session[session_index]->tx;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrix_connect_msg_t )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n",(int) result);
      elite_msg_return_payload_buffer( &matrix_connect_msg_t);
      return(result);
   }
   async_status_ptr->state_tx = VSM_ACTION_CREATE_PORT;
   return result;
}

static ADSPResult vsm_control_rx_mixer_input(void* instance_ptr, uint32_t session_index,
      uint32_t command,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   if(vsm_ptr->session[session_index]->mixer_in_port_ptr)
   {
      elite_msg_any_t mixer_control_msg_in_t;

      uint32_t size = sizeof(elite_msg_custom_vmx_stop_run_t);
      result = elite_msg_create_msg(&mixer_control_msg_in_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      elite_msg_custom_stop_t *payload_ptr = (elite_msg_custom_vmx_stop_run_t*)mixer_control_msg_in_t.pPayload;
      payload_ptr->unSecOpCode = command;
      payload_ptr->unPortID = vsm_ptr->session[session_index]->mixer_in_port_ptr->inport_id;
      payload_ptr->unDirection = VMX_INPUT_DIRECTION;
      if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&mixer_control_msg_in_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n",(int) result);
         elite_msg_return_payload_buffer(&mixer_control_msg_in_t);
         return(result);
      }
      else
      {
         if( command == ELITEMSG_CUSTOM_VMX_RUN)
         {
            async_status_ptr->state_rx = VSM_ACTION_RUN_PORT;
         }
         else if( command == ELITEMSG_CUSTOM_VMX_STOP)
         {
            async_status_ptr->state_rx = VSM_ACTION_STOP_PORT;
         }
      }
   }

   return result;
}

static ADSPResult vsm_control_tx_mixer_output(void* instance_ptr, uint32_t session_index,
      uint32_t command,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   // tx path - stopping output port
   if(vsm_ptr->session[session_index]->mixer_out_port_ptr)
   {
      elite_msg_any_t mixer_control_msg_out_t;
      uint32_t size = sizeof(elite_msg_custom_vmx_stop_run_t);
      result = elite_msg_create_msg(&mixer_control_msg_out_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_tx_resp_q_ptr,
            token,
            ADSP_EOK);

      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }

      elite_msg_custom_stop_t *payload_ptr = (elite_msg_custom_vmx_stop_run_t*)mixer_control_msg_out_t.pPayload;
      payload_ptr->unSecOpCode = command;
      payload_ptr->unPortID = vsm_ptr->session[session_index]->mixer_out_port_ptr->outport_id;
      payload_ptr->unDirection = VMX_OUTPUT_DIRECTION;
      if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&mixer_control_msg_out_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n",(int) result);
         elite_msg_return_payload_buffer(&mixer_control_msg_out_t);
         return(result);
      }
      else
      {
         if( command == ELITEMSG_CUSTOM_VMX_RUN)
         {
            async_status_ptr->state_tx = VSM_ACTION_RUN_PORT;
         }
         else if( command == ELITEMSG_CUSTOM_VMX_STOP)
         {
            async_status_ptr->state_tx = VSM_ACTION_STOP_PORT;
         }
      }
   }

   return result;
}


static ADSPResult vsm_connect_rx_downstream( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM acquired input port on rx matrix! PortID = %d!!\n",
         (int) vsm_ptr->session[session_index]->mixer_in_port_ptr->inport_id);
   //Connect voiceDec to matrix
   //First map the cvd handle to EliteSvc handle.
   elite_msg_any_t rx_connect_msg_t;
   uint32_t size = sizeof(elite_msg_cmd_connect_t);
   result = elite_msg_create_msg( &rx_connect_msg_t,
         &size,
         ELITE_CMD_CONNECT,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   elite_msg_cmd_connect_t* connect_payload_ptr = (elite_msg_cmd_connect_t*)rx_connect_msg_t.pPayload;

   connect_payload_ptr->pSvcHandle = &vsm_ptr->session[session_index]->mixer_in_port_ptr->port_handle; //TODO: modify per session numbering protocol

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ,(uint64_t*)&rx_connect_msg_t);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect Rx downstream!!\n");
      elite_msg_return_payload_buffer( &rx_connect_msg_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_CONNECT_DOWNSTREAM;

   return result;
}

/* these functions abuse the ELITE_DATA_MEDIA_TYPE in a way, since this is not going to the dataQ of dynamic service
 * to set the vocoder media type.  However, this can be used rather than a custom message
 */
static ADSPResult vsm_send_rx_media_type( void* instance_ptr, uint32_t session_index,
      uint32_t media_type,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{

   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t msg_rx_t;
   elite_msg_data_media_type_header_t *payload_ptr = NULL;

   uint32_t size = sizeof(elite_msg_data_media_type_header_t);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_DATA_MEDIA_TYPE,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_data_media_type_header_t*) msg_rx_t.pPayload;
   payload_ptr->unMediaTypeFormat = media_type;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set vocoder type on rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }

   async_status_ptr->state_rx = VSM_ACTION_SET_MEDIATYPE;

   return result;
}

static ADSPResult vsm_send_rx_samp_rate ( void* instance_ptr, uint32_t session_index,
      vsm_set_samp_rate_t   *samp_rate_cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_voc_stream_set_sampling_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_stream_set_sampling_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_stream_set_sampling_type*) msg_rx_t.pPayload;

   payload_ptr->sec_opcode = VDEC_SET_STREAM_PP_SAMP_RATE;
   async_status_ptr->state_rx = VSM_ACTION_SET_PP_SAMP_RATE;
   payload_ptr->tx_samp_rate = samp_rate_cmd_ptr->tx_samp_rate;
   payload_ptr->rx_samp_rate = samp_rate_cmd_ptr->rx_samp_rate;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set sampling rate on rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }

   return result;
}

static ADSPResult vsm_send_tx_soft_mute( void* instance_ptr, uint32_t session_index,
      voice_set_soft_mute_v2_t *cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token, uint32_t opcode)
{
   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_voc_set_soft_mute_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_set_soft_mute_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   if( VOICE_SET_MUTE_RX_ONLY == cmd_ptr->direction)
   {
      return ADSP_EBADPARAM;
   }

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_set_soft_mute_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = VENC_SET_MUTE_CMD;
   payload_ptr->mute       = cmd_ptr->mute;
   payload_ptr->ramp_duration       = cmd_ptr->ramp_duration;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set VFR type on tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }

   async_status_ptr->state_tx = VSM_ACTION_SET_SOFT_MUTE;

   return result;
}

static ADSPResult vsm_send_rx_soft_mute( void* instance_ptr, uint32_t session_index,
      voice_set_soft_mute_v2_t *cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token, uint32_t opcode)
{

   ADSPResult result = ADSP_EOK;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_voc_set_soft_mute_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_set_soft_mute_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   if( VOICE_SET_MUTE_TX_ONLY == cmd_ptr->direction)
   {
      return ADSP_EBADPARAM;
   }

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_set_soft_mute_type*) msg_rx_t.pPayload;
   payload_ptr->sec_opcode = VDEC_SET_MUTE_CMD;
   payload_ptr->mute       = cmd_ptr->mute;
   payload_ptr->ramp_duration       = cmd_ptr->ramp_duration;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set mute on rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }

   async_status_ptr->state_rx = VSM_ACTION_SET_SOFT_MUTE;

   return result;
}

static ADSPResult vsm_reinit_rx_session( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_header_t *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_header_t);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_header_t*) msg_rx_t.pPayload;
   payload_ptr->unSecOpCode = VDEC_REINIT_CMD;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set VFR type rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_REINIT_DYNAMIC_SVC;

   return result;
}
static ADSPResult vsm_reinit_tx_session( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{

   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_header_t *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_header_t);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_header_t*) msg_tx_t.pPayload;
   payload_ptr->unSecOpCode = VENC_REINIT_CMD;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set VFR type tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_REINIT_DYNAMIC_SVC;

   return result;
}

static ADSPResult vsm_send_tx_media_type ( void* instance_ptr, uint32_t session_index,
      uint32_t media_type,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_data_media_type_header_t *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_data_media_type_header_t);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_DATA_MEDIA_TYPE,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_data_media_type_header_t*) msg_tx_t.pPayload;
   payload_ptr->unMediaTypeFormat = media_type;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set vocoder type on tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_MEDIATYPE;

   return result;
}

static ADSPResult vsm_send_tx_samp_rate ( void* instance_ptr, uint32_t session_index,
      vsm_set_samp_rate_t   *samp_rate_cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_voc_stream_set_sampling_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_stream_set_sampling_type);

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_stream_set_sampling_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = VENC_SET_STREAM_PP_SAMP_RATE;
   payload_ptr->tx_samp_rate = samp_rate_cmd_ptr->tx_samp_rate;
   payload_ptr->rx_samp_rate = samp_rate_cmd_ptr->rx_samp_rate;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set sampling rate on tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_PP_SAMP_RATE;
   return result;
}

static ADSPResult vsm_send_tx_vfr_mode( void* instance_ptr, uint32_t session_index,
      int32_t* vfr_cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token,
      uint32_t sec_opcode)
{
   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_voc_timing_param_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_timing_param_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg, sess_index(%lx)\n",session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_timing_param_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = sec_opcode;
   payload_ptr->param_data_ptr = vfr_cmd_ptr;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to set timing cmd on venc, sess_index(%lx)\n",session_index);
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_VFR;

   return result;
}


static ADSPResult vsm_send_rx_vfr_mode(  void* instance_ptr, uint32_t session_index,
      int32_t* vfr_cmd_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token,
      uint32_t sec_opcode)
{

   ADSPResult result = ADSP_EOK;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_voc_timing_param_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_timing_param_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg,sess_index(%lx)\n",session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_timing_param_type*) msg_rx_t.pPayload;
   payload_ptr->sec_opcode = sec_opcode;
   payload_ptr->param_data_ptr = vfr_cmd_ptr;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set VFR type on rx,sess_index(%lx)\n",session_index);
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }

   async_status_ptr->state_rx = VSM_ACTION_SET_VFR;

   return result;
}

static int32_t vsm_convert_to_apr_result( void *instance_ptr, uint32_t session_index,
      ADSPResult rx_result, ADSPResult tx_result)
{
   int32_t rc = APR_EOK;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;

   /* ensure we treat result as success if session doesn't even exist */
   if( ! vsm_ptr->session[session_index]->rx)
   {
      rx_result = ADSP_EOK;
   }
   if( ! vsm_ptr->session[session_index]->tx)
   {
      tx_result = ADSP_EOK;
   }

   if( ADSP_FAILED(tx_result) &&
         ADSP_FAILED(rx_result))
   {
      /* if both failed with different error codes, return generic EFAILED, else return matching error code since
       * APR<->ADSP mapping is same */
      rc = ( tx_result == rx_result ) ? ( (int32_t) tx_result ) : (APR_EFAILED);
   }
   else if( ADSP_FAILED(tx_result) &&
         ADSP_SUCCEEDED(rx_result))
   {
      rc = (int32_t) tx_result;  // currently mapping is same between APR and ADSP error codes */
   }
   else if( ADSP_FAILED(rx_result) &&
         ADSP_SUCCEEDED(tx_result))
   {
      rc = (int32_t) rx_result;  // currently mapping is same between APR and ADSP error codes */
   }

   return rc;
}

static ADSPResult vsm_start_record_tx ( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_voc_svc_connect_record_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_svc_connect_record_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init


   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   payload_ptr = (elite_msg_custom_voc_svc_connect_record_type*) msg_tx_t.pPayload;
   payload_ptr->svc_handle_ptr  = vsm_ptr->session[session_index]->tx_afe_handle_ptr;
   payload_ptr->afe_drift_ptr = vsm_ptr->session[session_index]->tx_afe_drift_ptr;
   payload_ptr->sec_opcode = VENC_START_RECORD_CMD_V2;
   payload_ptr->record_mode  = vsm_ptr->session[session_index]->record_mode;
   payload_ptr->aud_ref_port = vsm_ptr->session[session_index]->aud_ref_port_tx;
   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to send start record on tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_START_RECORD;

   return result;
}

static ADSPResult vsm_start_record_rx ( void* instance_ptr, uint32_t session_index,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_voc_svc_connect_record_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_svc_connect_record_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   payload_ptr = (elite_msg_custom_voc_svc_connect_record_type*) msg_rx_t.pPayload;
   payload_ptr->svc_handle_ptr  = vsm_ptr->session[session_index]->rx_afe_handle_ptr;
   payload_ptr->afe_drift_ptr = vsm_ptr->session[session_index]->rx_afe_drift_ptr;
   payload_ptr->sec_opcode = VDEC_START_RECORD_CMD_V2;
   payload_ptr->record_mode  = vsm_ptr->session[session_index]->record_mode;
   payload_ptr->aud_ref_port = vsm_ptr->session[session_index]->aud_ref_port_rx;
   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to send start record on rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_START_RECORD;

   return result;
}

static ADSPResult vsm_connect_afe(void* instance_ptr, uint32_t session_index,
      uint16_t sampling_rate, uint16_t tx_session,
      uint16_t port, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   ADSPResult result = ADSP_EOK;
   afe_client_t afe_client;      // tx afe client params

   if( TRUE == tx_session)
   {
      async_status_ptr->state_tx = VSM_ACTION_NONE;  //init
   }
   else
   {
      async_status_ptr->state_rx = VSM_ACTION_NONE;  //init
   }
   if(session_index >= VSM_MAX_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }

   {
      elite_msg_any_t msg_t;
      uint32_t num_channels =  1; //MONO


      // Common Variables
      elite_msg_custom_afe_connect_t   *payload_ptr;
      uint32_t size;

      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::VSM_START_RECORD_CMD:Connecting to AFE PSUEDO/REAL PORT, fTx(%d),SR(%d),session_index(%lx)",
            tx_session,sampling_rate,session_index);

      if(VSM_RECORD_TX_RX_STEREO == vsm_ptr->session[session_index]->record_mode)
      {
         num_channels = 2; //STEREO
      }

      //***************************************************************************
      // Connecting to afe rx port
      //***************************************************************************
      // Build the rx client parameters
      // Initialize AFE client to all zeros (default values)
      memset(&afe_client, 0, sizeof(afe_client));
      afe_client.sample_rate      = sampling_rate;
      afe_client.data_path      = AFE_RX_VOC_MIXER_IN;
      afe_client.channel        = num_channels;
      afe_client.bytes_per_channel = 2;
      afe_client.bit_width = 16;
      afe_client.buf_size         = sampling_rate / 50; // 20ms
      afe_client.num_buffers       = 2;
      afe_client.interleaved      = FALSE;
      afe_client.cust_proc.resampler_type = IIR_BASED_SRC;
      afe_client.afe_client_handle        = NULL;
      afe_client.cust_proc.subscribe_to_avt_drift = FALSE; // dont subscribe to AV Timer, Voice Timer will take care of this
      // TODO: Voice Timer will take care of this

      size = sizeof(elite_msg_custom_afe_connect_t);
      result = elite_msg_create_msg( &msg_t,
            &size,
            ELITE_CUSTOM_MSG,
            (tx_session == TRUE) ? vsm_ptr->vsm_tx_resp_q_ptr : vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      payload_ptr = (elite_msg_custom_afe_connect_t *) msg_t.pPayload;
      payload_ptr->sec_op_code = ELITEMSG_CUSTOM_AFECONNECT_REQ;
      payload_ptr->afe_id     = port;
      payload_ptr->afe_client   = afe_client;

      // Send the msg to device static service cmdQ
      if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->vsm_afe_cmd_q_ptr,(uint64_t *)&msg_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
         return(result);
      }

      if( TRUE == tx_session)
      {
         async_status_ptr->state_tx = VSM_ACTION_CONNECT_AFE;
      }
      else
      {
         async_status_ptr->state_rx = VSM_ACTION_CONNECT_AFE;
      }
   }

   return result;
}

static ADSPResult vsm_process_start_record_msg( void *instance_ptr, elite_msg_any_t *msg_ptr,
      uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;
      elite_msg_custom_afe_connect_t   *afe_connect_ptr = (elite_msg_custom_afe_connect_t *) msg_ptr->pPayload;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_CONNECT_AFE == async_struct_ptr->async_status[free_index].state_rx)
         {
            vsm_ptr->session[session_index]->rx_afe_handle_ptr = (elite_svc_handle_t *) afe_connect_ptr->svc_handle_ptr;
            vsm_ptr->session[session_index]->rx_afe_drift_ptr = (void *) afe_connect_ptr->afe_drift_ptr;

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::AFE - VDEC Connected, session_index(%lx),pHandle(%p)",session_index,vsm_ptr->session[session_index]->rx_afe_handle_ptr);

            result = vsm_start_record_rx(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_rx = result;
               async_struct_ptr->async_status[free_index].state_rx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_START_RECORD == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx MT handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_CONNECT_AFE == async_struct_ptr->async_status[free_index].state_tx)
         {
            vsm_ptr->session[session_index]->tx_afe_handle_ptr = (elite_svc_handle_t *) afe_connect_ptr->svc_handle_ptr;
            vsm_ptr->session[session_index]->tx_afe_drift_ptr = (void *) afe_connect_ptr->afe_drift_ptr;

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::AFE - VENC Connected, session_index(%lx),pHandle(%p)",session_index,vsm_ptr->session[session_index]->tx_afe_handle_ptr);

            result = vsm_start_record_tx(vsm_ptr, session_index,
                  &async_struct_ptr->async_status[free_index],
                  param_payload_ptr->unClientToken);
            /* if action attempt fails, kill state machine and save fail code */
            if(ADSP_FAILED( result))
            {
               async_struct_ptr->async_status[free_index].result_tx = result;
               async_struct_ptr->async_status[free_index].state_tx  = VSM_ACTION_NONE;
            }
         }
         else if( VSM_ACTION_START_RECORD == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: session_index(%x) Done with setup recording",
               (int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_stop_record_tx(void* instance_ptr, uint32_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_voc_svc_connect_type *payload_ptr;

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   if(vsm_ptr->session[session_index]->tx)
   {
      size = sizeof(elite_msg_custom_voc_svc_connect_type);
      result = elite_msg_create_msg( &msg_tx_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_tx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      payload_ptr = (elite_msg_custom_voc_svc_connect_type*) msg_tx_t.pPayload;
      payload_ptr->sec_opcode = VENC_STOP_RECORD_CMD;
      payload_ptr->svc_handle_ptr  = NULL;

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to stop services!!\n");
         elite_msg_return_payload_buffer( &msg_tx_t);
         return result;
      }
      async_status_ptr->state_tx = VSM_ACTION_STOP_RECORD;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_Stop_Record_Tx Pending");
   return result;

}

static ADSPResult vsm_stop_record_rx(void* instance_ptr, uint32_t session_index, async_status_rxtx_t *async_status_ptr, uint32_t token)
{
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   uint32_t size = 0;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_voc_svc_connect_type *payload_ptr;

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_Stop_Record_Rx Invoked session_index(%x)",(int)session_index);

   if(vsm_ptr->session[session_index]->rx)
   {
      size = sizeof(elite_msg_custom_voc_svc_connect_type);
      result = elite_msg_create_msg( &msg_rx_t,
            &size,
            ELITE_CUSTOM_MSG,
            vsm_ptr->vsm_rx_resp_q_ptr,
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      payload_ptr = (elite_msg_custom_voc_svc_connect_type*) msg_rx_t.pPayload;
      payload_ptr->sec_opcode = VDEC_STOP_RECORD_CMD;
      payload_ptr->svc_handle_ptr  = NULL;

      result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to stop services!!\n");
         elite_msg_return_payload_buffer( &msg_rx_t);
         return result;
      }
      async_status_ptr->state_rx = VSM_ACTION_STOP_RECORD;
   }

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::VSM_Stop_Record_Rx Pending");
   return result;
}

static ADSPResult vsm_disconnect_afe(void* instance_ptr, uint32_t session_index,
      uint16_t port, async_status_rxtx_t *async_status_ptr, uint32_t token,
      uint16_t direction)
{
   voice_strm_mgr_t* vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   ADSPResult result = ADSP_EOK;
   if(session_index >= VSM_MAX_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_strm_session_t *current_session_ptr = vsm_ptr->session[session_index];

   {
      elite_msg_any_t msg_t;

      // Common Variables
      elite_msg_custom_afe_connect_t   *payload_ptr;
      uint32_t size;

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::VSM_STOP_RECORD_CMD:Disconnecting to AFE PSUEDO PORT,port(%x), session_index(%lx)",
            port, session_index);

      //***************************************************************************
      // DisConnecting to afe rx port
      //***************************************************************************
      size = sizeof(elite_msg_custom_afe_connect_t);
      result = elite_msg_create_msg( &msg_t,
            &size,
            ELITE_CUSTOM_MSG,
            ((VSM_RX_PATH == direction) ? vsm_ptr->vsm_rx_resp_q_ptr : vsm_ptr->vsm_tx_resp_q_ptr),
            token,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      payload_ptr = (elite_msg_custom_afe_connect_t *) msg_t.pPayload;
      payload_ptr->sec_op_code = ELITEMSG_CUSTOM_AFEDISCONNECT_REQ;
      payload_ptr->afe_id     = port;

      payload_ptr->svc_handle_ptr = ((VSM_RX_PATH == direction) ? current_session_ptr->rx_afe_handle_ptr : current_session_ptr->tx_afe_handle_ptr);

      // Send the msg to device static service cmdQ
      if (ADSP_FAILED(result =qurt_elite_queue_push_back(vsm_ptr->vsm_afe_cmd_q_ptr,(uint64_t *)&msg_t )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
         return(result);
      }
      //***************************************************************************
      // End of connecting tx port
      //***************************************************************************

      if (VSM_RX_PATH == direction)
      {
         async_status_ptr->state_rx = VSM_ACTION_DISCONNECT_AFE;
      }
      else
      {
         async_status_ptr->state_tx = VSM_ACTION_DISCONNECT_AFE;
      }

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::Sent Disconnect command to afe port (%x), session_index(%lx)",
            port, session_index);
   }
   return result;
}

static ADSPResult vsm_process_stop_record_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{
   int32_t         rc=ADSP_EOK;
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index              = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index             = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr                     = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result        = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;
      voice_strm_session_t *session_ptr = vsm_ptr->session[session_index];

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_STOP_RECORD == async_struct_ptr->async_status[free_index].state_rx)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::VDEC stop record path done, session_index(%lx)", session_index );
            if(VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->rx_tap_point)
            {
               uint16_t afe_port = vsm_ptr->session[session_index]->aud_ref_port_rx;
               result = vsm_disconnect_afe(vsm_ptr,session_index,afe_port,
                     &(async_struct_ptr->async_status[free_index]), param_payload_ptr->unClientToken,VSM_RX_PATH);
               if(ADSP_FAILED( response_result))
               {
                  async_struct_ptr->async_status[free_index].result_rx = response_result;
                  async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
               }
            }
         }
         else if (VSM_ACTION_DISCONNECT_AFE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx MT handling
            session_ptr->rx_afe_handle_ptr = NULL;
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::AFE - VDEC Disconnected, session_index(%lx)",
                  session_index );
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if ( VSM_ACTION_STOP_RECORD == async_struct_ptr->async_status[free_index].state_tx)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::VENC stop record path done, session_index(%lx)", session_index );
            if(VSM_TAP_POINT_STREAM_END == vsm_ptr->session[session_index]->tx_tap_point)
            {
               uint16_t afe_port = vsm_ptr->session[session_index]->aud_ref_port_tx;
               result = vsm_disconnect_afe(vsm_ptr,session_index,afe_port,
                     &(async_struct_ptr->async_status[free_index]), param_payload_ptr->unClientToken,VSM_TX_PATH);
               if(ADSP_FAILED( response_result))
               {
                  async_struct_ptr->async_status[free_index].result_tx = response_result;
                  async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
               }
            }
         }
         else if ( VSM_ACTION_DISCONNECT_AFE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
            session_ptr->tx_afe_handle_ptr = NULL;
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: ICCR::AFE - VENC Disconnected, session_index(%lx)",
                  session_index );
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         vsm_ptr->session[session_index]->pending = FALSE;
         vsm_ptr->session[session_index]->rx_tap_point = VSM_TAP_POINT_NONE;
         vsm_ptr->session[session_index]->tx_tap_point = VSM_TAP_POINT_NONE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: ICCR::session_index(%x) Done with stop recording",
               (int)session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_send_start_tx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *apr_packet_ptr,async_status_rxtx_t *async_status_ptr,uint32_t token)
{
   ADSPResult result;
   VoiceStrmMgr_t* me = (VoiceStrmMgr_t*)instance_ptr;
   elite_msg_any_t msg_tx;
   elite_msg_custom_voc_config_host_pcm_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx,
         &size,
         ELITE_CUSTOM_MSG,
         me->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg, session(%lx)\n",session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_config_host_pcm_type*) msg_tx.pPayload;
   payload_ptr->sec_opcode        = VENC_CONFIG_HOST_PCM;
   payload_ptr->apr_handle        = me->apr_handle;
   payload_ptr->apr_packet_ptr    = apr_packet_ptr;

   result = qurt_elite_queue_push_back(me->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to start Host PCM on tx stream, session(%lx)\n",session_index);
      elite_msg_return_payload_buffer( &msg_tx);
      return result;
   }

   async_status_ptr->state_tx = VSM_ACTION_START_HPCM;
   return result;
}

static ADSPResult vsm_send_start_rx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *apr_packet_ptr,async_status_rxtx_t *async_status_ptr,uint32_t token)
{
   ADSPResult result;
   VoiceStrmMgr_t* me = (VoiceStrmMgr_t*)instance_ptr;
   elite_msg_any_t msg_rx;
   elite_msg_custom_voc_config_host_pcm_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx,
         &size,
         ELITE_CUSTOM_MSG,
         me->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg, session(%lx)\n",session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_config_host_pcm_type*) msg_rx.pPayload;
   payload_ptr->sec_opcode        = VDEC_CONFIG_HOST_PCM;
   payload_ptr->apr_handle        = me->apr_handle;
   payload_ptr->apr_packet_ptr    = apr_packet_ptr;
   result = qurt_elite_queue_push_back(me->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to start Host PCM on rx stream, session(%lx)\n",session_index);
      elite_msg_return_payload_buffer( &msg_rx);
      return result;
   }

   async_status_ptr->state_rx = VSM_ACTION_START_HPCM;
   return result;
}

static ADSPResult vsm_send_stop_tx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *apr_packet_ptr,async_status_rxtx_t *async_status_ptr,uint32_t token)
{
   ADSPResult result;
   VoiceStrmMgr_t* me = (VoiceStrmMgr_t*)instance_ptr;
   elite_msg_any_t msg_tx;
   elite_msg_custom_voc_config_host_pcm_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE; // init

   result = elite_msg_create_msg( &msg_tx,
         &size,
         ELITE_CUSTOM_MSG,
         me->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg, session(%lx)\n",session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_config_host_pcm_type*) msg_tx.pPayload;
   payload_ptr->sec_opcode        = VENC_CONFIG_HOST_PCM;
   payload_ptr->apr_packet_ptr    = apr_packet_ptr;

   result = qurt_elite_queue_push_back(me->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to stop Host PCM on tx stream, session(%lx)\n",session_index);
      elite_msg_return_payload_buffer( &msg_tx);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_STOP_HPCM;

   return result;
}

static ADSPResult vsm_send_stop_rx_host_pcm( void* instance_ptr, uint32_t session_index, elite_apr_packet_t *apr_packet_ptr,async_status_rxtx_t *async_status_ptr,uint32_t token)
{
   ADSPResult result;
   VoiceStrmMgr_t* me = (VoiceStrmMgr_t*)instance_ptr;
   elite_msg_any_t msg_rx;
   elite_msg_custom_voc_config_host_pcm_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE; // init

   result = elite_msg_create_msg( &msg_rx,
         &size,
         ELITE_CUSTOM_MSG,
         me->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg, session(%lx)\n", session_index);
      return result;
   }
   payload_ptr = (elite_msg_custom_voc_config_host_pcm_type*) msg_rx.pPayload;
   payload_ptr->sec_opcode        = VDEC_CONFIG_HOST_PCM;
   payload_ptr->apr_packet_ptr    = apr_packet_ptr;

   result = qurt_elite_queue_push_back(me->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to start Host PCM on rx stream, session(%lx)\n", session_index);
      elite_msg_return_payload_buffer( &msg_rx);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_STOP_HPCM;
   return result;
}

static ADSPResult vsm_process_start_hpcm_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {
      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_START_HPCM == async_struct_ptr->async_status[free_index].state_rx)
         {
            vsm_ptr->session[session_index]->host_pcm_num_active_tap_points ++;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_START_HPCM == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
            vsm_ptr->session[session_index]->host_pcm_num_active_tap_points ++;
         }
      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM VOICE_CMD_START_HOST_PCM_V2 success with results - rx(%ld) tx(%ld) apr(%ld), session(%lx)",async_struct_ptr->async_status[free_index].result_rx,async_struct_ptr->async_status[free_index].result_tx,apr_result,session_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_process_stop_hpcm_msg( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_STOP_HPCM == async_struct_ptr->async_status[free_index].state_rx)
         {
            vsm_ptr->session[session_index]->host_pcm_num_active_tap_points --;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_STOP_HPCM == async_struct_ptr->async_status[free_index].state_tx)
         {
            vsm_ptr->session[session_index]->host_pcm_num_active_tap_points --;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         vsm_ptr->session[session_index]->pending = FALSE;

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VSM VOICE_CMD_STOP_HOST_PCM success with results - rx(%ld) tx(%ld) apr(%ld), session(%lx)",async_struct_ptr->async_status[free_index].result_rx,async_struct_ptr->async_status[free_index].result_tx,apr_result,session_index);
      }
   }

   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}

static ADSPResult vsm_send_tx_pkt_exchange_mode ( void* instance_ptr, uint32_t session_index,
      uint32_t pkt_exchange_mode,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_pkt_exchange_mode_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_pkt_exchange_mode_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   payload_ptr = (elite_msg_custom_pkt_exchange_mode_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = VENC_SET_PKT_EXCHANGE_MODE;
   payload_ptr->pkt_exchange_mode = pkt_exchange_mode;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set pkt exchange mode tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_PKT_EXCHANGE_MODE;

   return result;
}


static ADSPResult vsm_send_rx_pkt_exchange_mode ( void* instance_ptr, uint32_t session_index,
      uint32_t pkt_exchange_mode,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_pkt_exchange_mode_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_pkt_exchange_mode_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }

   payload_ptr = (elite_msg_custom_pkt_exchange_mode_type*) msg_rx_t.pPayload;
   payload_ptr->sec_opcode = VDEC_SET_PKT_EXCHANGE_MODE;
   payload_ptr->pkt_exchange_mode = pkt_exchange_mode;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set pkt exchange mode rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_SET_PKT_EXCHANGE_MODE;

   return result;
}

static ADSPResult vsm_send_tx_oob_pkt_exchange_config ( void* instance_ptr, uint32_t session_index,
      vsm_config_packet_exchange_t *vsm_config_oob_pkt_exchange_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_oob_pkt_exchange_config_type *payload_ptr = NULL;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   uint32_t size = sizeof(elite_msg_custom_oob_pkt_exchange_config_type);


   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_oob_pkt_exchange_config_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = VENC_SET_OOB_PKT_EXCHANGE_CONFIG;
   payload_ptr->param_data_ptr = (vsm_config_packet_exchange_t*)vsm_config_oob_pkt_exchange_ptr;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set pkt exchange config tx!!\n");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_OOB_PKT_EXCHANGE_CONFIG;

   return result;
}

static ADSPResult vsm_send_rx_oob_pkt_exchange_config ( void* instance_ptr, uint32_t session_index,
      vsm_config_packet_exchange_t *vsm_config_oob_pkt_exchange_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_oob_pkt_exchange_config_type *payload_ptr = NULL;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   uint32_t size = sizeof(elite_msg_custom_oob_pkt_exchange_config_type);


   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_oob_pkt_exchange_config_type*) msg_rx_t.pPayload;
   payload_ptr->sec_opcode = VDEC_SET_OOB_PKT_EXCHANGE_CONFIG;
   payload_ptr->param_data_ptr = (vsm_config_packet_exchange_t*)vsm_config_oob_pkt_exchange_ptr;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set pkt exchange config rx!!\n");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_SET_OOB_PKT_EXCHANGE_CONFIG;

   return result;
}

static ADSPResult voice_check_oob_buf_size(uint32_t payload_size)
{
   ADSPResult result = ADSP_EOK;

   if ((uint32_t)MIN_OOB_BUFFER_SIZE > payload_size )
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: payload_size received (%d) less than worst case size (%d)", (int) payload_size, (int) MIN_OOB_BUFFER_SIZE);
      result= ADSP_EBADPARAM;
   }
   return result;
}

static void vsm_set_mmpm_clocks(void* instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   if (TRUE == vsm_ptr->req_clock)
   {
   // If all sessions are successfully transitioned to STOP state, then release MMPM requests.
   if (!vsm_ptr->session_run_mask)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM has no sessions running, releasing all MMPM clock requests");

      if( ADSP_FAILED( result = voice_mmpm_release( &vsm_ptr->mmpm )))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to release MMPM resources for VSM");
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Released resources for VSM");

         // Reset MMPM Configuration settings
         vsm_ptr->mmpm_min_mips_per_thread = 0;
         vsm_ptr->mmpm_mips_request = 0;
      }
   }
   else
   {
      // Default to Max settings
      uint32_t mmpm_mips_request = VSM_MAX_CLK_TOTAL_MIPS;
      uint32_t mmpm_min_mips_per_thread = VOICE_PER_THREAD_MAXCLK;
      uint32_t num_sessions = 0;
      uint32_t max_clk_session = 0;
      uint32_t mid_clk_session = 0;
      uint32_t min_clk_session = 0;

      uint32_t session_run_mask = vsm_ptr->session_run_mask;
      uint32_t i=0;
      while (session_run_mask!=0)
      {
         i = voice_get_high_lsb (session_run_mask);
         session_run_mask = voice_clr_bit(session_run_mask, i);
         num_sessions++;
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM num_sessions(%d)", (int) num_sessions);

      // Make a local copy to parse through active sessions (reuse same variable)
      session_run_mask = vsm_ptr->session_run_mask;

      do
      {
         // Get first running session.
         uint32_t index = voice_get_high_lsb (session_run_mask);

         // Identify if session falls into max/mid/min clock range.
         if (VSM_MEDIA_TYPE_4GV_NB_MODEM      == vsm_ptr->session[index]->media_type_format
               || VSM_MEDIA_TYPE_4GV_WB_MODEM == vsm_ptr->session[index]->media_type_format
               || VSM_MEDIA_TYPE_4GV_NW_MODEM == vsm_ptr->session[index]->media_type_format
               || VSM_MEDIA_TYPE_4GV_NW       == vsm_ptr->session[index]->media_type_format
               || VSM_MEDIA_TYPE_EAMR         == vsm_ptr->session[index]->media_type_format
               || VSM_TTY_MODE_OFF            != vsm_ptr->session[index]->tty_state.m_etty_mode
            )
         {
            max_clk_session++;
         }
         else if (VSM_MEDIA_TYPE_AMR_WB_MODEM == vsm_ptr->session[index]->media_type_format
               || VFR_NONE                    == vsm_ptr->session[index]->vfr_mode)  //request high clock for VFR_NONE case to take
            // care of timing concerns when TW is enabled
         {
            mid_clk_session++;
         }
         else
         {
            min_clk_session++;
         }

         // Clear index bit that has been tested.
         session_run_mask = voice_clr_bit(session_run_mask, index);

      } while (session_run_mask!=0);

      // Total MIPS request
      // TODO: This is an approximation based on worst case estimates and
      // does not account for module enablement/disablement.
      mmpm_mips_request = (max_clk_session * VSM_MAX_CLK_TOTAL_MIPS)
         + (mid_clk_session * VSM_MID_CLK_TOTAL_MIPS)
         + (min_clk_session * VSM_MIN_CLK_TOTAL_MIPS);

      // If only 1 session, then set clock in accordance with above rules.
      // Else, it'd default to max clock.
      if (1 == num_sessions)
      {
         // Minimum MIPS per thread required
         mmpm_min_mips_per_thread = (max_clk_session * VOICE_PER_THREAD_MAXCLK)
            + (mid_clk_session * VOICE_PER_THREAD_MIDCLK)
            + (min_clk_session * VOICE_PER_THREAD_MINCLK);
      }

      // If any client is the Second Application Domain (APPS), then assume
      // end application to be ADSP Test client on Target.
      // Verify if this clock request is the same as before,
      // if it is, then no need to send one more request.
         if (vsm_ptr->mmpm_min_mips_per_thread != mmpm_min_mips_per_thread
               || vsm_ptr->mmpm_mips_request != mmpm_mips_request)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM requesting MIPS(%ld); mmpm_min_mips_per_thread(%d); sessions(%ld)", (long int) mmpm_mips_request, (int) mmpm_min_mips_per_thread, (long int) num_sessions );

         if (ADSP_FAILED(result = voice_mmpm_config( &vsm_ptr->mmpm,
                     mmpm_mips_request,
                     mmpm_min_mips_per_thread,
                     VOICE_DEFAULT_AXI_IB,
                     VOICE_DEFAULT_AXI_USAGE,
                     VOICE_DEFAULT_SLEEP_LATENCY_MICROSEC)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to config MMPM for VSM");
         }
         else
         {
            // Update latest clock request settings
            vsm_ptr->mmpm_min_mips_per_thread = mmpm_min_mips_per_thread;
            vsm_ptr->mmpm_mips_request = mmpm_mips_request;
         }
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VSM not requesting mips/clock (CVD will request OR Request not needed)");
      }
   }
}
}

static ADSPResult vsm_send_event_reg_unreg( void* instance_ptr, uint16_t session_index, uint32_t event_id, uint32_t opcode)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t msg_rx, msg_tx;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;
   elite_msg_custom_event_reg_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_event_reg_type);
   uint32_t rx_sec_opcode, tx_sec_opcode;
   if( VSM_CMD_REGISTER_EVENT == opcode)
   {
      tx_sec_opcode = VENC_REGISTER_EVENT;
      rx_sec_opcode = VDEC_REGISTER_EVENT;
   }
   else
   {
      tx_sec_opcode = VENC_UNREGISTER_EVENT;
      rx_sec_opcode = VDEC_UNREGISTER_EVENT;
   }

   if(vsm_ptr->session[session_index]->rx)
   {
      result = elite_msg_create_msg( &msg_rx,
            &size,
            ELITE_CUSTOM_MSG,
            NULL, //no response required
            0,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg,sess_index(%x)",session_index);
         return result;
      }
      payload_ptr = (elite_msg_custom_event_reg_type*) msg_rx.pPayload;
      payload_ptr->sec_opcode = rx_sec_opcode;
      payload_ptr->event_id = event_id;

      (void)qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx);
   }

   if(vsm_ptr->session[session_index]->tx)
   {
      result = elite_msg_create_msg( &msg_tx,
            &size,
            ELITE_CUSTOM_MSG,
            NULL, //no response required
            0,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg,sess_index(%x)",session_index);
         return result;
      }
      payload_ptr = (elite_msg_custom_event_reg_type*) msg_tx.pPayload;
      payload_ptr->sec_opcode = tx_sec_opcode;
      payload_ptr->event_id = event_id;

      (void)qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx);
   }

   return result;
}

static ADSPResult vsm_send_tx_enc_rate( void* /*instance_ptr*/,
      uint32_t session_index,
      vsm_enc_set_rate_t* enc_rate_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t msg_tx_t;
   elite_msg_custom_set_enc_rate_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_set_enc_rate_type);

   async_status_ptr->state_tx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_tx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_tx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!");
      return result;
   }

   payload_ptr = (elite_msg_custom_set_enc_rate_type*) msg_tx_t.pPayload;
   payload_ptr->sec_opcode = VENC_SET_ENC_RATE;
   payload_ptr->media_type = enc_rate_ptr->media_type;
   payload_ptr->encoder_rate = enc_rate_ptr->encoder_rate;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->tx->cmdQ, (uint64_t*)&msg_tx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set enc rate tx!!");
      elite_msg_return_payload_buffer( &msg_tx_t);
      return result;
   }
   async_status_ptr->state_tx = VSM_ACTION_SET_ENC_RATE;

   return result;

}

static ADSPResult vsm_send_rx_enc_rate( void* /*instance_ptr*/,
      uint32_t session_index,
      vsm_enc_set_rate_t* enc_rate_ptr,
      async_status_rxtx_t *async_status_ptr,
      uint32_t token)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t msg_rx_t;
   elite_msg_custom_set_enc_rate_type *payload_ptr = NULL;
   uint32_t size = sizeof(elite_msg_custom_set_enc_rate_type);

   async_status_ptr->state_rx = VSM_ACTION_NONE;  //init

   result = elite_msg_create_msg( &msg_rx_t,
         &size,
         ELITE_CUSTOM_MSG,
         vsm_ptr->vsm_rx_resp_q_ptr,
         token,
         ADSP_EOK);

   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!");
      return result;
   }

   payload_ptr = (elite_msg_custom_set_enc_rate_type*) msg_rx_t.pPayload;
   payload_ptr->sec_opcode = VDEC_SET_ENC_RATE;
   payload_ptr->media_type = enc_rate_ptr->media_type;
   payload_ptr->encoder_rate = enc_rate_ptr->encoder_rate;

   result = qurt_elite_queue_push_back(vsm_ptr->session[session_index]->rx->cmdQ, (uint64_t*)&msg_rx_t);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set enc rate tx!!");
      elite_msg_return_payload_buffer( &msg_rx_t);
      return result;
   }
   async_status_ptr->state_rx = VSM_ACTION_SET_ENC_RATE;

   return result;

}

static ADSPResult vsm_process_set_enc_rate( void *instance_ptr, elite_msg_any_t *msg_ptr, uint8_t qid)
{

   int32_t         rc=ADSP_EOK;
   ADSPResult   result = ADSP_EOK;
   voice_strm_mgr_t *vsm_ptr = (voice_strm_mgr_t*)instance_ptr;       // instance structure
   elite_apr_packet_t* apr_packet_ptr;

   /* Cmd Q -> TBD add functionality from VSM_ProcessAprMsg */
   if( VSM_CMD_Q == qid)
   {
      apr_packet_ptr = ( elite_apr_packet_t *) msg_ptr->pPayload;
   }
   else
   {

      elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
      uint32_t free_index = VOICE_ASYNC_EXTRACT_INDEX( param_payload_ptr->unClientToken);         // extracting Index from packed ClientToken
      uint32_t session_index = VOICE_ASYNC_EXTRACT_SESSION( param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
      if(VSM_MAX_SESSIONS <= session_index) //KW fix
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: session_index(%x) is out of bound!!",(int) session_index);
         return ADSP_EBADPARAM;
      }
      async_struct_rxtx_t *async_struct_ptr = &(vsm_ptr->session[session_index]->async_struct);
      apr_packet_ptr = (elite_apr_packet_t*)async_struct_ptr->async_status[free_index].apr_packet_ptr;
      uint32_t response_result = ((elite_msg_any_payload_t *) msg_ptr->pPayload)->unResponseResult;

      if( VSM_RX_RESP_Q == qid)
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_rx = response_result;
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_ENC_RATE == async_struct_ptr->async_status[free_index].state_rx)
         {
            async_struct_ptr->async_status[free_index].state_rx = VSM_ACTION_NONE;   // done with Rx  handling
         }
      }
      else
      {
         /* if ack fails, kill state machine and save fail code */
         if(ADSP_FAILED( response_result))
         {
            async_struct_ptr->async_status[free_index].result_tx = response_result;
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }
         else if( VSM_ACTION_SET_ENC_RATE == async_struct_ptr->async_status[free_index].state_tx)
         {
            async_struct_ptr->async_status[free_index].state_tx = VSM_ACTION_NONE;
         }

      }
      /* Rx and Tx both done, so can respond to APR packet and finish the command */
      if( VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_tx &&
            VSM_ACTION_NONE == async_struct_ptr->async_status[free_index].state_rx)
      {
         /* decide on aprResult to send to client */
         int32_t apr_result = vsm_convert_to_apr_result( vsm_ptr,
               session_index,
               async_struct_ptr->async_status[free_index].result_rx,
               async_struct_ptr->async_status[free_index].result_tx);

         rc = elite_apr_if_end_cmd( vsm_ptr->apr_handle, apr_packet_ptr, apr_result );

         voice_end_async_control_rx_tx( async_struct_ptr, free_index);
      }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   return result;
}
