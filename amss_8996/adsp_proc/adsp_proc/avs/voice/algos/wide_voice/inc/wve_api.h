/*======================================================================*/
/*  Widevoice Blind Bandwidth Extension Algorithm (Fixed Point)         */
/*                                                                      */
/*  Copyright (C) 2009 Qualcomm Technologies Incorporated. All rights                */
/*  reserved.                                                           */
/*======================================================================*/

/*======================================================================*/
/* FILE: wve.h                                                          */
/*----------------------------------------------------------------------*/
/* PURPOSE : Main Interface functions of Wide Voice Extension           */
/*======================================================================*/
#ifndef __WVE_HH
#define __WVE_HH

struct wvConfigStruct
{
 bool adaptive_bass_boost_voiced;
 bool fixed_bass_boost_9dB;
 short high_band_contrib; //Q14 value
 short low_band_contrib;  //Q14 value
 short nfact; //Q8 value
 short noise_mix_factor_alpha; //Q15
 short noise_mix_factor_beta; //Q14
 unsigned int silence_hb_egy_attn; //Q26
 short smf_sens_cntrl; //Q0
 short speech_buffer_len;//Q0
 short tilt_scal_fac; //Q15
 int unvoiced_highband_gain_cap; //Q23
 short voiced_egy_map_alpha;//Q15
 int voiced_egy_map_beta; //Q23
 short voiced_pitchgain_limit; //Q13
 short voiced_tilt_limit; //Q15
};

#define SPEECH_BUFFER_LEN        160
#define WB_SPEECH_BUFFER_LEN	 (2*SPEECH_BUFFER_LEN)

int wve_init(void * wvDataStr);

int wve_compute_data_struct_size();
void wve_config_default_init(wvConfigStruct* wvConfStr);

void wve_process(short *buf_in, short *buf_out, 
      void* wvDataStr,wvConfigStruct* wvConfStr);
#endif //__WVE_H
