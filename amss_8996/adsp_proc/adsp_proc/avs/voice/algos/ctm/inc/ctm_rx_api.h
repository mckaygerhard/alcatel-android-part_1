/*======================= COPYRIGHT NOTICE ======================*/
/* Copyright (c) 2006 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*===============================================================*/
/*---------------------------------------------------------------*/
/* FileName: ctmRx.h         FileType:ctmRx C Header             */
/*---------------------------------------------------------------*/
/* Description:                                                  */
/*   Publicizes the functions to systems code                    */
/*---------------------------------------------------------------*/
/* Revision History: None                                        */
/* Author            Date                      Comments          */
/* -------          ------                     ----------        */
/* Syed Arif        27,Feb,2008             Written for QDSP6    */
/*****************************************************************/

#ifndef CTMRX_API_H
#define CTMRX_API_H

#ifndef UNIT_TEST
typedef int32 Bool;
#endif

/* Public function declarations  */
extern int ctmrx_get_struct_size(void);

extern void init_ctmRx(void *pCTMRxStructInstance);

extern void deinit_ctmRx(void *pCTMRxStructInstance);

extern char ctmrx_demodulate(void *pCTMRxStructInstance,
                        int16 *inputSignalBufferRight,
            uint16 lengthToneVecRx,
            int16 *ttyCode,
            Bool *enquiryFromFarEndDetected,
                        Bool *ctmFromFarEndDetected,
                        Bool ctmCharacterTransmitted,
            Bool handover_flag
#ifdef MIPS_PROFILE
            ,void *pProfileStructInstance
#endif
                          );
extern Bool ctmrx_ttyregen(void *pCTMRxStructInstance,
            int16 *inputSignalBufferRight,
            uint16 lengthToneVecRx,
            int16 *txToneVecLeft,
            int16 *ttyCode,
            Bool *byPassed,
            Bool disableBypass
#ifdef MIPS_PROFILE
            ,void *pProfileStructInstance
#endif
                          );

#endif
