/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp47058 $ */
/*     $Revision: 4 $ */
/*     $Date: 2015-08-28 15:30:13 +0900 (Fri, 28 Aug 2015) $ */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*  Header file for the application layer interface of the LVVEFQ Tx Module             */
/*                                                                                      */
/*  This files includes all definitions, types, structures and function prototypes      */
/*  required by the calling layer. All other types, structures and functions are        */
/*  private.                                                                            */
/*                                                                                      */
/****************************************************************************************/
#ifndef __LVVEFQ_TX_H__
#define __LVVEFQ_TX_H__


/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#ifdef TARGET_BUILD
#include "qurt_elite.h"
#include "Elite.h"
#else
#include "Elite_CAPI.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
#include "LVVE.h"


/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/


/****************************************************************************************/
/*                                                                                      */
/*  Types                                                                               */
/*                                                                                      */
/****************************************************************************************/


/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/
typedef struct
{
   LVM_CHAR **message_ptr;
   LVM_INT32 message_size;
} LVVEFQ_LVAVC_Message_st;

typedef struct
{
  /* LVVEFQ */
  LVVE_Tx_InstanceParams_st InstanceParams;
  LVVE_Tx_ControlParams_st *ControlParams;
  LVM_MemoryTable_st MemTab;
  LVVE_Tx_Handle_t hInstance;
  LVM_INT16 *pEQ_Coefs;
  LVVE_VersionInfo *pVersion;

  /* general */
  uint32_t kpps;
  uint32_t delay;
  uint16_t enable;
  uint16_t ChannelCount;
  elite_svc_handle_t *m_pRxPeer;
  LVVEFQ_LVAVC_Message_st LVVEFQ_LVAVC_Message;

} LVVEFQ_Tx_Instance_st;


/****************************************************************************************/
/*                                                                                      */
/*  Function Prototypes                                                                 */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Tx_config(LVVEFQ_Tx_Instance_st *pInstance, uint32_t sample_rate);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
uint32_t LVVEFQ_Tx_get_size(LVVEFQ_Tx_Instance_st *pInstance);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_set_mem(LVVEFQ_Tx_Instance_st *pInstance,
                             int8_t *mem_addr_ptr,
                             uint32_t mem_size);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_kpps(LVVEFQ_Tx_Instance_st *pInstance,
                              uint32_t topology_id,
                              uint32_t *kpps_ptr);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_delay(LVVEFQ_Tx_Instance_st *pInstance,
                              uint32_t topology_id,
                              uint32_t *delay_ptr);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_init(LVVEFQ_Tx_Instance_st *pInstance,
                          uint16_t ChannelCount);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Tx_end(LVVEFQ_Tx_Instance_st *pInstance);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_process(LVVEFQ_Tx_Instance_st *pInstance,
                             short **InBuffer16_Mic,
                             short *IOBuffer16_Ref,
                             short *OutBuffer16_Mic,
                             uint32_t ProcessBlockSize);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_set_param(LVVEFQ_Tx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t param_size);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_param(LVVEFQ_Tx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t buf_size,
                               uint32_t *param_size_ptr);

/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_connect_rx_peer(LVVEFQ_Tx_Instance_st *pInstance,
                                     void *pRxPeer,
                                     uint32_t SvcID);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_disconnect_rx_peer(LVVEFQ_Tx_Instance_st *pInstance, uint32_t SvcID);

#ifndef TARGET_BUILD
void LVVEFQ_Tx_PrintParams(LVVEFQ_Tx_Instance_st *pInstance);
void LVVEFQ_Tx_SetDefaultParams(LVVEFQ_Tx_Instance_st *pInstance);
#endif

#ifdef __cplusplus
}
#endif // __cplusplus

#endif  //#ifndef __LVVEFQ_TX_H__

