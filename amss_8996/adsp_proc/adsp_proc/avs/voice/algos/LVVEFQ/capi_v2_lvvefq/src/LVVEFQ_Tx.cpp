/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp47058 $                                                              */
/*     $Revision: 4 $                                                               */
/*     $Date: 2015-08-28 15:30:13 +0900 (Fri, 28 Aug 2015) $                            */
/*                                                                                      */
/****************************************************************************************/
#if defined(LVVE)


/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#include "LVVEFQ_Tx.h"
#ifdef TARGET_BUILD
#include "EliteMsg_Util.h"
#include "adsp_vparams_api.h"
#include "adsp_vpm_api.h"
#else
#include "HAP_farf.h"
#include "HAP_SDK_test_defines.h"
#include <string.h>
//#include "LVVE_ExampleApp.h"
#endif

/****************************************************************************************/
/*                                                                                      */
/*  Macro definitions Constant / Define Declarations                                    */
/*                                                                                      */
/****************************************************************************************/
#define ROUNDTO8(x) ((((x) + 7) >> 3) << 3)

/* kpps */ /* TODO */
#define VOICE_LVVEFQ_SM_NB_KPPS (12420)  // 12.42 mpps in kpps for nb
#define VOICE_LVVEFQ_SM_WB_KPPS (18960)  // 18.96 mpps in kpps for wb
#define VOICE_LVVEFQ_SM_FB_KPPS (18960)  // 18.96 mpps in kpps for wb

#define VOICE_LVVEFQ_DM_NB_KPPS (29730)  // 29.73 mpps in kpps for nb
#define VOICE_LVVEFQ_DM_WB_KPPS (51110)  // 51.11 mpps in kpps for wb
#define VOICE_LVVEFQ_DM_FB_KPPS (51110)  // 51.11 mpps in kpps for wb

#define VOICE_LVVEFQ_TM_NB_KPPS (29730)  // 29.73 mpps in kpps for nb
#define VOICE_LVVEFQ_TM_WB_KPPS (51110)  // 51.11 mpps in kpps for wb
#define VOICE_LVVEFQ_TM_FB_KPPS (51110)  // 51.11 mpps in kpps for wb

static const uint32_t LVVEFQ_TX_RESERVED_PARAM_SIZE = sizeof(uint16_t);

static const uint32_t LVVEFQ_TX_ENABLE_PARAM_SIZE = sizeof(int16_t)
                                                  + LVVEFQ_TX_RESERVED_PARAM_SIZE;

static const uint32_t LVVEFQ_TX_CONTROL_PARAM_SIZE = sizeof(LVINPUTROUTING_Tx_ControlParams_st)
                                                   + sizeof(LVOUTPUTROUTING_Tx_ControlParams_st)
                                                   + sizeof(LVM_Mode_en)
                                                   + sizeof(LVM_Mode_en)
                                                   + sizeof(LVM_Mode_en)
                                                   + sizeof(LVM_UINT16)
                                                   + sizeof(LVM_INT16);

static const uint32_t LVVEFQ_TX_VOL_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVM_INT16)
                                               + LVVEFQ_TX_RESERVED_PARAM_SIZE;

static const uint32_t LVVEFQ_TX_HPF_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVM_INT16)
                                               + LVVEFQ_TX_RESERVED_PARAM_SIZE;

static const uint32_t LVVEFQ_TX_LPF_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVM_INT16)
                                               + LVVEFQ_TX_RESERVED_PARAM_SIZE;

static const uint32_t LVVEFQ_TX_EQ_PARAM_SIZE = sizeof(LVM_Mode_en)
                                              + sizeof(LVM_INT16)
                                              + LVVEFQ_TX_RESERVED_PARAM_SIZE
                                              + (LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

static const uint32_t LVVEFQ_TX_DRC_PARAM_SIZE = sizeof(LVDRC_ControlParams_st)
                                               + LVVEFQ_TX_RESERVED_PARAM_SIZE
                                               + LVVEFQ_TX_RESERVED_PARAM_SIZE;

static const uint32_t LVVEFQ_TX_CNG_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVCNG_ControlParams_st)
                                               + sizeof(uint16_t); /* RESERVED */


/****************************************************************************************/
/*                                                                                      */
/*  Function Definitions                                                                */
/*                                                                                      */
/****************************************************************************************/
static ADSPResult LVVEFQ_Tx_send_message(LVVEFQ_Tx_Instance_st *pInstance, uint32_t paramID);


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Tx_config(LVVEFQ_Tx_Instance_st *pInstance, uint32_t sample_rate)
{
#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_config: sample rate = %d", sample_rate);
#endif

  if(sample_rate == 8000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_8000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*4;
    pInstance->InstanceParams.ReferenceFIFOLength = 80*2;
  }
  else if(sample_rate == 16000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_16000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*8;
    pInstance->InstanceParams.ReferenceFIFOLength = 80*4;
  }
  else if(sample_rate == 48000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_48000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*24;
    pInstance->InstanceParams.ReferenceFIFOLength = 80*12;
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_config: Invalid sample rate = %d", sample_rate);
  }
  pInstance->InstanceParams.MaxBulkDelay = LVVE_MAX_BULK_DELAY;

}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
uint32_t LVVEFQ_Tx_get_size(LVVEFQ_Tx_Instance_st *pInstance)
{
  uint32_t mem_size = 0;
  int32_t i;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_get_size");
#endif

  /* control parameter */
  mem_size = ROUNDTO8(sizeof(LVVE_Tx_ControlParams_st));

  /* memory table */
  LVVE_Status = LVVE_Tx_GetMemoryTable(LVM_NULL, &(pInstance->MemTab), &(pInstance->InstanceParams));
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Tx_get_size: LVVE_Tx_GetMemoryTable returned %d", (int32_t)LVVE_Status);
    return 0;
  }

  for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++)
  {
    if(pInstance->MemTab.Region[i].Size != 0)
    {
      mem_size += ROUNDTO8(pInstance->MemTab.Region[i].Size);
    }
  }

  /* eq */
  mem_size += ROUNDTO8(LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

  pInstance->LVVEFQ_LVAVC_Message.message_size = LVVE_Tx2RxMessage_GetMaxSize();
  mem_size += ROUNDTO8(pInstance->LVVEFQ_LVAVC_Message.message_size);

  /* version info */
  mem_size += ROUNDTO8(sizeof(LVVE_VersionInfo));


#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_get_size: total memory size requested = %d", mem_size);
#endif

  return mem_size;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_set_mem(LVVEFQ_Tx_Instance_st *pInstance,
                             int8_t *mem_addr_ptr,
                             uint32_t mem_size)
{
  ADSPResult result = ADSP_EOK;
  int32_t i;
  int8_t *pBase;
  uint32_t temp_mem_size = 0;

#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_set_mem: total memory size acquired = %d", mem_size);
#endif

  /* assign base address for each region */
  pBase = mem_addr_ptr;

  /* control parameter */
  temp_mem_size = ROUNDTO8(sizeof(LVVE_Tx_ControlParams_st));
  pInstance->ControlParams = (LVVE_Tx_ControlParams_st *)pBase;
  memset(pInstance->ControlParams, 0, temp_mem_size);
  pBase += temp_mem_size;

  /* memory table */
  for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++)
  {
    if(pInstance->MemTab.Region[i].Size != 0)
    {
      temp_mem_size = ROUNDTO8(pInstance->MemTab.Region[i].Size);
      pInstance->MemTab.Region[i].pBaseAddress = (void *)pBase;
      memset(pInstance->MemTab.Region[i].pBaseAddress, 0, temp_mem_size);
      pBase += temp_mem_size;
    }
  }

  /* eq */
  temp_mem_size = ROUNDTO8(LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
  pInstance->pEQ_Coefs = (LVM_INT16 *)pBase;
  memset(pInstance->pEQ_Coefs, 0, temp_mem_size);
  pBase += temp_mem_size;

  temp_mem_size = ROUNDTO8(pInstance->LVVEFQ_LVAVC_Message.message_size);
  pInstance->LVVEFQ_LVAVC_Message.message_ptr = (LVM_CHAR **)pBase;
  memset((LVM_INT8 *)pInstance->LVVEFQ_LVAVC_Message.message_ptr, 0, temp_mem_size);
  pInstance->LVVEFQ_LVAVC_Message.message_size = 0;
  pBase += temp_mem_size;

  /* version info */
  temp_mem_size = ROUNDTO8(sizeof(LVVE_VersionInfo));
  pInstance->pVersion = (LVVE_VersionInfo *)pBase;
  memset(pInstance->pVersion, 0, temp_mem_size);
  pBase += temp_mem_size;

  return result;

}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_kpps(LVVEFQ_Tx_Instance_st *pInstance,
                              uint32_t topology_id,
                              uint32_t *kpps_ptr)
{
  ADSPResult result = ADSP_EOK;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_get_kpps");
#endif

  if((NULL != pInstance) && (NULL!= kpps_ptr))
  {
    pInstance->kpps = 0;
    if(pInstance->enable)
    {
      if(topology_id == VOICE_TOPOLOGY_LVVEFQ_TX_DM)
      {
        if(pInstance->InstanceParams.SampleRate == LVM_FS_48000)
          pInstance->kpps += VOICE_LVVEFQ_DM_FB_KPPS;
        else if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
          pInstance->kpps += VOICE_LVVEFQ_DM_WB_KPPS;
        else
          pInstance->kpps += VOICE_LVVEFQ_DM_NB_KPPS;
      }
      else if(topology_id == VOICE_TOPOLOGY_LVVEFQ_TX_TM)
      {
        if(pInstance->InstanceParams.SampleRate == LVM_FS_48000)
          pInstance->kpps += VOICE_LVVEFQ_TM_FB_KPPS;
        else if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
          pInstance->kpps += VOICE_LVVEFQ_TM_WB_KPPS;
        else
          pInstance->kpps += VOICE_LVVEFQ_TM_NB_KPPS;
      }
      else
      {
        if(pInstance->InstanceParams.SampleRate == LVM_FS_48000)
          pInstance->kpps += VOICE_LVVEFQ_SM_FB_KPPS;
        else if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
          pInstance->kpps += VOICE_LVVEFQ_SM_WB_KPPS;
        else
          pInstance->kpps += VOICE_LVVEFQ_SM_NB_KPPS;
      }
    }
    *kpps_ptr = pInstance->kpps;
  }
  else
  {
    result = ADSP_EBADPARAM;
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_kpps: Null pointers");
  }
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_delay(LVVEFQ_Tx_Instance_st *pInstance,
                              uint32_t topology_id,
                              uint32_t *delay_ptr)
{
  ADSPResult result = ADSP_EOK;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_get_delay");
#endif

  if((NULL != pInstance) && (NULL!= delay_ptr))
  {
    *delay_ptr = 0;
  }
  else
  {
    result = ADSP_EBADPARAM;
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_delay: Null pointers");
  }
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_init(LVVEFQ_Tx_Instance_st *pInstance,
                          uint16_t ChannelCount)
{
  ADSPResult result = ADSP_EOK;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_init");
#endif

  pInstance->enable = 0;
  pInstance->hInstance = LVM_NULL;

  /* instance handle */
  LVVE_Status = LVVE_Tx_GetInstanceHandle(&(pInstance->hInstance), &(pInstance->MemTab), &(pInstance->InstanceParams));
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_init: LVVE_Tx_GetInstanceHandle returned %d", (int32_t)LVVE_Status);
    return ADSP_EFAILED;
  }

  /* channel count */
  pInstance->ChannelCount = ChannelCount;

  /* input */
  LVVE_Status = LVVE_Tx_SetInputChannelCount(pInstance->hInstance, pInstance->ChannelCount);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_init: LVVE_Tx_SetInputChannelCount returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* output */
  LVVE_Status = LVVE_Tx_SetOutputChannelCount(pInstance->hInstance, LVVE_TX_OUTPUT_CHANNEL_COUNT_MAX);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_init: LVVE_Tx_SetOutputChannelCount returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* reference */
  LVVE_Status = LVVE_Tx_SetReferenceChannelCount(pInstance->hInstance, LVVE_TX_REFERENCE_CHANNEL_COUNT_MAX);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_init: LVVE_Tx_SetReferenceChannelCount returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* eq */
  pInstance->ControlParams->EQ_ControlParams.pEQ_Coefs = pInstance->pEQ_Coefs;

  /* set control parameters */
  LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_init: LVVE_Tx_SetControlParameters returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* initialize rx peer pointer */
  pInstance->m_pRxPeer = NULL;

  /* version info */
  LVVE_Status = LVVE_GetVersionInfo(pInstance->pVersion);
  if(LVVE_Status != LVVE_SUCCESS)
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Tx_init: LVVE_GetVersionInfo returned %d", (int32_t)LVVE_Status);
  else
  {
    MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"LVVEFQ_Tx_init: LVVE Version is %d.%d.%d",
          (int32_t)pInstance->pVersion->VersionNumber[5] - 48,
          ((int32_t)pInstance->pVersion->VersionNumber[7] - 48)*10+((int32_t)pInstance->pVersion->VersionNumber[8] - 48),
          ((int32_t)pInstance->pVersion->VersionNumber[10] - 48)*10+((int32_t)pInstance->pVersion->VersionNumber[11] - 48));
  }

  return result;

}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Tx_end(LVVEFQ_Tx_Instance_st *pInstance)
{
  int32_t i;
#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_end");
#endif

  pInstance->enable = 0;
  pInstance->kpps = 0;

  /* control parameter */
  if(pInstance->ControlParams != LVM_NULL)
  {
    pInstance->ControlParams = LVM_NULL;
  }

  /* memory table */
  for(i = 0; i< LVM_NR_MEMORY_REGIONS; i++)
  {
    pInstance->MemTab.Region[i].Size = 0;
    if(pInstance->MemTab.Region[i].pBaseAddress != LVM_NULL)
    {
      pInstance->MemTab.Region[i].pBaseAddress = LVM_NULL;
    }
  }

  /* eq */
  if(pInstance->pEQ_Coefs != LVM_NULL)
  {
    pInstance->pEQ_Coefs = LVM_NULL;
  }

  /* initialize rx peer pointer */
  if(pInstance->m_pRxPeer != NULL)
    pInstance->m_pRxPeer = NULL;

  if(pInstance->LVVEFQ_LVAVC_Message.message_ptr != NULL)
    pInstance->LVVEFQ_LVAVC_Message.message_ptr = NULL;

  /* version info */
  if(pInstance->pVersion != LVM_NULL)
    pInstance->pVersion = LVM_NULL;


}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_process(LVVEFQ_Tx_Instance_st *pInstance,
                             short **InBuffer16_Mic,
                             short *IOBuffer16_Ref,
                             short *OutBuffer16_Mic,
                             uint32_t ProcessBlockSize)
{
  ADSPResult result = ADSP_EOK;
  LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;

  if(ProcessBlockSize % 80 == 0)
  {
    LVM_PCM_Buffer_st MicrophoneBuffer;
    LVM_PCM_Channel_t MicrophoneChannels[LVVE_TX_INPUT_CHANNEL_COUNT_MAX];
    LVM_PCM_Buffer_st PreProcessedBuffer;
    LVM_PCM_Channel_t PreprocessedChannels[LVVE_TX_OUTPUT_CHANNEL_COUNT_MAX];
    LVM_PCM_Buffer_st PlaybackBuffer;
    LVM_PCM_Channel_t PlaybackChannels[LVVE_TX_REFERENCE_CHANNEL_COUNT_MAX];

    MicrophoneBuffer.channels = MicrophoneChannels;
    PreProcessedBuffer.channels = PreprocessedChannels;
    PlaybackBuffer.channels = PlaybackChannels;

    /* Select the required buffers */
    for(int32_t i = 0; i < LVVE_TX_INPUT_CHANNEL_COUNT_MAX; i++)
    {
      MicrophoneBuffer.channels[i] = (LVM_INT16 *)InBuffer16_Mic[i];
    }
    MicrophoneBuffer.FrameCount = ProcessBlockSize;
    PlaybackBuffer.channels[0] = IOBuffer16_Ref;
    PlaybackBuffer.FrameCount = ProcessBlockSize;
    PreProcessedBuffer.channels[0] = (LVM_INT16 *)OutBuffer16_Mic;

    LVVE_Status = LVVE_Tx_PushReference(pInstance->hInstance, /* Instance handle */
                                        &PlaybackBuffer);     /* Input buffer with echo reference signal (played back on speaker) */
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_process: LVVE_Tx_PushReference returned %d", (int32_t)LVVE_Status);
      result = ADSP_EFAILED;
    }

    LVVE_Status = LVVE_Tx_Process(pInstance->hInstance, /* Instance handle */
                                  &MicrophoneBuffer,    /* Input buffer with data from all microphones */
                                  &PreProcessedBuffer); /* Output buffer with preprocessed data */
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_process: LVVE_Tx_Process returned %d", (int32_t)LVVE_Status);
      result = ADSP_EFAILED;
    }

    LVVE_Status = LVVE_Tx_PublishTx2RxMessage(pInstance->hInstance,
                                              (LVVE_Tx2RxMessage_Handle_t)pInstance->LVVEFQ_LVAVC_Message.message_ptr,
                                              &(pInstance->LVVEFQ_LVAVC_Message.message_size));
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_process: LVVE_Tx_PublishTx2RxMessage returned %d", (int32_t)LVVE_Status);
      result = ADSP_EFAILED;
    }

    /* send noise estimate to Rx */
    if(pInstance->m_pRxPeer != NULL)
      LVVEFQ_Tx_send_message(pInstance, VOICE_PARAM_LVVEFQ_MESSAGE);

  }
  else // ProcessBlockSize % 80 != 0
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_process: Invalid block size = %d", ProcessBlockSize);
    result = ADSP_EFAILED;
  }

  return result;

}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_set_param(LVVEFQ_Tx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t param_size)
{
  ADSPResult result = ADSP_EOK;
  uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_set_param: parameter ID = 0x%08x, parameter size = %d", param_id, param_size);
#endif

  /* ENABLE */
  if(param_id == VOICE_PARAM_MOD_ENABLE)
  {
    if(param_size > LVVEFQ_TX_ENABLE_PARAM_SIZE) // 4byte
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for ENABLE: received = %d, required size = %d", param_size, LVVEFQ_TX_ENABLE_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    pInstance->enable = *(param_ptr+0);
  }

  /* CONTROL */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_CONTROL)
  {
    if(param_size > LVVEFQ_TX_CONTROL_PARAM_SIZE) // 20byte
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for CONTROL: received = %d, required size = %d", param_size, VOICE_PARAM_LVVEFQ_TX_CONTROL);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    memcpy((uint16_t *)&(pInstance->ControlParams->MicrophoneRouting), param_ptr, LVVEFQ_TX_CONTROL_PARAM_SIZE);

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for CONTROL returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* VOL */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_VOL)
  {
    if(param_size > LVVEFQ_TX_VOL_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for VOL: received = %d, required size = %d", param_size, LVVEFQ_TX_VOL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    pInstance->ControlParams->VOL_OperatingMode= (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->VOL_Gain = (LVM_INT16)*(param_ptr+2);

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for VOL returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* HPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_HPF)
  {
    if(param_size > LVVEFQ_TX_HPF_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for HPF: received = %d, required size = %d", param_size, LVVEFQ_TX_HPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    pInstance->ControlParams->HPF_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->MIC_HPF_CornerFreq = (LVM_UINT16)*(param_ptr+2);

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for HPF returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LPF)
  {
    if(param_size > LVVEFQ_TX_LPF_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for LPF: received = %d, required size = %d", param_size, LVVEFQ_TX_LPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    pInstance->ControlParams->LPF_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->MIC_LPF_CornerFreq = (LVM_UINT16)*(param_ptr+2);

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for LPF returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* HF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVHF)
  {
    if(param_size > sizeof(LVHF_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for LVHF: received = %d, required size = %d", param_size, sizeof(LVHF_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    memcpy((uint16_t *)&(pInstance->ControlParams->HF_ControlParams.OperatingMode), param_ptr, sizeof(LVHF_ControlParams_st));

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for LVHF returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }
  /* LVNV */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVNV)
  {
    if(param_size > sizeof(LVNV_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for LVNV: received = %d, required size = %d", param_size, sizeof(LVNV_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    memcpy(&(pInstance->ControlParams->NV_ControlParams), (LVNV_ControlParams_st *)param_ptr, sizeof(LVNV_ControlParams_st));

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for LVNV returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWM */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVWM)
  {
    if(param_size > sizeof(LVWM_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for LVWM: received = %d, required size = %d", param_size, sizeof(LVWM_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    memcpy((uint16_t *)&(pInstance->ControlParams->WM_ControlParams.OperatingMode), param_ptr, sizeof(LVWM_ControlParams_st));

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for LVWM returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVEQ */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVEQ)
  {
    if(param_size > LVVEFQ_TX_EQ_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for EQ: received = %d, required size = %d", param_size, LVVEFQ_TX_EQ_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    pInstance->ControlParams->EQ_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->EQ_ControlParams.EQ_Length = (LVM_UINT16)*(param_ptr+2);
    memcpy((uint16_t *)pInstance->pEQ_Coefs, param_ptr+4, LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

    /* override eq length in case of invalid eq length */
    if((pInstance->ControlParams->EQ_OperatingMode == LVM_MODE_ON) && (pInstance->ControlParams->EQ_ControlParams.EQ_Length == 0))
    {
      pInstance->ControlParams->EQ_ControlParams.EQ_Length = LVEQ_EQ_LENGTH_MIN;
    }

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for EQ returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVDRC */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVDRC)
  {
    if(param_size > LVVEFQ_TX_DRC_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for DRC: received = %d, required size = %d", param_size, LVVEFQ_TX_DRC_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    pInstance->ControlParams->DRC_ControlParams.OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->DRC_ControlParams.NumKnees = (LVM_UINT16)*(param_ptr+2);
    memcpy(&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveInputLevels[0]), (LVM_INT16 *)(param_ptr+4), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    memcpy(&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveOutputLevels[0]), (LVM_INT16 *)(param_ptr+10), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    pInstance->ControlParams->DRC_ControlParams.AttackTime = (LVM_INT16)*(param_ptr+15);
    pInstance->ControlParams->DRC_ControlParams.ReleaseTime = (LVM_INT16)*(param_ptr+16);
    pInstance->ControlParams->DRC_ControlParams.LimiterOperatingMode = (LVM_Mode_en)*(param_ptr+18);
    pInstance->ControlParams->DRC_ControlParams.LimitLevel = (LVM_INT16)*(param_ptr+20);

    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for DRC returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVCNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVCNG)
  {
    if(param_size > LVVEFQ_TX_CNG_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter size for CNG: received = %d, required size = %d", param_size, LVVEFQ_TX_CNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->CNG_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    memcpy(&(pInstance->ControlParams->CNG_ControlParams), (LVCNG_ControlParams_st *)(param_ptr+2), sizeof(LVCNG_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: LVVE_Tx_SetControlParameters error for CNG returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* ERROR */
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_set_param: Invalid parameter = 0x%08x", param_id);
    result = ADSP_EBADPARAM;
  }

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_get_param(LVVEFQ_Tx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t buf_size,
                               uint32_t *param_size_ptr)
{
  ADSPResult result = ADSP_EOK;
  uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;


  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_get_param");


  /* ENABLE */
  if(param_id == VOICE_PARAM_MOD_ENABLE)
  {
    if(LVVEFQ_TX_ENABLE_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = pInstance->enable;
      *(param_ptr+1) = 0;
      *param_size_ptr = LVVEFQ_TX_ENABLE_PARAM_SIZE;
    }
    else
    {
        MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for ENABLE: received = %d, required size = %d", buf_size, LVVEFQ_TX_ENABLE_PARAM_SIZE);
        result = ADSP_EBADPARAM;
    }
  }

  /* CONTROL */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_CONTROL)
  {
    if(LVVEFQ_TX_CONTROL_PARAM_SIZE <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)pInstance->ControlParams, LVVEFQ_TX_CONTROL_PARAM_SIZE);
      *param_size_ptr = LVVEFQ_TX_CONTROL_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for CONTROL: received = %d, required size = %d", buf_size, LVVEFQ_TX_CONTROL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* VOL */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_VOL)
  {
    if(LVVEFQ_TX_VOL_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->VOL_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->VOL_Gain;
      *(param_ptr+3) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_TX_VOL_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for VOL: received = %d, required size = %d", buf_size, LVVEFQ_TX_VOL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* HPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_HPF)
  {
    if(LVVEFQ_TX_HPF_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->HPF_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->MIC_HPF_CornerFreq;
      *(param_ptr+3) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_TX_HPF_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for HPF: received = %d, required size = %d", buf_size, LVVEFQ_TX_HPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LPF)
  {
    if(LVVEFQ_TX_LPF_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->LPF_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->MIC_LPF_CornerFreq;
      *(param_ptr+3) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_TX_LPF_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for LPF: received = %d, required size = %d", buf_size, LVVEFQ_TX_LPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* HF */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVHF)
  {
    if(sizeof(LVHF_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->HF_ControlParams), sizeof(LVHF_ControlParams_st));
      *param_size_ptr = sizeof(LVHF_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for LVHF: received = %d, required size = %d", buf_size, sizeof(LVHF_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }
  /* LVNV */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVNV)
  {
    if(sizeof(LVNV_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->NV_ControlParams), sizeof(LVNV_ControlParams_st));
      *param_size_ptr = sizeof(LVNV_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for LVNV: received = %d, required size = %d", buf_size, sizeof(LVNV_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWM */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVWM)
  {
    if(sizeof(LVWM_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->WM_ControlParams), sizeof(LVWM_ControlParams_st));
      *param_size_ptr = sizeof(LVWM_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for LVWM: received = %d, required size = %d", buf_size, sizeof(LVWM_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* LVEQ */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVEQ)
  {
    if(LVVEFQ_TX_EQ_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->EQ_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->EQ_ControlParams.EQ_Length;
      *(param_ptr+3) = 0; // RESERVED
      memcpy(param_ptr+4, (uint16_t *)pInstance->pEQ_Coefs, LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
      *param_size_ptr = LVVEFQ_TX_EQ_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for EQ: received = %d, required size = %d", buf_size, LVVEFQ_TX_EQ_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVDRC */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVDRC)
  {
    if(LVVEFQ_TX_DRC_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.NumKnees;
      *(param_ptr+3) = 0; // RESERVED
      memcpy(param_ptr+4, (uint16_t *)&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveInputLevels[0]), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+9) = 0; // RESERVED
      memcpy(param_ptr+10, (uint16_t *)&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveOutputLevels[0]), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+15) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.AttackTime;
      *(param_ptr+16) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.ReleaseTime;
      *(param_ptr+17) = 0; // RESERVED
      *(param_ptr+18) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.LimiterOperatingMode;
      *(param_ptr+19) = 0; // RESERVED
      *(param_ptr+20) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.LimitLevel;
      *(param_ptr+21) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_TX_DRC_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for DRC: received = %d, required size = %d", buf_size, LVVEFQ_TX_DRC_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVCNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_TX_LVCNG)
  {
    if(LVVEFQ_TX_CNG_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->CNG_OperatingMode;
      memcpy(param_ptr+2, (uint16_t *)&(pInstance->ControlParams->CNG_ControlParams.CNG_Volume), sizeof(LVCNG_ControlParams_st));
      *param_size_ptr = LVVEFQ_TX_CNG_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Insufficient buffer length for CNG: received = %d, required size = %d", buf_size, LVVEFQ_TX_CNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* ERROR */
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_get_param: Invalid parameter = 0x%08x", param_id);
    result = ADSP_EBADPARAM;
  }

MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] TJLVVEFQ_Get_param: parameter ID = 0x%08x, parameter size = %d", param_id, *param_size_ptr);
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_connect_rx_peer(LVVEFQ_Tx_Instance_st *pInstance, void *pRxPeer, uint32_t SvcID)
{
#ifdef LVVE_DEBUG
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_connect_rx_peer");
#endif // LVVE_DEBUG

  pInstance->m_pRxPeer = NULL;
  if(NULL != pRxPeer)
  {
    if(SvcID == ELITE_VOICEPROCRX_SVCID)
      pInstance->m_pRxPeer = (elite_svc_handle_t *)pRxPeer ;
    else
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_connect_rx_peer: Invalid service ID %d", SvcID);
      return ADSP_EBADPARAM;
    }
  }

  return ADSP_EOK;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Tx_disconnect_rx_peer(LVVEFQ_Tx_Instance_st *pInstance, uint32_t SvcID)
{
#ifdef LVVE_DEBUG
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_disconnect_rx_peer");
#endif // LVVE_DEBUG

  if(SvcID == ELITE_VOICEPROCRX_SVCID)
    pInstance->m_pRxPeer = NULL;
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_disconnect_rx_peer: Invalid service ID %d", SvcID);
    return ADSP_EBADPARAM;
  }

  return ADSP_EOK;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
static ADSPResult LVVEFQ_Tx_send_message(LVVEFQ_Tx_Instance_st *pInstance, uint32_t paramID)
{
  ADSPResult result = ADSP_EOK;
  uint32_t nSize = 0;
  elite_msg_any_t sMsg;
  elite_msg_param_cal_t *pPayload;

#ifdef LVVE_DEBUG
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Tx_send_message");
#endif

  nSize = sizeof(elite_msg_param_cal_t);
  result = elite_msg_create_msg(&sMsg, &nSize, ELITE_CMD_SET_PARAM, NULL, NULL, ADSP_EOK);
  if(ADSP_FAILED(result))
  {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_send_message: FAILED to create elite_msg");
    return result;
  }

  pPayload = (elite_msg_param_cal_t *)sMsg.pPayload;
  pPayload->unParamId = VOICE_PARAM_LVVEFQ_MESSAGE;
  pPayload->pnParamData = (int32_t *)&(pInstance->LVVEFQ_LVAVC_Message);

  result = qurt_elite_queue_push_back(pInstance->m_pRxPeer->cmdQ, (uint64_t *)&sMsg);
  if(ADSP_FAILED(result))
  {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Tx_send_message: Failed to send elite_msg");
    return result;
  }

  return ADSP_EOK;
}

#if 0//!defined(TARGET_BUILD) && defined (LVVE_DEBUG)
void LVVEFQ_Tx_PrintParams(LVVEFQ_Tx_Instance_st *pInstance)
{
  LVVE_Tx_ControlParams_st *ControlParams_Tx = pInstance->ControlParams;
  //int32_t i;

  FARF(HIGH, "ControlParams_Tx->MicrophoneRouting	                                   %d,          ",                        ControlParams_Tx->MicrophoneRouting	                                );
  FARF(HIGH, "ControlParams_Tx->OperatingMode                                          %d,          ",                        ControlParams_Tx->OperatingMode                                     );  
  FARF(HIGH, "ControlParams_Tx->Mute	                                               %d,          ",                        ControlParams_Tx->Mute	                                            );        
  FARF(HIGH, "ControlParams_Tx->BD_OperatingMode	                                   %d,          ",                        ControlParams_Tx->BD_OperatingMode	                                );
  FARF(HIGH, "ControlParams_Tx->BulkDelay                                              %d,          ",                        ControlParams_Tx->BulkDelay                                         );  
  FARF(HIGH, "ControlParams_Tx->BD_Gain	                                               %d,          ",                        ControlParams_Tx->BD_Gain	                                        );
  FARF(HIGH, "ControlParams_Tx->VOL_OperatingMode	                                   %d,          ",                        ControlParams_Tx->VOL_OperatingMode	                                );
  FARF(HIGH, "ControlParams_Tx->VOL_Gain	                                           %d,          ",                        ControlParams_Tx->VOL_Gain	                                        );
  FARF(HIGH, "ControlParams_Tx->HPF_OperatingMode	                                   %d,          ",                        ControlParams_Tx->HPF_OperatingMode	                                );
  FARF(HIGH, "ControlParams_Tx->MIC_HPF_CornerFreq		                               %d,          ",                        ControlParams_Tx->MIC_HPF_CornerFreq		                        );        
  FARF(HIGH, "ControlParams_Tx->LPF_OperatingMode	                                   %d,          ",                        ControlParams_Tx->LPF_OperatingMode	                                );
  FARF(HIGH, "ControlParams_Tx->MIC_LPF_CornerFreq		                               %d,          ",                        ControlParams_Tx->MIC_LPF_CornerFreq		                        );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.OperatingMode                         %d,          ",                        ControlParams_Tx->HF_ControlParams.OperatingMode                    );  
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.Mode	                               %d,          ",                        ControlParams_Tx->HF_ControlParams.Mode	                            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.TuningMode	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.TuningMode	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.InputGain	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.InputGain	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.OutputGain	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.OutputGain	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_limit	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_limit	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_LB_taps	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_LB_taps	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_LB_two_alpha	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_LB_two_alpha	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_LB_erl	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_LB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_HB_taps	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_HB_taps	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_HB_two_alpha	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_HB_two_alpha	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_HB_erl	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_HB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_preset_coefs                     %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_preset_coefs                );  
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.NLMS_offset	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.NLMS_offset	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_LF	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_LF	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_LF	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_LF	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_LF	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_LF	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_echo_cutoff_LF	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_echo_cutoff_LF	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_tail_alpha_LB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_tail_alpha_LB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_tail_portion_LB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_tail_portion_LB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_LB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_LB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_LB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_LB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_LB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_LB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_NL_atten_LB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_NL_atten_LB	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_tail_alpha_HB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_tail_alpha_HB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_tail_portion_HB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_tail_portion_HB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_HB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_high_HB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_HB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_dt_HB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_HB	               %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_low_HB	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_NL_atten_HB	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_NL_atten_HB	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_echo_relax_NE_threshold_LF       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_echo_relax_NE_threshold_LF  );  
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_echo_relax_NE_threshold	       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_echo_relax_NE_threshold	    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_e_alpha	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_e_alpha	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_gamma_n	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_gamma_n	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_spdet_near	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_spdet_near	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_spdet_act	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_spdet_act	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_limit_ns	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_limit_ns	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_CNI_Gain	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_CNI_Gain	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_NFE_blocksize	                   %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_NFE_blocksize	            );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.DENS_echo_clipping_level              %d,          ",                        ControlParams_Tx->HF_ControlParams.DENS_echo_clipping_level         );  
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.SPDET_far	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.SPDET_far	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.SPDET_mic	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.SPDET_mic	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.SPDET_x_clip	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.SPDET_x_clip	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_threshold	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_threshold	                );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_taps	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_taps	                        );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_erl	                           %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_erl	                        );
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_minimum_erl                       %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_minimum_erl                  );  
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_erl_step	                       %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_erl_step	                    );        
  FARF(HIGH, "ControlParams_Tx->HF_ControlParams.PCD_gamma_e_rescue                    %d,          ",                        ControlParams_Tx->HF_ControlParams.PCD_gamma_e_rescue               );  
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.OperatingMode	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.OperatingMode	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Config	                               %d,          ",                        ControlParams_Tx->NV_ControlParams.Config	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Mode	                               %d,          ",                        ControlParams_Tx->NV_ControlParams.Mode	                            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Tuning_mode	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.Tuning_mode	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.MicDistance	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.MicDistance	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Input_Gain_Mic0	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.Input_Gain_Mic0	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Input_Gain_Mic1	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.Input_Gain_Mic1	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.Output_Gain	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.Output_Gain	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_LB_taps	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_LB_taps	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_LB_twoalpha	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_LB_twoalpha	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_LB_erl	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_LB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_HB_taps	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_HB_taps	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_HB_twoalpha	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_HB_twoalpha	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_HB_erl	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_HB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_preset_coefs	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_preset_coefs	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS0_offset	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS0_offset	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_LB_taps	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_LB_taps	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_LB_twoalpha	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_LB_twoalpha	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_LB_erl	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_LB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_HB_taps	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_HB_taps	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_HB_twoalpha	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_HB_twoalpha	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_HB_erl	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_HB_erl	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_preset_coefs	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_preset_coefs	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.NLMS1_offset	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.NLMS1_offset	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.MicGainComp	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.MicGainComp	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_Taps	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_Taps	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_Erl	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_Erl	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_Threshold	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_Threshold	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_MinimumErl	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_MinimumErl	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_ErlStep	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_ErlStep	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.PCD_GammaERescue	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.PCD_GammaERescue	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.SPDET_far	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.SPDET_far	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.CAL_micPowFloorMin	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.CAL_micPowFloorMin	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.IMCOH_broadside_level_scale	       %d,          ",                        ControlParams_Tx->NV_ControlParams.IMCOH_broadside_level_scale	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.SDET_PosChange_Sensitivity	           %d,          ",                        ControlParams_Tx->NV_ControlParams.SDET_PosChange_Sensitivity	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.SDET_PosChange_ProtectPeriod	       %d,          ",                        ControlParams_Tx->NV_ControlParams.SDET_PosChange_ProtectPeriod	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[0]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[0]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[1]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[1]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[2]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[2]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[3]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[3]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[4]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[4]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[5]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[5]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[6]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[6]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table0[7]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table0[7]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[0]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[0]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[1]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[1]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[2]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[2]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[3]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[3]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[4]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[4]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[5]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[5]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[6]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[6]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_init_table1[7]	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_init_table1[7]	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.FSB_taps	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.FSB_taps	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.GSC_taps	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.GSC_taps	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.GSC_erl	                           %d,          ",                        ControlParams_Tx->NV_ControlParams.GSC_erl	                        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_LF	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_LF	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_LF	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_LF	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_LF	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_LF	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoCutoff_LF	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoCutoff_LF	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_LF	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_LF	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_LB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_LB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_LB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_LB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_LB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_LB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoAlphaRev_LB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoAlphaRev_LB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoTailPortion_LB	           %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoTailPortion_LB	        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_LB	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_LB	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_HB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaHi_HB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_HB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaLo_HB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_HB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoGammaDt_HB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoAlphaRev_HB	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoAlphaRev_HB	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoTailPortion_HB               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoTailPortion_HB          );  
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_HB	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_NlAtten_HB	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoRelax_NEThreshold_LF         %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoRelax_NEThreshold_LF    );  
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoRelax_NEThreshold	           %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoRelax_NEThreshold	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_CNILevel	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_CNILevel	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_MusicalTone_Sensitivity	       %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_MusicalTone_Sensitivity	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_GainEta	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_GainEta	                    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_SPDET_near	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_SPDET_near	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_AcThreshold	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_AcThreshold	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_Gamma_nn_max	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_Gamma_nn_max	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_lo	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_lo	        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_hi	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_hi	        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_highband	       %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_Constrained_nn_highband	    );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_FE_SPPB_SNR_min	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_FE_SPPB_SNR_min	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_SNR_min	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_SNR_min	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_SNR_max	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_SNR_max	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_highSNR	               %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_highSNR	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_lowSNR	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_lowSNR	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_max	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_max	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_rel	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_UGF_MG_rel	                );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_GAINDET_Sensitivity	           %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_GAINDET_Sensitivity	        );
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_NGS_Smoothing	                   %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_NGS_Smoothing	            );        
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.DNNS_EchoClippingLevel                %d,          ",                        ControlParams_Tx->NV_ControlParams.DNNS_EchoClippingLevel           );  
  FARF(HIGH, "ControlParams_Tx->NV_ControlParams.SpeechRecovery	                       %d,          ",                        ControlParams_Tx->NV_ControlParams.SpeechRecovery	                );
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.OperatingMode	                       %d,          ",                        ControlParams_Tx->WM_ControlParams.OperatingMode	                );
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.mode	                               %d,          ",                        ControlParams_Tx->WM_ControlParams.mode	                            );        
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_Target_level_lin	               %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_Target_level_lin	            );        
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_MinGainLin	                       %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_MinGainLin	                );
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_MaxGainLin	                       %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_MaxGainLin	                );
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_Attack	                           %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_Attack	                    );        
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_Release	                       %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_Release	                    );        
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.AVL_Limit_MaxOutputLin	               %d,          ",                        ControlParams_Tx->WM_ControlParams.AVL_Limit_MaxOutputLin	        );
  FARF(HIGH, "ControlParams_Tx->WM_ControlParams.SpDetect_Threshold	                   %d,          ",                        ControlParams_Tx->WM_ControlParams.SpDetect_Threshold	            );        
  FARF(HIGH, "ControlParams_Tx->EQ_OperatingMode                                       %d,          ",                        ControlParams_Tx->EQ_OperatingMode                                  );  
  FARF(HIGH, "ControlParams_Tx->EQ_ControlParams.EQ_Length	                           %d,          ",                        ControlParams_Tx->EQ_ControlParams.EQ_Length	                    );        

  //for(i = 0; i < 64; i++)
  //FARF(HIGH, "ControlParams_Tx->EQ_ControlParams.pEQ_Coefs[%d]            = %d",                                    i, ControlParams_Tx->EQ_ControlParams.pEQ_Coefs[i]                  );

  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.OperatingMode	                       %d,          ",                        ControlParams_Tx->DRC_ControlParams.OperatingMode	                );
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.NumKnees	                           %d,          ",                        ControlParams_Tx->DRC_ControlParams.NumKnees	                    );        
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[0]	       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[0]	);
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[1]	       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[1]	);
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[2]	       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[2]	);
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[3]	       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[3]	);
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[4]	       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveInputLevels[4]	);
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[0]       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[0]  );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[1]       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[1]  );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[2]       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[2]  );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[3]       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[3]  );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[4]       %d,          ",                        ControlParams_Tx->DRC_ControlParams.CompressorCurveOutputLevels[4]  );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.AttackTime	                       %d,          ",                        ControlParams_Tx->DRC_ControlParams.AttackTime	                    );        
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.ReleaseTime	                       %d,          ",                        ControlParams_Tx->DRC_ControlParams.ReleaseTime	                    );        
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.LimiterOperatingMode                 %d,          ",                        ControlParams_Tx->DRC_ControlParams.LimiterOperatingMode            );  
  FARF(HIGH, "ControlParams_Tx->DRC_ControlParams.LimitLevel	                       %d,          ",                        ControlParams_Tx->DRC_ControlParams.LimitLevel	                    );    

}


void LVVEFQ_Tx_SetDefaultParams(LVVEFQ_Tx_Instance_st *pInstance)
{
  LVVE_Tx_ControlParams_st *ControlParams_Tx = pInstance->ControlParams;
  LVVE_ReturnStatus_en LVVE_Status;

  ControlParams_Tx->MicrophoneRouting                                 = LVVE_MICROUTING_MODE_DEFAULT;
  ControlParams_Tx->OperatingMode                                     = LVVE_TX_MODE_ON;
  ControlParams_Tx->Mute                                              = LVM_MODE_OFF;
  ControlParams_Tx->BD_OperatingMode                                  = LVM_MODE_ON;
  if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)  
    ControlParams_Tx->BulkDelay                                       = 66;
  else
    ControlParams_Tx->BulkDelay                                       = 20;
  ControlParams_Tx->BD_Gain                                           = 8192;

  ControlParams_Tx->VOL_OperatingMode                                 = LVM_MODE_ON;
  ControlParams_Tx->VOL_Gain                                          = 0;

  ControlParams_Tx->HPF_OperatingMode                                 = LVM_MODE_OFF;
  ControlParams_Tx->MIC_HPF_CornerFreq                                = 100;

  ControlParams_Tx->LPF_OperatingMode                                 = LVM_MODE_OFF;
  if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
    ControlParams_Tx->MIC_LPF_CornerFreq                              = LVVE_MAX_LPF_TX_CORNER_HZ_WB;
  else
    ControlParams_Tx->MIC_LPF_CornerFreq                              = LVVE_MAX_LPF_TX_CORNER_HZ_NB;

#if (VOICE_MIC_CHANNEL_NUM == 1) 
  memcpy((LVHF_ControlParams_st *)&(ControlParams_Tx->HF_ControlParams),
         (LVHF_ControlParams_st *)&LVHF_ControlParams_Active,
         sizeof(LVHF_ControlParams_st));
#else
  memcpy((LVNV_ControlParams_st *)&(ControlParams_Tx->NV_ControlParams),
         (LVNV_ControlParams_st *)&LVNV_ControlParams_Active,
         sizeof(LVNV_ControlParams_st));
#endif

  memcpy((LVWM_ControlParams_st *)&(ControlParams_Tx->WM_ControlParams),
         (LVWM_ControlParams_st *)&LVWM_ControlParams_Active,
         sizeof(LVWM_ControlParams_st));

  ControlParams_Tx->EQ_OperatingMode                                  = LVM_MODE_OFF;
  if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
    ControlParams_Tx->EQ_ControlParams.EQ_Length                      = LVEQ_EQ_LENGTH_MAX;
  else
    ControlParams_Tx->EQ_ControlParams.EQ_Length                      = LVEQ_EQ_LENGTH_MAX/2;
  ControlParams_Tx->EQ_ControlParams.pEQ_Coefs                        = pInstance->pEQ_Coefs;
  memset(pInstance->pEQ_Coefs, 0, LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
  pInstance->pEQ_Coefs[0]                                             = 4096;

  memcpy((LVDRC_ControlParams_st *)&(ControlParams_Tx->DRC_ControlParams),
         (LVDRC_ControlParams_st *)&LVDRC_ControlParams_Active,
         sizeof(LVDRC_ControlParams_st));

  LVVE_Status = LVVE_Tx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
  if(LVVE_Status != LVVE_SUCCESS)
  {
	FARF(ERROR, "[ERROR] LVVEFQ_Tx_SetDefaultParams:  error returned %d", (int32_t)LVVE_Status);
  }


}
#endif

#endif // LVVE

