/*========================================================================
  Edit History


  when       who     what, where, why
  --------   ---     -----------------------------------------------------
  06/05/15           Created file.

==========================================================================*/

#ifdef TARGET_BUILD
#include "EliteMsg_Util.h"
#include "voice_capi_utils.h"
#include "adsp_vparams_internal_api.h"
#include "capi_v2_lvvefq.h"
#include "adsp_vparams_api.h"
#else
#include "HAP_farf.h"
#include "capi_v2_utils_props.h"
#include "capi_v2_lvvefq.h"

#include <stdlib.h>
#include <string.h>
#endif
#include "LVVEFQ_Rx.h"
#include "LVVEFQ_pcm_injection.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// TODO: Profile this. Assume 1k for now.
#define CAPI_V2_LVVEFQ_RX_STACK_SIZE  (1024)
#define CAPI_V2_LVVEFQ_RX_MEDIA_FORMAT_SIZE (sizeof(capi_v2_set_get_media_format_t) + \
                                       sizeof(capi_v2_standard_data_format_t))

// Sampling rate
#define NB_SAMPLING_RATE      (8000)
#define WB_SAMPLING_RATE      (16000)
#define FB_SAMPLING_RATE      (48000)

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))
#define MAX( x, y ) ( ((x) > (y)) ? (x) : (y) )

#define MAX_BUF_Q_ELEMENTS 2

typedef struct _capi_v2_callback_event_t
{
  capi_v2_event_id_t id;
  capi_v2_event_info_t info;
} capi_v2_callback_event_t;


typedef struct _capi_v2_lvvefq_rx_t
{
  // CAPI_V2 interface
  const capi_v2_vtbl_t* vtbl_ptr;

  // capi_v2 properties
  int heap_id;
  capi_v2_event_callback_info_t event_callback;
  capi_v2_port_num_info_t ports;

  //Media format
  voice_capi_data_format_struct_t in_fmt;
  voice_capi_data_format_struct_t out_fmt;

  capi_v2_session_identifier_t session_identify;
  LVVEFQ_Rx_Instance_st LVVEFQInstance;

  LVVEFQ_injection_st LVVEFQ_injection_Instance;

  imc_src_dst_info_t   capi_v2_voice_imc_rx_imc_info; //only one instance of the communication is supported. Tx <-> Rx
} capi_v2_lvvefq_rx_t;

static void capi_v2_voice_imc_rx_get_imc_handle(capi_v2_lvvefq_rx_t* me);
static void voice_imc_rx_recieve_buf(capi_v2_lvvefq_rx_t* me);

static bool_t is_supported_media_type(const voice_capi_data_format_struct_t* format_ptr)
{
  if (format_ptr->media_format.format_header.data_format != CAPI_V2_FIXED_POINT) {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Only fixed point data supported. Received %lu",
         format_ptr->media_format.format_header.data_format);
    return FALSE;
  }

  if (format_ptr->data_format.bits_per_sample != 16) {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Only 16 bit data supported. Received %lu",
         format_ptr->data_format.bits_per_sample);
    return FALSE;
  }

  if (format_ptr->data_format.data_interleaving != CAPI_V2_DEINTERLEAVED_UNPACKED) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Interleaved data not supported");
    return FALSE;
  }

  if (!format_ptr->data_format.data_is_signed) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Unsigned data not supported");
    return FALSE;
  }

  if (format_ptr->data_format.num_channels != 1) {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Only 1 channel supported. Received %lu.",
           format_ptr->data_format.num_channels);
    return FALSE;
  }

  return TRUE;
}

static capi_v2_err_t capi_v2_lvvefq_rx_process(capi_v2_t* _pif,
                                       capi_v2_stream_data_t* input[],
                                       capi_v2_stream_data_t* output[])
{
  capi_v2_buf_t *in_buf_ptr, *out_buf_ptr;
  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;

  in_buf_ptr  = input[0]->buf_ptr;
  out_buf_ptr = output[0]->buf_ptr;


  // If the state of RX module is registered, the recieve the buffer sent from TX module
	if(IMC_STATE_REGISTERED == me->capi_v2_voice_imc_rx_imc_info.state)
	{
		voice_imc_rx_recieve_buf(me);
	}

  if (me->LVVEFQ_injection_Instance.in_st.enable) {
    LVVEFQ_injection_process(&(me->LVVEFQ_injection_Instance), (short *)in_buf_ptr->data_ptr, in_buf_ptr->actual_data_len >> 1);
  }

  if (me->LVVEFQInstance.enable) {
    LVVEFQ_Rx_process(&(me->LVVEFQInstance),
                       (short *)in_buf_ptr->data_ptr,
                       (short *)out_buf_ptr->data_ptr,
                       in_buf_ptr->actual_data_len >> 1);
  }
 
  out_buf_ptr->actual_data_len = in_buf_ptr->actual_data_len;

  if (me->LVVEFQ_injection_Instance.out_st.enable) {
    LVVEFQ_injection_process(&(me->LVVEFQ_injection_Instance), (short *)out_buf_ptr->data_ptr, out_buf_ptr->actual_data_len >> 1);
  }

  return CAPI_V2_EOK;
}

static capi_v2_err_t capi_v2_lvvefq_rx_end(capi_v2_t* _pif)
{
   capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;
   me->vtbl_ptr = NULL;
#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] capi_v2_lvvefq_rx_end");
#endif


  	// De register IMC module
	if(IMC_STATE_NONE != me->capi_v2_voice_imc_rx_imc_info.state)
	{
		me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle->vtable->dest_deregister(me->capi_v2_voice_imc_rx_imc_info.dest_handle);
		me->capi_v2_voice_imc_rx_imc_info.state = IMC_STATE_NONE;
	}

	me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle->vtable->b.end(me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle);
	
   LVVEFQ_Rx_end(&(me->LVVEFQInstance));
   return CAPI_V2_EOK;
}

static capi_v2_err_t capi_v2_lvvefq_rx_set_param(capi_v2_t* _pif,
                                         uint32_t param_id,
                                         const capi_v2_port_info_t* port_info_ptr,
                                         capi_v2_buf_t* params_ptr)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;

  //Validate pointers
  if (NULL == _pif || NULL == params_ptr) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: set param received NULL pointer");
    return CAPI_V2_EBADPARAM;
  }

  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: set_param received parameter(0x%x)", param_id);

  if (ADSP_EOK == LVVEFQ_injection_set_param(&(me->LVVEFQ_injection_Instance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->actual_data_len)) {
    result = CAPI_V2_EOK;
  } else if (ADSP_EOK == LVVEFQ_Rx_set_param(&(me->LVVEFQInstance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->actual_data_len)) {
    result = CAPI_V2_EOK;
  } else {
    result = CAPI_V2_EBADPARAM;
  }

  
  if (param_id == VOICE_PARAM_MOD_ENABLE && me->event_callback.event_cb) {
    capi_v2_event_KPPS_t kpps;
    capi_v2_event_algorithmic_delay_t delay;
    capi_v2_port_info_t port_info = {TRUE,FALSE,0};
    uint32_t i = 0;
    
    capi_v2_callback_event_t callback_events[]= {
          { CAPI_V2_EVENT_KPPS,              { port_info, { (int8_t *)&kpps,  sizeof(capi_v2_event_KPPS_t),              sizeof(capi_v2_event_KPPS_t)              } } },
          { CAPI_V2_EVENT_ALGORITHMIC_DELAY, { port_info, { (int8_t *)&delay, sizeof(capi_v2_event_algorithmic_delay_t), sizeof(capi_v2_event_algorithmic_delay_t) } } },
    };
    
    LVVEFQ_Rx_get_kpps(&(me->LVVEFQInstance), &kpps.KPPS);
    LVVEFQ_Rx_get_delay(&(me->LVVEFQInstance), &delay.delay_in_us);

    for (i = 0; i < ARRAY_SIZE(callback_events); i++) {
      (me->event_callback.event_cb)(me->event_callback.event_context, callback_events[i].id, &callback_events[i].info);
    }
  }

  return result;
}

static capi_v2_err_t capi_v2_lvvefq_rx_get_param(capi_v2_t* _pif,
                                         uint32_t param_id,
                                         const capi_v2_port_info_t* port_info_ptr,
                                         capi_v2_buf_t* params_ptr)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;

  //Validate pointers
  if (NULL == _pif || NULL == params_ptr) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: get param received NULL pointer");
    return CAPI_V2_EBADPARAM;
  }

  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: get param id <0x%x> received", param_id);

  if (ADSP_EOK == LVVEFQ_injection_get_param(&(me->LVVEFQ_injection_Instance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->max_data_len, 
                                       (uint16_t *)&(params_ptr->actual_data_len))) {
    result = CAPI_V2_EOK;
  } else if (ADSP_EOK == LVVEFQ_Rx_get_param(&(me->LVVEFQInstance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->max_data_len, 
                                       &(params_ptr->actual_data_len))) {
    result = CAPI_V2_EOK;
  } else {
    result = CAPI_V2_EBADPARAM;
  }

  return result;
}

static capi_v2_err_t capi_v2_lvvefq_rx_set_properties(
  capi_v2_t* _pif,
  capi_v2_proplist_t* props_list)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  uint32_t i;
  capi_v2_prop_t* prop;

  if (NULL == props_list) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: set properties received NULL props pointer");
    return CAPI_V2_EFAILED;
  }

  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;
  prop = props_list->prop_ptr;

  for (i = 0; i < props_list->props_num; i++) {
    capi_v2_property_id_t id = prop->id;
    capi_v2_buf_t payload = prop->payload;

    switch(id) {
      case CAPI_V2_INPUT_MEDIA_FORMAT:
      {
        voice_capi_data_format_struct_t* mfmt_ptr = (voice_capi_data_format_struct_t*)(payload.data_ptr);
        if (mfmt_ptr->media_format.format_header.data_format == CAPI_V2_FIXED_POINT) {
          me->in_fmt = *mfmt_ptr;
          me->out_fmt = me->in_fmt;
          
          MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: media format update, sampling freq %d, num channels %d, bits per sample %d", me->in_fmt.data_format.sampling_rate, me->in_fmt.data_format.num_channels, me->in_fmt.data_format.bits_per_sample);

          if(!is_supported_media_type(mfmt_ptr)) {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Unsupported input media format");
            result |= CAPI_V2_EUNSUPPORTED;
          }
#ifndef TARGET_BUILD // If not called the below routines, process function won't be called in the test framework.
          else {
            if (me->event_callback.event_cb) {

              capi_v2_event_info_t event_info;
              capi_v2_event_process_state_t process_state;
				
              event_info.payload.data_ptr = (int8_t*)&(process_state);
              event_info.payload.actual_data_len = sizeof(capi_v2_event_process_state_t);
              event_info.payload.max_data_len = sizeof(capi_v2_event_process_state_t);
				
              process_state.is_enabled = TRUE;
				
              (me->event_callback.event_cb)(me->event_callback.event_context, CAPI_V2_EVENT_PROCESS_STATE, &event_info);
            }
          }
#endif    
        } else {
          MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: unsupported media data format %d, supports only FIXED_POINT", mfmt_ptr->media_format.format_header.data_format);
          result |= CAPI_V2_EUNSUPPORTED;
        }
        break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT:
      {
        voice_capi_data_format_struct_t* mfmt_ptr = (voice_capi_data_format_struct_t*)(payload.data_ptr);
        if (mfmt_ptr->media_format.format_header.data_format == CAPI_V2_FIXED_POINT) {
          me->out_fmt = *mfmt_ptr;
        } else {
          MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Unsupported output media format %d", mfmt_ptr->media_format.format_header.data_format);
          result |= CAPI_V2_EUNSUPPORTED;
        }
        break;
      }
      case CAPI_V2_ALGORITHMIC_RESET:
      {
        // Nothing to reset for lvvefq module. No buffering. So we are good to go now.
        result |= CAPI_V2_EOK;
        break;
      }
      case CAPI_V2_HEAP_ID:
      {
        capi_v2_heap_id_t* heap_id = (capi_v2_heap_id_t*)(payload.data_ptr);
        me->heap_id = heap_id->heap_id;
        break;
      }
      case CAPI_V2_EVENT_CALLBACK_INFO:
      {
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Recevied event callback pointer");
        capi_v2_event_callback_info_t* event_callback =
           (capi_v2_event_callback_info_t*)payload.data_ptr;
        me->event_callback.event_cb = event_callback->event_cb;
        me->event_callback.event_context = event_callback->event_context;
        break;
      }
      case CAPI_V2_PORT_NUM_INFO:
      {
        capi_v2_port_num_info_t* ports = (capi_v2_port_num_info_t*)(payload.data_ptr);
        me->ports.num_input_ports = ports->num_input_ports;
        me->ports.num_output_ports = ports->num_output_ports;
        MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Number of ports set, input: %d, output: %d", ports->num_input_ports, ports->num_output_ports);

        if (me->ports.num_input_ports != 1) {
          MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Number of input ports are not 2. Supports only number of input ports 2");
        }
        result |= CAPI_V2_EOK;
        break;
      }
	  		 
		case CAPI_V2_SESSION_IDENTIFIER:
		{
		capi_v2_session_identifier_t *session_identify = (capi_v2_session_identifier_t *)payload.data_ptr;
		if( payload.actual_data_len != sizeof(capi_v2_session_identifier_t) )
		{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_RX: Error! Size mismatch for port info property, got %lu", payload.actual_data_len);
		//bail out
		return CAPI_V2_EBADPARAM;
		}

		me->session_identify.service_id = session_identify->service_id;
		me->session_identify.session_id = session_identify->session_id;

		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "TJVOICE_IMC_RX: capi_v2_voice_imc_rx_set_properties(): session_identifies service_id(%lx), session_id(#lx)",
		                                 me->session_identify.service_id,
		                                 me->session_identify.session_id);
		break;
		}
	 

      default:
      {
        MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: lvvefq_rx_set_properties received unknown property 0x%x", id);
        result |= CAPI_V2_EUNSUPPORTED;
        break;
      }
    }
    prop++;
  }

  return result;
}

static capi_v2_err_t capi_v2_lvvefq_rx_get_properties(
  capi_v2_t* _pif,
  capi_v2_proplist_t* props_list)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;
  uint32_t i = 0;
  
  for (i = 0; i < props_list->props_num; i++) {
    capi_v2_property_id_t id = props_list[i].prop_ptr->id;
    capi_v2_buf_t* payload = &props_list[i].prop_ptr->payload;

    switch(id) {
      // CAPI_V2 queryable non-static properties
      case CAPI_V2_PORT_DATA_THRESHOLD:
      {
        payload->actual_data_len = 0;
        result = CAPI_V2_EOK;
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: No buffering required and threshold request not expected");
        break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT_SIZE:
      {
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: get_properties for output media format size");
        if (payload->max_data_len >= sizeof(capi_v2_output_media_format_size_t)) {
          capi_v2_output_media_format_size_t* psize = (capi_v2_output_media_format_size_t*)payload->data_ptr;
          psize->size_in_bytes = sizeof(me->out_fmt) - sizeof(me->out_fmt.media_format.format_header);
          payload->actual_data_len = sizeof(capi_v2_output_media_format_size_t);
        }
        break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT:
      {
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: get_properties for output media format ");
        if (payload->max_data_len >= sizeof(voice_capi_data_format_struct_t)) {
          voice_capi_data_format_struct_t* pfmt = (voice_capi_data_format_struct_t*)payload->data_ptr;
          *pfmt = me->out_fmt;
          payload->actual_data_len = sizeof(voice_capi_data_format_struct_t);
        }
        break;
      }
      case CAPI_V2_METADATA:
      {
        payload->actual_data_len = 0;
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: meadata does not apply");
        break;
      }
      default:
      {
        MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: received request to get unknown/unsupported property 0x%x", id);
        result |= CAPI_V2_EFAILED;
        break;
      }
    }
  }

  return result;
}

static const capi_v2_vtbl_t vtbl =
{
  capi_v2_lvvefq_rx_process,
  capi_v2_lvvefq_rx_end,
  capi_v2_lvvefq_rx_set_param,
  capi_v2_lvvefq_rx_get_param,
  capi_v2_lvvefq_rx_set_properties,
  capi_v2_lvvefq_rx_get_properties,
};

#if 0
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
#endif

static inline uint32_t align_to_8_byte(const uint32_t num)
{
   return ((num + 7) & (0xFFFFFFF8));
}

capi_v2_err_t capi_v2_lvvefq_rx_set_props(void* ctx, capi_v2_property_id_t id,
                                      capi_v2_buf_t* payload)
{
  capi_v2_err_t result = CAPI_V2_EOK;

  switch(id) {
    case CAPI_V2_INIT_MEMORY_REQUIREMENT:
    {
      capi_v2_init_memory_requirement_t* init_mem_req =
        (capi_v2_init_memory_requirement_t*)(payload->data_ptr);
      LVVEFQ_Rx_Instance_st LVVEFQInstance;
      uint32_t sampling_rate[] = {NB_SAMPLING_RATE, WB_SAMPLING_RATE, FB_SAMPLING_RATE};
      uint32_t max_mem_size = 0;
      uint32_t i = 0;

      init_mem_req->size_in_bytes = align_to_8_byte(sizeof(capi_v2_lvvefq_rx_t));

      // capi module don't know the media format for an input port at this point 
      // ( CAPI_V2_INPUT_MEDIA_FORMAT will be updated later )
      // So, request maximum size of memory  
      for (i = 0; i < ARRAY_SIZE(sampling_rate); i++) {
        LVVEFQ_Rx_config(&LVVEFQInstance, sampling_rate[i]); 
        max_mem_size = MAX(max_mem_size, LVVEFQ_Rx_get_size(&LVVEFQInstance));
      }	  
      init_mem_req->size_in_bytes += max_mem_size;

      payload->actual_data_len = sizeof(capi_v2_init_memory_requirement_t);

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: memory size requested = %d", init_mem_req->size_in_bytes);

      break;
    }
    case CAPI_V2_STACK_SIZE:
    {
      capi_v2_stack_size_t* stack_size =
        (capi_v2_stack_size_t*)(payload->data_ptr);
      stack_size->size_in_bytes = CAPI_V2_LVVEFQ_RX_STACK_SIZE;
      payload->actual_data_len = sizeof(capi_v2_stack_size_t);
      break;
    }
    case CAPI_V2_MAX_METADATA_SIZE:
    {
      // This module doesn't support metadata - return
      capi_v2_max_metadata_size_t* meta_size =
        (capi_v2_max_metadata_size_t*)(payload->data_ptr);
      meta_size->output_port_index = 0;
      meta_size->size_in_bytes = 0;
      payload->actual_data_len = sizeof(capi_v2_max_metadata_size_t);
      break;
    }
    case CAPI_V2_IS_INPLACE:
    {
      capi_v2_is_inplace_t* in_place =
        (capi_v2_is_inplace_t*)(payload->data_ptr);
      in_place->is_inplace = 1;
      payload->actual_data_len = sizeof(capi_v2_is_inplace_t);
      break;
    }
    case CAPI_V2_REQUIRES_DATA_BUFFERING:
    {
      capi_v2_requires_data_buffering_t* req_data_buffering =
        (capi_v2_requires_data_buffering_t*)(payload->data_ptr);
      req_data_buffering->requires_data_buffering = 0;
      payload->actual_data_len = sizeof(capi_v2_requires_data_buffering_t);
      break;
    }
    case CAPI_V2_NUM_NEEDED_FRAMEWORK_EXTENSIONS:
    {
      capi_v2_num_needed_framework_extensions_t* num_extns =
        (capi_v2_num_needed_framework_extensions_t*)(payload->data_ptr);
      num_extns->num_extensions = 0;
      break;
    }
    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: unknown static property 0x%08x", id);
      result = CAPI_V2_EFAILED;
      break;
    }
  }
  return result;
}

capi_v2_err_t capi_v2_lvvefq_rx_get_static_properties(
  capi_v2_proplist_t* init_set_properties,
  capi_v2_proplist_t* static_properties)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  uint32_t i = 0;

  if (NULL == static_properties) {
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: static_properties is NULL");
    return CAPI_V2_EFAILED;
  }

  for (i = 0; i < static_properties->props_num; i++) {
    capi_v2_buf_t* payload = &(static_properties->prop_ptr[i].payload);
  
    if (CAPI_V2_FAILED(result = capi_v2_lvvefq_rx_set_props(0, static_properties->prop_ptr[i].id, payload))) {
  	return result;
    }
  }
  return result;

}

capi_v2_err_t capi_v2_lvvefq_rx_init(capi_v2_t* _pif,
                             capi_v2_proplist_t* init_set_properties)
{
  capi_v2_err_t result = CAPI_V2_EOK;
  capi_v2_lvvefq_rx_t* me = (capi_v2_lvvefq_rx_t*)_pif;
  capi_v2_prop_t* props;
  uint32_t i;

  memset((void*)me, 0, sizeof(capi_v2_lvvefq_rx_t));

  // Initialize virtual function pointer table
  me->vtbl_ptr = &vtbl;

  // module is disabled by default
  me->event_callback.event_cb = NULL;
  me->event_callback.event_context = NULL;

  // Assuming default number of ports as 1
  me->ports.num_input_ports = 1;
  me->ports.num_output_ports = 1;

  // validate properties requested are ok to set at initialization
  // We are just printing message if not valid at init time and proceeding further
  props = init_set_properties->prop_ptr;
  for (i = 0; i < init_set_properties->props_num; i++) {
    switch(props->id) {
      case CAPI_V2_INPUT_MEDIA_FORMAT:
      case CAPI_V2_EVENT_CALLBACK_INFO:
      case CAPI_V2_HEAP_ID:
      case CAPI_V2_PORT_NUM_INFO:
        break;
      default:
        MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: This property with id <%d> is not supported at initialization of module", props->id);
        break;
    }
    props++;
  }

  // Setting parameters now
  if (init_set_properties->props_num) {
    result = capi_v2_lvvefq_rx_set_properties(_pif, init_set_properties);
  }

  int8_t* mem_ptr = (int8_t*)_pif;
  mem_ptr += align_to_8_byte(sizeof(capi_v2_lvvefq_rx_t));

  // Call both two functions again with right sampling rate
  LVVEFQ_Rx_config(&(me->LVVEFQInstance), me->in_fmt.data_format.sampling_rate);
  uint32_t mem_size = LVVEFQ_Rx_get_size(&(me->LVVEFQInstance));
  LVVEFQ_injection_config(&(me->LVVEFQ_injection_Instance), me->in_fmt.data_format.sampling_rate);

  LVVEFQ_Rx_set_mem(&(me->LVVEFQInstance), mem_ptr, mem_size);
  LVVEFQ_Rx_init(&(me->LVVEFQInstance));
  LVVEFQ_injection_init(&(me->LVVEFQ_injection_Instance));

  // Get the handle of IMC module. This also fetches the session identifier
  capi_v2_voice_imc_rx_get_imc_handle(me);

  // If session identifier is received and imc library is queried, register for IMC handle.
	if(NULL != me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle)
	{
		//Key is COMMID | RX module ID | Tx module Id | sessionID
		
		me->capi_v2_voice_imc_rx_imc_info.key[0] = (int8_t)VOICE_MODULE_LVVEFQ_IMC;
		me->capi_v2_voice_imc_rx_imc_info.key[1] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 8);
		me->capi_v2_voice_imc_rx_imc_info.key[2] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 16);
		me->capi_v2_voice_imc_rx_imc_info.key[3] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 24);
		me->capi_v2_voice_imc_rx_imc_info.key[4] = (int8_t)VOICE_MODULE_LVVEFQ_RX;
		me->capi_v2_voice_imc_rx_imc_info.key[5] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 8);
		me->capi_v2_voice_imc_rx_imc_info.key[6] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 16);
		me->capi_v2_voice_imc_rx_imc_info.key[7] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 24);
		me->capi_v2_voice_imc_rx_imc_info.key[8] = (int8_t)VOICE_MODULE_LVVEFQ_TX;
		me->capi_v2_voice_imc_rx_imc_info.key[9] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 8);
		me->capi_v2_voice_imc_rx_imc_info.key[10] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 16);
		me->capi_v2_voice_imc_rx_imc_info.key[11] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 24);
		imc_src_dst_info_t *comm_info_ptr;
		uint32_t param_size,max_payload_size,max_q_length,alloc_buffer;
		imc_src_dst_t src_dest;

		param_size = sizeof(me->capi_v2_voice_imc_rx_imc_info.key);
		comm_info_ptr = (imc_src_dst_info_t*)&me->capi_v2_voice_imc_rx_imc_info;
		src_dest = IMC_DESTINATION;
		max_q_length = alloc_buffer = MAX_BUF_Q_ELEMENTS;
		//Max size of all the payload + param Id
		max_payload_size = 0;

		// Register the soruce RX module
		capi_v2_err_t imc_result;
		imc_result = voice_imc_register_src_dest(comm_info_ptr,(int8_t*)me->capi_v2_voice_imc_rx_imc_info.key, param_size, max_q_length, alloc_buffer,max_payload_size,src_dest);
		if(CAPI_V2_FAILED(imc_result))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VOICE_IMC_RX: diamondvoice_set_param(): failed to register to IMC. result(0x%lx)",imc_result);
			return imc_result;
		}
	}
	else
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_RX: ERROR in fetching IMC handle : IMC lib handle=(%lx) ",
											  me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle);
	}

  return result;
}

/**
* This function is used to get the IMC module handle along with session ID for module
* by using event callback functionality.
*/
static void capi_v2_voice_imc_rx_get_imc_handle(capi_v2_lvvefq_rx_t * me)
{
	capi_v2_err_t result = CAPI_V2_EOK;
	capi_v2_event_get_library_instance_t lib_instance;
	capi_v2_event_info_t  event_info;

	lib_instance.id = ELITE_LIB_GET_IMC;
	lib_instance.ptr = NULL;

	event_info.port_info.is_valid = FALSE;
	event_info.payload.data_ptr = (int8_t *)&lib_instance;
	event_info.payload.actual_data_len =event_info.payload.max_data_len = sizeof(lib_instance);

	result = me->event_callback.event_cb(me->event_callback.event_context, CAPI_V2_EVENT_GET_LIBRARY_INSTANCE,&event_info);
	if(CAPI_V2_FAILED(result))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_RX: get_imc_handle(): failed to get IMC lib handle. result(0x%lx)",result);
		return;
	}
	else
	{
		MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VOICE_IMC_RX: get_imc_handle(): IMC Rx received IMC lib handle");
	}

	me->capi_v2_voice_imc_rx_imc_info.imc_lib_handle = (elite_lib_get_imc_t*)lib_instance.ptr;

	return;
}

/**
* This function is used to recieve the IMC buffer sent from source.
*/

static void voice_imc_rx_recieve_buf(capi_v2_lvvefq_rx_t* me)
{
	capi_v2_err_t imc_result,result=CAPI_V2_EOK;

	//int8_t *payload_ptr = NULL;
	LVVEFQ_LVAVC_Message_st ** buf = NULL ;
	//MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VOICE_IMC_RX: Begin voice_imc_rx_recieve_buf()");
	uint32_t payload_size,payload_max_size;
	bool_t is_queue_empty;
	imc_src_dst_info_t *imc_handle = &me->capi_v2_voice_imc_rx_imc_info;
	do
	{
		// Pop the data pointer for getting data sent from source
		imc_result = imc_handle->imc_lib_handle->vtable->dest_pop_buf(imc_handle->dest_handle,(void **)&buf,&payload_size,&payload_max_size,&is_queue_empty);
		if(is_queue_empty)
		{
			//MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VOICE_IMC_RX: capi_v2_voice_imc_rx_get_stat_prop(): Queue is empty");
			return;
		}
		else if(ADSP_FAILED(imc_result))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VOICE_IMC_RX: voice_imc_rx_recieve_buf(): error in popping imc payload, result(0x%lx)",imc_result);
			return;
		}
		else
		{
			//MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VOICE_IMC_RX: voice_imc_rx_recieve_buf(): IMC RX recieved counter = 0x%X !!",(int32_t)*buf);
			memscpy(&me->LVVEFQInstance.LVVEFQ_LVAVC_Message, sizeof(LVVEFQ_LVAVC_Message_st), *buf, sizeof(LVVEFQ_LVAVC_Message_st));
			//MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVE_IMC: LVVEFQ_LVAVC_Message.message_size = 0x%X !!", me->LVVEFQInstance.LVVEFQ_LVAVC_Message.message_size);
			imc_result = imc_handle->imc_lib_handle->vtable->dest_return_buf(imc_handle->dest_handle,buf,result);
			if(CAPI_V2_FAILED(imc_result))
			{
				MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VOICE_IMC_RX: voice_imc_rx_recieve_buf(): error in pushing imc payload. result(%lx)",imc_result);
				return;
			}
		}
	}while(CAPI_V2_SUCCEEDED(result));

	return;
}

#ifdef __cplusplus
}
#endif //__cplusplus
