/*========================================================================
  Edit History


  when       who     what, where, why
  --------   ---     -----------------------------------------------------
  06/05/15           Created file.

==========================================================================*/

#ifdef TARGET_BUILD
#include "EliteMsg_Util.h"
#include "Elite_fwk_extns_ecns.h"
#include "voice_capi_utils.h"
#include "adsp_vparams_internal_api.h"
#include "capi_v2_lvvefq.h"
#include "adsp_vparams_api.h"
#include "adsp_vpm_api.h"
#else
#include "HAP_farf.h"
#include "capi_v2_utils_props.h"
#include "capi_v2_lvvefq.h"

#include <stdlib.h>
#include <string.h>
#endif
#include "LVVEFQ_Tx.h"
#include "LVVEFQ_pcm_injection.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*==========================================================================
  Globals
  ========================================================================== */
#define ROUNDTO8(x) ((((uint32_t)(x) + 7) >> 3) << 3)
#define NB_SAMPLING_RATE      (8000)
#define WB_SAMPLING_RATE      (16000)
#define FB_SAMPLING_RATE      (48000)

#define SIZE_OF_ARRAY(a) (sizeof(a)/sizeof((a)[0]))
#define MAX( x, y ) ( ((x) > (y)) ? (x) : (y) )
#define CAPI_V2_LVVEFQ_TX_STACK_SIZE  (1024)


#define SM_NEAR_PRI_NUM_CHANNELS (1)
#define DM_PRI_NUM_CHANNELS (2)
#define QM_PRI_NUM_CHANNELS (4)
#define FAR_NUM_CHANNELS (1)

#define NUM_INPUT_PORTS  (2) // there are 2 ports: MIC input and Far-end EC
#define NUM_OUTPUT_PORTS (1)

#define INVALID    ((int32_t)(-1))

#define VOICE_IMC_RETRY_THRESHOLD 5
#define VOICE_IMC_TX_SRC_MAX_BUF_Q_ELEMENTS 2

typedef struct{
   capi_v2_set_get_media_format_t media_format;
   capi_v2_standard_data_format_t data_format;
} capi_data_format_struct_t;

typedef struct capi_v2_lvvefq_tx_t {
   const capi_v2_vtbl_t *vtbl;
   /**< Pointer to the virtual table functions */
   uint32_t sampling_rate;
   capi_v2_event_cb_f event_cb; /**< Callback function to issue to framework to report delay and kpps */
   void* event_context; /**< Event context that must be issued in the callback */
   uint32_t delay;
   uint32_t kpps;
   capi_v2_port_num_info_t num_port_info;
   capi_data_format_struct_t input_media_type[NUM_INPUT_PORTS];
   uint32_t heap_id;
   capi_data_format_struct_t output_media_type[NUM_OUTPUT_PORTS];
   int8_t primary_input_port_idx;
   int8_t primary_mic_idx;
   int8_t secondary_mic1_idx;
   int8_t secondary_mic2_idx;
   int8_t secondary_mic3_idx;
   int8_t far_input_port_idx;
   int8_t primary_output_port_idx;
   int16_t pri_num_channels;  // used to track number of input channels for primary port based on topology.

   LVVEFQ_Tx_Instance_st LVVEFQInstance;
   LVVEFQ_injection_st LVVEFQ_injection_Instance;
   imc_src_dst_info_t capi_v2_imc_tx_imc_info; //IMC supported. (Tx <-> Rx)
   capi_v2_session_identifier_t	session_identify;
  
} capi_v2_lvvefq_tx_t;

static capi_v2_err_t capi_v2_lvvefq_tx_process(capi_v2_t* _pif, capi_v2_stream_data_t* input[], capi_v2_stream_data_t* output[]);
static capi_v2_err_t capi_v2_lvvefq_tx_end(capi_v2_t* _pif);
static capi_v2_err_t capi_v2_lvvefq_tx_set_param(capi_v2_t* _pif, uint32_t param_id, const capi_v2_port_info_t *port_info_ptr, capi_v2_buf_t *params_ptr);
static capi_v2_err_t capi_v2_lvvefq_tx_get_param(capi_v2_t* _pif, uint32_t param_id, const capi_v2_port_info_t *port_info_ptr, capi_v2_buf_t *params_ptr);
static capi_v2_err_t capi_v2_lvvefq_tx_set_properties(capi_v2_t* _pif, capi_v2_proplist_t *props_ptr);
static capi_v2_err_t capi_v2_lvvefq_tx_get_properties(capi_v2_t* _pif, capi_v2_proplist_t *props_ptr);


static void capi_v2_lvvefq_tx_send_kpps(capi_v2_lvvefq_tx_t* me);
static void capi_v2_lvvefq_tx_send_delay(capi_v2_lvvefq_tx_t* me);
static capi_v2_err_t capi_v2_lvvefq_tx_send_output_mediatype(capi_v2_lvvefq_tx_t *me);
capi_v2_err_t capi_v2_voice_lvvefq_tx_init(capi_v2_lvvefq_tx_t *me, capi_v2_proplist_t *init_set_properties);
capi_v2_err_t capi_v2_voice_lvvefq_tx_get_static_properties(capi_v2_proplist_t *init_props_ptr, capi_v2_proplist_t *out_props_ptr);
static void capi_v2_imc_tx_get_imc_handle(capi_v2_lvvefq_tx_t* me);

static capi_v2_vtbl_t vtbl =
{
      capi_v2_lvvefq_tx_process,
      capi_v2_lvvefq_tx_end,
      capi_v2_lvvefq_tx_set_param,
      capi_v2_lvvefq_tx_get_param,
      capi_v2_lvvefq_tx_set_properties,
      capi_v2_lvvefq_tx_get_properties,
};

#if 0
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
#endif

/*----------------------------------------------------------------------------
 * Function Definitions
 * -------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * This function is used to query the static properties to create the CAPI.
 *
 * @param[in] init_props_ptr, pointer to the initializing prop list
 * @param[in, out] out_props_ptr, pointer to the output pro list
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/

static inline uint32_t align_to_8_byte(const uint32_t num)
{
   return ((num + 7) & (0xFFFFFFF8));
}

capi_v2_err_t capi_v2_lvvefq_tx_get_static_properties(capi_v2_proplist_t *init_props_ptr, capi_v2_proplist_t *out_props_ptr)
{

   capi_v2_err_t result;

   // Implement different static properties for different mic use cases in the case. 
   // currently the example supports single mic use case only. 
   result = capi_v2_voice_lvvefq_tx_get_static_properties(init_props_ptr, out_props_ptr);

   return result;
}


/*------------------------------------------------------------------------------
 * This function updates the static property information needed by module
 * Mandatory properties to be set 
	- CAPI_V2_INIT_MEMORY_REQUIREMENT
    - CAPI_V2_IS_INPLACE:
	- CAPI_V2_REQUIRES_DATA_BUFFERING
	- CAPI_V2_STACK_SIZE
	- CAPI_V2_NUM_NEEDED_FRAMEWORK_EXTENSIONS
	- CAPI_V2_NEEDED_FRAMEWORK_EXTENSIONS
	- CAPI_V2_CUSTOM_PROPERTY

 *----------------------------------------------------------------------------*/
capi_v2_err_t capi_v2_voice_lvvefq_tx_get_static_properties(capi_v2_proplist_t *init_props_ptr, capi_v2_proplist_t *out_props_ptr)
{
   // out_props_ptr cannot be NULL as we need to know the prop requested
   if (NULL == out_props_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 get_static_properties() FAILED received bad property pointer !!, %p", out_props_ptr);
      return CAPI_V2_EFAILED;
   }

   uint16_t i;
   capi_v2_prop_t *prop_ptr = out_props_ptr->prop_ptr;

   for (i = 0; i < out_props_ptr->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;

      switch (prop_ptr[i].id)
      {
         case CAPI_V2_INIT_MEMORY_REQUIREMENT:
            {
               if (payload->max_data_len >= sizeof(capi_v2_init_memory_requirement_t))
               {
                  capi_v2_init_memory_requirement_t *data_ptr = (capi_v2_init_memory_requirement_t*) payload->data_ptr;
                  LVVEFQ_Tx_Instance_st LVVEFQInstance;
                  uint32_t sampling_rate[] = {NB_SAMPLING_RATE, WB_SAMPLING_RATE, FB_SAMPLING_RATE};				  
                  uint32_t max_mem_size = 0;
                  uint32_t i = 0;

				  memset(&LVVEFQInstance, 0x00, sizeof(LVVEFQ_Tx_Instance_st));

                   data_ptr->size_in_bytes = align_to_8_byte(sizeof(capi_v2_lvvefq_tx_t));
				  
                  // capi module don't know the media format for an input port at this point
                  // ( CAPI_V2_INPUT_MEDIA_FORMAT will be updated later )
                  // So, request maximum size of memory
                  for (i = 0; i < SIZE_OF_ARRAY(sampling_rate); i++) {
                    LVVEFQ_Tx_config(&LVVEFQInstance, sampling_rate[i]); 
                    max_mem_size = MAX(max_mem_size, LVVEFQ_Tx_get_size(&LVVEFQInstance));
                  }   
                  data_ptr->size_in_bytes += max_mem_size;
				  
                  payload->actual_data_len = sizeof(capi_v2_init_memory_requirement_t);

               } else
               {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 get_static_properties() FAILED Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id,
                        payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }
         case CAPI_V2_IS_INPLACE:
            {
               if (payload->max_data_len >= sizeof(capi_v2_is_inplace_t))
               {
                  capi_v2_is_inplace_t *data_ptr = (capi_v2_is_inplace_t*) payload->data_ptr;
                  data_ptr->is_inplace = FALSE;		// No need to change inplace computing for ECNS module because input and output buffers are different
                  payload->actual_data_len = sizeof(capi_v2_is_inplace_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }
         case CAPI_V2_REQUIRES_DATA_BUFFERING:
            {
               if (payload->max_data_len >= sizeof(capi_v2_requires_data_buffering_t))
               {
                  capi_v2_requires_data_buffering_t *data_ptr = (capi_v2_requires_data_buffering_t*) payload->data_ptr;
                  data_ptr->requires_data_buffering = FALSE; // Depending on whether the module needs data buffering to be handled by framework, this should be FALSE for Voice module
                  payload->actual_data_len = sizeof(capi_v2_requires_data_buffering_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }
         case CAPI_V2_STACK_SIZE:
            {
               if (payload->max_data_len >= sizeof(capi_v2_stack_size_t))
               {
                  capi_v2_stack_size_t *data_ptr = (capi_v2_stack_size_t*) payload->data_ptr;
                  data_ptr->size_in_bytes = CAPI_V2_LVVEFQ_TX_STACK_SIZE; // Worst case stack size
				  payload->actual_data_len = sizeof(capi_v2_stack_size_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }

               break;
            }
         case CAPI_V2_NUM_NEEDED_FRAMEWORK_EXTENSIONS:
            {
               if (payload->max_data_len >= sizeof(capi_v2_num_needed_framework_extensions_t))
               {
                  capi_v2_num_needed_framework_extensions_t *data_ptr = (capi_v2_num_needed_framework_extensions_t *) payload->data_ptr;
                  data_ptr->num_extensions = 1; // Specify the number of framework extensions needed by ECNS
                  payload->actual_data_len = sizeof(capi_v2_num_needed_framework_extensions_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }

         case CAPI_V2_NEEDED_FRAMEWORK_EXTENSIONS:
            {
               if (payload->max_data_len >= (sizeof(capi_v2_framework_extension_id_t)))
               {
                  capi_v2_framework_extension_id_t *data_ptr = (capi_v2_framework_extension_id_t *) payload->data_ptr;
                  data_ptr[0].id = FWK_EXTN_ECNS; // Define the ID of framework extensions needed by module.
                  payload->actual_data_len = sizeof(capi_v2_framework_extension_id_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }
         case CAPI_V2_MAX_METADATA_SIZE:
            {
               if (payload->max_data_len >= (sizeof(capi_v2_max_metadata_size_t)))
               {
                  capi_v2_max_metadata_size_t *data_ptr = (capi_v2_max_metadata_size_t *) payload->data_ptr;
                  data_ptr->size_in_bytes = 0;
                  payload->actual_data_len = sizeof(capi_v2_max_metadata_size_t);
               } else
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Get Property id 0x%lx Bad param size %lu", (uint32_t) prop_ptr[i].id, payload->max_data_len);
                  return CAPI_V2_ENEEDMORE;
               }
               break;
            }
#ifdef TARGET_BUILD
         case CAPI_V2_CUSTOM_PROPERTY: ///set FALSE for below PROPERTY
            {
               capi_v2_get_ecns_property_t *ecns_custom_prop = (capi_v2_get_ecns_property_t *) payload->data_ptr;

               switch (ecns_custom_prop->sec_property_id)
               {
                  case CAPI_V2_PROPERTY_ID_ECNS_OUTPUT_CAPABILITIES:
                     {
                        ecns_output_capabilities_t *ecns_output_info = (ecns_output_capabilities_t*) ecns_custom_prop->ecns_info;
                        ecns_output_info->output_lec = FALSE; // Set this to TRUE to update framework that module provides Linear Echo Canceller output
                        ecns_output_info->output_nr = FALSE; // Set this to TRUE to update framework that module provides Noise Reduction output
                        payload->actual_data_len = sizeof(capi_v2_get_ecns_property_t);
                        break;
                     }
                  case CAPI_V2_PROPERTY_ID_ECNS_MONITORING_CAPABILITIES:
                     {
                        ecns_monitoring_capabilities_t *ecns_monitoring_info =  (ecns_monitoring_capabilities_t*)ecns_custom_prop->ecns_info;
                        ecns_monitoring_info->is_rtm_supported=FALSE; // Set TRUE if module has real time monitoring capabilities
                        payload->actual_data_len =  sizeof(capi_v2_get_ecns_property_t);
                        break;
                     }
                  case CAPI_V2_PROPERTY_ID_ECNS_VP3_CAPABILITIES:
                     {
                        ecns_vp3_capabilities_t *ecns_vp3_info =  (ecns_vp3_capabilities_t*)ecns_custom_prop->ecns_info;
                        ecns_vp3_info->is_vp3_supported=FALSE; // Set to TRUE if module supports VP3
                        payload->actual_data_len =  sizeof(capi_v2_get_ecns_property_t);
                        break;
                     }
                  case CAPI_V2_PROPERTY_ID_ECNS_RATE_MATCHING_CAPABILITIES:
                     {
                        ecns_rate_matching_capabilities_t *ecns_rate_matching_info =  (ecns_rate_matching_capabilities_t*)ecns_custom_prop->ecns_info;
                        ecns_rate_matching_info->is_rate_matching_supported=FALSE; // Set to TRUE if module supports rate matching/sample slip

                        payload->actual_data_len =  sizeof(capi_v2_get_ecns_property_t);
                        break;
                     }
                  case CAPI_V2_PROPERTY_ID_ECNS_STT_CAPABILITIES:
				   {
					  ecns_stt_capabilities_t *ecns_stt_info =  (ecns_stt_capabilities_t*)ecns_custom_prop->ecns_info;
					  ecns_stt_info->is_stt_supported=FALSE;
					  payload->actual_data_len =  sizeof(capi_v2_get_ecns_property_t);
					  break;
				   }
                  default:
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: ERROR !! Invalid CUSTOM property id %lx", ecns_custom_prop->sec_property_id);
                        break;
                     }
               }
               break;
            }
#endif
         default:
            {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 get_static_properties() FAILED Get Property for 0x%x. Not supported.", prop_ptr[i].id);
               return CAPI_V2_EBADPARAM;
            }
      }
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: get_static_properties() Get Property for 0x%x done", prop_ptr[i].id);
   }
   return CAPI_V2_EOK;
}

/*------------------------------------------------------------------------------
 * This function is used init the CAPI lib.
 *
 * @param[in] _pif, pointer to the CAPI lib.
 * @param[in] init_set_properties, pointer to the prop list that needs to be init'ed.
 *
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/
capi_v2_err_t capi_v2_lvvefq_tx_init(capi_v2_t* _pif, capi_v2_proplist_t *init_set_properties)
{
   capi_v2_err_t result;

   if (NULL == _pif)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 Init FAILED received bad pointer");
      return CAPI_V2_EFAILED;
   }

   capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t *) _pif;

   /* Clean the structure first */
   memset(me, 0, sizeof(capi_v2_lvvefq_tx_t));
   
   result=capi_v2_voice_lvvefq_tx_init(me, init_set_properties);


   int8_t* mem_ptr = (int8_t*)_pif;
   mem_ptr += align_to_8_byte(sizeof(capi_v2_lvvefq_tx_t));
   
   // Call both two functions again with right sampling rate
   LVVEFQ_Tx_config(&(me->LVVEFQInstance), me->sampling_rate);
   uint32_t mem_size = LVVEFQ_Tx_get_size(&(me->LVVEFQInstance));
   LVVEFQ_injection_config(&(me->LVVEFQ_injection_Instance), me->sampling_rate);
   
   LVVEFQ_Tx_set_mem(&(me->LVVEFQInstance), mem_ptr, mem_size);
   LVVEFQ_Tx_init(&(me->LVVEFQInstance), me->pri_num_channels);
   LVVEFQ_injection_init(&(me->LVVEFQ_injection_Instance));

   return result;

}

/*------------------------------------------------------------------------------
 * This function initializes the static property information for the module
 * Following properties are set by framework and should be handled by module.
	- CAPI_V2_INPUT_MEDIA_FORMAT
    - CAPI_V2_EVENT_CALLBACK_INFO
	- CAPI_V2_PORT_NUM_INFO
	- CAPI_V2_HEAP_ID
 *----------------------------------------------------------------------------*/
capi_v2_err_t capi_v2_voice_lvvefq_tx_init(capi_v2_lvvefq_tx_t *me, capi_v2_proplist_t *init_set_properties)
{

   uint32_t i, result = 0;
   //uint32_t event_cb_idx = -1;
   //capi_v2_event_cb_f cb_func =NULL;
   //void* cb_context = NULL;

   if (NULL == me)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 Init FAILED received bad pointer");
      return CAPI_V2_EFAILED;
   }

   if (NULL == init_set_properties || NULL == init_set_properties->prop_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 Init FAILED received bad pointer for init props");
      return CAPI_V2_EFAILED;
   }

   /* Set the vtable to allow the processing */
   me->vtbl = &vtbl;
   

  me->session_identify.service_id = 0xFFFF;
  me->session_identify.session_id = 0xFFFF;  //initialize with 0xFFFF


   capi_v2_prop_t *prop_ptr = init_set_properties->prop_ptr;

   for (i = 0; i < init_set_properties->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;
      switch ((uint32_t)(prop_ptr[i].id))
      {
         case CAPI_V2_INPUT_MEDIA_FORMAT:
            {
               capi_data_format_struct_t *lvvefq_data_format_ptr = (capi_data_format_struct_t*) payload->data_ptr;
               for (uint32_t i = 0; i < me->num_port_info.num_input_ports; i++)
               {
                  // Payload can be no smaller than the header for media format
                  if (payload->actual_data_len != sizeof(capi_data_format_struct_t) * me->num_port_info.num_input_ports)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Size mismatch for input format property, got %lu", payload->actual_data_len);
                     //bail out
                     return CAPI_V2_EBADPARAM;
                  }

                  if (CAPI_V2_FIXED_POINT != lvvefq_data_format_ptr[i].media_format.format_header.data_format)
                  {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! unsupported media format %d",
                           (int) lvvefq_data_format_ptr[i].media_format.format_header.data_format);
                     return CAPI_V2_EBADPARAM;
                  }
                  capi_v2_standard_data_format_t *pcm_format_ptr = &lvvefq_data_format_ptr[i].data_format;
                  // We only care about number of channels and sampling rate
                  if (((NB_SAMPLING_RATE != pcm_format_ptr->sampling_rate) && (WB_SAMPLING_RATE != pcm_format_ptr->sampling_rate)
                           && (FB_SAMPLING_RATE != pcm_format_ptr->sampling_rate)) || (16 != pcm_format_ptr->bits_per_sample))
                  {
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! invalid format chan %lu, SR %lu, bits per samp %lu", pcm_format_ptr->num_channels,
                           pcm_format_ptr->sampling_rate, pcm_format_ptr->bits_per_sample);
                     return CAPI_V2_EBADPARAM;
                  }
                  switch (lvvefq_data_format_ptr[i].data_format.channel_type[0])
                  {
                     case PCM_CHANNEL_PRI_MIC:
                     case PCM_CHANNEL_SEC_MIC_1:
                     case PCM_CHANNEL_SEC_MIC_2:
                     case PCM_CHANNEL_SEC_MIC_3:					 	
                        {
                           me->primary_mic_idx = me->secondary_mic1_idx = INVALID;
                           me->secondary_mic2_idx = me->secondary_mic3_idx = INVALID;

                           for(uint32_t j = 0; j < pcm_format_ptr->num_channels; j++) {
                              switch (lvvefq_data_format_ptr[i].data_format.channel_type[j]) {
                                 case PCM_CHANNEL_PRI_MIC:
                                    me->primary_mic_idx = j;
                                    break;
                                 case PCM_CHANNEL_SEC_MIC_1:
                                    me->secondary_mic1_idx = j;
                                    break;
                                 case PCM_CHANNEL_SEC_MIC_2:
                                    me->secondary_mic2_idx = j;
                                    break;								   	
                                 case PCM_CHANNEL_SEC_MIC_3:
                                    me->secondary_mic3_idx = j;
                                    break;
                                 default:
                                    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: ERROR !!! unrecognized input channel type");
                                    break;
                              }
                           }
						   
                           me->pri_num_channels = pcm_format_ptr->num_channels;
                           me->primary_input_port_idx = i;
                           MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: NEAR PORT at %d", i);
                           break;
                        }
                     case PCM_CHANNEL_FAR_REF:
                        {
                           if (FAR_NUM_CHANNELS != lvvefq_data_format_ptr[i].data_format.num_channels)
                           {
                             MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! invalid number of channels (%ld) for secondary port ",
                                    lvvefq_data_format_ptr[i].data_format.num_channels);
                              return CAPI_V2_EBADPARAM;
                           }
						   // Set far input port index
                           me->far_input_port_idx = i;
                           MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: FAR PORT at %d", i);
                           break;
                        }
                     default:
                        {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: ERROR !!! unrecognized input channel type");
                           break;
                        }
                  }
                  me->input_media_type[i] = lvvefq_data_format_ptr[i]; // saving the port media type information
               }

               me->sampling_rate = lvvefq_data_format_ptr[0].data_format.sampling_rate;
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: CAPI V2 Sampling rate (%d)", me->sampling_rate);
               capi_v2_lvvefq_tx_send_output_mediatype(me);
               break;
            }
         case CAPI_V2_EVENT_CALLBACK_INFO:
            {
			   // Set the event callback pointer
			   capi_v2_event_callback_info_t *cb_info_ptr = (capi_v2_event_callback_info_t*) payload->data_ptr;
               //event_cb_idx = i;
               //cb_func = me->event_cb = cb_info_ptr->event_cb;
               me->event_cb = cb_info_ptr->event_cb;
               //cb_context = me->event_context = cb_info_ptr->event_context;
               me->event_context = cb_info_ptr->event_context;
               break;
            }
         case CAPI_V2_PORT_NUM_INFO:
            {
               capi_v2_port_num_info_t *num_port_info = (capi_v2_port_num_info_t*) payload->data_ptr;
               if ((num_port_info->num_input_ports != NUM_INPUT_PORTS) /*|| (num_port_info->num_output_ports != NUM_OUTPUT_PORTS)*/)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! incorrect number of input ( %lu) or output ( %lu) ports", num_port_info->num_input_ports,
                        num_port_info->num_output_ports);
                  //bail out
                  return CAPI_V2_EBADPARAM;
               }
			   // Update module about port information
               me->num_port_info = *num_port_info;
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: CAPI V2 received port info");

               break;

            }
         case CAPI_V2_HEAP_ID:
            {
               if (sizeof(capi_v2_heap_id_t) != payload->actual_data_len)
               {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: CAPI V2 received bad size(%ld) for heap id property", payload->actual_data_len);
                  return CAPI_V2_EFAILED;
               }
			   // Update heap ID to module 
               capi_v2_heap_id_t* heap_id_ptr = (capi_v2_heap_id_t*) payload->data_ptr;
               me->heap_id = heap_id_ptr->heap_id;
               break;
            }
		 case CAPI_V2_SESSION_IDENTIFIER:
		 	{
				capi_v2_session_identifier_t *session_identify = (capi_v2_session_identifier_t *)payload->data_ptr;
				if( payload->actual_data_len != sizeof(capi_v2_session_identifier_t) )
				{
					MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_RX: Error! Size mismatch for port info property, got %lu", payload->actual_data_len);
					//bail out
					return CAPI_V2_EBADPARAM;
				}

				me->session_identify.service_id = session_identify->service_id;
				me->session_identify.session_id = session_identify->session_id;

				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "TJVOICE_IMC_RX: capi_v2_voice_imc_rx_set_properties(): session_identifies service_id(%lx), session_id(#lx)",
				                                 me->session_identify.service_id,
				                                 me->session_identify.session_id);
				break;
		 	}
         default:
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: Unsupported prop_id 0x%lx", (uint32_t) prop_ptr[i].id);
               return CAPI_V2_EOK;
            }
      }
   }

   //Initialize state information.
   me->capi_v2_imc_tx_imc_info.state = IMC_STATE_NONE;

   // Get the handle of IMC module. This also fetches the session identifier
   capi_v2_imc_tx_get_imc_handle(me);

	/*
	* If imc library is queried, register for IMC handle.
	*/
	if(NULL != me->capi_v2_imc_tx_imc_info.imc_lib_handle)
	{
		/*
		* Key is COMMID | RX module ID | Tx module Id | seesionID
		*/
		me->capi_v2_imc_tx_imc_info.key[0] = (int8_t)VOICE_MODULE_LVVEFQ_IMC;
		me->capi_v2_imc_tx_imc_info.key[1] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 8);
		me->capi_v2_imc_tx_imc_info.key[2] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 16);
		me->capi_v2_imc_tx_imc_info.key[3] = (int8_t)(VOICE_MODULE_LVVEFQ_IMC >> 24);
		me->capi_v2_imc_tx_imc_info.key[4] = (int8_t)VOICE_MODULE_LVVEFQ_RX;
		me->capi_v2_imc_tx_imc_info.key[5] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 8);
		me->capi_v2_imc_tx_imc_info.key[6] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 16);
		me->capi_v2_imc_tx_imc_info.key[7] = (int8_t)(VOICE_MODULE_LVVEFQ_RX >> 24);
		me->capi_v2_imc_tx_imc_info.key[8] = (int8_t)VOICE_MODULE_LVVEFQ_TX;
		me->capi_v2_imc_tx_imc_info.key[9] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 8);
		me->capi_v2_imc_tx_imc_info.key[10] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 16);
		me->capi_v2_imc_tx_imc_info.key[11] = (int8_t)(VOICE_MODULE_LVVEFQ_TX >> 24);
		imc_src_dst_info_t *comm_info_ptr;
		uint32_t param_size,max_q_length,alloc_buffer;
		uint32_t max_payload_size = sizeof(int32_t);
		imc_src_dst_t src_dest;

		param_size = sizeof(me->capi_v2_imc_tx_imc_info.key);
		comm_info_ptr = (imc_src_dst_info_t*)&me->capi_v2_imc_tx_imc_info;
		src_dest = IMC_SOURCE;
		max_q_length = alloc_buffer = VOICE_IMC_TX_SRC_MAX_BUF_Q_ELEMENTS;

		// register for intermodule communication
		capi_v2_err_t imc_result;
		imc_result = voice_imc_register_src_dest(comm_info_ptr,(int8_t*)me->capi_v2_imc_tx_imc_info.key, param_size, max_q_length, alloc_buffer,max_payload_size,src_dest);
		if(CAPI_V2_FAILED(imc_result))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VOICE_IMC_TX: failed to register to IMC. result(0x%lx)",imc_result);
			return imc_result;
		}
	}
   return result;

}

/*------------------------------------------------------------------------------
 * This function is responsible for processing of near end data. 
 *
 * @param[in] _pif, pointer to the CAPI lib.
 * @param[in] input, pointer to input data buffer.
 * @param[in] output, pointer to output data buffer.
 *
 * @return capi_v2_err_t, result code
 * It does passthrough of primary input port data to output port.
 *----------------------------------------------------------------------------*/

static capi_v2_err_t capi_v2_lvvefq_tx_process(capi_v2_t* _pif, capi_v2_stream_data_t* input[], capi_v2_stream_data_t* output[])
{
   int16 *ec_out_ptr;
   capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t *) _pif;

   
   int16 *ptr_Tx_Input[4] = {(int16*) (input[me->primary_input_port_idx]->buf_ptr[me->primary_mic_idx].data_ptr), NULL, NULL, NULL};
   int16 *ptr_Rx_Input[4] = {(int16*) (input[me->far_input_port_idx]->buf_ptr[0].data_ptr), NULL, NULL, NULL};
   uint32_t nSamples_near = input[me->primary_input_port_idx]->buf_ptr[me->primary_mic_idx].actual_data_len >> 1;

   bool_t is_queue_empty;
   capi_v2_err_t result=CAPI_V2_EOK;
   capi_v2_err_t resp_result;
   void *payload_header_ptr = NULL;
   
   //Check if the Tx has opend the communication. If it has not then open it
	// Also this is checked only for VOICE_IMC_RETRY_THRESHOLD number of times. 
	if( ((IMC_STATE_SOURCE_CLOSED_COMMUNICATION == me->capi_v2_imc_tx_imc_info.state) ||
		(IMC_STATE_REGISTERED == me->capi_v2_imc_tx_imc_info.state)) &&
		(me->capi_v2_imc_tx_imc_info.src_frame_num < VOICE_IMC_RETRY_THRESHOLD))
	{
		imc_src_dst_info_t *imc_instc_ptr = &me->capi_v2_imc_tx_imc_info;
		MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VOICE_IMC_TX: capi_v2_imc_tx_process(): opening communication for inter module communication");

		result = imc_instc_ptr->imc_lib_handle->vtable->src_open_comm( imc_instc_ptr->source_handle, &imc_instc_ptr->dest_handle,
			(const int8_t *)imc_instc_ptr->key,imc_instc_ptr->key_length);

		if(CAPI_V2_FAILED(result))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VOICE_IMC_TX: capi_v2_imc_tx_process(): Failed to open Communication. result(%lx)",result);
			imc_instc_ptr->src_frame_num++;
		}
		else
		{
			imc_instc_ptr->state = IMC_STATE_SOURCE_OPENED_COMMUNICATION;
		}
	}
	
   if ( SM_NEAR_PRI_NUM_CHANNELS != me->pri_num_channels)
   {
      ptr_Tx_Input[1] = (int16*) (input[me->primary_input_port_idx]->buf_ptr[me->secondary_mic1_idx].data_ptr);
      if(QM_PRI_NUM_CHANNELS == me->pri_num_channels)
      {
         ptr_Tx_Input[2] = (int16*) (input[me->primary_input_port_idx]->buf_ptr[me->secondary_mic2_idx].data_ptr);
         ptr_Tx_Input[3] = (int16*) (input[me->primary_input_port_idx]->buf_ptr[me->secondary_mic3_idx].data_ptr);
      }
   }

   ec_out_ptr = (int16 *) output[me->primary_output_port_idx]->buf_ptr[0].data_ptr;
   output[me->primary_output_port_idx]->buf_ptr[0].actual_data_len = nSamples_near << 1;

   if (me->LVVEFQ_injection_Instance.in_st.enable) {
     LVVEFQ_injection_process(&(me->LVVEFQ_injection_Instance), (short *)ptr_Tx_Input[0], nSamples_near);
   }

   if (me->LVVEFQInstance.enable) {
     LVVEFQ_Tx_process(&(me->LVVEFQInstance),
                        (short **)ptr_Tx_Input,
                        (short *)ptr_Rx_Input[0],
                        (short *)ec_out_ptr,
                        nSamples_near);
   } else {
     // If LVVE disabled, just passthrough the PCM samples because ECNS sould be non-inplace module
     memmove(ec_out_ptr, ptr_Tx_Input[0], nSamples_near << 1);
   }

   if (me->LVVEFQ_injection_Instance.out_st.enable) {
     LVVEFQ_injection_process(&(me->LVVEFQ_injection_Instance), (short *)ec_out_ptr, nSamples_near);
   }
	if(me->capi_v2_imc_tx_imc_info.state != IMC_STATE_SOURCE_OPENED_COMMUNICATION)
	{
		MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VOICE_IMC_TX:  VoiceIMCTx has not opened communication. Returning");
		return CAPI_V2_EOK;
	}
	/*
	* Send the data to destination module.
	* 1. pop the buffer from Source queue.
	* 2. fill the buffer with the data - payload header and data.
	* 3. send the buffer to destination.
	*/
	LVVEFQ_LVAVC_Message_st **buf = NULL ;
	//MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO, "VOICE_IMC_TX:  Trying to pop buffers from the queue");
	result = me->capi_v2_imc_tx_imc_info.imc_lib_handle->vtable->src_pop_buf(me->capi_v2_imc_tx_imc_info.source_handle,(void**)&buf, &resp_result,&is_queue_empty);
	if(is_queue_empty == FALSE)
	{
		uint32_t payload_size;

		if( CAPI_V2_FAILED( result))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_TX:  FAILED to pop buffers from the queue. result(%lx),is_queue_empty(%d)!!",result,(int)is_queue_empty);
			return CAPI_V2_EFAILED;
		}

		*buf = &me->LVVEFQInstance.LVVEFQ_LVAVC_Message;
		payload_size = sizeof(buf);
		//(me->counter_imc)++;
		//MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "VOICE_IMC_TX:  IMC TX sending counter = 0x%X !!",*buf);
		result = me->capi_v2_imc_tx_imc_info.imc_lib_handle->vtable->src_push_buf(me->capi_v2_imc_tx_imc_info.source_handle,me->capi_v2_imc_tx_imc_info.dest_handle,buf,payload_size);
		if( CAPI_V2_FAILED(result))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_TX:  FAILED to push buffers from the queue. result(%lx)!!",result);
			me->capi_v2_imc_tx_imc_info.imc_lib_handle->vtable->src_return_buf(me->capi_v2_imc_tx_imc_info.source_handle, payload_header_ptr,CAPI_V2_EOK);

			//IF the destination is not active then Close the connection and try again to open connection in next frame
			if(CAPI_V2_EUNSUPPORTED == result)
			{
				MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_TX:   Rx handle is not active. Closing the connection");
				me->capi_v2_imc_tx_imc_info.imc_lib_handle->vtable->src_close_connection(me->capi_v2_imc_tx_imc_info.source_handle,me->capi_v2_imc_tx_imc_info.dest_handle);
				me->capi_v2_imc_tx_imc_info.state = IMC_STATE_SOURCE_CLOSED_COMMUNICATION;
				me->capi_v2_imc_tx_imc_info.src_frame_num = 0;
			}
		}
	}
	else
	{
		MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VOICE_IMC_TX:  queue is empty. result(%lx)!!",result);
	}

   return CAPI_V2_EOK;
}

/*------------------------------------------------------------------------------
 * This function is responsible for processing of near end data. 
 * It does passthrough of primary input port data to output port.
 * Ensure that all memory allocated within the module is freed up here.
 *----------------------------------------------------------------------------*/
capi_v2_err_t capi_v2_lvvefq_tx_end(capi_v2_t* _pif)
{
   if (NULL == _pif)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: FAILED received bad property pointer");
      return CAPI_V2_EFAILED;
   }

   capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t*) _pif;

   imc_src_dst_info_t *imc_src_inst = &me->capi_v2_imc_tx_imc_info;

   //Close communication if already opened
	if(IMC_STATE_SOURCE_OPENED_COMMUNICATION == imc_src_inst->state)
	{
		imc_src_inst->imc_lib_handle->vtable->src_close_connection(imc_src_inst->source_handle,imc_src_inst->dest_handle);
		imc_src_inst->state = IMC_STATE_SOURCE_CLOSED_COMMUNICATION;
	}

	// Deregister the module
	if(IMC_STATE_NONE != imc_src_inst->state)
	{
		imc_src_inst->imc_lib_handle->vtable->src_deregister(imc_src_inst->source_handle);
		imc_src_inst->state = IMC_STATE_NONE;
	}
   imc_src_inst->imc_lib_handle->vtable->b.end(imc_src_inst->imc_lib_handle);
   //ToDo: please free the memory here
   LVVEFQ_Tx_end(&(me->LVVEFQInstance));

   return CAPI_V2_EOK;
}

/*------------------------------------------------------------------------------
 * This function is responsible for setting parameters in module. 
 *
 * @param[in] _pif, pointer to the CAPI lib.
 * @param[in] param_id, parameter id sent from client processor.
 * @param[in] port_info_ptr, pointer to port.
 * @param[in] params_ptr, data pointer to parameters.
 *
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/

capi_v2_err_t capi_v2_lvvefq_tx_set_param(capi_v2_t* _pif, uint32_t param_id, const capi_v2_port_info_t *port_info_ptr, capi_v2_buf_t *params_ptr)
{
    capi_v2_err_t nResult = CAPI_V2_EOK;
    capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t*) _pif;

   if (NULL == _pif || NULL == params_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: FAILED received bad property pointer for param_id property, 0x%lx", param_id);
      return CAPI_V2_EFAILED;
   }

   if (ADSP_EOK == LVVEFQ_injection_set_param(&(me->LVVEFQ_injection_Instance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->actual_data_len)) {
     nResult = CAPI_V2_EOK;
   }else if (ADSP_EOK == LVVEFQ_Tx_set_param(&(me->LVVEFQInstance),
                                        params_ptr->data_ptr, 
                                        param_id, 
                                        params_ptr->actual_data_len)) {
     nResult = CAPI_V2_EOK;
   } else {
     nResult = CAPI_V2_EBADPARAM;
   }


   if ( param_id == VOICE_PARAM_MOD_ENABLE )
   {
      uint32_t new_kpps = 0, new_delay = 0;
      uint32_t topology_id = (me->input_media_type[me->primary_input_port_idx].data_format.num_channels == 2) ? VOICE_TOPOLOGY_LVVEFQ_TX_DM : VOICE_TOPOLOGY_LVVEFQ_TX_SM;
      LVVEFQ_Tx_get_kpps(&(me->LVVEFQInstance),  topology_id, &new_kpps);
      LVVEFQ_Tx_get_delay(&(me->LVVEFQInstance), topology_id, &new_delay);
   
      if (new_kpps != me->kpps)
      {
         me->kpps = new_kpps;
      capi_v2_lvvefq_tx_send_kpps(me);
      }
   
      if (new_delay != me->delay)
      {
         me->delay = new_delay;
      capi_v2_lvvefq_tx_send_delay(me);
      }
   }

   return nResult;
}

/*------------------------------------------------------------------------------
 * This function is responsible for getting parameters in module. 
 *
 * @param[in] _pif, pointer to the CAPI lib.
 * @param[in] param_id, parameter id sent from client processor.
 * @param[in] port_info_ptr, pointer to port.
 * @param[in] params_ptr, data pointer to parameters to be written by module.
 *
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/

capi_v2_err_t capi_v2_lvvefq_tx_get_param(capi_v2_t* _pif, uint32_t param_id, const capi_v2_port_info_t *port_info_ptr, capi_v2_buf_t *params_ptr)
{
   if (NULL == _pif || NULL == params_ptr || NULL == params_ptr->data_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: FAILED received bad property pointer for param_id property, 0x%lx", param_id);
      return CAPI_V2_EFAILED;
   }

   capi_v2_err_t nResult = CAPI_V2_EOK;

   capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t*) _pif;

   if (ADSP_EOK == LVVEFQ_injection_get_param(&(me->LVVEFQ_injection_Instance),
                                       params_ptr->data_ptr, 
                                       param_id, 
                                       params_ptr->max_data_len, 
                                       (uint16_t *)&(params_ptr->actual_data_len))) {
     nResult = CAPI_V2_EOK;
   } else if (ADSP_EOK == LVVEFQ_Tx_get_param(&(me->LVVEFQInstance),
                                        params_ptr->data_ptr, 
                                        param_id, 
                                        params_ptr->max_data_len, 
                                        &(params_ptr->actual_data_len))) {
     nResult = CAPI_V2_EOK;
   } else {
     nResult = CAPI_V2_EBADPARAM;
   }

   return nResult;
}

/*------------------------------------------------------------------------------
 * This function is responsible for setting properties in module. 
 *
 * @param[in] _pif, pointer to the CAPI lib
 * @param[in] props_ptr, pointer to various properties
 *
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/

static capi_v2_err_t capi_v2_lvvefq_tx_set_properties(capi_v2_t* _pif, capi_v2_proplist_t *props_ptr)
{
   if (!_pif || !props_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Received bad pointer in set_properties");
      return CAPI_V2_EFAILED;
   }

   capi_v2_lvvefq_tx_t *me = (capi_v2_lvvefq_tx_t*) _pif;
   uint32_t i;
   for (i = 0; i < props_ptr->props_num; i++)
   {
      capi_v2_prop_t* current_prop_ptr = &(props_ptr->prop_ptr[i]);
      capi_v2_buf_t* payload_ptr = &(current_prop_ptr->payload);
      switch (current_prop_ptr->id)
      {
         case CAPI_V2_EVENT_CALLBACK_INFO:
            {
               capi_v2_event_callback_info_t *cb_info_ptr = (capi_v2_event_callback_info_t*) payload_ptr->data_ptr;
               me->event_cb = cb_info_ptr->event_cb;
               me->event_context = cb_info_ptr->event_context;
               break;
            }
         case CAPI_V2_ALGORITHMIC_RESET:
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LVVEFQ : Algorithm Reset done");
               break;
            }
		  
		 case CAPI_V2_SESSION_IDENTIFIER:
			 {
				capi_v2_session_identifier_t *session_identify = (capi_v2_session_identifier_t *)payload_ptr->data_ptr;
				if( payload_ptr->actual_data_len != sizeof(capi_v2_session_identifier_t) )
				{
				   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_TX:  Error! Size mismatch for port info property, got %lu", payload_ptr->actual_data_len);
				   //bail out
				   return CAPI_V2_EBADPARAM;
				}
				
				me->session_identify.service_id = session_identify->service_id;
				me->session_identify.session_id = session_identify->session_id;
				
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VOICE_IMC_TX:  session_identifies service_id(%lx), session_id(%lx)",
													  me->session_identify.service_id,
													  me->session_identify.session_id);
				break;
			 }
	
#ifndef TARGET_BUILD
		 case CAPI_V2_INPUT_MEDIA_FORMAT:
             {
                capi_data_format_struct_t *lvvefq_data_format_ptr = (capi_data_format_struct_t*) payload_ptr->data_ptr;
             
                   // Payload can be no smaller than the header for media format
                   if (payload_ptr->actual_data_len != sizeof(capi_data_format_struct_t))
                   {
                      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! Size mismatch for input format property, got %lu", payload_ptr->actual_data_len);
                      //bail out
                      return CAPI_V2_EBADPARAM;
                   }
             
                   if (CAPI_V2_FIXED_POINT != lvvefq_data_format_ptr->media_format.format_header.data_format)
                   {
                   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! unsupported media format %d",
                            (int) lvvefq_data_format_ptr->media_format.format_header.data_format);
                      return CAPI_V2_EBADPARAM;
                   }
                   capi_v2_standard_data_format_t *pcm_format_ptr = &lvvefq_data_format_ptr->data_format;
                   // We only care about number of channels and sampling rate
                   if (((NB_SAMPLING_RATE != pcm_format_ptr->sampling_rate) && (WB_SAMPLING_RATE != pcm_format_ptr->sampling_rate)
                            && (FB_SAMPLING_RATE != pcm_format_ptr->sampling_rate)) || (16 != pcm_format_ptr->bits_per_sample))
                   {
                   MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! invalid format chan %lu, SR %lu, bits per samp %lu", pcm_format_ptr->num_channels,
                            pcm_format_ptr->sampling_rate, pcm_format_ptr->bits_per_sample);
                      return CAPI_V2_EBADPARAM;
                   }
             
                   switch (lvvefq_data_format_ptr->data_format.channel_type[0])
                   {
                      case PCM_CHANNEL_PRI_MIC:
                      case PCM_CHANNEL_SEC_MIC_1:
                      case PCM_CHANNEL_SEC_MIC_2:
                      case PCM_CHANNEL_SEC_MIC_3:                        
                         {
                            me->primary_mic_idx = me->secondary_mic1_idx = INVALID;
                            me->secondary_mic2_idx = me->secondary_mic3_idx = INVALID;
             
                            for(uint32_t j = 0; j < pcm_format_ptr->num_channels; j++) {
                               switch (lvvefq_data_format_ptr->data_format.channel_type[j]) {
                                  case PCM_CHANNEL_PRI_MIC:
                                     me->primary_mic_idx = j;
                                     break;
                                  case PCM_CHANNEL_SEC_MIC_1:
                                     me->secondary_mic1_idx = j;
                                     break;
                                  case PCM_CHANNEL_SEC_MIC_2:
                                     me->secondary_mic2_idx = j;
                                     break;                                  
                                  case PCM_CHANNEL_SEC_MIC_3:
                                     me->secondary_mic3_idx = j;
                                     break;
                                  default:
                                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: ERROR !!! unrecognized input channel type");
                                     break;
                               }
                            }
                            
                            me->pri_num_channels = pcm_format_ptr->num_channels;
                            me->primary_input_port_idx = i;
                            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: NEAR PORT at %d", i);
                            break;
                         }
                      case PCM_CHANNEL_FAR_REF:
                         {
                            if (FAR_NUM_CHANNELS != lvvefq_data_format_ptr->data_format.num_channels)
                            {
                              MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! invalid number of channels (%ld) for secondary port ",
                                     lvvefq_data_format_ptr->data_format.num_channels);
                               return CAPI_V2_EBADPARAM;
                            }
                            // Set far input port index
                            me->far_input_port_idx = i;
                            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: FAR PORT at %d", i);
                            break;
                         }
                      default:
                         {
                      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: ERROR !!! unrecognized input channel type");
                            break;
                         }
                   }
                   me->input_media_type[0] = *lvvefq_data_format_ptr; // saving the port media type information
             
                me->sampling_rate = lvvefq_data_format_ptr->data_format.sampling_rate;
                MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: CAPI V2 Sampling rate (%d)", me->sampling_rate);
                capi_v2_lvvefq_tx_send_output_mediatype(me);
                break;
             }

		 case CAPI_V2_OUTPUT_MEDIA_FORMAT:
		 	{
				// should set the input and then call callback func so that the process will be called. 
				break;				
		 	}
#endif
         default:
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: tx_set_properties Unsupported prop  ID %lx", current_prop_ptr->id);
               return CAPI_V2_EUNSUPPORTED;
            }
      }
   }
   return CAPI_V2_EOK;
}


static capi_v2_err_t capi_v2_lvvefq_tx_get_properties(capi_v2_t* _pif, capi_v2_proplist_t *props_ptr)
{
   return CAPI_V2_EUNSUPPORTED;
}


/*------------------------------------------------------------------------------
 * This function is responsible for updating the kpps to caller service using 
 * event callback functionality. 
 *
 * @param[in] me, Module pointer. 
 *
 *----------------------------------------------------------------------------*/

void capi_v2_lvvefq_tx_send_kpps(capi_v2_lvvefq_tx_t* me)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   capi_v2_event_info_t kpps_event;
   capi_v2_event_KPPS_t kpps_payload;

   kpps_payload.KPPS = me->kpps;
   kpps_event.payload.data_ptr = (int8_t*) &kpps_payload;
   kpps_event.payload.actual_data_len = kpps_event.payload.max_data_len = sizeof(kpps_payload);

   kpps_event.port_info.is_valid = FALSE;

   result = me->event_cb(me->event_context, CAPI_V2_EVENT_KPPS, &kpps_event);

   if (result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! kpps reporting failed, error %ld", result);
   }

}

/*------------------------------------------------------------------------------
 * This function is responsible for updating the algorithm delay to caller 
 * service using event callback functionality. 
 *
 * @param[in] me, Module pointer. 
 *
 *----------------------------------------------------------------------------*/

void capi_v2_lvvefq_tx_send_delay(capi_v2_lvvefq_tx_t* me)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   capi_v2_event_info_t algorithmic_delay_event;
   capi_v2_event_algorithmic_delay_t algorithmic_delay_payload;

   algorithmic_delay_payload.delay_in_us = me->delay;
   algorithmic_delay_event.payload.data_ptr = (int8_t*) &algorithmic_delay_payload;
   algorithmic_delay_event.payload.actual_data_len = algorithmic_delay_event.payload.max_data_len = sizeof(algorithmic_delay_payload);

   algorithmic_delay_event.port_info.is_valid = FALSE;

   result = me->event_cb(me->event_context, CAPI_V2_EVENT_ALGORITHMIC_DELAY, &algorithmic_delay_event);

   if (result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Error! algorithmic_delay reporting failed, error %ld", result);
   }
}

/*------------------------------------------------------------------------------
 * This function is responsible for updating the output media format
 *
 * @param[in] me, Module pointer. 
 *
 * @return capi_v2_err_t, result code
 *----------------------------------------------------------------------------*/
static capi_v2_err_t capi_v2_lvvefq_tx_send_output_mediatype(capi_v2_lvvefq_tx_t *me)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   capi_v2_event_info_t process_event_info;

   if (NULL == me->event_context)
   {
      return CAPI_V2_EBADPARAM;
   }

   for (uint32_t i = 0; i < me->num_port_info.num_output_ports; i++)
   {
      me->output_media_type[i].data_format.bitstream_format = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      me->output_media_type[i].data_format.num_channels = 1;
      me->output_media_type[i].data_format.bits_per_sample = 16;
      me->output_media_type[i].data_format.data_is_signed = TRUE;
      me->output_media_type[i].data_format.data_interleaving = CAPI_V2_DEINTERLEAVED_UNPACKED;
      me->output_media_type[i].data_format.q_factor = 15;
      me->output_media_type[i].media_format.format_header.data_format = CAPI_V2_FIXED_POINT;
   }

   me->output_media_type[0].data_format.channel_type[0] = PCM_CHANNEL_PRI_MIC;
   me->primary_output_port_idx = 0;
   
   me->output_media_type[0].data_format.sampling_rate = me->sampling_rate;
   
   process_event_info.port_info.is_valid = TRUE;
   process_event_info.port_info.is_input_port = FALSE;
   process_event_info.port_info.port_index = 0;
   process_event_info.payload.actual_data_len = sizeof(me->output_media_type);
   process_event_info.payload.data_ptr = (int8_t *) &me->output_media_type;
   process_event_info.payload.max_data_len = sizeof(me->output_media_type);
   result = me->event_cb(me->event_context, CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED, &process_event_info);

   if (CAPI_V2_EOK != result)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Failed to raise event for output media type");
   } else
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: Raised event for output media type");
   }

#ifndef TARGET_BUILD
   {
       capi_v2_event_info_t event_info;
       capi_v2_event_process_state_t process_state;
	   process_state.is_enabled = TRUE;
	   event_info.port_info.is_valid = TRUE;
	   event_info.payload.actual_data_len = sizeof(capi_v2_event_process_state_t);
	   event_info.payload.data_ptr = (int8_t *) &process_state;
	   event_info.payload.max_data_len = sizeof(capi_v2_event_process_state_t);

	   result = me->event_cb(me->event_context, CAPI_V2_EVENT_PROCESS_STATE, &event_info);
	   if (CAPI_V2_EOK != result)
	   {
		  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVEFQ: Failed to raise event for CAPI_V2_EVENT_PROCESS_STATE");
	   } else
	   {
		  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LVVEFQ: Raised event for CAPI_V2_EVENT_PROCESS_STATE");
	   }
   }
#endif

   return result;
}

/**
* This function is used to get the IMC module handle along with session ID for module
* by using event callback functionality.
*/
static void capi_v2_imc_tx_get_imc_handle(capi_v2_lvvefq_tx_t* me)
{
	capi_v2_err_t result = CAPI_V2_EOK;
	capi_v2_event_get_library_instance_t lib_instance;
	capi_v2_event_info_t  event_info;

	lib_instance.id = ELITE_LIB_GET_IMC;
	lib_instance.ptr = NULL;

	event_info.port_info.is_valid = FALSE;
	event_info.payload.data_ptr = (int8_t *)&lib_instance;
	event_info.payload.actual_data_len =event_info.payload.max_data_len = sizeof(lib_instance);

	result = me->event_cb(me->event_context, CAPI_V2_EVENT_GET_LIBRARY_INSTANCE,&event_info);
	if(CAPI_V2_FAILED(result))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VOICE_IMC_TX: get_imc_handle(): failed to get IMC lib handle. result(0x%lx)",result);
		return;
	}
	else
	{
		MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VOICE_IMC_TX: get_imc_handle(): VOICE IMC Tx received IMC lib handle");
	}

	me->capi_v2_imc_tx_imc_info.imc_lib_handle = (elite_lib_get_imc_t*)lib_instance.ptr;

	return;
}

#ifdef __cplusplus
}
#endif //__cplusplus
