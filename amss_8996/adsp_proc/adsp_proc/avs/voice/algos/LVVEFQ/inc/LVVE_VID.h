/****************************************************************************************/
/*  Copyright (c) 2004-2015 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: beq06145 $*/
/*     $Revision: 78504 $*/
/*     $Date: 2015-06-25 14:44:37 +0530 (Thu, 25 Jun 2015) $*/
/*                                                                                      */
/****************************************************************************************/

/** @file
 *  Header file for the application layer interface of the REL_LVVE_HF_NV_AVC_AGC_RX_WBE_DRC_RX_SWB module
 *
 *  The suffix VID in header file stands for Voice Interface Definition
 *  This files includes all definitions, types, and structures required by the calling
 *  layer. Public functions are defined in the protoypes header files.
 *  All other types, structures and functions are private.
 */

#ifndef __REL_LVVE_HF_NV_AVC_AGC_RX_WBE_DRC_RX_SWB_VIDPP_H__
#define __REL_LVVE_HF_NV_AVC_AGC_RX_WBE_DRC_RX_SWB_VIDPP_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#include "LVC_Types.h"

/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVFENS_MASK1</td>
        <td>1</td>
        <td>Far End Noise Suppression is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVWBE_MASK1</td>
        <td>2</td>
        <td>Bandwidth Extension is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_HPF_MASK1</td>
        <td>4</td>
        <td>High Pass Filter is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVNG_MASK1</td>
        <td>16</td>
        <td>Noise Gate is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_NF_MASK1</td>
        <td>32</td>
        <td>Notch Filter is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_EQ_MASK1</td>
        <td>64</td>
        <td>Equalizer is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_AGC_MASK1</td>
        <td>128</td>
        <td>Automatic Gain Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_DRC_MASK1</td>
        <td>256</td>
        <td>Dynamic Range Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_VOL_MASK1</td>
        <td>512</td>
        <td>Volume Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_CNG_MASK1</td>
        <td>2048</td>
        <td>Comfort Noise Generator is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVAVC_MASK1</td>
        <td>8192</td>
        <td>ActiveVoiceContrast is present in the LVVE_Rx</td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_LVVE_Rx_Mask1_bm;
/**
* @def LVVIDHeader_LVVE_Rx_LVFENS_MASK1
* Far End Noise Suppression is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVFENS_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)1)
/**
* @def LVVIDHeader_LVVE_Rx_LVWBE_MASK1
* Bandwidth Extension is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVWBE_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)2)
/**
* @def LVVIDHeader_LVVE_Rx_HPF_MASK1
* High Pass Filter is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_HPF_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)4)
/**
* @def LVVIDHeader_LVVE_Rx_LVNG_MASK1
* Noise Gate is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVNG_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)16)
/**
* @def LVVIDHeader_LVVE_Rx_NF_MASK1
* Notch Filter is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_NF_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)32)
/**
* @def LVVIDHeader_LVVE_Rx_EQ_MASK1
* Equalizer is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_EQ_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)64)
/**
* @def LVVIDHeader_LVVE_Rx_AGC_MASK1
* Automatic Gain Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_AGC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)128)
/**
* @def LVVIDHeader_LVVE_Rx_DRC_MASK1
* Dynamic Range Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_DRC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)256)
/**
* @def LVVIDHeader_LVVE_Rx_VOL_MASK1
* Volume Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_VOL_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)512)
/**
* @def LVVIDHeader_LVVE_Rx_CNG_MASK1
* Comfort Noise Generator is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_CNG_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)2048)
/**
* @def LVVIDHeader_LVVE_Rx_LVAVC_MASK1
* ActiveVoiceContrast is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVAVC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)8192)
/**
* @def LVVIDHEADER_LVVE_RX_MASK1_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Rx_Mask1_bm
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK1_BM_MIN ((LVVIDHeader_LVVE_Rx_Mask1_bm)0)
/**
* @def LVVIDHEADER_LVVE_RX_MASK1_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Rx_Mask1_bm
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK1_BM_MAX ((LVVIDHeader_LVVE_Rx_Mask1_bm)((1) | (2) | (4) | (16) | (32) | (64) | (128) | (256) | (512) | (2048) | (8192) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_LVVE_Rx_Mask2_bm;
/**
* @def LVVIDHEADER_LVVE_RX_MASK2_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Rx_Mask2_bm
* @see LVVIDHeader_LVVE_Rx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK2_BM_MIN ((LVVIDHeader_LVVE_Rx_Mask2_bm)0)
/**
* @def LVVIDHEADER_LVVE_RX_MASK2_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Rx_Mask2_bm
* @see LVVIDHeader_LVVE_Rx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK2_BM_MAX ((LVVIDHeader_LVVE_Rx_Mask2_bm)((0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LVHF_MASK1</td>
        <td>1</td>
        <td>HandsFree is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_EQ_MASK1</td>
        <td>4</td>
        <td>Equalizer is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LVWM_MASK1</td>
        <td>8</td>
        <td>WhisperMode is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_DRC_MASK1</td>
        <td>16</td>
        <td>Dynamic Range Control is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_VOL_MASK1</td>
        <td>32</td>
        <td>Volume Control is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_HPF_MASK1</td>
        <td>128</td>
        <td>HPF is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LPF_MASK1</td>
        <td>256</td>
        <td>LPF is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LVNV_04_MASK1</td>
        <td>4096</td>
        <td>NoiseVoid is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_CNG_MASK1</td>
        <td>8192</td>
        <td>Comfort Noise Generator is present in the LVVE_Tx</td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_LVVE_Tx_Mask1_bm;
/**
* @def LVVIDHeader_LVVE_Tx_LVHF_MASK1
* HandsFree is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LVHF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)1)
/**
* @def LVVIDHeader_LVVE_Tx_EQ_MASK1
* Equalizer is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_EQ_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)4)
/**
* @def LVVIDHeader_LVVE_Tx_LVWM_MASK1
* WhisperMode is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LVWM_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)8)
/**
* @def LVVIDHeader_LVVE_Tx_DRC_MASK1
* Dynamic Range Control is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_DRC_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)16)
/**
* @def LVVIDHeader_LVVE_Tx_VOL_MASK1
* Volume Control is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_VOL_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)32)
/**
* @def LVVIDHeader_LVVE_Tx_HPF_MASK1
* HPF is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_HPF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)128)
/**
* @def LVVIDHeader_LVVE_Tx_LPF_MASK1
* LPF is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LPF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)256)
/**
* @def LVVIDHeader_LVVE_Tx_LVNV_04_MASK1
* NoiseVoid is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LVNV_04_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)4096)
/**
* @def LVVIDHeader_LVVE_Tx_CNG_MASK1
* Comfort Noise Generator is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_CNG_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)8192)
/**
* @def LVVIDHeader_LVVE_Tx_WNS_MASK1
* NoiseVoid with WindNoise Suppression is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_WNS_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)16384)
/**
* @def LVVIDHEADER_LVVE_TX_MASK1_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Tx_Mask1_bm
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK1_BM_MIN ((LVVIDHeader_LVVE_Tx_Mask1_bm)0)
/**
* @def LVVIDHEADER_LVVE_TX_MASK1_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Tx_Mask1_bm
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK1_BM_MAX ((LVVIDHeader_LVVE_Tx_Mask1_bm)((1) | (4) | (8) | (16) | (32) | (128) | (256) | (4096) | (8192) | (16384) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_LVVE_Tx_Mask2_bm;
/**
* @def LVVIDHEADER_LVVE_TX_MASK2_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Tx_Mask2_bm
* @see LVVIDHeader_LVVE_Tx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK2_BM_MIN ((LVVIDHeader_LVVE_Tx_Mask2_bm)0)
/**
* @def LVVIDHEADER_LVVE_TX_MASK2_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Tx_Mask2_bm
* @see LVVIDHeader_LVVE_Tx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK2_BM_MAX ((LVVIDHeader_LVVE_Tx_Mask2_bm)((0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1</td>
        <td>4</td>
        <td>Full Band configuration is present in the LVVE</td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_Configurations_Mask1_bm;
/**
* @def LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1
* Full Band configuration is present in the LVVE
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1 ((LVVIDHeader_Configurations_Mask1_bm)4)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MIN
* Minimal value for LVVIDHeader_Configurations_Mask1_bm
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MIN ((LVVIDHeader_Configurations_Mask1_bm)0)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX
* Maximal value for LVVIDHeader_Configurations_Mask1_bm
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX ((LVVIDHeader_Configurations_Mask1_bm)((4) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
</table> */
typedef LVM_UINT16 LVVIDHeader_Configurations_Mask2_bm;
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MIN
* Minimal value for LVVIDHeader_Configurations_Mask2_bm
* @see LVVIDHeader_Configurations_Mask2_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MIN ((LVVIDHeader_Configurations_Mask2_bm)0)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX
* Maximal value for LVVIDHeader_Configurations_Mask2_bm
* @see LVVIDHeader_Configurations_Mask2_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX ((LVVIDHeader_Configurations_Mask2_bm)((0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL</td>
        <td>1</td>
        <td>Enable the NASEL functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_FU</td>
        <td>2</td>
        <td>Enable the FU functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_RMS_ADAPTATION</td>
        <td>4</td>
        <td>Enable the RMS adaptation functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS</td>
        <td>16</td>
        <td>Enable Sound Class functionality of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS_GWCONFIG</td>
        <td>32</td>
        <td>Enable GW 2nd configuration of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS_GWCONFIG2</td>
        <td>64</td>
        <td>Enable GW 3rd and 4th configurations of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_NOISEBANDGAIN</td>
        <td>128</td>
        <td>Enable Noise level dependency for the gain per Band of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_ONEINSTANCE_CONFIG</td>
        <td>4096</td>
        <td>Enable 1 instance mode in case there is no external processing done between the 2 instances of LVAVC</td>
    </tr>
</table> */
typedef LVM_UINT16 LVAVC_ModeWord_bm;
/**
* @def LVAVC_NASEL
* Enable the NASEL functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL ((LVAVC_ModeWord_bm)1)
/**
* @def LVAVC_FU
* Enable the FU functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_FU ((LVAVC_ModeWord_bm)2)
/**
* @def LVAVC_RMS_ADAPTATION
* Enable the RMS adaptation functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_RMS_ADAPTATION ((LVAVC_ModeWord_bm)4)
/**
* @def LVAVC_NASEL_SOUNDCLASS
* Enable Sound Class functionality of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS ((LVAVC_ModeWord_bm)16)
/**
* @def LVAVC_NASEL_SOUNDCLASS_GWCONFIG
* Enable GW 2nd configuration of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS_GWCONFIG ((LVAVC_ModeWord_bm)32)
/**
* @def LVAVC_NASEL_SOUNDCLASS_GWCONFIG2
* Enable GW 3rd and 4th configurations of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS_GWCONFIG2 ((LVAVC_ModeWord_bm)64)
/**
* @def LVAVC_NASEL_NOISEBANDGAIN
* Enable Noise level dependency for the gain per Band of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_NOISEBANDGAIN ((LVAVC_ModeWord_bm)128)
/**
* @def LVAVC_ONEINSTANCE_CONFIG
* Enable 1 instance mode in case there is no external processing done between the 2 instances of LVAVC
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_ONEINSTANCE_CONFIG ((LVAVC_ModeWord_bm)4096)
/**
* @def LVAVC_MODEWORD_BM_MIN
* Minimal value for LVAVC_ModeWord_bm
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_MODEWORD_BM_MIN ((LVAVC_ModeWord_bm)0)
/**
* @def LVAVC_MODEWORD_BM_MAX
* Maximal value for LVAVC_ModeWord_bm
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_MODEWORD_BM_MAX ((LVAVC_ModeWord_bm)((1) | (2) | (4) | (16) | (32) | (64) | (128) | (4096) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVFENS_MODE_WNS</td>
        <td>1</td>
        <td>Wind Noise Suppression functionality</td>
    </tr>
</table> */
typedef LVM_UINT16 LVFENS_ModeWord_bm;
/**
* @def LVFENS_MODE_WNS
* Wind Noise Suppression functionality
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODE_WNS ((LVFENS_ModeWord_bm)1)
/**
* @def LVFENS_MODEWORD_BM_MIN
* Minimal value for LVFENS_ModeWord_bm
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODEWORD_BM_MIN ((LVFENS_ModeWord_bm)0)
/**
* @def LVFENS_MODEWORD_BM_MAX
* Maximal value for LVFENS_ModeWord_bm
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODEWORD_BM_MAX ((LVFENS_ModeWord_bm)((1) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVWBE_UPPER_BAND_CONTENT_DETECTOR</td>
        <td>1</td>
        <td>Enable the upper band content detector functionality</td>
    </tr>
    <tr>
        <td>@ref LVWBE_POSTPROCESSOR</td>
        <td>2</td>
        <td>Enable the post processor functionality</td>
    </tr>
    <tr>
        <td>@ref LVWBE_MID_BAND_COMPENSATION</td>
        <td>4</td>
        <td>Enable the mid band compensation functionality</td>
    </tr>
    <tr>
        <td>@ref LVWBE_TUNING_TONE_6KHZ</td>
        <td>8</td>
        <td>Output tone of 6 kHz for integration testing</td>
    </tr>
    <tr>
        <td>@ref LVWBE_TUNING_TONE_UPPER_BAND_CONTENT_DETECTOR</td>
        <td>16</td>
        <td>Output tone of 7 kHz for integration testing when no upperband content detected</td>
    </tr>
    <tr>
        <td>@ref LVWBE_UPPER_BAND_SNR_GAIN</td>
        <td>32</td>
        <td>Enable the SNR-dependent upper band gain functionality</td>
    </tr>
    <tr>
        <td>@ref LVWBE_BASE_BAND_LPF</td>
        <td>64</td>
        <td>Enable the base band low pass filter functionality</td>
    </tr>
</table> */
typedef LVM_UINT16 LVWBE_ModeWord_bm;
/**
* @def LVWBE_UPPER_BAND_CONTENT_DETECTOR
* Enable the upper band content detector functionality
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_UPPER_BAND_CONTENT_DETECTOR ((LVWBE_ModeWord_bm)1)
/**
* @def LVWBE_POSTPROCESSOR
* Enable the post processor functionality
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_POSTPROCESSOR ((LVWBE_ModeWord_bm)2)
/**
* @def LVWBE_MID_BAND_COMPENSATION
* Enable the mid band compensation functionality
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_MID_BAND_COMPENSATION ((LVWBE_ModeWord_bm)4)
/**
* @def LVWBE_TUNING_TONE_6KHZ
* Output tone of 6 kHz for integration testing
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_TUNING_TONE_6KHZ ((LVWBE_ModeWord_bm)8)
/**
* @def LVWBE_TUNING_TONE_UPPER_BAND_CONTENT_DETECTOR
* Output tone of 7 kHz for integration testing when no upperband content detected
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_TUNING_TONE_UPPER_BAND_CONTENT_DETECTOR ((LVWBE_ModeWord_bm)16)
/**
* @def LVWBE_UPPER_BAND_SNR_GAIN
* Enable the SNR-dependent upper band gain functionality
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_UPPER_BAND_SNR_GAIN ((LVWBE_ModeWord_bm)32)
/**
* @def LVWBE_BASE_BAND_LPF
* Enable the base band low pass filter functionality
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_BASE_BAND_LPF ((LVWBE_ModeWord_bm)64)
/**
* @def LVWBE_MODEWORD_BM_MIN
* Minimal value for LVWBE_ModeWord_bm
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_MODEWORD_BM_MIN ((LVWBE_ModeWord_bm)0)
/**
* @def LVWBE_MODEWORD_BM_MAX
* Maximal value for LVWBE_ModeWord_bm
* @see LVWBE_ModeWord_bm
*/
#define LVWBE_MODEWORD_BM_MAX ((LVWBE_ModeWord_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_AVL</td>
        <td>1</td>
        <td>Turns on or off the AVL. If off, both AGC and Limiter are bypassed</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_SPDETECT</td>
        <td>2</td>
        <td>Allows disabling speech detection in the AVL. When turned off, the AGC adapts its gain all the time.</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_LIMITER</td>
        <td>4</td>
        <td>Turns on or off the limiter.</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_EXT_SPDETECT</td>
        <td>8</td>
        <td>Used to tell the WhisperMode module if an external speech detection is available (If 0, there is no external speech detector).</td>
    </tr>
</table> */
typedef LVM_UINT16 LVWM_ModeWord_bm;
/**
* @def LVWM_MODE_AVL
* Turns on or off the AVL. If off, both AGC and Limiter are bypassed
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_AVL ((LVWM_ModeWord_bm)1)
/**
* @def LVWM_MODE_SPDETECT
* Allows disabling speech detection in the AVL. When turned off, the AGC adapts its gain all the time.
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_SPDETECT ((LVWM_ModeWord_bm)2)
/**
* @def LVWM_MODE_LIMITER
* Turns on or off the limiter.
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_LIMITER ((LVWM_ModeWord_bm)4)
/**
* @def LVWM_MODE_EXT_SPDETECT
* Used to tell the WhisperMode module if an external speech detection is available (If 0, there is no external speech detector).
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_EXT_SPDETECT ((LVWM_ModeWord_bm)8)
/**
* @def LVWM_MODEWORD_BM_MIN
* Minimal value for LVWM_ModeWord_bm
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODEWORD_BM_MIN ((LVWM_ModeWord_bm)0)
/**
* @def LVWM_MODEWORD_BM_MAX
* Maximal value for LVWM_ModeWord_bm
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODEWORD_BM_MAX ((LVWM_ModeWord_bm)((1) | (2) | (4) | (8) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NLMS</td>
        <td>1</td>
        <td>AEC NLMS functionality applied on all microphones</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FSB</td>
        <td>4</td>
        <td>Beamformer functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PCD</td>
        <td>8</td>
        <td>Path Change Detection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_GSC</td>
        <td>16</td>
        <td>Generalized Sidelobe Canceller functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DNNS</td>
        <td>32</td>
        <td>Dynamic Non-stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DES</td>
        <td>64</td>
        <td>Dynamic Echo Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NS</td>
        <td>128</td>
        <td>Stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NS_N</td>
        <td>256</td>
        <td>Non-stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PREEMPH</td>
        <td>512</td>
        <td>Pre- and de-emphasis functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DNNS_NGS</td>
        <td>1024</td>
        <td>Natural Gain Smoothing functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_CAL</td>
        <td>2048</td>
        <td>MICPOW Calibration functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PCD_DT</td>
        <td>4096</td>
        <td>Preserve Double Talk after PCD in speakerphone config.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_BODYNOISE_REM</td>
        <td>8192</td>
        <td>Body Noise Removal functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_CLIPPED_ECHO</td>
        <td>16384</td>
        <td>Clipped echo support</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_IMCOHFASTFLOOR</td>
        <td>32768</td>
        <td>Imcoh Fast Floor functionality. Should be disabled by default.</td>
    </tr>
</table> */
typedef LVM_UINT16 LVNV_ModeWord_bm;
/**
* @def LVNV_MODE_NLMS
* AEC NLMS functionality applied on all microphones
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_NLMS ((LVNV_ModeWord_bm)1)
/**
* @def LVNV_MODE_FSB
* Beamformer functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_FSB ((LVNV_ModeWord_bm)4)
/**
* @def LVNV_MODE_PCD
* Path Change Detection functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PCD ((LVNV_ModeWord_bm)8)
/**
* @def LVNV_MODE_GSC
* Generalized Sidelobe Canceller functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_GSC ((LVNV_ModeWord_bm)16)
/**
* @def LVNV_MODE_DNNS
* Dynamic Non-stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DNNS ((LVNV_ModeWord_bm)32)
/**
* @def LVNV_MODE_DES
* Dynamic Echo Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DES ((LVNV_ModeWord_bm)64)
/**
* @def LVNV_MODE_NS
* Stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_NS ((LVNV_ModeWord_bm)128)
/**
* @def LVNV_MODE_NS_N
* Non-stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_NS_N ((LVNV_ModeWord_bm)256)
/**
* @def LVNV_MODE_PREEMPH
* Pre- and de-emphasis functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PREEMPH ((LVNV_ModeWord_bm)512)
/**
* @def LVNV_MODE_DNNS_NGS
* Natural Gain Smoothing functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DNNS_NGS ((LVNV_ModeWord_bm)1024)
/**
* @def LVNV_MODE_CAL
* MICPOW Calibration functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_CAL ((LVNV_ModeWord_bm)2048)
/**
* @def LVNV_MODE_PCD_DT
* Preserve Double Talk after PCD in speakerphone config.
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PCD_DT ((LVNV_ModeWord_bm)4096)
/**
* @def LVNV_MODE_BODYNOISE_REM
* Body Noise Removal functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_BODYNOISE_REM ((LVNV_ModeWord_bm)8192)
/**
* @def LVNV_MODE_CLIPPED_ECHO
* Clipped echo support
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_CLIPPED_ECHO ((LVNV_ModeWord_bm)16384)
/**
* @def LVNV_MODE_IMCOHFASTFLOOR
* Imcoh Fast Floor functionality. Should be disabled by default.
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_IMCOHFASTFLOOR ((LVNV_ModeWord_bm)32768)
/**
* @def LVNV_MODEWORD_BM_MIN
* Minimal value for LVNV_ModeWord_bm
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODEWORD_BM_MIN ((LVNV_ModeWord_bm)0)
/**
* @def LVNV_MODEWORD_BM_MAX
* Maximal value for LVNV_ModeWord_bm
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODEWORD_BM_MAX ((LVNV_ModeWord_bm)((1) | (4) | (8) | (16) | (32) | (64) | (128) | (256) | (512) | (1024) | (2048) | (4096) | (8192) | (16384) | (32768) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_WNS</td>
        <td>1</td>
        <td>Wind Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_OUTPUT_HPF</td>
        <td>2</td>
        <td>High-pass filter at output functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FSB_HPF</td>
        <td>4</td>
        <td>FSB High-pass filter functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_SSRC_HEADROOM</td>
        <td>8</td>
        <td>SSRC headroom functionality</td>
    </tr>
</table> */
typedef LVM_UINT16 LVNV_ModeWord2_bm;
/**
* @def LVNV_MODE_WNS
* Wind Noise Suppression functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_WNS ((LVNV_ModeWord2_bm)1)
/**
* @def LVNV_MODE_OUTPUT_HPF
* High-pass filter at output functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_OUTPUT_HPF ((LVNV_ModeWord2_bm)2)
/**
* @def LVNV_MODE_FSB_HPF
* FSB High-pass filter functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_FSB_HPF ((LVNV_ModeWord2_bm)4)
/**
* @def LVNV_MODE_SSRC_HEADROOM
* SSRC headroom functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_SSRC_HEADROOM ((LVNV_ModeWord2_bm)8)
/**
* @def LVNV_MODEWORD2_BM_MIN
* Minimal value for LVNV_ModeWord2_bm
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODEWORD2_BM_MIN ((LVNV_ModeWord2_bm)0)
/**
* @def LVNV_MODEWORD2_BM_MAX
* Maximal value for LVNV_ModeWord2_bm
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODEWORD2_BM_MAX ((LVNV_ModeWord2_bm)((1) | (2) | (4) | (8) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_NLMS1</td>
        <td>1</td>
        <td>NLMS residual signal of second microphone (wideband synthesis is applied on the lowband with highband for 16 kHz)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_BMF_REF</td>
        <td>2</td>
        <td>Beamformer noise reference signal (wideband synthesis is applied on the lowband with highband for 16 kHz)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_FSB_OUT</td>
        <td>4</td>
        <td>FSB output signal (before wideband synthesis the highband is muted)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_SPDET_NE</td>
        <td>8</td>
        <td>Tuning beep when near end speech detector results in FSB updates</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_SPDET_DNNS</td>
        <td>16</td>
        <td>Tuning beep when speech activity is detected in DNNS</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_PCD</td>
        <td>32</td>
        <td>Tuning beep when when path change is detected</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_CAL_MICPOW</td>
        <td>64</td>
        <td>Tuning beep when MicPow is lower than Cal_micPowFloorMin</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_MONODET</td>
        <td>128</td>
        <td>Tuning beep when monaural detection (earbud removal) in stereo headset config or microphone coverage in handset config</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_INITCVGCONTROL</td>
        <td>256</td>
        <td>Disable Initial Convergence Control for AEC and DNNS</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_HB_MUTE</td>
        <td>512</td>
        <td>Mute high band (4kHz-8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_TB_MUTE</td>
        <td>1024</td>
        <td>Mute top band (above 8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_SPLITBAND_OFF</td>
        <td>2048</td>
        <td>Disable effect of LF parameters for echo subtraction</td>
    </tr>
</table> */
typedef LVM_UINT16 LVNV_TuningModeWord_bm;
/**
* @def TUNING_MODE_NLMS1
* NLMS residual signal of second microphone (wideband synthesis is applied on the lowband with highband for 16 kHz)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_NLMS1 ((LVNV_TuningModeWord_bm)1)
/**
* @def TUNING_MODE_BMF_REF
* Beamformer noise reference signal (wideband synthesis is applied on the lowband with highband for 16 kHz)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_BMF_REF ((LVNV_TuningModeWord_bm)2)
/**
* @def TUNING_MODE_FSB_OUT
* FSB output signal (before wideband synthesis the highband is muted)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_FSB_OUT ((LVNV_TuningModeWord_bm)4)
/**
* @def TUNING_MODE_SPDET_NE
* Tuning beep when near end speech detector results in FSB updates
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_SPDET_NE ((LVNV_TuningModeWord_bm)8)
/**
* @def TUNING_MODE_SPDET_DNNS
* Tuning beep when speech activity is detected in DNNS
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_SPDET_DNNS ((LVNV_TuningModeWord_bm)16)
/**
* @def TUNING_MODE_PCD
* Tuning beep when when path change is detected
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_PCD ((LVNV_TuningModeWord_bm)32)
/**
* @def TUNING_MODE_CAL_MICPOW
* Tuning beep when MicPow is lower than Cal_micPowFloorMin
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_CAL_MICPOW ((LVNV_TuningModeWord_bm)64)
/**
* @def TUNING_MODE_MONODET
* Tuning beep when monaural detection (earbud removal) in stereo headset config or microphone coverage in handset config
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_MONODET ((LVNV_TuningModeWord_bm)128)
/**
* @def TUNING_MODE_INITCVGCONTROL
* Disable Initial Convergence Control for AEC and DNNS
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_INITCVGCONTROL ((LVNV_TuningModeWord_bm)256)
/**
* @def TUNING_MODE_HB_MUTE
* Mute high band (4kHz-8kHz). Should be disabled by default.
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_HB_MUTE ((LVNV_TuningModeWord_bm)512)
/**
* @def TUNING_MODE_TB_MUTE
* Mute top band (above 8kHz). Should be disabled by default.
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_TB_MUTE ((LVNV_TuningModeWord_bm)1024)
/**
* @def TUNING_MODE_SPLITBAND_OFF
* Disable effect of LF parameters for echo subtraction
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_SPLITBAND_OFF ((LVNV_TuningModeWord_bm)2048)
/**
* @def LVNV_TUNINGMODEWORD_BM_MIN
* Minimal value for LVNV_TuningModeWord_bm
* @see LVNV_TuningModeWord_bm
*/
#define LVNV_TUNINGMODEWORD_BM_MIN ((LVNV_TuningModeWord_bm)0)
/**
* @def LVNV_TUNINGMODEWORD_BM_MAX
* Maximal value for LVNV_TuningModeWord_bm
* @see LVNV_TuningModeWord_bm
*/
#define LVNV_TUNINGMODEWORD_BM_MAX ((LVNV_TuningModeWord_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (128) | (256) | (512) | (1024) | (2048) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_WNSINPUT</td>
        <td>1</td>
        <td>Use this microphone as input to WNS</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_FSBOUT</td>
        <td>2</td>
        <td>This microphone contributes to FSB Output</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_FSBNOISEREF</td>
        <td>8</td>
        <td>Use this microphone FSB noise reference as input to the GSC</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_GSCINPUT</td>
        <td>16</td>
        <td>Use this microphone as GSC speech reference input (only supported for primary microphone)</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_SDET</td>
        <td>64</td>
        <td>Use this microphone in speech detector</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW</td>
        <td>4096</td>
        <td>Order of preference when microphone(s) are covered</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH</td>
        <td>8192</td>
        <td>Order of preference when microphone(s) are covered</td>
    </tr>
</table> */
typedef LVM_UINT16 LVNV_MicrophoneControlWord_bm;
/**
* @def LVNV_MICROPHONE_CONTROL_WNSINPUT
* Use this microphone as input to WNS
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_WNSINPUT ((LVNV_MicrophoneControlWord_bm)1)
/**
* @def LVNV_MICROPHONE_CONTROL_FSBOUT
* This microphone contributes to FSB Output
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_FSBOUT ((LVNV_MicrophoneControlWord_bm)2)
/**
* @def LVNV_MICROPHONE_CONTROL_FSBNOISEREF
* Use this microphone FSB noise reference as input to the GSC
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_FSBNOISEREF ((LVNV_MicrophoneControlWord_bm)8)
/**
* @def LVNV_MICROPHONE_CONTROL_GSCINPUT
* Use this microphone as GSC speech reference input (only supported for primary microphone)
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_GSCINPUT ((LVNV_MicrophoneControlWord_bm)16)
/**
* @def LVNV_MICROPHONE_CONTROL_SDET
* Use this microphone in speech detector
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_SDET ((LVNV_MicrophoneControlWord_bm)64)
/**
* @def LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW
* Order of preference when microphone(s) are covered
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW ((LVNV_MicrophoneControlWord_bm)4096)
/**
* @def LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH
* Order of preference when microphone(s) are covered
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH ((LVNV_MicrophoneControlWord_bm)8192)
/**
* @def LVNV_MICROPHONECONTROLWORD_BM_MIN
* Minimal value for LVNV_MicrophoneControlWord_bm
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONECONTROLWORD_BM_MIN ((LVNV_MicrophoneControlWord_bm)0)
/**
* @def LVNV_MICROPHONECONTROLWORD_BM_MAX
* Maximal value for LVNV_MicrophoneControlWord_bm
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONECONTROLWORD_BM_MAX ((LVNV_MicrophoneControlWord_bm)((1) | (2) | (8) | (16) | (64) | (4096) | (8192) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_DNNS_ACTIVITY</td>
        <td>1</td>
        <td>DNNS activity flag</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_MICPOWABOVEINST</td>
        <td>2</td>
        <td>MicPow above instrumental noise detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_CALREADY</td>
        <td>4</td>
        <td>Flag to indicate Calibration is ready</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_PCD</td>
        <td>8</td>
        <td>Flag to indicate path change detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_AEC_TDFARSPEAKS</td>
        <td>16</td>
        <td>Flag to indicate farend speech detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_MONAURALDET</td>
        <td>32</td>
        <td>Flag to indicate monaural detection (earbud removal) in stereo headset config</td>
    </tr>
</table> */
typedef LVM_UINT16 LVNV_StatusWord_bm;
/**
* @def LVNV_STATUS_DNNS_ACTIVITY
* DNNS activity flag
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_DNNS_ACTIVITY ((LVNV_StatusWord_bm)1)
/**
* @def LVNV_STATUS_MICPOWABOVEINST
* MicPow above instrumental noise detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_MICPOWABOVEINST ((LVNV_StatusWord_bm)2)
/**
* @def LVNV_STATUS_CALREADY
* Flag to indicate Calibration is ready
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_CALREADY ((LVNV_StatusWord_bm)4)
/**
* @def LVNV_STATUS_PCD
* Flag to indicate path change detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_PCD ((LVNV_StatusWord_bm)8)
/**
* @def LVNV_STATUS_AEC_TDFARSPEAKS
* Flag to indicate farend speech detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_AEC_TDFARSPEAKS ((LVNV_StatusWord_bm)16)
/**
* @def LVNV_STATUS_MONAURALDET
* Flag to indicate monaural detection (earbud removal) in stereo headset config
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_MONAURALDET ((LVNV_StatusWord_bm)32)
/**
* @def LVNV_STATUSWORD_BM_MIN
* Minimal value for LVNV_StatusWord_bm
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUSWORD_BM_MIN ((LVNV_StatusWord_bm)0)
/**
* @def LVNV_STATUSWORD_BM_MAX
* Maximal value for LVNV_StatusWord_bm
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUSWORD_BM_MAX ((LVNV_StatusWord_bm)((1) | (2) | (4) | (8) | (16) | (32) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_PATHCHANGE</td>
        <td>1</td>
        <td>PCD tuning beep</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_CLIP</td>
        <td>2</td>
        <td>SPDET_x_clip tuning beep</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_FAR_SPEAKS</td>
        <td>4</td>
        <td>SPDET_far tuning beep</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_MIC_SPEAKS</td>
        <td>8</td>
        <td>SPDET_mic tuning beep</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_HB_MUTE</td>
        <td>16</td>
        <td>Mute high band (4kHz-8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_TB_MUTE</td>
        <td>32</td>
        <td>Mute top band (above 8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVHF_CONTROL_TUNE_SPLITBAND_OFF</td>
        <td>64</td>
        <td>Disable effect of LF parameters for echo subtraction</td>
    </tr>
</table> */
typedef LVM_UINT16 LVHF_TuningModeWord;
/**
* @def LVHF_CONTROL_TUNE_PATHCHANGE
* PCD tuning beep
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_PATHCHANGE ((LVHF_TuningModeWord)1)
/**
* @def LVHF_CONTROL_TUNE_CLIP
* SPDET_x_clip tuning beep
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_CLIP ((LVHF_TuningModeWord)2)
/**
* @def LVHF_CONTROL_TUNE_FAR_SPEAKS
* SPDET_far tuning beep
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_FAR_SPEAKS ((LVHF_TuningModeWord)4)
/**
* @def LVHF_CONTROL_TUNE_MIC_SPEAKS
* SPDET_mic tuning beep
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_MIC_SPEAKS ((LVHF_TuningModeWord)8)
/**
* @def LVHF_CONTROL_TUNE_HB_MUTE
* Mute high band (4kHz-8kHz). Should be disabled by default.
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_HB_MUTE ((LVHF_TuningModeWord)16)
/**
* @def LVHF_CONTROL_TUNE_TB_MUTE
* Mute top band (above 8kHz). Should be disabled by default.
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_TB_MUTE ((LVHF_TuningModeWord)32)
/**
* @def LVHF_CONTROL_TUNE_SPLITBAND_OFF
* Disable effect of LF parameters for echo subtraction
* @see LVHF_TuningModeWord
*/
#define LVHF_CONTROL_TUNE_SPLITBAND_OFF ((LVHF_TuningModeWord)64)
/**
* @def LVHF_TUNINGMODEWORD_MIN
* Minimal value for LVHF_TuningModeWord
* @see LVHF_TuningModeWord
*/
#define LVHF_TUNINGMODEWORD_MIN ((LVHF_TuningModeWord)0)
/**
* @def LVHF_TUNINGMODEWORD_MAX
* Maximal value for LVHF_TuningModeWord
* @see LVHF_TuningModeWord
*/
#define LVHF_TUNINGMODEWORD_MAX ((LVHF_TuningModeWord)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_NLMS</td>
        <td>1</td>
        <td>NLMS functionality</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_DES</td>
        <td>2</td>
        <td>DES (Dynamic Echo Suppression)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_NS</td>
        <td>4</td>
        <td>NS (Noise Suppression)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_CNI</td>
        <td>8</td>
        <td>CNI (Comfort Noise Injection)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_NLES</td>
        <td>16</td>
        <td>NLES (Non Linear Echo Suppression)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_WNS</td>
        <td>32</td>
        <td>WNS (Wind Noise Suppression)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_PCD_DT</td>
        <td>64</td>
        <td>DT (Double Talk) protection after PCD</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_PCD</td>
        <td>128</td>
        <td>PCD (Path Change Detector)</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_INIT_ES</td>
        <td>256</td>
        <td>Initial echo suppression setting by PCD_gamma_e_rescue</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_SSRC_HEADROOM</td>
        <td>512</td>
        <td>SSRC headroom functionality</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_PRENLMS</td>
        <td>4096</td>
        <td>Pre-NLMS preprocessing</td>
    </tr>
    <tr>
        <td>@ref LVHF_MODE_CLIPPED_ECHO</td>
        <td>8192</td>
        <td>Clipped echo support</td>
    </tr>
</table> */
typedef LVM_UINT16 LVHF_ModeWord_bm;
/**
* @def LVHF_MODE_NLMS
* NLMS functionality
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_NLMS ((LVHF_ModeWord_bm)1)
/**
* @def LVHF_MODE_DES
* DES (Dynamic Echo Suppression)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_DES ((LVHF_ModeWord_bm)2)
/**
* @def LVHF_MODE_NS
* NS (Noise Suppression)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_NS ((LVHF_ModeWord_bm)4)
/**
* @def LVHF_MODE_CNI
* CNI (Comfort Noise Injection)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_CNI ((LVHF_ModeWord_bm)8)
/**
* @def LVHF_MODE_NLES
* NLES (Non Linear Echo Suppression)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_NLES ((LVHF_ModeWord_bm)16)
/**
* @def LVHF_MODE_WNS
* WNS (Wind Noise Suppression)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_WNS ((LVHF_ModeWord_bm)32)
/**
* @def LVHF_MODE_PCD_DT
* DT (Double Talk) protection after PCD
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_PCD_DT ((LVHF_ModeWord_bm)64)
/**
* @def LVHF_MODE_PCD
* PCD (Path Change Detector)
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_PCD ((LVHF_ModeWord_bm)128)
/**
* @def LVHF_MODE_INIT_ES
* Initial echo suppression setting by PCD_gamma_e_rescue
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_INIT_ES ((LVHF_ModeWord_bm)256)
/**
* @def LVHF_MODE_SSRC_HEADROOM
* SSRC headroom functionality
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_SSRC_HEADROOM ((LVHF_ModeWord_bm)512)
/**
* @def LVHF_MODE_PRENLMS
* Pre-NLMS preprocessing
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_PRENLMS ((LVHF_ModeWord_bm)4096)
/**
* @def LVHF_MODE_CLIPPED_ECHO
* Clipped echo support
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODE_CLIPPED_ECHO ((LVHF_ModeWord_bm)8192)
/**
* @def LVHF_MODEWORD_BM_MIN
* Minimal value for LVHF_ModeWord_bm
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODEWORD_BM_MIN ((LVHF_ModeWord_bm)0)
/**
* @def LVHF_MODEWORD_BM_MAX
* Maximal value for LVHF_ModeWord_bm
* @see LVHF_ModeWord_bm
*/
#define LVHF_MODEWORD_BM_MAX ((LVHF_ModeWord_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (128) | (256) | (512) | (4096) | (8192) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVHF_STATUS_FAR_SPEAKS</td>
        <td>1</td>
        <td>FarEnd is speaking</td>
    </tr>
    <tr>
        <td>@ref LVHF_STATUS_FAR_STARTS</td>
        <td>2</td>
        <td>FarEnd starts to speak</td>
    </tr>
    <tr>
        <td>@ref LVHF_STATUS_MIC_SPEAKS</td>
        <td>4</td>
        <td>Mic activity detected</td>
    </tr>
    <tr>
        <td>@ref LVHF_STATUS_CLIP</td>
        <td>8</td>
        <td>Clipping Detected</td>
    </tr>
    <tr>
        <td>@ref LVHF_STATUS_PATHCHANGE</td>
        <td>16</td>
        <td>Path Change detected</td>
    </tr>
</table> */
typedef LVM_UINT16 LVHF_StatusWord_bm;
/**
* @def LVHF_STATUS_FAR_SPEAKS
* FarEnd is speaking
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUS_FAR_SPEAKS ((LVHF_StatusWord_bm)1)
/**
* @def LVHF_STATUS_FAR_STARTS
* FarEnd starts to speak
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUS_FAR_STARTS ((LVHF_StatusWord_bm)2)
/**
* @def LVHF_STATUS_MIC_SPEAKS
* Mic activity detected
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUS_MIC_SPEAKS ((LVHF_StatusWord_bm)4)
/**
* @def LVHF_STATUS_CLIP
* Clipping Detected
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUS_CLIP ((LVHF_StatusWord_bm)8)
/**
* @def LVHF_STATUS_PATHCHANGE
* Path Change detected
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUS_PATHCHANGE ((LVHF_StatusWord_bm)16)
/**
* @def LVHF_STATUSWORD_BM_MIN
* Minimal value for LVHF_StatusWord_bm
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUSWORD_BM_MIN ((LVHF_StatusWord_bm)0)
/**
* @def LVHF_STATUSWORD_BM_MAX
* Maximal value for LVHF_StatusWord_bm
* @see LVHF_StatusWord_bm
*/
#define LVHF_STATUSWORD_BM_MAX ((LVHF_StatusWord_bm)((1) | (2) | (4) | (8) | (16) | (0)))

#define LVVIDHEADER_LVVE_RX_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Rx_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_RX_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Rx_Mask2_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_TX_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Tx_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_TX_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Tx_Mask2_bm in LVWireFormat.

#define LVVIDHEADER_CONFIGURATIONS_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_Configurations_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_CONFIGURATIONS_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_Configurations_Mask2_bm in LVWireFormat.

#define LVAVC_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVAVC_ModeWord_bm in LVWireFormat.

#define LVFENS_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVFENS_ModeWord_bm in LVWireFormat.

#define LVWBE_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVWBE_ModeWord_bm in LVWireFormat.

#define LVWM_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVWM_ModeWord_bm in LVWireFormat.

#define LVNV_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_ModeWord_bm in LVWireFormat.

#define LVNV_MODEWORD2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_ModeWord2_bm in LVWireFormat.

#define LVNV_TUNINGMODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_TuningModeWord_bm in LVWireFormat.

#define LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_MicrophoneControlWord_bm in LVWireFormat.

#define LVNV_STATUSWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_StatusWord_bm in LVWireFormat.

#define LVHF_TUNINGMODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVHF_TuningModeWord in LVWireFormat.

#define LVHF_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVHF_ModeWord_bm in LVWireFormat.

#define LVHF_STATUSWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVHF_StatusWord_bm in LVWireFormat.

#define LVM_MODE_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Mode_en in LVWireFormat.

#define LVM_CONFIG_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Config_en in LVWireFormat.

#define LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_ChannelType_en in LVWireFormat.

#define LVM_FS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Fs_en in LVWireFormat.

#define LVVIDHEADER_MESSAGEID_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVIDHeader_MessageID_en in LVWireFormat.

#define LVVIDHEADER_RETURNSTATUS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVIDHeader_ReturnStatus_en in LVWireFormat.

#define LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVBuffer_Channel_en in LVWireFormat.

#define LVVE_TX_OUTPUTCHANNEL_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVE_Tx_OutputChannel_en in LVWireFormat.

#define LVVIDHEADER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (8 \
 + LVVIDHEADER_LVVE_RX_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_RX_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_TX_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_TX_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_CONFIGURATIONS_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_CONFIGURATIONS_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_MESSAGEID_LVWIREFORMAT_LENGTH \
 + LVM_FS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVIDHeader ControlParameters in LVWireFormat.

#define LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTH (44 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVAVC_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVAVC ControlParameters in LVWireFormat.

#define LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTH (16 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVFENS_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVFENS ControlParameters in LVWireFormat.

#define LVWBE_CONTROLPARAMS_LVWIREFORMAT_LENGTH (16 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVWBE_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVWBE ControlParameters in LVWireFormat.

#define LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH (14 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVWM_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVWM ControlParameters in LVWireFormat.

#define LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH (8 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + 2*5 \
 + 2*5 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVDRC ControlParameters in LVWireFormat.

#define LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode LVCNG ControlParameters in LVWireFormat.

#define LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH (6 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + 2*5 \
 + 2*5) ///< Number of bytes to encode LVNG ControlParameters in LVWireFormat.

#define LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (6) ///< Number of bytes to encode LVNF ControlParameters in LVWireFormat.

#define LVNLPP_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode LVNLPP ControlParameters in LVWireFormat.

#define LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + 2*192) ///< Number of bytes to encode LVEQ ControlParameters in LVWireFormat.

#define LVVOL_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVOL ControlParameters in LVWireFormat.

#define LVHPF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVHPF ControlParameters in LVWireFormat.

#define LVMUTE_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVMUTE ControlParameters in LVWireFormat.

#define LVVE_RX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVWBE_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVNLPP_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Rx ControlParameters in LVWireFormat.

#define ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (18) ///< Number of bytes to encode AcousticEchoCanceller ControlParameters in LVWireFormat.

#define FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2*8) ///< Number of bytes to encode FsbChannel ControlParameters in LVWireFormat.

#define BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTH*2) ///< Number of bytes to encode Beamformer ControlParameters in LVWireFormat.

#define ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (48) ///< Number of bytes to encode EchoSuppression ControlParameters in LVWireFormat.

#define GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode GeneralSidelobeCanceller ControlParameters in LVWireFormat.

#define NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (28 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3) ///< Number of bytes to encode NoiseSuppression ControlParameters in LVWireFormat.

#define WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (16 \
 + 2*1 \
 + 2*2) ///< Number of bytes to encode WindNoiseSuppression ControlParameters in LVWireFormat.

#define PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH (12) ///< Number of bytes to encode PathChangeDetector ControlParameters in LVWireFormat.

#define SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH (16 \
 + 2*2) ///< Number of bytes to encode SpeechDetector ControlParameters in LVWireFormat.

#define LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_CONFIG_LVWIREFORMAT_LENGTH \
 + LVNV_MODEWORD_LVWIREFORMAT_LENGTH \
 + LVNV_MODEWORD2_LVWIREFORMAT_LENGTH \
 + LVNV_TUNINGMODEWORD_LVWIREFORMAT_LENGTH \
 + LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH*2 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH*2 \
 + 2*2 \
 + ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH*2 \
 + SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVNV ControlParameters in LVWireFormat.

#define LVHF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (110 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVHF_MODEWORD_LVWIREFORMAT_LENGTH \
 + LVHF_TUNINGMODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVHF ControlParameters in LVWireFormat.

#define LVBD_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVBD ControlParameters in LVWireFormat.

#define LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH \
 + LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH \
 + LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVINPUTROUTING_Tx ControlParameters in LVWireFormat.

#define LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVVE_TX_OUTPUTCHANNEL_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVOUTPUTROUTING_Tx ControlParameters in LVWireFormat.

#define LVLPF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVLPF ControlParameters in LVWireFormat.

#define LVVE_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (10 \
 + LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVHF_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Tx ControlParameters in LVWireFormat.

#define LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTH (6 \
 + LVAVC_MODEWORD_LVWIREFORMAT_LENGTH \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*8 \
 + 2*8) ///< Number of bytes to encode LVAVC StatusParameters in LVWireFormat.

#define LVWBE_STATUSPARAMS_LVWIREFORMAT_LENGTH (18) ///< Number of bytes to encode LVWBE StatusParameters in LVWireFormat.

#define LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode LVWM StatusParameters in LVWireFormat.

#define LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH (1 \
 + LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVWBE_STATUSPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Rx StatusParameters in LVWireFormat.

#define MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTH (4 \
 + 4*128 \
 + 4*128 \
 + 2*24) ///< Number of bytes to encode Microphone StatusParameters in LVWireFormat.

#define LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTH (18 \
 + LVNV_STATUSWORD_LVWIREFORMAT_LENGTH \
 + 4*64 \
 + 2*2 \
 + 2*2 \
 + MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTH*2) ///< Number of bytes to encode LVNV StatusParameters in LVWireFormat.

#define LVHF_STATUSPARAMS_LVWIREFORMAT_LENGTH (6 \
 + LVHF_STATUSWORD_LVWIREFORMAT_LENGTH \
 + 4*200 \
 + 4*136 \
 + 4*64) ///< Number of bytes to encode LVHF StatusParameters in LVWireFormat.

#define LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH (1 \
 + LVHF_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Tx StatusParameters in LVWireFormat.

/**
* @def LVVIDHEADER_HEADERVERSION_DEFAULT
* Default of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_DEFAULT (4)
/**
* @def LVVIDHEADER_HEADERVERSION_MIN
* MinValue of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_MIN (0)
/**
* @def LVVIDHEADER_HEADERVERSION_MAX
* MaxValue of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_MAX (32767)

/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT
* Default of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT (3)
/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_MIN
* MinValue of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_MAX
* MaxValue of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_MINORBASELINEVERSION_DEFAULT
* Default of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_DEFAULT (35)
/**
* @def LVVIDHEADER_MINORBASELINEVERSION_MIN
* MinValue of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MINORBASELINEVERSION_MAX
* MaxValue of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT
* Default of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT (04)
/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_MIN
* MinValue of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_MAX
* MaxValue of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT
* Default of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT (0)
/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_MIN
* MinValue of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_MAX
* MaxValue of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_LVVE_RX_ALGOMASK1_DEFAULT
* Default of LVVE_Rx_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_RX_ALGOMASK1_DEFAULT (LVVIDHEADER_LVVE_RX_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_RX_ALGOMASK2_DEFAULT
* Default of LVVE_Rx_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_RX_ALGOMASK2_DEFAULT (LVVIDHEADER_LVVE_RX_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_TX_ALGOMASK1_DEFAULT
* Default of LVVE_Tx_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_TX_ALGOMASK1_DEFAULT (LVVIDHEADER_LVVE_TX_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_TX_ALGOMASK2_DEFAULT
* Default of LVVE_Tx_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_TX_ALGOMASK2_DEFAULT (LVVIDHEADER_LVVE_TX_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_CONFIG_ALGOMASK1_DEFAULT
* Default of LVVE_Config_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_CONFIG_ALGOMASK1_DEFAULT (LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_CONFIG_ALGOMASK2_DEFAULT
* Default of LVVE_Config_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_CONFIG_ALGOMASK2_DEFAULT (LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_MESSAGEID_DEFAULT
* Default of MessageID
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MESSAGEID_DEFAULT (LVVE_RX_PRESET)

/**
* @def LVVIDHEADER_SAMPLERATE_DEFAULT
* Default of SampleRate
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_SAMPLERATE_DEFAULT (LVM_FS_8000)

/**
* @def LVVIDHEADER_VOLUMEINDEX_DEFAULT
* Default of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_DEFAULT (0)
/**
* @def LVVIDHEADER_VOLUMEINDEX_MIN
* MinValue of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_MIN (0)
/**
* @def LVVIDHEADER_VOLUMEINDEX_MAX
* MaxValue of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_MAX (255)

/**
* @def LVVIDHEADER_NUMVOLUMES_DEFAULT
* Default of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_DEFAULT (1)
/**
* @def LVVIDHEADER_NUMVOLUMES_MIN
* MinValue of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_MIN (0)
/**
* @def LVVIDHEADER_NUMVOLUMES_MAX
* MaxValue of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_MAX (255)

/**
* @def LVAVC_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVAVC_MODE_DEFAULT
* Default of Mode
* @see LVAVC_ControlParams_st
*/
#define LVAVC_MODE_DEFAULT (17)

/**
* @def LVAVC_GAIN_DEFAULT
* Default of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_DEFAULT (10)
/**
* @def LVAVC_GAIN_MIN
* MinValue of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_MIN (0)
/**
* @def LVAVC_GAIN_MAX
* MaxValue of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_MAX (30)

/**
* @def LVAVC_NOISEADAPTTH0_DEFAULT
* Default of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_DEFAULT (0)
/**
* @def LVAVC_NOISEADAPTTH0_MIN
* MinValue of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH0_MAX
* MaxValue of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_MAX (90)

/**
* @def LVAVC_NOISEADAPTTH1_DEFAULT
* Default of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_DEFAULT (40)
/**
* @def LVAVC_NOISEADAPTTH1_MIN
* MinValue of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH1_MAX
* MaxValue of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_MAX (90)

/**
* @def LVAVC_NOISEADAPTTH2_DEFAULT
* Default of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_DEFAULT (60)
/**
* @def LVAVC_NOISEADAPTTH2_MIN
* MinValue of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH2_MAX
* MaxValue of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_MAX (90)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT
* Default of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_MIN
* MinValue of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_MAX
* MaxValue of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT
* Default of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_MIN
* MinValue of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_MAX
* MaxValue of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT
* Default of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_MIN
* MinValue of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_MAX
* MaxValue of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT
* Default of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_MIN
* MinValue of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_MAX
* MaxValue of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT
* Default of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_MIN
* MinValue of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_MAX
* MaxValue of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT
* Default of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_MIN
* MinValue of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_MAX
* MaxValue of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT
* Default of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_MIN
* MinValue of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_MAX
* MaxValue of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_MAX (100)

/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT
* Default of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_MIN
* MinValue of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_MAX
* MaxValue of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_MAX (100)

/**
* @def LVAVC_NASEL_COMPKNEE1_DEFAULT
* Default of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_DEFAULT (60)
/**
* @def LVAVC_NASEL_COMPKNEE1_MIN
* MinValue of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE1_MAX
* MaxValue of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_MAX (90)

/**
* @def LVAVC_NASEL_COMPKNEE2_DEFAULT
* Default of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_DEFAULT (55)
/**
* @def LVAVC_NASEL_COMPKNEE2_MIN
* MinValue of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE2_MAX
* MaxValue of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_MAX (90)

/**
* @def LVAVC_NASEL_COMPKNEE3_DEFAULT
* Default of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_DEFAULT (50)
/**
* @def LVAVC_NASEL_COMPKNEE3_MIN
* MinValue of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE3_MAX
* MaxValue of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_MAX (90)

/**
* @def LVAVC_NASEL_COMPRATIO1_DEFAULT
* Default of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO1_MIN
* MinValue of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO1_MAX
* MaxValue of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_MAX (32767)

/**
* @def LVAVC_NASEL_COMPRATIO2_DEFAULT
* Default of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO2_MIN
* MinValue of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO2_MAX
* MaxValue of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_MAX (32767)

/**
* @def LVAVC_NASEL_COMPRATIO3_DEFAULT
* Default of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO3_MIN
* MinValue of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO3_MAX
* MaxValue of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_MAX (32767)

/**
* @def LVAVC_FU_MINGAIN_DEFAULT
* Default of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_DEFAULT (0)
/**
* @def LVAVC_FU_MINGAIN_MIN
* MinValue of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_MIN (0)
/**
* @def LVAVC_FU_MINGAIN_MAX
* MaxValue of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_MAX (54)

/**
* @def LVAVC_FU_MAXGAIN_DEFAULT
* Default of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_DEFAULT (10)
/**
* @def LVAVC_FU_MAXGAIN_MIN
* MinValue of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_MIN (0)
/**
* @def LVAVC_FU_MAXGAIN_MAX
* MaxValue of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_MAX (54)

/**
* @def LVAVC_LIMITER_THR_DEFAULT
* Default of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_DEFAULT (32767)
/**
* @def LVAVC_LIMITER_THR_MIN
* MinValue of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_MIN (0)
/**
* @def LVAVC_LIMITER_THR_MAX
* MaxValue of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_MAX (32767)

/**
* @def LVAVC_ADVANCED_CONFIG_DEFAULT
* Default of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_DEFAULT (0)
/**
* @def LVAVC_ADVANCED_CONFIG_MIN
* MinValue of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_MIN (0)
/**
* @def LVAVC_ADVANCED_CONFIG_MAX
* MaxValue of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_MAX (32767)

/**
* @def LVFENS_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVFENS_ControlParams_st
*/
#define LVFENS_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVFENS_MODE_DEFAULT
* Default of Mode
* @see LVFENS_ControlParams_st
*/
#define LVFENS_MODE_DEFAULT (1)

/**
* @def LVFENS_FENS_LIMIT_NS_DEFAULT
* Default of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_DEFAULT (10976)
/**
* @def LVFENS_FENS_LIMIT_NS_MIN
* MinValue of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_MIN (0)
/**
* @def LVFENS_FENS_LIMIT_NS_MAX
* MaxValue of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_MAX (32767)

/**
* @def LVFENS_FENS_NOISETHRESHOLD_DEFAULT
* Default of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_DEFAULT (96)
/**
* @def LVFENS_FENS_NOISETHRESHOLD_MIN
* MinValue of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_MIN (0)
/**
* @def LVFENS_FENS_NOISETHRESHOLD_MAX
* MaxValue of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_MAX (32767)

/**
* @def LVFENS_WNS_AGGRESSIVENESS_DEFAULT
* Default of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_DEFAULT (256)
/**
* @def LVFENS_WNS_AGGRESSIVENESS_MIN
* MinValue of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_MIN (0)
/**
* @def LVFENS_WNS_AGGRESSIVENESS_MAX
* MaxValue of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_MAX (32767)

/**
* @def LVFENS_WNS_POWERSCALEFACTOR_DEFAULT
* Default of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_DEFAULT (0)
/**
* @def LVFENS_WNS_POWERSCALEFACTOR_MIN
* MinValue of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_MIN (-96)
/**
* @def LVFENS_WNS_POWERSCALEFACTOR_MAX
* MaxValue of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_MAX (32)

/**
* @def LVFENS_WNS_MAXATTENUATION_DEFAULT
* Default of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_DEFAULT (8231)
/**
* @def LVFENS_WNS_MAXATTENUATION_MIN
* MinValue of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_MIN (0)
/**
* @def LVFENS_WNS_MAXATTENUATION_MAX
* MaxValue of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_MAX (32767)

/**
* @def LVFENS_WNS_HFSLOPE_DEFAULT
* Default of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_DEFAULT (-6)
/**
* @def LVFENS_WNS_HFSLOPE_MIN
* MinValue of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_MIN (-20)
/**
* @def LVFENS_WNS_HFSLOPE_MAX
* MaxValue of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_MAX (0)

/**
* @def LVFENS_WNS_GUSHTHRESHOLD_DEFAULT
* Default of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_DEFAULT (9830)
/**
* @def LVFENS_WNS_GUSHTHRESHOLD_MIN
* MinValue of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_MIN (0)
/**
* @def LVFENS_WNS_GUSHTHRESHOLD_MAX
* MaxValue of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_MAX (32767)

/**
* @def LVFENS_WNS_HPFCORNERFREQ_DEFAULT
* Default of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_DEFAULT (50)
/**
* @def LVFENS_WNS_HPFCORNERFREQ_MIN
* MinValue of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_MIN (0)
/**
* @def LVFENS_WNS_HPFCORNERFREQ_MAX
* MaxValue of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_MAX (1500)

/**
* @def LVWBE_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVWBE_ControlParams_st
*/
#define LVWBE_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVWBE_MODE_DEFAULT
* Default of Mode
* @see LVWBE_ControlParams_st
*/
#define LVWBE_MODE_DEFAULT (35)

/**
* @def LVWBE_MIDBANDGAIN_DEFAULT
* Default of MidBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_MIDBANDGAIN_DEFAULT (0)
/**
* @def LVWBE_MIDBANDGAIN_MIN
* MinValue of MidBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_MIDBANDGAIN_MIN (-90)
/**
* @def LVWBE_MIDBANDGAIN_MAX
* MaxValue of MidBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_MIDBANDGAIN_MAX (+12)

/**
* @def LVWBE_UPPERBANDGAIN_DEFAULT
* Default of UpperBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDGAIN_DEFAULT (-15)
/**
* @def LVWBE_UPPERBANDGAIN_MIN
* MinValue of UpperBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDGAIN_MIN (-90)
/**
* @def LVWBE_UPPERBANDGAIN_MAX
* MaxValue of UpperBandGain
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDGAIN_MAX (30)

/**
* @def LVWBE_UPPERBANDCORNERFREQ_DEFAULT
* Default of UpperBandCornerFreq
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDCORNERFREQ_DEFAULT (6000)
/**
* @def LVWBE_UPPERBANDCORNERFREQ_MIN
* MinValue of UpperBandCornerFreq
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDCORNERFREQ_MIN (4500)
/**
* @def LVWBE_UPPERBANDCORNERFREQ_MAX
* MaxValue of UpperBandCornerFreq
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDCORNERFREQ_MAX (7500)

/**
* @def LVWBE_UPPERBANDDETECTORTHRESHOLD_DEFAULT
* Default of UpperBandDetectorThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORTHRESHOLD_DEFAULT (-80)
/**
* @def LVWBE_UPPERBANDDETECTORTHRESHOLD_MIN
* MinValue of UpperBandDetectorThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORTHRESHOLD_MIN (-96)
/**
* @def LVWBE_UPPERBANDDETECTORTHRESHOLD_MAX
* MaxValue of UpperBandDetectorThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORTHRESHOLD_MAX (-60)

/**
* @def LVWBE_VOICEACTIVITYTHRESHOLD_DEFAULT
* Default of VoiceActivityThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VOICEACTIVITYTHRESHOLD_DEFAULT (19660)
/**
* @def LVWBE_VOICEACTIVITYTHRESHOLD_MIN
* MinValue of VoiceActivityThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VOICEACTIVITYTHRESHOLD_MIN (0)
/**
* @def LVWBE_VOICEACTIVITYTHRESHOLD_MAX
* MaxValue of VoiceActivityThreshold
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VOICEACTIVITYTHRESHOLD_MAX (32767)

/**
* @def LVWBE_POSTPROCESSORGAINMIN_DEFAULT
* Default of PostProcessorGainMin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINMIN_DEFAULT (-16)
/**
* @def LVWBE_POSTPROCESSORGAINMIN_MIN
* MinValue of PostProcessorGainMin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINMIN_MIN (-90)
/**
* @def LVWBE_POSTPROCESSORGAINMIN_MAX
* MaxValue of PostProcessorGainMin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINMIN_MAX (0)

/**
* @def LVWBE_POSTPROCESSORFREQHIGH_DEFAULT
* Default of PostProcessorFreqHigh
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQHIGH_DEFAULT (3800)
/**
* @def LVWBE_POSTPROCESSORFREQHIGH_MIN
* MinValue of PostProcessorFreqHigh
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQHIGH_MIN (3350)
/**
* @def LVWBE_POSTPROCESSORFREQHIGH_MAX
* MaxValue of PostProcessorFreqHigh
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQHIGH_MAX (4000)

/**
* @def LVWBE_POSTPROCESSORFREQLOW_DEFAULT
* Default of PostProcessorFreqLow
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQLOW_DEFAULT (500)
/**
* @def LVWBE_POSTPROCESSORFREQLOW_MIN
* MinValue of PostProcessorFreqLow
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQLOW_MIN (0)
/**
* @def LVWBE_POSTPROCESSORFREQLOW_MAX
* MaxValue of PostProcessorFreqLow
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORFREQLOW_MAX (2450)

/**
* @def LVWM_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVWM_ControlParams_st
*/
#define LVWM_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVWM_MODE_DEFAULT
* Default of mode
* @see LVWM_ControlParams_st
*/
#define LVWM_MODE_DEFAULT (7)

/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT
* Default of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT (16384)
/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_MIN
* MinValue of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_MIN (0)
/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_MAX
* MaxValue of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_MAX (32767)

/**
* @def LVWM_AVL_MINGAINLIN_DEFAULT
* Default of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_DEFAULT (128)
/**
* @def LVWM_AVL_MINGAINLIN_MIN
* MinValue of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_MIN (0)
/**
* @def LVWM_AVL_MINGAINLIN_MAX
* MaxValue of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_MAX (512)

/**
* @def LVWM_AVL_MAXGAINLIN_DEFAULT
* Default of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_DEFAULT (8189)
/**
* @def LVWM_AVL_MAXGAINLIN_MIN
* MinValue of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_MIN (512)
/**
* @def LVWM_AVL_MAXGAINLIN_MAX
* MaxValue of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_MAX (32767)

/**
* @def LVWM_AVL_ATTACK_DEFAULT
* Default of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_DEFAULT (25520)
/**
* @def LVWM_AVL_ATTACK_MIN
* MinValue of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_MIN (0)
/**
* @def LVWM_AVL_ATTACK_MAX
* MaxValue of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_MAX (32767)

/**
* @def LVWM_AVL_RELEASE_DEFAULT
* Default of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_DEFAULT (32685)
/**
* @def LVWM_AVL_RELEASE_MIN
* MinValue of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_MIN (0)
/**
* @def LVWM_AVL_RELEASE_MAX
* MaxValue of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_MAX (32767)

/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT
* Default of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT (23197)
/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN
* MinValue of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN (0)
/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX
* MaxValue of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX (32767)

/**
* @def LVWM_SPDETECT_THRESHOLD_DEFAULT
* Default of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_DEFAULT (9216)
/**
* @def LVWM_SPDETECT_THRESHOLD_MIN
* MinValue of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_MIN (0)
/**
* @def LVWM_SPDETECT_THRESHOLD_MAX
* MaxValue of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_MAX (32767)

/**
* @def LVDRC_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVDRC_ControlParams_st
*/
#define LVDRC_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVDRC_NUMKNEES_DEFAULT
* Default of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_DEFAULT (5)
/**
* @def LVDRC_NUMKNEES_MIN
* MinValue of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_MIN (1)
/**
* @def LVDRC_NUMKNEES_MAX
* MaxValue of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_MAX (5)

/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT
* Default of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-96,-70,-50,-24,0}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH
* Length of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH (5)

/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT
* Default of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-70,-38,-14,0}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH
* Length of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH (5)

/**
* @def LVDRC_ATTACKTIME_DEFAULT
* Default of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_DEFAULT (10)
/**
* @def LVDRC_ATTACKTIME_MIN
* MinValue of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_MIN (0)
/**
* @def LVDRC_ATTACKTIME_MAX
* MaxValue of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_MAX (32767)

/**
* @def LVDRC_RELEASETIME_DEFAULT
* Default of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_DEFAULT (2500)
/**
* @def LVDRC_RELEASETIME_MIN
* MinValue of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_MIN (0)
/**
* @def LVDRC_RELEASETIME_MAX
* MaxValue of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_MAX (32767)

/**
* @def LVDRC_LIMITEROPERATINGMODE_DEFAULT
* Default of LimiterOperatingMode
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITEROPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVDRC_LIMITLEVEL_DEFAULT
* Default of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_DEFAULT (0)
/**
* @def LVDRC_LIMITLEVEL_MIN
* MinValue of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_MIN (-96)
/**
* @def LVDRC_LIMITLEVEL_MAX
* MaxValue of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_MAX (0)

/**
* @def LVCNG_CNG_VOLUME_DEFAULT
* Default of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_DEFAULT (-90)
/**
* @def LVCNG_CNG_VOLUME_MIN
* MinValue of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_MIN (-96)
/**
* @def LVCNG_CNG_VOLUME_MAX
* MaxValue of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_MAX (-60)

/**
* @def LVNG_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVNG_ControlParams_st
*/
#define LVNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVNG_NUMKNEES_DEFAULT
* Default of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_DEFAULT (5)
/**
* @def LVNG_NUMKNEES_MIN
* MinValue of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_MIN (1)
/**
* @def LVNG_NUMKNEES_MAX
* MaxValue of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_MAX (5)

/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT
* Default of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-80,-70,-50,-24,0}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH
* Length of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH (5)

/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT
* Default of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-80,-50,-24,0}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH
* Length of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH (5)

/**
* @def LVNG_ATTACKTIME_DEFAULT
* Default of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_DEFAULT (50)
/**
* @def LVNG_ATTACKTIME_MIN
* MinValue of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_MIN (0)
/**
* @def LVNG_ATTACKTIME_MAX
* MaxValue of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_MAX (32767)

/**
* @def LVNG_RELEASETIME_DEFAULT
* Default of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_DEFAULT (50)
/**
* @def LVNG_RELEASETIME_MIN
* MinValue of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_MIN (0)
/**
* @def LVNG_RELEASETIME_MAX
* MaxValue of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_MAX (32767)

/**
* @def LVNF_NF_ATTENUATION_DEFAULT
* Default of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_DEFAULT (10)
/**
* @def LVNF_NF_ATTENUATION_MIN
* MinValue of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_MIN (0)
/**
* @def LVNF_NF_ATTENUATION_MAX
* MaxValue of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_MAX (24)

/**
* @def LVNF_NF_FREQUENCY_DEFAULT
* Default of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_DEFAULT (800)
/**
* @def LVNF_NF_FREQUENCY_MIN
* MinValue of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_MIN (400)
/**
* @def LVNF_NF_FREQUENCY_MAX
* MaxValue of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_MAX (1500)

/**
* @def LVNF_NF_QFACTOR_DEFAULT
* Default of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_DEFAULT (120)
/**
* @def LVNF_NF_QFACTOR_MIN
* MinValue of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_MIN (50)
/**
* @def LVNF_NF_QFACTOR_MAX
* MaxValue of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_MAX (500)

/**
* @def LVNLPP_NLPP_LIMIT_DEFAULT
* Default of NLPP_Limit
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_LIMIT_DEFAULT (0)
/**
* @def LVNLPP_NLPP_LIMIT_MIN
* MinValue of NLPP_Limit
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_LIMIT_MIN (-24)
/**
* @def LVNLPP_NLPP_LIMIT_MAX
* MaxValue of NLPP_Limit
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_LIMIT_MAX (0)

/**
* @def LVNLPP_NLPP_HPF_CORNERFREQ_DEFAULT
* Default of NLPP_HPF_CornerFreq
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVNLPP_NLPP_HPF_CORNERFREQ_MIN
* MinValue of NLPP_HPF_CornerFreq
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_HPF_CORNERFREQ_MIN (50)
/**
* @def LVNLPP_NLPP_HPF_CORNERFREQ_MAX
* MaxValue of NLPP_HPF_CornerFreq
* @see LVNLPP_ControlParams_st
*/
#define LVNLPP_NLPP_HPF_CORNERFREQ_MAX (1000)

/**
* @def LVEQ_EQ_LENGTH_DEFAULT
* Default of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_DEFAULT (192)
/**
* @def LVEQ_EQ_LENGTH_MIN
* MinValue of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_MIN (8)
/**
* @def LVEQ_EQ_LENGTH_MAX
* MaxValue of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_MAX (192)

/**
* @def LVEQ_EQ_COEFS_DEFAULT
* Default of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_DEFAULT {4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVEQ_EQ_COEFS_ARRAYMIN
* MinValue of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def LVEQ_EQ_COEFS_ARRAYMAX
* MaxValue of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVEQ_EQ_COEFS_LENGTH
* Length of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_LENGTH (192)

/**
* @def LVVOL_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVOL_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_DEFAULT (0)
/**
* @def LVVOL_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_MIN (-96)
/**
* @def LVVOL_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_MAX (24)

/**
* @def LVHPF_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVHPF_HPF_CORNERFREQ_DEFAULT
* Default of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVHPF_HPF_CORNERFREQ_MIN
* MinValue of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_MIN (50)
/**
* @def LVHPF_HPF_CORNERFREQ_MAX
* MaxValue of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_MAX (1500)

/**
* @def LVMUTE_MUTE_DEFAULT
* Default of Mute
* @see LVMUTE_ControlParams_st
*/
#define LVMUTE_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_OPERATINGMODE_DEFAULT (LVM_MODE_ON)

/**
* @def LVVE_RX_MUTE_DEFAULT
* Default of Mute
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_DEFAULT (0)
/**
* @def LVVE_RX_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_MIN (-96)
/**
* @def LVVE_RX_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_MAX (24)



/**
* @def LVVE_RX_NLPP_OPERATINGMODE_DEFAULT
* Default of NLPP_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_NLPP_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)



/**
* @def LVVE_RX_EQ_OPERATINGMODE_DEFAULT
* Default of EQ_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_EQ_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)



/**
* @def LVVE_RX_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_HPF_CORNERFREQ_DEFAULT
* Default of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVVE_RX_HPF_CORNERFREQ_MIN
* MinValue of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_MIN (50)
/**
* @def LVVE_RX_HPF_CORNERFREQ_MAX
* MaxValue of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_MAX (1500)

/**
* @def LVVE_RX_CNG_OPERATINGMODE_DEFAULT
* Default of CNG_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_CNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)




/**
* @def LVVE_RX_NF_OPERATINGMODE_DEFAULT
* Default of NF_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_NF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)


/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_DEFAULT
* Default of AEC_NLMS_LB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_DEFAULT (128)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MIN
* MinValue of AEC_NLMS_LB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MIN (16)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MAX
* MaxValue of AEC_NLMS_LB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MAX (128)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_DEFAULT
* Default of AEC_NLMS_LB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_DEFAULT (8192)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MIN
* MinValue of AEC_NLMS_LB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MIN (0)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MAX
* MaxValue of AEC_NLMS_LB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MAX (32767)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_DEFAULT
* Default of AEC_NLMS_LB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_DEFAULT (4096)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MIN
* MinValue of AEC_NLMS_LB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MIN (64)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MAX
* MaxValue of AEC_NLMS_LB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MAX (32767)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_DEFAULT
* Default of AEC_NLMS_HB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_DEFAULT (64)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MIN
* MinValue of AEC_NLMS_HB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MIN (16)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MAX
* MaxValue of AEC_NLMS_HB_taps
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MAX (128)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_DEFAULT
* Default of AEC_NLMS_HB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_DEFAULT (8192)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MIN
* MinValue of AEC_NLMS_HB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MIN (0)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MAX
* MaxValue of AEC_NLMS_HB_twoalpha
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MAX (32767)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_DEFAULT
* Default of AEC_NLMS_HB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_DEFAULT (2048)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MIN
* MinValue of AEC_NLMS_HB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MIN (64)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MAX
* MaxValue of AEC_NLMS_HB_erl
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MAX (32767)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_DEFAULT
* Default of AEC_NLMS_preset_coefs
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_DEFAULT (2)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MIN
* MinValue of AEC_NLMS_preset_coefs
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MIN (0)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MAX
* MaxValue of AEC_NLMS_preset_coefs
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MAX (2)

/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_DEFAULT
* Default of AEC_NLMS_offset
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_DEFAULT (100)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MIN
* MinValue of AEC_NLMS_offset
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MIN (0)
/**
* @def ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MAX
* MaxValue of AEC_NLMS_offset
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MAX (32767)

/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT
* Default of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT (2048)
/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN
* MinValue of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN (0)
/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX
* MaxValue of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX (32767)

/**
* @def FSBCHANNEL_FSB_INITTABLE_DEFAULT
* Default of FSB_InitTable
* @see FsbChannel_ControlParams_st
*/
#define FSBCHANNEL_FSB_INITTABLE_DEFAULT {0,0,0,0,10922,0,0,0}
/**
* @def FSBCHANNEL_FSB_INITTABLE_ARRAYMIN
* MinValue of FSB_InitTable
* @see FsbChannel_ControlParams_st
*/
#define FSBCHANNEL_FSB_INITTABLE_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def FSBCHANNEL_FSB_INITTABLE_ARRAYMAX
* MaxValue of FSB_InitTable
* @see FsbChannel_ControlParams_st
*/
#define FSBCHANNEL_FSB_INITTABLE_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def FSBCHANNEL_FSB_INITTABLE_LENGTH
* Length of FSB_InitTable
* @see FsbChannel_ControlParams_st
*/
#define FSBCHANNEL_FSB_INITTABLE_LENGTH (8)

/**
* @def BEAMFORMER_FSB_TAPS_DEFAULT
* Default of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_DEFAULT (16)
/**
* @def BEAMFORMER_FSB_TAPS_MIN
* MinValue of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_MIN (8)
/**
* @def BEAMFORMER_FSB_TAPS_MAX
* MaxValue of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_MAX (24)

/**
* @def BEAMFORMER_FSBCHANNEL_LENGTH
* Length of FsbChannel
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSBCHANNEL_LENGTH (2)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT
* Default of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT (450)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN
* MinValue of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX
* MaxValue of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT
* Default of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN
* MinValue of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX
* MaxValue of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT
* Default of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN
* MinValue of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX
* MaxValue of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT
* Default of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT (600)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN
* MinValue of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX
* MaxValue of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX (4000)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT
* Default of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN
* MinValue of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX
* MaxValue of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT
* Default of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT (450)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN
* MinValue of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX
* MaxValue of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT
* Default of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN
* MinValue of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX
* MaxValue of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT
* Default of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN
* MinValue of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX
* MaxValue of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT
* Default of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT (10000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN
* MinValue of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX
* MaxValue of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT
* Default of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT (8000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN
* MinValue of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX
* MaxValue of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT
* Default of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT (128)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN
* MinValue of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX
* MaxValue of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT
* Default of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN
* MinValue of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX
* MaxValue of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT
* Default of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN
* MinValue of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX
* MaxValue of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT
* Default of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN
* MinValue of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX
* MaxValue of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT
* Default of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN
* MinValue of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX
* MaxValue of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT
* Default of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN
* MinValue of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX
* MaxValue of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT
* Default of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT (64)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN
* MinValue of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX
* MaxValue of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_DEFAULT
* Default of DNNS_EchoScale_TB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_DEFAULT (32767)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MIN
* MinValue of DNNS_EchoScale_TB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MAX
* MaxValue of DNNS_EchoScale_TB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT
* Default of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN
* MinValue of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX
* MaxValue of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT
* Default of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN
* MinValue of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX
* MaxValue of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT
* Default of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT (32767)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN
* MinValue of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX
* MaxValue of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_DEFAULT
* Default of DNNS_GainEta
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_DEFAULT (1024)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_MIN
* MinValue of DNNS_GainEta
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_MAX
* MaxValue of DNNS_GainEta
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT
* Default of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT (512)
/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN
* MinValue of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX
* MaxValue of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT
* Default of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_MIN
* MinValue of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_MAX
* MaxValue of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_MAX (32767)

/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT
* Default of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT (16)
/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_MIN
* MinValue of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_MIN (8)
/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_MAX
* MaxValue of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_MAX (16)

/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT
* Default of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT (256)
/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_MIN
* MinValue of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_MIN (64)
/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_MAX
* MaxValue of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT
* Default of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT (8192)
/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN
* MinValue of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX
* MaxValue of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT
* Default of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN
* MinValue of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX
* MaxValue of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT
* Default of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN
* MinValue of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX
* MaxValue of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT
* Default of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN
* MinValue of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX
* MaxValue of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_DEFAULT
* Default of DNNS_UGF_MG_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_DEFAULT {200,4000,8000}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMIN
* MinValue of DNNS_UGF_MG_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMAX
* MaxValue of DNNS_UGF_MG_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMAX {8000,8000,8000}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_LENGTH
* Length of DNNS_UGF_MG_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_DEFAULT
* Default of DNNS_UGF_MG_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_DEFAULT {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMIN
* MinValue of DNNS_UGF_MG_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMIN {-5,-5,-5}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMAX
* MaxValue of DNNS_UGF_MG_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMAX {63,63,63}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_LENGTH
* Length of DNNS_UGF_MG_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_DEFAULT
* Default of DNNS_UGF_MG_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_DEFAULT {20,20,20}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMIN
* MinValue of DNNS_UGF_MG_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMIN {-5,-5,-5}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMAX
* MaxValue of DNNS_UGF_MG_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMAX {63,63,63}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_LENGTH
* Length of DNNS_UGF_MG_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_DEFAULT
* Default of DNNS_UGF_MG_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_DEFAULT {5827,5827,5827}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMIN
* MinValue of DNNS_UGF_MG_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMAX
* MaxValue of DNNS_UGF_MG_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMAX {32767,32767,32767}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_LENGTH
* Length of DNNS_UGF_MG_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_DEFAULT
* Default of DNNS_UGF_MG_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_DEFAULT {5827,5827,5827}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMIN
* MinValue of DNNS_UGF_MG_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMAX
* MaxValue of DNNS_UGF_MG_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMAX {32767,32767,32767}
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_LENGTH
* Length of DNNS_UGF_MG_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_MAX_DEFAULT
* Default of DNNS_UGF_MG_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_MAX_DEFAULT (16423)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_MAX_MIN
* MinValue of DNNS_UGF_MG_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_MAX_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_MAX_MAX
* MaxValue of DNNS_UGF_MG_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_MAX_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_REL_DEFAULT
* Default of DNNS_UGF_MG_rel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_REL_DEFAULT (9093)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_REL_MIN
* MinValue of DNNS_UGF_MG_rel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_REL_MIN (8192)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_REL_MAX
* MaxValue of DNNS_UGF_MG_rel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_REL_MAX (16384)

/**
* @def NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_DEFAULT
* Default of DNNS_GAINDET_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MIN
* MinValue of DNNS_GAINDET_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MAX
* MaxValue of DNNS_GAINDET_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_DEFAULT
* Default of DNNS_UGF_MG_NonStatRel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_DEFAULT (512)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MIN
* MinValue of DNNS_UGF_MG_NonStatRel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MIN (512)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MAX
* MaxValue of DNNS_UGF_MG_NonStatRel
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_DEFAULT
* Default of DNNS_UGF_MG_HNDST_NonStatRelBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_DEFAULT (512)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MIN
* MinValue of DNNS_UGF_MG_HNDST_NonStatRelBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MIN (512)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MAX
* MaxValue of DNNS_UGF_MG_HNDST_NonStatRelBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_DEFAULT
* Default of DNNS_UGF_MG_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_DEFAULT (2048)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MIN
* MinValue of DNNS_UGF_MG_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MIN (2048)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MAX
* MaxValue of DNNS_UGF_MG_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_DEFAULT
* Default of DNNS_UGF_MG_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_DEFAULT (5827)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MIN
* MinValue of DNNS_UGF_MG_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MAX
* MaxValue of DNNS_UGF_MG_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT
* Default of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT (819)
/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN
* MinValue of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX
* MaxValue of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX (1024)

/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT
* Default of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN
* MinValue of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN (-1024)
/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX
* MaxValue of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX (1024)

/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_DEFAULT
* Default of DNNS_SpeechRecovery
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_DEFAULT (32767)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MIN
* MinValue of DNNS_SpeechRecovery
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MAX
* MaxValue of DNNS_SpeechRecovery
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_DEFAULT
* Default of WNS_PowerScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_DEFAULT (0)
/**
* @def WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MIN
* MinValue of WNS_PowerScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MIN (-96)
/**
* @def WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MAX
* MaxValue of WNS_PowerScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MAX (32)

/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT
* Default of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT {12}
/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMIN
* MinValue of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMIN {0}
/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMAX
* MaxValue of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMAX {96}
/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_LENGTH
* Length of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_LENGTH (1)

/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT
* Default of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT (6554)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN
* MinValue of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX
* MaxValue of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_DEFAULT
* Default of WNS_DetectorMaxFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_DEFAULT (1000)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MIN
* MinValue of WNS_DetectorMaxFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MIN (350)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MAX
* MaxValue of WNS_DetectorMaxFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MAX (2000)

/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT
* Default of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT (50)
/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN
* MinValue of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX
* MaxValue of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX (1500)

/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT
* Default of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT (256)
/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN
* MinValue of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX
* MaxValue of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_MAXATTENUATION_DEFAULT
* Default of WNS_MaxAttenuation
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXATTENUATION_DEFAULT (8231)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MIN
* MinValue of WNS_MaxAttenuation
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MAX
* MaxValue of WNS_MaxAttenuation
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT
* Default of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT (9830)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN
* MinValue of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX
* MaxValue of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT
* Default of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT (-6)
/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN
* MinValue of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN (-20)
/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX
* MaxValue of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX (0)

/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT
* Default of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT {0,0}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN
* MinValue of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN {-96,-96}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX
* MaxValue of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX {32,32}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH
* Length of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH (2)

/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT
* Default of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT (32)
/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_MIN
* MinValue of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_MIN (16)
/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_MAX
* MaxValue of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_MAX (64)

/**
* @def PATHCHANGEDETECTOR_PCD_ERL_DEFAULT
* Default of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_DEFAULT (64)
/**
* @def PATHCHANGEDETECTOR_PCD_ERL_MIN
* MinValue of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_MIN (64)
/**
* @def PATHCHANGEDETECTOR_PCD_ERL_MAX
* MaxValue of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT
* Default of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT (5000)
/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN
* MinValue of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN (0)
/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX
* MaxValue of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT
* Default of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT (64)
/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN
* MinValue of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN (64)
/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX
* MaxValue of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT
* Default of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT (16800)
/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN
* MinValue of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN (16384)
/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX
* MaxValue of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT
* Default of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT (2048)
/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN
* MinValue of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN (0)
/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX
* MaxValue of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_DEFAULT
* Default of SDET_FarThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_DEFAULT (16384)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_MIN
* MinValue of SDET_FarThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_MAX
* MaxValue of SDET_FarThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT
* Default of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT (29)
/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN
* MinValue of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX
* MaxValue of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT
* Default of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT (11470)
/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN
* MinValue of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN (6554)
/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX
* MaxValue of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX (19661)

/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT
* Default of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT (32767)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN
* MinValue of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX
* MaxValue of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT
* Default of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT (60)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN
* MinValue of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX
* MaxValue of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT
* Default of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT (0)
/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN
* MinValue of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX
* MaxValue of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT
* Default of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT (41)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN
* MinValue of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX
* MaxValue of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT
* Default of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT (3840)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN
* MinValue of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX
* MaxValue of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT
* Default of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT {96,96}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN
* MinValue of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN {0,0}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX
* MaxValue of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX {96,96}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH
* Length of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH (2)

/**
* @def LVNV_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVNV_ControlParams_st
*/
#define LVNV_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVNV_CONFIG_DEFAULT
* Default of Config
* @see LVNV_ControlParams_st
*/
#define LVNV_CONFIG_DEFAULT (LVM_CONFIG_HANDSET)

/**
* @def LVNV_MODE_DEFAULT
* Default of Mode
* @see LVNV_ControlParams_st
*/
#define LVNV_MODE_DEFAULT (8191)

/**
* @def LVNV_MODE2_DEFAULT
* Default of Mode2
* @see LVNV_ControlParams_st
*/
#define LVNV_MODE2_DEFAULT (1)

/**
* @def LVNV_TUNINGMODE_DEFAULT
* Default of TuningMode
* @see LVNV_ControlParams_st
*/
#define LVNV_TUNINGMODE_DEFAULT (0)

/**
* @def LVNV_MICROPHONEMODE_DEFAULT
* Default of MicrophoneMode
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEMODE_DEFAULT {83,75}
/**
* @def LVNV_MICROPHONEMODE_LENGTH
* Length of MicrophoneMode
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEMODE_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONX_DEFAULT
* Default of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_DEFAULT {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONX_ARRAYMIN
* MinValue of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONX_ARRAYMAX
* MaxValue of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONX_LENGTH
* Length of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONY_DEFAULT
* Default of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_DEFAULT {0,90}
/**
* @def LVNV_MICROPHONEPOSITIONY_ARRAYMIN
* MinValue of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONY_ARRAYMAX
* MaxValue of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONY_LENGTH
* Length of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONZ_DEFAULT
* Default of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_DEFAULT {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONZ_ARRAYMIN
* MinValue of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONZ_ARRAYMAX
* MaxValue of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONZ_LENGTH
* Length of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_LENGTH (2)

/**
* @def LVNV_CHANNELTYPE_DEFAULT
* Default of ChannelType
* @see LVNV_ControlParams_st
*/
#define LVNV_CHANNELTYPE_DEFAULT {LVM_MICROPHONE_CHANNEL,LVM_MICROPHONE_CHANNEL}
/**
* @def LVNV_CHANNELTYPE_LENGTH
* Length of ChannelType
* @see LVNV_ControlParams_st
*/
#define LVNV_CHANNELTYPE_LENGTH (2)

/**
* @def LVNV_MICROPHONEINPUTGAIN_DEFAULT
* Default of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_DEFAULT {8192,8192}
/**
* @def LVNV_MICROPHONEINPUTGAIN_ARRAYMIN
* MinValue of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEINPUTGAIN_ARRAYMAX
* MaxValue of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_ARRAYMAX {32767,32767}
/**
* @def LVNV_MICROPHONEINPUTGAIN_LENGTH
* Length of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_LENGTH (2)

/**
* @def LVNV_OUTPUTGAIN_DEFAULT
* Default of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_DEFAULT (2048)
/**
* @def LVNV_OUTPUTGAIN_MIN
* MinValue of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_MIN (0)
/**
* @def LVNV_OUTPUTGAIN_MAX
* MaxValue of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_MAX (32767)

/**
* @def LVNV_AECPARAMS_LENGTH
* Length of AECParams
* @see LVNV_ControlParams_st
*/
#define LVNV_AECPARAMS_LENGTH (2)








/**
* @def LVHF_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVHF_ControlParams_st
*/
#define LVHF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVHF_MODE_DEFAULT
* Default of Mode
* @see LVHF_ControlParams_st
*/
#define LVHF_MODE_DEFAULT (4607)

/**
* @def LVHF_TUNINGMODE_DEFAULT
* Default of TuningMode
* @see LVHF_ControlParams_st
*/
#define LVHF_TUNINGMODE_DEFAULT (0)

/**
* @def LVHF_INPUTGAIN_DEFAULT
* Default of InputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_INPUTGAIN_DEFAULT (8192)
/**
* @def LVHF_INPUTGAIN_MIN
* MinValue of InputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_INPUTGAIN_MIN (0)
/**
* @def LVHF_INPUTGAIN_MAX
* MaxValue of InputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_INPUTGAIN_MAX (32767)

/**
* @def LVHF_OUTPUTGAIN_DEFAULT
* Default of OutputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_OUTPUTGAIN_DEFAULT (2048)
/**
* @def LVHF_OUTPUTGAIN_MIN
* MinValue of OutputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_OUTPUTGAIN_MIN (0)
/**
* @def LVHF_OUTPUTGAIN_MAX
* MaxValue of OutputGain
* @see LVHF_ControlParams_st
*/
#define LVHF_OUTPUTGAIN_MAX (32767)

/**
* @def LVHF_NLMS_LIMIT_DEFAULT
* Default of NLMS_limit
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LIMIT_DEFAULT (0)
/**
* @def LVHF_NLMS_LIMIT_MIN
* MinValue of NLMS_limit
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LIMIT_MIN (-24)
/**
* @def LVHF_NLMS_LIMIT_MAX
* MaxValue of NLMS_limit
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LIMIT_MAX (0)

/**
* @def LVHF_NLMS_LB_TAPS_DEFAULT
* Default of NLMS_LB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TAPS_DEFAULT (64)
/**
* @def LVHF_NLMS_LB_TAPS_MIN
* MinValue of NLMS_LB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TAPS_MIN (16)
/**
* @def LVHF_NLMS_LB_TAPS_MAX
* MaxValue of NLMS_LB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TAPS_MAX (200)

/**
* @def LVHF_NLMS_LB_TWO_ALPHA_DEFAULT
* Default of NLMS_LB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TWO_ALPHA_DEFAULT (8192)
/**
* @def LVHF_NLMS_LB_TWO_ALPHA_MIN
* MinValue of NLMS_LB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TWO_ALPHA_MIN (0)
/**
* @def LVHF_NLMS_LB_TWO_ALPHA_MAX
* MaxValue of NLMS_LB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_TWO_ALPHA_MAX (32767)

/**
* @def LVHF_NLMS_LB_ERL_DEFAULT
* Default of NLMS_LB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_ERL_DEFAULT (128)
/**
* @def LVHF_NLMS_LB_ERL_MIN
* MinValue of NLMS_LB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_ERL_MIN (64)
/**
* @def LVHF_NLMS_LB_ERL_MAX
* MaxValue of NLMS_LB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_ERL_MAX (32767)

/**
* @def LVHF_NLMS_HB_TAPS_DEFAULT
* Default of NLMS_HB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TAPS_DEFAULT (64)
/**
* @def LVHF_NLMS_HB_TAPS_MIN
* MinValue of NLMS_HB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TAPS_MIN (16)
/**
* @def LVHF_NLMS_HB_TAPS_MAX
* MaxValue of NLMS_HB_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TAPS_MAX (136)

/**
* @def LVHF_NLMS_HB_TWO_ALPHA_DEFAULT
* Default of NLMS_HB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TWO_ALPHA_DEFAULT (8192)
/**
* @def LVHF_NLMS_HB_TWO_ALPHA_MIN
* MinValue of NLMS_HB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TWO_ALPHA_MIN (0)
/**
* @def LVHF_NLMS_HB_TWO_ALPHA_MAX
* MaxValue of NLMS_HB_two_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_TWO_ALPHA_MAX (32767)

/**
* @def LVHF_NLMS_HB_ERL_DEFAULT
* Default of NLMS_HB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_ERL_DEFAULT (128)
/**
* @def LVHF_NLMS_HB_ERL_MIN
* MinValue of NLMS_HB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_ERL_MIN (64)
/**
* @def LVHF_NLMS_HB_ERL_MAX
* MaxValue of NLMS_HB_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_ERL_MAX (32767)

/**
* @def LVHF_NLMS_PRESET_COEFS_DEFAULT
* Default of NLMS_preset_coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_PRESET_COEFS_DEFAULT (1)
/**
* @def LVHF_NLMS_PRESET_COEFS_MIN
* MinValue of NLMS_preset_coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_PRESET_COEFS_MIN (0)
/**
* @def LVHF_NLMS_PRESET_COEFS_MAX
* MaxValue of NLMS_preset_coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_PRESET_COEFS_MAX (2)

/**
* @def LVHF_NLMS_OFFSET_DEFAULT
* Default of NLMS_offset
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_OFFSET_DEFAULT (767)
/**
* @def LVHF_NLMS_OFFSET_MIN
* MinValue of NLMS_offset
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_OFFSET_MIN (0)
/**
* @def LVHF_NLMS_OFFSET_MAX
* MaxValue of NLMS_offset
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_OFFSET_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_HIGH_LF_DEFAULT
* Default of DENS_gamma_e_high_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LF_DEFAULT (512)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_LF_MIN
* MinValue of DENS_gamma_e_high_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LF_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_LF_MAX
* MaxValue of DENS_gamma_e_high_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LF_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_DT_LF_DEFAULT
* Default of DENS_gamma_e_dt_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LF_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_DT_LF_MIN
* MinValue of DENS_gamma_e_dt_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LF_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_DT_LF_MAX
* MaxValue of DENS_gamma_e_dt_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LF_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_LOW_LF_DEFAULT
* Default of DENS_gamma_e_low_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LF_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_LOW_LF_MIN
* MinValue of DENS_gamma_e_low_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LF_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_LOW_LF_MAX
* MaxValue of DENS_gamma_e_low_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LF_MAX (32767)

/**
* @def LVHF_DENS_ECHO_CUTOFF_LF_DEFAULT
* Default of DENS_echo_cutoff_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CUTOFF_LF_DEFAULT (600)
/**
* @def LVHF_DENS_ECHO_CUTOFF_LF_MIN
* MinValue of DENS_echo_cutoff_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CUTOFF_LF_MIN (0)
/**
* @def LVHF_DENS_ECHO_CUTOFF_LF_MAX
* MaxValue of DENS_echo_cutoff_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CUTOFF_LF_MAX (4000)

/**
* @def LVHF_DENS_TAIL_ALPHA_LB_DEFAULT
* Default of DENS_tail_alpha_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_LB_DEFAULT (25395)
/**
* @def LVHF_DENS_TAIL_ALPHA_LB_MIN
* MinValue of DENS_tail_alpha_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_LB_MIN (0)
/**
* @def LVHF_DENS_TAIL_ALPHA_LB_MAX
* MaxValue of DENS_tail_alpha_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_LB_MAX (32767)

/**
* @def LVHF_DENS_TAIL_PORTION_LB_DEFAULT
* Default of DENS_tail_portion_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_LB_DEFAULT (29491)
/**
* @def LVHF_DENS_TAIL_PORTION_LB_MIN
* MinValue of DENS_tail_portion_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_LB_MIN (0)
/**
* @def LVHF_DENS_TAIL_PORTION_LB_MAX
* MaxValue of DENS_tail_portion_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_LB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_HIGH_LB_DEFAULT
* Default of DENS_gamma_e_high_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LB_DEFAULT (512)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_LB_MIN
* MinValue of DENS_gamma_e_high_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_LB_MAX
* MaxValue of DENS_gamma_e_high_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_LB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_DT_LB_DEFAULT
* Default of DENS_gamma_e_dt_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LB_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_DT_LB_MIN
* MinValue of DENS_gamma_e_dt_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_DT_LB_MAX
* MaxValue of DENS_gamma_e_dt_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_LB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_LOW_LB_DEFAULT
* Default of DENS_gamma_e_low_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LB_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_LOW_LB_MIN
* MinValue of DENS_gamma_e_low_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_LOW_LB_MAX
* MaxValue of DENS_gamma_e_low_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_LB_MAX (32767)

/**
* @def LVHF_DENS_NL_ATTEN_LB_DEFAULT
* Default of DENS_NL_atten_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LB_DEFAULT (0)
/**
* @def LVHF_DENS_NL_ATTEN_LB_MIN
* MinValue of DENS_NL_atten_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LB_MIN (0)
/**
* @def LVHF_DENS_NL_ATTEN_LB_MAX
* MaxValue of DENS_NL_atten_LB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LB_MAX (2048)

/**
* @def LVHF_DENS_TAIL_ALPHA_HB_DEFAULT
* Default of DENS_tail_alpha_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_HB_DEFAULT (25395)
/**
* @def LVHF_DENS_TAIL_ALPHA_HB_MIN
* MinValue of DENS_tail_alpha_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_HB_MIN (0)
/**
* @def LVHF_DENS_TAIL_ALPHA_HB_MAX
* MaxValue of DENS_tail_alpha_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_ALPHA_HB_MAX (32767)

/**
* @def LVHF_DENS_TAIL_PORTION_HB_DEFAULT
* Default of DENS_tail_portion_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_HB_DEFAULT (29491)
/**
* @def LVHF_DENS_TAIL_PORTION_HB_MIN
* MinValue of DENS_tail_portion_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_HB_MIN (0)
/**
* @def LVHF_DENS_TAIL_PORTION_HB_MAX
* MaxValue of DENS_tail_portion_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_TAIL_PORTION_HB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_HIGH_HB_DEFAULT
* Default of DENS_gamma_e_high_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_HB_DEFAULT (512)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_HB_MIN
* MinValue of DENS_gamma_e_high_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_HB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_HIGH_HB_MAX
* MaxValue of DENS_gamma_e_high_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_HIGH_HB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_DT_HB_DEFAULT
* Default of DENS_gamma_e_dt_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_HB_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_DT_HB_MIN
* MinValue of DENS_gamma_e_dt_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_HB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_DT_HB_MAX
* MaxValue of DENS_gamma_e_dt_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_DT_HB_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_LOW_HB_DEFAULT
* Default of DENS_gamma_e_low_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_HB_DEFAULT (256)
/**
* @def LVHF_DENS_GAMMA_E_LOW_HB_MIN
* MinValue of DENS_gamma_e_low_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_HB_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_LOW_HB_MAX
* MaxValue of DENS_gamma_e_low_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_LOW_HB_MAX (32767)

/**
* @def LVHF_DENS_NL_ATTEN_HB_DEFAULT
* Default of DENS_NL_atten_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_HB_DEFAULT (0)
/**
* @def LVHF_DENS_NL_ATTEN_HB_MIN
* MinValue of DENS_NL_atten_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_HB_MIN (0)
/**
* @def LVHF_DENS_NL_ATTEN_HB_MAX
* MaxValue of DENS_NL_atten_HB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_HB_MAX (2048)

/**
* @def LVHF_DENS_NL_ATTEN_LOW_DEFAULT
* Default of DENS_NL_atten_Low
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LOW_DEFAULT (0)
/**
* @def LVHF_DENS_NL_ATTEN_LOW_MIN
* MinValue of DENS_NL_atten_Low
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LOW_MIN (0)
/**
* @def LVHF_DENS_NL_ATTEN_LOW_MAX
* MaxValue of DENS_NL_atten_Low
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NL_ATTEN_LOW_MAX (2048)

/**
* @def LVHF_DENS_ECHO_SCALE_TB_DEFAULT
* Default of DENS_echo_scale_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_SCALE_TB_DEFAULT (32767)
/**
* @def LVHF_DENS_ECHO_SCALE_TB_MIN
* MinValue of DENS_echo_scale_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_SCALE_TB_MIN (0)
/**
* @def LVHF_DENS_ECHO_SCALE_TB_MAX
* MaxValue of DENS_echo_scale_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_SCALE_TB_MAX (32767)

/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_DEFAULT
* Default of DENS_echo_relax_NE_threshold_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_DEFAULT (0)
/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MIN
* MinValue of DENS_echo_relax_NE_threshold_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MIN (0)
/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MAX
* MaxValue of DENS_echo_relax_NE_threshold_LF
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MAX (32767)

/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_DEFAULT
* Default of DENS_echo_relax_NE_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_DEFAULT (0)
/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MIN
* MinValue of DENS_echo_relax_NE_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MIN (0)
/**
* @def LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MAX
* MaxValue of DENS_echo_relax_NE_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_E_ALPHA_DEFAULT
* Default of DENS_gamma_e_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_ALPHA_DEFAULT (24000)
/**
* @def LVHF_DENS_GAMMA_E_ALPHA_MIN
* MinValue of DENS_gamma_e_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_ALPHA_MIN (0)
/**
* @def LVHF_DENS_GAMMA_E_ALPHA_MAX
* MaxValue of DENS_gamma_e_alpha
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_E_ALPHA_MAX (32767)

/**
* @def LVHF_DENS_GAMMA_N_DEFAULT
* Default of DENS_gamma_n
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_N_DEFAULT (280)
/**
* @def LVHF_DENS_GAMMA_N_MIN
* MinValue of DENS_gamma_n
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_N_MIN (0)
/**
* @def LVHF_DENS_GAMMA_N_MAX
* MaxValue of DENS_gamma_n
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_GAMMA_N_MAX (32767)

/**
* @def LVHF_DENS_SPDET_NEAR_DEFAULT
* Default of DENS_spdet_near
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_NEAR_DEFAULT (512)
/**
* @def LVHF_DENS_SPDET_NEAR_MIN
* MinValue of DENS_spdet_near
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_NEAR_MIN (0)
/**
* @def LVHF_DENS_SPDET_NEAR_MAX
* MaxValue of DENS_spdet_near
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_NEAR_MAX (32767)

/**
* @def LVHF_DENS_SPDET_ACT_DEFAULT
* Default of DENS_spdet_act
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_ACT_DEFAULT (768)
/**
* @def LVHF_DENS_SPDET_ACT_MIN
* MinValue of DENS_spdet_act
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_ACT_MIN (0)
/**
* @def LVHF_DENS_SPDET_ACT_MAX
* MaxValue of DENS_spdet_act
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_SPDET_ACT_MAX (32767)

/**
* @def LVHF_DENS_LIMIT_NS_DEFAULT
* Default of DENS_limit_ns
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_DEFAULT (10361)
/**
* @def LVHF_DENS_LIMIT_NS_MIN
* MinValue of DENS_limit_ns
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_MIN (0)
/**
* @def LVHF_DENS_LIMIT_NS_MAX
* MaxValue of DENS_limit_ns
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_MAX (32767)

/**
* @def LVHF_DENS_LIMIT_NS_TB_DEFAULT
* Default of DENS_limit_ns_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_TB_DEFAULT (5827)
/**
* @def LVHF_DENS_LIMIT_NS_TB_MIN
* MinValue of DENS_limit_ns_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_TB_MIN (0)
/**
* @def LVHF_DENS_LIMIT_NS_TB_MAX
* MaxValue of DENS_limit_ns_TB
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_LIMIT_NS_TB_MAX (32767)

/**
* @def LVHF_DENS_CNI_GAIN_DEFAULT
* Default of DENS_CNI_Gain
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_CNI_GAIN_DEFAULT (16384)
/**
* @def LVHF_DENS_CNI_GAIN_MIN
* MinValue of DENS_CNI_Gain
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_CNI_GAIN_MIN (0)
/**
* @def LVHF_DENS_CNI_GAIN_MAX
* MaxValue of DENS_CNI_Gain
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_CNI_GAIN_MAX (32767)

/**
* @def LVHF_DENS_NFE_BLOCKSIZE_DEFAULT
* Default of DENS_NFE_blocksize
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NFE_BLOCKSIZE_DEFAULT (150)
/**
* @def LVHF_DENS_NFE_BLOCKSIZE_MIN
* MinValue of DENS_NFE_blocksize
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NFE_BLOCKSIZE_MIN (0)
/**
* @def LVHF_DENS_NFE_BLOCKSIZE_MAX
* MaxValue of DENS_NFE_blocksize
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_NFE_BLOCKSIZE_MAX (32767)

/**
* @def LVHF_DENS_ECHO_CLIPPING_LEVEL_DEFAULT
* Default of DENS_echo_clipping_level
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CLIPPING_LEVEL_DEFAULT (32767)
/**
* @def LVHF_DENS_ECHO_CLIPPING_LEVEL_MIN
* MinValue of DENS_echo_clipping_level
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CLIPPING_LEVEL_MIN (0)
/**
* @def LVHF_DENS_ECHO_CLIPPING_LEVEL_MAX
* MaxValue of DENS_echo_clipping_level
* @see LVHF_ControlParams_st
*/
#define LVHF_DENS_ECHO_CLIPPING_LEVEL_MAX (32767)

/**
* @def LVHF_WNS_AGGRESSIVENESS_DEFAULT
* Default of WNS_Aggressiveness
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_AGGRESSIVENESS_DEFAULT (256)
/**
* @def LVHF_WNS_AGGRESSIVENESS_MIN
* MinValue of WNS_Aggressiveness
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_AGGRESSIVENESS_MIN (0)
/**
* @def LVHF_WNS_AGGRESSIVENESS_MAX
* MaxValue of WNS_Aggressiveness
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_AGGRESSIVENESS_MAX (32767)

/**
* @def LVHF_WNS_POWERSCALEFACTOR_DEFAULT
* Default of WNS_PowerScaleFactor
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_POWERSCALEFACTOR_DEFAULT (0)
/**
* @def LVHF_WNS_POWERSCALEFACTOR_MIN
* MinValue of WNS_PowerScaleFactor
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_POWERSCALEFACTOR_MIN (-96)
/**
* @def LVHF_WNS_POWERSCALEFACTOR_MAX
* MaxValue of WNS_PowerScaleFactor
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_POWERSCALEFACTOR_MAX (32)

/**
* @def LVHF_WNS_MAXATTENUATION_DEFAULT
* Default of WNS_MaxAttenuation
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_MAXATTENUATION_DEFAULT (8231)
/**
* @def LVHF_WNS_MAXATTENUATION_MIN
* MinValue of WNS_MaxAttenuation
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_MAXATTENUATION_MIN (0)
/**
* @def LVHF_WNS_MAXATTENUATION_MAX
* MaxValue of WNS_MaxAttenuation
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_MAXATTENUATION_MAX (32767)

/**
* @def LVHF_WNS_HFSLOPE_DEFAULT
* Default of WNS_HFSlope
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HFSLOPE_DEFAULT (-6)
/**
* @def LVHF_WNS_HFSLOPE_MIN
* MinValue of WNS_HFSlope
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HFSLOPE_MIN (-20)
/**
* @def LVHF_WNS_HFSLOPE_MAX
* MaxValue of WNS_HFSlope
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HFSLOPE_MAX (0)

/**
* @def LVHF_WNS_GUSHTHRESHOLD_DEFAULT
* Default of WNS_GushThreshold
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_GUSHTHRESHOLD_DEFAULT (9830)
/**
* @def LVHF_WNS_GUSHTHRESHOLD_MIN
* MinValue of WNS_GushThreshold
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_GUSHTHRESHOLD_MIN (0)
/**
* @def LVHF_WNS_GUSHTHRESHOLD_MAX
* MaxValue of WNS_GushThreshold
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_GUSHTHRESHOLD_MAX (32767)

/**
* @def LVHF_WNS_HPFCORNERFREQ_DEFAULT
* Default of WNS_HPFCornerFreq
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HPFCORNERFREQ_DEFAULT (50)
/**
* @def LVHF_WNS_HPFCORNERFREQ_MIN
* MinValue of WNS_HPFCornerFreq
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HPFCORNERFREQ_MIN (0)
/**
* @def LVHF_WNS_HPFCORNERFREQ_MAX
* MaxValue of WNS_HPFCornerFreq
* @see LVHF_ControlParams_st
*/
#define LVHF_WNS_HPFCORNERFREQ_MAX (1500)

/**
* @def LVHF_SPDET_FAR_DEFAULT
* Default of SPDET_far
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_FAR_DEFAULT (16384)
/**
* @def LVHF_SPDET_FAR_MIN
* MinValue of SPDET_far
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_FAR_MIN (0)
/**
* @def LVHF_SPDET_FAR_MAX
* MaxValue of SPDET_far
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_FAR_MAX (32767)

/**
* @def LVHF_SPDET_MIC_DEFAULT
* Default of SPDET_mic
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_MIC_DEFAULT (16384)
/**
* @def LVHF_SPDET_MIC_MIN
* MinValue of SPDET_mic
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_MIC_MIN (0)
/**
* @def LVHF_SPDET_MIC_MAX
* MaxValue of SPDET_mic
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_MIC_MAX (32767)

/**
* @def LVHF_SPDET_X_CLIP_DEFAULT
* Default of SPDET_x_clip
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_X_CLIP_DEFAULT (0)
/**
* @def LVHF_SPDET_X_CLIP_MIN
* MinValue of SPDET_x_clip
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_X_CLIP_MIN (0)
/**
* @def LVHF_SPDET_X_CLIP_MAX
* MaxValue of SPDET_x_clip
* @see LVHF_ControlParams_st
*/
#define LVHF_SPDET_X_CLIP_MAX (32767)

/**
* @def LVHF_PCD_THRESHOLD_DEFAULT
* Default of PCD_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_THRESHOLD_DEFAULT (20000)
/**
* @def LVHF_PCD_THRESHOLD_MIN
* MinValue of PCD_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_THRESHOLD_MIN (0)
/**
* @def LVHF_PCD_THRESHOLD_MAX
* MaxValue of PCD_threshold
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_THRESHOLD_MAX (32767)

/**
* @def LVHF_PCD_TAPS_DEFAULT
* Default of PCD_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_TAPS_DEFAULT (16)
/**
* @def LVHF_PCD_TAPS_MIN
* MinValue of PCD_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_TAPS_MIN (16)
/**
* @def LVHF_PCD_TAPS_MAX
* MaxValue of PCD_taps
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_TAPS_MAX (64)

/**
* @def LVHF_PCD_ERL_DEFAULT
* Default of PCD_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_DEFAULT (64)
/**
* @def LVHF_PCD_ERL_MIN
* MinValue of PCD_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_MIN (64)
/**
* @def LVHF_PCD_ERL_MAX
* MaxValue of PCD_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_MAX (32767)

/**
* @def LVHF_PCD_MINIMUM_ERL_DEFAULT
* Default of PCD_minimum_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_MINIMUM_ERL_DEFAULT (64)
/**
* @def LVHF_PCD_MINIMUM_ERL_MIN
* MinValue of PCD_minimum_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_MINIMUM_ERL_MIN (64)
/**
* @def LVHF_PCD_MINIMUM_ERL_MAX
* MaxValue of PCD_minimum_erl
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_MINIMUM_ERL_MAX (32767)

/**
* @def LVHF_PCD_ERL_STEP_DEFAULT
* Default of PCD_erl_step
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_STEP_DEFAULT (16800)
/**
* @def LVHF_PCD_ERL_STEP_MIN
* MinValue of PCD_erl_step
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_STEP_MIN (16384)
/**
* @def LVHF_PCD_ERL_STEP_MAX
* MaxValue of PCD_erl_step
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_ERL_STEP_MAX (32767)

/**
* @def LVHF_PCD_GAMMA_E_RESCUE_DEFAULT
* Default of PCD_gamma_e_rescue
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_GAMMA_E_RESCUE_DEFAULT (5000)
/**
* @def LVHF_PCD_GAMMA_E_RESCUE_MIN
* MinValue of PCD_gamma_e_rescue
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_GAMMA_E_RESCUE_MIN (0)
/**
* @def LVHF_PCD_GAMMA_E_RESCUE_MAX
* MaxValue of PCD_gamma_e_rescue
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_GAMMA_E_RESCUE_MAX (32767)

/**
* @def LVBD_BD_OPERATINGMODE_DEFAULT
* Default of BD_OperatingMode
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVBD_BULKDELAY_DEFAULT
* Default of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_DEFAULT (0)
/**
* @def LVBD_BULKDELAY_MIN
* MinValue of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_MIN (0)
/**
* @def LVBD_BULKDELAY_MAX
* MaxValue of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_MAX (6400)

/**
* @def LVBD_BD_GAIN_DEFAULT
* Default of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_DEFAULT (8192)
/**
* @def LVBD_BD_GAIN_MIN
* MinValue of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_MIN (0)
/**
* @def LVBD_BD_GAIN_MAX
* MaxValue of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_MAX (32767)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_0_DEFAULT
* Default of MicrophoneChannel_0
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_0_DEFAULT (LVBUFFER_CHANNEL_0)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_1_DEFAULT
* Default of MicrophoneChannel_1
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_1_DEFAULT (LVBUFFER_CHANNEL_1)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_2_DEFAULT
* Default of MicrophoneChannel_2
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_2_DEFAULT (LVBUFFER_CHANNEL_2)

/**
* @def LVOUTPUTROUTING_TX_OUTPUTCHANNEL_0_DEFAULT
* Default of OutputChannel_0
* @see LVOUTPUTROUTING_Tx_ControlParams_st
*/
#define LVOUTPUTROUTING_TX_OUTPUTCHANNEL_0_DEFAULT (LVVE_TX_OUTPUT_CHANNEL_0)

/**
* @def LVLPF_LPF_OPERATINGMODE_DEFAULT
* Default of LPF_OperatingMode
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVLPF_LPF_CORNERFREQ_DEFAULT
* Default of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_DEFAULT (3700)
/**
* @def LVLPF_LPF_CORNERFREQ_MIN
* MinValue of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_MIN (3500)
/**
* @def LVLPF_LPF_CORNERFREQ_MAX
* MaxValue of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_MAX (23400)



/**
* @def LVVE_TX_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_OPERATINGMODE_DEFAULT (LVM_MODE_ON)

/**
* @def LVVE_TX_MUTE_DEFAULT
* Default of Mute
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_BD_OPERATINGMODE_DEFAULT
* Default of BD_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_BULKDELAY_DEFAULT
* Default of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_DEFAULT (0)
/**
* @def LVVE_TX_BULKDELAY_MIN
* MinValue of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_MIN (0)
/**
* @def LVVE_TX_BULKDELAY_MAX
* MaxValue of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_MAX (6400)

/**
* @def LVVE_TX_BD_GAIN_DEFAULT
* Default of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_DEFAULT (8192)
/**
* @def LVVE_TX_BD_GAIN_MIN
* MinValue of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_MIN (0)
/**
* @def LVVE_TX_BD_GAIN_MAX
* MaxValue of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_MAX (32767)

/**
* @def LVVE_TX_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_DEFAULT (0)
/**
* @def LVVE_TX_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_MIN (-96)
/**
* @def LVVE_TX_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_MAX (24)

/**
* @def LVVE_TX_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT
* Default of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_MIN
* MinValue of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_MIN (50)
/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_MAX
* MaxValue of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_MAX (1500)

/**
* @def LVVE_TX_LPF_OPERATINGMODE_DEFAULT
* Default of LPF_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_LPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT
* Default of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT (3700)
/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_MIN
* MinValue of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_MIN (3500)
/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_MAX
* MaxValue of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_MAX (23400)




/**
* @def LVVE_TX_EQ_OPERATINGMODE_DEFAULT
* Default of EQ_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_EQ_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)



/**
* @def LVVE_TX_CNG_OPERATINGMODE_DEFAULT
* Default of CNG_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_CNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)


/**
* @def LVAVC_MODULESTATUS_DEFAULT
* Default of ModuleStatus
* @see LVAVC_ControlParams_st
*/
#define LVAVC_MODULESTATUS_DEFAULT (0)

/**
* @def LVAVC_VADDECISION_DEFAULT
* Default of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_DEFAULT (0)
/**
* @def LVAVC_VADDECISION_MIN
* MinValue of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_MIN (0)
/**
* @def LVAVC_VADDECISION_MAX
* MaxValue of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_MAX (1)

/**
* @def LVAVC_VOICINGPROBABILITYDECISION_DEFAULT
* Default of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_DEFAULT (0)
/**
* @def LVAVC_VOICINGPROBABILITYDECISION_MIN
* MinValue of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_MIN (0)
/**
* @def LVAVC_VOICINGPROBABILITYDECISION_MAX
* MaxValue of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_MAX (3)

/**
* @def LVAVC_NOISEADAPTIVEGAINS_DEFAULT
* Default of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN
* MinValue of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX
* MaxValue of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_LENGTH
* Length of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_LENGTH (3)

/**
* @def LVAVC_BANDWEIGHTINGS_DEFAULT
* Default of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_DEFAULT {32767,32767,32767}
/**
* @def LVAVC_BANDWEIGHTINGS_ARRAYMIN
* MinValue of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_BANDWEIGHTINGS_ARRAYMAX
* MaxValue of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_BANDWEIGHTINGS_LENGTH
* Length of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_LENGTH (3)

/**
* @def LVAVC_COMPRESSIONGAINS_DEFAULT
* Default of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_COMPRESSIONGAINS_ARRAYMIN
* MinValue of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_COMPRESSIONGAINS_ARRAYMAX
* MaxValue of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_COMPRESSIONGAINS_LENGTH
* Length of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_LENGTH (3)

/**
* @def LVAVC_OUTPUTGAINS_DEFAULT
* Default of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_OUTPUTGAINS_ARRAYMIN
* MinValue of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_OUTPUTGAINS_ARRAYMAX
* MaxValue of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_OUTPUTGAINS_LENGTH
* Length of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_LENGTH (3)

/**
* @def LVAVC_N_FORMANTS_DEFAULT
* Default of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_DEFAULT (0)
/**
* @def LVAVC_N_FORMANTS_MIN
* MinValue of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_MIN (0)
/**
* @def LVAVC_N_FORMANTS_MAX
* MaxValue of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_MAX (8)

/**
* @def LVAVC_GAIN_FORMANTS_DEFAULT
* Default of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_GAIN_FORMANTS_ARRAYMIN
* MinValue of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_GAIN_FORMANTS_ARRAYMAX
* MaxValue of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVAVC_GAIN_FORMANTS_LENGTH
* Length of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_LENGTH (8)

/**
* @def LVAVC_BIN_FORMANTS_DEFAULT
* Default of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_BIN_FORMANTS_ARRAYMIN
* MinValue of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_BIN_FORMANTS_ARRAYMAX
* MaxValue of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVAVC_BIN_FORMANTS_LENGTH
* Length of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_LENGTH (8)

/**
* @def LVWBE_VADDECISION_DEFAULT
* Default of VadDecision
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VADDECISION_DEFAULT (0)
/**
* @def LVWBE_VADDECISION_MIN
* MinValue of VadDecision
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VADDECISION_MIN (0)
/**
* @def LVWBE_VADDECISION_MAX
* MaxValue of VadDecision
* @see LVWBE_ControlParams_st
*/
#define LVWBE_VADDECISION_MAX (32767)

/**
* @def LVWBE_POSTPROCESSORGAINLIN_DEFAULT
* Default of PostProcessorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINLIN_DEFAULT (0)
/**
* @def LVWBE_POSTPROCESSORGAINLIN_MIN
* MinValue of PostProcessorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINLIN_MIN (0)
/**
* @def LVWBE_POSTPROCESSORGAINLIN_MAX
* MaxValue of PostProcessorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_POSTPROCESSORGAINLIN_MAX (32767)

/**
* @def LVWBE_UPPERBANDSNRGAINLIN_DEFAULT
* Default of UpperBandSnrGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDSNRGAINLIN_DEFAULT (0)
/**
* @def LVWBE_UPPERBANDSNRGAINLIN_MIN
* MinValue of UpperBandSnrGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDSNRGAINLIN_MIN (0)
/**
* @def LVWBE_UPPERBANDSNRGAINLIN_MAX
* MaxValue of UpperBandSnrGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDSNRGAINLIN_MAX (32767)

/**
* @def LVWBE_UPPERBANDOVERALLGAINLIN_DEFAULT
* Default of UpperBandOverallGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDOVERALLGAINLIN_DEFAULT (0)
/**
* @def LVWBE_UPPERBANDOVERALLGAINLIN_MIN
* MinValue of UpperBandOverallGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDOVERALLGAINLIN_MIN (0)
/**
* @def LVWBE_UPPERBANDOVERALLGAINLIN_MAX
* MaxValue of UpperBandOverallGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDOVERALLGAINLIN_MAX (32767)

/**
* @def LVWBE_UPPERBANDDETECTORGAINLIN_DEFAULT
* Default of UpperBandDetectorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORGAINLIN_DEFAULT (0)
/**
* @def LVWBE_UPPERBANDDETECTORGAINLIN_MIN
* MinValue of UpperBandDetectorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORGAINLIN_MIN (0)
/**
* @def LVWBE_UPPERBANDDETECTORGAINLIN_MAX
* MaxValue of UpperBandDetectorGainLin
* @see LVWBE_ControlParams_st
*/
#define LVWBE_UPPERBANDDETECTORGAINLIN_MAX (32767)

/**
* @def LVWBE_SNRDB_DEFAULT
* Default of SnrDB
* @see LVWBE_ControlParams_st
*/
#define LVWBE_SNRDB_DEFAULT (0)
/**
* @def LVWBE_SNRDB_MIN
* MinValue of SnrDB
* @see LVWBE_ControlParams_st
*/
#define LVWBE_SNRDB_MIN (-2147483648)
/**
* @def LVWBE_SNRDB_MAX
* MaxValue of SnrDB
* @see LVWBE_ControlParams_st
*/
#define LVWBE_SNRDB_MAX (2147483647)

/**
* @def LVWBE_FRAMEENERGY_DEFAULT
* Default of FrameEnergy
* @see LVWBE_ControlParams_st
*/
#define LVWBE_FRAMEENERGY_DEFAULT (0)
/**
* @def LVWBE_FRAMEENERGY_MIN
* MinValue of FrameEnergy
* @see LVWBE_ControlParams_st
*/
#define LVWBE_FRAMEENERGY_MIN (0)
/**
* @def LVWBE_FRAMEENERGY_MAX
* MaxValue of FrameEnergy
* @see LVWBE_ControlParams_st
*/
#define LVWBE_FRAMEENERGY_MAX (2147483647)

/**
* @def LVWM_AVL_GAIN_DEFAULT
* Default of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_DEFAULT (0)
/**
* @def LVWM_AVL_GAIN_MIN
* MinValue of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_MIN (0)
/**
* @def LVWM_AVL_GAIN_MAX
* MaxValue of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_MAX (32767)




/**
* @def LVVE_RX_DUMMY_DEFAULT
* Default of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_DEFAULT (0)
/**
* @def LVVE_RX_DUMMY_MIN
* MinValue of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_MIN (-128)
/**
* @def LVVE_RX_DUMMY_MAX
* MaxValue of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_MAX (127)

/**
* @def MICROPHONE_NLMS_LB_COEFF_DEFAULT
* Default of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_NLMS_LB_COEFF_ARRAYMIN
* MinValue of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def MICROPHONE_NLMS_LB_COEFF_ARRAYMAX
* MaxValue of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def MICROPHONE_NLMS_LB_COEFF_LENGTH
* Length of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_LENGTH (128)

/**
* @def MICROPHONE_NLMS_HB_COEFF_DEFAULT
* Default of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_NLMS_HB_COEFF_ARRAYMIN
* MinValue of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def MICROPHONE_NLMS_HB_COEFF_ARRAYMAX
* MaxValue of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def MICROPHONE_NLMS_HB_COEFF_LENGTH
* Length of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_LENGTH (128)

/**
* @def MICROPHONE_FSB_COEFF_DEFAULT
* Default of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_FSB_COEFF_ARRAYMIN
* MinValue of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def MICROPHONE_FSB_COEFF_ARRAYMAX
* MaxValue of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def MICROPHONE_FSB_COEFF_LENGTH
* Length of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_LENGTH (24)

/**
* @def MICROPHONE_LOWFREQUENCYENERGY_DEFAULT
* Default of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_DEFAULT (0)
/**
* @def MICROPHONE_LOWFREQUENCYENERGY_MIN
* MinValue of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_MIN (0)
/**
* @def MICROPHONE_LOWFREQUENCYENERGY_MAX
* MaxValue of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_MAX (2147483647)

/**
* @def LVNV_STATUS_DEFAULT
* Default of Status
* @see LVNV_ControlParams_st
*/
#define LVNV_STATUS_DEFAULT (0)

/**
* @def LVNV_DUMMY1_DEFAULT
* Default of Dummy1
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY1_DEFAULT (0)
/**
* @def LVNV_DUMMY1_MIN
* MinValue of Dummy1
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY1_MIN (0)
/**
* @def LVNV_DUMMY1_MAX
* MaxValue of Dummy1
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY1_MAX (0)

/**
* @def LVNV_PCD_COEFF_DEFAULT
* Default of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVNV_PCD_COEFF_ARRAYMIN
* MinValue of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def LVNV_PCD_COEFF_ARRAYMAX
* MaxValue of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def LVNV_PCD_COEFF_LENGTH
* Length of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_LENGTH (64)

/**
* @def LVNV_SNR_ESTWB_16_DEFAULT
* Default of SNR_estWB_16
* @see LVNV_ControlParams_st
*/
#define LVNV_SNR_ESTWB_16_DEFAULT (0)
/**
* @def LVNV_SNR_ESTWB_16_MIN
* MinValue of SNR_estWB_16
* @see LVNV_ControlParams_st
*/
#define LVNV_SNR_ESTWB_16_MIN (0)
/**
* @def LVNV_SNR_ESTWB_16_MAX
* MaxValue of SNR_estWB_16
* @see LVNV_ControlParams_st
*/
#define LVNV_SNR_ESTWB_16_MAX (32767)

/**
* @def LVNV_BROADSIDE_ICC_DEFAULT
* Default of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_DEFAULT (0)
/**
* @def LVNV_BROADSIDE_ICC_MIN
* MinValue of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_MIN (0)
/**
* @def LVNV_BROADSIDE_ICC_MAX
* MaxValue of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_MAX (32767)

/**
* @def LVNV_SDET_FAREND_DETECT_DEFAULT
* Default of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_DEFAULT {0,0}
/**
* @def LVNV_SDET_FAREND_DETECT_ARRAYMIN
* MinValue of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_ARRAYMIN {0,0}
/**
* @def LVNV_SDET_FAREND_DETECT_ARRAYMAX
* MaxValue of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_ARRAYMAX {32767,32767}
/**
* @def LVNV_SDET_FAREND_DETECT_LENGTH
* Length of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_LENGTH (2)

/**
* @def LVNV_DNNS_GAMMAE_DEFAULT
* Default of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_DEFAULT {0,0}
/**
* @def LVNV_DNNS_GAMMAE_ARRAYMIN
* MinValue of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_ARRAYMIN {0,0}
/**
* @def LVNV_DNNS_GAMMAE_ARRAYMAX
* MaxValue of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_ARRAYMAX {32767,32767}
/**
* @def LVNV_DNNS_GAMMAE_LENGTH
* Length of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_LENGTH (2)

/**
* @def LVNV_SDET_COH_DEFAULT
* Default of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_DEFAULT (0)
/**
* @def LVNV_SDET_COH_MIN
* MinValue of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_MIN (0)
/**
* @def LVNV_SDET_COH_MAX
* MaxValue of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_MAX (32767)

/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT
* Default of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT (0)
/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_MIN
* MinValue of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_MIN (0)
/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_MAX
* MaxValue of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_MAX (32767)

/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT
* Default of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT (0)
/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_MIN
* MinValue of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_MIN (0)
/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_MAX
* MaxValue of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_MAX (32767)

/**
* @def LVNV_DNNS_MEANSNRLONG_DEFAULT
* Default of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_DEFAULT (0)
/**
* @def LVNV_DNNS_MEANSNRLONG_MIN
* MinValue of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_MIN (-32768)
/**
* @def LVNV_DNNS_MEANSNRLONG_MAX
* MaxValue of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_MAX (32767)

/**
* @def LVNV_WNS_CUTOFFFREQ_DEFAULT
* Default of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_DEFAULT (0)
/**
* @def LVNV_WNS_CUTOFFFREQ_MIN
* MinValue of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_MIN (0)
/**
* @def LVNV_WNS_CUTOFFFREQ_MAX
* MaxValue of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_MAX (8000)

/**
* @def LVNV_WNS_WINDPROBABILITY_DEFAULT
* Default of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_DEFAULT (0)
/**
* @def LVNV_WNS_WINDPROBABILITY_MIN
* MinValue of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_MIN (0)
/**
* @def LVNV_WNS_WINDPROBABILITY_MAX
* MaxValue of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_MAX (32767)

/**
* @def LVNV_MICROPHONESTATUS_LENGTH
* Length of MicrophoneStatus
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONESTATUS_LENGTH (2)

/**
* @def LVHF_GAMMAE_DEFAULT
* Default of gammae
* @see LVHF_ControlParams_st
*/
#define LVHF_GAMMAE_DEFAULT (0)
/**
* @def LVHF_GAMMAE_MIN
* MinValue of gammae
* @see LVHF_ControlParams_st
*/
#define LVHF_GAMMAE_MIN (0)
/**
* @def LVHF_GAMMAE_MAX
* MaxValue of gammae
* @see LVHF_ControlParams_st
*/
#define LVHF_GAMMAE_MAX (32767)

/**
* @def LVHF_STATUS_DEFAULT
* Default of status
* @see LVHF_ControlParams_st
*/
#define LVHF_STATUS_DEFAULT (0)

/**
* @def LVHF_NLMS_LB_COEFS_DEFAULT
* Default of NLMS_LB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVHF_NLMS_LB_COEFS_ARRAYMIN
* MinValue of NLMS_LB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}
/**
* @def LVHF_NLMS_LB_COEFS_ARRAYMAX
* MaxValue of NLMS_LB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def LVHF_NLMS_LB_COEFS_LENGTH
* Length of NLMS_LB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_LB_COEFS_LENGTH (200)

/**
* @def LVHF_NLMS_HB_COEFS_DEFAULT
* Default of NLMS_HB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVHF_NLMS_HB_COEFS_ARRAYMIN
* MinValue of NLMS_HB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}
/**
* @def LVHF_NLMS_HB_COEFS_ARRAYMAX
* MaxValue of NLMS_HB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def LVHF_NLMS_HB_COEFS_LENGTH
* Length of NLMS_HB_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_NLMS_HB_COEFS_LENGTH (136)

/**
* @def LVHF_PCD_COEFS_DEFAULT
* Default of PCD_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVHF_PCD_COEFS_ARRAYMIN
* MinValue of PCD_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}
/**
* @def LVHF_PCD_COEFS_ARRAYMAX
* MaxValue of PCD_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def LVHF_PCD_COEFS_LENGTH
* Length of PCD_Coefs
* @see LVHF_ControlParams_st
*/
#define LVHF_PCD_COEFS_LENGTH (64)

/**
* @def LVHF_LOWFREQUENCYPOWER_DEFAULT
* Default of LowFrequencyPower
* @see LVHF_ControlParams_st
*/
#define LVHF_LOWFREQUENCYPOWER_DEFAULT (0)
/**
* @def LVHF_LOWFREQUENCYPOWER_MIN
* MinValue of LowFrequencyPower
* @see LVHF_ControlParams_st
*/
#define LVHF_LOWFREQUENCYPOWER_MIN (0)
/**
* @def LVHF_LOWFREQUENCYPOWER_MAX
* MaxValue of LowFrequencyPower
* @see LVHF_ControlParams_st
*/
#define LVHF_LOWFREQUENCYPOWER_MAX (2147483647)




/**
* @def LVVE_TX_DUMMY_DEFAULT
* Default of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_DEFAULT (0)
/**
* @def LVVE_TX_DUMMY_MIN
* MinValue of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_MIN (-128)
/**
* @def LVVE_TX_DUMMY_MAX
* MaxValue of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_MAX (127)

/**
* Enum type for LVVIDHeader_MessageID_en
*/
typedef enum
{
    LVVE_RX_PRESET = 0, ///< VID header Message ID for LVVE_Rx presets
    LVVE_TX_PRESET = 1, ///< VID header Message ID for LVVE_Tx Presets
    LVVIDHEADER_MESSAGEID_EN_DUMMY = LVM_MAXENUM
} LVVIDHeader_MessageID_en;

/**
* Enum type for LVVIDHeader_ReturnStatus_en
*/
typedef enum
{
    LVVIDHEADER_NULLADDRESS = 0, ///< LVVIDHeader module returns NULL address error
    LVVIDHEADER_SUCCESS = 1, ///< LVVIDHeader module returns with success
    LVVIDHEADER_RETURNSTATUS_EN_DUMMY = LVM_MAXENUM
} LVVIDHeader_ReturnStatus_en;

/**
* Enum type for LVBuffer_Channel_en
*/
typedef enum
{
    LVBUFFER_CHANNEL_0 = 0, ///< First channel of the audio buffer
    LVBUFFER_CHANNEL_1 = 1, ///< Second channel of the audio buffer
    LVBUFFER_CHANNEL_2 = 2, ///< Third channel of the audio buffer
    LVBUFFER_CHANNEL_EN_DUMMY = LVM_MAXENUM
} LVBuffer_Channel_en;

/**
* Enum type for LVVE_Tx_OutputChannel_en
*/
typedef enum
{
    LVVE_TX_OUTPUT_CHANNEL_0 = 0, ///< First channel of the processed audio buffer from VoiceExperience
    LVVE_TX_OUTPUT_REFERENCE_CHANNEL_0 = 100, ///< First channel of the audio buffer returned by the AEC Reference FIFO, processed with the LVBD module.
    LVVE_TX_OUTPUTCHANNEL_EN_DUMMY = LVM_MAXENUM
} LVVE_Tx_OutputChannel_en;

/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/
/**
* The status parameters are used to retrieve the status of the module.
* @see LVAVC_GetStatusParameters
*/
typedef struct
{
    /**
    Status of the LVAVC module: Current Operating mode and ModeWord combined.
    */
    LVAVC_ModeWord_bm ModuleStatus;          ///< Module status

    /**
    Current VAD decision.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_VADDECISION_MIN (0)</td>
        <td>@ref LVAVC_VADDECISION_DEFAULT (0)</td>
        <td>@ref LVAVC_VADDECISION_MAX (1)</td>
    </tr>
    </table> */
    LVM_INT16 VadDecision;          ///< VAD decision

    /**
    Current Voicing Probability and SNR decisions.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_MIN (0)</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_DEFAULT (0)</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_MAX (3)</td>
    </tr>
    </table> */
    LVM_INT16 VoicingProbabilityDecision;          ///< Voicing Probability and SNR decisions

    /**
    Gain of Noise Adaptive Downlink modules.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 NoiseAdaptiveGains[LVAVC_NOISEADAPTIVEGAINS_LENGTH];          ///< Noise Adaptive Gains

    /**
    Band Weightings.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_DEFAULT {32767,32767,32767}</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 BandWeightings[LVAVC_BANDWEIGHTINGS_LENGTH];          ///< Band Weightings

    /**
    Compression Gains.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 CompressionGains[LVAVC_COMPRESSIONGAINS_LENGTH];          ///< Compression Gains

    /**
    Otput Gains.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVAVC_OUTPUTGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_OUTPUTGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_OUTPUTGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 OutputGains[LVAVC_OUTPUTGAINS_LENGTH];          ///< Output Gains

    /**
    Number of formants.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_N_FORMANTS_MIN (0)</td>
        <td>@ref LVAVC_N_FORMANTS_DEFAULT (0)</td>
        <td>@ref LVAVC_N_FORMANTS_MAX (8)</td>
    </tr>
    </table> */
    LVM_INT16 N_formants;          ///< Number of formants

    /**
    Gain of Formants.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVAVC_GAIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_GAIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_GAIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 Gain_Formants[LVAVC_GAIN_FORMANTS_LENGTH];          ///< Formants Gain

    /**
    Bin of Formants.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_BIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_BIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_BIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 Bin_Formants[LVAVC_BIN_FORMANTS_LENGTH];          ///< Formants Bin

} LVAVC_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVWBE_GetStatusParameters
*/
typedef struct
{
    /**
    Current VAD decision.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWBE_VADDECISION_MIN (0)</td>
        <td>@ref LVWBE_VADDECISION_DEFAULT (0)</td>
        <td>@ref LVWBE_VADDECISION_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 VadDecision;          ///< VAD decision

    /**
    Current gain as computed in post processor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWBE_POSTPROCESSORGAINLIN_MIN (0)</td>
        <td>@ref LVWBE_POSTPROCESSORGAINLIN_DEFAULT (0)</td>
        <td>@ref LVWBE_POSTPROCESSORGAINLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PostProcessorGainLin;          ///< Post proccesor gain

    /**
    SNR based gain on upper band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWBE_UPPERBANDSNRGAINLIN_MIN (0)</td>
        <td>@ref LVWBE_UPPERBANDSNRGAINLIN_DEFAULT (0)</td>
        <td>@ref LVWBE_UPPERBANDSNRGAINLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandSnrGainLin;          ///< Upper band gain based on SNR

    /**
    Total gain on upper-band
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVWBE_UPPERBANDOVERALLGAINLIN_MIN (0)</td>
        <td>@ref LVWBE_UPPERBANDOVERALLGAINLIN_DEFAULT (0)</td>
        <td>@ref LVWBE_UPPERBANDOVERALLGAINLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandOverallGainLin;          ///< Upper band total gain

    /**
    upperband content detector based linear gain on upperband
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWBE_UPPERBANDDETECTORGAINLIN_MIN (0)</td>
        <td>@ref LVWBE_UPPERBANDDETECTORGAINLIN_DEFAULT (0)</td>
        <td>@ref LVWBE_UPPERBANDDETECTORGAINLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandDetectorGainLin;          ///< Upper band content detector Gain

    /**
    Signal to noise ratio in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.15</td>
        <td>@ref LVWBE_SNRDB_MIN (-2147483648)</td>
        <td>@ref LVWBE_SNRDB_DEFAULT (0)</td>
        <td>@ref LVWBE_SNRDB_MAX (2147483647)</td>
    </tr>
    </table> */
    LVM_INT32 SnrDB;          ///< Signal to noise ratio in dB

    /**
    Signal to noise ratio.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>33.0</td>
        <td>@ref LVWBE_FRAMEENERGY_MIN (0)</td>
        <td>@ref LVWBE_FRAMEENERGY_DEFAULT (0)</td>
        <td>@ref LVWBE_FRAMEENERGY_MAX (2147483647)</td>
    </tr>
    </table> */
    LVM_INT32 FrameEnergy;          ///< Energy of current Frame

} LVWBE_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVWM_GetStatusParameters
*/
typedef struct
{
    /**
    The internal gain setting used by WhisperMode in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_GAIN_MIN (0)</td>
        <td>@ref LVWM_AVL_GAIN_DEFAULT (0)</td>
        <td>@ref LVWM_AVL_GAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_Gain;          ///< gain (dB)

} LVWM_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVVE_Rx_GetStatusParameters
*/
typedef struct
{
    /**
    Active Voice Contrast Status Parameter Structure.
    */
    LVAVC_StatusParams_st AVC_StatusParams;          ///< Active Voice Contrast Status Parameter Structure

    /**
    Whisper Mode Status Parameter Structure.
    */
    LVWM_StatusParams_st WM_StatusParams;          ///< Whisper Mode Status Parameter Structure

    /**
    Bandwidth Extension Status Parameter Structure.
    */
    LVWBE_StatusParams_st WBE_StatusParams;          ///< Bandwidth Extension Status Parameter Structure

    /**
    Dummy Param to avoid an empty struct which is not allowed in C in some LVVE
    build configurations.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.7</td>
        <td>@ref LVVE_RX_DUMMY_MIN (-128)</td>
        <td>@ref LVVE_RX_DUMMY_DEFAULT (0)</td>
        <td>@ref LVVE_RX_DUMMY_MAX (127)</td>
    </tr>
    </table> */
    LVM_INT8 dummy;          ///< Dummy Param

} LVVE_Rx_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see Microphone_GetStatusParameters
*/
typedef struct
{
    /**
    Table of coefficients of the low-band NLMS filter for this microphone.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 NLMS_LB_Coeff[MICROPHONE_NLMS_LB_COEFF_LENGTH];          ///< NLMS low-band coefficients.

    /**
    Table of coefficients of the high-band NLMS filter for this microphone.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 NLMS_HB_Coeff[MICROPHONE_NLMS_HB_COEFF_LENGTH];          ///< NLMS high-band coefficients.

    /**
    Table of coefficients of FSB for this microphone.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref MICROPHONE_FSB_COEFF_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref MICROPHONE_FSB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_FSB_COEFF_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 FSB_Coeff[MICROPHONE_FSB_COEFF_LENGTH];          ///< FSB coefficients.

    /**
    Low Frequency Energy used for wind noise suppression for this microphone. Can
    be used to tune WNS_EnergyScaleFactor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>31.0</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_MIN (0)</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_DEFAULT (0)</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_MAX (2147483647)</td>
    </tr>
    </table> */
    LVM_INT32 LowFrequencyEnergy;          ///< Low Frequency Energy.

} Microphone_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVNV_GetStatusParameters
*/
typedef struct
{
    /**
    Bit mask describing current state of LVNV, used for debugging.
    */
    LVNV_StatusWord_bm Status;          ///< Status word

    /**
    To keep correct memory alignment. To be removed when LVNV_StatusWord_bm has
    become int32 as it should be.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_DUMMY1_MIN (0)</td>
        <td>@ref LVNV_DUMMY1_DEFAULT (0)</td>
        <td>@ref LVNV_DUMMY1_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 Dummy1;          ///< Dummy for memory alignment.

    /**
    Table of coefficients of PCD filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.26</td>
        <td>@ref LVNV_PCD_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref LVNV_PCD_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVNV_PCD_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 PCD_Coeff[LVNV_PCD_COEFF_LENGTH];          ///< PCD coefficients.

    /**
    Broadband SNR estimate to determine speech presence probability.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SNR_ESTWB_16_MIN (0)</td>
        <td>@ref LVNV_SNR_ESTWB_16_DEFAULT (0)</td>
        <td>@ref LVNV_SNR_ESTWB_16_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SNR_estWB_16;          ///< Broadband SNR

    /**
    Value of the broadside detector (soft detection).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_BROADSIDE_ICC_MIN (0)</td>
        <td>@ref LVNV_BROADSIDE_ICC_DEFAULT (0)</td>
        <td>@ref LVNV_BROADSIDE_ICC_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 Broadside_ICC;          ///< Broadside detection

    /**
    Probability of far-end being present (soft detection) for each band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_DEFAULT {0,0}</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_ARRAYMAX {32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 SDET_FarEnd_Detect[LVNV_SDET_FAREND_DETECT_LENGTH];          ///< Far-end detection for each band.

    /**
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVNV_DNNS_GAMMAE_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_DNNS_GAMMAE_DEFAULT {0,0}</td>
        <td>@ref LVNV_DNNS_GAMMAE_ARRAYMAX {32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_GammaE[LVNV_DNNS_GAMMAE_LENGTH];          ///< Oversubtraction factor for echo for each band.

    /**
    Inter-microphone coherence detection (soft detection).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_COH_MIN (0)</td>
        <td>@ref LVNV_SDET_COH_DEFAULT (0)</td>
        <td>@ref LVNV_SDET_COH_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_Coh;          ///< Inter-microphone coherence.

    /**
    Floor compensation for inter-microphone coherence detection (soft detection) in
    low-band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_MIN (0)</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT (0)</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_IMCoh_floorcomp;          ///< Floor compensation for inter-microphone coherence

    /**
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_MIN (0)</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT (0)</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_SpPresProbBroadband;          ///< Broadband speech presence probability for low-band.

    /**
    Long-term SNR-level averaged over frequency bins.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_MIN (-32768)</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_DEFAULT (0)</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_meanSNRlong;          ///< Mean long-term SNR-level.

    /**
    Cut-off frequency in Hz for wind noise suppression. Can be used to tune
    WNS_PowerScaleFactor. This status parameter is only valid when at least 2 input
    channels are provided.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_MIN (0)</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_DEFAULT (0)</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_MAX (8000)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_CutoffFreq;          ///< Cut-off frequency for wind noise suppression.

    /**
    Probability of wind noise presence. Can be used to tune WNS_DetectorThreshold.
    This status parameter is only valid when at least 2 input channels are
    provided.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_MIN (0)</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_DEFAULT (0)</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_WindProbability;          ///< Probability of wind noise presence.

    /**
    Array of Microphone status. One status structure per microphone.
    */
    Microphone_StatusParams_st MicrophoneStatus[LVNV_MICROPHONESTATUS_LENGTH];          ///< Microphone status.

} LVNV_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVHF_GetStatusParameters
*/
typedef struct
{
    /**
    Current value of the echo subtraction factor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVHF_GAMMAE_MIN (0)</td>
        <td>@ref LVHF_GAMMAE_DEFAULT (0)</td>
        <td>@ref LVHF_GAMMAE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 gammae;          ///< multiplicator factor of echo suppression

    /**
    Bit mask describing the current state of LVHF, used for debugging.
    */
    LVHF_StatusWord_bm status;          ///< current state

    /**
    Array of NLMS lowband filter coefficients.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref LVHF_NLMS_LB_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}</td>
        <td>@ref LVHF_NLMS_LB_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVHF_NLMS_LB_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 NLMS_LB_Coefs[LVHF_NLMS_LB_COEFS_LENGTH];          ///< Array of NLMS lowband filter coefficients.

    /**
    Array of NLMS highband filter coefficients.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref LVHF_NLMS_HB_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}</td>
        <td>@ref LVHF_NLMS_HB_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVHF_NLMS_HB_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 NLMS_HB_Coefs[LVHF_NLMS_HB_COEFS_LENGTH];          ///< Array of NLMS highband filter coefficients.

    /**
    Array of NLMS PCD filter coefficients.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref LVHF_PCD_COEFS_ARRAYMIN {-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648,-2147483648}</td>
        <td>@ref LVHF_PCD_COEFS_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVHF_PCD_COEFS_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table> */
    LVM_INT32 PCD_Coefs[LVHF_PCD_COEFS_LENGTH];          ///< Array of NLMS PCD filter coefficients.

    /**
    Low Frequency Power used for wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>31.0</td>
        <td>@ref LVHF_LOWFREQUENCYPOWER_MIN (0)</td>
        <td>@ref LVHF_LOWFREQUENCYPOWER_DEFAULT (0)</td>
        <td>@ref LVHF_LOWFREQUENCYPOWER_MAX (2147483647)</td>
    </tr>
    </table> */
    LVM_INT32 LowFrequencyPower;          ///< Low Frequency Power.

} LVHF_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVVE_Tx_GetStatusParameters
*/
typedef struct
{
    /**
    HandsFree Status Parameter Structure.
    */
    LVHF_StatusParams_st HF_StatusParams;          ///< HandsFree Status Parameters Structure

    /**
    NoiseVoid Status Parameter Structure.
    */
    LVNV_StatusParams_st NV_StatusParams;          ///< NoiseVoid Status Parameters Structrue

    /**
    WhisperMode Status Parameter Structure.
    */
    LVWM_StatusParams_st WM_StatusParams;          ///< WhisperMode Status Parameter Structrue

    /**
    Dummy Param to avoid an empty struct which is not allowed in C in some LVVE
    build configurations.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.7</td>
        <td>@ref LVVE_TX_DUMMY_MIN (-128)</td>
        <td>@ref LVVE_TX_DUMMY_DEFAULT (0)</td>
        <td>@ref LVVE_TX_DUMMY_MAX (127)</td>
    </tr>
    </table> */
    LVM_INT8 dummy;          ///< Dummy Param

} LVVE_Tx_StatusParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVIDHeader_SetControlParameters function but they
* will not take effect until the next call to the LVVIDHeader_Process function.
*
* @see LVVIDHeader_SetControlParameters
* @see LVVIDHeader_Process
*/
typedef struct
{
    /**
    This number always increments by one when there is change in the header format.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_DEFAULT (4)</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 HeaderVersion;          ///< Header Format Version

    /**
    This variable holds value of the Major baseline version.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT (3)</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 MajorBaselineVersion;          ///< Major baseline version of the product

    /**
    This variable holds value of the Minor baseline version.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_DEFAULT (35)</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 MinorBaselineVersion;          ///< Minor baseline version of the product

    /**
    This variable holds value of the MinorMinor baseline version.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT (04)</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 MinorMinorBaselineVersion;          ///< MinorMinor baseline version of the product

    /**
    This variable holds value of the patch baseline version.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT (0)</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 PatchBaselineVersion;          ///< Patch baseline version of the product

    /**
    First LVVE_Rx algorithm mask which holds information of which LVVE_Rx
    algorithms are present.
    */
    LVVIDHeader_LVVE_Rx_Mask1_bm LVVE_Rx_AlgoMask1;          ///< This variable forms first part of LVVE_Rx algorithm mask

    /**
    Second LVVE_Rx algorithm mask which holds information of which LVVE_Rx
    algorithms are present.
    */
    LVVIDHeader_LVVE_Rx_Mask2_bm LVVE_Rx_AlgoMask2;          ///< This variable forms second part of LVVE_Rx algorithm mask

    /**
    First LVVE_Tx algorithm mask which holds information of which LVVE_Tx
    algorithms are present.
    */
    LVVIDHeader_LVVE_Tx_Mask1_bm LVVE_Tx_AlgoMask1;          ///< This variable forms first part of LVVE_Tx mask

    /**
    Second LVVE_Tx algorithm mask which holds information of which LVVE_Tx
    algorithms are present.
    */
    LVVIDHeader_LVVE_Tx_Mask2_bm LVVE_Tx_AlgoMask2;          ///< This variable forms second part of LVVE_Tx mask

    /**
    First LVVE configuration mask which holds information of which configurations
    of LVVE_Tx and LVVE_Rx algorithm are present.
    */
    LVVIDHeader_Configurations_Mask1_bm LVVE_Config_AlgoMask1;          ///< This variable forms first part of common LVVE_Tx and LVVE_Rx configuration mask

    /**
    Second LVVE configuration mask which holds information of which configurations
    of LVVE_Tx and LVVE_Rx algorithm are present.
    */
    LVVIDHeader_Configurations_Mask2_bm LVVE_Config_AlgoMask2;          ///< This variable forms second part of common LVVE_Tx and LVVE_Rx configuration mask

    /**
    Messge ID is used to identify stream
    */
    LVVIDHeader_MessageID_en MessageID;          ///< This param holds message ID of the buffer

    /**
    This parameter specifies the sample rate information of the buffer.
    */
    LVM_Fs_en SampleRate;          ///< Sample rate information of the buffer.

    /**
    This parameter specifies the volume index corresponding to the subsequent data.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_MIN (0)</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_DEFAULT (0)</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 VolumeIndex;          ///< Volume index corresponding to the subsequent data.

    /**
    This parameter specifies how many volumes are present in the binary file.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_MIN (0)</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_DEFAULT (1)</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_MAX (255)</td>
    </tr>
    </table> */
    LVM_UINT8 NumVolumes;          ///< Number of volumes present in the binary file.

} LVVIDHeader_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVAVC_SetControlParameters function but they
* will not take effect until the next call to the LVAVC_Process function.
*
* @see LVAVC_SetControlParameters
* @see LVAVC_Process
*/
typedef struct
{
    /**
    OperatingMode enables/disables Active Voice Contrast.
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    The mode word allows enabling or disabling certain functional blocks in Active
    Voice Contrast.
    */
    LVAVC_ModeWord_bm Mode;          ///< The mode word to control the internal operation of LVAVC.

    /**
    RMS Gain applied by AVC - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_GAIN_MIN (0)</td>
        <td>@ref LVAVC_GAIN_DEFAULT (10)</td>
        <td>@ref LVAVC_GAIN_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 Gain;          ///< RMS Gain applied by AVC - dB parameter

    /**
    Estimated ambient noise energy below which AVC effect is smoothed.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH0_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH0_DEFAULT (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH0_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NoiseAdaptTh0;          ///< Noise enabling Threshold

    /**
    Noise Adaptive Threshold low dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH1_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH1_DEFAULT (40)</td>
        <td>@ref LVAVC_NOISEADAPTTH1_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NoiseAdaptTh1;          ///< Noise Adaptive Threshold low dB parameter

    /**
    Noise Adaptive Threshold high dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH2_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH2_DEFAULT (60)</td>
        <td>@ref LVAVC_NOISEADAPTTH2_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NoiseAdaptTh2;          ///< Noise Adaptive Threshold high dB parameter

    /**
    Noise Adaptive Gain Min for Band1 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmin1;          ///< Noise Adaptive Gain Min for Band1 - dB parameter

    /**
    Noise Adaptive Gain Min for Band2 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmin2;          ///< Noise Adaptive Gain Min for Band2 - dB parameter

    /**
    Noise Adaptive Gain Min for Band3 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmin3;          ///< Noise Adaptive Gain Min for Band3 - dB parameter

    /**
    Noise Adaptive Gain Max for Band1 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmax1;          ///< Noise Adaptive Gain Max for Band1 - dB parameter

    /**
    Noise Adaptive Gain Max for Band2 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmax2;          ///< Noise Adaptive Gain Max for Band2 - dB parameter

    /**
    Noise Adaptive Gain Max for Band3 - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_MAX (30)</td>
    </tr>
    </table> */
    LVM_UINT16 NASEL_NoiseAdaptGmax3;          ///< Noise Adaptive Gain Max for Band3 - dB parameter

    /**
    Attack parameter for Noise Adapt module of NASEL.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_MAX (100)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_NoiseAdaptAttack;          ///< Noise Adaptive Attack parameter in dB/s

    /**
    Reelase parameter for Noise Adapt module of NASEL.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_MAX (100)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_NoiseAdaptRelease;          ///< Noise Adaptive Release parameter in dB/s

    /**
    Compression knee for low band of NASEL in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_DEFAULT (60)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompKnee1;          ///< Compression knee for low band of NASEL in dB

    /**
    Compression knee for mid band of NASEL in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_DEFAULT (55)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompKnee2;          ///< Compression knee for mid band of NASEL in dB

    /**
    Compression knee for high band of NASEL in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_DEFAULT (50)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_MAX (90)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompKnee3;          ///< Compression knee for high band of NASEL in dB

    /**
    Compression ratio for low band of NASEL.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompRatio1;          ///< Compression ratio for low band of NASEL

    /**
    Compression ratio for mid band of NASEL.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompRatio2;          ///< Compression ratio for mid band of NASEL

    /**
    Compression ratio for high band of NASEL.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NASEL_CompRatio3;          ///< Compression ratio for high band of NASEL

    /**
    FU Minimum spectral Gain - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_FU_MINGAIN_MIN (0)</td>
        <td>@ref LVAVC_FU_MINGAIN_DEFAULT (0)</td>
        <td>@ref LVAVC_FU_MINGAIN_MAX (54)</td>
    </tr>
    </table> */
    LVM_UINT16 FU_MinGain;          ///< FU Min Gain

    /**
    FU Maximum spectral Gain - dB parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_FU_MAXGAIN_MIN (0)</td>
        <td>@ref LVAVC_FU_MAXGAIN_DEFAULT (10)</td>
        <td>@ref LVAVC_FU_MAXGAIN_MAX (54)</td>
    </tr>
    </table> */
    LVM_UINT16 FU_MaxGain;          ///< FU Max Gain

    /**
    Limiter threshold for AVC.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_LIMITER_THR_MIN (0)</td>
        <td>@ref LVAVC_LIMITER_THR_DEFAULT (32767)</td>
        <td>@ref LVAVC_LIMITER_THR_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 Limiter_Thr;          ///< Limiter threshold for AVC

    /**
    Advanced configuration for AVC.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_MIN (0)</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_DEFAULT (0)</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 Advanced_Config;          ///< Advanced configuration for AVC

} LVAVC_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVFENS_SetControlParameters function but they
* will not take effect until the next call to the LVFENS_Process function.
*
* @see LVFENS_SetControlParameters
* @see LVFENS_Process
*/
typedef struct
{
    /**
    */
    LVM_Mode_en OperatingMode;          ///< Operating Mode

    /**
    The mode word to enable/disable internal algorithm blocks.
    */
    LVFENS_ModeWord_bm Mode;          ///< Mode word

    /**
    The maximum amount of background noise suppression. FENS_limit_ns =
    \f$32767*10^{-max noise reduction[dB]/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_MIN (0)</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_DEFAULT (10976)</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 FENS_limit_NS;          ///< FENS Noise Suppression Limit

    /**
    Additional noise suppression will be applied when noise is below the
    FENS_NoiseThreshold.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_MIN (0)</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_DEFAULT (96)</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 FENS_NoiseThreshold;          ///< FENS Noise Threshold

    /**
    Factor to control the amount of wind noise suppression. A higher value means
    more wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_MIN (0)</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_DEFAULT (256)</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_Aggressiveness;          ///< Aggressiveness of wind noise suppression

    /**
    Scale factor of powers used for wind noise suppression. <br>
    Unit: dB<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_MIN (-96)</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_DEFAULT (0)</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_MAX (32)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_PowerScaleFactor;          ///< Scale factor of powers used for wind noise suppression

    /**
    The maximum amount of wind noise suppression. WNS_MaxAttenuation =
    \f$32767*10^{-max noise reduction[dB]/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_MIN (0)</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_DEFAULT (8231)</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_MaxAttenuation;          ///< Maximum wind noise attenuation

    /**
    High frequency slope used for wind noise suppression. <br>
    Unit: dB<br>
    A higher value allows more wind reduction. A smaller value can be used to
    protect high frequency speech components.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_HFSLOPE_MIN (-20)</td>
        <td>@ref LVFENS_WNS_HFSLOPE_DEFAULT (-6)</td>
        <td>@ref LVFENS_WNS_HFSLOPE_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HFSlope;          ///< High frequency slope used for wind noise suppression

    /**
    Threshold for wind gush suppression. A higher threshold means less suppression
    of wind gushes.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_MIN (0)</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_DEFAULT (9830)</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_GushThreshold;          ///< Threshold for wind gush suppression

    /**
    The 3dB corner frequency of the high pass filter that was applied before LVFENS
    (e.g. by the codex). This is needed by our wind noise suppression algorithm in
    order to do a correct detection. A value of 0 means no high pass filtering was
    done on the signal beforehand. <br>
    Unit: Hz<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_MIN (0)</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HPFCornerFreq;          ///< High Pass Filter Corner Frequency in Hz

} LVFENS_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVWBE_SetControlParameters function but they
* will not take effect until the next call to the LVWBE_Process function.
*
* @see LVWBE_SetControlParameters
* @see LVWBE_Process
*/
typedef struct
{
    /**
    OperatingMode enables/disables Bandwidth Extension.
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    The mode word allows enabling or disabling certain functional blocks in
    Bandwidth Extension.
    */
    LVWBE_ModeWord_bm Mode;          ///< The mode word to control the internal operation of LVWBE.

    /**
    MidBandGain is a fixed gain (in dB) applied to the extended mid band, used to
    control the amount of Bandwidth Extension effect in mid-band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_MIDBANDGAIN_MIN (-90)</td>
        <td>@ref LVWBE_MIDBANDGAIN_DEFAULT (0)</td>
        <td>@ref LVWBE_MIDBANDGAIN_MAX (+12)</td>
    </tr>
    </table> */
    LVM_INT16 MidBandGain;          ///< Mid band gain

    /**
    UpperBandGain is a fixed gain (in dB) applied to the extended band, used to
    control the amount of Bandwidth Extension effect.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_UPPERBANDGAIN_MIN (-90)</td>
        <td>@ref LVWBE_UPPERBANDGAIN_DEFAULT (-15)</td>
        <td>@ref LVWBE_UPPERBANDGAIN_MAX (30)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandGain;          ///< Upper band gain

    /**
    Low-pass filter cutoff frequency for extended UpperBand. It should be a
    multiple of 500 (Hz).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_UPPERBANDCORNERFREQ_MIN (4500)</td>
        <td>@ref LVWBE_UPPERBANDCORNERFREQ_DEFAULT (6000)</td>
        <td>@ref LVWBE_UPPERBANDCORNERFREQ_MAX (7500)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandCornerFreq;          ///< UpperBand filter corner frequency

    /**
    UpperBandDetectorThreshold controls the threshold (in dB) for the upper band
    content detector.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_UPPERBANDDETECTORTHRESHOLD_MIN (-96)</td>
        <td>@ref LVWBE_UPPERBANDDETECTORTHRESHOLD_DEFAULT (-80)</td>
        <td>@ref LVWBE_UPPERBANDDETECTORTHRESHOLD_MAX (-60)</td>
    </tr>
    </table> */
    LVM_INT16 UpperBandDetectorThreshold;          ///< Upper band content detector threshold

    /**
    VoiceActivityThreshold controls the sensitivity of the Voice Activity Detector
    (VAD).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVWBE_VOICEACTIVITYTHRESHOLD_MIN (0)</td>
        <td>@ref LVWBE_VOICEACTIVITYTHRESHOLD_DEFAULT (19660)</td>
        <td>@ref LVWBE_VOICEACTIVITYTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 VoiceActivityThreshold;          ///< Voice Activity Detector Threshold

    /**
    Minimum gain of the post-processor (in dB).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_POSTPROCESSORGAINMIN_MIN (-90)</td>
        <td>@ref LVWBE_POSTPROCESSORGAINMIN_DEFAULT (-16)</td>
        <td>@ref LVWBE_POSTPROCESSORGAINMIN_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 PostProcessorGainMin;          ///< Post processor minimum gain

    /**
    Upper frequency boundary of the post-processor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_POSTPROCESSORFREQHIGH_MIN (3350)</td>
        <td>@ref LVWBE_POSTPROCESSORFREQHIGH_DEFAULT (3800)</td>
        <td>@ref LVWBE_POSTPROCESSORFREQHIGH_MAX (4000)</td>
    </tr>
    </table> */
    LVM_INT16 PostProcessorFreqHigh;          ///< Post processor high frequency parameter

    /**
    Lower frequency boundary of the post-processor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVWBE_POSTPROCESSORFREQLOW_MIN (0)</td>
        <td>@ref LVWBE_POSTPROCESSORFREQLOW_DEFAULT (500)</td>
        <td>@ref LVWBE_POSTPROCESSORFREQLOW_MAX (2450)</td>
    </tr>
    </table> */
    LVM_INT16 PostProcessorFreqLow;          ///< Post processor low frequency parameter

} LVWBE_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVWM_SetControlParameters function but they
* will not take effect until the next call to the LVWM_Process function.
*
* @see LVWM_SetControlParameters
* @see LVWM_Process
*/
typedef struct
{
    /**
    Control the operating mode (ON/OFF) of the algorithm.
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    The mode word to enable/disable internal algorithm blocks of WhisperMode.
    */
    LVWM_ModeWord_bm mode;          ///< Mode word

    /**
    The desired output level of the AGC. <br>
    AVL_Target_level_lin = \f$32767*10^{TargetLeveldB/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_MIN (0)</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT (16384)</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_Target_level_lin;          ///< The desired output level of the AGC.

    /**
    The maximal attenuation of the AGC. <br>
    AVL_MinGainLin = \f$512*10^{MinGaindB/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_MINGAINLIN_MIN (0)</td>
        <td>@ref LVWM_AVL_MINGAINLIN_DEFAULT (128)</td>
        <td>@ref LVWM_AVL_MINGAINLIN_MAX (512)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_MinGainLin;          ///< The maximal attenuation of the AGC.

    /**
    The maximal gain of the AGC. <br>
    AVL_MaxGainLin = \f$512*10^{MaxGaindB/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_MIN (512)</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_DEFAULT (8189)</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_MaxGainLin;          ///< The maximal gain of the AGC.

    /**
    The gain of the AGC is smoothed across time. The time constant of the smoothing
    depends on whether the gain is increasing or decreasing. When a sudden loud
    signal is encountered (e.g. attack of speech), the attack time is used. When
    the gain is increasing (e.g. long period of whispered speech), the release time
    is used. <br>

    AVL_Attack = \f$32767*\exp(-1/(100*AttackTimeSec))\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_ATTACK_MIN (0)</td>
        <td>@ref LVWM_AVL_ATTACK_DEFAULT (25520)</td>
        <td>@ref LVWM_AVL_ATTACK_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_Attack;          ///< The attack time of the AGC.

    /**
    The gain of the AGC is smoothed across time. The time constant of the smoothing
    depends on whether the gain is increasing or decreasing. When a sudden loud
    signal is encountered (e.g. attack of speech), the attack time is used. When
    the gain is increasing (e.g. long period of whispered speech), the release time
    is used. <br>

    AVL_Release = \f$32767*\exp(-1/(100*ReleasTimeSec))\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_RELEASE_MIN (0)</td>
        <td>@ref LVWM_AVL_RELEASE_DEFAULT (32685)</td>
        <td>@ref LVWM_AVL_RELEASE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_Release;          ///< The release time of the AGC.

    /**
    AVL_Limit_MaxOutputLin = \f$32767*10^{TargetLimiterLeveldB/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN (0)</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT (23197)</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AVL_Limit_MaxOutputLin;          ///< The level to which the signal will be limited.

    /**
    The higher the value of the threshold, the less speech detections will occur.
    <br>
    SpDetect_Threshold = \f$512 * [0,64]\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_MIN (0)</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_DEFAULT (9216)</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SpDetect_Threshold;          ///< The threshold used in the speech detector of WhisperMode.

} LVWM_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVDRC_SetControlParameters function but they
* will not take effect until the next call to the LVDRC_Process function.
*
* @see LVDRC_SetControlParameters
* @see LVDRC_Process
*/
typedef struct
{
    /**
    Operating mode for DRC
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    Sets the number of sections (knee points) in the compressor curve
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVDRC_NUMKNEES_MIN (1)</td>
        <td>@ref LVDRC_NUMKNEES_DEFAULT (5)</td>
        <td>@ref LVDRC_NUMKNEES_MAX (5)</td>
    </tr>
    </table> */
    LVM_UINT16 NumKnees;          ///< Number of Knee points in compressor curve

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. Th input level for the first knee should be equal to or
    larger than -96dB, and the input level for the last knee should be 0dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-96,-70,-50,-24,0}</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table> */
    LVM_INT16 CompressorCurveInputLevels[LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH];          ///< Compressor Curve Input Levels

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The output level for the first knee should be equal to or
    larger than -96dB, and the output level for the last knee should be equal to or
    smaller than 0dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-70,-38,-14,0}</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table> */
    LVM_INT16 CompressorCurveOutputLevels[LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH];          ///< Compressor Curve Output Levels

    /**
    The AttackTime parameter is the time constant controlling the rate of reaction
    to increase in signal level. The AttackTime is specified in increments of 100�s
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_ATTACKTIME_MIN (0)</td>
        <td>@ref LVDRC_ATTACKTIME_DEFAULT (10)</td>
        <td>@ref LVDRC_ATTACKTIME_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AttackTime;          ///< Attack Time

    /**
    The ReleaseTime parameter is the time constant controlling the rate of reaction
    to decrease in signal level. The ReleaseTime is specified in increments of
    100�s.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_RELEASETIME_MIN (0)</td>
        <td>@ref LVDRC_RELEASETIME_DEFAULT (2500)</td>
        <td>@ref LVDRC_RELEASETIME_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 ReleaseTime;          ///< Release Time

    /**
    Enable or Disable limiter with soft clipping.
    */
    LVM_Mode_en LimiterOperatingMode;          ///< Enable or Disable limiter with soft clipping

    /**
    Sets the limit level of the compressor in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_LIMITLEVEL_MIN (-96)</td>
        <td>@ref LVDRC_LIMITLEVEL_DEFAULT (0)</td>
        <td>@ref LVDRC_LIMITLEVEL_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 LimitLevel;          ///< Sets the limit level of the compressor in dB

} LVDRC_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVCNG_SetControlParameters function but they
* will not take effect until the next call to the LVCNG_Process function.
*
* @see LVCNG_SetControlParameters
* @see LVCNG_Process
*/
typedef struct
{
    /**
    Sets the level of the noise generated by the Comfort Noise Generator. The level
    is expressed in dBFS RMS.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVCNG_CNG_VOLUME_MIN (-96)</td>
        <td>@ref LVCNG_CNG_VOLUME_DEFAULT (-90)</td>
        <td>@ref LVCNG_CNG_VOLUME_MAX (-60)</td>
    </tr>
    </table> */
    LVM_INT16 CNG_Volume;          ///< CNG volume level in dBFS RMS

} LVCNG_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNG_SetControlParameters function but they
* will not take effect until the next call to the LVNG_Process function.
*
* @see LVNG_SetControlParameters
* @see LVNG_Process
*/
typedef struct
{
    /**
    Operating mode for Noise Gate
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    Sets the number of sections (knee points) in the compressor curve
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNG_NUMKNEES_MIN (1)</td>
        <td>@ref LVNG_NUMKNEES_DEFAULT (5)</td>
        <td>@ref LVNG_NUMKNEES_MAX (5)</td>
    </tr>
    </table> */
    LVM_UINT16 NumKnees;          ///< Number of Knee points in compressor curve

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The input level for the first knee should be equal to or
    larger than -96dB, and the input level for the last knee should be 0dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-80,-70,-50,-24,0}</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table> */
    LVM_INT16 CompressorCurveInputLevels[LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH];          ///< Compressor Curve Input Levels

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The output level for the first knee should be equal to or
    larger than -96dB, and the output level for the last knee should be equal to or
    smaller than 0dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-80,-50,-24,0}</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table> */
    LVM_INT16 CompressorCurveOutputLevels[LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH];          ///< Compressor Curve Output Levels

    /**
    The AttackTime parameter is the time constant controlling the rate of reaction
    to increase in signal level. The AttackTime is specified in increments of 100�s
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_ATTACKTIME_MIN (0)</td>
        <td>@ref LVNG_ATTACKTIME_DEFAULT (50)</td>
        <td>@ref LVNG_ATTACKTIME_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AttackTime;          ///< Attack Time

    /**
    The ReleaseTime parameter is the time constant controlling the rate of reaction
    to decrease in signal level. The ReleaseTime is specified in increments of
    100�s.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_RELEASETIME_MIN (0)</td>
        <td>@ref LVNG_RELEASETIME_DEFAULT (50)</td>
        <td>@ref LVNG_RELEASETIME_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 ReleaseTime;          ///< Release Time

} LVNG_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNF_SetControlParameters function but they
* will not take effect until the next call to the LVNF_Process function.
*
* @see LVNF_SetControlParameters
* @see LVNF_Process
*/
typedef struct
{
    /**
    Sets the Notch Filter attenuation in dB.(i.e. negative gain). This attenuation
    is reached at NF_Frequency.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNF_NF_ATTENUATION_MIN (0)</td>
        <td>@ref LVNF_NF_ATTENUATION_DEFAULT (10)</td>
        <td>@ref LVNF_NF_ATTENUATION_MAX (24)</td>
    </tr>
    </table> */
    LVM_INT16 NF_Attenuation;          ///< Notch Filter Attenuation

    /**
    Sets the filter center frequency in Hz .
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNF_NF_FREQUENCY_MIN (400)</td>
        <td>@ref LVNF_NF_FREQUENCY_DEFAULT (800)</td>
        <td>@ref LVNF_NF_FREQUENCY_MAX (1500)</td>
    </tr>
    </table> */
    LVM_UINT16 NF_Frequency;          ///< Notch Filter Frequency

    /**
    Sets the width of the filter. <br>
    \f$NF_QFactor= 100*Q\f$ <br>
    Where Q is the usual float range of a Q factor in float. <br>
    Note: The QFactor is defined at half the attenuation in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNF_NF_QFACTOR_MIN (50)</td>
        <td>@ref LVNF_NF_QFACTOR_DEFAULT (120)</td>
        <td>@ref LVNF_NF_QFACTOR_MAX (500)</td>
    </tr>
    </table> */
    LVM_UINT16 NF_QFactor;          ///< Notch Filter Qfactor

} LVNF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNLPP_SetControlParameters function but they
* will not take effect until the next call to the LVNLPP_Process function.
*
* @see LVNLPP_SetControlParameters
* @see LVNLPP_Process
*/
typedef struct
{
    /**
    Limits the signal level compared to digital full scale in dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNLPP_NLPP_LIMIT_MIN (-24)</td>
        <td>@ref LVNLPP_NLPP_LIMIT_DEFAULT (0)</td>
        <td>@ref LVNLPP_NLPP_LIMIT_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 NLPP_Limit;          ///< NLPP Limit

    /**
    Sets the -3dB corner frequency of the high-pass filter in Hz.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNLPP_NLPP_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVNLPP_NLPP_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVNLPP_NLPP_HPF_CORNERFREQ_MAX (1000)</td>
    </tr>
    </table> */
    LVM_UINT16 NLPP_HPF_CornerFreq;          ///< NLPP HPF Corner Frequency

} LVNLPP_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVEQ_SetControlParameters function but they
* will not take effect until the next call to the LVEQ_Process function.
*
* @see LVEQ_SetControlParameters
* @see LVEQ_Process
*/
typedef struct
{
    /**
    Sets the length of the Equalizer impulse response. This must never be more than
    the value of EQ_MaxLength set at initialization time. The EQ_Length must be a
    multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVEQ_EQ_LENGTH_MIN (8)</td>
        <td>@ref LVEQ_EQ_LENGTH_DEFAULT (192)</td>
        <td>@ref LVEQ_EQ_LENGTH_MAX (192)</td>
    </tr>
    </table> */
    LVM_UINT16 EQ_Length;          ///< EQ Tap Length

    /**
    Pointer to an array containing the samples of the Equalizer impulse response.
    The format of the array content is specified below. The samples of the
    Equalizer impulse response must be in Q3.12 format.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>3.12</td>
        <td>@ref LVEQ_EQ_COEFS_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref LVEQ_EQ_COEFS_DEFAULT {4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVEQ_EQ_COEFS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 *pEQ_Coefs;          ///< Equalizer impulse response

} LVEQ_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVOL_SetControlParameters function but they
* will not take effect until the next call to the LVVOL_Process function.
*
* @see LVVOL_SetControlParameters
* @see LVVOL_Process
*/
typedef struct
{
    /**
    Turns on/off VOL_Gain.
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVOL_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVOL_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVOL_VOL_GAIN_MAX (24)</td>
    </tr>
    </table> */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

} LVVOL_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVHPF_SetControlParameters function but they
* will not take effect until the next call to the LVHPF_Process function.
*
* @see LVHPF_SetControlParameters
* @see LVHPF_Process
*/
typedef struct
{
    /**
    Turns on/off High Pass filter.
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_UINT16 HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

} LVHPF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVMUTE_SetControlParameters function but they
* will not take effect until the next call to the LVMUTE_Process function.
*
* @see LVMUTE_SetControlParameters
* @see LVMUTE_Process
*/
typedef struct
{
    /**
    This param can mute unmute the Rx/Tx engine output.
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

} LVMUTE_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVE_Rx_SetControlParameters function but they
* will not take effect until the next call to the LVVE_Rx_Process function.
*
* @see LVVE_Rx_SetControlParameters
* @see LVVE_Rx_Process
*/
typedef struct
{
    /**
    This enumerated type is used to set the operating mode of the Rx path. The
    processing can be separately set to enable all processing modules (i.e., ON),
    or to disable all processing modules (i.e., OFF). When OFF, the input is copied
    to the output with the applied channel routing and the LVVE sub modules can be
    provided with invalid parameters for modules. The sub module functions are not
    executed in this scenario.
    */
    LVM_Mode_en OperatingMode;          ///< Turns on/off all VoiceExperience processing modules on Rx

    /**
    This param can mute unmute the Rx/Tx engine output.
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

    /**
    Turns on/off VOL_Gain.
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVE_RX_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVE_RX_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVE_RX_VOL_GAIN_MAX (24)</td>
    </tr>
    </table> */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

    /**
    Far End Noise Suppression Control Parameter Structure.
    */
    LVFENS_ControlParams_st FENS_ControlParams;          ///< Far End Noise Suppression Control Parameter Structure

    /**
    Bandwidth Extension Control Parameter Structure.
    */
    LVWBE_ControlParams_st WBE_ControlParams;          ///< Bandwidth Extension Control Parameter Structure

    /**
    Turns on/off NLPP.
    */
    LVM_Mode_en NLPP_OperatingMode;          ///< Turns on/off NLPP

    /**
    NLPP Control Parameter Structure.
    */
    LVNLPP_ControlParams_st NLPP_ControlParams;          ///< NLPP Control Parameter Structure

    /**
    Active Voice Constrat Parameter Structure.
    */
    LVAVC_ControlParams_st AVC_ControlParams;          ///< Active Voice Contrast Parameter Structure

    /**
    Equalizer Operating mode.
    */
    LVM_Mode_en EQ_OperatingMode;          ///< Equalizer Operating mode

    /**
    Equalizer Control Parameters Structure.
    */
    LVEQ_ControlParams_st EQ_ControlParams;          ///< Equalizer Control Parameters Structure

    /**
    DRC Control Parameter Strcuture.
    */
    LVDRC_ControlParams_st DRC_ControlParams;          ///< DRC Control Parameter Strcuture

    /**
    Turns on/off High Pass filter.
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_UINT16 HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    Turns on/off Conformt Noise Generator Module.
    */
    LVM_Mode_en CNG_OperatingMode;          ///< Turns on/off Comfort Noise Generator Module

    /**
    Comfort Noise Generator Control Parameter Structure.
    */
    LVCNG_ControlParams_st CNG_ControlParams;          ///< Comfort Noise Generator Control Parameter Structure

    /**
    Whisper Mode Control Parameter Strcuture.
    */
    LVWM_ControlParams_st WM_ControlParams;          ///< Whisper Mode Control Parameter Strcuture

    /**
    Noise Gate Control Parameter Structure.
    */
    LVNG_ControlParams_st NG_ControlParams;          ///< Noise Gate Control Parameter Structure

    /**
    Turns on/off Notch Filter.
    */
    LVM_Mode_en NF_OperatingMode;          ///< Turns on/off Notch Filter

    /**
    Notch Filter Control Parameter Structure.
    */
    LVNF_ControlParams_st NF_ControlParams;          ///< Notch Filter Control Parameter Structure

} LVVE_Rx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the AcousticEchoCanceller_SetControlParameters function but they
* will not take effect until the next call to the AcousticEchoCanceller_Process function.
*
* @see AcousticEchoCanceller_SetControlParameters
* @see AcousticEchoCanceller_Process
*/
typedef struct
{
    /**
    Length of the low-band adaptive echo cancellation filter. The length should be
    chosen such that most of the energy of the impulse response is covered by the
    filter. The length should be a multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MIN (16)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_DEFAULT (128)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TAPS_MAX (128)</td>
    </tr>
    </table> */
    LVM_UINT16 AEC_NLMS_LB_taps;          ///< Number of NLMS low-band taps.

    /**
    Step size for the update of the low-band adaptive filter coefficients. It is
    recommended not to change the default value.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MIN (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_DEFAULT (8192)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_TWOALPHA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_LB_twoalpha;          ///< NLMS low-band step size

    /**
    Adaptive step-size control for the low-band adaptive echo cancellation filter.
    This parameter is used to slow down the update speed of the adaptive filter
    during double talk situations.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MIN (64)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_DEFAULT (4096)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_LB_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_LB_erl;          ///< NLMS low-band erl

    /**
    Length of the high-band adaptive echo cancellation filter. The length should be
    chosen such that most of the energy of the impulse response is covered by the
    filter. The length should be a multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MIN (16)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_DEFAULT (64)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TAPS_MAX (128)</td>
    </tr>
    </table> */
    LVM_UINT16 AEC_NLMS_HB_taps;          ///< Number of NLMS high-band taps.

    /**
    Step size for the update of the high-band adaptive filter coefficients. It is
    recommended not to change the default value.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MIN (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_DEFAULT (8192)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_TWOALPHA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_HB_twoalpha;          ///< NLMS high-band step size

    /**
    Adaptive step-size control for the high-band adaptive echo cancellation filter.
    This parameter is used to slow down the update speed of the adaptive filter
    during double talk situations.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MIN (64)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_DEFAULT (2048)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_HB_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_HB_erl;          ///< NLMS high-band erl

    /**
    Indicates whether a preset of adaptive filter coefficients should be done.<br>
    0 = No preset (coefficients as they are).<br>
    1 = Preset with internally calculated coefficients.<br>
    2 = Preset with zero coefficients.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MIN (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_DEFAULT (2)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_PRESET_COEFS_MAX (2)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_preset_coefs;          ///< NLMS preset

    /**
    Offset added to the normalization of the adaptation of the filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MIN (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_DEFAULT (100)</td>
        <td>@ref ACOUSTICECHOCANCELLER_AEC_NLMS_OFFSET_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 AEC_NLMS_offset;          ///< NLMS offset

    /**
    Gain applied to the output of the AEC residual signal.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT (2048)</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 MicrophoneGainComp;          ///< Microphone Gain Compensation

} AcousticEchoCanceller_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the FsbChannel_SetControlParameters function but they
* will not take effect until the next call to the FsbChannel_Process function.
*
* @see FsbChannel_SetControlParameters
* @see FsbChannel_Process
*/
typedef struct
{
    /**
    Set of coefficients for the adaptive filter FSB inside the Beamformer. These
    values should be determined for the given microphone configuration
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref FSBCHANNEL_FSB_INITTABLE_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref FSBCHANNEL_FSB_INITTABLE_DEFAULT {0,0,0,0,10922,0,0,0}</td>
        <td>@ref FSBCHANNEL_FSB_INITTABLE_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 FSB_InitTable[FSBCHANNEL_FSB_INITTABLE_LENGTH];          ///< FSB initial coefficients.

} FsbChannel_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the Beamformer_SetControlParameters function but they
* will not take effect until the next call to the Beamformer_Process function.
*
* @see Beamformer_SetControlParameters
* @see Beamformer_Process
*/
typedef struct
{
    /**
    Length of the Beamformer filters to model the acoustical paths between the
    speech source and the microphones. The length must be a multiple of 8. It must
    be equal to 16 for the handset application.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref BEAMFORMER_FSB_TAPS_MIN (8)</td>
        <td>@ref BEAMFORMER_FSB_TAPS_DEFAULT (16)</td>
        <td>@ref BEAMFORMER_FSB_TAPS_MAX (24)</td>
    </tr>
    </table> */
    LVM_UINT16 FSB_taps;          ///< Number of FSB taps

    /**
    Array of microphone dependent Fsb parameters.
    */
    FsbChannel_ControlParams_st FsbChannel[BEAMFORMER_FSBCHANNEL_LENGTH];          ///< Fsb channel Params.

} Beamformer_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the EchoSuppression_SetControlParameters function but they
* will not take effect until the next call to the EchoSuppression_Process function.
*
* @see EchoSuppression_SetControlParameters
* @see EchoSuppression_Process
*/
typedef struct
{
    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during farend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT (450)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaHi_LF;          ///< High echo oversubtraction factor LF

    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during nearend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaLo_LF;          ///< Low echo oversubtraction factor LF

    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during double talk only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaDt_LF;          ///< Mid echo oversubtraction factor LF

    /**
    Cutoff frequency in Hz for low frequency band in low-band
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT (600)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX (4000)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoCutoff_LF;          ///< Cut off frequency of LF in Hz

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    nearend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_NlAttenLow;          ///< Non-linear echo subtraction factor for lowest frequencies

    /**
    Echo oversubtraction factor for low-band that is applied during farend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT (450)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaHi_LB;          ///< High echo oversubtraction factor LB

    /**
    Echo oversubtraction factor for low-band that is applied during nearend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaLo_LB;          ///< Low echo oversubtraction factor LB

    /**
    Echo oversubtraction factor for low-band that is applied during double talk
    only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaDt_LB;          ///< Mid echo oversubtraction factor LB

    /**
    Parameter related to the reverberation time of the acoustical environment,
    which represents the decay in low-band energy over time of the echo tail of the
    impulse response.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT (10000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoAlphaRev_LB;          ///< Echo reverberation factor LB

    /**
    Parameter representing the portion of the echo tail (estimated by the low-band
    NLMS filter) that has to be extrapolated in time.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT (8000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoTailPortion_LB;          ///< Echo tail portion LB

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    farend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT (128)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_NlAtten_LB;          ///< Non-linear echo subtraction factor LB

    /**
    Echo oversubtraction factor for high-band that is applied during farend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaHi_HB;          ///< High echo oversubtraction factor HB

    /**
    Echo oversubtraction factor for high-band that is applied during nearend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaLo_HB;          ///< Low echo oversubtraction factor HB

    /**
    Echo oversubtraction factor for high-band that is applied during double talk
    only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoGammaDt_HB;          ///< Mid echo oversubtraction factor HB

    /**
    Parameter related to the reverberation time of the acoustical environment,
    which represents the decay in high-band energy over time of the echo tail of
    the impulse response.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoAlphaRev_HB;          ///< Echo reverberation factor HB

    /**
    Parameter representing the portion of the echo tail (estimated by the high-band
    NLMS filter) that has to be extrapolated in time.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoTailPortion_HB;          ///< Echo tail portion HB

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    farend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT (64)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_NlAtten_HB;          ///< Non-linear echo subtraction factor HB

    /**
    Maximal echo scaling factor additionally applied to the top band (above 7kHz).
    The true factor applied depends on how much the echo estimate is above the
    floor. A value of 32767 means no additional attenuation is applied. A value of
    8192 would allow up to 12dB of additional echo suppression in the top band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_DEFAULT (32767)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOSCALE_TB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoScale_TB;          ///< Echo scaling factor for top band

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequenices below EchoCutoff_LF
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoRelax_NEThreshold_LF;          ///< Echo relaxation threshold for low frequencies

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequenices above EchoCutoff_LF
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoRelax_NEThreshold;          ///< Echo relaxation threshold

    /**
    Level to which the echo estimate is clipped inside the post-processor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT (32767)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_EchoClippingLevel;          ///< Clipping level for echo inside DNNS

    /**
    Echo oversubtraction factor applied to the estimated non-stationary noise
    floor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_DEFAULT (1024)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_GainEta;          ///< Echo oversubtraction factor.

    /**
    Near-end speech detector threshold used in Dynamic Echo Suppression (DES). The
    lower the threshold, the more detections.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT (512)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_SPDET_NearThreshold;          ///< Near-end speech detector threshold.

    /**
    The amount of comfort noise insertion. This parameter only takes effect from
    LifeVibes&trade; VoiceExperience 4.0 onwards
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_CNILevel;          ///< Comfort noise insertion level.

} EchoSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the GeneralSidelobeCanceller_SetControlParameters function but they
* will not take effect until the next call to the GeneralSidelobeCanceller_Process function.
*
* @see GeneralSidelobeCanceller_SetControlParameters
* @see GeneralSidelobeCanceller_Process
*/
typedef struct
{
    /**
    Length of the GSC to model the transfer function between the noise reference
    and the residual signal, in order to reduce the noise and interfering sounds
    from the desired signal. This number should be a multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_MIN (8)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT (16)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_MAX (16)</td>
    </tr>
    </table> */
    LVM_UINT16 GSC_taps;          ///< Number of GSC taps for noise cancellation

    /**
    Adaptive step-size control for GSC to avoid divergence of the adaptive filter
    during desired speech. In general, GSC_erl is lower than NLMS_erl and ranges
    between 0 and 30 dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_MIN (64)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT (256)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 GSC_erl;          ///< GSC erl

} GeneralSidelobeCanceller_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the NoiseSuppression_SetControlParameters function but they
* will not take effect until the next call to the NoiseSuppression_Process function.
*
* @see NoiseSuppression_SetControlParameters
* @see NoiseSuppression_Process
*/
typedef struct
{
    /**
    Maximum oversubtraction factor for non-stationary noise. This parameter only
    takes effect from LifeVibes&trade; VoiceExperience 3.1 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT (8192)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_Gamma_nn_max;          ///< Maximum oversubtraction factor for non-stationary noise.

    /**
    Relates to the amount of non-stationary noise suppression for low frequencies.
    The higher the value, the more non-stationary noise is suppressed. A value of
    256 would only keep stationary NS. A value > 256 would make a trade-off between
    stationary and non-stationary NS.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_Constrained_nn_lo;          ///< Trade-off between stationary and non-stationary NS for low-band.

    /**
    Relates to the amount of non-stationary noise suppression for mid frequencies.
    The higher the value, the more non-stationary noise is suppressed. A value of
    256 would only keep stationary NS. A value > 256 would make a trade-off between
    stationary and non-stationary NS. This parameter only takes effect from
    LifeVibes&trade; VoiceExperience 3.1 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_Constrained_nn_hi;          ///< Trade-off between stationary and non-stationary NS for mid-band.

    /**
    Controls the amount of non-stationary noise suppression in the high-band
    (between 4kHz and 8kHz). The higher the value, the more non-stationary noise is
    suppressed. Value of 256 means there is no non-stationary noise suppression in
    high band. This parameter only takes effect from LifeVibes&trade;
    VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_Constrained_nn_highband;          ///< Trade-off between stationary and non-stationary NS for high-band.

    /**
    Upper frequency (in Hz) of region for mingain computation.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_DEFAULT {200,4000,8000}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_ARRAYMAX {8000,8000,8000}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_SNR_freq[NOISESUPPRESSION_DNNS_UGF_MG_SNR_FREQ_LENGTH];          ///< Upper frequency of SNR region.

    /**
    Low SNR-level for mingain computation in [dB]. This parameter belongs to a set
    of 4 to determine a piecewise linear curve to tune the amount of noise
    suppression. Between low SNR-level and high SNR-level in [dB], the NS is
    linear. Below low SNR-level and above high SNR-level, NS is constant.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMIN {-5,-5,-5}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_DEFAULT {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_ARRAYMAX {63,63,63}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_SNR_min[NOISESUPPRESSION_DNNS_UGF_MG_SNR_MIN_LENGTH];          ///< Low SNR-level for mingain.

    /**
    High SNR-level for mingain computation in [dB]. Note that the value of this
    parameter must be larger than or equal to the value of DNNS_UGF_MG_SNR_min.
    This parameter belongs to a set of 4 to determine a piecewise linear curve to
    tune the amount of noise suppression. Between low SNR-level and high SNR-level
    in [dB], the NS is linear. Below low SNR-level and above high SNR-level, NS is
    constant.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMIN {-5,-5,-5}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_DEFAULT {20,20,20}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_ARRAYMAX {63,63,63}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_SNR_max[NOISESUPPRESSION_DNNS_UGF_MG_SNR_MAX_LENGTH];          ///< High SNR-level for mingain.

    /**
    Maximal amount of noise suppression at high SNR-level. This parameter belongs
    to a set of 4 to determine a piecewise linear curve to tune the amount of noise
    suppression. Between low SNR-level and high SNR-level in [dB], the NS is
    linear. Below low SNR-level and above high SNR-level, NS is constant.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_DEFAULT {5827,5827,5827}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_highSNR[NOISESUPPRESSION_DNNS_UGF_MG_HIGHSNR_LENGTH];          ///< Maximal noise suppression at high SNR-level.

    /**
    Maximal amount of noise suppression at low SNR-level. This parameter belongs to
    a set of 4 to determine a piecewise linear curve to tune the amount of noise
    suppression. Between low SNR-level and high SNR-level in [dB], the NS is
    linear. Below low SNR-level and above high SNR-level, NS is constant.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_DEFAULT {5827,5827,5827}</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_lowSNR[NOISESUPPRESSION_DNNS_UGF_MG_LOWSNR_LENGTH];          ///< Maximal noise suppression at low SNR-level.

    /**
    Minimum amount of noise suppression during speech. This parameter should be set
    between max(DNNS_UGF_MG_lowSNR,DNNS_UGF_MG_highSNR) and 32767. The higher the
    parameter, the better the speech is preserved at the expense of potentially
    less non-stationary noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_MAX_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_MAX_DEFAULT (16423)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_MAX_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_max;          ///< Minimum noise suppression during speech

    /**
    Maximum noise suppression during speech. It is relative to the SNR dependent
    noise suppression controlled by the parameters DNNS_UGF_MG_lowSNR and
    DNNS_UGF_MG_highSNR. The higher the parameter, the better the preservation of
    speech during speech segments at the expense of potentially audible noise
    pumping.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_REL_MIN (8192)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_REL_DEFAULT (9093)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_REL_MAX (16384)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_rel;          ///< Maximum noise suppression during speech

    /**
    Sensitivity on the gain refinement. The smaller the parameter, the higher the
    sensitivity on the gain refinement.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAINDET_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_GAINDET_Sensitivity;          ///< Sensitivity on the gain refinement

    /**
    amount of non-stationary NS compared to the stationary NS (>1 means less
    non-stat NS; <1 means more non-stat NS). E.g., 4 = 12dB less non-stationary NS
    (both SPKPH and HNDST mode).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MIN (512)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_DEFAULT (512)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_NONSTATREL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_NonStatRel;          ///< Non-stationary NS compared to stationary NS ratio

    /**
    amount of non-stationary NS compared to the stationary NS in broadside (HNDST
    mode only)
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MIN (512)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_DEFAULT (512)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_HNDST_NONSTATRELBROADSIDE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_HNDST_NonStatRelBroadSide;          ///< Non-stationary NS compared to stationary NS ratio in Broadside

    /**
    Relaxation factor for the minimum gain during recovery after position change
    (HNDST mode only)
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MIN (2048)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_DEFAULT (2048)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_RELAXPOSCHANGE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_RelaxPosChange;          ///< Minimum Gain relaxation after position change

    /**
    Maximal amount of noise suppression for top band. This value must always be set
    to the lowest of DNNS_UGF_MG_highSNR and DNNS_UGF_MG_lowSNR, or lower. In
    speech segments the parameter will have almost no effect, while in noise or
    echo segments it becomes active.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_DEFAULT (5827)</td>
        <td>@ref NOISESUPPRESSION_DNNS_UGF_MG_TOPBAND_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_UGF_MG_Topband;          ///< Maximal noise suppression for top band

    /**
    Controls the amount of smoothing. A large value makes the speech sound natural
    and continuous, but since a lot of smoothing is applied it can also introduce
    speech attenuation. A small value implies less smoothing, hence less
    naturalness of the speech, but also less attenuation. It is advised to keep the
    value of this parameter within the range [307; 819] to avoid artifacts. This
    parameter only takes effect from LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT (819)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX (1024)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_NGS_Smoothing;          ///< Maximum oversubtraction factor for non-stationary noise.

    /**
    Controls the amount of musical tone suppression. A large value implies that
    less musical tones will be suppressed. A small value implies a more aggressive
    suppression of musical tones but possibly also some speech attenuation. It is
    advised to use a value within the range [-308; 308]. This parameter only takes
    effect from LifeVibes&trade; VoiceExperience 4.0 onwards
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN (-1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX (1024)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_MusicalTone_Sensitivity;          ///< Musical tone suppression sensitivity.

    /**
    Controls the trade-off between the amount of speech recovery that is applied
    and the amount of residual noise during speech (especially in car noise
    environments). By tuning the parameter down, the speech can be slightly more
    attenuated, but some residual noises can be removed. It is expected that this
    parameter needs a lower value in handset configurations than in speakerphone
    configs.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_DEFAULT (32767)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DNNS_SpeechRecovery;          ///< Sensitivity of the speech recovery mechanism.

} NoiseSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the WindNoiseSuppression_SetControlParameters function but they
* will not take effect until the next call to the WindNoiseSuppression_Process function.
*
* @see WindNoiseSuppression_SetControlParameters
* @see WindNoiseSuppression_Process
*/
typedef struct
{
    /**
    Scale factor (in dB) of powers. Used for computing the cut-off frequency for
    wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MIN (-96)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_DEFAULT (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_POWERSCALEFACTOR_MAX (32)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_PowerScaleFactor;          ///< Scale factor (in dB) of powers used for wind noise suppression

    /**
    Speech level difference in dB for the 2nd microphone taking primary microphone
    as a reference. Only used in handset mode during wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMIN {0}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT {12}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_ARRAYMAX {96}</td>
    </tr>
    </table> */
    LVM_INT16 WNS_MicSpeechLevelDiff[WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_LENGTH];          ///< Mic speech level difference for wind noise suppression

    /**
    Threshold on the wind probability below which wind noise suppression is
    disabled. A higher value means less wind noise detections.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT (6554)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_DetectorThreshold;          ///< Threshold for wind noise detection

    /**
    Upper frequency used for one of the wind noise detection features. <br>
    Unit: Hz<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MIN (350)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_DEFAULT (1000)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORMAXFREQ_MAX (2000)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_DetectorMaxFreq;          ///< Upper frequency for wind noise detection

    /**
    The 3dB corner frequency of the high pass filter that was applied before
    NoiseVoid. This is needed by our wind noise suppression algorithm in order to
    do a correct detection. It is recommended to set this parameter to the same
    value as LVHPF CornerFreq. <br>
    Unit: Hz<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT (50)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HPFCornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    Parameter to control the amount of 1-mic wind noise suppression. A higher value
    means more wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT (256)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_Aggressiveness;          ///< Aggressiveness of 1-mic wind noise suppression

    /**
    The maximum amount of wind noise suppression 1-mic. WNS_MaxAttenuation =
    \f$32767*10^{-max noise reduction[dB]/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXATTENUATION_DEFAULT (8231)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXATTENUATION_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_MaxAttenuation;          ///< Maximum wind noise attenuation 1-mic

    /**
    Threshold for wind gush suppression 1-mic . A higher threshold means less
    suppression of wind gushes.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT (9830)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_GushThreshold;          ///< Threshold for wind gush suppression 1-mic

    /**
    High frequency slope used for 1-mic wind noise suppression. <br>
    Unit: dB<br>
    A higher value allows more wind reduction. A smaller value can be used to
    protect high frequency speech components.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN (-20)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT (-6)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HFSlope;          ///< High frequency slope used for 1-mic wind noise suppression

    /**
    Scale factor of powers used for 1-mic wind noise suppression. <br>
    Unit: dB<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN {-96,-96}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT {0,0}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX {32,32}</td>
    </tr>
    </table> */
    LVM_INT16 WNS_EnergyScaleFactor[WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH];          ///< Scale factor of powers used for 1-mic wind noise suppression

} WindNoiseSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the PathChangeDetector_SetControlParameters function but they
* will not take effect until the next call to the PathChangeDetector_Process function.
*
* @see PathChangeDetector_SetControlParameters
* @see PathChangeDetector_Process
*/
typedef struct
{
    /**
    Length of the background adaptive echo cancellation filter. The length should
    be chosen such that only the main peak of the impulse response is covered by
    the filter. The length should be a multiple of 8. This parameter only takes
    effect from LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_MIN (16)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT (32)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_MAX (64)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_Taps;          ///< Number of PCD taps

    /**
    Adaptive step size control for the background adaptive echo cancellation
    filter. This parameter only takes effect from LifeVibes&trade; VoiceExperience
    4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_MIN (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_DEFAULT (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_Erl;          ///< Step size control for PCD

    /**
    Threshold to detect acoustical path changes. This parameter only takes effect
    from LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN (0)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT (5000)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_Threshold;          ///< Threshold for PCD

    /**
    If a path change is detected, NLMS_erl of the NLMS is set to this value, to
    ensure a fast adapting filter. This parameter only takes effect from
    LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_MinimumErl;          ///< Step size control of NLMS in case of PCD

    /**
    After a path change, the NLMS_erl of the NLMS increases back to the nominal
    value. The speed to increase can be controlled by this parameter. This
    parameter only takes effect from LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN (16384)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT (16800)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_ErlStep;          ///< Speed with which nominal erl value is restored in case of PCD

    /**
    Gain factor applied to the echo subtraction during acoustical path change. This
    parameter only takes effect from LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN (0)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT (2048)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_GammaERescue;          ///< Echo oversubtraction factor in case of PCD

} PathChangeDetector_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the SpeechDetector_SetControlParameters function but they
* will not take effect until the next call to the SpeechDetector_Process function.
*
* @see SpeechDetector_SetControlParameters
* @see SpeechDetector_Process
*/
typedef struct
{
    /**
    Threshold for far-end activity detection. This parameter only takes effect from
    LifeVibes&trade; VoiceExperience 4.0 onwards.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_DEFAULT (16384)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_FarThreshold;          ///< Far-end activity detection threshold

    /**
    A parameter representing a threshold for the detection of instrumental noise.
    The higher the value for this parameter, the lower the noise suppression
    performance.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT (29)</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_CAL_MicPowFloorMin;          ///< Instrumental noise threshold

    /**
    Sensitivity parameter for broadside detection. The higher the value for this
    parameter, the more sensitive the detector.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN (6554)</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT (11470)</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX (19661)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_IMCOH_BroadsideLevelScale;          ///< Broadside sensitivity

    /**
    Sensitivity parameter for position change. The higher the value for this
    parameter, the less sensitive the detector.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_PosChange_Sensitivity;          ///< Position change sensitivity

    /**
    This parameter is used to determine the period during which speech is protected
    after handset position change. It defines the number of beamformer updates
    which must occur before the system is defined to be well adjusted to the new
    position.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT (60)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_PosChange_ProtectPeriod;          ///< Speech protection period after handset position change

    /**
    This parameter is used to determine the period during which speech is protected
    after a change in noise characteristics. It defines the number of frames which
    must occur before noise suppression goes back after a period of silence is
    detected.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_MaxConvergencePeriod;          ///< Speech protection period after silence period is detected

    /**
    This parameter is used to determine the maximum amplitude used in low-level
    noise detection.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT (41)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_NoiseChangeFloor;          ///< Floor for idle noise detection in noise change detection

    /**
    This parameter is the threshold to determine if a noise characteristics change
    has occured.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT (3840)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SDET_NoiseChangeThreshold;          ///< Threshold for noise change detection

    /**
    Level difference in dB for each microphone, as compared to some reference
    microphone(s), which suggests that microphone is covered and triggers special
    processing.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN {0,0}</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT {96,96}</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX {96,96}</td>
    </tr>
    </table> */
    LVM_INT16 SDET_MicCovThreshold[SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH];          ///< SDET microphone coverage threshold for each microphone.

} SpeechDetector_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNV_SetControlParameters function but they
* will not take effect until the next call to the LVNV_Process function.
*
* @see LVNV_SetControlParameters
* @see LVNV_Process
*/
typedef struct
{
    /**
    */
    LVM_Mode_en OperatingMode;          ///< Operating Mode

    /**
    Control word to select between Handset and Speakerphone mode.
    */
    LVM_Config_en Config;          ///< Configuration

    /**
    The mode word to enable/disable internal algorithm blocks of LVNV.
    */
    LVNV_ModeWord_bm Mode;          ///< Mode word

    /**
    Extension of the mode word to enable/disable internal algorithm blocks of LVNV.
    */
    LVNV_ModeWord2_bm Mode2;          ///< Extended Mode word

    /**
    When the tuning mode word is enabled, the output of the module is replaced by a
    special signal, which provides knowledge on the internal state of the
    algorithm. Only one bit is allowed to be enabled at a time.
    */
    LVNV_TuningModeWord_bm TuningMode;          ///< Tuning mode word

    /**
    Sets the control for the microphone.
    */
    LVNV_MicrophoneControlWord_bm MicrophoneMode[LVNV_MICROPHONEMODE_LENGTH];          ///< Microphone mode word

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_DEFAULT {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_ARRAYMAX {400,400}</td>
    </tr>
    </table> */
    LVM_UINT16 MicrophonePositionX[LVNV_MICROPHONEPOSITIONX_LENGTH];          ///< Microphone X Position in millimeters.

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_DEFAULT {0,90}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_ARRAYMAX {400,400}</td>
    </tr>
    </table> */
    LVM_UINT16 MicrophonePositionY[LVNV_MICROPHONEPOSITIONY_LENGTH];          ///< Microphone Y Position in millimeters.

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_DEFAULT {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_ARRAYMAX {400,400}</td>
    </tr>
    </table> */
    LVM_UINT16 MicrophonePositionZ[LVNV_MICROPHONEPOSITIONZ_LENGTH];          ///< Microphone Z Position in millimeters.

    /**
    Type of input signal for each channel.
    */
    LVM_ChannelType_en ChannelType[LVNV_CHANNELTYPE_LENGTH];          ///< Type of input signal for each channel.

    /**
    Gain applied to the Microphone channel in the Tx input. Used to scale the
    microphone signal in order to give the NLMS_LB and NLMS_HB filters headroom for
    correct operation.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_DEFAULT {8192,8192}</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_ARRAYMAX {32767,32767}</td>
    </tr>
    </table> */
    LVM_INT16 MicrophoneInputGain[LVNV_MICROPHONEINPUTGAIN_LENGTH];          ///< The microphone input gain.

    /**
    Gain applied to the Tx output of LVNV. Compensates for the applied
    MicrophoneInputGain in order to preserve the overall Tx gain.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVNV_OUTPUTGAIN_MIN (0)</td>
        <td>@ref LVNV_OUTPUTGAIN_DEFAULT (2048)</td>
        <td>@ref LVNV_OUTPUTGAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 OutputGain;          ///< The output gain.

    /**
    Array of Microphone dependent parameters.
    */
    AcousticEchoCanceller_ControlParams_st AECParams[LVNV_AECPARAMS_LENGTH];          ///< AEC Params.

    /**
    */
    SpeechDetector_ControlParams_st SDETParams;          ///< Speech Detector Params

    /**
    */
    Beamformer_ControlParams_st BMFParams;          ///< Beamformer Params

    /**
    */
    GeneralSidelobeCanceller_ControlParams_st GSCParams;          ///< General Sidelobe Canceller Params

    /**
    */
    EchoSuppression_ControlParams_st EchoSuppressionParams;          ///< Echo Suppression Params

    /**
    */
    NoiseSuppression_ControlParams_st NoiseSuppressionParams;          ///< Noise Suppression Params

    /**
    */
    WindNoiseSuppression_ControlParams_st WindNoiseSuppressionParams;          ///< WindNoise Suppression Params

    /**
    */
    PathChangeDetector_ControlParams_st PCDParams;          ///< Echo Path Change Detector Params

} LVNV_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVHF_SetControlParameters function but they
* will not take effect until the next call to the LVHF_Process function.
*
* @see LVHF_SetControlParameters
* @see LVHF_Process
*/
typedef struct
{
    /**
    */
    LVM_Mode_en OperatingMode;          ///< Operating Mode

    /**
    The mode word to enable/disable internal algorithm blocks of HandsFree
    */
    LVHF_ModeWord_bm Mode;          ///< Mode Word

    /**
    The tuning mode is used to enable/disable internal algorithm tuning
    capabilities in the different blocks of HandsFree. Only one bit allowed to be
    enabled at a time.
    */
    LVHF_TuningModeWord TuningMode;          ///< Tuning Mode

    /**
    Gain applied at the Tx input of LVHF. Used to scale the microphone signal in
    order to give the NLMS filter headroom for correct operation.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVHF_INPUTGAIN_MIN (0)</td>
        <td>@ref LVHF_INPUTGAIN_DEFAULT (8192)</td>
        <td>@ref LVHF_INPUTGAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 InputGain;          ///< Input Gain

    /**
    Gain applied at the Tx output of LVHF. Compensates the applied InputGain in
    order to preserve the overall Tx gain.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVHF_OUTPUTGAIN_MIN (0)</td>
        <td>@ref LVHF_OUTPUTGAIN_DEFAULT (2048)</td>
        <td>@ref LVHF_OUTPUTGAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 OutputGain;          ///< Output Gain

    /**
    Limit the NLMS reference signal. Value in dB Full Scale.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_NLMS_LIMIT_MIN (-24)</td>
        <td>@ref LVHF_NLMS_LIMIT_DEFAULT (0)</td>
        <td>@ref LVHF_NLMS_LIMIT_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_limit;          ///< Limit the NLMS reference signal.

    /**
    Length of the low-band (0-4kHz) adaptive echo cancellation filter. The Length
    should be chosen such that most of the energy of the impulse response is
    covered by the filter. The length should be a multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHF_NLMS_LB_TAPS_MIN (16)</td>
        <td>@ref LVHF_NLMS_LB_TAPS_DEFAULT (64)</td>
        <td>@ref LVHF_NLMS_LB_TAPS_MAX (200)</td>
    </tr>
    </table> */
    LVM_UINT16 NLMS_LB_taps;          ///< Number of taps for LB NLMS.

    /**
    Step size for the update of the low-band (0-4kHz) adaptive filter coefficients.
    It is recommended not to change the default value.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_NLMS_LB_TWO_ALPHA_MIN (0)</td>
        <td>@ref LVHF_NLMS_LB_TWO_ALPHA_DEFAULT (8192)</td>
        <td>@ref LVHF_NLMS_LB_TWO_ALPHA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_LB_two_alpha;          ///< Step Size of LB NLMS

    /**
    Adaptive step size control for the low-band (0-4kHz) adaptive echo cancellation
    filter. This parameter is used to slow down the update speed of the adaptive
    filter during double talk situations.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref LVHF_NLMS_LB_ERL_MIN (64)</td>
        <td>@ref LVHF_NLMS_LB_ERL_DEFAULT (128)</td>
        <td>@ref LVHF_NLMS_LB_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_LB_erl;          ///< ERL of LB NLMS

    /**
    Length of the high-band (4-8kHz) adaptive echo cancellation filter. The Length
    should be chosen such that most of the energy of the impulse response is
    covered by the filter. The length should be a multiple of 8. In case of
    narrowband processing, this parameter has no effect.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHF_NLMS_HB_TAPS_MIN (16)</td>
        <td>@ref LVHF_NLMS_HB_TAPS_DEFAULT (64)</td>
        <td>@ref LVHF_NLMS_HB_TAPS_MAX (136)</td>
    </tr>
    </table> */
    LVM_UINT16 NLMS_HB_taps;          ///< Number of taps for HB NLMS.

    /**
    Step size for the update of the high-band (4-8kHz) adaptive filter
    coefficients. It is recommended not to change the default value. In case of
    narrowband processing, this parameter has no effect.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_NLMS_HB_TWO_ALPHA_MIN (0)</td>
        <td>@ref LVHF_NLMS_HB_TWO_ALPHA_DEFAULT (8192)</td>
        <td>@ref LVHF_NLMS_HB_TWO_ALPHA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_HB_two_alpha;          ///< Step Size of HB NLMS

    /**
    Adaptive step size control for the high-band (4-8kHz) adaptive echo
    cancellation filter. This parameter is used to slow down the update speed of
    the adaptive filter during double talk situations. In case of narrowband
    processing, this parameter has no effect.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref LVHF_NLMS_HB_ERL_MIN (64)</td>
        <td>@ref LVHF_NLMS_HB_ERL_DEFAULT (128)</td>
        <td>@ref LVHF_NLMS_HB_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_HB_erl;          ///< ERL of HB NLMS

    /**
    Indicates whether a preset of adaptive filter coefficients should be done. <br>
    0 = No preset (Coefficients as they are)<br>
    1= Preset with internally calculated coefficients.<br>
    2= Preset with zero coefficients.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_NLMS_PRESET_COEFS_MIN (0)</td>
        <td>@ref LVHF_NLMS_PRESET_COEFS_DEFAULT (1)</td>
        <td>@ref LVHF_NLMS_PRESET_COEFS_MAX (2)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_preset_coefs;          ///< Coefficient preset.

    /**
    Offset added to the normalization of the adaptation of the filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_NLMS_OFFSET_MIN (0)</td>
        <td>@ref LVHF_NLMS_OFFSET_DEFAULT (767)</td>
        <td>@ref LVHF_NLMS_OFFSET_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 NLMS_offset;          ///< NLMS offset

    /**
    Echo subtraction factor applied during farend-only for the LF (below
    EchoCutoff_LF).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LF_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LF_DEFAULT (512)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_high_LF;          ///< FE Echo subtraction LF

    /**
    Echo subtraction factor applied during double talk for the LF (below
    EchoCutoff_LF).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LF_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LF_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_dt_LF;          ///< DT Echo subtraction factor LF

    /**
    Echo subtraction factor applied during nearend-only for the LF (below
    EchoCutoff_LF).
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LF_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LF_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_low_LF;          ///< NE Echo subtraction factor LF

    /**
    Cutoff frequency of echo suppression in Hz for low frequency band in low-band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_DENS_ECHO_CUTOFF_LF_MIN (0)</td>
        <td>@ref LVHF_DENS_ECHO_CUTOFF_LF_DEFAULT (600)</td>
        <td>@ref LVHF_DENS_ECHO_CUTOFF_LF_MAX (4000)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_echo_cutoff_LF;          ///< Cut off frequency of LF echo suppression in Hz

    /**
    Parameter related to the reverberation of the acoustic environment. It
    represents the decay in time of the energy of the echo tail of the impulse
    response for the LB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_LB_DEFAULT (25395)</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_tail_alpha_LB;
    /**
    Parameter related to the portion of the echo tail that has to be extrapolated
    in time for LB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_LB_DEFAULT (29491)</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_tail_portion_LB;
    /**
    Echo subtraction factor applied during farend-only for the LB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LB_DEFAULT (512)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_high_LB;          ///< FE Echo subtraction LB

    /**
    Echo subtraction factor applied during double talk for the LB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LB_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_dt_LB;          ///< DT Echo subtraction factor LB

    /**
    Echo subtraction factor applied during nearend-only for the LB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LB_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_LB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_low_LB;          ///< NE Echo subtraction factor LB

    /**
    The amount of extra non-linear suppression relative to the maximum linear echo
    suppression for the LB. The use of these non-linear echo suppression mechanisms
    should be avoided as much as possible since it affects the double talk
    performance.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LB_MIN (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LB_DEFAULT (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LB_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_NL_atten_LB;          ///< LB Non linear attenuation

    /**
    Parameter related to the reverberation of the acoustic environment. It
    represents the decay in time of the energy of the echo tail of the impulse
    response for the HB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_HB_DEFAULT (25395)</td>
        <td>@ref LVHF_DENS_TAIL_ALPHA_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_tail_alpha_HB;
    /**
    Parameter related to the portion of the echo tail that has to be extrapolated
    in time for HB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_HB_DEFAULT (29491)</td>
        <td>@ref LVHF_DENS_TAIL_PORTION_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_tail_portion_HB;
    /**
    Echo subtraction factor applied during farend-only for the HB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_HB_DEFAULT (512)</td>
        <td>@ref LVHF_DENS_GAMMA_E_HIGH_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_high_HB;          ///< FE Echo subtraction HB

    /**
    Echo subtraction factor applied during double talk for the HB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_HB_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_DT_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_dt_HB;          ///< DT Echo subtraction factor HB

    /**
    Echo subtraction factor applied during nearend-only for the HB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_HB_DEFAULT (256)</td>
        <td>@ref LVHF_DENS_GAMMA_E_LOW_HB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_low_HB;          ///< NE Echo subtraction factor HB

    /**
    The amount of extra non-linear suppression relative to the maximum linear echo
    suppression. for the HB. The use of these non-linear echo suppression
    mechanisms should be avoided as much as possible since it affects the double
    talk performance.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVHF_DENS_NL_ATTEN_HB_MIN (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_HB_DEFAULT (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_HB_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_NL_atten_HB;          ///< NE Echo subtraction factor HB

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    nearend-only.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LOW_MIN (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LOW_DEFAULT (0)</td>
        <td>@ref LVHF_DENS_NL_ATTEN_LOW_MAX (2048)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_NL_atten_Low;          ///< Non-linear echo subtraction factor for lowest frequencies

    /**
    Maximal echo scaling factor additionally applied to the top band (above 7kHz).
    The true factor applied depends on how much the echo estimate is above the
    floor. A value of 32767 means no additional attenuation is applied. A value of
    8192 would allow up to 12dB of additional echo suppression in the top band.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_ECHO_SCALE_TB_MIN (0)</td>
        <td>@ref LVHF_DENS_ECHO_SCALE_TB_DEFAULT (32767)</td>
        <td>@ref LVHF_DENS_ECHO_SCALE_TB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_echo_scale_TB;          ///< Echo scaling factor for top band

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequencies below echo_cutoff_LF
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MIN (0)</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_DEFAULT (0)</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_LF_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_echo_relax_NE_threshold_LF;          ///< Echo relaxation threshold for low frequencies

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequencies above EchoCutoff_LF
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MIN (0)</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_DEFAULT (0)</td>
        <td>@ref LVHF_DENS_ECHO_RELAX_NE_THRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_echo_relax_NE_threshold;          ///< Echo relaxation threshold

    /**
    Smoothing factor applied when switching between gamma_e values.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_GAMMA_E_ALPHA_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_E_ALPHA_DEFAULT (24000)</td>
        <td>@ref LVHF_DENS_GAMMA_E_ALPHA_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_e_alpha;          ///< Smoothing factor for gamma_e

    /**
    Gain factor for Noise subtraction.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_GAMMA_N_MIN (0)</td>
        <td>@ref LVHF_DENS_GAMMA_N_DEFAULT (280)</td>
        <td>@ref LVHF_DENS_GAMMA_N_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_gamma_n;          ///< Gain factor

    /**
    Threshold for nearend activity detection.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_SPDET_NEAR_MIN (0)</td>
        <td>@ref LVHF_DENS_SPDET_NEAR_DEFAULT (512)</td>
        <td>@ref LVHF_DENS_SPDET_NEAR_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_spdet_near;
    /**
    Threshold for microphone activity detection.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_DENS_SPDET_ACT_MIN (0)</td>
        <td>@ref LVHF_DENS_SPDET_ACT_DEFAULT (768)</td>
        <td>@ref LVHF_DENS_SPDET_ACT_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_spdet_act;
    /**
    The maximum amount of noise suppression. <br>
    DENS_limit_ns = \f$32767*10^{-MaxNoiseReduction[dB]/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_LIMIT_NS_MIN (0)</td>
        <td>@ref LVHF_DENS_LIMIT_NS_DEFAULT (10361)</td>
        <td>@ref LVHF_DENS_LIMIT_NS_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_limit_ns;          ///< Maximum noise suppression.

    /**
    The maximum amount of noise suppression for top band. Currently, this parameter
    has no effect.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_LIMIT_NS_TB_MIN (0)</td>
        <td>@ref LVHF_DENS_LIMIT_NS_TB_DEFAULT (5827)</td>
        <td>@ref LVHF_DENS_LIMIT_NS_TB_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_limit_ns_TB;          ///< Maximum noise suppression for top band

    /**
    The amount of comfort noise insertion.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref LVHF_DENS_CNI_GAIN_MIN (0)</td>
        <td>@ref LVHF_DENS_CNI_GAIN_DEFAULT (16384)</td>
        <td>@ref LVHF_DENS_CNI_GAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_CNI_Gain;          ///< CNI gain

    /**
    The window length (expressed in integer multiples of 10ms) for the estimation
    of the noise components. It is recommended to keep this parameter at its
    default value.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_DENS_NFE_BLOCKSIZE_MIN (0)</td>
        <td>@ref LVHF_DENS_NFE_BLOCKSIZE_DEFAULT (150)</td>
        <td>@ref LVHF_DENS_NFE_BLOCKSIZE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_NFE_blocksize;          ///< NFE blocksize

    /**
    Level to which the echo estimate is clipped inside the post-processor.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_DENS_ECHO_CLIPPING_LEVEL_MIN (0)</td>
        <td>@ref LVHF_DENS_ECHO_CLIPPING_LEVEL_DEFAULT (32767)</td>
        <td>@ref LVHF_DENS_ECHO_CLIPPING_LEVEL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 DENS_echo_clipping_level;          ///< Clipping level for echo inside DENS

    /**
    Parameter to control the amount of wind noise suppression. A higher value means
    more wind noise suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_WNS_AGGRESSIVENESS_MIN (0)</td>
        <td>@ref LVHF_WNS_AGGRESSIVENESS_DEFAULT (256)</td>
        <td>@ref LVHF_WNS_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_Aggressiveness;          ///< Aggressiveness of wind noise suppression

    /**
    Scale factor of powers used for wind noise suppression. <br>
    Unit: dB<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_WNS_POWERSCALEFACTOR_MIN (-96)</td>
        <td>@ref LVHF_WNS_POWERSCALEFACTOR_DEFAULT (0)</td>
        <td>@ref LVHF_WNS_POWERSCALEFACTOR_MAX (32)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_PowerScaleFactor;          ///< Scale factor of powers used for wind noise suppression

    /**
    The maximum amount of wind noise suppression. WNS_MaxAttenuation =
    \f$32767*10^{-max noise reduction[dB]/20}\f$
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_WNS_MAXATTENUATION_MIN (0)</td>
        <td>@ref LVHF_WNS_MAXATTENUATION_DEFAULT (8231)</td>
        <td>@ref LVHF_WNS_MAXATTENUATION_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_MaxAttenuation;          ///< Maximum wind noise attenuation

    /**
    High frequency slope used for wind noise suppression. <br>
    Unit: dB<br>
    A higher value allows more wind reduction. A smaller value can be used to
    protect high frequency speech components.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_WNS_HFSLOPE_MIN (-20)</td>
        <td>@ref LVHF_WNS_HFSLOPE_DEFAULT (-6)</td>
        <td>@ref LVHF_WNS_HFSLOPE_MAX (0)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HFSlope;          ///< High frequency slope used for wind noise suppression

    /**
    Threshold for wind gush suppression. A higher threshold means less suppression
    of wind gushes.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_WNS_GUSHTHRESHOLD_MIN (0)</td>
        <td>@ref LVHF_WNS_GUSHTHRESHOLD_DEFAULT (9830)</td>
        <td>@ref LVHF_WNS_GUSHTHRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_GushThreshold;          ///< Threshold for wind gush suppression

    /**
    The 3dB corner frequency of the high pass filter that was applied before LVHF.
    This is needed by our wind noise suppression algorithm in order to do a correct
    detection. It is recommended to set this parameter to the same value as LVHPF
    CornerFreq. <br>
    Unit: Hz<br>

    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVHF_WNS_HPFCORNERFREQ_MIN (0)</td>
        <td>@ref LVHF_WNS_HPFCORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVHF_WNS_HPFCORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_INT16 WNS_HPFCornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    Threshold for farend activity detection.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_SPDET_FAR_MIN (0)</td>
        <td>@ref LVHF_SPDET_FAR_DEFAULT (16384)</td>
        <td>@ref LVHF_SPDET_FAR_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SPDET_far;
    /**
    Threshold for microphone activity detection.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_SPDET_MIC_MIN (0)</td>
        <td>@ref LVHF_SPDET_MIC_DEFAULT (16384)</td>
        <td>@ref LVHF_SPDET_MIC_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SPDET_mic;
    /**
    Threshold to activate the non-linear echo-suppression.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_SPDET_X_CLIP_MIN (0)</td>
        <td>@ref LVHF_SPDET_X_CLIP_DEFAULT (0)</td>
        <td>@ref LVHF_SPDET_X_CLIP_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 SPDET_x_clip;
    /**
    Threshold to detect acoustical path changes.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVHF_PCD_THRESHOLD_MIN (0)</td>
        <td>@ref LVHF_PCD_THRESHOLD_DEFAULT (20000)</td>
        <td>@ref LVHF_PCD_THRESHOLD_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_threshold;          ///< PCD threshold

    /**
    Length of the background adaptive filter. The length should be chosen such that
    only the main peak of the impulse response is covered by the filter. The length
    should be a multiple of 8.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHF_PCD_TAPS_MIN (16)</td>
        <td>@ref LVHF_PCD_TAPS_DEFAULT (16)</td>
        <td>@ref LVHF_PCD_TAPS_MAX (64)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_taps;          ///< Number of taps of background NLMS.

    /**
    Adaptive step size control for the background adaptive filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref LVHF_PCD_ERL_MIN (64)</td>
        <td>@ref LVHF_PCD_ERL_DEFAULT (64)</td>
        <td>@ref LVHF_PCD_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_erl;          ///< Step size of background NLMS.

    /**
    If a path change is detected, NLMS_erl of the NLMS is set to this value, to
    ensure a fast adapting filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref LVHF_PCD_MINIMUM_ERL_MIN (64)</td>
        <td>@ref LVHF_PCD_MINIMUM_ERL_DEFAULT (64)</td>
        <td>@ref LVHF_PCD_MINIMUM_ERL_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_minimum_erl;          ///< Step size of NLMS after PCD.

    /**
    After a path change the NLMS_erl of the NLMS increases back to the nominal
    value. The speed to increase can be controlled by this parameter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref LVHF_PCD_ERL_STEP_MIN (16384)</td>
        <td>@ref LVHF_PCD_ERL_STEP_DEFAULT (16800)</td>
        <td>@ref LVHF_PCD_ERL_STEP_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_erl_step;          ///< Step of ERL increase after PCD.

    /**
    Gain factor applied to the echo subtraction during acoustical path change.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVHF_PCD_GAMMA_E_RESCUE_MIN (0)</td>
        <td>@ref LVHF_PCD_GAMMA_E_RESCUE_DEFAULT (5000)</td>
        <td>@ref LVHF_PCD_GAMMA_E_RESCUE_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 PCD_gamma_e_rescue;          ///< gamma_e after PCD

} LVHF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVBD_SetControlParameters function but they
* will not take effect until the next call to the LVBD_Process function.
*
* @see LVBD_SetControlParameters
* @see LVBD_Process
*/
typedef struct
{
    /**
    Set Bulk Delay operating mode on/off.
    */
    LVM_Mode_en BD_OperatingMode;          ///< Set Bulk Delay operating mode on/off

    /**
    This parameter compensates for the fixed delay in the echo path and can be
    measured in advance. This delay is caused by the audio I/O buffering, AD/DA
    converter and propagation time between the loudspeaker and microphone. The unit
    of 'BulkDelay' is [number of samples] at the respective sampling rate.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVBD_BULKDELAY_MIN (0)</td>
        <td>@ref LVBD_BULKDELAY_DEFAULT (0)</td>
        <td>@ref LVBD_BULKDELAY_MAX (6400)</td>
    </tr>
    </table> */
    LVM_UINT16 BulkDelay;          ///< Sets Bulk Delay

    /**
    This param sets the gain for the Echo reference signal. Value 8192 belong to
    0dB
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVBD_BD_GAIN_MIN (0)</td>
        <td>@ref LVBD_BD_GAIN_DEFAULT (8192)</td>
        <td>@ref LVBD_BD_GAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 BD_Gain;          ///< Apply Gain to Echo reference signal

} LVBD_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVINPUTROUTING_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVINPUTROUTING_Tx_Process function.
*
* @see LVINPUTROUTING_Tx_SetControlParameters
* @see LVINPUTROUTING_Tx_Process
*/
typedef struct
{
    /**
    Selects the input channel for the first microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.
    */
    LVBuffer_Channel_en MicrophoneChannel_0;          ///< Channel routing for the first microphone input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the second microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.
    */
    LVBuffer_Channel_en MicrophoneChannel_1;          ///< Channel routing for the second microphone input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the third microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.
    */
    LVBuffer_Channel_en MicrophoneChannel_2;          ///< Channel routing for the third microphone input of the LVVE_Tx_Process function.

} LVINPUTROUTING_Tx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVOUTPUTROUTING_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVOUTPUTROUTING_Tx_Process function.
*
* @see LVOUTPUTROUTING_Tx_SetControlParameters
* @see LVOUTPUTROUTING_Tx_Process
*/
typedef struct
{
    /**
    Selects the origin for the first audio channel of the output stream. The origin
    can be selected from the processed output of the VoiceExperience processing or
    from traces of internal signals (e.g. the AEC Reference).
    */
    LVVE_Tx_OutputChannel_en OutputChannel_0;          ///< Channel routing for the output channels returned by the Process function.

} LVOUTPUTROUTING_Tx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVLPF_SetControlParameters function but they
* will not take effect until the next call to the LVLPF_Process function.
*
* @see LVLPF_SetControlParameters
* @see LVLPF_Process
*/
typedef struct
{
    /**
    Turns on/off Low Pass filter.
    */
    LVM_Mode_en LPF_OperatingMode;          ///< Turns on/off Low Pass filter

    /**
    Sets the 3dB corner frequency of the low-pass filter. In case NoiseVoid is
    enabled, the same low-pass filter will be applied to the second microphone
    channel. The range of frequency is [3500 3900] for 8KHz, [3500 7800] for 16KHz,
    [3500, 11700] for 24KHz, [3500, 15600] for 32KHz and [3500, 23400] for 48KHz
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_MIN (3500)</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_DEFAULT (3700)</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_MAX (23400)</td>
    </tr>
    </table> */
    LVM_UINT16 LPF_CornerFreq;          ///< Low Pass Filter Corner Frequency in Hz

} LVLPF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVE_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVVE_Tx_Process function.
*
* @see LVVE_Tx_SetControlParameters
* @see LVVE_Tx_Process
*/
typedef struct
{
    /**
    Microphone Routing Control Parameter Structure.
    */
    LVINPUTROUTING_Tx_ControlParams_st MicrophoneRouting;          ///< Microphone Routing Control Parameter Structure

    /**
    Output Routing Control Parameter Structure.
    */
    LVOUTPUTROUTING_Tx_ControlParams_st OutputRouting;          ///< Output Routing Control Parameter Structure

    /**
    This enumerated type is used to set the operating mode of the Tx path. The
    processing can be separately set to enable all processing modules (i.e., ON),
    or to disable all processing modules (i.e., OFF). When OFF, the input is copied
    to the output with the applied channel routing and the LVVE sub modules can be
    provided with invalid parameters for modules. The sub module functions are not
    executed in this scenario.
    */
    LVM_Mode_en OperatingMode;          ///< Turns on/off all VoiceExperience processing modules on Tx

    /**
    This param can mute unmute the Rx/Tx engine output.
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

    /**
    Set Bulk Delay operating mode on/off.
    */
    LVM_Mode_en BD_OperatingMode;          ///< Set Bulk Delay operating mode on/off

    /**
    This parameter compensates for the fixed delay in the echo path and can be
    measured in advance. This delay is caused by the audio I/O buffering, AD/DA
    converter and propagation time between the loudspeaker and microphone. The unit
    of 'BulkDelay' is [number of samples] at the respective sampling rate.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_BULKDELAY_MIN (0)</td>
        <td>@ref LVVE_TX_BULKDELAY_DEFAULT (0)</td>
        <td>@ref LVVE_TX_BULKDELAY_MAX (6400)</td>
    </tr>
    </table> */
    LVM_UINT16 BulkDelay;          ///< Sets Bulk Delay

    /**
    This param sets the gain for the Echo reference signal. Value 8192 belong to
    0dB
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVVE_TX_BD_GAIN_MIN (0)</td>
        <td>@ref LVVE_TX_BD_GAIN_DEFAULT (8192)</td>
        <td>@ref LVVE_TX_BD_GAIN_MAX (32767)</td>
    </tr>
    </table> */
    LVM_INT16 BD_Gain;          ///< Apply Gain to Echo reference signal

    /**
    Turns on/off VOL_Gain.
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVE_TX_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVE_TX_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVE_TX_VOL_GAIN_MAX (24)</td>
    </tr>
    </table> */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

    /**
    Turns on/off High Pass filter.
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table> */
    LVM_UINT16 MIC_HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    Turns on/off Low Pass filter.
    */
    LVM_Mode_en LPF_OperatingMode;          ///< Turns on/off Low Pass filter

    /**
    Sets the 3dB corner frequency of the low-pass filter. In case NoiseVoid is
    enabled, the same low-pass filter will be applied to the second microphone
    channel. The range of frequency is [3500 3900] for 8KHz, [3500 7800] for 16KHz,
    [3500, 11700] for 24KHz, [3500, 15600] for 32KHz and [3500, 23400] for 48KHz
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_MIN (3500)</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT (3700)</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_MAX (23400)</td>
    </tr>
    </table> */
    LVM_UINT16 MIC_LPF_CornerFreq;          ///< Low Pass Filter Corner Frequency in Hz

    /**
    HandsFree Control Parameter Strcuture.
    */
    LVHF_ControlParams_st HF_ControlParams;          ///< HandsFree Control Parameter Structure

    /**
    NoiseVoid Control Parameter Structure.
    */
    LVNV_ControlParams_st NV_ControlParams;          ///< NoiseVoid Control Parameter Structure

    /**
    WhisperMode Control Parameter Structure.
    */
    LVWM_ControlParams_st WM_ControlParams;          ///< WhisperMode Control Parameter Structure

    /**
    Equalizer Operating mode.
    */
    LVM_Mode_en EQ_OperatingMode;          ///< Equalizer Operating mode

    /**
    Equalizer Control Parameter Structure.
    */
    LVEQ_ControlParams_st EQ_ControlParams;          ///< Equalizer Control Parameters Structure

    /**
    DRC Control Parameter Structure.
    */
    LVDRC_ControlParams_st DRC_ControlParams;          ///< DRC Control Parameter Structure

    /**
    Turns on/off Conformt Noise Generator Module.
    */
    LVM_Mode_en CNG_OperatingMode;          ///< Turns on/off Comfort Noise Generator Module

    /**
    Comfort Noise Generator Control Parameter Structure.
    */
    LVCNG_ControlParams_st CNG_ControlParams;          ///< Comfort Noise Generator Control Parameter Structure

} LVVE_Tx_ControlParams_st;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif      /* __REL_LVVE_HF_NV_AVC_AGC_RX_WBE_DRC_RX_SWB_VIDPP_H__ */

/* End of file */
