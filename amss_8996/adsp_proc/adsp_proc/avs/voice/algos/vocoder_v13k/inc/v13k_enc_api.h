/*======================= COPYRIGHT NOTICE ======================*/
/* Copyright (c) 2006 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*===============================================================*/

#ifndef __V13K_ENC_API_H
#define __V13K_ENC_API_H

#include "tty_api.h"

extern int16 v13k_get_enc_struct_size(void);

extern void v13k_init_encoder13(void *encStructPtr);

extern void v13k_set_enc_params(void *encStructPtr,
                         uint16 maxRate,
                         uint16 minRate,
                         uint16 rateModulationCmd,
                         uint16 comfortNoiseEnable,
                         uint16 reinitVocBNE,
                         uint16 reducedRateLevel,
                         int16 nrOccurredVocoder);

extern void v13k_lvb_enc(void *sEncoderPtr,
                  int16 *inpSamplesPtr,
                  int16 *pktPtr,
                  int16 ttyEncflag,
                  TTYEncStruct *pTTYEncStruct
                  );

extern int32 v13k_repacking_dsp_to_mvs(int8 *iPacket);

#endif //__v13k_ENC_API_H
