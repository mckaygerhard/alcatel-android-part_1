/*======================= COPYRIGHT NOTICE ======================*/
/* Copyright (c) 2006 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*===============================================================*/
/*---------------------------------------------------------------*/
/* FileName: lvb_export.h           FileType:lvb_export C Header */
/*---------------------------------------------------------------*/
/* Description:                                                  */
/*   Publicizes the functions to systems code                    */
/*---------------------------------------------------------------*/
/* Revision History: None                                        */
/* Author            Date                      Comments          */
/* -------          ------                     ----------        */
/* Syed Arif        26,Dec,2006      Written based on QDSP4 V13K */
/*****************************************************************/

#ifndef LVB_EXPORT_H
#define LVB_EXPORT_H

/* Public function declarations  */
extern int lvb_get_struct_size(void);

extern void lvb_analysis_init(void *plvbStructInstance,
                              int16 wbFlag
                              );
extern void lvb_analysis(void *plvbStructInstance,
                         int16 *buffer,
                         int16 *bnLpc,
                         int16 *bnScaleValue
                         );
#endif
