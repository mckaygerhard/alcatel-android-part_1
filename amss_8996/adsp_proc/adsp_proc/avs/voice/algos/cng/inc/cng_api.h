/*======================= COPYRIGHT NOTICE ==================================*/
/* Copyright (c) 2007 by Qualcomm Technologies, Inc. All rights reserved.                 */
/* All data and information contained in or disclosed by this                */
/* document is confidential and proprietary information of                   */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.              */
/* By accepting this material the recipient agrees that this                 */
/* material and the information contained therein is held in                 */
/* confidence and in trust and will not be used, copied,                     */
/* reproduced in whole or in part, nor its contents revealed in              */
/* any manner to others without the express written permission               */
/* of Qualcomm Technologies, Inc.                                                         */
/*===========================================================================*/
/*---------------------------------------------------------------------------*/
/* FileName: cng_export.h         FileType:cng_export C Header               */
/*---------------------------------------------------------------------------*/
/* Description:                                                              */
/*   Publicizes the functions to systems code                                */
/*---------------------------------------------------------------------------*/
/* Revision History: None                                                    */
/* Author            Date                      Comments                      */
/* -------          ------                     ----------                    */
/* Syed Arif        17,Sep,2007      Written based on QDSP4 V13K             */
/* Song Wang        12,Nov,2009      Modify cng_noise_synthesis interface    */
/*                                   adding input buffer for random signal   */
/*                                   generation.                             */
/* Song Wang        01,Oct,2010      modified cng_noise_synthesis interface  */
/*****************************************************************************/

#ifndef CNG_EXPORT_H
#define CNG_EXPORT_H

/* Public function declarations  */
extern int cng_get_struct_size(void);

extern void cng_init_params(void *pCngStructInstance,
                            int16 wbFlag,
                            int16 maxNoise
                            );

extern void cng_noise_synthesis(void *pCngStructInstance,	
                                int16 *OutputPtr,
                                int16 *LPCPtr,
                                int16 noOfSamples,
                                int16 *bnScaleValue,
				                    int16 *inputPtr
                               );
#endif
