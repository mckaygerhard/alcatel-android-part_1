/**
@file ListenStreamMgr_SessionRespHandler.h
@brief This file declares session response handler functions for ListenStreamMgr.

*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_SessionRespHandler.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/18/2012  Sudhir      Initial version
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

#ifndef LISTEN_STREAM_MGR_SESSION_RESP_HANDLER_H
#define LISTEN_STREAM_MGR_SESSION_RESP_HANDLER_H

/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/

/// System
#include "qurt_elite.h"

/// Listen
#include "Elite.h"
#include "ListenStreamMgr_Type.h"


#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*-------------------------------------------------------------------------
Preprocessor Definitions and Constants
-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
Type Declarations
-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
Class Definitions
----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
Function Declarations and Documentation
----------------------------------------------------------------------------*/

/* Session RespQ: processing functions and utility functions */
/*
   This function process appropriate response handler based on opcode .

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param nChannelBit[in] On which bit signal got.  
                           
   @return Success or Failure
*/
ADSPResult lsm_process_session_resp_q(lsm_t *pMe, int32_t nChannelBit);

/* Session RespQ: processing functions and utility functions */
/*
   This function process unsupported messages in the session's response queue.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_unsupported_handler(lsm_t *pMe,
                                              lsm_session_state_type_t *pSession);
											  
											  
/*
   This function process ELITE_CMD_DESTROY_SERVICE messages in the session's response queue.
   This message is an Acknolowledge to LSM from a dynamic service that signals the dynamic service
   is ready to be destroyed.  LSM needs to qurt_thread_join on the dynamic service's thread to avoid
   memory leak. To avoid block in system, dynamic service SHOULD only send such an ack when it finish
   all blocking operations such as releasing buffer queue etc before they send this ack.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_destroy_svc_handler(lsm_t *pMe,
                                              lsm_session_state_type_t *pSession );

											  


											  
											  
/*
   This function process ELITE_CUSTOM_MSG messages in the session's response queue.
   Typically, need to further look up the secondary opcode in the message payload
   buffer.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_custom_msg_handler(lsm_t *pMe,
                                              lsm_session_state_type_t *pSession );



/*
   This function process ELITE_CMD_SET_PARAM messages in the session's response queue.
   This message is an Acknolowledge to LSM from a dynamic service that signals the dynamic service
   accepts the param in SET_PARAM message.  This is currently used to configure ListenProcSvc.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_set_param_handler( lsm_t *pMe,
                                              lsm_session_state_type_t *pSession );
											  
											  
/*
   This function process ELITE_CMD_GET_PARAM messages in the session's response queue. This is an
   Ack from the dynamic service for the GET_PARAM command.
   This is currently used to configure ListenProcSvc.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_get_param_handler( lsm_t *pMe,
                                              lsm_session_state_type_t *pSession );
											  
											  
											  
/*
  This function generates a generic ACK.

  @param pMe[in/out]      This points to the instance of ListenStreamMgr.
  @param pSession[in/out] This points to the session that are to
                          process the response queue.
  @return
  Indication of success or failure.

  @dependencies
  None.

*/

ADSPResult lsm_session_resp_q_generic_ack( lsm_t *pMe,
                                             lsm_session_state_type_t *pSession );		

/*
   This function process ELITEMSG_CUSTOM_LISTEN_START messages in the session's response queue.
   This message is an Acknolowledge to LSM from a dynamic service that signals the dynamic service
   accepts START comamnd. 

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/						 
ADSPResult lsm_session_resp_q_start_handler(lsm_t *pMe, lsm_session_state_type_t *pSession);

/*
   This function process ELITEMSG_CUSTOM_LISTEN_STOP messages in the session's response queue.
   This message is an Acknolowledge to LSM from a dynamic service that signals the dynamic service
   accepts STOP comamnd. 

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_stop_handler(lsm_t *pMe, lsm_session_state_type_t *pSession);											 

/*
   This function process ELITEMSG_CUSTOM_LISTEN_EOB messages in the session's response queue.
   This message is an Acknolowledge to LSM from a dynamic service that signals the dynamic service
   accepts EOB comamnd.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the message in
                           the response queue.
   @return Success or Failure
*/
ADSPResult lsm_session_resp_q_eob_handler(lsm_t *pMe, lsm_session_state_type_t *pSession);
#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef LISTEN_STREAM_MGR_SESSION_RESP_HANDLER_H

