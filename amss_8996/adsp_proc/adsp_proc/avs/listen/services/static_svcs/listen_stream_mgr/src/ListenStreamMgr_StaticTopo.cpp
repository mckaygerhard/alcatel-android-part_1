/** @file ListenStreamMgr_StaticTopo.cpp

 @brief This file contains Listen Specific Topology management

 */

/*==============================================================================
 $Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_StaticTopo.cpp#1 $

 Edit History

 when       who     what, where, why
 --------   ---     ----------------------------------------------------------
 6/13/2014  Unni    Initial version
 =============================================================================*/

/*------------------------------------------------------------------------------
   Copyright (c) 2012-2014 Qualcomm  Technologies, Inc.  All rights reserved.
   Qualcomm Technologies Proprietary and Confidential.
 -----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * Include Files
 * ---------------------------------------------------------------------------*/
#include "qurt_elite.h"
#include "ListenStreamMgr_StaticTopo.h"
#include "adsp_amdb_static.h"
#include "capi_v2_voicewakeup_v2.h"
#include "adsp_lsm_session_commands.h"
#include "adsp_lsm_service_commands.h"
#include "adsp_core_api.h"
#include "EliteCmnTopology_db.h"


/*------------------------------------------------------------------------------
 * Macro definitions
 * ---------------------------------------------------------------------------*/
#define SIZE_OF_ARRAY(a) (sizeof(a)/sizeof((a)[0]))

/*------------------------------------------------------------------------------
 * Local Type declarations
 * ---------------------------------------------------------------------------*/
typedef struct
{
  int32_t module_id;
  /* module id used for amdb registration */

  capi_v2_get_static_properties_f  get_static_prop_f;
  /**< function pointer to get memory requirements of library. */

   capi_v2_init_f init_f;
   /**< function pointer to init the appi module. */

} lsm_static_appi_capi_v2_info_t;

/*------------------------------------------------------------------------------
 * Data definitions
 * ---------------------------------------------------------------------------*/


static avcs_module_info_t avcs_lsm_none_mod_def =
{
};


static avcs_module_info_t avcs_lsm_mod_def_voice_wakeup_v2 =
{
    LSM_MODULE_VOICE_WAKEUP_V2
};


/* Static Topology definitions */
static avcs_topology_definition_t avcs_lsm_topo_def_none=
{
    0,
  LSM_TX_TOPOLOGY_ID_NONE,
    1 << AVCS_TOPO_CFG_AREA_LSM_BIT,
    0
};

static avcs_topology_definition_t avcs_lsm_topo_def_voice_wakeup_v2=
{
    0,
  LSM_TX_TOPOLOGY_ID_VOICE_WAKEUP_V2,
    1 << AVCS_TOPO_CFG_AREA_LSM_BIT,
    1
};

/* Static Topology List */
static avcs_topology_definition_t avcs_lsm_static_topos[]=
{
  avcs_lsm_topo_def_none,
  avcs_lsm_topo_def_voice_wakeup_v2,
};

static avcs_module_info_t *avcs_lsm_static_topos_mods_ptr[] =
{
  &avcs_lsm_none_mod_def,
  &avcs_lsm_mod_def_voice_wakeup_v2
};

/*------------------------------------------------------------------------------
 * Function definitions
 * ---------------------------------------------------------------------------*/

ADSPResult lsm_add_static_topos_to_cmn_db(void)
{
  ADSPResult result = ADSP_EOK;

  for (uint32_t i=0; i< SIZE_OF_ARRAY(avcs_lsm_static_topos); i++)
  {
    avcs_topology_definition_t *topo_def_ptr = &avcs_lsm_static_topos[i];
    avcs_module_info_t *mod_def_ptr = avcs_lsm_static_topos_mods_ptr[i];

    //Add single topology
    result = elite_cmn_topo_db_add_topology(topo_def_ptr, mod_def_ptr,ELITE_CMN_STATIC_TOPOLOGIES);
    if (ADSP_FAILED(result))
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "elite_cmn_topo_db_add_topology failed with %d", result);
      return result;
    }
  }
  return result;
}


/*
  This function add static topology to centralized topology db and
  loads the static appi libraries into amdb

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
ADSPResult lsm_add_static_topos_and_mods_to_cmn_db(void)
{
  ADSPResult result = ADSP_EOK;

  /* Load static topologies via amdb */
  result = lsm_add_static_topos_to_cmn_db();
  if (ADSP_FAILED(result))
  {
    return result;
  }

  return result;
}

/*
  This function gets topology_id from app_id to support LSM_SESSION_CMD_OPEN_TX
  which uses app_id

  @param[in]   app_id: app_id
  @param[out]  topology_id_ptr: pointer to return value (topology_id)

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
ADSPResult lsm_get_topology_id_from_app_id(uint32_t app_id,
                                           uint32_t *topology_id_ptr)
{
  if(NULL == topology_id_ptr)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Null pointer topology_id_ptr");
    return ADSP_EBADPARAM;
  }

  /* Map app id to module id */
  switch(app_id)
  {
    case LSM_VOICE_WAKEUP_APP_V2:
      *topology_id_ptr = LSM_TX_TOPOLOGY_ID_VOICE_WAKEUP_V2;
    break;

    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "app_id[%d] not supported", app_id);
      return ADSP_EUNSUPPORTED;
    }
    break;
  }

  return ADSP_EOK;
}

