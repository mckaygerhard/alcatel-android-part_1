/**
@file ListenStreamMgr_SessionCmdHandler.cpp

@brief This file contains the implementation for session cmdQ Handler functions
for ListenStreamMgr.

 */

/*========================================================================
$Header $ //source/qcom/qct/multimedia2/Listen/elite/static_svcs/ListenStreamMgr/main/latest/src/ListenStreamMgr_SessionCmdHandler.cpp#18 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------

==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2014 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "ListenStreamMgr_Type.h"
#include "ListenStreamMgr_Session.h"
#include "ListenStreamMgr_AprIf.h"
#include "ListenStreamMgr_AprDataBase.h"
#include "ListenStreamMgr_Session.h"
#include "ListenStreamMgr_SessionCmdHandler.h"
#include "ListenStreamMgr_StaticTopo.h"
#include "ListenProcSvc.h"
#include "ListenProcSvc_CustMsg.h"
#include "EliteMsg_Custom.h"
#include "EliteMsg_AfeCustom.h"
#include "adsp_lsm_session_commands.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/
#define DEFAULT_16_BITS_PER_SAMPLE 16
#define DEFAULT_16K_SAMPLE_RATE 16000

/*----------------------------------------------------------------------------
 * Constant / Define Declarations
 *--------------------------------------------------------------------------*/
extern uint32_t lsm_memory_map_client;

/* =======================================================================
 **                          Function Definitions
 ** ======================================================================= */
/*
   Static function processes to initailize the session state

   @param pSession[in/out] This points to the instance of LSM session State
   @return Success or Failure
*/
static void lsm_init_session_state(lsm_session_state_type_t *pSession);

/* =======================================================================
 **        Session State Machine: CmdQ message handler for APR messages
 ** ======================================================================= */

/*
   This function calls appropriate cmd handler based on opcode received.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param nChannelBit[in] On which bit signal got.
   @return Success or Failure
*/
ADSPResult lsm_process_session_cmd_q(lsm_t *pMe, int32_t nChannelBit)
{
  ADSPResult result = ADSP_EOK;
  uint32_t opcode;
  uint8_t ucSessionId = (nChannelBit >> 1) - 1;
  lsm_session_state_type_t *pSession = &(pMe->aSessions[ucSessionId]);
  /* Take next msg off the q */
  result = qurt_elite_queue_pop_front(pSession->session_cmd_q_ptr, (uint64_t*)&(pSession->processed_msg));
  opcode = pSession->processed_msg.unOpCode;
  switch (opcode)
  {
    /* currently session is supporting only ELITE_APR_PACKET */
    case ELITE_APR_PACKET:
    {
      result = lsm_session_cmd_q_apr_pkt_handler(pMe,pSession);
      break;
    }
    /* this will never trigger */
    default:
    {
      result = lsm_session_cmd_q_unsupported_handler(pMe,pSession);
      break;
    }
  }
  return result;
}

/*
   This function process the client command that are send to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_apr_pkt_handler( lsm_t *pMe,
                                              lsm_session_state_type_t *pSession )
{
  ADSPResult result = ADSP_EOK;
  elite_apr_packet_t *pPkt = (elite_apr_packet_t *) (pSession->processed_msg.pPayload);

  /** Sanity check */
  if ( (ELITE_APR_PACKET != pSession->processed_msg.unOpCode) || (!pPkt) )
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM [%u]: invalid opcode/payload 0x%lx",
          pSession->ucInternalSessionId,
          pSession->processed_msg.unOpCode);

    return ADSP_EFAILED;
  }

  uint32_t ulOpCode = elite_apr_if_get_opcode( pPkt );

  MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM [%u]: receive apr command 0x%lx at port [0x%4x]",
        pSession->ucInternalSessionId, ulOpCode, elite_apr_if_get_dst_port( pPkt ) );

  switch ( ulOpCode )
  {
    case LSM_SESSION_CMD_OPEN_TX:
    case LSM_SESSION_CMD_OPEN_TX_V2:
    {
      lsm_init_session_state(pSession);
      result =  lsm_session_cmd_q_open_tx_stream_handler(pMe, pSession);
      break;
    }

    case LSM_SESSION_CMD_CLOSE:
    {
      result =  lsm_session_cmd_q_close_tx_stream_handler(pMe,pSession);
      break;
    }

    case LSM_SESSION_CMD_SET_PARAMS:
    case LSM_SESSION_CMD_SET_PARAMS_V2:
    {
      result =  lsm_session_cmd_q_set_param_handler(pMe,pSession);
      break;
    }

    case LSM_SESSION_CMD_GET_PARAMS:
    case LSM_SESSION_CMD_GET_PARAMS_V2:
    {
    	result =  lsm_session_cmd_q_get_param_handler(pMe,pSession);
    	break;
    }

    case LSM_SESSION_CMD_REGISTER_SOUND_MODEL:
    {
      result =  lsm_session_cmd_q_register_sound_model_handler(pMe,pSession);
      break;
    }

    case LSM_SESSION_CMD_DEREGISTER_SOUND_MODEL:
    {
      result =  lsm_session_cmd_q_deregister_sound_model_handler(pMe,pSession);
      break;
    }

    case LSM_SESSION_CMD_START:
    {
      result =  lsm_session_cmd_q_start_handler(pMe,pSession);
      break;
    }

    case LSM_SESSION_CMD_STOP:
    {
      result =  lsm_session_cmd_q_stop_handler(pMe,pSession);
      break;
    }
    case LSM_SESSION_CMD_EOB :
    {
      result =  lsm_session_cmd_q_eob_handler(pMe,pSession);
      break;
    }
    default:
    {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM: unsupported command 0x%lx at port [0x%4lx]",
            ulOpCode, (uint32_t)elite_apr_if_get_dst_port( pPkt ) );
      result = ADSP_EUNSUPPORTED;
      /* Generate ACK and free apr packet */
      lsm_generate_ack( pPkt, ADSP_EUNSUPPORTED, NULL, 0, 0);

    }
  }

  return result;
}

/*
  Process unsupported messages in the session's command queue.

  @param TBD

  @return
  Success/Failure

  @dependencies
  None.
 */
ADSPResult lsm_session_cmd_q_unsupported_handler(lsm_t *pMe,
                                                 lsm_session_state_type_t *pSession)
{
  elite_apr_packet_t *pPkt = (elite_apr_packet_t *) (pSession->processed_msg.pPayload);

  MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM : unsupported command 0x%lx at port [0x%4lx]",
        (uint32_t)elite_apr_if_get_opcode( pPkt ), (uint32_t)elite_apr_if_get_dst_port( pPkt ) );

  /*no ack for unsupported commands like in elite_svc_unsupported function*/
  return ADSP_EUNSUPPORTED;
}

/*
   This function process LSM_SESSION_CMD_OPEN_TX command that are send to a
   given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The
                      channel bit in this instance that associated with the
                      given session might be turned on/off depending on command
                      processing.
   @param pSession[in/out] This points to the session that are to process the
                           client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_open_tx_stream_handler(lsm_t *pMe,
                                             lsm_session_state_type_t *pSession)
{
  ADSPResult result=ADSP_EOK;

  lsm_session_cmd_open_tx_t *cmd_open_tx_ptr = NULL;
  lsm_session_cmd_open_tx_v2_t *cmd_open_tx_v2_ptr = NULL;

  elite_apr_packet_t *pkt_ptr =
                       (elite_apr_packet_t*) (pSession->processed_msg.pPayload);
  uint32_t open_cmd_opcode = elite_apr_if_get_opcode(pkt_ptr);

  qurt_elite_queue_t *output_bufq_ptr = NULL;
  /* This pointer is provided to listen dynamic svc for initialization. */
  listen_proc_svc_init_params_t open_params;
  memset(&open_params,0, sizeof(listen_proc_svc_init_params_t));

  /* Handling multiple version of open cmds */
  switch(open_cmd_opcode)
  {
    case LSM_SESSION_CMD_OPEN_TX:
    {
      elite_apr_if_get_payload( (void **)&cmd_open_tx_ptr, pkt_ptr );
      open_params.sampling_rate = cmd_open_tx_ptr->sample_rate;
      result = lsm_get_topology_id_from_app_id(cmd_open_tx_ptr->app_id,
                                               &open_params.topology_id);
      if(ADSP_FAILED(result))
      {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
              "LSM OPEN CMD: Unknown app_id [%d]:: 0x%x ",
              cmd_open_tx_ptr->app_id,result);
        goto done;
      }
      open_params.revision = LISTEN_PROC_OPEN_USING_APP_ID;
    }
    break;

    case LSM_SESSION_CMD_OPEN_TX_V2:
    {
      elite_apr_if_get_payload( (void **)&cmd_open_tx_v2_ptr, pkt_ptr );
      open_params.topology_id = cmd_open_tx_v2_ptr->topology_id;
      open_params.sampling_rate = DEFAULT_16K_SAMPLE_RATE;
      open_params.revision = LISTEN_PROC_OPEN_USING_TOPOLOGY_ID;
    }
    break;

    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "LSM OPEN CMD: Unknown opcode :: 0x%lx ",open_cmd_opcode);
      result = ADSP_EUNEXPECTED;
      goto done;
    }
    break;
  }

  /* sanity check of session state */
  if ( (LISTEN_STREAM_MGR_SESSION_DEINIT != pSession->session_state))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "Session already opened, current State %d",
          pSession->session_state);

    result = ADSP_EALREADY;
    goto done;
  }

  /* compose call back data for Listen proc Svc to raise event to client */
  lsm_callback_handle_t cbData;
  result = lsm_session_fill_cb_data_for_dynamic_svc(pMe, pSession, &cbData);

  if (ADSP_FAILED(result))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM OPEN CMD: FAILED to create call back for Listen session[0x%p]",
          pSession);
    goto done;
  }

  /* hard coding to 16 bits. In future need to expose api for >16 bits */
  open_params.bits_per_sample = DEFAULT_16_BITS_PER_SAMPLE;
  open_params.num_channels =  1;
  open_params.session_id = pSession->ucInternalSessionId;

  /* Send the metadata request to Listen Proc svc in InitParams structure */
  open_params.callback_data_ptr = &cbData;

  result = listen_proc_svc_create ( &open_params,
                                    (void**)(&(pSession->dyn_svc.handle)));

  if (ADSP_FAILED(result) || !(pSession->dyn_svc.handle))
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "OPEN CMD: FAILED to create listen service result[0x%x], handle[%p]",
          result,
          pSession->dyn_svc.handle);
    if(!(pSession->dyn_svc.handle))
    {
      /* Ensure that result is failed if not handle was created */
      result = ADSP_EFAILED;
    }
    goto done;
  }

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Listen ProcSvc Created succesfuly");

  pSession->dyn_svc.thread_id = (pSession->dyn_svc.handle)->threadId;
  pSession->session_state = LISTEN_STREAM_MGR_SESSION_INIT;

   //Register data queue
   output_bufq_ptr = pSession->dyn_svc.handle->gpQ;

   // Register for buffer queue
   if ( ADSP_FAILED(result = lsm_apr_router_register_buf_q(cbData.self_port, output_bufq_ptr) ) )
   {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM [%u]: can not register output bufq",
           pSession->ucInternalSessionId);
     // generate ack and free apr packet
     lsm_generate_ack( pkt_ptr, result, NULL, 0, 0);
     return result;
   }

done:
  /* generate ack and free apr packet */
  lsm_generate_ack( pkt_ptr, result, NULL, 0, 0);
  return result;
}

/*
   This function process   LSM_SESSION_CMD_CLOSE command that are to CLOSE a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_close_tx_stream_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) (pSession->processed_msg.pPayload);

  uint32_t unPayloadSize;
  elite_msg_any_t msg;
  elite_apr_port_t port;
  ADSPResult result = ADSP_EOK;

  if (LISTEN_STREAM_MGR_SESSION_DEINIT == pSession->session_state)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM CLOSE CMD: Invalid request to close an unopen Session %d",
          pSession->session_state);

    /* ACK that this command is not expected in such state and release packet */
    lsm_generate_ack( pPkt, ADSP_EOK, 0 , 0 , 0);
    return ADSP_EOK;
  }
   
  port = elite_apr_if_get_dst_port( pPkt ); 
  /*De Register buf queue*/
  if ( ADSP_FAILED(  lsm_apr_router_deregister_buf_q(port) ) )
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM [%u]: can not deregister output bufq",
           pSession->ucInternalSessionId);
  }

  /*  Destroy dynamic service */
  unPayloadSize = sizeof( elite_msg_cmd_connect_t);
  if ( ADSP_FAILED(result = elite_msg_create_msg( &msg, &unPayloadSize,
                                                  ELITE_CMD_DESTROY_SERVICE, 
                                                  pSession->session_resp_q_ptr,
                                                  0, 
                                                  0 ) ) )
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM CLOSE CMD: Fail to create disconnect message");
    lsm_generate_ack( pPkt, result, NULL, 0, 0 );
    return result;
  }

  if (ADSP_FAILED(result = qurt_elite_queue_push_back(pSession->dyn_svc.handle->cmdQ, (uint64_t*)&msg )))
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM CLOSE CMD: Failed to send disconnect message");
    lsm_generate_ack( pPkt, result, NULL, 0, 0 );
    elite_msg_finish_msg(&msg, ADSP_EFAILED);
    return result;
  }

  /* updated expected response mask */
  pSession->required_resp++ ;

  /* Listen to respQ, switch off  command Q */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);

  return ADSP_EOK;
}

/*
   This function processes  LSM_SESSION_CMD_START command that is sent to a given session

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_start_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  uint32_t ulClientToken=0;
  elite_msg_any_t msg;
  elite_svc_handle_t *pDynSvcHandle;
  uint32_t payloadSize;
  elite_msg_custom_start_t *start_payload_ptr;

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM start cmd Begin");

  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);
  /* Accept start command only when session state is init */
  if ( pSession->session_state != LISTEN_STREAM_MGR_SESSION_INIT )
  {
    if (pSession->session_state == LISTEN_STREAM_MGR_SESSION_ACTIVE )
    {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"SessionStartCmdHandler Already in Active  State");
      result = ADSP_EOK;
      goto __bailout;

    }
    else  /* deinit state */
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"SessionStartCmdHandler Bad State State::0x%x",pSession->session_state);
      result = ADSP_ENOTREADY;
      goto __bailout;
    }
  }

  payloadSize = sizeof(elite_msg_custom_start_t);
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg,&payloadSize ,
                                                   ELITE_CUSTOM_MSG,
                                                   pSession->session_resp_q_ptr,
                                                   ulClientToken,
                                                   0 ) ) )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenStreamMgr [Session ID = %u, ]: Failed to create Elite message",
          pSession->ucInternalSessionId);
    result = ADSP_ENOMEMORY;
    goto __bailout;
  }

  start_payload_ptr =      (elite_msg_custom_start_t *)msg.pPayload;

  start_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_LISTEN_START;

  /* get dynamic command queue and push the msg into it */
  pDynSvcHandle = pSession->dyn_svc.handle;

  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
    elite_msg_release_msg(&msg);
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "SessionStartCmdHandler Failed to send ELITE_CMD_START to dynamic svc");
    result = ADSP_EFAILED;
    goto __bailout;
  }

  /* updated expected response mask */
  pSession->required_resp++ ;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);
  return ADSP_EOK;

__bailout:
  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;

}

/*
   This function processes  LSM_SESSION_CMD_STOP command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_stop_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  elite_msg_any_t msg;
  uint32_t ulClientToken=0;
  uint32_t payloadSize = 0;
  elite_svc_handle_t *pDynSvcHandle;
  elite_msg_custom_stop_t *stop_payload_ptr;

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM STOP CMD Begin");

  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);

  if ( pSession->session_state != LISTEN_STREAM_MGR_SESSION_ACTIVE )
  {

    /* do nothing if already in INIT state and still ack succeccs. */
    /* For any other state, ACK NOTREADY */
    if (pSession->session_state == LISTEN_STREAM_MGR_SESSION_INIT )
    {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM STOP CMD: Already in INIT  State");
      result = ADSP_EOK;
      goto __bailout;
    }
    else /* deinit state */
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM STOP CMD: Bad State State::0x%x",pSession->session_state);
      result = ADSP_ENOTREADY;
      goto __bailout;
    }
    return result;
  }

  payloadSize = sizeof(elite_msg_custom_stop_t);
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg,&payloadSize ,
                                                   ELITE_CUSTOM_MSG,
                                                   pSession->session_resp_q_ptr,
                                                   ulClientToken,
                                                   0 ) ) )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenStreamMgr [Session ID = %u, ]: Failed to create Elite message",
          pSession->ucInternalSessionId);

    /* ack with error */
    return lsm_generate_ack( pPkt, ADSP_ENOMEMORY, NULL, 0, 0 );
  }

  stop_payload_ptr =      (elite_msg_custom_stop_t *)msg.pPayload;

  stop_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_LISTEN_STOP;

  /* get dynamic command queue and push the msg into it */
  pDynSvcHandle = pSession->dyn_svc.handle;

  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
    elite_msg_release_msg(&msg);
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM STOP CMD Failed to send ELITE_CMD_STOP to dynamic svc");
    result = ADSP_EFAILED;
    goto __bailout;
  }

  /* updated expected response mask */
  pSession->required_resp++;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);

  return ADSP_EOK;

  __bailout:
  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;

}

/*
   This function processes  LSM_SESSION_CMD_DEREGISTER_SOUND_MODEL command that is sent to a given session.


   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_deregister_sound_model_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  elite_msg_any_t msg;
  uint32_t ulClientToken=0;
  uint32_t payloadSize = 0;
  elite_svc_handle_t *pDynSvcHandle;
  elite_msg_custom_deregister_sound_model_t *dereg_snd_model_payload_ptr;

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM Dereg snd model Begin");

  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);

  if ( pSession->session_state != LISTEN_STREAM_MGR_SESSION_INIT )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Accept this only in Init state,curr state:0x%x",pSession->session_state);
    result = ADSP_ENOTREADY;
    goto __bailout;
  }

  payloadSize = sizeof(elite_msg_custom_deregister_sound_model_t);
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg,&payloadSize ,
                                                   ELITE_CUSTOM_MSG,
                                                   pSession->session_resp_q_ptr,
                                                   ulClientToken,
                                                   0 ) ) )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM [Session ID = %u, ]: Failed to create Elite message",
          pSession->ucInternalSessionId);
    result = ADSP_ENOMEMORY;
    goto __bailout;
  }

  dereg_snd_model_payload_ptr =      (elite_msg_custom_deregister_sound_model_t *)msg.pPayload;

  dereg_snd_model_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_LISTEN_DEREGISTER_SOUND_MODEL;

  /* get dynamic command queue and push the msg into it */
  pDynSvcHandle = pSession->dyn_svc.handle;

  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
    elite_msg_release_msg(&msg);
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to send ELITEMSG_CUSTOM_LISTEN_DEREGISTER_SOUND_MODEL to dynamic svc");
    result = ADSP_EFAILED;
    goto __bailout;
  }

  /* updated expected response mask */
  pSession->required_resp++;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);
  return ADSP_EOK;

  __bailout:
  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;
}

/*
   This function processes  LSM_SESSION_CMD_REGISTER_SOUND_MODEL command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_register_sound_model_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  elite_msg_any_t msg;
  uint32_t ulClientToken=0;
  uint32_t payloadSize = 0;
  elite_svc_handle_t *pDynSvcHandle;
  elite_msg_custom_register_sound_model_t *reg_snd_model_payload_ptr;

  lsm_session_cmd_register_sound_model_t *pPayload;

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM Register Snd model Begin");

  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);
  pPayload = (lsm_session_cmd_register_sound_model_t *)elite_apr_if_get_payload_ptr(pPkt);

  if ( pSession->session_state != LISTEN_STREAM_MGR_SESSION_INIT )
  {

    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Accept this only in Init state,curr state:0x%x",pSession->session_state);
    result = ADSP_ENOTREADY;
    goto __bailout;
  }

  payloadSize = sizeof(elite_msg_custom_register_sound_model_t);
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg,&payloadSize ,
                                                   ELITE_CUSTOM_MSG,
                                                   pSession->session_resp_q_ptr,
                                                   ulClientToken,
                                                   0 ) ) )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM [Session ID = %u, ]: Failed to create Elite message",
          pSession->ucInternalSessionId);
    result = ADSP_ENOMEMORY;
    goto __bailout;
  }

  reg_snd_model_payload_ptr =      (elite_msg_custom_register_sound_model_t *)msg.pPayload;

  reg_snd_model_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_LISTEN_REGISTER_SOUND_MODEL;
  reg_snd_model_payload_ptr->model_size  = pPayload->model_size;
  reg_snd_model_payload_ptr->model_addr_lsw  = pPayload->model_addr_lsw;
  reg_snd_model_payload_ptr->model_addr_msw  =  pPayload->model_addr_msw;
  reg_snd_model_payload_ptr->mem_map_handle  = pPayload->mem_map_handle;


  /* get dynamic command queue and push the msg into it */
  pDynSvcHandle = pSession->dyn_svc.handle;

  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
    elite_msg_release_msg(&msg);
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to send ELITEMSG_CUSTOM_LISTEN_REGISTER_SOUND_MODEL to dynamic svc");
    result = ADSP_EFAILED;
    goto __bailout;
  }

  /* updated expected response mask */
  pSession->required_resp++;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);
  return ADSP_EOK;

  __bailout:

  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;

}

/*
   This function processes  LSM_SESSION_CMD_SET_PARAM command that is sent to a given session.


   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_set_param_handler(lsm_t* pMe,
                                               lsm_session_state_type_t *pSession)

{
  ADSPResult result=ADSP_EOK;
  elite_apr_packet_t *pPkt  = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);
  elite_apr_port_t    port = elite_apr_if_get_dst_port( pPkt );
  uint8_t session_id   = LSM_GET_SESSION_ID( port );
  uint32_t ulClientToken = 0;
  elite_svc_handle_t *pDynSvcHandle;
  elite_msg_param_cal_t *pMsgPayload;
  uint32_t unPayloadSize;
  elite_msg_any_t msg;
  int32_t *paramBase = NULL;

  MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"LSM [Session ID = %u, ]: enter SetParamsCmdHandler",  session_id);

  if (LISTEN_STREAM_MGR_SESSION_DEINIT == pSession->session_state)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [Session ID = %u, ]: Cannot set  params on a closed stream", pSession->session_state);
    result = ADSP_EUNEXPECTED;
    goto __bailout;
  }

  /** lsm_session_cmd_set_params_t */
  lsm_session_cmd_set_params_t *pParamPayload;
  pParamPayload = (lsm_session_cmd_set_params_t *)elite_apr_if_get_payload_ptr(pPkt);

  if (0 == pParamPayload->mem_map_handle) /* in-band */
  {
    /* Address just beyond the Set params header lsm_session_cmd_set_params_t points to lsm_session_param_data_t */
    paramBase  = (int32_t *)(pParamPayload + 1);
  }
  else /* out-of-band */
  {
    if (!check_cache_line_alignment(pParamPayload->data_payload_addr_lsw))
    {
      result = ADSP_EBADPARAM;
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u]: Physical memory address is not aligned to cache line, in set param.",session_id);
      return lsm_generate_ack(pPkt, result, NULL, 0, 0 );
    }
    else
    {
      elite_mem_shared_memory_map_t bufferMemNode;
      bufferMemNode.unMemMapClient     = lsm_memory_map_client;
      bufferMemNode.unMemMapHandle     = pParamPayload->mem_map_handle;
      bufferMemNode.unMemSize          = pParamPayload->data_payload_size;
      bufferMemNode.unPhysAddrLsw      = pParamPayload->data_payload_addr_lsw;
      bufferMemNode.unPhysAddrMsw      = pParamPayload->data_payload_addr_msw;
      bufferMemNode.unVirtAddr         = 0;

      result =  elite_mem_map_get_shm_attrib(bufferMemNode.unPhysAddrLsw, bufferMemNode.unPhysAddrMsw,
                                             bufferMemNode.unMemSize, &bufferMemNode);
      if (ADSP_FAILED(result))
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u,]:Failed to map physical memory for SetParam.",  session_id);
        goto __bailout;
      }

      /* Since this buffer will be read, need to invalidate the cache. */
      result = elite_mem_invalidate_cache(&bufferMemNode);
      if (ADSP_FAILED(result))
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u]: Failed to invalidate cache for SetParam.",session_id);

        goto __bailout;
      }
      paramBase  = (int32_t*)(bufferMemNode.unVirtAddr);
    }
  }

  /* Pre-Scan for non-zero reserved field in LSM_SESSION_CMD_SET_PARAMS (v1)
   *
   * Note: Knowledge of V1 vs V2 is contained in Static service. Dynamic service
   * directly works on the payload as both V1 and V2 are binary compatible. It
   * is specified that V1 should have reserved field zero, and if it wasn't,
   * then dynamic service could run into an error. This scan in static could
   * prevent crash in dynamic service.
   */
  if(LSM_SESSION_CMD_SET_PARAMS == elite_apr_if_get_opcode(pPkt))
  {
    lsm_session_param_data_t* set_param_v1_iter =
                                         (lsm_session_param_data_t*)(paramBase);

    /* Reserved field Zero Check for Set_Param V1 */
    while((int8_t*)set_param_v1_iter <
          (int8_t*)paramBase + pParamPayload->data_payload_size)
    {
      if(set_param_v1_iter->reserved != 0)
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
              "Non-zero reserved filed in LSM_SESSION_CMD_SET_PARAMS %x",
              set_param_v1_iter->reserved );
        goto __bailout;
      }
      set_param_v1_iter = (lsm_session_param_data_t*)(
                          (int8_t*)set_param_v1_iter +
                          sizeof(lsm_session_param_data_t) +
                          set_param_v1_iter->param_size);
    }
  }

  unPayloadSize = sizeof( elite_msg_param_cal_t );
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg, &unPayloadSize,
                                                   ELITE_CMD_SET_PARAM,
                                                   pSession->session_resp_q_ptr,
                                                   ulClientToken,
                                                   0 ) ) )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM [Session ID = %u]: Failed to create message",session_id );
    goto __bailout;
  }

  pMsgPayload = (elite_msg_param_cal_t*) msg.pPayload;
  pMsgPayload->unParamId  = ELITEMSG_PARAM_ID_CAL;
  pMsgPayload->pnParamData   = paramBase;
  pMsgPayload->unSize        = pParamPayload->data_payload_size;

  /* get dynamic command queue and push the msg into it */
  pDynSvcHandle = pSession->dyn_svc.handle;
  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);


  if (ADSP_FAILED(result))
  {
    /* return message to buffer queue */
    elite_msg_release_msg(&msg);
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM[%u]: FAILED to send set param Configuration parameters !!\n",
          session_id);
    goto __bailout;
  }

  /* updated expected response mask */
  pSession->required_resp++;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);
  return ADSP_EOK;

  __bailout:
  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;

}


/*
   This function processes  LSM_SESSION_CMD_GET_PARAM command that is sent to a given session.


   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_get_param_handler(lsm_t* pMe,
                                               lsm_session_state_type_t *session_ptr)

{
  ADSPResult result=ADSP_EOK;
  elite_apr_packet_t *packet_ptr  = (elite_apr_packet_t *) ((session_ptr->processed_msg).pPayload);
  elite_apr_packet_t *ack_packet_ptr = NULL;
  elite_apr_port_t    port = elite_apr_if_get_dst_port( packet_ptr );
  uint8_t session_id   = LSM_GET_SESSION_ID( port );
  uint32_t client_token = 0;
  elite_svc_handle_t *dyn_svc_handle_ptr;
  elite_msg_param_cal_t *msg_payload_ptr = NULL;
  uint32_t payload_size;
  elite_msg_any_t msg;
  lsm_session_cmd_get_params_v2_t *param_payload_ptr;
  lsm_session_param_data_v2_t *param_header_ptr;

  MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"LSM [Session ID = %u, ]: enter GET_PARAM command handler",  session_id);

  if (LISTEN_STREAM_MGR_SESSION_DEINIT == session_ptr->session_state)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [Session ID = %u, ]: Cannot get  params on a closed stream", session_ptr->session_state);
    result = ADSP_EUNEXPECTED;
    goto __bailout;
  }

  param_payload_ptr = (lsm_session_cmd_get_params_v2_t *)elite_apr_if_get_payload_ptr(packet_ptr);

  if(0 == param_payload_ptr->param_max_size)
  {
	  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u]: param_max_size for get_param cannot be 0.",session_id);
	  goto __bailout;
  }

  payload_size = sizeof(elite_msg_param_cal_t);
  if(ADSP_FAILED(result = elite_msg_create_msg( &msg, &payload_size,
		  ELITE_CMD_GET_PARAM,
		  session_ptr->session_resp_q_ptr,
		  client_token,
		  0 ) ) )
  {
	  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM [Session ID = %u]: Failed to create message",session_id );
	  goto __bailout;
  }

  msg_payload_ptr = (elite_msg_param_cal_t*) msg.pPayload;
  msg_payload_ptr->unParamId  = ELITEMSG_PARAM_ID_CAL;
  msg_payload_ptr->unSize = param_payload_ptr->param_max_size;

  /* out-of-band case*/
  if(0 != param_payload_ptr->mem_map_handle)
  {
	  if(!check_cache_line_alignment(param_payload_ptr->data_payload_addr_lsw))
	  {
		  result = ADSP_EBADPARAM;
		  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u]: Physical memory address is not aligned to cache line, in set param.",session_id);
		  goto __bailout;
	  }
	  else
	  {
		  elite_mem_shared_memory_map_t bufferMemNode;
		  bufferMemNode.unMemMapClient     = lsm_memory_map_client;
		  bufferMemNode.unMemMapHandle     = param_payload_ptr->mem_map_handle;
		  bufferMemNode.unMemSize          = param_payload_ptr->param_max_size;
		  bufferMemNode.unPhysAddrLsw      = param_payload_ptr->data_payload_addr_lsw;
		  bufferMemNode.unPhysAddrMsw      = param_payload_ptr->data_payload_addr_msw;
		  bufferMemNode.unVirtAddr         = 0;

		  result =  elite_mem_map_get_shm_attrib(bufferMemNode.unPhysAddrLsw, bufferMemNode.unPhysAddrMsw,
				  bufferMemNode.unMemSize, &bufferMemNode);
		  if (ADSP_FAILED(result))
		  {
			  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u,]:Failed to map physical memory for GetParam.",  session_id);
			  goto __bailout;
		  }

		  msg_payload_ptr->pnParamData = (int32_t *)bufferMemNode.unVirtAddr;
	  }
  }
  else /*in-band case*/
  {
	  /*static service needs to allocate large enough packet for the response*/
	  uint32_t ack_payload_size = sizeof(lsm_session_cmdrsp_get_params_v2_t) + param_payload_ptr->param_max_size;
	  int32_t* apr_packet_base_ptr;
	  uint32_t opcode = LSM_SESSION_CMDRSP_GET_PARAMS_V2;

	  if(elite_apr_if_get_opcode(packet_ptr) == LSM_SESSION_CMD_GET_PARAMS)
	  {
	    /* Backward compatibility */
	    opcode = LSM_SESSION_CMDRSP_GET_PARAMS;
	  }

	  /*Allocate the APR packet*/
	  result = elite_apr_if_alloc_cmd_rsp(lsm_get_apr_handle(),
	              elite_apr_if_get_dst_addr(packet_ptr),
	              elite_apr_if_get_dst_port(packet_ptr),
	              elite_apr_if_get_src_addr(packet_ptr),
	              elite_apr_if_get_src_port(packet_ptr),
	              elite_apr_if_get_client_token(packet_ptr),
	              opcode,
	              ack_payload_size,
	              &ack_packet_ptr);

	  if(NULL == ack_packet_ptr)
	  {
		  result = ADSP_EFAILED;
	  }

	  if(ADSP_FAILED(result))
	  {
		  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM [%u,]:Failed to allocate response packet for GetParam with result %l",
				  session_id, result);
		  goto __bailout;
	  }

	  /*assign the current message to the allocate response packet*/
	  session_ptr->processed_msg.pPayload = ack_packet_ptr;
	  apr_packet_base_ptr = (int32_t *)elite_apr_if_get_payload_ptr(ack_packet_ptr);
	  /*assign the param data ptr of the internal message to the correct offset in APR packet*/
	  msg_payload_ptr->pnParamData = apr_packet_base_ptr + 1; /*Offset by the status to access the module ID.*/
  }

  /*initialize the response payload to 0 for consistency*/
  memset(msg_payload_ptr->pnParamData, 0, param_payload_ptr->param_max_size);

  /*populate the param data header*/
  param_header_ptr = (lsm_session_param_data_v2_t *)msg_payload_ptr->pnParamData;
  param_header_ptr->module_id = param_payload_ptr->module_id;
  param_header_ptr->param_id = param_payload_ptr->param_id;
  /*initalize this to the max size for the actual payload*/
  param_header_ptr->param_size = param_payload_ptr->param_max_size - sizeof(lsm_session_param_data_v2_t);

  /* get dynamic command queue and push the msg into it */
  dyn_svc_handle_ptr = session_ptr->dyn_svc.handle;
  result = qurt_elite_queue_push_back(dyn_svc_handle_ptr->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
    /* return message to buffer queue */
    elite_msg_release_msg(&msg);
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM[%u]: FAILED to push get param message to dynamic service \n",
          session_id);
    goto __bailout;
  }

  /* updated expected response mask */
  session_ptr->required_resp++;
  session_ptr->overall_result = 0;
  /* now wait for the responses to come back...listen only to reponse queue */
  pMe->curr_bit_field |= session_ptr->session_resp_mask;
  pMe->curr_bit_field &= ~(session_ptr->session_cmd_mask);

  /*Free up the incoming APR packet for in-band since we have created another for the response*/
  if(0 == param_payload_ptr->mem_map_handle)
  {
      elite_apr_if_free(lsm_get_apr_handle(), packet_ptr);
  }

  return ADSP_EOK;

__bailout:
  lsm_generate_ack(packet_ptr, result, 0, 0, 0);
  return result;

}

/*
   This function processes  LSM_SESSION_CMD_EOB command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_eob_handler(lsm_t *pMe,lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  elite_msg_any_t msg;
  uint32_t ulClientToken=0;
  uint32_t payloadSize = 0;
  elite_svc_handle_t *pDynSvcHandle;
  elite_msg_custom_eob_t *eob_payload_ptr;

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM EOB CMD Begin");

  elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);

  if ( pSession->session_state != LISTEN_STREAM_MGR_SESSION_ACTIVE )
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM[%u] EOB CMD in expected only in Active session"
          ,pSession->ucInternalSessionId);
     result = ADSP_EUNEXPECTED;
     goto __bailout;

  }

  payloadSize = sizeof(elite_msg_custom_eob_t);
  if ( ADSP_FAILED( result = elite_msg_create_msg( &msg,&payloadSize ,
                                               ELITE_CUSTOM_MSG,
                                               pSession->session_resp_q_ptr,
                                               ulClientToken,
                                               0 ) ) )
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "LSM [%u]: Failed to create Elite message",
        pSession->ucInternalSessionId);

     result = ADSP_ENOMEMORY;
     goto __bailout;
  }

  eob_payload_ptr = (elite_msg_custom_eob_t *)msg.pPayload;

  eob_payload_ptr->unSecOpCode = ELITEMSG_CUSTOM_LISTEN_EOB;

  /*get dynamic command queue and push the msg into it*/
  pDynSvcHandle = pSession->dyn_svc.handle;

  result = qurt_elite_queue_push_back(pDynSvcHandle->cmdQ, (uint64_t*)&msg);

  if (ADSP_FAILED(result))
  {
     elite_msg_release_msg(&msg);
     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM EOB CMD Failed to send ELITE_CMD_EOB to dynamic svc");
     result = ADSP_EFAILED;
     goto __bailout;
  }

  /* updated expected response mask*/
  pSession->required_resp++;
  pSession->overall_result = 0;
  /* now wait for the responses to come back...listen only to response queue*/
  pMe->curr_bit_field |= pSession->session_resp_mask;
  pMe->curr_bit_field &= ~(pSession->session_cmd_mask);

  return ADSP_EOK;

__bailout:
  lsm_generate_ack(pPkt, result, 0, 0, 0);
  return result;
}

/*
   Static function processes to initailize the session state

   @param pSession[in/out] This points to the instance of LSM session State
   @return Success or Failure
*/
static void lsm_init_session_state(lsm_session_state_type_t *pSession)
{
  /* if the session is closed, init all svc state structs */
  if (LISTEN_STREAM_MGR_SESSION_DEINIT == pSession->session_state)
  {
    pSession->required_resp = 0;
    pSession->overall_result =0;
  }
}

