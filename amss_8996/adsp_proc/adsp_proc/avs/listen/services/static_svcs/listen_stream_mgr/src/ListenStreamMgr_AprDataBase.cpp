
/*========================================================================
Elite

This file defines functions, variables for managing APR routing
table.

Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.

*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_AprDataBase.cpp#1 $$DateTime: 2016/06/05 22:53:09 $$Author: pwbldsvc $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/18/2012  Sudhir      Initial version
========================================================================== */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "ListenStreamMgr_AprDataBase.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

// look up table for listen router
static struct {
    lsm_node_t          apr_router_nodes[LSM_MAX_SESSION_ID + 1 ];
    /**< at IPC level, Session ID 1- LSM_MAX_SESSION_ID .
         For easy of implementation, add empty space ahead.
         */
     qurt_elite_mutex_t  apr_router_lock; 
     /**< Mutex for synchronisation b/w APR callback function and dynamic threads*/
} lsm_apr_router_database;

// Store the system command queue.
static qurt_elite_queue_t* lsm_system_cmd_q;


/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*
   This function is called after ListenStreamMgr is created. It fills in the
   command queue of different sessions.
   Given that the client is generating handle, we already know the command queue
   of each session in creation time. Hence, fill it here.
*/
ADSPResult lsm_apr_router_init(lsm_t *pInst)
{
    // Init LUT
    memset( lsm_apr_router_database.apr_router_nodes, 0, sizeof(lsm_apr_router_database.apr_router_nodes) );
    qurt_elite_mutex_init(&lsm_apr_router_database.apr_router_lock);
    for ( uint32_t i = 0; i < LSM_MAX_SESSION_ID; i ++ ) 
    {
       if (ADSP_FAILED(lsm_apr_router_register_cmd_q(i+1, pInst->aSessions[i].session_cmd_q_ptr)))
       {
          MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"LSM: Error in router init %lu", i);
          return ADSP_ENOTREADY;
       }
    }
	//Register system command queue seperately.
	lsm_system_cmd_q = pInst->serviceHandle.cmdQ;
    return ADSP_EOK;
}

/*
This function will register the output buffer queue of dynamic session
*/
ADSPResult lsm_apr_router_register_buf_q( elite_apr_port_t port, qurt_elite_queue_t *pBufQ )
{

   uint8_t ssn_id = LSM_GET_SESSION_ID( port);

    if ((ssn_id == 0) || (ssn_id > LSM_MAX_SESSION_ID))
    {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM:Session Out of Range:[%u]", ssn_id);
       return ADSP_EBADPARAM;
    }
    qurt_elite_mutex_lock(&lsm_apr_router_database.apr_router_lock);
    lsm_node_t *pNode = &(lsm_apr_router_database.apr_router_nodes[ssn_id]);
    pNode->bufQ = pBufQ;
    qurt_elite_mutex_unlock(&lsm_apr_router_database.apr_router_lock);
    return ADSP_EOK;
}

/*
This function will deregister the output buffer queue of dynamic session
*/
ADSPResult lsm_apr_router_deregister_buf_q( elite_apr_port_t port)
{

    uint8_t ssn_id = LSM_GET_SESSION_ID( port);

    if ((ssn_id == 0) || (ssn_id > LSM_MAX_SESSION_ID))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM:Session Out of Range:[%u]", ssn_id);
       return ADSP_EBADPARAM;
    }
    qurt_elite_mutex_lock(&lsm_apr_router_database.apr_router_lock);
    lsm_node_t *pNode = &(lsm_apr_router_database.apr_router_nodes[ssn_id]);
    pNode->bufQ = NULL;
    qurt_elite_mutex_unlock(&lsm_apr_router_database.apr_router_lock);

    return ADSP_EOK;
}

/*
   This is the function to register the command queue with the APR router.
*/
ADSPResult lsm_apr_router_register_cmd_q( uint8_t major, qurt_elite_queue_t *pCmdQ )
{
    lsm_node_t *pNode;

    if ((major > LSM_MAX_SESSION_ID) || (NULL == pCmdQ)) 
    {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
         "ListenStreamMgr_AprRouterRegister::BadParam: address[%u], cmdQ=0x%lx", major, (uint32_t) pCmdQ);
        return ADSP_EBADPARAM;
    }
    pNode = &(lsm_apr_router_database.apr_router_nodes[ major ]);
    pNode->cmdQ = pCmdQ;

    return ADSP_EOK;
}



/*
   This utility function get the command queue of corresponding command queue
*/
ADSPResult  lsm_apr_router_get_cmd_q(elite_apr_port_t port, qurt_elite_queue_t **pCmdQ)
{
   uint8_t ssn_id = LSM_GET_SESSION_ID( port);

   if (  (ssn_id == 0) || (ssn_id > LSM_MAX_SESSION_ID) ) {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "ListenStreamMgr_AprRouterRegister:Address Out of Range:[%u]", ssn_id);
       return ADSP_EBADPARAM;
   }

   lsm_node_t *pNode = &(lsm_apr_router_database.apr_router_nodes[ssn_id]);

   *pCmdQ = pNode->cmdQ;

   if ( NULL == (*pCmdQ) ) 
   {
     return ADSP_EBADPARAM;
   }
   return ADSP_EOK;

}

/*
   This utility function get the output buffer queue of corresponding session
*/
 ADSPResult  lsm_apr_router_get_buf_q( elite_apr_port_t port,
                                       qurt_elite_queue_t **pBufQ)
{

  uint8_t ssn_id = LSM_GET_SESSION_ID( port);

   if (  (ssn_id == 0) || (ssn_id > LSM_MAX_SESSION_ID) ) 
   {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM:Session Out of Range:[%u]", ssn_id);
       return ADSP_EBADPARAM;
   }
   
   qurt_elite_mutex_lock(&lsm_apr_router_database.apr_router_lock);
   lsm_node_t *pNode = &(lsm_apr_router_database.apr_router_nodes[ssn_id]);

   *pBufQ = pNode->bufQ;

   if ( NULL == (*pBufQ) )
   {
     qurt_elite_mutex_unlock(&lsm_apr_router_database.apr_router_lock);
     MSG(MSG_SSID_QDSP6,DBG_ERROR_PRIO,"lsm_apr_router_get_data_q ENOTREADY");
     return ADSP_ENOTREADY;
   }
   qurt_elite_mutex_unlock(&lsm_apr_router_database.apr_router_lock);
   return ADSP_EOK;

}

/*
   This utility function get the system command queue
*/
ADSPResult lsm_apr_router_get_system_cmd_q(qurt_elite_queue_t **pCmdQ)
{
	*pCmdQ = lsm_system_cmd_q;
	return ADSP_EOK;
}


