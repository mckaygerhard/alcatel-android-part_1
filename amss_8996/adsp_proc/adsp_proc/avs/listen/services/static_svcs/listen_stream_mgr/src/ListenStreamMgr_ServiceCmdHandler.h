/**
@file ListenStreamMgr_ServiceCmdHandler.h
@brief This file declares service cmd handler functions for ListenStreamMgr.

*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_ServiceCmdHandler.h#1 $

when        who       what, where, why
--------    ---       -----------------------------------------------------
07/14/2014  Unni      Initial version
==========================================================================*/
/*-----------------------------------------------------------------------
   Copyright (c) 2012-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
   Qualcomm Proprietary and Confidential.
-----------------------------------------------------------------------*/

#ifndef LISTEN_STREAM_MGR_SERVICE_CMD_HANDLER_H
#define LISTEN_STREAM_MGR_SERVICE_CMD_HANDLER_H

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

/* System */
#include "qurt_elite.h"
#include "Elite.h"

/* Listen */
#include "ListenStreamMgr_Type.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations
 *--------------------------------------------------------------------------*/
												
/*
   This function process the client command that are send to LSM service.

   @param pMe[in/out] This points to the instance of ListenStreamMgr

   @return Success or Failure
*/
ADSPResult lsm_service_cmd_q_apr_pkt_handler(lsm_t *me_ptr);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* #ifndef LISTEN_STREAM_MGR_SERVICE_CMD_HANDLER_H */

