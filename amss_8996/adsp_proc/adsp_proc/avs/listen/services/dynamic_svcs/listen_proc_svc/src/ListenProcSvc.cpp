/**
  @file ListenProcSvc.cpp
  @brief This file contains Listen processing service implementation. These
  include Session Variable management, Data/Command Queue Handling, etc.

  Object Oriented Concept is applied here, as in each Session can be considered
  as an Object, Session variables as Object Member Variables and Function
  implemented here as Object Member Functions.
*/

/*==============================================================================
  Copyright (c) 2012-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*==============================================================================

  $Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/dynamic_svcs/listen_proc_svc/src/ListenProcSvc.cpp#1 $

  Edit History

  when       who      what, where, why
  --------   ---      -------------------------------------------------------
  06/23/14   Unni     Added LSM topology support
  11/26/12   Sudhir   Initial version
==============================================================================*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "ListenProcSvc_Includes.h"
#include "AFEInterface.h"
#include "EliteMsg_AfeCustom.h"
#include "ListenProcSvc_CustMsg.h"
#include "adsp_media_fmt.h"
#include "EliteTopology_db_if.h"
#include "ListenProcSvc_mmpm.h"
#include "EliteCmnTopology_db_if.h"
#include "adsp_core_api.h"
#include "Elite_fwk_extns_detection_engine.h"

/*------------------------------------------------------------------------------
 * Constant / Define Declarations
 *----------------------------------------------------------------------------*/
static char THREAD_NAME[]="LPro";
#define LISTEN_DYNA_SVC_PRIO  ELITETHREAD_DYNA_ENC_SVC_PRIO
#define LISTEN_INPUT_AFE_INTERNAL_FRAME_PER_SEC 100 /* Limitation From SMAD */
#define SIZE_OF_ARRAY(a) (sizeof(a)/sizeof((a)[0]))

/*------------------------------------------------------------------------------
 * Local Function Declaration
 *----------------------------------------------------------------------------*/
ADSPResult listen_proc_svc_custom_msg_handler(listen_proc_svc_t* me_ptr);
static int listen_proc_svc_workloop(void* pInstance);
static int listen_proc_svc_destroy_yourself(listen_proc_svc_t* me_ptr);
static int listen_proc_svc_return_unsupported(listen_proc_svc_t* me_ptr);
static void listen_proc_svc_destroy_input_data_q(listen_proc_svc_t* me_ptr);
static void listen_proc_svc_destroy_output_buf_q(listen_proc_svc_t* me_ptr);
static int listen_proc_svc_process_cmd_q(listen_proc_svc_t* me_ptr);
static int listen_proc_svc_process_input_data_q(listen_proc_svc_t* me_ptr);
static void listen_proc_svc_destroy_response_q(listen_proc_svc_t* me_ptr);

/*------------------------------------------------------------------------------
 * Static variables
 *----------------------------------------------------------------------------*/
typedef int (*qurt_elite_queue_handler)(listen_proc_svc_t* me_ptr);

/**< @brief Queue handlers table.*/
static qurt_elite_queue_handler pProcQHandler[LISTEN_PROC_NUM_Q]=
{
    listen_proc_svc_process_cmd_q,
    listen_proc_svc_process_input_data_q,
};

/*------------------------------------------------------------------------------
 * Function Definitions
 *----------------------------------------------------------------------------*/
/**
  Creates an instance of the  Listen Processing service.

  @param [in] init_param_ptr: Pointer to listen_proc_svc_init_params_t
  @param [out] handle_ptr_ptr: Service entry handle returned to the caller/LSM.

  @return
  Success or failure of the instance creation.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_create(const listen_proc_svc_init_params_t* init_param_ptr,
                                  void **handle)
{
  ADSPResult result = ADSP_EOK;
  const lsm_topology_definition_t *lsm_topo_def_ptr = NULL;
  char threadname[10];
  uint32_t stack_size;
  elite_cmn_topo_db_entry_t *topology_ptr = NULL;
  uint32_t actual_size = 0;
  uint32_t topo_handle = ELITE_CMN_TOPO_DB_INVALID_HANDLE;


  /* Sanity checks of input params */
  if (NULL == handle)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Invalid input params!");
    return ADSP_EBADPARAM;
  }

  /* allocate instance struct */
  listen_proc_svc_t *me_ptr =
      (listen_proc_svc_t*) qurt_elite_memory_malloc(sizeof(listen_proc_svc_t),
                                                    QURT_ELITE_HEAP_DEFAULT);
  if (!me_ptr)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "Failed to allocate memory for listen processing service instance!");
    *handle = NULL;
    return ADSP_ENOMEMORY;
  }

  /* zero out all the fields. */
  memset(me_ptr, 0, sizeof(listen_proc_svc_t));

  result = listen_proc_svc_create_svc_q(me_ptr);
  if(ADSP_FAILED(result))
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "Failed to create service Queues!!");
    *handle = NULL;
    goto __bail_out;
  }


  /* Make a copy of LSM call back structure */
  memscpy( &me_ptr->callback_data, sizeof(lsm_callback_handle_t),
           init_param_ptr->callback_data_ptr, sizeof(lsm_callback_handle_t));

  /* Fill the media  format and other initialization */
  me_ptr->sample_rate = init_param_ptr->sampling_rate;
  me_ptr->bits_per_sample = init_param_ptr->bits_per_sample;
  me_ptr->num_channels = init_param_ptr->num_channels;
  me_ptr->afe_port_id = AFE_PORT_ID_INVALID;
  me_ptr->state = LISTEN_PROC_STATE_STOP;
  me_ptr->revision = init_param_ptr->revision;
  me_ptr->session_id = init_param_ptr->session_id;
  me_ptr->detection_status = LSM_DETECTION_STATUS_NONE;

  /* Register unique MMPM request per session */
  result = lsm_mmpm_register(&me_ptr->mmpm_info, me_ptr->session_id);
  if(ADSP_FAILED(result))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenProc[%d] failed to resgister to mmpm",
          me_ptr->session_id);
    *handle = NULL;
    goto __bail_out;
  }


  // Search the topology definition in common data base, if not, search in old data base
  result = elite_cmn_topo_db_get_topology(1 << AVCS_TOPO_CFG_AREA_LSM_BIT,
                                          init_param_ptr->topology_id, &topology_ptr, &actual_size, &topo_handle);

  if(ADSP_FAILED(result) || 0 == actual_size || 0 == topo_handle ||
      ELITE_CMN_TOPO_DB_INVALID_HANDLE == topo_handle || NULL == topology_ptr)
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ListenProc[%d]: Topology %lx is not present in "
          "common database, searching in old database", me_ptr->session_id, init_param_ptr->topology_id);

    /* Get topology information based on id */
    result = topo_db_get_topology(TOPO_DB_CLIENT_TYPE_LSM,
                                  init_param_ptr->topology_id,
                                  (const audproc_topology_definition_t**)&lsm_topo_def_ptr);
    if(ADSP_FAILED(result) || NULL == lsm_topo_def_ptr)
    {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] failed to get topology definition for topology id %lx, status[%x]!!",
            me_ptr->session_id,init_param_ptr->topology_id, result);
      goto __bail_out;
    }

    /* Create topology */
    result = topo_create(&me_ptr->topo_handle,
                         (topology_definition_t*)lsm_topo_def_ptr,
                         lsm_capi_v2_event_callback,
                         me_ptr,
                         NULL);
    if(ADSP_FAILED(result))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] failed to create topology for topology id %lx!!",
            me_ptr->session_id,init_param_ptr->topology_id);
      *handle = NULL;
      goto __bail_out;
    }
  }
  else
  {
    //Cross check whether actual_size returned from common database is valid
    uint32_t num_modules = topology_ptr->topo_def.num_modules;
    uint32_t size_req = (sizeof(elite_cmn_topo_db_entry_t) - sizeof(avcs_module_info_t)) + (num_modules  *
        sizeof(avcs_module_info_t));
    if(size_req != actual_size)
    {
      MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ListenProc[%d]: Topology %lx is present in common database."
          " However, actual_size %lu does not match size_req %lu",
            me_ptr->session_id, init_param_ptr->topology_id, actual_size, size_req);

      result = ADSP_EBADPARAM;
      elite_cmn_topo_db_release_topology(topo_handle);
      goto __bail_out;
    }

    // create the topology
    if(ADSP_EOK != (result = topo_create_common_db(&me_ptr->topo_handle, topology_ptr, lsm_capi_v2_event_callback,
                                                   me_ptr, NULL)))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create the topology for topology id %ld :%d",
            init_param_ptr->topology_id, result);
      elite_cmn_topo_db_release_topology(topo_handle);
      goto __bail_out;
    }

    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ListenProc[%d]: topology creation for Topology id %lX is done successfully ", 
          me_ptr->session_id, init_param_ptr->topology_id);

    elite_cmn_topo_db_release_topology(topo_handle);
  }

  /* Statically defining input frame requirement, expecting all modules
     to support this frame size */
  me_ptr->input_frame_size = me_ptr->sample_rate /
                             LISTEN_INPUT_AFE_INTERNAL_FRAME_PER_SEC;

  snprintf(threadname, 10,"%s%x",THREAD_NAME,me_ptr->session_id);

  stack_size = ((topo_t*)me_ptr->topo_handle)->stack_size;

  /* Launch the thread */
  result = qurt_elite_thread_launch(&me_ptr->svc_handle.threadId,
                                    threadname,
                                    NULL,
                                    stack_size,
                                    LISTEN_DYNA_SVC_PRIO,
                                    listen_proc_svc_workloop,
                                    (void*)me_ptr,
                                    QURT_ELITE_HEAP_DEFAULT);
  if(ADSP_FAILED(result))
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenProc[%d] failed to launch service thread!! Stack size = %lu",
          me_ptr->session_id, stack_size);
    *handle = NULL;
    goto __bail_out;
  }

  *handle = &(me_ptr->svc_handle);


  MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ListenProc[%d] creation success!! "
        "Frame size = %lu, Stack size = %lu",
        me_ptr->session_id,
        me_ptr->input_frame_size,
        stack_size);
  return ADSP_EOK;

__bail_out:

  listen_proc_svc_destroy_svc(me_ptr);
  return result;


}

/** Destroy the dynamic service

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  None

  @dependencies
  None.
*/
void listen_proc_svc_destroy_svc(listen_proc_svc_t* me_ptr)
{
  if(!me_ptr)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Bad Param" );
    return;
  }

  /* Deregister from mmpm */
  lsm_mmpm_deregister(&me_ptr->mmpm_info);

  /* Destroy topology */
  topo_destroy((topo_handle_t)me_ptr->topo_handle, NULL);
  me_ptr->topo_handle = NULL;

  /* call utility function to destroy data Q */
  listen_proc_svc_destroy_input_data_q(me_ptr);

  /* call utility function to destroy output buffer Q */
  listen_proc_svc_destroy_output_buf_q(me_ptr);

  /* call utility function to destroy cmd Q */
  elite_svc_destroy_cmd_queue(me_ptr->svc_handle.cmdQ);

  listen_proc_svc_destroy_response_q(me_ptr);

  qurt_elite_channel_destroy(&me_ptr->channel);

  /* free memory */
  if(NULL != me_ptr->lab_struct_ptr)
  {
    qurt_elite_memory_free(me_ptr->lab_struct_ptr);
    me_ptr->lab_struct_ptr = NULL;
  }
  qurt_elite_memory_free(me_ptr);

  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
      "ListenProc_Destroy all memory deallocated");
}

/*------------------------------------------------------------------------------
 * Static Function Definitions
 *----------------------------------------------------------------------------*/

/**
  Destroys internal input data queue

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  None

  @dependencies
  None.
*/
static void listen_proc_svc_destroy_input_data_q(listen_proc_svc_t* me_ptr)
{
  (void)listen_proc_svc_flush_input_q(me_ptr);

  /* clean up data queue */
  if (me_ptr->svc_handle.dataQ)
  {
    /* destroy the q. */
    qurt_elite_queue_destroy(me_ptr->svc_handle.dataQ);
  }
}

/**
  Destroys internal output buffer queue

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  None

  @dependencies
  None.
*/

static void listen_proc_svc_destroy_output_buf_q(listen_proc_svc_t* me_ptr)
{
    // clean up buffer queue
   (void)listen_proc_svc_flush_output_q(me_ptr,ADSP_EOK);

   if (me_ptr->svc_handle.gpQ)
   {
      // destroy the q.
      qurt_elite_queue_destroy(me_ptr->svc_handle.gpQ);
   }
}



/**
  Destroys response queue

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  None

  @dependencies
  None.
*/
static void listen_proc_svc_destroy_response_q(listen_proc_svc_t* me_ptr)
{
  /* clean up response  queue */
  if (me_ptr->response_q_ptr)
  {
    qurt_elite_queue_destroy(me_ptr->response_q_ptr);
  }
}

/**
  This function is the main work loop for the service. Commands are handled with
  the highest priority. Data processing is handled only when command queue is
  empty.

  @param pInstance [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
static int listen_proc_svc_workloop(void* pInstance)
{
  listen_proc_svc_t *me_ptr = (listen_proc_svc_t*)pInstance;
  ADSPResult result=ADSP_EOK;

  me_ptr->curr_bit_field = LISTEN_PROC_CMD_SIG |LISTEN_PROC_DATA_SIG ;
  /*  Enter forever loop */
  for(;;)
  {
    uint32_t  nMask;
    /*  block on any one or more of selected queues to get a msg */
    me_ptr->channel_status = qurt_elite_channel_wait(&(me_ptr->channel),
                                                     me_ptr->curr_bit_field);
    for ( uint32_t i =0; i < LISTEN_PROC_NUM_Q; i++ )
    {
      nMask = 0x1 << (31 - i);
      if ( nMask & me_ptr->channel_status )
      {
        result = pProcQHandler[i](me_ptr);

        /* Service is destroyed, return immediately */
        if ( result == ADSP_ETERMINATED )
        {
          MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "WorkLoop terminated");
          return ADSP_EOK;
        }
      }
    }
  }
  return result;
}

/**
  This function processes commands from the LSM

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
static int listen_proc_svc_process_cmd_q(listen_proc_svc_t* me_ptr)
{
  ADSPResult result=ADSP_EOK;

  /* Take next msg off the q */
  result = qurt_elite_queue_pop_front(me_ptr->svc_handle.cmdQ,
                                      (uint64_t*)&(me_ptr->cmd_msg) );

  /* Process the msg */
  if (ADSP_EOK == result)
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "ProcessCmdQueue[%d]: Message ID 0x%lu!!",
          me_ptr->session_id,me_ptr->cmd_msg.unOpCode);
    switch (me_ptr->cmd_msg.unOpCode)
    {
      case ELITE_CUSTOM_MSG:
      {
        result =  listen_proc_svc_custom_msg_handler(me_ptr);
        break;
      }
      case ELITE_CMD_DESTROY_SERVICE:
      {
        result = listen_proc_svc_destroy_yourself(me_ptr);
        break;
      }
      case ELITE_CMD_SET_PARAM:
      {
        result = listen_proc_svc_set_param_handler(me_ptr);
        break;
      }
      case ELITE_CMD_GET_PARAM:
      {
        result = listen_proc_svc_get_param_handler(me_ptr);
        break;
      }
      default:
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
              "Unsupported message ID 0x%lx!!",
              me_ptr->cmd_msg.unOpCode);
        result = listen_proc_svc_return_unsupported(me_ptr);
        break;
      }
    }
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ListenProc[%d] Failed to dequeue command message!",
    		me_ptr->session_id);
  }

  return result;
}

/**
  This function handles custom messages coming from other 
  services 

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_custom_msg_handler(listen_proc_svc_t* me_ptr)
{

  ADSPResult result = ADSP_EOK;
  elite_msg_custom_header_t *pParamMsg =
                          (elite_msg_custom_header_t *)me_ptr->cmd_msg.pPayload;
  uint32_t sec_opcode = pParamMsg->unSecOpCode;

  switch (sec_opcode)
  {
    case ELITEMSG_CUSTOM_LISTEN_START:
    {
      result = listen_proc_svc_start_handler(me_ptr);
      break;
    }

    case ELITEMSG_CUSTOM_LISTEN_STOP:
    {
      result = listen_proc_svc_stop_handler(me_ptr);
      break;
    }

    case ELITEMSG_CUSTOM_LISTEN_REGISTER_SOUND_MODEL:
    {
      result = listen_proc_svc_register_sound_model_handler(me_ptr);
      break;
    }

    case ELITEMSG_CUSTOM_LISTEN_DEREGISTER_SOUND_MODEL:
    {
      result = listen_proc_svc_deregister_sound_model_handler(me_ptr);
      break;
    }
    case ELITEMSG_CUSTOM_LISTEN_EOB:
    {
      result = listen_proc_svc_eob_handler(me_ptr);
      break;
    }
    default:
      result = ADSP_EUNSUPPORTED;
      break;
  }

  return elite_msg_finish_msg(&me_ptr->cmd_msg, result);
}   

/**
  This function processes input data media type

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
  @param data_media_ptr [in/out] This points to the data media type

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult static listen_proc_svc_process_media_type(listen_proc_svc_t *me_ptr,
                                elite_msg_data_media_type_apr_t *data_media_ptr)
{
  ADSPResult result=ADSP_EOK;
  topo_media_fmt_t media_fmt;
  uint32_t iter = 0;

  MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO, "ListenProc[%d] Received Media Format command",
      me_ptr->session_id);


  /* Checks for validating Media type got is inline Start Configuration.
     During Start, we have temporary buffer allocation based on
     - num_channels
     - bytes_per_channel
     - int_samples_per_period
     Need to ensure that media format got is in accordance to the configuration
     used during connection */
  if(ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM == data_media_ptr->unMediaFormatID)
  {
    elite_multi_channel_pcm_fmt_blk_t *fmt_ptr =
        (elite_multi_channel_pcm_fmt_blk_t*)elite_msg_get_media_fmt_blk(
                                                                data_media_ptr);

    if(me_ptr->bits_per_sample != fmt_ptr->bits_per_sample)
    {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] started with bits_per_sample=%d but media type got was=%d",
            me_ptr->session_id,me_ptr->bits_per_sample,fmt_ptr->bits_per_sample);
      return ADSP_EUNEXPECTED;
    }
    if(me_ptr->num_channels != fmt_ptr->num_channels)
    {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] started with num_channels=%d but media type got was=%d",
            me_ptr->session_id,me_ptr->num_channels,fmt_ptr->num_channels);
      return ADSP_EUNEXPECTED;
    }
    if(me_ptr->sample_rate != fmt_ptr->sample_rate)
    {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] started with sample_rate=%lu but media type got was=%lu",
            me_ptr->session_id,me_ptr->sample_rate,fmt_ptr->sample_rate);
      return ADSP_EUNEXPECTED;
    }
    
    /* Translation from Elite media fmt to CAPI_V2 media fmt */
    media_fmt.f.bits_per_sample   = fmt_ptr->bits_per_sample;
    media_fmt.f.num_channels      = fmt_ptr->num_channels;
    media_fmt.f.sampling_rate     = fmt_ptr->sample_rate;
    media_fmt.f.data_is_signed    = fmt_ptr->is_signed;
    media_fmt.f.bitstream_format  = ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2;
    media_fmt.f.q_factor          = (fmt_ptr->bits_per_sample == 16) ?
                                    PCM_16BIT_Q_FORMAT : ELITE_32BIT_PCM_Q_FORMAT;

    if(fmt_ptr->is_interleaved)
    {
      media_fmt.f.data_interleaving = CAPI_V2_INTERLEAVED;
    }
    else
    {
      media_fmt.f.data_interleaving = CAPI_V2_DEINTERLEAVED_UNPACKED;
    }
    for( iter=0 ; iter<SIZE_OF_ARRAY(fmt_ptr->channel_mapping) ; iter++ )
    {
      media_fmt.f.channel_type[iter] = fmt_ptr->channel_mapping[iter];
    }
    media_fmt.h.data_format = CAPI_V2_FIXED_POINT;
  
  }
  else
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenProc[%d] Unknown Media Format Id [%x]",
          me_ptr->session_id,data_media_ptr->unMediaFormatID);
    return ADSP_EUNEXPECTED;
  }

  /* Set Media type to Topology */
  result = topo_set_input_media_type((topo_handle_t)me_ptr->topo_handle,
                                     &media_fmt,
                                     NULL);
  if(ADSP_SUCCEEDED(result))
  {
    /* Set is_data_to_process for successful media format */
    me_ptr->is_data_to_process = TRUE;
  }

  return result;
}

/**
  This function processes input data from AFE

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
static int listen_proc_svc_process_input_data_q(listen_proc_svc_t *me_ptr)
{
  ADSPResult result = ADSP_EOK;

  /*Take next msg off the q*/
  result= qurt_elite_queue_pop_front(me_ptr->svc_handle.dataQ,
                                     (uint64_t*)&me_ptr->input_data_msg);
  if (ADSP_FAILED(result))
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "ListenProc[%d] ProcessSourceInpDataQueue failed to dequeue input data msg! 0x%p",
          me_ptr->session_id, me_ptr->svc_handle.dataQ);
    me_ptr->input_data_msg.pPayload = NULL;
    return result;
  }

  if(!me_ptr->input_data_msg.pPayload)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "ListenProc[%d] ProcessSourceInpDataQueue Input data msg payload is NULL!",
        me_ptr->session_id);
    elite_msg_release_msg( &me_ptr->input_data_msg );
    return ADSP_EBADPARAM;
  }

  switch (me_ptr->input_data_msg.unOpCode)
  {
    case ELITE_DATA_MEDIA_TYPE:
    {
      elite_msg_data_media_type_apr_t *data_media_ptr =
              (elite_msg_data_media_type_apr_t*)me_ptr->input_data_msg.pPayload;

      /* Process media type */
      result = listen_proc_svc_process_media_type(me_ptr, data_media_ptr);
      if(ADSP_FAILED(result))
      {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
              "ListenProc[%d] Failed to process data buffer result=0x%x",
              me_ptr->session_id, result);
      }

      /*TODO: Idle place to call MMPM voting. Currenlty called at static
              lsm service during start handler */

      result = elite_msg_finish_msg(&me_ptr->input_data_msg, result);
      me_ptr->input_data_msg.pPayload = NULL;
      break;
    }

    case ELITE_DATA_BUFFER:
    {
      /* if it is not in run state return the buffers  OR
         if data_to_process == FALSE return the buffers */
      if((LISTEN_PROC_STATE_RUN != me_ptr->state) ||
         (FALSE == me_ptr->is_data_to_process)      )
      {
        /* Return payload buffer to buffer manger queue */
        MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
              "ListenProc[%d] Returning buffers without processing,"
              "Listen in STOP state or currently data is not processing: "
              "State:%d, Ready to process:%d",
              me_ptr->session_id, me_ptr->state,me_ptr->is_data_to_process);
        result = elite_msg_return_payload_buffer(&me_ptr->input_data_msg);
        me_ptr->input_data_msg.pPayload = NULL;
      }
      else
      {
        elite_msg_data_buffer_t* inp_data_buf_ptr =
                     (elite_msg_data_buffer_t*) me_ptr->input_data_msg.pPayload;

        if(!inp_data_buf_ptr->nActualSize)
        {
          MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ListenProc[%d] Got empty data buffer",
        		me_ptr->session_id);
          result = elite_msg_release_msg( &me_ptr->input_data_msg );
          me_ptr->input_data_msg.pPayload = NULL;
          break;
        }


        /* Process directly on Elite Data Buffer, no buffering in Listen
           Service */
        result = listen_proc_svc_process(me_ptr,
                                         inp_data_buf_ptr->nActualSize,
                                         (int8_t *)&inp_data_buf_ptr->nDataBuf);
        if(ADSP_FAILED(result))
        {
          MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
                "ListenProc[%d] Failed to process data buffer result=0x%x",
                me_ptr->session_id, result);
        }

        result = elite_msg_finish_msg(&me_ptr->input_data_msg, result);
        me_ptr->input_data_msg.pPayload = NULL;
      }
      break;
    }

    case ELITE_DATA_EOS:
    {
      MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO, "ListenProc[%d] Received EOS command",me_ptr->session_id);
      /*
       * After receiving EOS, don't process any new data.
       * EOS will come in two cases:
       * 1. When DC reset command send by listen proc, afe will send EOS
       * 2. When AFe port stops to which this is connected as client
       */
      me_ptr->is_data_to_process = FALSE;
      me_ptr->detection_status = LSM_DETECTION_STATUS_NONE;
      result = elite_msg_finish_msg(&me_ptr->input_data_msg, ADSP_EOK);
      me_ptr->input_data_msg.pPayload = NULL;
      break;
    }
    default:
    {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
            "ListenProc[%d] Unexpected Opcoded for input data msg 0x%lx",
            me_ptr->session_id, me_ptr->input_data_msg.unOpCode);
      result = elite_msg_finish_msg(&me_ptr->input_data_msg, ADSP_EUNEXPECTED);
      me_ptr->input_data_msg.pPayload = NULL;
      break;
    }
  }

  /* At this point, result should point to the error code corresponding how
     input_data_msg was released */
  if(ADSP_FAILED(result))
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "ListenProc[%d] Failed to release input data message %d",
          me_ptr->session_id, result);
  }
  return (result);
}

/**
  This function destroys the listen Processing service

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
static int listen_proc_svc_destroy_yourself(listen_proc_svc_t* me_ptr)
{
  ADSPResult result= ADSP_EOK;
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ListenProc[%d] DestroyYourself",me_ptr->session_id);

  /*
   * 1-Send disable command to AFE
   * 2-Flush the buffers if any
   * 3-Disconnect command to AFE
   */

  if (me_ptr->afe_port_id != AFE_PORT_ID_INVALID)
  {
    /*send disable client command*/
    result = listen_proc_send_cmd_to_afe(me_ptr,
                                         me_ptr->afe_port_id,
                                         ELITEMSG_CUSTOM_AFECLIENTDISABLE);
    if (ADSP_FAILED(result))
    {
      /*continue even though it fails*/
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] Connect to AFE port is failed, status:: %d",
            me_ptr->session_id, result);
    }

    /*flush the buffers*/
    listen_proc_svc_flush_input_q(me_ptr);

    /* send disconnect client command*/
    result = listen_proc_send_cmd_to_afe(me_ptr,
                                         me_ptr->afe_port_id,
                                         ELITEMSG_CUSTOM_AFEDISCONNECT_REQ);
    if (ADSP_FAILED(result))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "ListenProc[%d] Connect to AFE port is failed, status:: %d",
            me_ptr->session_id, result);
    }
  }

  elite_msg_finish_msg(&me_ptr->cmd_msg, ADSP_EOK);

  listen_proc_svc_destroy_svc(me_ptr);

  return ADSP_ETERMINATED;
}

/**
  This function processes an unsupported command sent to the 
  listen processing service 

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_return_unsupported(listen_proc_svc_t* me_ptr)
{
  /*unsupported command..finish msg with error code*/
  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "Unsupported command with opcode = 0x%lx",
        me_ptr->cmd_msg.unOpCode);
  return elite_msg_finish_msg(&(me_ptr->cmd_msg), ADSP_EUNSUPPORTED);
}


