/**
@file ListenProcSvc_lab_handler.cpp
@brief This file contains api for Look Ahead Buffer
 */

/*===========================================================================
NOTE: The @brief description and any detailed descriptions above do not appear
      in the PDF.

      The core_mainpage.dox file contains all file/group descriptions that
      are in the output PDF generated using Doxygen and Latex. To edit or
      update any of the file/group text in the PDF, edit the
      core_mainpage.dox file or contact Tech Pubs.
===========================================================================*/

/*===========================================================================
  Copyright (c) 2013-2014 QUALCOMM technologies Inc (QTI).
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/dynamic_svcs/listen_proc_svc/src/ListenProcSvc_lab_handler.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
3/11/14   SivaNaga      Created file.

========================================================================== */
#include "adsp_lsm_api.h"
#include "ListenProcSvc_lab_handler.h"
#include "ListenProcSvc_CustMsg.h"
#include "AFEInterface.h"
#include "EliteMsg_AfeCustom.h"
#include "Elite_fwk_extns_detection_engine.h"

#define LSM_API_VERSION_LAB_CONFIG_V1       0x1

static ADSPResult send_event_to_client(lsm_callback_handle_t *cb_data_ptr,
                                       uint32_t bit_mask);
/*
 * Function for initializing Look Ahead Buffer(LAB)
 * functionality :
 * return : ADSP error status
 */
ADSPResult listen_proc_lab_init(lsm_lab_struct_t *lab_struct_ptr,
		                        uint32_t sampling_rate,
		                        int16_t bits_per_sample,
		                        uint32_t max_kwed_delay_bytes)
{
  ADSPResult result = ADSP_EOK;
  uint16_t bytes_per_sample = (bits_per_sample>16)?4:2;
  uint32_t total_delay_bytes;
  uint32_t bytes_per_ms = (sampling_rate*bytes_per_sample)/1000;

  MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"listen_proc_lab_init : max_kwed_delay_bytes = %lu",
        max_kwed_delay_bytes);

  /* Populate the max frame delays returned by keyword end detection into dynamic structure
     This values is used to check if the frame delays returned after success detection is less than this value or not*/
  lab_struct_ptr->max_kwed_delay_bytes = max_kwed_delay_bytes;
  /* convert max_kwed_frame_delay and apps_wakeup_latency_ms into samples and sum up to get the circular buffer capacity*/
  total_delay_bytes = lab_struct_ptr->max_kwed_delay_bytes + (lab_struct_ptr->apps_wakeup_latency_ms * bytes_per_ms);

  /* circ buf memory depends on apps_wakeup_latency and max_kwed_delay_samples
         So allocating the circ buf in listen_proc_start and freeing it in listen_proc_stop so that if new setparam
         values comes in between lsm_stop and lsm_start they can be served*/
  lab_struct_ptr->circ_buf_ptr = (int8_t *) qurt_elite_memory_malloc(total_delay_bytes,
		                                                             QURT_ELITE_HEAP_DEFAULT);
  if(NULL == lab_struct_ptr->circ_buf_ptr)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"No memory = %lu for circ buf",
          total_delay_bytes);
    return ADSP_ENOMEMORY;
  }
  if(CIRCBUF_FAIL == circ_buf_init(&lab_struct_ptr->circ_buf_struct,
		                           lab_struct_ptr->circ_buf_ptr,
		                           total_delay_bytes))
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Circ buf init failed");
    return ADSP_EFAILED;
  }

  return result;
}

/*
 * Function for starting Look Ahead Buffer(LAB)
 * functionality :
 * return : ADSP error status
 */
ADSPResult listen_proc_lab_start(listen_proc_svc_t *session_ptr,  uint16_t bytes_per_sample)
{
  ADSPResult result = ADSP_EOK;
  lsm_lab_struct_t *lab_struct_ptr = (lsm_lab_struct_t *)session_ptr->lab_struct_ptr;
  /* adjust the read pointer of circular buffer so that it will start reading from the the kwed_position_samples number of samples from write_ptr */
  lab_struct_ptr->kwed_position_bytes = (lab_struct_ptr->kwed_position_bytes > lab_struct_ptr->max_kwed_delay_bytes) ? \
		                          lab_struct_ptr->max_kwed_delay_bytes : lab_struct_ptr->kwed_position_bytes;

  MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"listen_proc_lab_start : kwed_position_samples = %lu",
        lab_struct_ptr->kwed_position_bytes);

  // Register for pass through with AFE
  if (ADSP_FAILED(result = listen_proc_send_register_cmd_to_afe(session_ptr,
		                                                        AFE_PASS_THROUGH,
		                                                        TRUE,
		                                                        ELITEMSG_CUSTOM_AFE_CLIENT_CRITERIA_REGISTER)))
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "listen_proc_lab_start session : %d Pass through reg with AFE failed, status:: %d",
          session_ptr->session_id, result);
    listen_proc_lab_stop(session_ptr);
    return ADSP_EFAILED;
  }
  lab_struct_ptr->is_reg_with_afe = TRUE;

  return ADSP_EOK;
}

/*
 * Function for copying samples into circular buffer for Look Ahead Buffer(LAB)
 * functionality :
 * return : ADSP error status
 */
ADSPResult listen_proc_lab_process(lsm_lab_struct_t *lab_struct_ptr, int8_t *inp_ptr, uint32_t bytes_to_write,lsm_callback_handle_t *cb_data_ptr, int8_t algo_status)
{
  ADSPResult result = ADSP_EOK;
  circbuf_result status;
#ifdef  LAB_DEBUG
  MSG_3(MSG_SSID_QDSP6,DBG_HIGH_PRIO,
		"listen_proc_lab_process : bytes_to_write = %lu, algo_status = %d, port = %d",
		bytes_to_write,
		algo_status,
		cb_data_ptr->self_port);
#endif
  switch(algo_status)
  {
    case LSM_DETECTION_STATUS_DISCOVERY:
    {
      status = circ_buf_write(&lab_struct_ptr->circ_buf_struct,inp_ptr,bytes_to_write);
      if(CIRCBUF_SUCCESS != status)
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        	  "listen_proc_lab_process : Circ buf write failed, status = %d",status);
        result = ADSP_EFAILED;
      }
      break;
    }
    case LSM_DETECTION_STATUS_DETECTED:
    {
      status = circ_buf_write(&lab_struct_ptr->circ_buf_struct,inp_ptr,bytes_to_write);
      if(CIRCBUF_OVERFLOW == status)
      {
        send_event_to_client(cb_data_ptr,LSM_DATA_OVERFLOW_BIT_MASK);
      }
      else if(CIRCBUF_SUCCESS != status)
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        	  "listen_proc_lab_process : Circ buf write failed, status = %d",status);
        result = ADSP_EFAILED;
      }
      break;
    }
    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Unsupported algo status : %d",algo_status);
      result = ADSP_EFAILED;
      break;
    }
  }
  if(TRUE == lab_struct_ptr->start_lab)
  {
	  if(CIRCBUF_SUCCESS != circ_adjust_read(&lab_struct_ptr->circ_buf_struct,
			                                 lab_struct_ptr->kwed_position_bytes))
	  {
		  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Failed to update the circ buf with the kwed position");
		  return ADSP_EFAILED;
	  }
    lab_struct_ptr->start_lab = FALSE;
  }
  return result;
}

/*
  This function copies data from internal circ buffer to client buffer

  @param lab_struct_ptr [in] This points to the instance of lsm_lab_struct_t
         out_buf_params_ptr [in/out] This points to the lsm_proc_svc_out_buf_param_t

  @return
  None

  @dependencies
  None.
 */
void listen_proc_lab_copy_data_to_out_buf(lsm_lab_struct_t *lab_struct_ptr,
		                                     lsm_proc_svc_out_buf_param_t *out_buf_params_ptr)
{
  uint32_t out_buf_num_bytes;
  uint32_t bytes_to_copy;
  circ_buf_struct_t *circ_buff_struct_ptr = &lab_struct_ptr->circ_buf_struct;
  circbuf_result status;

  out_buf_num_bytes = out_buf_params_ptr->empty_bytes;

  if (out_buf_num_bytes > 0)
  {

    bytes_to_copy = circ_buff_struct_ptr->unread_bytes;
    bytes_to_copy = (bytes_to_copy > out_buf_num_bytes) ? out_buf_num_bytes : bytes_to_copy;

    int8_t *dest_ptr = ((int8_t *)(out_buf_params_ptr->shared_memmap_type_node.unVirtAddr)) + \
    		           (out_buf_params_ptr->out_buf_wr_offset);

    status = circ_buf_read(circ_buff_struct_ptr,dest_ptr,bytes_to_copy);
    if(CIRCBUF_FAIL == status)
    {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
         "listen_proc_lab_copy_data_to_buffer : Circ buf read failed");
      return;
    }
#ifdef LAB_DEBUG
    FILE *fp;
    fp = fopen("lab_input.raw","ab");
    fwrite(dest_ptr,1,bytes_to_copy,fp);
    fclose(fp);
#endif
    out_buf_params_ptr->empty_bytes = out_buf_params_ptr->empty_bytes - bytes_to_copy;
    out_buf_params_ptr->out_buf_wr_offset = out_buf_params_ptr->out_buf_wr_offset + bytes_to_copy;

  }

  return;
}

/*
 * Function for stopping Look Ahead Buffer(LAB)
 * functionality :
 * return : ADSP error status
 */
ADSPResult listen_proc_lab_stop(listen_proc_svc_t *session_ptr)
{
  ADSPResult result = ADSP_EOK;
  lsm_lab_struct_t *lab_struct_ptr = (lsm_lab_struct_t *)session_ptr->lab_struct_ptr;
  /* reset the circ buf */
  circ_buf_reset(&lab_struct_ptr->circ_buf_struct);

  if(TRUE == lab_struct_ptr->is_reg_with_afe)
  {
    // De-Register for pass through with AFE
    if (ADSP_FAILED(result = listen_proc_send_register_cmd_to_afe(session_ptr,
    		                                                      AFE_PASS_THROUGH,
    		                                                      FALSE,
    		                                                      ELITEMSG_CUSTOM_AFE_CLIENT_CRITERIA_REGISTER)))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
    		"listen_proc_lab_stop session : %d Pass through dereg with AFE failed, status:: %d",
    		session_ptr->session_id, result);
    }
    lab_struct_ptr->is_reg_with_afe = FALSE;
  }
  return result;
}

/*
 * Function for doing setparam for Look Ahead Buffer(LAB)
 * functionality :
 * return : cpe error status
 */
ADSPResult listen_proc_lab_setparam(listen_proc_svc_t *session_ptr,
		                            uint32_t param_id,
		                            uint32_t param_size,
		                            int8_t *params_buffer_ptr)
{
  ADSPResult result = ADSP_EOK;

  if(NULL == session_ptr->lab_struct_ptr)
  {
    session_ptr->lab_struct_ptr = qurt_elite_memory_malloc(sizeof(lsm_lab_struct_t),
    		                                               QURT_ELITE_HEAP_DEFAULT);
    if(NULL == session_ptr->lab_struct_ptr)
    {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"No memory for allocating LAB struct");
      return ADSP_ENOMEMORY;
    }
    memset(session_ptr->lab_struct_ptr,0,sizeof(lsm_lab_struct_t));
  }

  lsm_lab_struct_t *lab_session_ptr = (lsm_lab_struct_t *)session_ptr->lab_struct_ptr;
  lsm_param_id_enable_t *enable_ptr = (lsm_param_id_enable_t *)params_buffer_ptr;
  switch(param_id)
  {
    case LSM_PARAM_ID_ENABLE:
    {
      if (param_size < sizeof(lsm_param_id_enable_t))
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Set LSM_PARAM_ID_ENABLE fail: paramsize: %lu",
              param_size);
        return ADSP_EBADPARAM;
      }

      if(enable_ptr->enable == 1)
      {
        lab_session_ptr->lab_enable = TRUE;
      }
      else if (enable_ptr->enable == 0)
      {
        lab_session_ptr->lab_enable = FALSE;
      }
      else
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LSM_PARAM_ID_ENABLE invalid parameter:%d",
        	  enable_ptr->enable);
        return ADSP_EBADPARAM;
      }
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM LAB enable:%d",enable_ptr->enable);
      break;
    }
    case LSM_PARAM_ID_LAB_CONFIG:
    {
      /* Choose the appropriate config version  */
      uint32_t lab_config_minor_version =  *((uint32_t *)params_buffer_ptr);

      if(lab_config_minor_version > LSM_API_VERSION_LAB_CONFIG)
      {
        lab_config_minor_version = LSM_API_VERSION_LAB_CONFIG;
      }

      if(LSM_API_VERSION_LAB_CONFIG_V1 == lab_config_minor_version)
      {
        if (param_size < sizeof(lsm_param_id_lab_config_v1_t))
        {
          MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Set LAB_CONFIG fail: paramsize: %lu",
        		param_size);
          return ADSP_EBADPARAM;
        }

        lsm_param_id_lab_config_v1_t *lab_config_param_ptr = (lsm_param_id_lab_config_v1_t *)params_buffer_ptr;
        lab_session_ptr->apps_wakeup_latency_ms = lab_config_param_ptr->wake_up_latency_ms;
        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"APPS Wakeup latency in ms = %lu",
        	  lab_config_param_ptr->wake_up_latency_ms);
      }
      break;
    }
    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"lab_modeule Received Unsupported Param ID : 0x%lx",
    		param_id);
      result = ADSP_EUNSUPPORTED;
      break;
    }
  }
  return result;
}


/*
 * Function for doing getparam for Look Ahead Buffer(LAB)
 * functionality :
 * return : cpe error status
 */
ADSPResult listen_proc_lab_get_param(listen_proc_svc_t *session_ptr,
                                     uint32_t param_id,
                                     uint32_t param_size,
                                     int8_t *params_buffer_ptr,
                                     uint32_t *actual_param_buf_len)
{
  ADSPResult result = ADSP_EOK;

  lsm_lab_struct_t *lab_session_ptr = (lsm_lab_struct_t *)session_ptr->lab_struct_ptr;

  switch(param_id)
  {
    case LSM_PARAM_ID_ENABLE:
    {
      lsm_param_id_enable_t *enable_ptr = (lsm_param_id_enable_t *)params_buffer_ptr;
      if (param_size < sizeof(lsm_param_id_enable_t))
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Get LSM_PARAM_ID_ENABLE fail: paramsize: %lu",
              param_size);
        return ADSP_EBADPARAM;
      }

      if(NULL == lab_session_ptr)
      {
        enable_ptr->enable = 0;
      }
      else if(FALSE == lab_session_ptr->lab_enable)
      {
        enable_ptr->enable = 0;
      }
      else
      {
        enable_ptr->enable = 1;
      }
      enable_ptr->reserved = 0;
      *actual_param_buf_len = sizeof(lsm_param_id_enable_t);
      break;
    }
    case LSM_PARAM_ID_LAB_CONFIG:
    {
      lsm_param_id_lab_config_v1_t *lab_cfg_ptr = (lsm_param_id_lab_config_v1_t *)params_buffer_ptr;
      if (param_size < sizeof(lsm_param_id_lab_config_v1_t))
      {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Get LSM_PARAM_ID_LAB_CONFIG fail: paramsize: %lu",
              param_size);
        return ADSP_EBADPARAM;
      }
      lab_cfg_ptr->minor_version = LSM_API_VERSION_LAB_CONFIG_V1;
      if(NULL == lab_session_ptr)
      {
        /*TODO: Need to set a default if not set by client, this is part of RTC requirements*/
        lab_cfg_ptr->wake_up_latency_ms = 0;
      }
      else
      {
        lab_cfg_ptr->wake_up_latency_ms = lab_session_ptr->apps_wakeup_latency_ms;
      }
      *actual_param_buf_len = sizeof(lsm_param_id_lab_config_v1_t);
      break;
    }
    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"lab_modeule Received Unsupported Param ID : 0x%lx",
            param_id);
      result = ADSP_EUNSUPPORTED;
      break;
    }
  }
  return result;
}

/*
 * Function for deinitializing Look Ahead Buffer(LAB)
 * functionality :
 * return : ADSP error status
 */
ADSPResult listen_proc_lab_deinit(listen_proc_svc_t *session_ptr)
{
  ADSPResult result = ADSP_EOK;
  lsm_lab_struct_t *lab_struct_ptr = (lsm_lab_struct_t *)session_ptr->lab_struct_ptr;

  listen_proc_lab_stop(session_ptr);

  circ_buf_deinit(&lab_struct_ptr->circ_buf_struct);

  if(NULL != lab_struct_ptr->circ_buf_ptr)
  {
    qurt_elite_memory_free(lab_struct_ptr->circ_buf_ptr);
    lab_struct_ptr->circ_buf_ptr = NULL;
  }
  return result;
}

static ADSPResult send_event_to_client(lsm_callback_handle_t *cb_data_ptr,
                                       uint32_t bit_mask)
{
  ADSPResult result = ADSP_EOK;
  elite_apr_packet_t* apr_pkt_ptr = NULL;
  if (ADSP_FAILED(result = elite_apr_if_alloc_event(cb_data_ptr->apr_handle,
                                                    cb_data_ptr->self_addr,
                                                    cb_data_ptr->self_port,
                                                    cb_data_ptr->client_addr,
                                                    cb_data_ptr->client_port,
                                                    0,
                                                    LSM_DATA_EVENT_STATUS,
                                                    sizeof(lsm_data_event_status_t), &apr_pkt_ptr)))
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
       "listen_proc_lab_process: Could not allocate circ buf status event packet = %d!",
       result);
     return result ;
  }
  if(NULL == apr_pkt_ptr )
  {
     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
       "listen_proc_lab_process: Not enough memory to allocate apr pkt");
   return ADSP_ENOMEMORY;
  }

  memset(apr_pkt_ptr, 0, sizeof(lsm_data_event_status_t));

  lsm_data_event_status_t *circ_buf_status = (lsm_data_event_status_t *)elite_apr_if_get_payload_ptr(apr_pkt_ptr);
  circ_buf_status->status |= LSM_DATA_OVERFLOW_BIT_MASK;

  //Now send the event to client
  if (ADSP_FAILED (result = elite_apr_if_async_send(cb_data_ptr->apr_handle, apr_pkt_ptr)))
  {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
           "listen_proc_lab_process : Failed to send LSM_DATA_EVENT_STATUS event = %d!",
           result);
       if (ADSP_FAILED(result = elite_apr_if_free(cb_data_ptr->apr_handle, apr_pkt_ptr)))
       {
          MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Freeing Packet failed = %d!", result);
       }
       return result;
  }
  MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO,
      "listen_proc_lab_process : Circ buf status Event Sent to apps, status:%lu",
      circ_buf_status->status);
  return result;
}
