# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/avs/listen/lsm_devcfg/target/config/lsm_target_devcfg.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 139 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/avs/listen/lsm_devcfg/target/config/lsm_target_devcfg.xml" 2
<!--
  This file contains LSM device configuration parsing information

  Copyright (c) 2009-2015 Qualcomm Technologies, Incorporated. All Rights Reserved.
  QUALCOMM Proprietary. Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 -->

<!-- NULL Driver does not require Dal Driver Interface APIs, since none of LSM devcfg uses DAL Device framework -->
<driver name="NULL">
   <device id="LSM">
      <props name="lsm_mmpm_prop_struct_ptr" type=DALPROP_ATTR_TYPE_STRUCT_PTR>lsm_mmpm_prop</props>
   </device>
</driver>
