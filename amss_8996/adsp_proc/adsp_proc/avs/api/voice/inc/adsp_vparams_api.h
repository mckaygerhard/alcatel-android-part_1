#ifndef __ADSP_VPARAMS_API_H__
#define __ADSP_VPARAMS_API_H__

/**
  @file adsp_vparams_api.h
  @brief This file contains module and parameter IDs for calibration purposes.
*/
/*===========================================================================
NOTE: The @brief description above does not appear in the PDF.
      The description that displays in the PDF is located in the
      Voice_mainpage.dox file. Contact Tech Pubs for assistance.
===========================================================================*/
/*===========================================================================
  Copyright (c) 2010, 2012-2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/
/*===========================================================================
                        EDIT HISTORY FOR MODULE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/17/15   sw      (Tech Pubs) Merged Doxygen comments from 8952; edited
                   comments for 8996.
02/25/13   sw      (Tech Pubs) Updated Doxygen for 2.0 internal draft.
02/27/12   sw      (Tech Pubs) Updated Doxygen comments for Interface Spec doc.
10/11/10   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
===========================================================================*/


/** @ingroup cal_param_ids
    Parameter used to enable or disable a module.

    @inputtable{Cal_ID_0x00010E00.tex}
*/
#define VOICE_PARAM_MOD_ENABLE             (0x00010E00)

/** @cond OEM_only */
/** @ingroup group cal_module_ids
    Module that identifies the Limiter algorithm in the voice stream
    on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_TX_STREAM_LIMITER
*/
#define VOICE_MODULE_TX_STREAM_LIMITER (0x00010F15)

/** @ingroup cal_param_ids
    ID of the Limiter parameter used to control the dynamic range of signals.

    The maximum array size for this parameter is:
    - Version 0 -- 16 bytes

    @inputtable{Cal_ID_0x00010E33.tex} @newpage
*/
#define VOICE_PARAM_TX_STREAM_LIMITER (0x00010E33)

/** @ingroup group cal_module_ids
    Module that identifies the Comfort Noise Generation algorithm in the
    voice stream on the Tx path.

    This module supports the #VOICE_PARAM_MOD_ENABLE parameter ID.
*/
#define VOICE_MODULE_ENC_CNG                (0x00010F13)
/** @endcond */

/** @addtogroup cal_param_ids
@{ */
/** @cond OEM_only */
/** ID of the single-mic ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 92 bytes

    @note1hang Single-mic ECNS also supports a versioned command,
               #VOICE_PARAM_SMECNS_EXT.

    @inputtable{Cal_ID_0x00010E01.tex}
*/
#define VOICE_PARAM_SMECNS                 (0x00010E01)

/** ID of the SMECNS Preset parameter that contains preset coefficients for
    single-mic ECNS.

    The maximum array size for this parameter is:
    - Version 0 -- 1604 bytes

    @inputtable{Cal_ID_0x00010E02.tex}
*/
#define VOICE_PARAM_SMECNS_PRESET          (0x00010E02)

/** ID of the VP3 parameter, which contains VP3 data for all voice processing
    modules.

    The #VOICE_PARAM_VP3_SIZE parameter provides the size, which can be
    different for each software device ID (each device ID can correspond to a
    different topology ID for voice Tx processing).

    This parameter must be saved before destruction or reinitialization of the
    voice processing session, and it must be restored before the voice
    processing session is started.

    @inputtable{Cal_ID_0x00010E03.tex} @newpage
*/
#define VOICE_PARAM_VP3                    (0x00010E03)
/** @endcond */

/** @cond OEM_only */
/** ID of the Wide Voice algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 44 bytes

    @inputtable{Cal_ID_0x00010E04.tex} @newpage
*/
#define VOICE_PARAM_WV                     (0x00010E04)

/** ID of the Wide Voice v2 algorithm parameter, which supports all
    types of NB vocoders and WB vocoders running in NB mode of operation.

    The fixed size for this parameter is:
    - Version 0 -- 64 bytes

    @inputtable{Cal_ID_0x00010E42.tex} @newpage
*/
#define VOICE_PARAM_WV_V2                  (0x00010E42)

/** ID of the Slow Talk parameter for the voice activity detection and
    expansion algorithm.

    The fixed size for this parameter is:
    - Version 0 -- 60 bytes

    @inputtable{Cal_ID_0x00010E05.tex}
*/
#define VOICE_PARAM_ST                     (0x00010E05)

/** ID of the Slow Talk Expansion algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E16.tex} @newpage
*/
#define VOICE_PARAM_ST_EXP                 (0x00010E16)

/** ID of the AVC/RVE algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 148 bytes

    @inputtable{Cal_ID_0x00010E06.tex} @newpage
*/
#define VOICE_PARAM_AVC_RVE                (0x00010E06)

/** ID of the AVC/RVE v2 algorithm parameter, which upgrades the
    single-mic noise estimate method to the Minimum Statistics method.

    The fixed size for this parameter is:
    - Version 0 -- 164 bytes
    - Version 1 extension -- 180 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E41.tex}

    @par VOICE_PARAM_AVC_RVE_V2 version 1 extensionension
    @inputtable{Cal_ID_0x00010E41_V1_ext.tex}
*/
#define VOICE_PARAM_AVC_RVE_V2                (0x00010E41)

/** ID of the parameter used to enable the AVC/RVE algorithms.

    The fixed size for this parameter is:
    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E15.tex} @newpage
*/
#define VOICE_PARAM_AVC_RVE_ENABLE         (0x00010E15)
/** @endcond */

/** @cond OEM_only */
/** ID of the PBE algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 56 bytes, followed by a variable size IIR filter structure

    The filter sizes are defined by configurable parameters. The maximum size
    for voice applications is 236 bytes. The filter order is assumed to be 3
    for high, low, and band-pass filters.

    @inputtable{Cal_ID_0x00010E07.tex} @newpage
*/
#define VOICE_PARAM_PBE                    (0x00010E07)

/** ID of the multiband dynamic range control (MBDRC) algorithm
    parameter.

    The fixed size for this parameter is:
    - Version 0 -- 12 bytes, followed by variable size DRC and FIR filter
      structures

    The filter sizes are defined by configurable parameters. The maximum size
    for voice applications is 472 bytes. For voice, the maximum number of
    bands is assumed to be 3.

    @inputtable{Cal_ID_0x00010E08.tex}
*/
#define VOICE_PARAM_MBDRC                  (0x00010E08)

/** ID of the MBDRC v2 algorithm parameter. This parameter ID
    supports the following features:
    - Calibration for up to five bands
    - NB, WB, SWB, and FB sampling rates
    - Higher order FIR filter for SWB and FB sampling rates (up to 48 kHz
      processing)

    The fixed size for this parameter is dynamic based on the number of bands
    and the FIR filter length, which depends on the sampling rate (8, 16, 32,
    48 kHz).

    In the following table, the parameters in bold correspond to subband DRC
    configuration, which is repeated based on the number of bands (usNumBands).

    @inputtable{Cal_ID_0x00010E66.tex}

    The filterlength variable is hardcoded based on the number of bands and
    voice sampling rates:

    @inputtable{Cal_ID_0x00010E66_filter_length.tex} @newpage
*/
#define VOICE_PARAM_MBDRC_V2                 (0x00010E66)

/** ID of the parameter used to monitor the MBDRC filter crossover frequencies
    for different subbands.

    @inputtable{Cal_ID_0x00010E67.tex}
*/
#define VOICE_PARAM_MBDRC_FILTER_XOVER_FREQ  (0x00010E67)

/** ID of the IIR Tuning Filter algorithm parameter for voice.

    The fixed size for this parameter is:
    - Version 0 -- 2 bytes, followed by variable-sized IIR filter structures

    The filter sizes are defined by configurable parameters. The maximum size
    for voice applications is 224 bytes. For voice, the maximum number of
    filter stages is assumed to be 10. If the number of stages is an odd
    number, 2 bytes of padding must be added at the end of the payload.

    @inputtable{Cal_ID_0x00010E09.tex}
*/
#define VOICE_PARAM_IIR                    (0x00010E09)
/** @endcond */

/** @cond OEM_only */
/** ID of the FIR tuning filter algorithm parameter for voice.

    This parameter contains a field that indicates the number of filter taps,
    followed by an FIR filter coefficient structure. The supported Q-factor is
    Q14.

    The maximum number of FIR taps for voice applications is 128 taps.
    The maximum size for this parameter is:
    - Version 0 -- 260 bytes

    @inputtable{Cal_ID_0x00010E0A.tex} @newpage
*/
#define VOICE_PARAM_FIR                    (0x00010E0A)

/** ID of the FNS algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 76 bytes

    @inputtable{Cal_ID_0x00010E0B.tex} @newpage
*/
#define VOICE_PARAM_FNS                    (0x00010E0B)

/** ID of the FNS v2 algorithm parameter, which incorporates a comfort
    noise injection module.

    The maximum array size for this parameter is:
    - Version 0 -- 84 bytes

    @inputtable{Cal_ID_0x00010E43.tex}
*/
#define VOICE_PARAM_FNS_V2                    (0x00010E43)
/** @endcond */
/** @} */ /* end_addtogroup cal_param_ids */

/** @cond OEM_only */
/** @ingroup cal_module_ids
    Module that identifies the Time Warp algorithm in the voice postprocessor
    on the Rx path.

    This module supports the #VOICE_PARAM_MOD_ENABLE parameter ID.
*/
#define VOICE_MODULE_TIMEWARP        (0x00010F0B)
/** @endcond */

/** @addtogroup cal_param_ids
@{ */
/** @cond OEM_only */
/** ID of the DTMF Detection algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 36 bytes

    @inputtable{Cal_ID_0x00010E0C.tex} @newpage
*/
#define VOICE_PARAM_DTMF_DETECTION         (0x00010E0C)

/** ID of the Echo Canceller Rx algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 20 bytes

    @inputtable{Cal_ID_0x00010E0D.tex}
*/
#define VOICE_PARAM_ECRX                   (0x00010E0D)

/** ID of the Dynamic Range Control (DRC) algorithm parameter.

    The fixed size for this parameter is:
    - Version 0 -- 56 bytes

    @inputtable{Cal_ID_0x00010E0E.tex}
*/
#define VOICE_PARAM_DRC                    (0x00010E0E)

/** ID of the dual-mic ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 448 bytes

    @inputtable{Cal_ID_0x00010E11.tex} @newpage
*/
#define VOICE_PARAM_DMECNS                 (0x00010E11)

/** ID of the Fluence dual-mic and tri-mic ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 468 bytes

    @inputtable{Cal_ID_0x00010E20.tex} @newpage
*/
#define VOICE_PARAM_FLECNS                 (0x00010E20)
/** @endcond */

/** @cond OEM_only */
/** ID of the Fluence dual-mic and tri-mic ECNS with versioning parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 92 bytes

    @inputtable{Cal_ID_0x00010E1F.tex}
*/
#define VOICE_PARAM_FLECNS_EXT               (0x00010E1F)

/** ID of the Fluence Pro (quad-mic) and Fluence v5 (single-mic and dual-mic)
    ECNS parameter.

    The maximum array size for this parameter is:
     - Version 0 -- 904 bytes
     - Version 1 extension -- 908 bytes
     - Version 2 extension -- 920 bytes
     - Version 3 extension -- 932 bytes
     - Version 4 extension -- 976 bytes
     - Version 5 extension -- 984 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E1C.tex}

    @par VOICE_PARAM_FPECNS version 1 extension
    @inputtable{Cal_ID_0x00010E1C_V1_ext.tex}

    @par VOICE_PARAM_FPECNS version 2 extension
    @inputtable{Cal_ID_0x00010E1C_V2_ext.tex}

    @par VOICE_PARAM_FPECNS version 3 extension
    @inputtable{Cal_ID_0x00010E1C_V3_ext.tex}

    @par VOICE_PARAM_FPECNS version 4 extension
    @inputtable{Cal_ID_0x00010E1C_V4_ext.tex}

    @par VOICE_PARAM_FPECNS version 5 extension
    @inputtable{Cal_ID_0x00010E1C_V5_ext.tex} @newpage
*/
#define VOICE_PARAM_FPECNS                 (0x00010E1C)
/** @endcond */

/** @cond OEM_only */
/** ID of the Fluence v5 ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 904 bytes
    - Version 1 extension -- 908 bytes
    - Version 2 extension -- 912 bytes
    - Version 3 extension -- 1108 bytes
    - Version 4 extension -- 5380 bytes
    - Version 5 extension -- 5496 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E26.tex} @newpage

    @par VOICE_PARAM_FV5ECNS version 1 extension
    @inputtable{Cal_ID_0x00010E26_V1_ext.tex}

    @par VOICE_PARAM_FV5ECNS version 2 extension
    @inputtable{Cal_ID_0x00010E26_V2_ext.tex}

    @par VOICE_PARAM_FV5ECNS version 3 extension
    @inputtable{Cal_ID_0x00010E26_V3_ext.tex}

    @par VOICE_PARAM_FV5ECNS version 4 extension
    @inputtable{Cal_ID_0x00010E26_V4_ext.tex} @newpage

    @par VOICE_PARAM_FV5ECNS version 5 extension
    @inputtable{Cal_ID_0x00010E26_V5_ext.tex} @newpage
*/
#define VOICE_PARAM_FV5ECNS                (0x00010E26)

/** ID of the Fluence v5 Broadside and Pro v2 ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 716 bytes
    - Version 1 extension -- 724 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E35.tex}

    @par VOICE_PARAM_FLUENCE_EC version 1 extension 
    @inputtable{Cal_ID_0x00010E35_V1_ext.tex} @newpage
*/
#define VOICE_PARAM_FLUENCE_EC                (0x00010E35)
/** @endcond */

/** @cond OEM_only */
/** ID of the Fluence v5 Broadside and Pro v2 ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 1064 bytes
    - Version 1 extension -- 1052 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E36.tex}

    @par VOICE_PARAM_FLUENCE_NS_SPK version 1 extension
    @inputtable{Cal_ID_0x00010E36_V1_ext.tex} @newpage
*/
#define VOICE_PARAM_FLUENCE_NS_SPK            (0x00010E36)

/** ID of the single-mic ECNS with versioning parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 96 bytes
    - Version 1 extension -- 112 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E27.tex}

    @par VOICE_PARAM_SMECNS_EXT version 1 extension
    @inputtable{Cal_ID_0x00010E27_V1_ext.tex} @newpage
*/
#define VOICE_PARAM_SMECNS_EXT               (0x00010E27)

/** ID of the read-only parameter structure for Fluence dual-mic and Fluence
    active noise cancellation (ANC) tri-mic ECNS.

    The maximum array size for this parameter is:
    - Version 0 -- 240 bytes

    @inputtable{Cal_ID_0x00010E1D.tex} @newpage
*/
#define VOICE_PARAM_FL_STATE               (0x00010E1D)

/** ID of the read-only parameter structure for internal status monitoring of
    Fluence Pro (quad-mic) and Fluence v5 (single-mic and dual-mic) ECNS.

    The maximum array size for this parameter is:
    - Version 0 -- 860 bytes

    @inputtable{Cal_ID_0x00010E30.tex} @newpage
*/
#define VOICE_PARAM_FP_STATE               (0x00010E30)
/** @endcond */

/** @cond OEM_only */
/** ID of the read-only parameter structure for internal status monitoring of
    Fluence v5 single-mic, dual-mic ECNS.

    The maximum array size for this parameter is:
    - Version 0 -- 116 bytes
    - Version 1 extension -- 180 bytes
    - Version 2 extension -- 228 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E31.tex} @newpage

    @par VOICE_PARAM_FV5_STATE version 1 extension
    @inputtable{Cal_ID_0x00010E31_V1_ext.tex}

    @par VOICE_PARAM_FV5_STATE version 2 extension
    @inputtable{Cal_ID_0x00010E31_V2_ext.tex}
*/
#define VOICE_PARAM_FV5_STATE              (0x00010E31)

/** ID of the read-only parameter structure for Fluence v5 spatial filter
    coefficients (internal status monitoring of single-mic and dual-mic
    ECNS).

    The maximum array size for this parameter is:
    - Version 0 -- 4100 bytes

    @inputtable{Cal_ID_0x00010E2D.tex} @newpage
*/
#define VOICE_PARAM_FV5_SPF_COEFF              (0x00010E2D)

/** For the Fluence v5 dual-mic Broadside and Fluence Pro v2 quad-mic
    Speakerphone modes, ID of the read-only parameter structure for the noise
    suppressor's internal status monitoring.

    The maximum array size for this parameter is:
    - Version 0 -- 980 bytes
    - Version 1 extension -- 984 bytes

    @par Version 0
    @inputtable{Cal_ID_0x00010E40.tex}

    @par VOICE_PARAM_FLUENCE_NS_SPK_STATE version 1 extension
    @inputtable{Cal_ID_0x00010E40_V1_ext.tex}
*/
#define VOICE_PARAM_FLUENCE_NS_SPK_STATE    (0x00010E40)
/** @endcond */

/** @cond OEM_only */
/** ID of the parameter for Fixed Echo Path Delay in the firmware. The delay
    is in multiples of 8 kHz samples.

    The maximum array size for this parameter is:
    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E1E.tex}
*/
#define VOICE_PARAM_ECHO_PATH_DELAY        (0x00010E1E)
/** @endcond */

/** @cond OEM_only */
/** ID of the single-mic ECNS v2 parameter for noise suppression.

    The maximum array size for this parameter is:
    - Version 0 -- 96 bytes

    @inputtable{Cal_ID_0x00010E62.tex} @newpage
*/
#define VOICE_PARAM_SMECNS_V2_NS              (0x00010E62)

/** ID of the single-mic ECNS v2 parameter for echo cancellation.

    The maximum array size for this parameter is:
    - Version 0 -- 156 bytes

    @inputtable{Cal_ID_0x00010E61.tex} @newpage
*/
#define VOICE_PARAM_SMECNS_V2_EC              (0x00010E61)

/** ID of the read-only parameter structure for internal status monitoring of
    an echo cancellation block of the single-mic ECNS v2 parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 696 bytes

    @inputtable{Cal_ID_0x00010E64.tex} @newpage
*/
#define VOICE_PARAM_SMECNS_V2_EC_STATE     (0x00010E64)

/** ID of the read-only parameter structure for internal status monitoring of
    a noise suppression block of the single-mic ECNS v2 parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 32 bytes

    @inputtable{Cal_ID_0x00010E65.tex}
*/
#define VOICE_PARAM_SMECNS_V2_NS_STATE     (0x00010E65)
/** @endcond */

/** @cond OEM_only */
/** ID of the parameter for fixed echo path delay in the firmware. The delay
    is in multiples of 8 kHz samples.

    The maximum array size for this parameter is:
    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E63.tex}
*/
#define VOICE_PARAM_SMECNS_V2_EC_DELAY         (0x00010E63)
/** @endcond */

/** ID of the Volume Control algorithm parameter on the Tx and Rx paths.
    The fixed size for this parameter is:

    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E12.tex} @newpage
*/
#define VOICE_PARAM_VOL                    (0x00010E12)

/** @cond OEM_only */
/** ID of the Gain parameter.

    The fixed size for this parameter is:
    - Version 0 -- 4 bytes

    @inputtable{Cal_ID_0x00010E13.tex}
*/
#define VOICE_PARAM_GAIN                   (0x00010E13)

/** ID of the Adaptive Input Gain (AIG) algorithm parameter on the Tx and Rx
    paths.

    The fixed size for this parameter is:
    - Version 0 -- 36 bytes

    @inputtable{Cal_ID_0x00010E17.tex} @newpage
*/
#define VOICE_PARAM_AIG                    (0x00010E17)
/** @endcond */

/** @cond OEM_only */
/** ID of the read-only parameter used to return the Adaptive Filter (AF)
    coefficients of the currently configured echo canceller.

    @inputtable{Cal_ID_0x00010E19.tex} @vertspace{-3}
    @vertspace{-3}

    @subhead{Supported topology IDs}
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS -- Single-mic ECNS
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS_V2 -- Single-mic ECNS v2
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCE -- Dual-mic Fluence
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_QM_FLUENCE_PRO -- Quad-mic Fluence Pro
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_FLUENCEV5 -- Single-mic Fluence v5
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCEV5 -- Dual-mic Fluence v5

    For detailed information, refer to <i>Topology IDs</i> in
    @xrefcond{Q8,80-NF772-13,80-NA609-13}.

    @subhead{AFCoeffData data structures}
    AFCoeffData is defined in the following topology AF data structure tables
    and is based on the voice processing topology. The structure length is
    variable based on the number of taps the module uses at the time of the
    query.

    AF data structure for the VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS
    topology:

    @inputtable{Cal_ID_0x00010E19_topo_AFCoeffData_TX_SM_ECNS.tex}
    @vertspace{-6}

    AF data structure for the VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCE
    topology:

    @inputtable{Cal_ID_0x00010E19_topo_AFCoeffData_TX_DM_FLUENCE.tex}
    @newpage

    AF data structure for the following topologies:
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_QM_FLUENCE_PRO
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_QM_FLUENCE_PROV2
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_FLUENCEV5
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_SM_ECNS_V2
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCEV5
    - VSS_IVOCPROC_TOPOLOGY_ID_TX_DM_FLUENCEV_BROADSIDE

    @inputtable{Cal_ID_0x00010E19_topo_AFCoeffData_TX_QM_FLUENCE_PRO.tex}
    @vertspace{-6}

    @subhead{AecPathStruct variable length payload}
    Each AecPathStruct is defined by the following variable length payload.
    @vertspace{6}

    @inputtable{Cal_ID_0x00010E19_topo_AecPathStruct_payload.tex}
    @vertspace{-6}

    The total number of taps over all filters for both Fg and Bg arrays must
    not exceed 2000. Fg and Bg filters for a particular filter index i use the
    same length AFLen[i]. This means the sum of AFLen[i] over i from 0 to
    (NumAfFilter-1) must not exceed (2000 / 2) = 1000. The maximum size is
    4024&nbsp;bytes.

    BASE represents the byte offset of the current AecPath i, which has filter
    length AFLen[i]. The next AecPathStruct for index (i+1) starts at offset
    BASE+4+4*AFLen[i]. @newpage

    @subhead{Maximum size of VOICE\_PARAM\_AF\_COEFFS}
    The memory allocated to get the AF coefficents must be large enough to
    accommodate the maximum size of the VOICE_PARAM_AF_COEFFS structure. The
    maximum size depends on the maximum number of AF taps allowed for the
    current topology configuration.

    The following table lists the maximum size according to topology.
    For detailed information on the topology IDs, refer to
    @xrefcond{Q8,80-NF772-13,80-NA609-13}. @vertspace{6}

    @inputtable{Cal_ID_0x00010E19_maximum_size.tex}
*/
#define VOICE_PARAM_AF_COEFFS              (0x00010E19)

/** ID of the read-only parameter used to read the size of VP3 data. This
    parameter is a multiple of 4 bytes, which is defined by the voice
    processor Tx to maintain parameter alignment.

    @inputtable{Cal_ID_0x00010E1A.tex} @newpage
*/
#define VOICE_PARAM_VP3_SIZE               (0x00010E1A)

/** @endcond */

/** ID of the Soft-stepping Volume Control algorithm parameter on Tx and Rx
    paths. This parameter is used to change the volume linearly over ramp
    duration.

    This read-only parameter has a total size of:
    - Version 0 -- 8 bytes

    @inputtable{Cal_ID_0x00010E21.tex}
*/
#define VOICE_PARAM_SOFT_VOL               (0x00010E21)

/** @cond OEM_only */
/** ID of the Voice Plus dual-mic ECNS parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 80 bytes

    @inputtable{Cal_ID_0x00010E34.tex}
*/
#define VOICE_PARAM_VPECNS                 (0x00010E34)

/** ID of the Sound Focus parameter for ECNS.

    The parameters listed in this structure correspond to how a sound focus
    application is configured. This is a read/write parameter
    (SET_PARAM and GET_PARAM commands are supported).

    This parameter ID applies only to #VOICE_MODULE_FLUENCE_PROV2 and
    #VOICE_MODULE_FLUENCEV5_BROADSIDE. This parameter ID can only be set or
    gotten (GET_PARAM) on topologies that contain these modules. 

    More than one topology supports this parameter and the client might not
    have knowledge of the ECNS module ID. Therefore, the generic module ID,
    #VOICEPROC_MODULE_TX, supports this parameter. The VPM ensures
    that the payload is forwarded to the ECNS module in the chain.

    Upon a query (GET_PARAM), the data returned is consistent with what the
    algorithm is using internally.

    The maximum array size for this parameter is:
    - Version 0 -- 28 bytes

    @inputtable{Cal_ID_0x00010E37.tex} @newpage
*/
#define VOICE_PARAM_FLUENCE_SOUNDFOCUS (0x00010E37)

/** ID of the Source Tracking parameter for ECNS.

    Based on the sector configuration the user sets, this structure is 
    populated and is made available by Fluence once every 20 ms. This is a 
    read-only parameter (only the GET_PARAM commands are supported).

    This parameter ID applies only to #VOICE_MODULE_FLUENCE_PROV2 and
    #VOICE_MODULE_FLUENCEV5_BROADSIDE. This parameter ID can only be gotten
    (GET_PARAM) on topologies that contain these modules. 

    More than one topology supports this parameter and the client might not
    have knowledge of the ECNS module ID. Therefore, the generic module ID,
    #VOICEPROC_MODULE_TX, supports this parameter. The VPM ensures
    that the payload is forwarded to the ECNS module in the chain.

    The maximum APR packet size allowed varies across chipsets based on the
    available memory. The client must consider the chipset and packet size to
    choose either the out-of-band or in-band form of transfer for this parameter.

    The maximum array size for this parameter is:
    - Version 0 -- 376 bytes

    @inputtable{Cal_ID_0x00010E38.tex} @newpage
*/
#define VOICE_PARAM_FLUENCE_SOURCETRACKING (0x00010E38)

/** ID of the Fixed Steering Vector parameter for ECNS.

    This parameter ID applies only to #VOICE_MODULE_FLUENCE_PROV2.

    The maximum APR packet size allowed varies across chipsets based on the
    available memory. The client must consider the chipset and packet size to
    choose either the out-of-band or in-band form of transfer for this parameter.

    The maximum array size for this parameter is 30784 bytes

    @inputtable{Cal_ID_0x00010EA9.tex} @newpage
*/
#define VOICE_PARAM_FSTV (0x00010EA9)

/** @endcond */
/** @} */ /* end_addtogroup cal_param_ids */

/** @addtogroup cal_module_ids
@{ */
/** @cond OEM_only */
/** Module that identifies single-mic ECNS in the voice processor on the
    Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_SMECNS (supported only for backward compatibility)
    - #VOICE_PARAM_SMECNS_PRESET
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_FNS
    - #VOICE_PARAM_SMECNS_EXT @newpage
*/
#define VOICE_MODULE_SMECNS                (0x00010EE0)

/** Module that identifies single-mic ECNS version 2 in the voice processor
    on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_SMECNS_V2_EC_DELAY
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_SMECNS_V2_EC
    - #VOICE_PARAM_SMECNS_V2_NS
    - #VOICE_PARAM_SMECNS_V2_NS_STATE
    - #VOICE_PARAM_SMECNS_V2_EC_STATE
*/
#define VOICE_MODULE_SMECNS_V2                (0x00010F1F)

/** Module that identifies dual-mic ECNS in the voice processor on the
    Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_DMECNS (supported only for backward compatability)
    - #VOICE_PARAM_FLECNS
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_FL_STATE
    - #VOICE_PARAM_FLECNS_EXT
*/
#define VOICE_MODULE_DMECNS                (0x00010EE1)

/** @endcond */

/** @cond OEM_only */
/** Module that identifies Fluence Pro quad-mic ECNS in the voice processor on
    the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FPECNS
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_ECHO_PATH_DELAY @newpage
*/
#define VOICE_MODULE_FPECNS_QM             (0x00010F08)

/** Module that identifies Fluence Pro v2 quad-mic ECNS in the voice
    processor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_ECHO_PATH_DELAY
    - #VOICE_PARAM_FLUENCE_EC
    - #VOICE_PARAM_FLUENCE_NS_SPK
    - #VOICE_PARAM_FP_STATE
    - #VOICE_PARAM_FLUENCE_NS_SPK_STATE
*/
#define VOICE_MODULE_FLUENCE_PROV2 (0x00010F17)

/** Module that identifies dual-mic Fluence v5 ECNS in the voice processor on
    the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FPECNS
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_ECHO_PATH_DELAY
    - #VOICE_PARAM_FV5ECNS
    - #VOICE_PARAM_FP_STATE
    - #VOICE_PARAM_FV5_STATE
    - #VOICE_PARAM_FV5_SPF_COEFF
*/
#define VOICE_MODULE_FV5ECNS_DM            (0x00010F0A)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies dual-mic Fluence v5 Broadside ECNS in the voice
    processor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_ECHO_PATH_DELAY
    - #VOICE_PARAM_FLUENCE_EC
    - #VOICE_PARAM_FLUENCE_NS_SPK
    - #VOICE_PARAM_FP_STATE
    - #VOICE_PARAM_FLUENCE_NS_SPK_STATE @newpage
*/
#define VOICE_MODULE_FLUENCEV5_BROADSIDE (0x00010F18)

/** Identifies single-mic Fluence v5 ECNS in the voice processor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FPECNS
    - #VOICE_PARAM_AF_COEFFS
    - #VOICE_PARAM_ECHO_PATH_DELAY
    - #VOICE_PARAM_FV5ECNS
    - #VOICE_PARAM_FP_STATE
    - #VOICE_PARAM_FV5_STATE
    - #VOICE_PARAM_FV5_SPF_COEFF
*/
#define VOICE_MODULE_FV5ECNS_SM            (0x00010F09)

/** Module that identifies the Wide Voice algorithm in the voice decoder
    postprocessor.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_WV
*/
#define VOICE_MODULE_WV                    (0x00010EE2)

/** Module that identifies the Wide Voice v2 algorithm in the voice decoder
    postprocessor.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_WV_V2
*/
#define VOICE_MODULE_WV_V2                 (0x00010F1A)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies the Slow Talk algorithm in the voice decoder
    postprocessor.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_ST
    - #VOICE_PARAM_ST_EXP @newpage
*/
#define VOICE_MODULE_ST                    (0x00010EE3)

/** Module that identifies the AVC/RVE algorithm in the voice preprocessor on
    the Tx and Rx paths.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_AVC_RVE_ENABLE
    - #VOICE_PARAM_AVC_RVE
    - #VOICE_PARAM_AVC_RVE_V2
*/
#define VOICE_MODULE_RX_AVCRVE             (0x00010EE4)

/** Module that identifies the AVC/RVE algorithm that is part of voice
    postprocessing on the Tx path. This module is the Tx counterpart of
    VOICE_MODULE_RX_AVCRVE on the Rx path.

    This module does not support calibration data. The VPM ensures that
    VOICE_MODULE_RX_AVCRVE calibration is applied to this module.
*/
#define VOICE_MODULE_TX_AVCRVE             (0x00010F1B)

/** Module that identifies the PBE algorithm in the voice processor on the
    Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_PBE
*/
#define VOICE_MODULE_PBE                   (0x00010EE5)

/** Module that identifies the Multiband Dynamic Range Control (MDRC)
    algorithm in the voice processor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_MBDRC
*/
#define VOICE_MODULE_MBDRC                 (0x00010EE6)

/** Identifies the multiband Dynamic Range Control (MBDRC) algorithm in the
    voice processor on the Rx path. This module supports the following
    features:
    - Calibration for up to five bands
    - Sampling rates NB, WB, SWB, FB
    - Higher order FIR filter for SWB and FB sampling rates

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_MBDRC_V2
    - #VOICE_PARAM_MBDRC_FILTER_XOVER_FREQ
*/
#define VOICE_MODULE_MBDRC_V2              (0x00010F23)

/** Module that identifies the IIR algorithm in the voice processor on the
    Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_TX_IIR                (0x00010EE7)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies the IIR algorithm in the voice processor on the
    Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_RX_IIR                (0x00010EE8)

/** Module that identifies the FIR algorithm in the voice processor on the
    Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FIR
 */
#define VOICE_MODULE_TX_FIR                (0x00010EE9)

/** Module that identifies the FIR algorithm in the voice processor on the
    Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FIR
*/
#define VOICE_MODULE_RX_FIR                (0x00010EEA)

/** Module that identifies the FNS algorithm in the voice postprocessor on the
    Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_FNS
*/
#define VOICE_MODULE_FNS                   (0x00010EEB)

/** Module that identifies the DTMF detection algorithm in the voice
    postprocessor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_DTMF_DETECTION
*/
#define VOICE_MODULE_DTMF_DETECTION        (0x00010EEC)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies the DTMF detection algorithm in the voice
    postprocessor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_DTMF_DETECTION
*/
#define VOICE_MODULE_TX_DTMF_DETECTION     (0x00010F01)

/** Module that identifies the echo canceller Rx algorithm in the voice
    postprocessor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_ECRX
*/
#define VOICE_MODULE_RX_EC                 (0x00010EED)

/** Module that identifies the IIR algorithm in the voice processor on the
    Tx mic1 input path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_IIR_MIC1              (0x00010EF0)

/** Module that identifies the IIR algorithm in the voice processor on the
    Tx mic2 input path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_IIR_MIC2              (0x00010EF1)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies the IIR algorithm in the voice processor on the
    Tx mic3 input path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_IIR_MIC3              (0x00010F03)

/** Module that identifies the IIR algorithm in the voice processor on the
    Tx mic4 input path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR @newpage
*/
#define VOICE_MODULE_IIR_MIC4              (0x00010F04)

/** Module that identifies the DRC algorithm in the voice
    preprocessor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_DRC
*/
#define VOICE_MODULE_RX_DRC                (0x00010EF2)

/** Module that identifies the DRC algorithm in the voice
    preprocessor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_DRC
*/
#define VOICE_MODULE_TX_DRC                (0x00010EF3)
/** @endcond */

/** Module that identifies the Volume Control algorithm in the voice
    preprocessor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_VOL
    - #VOICE_PARAM_SOFT_VOL
*/
#define VOICE_MODULE_TX_VOL                (0x00010EF4)

/** Module that identifies the Volume Control algorithm in the voice
    preprocessor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_VOL
    - #VOICE_PARAM_SOFT_VOL
*/
#define VOICE_MODULE_RX_VOL                (0x00010EF5)

/** Module that identifies supported generic parameters that are not tied to
    a specific algorithm module in the voice preprocessor on the Tx path.
    This module ID is also used as the default identifier for the host PCM.

  @if OEM_only
    This module supports the following parameter IDs:
    - #VOICE_PARAM_VP3
    - #VOICE_PARAM_VP3_SIZE (read only)
  @endif
*/
#define VOICEPROC_MODULE_TX          (0x00010EF6)

/** Module that identifies supported generic parameters that are not tied to
    a specific algorithm module in the voice preprocessor on the Rx path.
    This module ID is also used as the default identifier for the host PCM.
    @if OEM_only @newpage @endif
*/
#define VOICEPROC_MODULE_RX          (0x00010F06)

/** Module that identifies supported generic parameters that are not tied to
    a specific algorithm module on the voice Tx stream.
*/
#define VOICESTREAM_MODULE_TX              (0x00010F07)

/** Module that identifies supported generic parameters that are not tied to
    an algorithm module in the voice Rx stream.
*/
#define VOICESTREAM_MODULE_RX              (0x00010F14)

/** Module that identifies voice mic gain in the voice preprocessor on
    the Tx path.

  @if OEM_only
    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_GAIN
  @endif
  @if ISV_only
    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
  @endif
*/
#define VOICE_MODULE_TX_MIC_GAIN           (0x00010EF7)

/** Module that identifies voice encoder gain in the voice preprocessor on
    the Tx path.

  @if OEM_only
    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_GAIN
  @endif
  @if ISV_only
    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
  @endif
*/
#define VOICE_MODULE_TX_ENC_GAIN           (0x00010EF8)

/** Module that identifies voice speaker gain in the voice preprocessor on
    the Tx path.

  @if OEM_only
    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_GAIN
  @endif
  @if ISV_only
    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
  @endif
*/
#define VOICE_MODULE_RX_SPKR_GAIN          (0x00010EF9)

/** Module that identifies voice decoder gain in the voice preprocessor on
    the Rx path.

    @if OEM_only
    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_GAIN @newpage
  @endif
  @if ISV_only
    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
  @endif
*/
#define VOICE_MODULE_RX_DEC_GAIN           (0x00010EFA)

/** @cond OEM_only */
/** Module that identifies the HPF 12 IIR algorithm in the voice processor on
    the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_TX_HPF                (0x00010F11)

/** Module that identifies the Fixed Elliptical Filter in the voice
    preprocessor on the Tx path.

    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
*/
#define VOICE_MODULE_TX_ELLIPTICAL_FILTER  (0x00010EFD)

/** Module that identifies the Fixed Slope Filter in the voice preprocessor on
    the Tx path.

    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
*/
#define VOICE_MODULE_TX_SLOPE_FILTER       (0x00010EFE)

/** Module that identifies the HPF 12 IIR algorithm in the voice processor on
    the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_IIR
*/
#define VOICE_MODULE_RX_HPF                (0x00010F12)

/** Module that identifies the Adaptive Input Gain control algorithm in the
    voice preprocessor on the Rx path.
    preprocessor on the Rx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_AIG
*/
#define VOICE_MODULE_RX_AIG                (0x00010EFF)
/** @endcond */

/** @cond OEM_only */
/** Module that identifies the Adaptive Input Gain control algorithm in the
    voice preprocessor on the Tx path.
    preprocessor on the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_AIG
*/
#define VOICE_MODULE_TX_AIG                (0x00010F00)

/** Module that identifies dual-mic Voice Plus ECNS in the voice processor on
    the Tx path.

    This module supports the following parameter IDs:
    - #VOICE_PARAM_MOD_ENABLE
    - #VOICE_PARAM_VPECNS
*/
#define VOICE_MODULE_VPECNS                (0x00010F16)

/** Module that identifies the Blind Bandwidth extension for eAMR and AMR-NB
    vocoders on the Rx path.

    This module supports the following parameter ID:
    - #VOICE_PARAM_MOD_ENABLE
*/
#define VOICE_MODULE_BEAMR                    (0x00010F19)
/** @endcond */
/** @} */ /* end_addtogroup cal_module_ids */

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_SM_ECNS_1
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_sm_ecns_1.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_sm_ecns_1_get_static_properties()
    - Initialize function name:
      capi_v2_custom_sm_ecns_1_init()
*/
#define VOICE_MODULE_CUSTOM_SM_ECNS_1         (0x10027050)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_SM_ECNS_2
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_sm_ecns_2.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_sm_ecns_2_get_static_properties
    - Initialize function name:
      capi_v2_custom_sm_ecns_2_init @newpage
*/
#define VOICE_MODULE_CUSTOM_SM_ECNS_2         (0x10027051)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_SM_ECNS_3
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_sm_ecns_3.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_sm_ecns_3_get_static_properties()
    - Initialize function name:
      capi_v2_custom_sm_ecns_3_init()
*/
#define VOICE_MODULE_CUSTOM_SM_ECNS_3         (0x10027052)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_DM_ECNS_1
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.
    
    @par Shared object name
    custom_dm_ecns_1.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_dm_ecns_1_get_static_properties()
    - Initialize function name:
      capi_v2_custom_dm_ecns_1_init() @newpage
*/
#define VOICE_MODULE_CUSTOM_DM_ECNS_1         (0x10027053)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_DM_ECNS_2
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_dm_ecns_2.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_dm_ecns_2_get_static_properties()
    - Initialize function name:
      capi_v2_custom_dm_ecns_2_init()
*/
#define VOICE_MODULE_CUSTOM_DM_ECNS_2         (0x10027054)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_DM_ECNS_3
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.
    
    @par Shared object name
    custom_dm_ecns_3.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_dm_ecns_3_get_static_properties()
    - Initialize function name:
      capi_v2_custom_dm_ecns_3_init() @newpage
*/
#define VOICE_MODULE_CUSTOM_DM_ECNS_3         (0x10027055)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_QM_ECNS_1
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.
    
    @par Shared object name
    custom_qm_ecns_1.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_qm_ecns_1_get_static_properties()
    - Initialize function name:
      capi_v2_custom_qm_ecns_1_init()
*/
#define VOICE_MODULE_CUSTOM_QM_ECNS_1         (0x10027056)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_QM_ECNS_2
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_qm_ecns_2.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_qm_ecns_2_get_static_properties()
    - Initialize function name:
      capi_v2_custom_qm_ecns_2_init() @newpage
*/
#define VOICE_MODULE_CUSTOM_QM_ECNS_2         (0x10027057)

/** Custom ECNS module ID that corresponds to the VPM_TX_CUSTOM_QM_ECNS_3
    topology. This module supports custom parameter IDs.

    The shared object name and entry functions are predefined, and they
    must be used for implementation.

    @par Shared object name
    custom_qm_ecns_3.so.1

    @par Shared entry function names
    - Get static properties function name:
      capi_v2_custom_qm_ecns_3_get_static_properties()
    - Initialize function name:
      capi_v2_custom_qm_ecns_3_init()
*/
#define VOICE_MODULE_CUSTOM_QM_ECNS_3         (0x10027058)

/** @cond OEM_only */
/** @ingroup cal_module_ids
    Identifies the gain of voice downlink path processing.

    This module supports the following parameter ID:
    - #VOICE_PARAM_GAIN
*/
#define VOICE_MODULE_RX_GAIN                  (0x00010F20)
/** @endcond */
#ifdef LVVE/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
/* LVVEFQ Tx Parameter ID */
#define VOICE_PARAM_LVVEFQ_TX_CONTROL                (0x1000B000)    // 268480512
#define VOICE_PARAM_LVVEFQ_TX_VOL                    (0x1000B001)    // 268480513
#define VOICE_PARAM_LVVEFQ_TX_HPF                    (0x1000B002)    // 268480514
#define VOICE_PARAM_LVVEFQ_TX_LPF                    (0x1000B003)    // 268480515
#define VOICE_PARAM_LVVEFQ_TX_LVHF                   (0x1000B004)    // 268480516
#define VOICE_PARAM_LVVEFQ_TX_LVNV                   (0x1000B005)    // 268480517
#define VOICE_PARAM_LVVEFQ_TX_LVWM                   (0x1000B006)    // 268480518
#define VOICE_PARAM_LVVEFQ_TX_LVEQ                   (0x1000B007)    // 268480519
#define VOICE_PARAM_LVVEFQ_TX_LVDRC                  (0x1000B009)    // 268480521
#define VOICE_PARAM_LVVEFQ_TX_LVCNG                  (0x1000B00A)    // 268480522

/* LVVEFQ Rx Parameter ID */
#define VOICE_PARAM_LVVEFQ_RX_CONTROL                (0x1000B100)    // 268480768
#define VOICE_PARAM_LVVEFQ_RX_VOL                    (0x1000B101)    // 268480769
#define VOICE_PARAM_LVVEFQ_RX_LVFENS                 (0x1000B102)    // 268480770
#define VOICE_PARAM_LVVEFQ_RX_LVWBE                  (0x1000B10D)    // 268480781
#define VOICE_PARAM_LVVEFQ_RX_LVNLPP                 (0x1000B104)    // 268480772
#define VOICE_PARAM_LVVEFQ_RX_LVAVC                  (0x1000B10E)    // 268480782
#define VOICE_PARAM_LVVEFQ_RX_LVEQ                   (0x1000B106)    // 268480774
#define VOICE_PARAM_LVVEFQ_RX_LVDRC                  (0x1000B107)    // 268480775
#define VOICE_PARAM_LVVEFQ_RX_HPF                    (0x1000B108)    // 268480776
#define VOICE_PARAM_LVVEFQ_RX_LVCNG                  (0x1000B109)    // 268480777
#define VOICE_PARAM_LVVEFQ_RX_LVWM                   (0x1000B10A)    // 268480778
#define VOICE_PARAM_LVVEFQ_RX_LVNG                   (0x1000B10B)    // 268480779
#define VOICE_PARAM_LVVEFQ_RX_LVNF                   (0x1000B10C)    // 268480780

/* parameter definition for misc. */
/* Tx Rx message */
#define VOICE_PARAM_LVVEFQ_MESSAGE                   (0x1000B300)    // 268481280

/* LVVEFQ Tx Module ID */
#define VOICE_MODULE_LVVEFQ_TX                       (0x1000B500)    // 268481792

/* LVVEFQ Rx Module ID */
#define VOICE_MODULE_LVVEFQ_RX                       (0x1000B501)    // 268481793
#define VOICE_MODULE_LVVEFQ_TX_INJECTION       (0x1000B502)     // 268481794
#define VOICE_MODULE_LVVEFQ_RX_INJECTION       (0x1000B503)     // 268481795
#define VOICE_MODULE_LVVEFQ_RX_COMMON          (0x1000B504)     // 268481796
#define VOICE_MODULE_LVVEFQ_IMC          (0x1000B505)     // 268481797

#define VOICE_PARAM_LVVEFQ_INJECTION_IN         (0x1000B301)  // 268481281
#define VOICE_PARAM_LVVEFQ_INJECTION_OUT        (0x1000B302)  // 268481282

#endif // LVVE/*TCT-NB Tianhongwei end 2016/01/04*/
/** @} */ /* end_addtogroup cal_module_ids */

#endif /* __ADSP_VPARAMS_API_H__ */
