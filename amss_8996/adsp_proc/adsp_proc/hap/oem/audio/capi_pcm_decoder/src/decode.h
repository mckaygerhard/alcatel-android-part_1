#ifndef DECODE_H
#define DECODE_H
/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C extern
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ( ( void * ) 0 )
#endif
#endif

#include "mmdefs.h"
#include "adsp_error_codes.h"

//error codes
#define PT_NO_ERROR 0
#define PT_NOT_ENOUGH_DATA 1
#define PT_INCORRECT_TYPE 2

#define DECODE_SUCCESS                     0
#define DECODE_FAILURE					   1
#define SAMPLE_BUFFERTOOSMALL_FAILURE      2
#define SAMPLE_NEED_MORE		           3
#define PCM_FORMAT_MAX_NUM_CHANNEL  	   8

#define __DEINTERLEAVE_MULTICH__(inBuf, outBuf, numChannels, numSamplesToDeInterleave) \
{                                                                   \
    uint32_t i = 0;                                                 \
    uint32_t j = 0;                                                 \
                                                                    \
    if(!inBuf || !outBuf || !numChannels)                           \
    {                                                               \
        return DECODE_FAILURE;                                      \
    }                                                               \
                                                                    \
    for(i = 0; i < numChannels; i++)                                \
    {                                                               \
        if(!outBuf[i])                                              \
        {                                                           \
            return DECODE_FAILURE;                                  \
        }                                                           \
    }                                                               \
                                                                    \
    for(i = 0; i < numSamplesToDeInterleave; i++)                   \
    {                                                               \
        for(j = 0; j < numChannels; j++)                            \
        {                                                           \
            outBuf[j][i] = inBuf[(numChannels * i) + j];            \
        }                                                           \
    }                                                               \
                                                                    \
    return DECODE_SUCCESS;                                                \
}

typedef struct decode_t decode_t;
struct decode_t {
  //Library specific parameters
  int16_t m_nNoOfChannels; /* number of channels */
};

ADSPResult DeInterleave_Multichannel_16(int16_t* inBuf,
                                        int16_t* outBuf[],
                                        uint32_t numChannels,
                                        uint32_t numSamplesToDeInterleave);

ADSPResult DeInterleave_Multichannel_32(int32_t* inBuf,
                                        int32_t* outBuf[],
                                        uint32_t numChannels,
                                        uint32_t numSamplesToDeInterleave);

void Init(int16_t nNoOfChannels);

/*==========================================================================  */
/* FUNCTION: DecoderFrame                                                     */
/*                                                                            */
/* DESCRIPTION: Processes the PCM data for Mono or Stereo File. Simple copy   */
/*                                                                            */
/* INPUTS: samples_to_copy: This indicates the number of samples to be copied */
/* 	       num_channels: Number of Channels(Supports mono and stereo 		  */
/*         bytes_per_sample_in: This indicates the number of bytes from the   */
/*         PCM input buffer to be decoded.                                    */
/*	       bytes_per_sample_out: This indicates the number of bytes per sample*/
/*         for output file.                                                   */
/*         nInpSize: This indicates the size of input frame.	              */
/*         nOutputSize: This is an ouput parameter to indicate how many bytes */
/*	       of input is decoded successfully.	              				  */
/*         src_ptr: This points to the input buffer.   				          */
/*         dest_ptr: This points to the output buffer.					      */
/* OUTPUTS: return DECODE_SUCCESS if Pcm data copied from input buffer to     */
/*          output buffer.		                                              */
/*============================================================================*/
ADSPResult DecodeFrame(uint32_t samples_to_copy,
                       uint32_t num_channels,
                       uint32_t bytes_per_sample_in,
                       uint32_t bytes_per_sample_out,
                       uint32_t nInpSize,
                       uint32_t* nOutputSize,
                       int8_t* src_ptr,
                       int8_t* dest_ptr
                       );

#endif /*#ifndef DECODE_H */

