# *************************************************
# Copyright (c) 2016  ARM Ltd (or its subsidiaries)
# *************************************************
import glob
import logging
import os
import re
import shutil
import subprocess
import stat

logger = logging.getLogger('ARM-TA-QC-TREE-HELPER')


def find_tz_root(base_path):
    if os.path.basename(os.path.abspath(base_path)) == 'trustzone_images':
        logger.debug('package root found: ' + base_path)
        return base_path

    for (path, dirs, _) in os.walk(base_path):
        if 'trustzone_images' in dirs:
            logger.debug('package root found: ' + path)
            return os.path.join(path, 'trustzone_images')
    raise Exception('invalid package structure!!!')


def is_arm_ta_integrated(pkg_root):
    pattern = re.compile(r'sa?mple?ap')
    for (path, _, files) in os.walk(os.path.join(pkg_root, 'core/securemsm/trustzone/qsapps/')):
        if pattern.search(path) and 'SConscript' in files:
            scons_file = os.path.join(path, 'SConscript')
            logger.debug('sample application source found: ' + scons_file)
            with open(scons_file, 'rt') as fh:
                source = fh.read()
                if 'ARM-TA-PATCH APPLIED' in source:
                    logger.info('ARM-TA Patch is already applied')
                    return True
    return False


def copy_files_to_tree(arm_ta_dir, files):
    if os.path.isdir(arm_ta_dir):
        shutil.rmtree(arm_ta_dir)
    os.makedirs(arm_ta_dir)
    for source_file in files:
        logger.debug('copy %s -> %s', source_file, arm_ta_dir)
        shutil.copy(source_file, arm_ta_dir)
        (root, ext) = os.path.splitext(source_file)
        if ext == '.c':
            header_file = root + '.h'
            if os.path.isfile(header_file):
                logger.debug('copy %s -> %s', header_file, arm_ta_dir)
                shutil.copy(header_file, arm_ta_dir)


def get_patch_file_strip_number(patch_file):
    with open(patch_file, 'rt') as fh:
        patch_source = fh.read()
    match = re.search(r'^--- (\S+)', patch_source, re.MULTILINE)
    if not match:
        raise Exception('malformed patch file')
    path_list = match.group(1).split('/')
    index = path_list.index('trustzone_images') + 1
    return index


def apply_arm_ta_patch(pkg_root, patch_file, is_reverse=False):
    patch_verbosity = []
    if logger.isEnabledFor(logging.DEBUG):
        patch_verbosity = ['--verbose']
    elif not logger.isEnabledFor(logging.INFO):
        patch_verbosity = ['--quiet']

    command = 'applying' if not is_reverse else 'removing'
    logger.info('%s ARM-TA patch: %s', command, os.path.basename(patch_file))

    strip_num = get_patch_file_strip_number(patch_file)
    logger.debug('patch file relative strip is %d', strip_num)

    extra_arg = [] if not is_reverse else ['-R']
    subprocess.check_call(
        [
            'patch',
            '--ignore-whitespace',
            '--no-backup-if-mismatch',
            '--strip=' + str(strip_num),
            '--input=' + patch_file
        ] + extra_arg + patch_verbosity,
        cwd=pkg_root
    )


def replace_envsetup(orig, custom):
    if not os.path.isfile(orig):
        raise Exception(orig + ' file not found')

    backup_file = orig + '.bak'
    if not os.path.isfile(backup_file):
        shutil.move(orig, backup_file)
        logger.debug('created backup file: ' + backup_file)

    shutil.copyfile(custom, orig)

    # chmod +x envsetup.sh
    os.chmod(orig, os.stat(orig).st_mode | stat.S_IEXEC)

    logger.debug('replaced envsetup.sh with ' + custom)


def glob_re(path, pattern, flags=0):
    pattern = re.compile(pattern, flags)
    return [os.path.join(path, f) for f in os.listdir(path) if pattern.search(f)]


def collect_artifacts(pkg_root, out_dir, build_id, sample_app_target_name, ta_name):
    bxx_artifacts = glob_re(
        os.path.join(pkg_root, 'build/ms/bin/PIL_IMAGES/SPLITBINS_' + build_id),
        sample_app_target_name + r'\.((mdt)|(b\d+))$',
        re.IGNORECASE)
    if not bxx_artifacts:
        raise Exception(
            'Could not find ANY *.mdt or *.bxx files under ' +
            os.path.join(pkg_root, 'build/ms/bin/PIL_IMAGES/SPLITBINS_' + build_id)
        )

    elf_artifacts = []
    elf_dir = os.path.join(pkg_root, 'core/bsp/trustzone/qsapps/*/build/' + build_id)
    for x in ['.mbn', '.map', '.elf']:

        file_name = os.path.join(elf_dir, sample_app_target_name + x)
        found_files = glob.glob(file_name)
        if not found_files or len(found_files) > 1:
            raise Exception('Could not find ' + file_name)
        elf_artifacts.append(found_files[0])

    for artifact in bxx_artifacts + elf_artifacts:
        # rename files keep file extensions
        file_extension = os.path.splitext(artifact)[1]
        target_file = os.path.join(out_dir, ta_name + file_extension)
        logger.debug(
            'copy {src} -> {dst}'.format(
                src=artifact.replace(pkg_root + '/', ''),
                dst=target_file
            )
        )
        shutil.copyfile(artifact, target_file)


def restore(pkg_root, patch_file, integrated):
    arm_ta_dir = os.path.join(pkg_root, 'arm-ta')
    if os.path.isfile(arm_ta_dir):
        logger.debug('deleting %s', arm_ta_dir)
        shutil.rmtree(arm_ta_dir)
    if integrated:
        apply_arm_ta_patch(pkg_root=pkg_root, patch_file=patch_file, is_reverse=True)

    setenv_file = os.path.join(pkg_root, 'build', 'ms', 'setenv.sh')
    if os.path.isfile(setenv_file + '.bak'):
        logger.debug('restoring %s', setenv_file)
        shutil.move(setenv_file + '.bak', setenv_file)