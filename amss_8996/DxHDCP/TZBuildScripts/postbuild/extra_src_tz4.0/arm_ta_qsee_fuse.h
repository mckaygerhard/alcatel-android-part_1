#ifndef ARM_TA_QSEE_FUSE_H
#define ARM_TA_QSEE_FUSE_H

/**
@file qsee_fuse.h
@brief Provide HW & SW Fuse functionality
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/**
 * Enumeration for the SW fues TZ supports
 */
typedef enum
{
  DX_QSEE_HLOS_IMG_TAMPER_FUSE = 0, /**< Used during boot to determine
                                        if the HLOS image has successfully
                                        been authenticated. */
  DX_QSEE_WINSECAPP_LOADED_FUSE = 1, /**< Used by WinSec App to prevent
                                        reloading. */
  DX_QSEE_UEFISECAPP_LOADED_FUSE = 2, /**< Used by UefiSecApp to prevent
                                        reloading. */


  DX_QSEE_FUSES_INVALID        = 0x7FFFFFFF,

} dx_qsee_sw_fuse_t;

/**
 * @brief      Query whether the given SW fuse has been blown
 *
 * @param[in]  fuse_num    the SW fuse to query
 * @param[out] is_blown    whether the given fuse has been blown
 * @param[in]  is_blown_sz size of the return pointer
 *
 * @return     0 on success, negative on failure
 * @warning    function is not thread-safe
 */
int dx_qsee_is_sw_fuse_blown
(
  dx_qsee_sw_fuse_t    fuse_num,
  bool*          is_blown,
  uint32_t            is_blown_sz
);


#endif /* ARM_TA_QSEE_FUSE_H */

