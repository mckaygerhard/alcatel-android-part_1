
#include <stddef.h>
#include <stdint.h>


#include "qsee_cppf.h"
#include "arm_ta_qsee_cppf.h"



CPPFErrNoType dx_qsee_set_hdcp_min_enc_level(MinEnLevelType min_enc_level)
{
    return qsee_set_hdcp_min_enc_level(min_enc_level);
}


CPPFErrNoType dx_qsee_get_hdcp_min_enc_level(MinEnLevelType *min_enc_level)
{
    return qsee_get_hdcp_min_enc_level((uint8_t *)min_enc_level);
}


