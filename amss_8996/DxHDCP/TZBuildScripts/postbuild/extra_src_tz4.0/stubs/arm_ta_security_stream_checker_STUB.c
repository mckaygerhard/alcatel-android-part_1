#include <comdef.h>

#define IN
#define OUT
#define INOUT
#define TZAPP_SCHK_R_OK						0x0000

uint32_t security_stream_checker_init( IN	uint32_t caller_application,
									 IN	uint32_t crypto_mode
								   )
{
	return TZAPP_SCHK_R_OK;
}

uint32_t security_stream_checker( IN	uint32_t			caller_application,
								IN	uint32_t			crypto_mode,
								IN	uint32_t			stream_type,
								IN	uint8_t			*data,
								IN	uint32_t			data_length,
								IN	uint8_t			*vector1,	/*  64bit for AES CTR. 128bit for AES CBC */
								IN	uint8_t			*vector2	/* 64bit for AES CTR. NULL for AES CBC 	  */
							  )
{
	return TZAPP_SCHK_R_OK;
}

