#ifndef ARM_TA_QSEEHEAP_H
#define ARM_TA_QSEEHEAP_H

/**
@file qsee_heap.h
@brief Provide heap management API wrappers
*/


/*===========================================================================

DESCRIPTION
  Wrapper for secure malloc and free calls for secure heap.

===========================================================================*/



#include <stdint.h>

/* ------------------------------------------------------------------------
** Functions
** ------------------------------------------------------------------------ */

#ifdef __cplusplus
   extern "C"
   {
#endif
/**
 * @brief   Allocates a block of size bytes from the heap.  If heap_ptr is NULL or size is 0, the NULL pointer will be silently returned.
 *
 * @param[in] size Number of bytes to allocate from the heap. 
 *
 * @return   Returns a pointer to the newly allocated block, or NULL if the block could not be allocated.
 *
 */
void* dx_qsee_malloc(size_t size);

/**
 * @brief  Deallocates the ptr block of memory. 
 *
 * @param[in] ptr pointer to block which will be freed 
 *
 */
void dx_qsee_free(void *ptr);

#ifdef __cplusplus
   }
#endif

#endif /* ARM_TA_QSEEHEAP_H */

