#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_timer.h"
#include "arm_ta_qsee_timer.h"

unsigned long long dx_qsee_get_uptime(void)
{
	return qsee_get_uptime();
}



