#ifndef DX_QSEE_HDCP2P_H
#define DX_QSEE_HDCP2P_H

#include <stddef.h> // size_t
#include <stdint.h> // int32_t
#include <stdbool.h> // bool


struct dx_hdcp2p_ifs_ {
    int32_t (*open)(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
    void (*close)(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
    bool (*isClosed)(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
    int32_t (*getTopology)(
        struct dx_hdcp2p_ifs_ *hdcp2p_ifs,
        size_t receiverIdList_len,
        uint8_t *receiverIdList_ptr,
        size_t *receiverIdList_lenout,
        uint32_t *depth,
        uint32_t *deviceCount,
        uint32_t *maxDevicesExceeded,
        uint32_t *maxCascadeExceeded,
        uint32_t *hdcp2LegacyDeviceDownstream,
        uint32_t *hdcp1DeviceDownstream
        );
};


int32_t dx_qsee_hdcp2p_init(struct dx_hdcp2p_ifs_ **hdcp2p_ifs);

void dx_qsee_hdcp2p_finish(struct dx_hdcp2p_ifs_ **hdcp2p_ifs);

#endif // DX_QSEE_HDCP2P_H