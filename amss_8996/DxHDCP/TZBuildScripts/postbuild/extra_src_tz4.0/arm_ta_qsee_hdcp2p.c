#include "arm_ta_qsee_hdcp2p.h"
#include "arm_ta_qsee_env.h"
#include "arm_ta_qsee_heap.h"
#include "arm_ta_qsee_log.h"

typedef struct dx_hdcp2p_ifs_ dx_hdcp2p_ifs;

static int32_t _dx_qsee_hdcp2p_open(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
static void _dx_qsee_hdcp2p_close(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
static bool _dx_qsee_hdcp2p_is_closed(struct dx_hdcp2p_ifs_ *hdcp2p_ifs);
static int32_t _dx_qsee_hdcp2p_get_topology(
struct dx_hdcp2p_ifs_ *hdcp2p_ifs,
    size_t receiverIdList_len,
    uint8_t *receiverIdList_ptr,
    size_t *receiverIdList_lenout,
    uint32_t *depth,
    uint32_t *deviceCount,
    uint32_t *maxDevicesExceeded,
    uint32_t *maxCascadeExceeded,
    uint32_t *hdcp2LegacyDeviceDownstream,
    uint32_t *hdcp1DeviceDownstream
    );

#if defined(DX_QSEE_TOPOLOGY)

#include "CHdcp2p2Service.h"
#include "IHdcp2p2Service.h"

// Compute the offset of a field within its defining structure.
#define DX_QSEE_HDCP2P2_OFFSETOF(Field_,Struct_) \
    ((char*)(&(((Struct_*)0)->Field_))-(char*)0)

// Convert a pointer to a field into a pointer to the containing structure.
#define DX_QSEE_HDCP2P2_CONTAINER(Field_,Struct_,Ptr_) \
    (Struct_*)(((char*)Ptr_) - DX_QSEE_HDCP2P2_OFFSETOF(Field_,Struct_))

#define DX_QSEE_HDCP2P_MAGIC 0x5b2c750cL

typedef struct
{
    uint32_t magic;
    dx_hdcp2p_ifs ifs;
    Object qcIpcObj;
} dx_qsee_hdcp2p;

int32_t dx_qsee_hdcp2p_init(struct dx_hdcp2p_ifs_ **hdcp2p_ifs)
{
    if (!hdcp2p_ifs) {
        return 1;
    }

    dx_qsee_hdcp2p *self = (dx_qsee_hdcp2p *)dx_qsee_malloc(sizeof(*self));
    if (!self) {
        return 1;
    }

    self->magic = DX_QSEE_HDCP2P_MAGIC;
    self->ifs.open = _dx_qsee_hdcp2p_open;
    self->ifs.close = _dx_qsee_hdcp2p_close;
    self->ifs.isClosed = _dx_qsee_hdcp2p_is_closed;
    self->ifs.getTopology = _dx_qsee_hdcp2p_get_topology;
    self->qcIpcObj.context = NULL;
    self->qcIpcObj.invoke = NULL;

    *hdcp2p_ifs = &self->ifs;
    return 0;
}

void dx_qsee_hdcp2p_finish(struct dx_hdcp2p_ifs_ **hdcp2p_ifs)
{
    if (hdcp2p_ifs && *hdcp2p_ifs) {
        dx_qsee_hdcp2p *self = DX_QSEE_HDCP2P2_CONTAINER(ifs, dx_qsee_hdcp2p, *hdcp2p_ifs);

        if (DX_QSEE_HDCP2P_MAGIC != self->magic) {
            dx_qsee_log(DX_QSEE_LOG_MSG_ERROR, "dx_qsee_hdcp2p_finish: ERROR!!! Invalid magic");
            return;
        }

        self->ifs.close(&self->ifs);
        dx_qsee_free(self);
        *hdcp2p_ifs = NULL;
    }
}

static int32_t _dx_qsee_hdcp2p_open(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    if (!hdcp2p_ifs) {
        return 1;
    }

    dx_qsee_hdcp2p *self = DX_QSEE_HDCP2P2_CONTAINER(ifs, dx_qsee_hdcp2p, hdcp2p_ifs);

    if (DX_QSEE_HDCP2P_MAGIC != self->magic) {
        dx_qsee_log(DX_QSEE_LOG_MSG_ERROR, "_dx_qsee_hdcp2p_open: ERROR!!! Invalid magic");
        return 1;
    }

    return dx_qsee_open(CHdcp2p2Service_UID, &self->qcIpcObj);
}

static void _dx_qsee_hdcp2p_close(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    if (hdcp2p_ifs) {
        dx_qsee_hdcp2p *self = DX_QSEE_HDCP2P2_CONTAINER(ifs, dx_qsee_hdcp2p, hdcp2p_ifs);

        if (DX_QSEE_HDCP2P_MAGIC != self->magic) {
            dx_qsee_log(DX_QSEE_LOG_MSG_ERROR, "_dx_qsee_hdcp2p_close: ERROR!!! Invalid magic");
            return;
        }

        if (!Object_isNull(self->qcIpcObj)) {
            IHdcp2p2Service_release(self->qcIpcObj);
            self->qcIpcObj.invoke = NULL;
        }
    }
}

static bool _dx_qsee_hdcp2p_is_closed(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    bool ret = true;

    if (hdcp2p_ifs) {
        dx_qsee_hdcp2p *self = DX_QSEE_HDCP2P2_CONTAINER(ifs, dx_qsee_hdcp2p, hdcp2p_ifs);

        if (DX_QSEE_HDCP2P_MAGIC == self->magic) {
            ret = Object_isNull(self->qcIpcObj);
        }
        else {
            dx_qsee_log(DX_QSEE_LOG_MSG_ERROR, "_dx_qsee_hdcp2p_is_closed: ERROR!!! Invalid magic");
        }
    }

    return ret;
}

static int32_t _dx_qsee_hdcp2p_get_topology(
    struct dx_hdcp2p_ifs_ *hdcp2p_ifs,
    size_t receiverIdList_len,
    uint8_t *receiverIdList_ptr,
    size_t *receiverIdList_lenout,
    uint32_t *depth,
    uint32_t *deviceCount,
    uint32_t *maxDevicesExceeded,
    uint32_t *maxCascadeExceeded,
    uint32_t *hdcp2LegacyDeviceDownstream,
    uint32_t *hdcp1DeviceDownstream
    )
{
    if (!hdcp2p_ifs || !receiverIdList_ptr || !receiverIdList_lenout ||
        !depth || !deviceCount || !maxDevicesExceeded || !maxCascadeExceeded ||
        !hdcp2LegacyDeviceDownstream || !hdcp1DeviceDownstream) {
        return 1;
    }

    dx_qsee_hdcp2p *self = DX_QSEE_HDCP2P2_CONTAINER(ifs, dx_qsee_hdcp2p, hdcp2p_ifs);

    if (DX_QSEE_HDCP2P_MAGIC != self->magic) {
        dx_qsee_log(DX_QSEE_LOG_MSG_ERROR, "_dx_qsee_hdcp2p_get_topology: ERROR!!! Invalid magic");
        return 1;
    }

    int32_t err;
    IHdcp2p2Service_Topology topology = { 0 };

    err = IHdcp2p2Service_getTopology(
        self->qcIpcObj,
        IHdcp2p2Service_HDCP_TXMTR_HDMI,
        receiverIdList_ptr,
        receiverIdList_len,
        receiverIdList_lenout,
        &topology
        );
    if (!Object_isOK(err)) {
        return 1;
    }

    *depth = topology.depth;
    *deviceCount = topology.deviceCount;
    *maxDevicesExceeded = topology.maxDevicesExceeded;
    *maxCascadeExceeded = topology.maxCascadeExceeded;
    *hdcp2LegacyDeviceDownstream = topology.hdcp2LegacyDeviceDownstream;
    *hdcp1DeviceDownstream = topology.hdcp1DeviceDownstream;
    return 0;
}

#else

int32_t dx_qsee_hdcp2p_init(struct dx_hdcp2p_ifs_ **hdcp2p_ifs)
{
    if (!hdcp2p_ifs) {
        return 1;
    }

    *hdcp2p_ifs = (dx_hdcp2p_ifs *)dx_qsee_malloc(sizeof(dx_hdcp2p_ifs));
    if (!hdcp2p_ifs) {
        return 1;
    }

    (*hdcp2p_ifs)->open = _dx_qsee_hdcp2p_open;
    (*hdcp2p_ifs)->close = _dx_qsee_hdcp2p_close;
    (*hdcp2p_ifs)->isClosed = _dx_qsee_hdcp2p_is_closed;
    (*hdcp2p_ifs)->getTopology = _dx_qsee_hdcp2p_get_topology;

    return 0;
}

void dx_qsee_hdcp2p_finish(struct dx_hdcp2p_ifs_ **hdcp2p_ifs)
{
    if (hdcp2p_ifs && *hdcp2p_ifs) {
        dx_qsee_free(*hdcp2p_ifs);
        *hdcp2p_ifs = NULL;
    }
}

static int32_t _dx_qsee_hdcp2p_open(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    (void)hdcp2p_ifs;
    return 123;
}

static void _dx_qsee_hdcp2p_close(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    (void)hdcp2p_ifs;
}

static bool _dx_qsee_hdcp2p_is_closed(struct dx_hdcp2p_ifs_ *hdcp2p_ifs)
{
    (void)hdcp2p_ifs;

    return true;
}

static int32_t _dx_qsee_hdcp2p_get_topology(
    struct dx_hdcp2p_ifs_ *hdcp2p_ifs,
    size_t receiverIdList_len,
    uint8_t *receiverIdList_ptr,
    size_t *receiverIdList_lenout,
    uint32_t *depth,
    uint32_t *deviceCount,
    uint32_t *maxDevicesExceeded,
    uint32_t *maxCascadeExceeded,
    uint32_t *hdcp2LegacyDeviceDownstream,
    uint32_t *hdcp1DeviceDownstream
    )
{
    (void)hdcp2p_ifs;

    if (!receiverIdList_ptr || !receiverIdList_lenout ||
        !depth || !deviceCount || !maxDevicesExceeded || !maxCascadeExceeded ||
        !hdcp2LegacyDeviceDownstream || !hdcp1DeviceDownstream) {
        return 1;
    }

    *receiverIdList_lenout = 0;
    *depth = 0;
    *deviceCount = 0;
    *maxDevicesExceeded = 0;
    *maxCascadeExceeded = 0;
    *hdcp2LegacyDeviceDownstream = 0;
    *hdcp1DeviceDownstream = 0;
    return 123;
}

#endif // DX_QSEE_TOPOLOGY
