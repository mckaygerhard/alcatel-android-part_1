
#include <stddef.h>
#include <stdint.h>

#include "qsee_log.h"
#include "qsee_hmac.h"
#include "arm_ta_qsee_hmac.h"


static int Map_DX_QSEE_HMAC_ALGO_ET(DX_QSEE_HMAC_ALGO_ET param_in, QSEE_HMAC_ALGO_ET *param_out)
{
    int res = 0;

    if (param_out == 0)
        return -1;
    switch (param_in)
    {
    case DX_QSEE_HMAC_SHA1:
        *param_out = QSEE_HMAC_SHA1;
        break;

    case DX_QSEE_HMAC_SHA256:
        *param_out = QSEE_HMAC_SHA256;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid HMAC_ALGO_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}



int dx_qsee_hmac(DX_QSEE_HMAC_ALGO_ET alg, const uint8_t *msg, uint32_t msg_len,
              const uint8_t *key, uint16_t key_len, uint8_t *msg_digest)
{

    QSEE_HMAC_ALGO_ET alg_converted = QSEE_HMAC_SHA1;

    if (0 != Map_DX_QSEE_HMAC_ALGO_ET(alg, &alg_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_HMAC_ALGO_ET failed!", __FUNCTION__);
        return -1;
    }

	return qsee_hmac(alg_converted, msg, msg_len, key, key_len, msg_digest);
}



