#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_services.h"
#include "arm_ta_qsee_services.h"


int dx_qsee_encapsulate_inter_app_message(char* dest_app_name, uint8_t* in_buf,
  uint32_t in_len, uint8_t* out_buf, uint32_t* out_len)
{
    return qsee_encapsulate_inter_app_message(dest_app_name, in_buf,
  in_len, out_buf, out_len);
}


int dx_qsee_decapsulate_inter_app_message(char* source_app_name, uint8_t* in_buf,
  uint32_t in_len, uint8_t* out_buf, uint32_t* out_len)
{
    return qsee_decapsulate_inter_app_message(source_app_name, in_buf,
  in_len, out_buf, out_len);

}


int dx_qsee_register_shared_buffer(void *address, unsigned int size)
{
    return qsee_register_shared_buffer(address, size);
}


int dx_qsee_deregister_shared_buffer(void *address)
{
    return qsee_deregister_shared_buffer(address);
}


int dx_qsee_prepare_shared_buf_for_nosecure_read(void * address, unsigned int size)
{
    return qsee_prepare_shared_buf_for_nosecure_read(address, size);
}


int dx_qsee_prepare_shared_buf_for_secure_read(void *address, unsigned int size)
{
    return qsee_prepare_shared_buf_for_secure_read(address, size);
}
