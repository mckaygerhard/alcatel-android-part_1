#ifndef ARM_TA_QSEE_RSA_H
#define ARM_TA_QSEE_RSA_H

/**
@file qsee_rsa.h
@brief Provide SECRSA API wrappers
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
typedef uint32_t DX_QSEE_BLONG;
#define DX_QSEE_MAX_KEY_SIZE          (4128)                                              ///< Maximum key size in bits
#define DX_QSEE_BLONG_SIZE            (sizeof(DX_QSEE_BLONG) )                               ///< Bytes per digit
#define DX_QSEE_BLONGS_PER_KEY        ((DX_QSEE_MAX_KEY_SIZE) + (8*DX_QSEE_BLONG_SIZE) - 1)/(8*DX_QSEE_BLONG_SIZE)  ///< Digits per key


/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

typedef struct
{
  DX_QSEE_BLONG a[DX_QSEE_BLONGS_PER_KEY];
  int n;                  
} DX_QSEE_BigInt;

/** DX_QSEE_S_BIGINT type */
typedef struct DX_QSEE_S_BIGINT_ {
  DX_QSEE_BigInt bi;
	int sign;
} DX_QSEE_S_BIGINT;


/** QSEE RSA Key Type  */
typedef enum
{
   /** public key  */
   DX_QSEE_RSA_KEY_PUBLIC  = 0,
   /** private key in non CRT representation */
   DX_QSEE_RSA_KEY_PRIVATE = 1,
   /** private key in CRT representation */
   DX_QSEE_RSA_KEY_PRIVATE_CRT = 2,
   /** private/public key pair */
   DX_QSEE_RSA_KEY_PRIVATE_PUBLIC = 3,

   DX_QSEE_RSA_KEY_INVALID = 0x7FFFFFFF

}DX_QSEE_RSA_KEY_TYPE;


/** RSA PKCS key */
typedef struct DX_QSEE_RSA_KEY
{
   /** Type of key, QSEE_RSA_PRIVATE or QSEE_RSA_PUBLIC */
   DX_QSEE_RSA_KEY_TYPE type;
   /** RSA key bit length **/
   int bitLength;
   /** The public exponent */
   DX_QSEE_S_BIGINT *e; 
   /** The private exponent */
   DX_QSEE_S_BIGINT *d; 
   /** The modulus */
   DX_QSEE_S_BIGINT *N; 
   /** The p factor of N */
   DX_QSEE_S_BIGINT *p; 
   /** The q factor of N */
   DX_QSEE_S_BIGINT *q; 
   /** The 1/q mod p CRT param */
   DX_QSEE_S_BIGINT *qP; 
   /** The d mod (p - 1) CRT param */
   DX_QSEE_S_BIGINT *dP; 
   /** The d mod (q - 1) CRT param */
   DX_QSEE_S_BIGINT *dQ;
} DX_QSEE_RSA_KEY;


/** RSA padding type. PKCS #1 v2.1*/
typedef enum
{
   /*PKCS1 v1.5 signature*/
   DX_QSEE_RSA_PAD_PKCS1_V1_5_SIG = 1, 
   /*PKCS1 v1.5 encryption*/
   DX_QSEE_RSA_PAD_PKCS1_V1_5_ENC = 2, 
   /*OAEP Encryption*/ 
   DX_QSEE_RSA_PAD_PKCS1_OAEP = 3, 
   /*PSS Signature*/
   DX_QSEE_RSA_PAD_PKCS1_PSS = 4,  
   /* No Padding */
   DX_QSEE_RSA_NO_PAD = 5,  

   DX_QSEE_RSA_INVALID = 0x7FFFFFFF,

} DX_QSEE_RSA_PADDING_TYPE;

typedef struct DX_QSEE_RSA_OAEP_PAD_INFO
{
   /** index of Hash & Mask generation function desired */
   int hashidx;
   /** Label to add to the message */
   unsigned char *label;
   /** Length of label */
   int labellen;
} DX_QSEE_RSA_OAEP_PAD_INFO;

typedef struct DX_QSEE_RSA_PSS_PAD_INFO
{
   /** index of Hash & Mask generation function desired */
   int hashidx;
   /** Length of salt */
   int saltlen;
} DX_QSEE_RSA_PSS_PAD_INFO;


/*index of hash algorithm used for generating signature */
typedef enum 
{
   DX_QSEE_HASH_IDX_NULL = 1,
   DX_QSEE_HASH_IDX_SHA1,
   DX_QSEE_HASH_IDX_SHA256,
   DX_QSEE_HASH_IDX_SHA384,
   DX_QSEE_HASH_IDX_SHA512,
   DX_QSEE_HASH_IDX_MAX,
   DX_QSEE_HASH_IDX_INVALID = 0x7FFFFFF,

}DX_QSEE_HASH_IDX;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/




/**
 * @brief  This function does PKCS #1 padding then verify 
 *         signature.
 *
 * @param[in] key          The private RSA key to use
 * @param[in] padding_type Type of padding
 * @param[in] padding_info PSS padding parameters 
 * @param[in] hashidx      The index of the hash desired
 * @param[in] hash         The hash to sign (octets)
 * @param[in] hashlen      The length of the hash to sign 
 * @param[in] sig          The signature
 * @param[in] siglen       The max size and resulting size of 
 *                         the signature
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_rsa_verify_signature
(
   DX_QSEE_RSA_KEY              *key, 
   DX_QSEE_RSA_PADDING_TYPE     padding_type, 
   void                      *padding_info,
   DX_QSEE_HASH_IDX             hashidx,
   unsigned char             *hash, 
   int                       hashlen,
   unsigned char             *sig, 
   int                       siglen
);

/**
 * @brief  This function does PKCS #1 v1.5 padding then encrypt.
 *
 * @param[in] key           The RSA key to encrypt to 
 * @param[in] padding_type  Type of padding
 * @param[in] padding_info  OAEP padding parameters 
 * @param[in] msg           The plaintext
 * @param[in] msglen        The length of the plaintext(octets)
 * @param[out] cipher       The ciphertext 
 * @param[in,out] cipherlen The max size and resulting size of 
 *                          the ciphertext
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_rsa_encrypt
(
   DX_QSEE_RSA_KEY            *key, 
   DX_QSEE_RSA_PADDING_TYPE   padding_type, 
   void                    *padding_info,
   const unsigned char     *msg, 
   int                     msglen,
   unsigned char           *cipher,
   int                     *cipherlen
);

/**
 * @brief  This function does PKCS #1 decrypt then v1.5 depad.
 *
 * @param[in] key           The corresponding private RSA key 
 * @param[in] padding_type  Type of padding
 * @param[in] padding_info  OAEP padding parameters 
 * @param[in] cipher        The ciphertext
 * @param[in] cipherlen     The length of the ciphertext(octets)
 * @param[out] msg          The plaintext 
 * @param[in,out] msglen    The max size and resulting size of 
 *                          the plaintext
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_rsa_decrypt
(
   DX_QSEE_RSA_KEY              *key,
   DX_QSEE_RSA_PADDING_TYPE     padding_type,
   void                    *padding_info,
   unsigned char           *cipher,
   int                     cipherlen,
   unsigned char           *msg,
   int                     *msglen
);

/**
 * @brief
 *  Read an unsigned buffer of bytes into a big integer
 *
 * @param a[out]   Pointer to big integer
 * @param buf[in]  Pointer to array of bytes
 * @param len[in]  Array length 
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_BIGINT_read_unsigned_bin
(
   DX_QSEE_BigInt * a, 
   const uint8_t * buf,
   uint32_t len
);



#endif /*ARM_TA_QSEE_RSA_H*/

