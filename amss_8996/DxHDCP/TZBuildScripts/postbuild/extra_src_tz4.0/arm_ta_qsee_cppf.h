#ifndef ARM_TA_QSEE_CPPF_H
#define ARM_TA_QSEE_CPPF_H

#include "qsee_cppf.h"

CPPFErrNoType dx_qsee_set_hdcp_min_enc_level(MinEnLevelType min_enc_level);
CPPFErrNoType dx_qsee_get_hdcp_min_enc_level(MinEnLevelType *min_enc_level);


#endif // ARM_TA_QSEE_CPPF_H
