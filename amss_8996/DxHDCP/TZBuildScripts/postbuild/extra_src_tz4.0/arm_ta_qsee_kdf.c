
#include <stddef.h>
#include <stdint.h>

#include "qsee_kdf.h"
#include "arm_ta_qsee_kdf.h"



int dx_qsee_kdf(const void *key, unsigned int key_len, const void *label, unsigned int label_len,
        const void *context, unsigned int context_len, void* output, unsigned int output_len)
{
    return qsee_kdf(key, key_len, label, label_len,
            context, context_len, output, output_len);

}

