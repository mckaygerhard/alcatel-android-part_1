﻿/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef _DX_HDCP_TRANSMITTER_H
#define _DX_HDCP_TRANSMITTER_H

/*! \file DX_Hdcp_Transmitter.h
This module provides HDCP transmitter services.
*/

#include "DX_Hdcp_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif
#define HDCP_VERSION    1
/*! Inits transmitter & default parameters values
\parameters:
	notifyCallbackFunction -	Callback function reference for events notification
\return initialization status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Init(EventCbFunction notifyCallbackFunction);

/*! Opens transmitter's new session. 
\parameters:
	sessionID -			Session ID (value returned by function)
\return session open status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Open_Session(DxSessionID_t *sessionID);

/*! Opens transmitter's connection. 
\parameters:
	remoteIpAddress -	Remote IP address (4 bytes)
	ctrlPort -			Control port (for HDCP authentication messages)
	sessionID -			Session ID
	clientID -			Connection ID (value returned by function)
	timeout -			Connection timeout (in milli-seconds)
\return connection status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Connect(uint8_t *remoteIpAddress, uint32_t ctrlPort, DxSessionID_t sessionID, DxClientID_t *clientID, uint32_t timeout);

/*! Re-authenticate upon an already opened transmitter's connection. 
\parameters:
	clientID -			Connection ID
\return connection status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_ReAuthenticate(DxClientID_t clientID, uint32_t timeout);

/*! Opens transmitter's new stream.
\parameters:
	sessionID -			Session ID
	contentStreamID -	Stream content stream ID
	streamID -			Stream ID (value returned by function)
	streamType -		Stream type (audio/video etc)			
\return stream open status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Open_Stream_2(DxSessionID_t sessionID, uint32_t contentStreamID, DxStreamID_t *streamID, EDxHdcpStreamType streamType);

/*! Closes transmitter's stream.
\parameters:
	streamID -			Stream ID
\return stream close status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Close_Stream(DxStreamID_t streamID);

/*! Encrypts data to be transmitted. 
	Note: this functon is deprecated and must never be called. Use DX_HDCP_Tsmt_Encrypt2 instead.
\parameters:
	sessionID -			Session ID
	streamID -			Stream ID
	pesPrivateData -	PES private data (value returned by function)
	msgIn -				Input plane data
	msgOut -			Output encrypted data (value returned by function)
	msgLen -			Input/Output data length
\return encryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Encrypt(DxSessionID_t sessionID, DxStreamID_t streamID, DxPesData_t pesPrivateData, uint64_t msgIn, uint64_t msgOut, uint32_t msgLen);


/*! Encrypts data to be transmitted. 
\parameters:
	sessionID -			Session ID
	streamID -			Stream ID
	pesPrivateData -	PES private data (value returned by function)
	msgIn -				Input plane data
	msgOut -			Output encrypted data (value returned by function)
	msgLen -			Input/Output data length
	msgInOffset		-	Offset related to the beginning of input data, in bytes
	inputBufferType -	Input buffer type (virtual memory address or shared memory handle)
	outputBufferType -	Output buffer type (virtual memory address or shared memory handle)
\return encryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Encrypt2(DxSessionID_t       sessionID, 
											DxStreamID_t        streamID, 
											DxPesData_t         pesPrivateData, 
											uint64_t            msgIn, 
											uint64_t            msgOut, 
											uint32_t            msgLen, 
											uint32_t            msgInOffset, 
											EDxHdcpBufferType   inputBufferType, 
											EDxHdcpBufferType   outputBufferType);

/*! Closes transmitter's connection. 
\parameters:
	clientID -			Connection ID
\return connection status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Disconnect(DxClientID_t clientID);

/*! Stops transmitter's session.
\parameters:
	sessionID -			Session ID
\return stop status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Close_Session(DxSessionID_t sessionID);

/*! Closes transmitter
\return close status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Close();

/*! Updates received SRM data
\parameters:
	srmData -			SRM data structure
	srmDtaLen -			SRM data structure length
\return SRM update status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Srm_Update(uint8_t *srmData, uint32_t srmDtaLen);

/*! Queries whether a given device appears in the SRM
\parameters:
	deviceID -			Requested receiver's ID
	result -			True/False indication (value returned by function)
\return SRM query status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Srm_Query(DxHdcpReceiverId_t deviceID, bool *result);

/*! Get full topology of receivers & repeaters
\parameters:
	topologyData -		[OUT parameter] Aggregated Receivers / Repeaters topology data [Note: The depth is the maximum depth the Transmitter has got from its downstream devices].
\return Status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Get_Topology(DxHdcpTopologyData_t *topologyData);

/*! Set system parameter. This API is deprecated and should not be used.
\parameters:
	paramID -			Parameter key
	paramData -			Parameter value (returned by function)
\return set-parameter status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_Set_Parameter(EDxHdcpConfigParam paramID, void *paramData);

/*! Get version
\return transmitter's version
*/
EXTERNAL_API char *DX_HDCP_Tsmt_Get_Version();

/*! Removes stored pairing information
 * Note: The DX_HDCP_Tsmt_Init() should be called before using this API.
\parameters:
\return Status
*/
EXTERNAL_API uint32_t DX_HDCP_Tsmt_RemoveStoredPairingInfo();

//Move this one to a new common file in the future, if further work is to be done. For now this is the only commmon function.
/*! Returns HDCP version
\return HDCP version
*/
EXTERNAL_API uint32_t DX_HDCP_GetVersion();
#ifdef  __cplusplus
}
#endif

#endif
