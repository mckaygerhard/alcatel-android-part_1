//============================================================================
//  Name:                                                                     
//    std_loadsim_wcnss.cmm 
//
//  Description:                                                              
//    Script to load WCNSS logs
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//  Description:
//  T32 simulator loader specific to wcnss dumps. This file has some 8952 specific areas.
// 
//  Dependencies:
//  Depends on cmm script framework (various dependent files) to work, and assumes that a 
//  sanitized argument line has been passed in from std_loadsim.cmm script
//  
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 08/03/2015 JBILLING      Added support for automation and error message passing
// 07/13/2015 c_gunnan      Created for 8976
// 10/23/2013 yzhai         Removed "restore saved TCM" and "restore TBL state" part of the script.
// 09/06/2012 AJCheriyan    Added USB RAM dump support
// 08/21/2012 AJCheriyan    Fixed references to ADSP roots
// 07/10/2012 AJCheriyan    Created for B-family 
//




///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// std_loadsim_wcnss //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


//###################Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11


//#####################Select Subroutine###################
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine


// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"
IF !(("&SUBROUTINE"=="VERIFYBUILD")||("&SUBROUTINE"=="HELP")||("&SUBROUTINE"=="help"))
(
    &SUBROUTINE="MAIN"   
)

    // This should be created by some top level script. The setupenv for each proc would
    // set this up
    AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &ArgumentLine
    LOCAL &rvalue
    ENTRY %LINE &rvalue

    ENDDO &rvalue



////////////////////////////////////////
//
//            MAIN
//            Main std_loadsim_wcnss logic
//            Expected input: None. Relies on global variables
//
/////////////////////////////////////////

MAIN:
    LOCAL &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &alternateelf &extraoptions 
    ENTRY &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &alternateelf &extraoptions 

    LOCAL &rvalue
    
    // Load the memory map to initialize variables
    do std_memorymap 

    // First, all the sanity checks
    GOSUB CHECKBINARIES
        ENTRY %LINE &rvalue
        IF "&rvalue"!="SUCCESS"
        (
            GOSUB EXIT &rvalue
        )
        
    // Binaries look good. Else, we wouldn't be here    
    GOSUB SETUPSIM
    
    // Load the binaries
    GOSUB LOADBIN
    
    // Setup the environment
    do std_setupenv noareaclear
    
    // Load the symbols
    do std_loadsyms_wcnss &imagebuildroot locally NULL NULL &alternateelf &extraoptions
        ENTRY %LINE &rvalue
        IF "&rvalue"!="SUCCESS"
        (
            GOSUB EXIT &rvalue    
        )
        
    
    // Load the "state" at the time of the crash
    GOSUB RESTORESTATE
    ENTRY %LINE &rvalue
    
    //Wait for a few seconds if in automation mode  
    //in case user wants to spot check results.
    IF STRING.SCAN("&extraoptions","forcesilent",0)!=-1
    (
        WAIT.3s
    )
    // Off you go..
    GOSUB EXIT &rvalue

////////////////////////////////////////
//
//          VERIFYBUILD
//          Public function
//          Verify that needed files are present
//          Expected input: Build Location
//
/////////////////////////////////////////
VERIFYBUILD:
    LOCAL &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    ENTRY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    LOCAL &result &LOCAL_BUILDROOT
    
    &result="SUCCESS"
    &LOCAL_BUILDROOT="&IARG0"
    PRINT "Checking that needed files exist"
    
    AREA.RESET
    AREA.CREATE std_loadsim_build_verify 125. 6.
    AREA.SELECT std_loadsim_build_verify
    WINPOS 0. 0. 80. 6.
    AREA.VIEW std_loadsim_build_verify
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/std_extensions.cmm")
    (
        PRINT "Found std_extensions.cmm"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_extensions.cmm. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_WCNSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/std_toolsconfig.cmm")
    (
        PRINT "Found std_toolsconfig.cmm"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_toolsconfig.cmm. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_WCNSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/std_scripts.men")
    (
        PRINT "Found std_scripts.men"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_scripts.men. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_WCNSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/debugtools/err/cmm/load_coredump.cmm")
    (
        PRINT "Found load_coredump.cmm"
    )
    ELSE
    (
        PRINT "Missing core/debugtools/err/cmm/load_coredump.cmm. Some context may be lost!"
        //&result="FAILURE_VERIFYBUILD_WCNSS"
    )

    IF ("&result"=="SUCCESS")
    (
        PRINT "Verification success: Found needed context files"
        PRINT " "
    )
    ELSE
    (
        GOSUB FATALEXIT "Error: Needed build files (std_extensions, std_scripts, std_toolsconfig) not found"
    )

    
    

    RETURN &result
////////////////////////////////////////
//
//          SETUPSIM
//          Private function
//          Set the simulator for the processor we want
//          Expected input: None
//
/////////////////////////////////////////
SETUPSIM:
	SYS.CPU ARM926EJ
	SYS.UP
    

    RETURN

    
////////////////////////////////////////
//
//          CHECKBINARIES
//          Private function
//          Checks if the binaries for the system are present in the location
//          Loglocation should not be empty and assumes memory map is loaded
//          Expected input: None. Uses global variables
//          &logtype=<AUTODETECT,JTAG,USB> 
//
/////////////////////////////////////////
CHECKBINARIES:
LOCAL &file1 &file2 &logclass

    
    // This is the best place to determine the type of the log too
    IF ("&logtype"=="AUTODETECT")
    (
    
                &logclass="&logtype"
                
                //Check files present and try and auto-assign.
                IF ("&logclass"!="SSR")
                (
                    // Check for SSR logs
                    // User should have given full path to ssr file. 
                    IF (FILE.EXIST(&logpath))
                    (
                        &filetype=FILE.TYPE(&logpath)
                        IF ("&filetype"=="ELF")||("&filetype"=="BINARY")
                        (
                            &logclass="SSR"
                        )
                    )
                )

                IF ("&logclass"=="AUTODETECT")
                (
                    // Check for USB logs
                    do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
                    ENTRY &file1

                    IF ("&file1"=="TRUE")
                    (
                        &logclass="USB"
                    )
                )
                
                IF ("&logclass"=="AUTODETECT")
                (
                    // Check for JTAG logs
                    do std_utils FILEXIST EXIT &logpath &WCNSS_SW_log
                    ENTRY &file1

                    IF ("&file1"=="TRUE")
                    (
                         &logclass="JTAG"
                    )
                )
                
                
                // If we even after that, we are stuck with "AUTODETECT" we have a problem
                IF ("&logclass"=="AUTODETECT")
                (
                    AREA
                    PRINT %ERROR "USB/SSR/JTAG logs not found in folder: &logpath. If using SSR type log, please give full path and filename.'do std_loadsim help' for more"
                    GOSUB FATALEXIT "Failed to auto-detect log type. Unexpected file names or types in &logpath"
                )
                ELSE
                (
                    // Safe to change the logtype
                    &logtype="&logclass"
                    PRINT "Detected &logtype logs in: &logpath"
                )
    )
            
    IF ("&logtype"=="JTAG")
    (
        
        
        // Check for JTAG logs
        do std_utils FILEXIST EXIT &logpath &WCNSS_SW_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            AREA
            PRINT %ERROR "JTAG WCNSS logs not present in folder: &logpath"
            PRINT %ERROR "If you have only a binary, provide address"
            PRINT %ERROR "of WCNSS's DDR location when binary collected, via command line. e.g.: "
            PRINT %ERROR "do std_loadsim Img=wcnss Bld=C:\my\build Log=C:\temp\mylog\binary.bin extraoption=physaddr->0x8F800000"
            PRINT %ERROR "Type 'do std_loadsim help' for more information"
            GOSUB FATALEXIT "JTag logs not found in &logpath"
            
        )
        //If user has specified jtag logs and provided a physical address,
        //Then use the logic from SSR mode. If they haven't provided a
        //physical address, then look for Shared Imem log. If it's not there, exit.
        IF STRING.SCAN("&extraoptions","physaddr->",0)!=-1
        (
            &logpath="&logpath/&DDR_Periph_log"
            &logtype="SSR"
        )
        ELSE
        (
            // Check for Shared IMEM logs
            do std_utils FILEXIST EXIT &logpath &SHARED_IMEM_log
            ENTRY &file1

            IF ("&file1"=="FALSE")
            (
                AREA
                PRINT %ERROR "JTAG Shared IMEM logs not present in folder: &logpath"
                PRINT %ERROR "If you have only a binary, provide address"
                PRINT %ERROR "of WCNSS's DDR location when binary collected, via command line. e.g.: "
                PRINT %ERROR "do std_loadsim Img=wcnss Bld=C:\my\build Log=C:\temp\mylog\binary.bin extraoption=physaddr->0x8F800000"
                PRINT %ERROR "Type 'do std_loadsim help' for more information"
                GOSUB FATALEXIT "Shared IMEM logs not found in &logpath, for log type &logtype"
            )
        )

    )
    
    IF ("&logtype"=="USB")
    (
        
        // Check for USB logs
        do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            AREA
            PRINT %ERROR "USB WCNSS logs not present in folder: &logpath. (If trying ot use SSR log type, please provide full path and filename. 'do std_loadsim help' for more)"
            GOSUB FATALEXIT "USB logs not found in &logpath, for log type &logtype"
        )
        

        // Check for USB logs
        do std_utils FILEXIST EXIT &logpath &OCIMEM_USB_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            PRINT %ERROR "USB Shared IMEM logs not present in folder: &logpath"
            GOSUB FATALEXIT "Shared IMEM logs not found in &logpath, for log type &logtype"
        )
        
        
    )
    
    IF ("&logtype"=="SSR")
    (
        LOCAL &filetype
        // Check for SSR logs
        IF !(FILE.EXIST(&logpath))
        (
            AREA
            PRINT %ERROR "Not able to access SSR WCNSS logs at: &logpath. If using SSR type log, please give full path and filename. 'do std_loadsim help' for more"
            GOSUB FATALEXIT "SSR logs not found at &logpath, for log type &logtype"
            
        )
        
        &filetype=FILE.TYPE(&logpath)
        
        IF ("&filetype"!="ELF")
        (
            IF ("&filetype"=="BINARY")
            (
        
                IF STRING.SCAN("&extraoptions","physaddr->",0)==-1
                (
                
                        WINPOS 37% 37% 85. 15.
                        AREA.CREATE A0003
                        AREA.SELECT A0003
                        AREA.VIEW A0003
                        //AREA.RESET
                        //AREA
                        PRINT %ERROR "Binary dump file type specified for dump file, but no 'physaddr->' specified "
                        PRINT %ERROR "in extraoption. Please specify a physaddr->0x<physical_base> in command"
                        PRINT %ERROR "line for  binary standalone load. Note that binary standalone loading is only "
                        PRINT %ERROR "available from command line at this time"
                        PRINT " "
                        PRINT %ERROR "Type 'do std_loadsim help' for additional details"
                        PRINT " "
                        PRINT " "
                        PRINT " "
                        AREA.SELECT A000
                        AREA.VIEW A000
                        GOSUB FATALEXIT "physical address needed for logtype &logtype and filetype &filetype. See help menu"

                
                )
            )
            ELSE
            (
                PRINT %ERROR "Wrong Filetype for SSR Log file: &logpath. Expected: ELF or BIN, got &filetype"
                GOSUB FATALEXIT "Wrong Filetype for SSR Log file: &logpath. Expected: ELF or BIN, got &filetype"
            )
        )
        
    )

    RETURN SUCCESS

////////////////////////////////////////
//
//          LOADBIN
//          Private function
//          Loads the saved binaries
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//
/////////////////////////////////////////

LOADBIN:
LOCAL &Bin
	
    IF ("&logtype"=="JTAG")
    (
        do std_utils LOADBIN &logpath &SHARED_IMEM_log &SHARED_IMEM_start
        
        // Load the memory map again for relocated images
        do std_memorymap DYNAMIC
        
        // Now load the logs
        do std_utils LOADBIN &logpath &WCNSS_SW_log &WCNSS_SW_start
    )
    ELSE IF ("&logtype"=="USB")
    (
        // Load the shared IMEM logs
        do std_utils LOADBIN &logpath &OCIMEM_USB_log &OCIMEM_start
        
        // Load the memory map again for relocated images
        do std_memorymap DYNAMIC

        
        //for 2GB DDR DDRCS0 is loading at  &DDR_1_start
        //for 3GB DDR DDRCS1 is loading at &DDR_1_start. so workaround for this
        do ddr_select &logpath 
        ENTRY &Bin
        
        //Load the binary. 
        //skip memory up to start of subsystem software dump, subtracting start of ddr region start
        IF ("&Bin"=="DDRCS0")
        (
            do std_utils LOADBIN &logpath &DDR_1_USB_log &WCNSS_SW_start &WCNSS_SW_start&(~&DDR_1_start) &WCNSS_SW_size
        )
        ELSE
        (
            do std_utils LOADBIN &logpath &DDR_2_USB_log &WCNSS_SW_start &WCNSS_SW_start&(~&DDR_1_start) &WCNSS_SW_size
        )
    )
    ELSE IF ("&logtype"=="SSR")
    (
        
        
        IF FILE.TYPE(&logpath)=="ELF"
        (
            // Load the memory map again for relocated images
            //do std_memorymap DYNAMIC
            ON ERROR CONTINUE
            &tempdir=OS.ENV(TEMP)
            &tempfile="&tempdir\datalogfile.txt"
            PRINTER.FILE &tempfile
            PRINTER.FILETYPE ASCII
            PRINTER.SIZE 0XFA, 0XFA
            
            
            SYS.LOG.INIT
            SYS.LOG.RESET
            SYS.LOG.SIZE 10000.
            SYS.LOG.ON
            
            //DATA.LOG
            DATA.LOAD.ELF &logpath /noclear
            WP.DATA.LOG
                    
            SYS.LOG.OFF
            
            LOCAL &linelst &index
            open #1 &tempfile
            read #1 %line &linelst
            read #1 %line &linelst
            read #1 %line &linelst
            close #1
            &index=string.scan("&linelst","P:",0)
            
            &address=string.mid("&linelst",&index+2,8.)
            &WCNSS_SW_start=ADDRESS.OFFSET(P:&address)
            ON ERROR
            PRINT "Detected from &logpath that WCNSS SW start is at &WCNSS_SW_start"
        )
        ELSE IF FILE.TYPE(&logpath)=="BINARY"
        (
            IF STRING.SCAN("&extraoptions","physaddr->",0)==-1
            (
                PRINT %ERROR "Binary dump file specified for SSR file, but no 'physaddr->' specified "
            )
            ELSE
            (
                &index=string.scan("&extraoptions","physaddr->",0)
                
                &address=string.mid("&extraoptions",&index+10.,8.)
                //If '0x' specified, shift address up. Else leave it.
                IF STRING.SCAN(STRING.UPR("&address"),"0X",0)!=-1
                (
                    &address=&address*0x100
                )
                &WCNSS_SW_start=ADDRESS.OFFSET(P:&address)
                PRINT "Setting WCNSS Start address as &WCNSS_SW_start per extraoption specified"
                DATA.LOAD.BINARY &logpath &WCNSS_SW_start
            )
        
        
        )
        ELSE
        (
            //Never should get here
            PRINT %ERROR "Unrecognized filetype. expected BIN or ELF"
        )
    )
    RETURN
    
////////////////////////////////////////
//
//          RESTORESTATE
//          Private function
//          To load the error information from the saved logs
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//          Expects various files to be present
//
/////////////////////////////////////////
RESTORESTATE:


	 GOSUB RESTORE_CMEM
     GOSUB RESTORE_REGISTER_CONTEXT &logtype      
            

    RETURN
    

	
////////////////////////////////////////
//
//          RESTORE_CMEM_CONTEXT
//          Private function
//          Restore cmem context from ddr  
//          Expected input: None. Uses global variables
/////////////////////////////////////////
RESTORE_CMEM:
LOCAL &SPECIAL_LOG &SPECIAL_LOG_FILE &CODE_BASE &CODE_LENGTH &DATA_LENGTH &DATA_BASE
	
	IF ("&SPECIAL_LOG"=="0x1")
	(
		&start=data.long(D:&Vir_Start_ADDR)
		IF ("&start"=="0x0")
		(
		D.LOAD.ELF &SPECIAL_LOG_FILE /noclear /noreg
		)
	)
	//	The CMEM part of the memory should be restored from DDR back to CMEM
	&CODE_BASE=DATA.LONG(D:(ADDRESS.OFFSET(Load__LR__UBSP_IMAGE__Base)))
	// 64kB allocated for Code
	&CODE_LENGTH=0x10000
	&DATA_BASE=DATA.LONG(D:(ADDRESS.OFFSET(Load__LR__CMEM_DATA_SEG__Base)))
	// 56kB allocated for Data
	&DATA_LENGTH=0xE000

	// Now restore CMEM from DDR - restore code and data separately
	do std_utils MEMCOPY &WCNSS_CMEM_start &CODE_BASE &CODE_LENGTH
	do std_utils MEMCOPY &WCNSS_CMEM_start+&CODE_LENGTH &DATA_BASE &DATA_LENGTH
	
	RETURN

////////////////////////////////////////
//
//          RESTORE_REGISTER_CONTEXT
//          Private function
//          Restore register context from existing dump files 
//          Expected input: None. Uses global variables
//          Needs script files to be present, or will throw error
//
/////////////////////////////////////////
RESTORE_REGISTER_CONTEXT:
LOCAL &logtype
ENTRY &logtype
    IF ("&logtype"=="USB")
    (
            &runscript="true"
            ON ERROR GOSUB
            (
                    PRINT "Warning! Error ocurred during load_coredump. Some information may be lost."
                    &runscript="false"
            )
            IF ("&runscript"=="true")
            (
                //coredump not always populated
                do std_utils EXECUTESCRIPT EXIT &imagebuildroot/&processor_root_name/core/debugtools/err/cmm/load_coredump.cmm
            )
            ON ERROR
            
           
    )
    

    // Restore the registers from file if JTAG logs
    IF ("&logtype"=="JTAG")
    (
        THREAD 0
        do std_utils EXECUTESCRIPT EXIT &logpath/&WCNSS_regs
        THREAD 1
        do std_utils EXECUTESCRIPT EXIT &logpath/&WCNSS_mmu

    )
    
    RETURN
    

    
EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue
    

//Should never get here. 
FATALEXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    IF STRING.SCAN("&FAILUREKEYWORD","FAILUREKEYWORD",0)==-1
    (
        GOSUB EXIT &FAILUREKEYWORD - &rvalue
    )
    ELSE
    (
        GOSUB EXIT &rvalue
    )
    
//Should never get here
    END
