/*
#============================================================================
#  Name:                                                                     
#    VALIDATED_EMMC_FIREHOSE_8917_LITE_ASIC_oem_uuid.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 02:58:49 2016 
#============================================================================
*/
const char OEM_IMAGE_UUID_STRING_AUTO_UPDATED[]="OEM_IMAGE_UUID_STRING=Q_SENTINEL_{B9340B8E-86E0-4E70-8667-F60FA60D8CD5}_20160818_0258";
