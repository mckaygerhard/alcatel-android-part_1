/*
#============================================================================
#  Name:                                                                     
#    VALIDATED_EMMC_FIREHOSE_8953_DDR_ASIC_oem_uuid.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 03:01:04 2016 
#============================================================================
*/
const char OEM_IMAGE_UUID_STRING_AUTO_UPDATED[]="OEM_IMAGE_UUID_STRING=Q_SENTINEL_{BE3DCBFB-CA9B-4D9D-AC32-03D7D684404A}_20160818_0301";
