/*
#============================================================================
#  Name:                                                                     
#    DDR_DEBUG_8917_ASIC_oem_uuid.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 02:59:21 2016 
#============================================================================
*/
const char OEM_IMAGE_UUID_STRING_AUTO_UPDATED[]="OEM_IMAGE_UUID_STRING=Q_SENTINEL_{3F0DE0AF-60DE-4B22-9449-853576ED70DE}_20160818_0259";
