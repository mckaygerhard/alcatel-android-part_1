/*============================================================================
  FILE:         AdcBsp.c

  OVERVIEW:     Board support package for the ADC boot for 8952.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-08-05  PR  Initial version (from 8952).

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "AdcBootInternal.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*
 * 8937_PM8950
 */
static const AdcPhysicalDeviceType adcPhysicalDevices_8937_PM8950[] = {
   /* VADC */
   {
      /* .pszDevName */ "DALDEVICEID_VADC_8937_PM8950",
   },
};

const AdcBspType AdcBootBsp_8937_PM8950[] = {
   {
      /* .paAdcPhysicalDevices */ adcPhysicalDevices_8937_PM8950,
      /* .uNumPhysicalDevices  */ ARRAY_LENGTH(adcPhysicalDevices_8937_PM8950)
   }
};

/*
 * 8937_PM8950_PMI8950
 */
static const AdcPhysicalDeviceType adcPhysicalDevices_8937_PM8950_PMI8950[] = {
   /* VADC PM8950 */
   {
      /* .pszDevName */ "DALDEVICEID_VADC_8937_PM8950",
   },
   /* VADC PMI8950 */
   {
      /* .pszDevName */ "DALDEVICEID_VADC_8937_PMI8950",
   },
};

const AdcBspType AdcBootBsp_8937_PM8950_PMI8950[] = {
   {
      /* .paAdcPhysicalDevices */ adcPhysicalDevices_8937_PM8950_PMI8950,
      /* .uNumPhysicalDevices  */ ARRAY_LENGTH(adcPhysicalDevices_8937_PM8950_PMI8950)
   }
};

