/*============================================================================
  FILE:         TsensBootBsp.c

  OVERVIEW:     8937 BSP for Tsens boot.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-09-22  PR   Continuous mode, SW WA to QCTDD02523882 and QCTDD02529297.  
  2015-08-17  PR   8937 TSENS BSP file(ported from 8952).
  2015-06-08  PR   updated MTC configuration with SPT recommended tuning values.
  2015-02-11  SA   Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBootBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

#define TSENS_PERIOD                         0   // continuous mode
#define TSENS_PERIOD_SLEEP                0xFE   // 870ms
#define TSENS_SENSOR_CONV_TIME_US          150
#define TSENS_NUM_GET_TEMP_RETRIES           5
#define TSENS_GLOBAL_CONFIG         0x000BCCA3  

#define TSENS_Y1                            30
#define TSENS_Y2                           120

/* Thresholds */
#define TSENS_THRESHOLD_MIN               -35
#define TSENS_THRESHOLD_MAX               120

#define TSENS_MTC_THRESHOLD1_TEMP          95
#define TSENS_MTC_THRESHOLD2_TEMP         105
#define TSENS_MTC_THRESHOLD1_MARGIN         5
#define TSENS_MTC_THRESHOLD2_MARGIN         5

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const TsensBootSensorType aSensors[] =
{
   /* Sensor 0 */
   {
      /* .uTsensConfig         */ 0x9C,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_DISABLED,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 1 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 2 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 3 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 4 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 5 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 6 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 7 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 8 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 9 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 10 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
};

static const TsensBootSensorType aSensors_8917[] =
{
   /* Sensor 0 */
   {
      /* .uTsensConfig         */ 0x9C,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 1 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 2 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 3 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 4 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 5 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 6 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 7 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 8 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 9 */
   {
      /* .uTsensConfig         */ 0x49,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default          */ 500,
      /* .nM_default           */ 10923,  /* TSENS_FACTOR * (1 / 3.0) */
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   }
};

static const TsensBootMTCConfigType aMTCConfig[] =
{
   /* Zone 0 */
   {
      /* .bIsZoneEnabled    */ TRUE,
      /* .uPSCommandTh2Viol */ TSENS_BSP_MTC_SYS_PERF_25,
      /* .uPSCommandTh1Viol */ TSENS_BSP_MTC_SYS_PERF_50,
      /* .uPSCommandCool    */ TSENS_BSP_MTC_SYS_PERF_100,
      /* .uSensorMask       */ 0x200, // S9
      /* .bIsTH1Enabled     */ TRUE,
      /* .bIsTH2Enabled     */ TRUE,
   },
   /* Zone 1 */
   {
      /* .bIsZoneEnabled    */ TRUE,
      /* .uPSCommandTh2Viol */ TSENS_BSP_MTC_SYS_PERF_25,
      /* .uPSCommandTh1Viol */ TSENS_BSP_MTC_SYS_PERF_50,
      /* .uPSCommandCool    */ TSENS_BSP_MTC_SYS_PERF_100,
      /* .uSensorMask       */ 0x1F0, // S4, S5, S6, S7 and S8
      /* .bIsTH1Enabled     */ TRUE,
      /* .bIsTH2Enabled     */ TRUE,
   }
};

const TsensBootBspType TsensBootBsp[] =
{
   {
      /* .paSensors            */ aSensors,
      /* .uNumSensors          */ ARRAY_LENGTH(aSensors),
      /* .uPeriod              */ TSENS_PERIOD,
      /* .uPeriodSleep         */ TSENS_PERIOD_SLEEP,
      /* .bAutoAdjustPeriod    */ TRUE,
      /* .uSensorConvTime_us   */ TSENS_SENSOR_CONV_TIME_US,
      /* .uNumGetTempRetries   */ TSENS_NUM_GET_TEMP_RETRIES,
      /* .uGlobalConfig        */ TSENS_GLOBAL_CONFIG,
      /* .nY1                  */ TSENS_Y1,
      /* .nY2                  */ TSENS_Y2,
      /* .bIsMTCSupported      */ TRUE,
      /* .paMTCConfig          */ aMTCConfig,
      /* .uNumMTCZones         */ ARRAY_LENGTH(aMTCConfig),
      /* .uMTCThreshold2Margin */ TSENS_MTC_THRESHOLD1_MARGIN,
      /* .uMTCThreshold2Margin */ TSENS_MTC_THRESHOLD2_MARGIN,
      /* .pCpuThresholdsCfg    */ NULL
   }
};

const TsensBootBspType TsensBootBsp_8917[] =
{
   {
      /* .paSensors            */ aSensors_8917,
      /* .uNumSensors          */ ARRAY_LENGTH(aSensors_8917),
      /* .uPeriod              */ TSENS_PERIOD,
      /* .uPeriodSleep         */ TSENS_PERIOD_SLEEP,
      /* .bAutoAdjustPeriod    */ TRUE,
      /* .uSensorConvTime_us   */ TSENS_SENSOR_CONV_TIME_US,
      /* .uNumGetTempRetries   */ TSENS_NUM_GET_TEMP_RETRIES,
      /* .uGlobalConfig        */ TSENS_GLOBAL_CONFIG,
      /* .nY1                  */ TSENS_Y1,
      /* .nY2                  */ TSENS_Y2,
      /* .bIsMTCSupported      */ TRUE,
      /* .paMTCConfig          */ aMTCConfig,
      /* .uNumMTCZones         */ ARRAY_LENGTH(aMTCConfig),
      /* .uMTCThreshold2Margin */ TSENS_MTC_THRESHOLD1_MARGIN,
      /* .uMTCThreshold2Margin */ TSENS_MTC_THRESHOLD2_MARGIN,
      /* .pCpuThresholdsCfg    */ NULL
   }
};

