/*============================================================================
  FILE:         TsensBoot.c

  OVERVIEW:     Implementation of TSENS in the boot

  DEPENDENCIES: None

                Copyright (c) 2012-2016 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  $Header: //components/rel/boot.bf/3.3/boot_images/core/hwengines/tsens/src/V2/TsensBoot.c#4 $$DateTime: 2016/03/09 04:46:25 $$Author: pwbldsvc $


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2016-03-09  SA   Fix KW warnings.
  2016-02-04  SA   MTC register programming sequence change.
  2015-07-24  PR   2.x Tsens controller and new MTC features support for 8953.
  2015-02-24  SA   Added MTC support.
  2014-07-09  jjo  Add support for 20 nm TSENS.
  2013-01-15  jjo  Fixed timeout to account for all sensors being enabled.
  2012-11-14  jjo  Added critical thresholds.
  2012-09-13  jjo  Only check the ready bit on the first read.
  2012-07-30  jjo  Now using a C file for properties.
  2012-05-23  jjo  Added DAL properties.
  2012-05-20  jjo  Ported to 8974.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBoot.h"
#include "HALtsens.h"
#include "busywait.h"
#include "DALSys.h"
#include "DALStdDef.h"
#include "TsensBootBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef struct
{
   DALBOOL bChannelIsDead;
   uint32 uCodeAtZero;
   uint32 uShiftedSlope;
} TsensChannelType;

typedef struct
{
   TsensChannelType aChannels[TSENS_MAX_NUM_SENSORS];
   uint32 uVersion;
   uint32 uNumChannelsEnabled;
   uint32 uChannelEnableMask;
   uint32 uLowerEnableMask;
   uint32 uUpperEnableMask;
   uint32 uCriticalEnableMask;
   DALBOOL bBroadcastEnabled;
   int32 nBase1;
   int32 nBase2;
   int32 nShiftedSlope;
} TsensControllerType;

typedef struct
{
   const TsensBootBspType *pTsensBsp;
   TsensControllerType *pController;
   int32 nLastTempDeciDegC;
   uint32 uLastSensor;
   int32 nThresholdMax;
   int32 nThresholdMin;
} TsensDevCtxtType;
/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
TsensDevCtxtType gTsensDevCtxt = {0};
TsensControllerType gTsensController = {0};
DALBOOL gbTsensInitialized = FALSE;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static boolean bFirstReadDone = FALSE;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
static int32 Tsens_CheckThreshold(int32 nThreshold)
{
   if (nThreshold > HAL_tsens_GetMaxTemp())
   {
      return HAL_tsens_GetMaxTemp();
   }
   else if (nThreshold < HAL_tsens_GetMinTemp())
   {
      return HAL_tsens_GetMinTemp();
   }
   else
   {
      return nThreshold;
   }
}

static int32 Tsens_DivideWithRounding(int32 nNum, int32 nDen)
{
   int32 nOffset = nDen / 2;

   if (nNum < 0)
   {
      nOffset *= -1;
   }

   return (nNum + nOffset) / nDen;
}

static TsensResultType Tsens_GetCalibration(void)
{
   const TsensBootBspType *pBsp = gTsensDevCtxt.pTsensBsp;
   const int32 nCalPoint1DeciDegC = pBsp->nCalPoint1DeciDegC;
   const int32 nCalPoint2DeciDegC = pBsp->nCalPoint2DeciDegC;
   const int32 nDeltaDeciDegC = nCalPoint2DeciDegC - nCalPoint1DeciDegC;
   const int32 nShiftedSlopeNum = nDeltaDeciDegC << pBsp->uShift;
   const TsensBootSensorType *pSensorCfg;
   HAL_tsens_Calibration eSensorCalType = HAL_TSENS_NOT_CALIBRATED;
   TsensControllerType *pController;
   HAL_tsens_Calibration eCalType;
   boolean bUseRedundant;
   TsensChannelType *pChannel;
   uint32 uSensor;
   int32 nCode1;
   int32 nNum;
   int32 nDen;

   bUseRedundant = HAL_tsens_UseRedundant();

   eCalType = HAL_tsens_CalSelect(bUseRedundant);
   pController = gTsensDevCtxt.pController;
   
   pController->uChannelEnableMask = 0;

      /* Get the base points as applicable */
      switch (eCalType)
      {
         case HAL_TSENS_ONE_POINT:
            pController->nBase1 = (int32)HAL_tsens_GetBaseX1(bUseRedundant);
            break;

         case HAL_TSENS_TWO_POINT:
            pController->nBase1 = (int32)HAL_tsens_GetBaseX1(bUseRedundant);
            pController->nBase2 = (int32)HAL_tsens_GetBaseX2(bUseRedundant);

            // uShiftedSlope = ((T2 - T1) << N) / (C2 - C1)
            nDen = pController->nBase2 - pController->nBase1;
            pController->nShiftedSlope = Tsens_DivideWithRounding(nShiftedSlopeNum, nDen);
            break;

         default:
            break;
      }


   /* Get the cal data per sensor */
   for (uSensor = 0; uSensor < pBsp->uNumSensors; uSensor++)
   {
      pSensorCfg = &pBsp->paSensors[uSensor];
      pChannel = &pController->aChannels[uSensor];
      pChannel->bChannelIsDead = FALSE;

      /* Check to see if default calibration is being forced */
      switch (pSensorCfg->eCal)
      {
         case TSENS_BSP_SENSOR_CAL_NORMAL:
            /* Use the normal method */
            eSensorCalType = eCalType;
            break;

         case TSENS_BSP_SENSOR_CAL_IGNORE_DEVICE_CAL:
            /* Force default char data */
            eSensorCalType = HAL_TSENS_NOT_CALIBRATED;
            break;

         default:
            return TSENS_ERROR;
      }

      switch (eSensorCalType)
      {
         case HAL_TSENS_ONE_POINT:
            nCode1 = pController->nBase1 + (int32)HAL_tsens_GetPointX1(bUseRedundant, uSensor);

            // uCodeAtZero = C1 - T1 * slope
            nNum = nCalPoint1DeciDegC * (pSensorCfg->nCalPoint2CodeDefault - pSensorCfg->nCalPoint1CodeDefault);
            pChannel->uCodeAtZero = (uint32)(nCode1 - Tsens_DivideWithRounding(nNum, nDeltaDeciDegC));

            // uShiftedSlope = ((T2 - T1) << N) / (C2 - C1)
            nDen = pSensorCfg->nCalPoint2CodeDefault - pSensorCfg->nCalPoint1CodeDefault;
            pChannel->uShiftedSlope = (uint32)Tsens_DivideWithRounding(nShiftedSlopeNum, nDen);
            break;

         case HAL_TSENS_TWO_POINT:
            nCode1 = pController->nBase1 + (int32)HAL_tsens_GetPointX1(bUseRedundant, uSensor);

            // uCodeAtZero = C1 - T1 * slope
            nNum = nCalPoint1DeciDegC * (pController->nBase2 - pController->nBase1);
            pChannel->uCodeAtZero = (uint32)(nCode1 - Tsens_DivideWithRounding(nNum, nDeltaDeciDegC));

            // uShiftedSlope
            pChannel->uShiftedSlope = pController->nShiftedSlope;
            break;

         default:
            // uCodeAtZero = ((T1 * (C1 - C2)) / (T2 - T1)) + C1
            nNum = (pSensorCfg->nCalPoint1CodeDefault - pSensorCfg->nCalPoint2CodeDefault) * nCalPoint1DeciDegC;
            pChannel->uCodeAtZero = (uint32)(Tsens_DivideWithRounding(nNum, nDeltaDeciDegC) + pSensorCfg->nCalPoint1CodeDefault);

            // uShiftedSlope = ((T2 - T1) << N) / (C2 - C1)
            nDen = pSensorCfg->nCalPoint2CodeDefault - pSensorCfg->nCalPoint1CodeDefault;
            pChannel->uShiftedSlope = (uint32)Tsens_DivideWithRounding(nShiftedSlopeNum, nDen);
            break;
      }

      pController->uChannelEnableMask |= 1 << uSensor;
      pController->uNumChannelsEnabled++;
   }

   return TSENS_SUCCESS;
}

/* ============================================================================
**
**  Tsens_MTC_Init
**
**  Description:
**    Initializes TSENS MTC in the boot.
**
**  Parameters:
**    None
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    None
**
** ========================================================================= */
static TsensResultType Tsens_MTC_Init()
{
   uint32 i;
   int32 nTh1,nTh2;
   const TsensBootMTCConfigType *pMTCConfig;
   const TsensBootSensorType *pSensor;
   const TsensBootBspType *pTsensBootBsp = NULL;

   if (NULL != gTsensDevCtxt.pTsensBsp)
   {
       pTsensBootBsp = gTsensDevCtxt.pTsensBsp;
   }
   else
   {
      return TSENS_ERROR;
   }

   // Set the threshold temperature for all sensors
   for (i = 0; i < pTsensBootBsp->uNumSensors; ++i)
   {
      pSensor = &pTsensBootBsp->paSensors[i];
      nTh1=Tsens_CheckThreshold((int32)pSensor->uMTCThreshold1Temp);
      nTh2=Tsens_CheckThreshold((int32)pSensor->uMTCThreshold2Temp);
      if(nTh2>nTh1)
      {
         HAL_tsens_MTC_SetThresholds(i, nTh1, nTh2);
         if((nTh2+pTsensBootBsp->uMTCThreshold2Margin) <= (nTh1+pTsensBootBsp->uMTCThreshold1Margin))
         {
            HAL_tsens_MTC_SetMargins(i, 0, 0);  
         }
         else
         {
            HAL_tsens_MTC_SetMargins(i, pTsensBootBsp->uMTCThreshold1Margin, pTsensBootBsp->uMTCThreshold2Margin);
         }
      }
      else
      {
         return TSENS_ERROR;
      }
   }

   // Configure and enable MTC zones
   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      HAL_tsens_MTC_ConfigZoneSwMask(i, pMTCConfig->bIsTH1Enabled, pMTCConfig->bIsTH2Enabled);
   }

   HAL_tsens_SetMTCState(HAL_TSENS_ENABLE_MTC);

   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      HAL_tsens_MTC_ConfigZone(i, pMTCConfig->uPSCommandTh2Viol, pMTCConfig->uPSCommandTh1Viol, pMTCConfig->uPSCommandCool, pMTCConfig->uSensorMask);
   }

   return TSENS_SUCCESS;
}

static TsensResultType Tsens_Tsens_SetCpuThresholdsInternal(uint32 uCpuIndex, int32 nCpuLowDegC, int32 nCpuHighDegC)
{
   boolean bCpuHighEn;
   boolean bCpuLowEn;
   uint32 uSensor;
   const TsensBootBspType *pTsensBootBsp = NULL;

   if (uCpuIndex >= TSENS_MAX_NUM_CPU_SENSORS)
   {
      return TSENS_ERROR;
   }
   if (NULL != gTsensDevCtxt.pTsensBsp)
   {
       pTsensBootBsp = gTsensDevCtxt.pTsensBsp;
   }
   else
   {
      return TSENS_ERROR;
   }

   uSensor = (pTsensBootBsp->pCpuThresholdsCfg->uCpuIndexes >> (uCpuIndex * 4)) & 0xf;
   if (uSensor >= TSENS_MAX_NUM_SENSORS)
   {
      return TSENS_ERROR;
   }

   if (nCpuHighDegC != TSENS_THRESHOLD_DISABLED)
   {
      bCpuHighEn = TRUE;
      nCpuHighDegC = Tsens_CheckThreshold(nCpuHighDegC);
   }
   else
   {
      bCpuHighEn = FALSE;
      nCpuHighDegC = HAL_tsens_GetMaxTemp();
   }

   if (nCpuLowDegC != TSENS_THRESHOLD_DISABLED)
   {
      bCpuLowEn = TRUE;
      nCpuLowDegC = Tsens_CheckThreshold(nCpuLowDegC);
   }
   else
   {
      bCpuLowEn = FALSE;
      nCpuLowDegC = 0;
   }

   HAL_tsens_SetCpuHighLowThreshold(uCpuIndex, nCpuHighDegC, nCpuLowDegC, bCpuHighEn, bCpuLowEn);

   return TSENS_SUCCESS;
}


/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  Tsens_Init
**
**  Description:
**    Initializes TSENS in the boot.
**
**  Parameters:
**    None
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    None
**
** ========================================================================= */
TsensResultType Tsens_Init(void)
{
   DALSYS_PROPERTY_HANDLE_DECLARE(hTsensBootProperties);
   DALSYSPropertyVar propertyVar;
   DALResult status;
   TsensResultType result;
   const TsensBootSensorType *pSensor;
   uint32 uSensor;
   uint32 uSensorEnableMask;
   uint32 uDisabledCPUsMask;
   uint32 uCpuIndex;
   const CpuThresholdsConfigType *pCpuThresholdsCfg;
   const TsensBootBspType *pTsensBootBsp = NULL;
   const TsensChannelType *pChannel;
   TsensControllerType *pController=NULL;
   
   if (gbTsensInitialized == TRUE)
   {
      return TSENS_SUCCESS;
   }

   /* Get the properties */
   status = DALSYS_GetDALPropertyHandleStr("QTsens", hTsensBootProperties);
   if (status != DAL_SUCCESS)
   {
      return TSENS_ERROR;
   }

   status = DALSYS_GetPropertyValue(hTsensBootProperties,
                                    "TSENS_BOOT_BSP",
                                    0,
                                    &propertyVar);
   if (status != DAL_SUCCESS)
   {
      return TSENS_ERROR;
   }

   gTsensDevCtxt.pTsensBsp = (TsensBootBspType *)propertyVar.Val.pStruct;
   if(gTsensDevCtxt.pTsensBsp == NULL)
   {
      return TSENS_ERROR;
   }
   pTsensBootBsp = gTsensDevCtxt.pTsensBsp;
   
   gTsensDevCtxt.pController = &gTsensController;
   pController = gTsensDevCtxt.pController;
   uSensorEnableMask = 0;
   for (uSensor = 0; uSensor < pTsensBootBsp->uNumSensors; uSensor++)
   {
      uSensorEnableMask |= 1 << uSensor;
   }

   result = Tsens_GetCalibration();
   if (result != TSENS_SUCCESS)
   {
      return result;
   }
  
   /* Configure & start Tsens */
   HAL_tsens_SetState(HAL_TSENS_DISABLE);
   HAL_tsens_Reset();
   HAL_tsens_SetPSHoldResetEn(pTsensBootBsp->bPSHoldResetEn);
   HAL_tsens_SetResultFormat(HAL_TSENS_RESULT_TYPE_DECI_DEG_C);
   HAL_tsens_SetPeriod(pTsensBootBsp->uPeriod);
   HAL_tsens_SetPeriodSleep(pTsensBootBsp->uPeriodSleep);
   HAL_tsens_SetAutoAdjustPeriod(pTsensBootBsp->bAutoAdjustPeriod);
   if(pTsensBootBsp->bStandAlone == TRUE)
   {
      HAL_tsens_SelectADCClkSrc(HAL_TSENS_EXTERNAL);
   }
   else
   {
      HAL_tsens_SelectADCClkSrc(HAL_TSENS_INTERNAL);
   }
   HAL_tsens_SetSensorsEnabled(uSensorEnableMask);   
   HAL_tsens_SetSidebandSensorsEnabled(pTsensBootBsp->uSidebandEnMask&uSensorEnableMask);
  // HAL_tsens_SetValidBitDelay(0x0);
   HAL_tsens_SetPWMEn(pTsensBootBsp->bPWMEn);
   HAL_tsens_Init(pTsensBootBsp->uGlobalConfig);

   /* Configure the sensor & set the critical thresholds */
   for (uSensor = 0; uSensor < pTsensBootBsp->uNumSensors; uSensor++)
   {
      pSensor = &pTsensBootBsp->paSensors[uSensor];
      pChannel = &pController->aChannels[uSensor];
      /* Sensor configuration */
      HAL_tsens_ConfigSensor(uSensor, pSensor->uTsensConfig);
      HAL_tsens_SetSensorID(uSensor,uSensor); 
      if (pChannel->bChannelIsDead == FALSE)
      {
         /* Set up scaling parameters */
         HAL_tsens_SetConversionFactors(uSensor, pTsensBootBsp->uShift, pChannel->uShiftedSlope, pChannel->uCodeAtZero);
      }
      
      /* Critical threshold configuration */
      if (pSensor->nCriticalMin != TSENS_THRESHOLD_DISABLED)
      {
         HAL_tsens_SetThreshold(HAL_TSENS_MIN_LIMIT_TH, uSensor, pSensor->nCriticalMin);
         HAL_tsens_EnableInterrupt(HAL_TSENS_MIN_LIMIT_TH, uSensor);
      }
      else
      {
         HAL_tsens_DisableInterrupt(HAL_TSENS_MIN_LIMIT_TH, uSensor);
      }

      if (pSensor->nCriticalMax != TSENS_THRESHOLD_DISABLED)
      {
         HAL_tsens_SetThreshold(HAL_TSENS_MAX_LIMIT_TH, uSensor, pSensor->nCriticalMax);
         HAL_tsens_EnableInterrupt(HAL_TSENS_MAX_LIMIT_TH, uSensor);
      }
      else
      {
         HAL_tsens_DisableInterrupt(HAL_TSENS_MAX_LIMIT_TH, uSensor);
      }
   }

   if (pTsensBootBsp->bIsMTCSupported)
   {
      result = Tsens_MTC_Init();
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
   }

   /* LMh-Lite configuration */
   pCpuThresholdsCfg = pTsensBootBsp->pCpuThresholdsCfg;
   if (pCpuThresholdsCfg != NULL)
   {
      // Get disabled CPUs mask
      uDisabledCPUsMask = HAL_tsens_GetDisabledCPUsMask();

      for (uCpuIndex = 0; uCpuIndex < TSENS_MAX_NUM_CPU_SENSORS; uCpuIndex++)
      {
         if (uDisabledCPUsMask & (1 << uCpuIndex))
         {
            // This CPU is disabled
            continue;
         }
         result = Tsens_Tsens_SetCpuThresholdsInternal(uCpuIndex,
                                                       pCpuThresholdsCfg->nCpuLowThreshold[uCpuIndex],
                                                       pCpuThresholdsCfg->nCpuHighThreshold[uCpuIndex]);
         if (result != TSENS_SUCCESS)
         {
            return result;
         }
      }

      HAL_tsens_SetCpuIndexes(pCpuThresholdsCfg->uCpuIndexes);
      HAL_tsens_SetCpuHighLowEnable(pCpuThresholdsCfg->bCpuThresholdsEnabled);
   }

   HAL_tsens_SetState(HAL_TSENS_ENABLE);

   gbTsensInitialized = TRUE;
   bFirstReadDone = FALSE;
   return TSENS_SUCCESS;
}

/* ============================================================================
**
**  Tsens_GetTemp
**
**  Description:
**    Gets the current temperature of the sensor.
**
**  Parameters:
**    uSensor [in]: sensor index
**    pnDegC [out]: sensor temperature in degrees C
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    Tsens_Init must be called once prior to calling this function.
**
** ========================================================================= */
TsensResultType Tsens_GetTemp(uint32 uSensor, int32 *pnDegC)
{
   DALBOOL bMeasComplete = FALSE;
   uint32 uTimerCnt;
   int32 nTemperature = 0;
   TsensResultType result;
   uint32 uWaitTime_us;
   int32 nTemperatureTry1;
   int32 nTemperatureTry2;
   int32 nTemperatureTry3;
   boolean bValid;
   const TsensBootBspType *pTsensBootBsp = NULL;
   
   if (pnDegC == NULL)
   {
      return TSENS_ERROR;
   }

   if (gbTsensInitialized != TRUE)
   {
      return TSENS_ERROR_NOT_INITIALIZED;
   }
   pTsensBootBsp = gTsensDevCtxt.pTsensBsp;
   if (uSensor >= pTsensBootBsp->uNumSensors)
   {
      return TSENS_ERROR;
   }

   /* for continuous mode(measure period=0), TRDY stays low and hence wait for all sensors to convert once and relay on VALID bit alone*/
   if((!pTsensBootBsp->uPeriod) && (bFirstReadDone == FALSE))
   {
      DALSYS_BusyWait(pTsensBootBsp->uSensorConvTime_us * pTsensBootBsp->uNumSensors);
      bFirstReadDone=TRUE;
   }
   
   /* Need to delay until TSENS performs the first reads */
   if (bFirstReadDone == FALSE)
   {
      uWaitTime_us = pTsensBootBsp->uSensorConvTime_us * pTsensBootBsp->uNumSensors;

      for (uTimerCnt = 0; uTimerCnt < pTsensBootBsp->uNumGetTempRetries; uTimerCnt++)
      {
         if (HAL_tsens_TempMeasurementIsComplete() == TRUE)
         {
            bMeasComplete = TRUE;
            break;
         }
         DALSYS_BusyWait(uWaitTime_us);
      }

      if (bMeasComplete == FALSE)
      {
         if (HAL_tsens_TempMeasurementIsComplete() == FALSE)
         {
            return TSENS_ERROR_TIMEOUT;
         }
      }

      bFirstReadDone = TRUE;
   }

   /* Read the temperature of a given sensor */
   bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nTemperatureTry1);
   if (bValid == TRUE)
   {
      nTemperature = nTemperatureTry1;
   }
   else
   {
      bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nTemperatureTry2);
      if (bValid == TRUE)
      {
         nTemperature = nTemperatureTry2;
      }
      else
      {
         bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nTemperatureTry3);
         if (bValid == TRUE)
         {
            nTemperature = nTemperatureTry3;
         }
         else if (nTemperatureTry1 == nTemperatureTry2)
         {
            nTemperature = nTemperatureTry1;
         }
         else if (nTemperatureTry2 == nTemperatureTry3)
         {
            nTemperature = nTemperatureTry2;
         }
         else
         {
            nTemperature = nTemperatureTry1;
         }
      }
   }
   result = TSENS_SUCCESS;
   *pnDegC = nTemperature/10; /*convert from DeciDegrees to Degrees */
   gTsensDevCtxt.nLastTempDeciDegC = *pnDegC;
   gTsensDevCtxt.uLastSensor = uSensor;

   return result;
}

/* ============================================================================
**
**  Tsens_GetNumSensors
**
**  Description:
**    Gets the number of sensors. Note that the sensor index is zero-based, i.e.
**    uSensor = {0 to *puNumSensors - 1}
**
**  Parameters:
**    puNumSensors [out]: number of sensors
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    Tsens_Init must be called once prior to calling this function.
**
** ========================================================================= */
TsensResultType Tsens_GetNumSensors(uint32 *puNumSensors)
{
   const TsensBootBspType *pTsensBootBsp = NULL;
   if (puNumSensors == NULL)
   {
      return TSENS_ERROR;
   }

   if (gbTsensInitialized != TRUE)
   {
      return TSENS_ERROR_NOT_INITIALIZED;
   }
   pTsensBootBsp = gTsensDevCtxt.pTsensBsp;
   *puNumSensors = pTsensBootBsp->uNumSensors;

   return TSENS_SUCCESS;
}

