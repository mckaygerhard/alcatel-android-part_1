#ifndef __MMSS_OXILI_RBBM_HWIO_H__
#define __MMSS_OXILI_RBBM_HWIO_H__
/*
===========================================================================
*/
/**
  @file mmss_oxili_rbbm_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MMSS_OXILI_RBBM_BLOCK0

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8976/mmss_oxili_rbbm_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: MMSS_OXILI_RBBM_BLOCK0
 *--------------------------------------------------------------------------*/

#define MMSS_OXILI_RBBM_BLOCK0_REG_BASE                                                          (MMSS_BASE      + 0x00300000)

#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000000)
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_VERSION_ADDR, HWIO_MMSS_OXILI_RBBM_HW_VERSION_RMSK)
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_VERSION_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_STEP_BMSK                                                0xffff0000
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_STEP_SHFT                                                      0x10
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_MINOR_BMSK                                                   0xfff0
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_MINOR_SHFT                                                      0x4
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_MAJOR_BMSK                                                      0xf
#define HWIO_MMSS_OXILI_RBBM_HW_VERSION_MAJOR_SHFT                                                      0x0

#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000004)
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_RELEASE_ADDR, HWIO_MMSS_OXILI_RBBM_HW_RELEASE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_RELEASE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_YEAR_BMSK                                                0xe0000000
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_YEAR_SHFT                                                      0x1d
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_MONTH_BMSK                                               0x1e000000
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_MONTH_SHFT                                                     0x19
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_DAY_BMSK                                                  0x1f00000
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_DAY_SHFT                                                       0x14
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_RELEASE_NUM_BMSK                                            0xf0000
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_RELEASE_NUM_SHFT                                               0x10
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_HM_ID_BMSK                                                   0xffff
#define HWIO_MMSS_OXILI_RBBM_HW_RELEASE_HM_ID_SHFT                                                      0x0

#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000008)
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_RMSK                                                0x7fff7ff
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_ADDR, HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_RMSK)
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_RB_BMSK                                         0x7000000
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_RB_SHFT                                              0x18
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_CONFIG_ID_BMSK                                       0xff0000
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_CONFIG_ID_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_AXI_PORTS_BMSK                                     0xf000
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_AXI_PORTS_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_UCHE_BANKS_BMSK                                     0x700
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_UCHE_BANKS_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_VPC_MEM_DEPTH_L2_BMSK                                    0xf0
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_VPC_MEM_DEPTH_L2_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_ENH_SP_BMSK                                               0x8
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_ENH_SP_SHFT                                               0x3
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_SP_BMSK                                               0x7
#define HWIO_MMSS_OXILI_RBBM_HW_CONFIGURATION_NUM_SP_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_ADDR                                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000080)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RMSK                                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_HM_BMSK                                           0xc0000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_HM_SHFT                                                 0x1e
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VBIF_BMSK                                         0x30000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VBIF_SHFT                                               0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_UCHE_BMSK                                          0xc000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_UCHE_SHFT                                               0x1a
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VSC_BMSK                                           0x3000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VSC_SHFT                                                0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_RAS_BMSK                                            0xc00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_RAS_SHFT                                                0x16
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_TSE_BMSK                                            0x300000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_TSE_SHFT                                                0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_HLSQ_BMSK                                            0xc0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_HLSQ_SHFT                                               0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VPC_BMSK                                             0x30000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VPC_SHFT                                                0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VFD_BMSK                                              0xc000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_VFD_SHFT                                                 0xe
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_PC_BMSK                                               0x3000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_PC_SHFT                                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_CP_BMSK                                                0xc00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_CP_SHFT                                                  0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_RBMARB_BMSK                                            0x300
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_RBMARB_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_SPTP_BMSK                                               0xff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_CGC_CTL_SPTP_SHFT                                                0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000108)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RMSK                                                            0x3
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_CGC_CTR_MARB_BMSK                                               0x3
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_CGC_CTR_MARB_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001e0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE8_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE8_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE7_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE7_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE6_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE6_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE5_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE5_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE4_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE4_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE3_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE3_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE2_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE2_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE1_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB0_CGC_CTL_RB_MODE1_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001e4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE8_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE8_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE7_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE7_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE6_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE6_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE5_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE5_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE4_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE4_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE3_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE3_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE2_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE2_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE1_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB1_CGC_CTL_RB_MODE1_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001e8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE8_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE8_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE7_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE7_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE6_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE6_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE5_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE5_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE4_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE4_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE3_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE3_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE2_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE2_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE1_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB2_CGC_CTL_RB_MODE1_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001ec)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE8_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE8_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE7_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE7_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE6_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE6_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE5_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE5_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE4_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE4_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE3_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE3_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE2_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE2_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE1_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_RB3_CGC_CTL_RB_MODE1_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001f0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_RMSK                                                    0x77777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_L1_MODE_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_L1_MODE_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE12_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE12_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE11_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE11_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE10_BMSK                                     0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE10_SHFT                                      0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE9_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB0_CGC_CTL_RB_MODE9_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001f4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_RMSK                                                    0x77777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_L1_MODE_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_L1_MODE_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE12_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE12_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE11_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE11_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE10_BMSK                                     0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE10_SHFT                                      0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE9_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB1_CGC_CTL_RB_MODE9_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001f8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_RMSK                                                    0x77777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_L1_MODE_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_L1_MODE_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE12_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE12_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE11_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE11_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE10_BMSK                                     0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE10_SHFT                                      0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE9_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB2_CGC_CTL_RB_MODE9_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001fc)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_RMSK                                                    0x77777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_L1_MODE_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_L1_MODE_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE12_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE12_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE11_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE11_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE10_BMSK                                     0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE10_SHFT                                      0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE9_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_RB3_CGC_CTL_RB_MODE9_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001a0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE7_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE7_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP0_CGC_CTL_SP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001a4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE7_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE7_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP1_CGC_CTL_SP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001a8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE7_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE7_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP2_CGC_CTL_SP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001ac)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_RMSK                                                  0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE7_BMSK                                 0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE7_SHFT                                       0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_SP3_CGC_CTL_SP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001b0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE14_BMSK                                0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE14_SHFT                                     0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE13_BMSK                                 0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE13_SHFT                                     0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE12_BMSK                                  0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE12_SHFT                                     0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE11_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE11_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE10_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE10_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE9_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE9_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE8_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP0_CGC_CTL_SP_MODE8_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001b4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE14_BMSK                                0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE14_SHFT                                     0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE13_BMSK                                 0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE13_SHFT                                     0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE12_BMSK                                  0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE12_SHFT                                     0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE11_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE11_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE10_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE10_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE9_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE9_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE8_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP1_CGC_CTL_SP_MODE8_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001b8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE14_BMSK                                0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE14_SHFT                                     0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE13_BMSK                                 0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE13_SHFT                                     0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE12_BMSK                                  0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE12_SHFT                                     0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE11_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE11_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE10_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE10_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE9_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE9_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE8_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP2_CGC_CTL_SP_MODE8_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001bc)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE14_BMSK                                0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE14_SHFT                                     0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE13_BMSK                                 0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE13_SHFT                                     0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE12_BMSK                                  0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE12_SHFT                                     0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE11_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE11_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE10_BMSK                                    0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE10_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE9_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE9_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE8_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_SP3_CGC_CTL_SP_MODE8_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001c0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_RMSK                                                     0xf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_2_BMSK                                       0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_2_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_1_BMSK                                        0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_1_SHFT                                          0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP0_CGC_HYST_SP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001c4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_RMSK                                                     0xf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_2_BMSK                                       0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_2_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_1_BMSK                                        0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_1_SHFT                                          0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP1_CGC_HYST_SP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001c8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_RMSK                                                     0xf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_2_BMSK                                       0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_2_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_1_BMSK                                        0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_1_SHFT                                          0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP2_CGC_HYST_SP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001cc)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_RMSK                                                     0xf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_2_BMSK                                       0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_2_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_1_BMSK                                        0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_1_SHFT                                          0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_SP3_CGC_HYST_SP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001d0)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_RMSK                                                    0x71c7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_2_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_2_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_1_BMSK                                      0x1c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_1_SHFT                                        0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP0_CGC_DELAY_SP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001d4)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_RMSK                                                    0x71c7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_2_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_2_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_1_BMSK                                      0x1c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_1_SHFT                                        0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP1_CGC_DELAY_SP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001d8)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_RMSK                                                    0x71c7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_2_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_2_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_1_BMSK                                      0x1c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_1_SHFT                                        0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP2_CGC_DELAY_SP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000001dc)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_RMSK                                                    0x71c7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_2_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_2_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_1_BMSK                                      0x1c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_1_SHFT                                        0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_SP3_CGC_DELAY_SP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_ADDR                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000238)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_RMSK                                          0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_CGC_DELAY_RB_MARB_CCU_L1_BMSK                 0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_0_CGC_DELAY_RB_MARB_CCU_L1_SHFT                 0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_ADDR                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000023c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_RMSK                                          0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_CGC_DELAY_RB_MARB_CCU_L1_BMSK                 0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_1_CGC_DELAY_RB_MARB_CCU_L1_SHFT                 0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_ADDR                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000240)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_RMSK                                          0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_CGC_DELAY_RB_MARB_CCU_L1_BMSK                 0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_2_CGC_DELAY_RB_MARB_CCU_L1_SHFT                 0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_ADDR                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000244)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_RMSK                                          0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_CGC_DELAY_RB_MARB_CCU_L1_BMSK                 0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_RB_MARB_CCU_L1_3_CGC_DELAY_RB_MARB_CCU_L1_SHFT                 0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000208)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_RMSK                                                0x1ff7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_Z_C_BMSK                                0x1c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_Z_C_SHFT                                   0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_C_BMSK                                   0x380
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_C_SHFT                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_Z_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_CCU_Z_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_MARB_BMSK                                      0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU0_CGC_CTL_MARB_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000020c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_RMSK                                                0x1ff7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_Z_C_BMSK                                0x1c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_Z_C_SHFT                                   0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_C_BMSK                                   0x380
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_C_SHFT                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_Z_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_CCU_Z_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_MARB_BMSK                                      0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU1_CGC_CTL_MARB_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000210)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_RMSK                                                0x1ff7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_Z_C_BMSK                                0x1c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_Z_C_SHFT                                   0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_C_BMSK                                   0x380
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_C_SHFT                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_Z_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_CCU_Z_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_MARB_BMSK                                      0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU2_CGC_CTL_MARB_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000214)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_RMSK                                                0x1ff7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_Z_C_BMSK                                0x1c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_Z_C_SHFT                                   0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_C_BMSK                                   0x380
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_C_SHFT                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_Z_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_CCU_Z_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_MARB_BMSK                                      0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_MARB_CCU3_CGC_CTL_MARB_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000200)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_RMSK                                                 0x3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_CGC_HYST_DCOM_BMSK                                   0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_CGC_HYST_DCOM_SHFT                                     0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_CGC_HYST_COM_BMSK                                      0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_COM_DCOM_CGC_HYST_COM_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000234)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_RMSK                                                 0x77
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_CGC_DELAY_DCOM_BMSK                                  0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_CGC_DELAY_DCOM_SHFT                                   0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_CGC_DELAY_COM_BMSK                                    0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_COM_DCOM_CGC_DELAY_COM_SHFT                                    0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000204)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_RMSK                                                   0x77
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_CGC_CTL_DCOM_MODE_BMSK                                 0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_CGC_CTL_DCOM_MODE_SHFT                                  0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_CGC_CTL_COM_MODE_BMSK                                   0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_COM_DCOM_CGC_CTL_COM_MODE_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000074)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_RMSK                                          0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_RBBM_MODE_BMSK                        0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_RBBM_MODE_SHFT                             0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_RAS_MODE_BMSK                          0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_RAS_MODE_SHFT                              0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE4_BMSK                          0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE4_SHFT                             0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE3_BMSK                           0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE3_SHFT                              0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE2_BMSK                            0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE2_SHFT                              0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE1_BMSK                             0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE1_SHFT                              0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE0_BMSK                              0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TSE_RAS_RBBM_CGC_CTL_TSE_MODE0_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000078)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_RMSK                                          0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_RBBM_BMSK                            0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_RBBM_SHFT                                0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_RAS_BMSK                               0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_RAS_SHFT                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_TSE_1_BMSK                              0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_TSE_1_SHFT                                0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_TSE_0_BMSK                                0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TSE_RAS_RBBM_CGC_HYST_TSE_0_SHFT                                0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000007c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_RMSK                                           0x7777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_RBBM_BMSK                            0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_RBBM_SHFT                               0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_RAS_BMSK                              0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_RAS_SHFT                                0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_TSE_1_BMSK                             0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_TSE_1_SHFT                              0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_TSE_0_BMSK                              0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TSE_RAS_RBBM_CGC_DELAY_TSE_0_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000010)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_RMSK                                                   0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP0_CGC_CTL_TP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000014)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_RMSK                                                   0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP1_CGC_CTL_TP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000018)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_RMSK                                                   0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP2_CGC_CTL_TP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000001c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_RMSK                                                   0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE6_BMSK                                  0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE6_SHFT                                       0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_TP3_CGC_CTL_TP_MODE0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000020)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_RMSK                                                     0x7777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE10_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE10_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE9_BMSK                                     0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE9_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE8_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE8_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE7_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP0_CGC_CTL_TP_MODE7_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000024)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_RMSK                                                     0x7777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE10_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE10_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE9_BMSK                                     0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE9_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE8_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE8_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE7_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP1_CGC_CTL_TP_MODE7_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000028)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_RMSK                                                     0x7777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE10_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE10_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE9_BMSK                                     0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE9_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE8_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE8_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE7_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP2_CGC_CTL_TP_MODE7_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000002c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_RMSK                                                     0x7777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE10_BMSK                                   0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE10_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE9_BMSK                                     0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE9_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE8_BMSK                                      0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE8_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE7_BMSK                                       0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_TP3_CGC_CTL_TP_MODE7_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000030)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_RMSK                                                 0x1ef7bdef
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_5_BMSK                                   0x1e000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_5_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_4_BMSK                                     0xf00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_4_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_3_BMSK                                      0x78000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_3_SHFT                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_2_BMSK                                       0x3c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_2_SHFT                                          0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_1_BMSK                                        0x1e0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_1_SHFT                                          0x5
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP0_CGC_HYST_TP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000034)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_RMSK                                                 0x1ef7bdef
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_5_BMSK                                   0x1e000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_5_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_4_BMSK                                     0xf00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_4_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_3_BMSK                                      0x78000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_3_SHFT                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_2_BMSK                                       0x3c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_2_SHFT                                          0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_1_BMSK                                        0x1e0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_1_SHFT                                          0x5
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP1_CGC_HYST_TP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000038)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_RMSK                                                 0x1ef7bdef
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_5_BMSK                                   0x1e000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_5_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_4_BMSK                                     0xf00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_4_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_3_BMSK                                      0x78000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_3_SHFT                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_2_BMSK                                       0x3c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_2_SHFT                                          0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_1_BMSK                                        0x1e0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_1_SHFT                                          0x5
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP2_CGC_HYST_TP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000003c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_RMSK                                                 0x1ef7bdef
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_5_BMSK                                   0x1e000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_5_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_4_BMSK                                     0xf00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_4_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_3_BMSK                                      0x78000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_3_SHFT                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_2_BMSK                                       0x3c00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_2_SHFT                                          0xa
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_1_BMSK                                        0x1e0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_1_SHFT                                          0x5
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_0_BMSK                                          0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_TP3_CGC_HYST_TP_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000040)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_RMSK                                                  0x777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP0_CGC_DELAY_TP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000044)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_RMSK                                                  0x777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP1_CGC_DELAY_TP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000048)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_RMSK                                                  0x777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP2_CGC_DELAY_TP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000004c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_RMSK                                                  0x777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_5_BMSK                                   0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_5_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_4_BMSK                                    0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_4_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_3_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_3_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_2_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_2_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_1_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_1_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_0_BMSK                                        0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_TP3_CGC_DELAY_TP_0_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000060c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_RMSK                                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_CGC_STATUS_TP_0_BMSK                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP0_CGC_STATUS_TP_0_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000610)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_RMSK                                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_CGC_STATUS_TP_0_BMSK                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP1_CGC_STATUS_TP_0_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000614)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_RMSK                                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_CGC_STATUS_TP_0_BMSK                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP2_CGC_STATUS_TP_0_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000618)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_RMSK                                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_CGC_STATUS_TP_0_BMSK                                    0x7ff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_TP3_CGC_STATUS_TP_0_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000228)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE6_BMSK                               0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE6_SHFT                                    0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE5_BMSK                                0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE5_SHFT                                    0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE4_BMSK                                 0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE4_SHFT                                    0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE3_BMSK                                  0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE3_SHFT                                     0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE2_BMSK                                   0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE2_SHFT                                     0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE1_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE1_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE0_BMSK                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_HLSQ_CGC_CTL_HLSQ_MODE0_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000022c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_RMSK                                                  0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_3_BMSK                                  0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_3_SHFT                                      0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_2_BMSK                                    0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_2_SHFT                                       0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_1_BMSK                                     0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_1_SHFT                                       0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_0_BMSK                                       0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_HLSQ_CGC_HYST_HLSQ_0_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000230)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_RMSK                                                 0x777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_HLSQ_TP_EARLY_CYC_BMSK                           0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_HLSQ_TP_EARLY_CYC_SHFT                               0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_HLSQ_SP_EARLY_CYC_BMSK                            0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_HLSQ_SP_EARLY_CYC_SHFT                               0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_3_BMSK                                  0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_3_SHFT                                     0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_2_BMSK                                   0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_2_SHFT                                     0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_1_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_1_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_0_BMSK                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_HLSQ_CGC_DELAY_HLSQ_0_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000061c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_RMSK                                                    0x7f
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_CGC_STATUS_HLSQ_BMSK                                    0x7f
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_HLSQ_CGC_STATUS_HLSQ_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000050)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_RMSK                                                 0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE7_BMSK                              0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE7_SHFT                                    0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE6_BMSK                               0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE6_SHFT                                    0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE5_BMSK                                0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE5_SHFT                                    0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE4_BMSK                                 0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE4_SHFT                                    0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE3_BMSK                                  0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE3_SHFT                                     0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE2_BMSK                                   0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE2_SHFT                                     0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE1_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE1_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE0_BMSK                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL_UCHE_CGC_CTL_UCHE_MODE0_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000054)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_RMSK                                                0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE15_BMSK                            0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE15_SHFT                                  0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE14_BMSK                             0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE14_SHFT                                  0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE13_BMSK                              0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE13_SHFT                                  0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE12_BMSK                               0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE12_SHFT                                  0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE11_BMSK                                0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE11_SHFT                                   0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE10_BMSK                                 0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE10_SHFT                                   0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE9_BMSK                                   0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE9_SHFT                                    0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE8_BMSK                                    0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL2_UCHE_CGC_CTL_UCHE_MODE8_SHFT                                    0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000058)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_RMSK                                                0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE23_BMSK                            0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE23_SHFT                                  0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE22_BMSK                             0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE22_SHFT                                  0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE21_BMSK                              0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE21_SHFT                                  0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE20_BMSK                               0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE20_SHFT                                  0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE19_BMSK                                0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE19_SHFT                                   0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE18_BMSK                                 0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE18_SHFT                                   0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE17_BMSK                                  0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE17_SHFT                                   0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE16_BMSK                                   0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL3_UCHE_CGC_CTL_UCHE_MODE16_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000005c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_RMSK                                                0x77777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE31_BMSK                            0x70000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE31_SHFT                                  0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE30_BMSK                             0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE30_SHFT                                  0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE29_BMSK                              0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE29_SHFT                                  0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE28_BMSK                               0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE28_SHFT                                  0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE27_BMSK                                0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE27_SHFT                                   0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE26_BMSK                                 0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE26_SHFT                                   0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE25_BMSK                                  0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE25_SHFT                                   0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE24_BMSK                                   0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CTL4_UCHE_CGC_CTL_UCHE_MODE24_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000060)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_RMSK                                                 0xfffffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_6_BMSK                                 0xf000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_6_SHFT                                      0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_5_BMSK                                  0xf00000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_5_SHFT                                      0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_4_BMSK                                   0xf0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_4_SHFT                                      0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_3_BMSK                                    0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_3_SHFT                                       0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_2_BMSK                                     0xf00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_2_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_1_BMSK                                      0xf0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_1_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_0_BMSK                                       0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_UCHE_CGC_HYST_UCHE_0_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000064)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_RMSK                                                0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_6_BMSK                               0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_6_SHFT                                    0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_5_BMSK                                0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_5_SHFT                                    0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_4_BMSK                                 0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_4_SHFT                                    0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_3_BMSK                                  0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_3_SHFT                                     0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_2_BMSK                                   0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_2_SHFT                                     0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_1_BMSK                                    0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_1_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_0_BMSK                                     0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_UCHE_CGC_DELAY_UCHE_0_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000620)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_CGC_STATUS_UCHE_BMSK                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_UCHE_CGC_STATUS_UCHE_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000218)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_RMSK                                          0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_L1_BMSK                              0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_L1_SHFT                                  0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_CCU_BMSK                               0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_CCU_SHFT                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_MARB_BMSK                               0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_MARB_SHFT                                 0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_RB_BMSK                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU0_CGC_HYST_RB_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000021c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_RMSK                                          0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_L1_BMSK                              0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_L1_SHFT                                  0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_CCU_BMSK                               0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_CCU_SHFT                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_MARB_BMSK                               0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_MARB_SHFT                                 0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_RB_BMSK                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU1_CGC_HYST_RB_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000220)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_RMSK                                          0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_L1_BMSK                              0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_L1_SHFT                                  0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_CCU_BMSK                               0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_CCU_SHFT                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_MARB_BMSK                               0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_MARB_SHFT                                 0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_RB_BMSK                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU2_CGC_HYST_RB_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000224)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_RMSK                                          0x3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_L1_BMSK                              0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_L1_SHFT                                  0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_CCU_BMSK                               0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_CCU_SHFT                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_MARB_BMSK                               0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_MARB_SHFT                                 0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_RB_BMSK                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_RB_MARB_CCU3_CGC_HYST_RB_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000070)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_RMSK                                                  0xf3cf3cf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VSC_BMSK                                     0xf000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VSC_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_TESS_BMSK                                     0x3c0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_TESS_SHFT                                         0x12
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VFD_BMSK                                        0xf000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VFD_SHFT                                           0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VPC_BMSK                                         0x3c0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_VPC_SHFT                                           0x6
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_PC_BMSK                                            0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_HYST_GPC_CGC_HYST_PC_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000068)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_RMSK                                                  0x7777777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VSC_BMSK                                     0x7000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VSC_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_TESS_BMSK                                     0x700000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_TESS_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VFD_BMSK                                       0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VFD_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_GL_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_GL_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_LM_BMSK                                      0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_LM_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_SO_BMSK                                       0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_VPC_SO_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_PC_BMSK                                            0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_MODE_GPC_CGC_MODE_PC_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000006c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_RMSK                                                   0x77777
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VSC_BMSK                                     0x70000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VSC_SHFT                                        0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_TESS_BMSK                                     0x7000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_TESS_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VFD_BMSK                                       0x700
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VFD_SHFT                                         0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VPC_BMSK                                        0x70
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_VPC_SHFT                                         0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_PC_BMSK                                          0x7
#define HWIO_MMSS_OXILI_RBBM_CLOCK_DELAY_GPC_CGC_DELAY_PC_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000608)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RBBM_BMSK                                      0x80000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RBBM_SHFT                                            0x1f
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_HM_BMSK                                        0x40000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_HM_SHFT                                              0x1e
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VBIF_BMSK                                      0x20000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VBIF_SHFT                                            0x1d
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_UCHE_BMSK                                      0x10000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_UCHE_SHFT                                            0x1c
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VSC_BMSK                                        0x8000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VSC_SHFT                                             0x1b
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RAS_BMSK                                        0x4000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RAS_SHFT                                             0x1a
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_TSE_BMSK                                        0x2000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_TSE_SHFT                                             0x19
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_HLSQ_BMSK                                       0x1000000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_HLSQ_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VPC_BMSK                                         0x800000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VPC_SHFT                                             0x17
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VFD_BMSK                                         0x400000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_VFD_SHFT                                             0x16
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_PC_BMSK                                          0x200000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_PC_SHFT                                              0x15
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_CP_BMSK                                          0x100000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_CP_SHFT                                              0x14
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RBMARB_BMSK                                       0xf0000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_RBMARB_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_SPTP_BMSK                                          0xffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_STATUS_CGC_CTL_SPTP_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_ADDR                                                    (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000084)
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_RMSK                                                         0xfff
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_ADDR, HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_IN)
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_SP_LP_CNT_BMSK                                               0xfff
#define HWIO_MMSS_OXILI_RBBM_SP_HYST_CNT_SP_LP_CNT_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000088)
#define HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_RMSK                                                          0x1
#define HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_GL_SW_RESET_BMSK                                              0x1
#define HWIO_MMSS_OXILI_RBBM_SW_RESET_CMD_GL_SW_RESET_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000114)
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_RMSK                                             0x7fffffff
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_HM_SW_RESET_BMSK                                 0x40000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_HM_SW_RESET_SHFT                                       0x1e
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_UCHE_SW_RESET_BMSK                               0x20000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_UCHE_SW_RESET_SHFT                                     0x1d
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VSC_SW_RESET_BMSK                                0x10000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VSC_SW_RESET_SHFT                                      0x1c
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_RAS_SW_RESET_BMSK                                 0x8000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_RAS_SW_RESET_SHFT                                      0x1b
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TSE_SW_RESET_BMSK                                 0x4000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TSE_SW_RESET_SHFT                                      0x1a
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_HLSQ_SW_RESET_BMSK                                0x2000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_HLSQ_SW_RESET_SHFT                                     0x19
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VPC_SW_RESET_BMSK                                 0x1000000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VPC_SW_RESET_SHFT                                      0x18
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VFD_SW_RESET_BMSK                                  0x800000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_VFD_SW_RESET_SHFT                                      0x17
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TESS_SW_RESET_BMSK                                 0x400000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TESS_SW_RESET_SHFT                                     0x16
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_PC_SW_RESET_BMSK                                   0x200000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_PC_SW_RESET_SHFT                                       0x15
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_CP_SW_RESET_BMSK                                   0x100000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_CP_SW_RESET_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_CCU_SW_RESET_BMSK                                   0xf0000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_CCU_SW_RESET_SHFT                                      0x10
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_MARB_SW_RESET_BMSK                                   0xf000
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_MARB_SW_RESET_SHFT                                      0xc
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_RB_SW_RESET_BMSK                                      0xf00
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_RB_SW_RESET_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TP_SW_RESET_BMSK                                       0xf0
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_TP_SW_RESET_SHFT                                        0x4
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_SP_SW_RESET_BMSK                                        0xf
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD_SP_SW_RESET_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000118)
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_RMSK                                                 0x3ff
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_SPTP_GDSC_SW_RESET_BMSK                              0x200
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_SPTP_GDSC_SW_RESET_SHFT                                0x9
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_VBIF_SW_RESET_BMSK                                   0x1f0
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_VBIF_SW_RESET_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_RBBM_SW_RESET_BMSK                                     0x8
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_RBBM_SW_RESET_SHFT                                     0x3
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_DPM_SW_RESET_BMSK                                      0x4
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_DPM_SW_RESET_SHFT                                      0x2
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_DCOM_SW_RESET_BMSK                                     0x2
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_DCOM_SW_RESET_SHFT                                     0x1
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_COM_SW_RESET_BMSK                                      0x1
#define HWIO_MMSS_OXILI_RBBM_BLOCK_SW_RESET_CMD2_COM_SW_RESET_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000011c)
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_RMSK                                                        0x3ff
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_ADDR, HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_RMSK)
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_IN)
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_CYCLES_BMSK                                                 0x3ff
#define HWIO_MMSS_OXILI_RBBM_RESET_CYCLES_CYCLES_SHFT                                                   0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000008c)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_RMSK                                                             0x31
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL0_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_CTL0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_CTL0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_AHB_CTL0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_AHB_CTL0_IN)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_AHB_RD_ERROR_EN_BMSK                                             0x20
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_AHB_RD_ERROR_EN_SHFT                                              0x5
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_AHB_WR_ERROR_EN_BMSK                                             0x10
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_AHB_WR_ERROR_EN_SHFT                                              0x4
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_DEFAULT_SLAVE_RESP_BMSK                                           0x1
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL0_DEFAULT_SLAVE_RESP_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000090)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_RMSK                                                       0xf7ffffff
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_CTL1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_AHB_CTL1_IN)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_EN_BMSK                                       0x80000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_EN_SHFT                                             0x1f
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_REL_BMSK                                      0x40000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_REL_SHFT                                            0x1e
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_EN_BMSK                                        0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_EN_SHFT                                              0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_REL_BMSK                                       0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_REL_SHFT                                             0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ERROR_STATUS_EN_BMSK                                        0x4000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ERROR_STATUS_EN_SHFT                                             0x1a
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_EN_BMSK                                             0x2000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_EN_SHFT                                                  0x19
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_BUS_REL_BMSK                                        0x1000000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_BUS_REL_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_VAL_BMSK                                        0xff0000
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_PFP_SPLIT_TO_VAL_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_VAL_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_ME_SPLIT_TO_VAL_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_VAL_BMSK                                                 0xff
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL1_TXFR_TO_VAL_SHFT                                                  0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000010c)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_RMSK                                                            0x3ff
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_CTL2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_AHB_CTL2_IN)
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_EN_BMSK                                            0x200
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_EN_SHFT                                              0x9
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_REL_BMSK                                           0x100
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_REL_SHFT                                             0x8
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_VAL_BMSK                                            0xff
#define HWIO_MMSS_OXILI_RBBM_AHB_CTL2_ETS_SPLIT_TO_VAL_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_ADDR                                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000094)
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RMSK                                                              0x1f
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_AHB_ERROR_BMSK                                              0x10
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_AHB_ERROR_SHFT                                               0x4
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_ETS_SPLIT_BMSK                                               0x8
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_ETS_SPLIT_SHFT                                               0x3
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_PFP_SPLIT_BMSK                                               0x4
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_PFP_SPLIT_SHFT                                               0x2
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_ME_SPLIT_BMSK                                                0x2
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_ME_SPLIT_SHFT                                                0x1
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_TXFR_TIMEOUT_BMSK                                            0x1
#define HWIO_MMSS_OXILI_RBBM_AHB_CMD_RESET_TXFR_TIMEOUT_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000624)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_RMSK                                                     0xfffffeff
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_AHB_REQUESTS_BMSK                                        0xf0000000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_AHB_REQUESTS_SHFT                                              0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_AHB_GRANTS_BMSK                                           0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_AHB_GRANTS_SHFT                                                0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_ETS_SPLIT_BMSK                                          0x800000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_ETS_SPLIT_SHFT                                              0x17
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_PFP_SPLIT_BMSK                                          0x400000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_PFP_SPLIT_SHFT                                              0x16
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_ME_SPLIT_BMSK                                           0x200000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_CP_ME_SPLIT_SHFT                                               0x15
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_MID_BMSK                                                   0x180000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_MID_SHFT                                                       0x13
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_SID_BMSK                                                    0x78000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_SID_SHFT                                                        0xf
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_BUSY_BMSK                                                    0x4000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_BUSY_SHFT                                                       0xe
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_WRITE_BMSK                                                   0x2000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_WRITE_SHFT                                                      0xd
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_STALLED_BMSK                                                 0x1000
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_STALLED_SHFT                                                    0xc
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_BMSK                                                     0x800
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_SHFT                                                       0xb
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_SPLIT_BMSK                                               0x400
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_SPLIT_SHFT                                                 0xa
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_ERROR_BMSK                                               0x200
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_TXFR_ERROR_SHFT                                                 0x9
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_STALL_TIME_BMSK                                                0xff
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS_STALL_TIME_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_ADDR                                                    (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000628)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_RMSK                                                           0xf
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_HMASTLOCK_BMSK                                                 0x8
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_HMASTLOCK_SHFT                                                 0x3
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_AHB_HLOCKS_BMSK                                                0x7
#define HWIO_MMSS_OXILI_RBBM_AHB_STATUS2_AHB_HLOCKS_SHFT                                                0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000062c)
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_RMSK                                        0x3f3fffff
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_TIMEOUT_BMSK                                0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_TIMEOUT_SHFT                                      0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_WRITE_BMSK                                  0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_WRITE_SHFT                                        0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_SLAVE_ID_BMSK                                0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_SLAVE_ID_SHFT                                     0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_MASTER_ID_BMSK                                0x300000
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_MASTER_ID_SHFT                                    0x14
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_HADDR_BMSK                                     0xfffff
#define HWIO_MMSS_OXILI_RBBM_AHB_TXFR_TIMEOUT_STATUS_HADDR_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000630)
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_RMSK                                            0x7f3fffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_SPLIT_BMSK                                      0x40000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_SPLIT_SHFT                                            0x1e
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_TIMEOUT_BMSK                                    0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_TIMEOUT_SHFT                                          0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_WRITE_BMSK                                      0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_WRITE_SHFT                                            0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_SLAVE_ID_BMSK                                    0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_SLAVE_ID_SHFT                                         0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_MASTER_ID_BMSK                                    0x300000
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_MASTER_ID_SHFT                                        0x14
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_HADDR_BMSK                                         0xfffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ME_SPLIT_STATUS_HADDR_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000634)
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_RMSK                                           0x7f3fffff
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_SPLIT_BMSK                                     0x40000000
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_SPLIT_SHFT                                           0x1e
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_TIMEOUT_BMSK                                   0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_TIMEOUT_SHFT                                         0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_WRITE_BMSK                                     0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_WRITE_SHFT                                           0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_SLAVE_ID_BMSK                                   0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_SLAVE_ID_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_MASTER_ID_BMSK                                   0x300000
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_MASTER_ID_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_HADDR_BMSK                                        0xfffff
#define HWIO_MMSS_OXILI_RBBM_AHB_PFP_SPLIT_STATUS_HADDR_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000638)
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_RMSK                                           0x7f3fffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_SPLIT_BMSK                                     0x40000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_SPLIT_SHFT                                           0x1e
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_TIMEOUT_BMSK                                   0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_TIMEOUT_SHFT                                         0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_WRITE_BMSK                                     0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_WRITE_SHFT                                           0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_SLAVE_ID_BMSK                                   0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_SLAVE_ID_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_MASTER_ID_BMSK                                   0x300000
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_MASTER_ID_SHFT                                       0x14
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_HADDR_BMSK                                        0xfffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ETS_SPLIT_STATUS_HADDR_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000063c)
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_RMSK                                               0x3f3fffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_TIMEOUT_BMSK                                       0x20000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_TIMEOUT_SHFT                                             0x1d
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_WRITE_BMSK                                         0x10000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_WRITE_SHFT                                               0x1c
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_SLAVE_ID_BMSK                                       0xf000000
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_SLAVE_ID_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_MASTER_ID_BMSK                                       0x300000
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_MASTER_ID_SHFT                                           0x14
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_HADDR_BMSK                                            0xfffff
#define HWIO_MMSS_OXILI_RBBM_AHB_ERROR_STATUS_HADDR_SHFT                                                0x0

#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000098)
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_RMSK                                                  0x3
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_BLOCK_SEL_BMSK                                        0x3
#define HWIO_MMSS_OXILI_RBBM_RB_SUB_BLOCK_SEL_CTL_BLOCK_SEL_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000009c)
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_RMSK                                                      0x10001
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_DST_STATE_ID_BMSK                                         0x10000
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_DST_STATE_ID_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_SRC_STATE_ID_BMSK                                             0x1
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_CMD_SRC_STATE_ID_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000640)
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_RMSK                                                   0x10001
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_DST_STATE_ID_BMSK                                      0x10000
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_DST_STATE_ID_SHFT                                         0x10
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_SRC_STATE_ID_BMSK                                          0x1
#define HWIO_MMSS_OXILI_RBBM_GFX_COPY_STATUS_SRC_STATE_ID_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000a0)
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_RMSK                                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ADDR, HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_RMSK)
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_IN)
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ACC_63_32_BMSK                                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_RAM_ACC_63_32_ACC_63_32_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000a4)
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_RMSK                                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPR0_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_GPR0_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPR0_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_GPR0_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_GPR0_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_GPR0_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_NA_BMSK                                                    0xffffff80
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_NA_SHFT                                                           0x7
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_HLSQ_ADDR_CHANGE_ENABLE_BMSK                                     0x40
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_HLSQ_ADDR_CHANGE_ENABLE_SHFT                                      0x6
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_DISABLE_CLK_OFF_SPTP_BMSK                                        0x20
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_DISABLE_CLK_OFF_SPTP_SHFT                                         0x5
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_REQ_AFTER_DATA_DIS_BMSK                                          0x10
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_REQ_AFTER_DATA_DIS_SHFT                                           0x4
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_PRIORLVL_FLAG_BMSK                                                0xc
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_PRIORLVL_FLAG_SHFT                                                0x2
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_REQPRIOR_C_FLAG_BMSK                                              0x3
#define HWIO_MMSS_OXILI_RBBM_GPR0_CTL_REQPRIOR_C_FLAG_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000a8)
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_RMSK                                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPR1_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_GPR1_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPR1_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_GPR1_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_GPR1_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_GPR1_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_NA_BMSK                                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_GPR1_CTL_NA_SHFT                                                           0x0

#define HWIO_MMSS_OXILI_RBBM_STATUS_ADDR                                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000644)
#define HWIO_MMSS_OXILI_RBBM_STATUS_RMSK                                                         0xfffff8bf
#define HWIO_MMSS_OXILI_RBBM_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_BMSK                                        0x80000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_SHFT                                              0x1f
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_CP_BMSK                                     0x40000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_CP_SHFT                                           0x1e
#define HWIO_MMSS_OXILI_RBBM_STATUS_HLSQ_BUSY_BMSK                                               0x20000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_HLSQ_BUSY_SHFT                                                     0x1d
#define HWIO_MMSS_OXILI_RBBM_STATUS_ARB_BUSY_BMSK                                                0x10000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_ARB_BUSY_SHFT                                                      0x1c
#define HWIO_MMSS_OXILI_RBBM_STATUS_VSC_BUSY_BMSK                                                 0x8000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_VSC_BUSY_SHFT                                                      0x1b
#define HWIO_MMSS_OXILI_RBBM_STATUS_MARB_BUSY_BMSK                                                0x4000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_MARB_BUSY_SHFT                                                     0x1a
#define HWIO_MMSS_OXILI_RBBM_STATUS_TPL1_BUSY_BMSK                                                0x2000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_TPL1_BUSY_SHFT                                                     0x19
#define HWIO_MMSS_OXILI_RBBM_STATUS_SP_BUSY_BMSK                                                  0x1000000
#define HWIO_MMSS_OXILI_RBBM_STATUS_SP_BUSY_SHFT                                                       0x18
#define HWIO_MMSS_OXILI_RBBM_STATUS_UCHE_BUSY_BMSK                                                 0x800000
#define HWIO_MMSS_OXILI_RBBM_STATUS_UCHE_BUSY_SHFT                                                     0x17
#define HWIO_MMSS_OXILI_RBBM_STATUS_VPC_BUSY_BMSK                                                  0x400000
#define HWIO_MMSS_OXILI_RBBM_STATUS_VPC_BUSY_SHFT                                                      0x16
#define HWIO_MMSS_OXILI_RBBM_STATUS_VFD_BUSY_BMSK                                                  0x200000
#define HWIO_MMSS_OXILI_RBBM_STATUS_VFD_BUSY_SHFT                                                      0x15
#define HWIO_MMSS_OXILI_RBBM_STATUS_TESS_BUSY_BMSK                                                 0x100000
#define HWIO_MMSS_OXILI_RBBM_STATUS_TESS_BUSY_SHFT                                                     0x14
#define HWIO_MMSS_OXILI_RBBM_STATUS_PC_VSD_BUSY_BMSK                                                0x80000
#define HWIO_MMSS_OXILI_RBBM_STATUS_PC_VSD_BUSY_SHFT                                                   0x13
#define HWIO_MMSS_OXILI_RBBM_STATUS_PC_DCALL_BUSY_BMSK                                              0x40000
#define HWIO_MMSS_OXILI_RBBM_STATUS_PC_DCALL_BUSY_SHFT                                                 0x12
#define HWIO_MMSS_OXILI_RBBM_STATUS_RB_BUSY_BMSK                                                    0x20000
#define HWIO_MMSS_OXILI_RBBM_STATUS_RB_BUSY_SHFT                                                       0x11
#define HWIO_MMSS_OXILI_RBBM_STATUS_RAS_BUSY_BMSK                                                   0x10000
#define HWIO_MMSS_OXILI_RBBM_STATUS_RAS_BUSY_SHFT                                                      0x10
#define HWIO_MMSS_OXILI_RBBM_STATUS_TSE_BUSY_BMSK                                                    0x8000
#define HWIO_MMSS_OXILI_RBBM_STATUS_TSE_BUSY_SHFT                                                       0xf
#define HWIO_MMSS_OXILI_RBBM_STATUS_VBIF_BUSY_BMSK                                                   0x4000
#define HWIO_MMSS_OXILI_RBBM_STATUS_VBIF_BUSY_SHFT                                                      0xe
#define HWIO_MMSS_OXILI_RBBM_STATUS_DPM_BUSY_BMSK                                                    0x2000
#define HWIO_MMSS_OXILI_RBBM_STATUS_DPM_BUSY_SHFT                                                       0xd
#define HWIO_MMSS_OXILI_RBBM_STATUS_DCOM_BUSY_BMSK                                                   0x1000
#define HWIO_MMSS_OXILI_RBBM_STATUS_DCOM_BUSY_SHFT                                                      0xc
#define HWIO_MMSS_OXILI_RBBM_STATUS_COM_BUSY_BMSK                                                     0x800
#define HWIO_MMSS_OXILI_RBBM_STATUS_COM_BUSY_SHFT                                                       0xb
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_HYST_BMSK                                         0x80
#define HWIO_MMSS_OXILI_RBBM_STATUS_GPU_BUSY_IGN_AHB_HYST_SHFT                                          0x7
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_BUSY_IGN_HYST_BMSK                                              0x20
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_BUSY_IGN_HYST_SHFT                                               0x5
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_NRT_BUSY_BMSK                                                   0x10
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_NRT_BUSY_SHFT                                                    0x4
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_ETS_BUSY_BMSK                                                    0x8
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_ETS_BUSY_SHFT                                                    0x3
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_PFP_BUSY_BMSK                                                    0x4
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_PFP_BUSY_SHFT                                                    0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_ME_BUSY_BMSK                                                     0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS_CP_ME_BUSY_SHFT                                                     0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS_HI_BUSY_BMSK                                                        0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS_HI_BUSY_SHFT                                                        0x0

#define HWIO_MMSS_OXILI_RBBM_STATUS1_ADDR                                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000648)
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RMSK                                                         0xf0f0f0f
#define HWIO_MMSS_OXILI_RBBM_STATUS1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS1_ADDR, HWIO_MMSS_OXILI_RBBM_STATUS1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_STATUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB3_BUSY_BMSK                                              0x8000000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB3_BUSY_SHFT                                                   0x1b
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB2_BUSY_BMSK                                              0x4000000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB2_BUSY_SHFT                                                   0x1a
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB1_BUSY_BMSK                                              0x2000000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB1_BUSY_SHFT                                                   0x19
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB0_BUSY_BMSK                                              0x1000000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_MARB0_BUSY_SHFT                                                   0x18
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB3_BUSY_BMSK                                                  0x80000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB3_BUSY_SHFT                                                     0x13
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB2_BUSY_BMSK                                                  0x40000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB2_BUSY_SHFT                                                     0x12
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB1_BUSY_BMSK                                                  0x20000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB1_BUSY_SHFT                                                     0x11
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB0_BUSY_BMSK                                                  0x10000
#define HWIO_MMSS_OXILI_RBBM_STATUS1_RB0_BUSY_SHFT                                                     0x10
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL13_BUSY_BMSK                                                  0x800
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL13_BUSY_SHFT                                                    0xb
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL12_BUSY_BMSK                                                  0x400
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL12_BUSY_SHFT                                                    0xa
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL11_BUSY_BMSK                                                  0x200
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL11_BUSY_SHFT                                                    0x9
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL10_BUSY_BMSK                                                  0x100
#define HWIO_MMSS_OXILI_RBBM_STATUS1_TPL10_BUSY_SHFT                                                    0x8
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP3_BUSY_BMSK                                                      0x8
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP3_BUSY_SHFT                                                      0x3
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP2_BUSY_BMSK                                                      0x4
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP2_BUSY_SHFT                                                      0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP1_BUSY_BMSK                                                      0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP1_BUSY_SHFT                                                      0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP0_BUSY_BMSK                                                      0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS1_SP0_BUSY_SHFT                                                      0x0

#define HWIO_MMSS_OXILI_RBBM_STATUS2_ADDR                                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000064c)
#define HWIO_MMSS_OXILI_RBBM_STATUS2_RMSK                                                        0x800f0f0f
#define HWIO_MMSS_OXILI_RBBM_STATUS2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS2_ADDR, HWIO_MMSS_OXILI_RBBM_STATUS2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_STATUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_STATUS2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_STATUS2_LAZY_BUSY_BMSK                                              0x80000000
#define HWIO_MMSS_OXILI_RBBM_STATUS2_LAZY_BUSY_SHFT                                                    0x1f
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU3_BUSY_BMSK                                                 0x80000
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU3_BUSY_SHFT                                                    0x13
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU2_BUSY_BMSK                                                 0x40000
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU2_BUSY_SHFT                                                    0x12
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU1_BUSY_BMSK                                                 0x20000
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU1_BUSY_SHFT                                                    0x11
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU0_BUSY_BMSK                                                 0x10000
#define HWIO_MMSS_OXILI_RBBM_STATUS2_CCU0_BUSY_SHFT                                                    0x10
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP3_FS_BUSY_BMSK                                                 0x800
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP3_FS_BUSY_SHFT                                                   0xb
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP2_FS_BUSY_BMSK                                                 0x400
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP2_FS_BUSY_SHFT                                                   0xa
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP1_FS_BUSY_BMSK                                                 0x200
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP1_FS_BUSY_SHFT                                                   0x9
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP0_FS_BUSY_BMSK                                                 0x100
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP0_FS_BUSY_SHFT                                                   0x8
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP3_VS_BUSY_BMSK                                                   0x8
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP3_VS_BUSY_SHFT                                                   0x3
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP2_VS_BUSY_BMSK                                                   0x4
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP2_VS_BUSY_SHFT                                                   0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP1_VS_BUSY_BMSK                                                   0x2
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP1_VS_BUSY_SHFT                                                   0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP0_VS_BUSY_BMSK                                                   0x1
#define HWIO_MMSS_OXILI_RBBM_STATUS2_SP0_VS_BUSY_SHFT                                                   0x0

#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000ac)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_RMSK                                                 0xff
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_WAIT_IDLE_CLOCKS_BMSK                                0xff
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL_WAIT_IDLE_CLOCKS_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006e0)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_ADDR, HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_IN)
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_GPU_IDLE_HYST_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_WAIT_IDLE_CLOCKS_CTL2_GPU_IDLE_HYST_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000b0)
#define HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_RMSK                                                 0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_WAIT_GPU_IDLE_BMSK                                   0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_FOR_GPU_IDLE_CMD_WAIT_GPU_IDLE_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000b4)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_RMSK                                                   0x1fffff
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_GPU_IDLE_BMSK                                     0x100000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_GPU_IDLE_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_DPM_IDLE_BMSK                                      0x80000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_DPM_IDLE_SHFT                                         0x13
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_DCOM_IDLE_BMSK                                     0x40000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_DCOM_IDLE_SHFT                                        0x12
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_COM_IDLE_BMSK                                      0x20000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_COM_IDLE_SHFT                                         0x11
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_CCU_IDLE_BMSK                                      0x10000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_CCU_IDLE_SHFT                                         0x10
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_HLSQ_IDLE_BMSK                                      0x8000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_HLSQ_IDLE_SHFT                                         0xf
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_ARB_IDLE_BMSK                                       0x4000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_ARB_IDLE_SHFT                                          0xe
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VSC_IDLE_BMSK                                       0x2000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VSC_IDLE_SHFT                                          0xd
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_MARB_IDLE_BMSK                                      0x1000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_MARB_IDLE_SHFT                                         0xc
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TPL1_IDLE_BMSK                                       0x800
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TPL1_IDLE_SHFT                                         0xb
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_SP_IDLE_BMSK                                         0x400
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_SP_IDLE_SHFT                                           0xa
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_UCHE_IDLE_BMSK                                       0x200
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_UCHE_IDLE_SHFT                                         0x9
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VPC_IDLE_BMSK                                        0x100
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VPC_IDLE_SHFT                                          0x8
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VFD_IDLE_BMSK                                         0x80
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VFD_IDLE_SHFT                                          0x7
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TESS_IDLE_BMSK                                        0x40
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TESS_IDLE_SHFT                                         0x6
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_PC_VSD_IDLE_BMSK                                      0x20
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_PC_VSD_IDLE_SHFT                                       0x5
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_PC_DCALL_IDLE_BMSK                                    0x10
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_PC_DCALL_IDLE_SHFT                                     0x4
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_RB_IDLE_BMSK                                           0x8
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_RB_IDLE_SHFT                                           0x3
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_RAS_IDLE_BMSK                                          0x4
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_RAS_IDLE_SHFT                                          0x2
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TSE_IDLE_BMSK                                          0x2
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_TSE_IDLE_SHFT                                          0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VBIF_IDLE_BMSK                                         0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CMD_WAIT_VBIF_IDLE_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000b8)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RMSK                                                   0x1fffff
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_GPU_IDLE_BMSK                                          0x100000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_GPU_IDLE_SHFT                                              0x14
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_DPM_IDLE_BMSK                                           0x80000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_DPM_IDLE_SHFT                                              0x13
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_DCOM_IDLE_BMSK                                          0x40000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_DCOM_IDLE_SHFT                                             0x12
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_COM_IDLE_BMSK                                           0x20000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_COM_IDLE_SHFT                                              0x11
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_CCU_IDLE_BMSK                                           0x10000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_CCU_IDLE_SHFT                                              0x10
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_HLSQ_IDLE_BMSK                                           0x8000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_HLSQ_IDLE_SHFT                                              0xf
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ARB_IDLE_BMSK                                            0x4000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_ARB_IDLE_SHFT                                               0xe
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VSC_IDLE_BMSK                                            0x2000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VSC_IDLE_SHFT                                               0xd
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_MARB_IDLE_BMSK                                           0x1000
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_MARB_IDLE_SHFT                                              0xc
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TPL1_IDLE_BMSK                                            0x800
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TPL1_IDLE_SHFT                                              0xb
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_SP_IDLE_BMSK                                              0x400
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_SP_IDLE_SHFT                                                0xa
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_UCHE_IDLE_BMSK                                            0x200
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_UCHE_IDLE_SHFT                                              0x9
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VPC_IDLE_BMSK                                             0x100
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VPC_IDLE_SHFT                                               0x8
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VFD_IDLE_BMSK                                              0x80
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VFD_IDLE_SHFT                                               0x7
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TESS_IDLE_BMSK                                             0x40
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TESS_IDLE_SHFT                                              0x6
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_PC_VSD_IDLE_BMSK                                           0x20
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_PC_VSD_IDLE_SHFT                                            0x5
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_PC_DCALL_IDLE_BMSK                                         0x10
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_PC_DCALL_IDLE_SHFT                                          0x4
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RB_IDLE_BMSK                                                0x8
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RB_IDLE_SHFT                                                0x3
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RAS_IDLE_BMSK                                               0x4
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_RAS_IDLE_SHFT                                               0x2
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TSE_IDLE_BMSK                                               0x2
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_TSE_IDLE_SHFT                                               0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VBIF_IDLE_BMSK                                              0x1
#define HWIO_MMSS_OXILI_RBBM_WAIT_UNTIL_CTL_VBIF_IDLE_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000bc)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_RMSK                                         0x4fffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ENABLE_HANG_INT_BMSK                         0x40000000
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_ENABLE_HANG_INT_SHFT                               0x1e
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_HANG_COUNT_BMSK                               0xfffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_INT_CTL_HANG_COUNT_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000c0)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_INTERFACE_HANG_MASK_BMSK                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL0_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000c4)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_INTERFACE_HANG_MASK_BMSK                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL1_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000c8)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_INTERFACE_HANG_MASK_BMSK                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL2_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000cc)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_INTERFACE_HANG_MASK_BMSK                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL3_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000d0)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_INTERFACE_HANG_MASK_BMSK                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL4_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000248)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_RMSK                                           0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_IN)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_INTERFACE_HANG_MASK_BMSK                       0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_HANG_MASK_CTL5_INTERFACE_HANG_MASK_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000650)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_INTERFACE_SRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS0_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000658)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_INTERFACE_SRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS1_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000660)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_INTERFACE_SRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS2_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000668)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_INTERFACE_SRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS3_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000670)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_INTERFACE_SRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS4_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000678)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_RMSK                                             0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_INTERFACE_SRDY_BMSK                              0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_SRDY_STATUS5_INTERFACE_SRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000654)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_INTERFACE_RRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS0_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000065c)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_INTERFACE_RRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS1_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000664)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_INTERFACE_RRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS2_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000066c)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_INTERFACE_RRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS3_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000674)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_INTERFACE_RRDY_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS4_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000067c)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_RMSK                                             0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_ADDR, HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_INTERFACE_RRDY_BMSK                              0x1fff
#define HWIO_MMSS_OXILI_RBBM_INTERFACE_RRDY_STATUS5_INTERFACE_RRDY_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_ADDR                                                    (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000d4)
#define HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_RMSK                                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_INT_SET_CMD_BMSK                                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_SET_CMD_INT_SET_CMD_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000d8)
#define HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_RMSK                                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_INT_CLEAR_CMD_BMSK                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_CLEAR_CMD_INT_CLEAR_CMD_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005f0)
#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_RMSK                                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_INT_UNMASKED_STATUS_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_UNMASKED_STATUS_INT_UNMASKED_STATUS_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000dc)
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_0_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_INT_0_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_0_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_0_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INT_0_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INT_0_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_INT_0_MASK_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_0_MASK_INT_0_MASK_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005f4)
#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_INT_0_STATUS_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_0_STATUS_INT_0_STATUS_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000e0)
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_1_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_INT_1_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_1_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_1_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INT_1_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INT_1_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_INT_1_MASK_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_1_MASK_INT_1_MASK_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005f8)
#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_INT_1_STATUS_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_1_STATUS_INT_1_STATUS_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000e4)
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_2_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_INT_2_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_2_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_2_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INT_2_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INT_2_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_INT_2_MASK_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_2_MASK_INT_2_MASK_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005fc)
#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_INT_2_STATUS_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_2_STATUS_INT_2_STATUS_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000e8)
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_3_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_INT_3_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_3_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_3_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INT_3_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INT_3_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_INT_3_MASK_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_3_MASK_INT_3_MASK_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000600)
#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_INT_3_STATUS_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_3_STATUS_INT_3_STATUS_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000ec)
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_RMSK                                                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_4_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_INT_4_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_4_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_INT_4_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_INT_4_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_INT_4_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_INT_4_MASK_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_4_MASK_INT_4_MASK_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000604)
#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_RMSK                                                   0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_INT_4_STATUS_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_INT_4_STATUS_INT_4_STATUS_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000110)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_RMSK                                                        0x7f
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_ADDR, HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_RMSK)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_IN)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_DEBUG_SEL_INT_BMSK                                          0x7f
#define HWIO_MMSS_OXILI_RBBM_DEBUG_SEL_INT_DEBUG_SEL_INT_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000f0)
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_RMSK                                             0xfff0fff
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C5_BMSK                                 0xc000000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C5_SHFT                                      0x1a
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C4_BMSK                                 0x3000000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C4_SHFT                                      0x18
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C3_BMSK                                  0xc00000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C3_SHFT                                      0x16
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C2_BMSK                                  0x300000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C2_SHFT                                      0x14
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C1_BMSK                                   0xc0000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C1_SHFT                                      0x12
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C0_BMSK                                   0x30000
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_PRIORLVL_C0_SHFT                                      0x10
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C5_BMSK                                     0xc00
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C5_SHFT                                       0xa
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C4_BMSK                                     0x300
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C4_SHFT                                       0x8
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C3_BMSK                                      0xc0
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C3_SHFT                                       0x6
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C2_BMSK                                      0x30
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C2_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C1_BMSK                                       0xc
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C1_SHFT                                       0x2
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C0_BMSK                                       0x3
#define HWIO_MMSS_OXILI_RBBM_VBIF_CLIENT_QOS_CTL_REQPRIOR_C0_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000100)
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_RMSK                                                      0x1
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_ADDR, HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_IN)
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_SP_RB_UAV_CG_SEL_BMSK                                     0x1
#define HWIO_MMSS_OXILI_RBBM_SP_RB_UAV_CG_SEL_SP_RB_UAV_CG_SEL_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ADDR                                                    (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005c0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_RMSK                                                           0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ENABLE_BMSK                                                    0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CTL_ENABLE_SHFT                                                    0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005c4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_3_BMSK                                   0x80000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_3_SHFT                                         0x1f
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_2_BMSK                                   0x40000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_2_SHFT                                         0x1e
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_1_BMSK                                   0x20000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_1_SHFT                                         0x1d
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_0_BMSK                                   0x10000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_HLSQ_0_SHFT                                         0x1c
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_7_BMSK                                     0x8000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_7_SHFT                                          0x1b
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_6_BMSK                                     0x4000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_6_SHFT                                          0x1a
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_5_BMSK                                     0x2000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_5_SHFT                                          0x19
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_4_BMSK                                     0x1000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_4_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_3_BMSK                                      0x800000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_3_SHFT                                          0x17
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_2_BMSK                                      0x400000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_2_SHFT                                          0x16
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_1_BMSK                                      0x200000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_1_SHFT                                          0x15
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_0_BMSK                                      0x100000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_VFD_0_SHFT                                          0x14
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_7_BMSK                                        0x80000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_7_SHFT                                           0x13
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_6_BMSK                                        0x40000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_6_SHFT                                           0x12
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_5_BMSK                                        0x20000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_5_SHFT                                           0x11
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_4_BMSK                                        0x10000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_4_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_3_BMSK                                         0x8000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_3_SHFT                                            0xf
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_2_BMSK                                         0x4000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_2_SHFT                                            0xe
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_1_BMSK                                         0x2000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_1_SHFT                                            0xd
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_0_BMSK                                         0x1000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_PC_0_SHFT                                            0xc
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_3_BMSK                                        0x800
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_3_SHFT                                          0xb
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_2_BMSK                                        0x400
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_2_SHFT                                          0xa
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_1_BMSK                                        0x200
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_1_SHFT                                          0x9
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_0_BMSK                                        0x100
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_RBBM_0_SHFT                                          0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_7_BMSK                                           0x80
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_7_SHFT                                            0x7
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_6_BMSK                                           0x40
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_6_SHFT                                            0x6
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_5_BMSK                                           0x20
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_5_SHFT                                            0x5
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_4_BMSK                                           0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_4_SHFT                                            0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_3_BMSK                                            0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_3_SHFT                                            0x3
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_2_BMSK                                            0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_2_SHFT                                            0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_1_BMSK                                            0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_1_SHFT                                            0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_0_BMSK                                            0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD0_SEL_CP_0_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005c8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_3_BMSK                                     0x80000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_3_SHFT                                           0x1f
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_2_BMSK                                     0x40000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_2_SHFT                                           0x1e
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_1_BMSK                                     0x20000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_1_SHFT                                           0x1d
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_0_BMSK                                     0x10000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TP_0_SHFT                                           0x1c
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_7_BMSK                                    0x8000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_7_SHFT                                         0x1b
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_6_BMSK                                    0x4000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_6_SHFT                                         0x1a
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_5_BMSK                                    0x2000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_5_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_4_BMSK                                    0x1000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_4_SHFT                                         0x18
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_3_BMSK                                     0x800000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_3_SHFT                                         0x17
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_2_BMSK                                     0x400000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_2_SHFT                                         0x16
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_1_BMSK                                     0x200000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_1_SHFT                                         0x15
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_0_BMSK                                     0x100000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_UCHE_0_SHFT                                         0x14
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_3_BMSK                                       0x80000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_3_SHFT                                          0x13
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_2_BMSK                                       0x40000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_2_SHFT                                          0x12
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_1_BMSK                                       0x20000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_1_SHFT                                          0x11
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_0_BMSK                                       0x10000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_RAS_0_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_3_BMSK                                        0x8000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_3_SHFT                                           0xf
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_2_BMSK                                        0x4000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_2_SHFT                                           0xe
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_1_BMSK                                        0x2000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_1_SHFT                                           0xd
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_0_BMSK                                        0x1000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_TSE_0_SHFT                                           0xc
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_3_BMSK                                         0x800
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_3_SHFT                                           0xb
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_2_BMSK                                         0x400
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_2_SHFT                                           0xa
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_1_BMSK                                         0x200
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_1_SHFT                                           0x9
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_0_BMSK                                         0x100
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_CCU_0_SHFT                                           0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_3_BMSK                                          0x80
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_3_SHFT                                           0x7
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_2_BMSK                                          0x40
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_2_SHFT                                           0x6
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_1_BMSK                                          0x20
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_1_SHFT                                           0x5
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_0_BMSK                                          0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_VPC_0_SHFT                                           0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_7_BMSK                                          0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_7_SHFT                                          0x3
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_6_BMSK                                          0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_6_SHFT                                          0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_5_BMSK                                          0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_5_SHFT                                          0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_4_BMSK                                          0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD1_SEL_HLSQ_4_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005cc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_RMSK                                               0x3ffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_VSC_1_BMSK                                     0x2000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_VSC_1_SHFT                                          0x19
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_VSC_0_BMSK                                     0x1000000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_VSC_0_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_7_BMSK                                       0x800000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_7_SHFT                                           0x17
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_6_BMSK                                       0x400000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_6_SHFT                                           0x16
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_5_BMSK                                       0x200000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_5_SHFT                                           0x15
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_4_BMSK                                       0x100000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_4_SHFT                                           0x14
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_3_BMSK                                        0x80000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_3_SHFT                                           0x13
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_2_BMSK                                        0x40000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_2_SHFT                                           0x12
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_1_BMSK                                        0x20000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_1_SHFT                                           0x11
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_0_BMSK                                        0x10000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_RB_0_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_11_BMSK                                        0x8000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_11_SHFT                                           0xf
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_10_BMSK                                        0x4000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_10_SHFT                                           0xe
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_9_BMSK                                         0x2000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_9_SHFT                                            0xd
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_8_BMSK                                         0x1000
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_8_SHFT                                            0xc
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_7_BMSK                                          0x800
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_7_SHFT                                            0xb
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_6_BMSK                                          0x400
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_6_SHFT                                            0xa
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_5_BMSK                                          0x200
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_5_SHFT                                            0x9
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_4_BMSK                                          0x100
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_4_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_3_BMSK                                           0x80
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_3_SHFT                                            0x7
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_2_BMSK                                           0x40
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_2_SHFT                                            0x6
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_1_BMSK                                           0x20
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_1_SHFT                                            0x5
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_0_BMSK                                           0x10
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_SP_0_SHFT                                            0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_7_BMSK                                            0x8
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_7_SHFT                                            0x3
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_6_BMSK                                            0x4
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_6_SHFT                                            0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_5_BMSK                                            0x2
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_5_SHFT                                            0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_4_BMSK                                            0x1
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_CMD2_SEL_TP_4_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005d0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_PERF_LOAD_LO_BMSK                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_LO_PERF_LOAD_LO_SHFT                                    0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005d4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_PERF_LOAD_HI_BMSK                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_LOAD_VALUE_HI_PERF_LOAD_HI_SHFT                                    0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005d8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_RMSK                                                   0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_PERF_SEL_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_0_PERF_SEL_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005dc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_RMSK                                                   0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_PERF_SEL_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_1_PERF_SEL_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005e0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_RMSK                                                   0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_PERF_SEL_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_2_PERF_SEL_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005e4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_RMSK                                                   0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_IN)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_PERF_SEL_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_SEL_3_PERF_SEL_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005e8)
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RMSK                                                  0x1fffff
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ADDR, HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RMSK)
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_IN)
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_DPM_BUSY_BMSK                                         0x100000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_DPM_BUSY_SHFT                                             0x14
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_CCU_BUSY_BMSK                                          0x80000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_CCU_BUSY_SHFT                                             0x13
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_DCOM_BUSY_BMSK                                         0x40000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_DCOM_BUSY_SHFT                                            0x12
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_COM_BUSY_BMSK                                          0x20000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_COM_BUSY_SHFT                                             0x11
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_HLSQ_BUSY_BMSK                                         0x10000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_HLSQ_BUSY_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ARB_BUSY_BMSK                                           0x8000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_ARB_BUSY_SHFT                                              0xf
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VSC_BUSY_BMSK                                           0x4000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VSC_BUSY_SHFT                                              0xe
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_MARB_BUSY_BMSK                                          0x2000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_MARB_BUSY_SHFT                                             0xd
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TPL1_BUSY_BMSK                                          0x1000
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TPL1_BUSY_SHFT                                             0xc
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_SP_BUSY_BMSK                                             0x800
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_SP_BUSY_SHFT                                               0xb
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_UCHE_BUSY_BMSK                                           0x400
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_UCHE_BUSY_SHFT                                             0xa
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VPC_BUSY_BMSK                                            0x200
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VPC_BUSY_SHFT                                              0x9
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VFD_BUSY_BMSK                                            0x100
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VFD_BUSY_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TESS_BUSY_BMSK                                            0x80
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TESS_BUSY_SHFT                                             0x7
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_PC_VSD_BUSY_BMSK                                          0x40
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_PC_VSD_BUSY_SHFT                                           0x6
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_PC_DCALL_BUSY_BMSK                                        0x20
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_PC_DCALL_BUSY_SHFT                                         0x5
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RB_BUSY_BMSK                                              0x10
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RB_BUSY_SHFT                                               0x4
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RAS_BUSY_BMSK                                              0x8
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_RAS_BUSY_SHFT                                              0x3
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TSE_BUSY_BMSK                                              0x4
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_TSE_BUSY_SHFT                                              0x2
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VBIF_BUSY_BMSK                                             0x2
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_VBIF_BUSY_SHFT                                             0x1
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_CP_NRT_BUSY_BMSK                                           0x1
#define HWIO_MMSS_OXILI_RBBM_GPU_BUSY_MASKED_CP_NRT_BUSY_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005ec)
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_RMSK                                                        0xf
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_ADDR, HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_IN)
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_PIPE_MASK_BMSK                                              0xf
#define HWIO_MMSS_OXILI_RBBM_PERF_PIPE_MASK_PIPE_MASK_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000270)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000274)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_0_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000278)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000027c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_1_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000280)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000284)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_2_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000288)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000028c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_3_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000290)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000294)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_4_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000298)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000029c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_5_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002a0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002a4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_6_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002a8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002ac)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CP_7_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002b0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002b4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_0_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002b8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002bc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_1_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002c0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002c4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_2_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002c8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002cc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RBBM_3_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002d0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002d4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_0_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002d8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002dc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_1_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002e0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002e4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_2_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002e8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002ec)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_3_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002f0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002f4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_4_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002f8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000002fc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_5_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000300)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000304)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_6_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000308)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000030c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PC_7_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000310)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000314)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000318)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000031c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000320)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000324)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_2_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000328)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000032c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_3_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000330)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000334)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_4_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000338)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000033c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_5_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000340)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000344)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_6_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000348)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000034c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VFD_7_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000350)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000354)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_0_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000358)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000035c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_1_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000360)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000364)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_2_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000368)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000036c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_3_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000370)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000374)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_4_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000378)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000037c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_5_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000380)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000384)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_6_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000388)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000038c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_HLSQ_7_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000390)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000394)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000398)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000039c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003a0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003a4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_2_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003a8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003ac)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VPC_3_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003b0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003b4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003b8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003bc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003c0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003c4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_2_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003c8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003cc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_CCU_3_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003d0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003d4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003d8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003dc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003e0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003e4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_2_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003e8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003ec)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TSE_3_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003f0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003f4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003f8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000003fc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000400)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000404)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_2_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000408)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000040c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RAS_3_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000410)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000414)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_0_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000418)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000041c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_1_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000420)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000424)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_2_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000428)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000042c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_3_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000430)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000434)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_4_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000438)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000043c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_5_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000440)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000444)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_6_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000448)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_PERF_COUNT_LO_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_LO_PERF_COUNT_LO_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000044c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_PERF_COUNT_HI_BMSK                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_UCHE_7_HI_PERF_COUNT_HI_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000450)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000454)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_0_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000458)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000045c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_1_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000460)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000464)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_2_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000468)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000046c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_3_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000470)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000474)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_4_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000478)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000047c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_5_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000480)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000484)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_6_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000488)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000048c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_TP_7_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000490)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000494)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_0_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000498)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000049c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_1_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004a0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004a4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_2_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004a8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004ac)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_3_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004b0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004b4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_4_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004b8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004bc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_5_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004c0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004c4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_6_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004c8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004cc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_7_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004d0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004d4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_8_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004d8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004dc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_9_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004e0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004e4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_10_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004e8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004ec)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_SP_11_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004f0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004f4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_0_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004f8)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000004fc)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_1_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000500)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000504)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_2_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000508)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000050c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_3_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000510)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000514)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_4_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000518)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000051c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_5_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000520)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000524)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_6_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000528)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_PERF_COUNT_LO_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_LO_PERF_COUNT_LO_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000052c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_PERF_COUNT_HI_BMSK                                  0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_RB_7_HI_PERF_COUNT_HI_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000530)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000534)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000538)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000053c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_VSC_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000f8)
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RMSK                                                           0x3333
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_RBBM_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR_SP_BMSK                                         0x2000
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR_SP_SHFT                                            0xd
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR_MARB_BMSK                                       0x1000
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR_MARB_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR_SP_BMSK                                           0x200
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR_SP_SHFT                                             0x9
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR_MARB_BMSK                                         0x100
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR_MARB_SHFT                                           0x8
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR1_BMSK                                             0x20
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR1_SHFT                                              0x5
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR0_BMSK                                             0x10
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_ENABLE_PWR_CTR0_SHFT                                              0x4
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR1_BMSK                                               0x2
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR1_SHFT                                               0x1
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR0_BMSK                                               0x1
#define HWIO_MMSS_OXILI_RBBM_RBBM_CTL_RESET_PWR_CTR0_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000598)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000059c)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_0_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005a0)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_PERF_COUNT_LO_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_LO_PERF_COUNT_LO_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005a4)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_PERF_COUNT_HI_BMSK                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PERFCTR_PWR_1_HI_PERF_COUNT_HI_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005a8)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_RMSK                                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_ADDR, HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_SP_ACTIVE_CYCLES_POWER_BMSK                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_LO_SP_ACTIVE_CYCLES_POWER_SHFT                            0x0

#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005ac)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_RMSK                                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_ADDR, HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_SP_ACTIVE_CYCLES_POWER_BMSK                     0xffffffff
#define HWIO_MMSS_OXILI_RBBM_SP_POWER_COUNTER_HI_SP_ACTIVE_CYCLES_POWER_SHFT                            0x0

#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005b0)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_ADDR, HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_MARB_ACTIVE_CYCLES_POWER_BMSK                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_LO_MARB_ACTIVE_CYCLES_POWER_SHFT                        0x0

#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005b4)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_ADDR, HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_MARB_ACTIVE_CYCLES_POWER_BMSK                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_MARB_POWER_COUNTER_HI_MARB_ACTIVE_CYCLES_POWER_SHFT                        0x0

#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000f4)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_RMSK                                                 0x1
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_ADDR, HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_IN)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_RESET_BMSK                                           0x1
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_CNTL_RESET_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005b8)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_RMSK                                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_ADDR, HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_COUNTER_BMSK                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_LO_COUNTER_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_ADDR                                            (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000005bc)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_RMSK                                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_ADDR, HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_COUNTER_BMSK                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_ALWAYSON_COUNTER_HI_COUNTER_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000540)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_PIPESTAT_COUNT_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_LO_PIPESTAT_COUNT_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000544)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_PIPESTAT_COUNT_BMSK                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAVERTICES_HI_PIPESTAT_COUNT_SHFT                                 0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000548)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_LO_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000054c)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_IAPRIMITIVES_HI_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000550)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000554)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_VSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000558)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000055c)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_HSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000560)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000564)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_DSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000568)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000056c)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000570)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_LO_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000574)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_GSPRIMITIVES_HI_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000578)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000057c)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                               0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000580)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_RMSK                                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_PIPESTAT_COUNT_BMSK                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_LO_PIPESTAT_COUNT_SHFT                                0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000584)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_RMSK                                        0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_PIPESTAT_COUNT_BMSK                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CPRIMITIVES_HI_PIPESTAT_COUNT_SHFT                                0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000588)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000058c)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_PSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000590)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_LO_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_ADDR                                      (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000594)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_RMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_ADDR, HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_IN)
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_PIPESTAT_COUNT_BMSK                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PIPESTAT_CSINVOCATIONS_HI_PIPESTAT_COUNT_SHFT                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000128)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PONG_BLK_SEL_BMSK                                  0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PONG_BLK_SEL_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PONG_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PONG_INDEX_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PING_BLK_SEL_BMSK                                      0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PING_BLK_SEL_SHFT                                         0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PING_INDEX_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_A_PING_INDEX_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000012c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PONG_BLK_SEL_BMSK                                  0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PONG_BLK_SEL_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PONG_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PONG_INDEX_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PING_BLK_SEL_BMSK                                      0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PING_BLK_SEL_SHFT                                         0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PING_INDEX_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_B_PING_INDEX_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000130)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PONG_BLK_SEL_BMSK                                  0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PONG_BLK_SEL_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PONG_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PONG_INDEX_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PING_BLK_SEL_BMSK                                      0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PING_BLK_SEL_SHFT                                         0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PING_INDEX_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_C_PING_INDEX_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000134)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PONG_BLK_SEL_BMSK                                  0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PONG_BLK_SEL_SHFT                                        0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PONG_INDEX_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PONG_INDEX_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PING_BLK_SEL_BMSK                                      0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PING_BLK_SEL_SHFT                                         0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PING_INDEX_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_SEL_D_PING_INDEX_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000138)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_SEGT_BMSK                                           0xf0000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_SEGT_SHFT                                                 0x1c
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_POST_BMSK                                            0xfff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_POST_SHFT                                                 0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_MARKT_BMSK                                              0x8000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_MARKT_SHFT                                                 0xf
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_GRANU_BMSK                                              0x7000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_GRANU_SHFT                                                 0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_TRIGT_BMSK                                               0xfc0
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_TRIGT_SHFT                                                 0x6
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ENT_BMSK                                                  0x3f
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLT_ENT_SHFT                                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000013c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_CLEAR_BMSK                                          0xf0000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_CLEAR_SHFT                                                0x1c
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENABLE_BMSK                                          0xf000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENABLE_SHFT                                               0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_PTRP1_BMSK                                            0xf80000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_PTRP1_SHFT                                                0x13
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_PTRP0_BMSK                                             0x7c000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_PTRP0_SHFT                                                 0xe
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_CTLP_BMSK                                               0x3000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_CTLP_SHFT                                                  0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENI_BMSK                                                 0xfc0
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENI_SHFT                                                   0x6
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENM_BMSK                                                  0x3f
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CTLM_ENM_SHFT                                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000140)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_RMSK                                                     0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_OPL_BMSK                                                 0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPL_OPL_SHFT                                                    0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000024c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_RMSK                                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX3_BMSK                                   0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX3_SHFT                                         0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX2_BMSK                                     0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX2_SHFT                                         0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX1_BMSK                                       0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX1_SHFT                                          0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX0_BMSK                                         0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IDX_INTERNAL_IDX0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000250)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_RMSK                                                0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR7_BMSK                                       0xf0000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR7_SHFT                                             0x1c
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR6_BMSK                                        0xf000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR6_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR5_BMSK                                         0xf00000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR5_SHFT                                             0x14
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR4_BMSK                                          0xf0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR4_SHFT                                             0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR3_BMSK                                           0xf000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR3_SHFT                                              0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR2_BMSK                                            0xf00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR2_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR1_BMSK                                             0xf0
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR1_SHFT                                              0x4
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR0_BMSK                                              0xf
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_CLRC_CLR_PTR0_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000254)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOOPSCAN_BMSK                                    0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOOPSCAN_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOAD_IVT1_BMSK                                     0xfff000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOAD_IVT1_SHFT                                          0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOAD_IVT0_BMSK                                        0xfff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADIVT_LOAD_IVT0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000144)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_RMSK                                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_STICKYE_BMSK                                         0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_STICKYE_SHFT                                               0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_OPE_BMSK                                                 0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OPE_OPE_SHFT                                                    0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000148)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_3_BMSK                                       0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_3_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_2_BMSK                                         0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_2_SHFT                                             0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_1_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_1_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_0_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_0_IVTL_0_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000014c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_7_BMSK                                       0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_7_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_6_BMSK                                         0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_6_SHFT                                             0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_5_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_5_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_4_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_1_IVTL_4_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000150)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_11_BMSK                                      0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_11_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_10_BMSK                                        0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_10_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_9_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_9_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_8_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_2_IVTL_8_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000154)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_15_BMSK                                      0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_15_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_14_BMSK                                        0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_14_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_13_BMSK                                          0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_13_SHFT                                             0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_12_BMSK                                            0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTL_3_IVTL_12_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000158)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_3_BMSK                                     0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_3_SHFT                                           0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_2_BMSK                                       0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_2_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_1_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_1_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_0_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_0_MASKL_0_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000015c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_7_BMSK                                     0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_7_SHFT                                           0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_6_BMSK                                       0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_6_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_5_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_5_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_4_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_1_MASKL_4_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000160)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_11_BMSK                                    0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_11_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_10_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_10_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_9_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_9_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_8_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_2_MASKL_8_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000164)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_15_BMSK                                    0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_15_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_14_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_14_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_13_BMSK                                        0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_13_SHFT                                           0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_12_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKL_3_MASKL_12_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000168)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL7_BMSK                                      0xf0000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL7_SHFT                                            0x1c
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL6_BMSK                                       0xf000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL6_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL5_BMSK                                        0xf00000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL5_SHFT                                            0x14
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL4_BMSK                                         0xf0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL4_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL3_BMSK                                          0xf000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL3_SHFT                                             0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL2_BMSK                                           0xf00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL2_SHFT                                             0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL1_BMSK                                            0xf0
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL1_SHFT                                             0x4
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL0_BMSK                                             0xf
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_0_BYTEL0_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000016c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL15_BMSK                                     0xf0000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL15_SHFT                                           0x1c
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL14_BMSK                                      0xf000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL14_SHFT                                           0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL13_BMSK                                       0xf00000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL13_SHFT                                           0x14
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL12_BMSK                                        0xf0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL12_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL11_BMSK                                         0xf000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL11_SHFT                                            0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL10_BMSK                                          0xf00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL10_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL9_BMSK                                            0xf0
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL9_SHFT                                             0x4
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL8_BMSK                                             0xf
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_BYTEL_1_BYTEL8_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000170)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_3_BMSK                                       0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_3_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_2_BMSK                                         0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_2_SHFT                                             0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_1_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_1_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_0_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_0_IVTE_0_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000174)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_7_BMSK                                       0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_7_SHFT                                             0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_6_BMSK                                         0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_6_SHFT                                             0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_5_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_5_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_4_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_1_IVTE_4_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000178)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_11_BMSK                                      0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_11_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_10_BMSK                                        0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_10_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_9_BMSK                                           0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_9_SHFT                                              0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_8_BMSK                                             0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_2_IVTE_8_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000017c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_15_BMSK                                      0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_15_SHFT                                            0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_14_BMSK                                        0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_14_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_13_BMSK                                          0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_13_SHFT                                             0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_12_BMSK                                            0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_IVTE_3_IVTE_12_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000180)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_3_BMSK                                     0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_3_SHFT                                           0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_2_BMSK                                       0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_2_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_1_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_1_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_0_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_0_MASKE_0_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000184)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_7_BMSK                                     0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_7_SHFT                                           0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_6_BMSK                                       0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_6_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_5_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_5_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_4_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_1_MASKE_4_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000188)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_11_BMSK                                    0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_11_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_10_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_10_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_9_BMSK                                         0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_9_SHFT                                            0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_8_BMSK                                           0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_2_MASKE_8_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000018c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_15_BMSK                                    0xff000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_15_SHFT                                          0x18
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_14_BMSK                                      0xff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_14_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_13_BMSK                                        0xff00
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_13_SHFT                                           0x8
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_12_BMSK                                          0xff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MASKE_3_MASKE_12_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000190)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_NIBBLE_BMSK                                      0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_NIBBLEE_NIBBLE_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000194)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_PTRC_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC0_PTRC_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000198)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_IN)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_PTRC_BMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_PTRC1_PTRC_SHFT                                                 0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_ADDR                                             (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000019c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_RMSK                                             0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_ENABLE_BMSK                                 0x80000000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_ENABLE_SHFT                                       0x1f
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_ADDRESS_BMSK                                0x7fff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_ADDRESS_SHFT                                      0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_VALUE_BMSK                                      0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_LOADREG_LOAD_VALUE_SHFT                                         0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006b8)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_CRC_RESULT_BMSK                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR0_CRC_RESULT_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006bc)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_RMSK                                               0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_CRC_RESULT_BMSK                                    0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_MISR1_CRC_RESULT_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000680)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_RMSK                                         0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_STL_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_STL_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_STE_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_EVENT_LOGIC_STE_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_ADDR                                                (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000684)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_RMSK                                                   0x1ffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_INTR_BMSK                                              0x10000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_INTR_SHFT                                                 0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_STP_BMSK                                                0x8000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_STP_SHFT                                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_STT_BMSK                                                0x7000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_STT_SHFT                                                   0xc
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_COUNT_OVER_REG_BMSK                                      0xfff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_OVER_COUNT_OVER_REG_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000688)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_COUNTER1_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_COUNTER1_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_COUNTER0_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT0_COUNTER0_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000068c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_COUNTER3_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_COUNTER3_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_COUNTER2_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT1_COUNTER2_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000690)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_COUNTER5_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_COUNTER5_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_COUNTER4_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT2_COUNTER4_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000694)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_COUNTER7_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_COUNTER7_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_COUNTER6_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT3_COUNTER6_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000698)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_COUNTER9_BMSK                                     0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_COUNTER9_SHFT                                           0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_COUNTER8_BMSK                                         0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT4_COUNTER8_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000069c)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_RMSK                                              0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_COUNTER11_BMSK                                    0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_COUNTER11_SHFT                                          0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_COUNTER10_BMSK                                        0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_COUNT5_COUNTER10_SHFT                                           0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006a0)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_CUR_ADDR_BMSK                                 0xffff0000
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_CUR_ADDR_SHFT                                       0x10
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_TRIG_ADDR_BMSK                                    0xffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_ADDR_TRIG_ADDR_SHFT                                       0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006a4)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_TRACE_BUFFER0_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF0_TRACE_BUFFER0_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006a8)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_TRACE_BUFFER1_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF1_TRACE_BUFFER1_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006ac)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_TRACE_BUFFER2_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF2_TRACE_BUFFER2_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006b0)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_TRACE_BUFFER3_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF3_TRACE_BUFFER3_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_ADDR                                          (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006b4)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_RMSK                                          0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_ADDR, HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_TRACE_BUFFER4_BMSK                            0xffffffff
#define HWIO_MMSS_OXILI_RBBM_CFG_DEBBUS_TRACE_BUF4_TRACE_BUFFER4_SHFT                                   0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000000fc)
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_RMSK                                                        0x13
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ENABLE_BMSK                                                 0x10
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_ENABLE_SHFT                                                  0x4
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_DEBUG_SEL_BMSK                                               0x3
#define HWIO_MMSS_OXILI_RBBM_AHB_DEBUG_CTL_DEBUG_SEL_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_ADDR                                               (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000120)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_RMSK                                                      0x1
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_ADDR, HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_RMSK)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_IN)
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_DEBUG_LO_HI_GPIO_BMSK                                     0x1
#define HWIO_MMSS_OXILI_RBBM_DEBUG_LO_HI_GPIO_DEBUG_LO_HI_GPIO_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000124)
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_RMSK                                                    0xff
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ATB_CLOCK_EN_BMSK                                       0x80
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ATB_CLOCK_EN_SHFT                                        0x7
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ATB_ID_BMSK                                             0x7f
#define HWIO_MMSS_OXILI_RBBM_EXT_TRACE_BUS_CTL_ATB_ID_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000104)
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_RMSK                                                 0xff33f3f3
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_ADDR, HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_IN)
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_DATA_SEL_BMSK                              0xf0000000
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_DATA_SEL_SHFT                                    0x1c
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS1_DATA_SEL_BMSK                               0xf000000
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS1_DATA_SEL_SHFT                                    0x18
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_ARB_EN_BMSK                                  0x300000
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_ARB_EN_SHFT                                      0x14
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_XIN_EN_BMSK                                   0x3f000
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS2_XIN_EN_SHFT                                       0xc
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS1_XIN_EN_BMSK                                     0x3f0
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_TEST_BUS1_XIN_EN_SHFT                                       0x4
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_DEBUG_SEL_BMSK                                              0x3
#define HWIO_MMSS_OXILI_RBBM_VBIF_DEBUG_CTL_DEBUG_SEL_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_ADDR                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000258)
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_RMSK                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_ADDR, HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_RMSK)
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_IN)
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_NA_BMSK                                    0xfffffffe
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_NA_SHFT                                           0x1
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_HOST_ACCESS_BLOCK_DIS_BMSK                        0x1
#define HWIO_MMSS_OXILI_RBBM_AHB_HOST_ACCESS_HOLDBACK_HOST_ACCESS_BLOCK_DIS_SHFT                        0x0

#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000025c)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_RMSK                                                      0xffff
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_ADDR, HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_RMSK)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_IN)
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_SW_CONTROL_MODE_BMSK                                0x8000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_SW_CONTROL_MODE_SHFT                                   0xf
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_CORE_ON_BMSK                                        0x4000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_CORE_ON_SHFT                                           0xe
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_PERIPH_ON_BMSK                                      0x2000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_PERIPH_ON_SHFT                                         0xd
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_PERIPH_OFF_BMSK                                     0x1000
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_FORCE_PERIPH_OFF_SHFT                                        0xc
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_WAKE_UP_TIMER_BMSK                                         0xf00
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_WAKE_UP_TIMER_SHFT                                           0x8
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_SLEEP_TIMER_BMSK                                            0xf0
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_SLEEP_TIMER_SHFT                                             0x4
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_NUM_CLOCK_DELAY_BMSK                                         0xe
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_NUM_CLOCK_DELAY_SHFT                                         0x1
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_CLK_ENABLE_BMSK                                              0x1
#define HWIO_MMSS_OXILI_RBBM_CLOCK_CNTL_IP_CLK_ENABLE_SHFT                                              0x0

#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_ADDR                                                  (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000260)
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RMSK                                                    0xfff7ff
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_ADDR, HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RMSK)
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_IN)
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_REST_WAIT_BMSK                                       0xf00000
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_REST_WAIT_SHFT                                           0x14
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_FEW_WAIT_BMSK                                         0xf0000
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_FEW_WAIT_SHFT                                            0x10
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLK_DIS_WAIT_BMSK                                         0xf000
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLK_DIS_WAIT_SHFT                                            0xc
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RESTORE_BMSK                                               0x400
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RESTORE_SHFT                                                 0xa
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SAVE_BMSK                                                  0x200
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SAVE_SHFT                                                    0x9
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RETAIN_BMSK                                                0x100
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_RETAIN_SHFT                                                  0x8
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_REST_BMSK                                                0x80
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_REST_SHFT                                                 0x7
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_FEW_BMSK                                                 0x40
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_EN_FEW_SHFT                                                  0x6
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLAMP_IO_BMSK                                               0x20
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLAMP_IO_SHFT                                                0x5
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLK_DISABLE_BMSK                                            0x10
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_CLK_DISABLE_SHFT                                             0x4
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_PD_ARES_BMSK                                                 0x8
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_PD_ARES_SHFT                                                 0x3
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SW_OVERRIDE_BMSK                                             0x4
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SW_OVERRIDE_SHFT                                             0x2
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_HW_CONTROL_BMSK                                              0x2
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_HW_CONTROL_SHFT                                              0x1
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SW_COLLAPSE_BMSK                                             0x1
#define HWIO_MMSS_OXILI_RBBM_POWER_CNTL_IP_SW_COLLAPSE_SHFT                                             0x0

#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000264)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_RMSK                                            0x7fff
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ADDR, HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_IN)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_SW_CONTROL_MODE_BMSK                      0x4000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_SW_CONTROL_MODE_SHFT                         0xe
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_CORE_ON_BMSK                              0x2000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_CORE_ON_SHFT                                 0xd
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_PERIPH_ON_BMSK                            0x1000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_FORCE_PERIPH_ON_SHFT                               0xc
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_WAKE_UP_TIMER_BMSK                               0xf00
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_WAKE_UP_TIMER_SHFT                                 0x8
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_SLEEP_TIMER_BMSK                                  0xf0
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_SLEEP_TIMER_SHFT                                   0x4
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_NUM_CLOCK_DELAY_BMSK                               0xe
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_NUM_CLOCK_DELAY_SHFT                               0x1
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ICGC_MODE_BMSK                                     0x1
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_0_ICGC_MODE_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ADDR                                        (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x00000268)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_RMSK                                            0x7fff
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ADDR, HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_IN)
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_SW_CONTROL_MODE_BMSK                      0x4000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_SW_CONTROL_MODE_SHFT                         0xe
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_CORE_ON_BMSK                              0x2000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_CORE_ON_SHFT                                 0xd
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_PERIPH_ON_BMSK                            0x1000
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_FORCE_PERIPH_ON_SHFT                               0xc
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_WAKE_UP_TIMER_BMSK                               0xf00
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_WAKE_UP_TIMER_SHFT                                 0x8
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_SLEEP_TIMER_BMSK                                  0xf0
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_SLEEP_TIMER_SHFT                                   0x4
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_NUM_CLOCK_DELAY_BMSK                               0xe
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_NUM_CLOCK_DELAY_SHFT                               0x1
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ICGC_MODE_BMSK                                     0x1
#define HWIO_MMSS_OXILI_RBBM_SP_REGFILE_SLEEP_CNTL_1_ICGC_MODE_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_ADDR                                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x0000026c)
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_RMSK                                                0x1f3ff1
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_ADDR, HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_IN)
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_OFFSET_HYST_CONFIG_BMSK                             0x1f0000
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_OFFSET_HYST_CONFIG_SHFT                                 0x10
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_CLK_DIVIDE_BY_BMSK                                    0x3000
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_CLK_DIVIDE_BY_SHFT                                       0xc
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_DELAY_BMSK                                             0xff0
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_DELAY_SHFT                                               0x4
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_FF_EN_BMSK                                               0x1
#define HWIO_MMSS_OXILI_RBBM_BHS_FAST_FWD_CNTL_FF_EN_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006c0)
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_RMSK                                                     0x1f0ff7
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_POWER_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_POWER_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_POWER_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_PWR_ON_BMSK                                              0x100000
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_PWR_ON_SHFT                                                  0x14
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_GDSC_STATE_BMSK                                           0xf0000
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_GDSC_STATE_SHFT                                              0x10
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_BHS_SEQ_COUNTER_BMSK                                        0xff0
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_BHS_SEQ_COUNTER_SHFT                                          0x4
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_TP_BMSK                                            0x4
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_TP_SHFT                                            0x2
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_RF_1_BMSK                                          0x2
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_RF_1_SHFT                                          0x1
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_RF_0_BMSK                                          0x1
#define HWIO_MMSS_OXILI_RBBM_POWER_STATUS_CLK_OFF_SP_RF_0_SHFT                                          0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ADDR                                                       (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006e4)
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_RMSK                                                       0xffffffff
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_CTRL_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PPD_CTRL_IN)
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EN_TH_SEL_BMSK                                             0x80000000
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EN_TH_SEL_SHFT                                                   0x1f
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EPOCH_SIZE_BMSK                                            0x7f800000
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EPOCH_SIZE_SHFT                                                  0x17
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EPOCH_NUM_BMSK                                               0x7e0000
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_EPOCH_NUM_SHFT                                                   0x11
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_HW_TMD_TH_BMSK                                                0x1fc00
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_HW_TMD_TH_SHFT                                                    0xa
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_CLK_THROTTLE_LEL_BMSK                                           0x3c0
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_CLK_THROTTLE_LEL_SHFT                                             0x6
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_DPM_KPI_SELECT_BMSK                                              0x20
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_DPM_KPI_SELECT_SHFT                                               0x5
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_HW_TMD_MASK_BMSK                                                 0x10
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_HW_TMD_MASK_SHFT                                                  0x4
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_SW_TMD_VAL_BMSK                                                   0x8
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_SW_TMD_VAL_SHFT                                                   0x3
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_THROTTLE_MASK_BMSK                                                0x4
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_THROTTLE_MASK_SHFT                                                0x2
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_RESET_BMSK                                                        0x2
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_RESET_SHFT                                                        0x1
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ENABLE_BMSK                                                       0x1
#define HWIO_MMSS_OXILI_RBBM_PPD_CTRL_ENABLE_SHFT                                                       0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006e8)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_RMSK                                           0x7fff87ff
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_IN)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_SP_ALU_LANE_TH_BMSK                            0x7fff8000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_SP_ALU_LANE_TH_SHFT                                   0xf
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_TP_FILTER_TH_BMSK                                   0x7ff
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_1_TP_FILTER_TH_SHFT                                     0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_ADDR                                           (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006ec)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_RMSK                                           0xfffe01ff
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_IN)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_SP_RF_RW_TH_BMSK                               0xfffe0000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_SP_RF_RW_TH_SHFT                                     0x11
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_RB_WEN_TH_BMSK                                      0x1ff
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTRA_TH_2_RB_WEN_TH_SHFT                                        0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_ADDR                              (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006f0)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_RMSK                              0xc03f3f3f
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_IN)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_CLEAR_THR_EXIT_BMSK               0x80000000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_CLEAR_THR_EXIT_SHFT                     0x1f
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_CLEAR_THR_ENTER_BMSK              0x40000000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_CLEAR_THR_ENTER_SHFT                    0x1e
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_RB_HIGH_TH_BMSK                     0x3f0000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_RB_HIGH_TH_SHFT                         0x10
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_TP_HIGH_TH_BMSK                       0x3f00
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_TP_HIGH_TH_SHFT                          0x8
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_SP_HIGH_TH_BMSK                         0x3f
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_HIGH_CLEAR_THR_SP_HIGH_TH_SHFT                          0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_ADDR                                         (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006f4)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_RMSK                                           0x3f3f3f
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_IN)
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_RB_LOW_TH_BMSK                                 0x3f0000
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_RB_LOW_TH_SHFT                                     0x10
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_TP_LOW_TH_BMSK                                   0x3f00
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_TP_LOW_TH_SHFT                                      0x8
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_SP_LOW_TH_BMSK                                     0x3f
#define HWIO_MMSS_OXILI_RBBM_PPD_EPOCH_INTER_TH_LOW_SP_LOW_TH_SHFT                                      0x0

#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_ADDR                                                     (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006f8)
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_RMSK                                                           0x3f
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_PPD_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_PPD_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_EXIT_V2_BMSK                                          0x20
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_EXIT_V2_SHFT                                           0x5
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_ENTER_V2_BMSK                                         0x10
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_ENTER_V2_SHFT                                          0x4
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_HW_TMD_STATE_V2_BMSK                                            0x8
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_HW_TMD_STATE_V2_SHFT                                            0x3
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_EXIT_BMSK                                              0x4
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_EXIT_SHFT                                              0x2
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_ENTER_BMSK                                             0x2
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_THROTTLE_ENTER_SHFT                                             0x1
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_HW_TMD_STATE_BMSK                                               0x1
#define HWIO_MMSS_OXILI_RBBM_PPD_STATUS_HW_TMD_STATE_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006d8)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RMSK                                                 0xffffffff
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_ADDR, HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RMSK)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_IN)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RESET_FUNC_BMSK                                      0x80000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RESET_FUNC_SHFT                                            0x1f
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_MODE_SEL_BMSK                                        0x40000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_MODE_SEL_SHFT                                              0x1e
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RSVD_BMSK                                            0x20000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_RSVD_SHFT                                                  0x1d
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIG_SEL_BMSK                                        0x18000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIG_SEL_SHFT                                              0x1b
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                       0x4000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                            0x1a
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_START_CAPTURE_BMSK                                    0x2000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_START_CAPTURE_SHFT                                         0x19
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_EN_POWER_BMSK                                         0x1000000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_EN_POWER_SHFT                                              0x18
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                     0xff0000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                         0x10
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                       0xff00
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                          0x8
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                         0xf0
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                          0x4
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIGGER_POS_BMSK                                            0xc
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIGGER_POS_SHFT                                            0x2
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                            0x3
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                            0x0

#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ADDR                                                 (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006dc)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_RMSK                                                   0xffffff
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ADDR, HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_RMSK)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ADDR,v)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ADDR,m,v,HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_IN)
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_SEL_JTAG_CSR_BMSK                                      0x800000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_SEL_JTAG_CSR_SHFT                                          0x17
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                         0x7e0000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                             0x11
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ALARM_SLOPE_EN_BMSK                                     0x10000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_ALARM_SLOPE_EN_SHFT                                        0x10
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                         0xe000
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                            0xd
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_SLOPE_THRESHOLD_BMSK                                     0x1fe0
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_SLOPE_THRESHOLD_SHFT                                        0x5
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_EN_FUNC_BMSK                                               0x10
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_EN_FUNC_SHFT                                                0x4
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                              0x8
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                              0x3
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_STATUS_CLEAR_BMSK                                           0x4
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_STATUS_CLEAR_SHFT                                           0x2
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_EN_ALARM_BMSK                                               0x2
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_EN_ALARM_SHFT                                               0x1
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_CLOCK_EN_BMSK                                               0x1
#define HWIO_MMSS_OXILI_RBBM_VSENS_CONFIG_1_CLOCK_EN_SHFT                                               0x0

#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ADDR                                                   (MMSS_OXILI_RBBM_BLOCK0_REG_BASE      + 0x000006c4)
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_RMSK                                                      0x3ffff
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ADDR, HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_RMSK)
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ADDR, m)
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_VS_FSM_ST_BMSK                                            0x3c000
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_VS_FSM_ST_SHFT                                                0xe
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_SLOPE_NEG_BMSK                                             0x2000
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_SLOPE_NEG_SHFT                                                0xd
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_SLOPE_POS_BMSK                                             0x1000
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_SLOPE_POS_SHFT                                                0xc
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ALARM_MAX_BMSK                                              0x800
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ALARM_MAX_SHFT                                                0xb
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ALARM_MIN_BMSK                                              0x400
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_ALARM_MIN_SHFT                                                0xa
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                                        0x200
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                                          0x9
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                      0x100
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                        0x8
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_DATA_BMSK                                               0xff
#define HWIO_MMSS_OXILI_RBBM_VSENS_STATUS_FIFO_DATA_SHFT                                                0x0


#endif /* __MMSS_OXILI_RBBM_HWIO_H__ */
