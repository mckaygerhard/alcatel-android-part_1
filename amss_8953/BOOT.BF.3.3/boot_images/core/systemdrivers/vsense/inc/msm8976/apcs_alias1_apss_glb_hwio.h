#ifndef __APCS_ALIAS1_APSS_GLB_HWIO_H__
#define __APCS_ALIAS1_APSS_GLB_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_alias1_apss_glb_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    APCS_ALIAS1_APSS_GLB

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8976/apcs_alias1_apss_glb_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/



/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS1_APSS_GLB
 *--------------------------------------------------------------------------*/
//Arun defined it manaully
#define A53SS_BASE 0x0B000000  
 
#define APCS_ALIAS1_APSS_GLB_REG_BASE                                                     (A53SS_BASE      + 0x00011000)

#define HWIO_APCS_ALIAS1_GLB_SECURE_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS1_GLB_SECURE_RMSK                                                        0x3f
#define HWIO_APCS_ALIAS1_GLB_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR, HWIO_APCS_ALIAS1_GLB_SECURE_RMSK)
#define HWIO_APCS_ALIAS1_GLB_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_SECURE_IN)
#define HWIO_APCS_ALIAS1_GLB_SECURE_CFG_BMSK                                                    0x20
#define HWIO_APCS_ALIAS1_GLB_SECURE_CFG_SHFT                                                     0x5
#define HWIO_APCS_ALIAS1_GLB_SECURE_VOTE_BMSK                                                   0x10
#define HWIO_APCS_ALIAS1_GLB_SECURE_VOTE_SHFT                                                    0x4
#define HWIO_APCS_ALIAS1_GLB_SECURE_ACP_BMSK                                                     0x8
#define HWIO_APCS_ALIAS1_GLB_SECURE_ACP_SHFT                                                     0x3
#define HWIO_APCS_ALIAS1_GLB_SECURE_TST_BMSK                                                     0x4
#define HWIO_APCS_ALIAS1_GLB_SECURE_TST_SHFT                                                     0x2
#define HWIO_APCS_ALIAS1_GLB_SECURE_PWR_BMSK                                                     0x2
#define HWIO_APCS_ALIAS1_GLB_SECURE_PWR_SHFT                                                     0x1
#define HWIO_APCS_ALIAS1_GLB_SECURE_STS_BMSK                                                     0x1
#define HWIO_APCS_ALIAS1_GLB_SECURE_STS_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RMSK                                              0xf00007
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_ADDR,v)
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_TZ_SPARE_IPC_BMSK                                 0xf00000
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_TZ_SPARE_IPC_SHFT                                     0x14
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RPM_TZ_IPC_BMSK                                        0x7
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RPM_TZ_IPC_SHFT                                        0x0

#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADDR                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RMSK                                                 0xffff07
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADDR,v)
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_SPARE_IPC_BMSK                                       0xf00000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_SPARE_IPC_SHFT                                           0x14
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_WCN_IPC_BMSK                                          0xf0000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_WCN_IPC_SHFT                                             0x10
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_MSS_IPC_BMSK                                           0xf000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_MSS_IPC_SHFT                                              0xc
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADSP_IPC_BMSK                                           0xf00
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADSP_IPC_SHFT                                             0x8
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RPM_HLOS_IPC_BMSK                                         0x7
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RPM_HLOS_IPC_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK                                              0x7f001f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_BMSK                                    0x400000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_SHFT                                        0x16
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L1_RAM_COLLAPSE_DIS_BMSK                          0x3c0000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L1_RAM_COLLAPSE_DIS_SHFT                              0x12
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_SCU_RAM_COLLAPSE_DIS_BMSK                          0x20000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_SCU_RAM_COLLAPSE_DIS_SHFT                             0x11
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_BMSK                                0x10000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_SHFT                                   0x10
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_BMSK                                           0x1f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_RMSK                                                 0xf001f
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR, HWIO_APCS_ALIAS1_GLB_CLK_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_QGIC_WAKEUP_BMSK                                     0xf0000
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_QGIC_WAKEUP_SHFT                                        0x10
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_STS_BMSK                                                0x1f
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_STS_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK                                                  0x37ffff07
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                  0x20000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                        0x1d
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_BMSK                                      0x10000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_SHFT                                            0x1c
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_BMSK                                     0x4000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_SHFT                                          0x1a
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_BMSK                                         0x3ff0000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_SHFT                                              0x10
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_BMSK                                    0x8000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_SHFT                                       0xf
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_CLAMP_BMSK                                   0x4000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_CLAMP_SHFT                                      0xe
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_RET_SLP_BMSK                                     0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_RET_SLP_SHFT                                        0xd
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_BMSK                                            0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_SHFT                                               0xc
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_BMSK                                        0x800
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_SHFT                                          0xb
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_BMSK                                             0x400
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_SHFT                                               0xa
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_BMSK                                          0x200
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_SHFT                                            0x9
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_BMSK                                           0x100
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_SHFT                                             0x8
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_BMSK                                              0x4
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_SHFT                                              0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_BMSK                                            0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_SHFT                                            0x1
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_BMSK                                             0x1
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000018)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK                                               0x1f1f37ff
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_BMSK                                 0x10000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_SHFT                                       0x1c
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_BMSK                                    0x8000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_SHFT                                         0x1b
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_BMSK                                  0x4000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_SHFT                                       0x1a
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_BMSK                                    0x2000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_SHFT                                         0x19
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_BMSK                                    0x1000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_SHFT                                         0x18
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_BMSK                                     0x1f0000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_SHFT                                         0x10
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_BMSK                                       0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_SHFT                                          0xd
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_BMSK                                     0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_SHFT                                        0xc
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_RET_SLP_STS_BMSK                                     0x400
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_RET_SLP_STS_SHFT                                       0xa
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK                                          0x200
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT                                            0x9
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_BMSK                                        0x100
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_SHFT                                          0x8
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_ARRAY_HS_STS_BMSK                                     0xff
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_ARRAY_HS_STS_SHFT                                      0x0

#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000020)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_RMSK                                              0xff0000
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_A53_TEST_BUS_SEL_BMSK                             0xff0000
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_A53_TEST_BUS_SEL_SHFT                                 0x10

#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR                                                 (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000024)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_RMSK                                                        0x1
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_DBG_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_DBG_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_REQ_BMSK                                                    0x1
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_REQ_SHFT                                                    0x0

#define HWIO_APCS_ALIAS1_GLB_SPARE_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000028)
#define HWIO_APCS_ALIAS1_GLB_SPARE_RMSK                                                   0xc00000ff
#define HWIO_APCS_ALIAS1_GLB_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR, HWIO_APCS_ALIAS1_GLB_SPARE_RMSK)
#define HWIO_APCS_ALIAS1_GLB_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_SPARE_IN)
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_SO_IMPL_AXI10_CTRL_BMSK                        0x80000000
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_SO_IMPL_AXI10_CTRL_SHFT                              0x1f
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_CLK_GATE_FIX_BMSK                              0x40000000
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_CLK_GATE_FIX_SHFT                                    0x1e
#define HWIO_APCS_ALIAS1_GLB_SPARE_BITS_BMSK                                                    0xff
#define HWIO_APCS_ALIAS1_GLB_SPARE_BITS_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000002c)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_RMSK                                                      0xff
#define HWIO_APCS_ALIAS1_SPARE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR, HWIO_APCS_ALIAS1_SPARE_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_BITS_BMSK                                                 0xff
#define HWIO_APCS_ALIAS1_SPARE_STATUS_BITS_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_IDR_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS1_IDR_RMSK                                                          0xdffffff
#define HWIO_APCS_ALIAS1_IDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_IDR_ADDR, HWIO_APCS_ALIAS1_IDR_RMSK)
#define HWIO_APCS_ALIAS1_IDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_IDR_ADDR, m)
#define HWIO_APCS_ALIAS1_IDR_CFG_APCPLL_BMSK                                               0xc000000
#define HWIO_APCS_ALIAS1_IDR_CFG_APCPLL_SHFT                                                    0x1a
#define HWIO_APCS_ALIAS1_IDR_QDSSGEN_BMSK                                                  0x1000000
#define HWIO_APCS_ALIAS1_IDR_QDSSGEN_SHFT                                                       0x18
#define HWIO_APCS_ALIAS1_IDR_CFG_HBID_BMSK                                                  0xe00000
#define HWIO_APCS_ALIAS1_IDR_CFG_HBID_SHFT                                                      0x15
#define HWIO_APCS_ALIAS1_IDR_CFG_HPID_BMSK                                                  0x1f0000
#define HWIO_APCS_ALIAS1_IDR_CFG_HPID_SHFT                                                      0x10
#define HWIO_APCS_ALIAS1_IDR_L2_SIZE_BMSK                                                     0xf000
#define HWIO_APCS_ALIAS1_IDR_L2_SIZE_SHFT                                                        0xc
#define HWIO_APCS_ALIAS1_IDR_NUM_FRAME_BMSK                                                    0xf00
#define HWIO_APCS_ALIAS1_IDR_NUM_FRAME_SHFT                                                      0x8
#define HWIO_APCS_ALIAS1_IDR_VARIANT_BMSK                                                       0xf0
#define HWIO_APCS_ALIAS1_IDR_VARIANT_SHFT                                                        0x4
#define HWIO_APCS_ALIAS1_IDR_NUM_CPU_BMSK                                                        0xf
#define HWIO_APCS_ALIAS1_IDR_NUM_CPU_SHFT                                                        0x0

#define HWIO_APCS_ALIAS1_XO_CBCR_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS1_XO_CBCR_RMSK                                                     0x80000003
#define HWIO_APCS_ALIAS1_XO_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_XO_CBCR_ADDR, HWIO_APCS_ALIAS1_XO_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_XO_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_XO_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_XO_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_XO_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_XO_CBCR_IN)
#define HWIO_APCS_ALIAS1_XO_CBCR_CLK_OFF_BMSK                                             0x80000000
#define HWIO_APCS_ALIAS1_XO_CBCR_CLK_OFF_SHFT                                                   0x1f
#define HWIO_APCS_ALIAS1_XO_CBCR_HW_CTL_BMSK                                                     0x2
#define HWIO_APCS_ALIAS1_XO_CBCR_HW_CTL_SHFT                                                     0x1
#define HWIO_APCS_ALIAS1_XO_CBCR_SW_CTL_BMSK                                                     0x1
#define HWIO_APCS_ALIAS1_XO_CBCR_SW_CTL_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000038)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_RMSK                                                  0x80000003
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR, HWIO_APCS_ALIAS1_SLEEP_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_SLEEP_CBCR_IN)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_CLK_OFF_BMSK                                          0x80000000
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_CLK_OFF_SHFT                                                0x1f
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_HW_CTL_BMSK                                                  0x2
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_HW_CTL_SHFT                                                  0x1
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_SW_CTL_BMSK                                                  0x1
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_SW_CTL_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_CMD_RCGR_ADDR                                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS1_CMD_RCGR_RMSK                                                    0x80000013
#define HWIO_APCS_ALIAS1_CMD_RCGR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CMD_RCGR_ADDR, HWIO_APCS_ALIAS1_CMD_RCGR_RMSK)
#define HWIO_APCS_ALIAS1_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CMD_RCGR_ADDR, m)
#define HWIO_APCS_ALIAS1_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CMD_RCGR_ADDR,v)
#define HWIO_APCS_ALIAS1_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CMD_RCGR_ADDR,m,v,HWIO_APCS_ALIAS1_CMD_RCGR_IN)
#define HWIO_APCS_ALIAS1_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_APCS_ALIAS1_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_APCS_ALIAS1_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_APCS_ALIAS1_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_APCS_ALIAS1_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_APCS_ALIAS1_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_APCS_ALIAS1_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_APCS_ALIAS1_CFG_RCGR_ADDR                                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS1_CFG_RCGR_RMSK                                                         0xf1f
#define HWIO_APCS_ALIAS1_CFG_RCGR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CFG_RCGR_ADDR, HWIO_APCS_ALIAS1_CFG_RCGR_RMSK)
#define HWIO_APCS_ALIAS1_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CFG_RCGR_ADDR, m)
#define HWIO_APCS_ALIAS1_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CFG_RCGR_ADDR,v)
#define HWIO_APCS_ALIAS1_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CFG_RCGR_ADDR,m,v,HWIO_APCS_ALIAS1_CFG_RCGR_IN)
#define HWIO_APCS_ALIAS1_CFG_RCGR_ALT_SEL_BMSK                                                 0x800
#define HWIO_APCS_ALIAS1_CFG_RCGR_ALT_SEL_SHFT                                                   0xb
#define HWIO_APCS_ALIAS1_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_APCS_ALIAS1_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_APCS_ALIAS1_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_APCS_ALIAS1_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_CORE_CBCR_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS1_CORE_CBCR_RMSK                                                   0x80000003
#define HWIO_APCS_ALIAS1_CORE_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, HWIO_APCS_ALIAS1_CORE_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_CORE_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_CORE_CBCR_IN)
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_APCS_ALIAS1_CORE_CBCR_HW_CTL_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_CORE_CBCR_HW_CTL_SHFT                                                   0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_ENABLE_SHFT                                               0x0

#define HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_RMSK                                                          0x1
#define HWIO_APCS_ALIAS1_RBCPR_BCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR, HWIO_APCS_ALIAS1_RBCPR_BCR_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_BCR_IN)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_BLK_ARES_BMSK                                                 0x1
#define HWIO_APCS_ALIAS1_RBCPR_BCR_BLK_ARES_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_RBCPR_CBCR_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_RMSK                                                  0x80000003
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_CBCR_ADDR, HWIO_APCS_ALIAS1_RBCPR_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_CBCR_IN)
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_CLK_OFF_BMSK                                          0x80000000
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_CLK_OFF_SHFT                                                0x1f
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_HW_CTL_BMSK                                                  0x2
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_HW_CTL_SHFT                                                  0x1
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_SW_CTL_BMSK                                                  0x1
#define HWIO_APCS_ALIAS1_RBCPR_CBCR_SW_CTL_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000068)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_RMSK                                                         0x3
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR, HWIO_APCS_ALIAS1_RBCPR_PMIC_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_PMIC_IN)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_ADDRIDX_BMSK                                            0x2
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_ADDRIDX_SHFT                                            0x1
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_SIZE_BMSK                                               0x1
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_SIZE_SHFT                                               0x0

#define HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR                                                 (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000006c)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_RMSK                                                 0xffff003c
#define HWIO_APCS_ALIAS1_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS1_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR,v)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR,m,v,HWIO_APCS_ALIAS1_A53_CFG_STS_IN)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF1_BMSK                                   0xff000000
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF1_SHFT                                         0x18
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF2_BMSK                                     0xff0000
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF2_SHFT                                         0x10
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTINNER_BMSK                                        0x20
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTINNER_SHFT                                         0x5
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTOUTER_BMSK                                        0x10
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTOUTER_SHFT                                         0x4
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTCACHEMAINT_BMSK                                    0x8
#define HWIO_APCS_ALIAS1_A53_CFG_STS_BROADCASTCACHEMAINT_SHFT                                    0x3
#define HWIO_APCS_ALIAS1_A53_CFG_STS_SYSBARDISABLE_BMSK                                          0x4
#define HWIO_APCS_ALIAS1_A53_CFG_STS_SYSBARDISABLE_SHFT                                          0x2

#define HWIO_APCS_ALIAS1_AUX_CLK_ON_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_RMSK                                                         0xf
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_AUX_CLK_ON_ADDR, HWIO_APCS_ALIAS1_AUX_CLK_ON_RMSK)
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_AUX_CLK_ON_ADDR, m)
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_AUX_CLK_ON_ADDR,v)
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_AUX_CLK_ON_ADDR,m,v,HWIO_APCS_ALIAS1_AUX_CLK_ON_IN)
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_AUX_CLK_ON_REQ_BMSK                                          0xf
#define HWIO_APCS_ALIAS1_AUX_CLK_ON_AUX_CLK_ON_REQ_SHFT                                          0x0

#define HWIO_APCS_ALIAS1_QSB_LP_ADDR                                                      (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS1_QSB_LP_RMSK                                                            0x7f
#define HWIO_APCS_ALIAS1_QSB_LP_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QSB_LP_ADDR, HWIO_APCS_ALIAS1_QSB_LP_RMSK)
#define HWIO_APCS_ALIAS1_QSB_LP_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QSB_LP_ADDR, m)
#define HWIO_APCS_ALIAS1_QSB_LP_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QSB_LP_ADDR,v)
#define HWIO_APCS_ALIAS1_QSB_LP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QSB_LP_ADDR,m,v,HWIO_APCS_ALIAS1_QSB_LP_IN)
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_CNTR_BMSK                                                0x7e
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_CNTR_SHFT                                                 0x1
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_OVERRIDE_BIT_BMSK                                         0x1
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_OVERRIDE_BIT_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_FFWD_BCR_ADDR                                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS1_FFWD_BCR_RMSK                                                           0x1
#define HWIO_APCS_ALIAS1_FFWD_BCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_FFWD_BCR_ADDR, HWIO_APCS_ALIAS1_FFWD_BCR_RMSK)
#define HWIO_APCS_ALIAS1_FFWD_BCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_FFWD_BCR_ADDR, m)
#define HWIO_APCS_ALIAS1_FFWD_BCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_FFWD_BCR_ADDR,v)
#define HWIO_APCS_ALIAS1_FFWD_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_FFWD_BCR_ADDR,m,v,HWIO_APCS_ALIAS1_FFWD_BCR_IN)
#define HWIO_APCS_ALIAS1_FFWD_BCR_BLK_ARES_BMSK                                                  0x1
#define HWIO_APCS_ALIAS1_FFWD_BCR_BLK_ARES_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_FFWD_CBCR_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS1_FFWD_CBCR_RMSK                                                   0x80000003
#define HWIO_APCS_ALIAS1_FFWD_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_FFWD_CBCR_ADDR, HWIO_APCS_ALIAS1_FFWD_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_FFWD_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_FFWD_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_FFWD_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_FFWD_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_FFWD_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_FFWD_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_FFWD_CBCR_IN)
#define HWIO_APCS_ALIAS1_FFWD_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_APCS_ALIAS1_FFWD_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_APCS_ALIAS1_FFWD_CBCR_HW_CTL_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_FFWD_CBCR_HW_CTL_SHFT                                                   0x1
#define HWIO_APCS_ALIAS1_FFWD_CBCR_SW_CTL_BMSK                                                   0x1
#define HWIO_APCS_ALIAS1_FFWD_CBCR_SW_CTL_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000100)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_RMSK                                                  0xf3
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_ADDR, HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_RMSK)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_IN)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_OVERIDE_VAL_BMSK                                0xf0
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_OVERIDE_VAL_SHFT                                 0x4
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_MODE_EN_BMSK                                     0x2
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_MODE_EN_SHFT                                     0x1
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_OVERIDE_BMSK                                     0x1
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_CTL_STATE_OVERIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000104)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_RMSK                                         0xffffffff
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR, HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_BOOTFSM_STATUS_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_BOOTFSM_STATUS_SHFT                                 0x0

#define HWIO_APCS_ALIAS1_VERSION_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000fd0)
#define HWIO_APCS_ALIAS1_VERSION_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS1_VERSION_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_VERSION_ADDR, HWIO_APCS_ALIAS1_VERSION_RMSK)
#define HWIO_APCS_ALIAS1_VERSION_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_VERSION_ADDR, m)
#define HWIO_APCS_ALIAS1_VERSION_MAJOR_BMSK                                               0xf0000000
#define HWIO_APCS_ALIAS1_VERSION_MAJOR_SHFT                                                     0x1c
#define HWIO_APCS_ALIAS1_VERSION_MINOR_BMSK                                                0xfff0000
#define HWIO_APCS_ALIAS1_VERSION_MINOR_SHFT                                                     0x10
#define HWIO_APCS_ALIAS1_VERSION_STEP_BMSK                                                    0xffff
#define HWIO_APCS_ALIAS1_VERSION_STEP_SHFT                                                       0x0

#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_ADDR                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000110)
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_RMSK                                           0x101ff
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                         0x10000
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                            0x10
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                               0x1ff
#define HWIO_APCS_ALIAS1_L2_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                 0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000200)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RMSK                                              0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                            0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                  0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                             0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                   0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                     0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                          0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000204)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RMSK                                            0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                          0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                           0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                 0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                   0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                        0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR                                      (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000208)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RMSK                                      0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK            0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                  0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK             0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                   0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                     0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                          0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_ADDR                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000020c)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RMSK                                       0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK              0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                    0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK               0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                     0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                       0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                            0x0

#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_ADDR                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000214)
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_RMSK                                                     0x3
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_ADDR, HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_RMSK)
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_ADDR, m)
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_ADDR,v)
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_ADDR,m,v,HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_IN)
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_SEL_BMSK                                                 0x3
#define HWIO_APCS_ALIAS1_CPUSPM_RPM_MUX_SEL_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000218)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_RMSK                                                      0x11
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR, HWIO_APCS_ALIAS1_L2_FLUSH_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_FLUSH_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ACINACTM_BMSK                                             0x10
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_ACINACTM_SHFT                                              0x4
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_L2_FLUSH_REQ_BMSK                                          0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTL_L2_FLUSH_REQ_SHFT                                          0x0

#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000234)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_RMSK                                                       0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR, HWIO_APCS_ALIAS1_L2_FLUSH_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_L2_FLUSH_DONE_BMSK                                         0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_L2_FLUSH_DONE_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR                                                 (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000220)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_RMSK                                                 0x80ffffff
#define HWIO_APCS_ALIAS1_AINACTS_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR, HWIO_APCS_ALIAS1_AINACTS_CNT_RMSK)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR, m)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR,v)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR,m,v,HWIO_APCS_ALIAS1_AINACTS_CNT_IN)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_WIP_BMSK                                 0x80000000
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_WIP_SHFT                                       0x1f
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_VAL_BMSK                                   0xffffff
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_VAL_SHFT                                        0x0

#define HWIO_APCS_ALIAS1_ACP_DBG_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000224)
#define HWIO_APCS_ALIAS1_ACP_DBG_RMSK                                                            0x3
#define HWIO_APCS_ALIAS1_ACP_DBG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_DBG_ADDR, HWIO_APCS_ALIAS1_ACP_DBG_RMSK)
#define HWIO_APCS_ALIAS1_ACP_DBG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_DBG_ADDR, m)
#define HWIO_APCS_ALIAS1_ACP_DBG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_ACP_DBG_ADDR,v)
#define HWIO_APCS_ALIAS1_ACP_DBG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_ACP_DBG_ADDR,m,v,HWIO_APCS_ALIAS1_ACP_DBG_IN)
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_VAL_BMSK                                                0x2
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_VAL_SHFT                                                0x1
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_OVERRIDE_BMSK                                           0x1
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_OVERRIDE_SHFT                                           0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000244)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_RMSK                                           0xffffffff
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR,v)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR,m,v,HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_IN)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACTIVE_BMSK                               0xf0000000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACTIVE_SHFT                                     0x1c
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQDENY_BMSK                                  0xf000000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQDENY_SHFT                                       0x18
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACCEPTN_BMSK                                0xf00000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACCEPTN_SHFT                                    0x14
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQREQN_BMSK                                    0xf0000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQREQN_SHFT                                       0x10
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACTIVE_BMSK                                    0xf000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACTIVE_SHFT                                       0xc
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQDENY_BMSK                                       0xf00
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQDENY_SHFT                                         0x8
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACCEPTN_BMSK                                     0xf0
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACCEPTN_SHFT                                      0x4
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQREQN_BMSK                                         0xf
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQREQN_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR                                                 (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000248)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_RMSK                                                        0xf
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_L2_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR,v)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR,m,v,HWIO_APCS_ALIAS1_QCHANNEL_L2_IN)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACTIVE_BMSK                                              0x8
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACTIVE_SHFT                                              0x3
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QDENY_BMSK                                                0x4
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QDENY_SHFT                                                0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACCEPTN_BMSK                                             0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACCEPTN_SHFT                                             0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QREQN_BMSK                                                0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QREQN_SHFT                                                0x0

#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000022c)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_RMSK                                                     0x110
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_MISC_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_MISC_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_GATE_ACP_CLK_BMSK                                        0x100
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_GATE_ACP_CLK_SHFT                                          0x8
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ACP_STALL_REQ_BMSK                                        0x10
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ACP_STALL_REQ_SHFT                                         0x4

#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000230)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_RMSK                                                     0x110
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR, HWIO_APCS_ALIAS1_MISC_PWR_STS_RMSK)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ACP_STALL_ACK_BMSK                                        0x10
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ACP_STALL_ACK_SHFT                                         0x4

#define HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000240)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_RMSK                                                      0xf
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR, HWIO_APCS_ALIAS1_CORE_HS_STATE_RMSK)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_HS_STATE_BMSK                                             0xf
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_HS_STATE_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_ADDR                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_RMSK                                              0xf
#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_ADDR, HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_RMSK)
#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_APC_ITM_ARB_STATE_BMSK                            0xf
#define HWIO_APCS_ALIAS1_APC_ITM_ARB_STATE_CSR_APC_ITM_ARB_STATE_SHFT                            0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000250)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_RMSK                                             0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_EN_BMSK                                          0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_EN_SHFT                                           0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000254)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_RMSK                                                0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                      0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                       0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000258)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_RMSK                                       0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                    0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000025c)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_RMSK                                          0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                              0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                   0x0

#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000260)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_RMSK                                                  0xff
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_EVENT_VAL_BMSK                                        0xff
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_EVENT_VAL_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000264)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_RMSK                                            0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                     0x0

#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000210)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_RMSK                                                0x7
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                0x4
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                0x2
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IGNORE_BMSK                                         0x2
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IGNORE_SHFT                                         0x1
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                     0x1
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                     0x0

#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_ADDR                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000268)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_RMSK                                    0x8000000f
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_RESET_BMSK           0x80000000
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_RESET_SHFT                 0x1f
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_FREEZE_BMSK                 0x8
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_FREEZE_SHFT                 0x3
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_CLK_EN_BMSK                 0x4
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_CLK_EN_SHFT                 0x2
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_MODE_BMSK                   0x3
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_CFG_SPM_WAKEUP_COUNTER_MODE_SHFT                   0x0

#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_ADDR                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000026c)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_RMSK                                        0xffffffff
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_SPM_WAKEUP_COUNTER_VALUE_BMSK               0xffffffff
#define HWIO_APCS_ALIAS1_L2SPM_WAKEUP_COUNTER_SPM_WAKEUP_COUNTER_VALUE_SHFT                      0x0

#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_ADDR                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000270)
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_RMSK                                                0x3
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_ADDR, HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_RMSK)
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_ADDR, m)
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_ADDR,v)
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_ADDR,m,v,HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_IN)
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_PDXFIFO_WR_PNTR_SEL_VAL_BMSK                        0x3
#define HWIO_APCS_ALIAS1_PDXFIFO_WR_PNTR_SEL_PDXFIFO_WR_PNTR_SEL_VAL_SHFT                        0x0

#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000274)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_RMSK                                            0x8000001f
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_ADDR, HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_RMSK)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_ADDR, m)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_ADDR,v)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_ADDR,m,v,HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_IN)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_ON_BMSK                    0x80000000
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_ON_SHFT                          0x1f
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CDIV_DIV_VALUE_BMSK                      0x1c
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CDIV_DIV_VALUE_SHFT                       0x2
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_CTL_BMSK                          0x2
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_CTL_SHFT                          0x1
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_EN_BMSK                           0x1
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_CBC_STANDBY_EXIT_CBC_CLK_EN_SHFT                           0x0

#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000278)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_RMSK                                                   0xf
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_ADDR, HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_RMSK)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_ADDR, m)
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_STANDBY_EXIT_ITM_STATE_BMSK                            0xf
#define HWIO_APCS_ALIAS1_STANDBY_EXIT_ITM_STANDBY_EXIT_ITM_STATE_SHFT                            0x0

#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_ADDR                                             (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000280)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_RMSK                                             0x80000003
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_ADDR, HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_IN)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_CLK_OFF_BMSK                                     0x80000000
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_CLK_OFF_SHFT                                           0x1f
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_HW_CTL_BMSK                                             0x2
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_HW_CTL_SHFT                                             0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_SW_CTL_BMSK                                             0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CBCR_SW_CTL_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_ADDR                                             (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000284)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_RMSK                                             0x80000003
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_ADDR, HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_IN)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_CLK_OFF_BMSK                                     0x80000000
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_CLK_OFF_SHFT                                           0x1f
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_HW_CTL_BMSK                                             0x2
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_HW_CTL_SHFT                                             0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_SW_CTL_BMSK                                             0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CBCR_SW_CTL_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000288)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_RMSK                                                 0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR, HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_IN)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_SEL_BMSK                                         0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_SEL_SHFT                                         0x0

#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000028c)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_RMSK                                                 0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR, HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_SEL_BMSK                                         0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_SEL_SHFT                                         0x0



#endif /* __APCS_ALIAS1_APSS_GLB_HWIO_H__ */
