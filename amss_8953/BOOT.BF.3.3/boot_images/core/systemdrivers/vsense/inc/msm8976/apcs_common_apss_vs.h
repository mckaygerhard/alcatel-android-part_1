#ifndef __APCS_COMMON_APSS_VS_H__
#define __APCS_COMMON_APSS_VS_H__
/*
===========================================================================
*/
/**
  @file apcs_common_apss_vs.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8956 (Eldarion) [eldarion_v1.0_p3q1r15]
 
  This file contains HWIO register definitions for the following modules:
    APCS_COMMON_APSS_VS

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8976/apcs_common_apss_vs.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: APCS_COMMON_APSS_VS
 *--------------------------------------------------------------------------*/

 //Arun defined it manaully
#define A53SS_BASE 0x0B000000  
 
#define APCS_COMMON_APSS_VS_REG_BASE                                          (A53SS_BASE      + 0x001d6000)

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000000)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESET_FUNC_BMSK                  0x80000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESET_FUNC_SHFT                        0x1f
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_MODE_SEL_BMSK                    0x40000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_MODE_SEL_SHFT                          0x1e
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_VCO_POWER_SEL_BMSK               0x20000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_VCO_POWER_SEL_SHFT                     0x1d
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_SEL_BMSK                    0x18000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_SEL_SHFT                          0x1b
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_TRIGGER_BMSK                   0x4000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_TRIGGER_SHFT                        0x1a
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_START_CAPTURE_BMSK                0x2000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_START_CAPTURE_SHFT                     0x19
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_POWER_BMSK                     0x1000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_POWER_SHFT                          0x18
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                 0xff0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                     0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                   0xff00
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                      0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                     0xf0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                      0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIGGER_POS_BMSK                        0xc
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIGGER_POS_SHFT                        0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESERVE_BMSK                            0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESERVE_SHFT                            0x0

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000004)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK                               0xffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SEL_JTAG_CSR_BMSK                  0x800000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SEL_JTAG_CSR_SHFT                      0x17
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FIFO_ADDR_BMSK                     0x7e0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FIFO_ADDR_SHFT                         0x11
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_BMSK             0x10000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_SHFT                0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                     0xe000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                        0xd
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_THRESHOLD_BMSK                 0x1fe0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_THRESHOLD_SHFT                    0x5
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_FUNC_BMSK                           0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_FUNC_SHFT                            0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLAMP_DIS_BMSK                          0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLAMP_DIS_SHFT                          0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_STATUS_CLEAR_BMSK                       0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_STATUS_CLEAR_SHFT                       0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_ALARM_BMSK                           0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_ALARM_SHFT                           0x1
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_BMSK                   0x1
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_SHFT                   0x0

#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR                               (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000008)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK                                  0x3ffff
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_BMSK                        0x3c000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_SHFT                            0xe
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_NEG_BMSK                         0x2000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_NEG_SHFT                            0xd
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_POS_BMSK                         0x1000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_POS_SHFT                            0xc
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_BMSK                          0x800
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_SHFT                            0xb
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_BMSK                          0x400
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_SHFT                            0xa
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                    0x200
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                      0x9
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                  0x100
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                    0x8
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_BMSK                           0xff
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_SHFT                            0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESET_FUNC_BMSK                  0x80000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESET_FUNC_SHFT                        0x1f
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_MODE_SEL_BMSK                    0x40000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_MODE_SEL_SHFT                          0x1e
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_VCO_POWER_SEL_BMSK               0x20000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_VCO_POWER_SEL_SHFT                     0x1d
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_SEL_BMSK                    0x18000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_SEL_SHFT                          0x1b
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_TRIGGER_BMSK                   0x4000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_TRIGGER_SHFT                        0x1a
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_START_CAPTURE_BMSK                0x2000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_START_CAPTURE_SHFT                     0x19
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_POWER_BMSK                     0x1000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_POWER_SHFT                          0x18
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                 0xff0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                     0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                   0xff00
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                      0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                     0xf0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                      0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIGGER_POS_BMSK                        0xc
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIGGER_POS_SHFT                        0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESERVE_BMSK                            0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESERVE_SHFT                            0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000010)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK                               0xffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SEL_JTAG_CSR_BMSK                  0x800000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SEL_JTAG_CSR_SHFT                      0x17
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FIFO_ADDR_BMSK                     0x7e0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FIFO_ADDR_SHFT                         0x11
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_BMSK             0x10000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_SHFT                0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                     0xe000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                        0xd
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_THRESHOLD_BMSK                 0x1fe0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_THRESHOLD_SHFT                    0x5
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_FUNC_BMSK                           0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_FUNC_SHFT                            0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLAMP_DIS_BMSK                          0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLAMP_DIS_SHFT                          0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_STATUS_CLEAR_BMSK                       0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_STATUS_CLEAR_SHFT                       0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_ALARM_BMSK                           0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_ALARM_SHFT                           0x1
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_BMSK                   0x1
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_SHFT                   0x0

#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR                               (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000014)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK                                  0x3ffff
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_BMSK                        0x3c000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_SHFT                            0xe
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_NEG_BMSK                         0x2000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_NEG_SHFT                            0xd
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_POS_BMSK                         0x1000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_POS_SHFT                            0xc
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_BMSK                          0x800
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_SHFT                            0xb
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_BMSK                          0x400
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_SHFT                            0xa
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                    0x200
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                      0x9
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                  0x100
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                    0x8
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_BMSK                           0xff
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_SHFT                            0x0


#endif /* __APCS_COMMON_APSS_VS_H__ */
