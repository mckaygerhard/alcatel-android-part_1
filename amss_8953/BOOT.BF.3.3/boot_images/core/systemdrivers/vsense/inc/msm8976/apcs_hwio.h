#ifndef __APCS_HWIO_H__
#define __APCS_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8956 (Eldarion) [eldarion_v1.0_p3q1r15]
 
  This file contains HWIO register definitions for the following modules:
    APCS_COMMON_APSS_SHARED

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8976/apcs_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: APCS_COMMON_APSS_SHARED
 *--------------------------------------------------------------------------*/
//Arun defined it manaully
#define A53SS_BASE 0x0B000000  
 
#define APCS_COMMON_APSS_SHARED_REG_BASE                                            (A53SS_BASE      + 0x001d1000)

#define HWIO_APCS_COMMON_SHR_SECURE_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000004)
#define HWIO_APCS_COMMON_SHR_SECURE_RMSK                                                   0x3
#define HWIO_APCS_COMMON_SHR_SECURE_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SHR_SECURE_ADDR, HWIO_APCS_COMMON_SHR_SECURE_RMSK)
#define HWIO_APCS_COMMON_SHR_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SHR_SECURE_ADDR, m)
#define HWIO_APCS_COMMON_SHR_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SHR_SECURE_ADDR,v)
#define HWIO_APCS_COMMON_SHR_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SHR_SECURE_ADDR,m,v,HWIO_APCS_COMMON_SHR_SECURE_IN)
#define HWIO_APCS_COMMON_SHR_SECURE_CLK_BMSK                                               0x2
#define HWIO_APCS_COMMON_SHR_SECURE_CLK_SHFT                                               0x1
#define HWIO_APCS_COMMON_SHR_SECURE_PWR_BMSK                                               0x1
#define HWIO_APCS_COMMON_SHR_SECURE_PWR_SHFT                                               0x0

#define HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR                                         (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000100)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_RMSK                                               0xef
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR, HWIO_APCS_COMMON_CCI_INPUT_CFG_RMSK)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR, m)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR,v)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR,m,v,HWIO_APCS_COMMON_CCI_INPUT_CFG_IN)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_CFG_CLRMON_DISABLE_BMSK                            0xc0
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_CFG_CLRMON_DISABLE_SHFT                             0x6
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_CCI_TSREADY_FORCE_DISABLE_BMSK                     0x20
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_CCI_TSREADY_FORCE_DISABLE_SHFT                      0x5
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_PROTNSS2_BMSK                                       0x8
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_PROTNSS2_SHFT                                       0x3
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BARRIERTERMINATE_BMSK                               0x7
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BARRIERTERMINATE_SHFT                               0x0

#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000104)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_RMSK                                          0xffffffff
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_IN          \
        in_dword_masked(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR, HWIO_APCS_COMMON_ADDRMAPS_CCI_RMSK)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR, m)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR,v)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR,m,v,HWIO_APCS_COMMON_ADDRMAPS_CCI_IN)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP15_BMSK                                0xc0000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP15_SHFT                                      0x1e
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP14_BMSK                                0x30000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP14_SHFT                                      0x1c
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP13_BMSK                                 0xc000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP13_SHFT                                      0x1a
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP12_BMSK                                 0x3000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP12_SHFT                                      0x18
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP11_BMSK                                  0xc00000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP11_SHFT                                      0x16
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP10_BMSK                                  0x300000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP10_SHFT                                      0x14
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP9_BMSK                                    0xc0000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP9_SHFT                                       0x12
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP8_BMSK                                    0x30000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP8_SHFT                                       0x10
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP7_BMSK                                     0xc000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP7_SHFT                                        0xe
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP6_BMSK                                     0x3000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP6_SHFT                                        0xc
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP5_BMSK                                      0xc00
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP5_SHFT                                        0xa
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP4_BMSK                                      0x300
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP4_SHFT                                        0x8
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP3_BMSK                                       0xc0
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP3_SHFT                                        0x6
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP2_BMSK                                       0x30
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP2_SHFT                                        0x4
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP1_BMSK                                        0xc
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP1_SHFT                                        0x2
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP0_BMSK                                        0x3
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP0_SHFT                                        0x0

#define HWIO_APCS_COMMON_CCI_RCGR_MUX_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000004c)
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_RMSK                                                 0x1
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_RCGR_MUX_ADDR, HWIO_APCS_COMMON_CCI_RCGR_MUX_RMSK)
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_RCGR_MUX_ADDR, m)
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_RCGR_MUX_ADDR,v)
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_RCGR_MUX_ADDR,m,v,HWIO_APCS_COMMON_CCI_RCGR_MUX_IN)
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_MUX_SEL_BMSK                                         0x1
#define HWIO_APCS_COMMON_CCI_RCGR_MUX_MUX_SEL_SHFT                                         0x0

#define HWIO_APCS_COMMON_CCI_CMD_RCGR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000050)
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_RMSK                                          0x80000013
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CMD_RCGR_ADDR, HWIO_APCS_COMMON_CCI_CMD_RCGR_RMSK)
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CMD_RCGR_ADDR, m)
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_CMD_RCGR_ADDR,v)
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_CMD_RCGR_ADDR,m,v,HWIO_APCS_COMMON_CCI_CMD_RCGR_IN)
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_ROOT_OFF_BMSK                                 0x80000000
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_ROOT_OFF_SHFT                                       0x1f
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                 0x10
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                  0x4
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_ROOT_EN_BMSK                                         0x2
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_ROOT_EN_SHFT                                         0x1
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_UPDATE_BMSK                                          0x1
#define HWIO_APCS_COMMON_CCI_CMD_RCGR_UPDATE_SHFT                                          0x0

#define HWIO_APCS_COMMON_CCI_CFG_RCGR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000054)
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_RMSK                                               0xf1f
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CFG_RCGR_ADDR, HWIO_APCS_COMMON_CCI_CFG_RCGR_RMSK)
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CFG_RCGR_ADDR, m)
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_CFG_RCGR_ADDR,v)
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_CFG_RCGR_ADDR,m,v,HWIO_APCS_COMMON_CCI_CFG_RCGR_IN)
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_ALT_SEL_BMSK                                       0x800
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_ALT_SEL_SHFT                                         0xb
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_SRC_SEL_BMSK                                       0x700
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_SRC_SEL_SHFT                                         0x8
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_SRC_DIV_BMSK                                        0x1f
#define HWIO_APCS_COMMON_CCI_CFG_RCGR_SRC_DIV_SHFT                                         0x0

#define HWIO_APCS_COMMON_CCI_CBCR_ADDR                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000058)
#define HWIO_APCS_COMMON_CCI_CBCR_RMSK                                              0x80000003
#define HWIO_APCS_COMMON_CCI_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CBCR_ADDR, HWIO_APCS_COMMON_CCI_CBCR_RMSK)
#define HWIO_APCS_COMMON_CCI_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_CCI_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_CCI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_CBCR_ADDR,m,v,HWIO_APCS_COMMON_CCI_CBCR_IN)
#define HWIO_APCS_COMMON_CCI_CBCR_CLK_OFF_BMSK                                      0x80000000
#define HWIO_APCS_COMMON_CCI_CBCR_CLK_OFF_SHFT                                            0x1f
#define HWIO_APCS_COMMON_CCI_CBCR_HW_CTL_BMSK                                              0x2
#define HWIO_APCS_COMMON_CCI_CBCR_HW_CTL_SHFT                                              0x1
#define HWIO_APCS_COMMON_CCI_CBCR_CLK_ENABLE_BMSK                                          0x1
#define HWIO_APCS_COMMON_CCI_CBCR_CLK_ENABLE_SHFT                                          0x0

#define HWIO_APCS_COMMON_CCI_HIER_CG_ADDR                                           (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000005c)
#define HWIO_APCS_COMMON_CCI_HIER_CG_RMSK                                               0x3ff1
#define HWIO_APCS_COMMON_CCI_HIER_CG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR, HWIO_APCS_COMMON_CCI_HIER_CG_RMSK)
#define HWIO_APCS_COMMON_CCI_HIER_CG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR, m)
#define HWIO_APCS_COMMON_CCI_HIER_CG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR,v)
#define HWIO_APCS_COMMON_CCI_HIER_CG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR,m,v,HWIO_APCS_COMMON_CCI_HIER_CG_IN)
#define HWIO_APCS_COMMON_CCI_HIER_CG_TIMER_CNT_VAL_BMSK                                 0x3ff0
#define HWIO_APCS_COMMON_CCI_HIER_CG_TIMER_CNT_VAL_SHFT                                    0x4
#define HWIO_APCS_COMMON_CCI_HIER_CG_HIER_CG_EN_BMSK                                       0x1
#define HWIO_APCS_COMMON_CCI_HIER_CG_HIER_CG_EN_SHFT                                       0x0

#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_ADDR                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000070)
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_RMSK                                         0xf
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_ADDR, HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_RMSK)
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_ADDR, m)
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_ADDR,v)
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_ADDR,m,v,HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_IN)
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_CLK_DIV_BMSK                                 0xf
#define HWIO_APCS_COMMON_L1L2_SLP_SEQ_CLK_CTL_CLK_DIV_SHFT                                 0x0

#define HWIO_APCS_COMMON_CONFIG_BID_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000110)
#define HWIO_APCS_COMMON_CONFIG_BID_RMSK                                                   0x7
#define HWIO_APCS_COMMON_CONFIG_BID_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BID_ADDR, HWIO_APCS_COMMON_CONFIG_BID_RMSK)
#define HWIO_APCS_COMMON_CONFIG_BID_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BID_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_BID_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_BID_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_BID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_BID_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_BID_IN)
#define HWIO_APCS_COMMON_CONFIG_BID_CONFIG_BID_BMSK                                        0x7
#define HWIO_APCS_COMMON_CONFIG_BID_CONFIG_BID_SHFT                                        0x0

#define HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000118)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_RMSK                                             0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR, HWIO_APCS_COMMON_CONFIG_MID_APC0_RMSK)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_MID_APC0_IN)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_CONFIG_MID_APC0_BMSK                             0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_CONFIG_MID_APC0_SHFT                              0x0

#define HWIO_APCS_COMMON_CONFIG_PID_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000114)
#define HWIO_APCS_COMMON_CONFIG_PID_RMSK                                                  0x1f
#define HWIO_APCS_COMMON_CONFIG_PID_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_PID_ADDR, HWIO_APCS_COMMON_CONFIG_PID_RMSK)
#define HWIO_APCS_COMMON_CONFIG_PID_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_PID_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_PID_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_PID_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_PID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_PID_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_PID_IN)
#define HWIO_APCS_COMMON_CONFIG_PID_CONFIG_PID_BMSK                                       0x1f
#define HWIO_APCS_COMMON_CONFIG_PID_CONFIG_PID_SHFT                                        0x0

#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000011c)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_RMSK                                             0xf
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR, HWIO_APCS_COMMON_CONFIG_BPMID_SEL_RMSK)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_BPMID_SEL_IN)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_BID_SEL_BMSK                             0x8
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_BID_SEL_SHFT                             0x3
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_PID_SEL_BMSK                             0x4
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_PID_SEL_SHFT                             0x2
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_MID_APC1_SEL_BMSK                        0x2
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_MID_APC1_SEL_SHFT                        0x1
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_MID_APC0_SEL_BMSK                        0x1
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CSR_CFG_MID_APC0_SEL_SHFT                        0x0

#define HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000120)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_RMSK                                             0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR, HWIO_APCS_COMMON_CONFIG_MID_APC1_RMSK)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_MID_APC1_IN)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_CONFIG_MID_APC1_BMSK                             0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_CONFIG_MID_APC1_SHFT                              0x0

#define HWIO_APCS_COMMON_PS_SW_RST_ADDR                                             (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000124)
#define HWIO_APCS_COMMON_PS_SW_RST_RMSK                                                    0x3
#define HWIO_APCS_COMMON_PS_SW_RST_IN          \
        in_dword_masked(HWIO_APCS_COMMON_PS_SW_RST_ADDR, HWIO_APCS_COMMON_PS_SW_RST_RMSK)
#define HWIO_APCS_COMMON_PS_SW_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_PS_SW_RST_ADDR, m)
#define HWIO_APCS_COMMON_PS_SW_RST_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_PS_SW_RST_ADDR,v)
#define HWIO_APCS_COMMON_PS_SW_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_PS_SW_RST_ADDR,m,v,HWIO_APCS_COMMON_PS_SW_RST_IN)
#define HWIO_APCS_COMMON_PS_SW_RST_A57_PS_SOFT_RST_BMSK                                    0x2
#define HWIO_APCS_COMMON_PS_SW_RST_A57_PS_SOFT_RST_SHFT                                    0x1
#define HWIO_APCS_COMMON_PS_SW_RST_A53_PS_SOFT_RST_BMSK                                    0x1
#define HWIO_APCS_COMMON_PS_SW_RST_A53_PS_SOFT_RST_SHFT                                    0x0

#define HWIO_APCS_COMMON_LMH_CMD_RCGR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000012c)
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_RMSK                                          0x80000013
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_LMH_CMD_RCGR_ADDR, HWIO_APCS_COMMON_LMH_CMD_RCGR_RMSK)
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_LMH_CMD_RCGR_ADDR, m)
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_LMH_CMD_RCGR_ADDR,v)
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_LMH_CMD_RCGR_ADDR,m,v,HWIO_APCS_COMMON_LMH_CMD_RCGR_IN)
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_ROOT_OFF_BMSK                                 0x80000000
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_ROOT_OFF_SHFT                                       0x1f
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                 0x10
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                  0x4
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_ROOT_EN_BMSK                                         0x2
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_ROOT_EN_SHFT                                         0x1
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_UPDATE_BMSK                                          0x1
#define HWIO_APCS_COMMON_LMH_CMD_RCGR_UPDATE_SHFT                                          0x0

#define HWIO_APCS_COMMON_LMH_CFG_RCGR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000130)
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_RMSK                                               0xf1f
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_LMH_CFG_RCGR_ADDR, HWIO_APCS_COMMON_LMH_CFG_RCGR_RMSK)
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_LMH_CFG_RCGR_ADDR, m)
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_LMH_CFG_RCGR_ADDR,v)
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_LMH_CFG_RCGR_ADDR,m,v,HWIO_APCS_COMMON_LMH_CFG_RCGR_IN)
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_ALT_SEL_BMSK                                       0x800
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_ALT_SEL_SHFT                                         0xb
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_SRC_SEL_BMSK                                       0x700
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_SRC_SEL_SHFT                                         0x8
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_SRC_DIV_BMSK                                        0x1f
#define HWIO_APCS_COMMON_LMH_CFG_RCGR_SRC_DIV_SHFT                                         0x0

#define HWIO_APCS_COMMON_RVBARADDR_LO_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000108)
#define HWIO_APCS_COMMON_RVBARADDR_LO_RMSK                                          0xffffffff
#define HWIO_APCS_COMMON_RVBARADDR_LO_IN          \
        in_dword_masked(HWIO_APCS_COMMON_RVBARADDR_LO_ADDR, HWIO_APCS_COMMON_RVBARADDR_LO_RMSK)
#define HWIO_APCS_COMMON_RVBARADDR_LO_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_RVBARADDR_LO_ADDR, m)
#define HWIO_APCS_COMMON_RVBARADDR_LO_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_RVBARADDR_LO_ADDR,v)
#define HWIO_APCS_COMMON_RVBARADDR_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_RVBARADDR_LO_ADDR,m,v,HWIO_APCS_COMMON_RVBARADDR_LO_IN)
#define HWIO_APCS_COMMON_RVBARADDR_LO_RVBARADDR_LO_REG_BMSK                         0xffffffff
#define HWIO_APCS_COMMON_RVBARADDR_LO_RVBARADDR_LO_REG_SHFT                                0x0

#define HWIO_APCS_COMMON_RVBARADDR_HI_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000010c)
#define HWIO_APCS_COMMON_RVBARADDR_HI_RMSK                                                0x3f
#define HWIO_APCS_COMMON_RVBARADDR_HI_IN          \
        in_dword_masked(HWIO_APCS_COMMON_RVBARADDR_HI_ADDR, HWIO_APCS_COMMON_RVBARADDR_HI_RMSK)
#define HWIO_APCS_COMMON_RVBARADDR_HI_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_RVBARADDR_HI_ADDR, m)
#define HWIO_APCS_COMMON_RVBARADDR_HI_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_RVBARADDR_HI_ADDR,v)
#define HWIO_APCS_COMMON_RVBARADDR_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_RVBARADDR_HI_ADDR,m,v,HWIO_APCS_COMMON_RVBARADDR_HI_IN)
#define HWIO_APCS_COMMON_RVBARADDR_HI_RVBARADDR_HI_REG_BMSK                               0x3f
#define HWIO_APCS_COMMON_RVBARADDR_HI_RVBARADDR_HI_REG_SHFT                                0x0

#define HWIO_APCS_COMMON_MPS_L2_MUX_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000200)
#define HWIO_APCS_COMMON_MPS_L2_MUX_RMSK                                                   0x3
#define HWIO_APCS_COMMON_MPS_L2_MUX_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MPS_L2_MUX_ADDR, HWIO_APCS_COMMON_MPS_L2_MUX_RMSK)
#define HWIO_APCS_COMMON_MPS_L2_MUX_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MPS_L2_MUX_ADDR, m)
#define HWIO_APCS_COMMON_MPS_L2_MUX_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MPS_L2_MUX_ADDR,v)
#define HWIO_APCS_COMMON_MPS_L2_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MPS_L2_MUX_ADDR,m,v,HWIO_APCS_COMMON_MPS_L2_MUX_IN)
#define HWIO_APCS_COMMON_MPS_L2_MUX_PERF_MPS_L2_CLKMUX_CONTROL_BMSK                        0x2
#define HWIO_APCS_COMMON_MPS_L2_MUX_PERF_MPS_L2_CLKMUX_CONTROL_SHFT                        0x1
#define HWIO_APCS_COMMON_MPS_L2_MUX_PWR_MPS_L2_CLKMUX_CONTROL_BMSK                         0x1
#define HWIO_APCS_COMMON_MPS_L2_MUX_PWR_MPS_L2_CLKMUX_CONTROL_SHFT                         0x0

#define HWIO_APCS_COMMON_CPR_SW_BYPASS_ADDR                                         (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000204)
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_RMSK                                                0xf
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPR_SW_BYPASS_ADDR, HWIO_APCS_COMMON_CPR_SW_BYPASS_RMSK)
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPR_SW_BYPASS_ADDR, m)
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CPR_SW_BYPASS_ADDR,v)
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CPR_SW_BYPASS_ADDR,m,v,HWIO_APCS_COMMON_CPR_SW_BYPASS_IN)
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_CPR_BYPASS_BMSK                                     0xf
#define HWIO_APCS_COMMON_CPR_SW_BYPASS_CPR_BYPASS_SHFT                                     0x0

#define HWIO_APCS_COMMON_SPARE_CFG3_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000208)
#define HWIO_APCS_COMMON_SPARE_CFG3_RMSK                                            0xffffffff
#define HWIO_APCS_COMMON_SPARE_CFG3_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SPARE_CFG3_ADDR, HWIO_APCS_COMMON_SPARE_CFG3_RMSK)
#define HWIO_APCS_COMMON_SPARE_CFG3_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SPARE_CFG3_ADDR, m)
#define HWIO_APCS_COMMON_SPARE_CFG3_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SPARE_CFG3_ADDR,v)
#define HWIO_APCS_COMMON_SPARE_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SPARE_CFG3_ADDR,m,v,HWIO_APCS_COMMON_SPARE_CFG3_IN)
#define HWIO_APCS_COMMON_SPARE_CFG3_COREARST_2_APCNSAW_ENABLE_BMSK                         0x1
#define HWIO_APCS_COMMON_SPARE_CFG3_COREARST_2_APCNSAW_ENABLE_SHFT                         0x0

#define HWIO_APCS_COMMON_CPU_PRESENT_ADDR                                           (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000020c)
#define HWIO_APCS_COMMON_CPU_PRESENT_RMSK                                                 0xff
#define HWIO_APCS_COMMON_CPU_PRESENT_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPU_PRESENT_ADDR, HWIO_APCS_COMMON_CPU_PRESENT_RMSK)
#define HWIO_APCS_COMMON_CPU_PRESENT_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPU_PRESENT_ADDR, m)
#define HWIO_APCS_COMMON_CPU_PRESENT_CORE_PRESENT_BMSK                                    0xff
#define HWIO_APCS_COMMON_CPU_PRESENT_CORE_PRESENT_SHFT                                     0x0

#define HWIO_APCS_COMMON_TCM_CTRL_ADDR                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000300)
#define HWIO_APCS_COMMON_TCM_CTRL_RMSK                                                  0x7fff
#define HWIO_APCS_COMMON_TCM_CTRL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_TCM_CTRL_ADDR, HWIO_APCS_COMMON_TCM_CTRL_RMSK)
#define HWIO_APCS_COMMON_TCM_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_TCM_CTRL_ADDR, m)
#define HWIO_APCS_COMMON_TCM_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_TCM_CTRL_ADDR,v)
#define HWIO_APCS_COMMON_TCM_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_TCM_CTRL_ADDR,m,v,HWIO_APCS_COMMON_TCM_CTRL_IN)
#define HWIO_APCS_COMMON_TCM_CTRL_TCM_REDIRECT_SW_CTRL_BMSK                             0x7fff
#define HWIO_APCS_COMMON_TCM_CTRL_TCM_REDIRECT_SW_CTRL_SHFT                                0x0

#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ADDR                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000400)
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_RMSK                                              0x1
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ADDR, HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_RMSK)
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ADDR, m)
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ADDR,v)
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ADDR,m,v,HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_IN)
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ACSNOOP_SW_CTRL_BMSK                              0x1
#define HWIO_APCS_COMMON_ACSNOOP_SW_CTRL_ACSNOOP_SW_CTRL_SHFT                              0x0

#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_ADDR                                    (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000128)
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_RMSK                                    0x800001ff
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_ADDR, HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_RMSK)
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_ADDR, m)
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_ADDR,v)
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_ADDR,m,v,HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_IN)
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_MUX_EN_BMSK                     0x80000000
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_MUX_EN_SHFT                           0x1f
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_LVL2_SEL_BMSK                        0x1f0
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_LVL2_SEL_SHFT                          0x4
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_LVL1_SEL_BMSK                          0xf
#define HWIO_APCS_COMMON_TESTBUS_MUX_CONFIG_TESTBUS_LVL1_SEL_SHFT                          0x0

#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000180)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_RMSK                                            0xff
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR, HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_RMSK)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR, m)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR,v)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR,m,v,HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_IN)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_SPM_WAKE_UP_WDOG_ENABLE_BMSK                    0xff
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_SPM_WAKE_UP_WDOG_ENABLE_SHFT                     0x0

#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001c8)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_RMSK                                             0x3
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR, HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_RMSK)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR, m)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR,v)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR,m,v,HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_IN)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_SLEEP_BMSK                         0x2
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_SLEEP_SHFT                         0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_WAKEUP_BMSK                        0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_WAKEUP_SHFT                        0x0

#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001cc)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_RMSK                                             0x3
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR, HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_RMSK)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR, m)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR,v)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR,m,v,HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_IN)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_SLEEP_BMSK                         0x2
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_SLEEP_SHFT                         0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_WAKEUP_BMSK                        0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_WAKEUP_SHFT                        0x0

#define HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR                                        (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001d0)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_RMSK                                               0x3
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR, HWIO_APCS_COMMON_CCISPM_TRIGGER_RMSK)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR, m)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR,v)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR,m,v,HWIO_APCS_COMMON_CCISPM_TRIGGER_IN)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_SLEEP_BMSK                                  0x2
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_SLEEP_SHFT                                  0x1
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_WAKEUP_BMSK                                 0x1
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_WAKEUP_SHFT                                 0x0

#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_ADDR                                (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000210)
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_RMSK                                      0x1f
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_ADDR, HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_RMSK)
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_ADDR, m)
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_ADDR,v)
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_ADDR,m,v,HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_IN)
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_L2_CGC_OVERRIDE_BMSK                      0x10
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_L2_CGC_OVERRIDE_SHFT                       0x4
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE3_L1_CGC_OVERRIDE_BMSK                 0x8
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE3_L1_CGC_OVERRIDE_SHFT                 0x3
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE2_L1_CGC_OVERRIDE_BMSK                 0x4
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE2_L1_CGC_OVERRIDE_SHFT                 0x2
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE1_L1_CGC_OVERRIDE_BMSK                 0x2
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE1_L1_CGC_OVERRIDE_SHFT                 0x1
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE0_L1_CGC_OVERRIDE_BMSK                 0x1
#define HWIO_APCS_COMMON_L1_L2_MEM_CGC_OVERRIDE_CORE0_L1_CGC_OVERRIDE_SHFT                 0x0

#define HWIO_APCS_COMMON_APC0_VS_CBCR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000214)
#define HWIO_APCS_COMMON_APC0_VS_CBCR_RMSK                                          0x80000003
#define HWIO_APCS_COMMON_APC0_VS_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VS_CBCR_ADDR, HWIO_APCS_COMMON_APC0_VS_CBCR_RMSK)
#define HWIO_APCS_COMMON_APC0_VS_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VS_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VS_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VS_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VS_CBCR_ADDR,m,v,HWIO_APCS_COMMON_APC0_VS_CBCR_IN)
#define HWIO_APCS_COMMON_APC0_VS_CBCR_CLK_OFF_BMSK                                  0x80000000
#define HWIO_APCS_COMMON_APC0_VS_CBCR_CLK_OFF_SHFT                                        0x1f
#define HWIO_APCS_COMMON_APC0_VS_CBCR_HW_CTL_BMSK                                          0x2
#define HWIO_APCS_COMMON_APC0_VS_CBCR_HW_CTL_SHFT                                          0x1
#define HWIO_APCS_COMMON_APC0_VS_CBCR_SW_CTL_BMSK                                          0x1
#define HWIO_APCS_COMMON_APC0_VS_CBCR_SW_CTL_SHFT                                          0x0

#define HWIO_APCS_COMMON_APC1_VS_CBCR_ADDR                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000218)
#define HWIO_APCS_COMMON_APC1_VS_CBCR_RMSK                                          0x80000003
#define HWIO_APCS_COMMON_APC1_VS_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VS_CBCR_ADDR, HWIO_APCS_COMMON_APC1_VS_CBCR_RMSK)
#define HWIO_APCS_COMMON_APC1_VS_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VS_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VS_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VS_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VS_CBCR_ADDR,m,v,HWIO_APCS_COMMON_APC1_VS_CBCR_IN)
#define HWIO_APCS_COMMON_APC1_VS_CBCR_CLK_OFF_BMSK                                  0x80000000
#define HWIO_APCS_COMMON_APC1_VS_CBCR_CLK_OFF_SHFT                                        0x1f
#define HWIO_APCS_COMMON_APC1_VS_CBCR_HW_CTL_BMSK                                          0x2
#define HWIO_APCS_COMMON_APC1_VS_CBCR_HW_CTL_SHFT                                          0x1
#define HWIO_APCS_COMMON_APC1_VS_CBCR_SW_CTL_BMSK                                          0x1
#define HWIO_APCS_COMMON_APC1_VS_CBCR_SW_CTL_SHFT                                          0x0

#define HWIO_APCS_COMMON_QSB_CBCR_ADDR                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000021c)
#define HWIO_APCS_COMMON_QSB_CBCR_RMSK                                              0x80000003
#define HWIO_APCS_COMMON_QSB_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_QSB_CBCR_ADDR, HWIO_APCS_COMMON_QSB_CBCR_RMSK)
#define HWIO_APCS_COMMON_QSB_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_QSB_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_QSB_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_QSB_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_QSB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_QSB_CBCR_ADDR,m,v,HWIO_APCS_COMMON_QSB_CBCR_IN)
#define HWIO_APCS_COMMON_QSB_CBCR_CLK_OFF_BMSK                                      0x80000000
#define HWIO_APCS_COMMON_QSB_CBCR_CLK_OFF_SHFT                                            0x1f
#define HWIO_APCS_COMMON_QSB_CBCR_HW_CTL_BMSK                                              0x2
#define HWIO_APCS_COMMON_QSB_CBCR_HW_CTL_SHFT                                              0x1
#define HWIO_APCS_COMMON_QSB_CBCR_SW_CTL_BMSK                                              0x1
#define HWIO_APCS_COMMON_QSB_CBCR_SW_CTL_SHFT                                              0x0

#define HWIO_APCS_COMMON_APB_CBCR_ADDR                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000220)
#define HWIO_APCS_COMMON_APB_CBCR_RMSK                                              0x80000003
#define HWIO_APCS_COMMON_APB_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APB_CBCR_ADDR, HWIO_APCS_COMMON_APB_CBCR_RMSK)
#define HWIO_APCS_COMMON_APB_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APB_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_APB_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APB_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_APB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APB_CBCR_ADDR,m,v,HWIO_APCS_COMMON_APB_CBCR_IN)
#define HWIO_APCS_COMMON_APB_CBCR_CLK_OFF_BMSK                                      0x80000000
#define HWIO_APCS_COMMON_APB_CBCR_CLK_OFF_SHFT                                            0x1f
#define HWIO_APCS_COMMON_APB_CBCR_HW_CTL_BMSK                                              0x2
#define HWIO_APCS_COMMON_APB_CBCR_HW_CTL_SHFT                                              0x1
#define HWIO_APCS_COMMON_APB_CBCR_SW_CTL_BMSK                                              0x1
#define HWIO_APCS_COMMON_APB_CBCR_SW_CTL_SHFT                                              0x0

#define HWIO_APCS_COMMON_CS_CBCR_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000224)
#define HWIO_APCS_COMMON_CS_CBCR_RMSK                                               0x80000003
#define HWIO_APCS_COMMON_CS_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CS_CBCR_ADDR, HWIO_APCS_COMMON_CS_CBCR_RMSK)
#define HWIO_APCS_COMMON_CS_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CS_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_CS_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CS_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_CS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CS_CBCR_ADDR,m,v,HWIO_APCS_COMMON_CS_CBCR_IN)
#define HWIO_APCS_COMMON_CS_CBCR_CLK_OFF_BMSK                                       0x80000000
#define HWIO_APCS_COMMON_CS_CBCR_CLK_OFF_SHFT                                             0x1f
#define HWIO_APCS_COMMON_CS_CBCR_HW_CTL_BMSK                                               0x2
#define HWIO_APCS_COMMON_CS_CBCR_HW_CTL_SHFT                                               0x1
#define HWIO_APCS_COMMON_CS_CBCR_SW_CTL_BMSK                                               0x1
#define HWIO_APCS_COMMON_CS_CBCR_SW_CTL_SHFT                                               0x0


#endif /* __APCS_HWIO_H__ */
