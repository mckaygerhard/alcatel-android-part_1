#ifndef __TCSR_HWIO_H__
#define __TCSR_HWIO_H__
/*
===========================================================================
*/
/**
  @file tcsr_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    TCSR_TCSR_REGS

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8994/tcsr_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_REGS
 *--------------------------------------------------------------------------*/

#define TCSR_TCSR_REGS_REG_BASE                                                                                                                               (CORE_TOP_CSR_BASE      + 0x00020000)

#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00001300)
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_IN          \
        in_dword_masked(HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_ADDR, HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_RMSK)
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_ADDR, m)
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_ADDR,v)
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_ADDR,m,v,HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_IN)
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_SPARE_1_BMSK                                                                                                          0xff000000
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_SPARE_1_SHFT                                                                                                                0x18
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_QRIB_APCCNONSECVMIDINIT_BMSK                                                                                            0xff0000
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_QRIB_APCCNONSECVMIDINIT_SHFT                                                                                                0x10
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_SPARE_0_BMSK                                                                                                              0xff00
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_SPARE_0_SHFT                                                                                                                 0x8
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_QRIB_APCCSECVMIDINIT_BMSK                                                                                                   0xff
#define HWIO_TCSR_KPSS_QRIB_VMIDMT_INIT_QRIB_APCCSECVMIDINIT_SHFT                                                                                                    0x0

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000100)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000104)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000108)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_BIMC_QXS1_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_BIMC_QXS1_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_BIMC_QXS1_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_INIT_BIMC_QXS1_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000010c)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_BIMC_QRIB_APP_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_BIMC_QRIB_APP_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_BIMC_QRIB_APP_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_INIT_BIMC_QRIB_APP_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000180)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_LPASS_LPM_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_LPASS_LPM_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_LPASS_LPM_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_INIT_LPASS_LPM_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00000184)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_RMSK                                                                                                              0x10001
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_LPASS_LPC_CSR_QRIB_XPU2_NSEN_INIT_BMSK                                                                            0x10000
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_LPASS_LPC_CSR_QRIB_XPU2_NSEN_INIT_SHFT                                                                               0x10
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_LPASS_LPC_CSR_QRIB_XPU2_EN_TZ_BMSK                                                                                    0x1
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_INIT_LPASS_LPC_CSR_QRIB_XPU2_EN_TZ_SHFT                                                                                    0x0

#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00000188)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_RMSK                                                                                                                0x10001
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_LPASS_LPAIF_QRIB_XPU2_NSEN_INIT_BMSK                                                                                0x10000
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_LPASS_LPAIF_QRIB_XPU2_NSEN_INIT_SHFT                                                                                   0x10
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_LPASS_LPAIF_QRIB_XPU2_EN_TZ_BMSK                                                                                        0x1
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_INIT_LPASS_LPAIF_QRIB_XPU2_EN_TZ_SHFT                                                                                        0x0

#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000018c)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_LPASS_RSMP_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_LPASS_RSMP_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_LPASS_RSMP_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_INIT_LPASS_RSMP_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00000190)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_RMSK                                                                                                              0x10001
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_LPASS_LPC_COM_QRIB_XPU2_NSEN_INIT_BMSK                                                                            0x10000
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_LPASS_LPC_COM_QRIB_XPU2_NSEN_INIT_SHFT                                                                               0x10
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_LPASS_LPC_COM_QRIB_XPU2_EN_TZ_BMSK                                                                                    0x1
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_INIT_LPASS_LPC_COM_QRIB_XPU2_EN_TZ_SHFT                                                                                    0x0

#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000194)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_LPASS_SB_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_LPASS_SB_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_LPASS_SB_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_INIT_LPASS_SB_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000198)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_LPASS_AXIM_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_LPASS_AXIM_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_LPASS_AXIM_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_INIT_LPASS_AXIM_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000019c)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_LPASS_CORE_AXIM_QRIB_XPU2_NSEN_INIT_BMSK                                                                        0x10000
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_LPASS_CORE_AXIM_QRIB_XPU2_NSEN_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_LPASS_CORE_AXIM_QRIB_XPU2_EN_TZ_BMSK                                                                                0x1
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_INIT_LPASS_CORE_AXIM_QRIB_XPU2_EN_TZ_SHFT                                                                                0x0

#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000200)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RBCPR_QDSS_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RBCPR_QDSS_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RBCPR_QDSS_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_INIT_RBCPR_QDSS_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00000380)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_NSEN_INIT_BMSK                                                                      0x10000
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_NSEN_INIT_SHFT                                                                         0x10
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_EN_TZ_BMSK                                                                              0x1
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_EN_TZ_SHFT                                                                              0x0

#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00000384)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_RMSK                                                                                                        0x10001
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_NSEN_INIT_BMSK                                                                0x10000
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_NSEN_INIT_SHFT                                                                   0x10
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_EN_TZ_BMSK                                                                        0x1
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_EN_TZ_SHFT                                                                        0x0

#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000038c)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_MMSS_VFE_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_MMSS_VFE_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_MMSS_VFE_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_INIT_MMSS_VFE_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000039c)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_MMSS_CPP_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_MMSS_CPP_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_MMSS_CPP_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_INIT_MMSS_CPP_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000003a4)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_RMSK                                                                                                                    0x10001
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_MMSS_FD_QRIB_XPU2_NSEN_INIT_BMSK                                                                                        0x10000
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_MMSS_FD_QRIB_XPU2_NSEN_INIT_SHFT                                                                                           0x10
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_MMSS_FD_QRIB_XPU2_EN_TZ_BMSK                                                                                                0x1
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_INIT_MMSS_FD_QRIB_XPU2_EN_TZ_SHFT                                                                                                0x0

#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000082c)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_RMSK                                                                                                               0x10001
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_MMSS_FD_VBIF_QRIB_XPU2_NSEN_INIT_BMSK                                                                              0x10000
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_MMSS_FD_VBIF_QRIB_XPU2_NSEN_INIT_SHFT                                                                                 0x10
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_MMSS_FD_VBIF_QRIB_XPU2_EN_TZ_BMSK                                                                                      0x1
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_INIT_MMSS_FD_VBIF_QRIB_XPU2_EN_TZ_SHFT                                                                                      0x0

#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000003a0)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_CAMSS_TOP_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_CAMSS_TOP_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_CAMSS_TOP_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_INIT_CAMSS_TOP_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000390)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_MMSS_JPEG_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_MMSS_JPEG_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_MMSS_JPEG_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_INIT_MMSS_JPEG_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000394)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_MMSS_MDP_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_MMSS_MDP_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_MMSS_MDP_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_INIT_MMSS_MDP_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000003ac)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_ADDR, HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_RMSK)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_ADDR,m,v,HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_IN)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_COPSS_QRIB_APU_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_COPSS_QRIB_APU_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_COPSS_QRIB_APU_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_INIT_COPSS_QRIB_APU_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000400)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_MMSS_VENUS0_QRIB_MMU_NS_BMSK                                                                                               0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_MMSS_VENUS0_QRIB_MMU_NS_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00000404)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_RMSK                                                                                                                    0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_MMSS_VENUS0_QRIB_MMU_NS_EN_BMSK                                                                                         0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_NS_EN_MMSS_VENUS0_QRIB_MMU_NS_EN_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00000410)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_RMSK                                                                                                                          0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_MMSS_VFE_QRIB_MMU_NS_BMSK                                                                                                     0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_MMSS_VFE_QRIB_MMU_NS_SHFT                                                                                                     0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000414)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_MMSS_VFE_QRIB_MMU_NS_EN_BMSK                                                                                               0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_NS_EN_MMSS_VFE_QRIB_MMU_NS_EN_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00000550)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_RMSK                                                                                                                          0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_MMSS_CPP_QRIB_MMU_NS_BMSK                                                                                                     0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_MMSS_CPP_QRIB_MMU_NS_SHFT                                                                                                     0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000554)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_MMSS_CPP_QRIB_MMU_NS_EN_BMSK                                                                                               0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_NS_EN_MMSS_CPP_QRIB_MMU_NS_EN_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00000558)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_RMSK                                                                                                                           0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_MMSS_FD_QRIB_MMU_NS_BMSK                                                                                                       0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_MMSS_FD_QRIB_MMU_NS_SHFT                                                                                                       0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000055c)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_RMSK                                                                                                                        0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_MMSS_FD_QRIB_MMU_NS_EN_BMSK                                                                                                 0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_NS_EN_MMSS_FD_QRIB_MMU_NS_EN_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00000418)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_RMSK                                                                                                                         0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_MMSS_JPEG_QRIB_MMU_NS_BMSK                                                                                                   0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_MMSS_JPEG_QRIB_MMU_NS_SHFT                                                                                                   0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000041c)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_MMSS_JPEG_QRIB_MMU_NS_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_NS_EN_MMSS_JPEG_QRIB_MMU_NS_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00000420)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_RMSK                                                                                                                          0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_MMSS_MDP_QRIB_MMU_NS_BMSK                                                                                                     0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_MMSS_MDP_QRIB_MMU_NS_SHFT                                                                                                     0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000424)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_MMSS_MDP_QRIB_MMU_NS_EN_BMSK                                                                                               0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_NS_EN_MMSS_MDP_QRIB_MMU_NS_EN_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00000428)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_RMSK                                                                                                                        0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_MMSS_OXILI_QRIB_MMU_NS_BMSK                                                                                                 0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_MMSS_OXILI_QRIB_MMU_NS_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000042c)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_MMSS_OXILI_QRIB_MMU_NS_EN_BMSK                                                                                           0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_NS_EN_MMSS_OXILI_QRIB_MMU_NS_EN_SHFT                                                                                           0x0

#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00000500)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_RMSK                                                                                                               0x10001
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_MMSS_OXILICX_QRIB_XPU2_NSEN_INIT_BMSK                                                                              0x10000
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_MMSS_OXILICX_QRIB_XPU2_NSEN_INIT_SHFT                                                                                 0x10
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_MMSS_OXILICX_QRIB_XPU2_EN_TZ_BMSK                                                                                      0x1
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_INIT_MMSS_OXILICX_QRIB_XPU2_EN_TZ_SHFT                                                                                      0x0

#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00000504)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_NSEN_INIT_BMSK                                                                        0x10000
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_NSEN_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_EN_TZ_BMSK                                                                                0x1
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_EN_TZ_SHFT                                                                                0x0

#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00000600)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_RMSK                                                                                                              0x10001
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_MMSS_OCMEM_DM_QRIB_XPU2_NSEN_INIT_BMSK                                                                            0x10000
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_MMSS_OCMEM_DM_QRIB_XPU2_NSEN_INIT_SHFT                                                                               0x10
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_MMSS_OCMEM_DM_QRIB_XPU2_EN_TZ_BMSK                                                                                    0x1
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_INIT_MMSS_OCMEM_DM_QRIB_XPU2_EN_TZ_SHFT                                                                                    0x0

#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000604)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_MMSS_OCMEM_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_MMSS_OCMEM_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_MMSS_OCMEM_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_INIT_MMSS_OCMEM_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00000608)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_NSEN_INIT_BMSK                                                                        0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_NSEN_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_EN_TZ_BMSK                                                                                0x1
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_EN_TZ_SHFT                                                                                0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000060c)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_NSEN_INIT_BMSK                                                                        0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_NSEN_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_EN_TZ_BMSK                                                                                0x1
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_EN_TZ_SHFT                                                                                0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00000280)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_NSEN_INIT_BMSK                                                                      0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_NSEN_INIT_SHFT                                                                         0x10
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_EN_TZ_BMSK                                                                              0x1
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_EN_TZ_SHFT                                                                              0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00000300)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_NSEN_INIT_BMSK                                                                    0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_NSEN_INIT_SHFT                                                                       0x10
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_EN_TZ_BMSK                                                                            0x1
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_EN_TZ_SHFT                                                                            0x0

#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000620)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_PCIE_PARF0_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_PCIE_PARF0_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_PCIE_PARF0_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_INIT_PCIE_PARF0_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000624)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_PCIE_PARF1_QRIB_XPU2_NSEN_INIT_BMSK                                                                                  0x10000
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_PCIE_PARF1_QRIB_XPU2_NSEN_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_PCIE_PARF1_QRIB_XPU2_EN_TZ_BMSK                                                                                          0x1
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_INIT_PCIE_PARF1_QRIB_XPU2_EN_TZ_SHFT                                                                                          0x0

#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000628)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_PCIE_SLV0_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_PCIE_SLV0_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_PCIE_SLV0_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_INIT_PCIE_SLV0_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000062c)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_PCIE_SLV1_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_PCIE_SLV1_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_PCIE_SLV1_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_INIT_PCIE_SLV1_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000630)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_RMSK                                                                                                                   0x10001
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_USB_PRIM_QRIB_XPU2_NSEN_INIT_BMSK                                                                                      0x10000
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_USB_PRIM_QRIB_XPU2_NSEN_INIT_SHFT                                                                                         0x10
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_USB_PRIM_QRIB_XPU2_EN_TZ_BMSK                                                                                              0x1
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_INIT_USB_PRIM_QRIB_XPU2_EN_TZ_SHFT                                                                                              0x0

#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00000800)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK                                                                                                                          0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, m)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,v)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,m,v,HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_BMSK                                                                                                     0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_SHFT                                                                                                     0x0

#define HWIO_TCSR_XPU_NSEN_STATUS_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00000820)
#define HWIO_TCSR_XPU_NSEN_STATUS_RMSK                                                                                                                               0x3
#define HWIO_TCSR_XPU_NSEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, HWIO_TCSR_XPU_NSEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_NSEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_BMSK                                                                                                         0x2
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_SHFT                                                                                                         0x1
#define HWIO_TCSR_XPU_NSEN_STATUS_MUTEX_XPU2_NSEN_STATUS_BMSK                                                                                                        0x1
#define HWIO_TCSR_XPU_NSEN_STATUS_MUTEX_XPU2_NSEN_STATUS_SHFT                                                                                                        0x0

#define HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00000824)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK                                                                                                                             0x3
#define HWIO_TCSR_XPU_VMIDEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_BMSK                                                                                                     0x2
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_SHFT                                                                                                     0x1
#define HWIO_TCSR_XPU_VMIDEN_STATUS_MUTEX_XPU2_VMIDEN_STATUS_BMSK                                                                                                    0x1
#define HWIO_TCSR_XPU_VMIDEN_STATUS_MUTEX_XPU2_VMIDEN_STATUS_SHFT                                                                                                    0x0

#define HWIO_TCSR_XPU_MSAEN_STATUS_ADDR                                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00000828)
#define HWIO_TCSR_XPU_MSAEN_STATUS_RMSK                                                                                                                              0x3
#define HWIO_TCSR_XPU_MSAEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, HWIO_TCSR_XPU_MSAEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_MSAEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_BMSK                                                                                                       0x2
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_SHFT                                                                                                       0x1
#define HWIO_TCSR_XPU_MSAEN_STATUS_MUTEX_XPU2_MSAEN_STATUS_BMSK                                                                                                      0x1
#define HWIO_TCSR_XPU_MSAEN_STATUS_MUTEX_XPU2_MSAEN_STATUS_SHFT                                                                                                      0x0

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00001000)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_BIMC_CFG_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_BIMC_CFG_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00001004)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001008)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_BIMC_QXS0_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_BIMC_QXS0_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000100c)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001010)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_BIMC_QXS1_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_ACR_BIMC_QXS1_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001014)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS1_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00001018)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_BIMC_QRIB_APP_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_ACR_BIMC_QRIB_APP_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000101c)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_BIMC_QRIB_APP_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001180)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_LPASS_LPM_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_ACR_LPASS_LPM_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001184)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00001188)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_LPASS_LPC_CSR_QRIB_XPU2_ACR_BMSK                                                                                0xffffffff
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_ACR_LPASS_LPC_CSR_QRIB_XPU2_ACR_SHFT                                                                                       0x0

#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000118c)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                       0x10001
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                   0x10000
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                      0x10
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                 0x1
#define HWIO_TCSR_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_CSR_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                 0x0

#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001190)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_LPASS_LPAIF_QRIB_XPU2_ACR_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_ACR_LPASS_LPAIF_QRIB_XPU2_ACR_SHFT                                                                                           0x0

#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00001194)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                         0x10001
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                       0x10000
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                          0x10
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                     0x1
#define HWIO_TCSR_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_LPASS_LPAIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                     0x0

#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001198)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_LPASS_RSMP_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_ACR_LPASS_RSMP_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000119c)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_LPASS_RSMP_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00001280)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_LPASS_LPC_COM_QRIB_XPU2_ACR_BMSK                                                                                0xffffffff
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_ACR_LPASS_LPC_COM_QRIB_XPU2_ACR_SHFT                                                                                       0x0

#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000011a4)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                       0x10001
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                   0x10000
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                      0x10
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                 0x1
#define HWIO_TCSR_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_LPASS_LPC_COM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                 0x0

#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000011b8)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_LPASS_SB_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_ACR_LPASS_SB_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000011bc)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_LPASS_SB_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000011c0)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                       0x1f001f
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB_QRIB_VMIDMT_ACR_INIT_BMSK                                                                         0x1f0000
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB_QRIB_VMIDMT_ACR_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB_QRIB_VMIDMT_VMID_INIT_BMSK                                                                            0x1f
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB_QRIB_VMIDMT_VMID_INIT_SHFT                                                                             0x0

#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000011c4)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_LPASS_SB_QRIB_VMIDMT_EN_BMSK                                                                                               0x1
#define HWIO_TCSR_LPASS_SB_QRIB_VMIDMT_EN_LPASS_SB_QRIB_VMIDMT_EN_SHFT                                                                                               0x0

#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000011c8)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                      0x1f001f
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_AIF_QRIB_VMIDMT_ACR_INIT_BMSK                                                                       0x1f0000
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_AIF_QRIB_VMIDMT_ACR_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_AIF_QRIB_VMIDMT_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_AIF_QRIB_VMIDMT_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000011cc)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_LPASS_AIF_QRIB_VMIDMT_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_LPASS_AIF_QRIB_VMIDMT_EN_LPASS_AIF_QRIB_VMIDMT_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000011d0)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                      0x1f001f
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_DML_QRIB_VMIDMT_ACR_INIT_BMSK                                                                       0x1f0000
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_DML_QRIB_VMIDMT_ACR_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_DML_QRIB_VMIDMT_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_DML_QRIB_VMIDMT_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000011d4)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_LPASS_DML_QRIB_VMIDMT_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_LPASS_DML_QRIB_VMIDMT_EN_LPASS_DML_QRIB_VMIDMT_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x000011d8)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                     0x1f001f
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_MIDI_QRIB_VMIDMT_ACR_INIT_BMSK                                                                     0x1f0000
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_MIDI_QRIB_VMIDMT_ACR_INIT_SHFT                                                                         0x10
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_MIDI_QRIB_VMIDMT_VMID_INIT_BMSK                                                                        0x1f
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_MIDI_QRIB_VMIDMT_VMID_INIT_SHFT                                                                         0x0

#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x000011dc)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_LPASS_MIDI_QRIB_VMIDMT_EN_BMSK                                                                                           0x1
#define HWIO_TCSR_LPASS_MIDI_QRIB_VMIDMT_EN_LPASS_MIDI_QRIB_VMIDMT_EN_SHFT                                                                                           0x0

#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000011e0)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                   0x1f001f
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_RSMPLR_QRIB_VMIDMT_ACR_INIT_BMSK                                                                 0x1f0000
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_RSMPLR_QRIB_VMIDMT_ACR_INIT_SHFT                                                                     0x10
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_RSMPLR_QRIB_VMIDMT_VMID_INIT_BMSK                                                                    0x1f
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_RSMPLR_QRIB_VMIDMT_VMID_INIT_SHFT                                                                     0x0

#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000011e4)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_RMSK                                                                                                                   0x1
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_LPASS_RSMPLR_QRIB_VMIDMT_EN_BMSK                                                                                       0x1
#define HWIO_TCSR_LPASS_RSMPLR_QRIB_VMIDMT_EN_LPASS_RSMPLR_QRIB_VMIDMT_EN_SHFT                                                                                       0x0

#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000011e8)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                   0x1f001f
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_HDMIRX_QRIB_VMIDMT_ACR_INIT_BMSK                                                                 0x1f0000
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_HDMIRX_QRIB_VMIDMT_ACR_INIT_SHFT                                                                     0x10
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_HDMIRX_QRIB_VMIDMT_VMID_INIT_BMSK                                                                    0x1f
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_HDMIRX_QRIB_VMIDMT_VMID_INIT_SHFT                                                                     0x0

#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000011ec)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_RMSK                                                                                                                   0x1
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_LPASS_HDMIRX_QRIB_VMIDMT_EN_BMSK                                                                                       0x1
#define HWIO_TCSR_LPASS_HDMIRX_QRIB_VMIDMT_EN_LPASS_HDMIRX_QRIB_VMIDMT_EN_SHFT                                                                                       0x0

#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000011f0)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                  0x1f001f
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SPDIFTX_QRIB_VMIDMT_ACR_INIT_BMSK                                                               0x1f0000
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SPDIFTX_QRIB_VMIDMT_ACR_INIT_SHFT                                                                   0x10
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SPDIFTX_QRIB_VMIDMT_VMID_INIT_BMSK                                                                  0x1f
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SPDIFTX_QRIB_VMIDMT_VMID_INIT_SHFT                                                                   0x0

#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x000011f4)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_RMSK                                                                                                                  0x1
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_LPASS_SPDIFTX_QRIB_VMIDMT_EN_BMSK                                                                                     0x1
#define HWIO_TCSR_LPASS_SPDIFTX_QRIB_VMIDMT_EN_LPASS_SPDIFTX_QRIB_VMIDMT_EN_SHFT                                                                                     0x0

#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001200)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_RBCPR_QDSS_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_ACR_RBCPR_QDSS_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00001204)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_RBCPR_QDSS_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00001380)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_BMSK                                                                          0xffffffff
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_MMSS_VENUS0_VBIF_QRIB_XPU2_ACR_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00001384)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                    0x10001
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_BMSK                                                             0x10000
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                0x10
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                           0x1
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                           0x0

#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001388)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                0x1f001f
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_CPU_QRIB_VMIDMT_ACR_INIT_BMSK                                                           0x1f0000
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_CPU_QRIB_VMIDMT_ACR_INIT_SHFT                                                               0x10
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_CPU_QRIB_VMIDMT_VMID_INIT_BMSK                                                              0x1f
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_CPU_QRIB_VMIDMT_VMID_INIT_SHFT                                                               0x0

#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000138c)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_RMSK                                                                                                                0x1
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_EN_BMSK                                                                            0x1
#define HWIO_TCSR_MMSS_VENUS0_CPU_QRIB_VMIDMT_EN_MMSS_VENUS0_CPU_QRIB_VMIDMT_INIT_EN_SHFT                                                                            0x0

#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00001390)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_RMSK                                                                                                      0xffffffff
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_BMSK                                                                    0xffffffff
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_ACR_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001394)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                 0x10001
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_BMSK                                                       0x10000
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_SHFT                                                          0x10
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                     0x1
#define HWIO_TCSR_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_MMSS_VENUS0_WRAPPER_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                     0x0

#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00001398)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                               0x1f001f
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_VBIF_QRIB_VMIDMT_ACR_INIT_BMSK                                                         0x1f0000
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_VBIF_QRIB_VMIDMT_ACR_INIT_SHFT                                                             0x10
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_VBIF_QRIB_VMIDMT_VMID_INIT_BMSK                                                            0x1f
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VENUS0_VBIF_QRIB_VMIDMT_VMID_INIT_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000139c)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_RMSK                                                                                                               0x1
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_EN_BMSK                                                                          0x1
#define HWIO_TCSR_MMSS_VENUS0_VBIF_QRIB_VMIDMT_EN_MMSS_VENUS0_VBIF_QRIB_VMIDMT_INIT_EN_SHFT                                                                          0x0

#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000013b0)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_MMSS_VFE_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_ACR_MMSS_VFE_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000013b4)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_MMSS_VFE_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000013b8)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                       0x1f001f
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VFE_QRIB_VMIDMT_ACR_INIT_BMSK                                                                         0x1f0000
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VFE_QRIB_VMIDMT_ACR_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VFE_QRIB_VMIDMT_VMID_INIT_BMSK                                                                            0x1f
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_VFE_QRIB_VMIDMT_VMID_INIT_SHFT                                                                             0x0

#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000013bc)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_MMSS_VFE_QRIB_VMIDMT_INIT_EN_BMSK                                                                                          0x1
#define HWIO_TCSR_MMSS_VFE_QRIB_VMIDMT_EN_MMSS_VFE_QRIB_VMIDMT_INIT_EN_SHFT                                                                                          0x0

#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000017e0)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_MMSS_CPP_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_ACR_MMSS_CPP_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000017e4)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_MMSS_CPP_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000017e8)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                       0x1f001f
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_CPP_QRIB_VMIDMT_ACR_INIT_BMSK                                                                         0x1f0000
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_CPP_QRIB_VMIDMT_ACR_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_CPP_QRIB_VMIDMT_VMID_INIT_BMSK                                                                            0x1f
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_CPP_QRIB_VMIDMT_VMID_INIT_SHFT                                                                             0x0

#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000017ec)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_MMSS_CPP_QRIB_VMIDMT_INIT_EN_BMSK                                                                                          0x1
#define HWIO_TCSR_MMSS_CPP_QRIB_VMIDMT_EN_MMSS_CPP_QRIB_VMIDMT_INIT_EN_SHFT                                                                                          0x0

#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00001800)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_MMSS_FD_QRIB_XPU2_ACR_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_ACR_MMSS_FD_QRIB_XPU2_ACR_SHFT                                                                                                   0x0

#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001804)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                             0x10001
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                               0x10000
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                  0x10
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                             0x1
#define HWIO_TCSR_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                             0x0

#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00001808)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_RMSK                                                                                                             0xffffffff
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_MMSS_FD_VBIF_QRIB_XPU2_ACR_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_ACR_MMSS_FD_VBIF_QRIB_XPU2_ACR_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000180c)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                        0x10001
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                     0x10000
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                        0x10
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                   0x1
#define HWIO_TCSR_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_FD_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                   0x0

#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000017f0)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_CAMSS_TOP_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_ACR_CAMSS_TOP_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000017f4)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_CAMSS_TOP_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000017f8)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                      0x1f001f
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_CAMSS_TOP_QRIB_VMIDMT_ACR_INIT_BMSK                                                                       0x1f0000
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_CAMSS_TOP_QRIB_VMIDMT_ACR_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_CAMSS_TOP_QRIB_VMIDMT_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_INIT_ACR_INIT_CAMSS_TOP_QRIB_VMIDMT_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000017fc)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_CAMSS_TOP_QRIB_VMIDMT_INIT_EN_BMSK                                                                                        0x1
#define HWIO_TCSR_CAMSS_TOP_QRIB_VMIDMT_EN_CAMSS_TOP_QRIB_VMIDMT_INIT_EN_SHFT                                                                                        0x0

#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000013c0)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_MMSS_JPEG_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_ACR_MMSS_JPEG_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000013c4)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_MMSS_JPEG_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000013c8)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                      0x1f001f
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_JPEG_QRIB_VMIDMT_ACR_INIT_BMSK                                                                       0x1f0000
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_JPEG_QRIB_VMIDMT_ACR_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_JPEG_QRIB_VMIDMT_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_JPEG_QRIB_VMIDMT_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000013cc)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_MMSS_JPEG_QRIB_VMIDMT_INIT_EN_BMSK                                                                                        0x1
#define HWIO_TCSR_MMSS_JPEG_QRIB_VMIDMT_EN_MMSS_JPEG_QRIB_VMIDMT_INIT_EN_SHFT                                                                                        0x0

#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000013d0)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_MMSS_MDP_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_ACR_MMSS_MDP_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000013d4)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_MMSS_MDP_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000013d8)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                       0x1f001f
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_MDP_QRIB_VMIDMT_ACR_INIT_BMSK                                                                         0x1f0000
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_MDP_QRIB_VMIDMT_ACR_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_MDP_QRIB_VMIDMT_VMID_INIT_BMSK                                                                            0x1f
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_MDP_QRIB_VMIDMT_VMID_INIT_SHFT                                                                             0x0

#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000013dc)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_MMSS_MDP_QRIB_VMIDMT_INIT_EN_BMSK                                                                                          0x1
#define HWIO_TCSR_MMSS_MDP_QRIB_VMIDMT_EN_MMSS_MDP_QRIB_VMIDMT_INIT_EN_SHFT                                                                                          0x0

#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000013f4)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_COPSS_QRIB_APU_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00001480)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                             0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_MMSS_VENUS0_QRIB_MMU_VMID_INIT_EN_BMSK                                                                           0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_EN_MMSS_VENUS0_QRIB_MMU_VMID_INIT_EN_SHFT                                                                           0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00001484)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_RMSK                                                                                                               0x1f
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_MMSS_VENUS0_QRIB_MMU_VMID_INIT_BMSK                                                                                0x1f
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INIT_MMSS_VENUS0_QRIB_MMU_VMID_INIT_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00001488)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_RMSK                                                                                                                  0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_MMSS_VENUS0_QRIB_MMU_VMID_EN_BMSK                                                                                     0x1
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_EN_MMSS_VENUS0_QRIB_MMU_VMID_EN_SHFT                                                                                     0x0

#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000148c)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_RMSK                                                                                                                    0x1f
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_MMSS_VENUS0_QRIB_MMU_VMID_BMSK                                                                                          0x1f
#define HWIO_TCSR_MMSS_VENUS0_MMU_QRIB_VMID_MMSS_VENUS0_QRIB_MMU_VMID_SHFT                                                                                           0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000014a0)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                                0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_MMSS_VFE_QRIB_MMU_VMID_INIT_EN_BMSK                                                                                 0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_EN_MMSS_VFE_QRIB_MMU_VMID_INIT_EN_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000014a4)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_RMSK                                                                                                                  0x1f
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_MMSS_VFE_QRIB_MMU_VMID_INIT_BMSK                                                                                      0x1f
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INIT_MMSS_VFE_QRIB_MMU_VMID_INIT_SHFT                                                                                       0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x000014a8)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_MMSS_VFE_QRIB_MMU_VMID_EN_BMSK                                                                                           0x1
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_EN_MMSS_VFE_QRIB_MMU_VMID_EN_SHFT                                                                                           0x0

#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000014ac)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_RMSK                                                                                                                       0x1f
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_MMSS_VFE_QRIB_MMU_VMID_BMSK                                                                                                0x1f
#define HWIO_TCSR_MMSS_VFE_MMU_QRIB_VMID_MMSS_VFE_QRIB_MMU_VMID_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000014e0)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                                0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_MMSS_CPP_QRIB_MMU_VMID_INIT_EN_BMSK                                                                                 0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_EN_MMSS_CPP_QRIB_MMU_VMID_INIT_EN_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000014e4)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_RMSK                                                                                                                  0x1f
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_MMSS_CPP_QRIB_MMU_VMID_INIT_BMSK                                                                                      0x1f
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INIT_MMSS_CPP_QRIB_MMU_VMID_INIT_SHFT                                                                                       0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x000014e8)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_MMSS_CPP_QRIB_MMU_VMID_EN_BMSK                                                                                           0x1
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_EN_MMSS_CPP_QRIB_MMU_VMID_EN_SHFT                                                                                           0x0

#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000014ec)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_RMSK                                                                                                                       0x1f
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_MMSS_CPP_QRIB_MMU_VMID_BMSK                                                                                                0x1f
#define HWIO_TCSR_MMSS_CPP_MMU_QRIB_VMID_MMSS_CPP_QRIB_MMU_VMID_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x000014f0)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                                 0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_MMSS_FD_QRIB_MMU_VMID_INIT_EN_BMSK                                                                                   0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_EN_MMSS_FD_QRIB_MMU_VMID_INIT_EN_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x000014f4)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_RMSK                                                                                                                   0x1f
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_MMSS_FD_QRIB_MMU_VMID_INIT_BMSK                                                                                        0x1f
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INIT_MMSS_FD_QRIB_MMU_VMID_INIT_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000014f8)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_MMSS_FD_QRIB_MMU_VMID_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_EN_MMSS_FD_QRIB_MMU_VMID_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x000014fc)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_RMSK                                                                                                                        0x1f
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_MMSS_FD_QRIB_MMU_VMID_BMSK                                                                                                  0x1f
#define HWIO_TCSR_MMSS_FD_MMU_QRIB_VMID_MMSS_FD_QRIB_MMU_VMID_SHFT                                                                                                   0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000014b0)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                               0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_MMSS_JPEG_QRIB_MMU_VMID_INIT_EN_BMSK                                                                               0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_EN_MMSS_JPEG_QRIB_MMU_VMID_INIT_EN_SHFT                                                                               0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x000014b4)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_RMSK                                                                                                                 0x1f
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_MMSS_JPEG_QRIB_MMU_VMID_INIT_BMSK                                                                                    0x1f
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INIT_MMSS_JPEG_QRIB_MMU_VMID_INIT_SHFT                                                                                     0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x000014b8)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_RMSK                                                                                                                    0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_MMSS_JPEG_QRIB_MMU_VMID_EN_BMSK                                                                                         0x1
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_EN_MMSS_JPEG_QRIB_MMU_VMID_EN_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x000014bc)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_RMSK                                                                                                                      0x1f
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_MMSS_JPEG_QRIB_MMU_VMID_BMSK                                                                                              0x1f
#define HWIO_TCSR_MMSS_JPEG_MMU_QRIB_VMID_MMSS_JPEG_QRIB_MMU_VMID_SHFT                                                                                               0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000014c0)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                                0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_MMSS_MDP_QRIB_MMU_VMID_INIT_EN_BMSK                                                                                 0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_EN_MMSS_MDP_QRIB_MMU_VMID_INIT_EN_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000014c4)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_RMSK                                                                                                                  0x1f
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_MMSS_MDP_QRIB_MMU_VMID_INIT_BMSK                                                                                      0x1f
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INIT_MMSS_MDP_QRIB_MMU_VMID_INIT_SHFT                                                                                       0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x000014c8)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_MMSS_MDP_QRIB_MMU_VMID_EN_BMSK                                                                                           0x1
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_EN_MMSS_MDP_QRIB_MMU_VMID_EN_SHFT                                                                                           0x0

#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000014cc)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_RMSK                                                                                                                       0x1f
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_MMSS_MDP_QRIB_MMU_VMID_BMSK                                                                                                0x1f
#define HWIO_TCSR_MMSS_MDP_MMU_QRIB_VMID_MMSS_MDP_QRIB_MMU_VMID_SHFT                                                                                                 0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x000014d0)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                              0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_MMSS_OXILI_QRIB_MMU_VMID_INIT_EN_BMSK                                                                             0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_EN_MMSS_OXILI_QRIB_MMU_VMID_INIT_EN_SHFT                                                                             0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x000014d4)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_RMSK                                                                                                                0x1f
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_MMSS_OXILI_QRIB_MMU_VMID_INIT_BMSK                                                                                  0x1f
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INIT_MMSS_OXILI_QRIB_MMU_VMID_INIT_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x000014d8)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_RMSK                                                                                                                   0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_MMSS_OXILI_QRIB_MMU_VMID_EN_BMSK                                                                                       0x1
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_EN_MMSS_OXILI_QRIB_MMU_VMID_EN_SHFT                                                                                       0x0

#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000014dc)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_RMSK                                                                                                                     0x1f
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_MMSS_OXILI_QRIB_MMU_VMID_BMSK                                                                                            0x1f
#define HWIO_TCSR_MMSS_OXILI_MMU_QRIB_VMID_MMSS_OXILI_QRIB_MMU_VMID_SHFT                                                                                             0x0

#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00001500)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_RMSK                                                                                                             0xffffffff
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_MMSS_OXILICX_QRIB_XPU2_ACR_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_ACR_MMSS_OXILICX_QRIB_XPU2_ACR_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00001504)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                        0x10001
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                     0x10000
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                        0x10
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                   0x1
#define HWIO_TCSR_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILICX_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                   0x0

#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001508)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                0x1f001f
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OXILI_RBBM_QRIB_VMIDMT_ACR_INIT_BMSK                                                           0x1f0000
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OXILI_RBBM_QRIB_VMIDMT_ACR_INIT_SHFT                                                               0x10
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OXILI_RBBM_QRIB_VMIDMT_VMID_INIT_BMSK                                                              0x1f
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OXILI_RBBM_QRIB_VMIDMT_VMID_INIT_SHFT                                                               0x0

#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000150c)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_RMSK                                                                                                                0x1
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_EN_BMSK                                                                            0x1
#define HWIO_TCSR_MMSS_OXILI_RBBM_QRIB_VMIDMT_EN_MMSS_OXILI_RBBM_QRIB_VMIDMT_INIT_EN_SHFT                                                                            0x0

#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001510)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_BMSK                                                                            0xffffffff
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_MMSS_OXILI_VBIF_QRIB_XPU2_ACR_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00001514)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                     0x10001
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_BMSK                                                               0x10000
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                  0x10
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                             0x1
#define HWIO_TCSR_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_MMSS_OXILI_VBIF_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00001600)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_BMSK                                                                                0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_MMSS_OCMEM_DM_QRIB_XPU2_ACR_SHFT                                                                                       0x0

#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00001604)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                       0x10001
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                   0x10000
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                      0x10
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                 0x1
#define HWIO_TCSR_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_DM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                 0x0

#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00001608)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                             0x1f001f
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_ACR_INIT_BMSK                                                     0x1f0000
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_ACR_INIT_SHFT                                                         0x10
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_VMID_INIT_BMSK                                                        0x1f
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_VMID_INIT_SHFT                                                         0x0

#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000160c)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_RMSK                                                                                                             0x1
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_EN_BMSK                                                                      0x1
#define HWIO_TCSR_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_EN_MMSS_OCMEM_DMOCMEM_QRIB_VMIDMT_INIT_EN_SHFT                                                                      0x0

#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00001610)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                               0x1f001f
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_ACR_INIT_BMSK                                                         0x1f0000
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_ACR_INIT_SHFT                                                             0x10
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_VMID_INIT_BMSK                                                            0x1f
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_VMID_INIT_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001614)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_RMSK                                                                                                               0x1
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_EN_BMSK                                                                          0x1
#define HWIO_TCSR_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_EN_MMSS_OCMEM_DMSYS_QRIB_VMIDMT_INIT_EN_SHFT                                                                          0x0

#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001618)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_MMSS_OCMEM_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_ACR_MMSS_OCMEM_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000161c)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001620)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_BMSK                                                                            0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_MMSS_OCMEM_OSW0_QRIB_XPU2_ACR_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00001624)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                     0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_BMSK                                                               0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                  0x10
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                             0x1
#define HWIO_TCSR_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW0_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001628)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_BMSK                                                                            0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_MMSS_OCMEM_OSW1_QRIB_XPU2_ACR_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000162c)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                     0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_BMSK                                                               0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                  0x10
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                             0x1
#define HWIO_TCSR_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSW1_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00001630)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_BMSK                                                                          0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_MMSS_OCMEM_OSWDM_QRIB_XPU2_ACR_SHFT                                                                                 0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00001284)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                    0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                             0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                0x10
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                           0x1
#define HWIO_TCSR_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWDM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                           0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001638)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_RMSK                                                                                                        0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_BMSK                                                                        0xffffffff
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_ACR_SHFT                                                                               0x0

#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001304)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                   0x10001
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_BMSK                                                           0x10000
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_SHFT                                                              0x10
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                         0x1
#define HWIO_TCSR_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_MMSS_OCMEM_OSWSYS_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                         0x0

#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00001660)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                     0x1f001f
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR0_QRIB_VMIDMT_ACR_INIT_BMSK                                                                     0x1f0000
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR0_QRIB_VMIDMT_ACR_INIT_SHFT                                                                         0x10
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR0_VMIDMT_VMID_INIT_BMSK                                                                             0x1f
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR0_VMIDMT_VMID_INIT_SHFT                                                                              0x0

#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001664)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_PCIE_MSTR0_QRIB_VMIDMT_INIT_EN_BMSK                                                                                      0x1
#define HWIO_TCSR_PCIE_MSTR0_QRIB_VMIDMT_EN_PCIE_MSTR0_QRIB_VMIDMT_INIT_EN_SHFT                                                                                      0x0

#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00001668)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                     0x1f001f
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR1_QRIB_VMIDMT_ACR_INIT_BMSK                                                                     0x1f0000
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR1_QRIB_VMIDMT_ACR_INIT_SHFT                                                                         0x10
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR1_VMIDMT_VMID_INIT_BMSK                                                                             0x1f
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_INIT_ACR_INIT_PCIE_MSTR1_VMIDMT_VMID_INIT_SHFT                                                                              0x0

#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000166c)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_RMSK                                                                                                                     0x1
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_PCIE_MSTR1_QRIB_VMIDMT_INIT_EN_BMSK                                                                                      0x1
#define HWIO_TCSR_PCIE_MSTR1_QRIB_VMIDMT_EN_PCIE_MSTR1_QRIB_VMIDMT_INIT_EN_SHFT                                                                                      0x0

#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001670)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_PCIE_PARF0_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_ACR_PCIE_PARF0_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00001674)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF0_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001678)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_PCIE_PARF1_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_ACR_PCIE_PARF1_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000167c)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_PCIE_PARF1_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001680)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_PCIE_SLV0_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_ACR_PCIE_SLV0_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001684)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV0_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001688)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_PCIE_SLV1_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_ACR_PCIE_SLV1_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000168c)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_PCIE_SLV1_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00001690)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                       0x1f001f
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_USB_PRIM_QRIB_VMIDMT_ACR_INIT_BMSK                                                                         0x1f0000
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_USB_PRIM_QRIB_VMIDMT_ACR_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_USB_PRIM_VMIDMT_VMID_INIT_BMSK                                                                                 0x1f
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_INIT_ACR_INIT_USB_PRIM_VMIDMT_VMID_INIT_SHFT                                                                                  0x0

#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001694)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_RMSK                                                                                                                       0x1
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_USB_PRIM_QRIB_VMIDMT_INIT_EN_BMSK                                                                                          0x1
#define HWIO_TCSR_USB_PRIM_QRIB_VMIDMT_EN_USB_PRIM_QRIB_VMIDMT_INIT_EN_SHFT                                                                                          0x0

#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000016a0)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_USB_PRIM_QRIB_XPU2_ACR_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_ACR_USB_PRIM_QRIB_XPU2_ACR_SHFT                                                                                                 0x0

#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x000016a4)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                            0x10001
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                             0x10000
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                0x10
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                           0x1
#define HWIO_TCSR_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_USB_PRIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001840)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_LPASS_AXIM_QRIB_XPU2_ACR_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_ACR_LPASS_AXIM_QRIB_XPU2_ACR_SHFT                                                                                             0x0

#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00001844)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                          0x10001
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                         0x10000
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                            0x10
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                       0x1
#define HWIO_TCSR_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_AXIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                       0x0

#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001848)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_BMSK                                                                            0xffffffff
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_LPASS_CORE_AXIM_QRIB_XPU2_ACR_SHFT                                                                                   0x0

#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000184c)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                     0x10001
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_BMSK                                                               0x10000
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                  0x10
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                             0x1
#define HWIO_TCSR_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_LPASS_CORE_AXIM_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                             0x0

#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001854)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_ADDR, HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_RMSK)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_ADDR,m,v,HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_IN)
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_COPSS_QRIB_APU_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_COPSS_QRIB_APU_XPU2_ACR_COPSS_QRIB_APU_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00002000)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MPM_QDSS_XPU2_NON_SECURE_INTR_BMSK                                                                                    0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MPM_QDSS_XPU2_NON_SECURE_INTR_SHFT                                                                                          0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DDR_PHY_CFG_XPU2_NON_SECURE_INTR_BMSK                                                                                 0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DDR_PHY_CFG_XPU2_NON_SECURE_INTR_SHFT                                                                                       0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_NOC_CFG_XPU2_NON_SECURE_INTR_BMSK                                                                                     0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_NOC_CFG_XPU2_NON_SECURE_INTR_SHFT                                                                                           0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                           0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                 0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_NON_SECURE_INTR_BMSK                                                                                          0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_NON_SECURE_INTR_SHFT                                                                                               0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPDM_XPU_NON_SECURE_INTR_BMSK                                                                                          0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPDM_XPU_NON_SECURE_INTR_SHFT                                                                                               0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPMI_RPU_NON_SECURE_INTR_BMSK                                                                                          0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPMI_RPU_NON_SECURE_INTR_SHFT                                                                                               0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_APU_NON_SECURE_INTR_BMSK                                                                                      0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_APU_NON_SECURE_INTR_SHFT                                                                                           0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_RPU_NON_SECURE_INTR_BMSK                                                                                         0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_RPU_NON_SECURE_INTR_SHFT                                                                                             0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH1_XPU2_NS_INTERRUPT_BMSK                                                                                         0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH1_XPU2_NS_INTERRUPT_SHFT                                                                                             0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NS_INTERRUPT_BMSK                                                                                         0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NS_INTERRUPT_SHFT                                                                                             0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CFG_XPU2_NS_INTERRUPT_BMSK                                                                                         0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CFG_XPU2_NS_INTERRUPT_SHFT                                                                                             0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SECURE_INTR_BMSK                                                                                             0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SECURE_INTR_SHFT                                                                                                0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_APU_NON_SEC_INTR_BMSK                                                                                                0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_APU_NON_SEC_INTR_SHFT                                                                                                   0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MSG_RAM_XPU_NS_INTR_BMSK                                                                                                 0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MSG_RAM_XPU_NS_INTR_SHFT                                                                                                    0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_WCSS_APU_NON_SECURE_IRQ_BMSK                                                                                             0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_WCSS_APU_NON_SECURE_IRQ_SHFT                                                                                                0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_APP_XPU2_NS_INTR_BMSK                                                                                                0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_APP_XPU2_NS_INTR_SHFT                                                                                                   0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_APCS_SYSKSMPUNONSECUREINT_BMSK                                                                                            0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_APCS_SYSKSMPUNONSECUREINT_SHFT                                                                                               0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_COPSS_APU_NON_SECURE_IRQ_BMSK                                                                                             0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_COPSS_APU_NON_SECURE_IRQ_SHFT                                                                                                0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_USB30_XPU_CONF_NON_SECURE_IRQ_BMSK                                                                                        0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_USB30_XPU_CONF_NON_SECURE_IRQ_SHFT                                                                                           0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSWSYS_MPU_NON_SECURE_INTR_BMSK                                                                                      0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSWSYS_MPU_NON_SECURE_INTR_SHFT                                                                                        0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSW1_MPU_NON_SECURE_INTR_BMSK                                                                                        0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSW1_MPU_NON_SECURE_INTR_SHFT                                                                                          0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSW0_MPU_NON_SECURE_INTR_BMSK                                                                                        0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_OSW0_MPU_NON_SECURE_INTR_SHFT                                                                                          0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_DM_RPU_NON_SECURE_INTR_BMSK                                                                                          0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_DM_RPU_NON_SECURE_INTR_SHFT                                                                                            0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_RPU_NON_SECURE_INTR_BMSK                                                                                              0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCMEM_RPU_NON_SECURE_INTR_SHFT                                                                                               0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MDSS_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                         0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MDSS_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                          0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CAMSS_JPEG_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                   0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CAMSS_JPEG_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                    0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CAMSS_VFE_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CAMSS_VFE_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MMCC_XPU2_NON_SECURE_INTR_BMSK                                                                                               0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MMCC_XPU2_NON_SECURE_INTR_SHFT                                                                                               0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_VENUS0_XPU2_NON_SECURE_INTR_BMSK                                                                                             0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_VENUS0_XPU2_NON_SECURE_INTR_SHFT                                                                                             0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OXILI_XPU2_NON_SECURE_INTR_BMSK                                                                                              0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OXILI_XPU2_NON_SECURE_INTR_SHFT                                                                                              0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MSS_XPU2_INTR_BMSK                                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MSS_XPU2_INTR_SHFT                                                                                                           0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00002004)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RPM_MPU_NON_SEC_INTR_BMSK                                                                                             0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RPM_MPU_NON_SEC_INTR_SHFT                                                                                                   0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCIMEM_MPU_NON_SECURE_INTR_BMSK                                                                                       0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCIMEM_MPU_NON_SECURE_INTR_SHFT                                                                                             0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SEC_CTRL_XPU2_NON_SEC_IRQ_BMSK                                                                                        0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SEC_CTRL_XPU2_NON_SEC_IRQ_SHFT                                                                                              0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                           0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                 0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_DEHR_XPU_NON_SECURE_INTR_BMSK                                                                                          0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_DEHR_XPU_NON_SECURE_INTR_SHFT                                                                                               0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_VENUS0_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                  0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_VENUS0_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                       0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OXILI_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                   0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OXILI_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                        0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MDSS_XPU2_NON_SECURE_INTR_BMSK                                                                                         0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MDSS_XPU2_NON_SECURE_INTR_SHFT                                                                                              0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_QDSS_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                     0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_QDSS_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                         0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MPM_XPU2_NON_SECURE_INTR_BMSK                                                                                           0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MPM_XPU2_NON_SECURE_INTR_SHFT                                                                                               0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BOOT_ROM_NON_SECURE_INTR_BMSK                                                                                           0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BOOT_ROM_NON_SECURE_INTR_SHFT                                                                                               0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_GFX_RBCPR_XPU2_NON_SECURE_INTR_BMSK                                                                                0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_GFX_RBCPR_XPU2_NON_SECURE_INTR_SHFT                                                                                    0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_MISC_XPU2_NON_SECURE_INTR_BMSK                                                                                      0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_MISC_XPU2_NON_SECURE_INTR_SHFT                                                                                         0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_PCIE20_1_INT_PARF_XPU2_NON_SECURE_INTR_BMSK                                                                              0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_PCIE20_1_INT_PARF_XPU2_NON_SECURE_INTR_SHFT                                                                                 0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_NOC_XPU2_NON_SECURE_INTR_BMSK                                                                                       0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MMSS_NOC_XPU2_NON_SECURE_INTR_SHFT                                                                                          0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCMEM_OSWDM_MPU_NON_SECURE_INTR_BMSK                                                                                     0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCMEM_OSWDM_MPU_NON_SECURE_INTR_SHFT                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CAMSS_XPU2_NON_SECURE_INTR_BMSK                                                                                           0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CAMSS_XPU2_NON_SECURE_INTR_SHFT                                                                                              0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CRYPTO1_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                    0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CRYPTO1_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                       0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                    0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                       0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_O_TCSR_MUTEX_NON_SECURE_INTR_BMSK                                                                                         0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_O_TCSR_MUTEX_NON_SECURE_INTR_SHFT                                                                                            0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_O_TCSR_REGS_NON_SECURE_INTR_BMSK                                                                                           0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_O_TCSR_REGS_NON_SECURE_INTR_SHFT                                                                                             0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_COPSS_MPU_NON_SECURE_IRQ_BMSK                                                                                              0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_COPSS_MPU_NON_SECURE_IRQ_SHFT                                                                                                0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RESERVEBIT_BMSK                                                                                                            0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RESERVEBIT_SHFT                                                                                                              0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_USB1_HS_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                         0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_USB1_HS_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                           0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC4_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC4_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                              0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC3_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC3_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                              0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC2_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC2_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                              0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC1_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDC1_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                              0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_TSIF_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                          0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_TSIF_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                          0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP2_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP2_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                             0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP1_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                             0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP1_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                             0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BAM_DMA_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BAM_DMA_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                           0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00002008)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RMSK                                                                                                                      0x3fff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RESERVEBIT_2_BMSK                                                                                                         0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RESERVEBIT_2_SHFT                                                                                                            0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_FD_VBIF_XPU_NON_SECURE_INTR_BMSK                                                                                          0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_FD_VBIF_XPU_NON_SECURE_INTR_SHFT                                                                                             0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_FD_XPU_NON_SECURE_INTR_BMSK                                                                                                0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_FD_XPU_NON_SECURE_INTR_SHFT                                                                                                  0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_UFS_XPU_NON_SECURE_INTR_BMSK                                                                                               0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_UFS_XPU_NON_SECURE_INTR_SHFT                                                                                                 0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_CAMSS_CPP_VBIF_XPU2_NON_SECURE_INTR_BMSK                                                                                   0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_CAMSS_CPP_VBIF_XPU2_NON_SECURE_INTR_SHFT                                                                                     0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RICA_CORE_TOP_XPU2_NON_SECURE_IRQ_BMSK                                                                                     0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RICA_CORE_TOP_XPU2_NON_SECURE_IRQ_SHFT                                                                                       0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RICA_CORE_VBIF_XPU2_NON_SECURE_IRQ_BMSK                                                                                     0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RICA_CORE_VBIF_XPU2_NON_SECURE_IRQ_SHFT                                                                                      0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_IPA_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_IPA_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RESERVEBIT_0_BMSK                                                                                                           0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_RESERVEBIT_0_SHFT                                                                                                            0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_UFS_ICE_XPU2_NON_SECURE_INTR_BMSK                                                                                           0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_UFS_ICE_XPU2_NON_SECURE_INTR_SHFT                                                                                            0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_1_INT_SLV_XPU2_NON_SECURE_INTR_BMSK                                                                                   0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_1_INT_SLV_XPU2_NON_SECURE_INTR_SHFT                                                                                   0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_CRYPTO2_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                                                                       0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_CRYPTO2_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                                                                       0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_0_INT_SLV_XPU2_NON_SECURE_INTR_BMSK                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_0_INT_SLV_XPU2_NON_SECURE_INTR_SHFT                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_0_INT_PARF_XPU2_NON_SECURE_INTR_BMSK                                                                                  0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_PCIE20_0_INT_PARF_XPU2_NON_SECURE_INTR_SHFT                                                                                  0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00002040)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK                                                                                                           0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MPM_QDSS_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                      0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MPM_QDSS_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                            0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DDR_PHY_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                   0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DDR_PHY_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                         0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_NOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                       0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_NOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                             0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                    0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                          0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                            0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                 0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPDM_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                            0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPDM_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                 0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPMI_RPU_NON_SECURE_INTR_ENABLE_BMSK                                                                            0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPMI_RPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                 0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_APU_NON_SECURE_INTR_ENABLE_BMSK                                                                        0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_APU_NON_SECURE_INTR_ENABLE_SHFT                                                                             0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_RPU_NON_SECURE_INTR_ENABLE_BMSK                                                                           0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_RPU_NON_SECURE_INTR_ENABLE_SHFT                                                                               0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_NS_INTERRUPT_BMSK                                                                                  0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_NS_INTERRUPT_SHFT                                                                                      0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NS_INTERRUPT_BMSK                                                                                  0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NS_INTERRUPT_SHFT                                                                                      0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_NS_INTERRUPT_BMSK                                                                                  0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_NS_INTERRUPT_SHFT                                                                                      0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                               0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                  0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_APU_NON_SEC_INTR_ENABLE_BMSK                                                                                  0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_APU_NON_SEC_INTR_ENABLE_SHFT                                                                                     0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MSG_RAM_XPU_NS_INTR_ENABLE_BMSK                                                                                   0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MSG_RAM_XPU_NS_INTR_ENABLE_SHFT                                                                                      0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_WCSS_APU_NON_SECURE_IRQ_ENABLE_BMSK                                                                               0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_WCSS_APU_NON_SECURE_IRQ_ENABLE_SHFT                                                                                  0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_APP_XPU2_NS_INTR_ENABLE_BMSK                                                                                  0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_APP_XPU2_NS_INTR_ENABLE_SHFT                                                                                     0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_APCS_SYSKSMPUNONSECUREINT_BMSK                                                                                     0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_APCS_SYSKSMPUNONSECUREINT_SHFT                                                                                        0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_COPSS_APU_NON_SECURE_IRQ_ENABLE_BMSK                                                                               0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_COPSS_APU_NON_SECURE_IRQ_ENABLE_SHFT                                                                                  0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_USB30_XPU_CONF_NON_SECURE_IRQ_ENABLE_BMSK                                                                          0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_USB30_XPU_CONF_NON_SECURE_IRQ_ENABLE_SHFT                                                                             0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSWSYS_MPU_NON_SECURE_INTR_ENABLE_BMSK                                                                        0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSWSYS_MPU_NON_SECURE_INTR_ENABLE_SHFT                                                                          0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSW1_MPU_NON_SECURE_INTR_ENABLE_BMSK                                                                          0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSW1_MPU_NON_SECURE_INTR_ENABLE_SHFT                                                                            0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSW0_MPU_NON_SECURE_INTR_ENABLE_BMSK                                                                          0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_OSW0_MPU_NON_SECURE_INTR_ENABLE_SHFT                                                                            0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_DM_RPU_NON_SECURE_INTR_ENABLE_BMSK                                                                            0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_DM_RPU_NON_SECURE_INTR_ENABLE_SHFT                                                                              0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_RPU_NON_SECURE_INTR_ENABLE_BMSK                                                                                0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCMEM_RPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                 0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MDSS_VBIF_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                           0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MDSS_VBIF_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                            0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                     0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                      0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                      0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                       0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MMCC_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                                 0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MMCC_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                                 0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_VENUS0_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                               0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_VENUS0_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                               0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OXILI_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                                0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OXILI_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                                0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MSS_XPU2_INTR_ENABLE_BMSK                                                                                             0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MSS_XPU2_INTR_ENABLE_SHFT                                                                                             0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00002044)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RMSK                                                                                                           0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RPM_MPU_NON_SEC_INTER_ENABLE_BMSK                                                                              0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RPM_MPU_NON_SEC_INTER_ENABLE_SHFT                                                                                    0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCIMEM_MPU_NON_SECURE_INTER_ENABLE_BMSK                                                                        0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCIMEM_MPU_NON_SECURE_INTER_ENABLE_SHFT                                                                              0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SEC_CTRL_XPU2_NON_SEC_IRQ_ENABLE_BMSK                                                                          0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SEC_CTRL_XPU2_NON_SEC_IRQ_ENABLE_SHFT                                                                                0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_BMSK                                                                             0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_SHFT                                                                                   0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_DEHR_XPU_NON_SECURE_INTER_ENABLE_BMSK                                                                           0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_DEHR_XPU_NON_SECURE_INTER_ENABLE_SHFT                                                                                0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_VENUS0_VBIF_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                   0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_VENUS0_VBIF_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                        0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OXILI_VBIF_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                    0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OXILI_VBIF_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                         0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MDSS_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                          0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MDSS_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                               0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_QDSS_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                       0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_QDSS_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                           0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MPM_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                            0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MPM_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                                0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BOOT_ROM_NON_SECURE_INTER_ENABLE_BMSK                                                                            0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BOOT_ROM_NON_SECURE_INTER_ENABLE_SHFT                                                                                0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                 0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                     0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_MISC_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                       0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_MISC_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                          0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                               0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                  0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_NOC_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                        0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MMSS_NOC_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                           0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCMEM_OSWDM_MPU_NON_SECURE_INTER_ENABLE_BMSK                                                                      0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCMEM_OSWDM_MPU_NON_SECURE_INTER_ENABLE_SHFT                                                                         0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CAMSS_XPU2_NON_SECURE_INTER_ENABLE_BMSK                                                                            0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CAMSS_XPU2_NON_SECURE_INTER_ENABLE_SHFT                                                                               0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CRYPTO1_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                      0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CRYPTO1_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                         0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                      0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                         0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_O_TCSR_MUTEX_NON_SECURE_INTER_ENABLE_BMSK                                                                          0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_O_TCSR_MUTEX_NON_SECURE_INTER_ENABLE_SHFT                                                                             0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_O_TCSR_REGS_NON_SECURE_INTER_ENABLE_BMSK                                                                            0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_O_TCSR_REGS_NON_SECURE_INTER_ENABLE_SHFT                                                                              0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_COPSS_MPU_NON_SECURE_IRQ_ENABLE_BMSK                                                                                0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_COPSS_MPU_NON_SECURE_IRQ_ENABLE_SHFT                                                                                  0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RESERVEBIT_BMSK                                                                                                     0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RESERVEBIT_SHFT                                                                                                       0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_USB1_HS_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                           0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_USB1_HS_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                             0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC4_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC4_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC3_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC3_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC2_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC2_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC1_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDC1_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_TSIF_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                            0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_TSIF_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                            0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP2_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP2_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                               0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP1_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP1_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                               0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BAM_DMA_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                             0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BAM_DMA_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                             0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00002048)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RMSK                                                                                                               0x3fff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RESERVEBIT_2_BMSK                                                                                                  0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RESERVEBIT_2_SHFT                                                                                                     0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_FD_VBIF_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_FD_VBIF_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_FD_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                                  0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_FD_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                    0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_UFS_XPU_NON_SECURE_INTR_ENABLE_BMSK                                                                                 0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_UFS_XPU_NON_SECURE_INTR_ENABLE_SHFT                                                                                   0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                     0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                       0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RICA_CORE_TOP_XPU2_NON_SECURE_IRQ_ENABLE_BMSK                                                                       0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RICA_CORE_TOP_XPU2_NON_SECURE_IRQ_ENABLE_SHFT                                                                         0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_NON_SECURE_IRQ_ENABLE_BMSK                                                                       0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_NON_SECURE_IRQ_ENABLE_SHFT                                                                        0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_IPA_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_IPA_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RESERVEBIT_0_BMSK                                                                                                    0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_RESERVEBIT_0_SHFT                                                                                                     0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_UFS_ICE_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                             0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_UFS_ICE_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                              0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                     0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                     0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_CRYPTO2_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                                                                         0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_CRYPTO2_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                                                                         0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                     0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                     0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_NON_SECURE_INTR_ENABLE_BMSK                                                                    0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                                                    0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00002010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT1_BMSK                                                                              0x80000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT1_SHFT                                                                                    0x1f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT0_BMSK                                                                              0x40000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT0_SHFT                                                                                    0x1e
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGIRPT_BMSK                                                                                  0x20000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGIRPT_SHFT                                                                                        0x1d
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RICA_CORE_VMIDMT_NSG_IRQ_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RICA_CORE_VMIDMT_NSG_IRQ_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_NSG_IRQ_BMSK                                                                                        0x8000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_NSG_IRQ_SHFT                                                                                             0x1b
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_NSGIRPT_BMSK                                                                              0x4000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_NSGIRPT_SHFT                                                                                   0x1a
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CAMSS_VMIDMT_NSGIRPT_BMSK                                                                                      0x2000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CAMSS_VMIDMT_NSGIRPT_SHFT                                                                                           0x19
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO1_VMIDMT_NSGIRPT_IRQ_BMSK                                                                                0x1000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO1_VMIDMT_NSGIRPT_IRQ_SHFT                                                                                     0x18
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_IRQ_BMSK                                                                                 0x800000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_IRQ_SHFT                                                                                     0x17
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RESERVEBIT_2_BMSK                                                                                               0x400000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                   0x16
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_USB1_HS_VMIDMT_NSGIRPT_BMSK                                                                                     0x200000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_USB1_HS_VMIDMT_NSGIRPT_SHFT                                                                                         0x15
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_TSIF_BAM_VMIDMT_NSGIRPT_BMSK                                                                                    0x100000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_TSIF_BAM_VMIDMT_NSGIRPT_SHFT                                                                                        0x14
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC4_VMIDMT_NSGIRPT_BMSK                                                                                         0x80000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC4_VMIDMT_NSGIRPT_SHFT                                                                                            0x13
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC3_VMIDMT_NSGIRPT_BMSK                                                                                         0x40000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC3_VMIDMT_NSGIRPT_SHFT                                                                                            0x12
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC2_VMIDMT_NSGIRPT_BMSK                                                                                         0x20000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC2_VMIDMT_NSGIRPT_SHFT                                                                                            0x11
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC1_VMIDMT_NSGIRPT_BMSK                                                                                         0x10000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SDC1_VMIDMT_NSGIRPT_SHFT                                                                                            0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP2_VMIDMT_NSGIRPT_BMSK                                                                                         0x8000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP2_VMIDMT_NSGIRPT_SHFT                                                                                            0xf
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_BMSK                                                                                         0x4000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_SHFT                                                                                            0xe
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BAM_DMA_VMIDMT_NSGIRPT_BMSK                                                                                       0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BAM_DMA_VMIDMT_NSGIRPT_SHFT                                                                                          0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SPDM_VMID_NSGIRPT_BMSK                                                                                            0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SPDM_VMID_NSGIRPT_SHFT                                                                                               0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGIRPT_BMSK                                                                                       0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGIRPT_SHFT                                                                                         0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGIRPT_BMSK                                                                                     0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGIRPT_SHFT                                                                                       0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_WCSS_VMIDMT_NSGIRPT_BMSK                                                                                           0x200
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_WCSS_VMIDMT_NSGIRPT_SHFT                                                                                             0x9
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_USB30_VMIDMT_NSGIRPT_IRQ_BMSK                                                                                      0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_USB30_VMIDMT_NSGIRPT_IRQ_SHFT                                                                                        0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OCMEM_DMDDR_VMIDMT_NSGIRPT_BMSK                                                                                     0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OCMEM_DMDDR_VMIDMT_NSGIRPT_SHFT                                                                                      0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_13_BMSK                                                                                      0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_13_SHFT                                                                                       0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                   0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                    0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_UFS_VMIDMT_NSGIRPT_BMSK                                                                                             0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_UFS_VMIDMT_NSGIRPT_SHFT                                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_CPU_VMIDMT_NSGIRPT_BMSK                                                                                       0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_CPU_VMIDMT_NSGIRPT_SHFT                                                                                       0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_5_BMSK                                                                                        0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_5_SHFT                                                                                        0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OXILI_RBBM_VMIDMT_NSGIRPT_BMSK                                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_OXILI_RBBM_VMIDMT_NSGIRPT_SHFT                                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_MSS_VMIDMT_CLIENT_SUMMARY_INTR_BMSK                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_MSS_VMIDMT_CLIENT_SUMMARY_INTR_SHFT                                                                                  0x0

#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00002014)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RMSK                                                                                                                  0x7ff
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                     0x400
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                       0xa
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                     0x200
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                       0x9
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_FD_MMU_NSG_IRQ_BMSK                                                                                                   0x100
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_FD_MMU_NSG_IRQ_SHFT                                                                                                     0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RICA_CORE_MMU_NSG_IRQ_BMSK                                                                                             0x80
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RICA_CORE_MMU_NSG_IRQ_SHFT                                                                                              0x7
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_0_BMSK                                                                                                      0x40
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RESERVEBIT_0_SHFT                                                                                                       0x6
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_CPP_MMU_NSGIRPT_BMSK                                                                                             0x20
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_CPP_MMU_NSGIRPT_SHFT                                                                                              0x5
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_MMU_NSGIRPT_BMSK                                                                                                  0x10
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_MMU_NSGIRPT_SHFT                                                                                                   0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGIRPT_BMSK                                                                                             0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGIRPT_SHFT                                                                                             0x3
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_VFE_MMU_NSGIRPT_BMSK                                                                                              0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_VFE_MMU_NSGIRPT_SHFT                                                                                              0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_VENUS0_MMU_NSGIRPT_BMSK                                                                                                 0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_VENUS0_MMU_NSGIRPT_SHFT                                                                                                 0x1
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_OXILI_MMU_NSGIRPT_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_OXILI_MMU_NSGIRPT_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00002018)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RMSK                                                                                                               0x7f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RESERVEBIT_BMSK                                                                                                    0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RESERVEBIT_SHFT                                                                                                     0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_IPA_VMIDMT_NSGIRPT_BMSK                                                                                            0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_IPA_VMIDMT_NSGIRPT_SHFT                                                                                             0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_NSGIRPT_BMSK                                                                              0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_NSGIRPT_SHFT                                                                               0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_NSGIRPT_BMSK                                                                               0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_NSGIRPT_SHFT                                                                               0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_CRYPTO2_VMIDMT_NSGIRPT_IRQ_BMSK                                                                                     0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_CRYPTO2_VMIDMT_NSGIRPT_IRQ_SHFT                                                                                     0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RESERVEBIT_4_BMSK                                                                                                   0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_RESERVEBIT_4_SHFT                                                                                                   0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_DEHR_VMIDMT_NSGIRPT_BMSK                                                                                            0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_DEHR_VMIDMT_NSGIRPT_SHFT                                                                                            0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00002050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK                                                                                                   0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT1_ENABLE_BMSK                                                                0x80000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT1_ENABLE_SHFT                                                                      0x1f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT0_ENABLE_BMSK                                                                0x40000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT0_ENABLE_SHFT                                                                      0x1e
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                    0x20000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                          0x1d
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_NSG_IRQ_ENABLE_BMSK                                                                   0x10000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_NSG_IRQ_ENABLE_SHFT                                                                         0x1c
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSG_IRQ_ENABLE_BMSK                                                                          0x8000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSG_IRQ_ENABLE_SHFT                                                                               0x1b
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                0x4000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                     0x1a
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                        0x2000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                             0x19
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                                                                  0x1000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                                                                       0x18
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                                                                   0x800000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                                                                       0x17
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_6_BMSK                                                                                        0x400000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_6_SHFT                                                                                            0x16
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_USB1_HS_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                       0x200000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_USB1_HS_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                           0x15
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                      0x100000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                          0x14
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC4_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x80000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC4_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0x13
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC3_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x40000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC3_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0x12
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC2_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x20000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC2_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0x11
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC1_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x10000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SDC1_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x8000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0xf
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                           0x4000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0xe
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                         0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                            0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGIRPT_ENABLE_BMSK                                                                              0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGIRPT_ENABLE_SHFT                                                                                 0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                         0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                           0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                       0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                         0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_WCSS_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                             0x200
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_WCSS_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                               0x9
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_USB30_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                                                                        0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_USB30_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                                                                          0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                       0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                        0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_13_ENABLE_BMSK                                                                        0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_13_ENABLE_SHFT                                                                         0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_5_BMSK                                                                                            0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_5_SHFT                                                                                             0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                               0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                                0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                         0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                         0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_5_ENABLE_BMSK                                                                          0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_5_ENABLE_SHFT                                                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                         0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                         0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_MSS_VMIDMT_CLIENT_SUMMARY_INTR_ENABLE_BMSK                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_MSS_VMIDMT_CLIENT_SUMMARY_INTR_ENABLE_SHFT                                                                    0x0

#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00002054)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RMSK                                                                                                           0x7ff
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_2_BMSK                                                                                              0x400
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_2_SHFT                                                                                                0xa
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_1_BMSK                                                                                              0x200
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_1_SHFT                                                                                                0x9
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_FD_MMU_NSG_IRQ_ENABLE_BMSK                                                                                     0x100
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_FD_MMU_NSG_IRQ_ENABLE_SHFT                                                                                       0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RICA_CORE_MMU_NSG_IRQ_ENABLE_BMSK                                                                               0x80
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RICA_CORE_MMU_NSG_IRQ_ENABLE_SHFT                                                                                0x7
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_0_BMSK                                                                                               0x40
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RESERVEBIT_0_SHFT                                                                                                0x6
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGIRPT_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGIRPT_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGIRPT_ENABLE_BMSK                                                                              0x10
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGIRPT_ENABLE_SHFT                                                                               0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGIRPT_ENABLE_BMSK                                                                                0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGIRPT_ENABLE_SHFT                                                                                0x3
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGIRPT_ENABLE_BMSK                                                                                0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGIRPT_ENABLE_SHFT                                                                                0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGIRPT_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGIRPT_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OXILI_MMU_NSGIRPT_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OXILI_MMU_NSGIRPT_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00002058)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RMSK                                                                                                        0x7f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RESERVEBIT_BMSK                                                                                             0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RESERVEBIT_SHFT                                                                                              0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_IPA_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                              0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_IPA_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                               0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                 0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                 0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                 0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                                                                       0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RESERVEBIT_8_BMSK                                                                                            0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_RESERVEBIT_8_SHFT                                                                                            0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_DEHR_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                              0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR1_ENABLE_DEHR_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                              0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00003000)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK                                                                                                             0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT1_BMSK                                                                              0x80000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT1_SHFT                                                                                    0x1f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT0_BMSK                                                                              0x40000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT0_SHFT                                                                                    0x1e
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGCFGIRPT_BMSK                                                                                  0x20000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGCFGIRPT_SHFT                                                                                        0x1d
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RICA_CORE_VMIDMT_NSGCFG_IRQ_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RICA_CORE_VMIDMT_NSGCFG_IRQ_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_NSGCFG_IRQ_BMSK                                                                                        0x8000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_NSGCFG_IRQ_SHFT                                                                                             0x1b
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_NSGCFGIRPT_BMSK                                                                              0x4000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_NSGCFGIRPT_SHFT                                                                                   0x1a
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CAMSS_VMIDMT_NSGCFGIRPT_BMSK                                                                                      0x2000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CAMSS_VMIDMT_NSGCFGIRPT_SHFT                                                                                           0x19
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO1_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                                                                0x1000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO1_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                                                                     0x18
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                                                                 0x800000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                                                                     0x17
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                  0x400000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                      0x16
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_USB1_HS_VMIDMT_NSGCFGIRPT_BMSK                                                                                     0x200000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_USB1_HS_VMIDMT_NSGCFGIRPT_SHFT                                                                                         0x15
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_TSIF_BAM_VMIDMT_NSGCFGIRPT_BMSK                                                                                    0x100000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_TSIF_BAM_VMIDMT_NSGCFGIRPT_SHFT                                                                                        0x14
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC4_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x80000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC4_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0x13
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC3_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x40000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC3_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0x12
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC2_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x20000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC2_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0x11
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC1_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x10000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SDC1_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP2_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x8000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP2_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0xf
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_BMSK                                                                                         0x4000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0xe
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BAM_DMA_VMIDMT_NSGCFGIRPT_BMSK                                                                                       0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BAM_DMA_VMIDMT_NSGCFGIRPT_SHFT                                                                                          0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SPDM_VMID_NSGCFGIRPT_BMSK                                                                                            0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SPDM_VMID_NSGCFGIRPT_SHFT                                                                                               0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGCFGIRPT_BMSK                                                                                       0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGCFGIRPT_SHFT                                                                                         0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGCFGIRPT_BMSK                                                                                     0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGCFGIRPT_SHFT                                                                                       0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_WCSS_VMIDMT_NSGCFGIRPT_BMSK                                                                                           0x200
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_WCSS_VMIDMT_NSGCFGIRPT_SHFT                                                                                             0x9
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_USB30_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                                                                      0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_USB30_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                                                                        0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OCMEM_DMDDR_VMIDMT_NSGCFGIRPT_BMSK                                                                                     0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OCMEM_DMDDR_VMIDMT_NSGCFGIRPT_SHFT                                                                                      0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_12_BMSK                                                                                         0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_12_SHFT                                                                                          0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                      0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                       0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_UFS_VMIDMT_NSGCFGIRPT_BMSK                                                                                             0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_UFS_VMIDMT_NSGCFGIRPT_SHFT                                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_CPU_VMIDMT_NSGCFGIRPT_BMSK                                                                                       0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_CPU_VMIDMT_NSGCFGIRPT_SHFT                                                                                       0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_4_BMSK                                                                                           0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_LPASS_IRQ_OUT_SECURITY_4_SHFT                                                                                           0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OXILI_RBBM_VMIDMT_NSGCFGIRPT_BMSK                                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_OXILI_RBBM_VMIDMT_NSGCFGIRPT_SHFT                                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_MSS_VMIDMT_CFG_SUMMARY_INTR_BMSK                                                                                        0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_MSS_VMIDMT_CFG_SUMMARY_INTR_SHFT                                                                                        0x0

#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00003004)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RMSK                                                                                                                     0x7ff
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                        0x400
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                          0xa
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                        0x200
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                          0x9
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_FD_MMU_NSGCFGIRPT_BMSK                                                                                                   0x100
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_FD_MMU_NSGCFGIRPT_SHFT                                                                                                     0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RICA_CORE_MMU_NSGCFGIRPT_BMSK                                                                                             0x80
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RICA_CORE_MMU_NSGCFGIRPT_SHFT                                                                                              0x7
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_0_BMSK                                                                                                         0x40
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RESERVEBIT_0_SHFT                                                                                                          0x6
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_MMU_NSGCFGIRPT_BMSK                                                                                                  0x20
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_MMU_NSGCFGIRPT_SHFT                                                                                                   0x5
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGCFGIRPT_BMSK                                                                                            0x10
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGCFGIRPT_SHFT                                                                                             0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_VFE_MMU_NSGCFGIRPT_BMSK                                                                                              0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_VFE_MMU_NSGCFGIRPT_SHFT                                                                                              0x3
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_CPP_MMU_NSGCFGIRPT_BMSK                                                                                              0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_CPP_MMU_NSGCFGIRPT_SHFT                                                                                              0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_VENUS0_MMU_NSGCFGIRPT_BMSK                                                                                                 0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_VENUS0_MMU_NSGCFGIRPT_SHFT                                                                                                 0x1
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_OXILI_MMU_NSGCFGIRPT_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_OXILI_MMU_NSGCFGIRPT_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00003008)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RMSK                                                                                                                  0x7f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RESERVEBIT_4_BMSK                                                                                                     0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RESERVEBIT_4_SHFT                                                                                                      0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_IPA_VMIDMT_NSGCFGIRPT_BMSK                                                                                            0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_IPA_VMIDMT_NSGCFGIRPT_SHFT                                                                                             0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_NSGCFGIRPT_BMSK                                                                              0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_NSGCFGIRPT_SHFT                                                                               0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_NSGCFGIRPT_BMSK                                                                               0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_NSGCFGIRPT_SHFT                                                                               0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_CRYPTO2_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                                                                     0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_CRYPTO2_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                                                                     0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RESERVEBIT_3_BMSK                                                                                                      0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_RESERVEBIT_3_SHFT                                                                                                      0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_DEHR_VMIDMT_NSGCFGIRPT_BMSK                                                                                            0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_DEHR_VMIDMT_NSGCFGIRPT_SHFT                                                                                            0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00003040)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK                                                                                                      0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT1_ENABLE_BMSK                                                                0x80000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT1_ENABLE_SHFT                                                                      0x1f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT0_ENABLE_BMSK                                                                0x40000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT0_ENABLE_SHFT                                                                      0x1e
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                    0x20000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                          0x1d
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_NSGCFG_IRQ_ENABLE_BMSK                                                                   0x10000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_NSGCFG_IRQ_ENABLE_SHFT                                                                         0x1c
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSGCFG_IRQ_ENABLE_BMSK                                                                          0x8000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSGCFG_IRQ_ENABLE_SHFT                                                                               0x1b
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                0x4000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                     0x1a
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CAMSS_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                        0x2000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CAMSS_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                             0x19
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                                                                  0x1000000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                                                                       0x18
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                                                                   0x800000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                                                                       0x17
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_6_BMSK                                                                                           0x400000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_6_SHFT                                                                                               0x16
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_USB1_HS_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                       0x200000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_USB1_HS_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                           0x15
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                      0x100000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                          0x14
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC4_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x80000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC4_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0x13
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC3_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x40000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC3_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0x12
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC2_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x20000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC2_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0x11
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC1_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x10000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SDC1_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x8000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0xf
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                           0x4000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0xe
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                         0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                            0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGCFGIRPT_ENABLE_BMSK                                                                              0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGCFGIRPT_ENABLE_SHFT                                                                                 0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                         0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                           0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                       0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                         0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_WCSS_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                             0x200
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_WCSS_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                               0x9
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_USB30_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                                                                        0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_USB30_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                                                                          0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                       0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                        0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_12_ENABLE_BMSK                                                                           0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_12_ENABLE_SHFT                                                                            0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_5_BMSK                                                                                               0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_5_SHFT                                                                                                0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                               0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                                0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                         0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                         0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_4_ENABLE_BMSK                                                                             0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_4_ENABLE_SHFT                                                                             0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                         0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                         0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_MSS_VMIDMT_CFG_SUMMARY_INTR_ENABLE_BMSK                                                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_MSS_VMIDMT_CFG_SUMMARY_INTR_ENABLE_SHFT                                                                          0x0

#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00003044)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RMSK                                                                                                              0x7ff
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_2_BMSK                                                                                                 0x400
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_2_SHFT                                                                                                   0xa
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_1_BMSK                                                                                                 0x200
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_1_SHFT                                                                                                   0x9
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_FD_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                     0x100
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_FD_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                       0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RICA_CORE_MMU_NSGCFGIRPT_IRQ_ENABLE_BMSK                                                                           0x80
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RICA_CORE_MMU_NSGCFGIRPT_IRQ_ENABLE_SHFT                                                                            0x7
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_0_BMSK                                                                                                  0x40
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RESERVEBIT_0_SHFT                                                                                                   0x6
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                              0x10
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                               0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                0x3
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OXILI_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OXILI_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00003048)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RMSK                                                                                                           0x7f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RESERVEBIT_9_BMSK                                                                                              0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RESERVEBIT_9_SHFT                                                                                               0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_IPA_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                              0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_IPA_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                               0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                 0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                 0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                 0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                                                                       0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RESERVEBIT_8_BMSK                                                                                               0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_RESERVEBIT_8_SHFT                                                                                               0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_DEHR_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                              0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR1_ENABLE_DEHR_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                              0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00004000)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MPM_QDSS_XPU2_SECURE_INTR_BMSK                                                                                            0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MPM_QDSS_XPU2_SECURE_INTR_SHFT                                                                                                  0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DDR_PHY_CFG_XPU2_SECURE_INTR_BMSK                                                                                         0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DDR_PHY_CFG_XPU2_SECURE_INTR_SHFT                                                                                               0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_NOC_CFG_XPU2_SECURE_INTR_BMSK                                                                                             0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_NOC_CFG_XPU2_SECURE_INTR_SHFT                                                                                                   0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                               0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                     0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_SECURE_INTR_BMSK                                                                                                  0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_SECURE_INTR_SHFT                                                                                                       0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPDM_XPU_SECURE_INTR_BMSK                                                                                                  0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPDM_XPU_SECURE_INTR_SHFT                                                                                                       0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPMI_RPU_SECURE_INTR_BMSK                                                                                                  0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPMI_RPU_SECURE_INTR_SHFT                                                                                                       0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_APU_SECURE_INTR_BMSK                                                                                              0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_APU_SECURE_INTR_SHFT                                                                                                   0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_RPU_SECURE_INTR_BMSK                                                                                                 0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_RPU_SECURE_INTR_SHFT                                                                                                     0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH1_XPU2_S_INTERRUPT_BMSK                                                                                              0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH1_XPU2_S_INTERRUPT_SHFT                                                                                                  0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_S_INTERRUPT_BMSK                                                                                              0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_S_INTERRUPT_SHFT                                                                                                  0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CFG_XPU2_S_INTERRUPT_BMSK                                                                                              0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CFG_XPU2_S_INTERRUPT_SHFT                                                                                                  0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SECURE_INTR_BMSK                                                                                                     0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SECURE_INTR_SHFT                                                                                                        0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_APU_SEC_INTR_BMSK                                                                                                        0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_APU_SEC_INTR_SHFT                                                                                                           0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MSG_RAM_XPU_SEC_INTR_BMSK                                                                                                    0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MSG_RAM_XPU_SEC_INTR_SHFT                                                                                                       0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_WCSS_APU_SECURE_IRQ_BMSK                                                                                                     0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_WCSS_APU_SECURE_IRQ_SHFT                                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_APP_XPU2_S_INTR_BMSK                                                                                                     0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_APP_XPU2_S_INTR_SHFT                                                                                                        0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_APCS_SYSKSMPUSECUREINT_BMSK                                                                                                   0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_APCS_SYSKSMPUSECUREINT_SHFT                                                                                                      0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_COPSS_APU_SECURE_IRQ_BMSK                                                                                                     0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_COPSS_APU_SECURE_IRQ_SHFT                                                                                                        0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_USB30_XPU_CONF_SECURE_IRQ_BMSK                                                                                                0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_USB30_XPU_CONF_SECURE_IRQ_SHFT                                                                                                   0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSWSYS_MPU_SECURE_INTR_BMSK                                                                                              0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSWSYS_MPU_SECURE_INTR_SHFT                                                                                                0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSW1_MPU_SECURE_INTR_BMSK                                                                                                0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSW1_MPU_SECURE_INTR_SHFT                                                                                                  0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSW0_MPU_SECURE_INTR_BMSK                                                                                                0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_OSW0_MPU_SECURE_INTR_SHFT                                                                                                  0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_DM_RPU_SECURE_INTR_BMSK                                                                                                  0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_DM_RPU_SECURE_INTR_SHFT                                                                                                    0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_RPU_SECURE_INTR_BMSK                                                                                                      0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCMEM_RPU_SECURE_INTR_SHFT                                                                                                       0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MDSS_VBIF_XPU2_SECURE_INTR_BMSK                                                                                                 0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MDSS_VBIF_XPU2_SECURE_INTR_SHFT                                                                                                  0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CAMSS_JPEG_VBIF_XPU2_SECURE_INTR_BMSK                                                                                           0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CAMSS_JPEG_VBIF_XPU2_SECURE_INTR_SHFT                                                                                            0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CAMSS_VFE_VBIF_XPU2_SECURE_INTR_BMSK                                                                                            0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CAMSS_VFE_VBIF_XPU2_SECURE_INTR_SHFT                                                                                             0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MMCC_XPU2_SECURE_INTR_BMSK                                                                                                       0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MMCC_XPU2_SECURE_INTR_SHFT                                                                                                       0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_VENUS0_XPU2_SECURE_INTR_BMSK                                                                                                     0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_VENUS0_XPU2_SECURE_INTR_SHFT                                                                                                     0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OXILI_XPU2_SECURE_INTR_BMSK                                                                                                      0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OXILI_XPU2_SECURE_INTR_SHFT                                                                                                      0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MSS_XPU2_SEC_INTR_BMSK                                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MSS_XPU2_SEC_INTR_SHFT                                                                                                           0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00004004)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RPM_MPU_SEC_INTR_BMSK                                                                                                     0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RPM_MPU_SEC_INTR_SHFT                                                                                                           0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCIMEM_MPU_SECURE_INTR_BMSK                                                                                               0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCIMEM_MPU_SECURE_INTR_SHFT                                                                                                     0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SEC_CTRL_XPU2_SEC_IRQ_BMSK                                                                                                0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SEC_CTRL_XPU2_SEC_IRQ_SHFT                                                                                                      0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                               0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                     0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_DEHR_XPU_SECURE_INTR_BMSK                                                                                                  0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_DEHR_XPU_SECURE_INTR_SHFT                                                                                                       0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_VENUS0_VBIF_XPU2_SECURE_INTR_BMSK                                                                                          0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_VENUS0_VBIF_XPU2_SECURE_INTR_SHFT                                                                                               0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OXILI_VBIF_XPU2_SECURE_INTR_BMSK                                                                                           0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OXILI_VBIF_XPU2_SECURE_INTR_SHFT                                                                                                0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MDSS_XPU2_SECURE_INTR_BMSK                                                                                                 0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MDSS_XPU2_SECURE_INTR_SHFT                                                                                                      0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_QDSS_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                             0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_QDSS_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                                 0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MPM_XPU2_SECURE_INTR_BMSK                                                                                                   0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MPM_XPU2_SECURE_INTR_SHFT                                                                                                       0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BOOT_ROM_SECURE_INTR_BMSK                                                                                                   0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BOOT_ROM_SECURE_INTR_SHFT                                                                                                       0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_GFX_RBCPR_XPU2_SECURE_INTR_BMSK                                                                                        0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_GFX_RBCPR_XPU2_SECURE_INTR_SHFT                                                                                            0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_MISC_XPU2_SECURE_INTR_BMSK                                                                                              0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_MISC_XPU2_SECURE_INTR_SHFT                                                                                                 0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_PCIE20_1_INT_PARF_XPU2_SECURE_INTR_BMSK                                                                                      0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_PCIE20_1_INT_PARF_XPU2_SECURE_INTR_SHFT                                                                                         0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_NOC_XPU2_SECURE_INTR_BMSK                                                                                               0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MMSS_NOC_XPU2_SECURE_INTR_SHFT                                                                                                  0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCMEM_OSWDM_MPU_SECURE_INTR_BMSK                                                                                             0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCMEM_OSWDM_MPU_SECURE_INTR_SHFT                                                                                                0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CAMSS_XPU2_SECURE_INTR_BMSK                                                                                                   0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CAMSS_XPU2_SECURE_INTR_SHFT                                                                                                      0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CRYPTO1_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                            0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CRYPTO1_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                               0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                            0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                               0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_O_TCSR_MUTEX_SECURE_INTR_BMSK                                                                                                 0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_O_TCSR_MUTEX_SECURE_INTR_SHFT                                                                                                    0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_O_TCSR_REGS_SECURE_INTR_BMSK                                                                                                   0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_O_TCSR_REGS_SECURE_INTR_SHFT                                                                                                     0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_COPSS_MPU_SECURE_IRQ_BMSK                                                                                                      0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_COPSS_MPU_SECURE_IRQ_SHFT                                                                                                        0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RESERVEBIT_BMSK                                                                                                                0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RESERVEBIT_SHFT                                                                                                                  0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_USB1_HS_APU_SEC_ERROR_IRQ_BMSK                                                                                                 0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_USB1_HS_APU_SEC_ERROR_IRQ_SHFT                                                                                                   0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC4_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC4_APU_SEC_ERROR_IRQ_SHFT                                                                                                      0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC3_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC3_APU_SEC_ERROR_IRQ_SHFT                                                                                                      0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC2_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC2_APU_SEC_ERROR_IRQ_SHFT                                                                                                      0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC1_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDC1_APU_SEC_ERROR_IRQ_SHFT                                                                                                      0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_TSIF_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                                  0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_TSIF_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                                  0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP2_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP2_APU_SEC_ERROR_IRQ_SHFT                                                                                                     0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP1_APU_SEC_ERROR_IRQ_BMSK                                                                                                     0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP1_APU_SEC_ERROR_IRQ_SHFT                                                                                                     0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BAM_DMA_APU_SEC_ERROR_IRQ_BMSK                                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BAM_DMA_APU_SEC_ERROR_IRQ_SHFT                                                                                                   0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00004008)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RMSK                                                                                                                          0x3fff
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR2_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR2_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR2_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RESERVEBIT_2_BMSK                                                                                                             0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RESERVEBIT_2_SHFT                                                                                                                0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_FD_VBIF_XPU_SECURE_INTR_BMSK                                                                                                  0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_FD_VBIF_XPU_SECURE_INTR_SHFT                                                                                                     0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_FD_XPU_SECURE_INTR_BMSK                                                                                                        0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_FD_XPU_SECURE_INTR_SHFT                                                                                                          0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_UFS_XPU_SECURE_INTR_BMSK                                                                                                       0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_UFS_XPU_SECURE_INTR_SHFT                                                                                                         0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_CAMSS_CPP_VBIF_XPU2_SECURE_INTR_BMSK                                                                                           0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_CAMSS_CPP_VBIF_XPU2_SECURE_INTR_SHFT                                                                                             0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RICA_CORE_TOP_XPU2_SECURE_IRQ_BMSK                                                                                             0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RICA_CORE_TOP_XPU2_SECURE_IRQ_SHFT                                                                                               0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RICA_CORE_VBIF_XPU2_SECURE_IRQ_BMSK                                                                                             0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RICA_CORE_VBIF_XPU2_SECURE_IRQ_SHFT                                                                                              0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_IPA_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                                  0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_IPA_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                                   0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RESERVEBIT_0_BMSK                                                                                                               0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_RESERVEBIT_0_SHFT                                                                                                                0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_UFS_ICE_XPU2_SECURE_INTR_BMSK                                                                                                   0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_UFS_ICE_XPU2_SECURE_INTR_SHFT                                                                                                    0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_1_INT_SLV_XPU2_SECURE_INTR_BMSK                                                                                           0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_1_INT_SLV_XPU2_SECURE_INTR_SHFT                                                                                           0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_CRYPTO2_BAM_APU_SEC_ERROR_IRQ_BMSK                                                                                               0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_CRYPTO2_BAM_APU_SEC_ERROR_IRQ_SHFT                                                                                               0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_0_INT_SLV_XPU2_SECURE_INTR_BMSK                                                                                           0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_0_INT_SLV_XPU2_SECURE_INTR_SHFT                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_0_INT_PARF_XPU2_SECURE_INTR_BMSK                                                                                          0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_PCIE20_0_INT_PARF_XPU2_SECURE_INTR_SHFT                                                                                          0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00004040)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MPM_QDSS_XPU2_SECURE_INTR_ENABLE_BMSK                                                                              0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MPM_QDSS_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                    0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DDR_PHY_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                                                                           0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DDR_PHY_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                 0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_NOC_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                                                                               0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_NOC_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                     0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_SECURE_INTR_ENABLE_SHFT                                                                                         0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPDM_XPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPDM_XPU_SECURE_INTR_ENABLE_SHFT                                                                                         0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPMI_RPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPMI_RPU_SECURE_INTR_ENABLE_SHFT                                                                                         0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_APU_SECURE_INTR_ENABLE_BMSK                                                                                0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_APU_SECURE_INTR_ENABLE_SHFT                                                                                     0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_RPU_SECURE_INTR_ENABLE_BMSK                                                                                   0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_RPU_SECURE_INTR_ENABLE_SHFT                                                                                       0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_S_INTERRUPT_BMSK                                                                                       0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_S_INTERRUPT_SHFT                                                                                           0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_S_INTERRUPT_BMSK                                                                                       0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_S_INTERRUPT_SHFT                                                                                           0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_S_INTERRUPT_BMSK                                                                                       0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_S_INTERRUPT_SHFT                                                                                           0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SECURE_INTR_ENABLE_BMSK                                                                                       0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SECURE_INTR_ENABLE_SHFT                                                                                          0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_APU_SEC_INTR_ENABLE_BMSK                                                                                          0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_APU_SEC_INTR_ENABLE_SHFT                                                                                             0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MSG_RAM_XPU_SEC_INTR_ENABLE_BMSK                                                                                      0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MSG_RAM_XPU_SEC_INTR_ENABLE_SHFT                                                                                         0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_WCSS_APU_SECURE_IRQ_ENABLE_BMSK                                                                                       0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_WCSS_APU_SECURE_IRQ_ENABLE_SHFT                                                                                          0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_APP_XPU2_S_INTR_ENABLE_BMSK                                                                                       0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_APP_XPU2_S_INTR_ENABLE_SHFT                                                                                          0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_APCS_SYSKSMPUSECUREINT_ENABLE_BMSK                                                                                     0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_APCS_SYSKSMPUSECUREINT_ENABLE_SHFT                                                                                        0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_COPSS_APU_SECURE_IRQ_ENABLE_BMSK                                                                                       0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_COPSS_APU_SECURE_IRQ_ENABLE_SHFT                                                                                          0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_USB30_XPU_CONF_SECURE_IRQ_ENABLE_BMSK                                                                                  0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_USB30_XPU_CONF_SECURE_IRQ_ENABLE_SHFT                                                                                     0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSWSYS_MPU_SECURE_INTR_ENABLE_BMSK                                                                                0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSWSYS_MPU_SECURE_INTR_ENABLE_SHFT                                                                                  0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSW1_MPU_SECURE_INTR_ENABLE_BMSK                                                                                  0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSW1_MPU_SECURE_INTR_ENABLE_SHFT                                                                                    0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSW0_MPU_SECURE_INTR_ENABLE_BMSK                                                                                  0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_OSW0_MPU_SECURE_INTR_ENABLE_SHFT                                                                                    0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_DM_RPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_DM_RPU_SECURE_INTR_ENABLE_SHFT                                                                                      0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_RPU_SECURE_INTR_ENABLE_BMSK                                                                                        0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCMEM_RPU_SECURE_INTR_ENABLE_SHFT                                                                                         0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MDSS_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                   0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MDSS_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                    0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                             0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                              0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                              0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                               0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MMCC_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                         0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MMCC_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                         0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_VENUS0_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                       0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_VENUS0_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                       0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OXILI_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                        0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OXILI_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                        0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MSS_XPU2_SEC_INTR_ENABLE_BMSK                                                                                             0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MSS_XPU2_SEC_INTR_ENABLE_SHFT                                                                                             0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00004044)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RPM_MPU_SEC_INTR_ENABLE_BMSK                                                                                       0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RPM_MPU_SEC_INTR_ENABLE_SHFT                                                                                             0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCIMEM_MPU_SECURE_INTR_ENABLE_BMSK                                                                                 0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCIMEM_MPU_SECURE_INTR_ENABLE_SHFT                                                                                       0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SEC_CTRL_XPU2_SEC_IRQ_ENABLE_BMSK                                                                                  0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SEC_CTRL_XPU2_SEC_IRQ_ENABLE_SHFT                                                                                        0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                        0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                              0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_DEHR_XPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_DEHR_XPU_SECURE_INTR_ENABLE_SHFT                                                                                         0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_VENUS0_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                            0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_VENUS0_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                 0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OXILI_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                             0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OXILI_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                  0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MDSS_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                   0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MDSS_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                        0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_QDSS_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                               0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_QDSS_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                   0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MPM_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                     0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MPM_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                         0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BOOT_ROM_SECURE_INTR_ENABLE_BMSK                                                                                     0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BOOT_ROM_SECURE_INTR_ENABLE_SHFT                                                                                         0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_SECURE_INTR_ENABLE_BMSK                                                                          0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_SECURE_INTR_ENABLE_SHFT                                                                              0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_MISC_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_MISC_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                   0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                        0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                           0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_NOC_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                 0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MMSS_NOC_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                    0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCMEM_OSWDM_MPU_SECURE_INTR_ENABLE_BMSK                                                                               0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCMEM_OSWDM_MPU_SECURE_INTR_ENABLE_SHFT                                                                                  0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CAMSS_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                     0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CAMSS_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                        0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CRYPTO1_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                              0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CRYPTO1_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                 0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                              0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                 0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_O_TCSR_MUTEX_SECURE_INTR_ENABLE_BMSK                                                                                   0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_O_TCSR_MUTEX_SECURE_INTR_ENABLE_SHFT                                                                                      0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_O_TCSR_REGS_SECURE_INTR_ENABLE_BMSK                                                                                     0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_O_TCSR_REGS_SECURE_INTR_ENABLE_SHFT                                                                                       0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_COPSS_MPU_SECURE_IRQ_ENABLE_BMSK                                                                                        0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_COPSS_MPU_SECURE_IRQ_ENABLE_SHFT                                                                                          0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RESERVEBIT_BMSK                                                                                                         0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RESERVEBIT_SHFT                                                                                                           0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_USB1_HS_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                   0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_USB1_HS_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                     0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC4_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC4_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                        0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC3_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC3_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                        0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC2_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC2_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                        0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC1_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDC1_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                        0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_TSIF_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                    0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_TSIF_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                    0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP2_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP2_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                       0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP1_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                       0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP1_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                       0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BAM_DMA_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                     0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BAM_DMA_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                     0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00004048)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RMSK                                                                                                                   0x3fff
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RESERVEBIT_2_BMSK                                                                                                      0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RESERVEBIT_2_SHFT                                                                                                         0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_FD_VBIF_XPU_SECURE_INTR_ENABLE_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_FD_VBIF_XPU_SECURE_INTR_ENABLE_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_FD_XPU_SECURE_INTR_ENABLE_BMSK                                                                                          0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_FD_XPU_SECURE_INTR_ENABLE_SHFT                                                                                            0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_UFS_XPU_SECURE_INTR_ENABLE_BMSK                                                                                         0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_UFS_XPU_SECURE_INTR_ENABLE_SHFT                                                                                           0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                             0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                               0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RICA_CORE_TOP_XPU2_SECURE_IRQ_ENABLE_BMSK                                                                               0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RICA_CORE_TOP_XPU2_SECURE_IRQ_ENABLE_SHFT                                                                                 0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_SECURE_IRQ_ENABLE_BMSK                                                                               0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_SECURE_IRQ_ENABLE_SHFT                                                                                0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_IPA_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                    0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_IPA_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                     0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RESERVEBIT_0_BMSK                                                                                                        0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_RESERVEBIT_0_SHFT                                                                                                         0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_UFS_ICE_XPU2_SECURE_INTR_ENABLE_BMSK                                                                                     0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_UFS_ICE_XPU2_SECURE_INTR_ENABLE_SHFT                                                                                      0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_SECURE_INTR_ENABLE_BMSK                                                                             0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_SECURE_INTR_ENABLE_SHFT                                                                             0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_CRYPTO2_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                                                                 0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_CRYPTO2_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                                                                 0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_SECURE_INTR_ENABLE_BMSK                                                                             0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_SECURE_INTR_ENABLE_SHFT                                                                             0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_SECURE_INTR_ENABLE_BMSK                                                                            0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_SECURE_INTR_ENABLE_SHFT                                                                            0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00004010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT1_BMSK                                                                                    0x80000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT1_SHFT                                                                                          0x1f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT0_BMSK                                                                                    0x40000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT0_SHFT                                                                                          0x1e
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_BAM_VMIDMT_GIRPT_BMSK                                                                                        0x20000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_BAM_VMIDMT_GIRPT_SHFT                                                                                              0x1d
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RICA_CORE_VMIDMT_G_IRQ_BMSK                                                                                       0x10000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RICA_CORE_VMIDMT_G_IRQ_SHFT                                                                                             0x1c
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_SECG_IRQ_BMSK                                                                                           0x8000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_SECG_IRQ_SHFT                                                                                                0x1b
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_GIRPT_BMSK                                                                                    0x4000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_GIRPT_SHFT                                                                                         0x1a
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CAMSS_VMIDMT_GIRPT_BMSK                                                                                            0x2000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CAMSS_VMIDMT_GIRPT_SHFT                                                                                                 0x19
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO1_VMIDMT_GIRPT_IRQ_BMSK                                                                                      0x1000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO1_VMIDMT_GIRPT_IRQ_SHFT                                                                                           0x18
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_IRQ_BMSK                                                                                       0x800000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_IRQ_SHFT                                                                                           0x17
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                   0x400000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                       0x16
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_USB1_HS_VMIDMT_GIRPT_BMSK                                                                                           0x200000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_USB1_HS_VMIDMT_GIRPT_SHFT                                                                                               0x15
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_TSIF_BAM_VMIDMT_GIRPT_BMSK                                                                                          0x100000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_TSIF_BAM_VMIDMT_GIRPT_SHFT                                                                                              0x14
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC4_VMIDMT_GIRPT_BMSK                                                                                               0x80000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC4_VMIDMT_GIRPT_SHFT                                                                                                  0x13
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC3_VMIDMT_GIRPT_BMSK                                                                                               0x40000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC3_VMIDMT_GIRPT_SHFT                                                                                                  0x12
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC2_VMIDMT_GIRPT_BMSK                                                                                               0x20000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC2_VMIDMT_GIRPT_SHFT                                                                                                  0x11
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC1_VMIDMT_GIRPT_BMSK                                                                                               0x10000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SDC1_VMIDMT_GIRPT_SHFT                                                                                                  0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP2_VMIDMT_GIRPT_BMSK                                                                                               0x8000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP2_VMIDMT_GIRPT_SHFT                                                                                                  0xf
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_BMSK                                                                                               0x4000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_SHFT                                                                                                  0xe
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BAM_DMA_VMIDMT_GIRPT_BMSK                                                                                             0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BAM_DMA_VMIDMT_GIRPT_SHFT                                                                                                0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SPDM_VMID_GIRPT_BMSK                                                                                                  0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SPDM_VMID_GIRPT_SHFT                                                                                                     0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_DAP_VMIDMT_GIRPT_BMSK                                                                                             0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_DAP_VMIDMT_GIRPT_SHFT                                                                                               0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_TRACE_VMIDMT_GIRPT_BMSK                                                                                           0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_TRACE_VMIDMT_GIRPT_SHFT                                                                                             0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_WCSS_VMIDMT_GIRPT_BMSK                                                                                                 0x200
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_WCSS_VMIDMT_GIRPT_SHFT                                                                                                   0x9
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_USB30_VMIDMT_GIRPT_IRQ_BMSK                                                                                            0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_USB30_VMIDMT_GIRPT_IRQ_SHFT                                                                                              0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OCMEM_DMDDR_VMIDMT_GIRPT_BMSK                                                                                           0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OCMEM_DMDDR_VMIDMT_GIRPT_SHFT                                                                                            0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_LPASS_IRQ_OUT_SECURITY_15_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_LPASS_IRQ_OUT_SECURITY_15_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                       0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                        0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_UFS_VMIDMT_GIRPT_BMSK                                                                                                   0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_UFS_VMIDMT_GIRPT_SHFT                                                                                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_CPU_VMIDMT_GIRPT_BMSK                                                                                             0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_CPU_VMIDMT_GIRPT_SHFT                                                                                             0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_LPASS_IRQ_OUT_SECURITY_7_BMSK                                                                                            0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_LPASS_IRQ_OUT_SECURITY_7_SHFT                                                                                            0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OXILI_RBBM_VMIDMT_GIRPT_BMSK                                                                                             0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_OXILI_RBBM_VMIDMT_GIRPT_SHFT                                                                                             0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_MSS_VMIDMT_CLIENT_SEC_SUMMARY_INTR_BMSK                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_MSS_VMIDMT_CLIENT_SEC_SUMMARY_INTR_SHFT                                                                                  0x0

#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00004014)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RMSK                                                                                                                      0x7ff
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                         0x400
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                           0xa
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                         0x200
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                           0x9
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_FD_MMU_G_IRQ_BMSK                                                                                                         0x100
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_FD_MMU_G_IRQ_SHFT                                                                                                           0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RICA_CORE_MMU_G_IRQ_BMSK                                                                                                   0x80
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RICA_CORE_MMU_G_IRQ_SHFT                                                                                                    0x7
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_0_BMSK                                                                                                          0x40
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RESERVEBIT_0_SHFT                                                                                                           0x6
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_MMU_GIRPT_BMSK                                                                                                        0x20
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_MMU_GIRPT_SHFT                                                                                                         0x5
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_JPEG_MMU_GIRPT_BMSK                                                                                                  0x10
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_JPEG_MMU_GIRPT_SHFT                                                                                                   0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_VFE_MMU_GIRPT_BMSK                                                                                                    0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_VFE_MMU_GIRPT_SHFT                                                                                                    0x3
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_CPP_MMU_GIRPT_BMSK                                                                                                    0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_CPP_MMU_GIRPT_SHFT                                                                                                    0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_VENUS0_MMU_GIRPT_BMSK                                                                                                       0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_VENUS0_MMU_GIRPT_SHFT                                                                                                       0x1
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_OXILI_MMU_GIRPT_BMSK                                                                                                        0x1
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_OXILI_MMU_GIRPT_SHFT                                                                                                        0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00004018)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RMSK                                                                                                                   0x7f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RESERVEBIT_5_BMSK                                                                                                      0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RESERVEBIT_5_SHFT                                                                                                       0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_IPA_VMIDMT_GIRPT_BMSK                                                                                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_IPA_VMIDMT_GIRPT_SHFT                                                                                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_GIRPT_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_GIRPT_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_GIRPT_BMSK                                                                                     0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_GIRPT_SHFT                                                                                     0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_CRYPTO2_VMIDMT_GIRPT_IRQ_BMSK                                                                                           0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_CRYPTO2_VMIDMT_GIRPT_IRQ_SHFT                                                                                           0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RESERVEBIT_4_BMSK                                                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_RESERVEBIT_4_SHFT                                                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_DEHR_VMIDMT_GIRPT_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_DEHR_VMIDMT_GIRPT_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00004050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK                                                                                                       0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT1_ENABLE_BMSK                                                                      0x80000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT1_ENABLE_SHFT                                                                            0x1f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT0_ENABLE_BMSK                                                                      0x40000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT0_ENABLE_SHFT                                                                            0x1e
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GIRPT_ENABLE_BMSK                                                                          0x20000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GIRPT_ENABLE_SHFT                                                                                0x1d
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_G_IRQ_ENABLE_BMSK                                                                         0x10000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_G_IRQ_ENABLE_SHFT                                                                               0x1c
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_SECG_IRQ_ENABLE_BMSK                                                                             0x8000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_SECG_IRQ_ENABLE_SHFT                                                                                  0x1b
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_GIRPT_ENABLE_BMSK                                                                      0x4000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_GIRPT_ENABLE_SHFT                                                                           0x1a
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CAMSS_VMIDMT_GIRPT_ENABLE_BMSK                                                                              0x2000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CAMSS_VMIDMT_GIRPT_ENABLE_SHFT                                                                                   0x19
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                                                                        0x1000000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                                                                             0x18
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                                                                         0x800000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                                                                             0x17
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_7_BMSK                                                                                            0x400000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_7_SHFT                                                                                                0x16
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_USB1_HS_VMIDMT_GIRPT_ENABLE_BMSK                                                                             0x200000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_USB1_HS_VMIDMT_GIRPT_ENABLE_SHFT                                                                                 0x15
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_GIRPT_ENABLE_BMSK                                                                            0x100000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_GIRPT_ENABLE_SHFT                                                                                0x14
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC4_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x80000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC4_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0x13
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC3_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x40000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC3_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0x12
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC2_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x20000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC2_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0x11
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC1_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x10000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SDC1_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP2_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x8000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP2_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0xf
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_BMSK                                                                                 0x4000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0xe
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_GIRPT_ENABLE_BMSK                                                                               0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_GIRPT_ENABLE_SHFT                                                                                  0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SPDM_VMID_GIRPT_ENABLE_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SPDM_VMID_GIRPT_ENABLE_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GIRPT_ENABLE_BMSK                                                                               0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GIRPT_ENABLE_SHFT                                                                                 0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GIRPT_ENABLE_BMSK                                                                             0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GIRPT_ENABLE_SHFT                                                                               0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_WCSS_VMIDMT_GIRPT_ENABLE_BMSK                                                                                   0x200
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_WCSS_VMIDMT_GIRPT_ENABLE_SHFT                                                                                     0x9
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_USB30_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                                                                              0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_USB30_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                                                                                0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_GIRPT_ENABLE_BMSK                                                                             0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_GIRPT_ENABLE_SHFT                                                                              0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_15_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_15_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_6_BMSK                                                                                                0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_6_SHFT                                                                                                 0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_UFS_VMIDMT_GIRPT_ENABLE_BMSK                                                                                     0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_UFS_VMIDMT_GIRPT_ENABLE_SHFT                                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_GIRPT_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_GIRPT_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_7_ENABLE_BMSK                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_7_ENABLE_SHFT                                                                              0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_GIRPT_ENABLE_BMSK                                                                               0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_GIRPT_ENABLE_SHFT                                                                               0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_MSS_VMIDMT_CLIENT_SEC_SUMMARY_INTR_ENABLE_BMSK                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_MSS_VMIDMT_CLIENT_SEC_SUMMARY_INTR_ENABLE_SHFT                                                                    0x0

#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00004054)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RMSK                                                                                                               0x7ff
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_2_BMSK                                                                                                  0x400
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_2_SHFT                                                                                                    0xa
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_1_BMSK                                                                                                  0x200
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_1_SHFT                                                                                                    0x9
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_FD_MMU_GCFG_IRQ_ENABLE_BMSK                                                                                        0x100
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_FD_MMU_GCFG_IRQ_ENABLE_SHFT                                                                                          0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RICA_CORE_MMU_GCFG_IRQ_ENABLE_BMSK                                                                                  0x80
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RICA_CORE_MMU_GCFG_IRQ_ENABLE_SHFT                                                                                   0x7
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_0_BMSK                                                                                                   0x40
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RESERVEBIT_0_SHFT                                                                                                    0x6
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_MMU_GIRPT_ENABLE_BMSK                                                                                          0x20
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_MMU_GIRPT_ENABLE_SHFT                                                                                           0x5
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GIRPT_ENABLE_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GIRPT_ENABLE_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GIRPT_ENABLE_BMSK                                                                                      0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GIRPT_ENABLE_SHFT                                                                                      0x3
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GIRPT_ENABLE_BMSK                                                                                      0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GIRPT_ENABLE_SHFT                                                                                      0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_VENUS0_MMU_GIRPT_ENABLE_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_VENUS0_MMU_GIRPT_ENABLE_SHFT                                                                                         0x1
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OXILI_MMU_GIRPT_ENABLE_BMSK                                                                                          0x1
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OXILI_MMU_GIRPT_ENABLE_SHFT                                                                                          0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00004058)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RMSK                                                                                                            0x7f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RESERVEBIT_10_BMSK                                                                                              0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RESERVEBIT_10_SHFT                                                                                               0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_IPA_VMIDMT_GIRPT_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_IPA_VMIDMT_GIRPT_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_GIRPT_ENABLE_BMSK                                                                      0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_GIRPT_ENABLE_SHFT                                                                       0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_GIRPT_ENABLE_BMSK                                                                       0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_GIRPT_ENABLE_SHFT                                                                       0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                                                                             0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                                                                             0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RESERVEBIT_9_BMSK                                                                                                0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_RESERVEBIT_9_SHFT                                                                                                0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_DEHR_VMIDMT_GIRPT_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR1_ENABLE_DEHR_VMIDMT_GIRPT_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00005000)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT1_BMSK                                                                                    0x80000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT1_SHFT                                                                                          0x1f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT0_BMSK                                                                                    0x40000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT0_SHFT                                                                                          0x1e
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_BAM_VMIDMT_GCFGIRPT_BMSK                                                                                        0x20000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_BAM_VMIDMT_GCFGIRPT_SHFT                                                                                              0x1d
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RICA_CORE_VMIDMT_GCFG_IRQ_BMSK                                                                                       0x10000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RICA_CORE_VMIDMT_GCFG_IRQ_SHFT                                                                                             0x1c
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_SECGCFG_IRQ_BMSK                                                                                           0x8000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_SECGCFG_IRQ_SHFT                                                                                                0x1b
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_GCFGIRPT_BMSK                                                                                    0x4000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OCMEM_DMOCMEM_VMIDMT_GCFGIRPT_SHFT                                                                                         0x1a
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CAMSS_VMIDMT_GCFGIRPT_BMSK                                                                                            0x2000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CAMSS_VMIDMT_GCFGIRPT_SHFT                                                                                                 0x19
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO1_VMIDMT_GCFGIRPT_IRQ_BMSK                                                                                      0x1000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO1_VMIDMT_GCFGIRPT_IRQ_SHFT                                                                                           0x18
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_BMSK                                                                                       0x800000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_SHFT                                                                                           0x17
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                      0x400000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                          0x16
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_USB1_HS_VMIDMT_GCFGIRPT_BMSK                                                                                           0x200000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_USB1_HS_VMIDMT_GCFGIRPT_SHFT                                                                                               0x15
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_TSIF_BAM_VMIDMT_GCFGIRPT_BMSK                                                                                          0x100000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_TSIF_BAM_VMIDMT_GCFGIRPT_SHFT                                                                                              0x14
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC4_VMIDMT_GCFGIRPT_BMSK                                                                                               0x80000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC4_VMIDMT_GCFGIRPT_SHFT                                                                                                  0x13
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC3_VMIDMT_GCFGIRPT_BMSK                                                                                               0x40000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC3_VMIDMT_GCFGIRPT_SHFT                                                                                                  0x12
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC2_VMIDMT_GCFGIRPT_BMSK                                                                                               0x20000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC2_VMIDMT_GCFGIRPT_SHFT                                                                                                  0x11
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC1_VMIDMT_GCFGIRPT_BMSK                                                                                               0x10000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SDC1_VMIDMT_GCFGIRPT_SHFT                                                                                                  0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP2_VMIDMT_GCFGIRPT_BMSK                                                                                               0x8000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP2_VMIDMT_GCFGIRPT_SHFT                                                                                                  0xf
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_BMSK                                                                                               0x4000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_SHFT                                                                                                  0xe
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BAM_DMA_VMIDMT_GCFGIRPT_BMSK                                                                                             0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BAM_DMA_VMIDMT_GCFGIRPT_SHFT                                                                                                0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SPDM_VMID_GCFGIRPT_BMSK                                                                                                  0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SPDM_VMID_GCFGIRPT_SHFT                                                                                                     0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_DAP_VMIDMT_GCFGIRPT_BMSK                                                                                             0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_DAP_VMIDMT_GCFGIRPT_SHFT                                                                                               0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_TRACE_VMIDMT_GCFGIRPT_BMSK                                                                                           0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_TRACE_VMIDMT_GCFGIRPT_SHFT                                                                                             0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_WCSS_VMIDMT_GCFGIRPT_BMSK                                                                                                 0x200
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_WCSS_VMIDMT_GCFGIRPT_SHFT                                                                                                   0x9
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_USB30_VMIDMT_GCFGIRPT_IRQ_BMSK                                                                                            0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_USB30_VMIDMT_GCFGIRPT_IRQ_SHFT                                                                                              0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OCMEM_DMDDR_VMIDMT_GCFGIRPT_BMSK                                                                                           0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OCMEM_DMDDR_VMIDMT_GCFGIRPT_SHFT                                                                                            0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_LPASS_IRQ_OUT_SECURITY_14_BMSK                                                                                             0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_LPASS_IRQ_OUT_SECURITY_14_SHFT                                                                                              0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                          0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                           0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_UFS_VMIDMT_GCFGIRPT_BMSK                                                                                                   0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_UFS_VMIDMT_GCFGIRPT_SHFT                                                                                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_CPU_VMIDMT_GCFGIRPT_BMSK                                                                                             0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_CPU_VMIDMT_GCFGIRPT_SHFT                                                                                             0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_LPASS_IRQ_OUT_SECURITY_6_BMSK                                                                                               0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_LPASS_IRQ_OUT_SECURITY_6_SHFT                                                                                               0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OXILI_RBBM_VMIDMT_GCFGIRPT_BMSK                                                                                             0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_OXILI_RBBM_VMIDMT_GCFGIRPT_SHFT                                                                                             0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_MSS_VMIDMT_CFG_SECURE_SUMMARY_INTR_BMSK                                                                                     0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_MSS_VMIDMT_CFG_SECURE_SUMMARY_INTR_SHFT                                                                                     0x0

#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00005004)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RMSK                                                                                                                         0x7ff
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_2_BMSK                                                                                                            0x400
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_2_SHFT                                                                                                              0xa
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_1_BMSK                                                                                                            0x200
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_1_SHFT                                                                                                              0x9
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_FD_MMU_GCFG_IRQ_BMSK                                                                                                         0x100
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_FD_MMU_GCFG_IRQ_SHFT                                                                                                           0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RICA_CORE_MMU_GCFG_IRQ_BMSK                                                                                                   0x80
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RICA_CORE_MMU_GCFG_IRQ_SHFT                                                                                                    0x7
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_0_BMSK                                                                                                             0x40
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RESERVEBIT_0_SHFT                                                                                                              0x6
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_MMU_GCFGIRPT_BMSK                                                                                                        0x20
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_MMU_GCFGIRPT_SHFT                                                                                                         0x5
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_JPEG_MMU_GCFGIRPT_BMSK                                                                                                  0x10
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_JPEG_MMU_GCFGIRPT_SHFT                                                                                                   0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_VFE_MMU_GCFGIRPT_BMSK                                                                                                    0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_VFE_MMU_GCFGIRPT_SHFT                                                                                                    0x3
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_CPP_MMU_GCFGIRPT_BMSK                                                                                                    0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_CPP_MMU_GCFGIRPT_SHFT                                                                                                    0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_VENUS0_MMU_GCFGIRPT_BMSK                                                                                                       0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_VENUS0_MMU_GCFGIRPT_SHFT                                                                                                       0x1
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_OXILI_MMU_GCFGIRPT_BMSK                                                                                                        0x1
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_OXILI_MMU_GCFGIRPT_SHFT                                                                                                        0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00005008)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RMSK                                                                                                                      0x7f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RESERVEBIT_5_BMSK                                                                                                         0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RESERVEBIT_5_SHFT                                                                                                          0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_IPA_VMIDMT_GCFGIRPT_BMSK                                                                                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_IPA_VMIDMT_GCFGIRPT_SHFT                                                                                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_GCFGIRPT_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_PCIE20_1_INT_MSTR_VMIDMT_GCFGIRPT_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_GCFGIRPT_BMSK                                                                                     0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_PCIE20_0_INT_MSTR_VMIDMT_GCFGIRPT_SHFT                                                                                     0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_CRYPTO2_VMIDMT_GCFGIRPT_IRQ_BMSK                                                                                           0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_CRYPTO2_VMIDMT_GCFGIRPT_IRQ_SHFT                                                                                           0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RESERVEBIT_4_BMSK                                                                                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_RESERVEBIT_4_SHFT                                                                                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_DEHR_VMIDMT_GCFGIRPT_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_DEHR_VMIDMT_GCFGIRPT_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00005040)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT1_ENABLE_BMSK                                                                      0x80000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT1_ENABLE_SHFT                                                                            0x1f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT0_ENABLE_BMSK                                                                      0x40000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT0_ENABLE_SHFT                                                                            0x1e
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                          0x20000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                0x1d
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_GCFG_IRQ_ENABLE_BMSK                                                                         0x10000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RICA_CORE_VMIDMT_GCFG_IRQ_ENABLE_SHFT                                                                               0x1c
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_SECGCFG_IRQ_ENABLE_BMSK                                                                             0x8000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_SECGCFG_IRQ_ENABLE_SHFT                                                                                  0x1b
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                      0x4000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OCMEM_DMOCMEM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                           0x1a
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CAMSS_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                              0x2000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CAMSS_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                   0x19
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                                                                        0x1000000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO1_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                                                                             0x18
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                                                                         0x800000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                                                                             0x17
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RESERVEBIT_6_BMSK                                                                                               0x400000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RESERVEBIT_6_SHFT                                                                                                   0x16
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_USB1_HS_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                             0x200000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_USB1_HS_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                 0x15
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                            0x100000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_TSIF_BAM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                0x14
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC4_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x80000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC4_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0x13
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC3_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x40000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC3_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0x12
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC2_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x20000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC2_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0x11
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC1_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x10000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SDC1_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP2_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x8000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP2_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0xf
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                 0x4000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0xe
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                               0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BAM_DMA_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                  0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SPDM_VMID_GCFGIRPT_ENABLE_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SPDM_VMID_GCFGIRPT_ENABLE_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                               0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                 0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                             0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                               0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_WCSS_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                   0x200
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_WCSS_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                     0x9
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_USB30_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                                                                              0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_USB30_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                                                                                0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                             0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OCMEM_DMDDR_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                              0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_14_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_14_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RESERVEBIT_5_BMSK                                                                                                   0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RESERVEBIT_5_SHFT                                                                                                    0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_UFS_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                     0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_UFS_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_CPU_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_6_ENABLE_BMSK                                                                                 0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_LPASS_IRQ_OUT_SECURITY_6_ENABLE_SHFT                                                                                 0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                               0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OXILI_RBBM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                               0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_MSS_VMIDMT_CFG_SECURE_SUMMARY_INTR_ENABLE_BMSK                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_MSS_VMIDMT_CFG_SECURE_SUMMARY_INTR_ENABLE_SHFT                                                                       0x0

#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00005044)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RMSK                                                                                                                  0x7ff
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_2_BMSK                                                                                                     0x400
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_2_SHFT                                                                                                       0xa
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_1_BMSK                                                                                                     0x200
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_1_SHFT                                                                                                       0x9
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_FD_MMU_GCFG_IRQ_ENABLE_BMSK                                                                                           0x100
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_FD_MMU_GCFG_IRQ_ENABLE_SHFT                                                                                             0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RICA_CORE_MMU_GCFG_IRQ_ENABLE_BMSK                                                                                     0x80
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RICA_CORE_MMU_GCFG_IRQ_ENABLE_SHFT                                                                                      0x7
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_0_BMSK                                                                                                      0x40
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RESERVEBIT_0_SHFT                                                                                                       0x6
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_MMU_GCFGIRPT_ENABLE_BMSK                                                                                          0x20
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_MMU_GCFGIRPT_ENABLE_SHFT                                                                                           0x5
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GCFGIRPT_ENABLE_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GCFGIRPT_ENABLE_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GCFGIRPT_ENABLE_BMSK                                                                                      0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GCFGIRPT_ENABLE_SHFT                                                                                      0x3
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GCFGIRPT_ENABLE_BMSK                                                                                      0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GCFGIRPT_ENABLE_SHFT                                                                                      0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_VENUS0_MMU_GCFGIRPT_ENABLE_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_VENUS0_MMU_GCFGIRPT_ENABLE_SHFT                                                                                         0x1
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OXILI_MMU_GCFGIRPT_ENABLE_BMSK                                                                                          0x1
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OXILI_MMU_GCFGIRPT_ENABLE_SHFT                                                                                          0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00005048)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RMSK                                                                                                               0x7f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RESERVEBIT_9_BMSK                                                                                                  0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RESERVEBIT_9_SHFT                                                                                                   0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_IPA_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_IPA_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                      0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_PCIE20_1_INT_MSTR_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                       0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                       0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_PCIE20_0_INT_MSTR_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                       0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                                                                             0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_CRYPTO2_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                                                                             0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RESERVEBIT_8_BMSK                                                                                                   0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_RESERVEBIT_8_SHFT                                                                                                   0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_DEHR_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR1_ENABLE_DEHR_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00006000)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MPM_QDSS_XPU2_MSA_INTR_BMSK                                                                                               0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MPM_QDSS_XPU2_MSA_INTR_SHFT                                                                                                     0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DDR_PHY_CFG_XPU2_MSA_INTR_BMSK                                                                                            0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DDR_PHY_CFG_XPU2_MSA_INTR_SHFT                                                                                                  0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_NOC_CFG_XPU2_MSA_INTR_BMSK                                                                                                0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_NOC_CFG_XPU2_MSA_INTR_SHFT                                                                                                      0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                               0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                     0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_BMSK                                                                                                     0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_SHFT                                                                                                          0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPDM_XPU_MSA_INTR_BMSK                                                                                                     0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPDM_XPU_MSA_INTR_SHFT                                                                                                          0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPMI_RPU_MSA_INTR_BMSK                                                                                                     0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPMI_RPU_MSA_INTR_SHFT                                                                                                          0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_APU_MSA_INTR_BMSK                                                                                                 0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_APU_MSA_INTR_SHFT                                                                                                      0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_RPU_MSA_INTR_BMSK                                                                                                    0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_RPU_MSA_INTR_SHFT                                                                                                        0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH1_XPU2_MSA_INTERRUPT_BMSK                                                                                            0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH1_XPU2_MSA_INTERRUPT_SHFT                                                                                                0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_BMSK                                                                                            0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_SHFT                                                                                                0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CFG_XPU2_MSA_INTERRUPT_BMSK                                                                                            0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CFG_XPU2_MSA_INTERRUPT_SHFT                                                                                                0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_BMSK                                                                                                        0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_SHFT                                                                                                           0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_APU_MSA_INTR_BMSK                                                                                                        0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_APU_MSA_INTR_SHFT                                                                                                           0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MSG_RAM_XPU_MSA_INTR_BMSK                                                                                                    0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MSG_RAM_XPU_MSA_INTR_SHFT                                                                                                       0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_WCSS_APU_MSA_INTR_BMSK                                                                                                       0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_WCSS_APU_MSA_INTR_SHFT                                                                                                          0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_APP_XPU2_MSA_INTR_BMSK                                                                                                   0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_APP_XPU2_MSA_INTR_SHFT                                                                                                      0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_APCS_SYSKSMPUMSAINT_BMSK                                                                                                      0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_APCS_SYSKSMPUMSAINT_SHFT                                                                                                         0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_COPSS_APU_MSA_IRQ_BMSK                                                                                                        0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_COPSS_APU_MSA_IRQ_SHFT                                                                                                           0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_USB30_XPU2_MSA_IRQ_BMSK                                                                                                       0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_USB30_XPU2_MSA_IRQ_SHFT                                                                                                          0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSWSYS_MPU_MSA_INTR_BMSK                                                                                                 0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSWSYS_MPU_MSA_INTR_SHFT                                                                                                   0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSW1_MPU_MSA_INTR_BMSK                                                                                                   0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSW1_MPU_MSA_INTR_SHFT                                                                                                     0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSW0_MPU_MSA_INTR_BMSK                                                                                                   0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_OSW0_MPU_MSA_INTR_SHFT                                                                                                     0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_DM_RPU_MSA_INTR_BMSK                                                                                                     0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_DM_RPU_MSA_INTR_SHFT                                                                                                       0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_RPU_MSA_INTR_BMSK                                                                                                         0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCMEM_RPU_MSA_INTR_SHFT                                                                                                          0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MDSS_VBIF_XPU2_MSA_INTR_BMSK                                                                                                    0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MDSS_VBIF_XPU2_MSA_INTR_SHFT                                                                                                     0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CAMSS_JPEG_VBIF_XPU2_MSA_INTR_BMSK                                                                                              0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CAMSS_JPEG_VBIF_XPU2_MSA_INTR_SHFT                                                                                               0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CAMSS_VFE_VBIF_XPU2_MSA_INTR_BMSK                                                                                               0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CAMSS_VFE_VBIF_XPU2_MSA_INTR_SHFT                                                                                                0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MMCC_XPU2_MSA_INTR_BMSK                                                                                                          0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MMCC_XPU2_MSA_INTR_SHFT                                                                                                          0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_VENUS0_XPU2_MSA_INTR_BMSK                                                                                                        0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_VENUS0_XPU2_MSA_INTR_SHFT                                                                                                        0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OXILI_XPU2_MSA_INTR_BMSK                                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OXILI_XPU2_MSA_INTR_SHFT                                                                                                         0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RESERVEBIT_BMSK                                                                                                                  0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RESERVEBIT_SHFT                                                                                                                  0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00006004)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RPM_MPU_MSA_INTR_BMSK                                                                                                     0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RPM_MPU_MSA_INTR_SHFT                                                                                                           0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCIMEM_MPU_MSA_INTR_BMSK                                                                                                  0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCIMEM_MPU_MSA_INTR_SHFT                                                                                                        0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SEC_CTRL_XPU2_MSA_IRQ_BMSK                                                                                                0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SEC_CTRL_XPU2_MSA_IRQ_SHFT                                                                                                      0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_LPASS_IRQ_OUT_SECURITY_BMSK                                                                                               0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_LPASS_IRQ_OUT_SECURITY_SHFT                                                                                                     0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_DEHR_XPU_MSA_INTR_BMSK                                                                                                     0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_DEHR_XPU_MSA_INTR_SHFT                                                                                                          0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_VENUS0_VBIF_XPU2_MSA_INTR_BMSK                                                                                             0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_VENUS0_VBIF_XPU2_MSA_INTR_SHFT                                                                                                  0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OXILI_VBIF_XPU2_MSA_INTR_BMSK                                                                                              0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OXILI_VBIF_XPU2_MSA_INTR_SHFT                                                                                                   0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MDSS_XPU2_MSA_INTR_BMSK                                                                                                    0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MDSS_XPU2_MSA_INTR_SHFT                                                                                                         0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_QDSS_BAM_XPU2_MSA_INTR_BMSK                                                                                                 0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_QDSS_BAM_XPU2_MSA_INTR_SHFT                                                                                                     0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MPM_XPU2_MSA_INTR_BMSK                                                                                                      0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MPM_XPU2_MSA_INTR_SHFT                                                                                                          0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BOOT_ROM_MSA_INTR_BMSK                                                                                                      0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BOOT_ROM_MSA_INTR_SHFT                                                                                                          0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_GFX_RBCPR_XPU2_MSA_INTR_BMSK                                                                                           0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_GFX_RBCPR_XPU2_MSA_INTR_SHFT                                                                                               0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_MISC_XPU2_MSA_INTR_BMSK                                                                                                 0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_MISC_XPU2_MSA_INTR_SHFT                                                                                                    0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_PCIE20_1_INT_PARF_XPU2_MSA_INTR_BMSK                                                                                         0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_PCIE20_1_INT_PARF_XPU2_MSA_INTR_SHFT                                                                                            0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_NOC_XPU2_MSA_INTR_BMSK                                                                                                  0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MMSS_NOC_XPU2_MSA_INTR_SHFT                                                                                                     0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCMEM_OSWDM_MPU_MSA_INTR_BMSK                                                                                                0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCMEM_OSWDM_MPU_MSA_INTR_SHFT                                                                                                   0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CAMSS_XPU2_MSA_INTR_BMSK                                                                                                      0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CAMSS_XPU2_MSA_INTR_SHFT                                                                                                         0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CRYPTO1_BAM_XPU2_MSA_INTR_BMSK                                                                                                0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CRYPTO1_BAM_XPU2_MSA_INTR_SHFT                                                                                                   0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CRYPTO0_BAM_XPU2_MSA_INTR_BMSK                                                                                                0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_CRYPTO0_BAM_XPU2_MSA_INTR_SHFT                                                                                                   0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_O_TCSR_MUTEX_MSA_INTR_BMSK                                                                                                    0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_O_TCSR_MUTEX_MSA_INTR_SHFT                                                                                                       0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_O_TCSR_REGS_MSA_INTR_BMSK                                                                                                      0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_O_TCSR_REGS_MSA_INTR_SHFT                                                                                                        0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_COPSS_MPU_MSA_IRQ_BMSK                                                                                                         0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_COPSS_MPU_MSA_IRQ_SHFT                                                                                                           0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RESERVEBIT_BMSK                                                                                                                0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RESERVEBIT_SHFT                                                                                                                  0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_USB1_HS_XPU2_MSA_INTR_BMSK                                                                                                     0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_USB1_HS_XPU2_MSA_INTR_SHFT                                                                                                       0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC4_XPU2_MSA_INTR_BMSK                                                                                                         0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC4_XPU2_MSA_INTR_SHFT                                                                                                          0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC3_XPU2_MSA_INTR_BMSK                                                                                                         0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC3_XPU2_MSA_INTR_SHFT                                                                                                          0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC2_XPU2_MSA_INTR_BMSK                                                                                                         0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC2_XPU2_MSA_INTR_SHFT                                                                                                          0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC1_XPU2_MSA_INTR_BMSK                                                                                                         0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDC1_XPU2_MSA_INTR_SHFT                                                                                                          0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_TSIF_BAM_XPU2_MSA_INTR_BMSK                                                                                                      0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_TSIF_BAM_XPU2_MSA_INTR_SHFT                                                                                                      0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP2_XPU2_MSA_INTR_BMSK                                                                                                         0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP2_XPU2_MSA_INTR_SHFT                                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP1_XPU2_MSA_INTR_BMSK                                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP1_XPU2_MSA_INTR_SHFT                                                                                                         0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BAM_DMA_XPU2_MSA_INTR_BMSK                                                                                                       0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BAM_DMA_XPU2_MSA_INTR_SHFT                                                                                                       0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00006008)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RMSK                                                                                                                          0x3fff
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR2_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR2_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR2_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RESERVEBIT_2_BMSK                                                                                                             0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RESERVEBIT_2_SHFT                                                                                                                0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_FD_VBIF_XPU_MSA_INTR_BMSK                                                                                                     0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_FD_VBIF_XPU_MSA_INTR_SHFT                                                                                                        0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_FD_XPU_MSA_INTR_BMSK                                                                                                           0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_FD_XPU_MSA_INTR_SHFT                                                                                                             0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_UFS_XPU_MSA_INTR_BMSK                                                                                                          0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_UFS_XPU_MSA_INTR_SHFT                                                                                                            0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_CAMSS_CPP_VBIF_XPU2_MSA_INTR_BMSK                                                                                              0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_CAMSS_CPP_VBIF_XPU2_MSA_INTR_SHFT                                                                                                0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RICA_CORE_TOP_XPU2_MSA_IRQ_BMSK                                                                                                0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RICA_CORE_TOP_XPU2_MSA_IRQ_SHFT                                                                                                  0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RICA_CORE_VBIF_XPU2_MSA_IRQ_BMSK                                                                                                0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RICA_CORE_VBIF_XPU2_MSA_IRQ_SHFT                                                                                                 0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_IPA_BAM_XPU2_MSA_INTR_BMSK                                                                                                      0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_IPA_BAM_XPU2_MSA_INTR_SHFT                                                                                                       0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RESERVEBIT_0_BMSK                                                                                                               0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_RESERVEBIT_0_SHFT                                                                                                                0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_UFS_ICE_XPU2_MSA_INTR_BMSK                                                                                                      0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_UFS_ICE_XPU2_MSA_INTR_SHFT                                                                                                       0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_1_INT_SLV_XPU2_MSA_INTR_BMSK                                                                                              0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_1_INT_SLV_XPU2_MSA_INTR_SHFT                                                                                              0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_CRYPTO2_BAM_XPU2_MSA_INTR_BMSK                                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_CRYPTO2_BAM_XPU2_MSA_INTR_SHFT                                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_0_INT_SLV_XPU2_MSA_INTR_BMSK                                                                                              0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_0_INT_SLV_XPU2_MSA_INTR_SHFT                                                                                              0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_0_INT_PARF_XPU2_MSA_INTR_BMSK                                                                                             0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_PCIE20_0_INT_PARF_XPU2_MSA_INTR_SHFT                                                                                             0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00006040)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MPM_QDSS_XPU2_MSA_INTR_ENABLE_BMSK                                                                                 0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MPM_QDSS_XPU2_MSA_INTR_ENABLE_SHFT                                                                                       0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DDR_PHY_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                              0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DDR_PHY_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                                    0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_NOC_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                                  0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_NOC_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                                        0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_BMSK                                                                                       0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_SHFT                                                                                            0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPDM_XPU_MSA_INTR_ENABLE_BMSK                                                                                       0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPDM_XPU_MSA_INTR_ENABLE_SHFT                                                                                            0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPMI_RPU_MSA_INTR_ENABLE_BMSK                                                                                       0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPMI_RPU_MSA_INTR_ENABLE_SHFT                                                                                            0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_APU_MSA_INTR_ENABLE_BMSK                                                                                   0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_APU_MSA_INTR_ENABLE_SHFT                                                                                        0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_BMSK                                                                                      0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_SHFT                                                                                          0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH1_XPU2_MSA_INTERRUPT_BMSK                                                                                     0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH1_XPU2_MSA_INTERRUPT_SHFT                                                                                         0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTERRUPT_BMSK                                                                                     0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTERRUPT_SHFT                                                                                         0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CFG_XPU2_MSA_INTERRUPT_BMSK                                                                                     0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CFG_XPU2_MSA_INTERRUPT_SHFT                                                                                         0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_BMSK                                                                                          0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_SHFT                                                                                             0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_APU_MSA_INTR_ENABLE_BMSK                                                                                          0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_APU_MSA_INTR_ENABLE_SHFT                                                                                             0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MSG_RAM_XPU_MSA_INTR_ENABLE_BMSK                                                                                      0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MSG_RAM_XPU_MSA_INTR_ENABLE_SHFT                                                                                         0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_WCSS_APU_MSA_INTR_ENABLE_BMSK                                                                                         0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_WCSS_APU_MSA_INTR_ENABLE_SHFT                                                                                            0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_APP_XPU2_MSA_INTR_ENABLE_BMSK                                                                                     0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_APP_XPU2_MSA_INTR_ENABLE_SHFT                                                                                        0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_APCS_SYSKSMPUMSAINT_BMSK                                                                                               0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_APCS_SYSKSMPUMSAINT_SHFT                                                                                                  0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_COPSS_APU_MSA_IRQ_ENABLE_BMSK                                                                                          0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_COPSS_APU_MSA_IRQ_ENABLE_SHFT                                                                                             0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_USB30_XPU2_MSA_IRQ_ENABLE_BMSK                                                                                         0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_USB30_XPU2_MSA_IRQ_ENABLE_SHFT                                                                                            0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSWSYS_MPU_MSA_INTR_ENABLE_BMSK                                                                                   0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSWSYS_MPU_MSA_INTR_ENABLE_SHFT                                                                                     0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSW1_MPU_MSA_INTR_ENABLE_BMSK                                                                                     0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSW1_MPU_MSA_INTR_ENABLE_SHFT                                                                                       0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSW0_MPU_MSA_INTR_ENABLE_BMSK                                                                                     0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_OSW0_MPU_MSA_INTR_ENABLE_SHFT                                                                                       0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_DM_RPU_MSA_INTR_ENABLE_BMSK                                                                                       0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_DM_RPU_MSA_INTR_ENABLE_SHFT                                                                                         0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_RPU_MSA_INTR_ENABLE_BMSK                                                                                           0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCMEM_RPU_MSA_INTR_ENABLE_SHFT                                                                                            0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MDSS_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                                      0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MDSS_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                       0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                                0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CAMSS_JPEG_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                 0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                                 0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CAMSS_VFE_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                  0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MMCC_XPU2_MSA_INTR_ENABLE_BMSK                                                                                            0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MMCC_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_VENUS0_XPU2_MSA_INTR_ENABLE_BMSK                                                                                          0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_VENUS0_XPU2_MSA_INTR_ENABLE_SHFT                                                                                          0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OXILI_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OXILI_XPU2_MSA_INTR_ENABLE_SHFT                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RESERVEBIT_BMSK                                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RESERVEBIT_SHFT                                                                                                           0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00006044)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RPM_MPU_MSA_INTR_ENABLE_BMSK                                                                                       0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RPM_MPU_MSA_INTR_ENABLE_SHFT                                                                                             0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCIMEM_MPU_MSA_INTR_ENABLE_BMSK                                                                                    0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCIMEM_MPU_MSA_INTR_ENABLE_SHFT                                                                                          0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SEC_CTRL_XPU2_MSA_IRQ_ENABLE_BMSK                                                                                  0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SEC_CTRL_XPU2_MSA_IRQ_ENABLE_SHFT                                                                                        0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_LPASS_IRQ_OUT_SECURITY_ENABLE_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_DEHR_XPU_MSA_INTR_ENABLE_BMSK                                                                                       0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_DEHR_XPU_MSA_INTR_ENABLE_SHFT                                                                                            0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_VENUS0_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                               0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_VENUS0_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                    0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OXILI_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                                0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OXILI_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MDSS_XPU2_MSA_INTR_ENABLE_BMSK                                                                                      0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MDSS_XPU2_MSA_INTR_ENABLE_SHFT                                                                                           0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_QDSS_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_QDSS_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                       0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                        0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_BMSK                                                                                        0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_SHFT                                                                                            0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_MSA_INTR_ENABLE_BMSK                                                                             0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_GFX_RBCPR_XPU2_MSA_INTR_ENABLE_SHFT                                                                                 0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_MISC_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_MISC_XPU2_MSA_INTR_ENABLE_SHFT                                                                                      0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_MSA_INTR_ENABLE_BMSK                                                                           0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_PCIE20_1_INT_PARF_XPU2_MSA_INTR_ENABLE_SHFT                                                                              0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_NOC_XPU2_MSA_INTR_ENABLE_BMSK                                                                                    0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MMSS_NOC_XPU2_MSA_INTR_ENABLE_SHFT                                                                                       0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCMEM_OSWDM_MPU_MSA_INTR_ENABLE_BMSK                                                                                  0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCMEM_OSWDM_MPU_MSA_INTR_ENABLE_SHFT                                                                                     0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CAMSS_XPU2_MSA_INTR_ENABLE_BMSK                                                                                        0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CAMSS_XPU2_MSA_INTR_ENABLE_SHFT                                                                                           0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CRYPTO1_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                  0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CRYPTO1_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                  0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_O_TCSR_MUTEX_MSA_INTR_ENABLE_BMSK                                                                                      0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_O_TCSR_MUTEX_MSA_INTR_ENABLE_SHFT                                                                                         0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_BMSK                                                                                        0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_SHFT                                                                                          0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_COPSS_MPU_MSA_IRQ_ENABLE_BMSK                                                                                           0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_COPSS_MPU_MSA_IRQ_ENABLE_SHFT                                                                                             0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RESERVEBIT_BMSK                                                                                                         0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RESERVEBIT_SHFT                                                                                                           0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_USB1_HS_XPU2_MSA_INTR_ENABLE_BMSK                                                                                       0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_USB1_HS_XPU2_MSA_INTR_ENABLE_SHFT                                                                                         0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC4_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC4_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC3_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC3_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC2_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC2_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC1_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDC1_XPU2_MSA_INTR_ENABLE_SHFT                                                                                            0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_TSIF_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                        0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_TSIF_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                        0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP2_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP2_XPU2_MSA_INTR_ENABLE_SHFT                                                                                           0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_BMSK                                                                                           0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_SHFT                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BAM_DMA_XPU2_MSA_INTR_ENABLE_BMSK                                                                                         0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BAM_DMA_XPU2_MSA_INTR_ENABLE_SHFT                                                                                         0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00006048)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RMSK                                                                                                                   0x3fff
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RESERVEBIT_2_BMSK                                                                                                      0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RESERVEBIT_2_SHFT                                                                                                         0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_FD_VBIF_XPU_MSA_INTR_ENABLE_BMSK                                                                                       0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_FD_VBIF_XPU_MSA_INTR_ENABLE_SHFT                                                                                          0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_FD_XPU_MSA_INTR_ENABLE_BMSK                                                                                             0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_FD_XPU_MSA_INTR_ENABLE_SHFT                                                                                               0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_UFS_XPU_MSA_INTR_ENABLE_BMSK                                                                                            0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_UFS_XPU_MSA_INTR_ENABLE_SHFT                                                                                              0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_MSA_INTR_ENABLE_BMSK                                                                                0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_CAMSS_CPP_VBIF_XPU2_MSA_INTR_ENABLE_SHFT                                                                                  0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RICA_CORE_TOP_XPU2_MSA_IRQ_ENABLE_BMSK                                                                                  0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RICA_CORE_TOP_XPU2_MSA_IRQ_ENABLE_SHFT                                                                                    0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_MSA_IRQ_ENABLE_BMSK                                                                                  0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RICA_CORE_VBIF_XPU2_MSA_IRQ_ENABLE_SHFT                                                                                   0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_IPA_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                        0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_IPA_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                         0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RESERVEBIT_0_BMSK                                                                                                        0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_RESERVEBIT_0_SHFT                                                                                                         0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_UFS_ICE_XPU2_MSA_INTR_ENABLE_BMSK                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_UFS_ICE_XPU2_MSA_INTR_ENABLE_SHFT                                                                                         0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_MSA_INTR_ENABLE_BMSK                                                                                0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_1_INT_SLV_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_CRYPTO2_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                     0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_CRYPTO2_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_MSA_INTR_ENABLE_BMSK                                                                                0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_0_INT_SLV_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_MSA_INTR_ENABLE_BMSK                                                                               0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR2_ENABLE_PCIE20_0_INT_PARF_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x0

#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00007000)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK                                                                                                                          0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, m)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,v)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,m,v,HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_BMSK                                                                                                     0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_SHFT                                                                                                        0x0

#define HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR                                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00007004)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK                                                                                                                       0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, m)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,v)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,m,v,HWIO_TCSR_SPDM_DLY_FIFO_EN_IN)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_BMSK                                                                                                      0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_SHFT                                                                                                             0x0

#define HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00007008)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK                                                                                                                          0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG1_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_BMSK                                                                                                        0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_SHFT                                                                                                           0x0

#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000700c)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_BMSK                                                                                                0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_SHFT                                                                                                       0x0

#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00007010)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_BMSK                                                                                                0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_SHFT                                                                                                       0x0

#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00007014)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_BMSK                                                                                                0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_SHFT                                                                                                       0x0

#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00007018)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_BMSK                                                                                                0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_SHFT                                                                                                       0x0

#define HWIO_TCSR_SOC_HW_VERSION_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00008000)
#define HWIO_TCSR_SOC_HW_VERSION_RMSK                                                                                                                         0xffffffff
#define HWIO_TCSR_SOC_HW_VERSION_IN          \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, HWIO_TCSR_SOC_HW_VERSION_RMSK)
#define HWIO_TCSR_SOC_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, m)
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_BMSK                                                                                                           0xf0000000
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_SHFT                                                                                                                 0x1c
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_BMSK                                                                                                            0xfff0000
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_SHFT                                                                                                                 0x10
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_BMSK                                                                                                               0xff00
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_SHFT                                                                                                                  0x8
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_BMSK                                                                                                                 0xff
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_SHFT                                                                                                                  0x0

#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00008020)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN2_CAHB_TSLV_INT_BMSK                                                                                           0x80000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN2_CAHB_TSLV_INT_SHFT                                                                                                 0x1f
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_FDAHB_TSLV_INT_BMSK                                                                                          0x40000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_FDAHB_TSLV_INT_SHFT                                                                                                0x1e
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_DAHB_TSLV_INT_BMSK                                                                                           0x20000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_DAHB_TSLV_INT_SHFT                                                                                                 0x1d
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_CAHB_TSLV_INT_BMSK                                                                                           0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_WLAN1_CAHB_TSLV_INT_SHFT                                                                                                 0x1c
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CDAHB2_TSLV_INT_BMSK                                                                                                0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CDAHB2_TSLV_INT_SHFT                                                                                                     0x1b
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CDAHB1_TSLV_INT_BMSK                                                                                                0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CDAHB1_TSLV_INT_SHFT                                                                                                     0x1a
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CCAHB_TSLV_INT_BMSK                                                                                                 0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_CCAHB_TSLV_INT_SHFT                                                                                                      0x19
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_AOAHB_TSLV_INT_BMSK                                                                                                 0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_AOAHB_TSLV_INT_SHFT                                                                                                      0x18
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_BMSK                                                                                         0x800000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_SHFT                                                                                             0x17
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT1_BMSK                                                                                           0x400000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT1_SHFT                                                                                               0x16
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT0_BMSK                                                                                           0x200000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT0_SHFT                                                                                               0x15
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_BMSK                                                                                         0x100000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_SHFT                                                                                             0x14
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS2_BUS_TIMEOUT_IRQ_BMSK                                                                                             0x80000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS2_BUS_TIMEOUT_IRQ_SHFT                                                                                                0x13
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS1_BUS_TIMEOUT_IRQ_BMSK                                                                                             0x40000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS1_BUS_TIMEOUT_IRQ_SHFT                                                                                                0x12
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS_BUS_TIMEOUT_IRQ_BMSK                                                                                              0x20000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS_BUS_TIMEOUT_IRQ_SHFT                                                                                                 0x11
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S7_IRQ_BMSK                                                                                               0x10000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S7_IRQ_SHFT                                                                                                  0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_4_IRQ_BMSK                                                                                                0x8000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_4_IRQ_SHFT                                                                                                   0xf
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_3_IRQ_BMSK                                                                                                0x4000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_3_IRQ_SHFT                                                                                                   0xe
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_2_IRQ_BMSK                                                                                                0x2000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_2_IRQ_SHFT                                                                                                   0xd
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_1_IRQ_BMSK                                                                                                0x1000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_1_IRQ_SHFT                                                                                                   0xc
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_0_IRQ_BMSK                                                                                                 0x800
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_0_IRQ_SHFT                                                                                                   0xb
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S0_IRQ_BMSK                                                                                                 0x400
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S0_IRQ_SHFT                                                                                                   0xa
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_ECAHB_TSLV_INT_IRQ_BMSK                                                                                                 0x200
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_WCSS_ECAHB_TSLV_INT_IRQ_SHFT                                                                                                   0x9
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S2_IRQ_BMSK                                                                                                 0x100
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S2_IRQ_SHFT                                                                                                   0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S3_IRQ_BMSK                                                                                                  0x80
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S3_IRQ_SHFT                                                                                                   0x7
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S6_IRQ_BMSK                                                                                                  0x40
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S6_IRQ_SHFT                                                                                                   0x6
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S5_IRQ_BMSK                                                                                                  0x20
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S5_IRQ_SHFT                                                                                                   0x5
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S4_IRQ_BMSK                                                                                                  0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S4_IRQ_SHFT                                                                                                   0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S3_IRQ_BMSK                                                                                                   0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S3_IRQ_SHFT                                                                                                   0x3
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S2_IRQ_BMSK                                                                                                   0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S2_IRQ_SHFT                                                                                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S1_IRQ_BMSK                                                                                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S1_IRQ_SHFT                                                                                                   0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S0_IRQ_BMSK                                                                                                   0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S0_IRQ_SHFT                                                                                                   0x0

#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00008084)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_RMSK                                                                                                                         0x3f
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS1_ADDR, HWIO_TCSR_TIMEOUT_INTR_STATUS1_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS1_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_RESERVEBIT_4_BMSK                                                                                                            0x20
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_RESERVEBIT_4_SHFT                                                                                                             0x5
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_RESERVEBIT_3_BMSK                                                                                                            0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_RESERVEBIT_3_SHFT                                                                                                             0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_BUS_TIMEOUT_SNOC_S1_IRQ_BMSK                                                                                                  0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_BUS_TIMEOUT_SNOC_S1_IRQ_SHFT                                                                                                  0x3
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_QHS_MMSS3_BUS_TIMEOUT_IRQ_BMSK                                                                                                0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_QHS_MMSS3_BUS_TIMEOUT_IRQ_SHFT                                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_WCSS_WLAN2_FDAHB_TSLV_INT_BMSK                                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_WCSS_WLAN2_FDAHB_TSLV_INT_SHFT                                                                                                0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_WCSS_WLAN2_DAHB_TSLV_INT_BMSK                                                                                                 0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS1_WCSS_WLAN2_DAHB_TSLV_INT_SHFT                                                                                                 0x0

#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00008030)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_BMSK                                                                                0x80000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1f
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_BMSK                                                                               0x40000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1e
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_BMSK                                                                                0x20000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1d
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_BMSK                                                                                0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1c
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_BMSK                                                                                     0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_SHFT                                                                                          0x1b
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_BMSK                                                                                     0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_SHFT                                                                                          0x1a
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_SHFT                                                                                           0x19
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_SHFT                                                                                           0x18
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_BMSK                                                                              0x800000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_SHFT                                                                                  0x17
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_BMSK                                                                         0x400000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_SHFT                                                                             0x16
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_BMSK                                                                         0x200000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_SHFT                                                                             0x15
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_BMSK                                                                              0x100000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_SHFT                                                                                  0x14
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                  0x80000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x13
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                  0x40000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x12
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                   0x20000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                      0x11
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                                    0x10000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                                       0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                                     0x8000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                        0xf
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                                     0x4000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                        0xe
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                                     0x2000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                        0xd
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                                     0x1000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                        0xc
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                                      0x800
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                        0xb
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_BMSK                                                                                      0x400
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_SHFT                                                                                        0xa
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_BMSK                                                                                      0x200
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_SHFT                                                                                        0x9
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                                      0x100
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                        0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_BMSK                                                                                       0x80
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_SHFT                                                                                        0x7
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                                       0x40
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                        0x6
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                                       0x20
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                        0x5
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                                       0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                        0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                        0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                        0x3
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                        0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                        0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                        0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                        0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                        0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                        0x0

#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00008074)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RMSK                                                                                                                     0x3f
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_ADDR, HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_IN)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RESERVEBIT_4_BMSK                                                                                                        0x20
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RESERVEBIT_4_SHFT                                                                                                         0x5
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RESERVEBIT_3_BMSK                                                                                                        0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_RESERVEBIT_3_SHFT                                                                                                         0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                                       0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                                       0x3
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_BMSK                                                                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x0

#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00008040)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_BMSK                                                                               0x80000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1f
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_BMSK                                                                              0x40000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x1e
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_BMSK                                                                               0x20000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1d
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_BMSK                                                                               0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1c
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_BMSK                                                                                    0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_SHFT                                                                                         0x1b
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_BMSK                                                                                    0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_SHFT                                                                                         0x1a
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_BMSK                                                                                     0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_SHFT                                                                                          0x19
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_BMSK                                                                                     0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_SHFT                                                                                          0x18
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_BMSK                                                                             0x800000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_SHFT                                                                                 0x17
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_BMSK                                                                        0x400000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_SHFT                                                                            0x16
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_BMSK                                                                        0x200000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_SHFT                                                                            0x15
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_BMSK                                                                             0x100000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_SHFT                                                                                 0x14
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                 0x80000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                    0x13
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                 0x40000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                    0x12
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                  0x20000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x11
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                                   0x10000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                                      0x10
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                                    0x8000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                       0xf
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                                    0x4000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                       0xe
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                                    0x2000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                       0xd
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                                    0x1000
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                       0xc
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                                     0x800
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                       0xb
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_BMSK                                                                                     0x400
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_SHFT                                                                                       0xa
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_BMSK                                                                                     0x200
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_SHFT                                                                                       0x9
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                                     0x100
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                       0x8
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_BMSK                                                                                      0x80
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_SHFT                                                                                       0x7
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                                      0x40
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                       0x6
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                                      0x20
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                       0x5
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                                      0x10
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                       0x4
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                       0x8
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                       0x3
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                       0x4
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                       0x2
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                       0x2
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                       0x1
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                       0x1
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                       0x0

#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00008078)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RMSK                                                                                                                    0x3f
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_ADDR, HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_IN)
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RESERVEBIT_4_BMSK                                                                                                       0x20
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RESERVEBIT_4_SHFT                                                                                                        0x5
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RESERVEBIT_3_BMSK                                                                                                       0x10
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_RESERVEBIT_3_SHFT                                                                                                        0x4
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                                      0x8
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                                      0x3
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                    0x4
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                    0x2
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_BMSK                                                                                    0x2
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x1
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_BMSK                                                                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_KPSS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x0

#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00008050)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_BMSK                                                                              0x80000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x1f
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_BMSK                                                                             0x40000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                   0x1e
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_BMSK                                                                              0x20000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x1d
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_BMSK                                                                              0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x1c
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_BMSK                                                                                   0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_SHFT                                                                                        0x1b
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_BMSK                                                                                   0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_SHFT                                                                                        0x1a
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_BMSK                                                                                    0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_SHFT                                                                                         0x19
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_BMSK                                                                                    0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_SHFT                                                                                         0x18
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_BMSK                                                                            0x800000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_SHFT                                                                                0x17
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_BMSK                                                                       0x400000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_SHFT                                                                           0x16
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_BMSK                                                                       0x200000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_SHFT                                                                           0x15
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_BMSK                                                                            0x100000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_SHFT                                                                                0x14
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                0x80000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                   0x13
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                0x40000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                   0x12
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                 0x20000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                    0x11
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                                  0x10000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                                     0x10
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                                   0x8000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                      0xf
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                                   0x4000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                      0xe
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                                   0x2000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                      0xd
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                                   0x1000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                      0xc
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                                    0x800
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                      0xb
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_BMSK                                                                                    0x400
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_SHFT                                                                                      0xa
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_BMSK                                                                                    0x200
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_SHFT                                                                                      0x9
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                                    0x100
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                      0x8
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_BMSK                                                                                     0x80
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_SHFT                                                                                      0x7
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                                     0x40
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                      0x6
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                                     0x20
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                      0x5
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                                     0x10
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                      0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                      0x8
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                      0x3
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                      0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                      0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                      0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                      0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                      0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                      0x0

#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000807c)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RMSK                                                                                                                   0x3f
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_ADDR, HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_IN)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RESERVEBIT_4_BMSK                                                                                                      0x20
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RESERVEBIT_4_SHFT                                                                                                       0x5
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RESERVEBIT_3_BMSK                                                                                                      0x10
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_RESERVEBIT_3_SHFT                                                                                                       0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                                     0x8
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                                     0x3
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                   0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00008060)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_BMSK                                                                                0x80000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN2_CAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1f
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_BMSK                                                                               0x40000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1e
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_BMSK                                                                                0x20000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_DAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1d
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_BMSK                                                                                0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_WLAN1_CAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x1c
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_BMSK                                                                                     0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CDAHB2_TSLV_INT_ENABLE_SHFT                                                                                          0x1b
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_BMSK                                                                                     0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CDAHB1_TSLV_INT_ENABLE_SHFT                                                                                          0x1a
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_CCAHB_TSLV_INT_ENABLE_SHFT                                                                                           0x19
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_AOAHB_TSLV_INT_ENABLE_SHFT                                                                                           0x18
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_BMSK                                                                              0x800000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OCMEM_AXI_TIMEOUT_NONSEQ_IRQ_ENABLE_SHFT                                                                                  0x17
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_BMSK                                                                         0x400000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_ENABLE_SHFT                                                                             0x16
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_BMSK                                                                         0x200000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_ENABLE_SHFT                                                                             0x15
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_BMSK                                                                              0x100000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_ENABLE_SHFT                                                                                  0x14
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                  0x80000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x13
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                  0x40000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x12
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                   0x20000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                      0x11
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                                    0x10000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                                       0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                                     0x8000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                        0xf
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                                     0x4000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                        0xe
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                                     0x2000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                        0xd
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                                     0x1000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                        0xc
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                                      0x800
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                        0xb
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_BMSK                                                                                      0x400
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S0_IRQ_ENABLE_SHFT                                                                                        0xa
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_BMSK                                                                                      0x200
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_WCSS_ECAHB_TSLV_INT_IRQ_ENABLE_SHFT                                                                                        0x9
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                                      0x100
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                        0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_BMSK                                                                                       0x80
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S3_IRQ_ENABLE_SHFT                                                                                        0x7
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                                       0x40
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                        0x6
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                                       0x20
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                        0x5
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                                       0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                        0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                        0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                        0x3
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                        0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                        0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                        0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                        0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                        0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                        0x0

#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00008080)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RMSK                                                                                                                     0x3f
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_ADDR, HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_IN)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RESERVEBIT_4_BMSK                                                                                                        0x20
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RESERVEBIT_4_SHFT                                                                                                         0x5
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RESERVEBIT_3_BMSK                                                                                                        0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_RESERVEBIT_3_SHFT                                                                                                         0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                                       0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                                       0x3
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_BMSK                                                                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_WCSS_WLAN2_FDAHB_TSLV_INT_ENABLE_SHFT                                                                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_BMSK                                                                                      0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE1_WCSS_WLAN2_DAHB_TSLV_INT_ENABLE_SHFT                                                                                      0x0

#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00008070)
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_RMSK                                                                                                                        0x1
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_ADDR, HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_RMSK)
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_ADDR, m)
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_ADDR,v)
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_ADDR,m,v,HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_IN)
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_WCSS_BUS_TIMEOUT_NOC_SOFT_RESET_BMSK                                                                                        0x1
#define HWIO_TCSR_WCSS_BUS_TIMEOUT_RESET_WCSS_BUS_TIMEOUT_NOC_SOFT_RESET_SHFT                                                                                        0x0

#define HWIO_TCSR_TCSR_CLK_EN_ADDR                                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000907c)
#define HWIO_TCSR_TCSR_CLK_EN_RMSK                                                                                                                                   0x1
#define HWIO_TCSR_TCSR_CLK_EN_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, HWIO_TCSR_TCSR_CLK_EN_RMSK)
#define HWIO_TCSR_TCSR_CLK_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, m)
#define HWIO_TCSR_TCSR_CLK_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_CLK_EN_ADDR,v)
#define HWIO_TCSR_TCSR_CLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_CLK_EN_ADDR,m,v,HWIO_TCSR_TCSR_CLK_EN_IN)
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_BMSK                                                                                                                       0x1
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_SHFT                                                                                                                       0x0

#define HWIO_TCSR_CX_VSENS_CONFIG_0_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00009008)
#define HWIO_TCSR_CX_VSENS_CONFIG_0_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_CX_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_TCSR_CX_VSENS_CONFIG_0_ADDR, HWIO_TCSR_CX_VSENS_CONFIG_0_RMSK)
#define HWIO_TCSR_CX_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_VSENS_CONFIG_0_ADDR, m)
#define HWIO_TCSR_CX_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_TCSR_CX_VSENS_CONFIG_0_ADDR,v)
#define HWIO_TCSR_CX_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CX_VSENS_CONFIG_0_ADDR,m,v,HWIO_TCSR_CX_VSENS_CONFIG_0_IN)
#define HWIO_TCSR_CX_VSENS_CONFIG_0_RESET_FUNC_BMSK                                                                                                           0x80000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_RESET_FUNC_SHFT                                                                                                                 0x1f
#define HWIO_TCSR_CX_VSENS_CONFIG_0_MODE_SEL_BMSK                                                                                                             0x40000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_MODE_SEL_SHFT                                                                                                                   0x1e
#define HWIO_TCSR_CX_VSENS_CONFIG_0_RESERVE_BMSK                                                                                                              0x20000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_RESERVE_SHFT                                                                                                                    0x1d
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIG_SEL_BMSK                                                                                                             0x18000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIG_SEL_SHFT                                                                                                                   0x1b
#define HWIO_TCSR_CX_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                                                                                            0x4000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                                                                                                 0x1a
#define HWIO_TCSR_CX_VSENS_CONFIG_0_START_CAPTURE_BMSK                                                                                                         0x2000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_START_CAPTURE_SHFT                                                                                                              0x19
#define HWIO_TCSR_CX_VSENS_CONFIG_0_EN_POWER_BMSK                                                                                                              0x1000000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_EN_POWER_SHFT                                                                                                                   0x18
#define HWIO_TCSR_CX_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                                                                                          0xff0000
#define HWIO_TCSR_CX_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                                                                                              0x10
#define HWIO_TCSR_CX_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                                                                                            0xff00
#define HWIO_TCSR_CX_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                                                                                               0x8
#define HWIO_TCSR_CX_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                                                                                              0xf0
#define HWIO_TCSR_CX_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                                                                                               0x4
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIGGER_POS_BMSK                                                                                                                 0xc
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIGGER_POS_SHFT                                                                                                                 0x2
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                                                                                                 0x3
#define HWIO_TCSR_CX_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                                                                                                 0x0

#define HWIO_TCSR_CX_VSENS_CONFIG_1_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000900c)
#define HWIO_TCSR_CX_VSENS_CONFIG_1_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_CX_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_TCSR_CX_VSENS_CONFIG_1_ADDR, HWIO_TCSR_CX_VSENS_CONFIG_1_RMSK)
#define HWIO_TCSR_CX_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_VSENS_CONFIG_1_ADDR, m)
#define HWIO_TCSR_CX_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_TCSR_CX_VSENS_CONFIG_1_ADDR,v)
#define HWIO_TCSR_CX_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CX_VSENS_CONFIG_1_ADDR,m,v,HWIO_TCSR_CX_VSENS_CONFIG_1_IN)
#define HWIO_TCSR_CX_VSENS_CONFIG_1_RESERVE_BMSK                                                                                                              0xff000000
#define HWIO_TCSR_CX_VSENS_CONFIG_1_RESERVE_SHFT                                                                                                                    0x18
#define HWIO_TCSR_CX_VSENS_CONFIG_1_SEL_JTAG_CSR_BMSK                                                                                                           0x800000
#define HWIO_TCSR_CX_VSENS_CONFIG_1_SEL_JTAG_CSR_SHFT                                                                                                               0x17
#define HWIO_TCSR_CX_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                                                                                              0x7e0000
#define HWIO_TCSR_CX_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                                                                                                  0x11
#define HWIO_TCSR_CX_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_BMSK                                                                                                      0x10000
#define HWIO_TCSR_CX_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_SHFT                                                                                                         0x10
#define HWIO_TCSR_CX_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                                                                                              0xe000
#define HWIO_TCSR_CX_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                                                                                                 0xd
#define HWIO_TCSR_CX_VSENS_CONFIG_1_SLOPE_THRESHOLD_BMSK                                                                                                          0x1fe0
#define HWIO_TCSR_CX_VSENS_CONFIG_1_SLOPE_THRESHOLD_SHFT                                                                                                             0x5
#define HWIO_TCSR_CX_VSENS_CONFIG_1_EN_FUNC_BMSK                                                                                                                    0x10
#define HWIO_TCSR_CX_VSENS_CONFIG_1_EN_FUNC_SHFT                                                                                                                     0x4
#define HWIO_TCSR_CX_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                                                                                                   0x8
#define HWIO_TCSR_CX_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                                                                                                   0x3
#define HWIO_TCSR_CX_VSENS_CONFIG_1_STATUS_CLEAR_BMSK                                                                                                                0x4
#define HWIO_TCSR_CX_VSENS_CONFIG_1_STATUS_CLEAR_SHFT                                                                                                                0x2
#define HWIO_TCSR_CX_VSENS_CONFIG_1_EN_ALARM_BMSK                                                                                                                    0x2
#define HWIO_TCSR_CX_VSENS_CONFIG_1_EN_ALARM_SHFT                                                                                                                    0x1
#define HWIO_TCSR_CX_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_BMSK                                                                                                            0x1
#define HWIO_TCSR_CX_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_SHFT                                                                                                            0x0

#define HWIO_TCSR_CX_VSENS_STATUS_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00009010)
#define HWIO_TCSR_CX_VSENS_STATUS_RMSK                                                                                                                        0xffffffff
#define HWIO_TCSR_CX_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_CX_VSENS_STATUS_ADDR, HWIO_TCSR_CX_VSENS_STATUS_RMSK)
#define HWIO_TCSR_CX_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_VSENS_STATUS_ADDR, m)
#define HWIO_TCSR_CX_VSENS_STATUS_RESERVE_BMSK                                                                                                                0xfffc0000
#define HWIO_TCSR_CX_VSENS_STATUS_RESERVE_SHFT                                                                                                                      0x12
#define HWIO_TCSR_CX_VSENS_STATUS_VS_FSM_ST_BMSK                                                                                                                 0x3c000
#define HWIO_TCSR_CX_VSENS_STATUS_VS_FSM_ST_SHFT                                                                                                                     0xe
#define HWIO_TCSR_CX_VSENS_STATUS_SLOPE_NEG_BMSK                                                                                                                  0x2000
#define HWIO_TCSR_CX_VSENS_STATUS_SLOPE_NEG_SHFT                                                                                                                     0xd
#define HWIO_TCSR_CX_VSENS_STATUS_SLOPE_POS_BMSK                                                                                                                  0x1000
#define HWIO_TCSR_CX_VSENS_STATUS_SLOPE_POS_SHFT                                                                                                                     0xc
#define HWIO_TCSR_CX_VSENS_STATUS_ALARM_MAX_BMSK                                                                                                                   0x800
#define HWIO_TCSR_CX_VSENS_STATUS_ALARM_MAX_SHFT                                                                                                                     0xb
#define HWIO_TCSR_CX_VSENS_STATUS_ALARM_MIN_BMSK                                                                                                                   0x400
#define HWIO_TCSR_CX_VSENS_STATUS_ALARM_MIN_SHFT                                                                                                                     0xa
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                                                                                                             0x200
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                                                                                                               0x9
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                                                                                           0x100
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                                                                                             0x8
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_DATA_BMSK                                                                                                                    0xff
#define HWIO_TCSR_CX_VSENS_STATUS_FIFO_DATA_SHFT                                                                                                                     0x0

#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00009014)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_CONFIG_0_ADDR, HWIO_TCSR_VDDA_VSENS_CONFIG_0_RMSK)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_CONFIG_0_ADDR, m)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_TCSR_VDDA_VSENS_CONFIG_0_ADDR,v)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VDDA_VSENS_CONFIG_0_ADDR,m,v,HWIO_TCSR_VDDA_VSENS_CONFIG_0_IN)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_RESET_FUNC_BMSK                                                                                                         0x80000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_RESET_FUNC_SHFT                                                                                                               0x1f
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_MODE_SEL_BMSK                                                                                                           0x40000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_MODE_SEL_SHFT                                                                                                                 0x1e
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_RESERVE_BMSK                                                                                                            0x20000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_RESERVE_SHFT                                                                                                                  0x1d
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIG_SEL_BMSK                                                                                                           0x18000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIG_SEL_SHFT                                                                                                                 0x1b
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                                                                                          0x4000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                                                                                               0x1a
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_START_CAPTURE_BMSK                                                                                                       0x2000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_START_CAPTURE_SHFT                                                                                                            0x19
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_EN_POWER_BMSK                                                                                                            0x1000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_EN_POWER_SHFT                                                                                                                 0x18
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                                                                                        0xff0000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                                                                                            0x10
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                                                                                          0xff00
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                                                                                             0x8
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                                                                                            0xf0
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                                                                                             0x4
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIGGER_POS_BMSK                                                                                                               0xc
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIGGER_POS_SHFT                                                                                                               0x2
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                                                                                               0x3
#define HWIO_TCSR_VDDA_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                                                                                               0x0

#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00009018)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_CONFIG_1_ADDR, HWIO_TCSR_VDDA_VSENS_CONFIG_1_RMSK)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_CONFIG_1_ADDR, m)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_TCSR_VDDA_VSENS_CONFIG_1_ADDR,v)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VDDA_VSENS_CONFIG_1_ADDR,m,v,HWIO_TCSR_VDDA_VSENS_CONFIG_1_IN)
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_RESERVE_BMSK                                                                                                            0xff000000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_RESERVE_SHFT                                                                                                                  0x18
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_SEL_JTAG_CSR_BMSK                                                                                                         0x800000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_SEL_JTAG_CSR_SHFT                                                                                                             0x17
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                                                                                            0x7e0000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                                                                                                0x11
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_BMSK                                                                                                    0x10000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_ALARM_SLOPE_ENABLE_SHFT                                                                                                       0x10
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                                                                                            0xe000
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                                                                                               0xd
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_SLOPE_THRESHOLD_BMSK                                                                                                        0x1fe0
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_SLOPE_THRESHOLD_SHFT                                                                                                           0x5
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_EN_FUNC_BMSK                                                                                                                  0x10
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_EN_FUNC_SHFT                                                                                                                   0x4
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                                                                                                 0x8
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                                                                                                 0x3
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_STATUS_CLEAR_BMSK                                                                                                              0x4
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_STATUS_CLEAR_SHFT                                                                                                              0x2
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_EN_ALARM_BMSK                                                                                                                  0x2
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_EN_ALARM_SHFT                                                                                                                  0x1
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_BMSK                                                                                                          0x1
#define HWIO_TCSR_VDDA_VSENS_CONFIG_1_CGC_CLOCK_ENABLE_SHFT                                                                                                          0x0

#define HWIO_TCSR_VDDA_VSENS_STATUS_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000901c)
#define HWIO_TCSR_VDDA_VSENS_STATUS_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_VDDA_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_STATUS_ADDR, HWIO_TCSR_VDDA_VSENS_STATUS_RMSK)
#define HWIO_TCSR_VDDA_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_VSENS_STATUS_ADDR, m)
#define HWIO_TCSR_VDDA_VSENS_STATUS_RESERVE_BMSK                                                                                                              0xfffc0000
#define HWIO_TCSR_VDDA_VSENS_STATUS_RESERVE_SHFT                                                                                                                    0x12
#define HWIO_TCSR_VDDA_VSENS_STATUS_VS_FSM_ST_BMSK                                                                                                               0x3c000
#define HWIO_TCSR_VDDA_VSENS_STATUS_VS_FSM_ST_SHFT                                                                                                                   0xe
#define HWIO_TCSR_VDDA_VSENS_STATUS_SLOPE_NEG_BMSK                                                                                                                0x2000
#define HWIO_TCSR_VDDA_VSENS_STATUS_SLOPE_NEG_SHFT                                                                                                                   0xd
#define HWIO_TCSR_VDDA_VSENS_STATUS_SLOPE_POS_BMSK                                                                                                                0x1000
#define HWIO_TCSR_VDDA_VSENS_STATUS_SLOPE_POS_SHFT                                                                                                                   0xc
#define HWIO_TCSR_VDDA_VSENS_STATUS_ALARM_MAX_BMSK                                                                                                                 0x800
#define HWIO_TCSR_VDDA_VSENS_STATUS_ALARM_MAX_SHFT                                                                                                                   0xb
#define HWIO_TCSR_VDDA_VSENS_STATUS_ALARM_MIN_BMSK                                                                                                                 0x400
#define HWIO_TCSR_VDDA_VSENS_STATUS_ALARM_MIN_SHFT                                                                                                                   0xa
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                                                                                                           0x200
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                                                                                                             0x9
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                                                                                         0x100
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                                                                                           0x8
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_DATA_BMSK                                                                                                                  0xff
#define HWIO_TCSR_VDDA_VSENS_STATUS_FIFO_DATA_SHFT                                                                                                                   0x0

#define HWIO_TCSR_SYS_POWER_CTRL_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000a000)
#define HWIO_TCSR_SYS_POWER_CTRL_RMSK                                                                                                                             0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, HWIO_TCSR_SYS_POWER_CTRL_RMSK)
#define HWIO_TCSR_SYS_POWER_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, m)
#define HWIO_TCSR_SYS_POWER_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SYS_POWER_CTRL_ADDR,v)
#define HWIO_TCSR_SYS_POWER_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SYS_POWER_CTRL_ADDR,m,v,HWIO_TCSR_SYS_POWER_CTRL_IN)
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_BMSK                                                                                                              0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_SHFT                                                                                                                 0x0

#define HWIO_TCSR_USB_CORE_ID_ADDR                                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000a004)
#define HWIO_TCSR_USB_CORE_ID_RMSK                                                                                                                                   0x3
#define HWIO_TCSR_USB_CORE_ID_IN          \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, HWIO_TCSR_USB_CORE_ID_RMSK)
#define HWIO_TCSR_USB_CORE_ID_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, m)
#define HWIO_TCSR_USB_CORE_ID_OUT(v)      \
        out_dword(HWIO_TCSR_USB_CORE_ID_ADDR,v)
#define HWIO_TCSR_USB_CORE_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_CORE_ID_ADDR,m,v,HWIO_TCSR_USB_CORE_ID_IN)
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_BMSK                                                                                                                       0x3
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_SHFT                                                                                                                       0x0

#define HWIO_TCSR_SPARE_REG0_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a044)
#define HWIO_TCSR_SPARE_REG0_RMSK                                                                                                                             0xffffffff
#define HWIO_TCSR_SPARE_REG0_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, HWIO_TCSR_SPARE_REG0_RMSK)
#define HWIO_TCSR_SPARE_REG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, m)
#define HWIO_TCSR_SPARE_REG0_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG0_ADDR,v)
#define HWIO_TCSR_SPARE_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG0_ADDR,m,v,HWIO_TCSR_SPARE_REG0_IN)
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_BMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_REG1_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a048)
#define HWIO_TCSR_SPARE_REG1_RMSK                                                                                                                             0xffffffff
#define HWIO_TCSR_SPARE_REG1_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, HWIO_TCSR_SPARE_REG1_RMSK)
#define HWIO_TCSR_SPARE_REG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, m)
#define HWIO_TCSR_SPARE_REG1_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG1_ADDR,v)
#define HWIO_TCSR_SPARE_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG1_ADDR,m,v,HWIO_TCSR_SPARE_REG1_IN)
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_BMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_REG2_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a04c)
#define HWIO_TCSR_SPARE_REG2_RMSK                                                                                                                                   0xff
#define HWIO_TCSR_SPARE_REG2_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, HWIO_TCSR_SPARE_REG2_RMSK)
#define HWIO_TCSR_SPARE_REG2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, m)
#define HWIO_TCSR_SPARE_REG2_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG2_ADDR,v)
#define HWIO_TCSR_SPARE_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG2_ADDR,m,v,HWIO_TCSR_SPARE_REG2_IN)
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_BMSK                                                                                                                        0xff
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_REG3_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a050)
#define HWIO_TCSR_SPARE_REG3_RMSK                                                                                                                                   0xff
#define HWIO_TCSR_SPARE_REG3_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, HWIO_TCSR_SPARE_REG3_RMSK)
#define HWIO_TCSR_SPARE_REG3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, m)
#define HWIO_TCSR_SPARE_REG3_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG3_ADDR,v)
#define HWIO_TCSR_SPARE_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG3_ADDR,m,v,HWIO_TCSR_SPARE_REG3_IN)
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_BMSK                                                                                                                        0xff
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_REG4_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a054)
#define HWIO_TCSR_SPARE_REG4_RMSK                                                                                                                                   0xff
#define HWIO_TCSR_SPARE_REG4_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG4_ADDR, HWIO_TCSR_SPARE_REG4_RMSK)
#define HWIO_TCSR_SPARE_REG4_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG4_ADDR, m)
#define HWIO_TCSR_SPARE_REG4_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG4_ADDR,v)
#define HWIO_TCSR_SPARE_REG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG4_ADDR,m,v,HWIO_TCSR_SPARE_REG4_IN)
#define HWIO_TCSR_SPARE_REG4_SPARE_REG4_BMSK                                                                                                                        0xff
#define HWIO_TCSR_SPARE_REG4_SPARE_REG4_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_REG5_ADDR                                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000a058)
#define HWIO_TCSR_SPARE_REG5_RMSK                                                                                                                                   0xff
#define HWIO_TCSR_SPARE_REG5_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG5_ADDR, HWIO_TCSR_SPARE_REG5_RMSK)
#define HWIO_TCSR_SPARE_REG5_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG5_ADDR, m)
#define HWIO_TCSR_SPARE_REG5_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG5_ADDR,v)
#define HWIO_TCSR_SPARE_REG5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG5_ADDR,m,v,HWIO_TCSR_SPARE_REG5_IN)
#define HWIO_TCSR_SPARE_REG5_SPARE_REG5_BMSK                                                                                                                        0xff
#define HWIO_TCSR_SPARE_REG5_SPARE_REG5_SHFT                                                                                                                         0x0

#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000a05c)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR, HWIO_TCSR_SPARE_QGIC_INTERRUPTS_RMSK)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR, m)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR,v)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR,m,v,HWIO_TCSR_SPARE_QGIC_INTERRUPTS_IN)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_SPARE_QGIC_INTERRUPTS_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_SPARE_QGIC_INTERRUPTS_SHFT                                                                                                   0x0

#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000a060)
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_RMSK                                                                                                                   0xffffffff
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_ADDR, HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_RMSK)
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_ADDR, m)
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_ADDR,v)
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_ADDR,m,v,HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_IN)
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_UFS_SPARE_SCM_OUTPUT_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_UFS_SPARE_SCM_OUTPUT_UFS_SPARE_SCM_OUTPUT_SHFT                                                                                                     0x0

#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000a068)
#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_RMSK                                                                                                                    0xffffffff
#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SPARE_SCM_INPUT_ADDR, HWIO_TCSR_UFS_SPARE_SCM_INPUT_RMSK)
#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SPARE_SCM_INPUT_ADDR, m)
#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_UFS_SPARE_SCM_INPUT_BMSK                                                                                                0xffffffff
#define HWIO_TCSR_UFS_SPARE_SCM_INPUT_UFS_SPARE_SCM_INPUT_SHFT                                                                                                       0x0

#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000a064)
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_IN          \
        in_dword_masked(HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_ADDR, HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_RMSK)
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_ADDR, m)
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_OUT(v)      \
        out_dword(HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_ADDR,v)
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_ADDR,m,v,HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_IN)
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_SDCC5_SPARE_SCM_OUTPUT_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_SDCC5_SPARE_SCM_OUTPUT_SDCC5_SPARE_SCM_OUTPUT_SHFT                                                                                                 0x0

#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000a06c)
#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_IN          \
        in_dword_masked(HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_ADDR, HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_RMSK)
#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_ADDR, m)
#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_SDCC5_SPARE_SCM_INPUT_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SDCC5_SPARE_SCM_INPUT_SDCC5_SPARE_SCM_INPUT_SHFT                                                                                                   0x0

#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000a070)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_RMSK                                                                                                                             0x1
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR, HWIO_TCSR_UFS_SCM_FAULT_IRQ_RMSK)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR, m)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_UFS_SCM_FAULT_IRQ_BMSK                                                                                                           0x1
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_UFS_SCM_FAULT_IRQ_SHFT                                                                                                           0x0

#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000a074)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_RMSK                                                                                                                           0x1
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_IN          \
        in_dword_masked(HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR, HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_RMSK)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR, m)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_SDCC5_SCM_FAULT_IRQ_BMSK                                                                                                       0x1
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_SDCC5_SCM_FAULT_IRQ_SHFT                                                                                                       0x0

#define HWIO_TCSR_IPA_WARMBOOT_EN_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000a078)
#define HWIO_TCSR_IPA_WARMBOOT_EN_RMSK                                                                                                                               0x1
#define HWIO_TCSR_IPA_WARMBOOT_EN_IN          \
        in_dword_masked(HWIO_TCSR_IPA_WARMBOOT_EN_ADDR, HWIO_TCSR_IPA_WARMBOOT_EN_RMSK)
#define HWIO_TCSR_IPA_WARMBOOT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_WARMBOOT_EN_ADDR, m)
#define HWIO_TCSR_IPA_WARMBOOT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_WARMBOOT_EN_ADDR,v)
#define HWIO_TCSR_IPA_WARMBOOT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_WARMBOOT_EN_ADDR,m,v,HWIO_TCSR_IPA_WARMBOOT_EN_IN)
#define HWIO_TCSR_IPA_WARMBOOT_EN_IPA_WARMBOOT_EN_BMSK                                                                                                               0x1
#define HWIO_TCSR_IPA_WARMBOOT_EN_IPA_WARMBOOT_EN_SHFT                                                                                                               0x0

#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000a07c)
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_OUT(v)      \
        out_dword(HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_ADDR,v)
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_ADDR,m,v,HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_IN)
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_TERMINATOR_SPARE0_OUTPUT_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE0_OUTPUT_TERMINATOR_SPARE0_OUTPUT_SHFT                                                                                             0x0

#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000a080)
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_OUT(v)      \
        out_dword(HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_ADDR,v)
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_ADDR,m,v,HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_IN)
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_TERMINATOR_SPARE1_OUTPUT_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE1_OUTPUT_TERMINATOR_SPARE1_OUTPUT_SHFT                                                                                             0x0

#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000a084)
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_OUT(v)      \
        out_dword(HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_ADDR,v)
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_ADDR,m,v,HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_IN)
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_TERMINATOR_SPARE2_OUTPUT_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE2_OUTPUT_TERMINATOR_SPARE2_OUTPUT_SHFT                                                                                             0x0

#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000a088)
#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE0_INPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE0_INPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE0_INPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_TERMINATOR_SPARE0_INPUT_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE0_INPUT_TERMINATOR_SPARE0_INPUT_SHFT                                                                                               0x0

#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000a08c)
#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE1_INPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE1_INPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE1_INPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_TERMINATOR_SPARE1_INPUT_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE1_INPUT_TERMINATOR_SPARE1_INPUT_SHFT                                                                                               0x0

#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000a090)
#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE2_INPUT_ADDR, HWIO_TCSR_TERMINATOR_SPARE2_INPUT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE2_INPUT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_TERMINATOR_SPARE2_INPUT_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_TERMINATOR_SPARE2_INPUT_TERMINATOR_SPARE2_INPUT_SHFT                                                                                               0x0

#define HWIO_TCSR_PHSS_USB2_PHY_SEL_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b000)
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_RMSK                                                                                                                             0x1
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_IN          \
        in_dword_masked(HWIO_TCSR_PHSS_USB2_PHY_SEL_ADDR, HWIO_TCSR_PHSS_USB2_PHY_SEL_RMSK)
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_PHSS_USB2_PHY_SEL_ADDR, m)
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_PHSS_USB2_PHY_SEL_ADDR,v)
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_USB2_PHY_SEL_ADDR,m,v,HWIO_TCSR_PHSS_USB2_PHY_SEL_IN)
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_PHSS_USB2_PHY_SEL_BMSK                                                                                                           0x1
#define HWIO_TCSR_PHSS_USB2_PHY_SEL_PHSS_USB2_PHY_SEL_SHFT                                                                                                           0x0

#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000b004)
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_RMSK                                                                                                                       0x1
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_IN          \
        in_dword_masked(HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_ADDR, HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_RMSK)
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_ADDR, m)
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_ADDR,v)
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_ADDR,m,v,HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_IN)
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_PHSS_ATEST_USB2_PHY_SEL_BMSK                                                                                               0x1
#define HWIO_TCSR_PHSS_ATEST_USB2_PHY_SEL_PHSS_ATEST_USB2_PHY_SEL_SHFT                                                                                               0x0

#define HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b008)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_RMSK                                                                                                                             0x3
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR, HWIO_TCSR_PHSS_TEST_BUS_SEL_RMSK)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR, m)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR,v)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR,m,v,HWIO_TCSR_PHSS_TEST_BUS_SEL_IN)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_PHSS_TEST_BUS_SEL_BMSK                                                                                                           0x3
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_PHSS_TEST_BUS_SEL_SHFT                                                                                                           0x0

#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n)                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000b040 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_RMSK                                                                                                                   0xfff
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_MAXn                                                                                                                       1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_6_IRQ_ENABLE_BMSK                                                                                     0x800
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_6_IRQ_ENABLE_SHFT                                                                                       0xb
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_5_IRQ_ENABLE_BMSK                                                                                     0x400
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_5_IRQ_ENABLE_SHFT                                                                                       0xa
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_4_IRQ_ENABLE_BMSK                                                                                     0x200
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_4_IRQ_ENABLE_SHFT                                                                                       0x9
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_3_IRQ_ENABLE_BMSK                                                                                     0x100
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_3_IRQ_ENABLE_SHFT                                                                                       0x8
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_2_IRQ_ENABLE_BMSK                                                                                      0x80
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_2_IRQ_ENABLE_SHFT                                                                                       0x7
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_1_IRQ_ENABLE_BMSK                                                                                      0x40
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_1_IRQ_ENABLE_SHFT                                                                                       0x6
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_6_IRQ_ENABLE_BMSK                                                                                      0x20
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_6_IRQ_ENABLE_SHFT                                                                                       0x5
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_5_IRQ_ENABLE_BMSK                                                                                      0x10
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_5_IRQ_ENABLE_SHFT                                                                                       0x4
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_4_IRQ_ENABLE_BMSK                                                                                       0x8
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_4_IRQ_ENABLE_SHFT                                                                                       0x3
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_3_IRQ_ENABLE_BMSK                                                                                       0x4
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_3_IRQ_ENABLE_SHFT                                                                                       0x2
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_2_IRQ_ENABLE_BMSK                                                                                       0x2
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_2_IRQ_ENABLE_SHFT                                                                                       0x1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_1_IRQ_ENABLE_BMSK                                                                                       0x1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_1_IRQ_ENABLE_SHFT                                                                                       0x0

#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_ADDR(n)                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000b080 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_RMSK                                                                                                                    0xfff
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_MAXn                                                                                                                        1
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_6_IRQ_ENABLE_BMSK                                                                                       0x800
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_6_IRQ_ENABLE_SHFT                                                                                         0xb
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_5_IRQ_ENABLE_BMSK                                                                                       0x400
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_5_IRQ_ENABLE_SHFT                                                                                         0xa
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_4_IRQ_ENABLE_BMSK                                                                                       0x200
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_4_IRQ_ENABLE_SHFT                                                                                         0x9
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_3_IRQ_ENABLE_BMSK                                                                                       0x100
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_3_IRQ_ENABLE_SHFT                                                                                         0x8
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_2_IRQ_ENABLE_BMSK                                                                                        0x80
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_2_IRQ_ENABLE_SHFT                                                                                         0x7
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_1_IRQ_ENABLE_BMSK                                                                                        0x40
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP2_UART_1_IRQ_ENABLE_SHFT                                                                                         0x6
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_6_IRQ_ENABLE_BMSK                                                                                        0x20
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_6_IRQ_ENABLE_SHFT                                                                                         0x5
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_5_IRQ_ENABLE_BMSK                                                                                        0x10
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_5_IRQ_ENABLE_SHFT                                                                                         0x4
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_4_IRQ_ENABLE_BMSK                                                                                         0x8
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_4_IRQ_ENABLE_SHFT                                                                                         0x3
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_3_IRQ_ENABLE_BMSK                                                                                         0x4
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_3_IRQ_ENABLE_SHFT                                                                                         0x2
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_2_IRQ_ENABLE_BMSK                                                                                         0x2
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_2_IRQ_ENABLE_SHFT                                                                                         0x1
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_1_IRQ_ENABLE_BMSK                                                                                         0x1
#define HWIO_TCSR_PHSS_UART_WCSS_INT_SEL_n_WCSS_BLSP1_UART_1_IRQ_ENABLE_SHFT                                                                                         0x0

#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n)                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000b0c0 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_RMSK                                                                                                                     0xfff
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MAXn                                                                                                                         1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_6_IRQ_ENABLE_BMSK                                                                                         0x800
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_6_IRQ_ENABLE_SHFT                                                                                           0xb
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_5_IRQ_ENABLE_BMSK                                                                                         0x400
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_5_IRQ_ENABLE_SHFT                                                                                           0xa
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_4_IRQ_ENABLE_BMSK                                                                                         0x200
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_4_IRQ_ENABLE_SHFT                                                                                           0x9
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_3_IRQ_ENABLE_BMSK                                                                                         0x100
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_3_IRQ_ENABLE_SHFT                                                                                           0x8
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_2_IRQ_ENABLE_BMSK                                                                                          0x80
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_2_IRQ_ENABLE_SHFT                                                                                           0x7
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_1_IRQ_ENABLE_BMSK                                                                                          0x40
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP2_UART_1_IRQ_ENABLE_SHFT                                                                                           0x6
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_6_IRQ_ENABLE_BMSK                                                                                          0x20
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_6_IRQ_ENABLE_SHFT                                                                                           0x5
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_5_IRQ_ENABLE_BMSK                                                                                          0x10
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_5_IRQ_ENABLE_SHFT                                                                                           0x4
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_4_IRQ_ENABLE_BMSK                                                                                           0x8
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_4_IRQ_ENABLE_SHFT                                                                                           0x3
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_3_IRQ_ENABLE_BMSK                                                                                           0x4
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_3_IRQ_ENABLE_SHFT                                                                                           0x2
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_2_IRQ_ENABLE_BMSK                                                                                           0x2
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_2_IRQ_ENABLE_SHFT                                                                                           0x1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_1_IRQ_ENABLE_BMSK                                                                                           0x1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_1_IRQ_ENABLE_SHFT                                                                                           0x0

#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_ADDR(n)                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000b100 + 0x10 * (n))
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_RMSK                                                                                                                     0xfff
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_MAXn                                                                                                                         1
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_6_IRQ_ENABLE_BMSK                                                                                         0x800
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_6_IRQ_ENABLE_SHFT                                                                                           0xb
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_5_IRQ_ENABLE_BMSK                                                                                         0x400
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_5_IRQ_ENABLE_SHFT                                                                                           0xa
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_4_IRQ_ENABLE_BMSK                                                                                         0x200
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_4_IRQ_ENABLE_SHFT                                                                                           0x9
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_3_IRQ_ENABLE_BMSK                                                                                         0x100
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_3_IRQ_ENABLE_SHFT                                                                                           0x8
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_2_IRQ_ENABLE_BMSK                                                                                          0x80
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_2_IRQ_ENABLE_SHFT                                                                                           0x7
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_1_IRQ_ENABLE_BMSK                                                                                          0x40
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP2_QUP_1_IRQ_ENABLE_SHFT                                                                                           0x6
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_6_IRQ_ENABLE_BMSK                                                                                          0x20
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_6_IRQ_ENABLE_SHFT                                                                                           0x5
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_5_IRQ_ENABLE_BMSK                                                                                          0x10
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_5_IRQ_ENABLE_SHFT                                                                                           0x4
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_4_IRQ_ENABLE_BMSK                                                                                           0x8
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_4_IRQ_ENABLE_SHFT                                                                                           0x3
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_3_IRQ_ENABLE_BMSK                                                                                           0x4
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_3_IRQ_ENABLE_SHFT                                                                                           0x2
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_2_IRQ_ENABLE_BMSK                                                                                           0x2
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_2_IRQ_ENABLE_SHFT                                                                                           0x1
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_1_IRQ_ENABLE_BMSK                                                                                           0x1
#define HWIO_TCSR_PHSS_QUP_WCSS_INT_SEL_n_WCSS_BLSP1_QUP_1_IRQ_ENABLE_SHFT                                                                                           0x0

#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n)                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000b140 + 0x10 * (n))
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_RMSK                                                                                                                      0xfff
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MAXn                                                                                                                          1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_6_IRQ_ENABLE_BMSK                                                                                           0x800
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_6_IRQ_ENABLE_SHFT                                                                                             0xb
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_5_IRQ_ENABLE_BMSK                                                                                           0x400
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_5_IRQ_ENABLE_SHFT                                                                                             0xa
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_4_IRQ_ENABLE_BMSK                                                                                           0x200
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_4_IRQ_ENABLE_SHFT                                                                                             0x9
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_3_IRQ_ENABLE_BMSK                                                                                           0x100
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_3_IRQ_ENABLE_SHFT                                                                                             0x8
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_2_IRQ_ENABLE_BMSK                                                                                            0x80
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_2_IRQ_ENABLE_SHFT                                                                                             0x7
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_1_IRQ_ENABLE_BMSK                                                                                            0x40
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP2_QUP_1_IRQ_ENABLE_SHFT                                                                                             0x6
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_6_IRQ_ENABLE_BMSK                                                                                            0x20
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_6_IRQ_ENABLE_SHFT                                                                                             0x5
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_5_IRQ_ENABLE_BMSK                                                                                            0x10
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_5_IRQ_ENABLE_SHFT                                                                                             0x4
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_4_IRQ_ENABLE_BMSK                                                                                             0x8
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_4_IRQ_ENABLE_SHFT                                                                                             0x3
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_3_IRQ_ENABLE_BMSK                                                                                             0x4
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_3_IRQ_ENABLE_SHFT                                                                                             0x2
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_2_IRQ_ENABLE_BMSK                                                                                             0x2
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_2_IRQ_ENABLE_SHFT                                                                                             0x1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_1_IRQ_ENABLE_BMSK                                                                                             0x1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_1_IRQ_ENABLE_SHFT                                                                                             0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00000458)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_RMSK                                                                                                                    0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_MMSS_RICA_CORE_QRIB_MMU_NS_BMSK                                                                                         0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_MMSS_RICA_CORE_QRIB_MMU_NS_SHFT                                                                                         0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000045c)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_RMSK                                                                                                                 0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_MMSS_RICA_CORE_QRIB_MMU_NS_EN_BMSK                                                                                   0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_NS_EN_MMSS_RICA_CORE_QRIB_MMU_NS_EN_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00001874)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_RMSK                                                                                                                 0x1f
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_MMSS_RICA_CORE_QRIB_MMU_VMID_BMSK                                                                                    0x1f
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_MMSS_RICA_CORE_QRIB_MMU_VMID_SHFT                                                                                     0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00001870)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_RMSK                                                                                                               0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_MMSS_RICA_CORE_QRIB_MMU_VMID_EN_BMSK                                                                               0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_EN_MMSS_RICA_CORE_QRIB_MMU_VMID_EN_SHFT                                                                               0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00001868)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_RMSK                                                                                                          0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_MMSS_RICA_CORE_QRIB_MMU_VMID_INIT_EN_BMSK                                                                     0x1
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_EN_MMSS_RICA_CORE_QRIB_MMU_VMID_INIT_EN_SHFT                                                                     0x0

#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000186c)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_RMSK                                                                                                            0x1f
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_ADDR, HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_MMSS_RICA_CORE_QRIB_MMU_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_MMSS_RICA_CORE_MMU_QRIB_VMID_INIT_MMSS_RICA_CORE_QRIB_MMU_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00001898)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_RMSK                                                                                                                           0x1
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_APCC_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_APCC_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_APCC_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_APCC_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_APCC_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_APCC_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_APCC_QRIB_VMIDMT_EN_BMSK                                                                                                       0x1
#define HWIO_TCSR_APCC_QRIB_VMIDMT_EN_APCC_QRIB_VMIDMT_EN_SHFT                                                                                                       0x0

#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000189c)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                           0x1f001f
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_APCC_QRIB_VMIDMT_ACR_INIT_BMSK                                                                                 0x1f0000
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_APCC_QRIB_VMIDMT_ACR_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_APCC_QRIB_VMIDMT_VMID_INIT_BMSK                                                                                    0x1f
#define HWIO_TCSR_APCC_QRIB_VMIDMT_INIT_ACR_INIT_APCC_QRIB_VMIDMT_VMID_INIT_SHFT                                                                                     0x0

#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000018a0)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_RMSK                                                                                                               0x1
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_TERMINATOR_SPARE_QRIB_VMIDMT_EN_BMSK                                                                               0x1
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_EN_TERMINATOR_SPARE_QRIB_VMIDMT_EN_SHFT                                                                               0x0

#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x000018a4)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                               0x1f001f
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_TERMINATOR_SPARE_QRIB_VMIDMT_ACR_INIT_BMSK                                                         0x1f0000
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_TERMINATOR_SPARE_QRIB_VMIDMT_ACR_INIT_SHFT                                                             0x10
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_TERMINATOR_SPARE_QRIB_VMIDMT_VMID_INIT_BMSK                                                            0x1f
#define HWIO_TCSR_TERMINATOR_SPARE_QRIB_VMIDMT_INIT_ACR_INIT_TERMINATOR_SPARE_QRIB_VMIDMT_VMID_INIT_SHFT                                                             0x0

#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001404)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_RMSK                                                                                                                 0x1
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_MMSS_RICA_CORE_QRIB_VMIDMT_EN_BMSK                                                                                   0x1
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_EN_MMSS_RICA_CORE_QRIB_VMIDMT_EN_SHFT                                                                                   0x0

#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001400)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                 0x1f001f
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_RICA_CORE_QRIB_VMIDMT_ACR_INIT_BMSK                                                             0x1f0000
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_RICA_CORE_QRIB_VMIDMT_ACR_INIT_SHFT                                                                 0x10
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_RICA_CORE_QRIB_VMIDMT_VMID_INIT_BMSK                                                                0x1f
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_VMIDMT_INIT_ACR_INIT_MMSS_RICA_CORE_QRIB_VMIDMT_VMID_INIT_SHFT                                                                 0x0

#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x000013f8)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_RMSK                                                                                                           0xffffffff
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_MMSS_RICA_CORE_QRIB_XPU2_ACR_BMSK                                                                              0xffffffff
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_ACR_MMSS_RICA_CORE_QRIB_XPU2_ACR_SHFT                                                                                     0x0

#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x000003b0)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_RMSK                                                                                                             0x10001
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_MMSS_RICA_CORE_QRIB_XPU2_NSEN_INIT_BMSK                                                                          0x10000
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_MMSS_RICA_CORE_QRIB_XPU2_NSEN_INIT_SHFT                                                                             0x10
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_MMSS_RICA_CORE_QRIB_XPU2_EN_TZ_BMSK                                                                                  0x1
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_INIT_MMSS_RICA_CORE_QRIB_XPU2_EN_TZ_SHFT                                                                                  0x0

#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x000013fc)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                      0x10001
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                 0x10000
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                    0x10
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                               0x1
#define HWIO_TCSR_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_MMSS_RICA_CORE_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                               0x0

#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00001208)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_RMSK                                                                                                                           0x1
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_WCSS_QRIB_VMIDMT_EN_BMSK                                                                                                       0x1
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_EN_WCSS_QRIB_VMIDMT_EN_SHFT                                                                                                       0x0

#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000120c)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                           0x1f001f
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_WCSS_QRIB_VMIDMT_ACR_INIT_BMSK                                                                                 0x1f0000
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_WCSS_QRIB_VMIDMT_ACR_INIT_SHFT                                                                                     0x10
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_WCSS_QRIB_VMIDMT_VMID_INIT_BMSK                                                                                    0x1f
#define HWIO_TCSR_WCSS_QRIB_VMIDMT_INIT_ACR_INIT_WCSS_QRIB_VMIDMT_VMID_INIT_SHFT                                                                                     0x0

#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00001850)
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_WCSS_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_WCSS_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_WCSS_QRIB_XPU2_ACR_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_WCSS_QRIB_XPU2_ACR_WCSS_QRIB_XPU2_ACR_SHFT                                                                                                         0x0

#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000003b4)
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_RMSK                                                                                                                       0x10001
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_WCSS_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_WCSS_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_WCSS_QRIB_XPU2_NSEN_INIT_BMSK                                                                                              0x10000
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_WCSS_QRIB_XPU2_NSEN_INIT_SHFT                                                                                                 0x10
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_WCSS_QRIB_XPU2_EN_TZ_BMSK                                                                                                      0x1
#define HWIO_TCSR_WCSS_QRIB_XPU2_INIT_WCSS_QRIB_XPU2_EN_TZ_SHFT                                                                                                      0x0

#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000187c)
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                                0x10001
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_WCSS_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                                     0x10000
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_WCSS_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                        0x10
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_WCSS_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                                   0x1
#define HWIO_TCSR_WCSS_QRIB_XPU2_VMIDEN_INIT_WCSS_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                                   0x0

#define HWIO_TCSR_EBI1_TEST_MUX_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000b200)
#define HWIO_TCSR_EBI1_TEST_MUX_RMSK                                                                                                                          0xffffffff
#define HWIO_TCSR_EBI1_TEST_MUX_IN          \
        in_dword_masked(HWIO_TCSR_EBI1_TEST_MUX_ADDR, HWIO_TCSR_EBI1_TEST_MUX_RMSK)
#define HWIO_TCSR_EBI1_TEST_MUX_INM(m)      \
        in_dword_masked(HWIO_TCSR_EBI1_TEST_MUX_ADDR, m)
#define HWIO_TCSR_EBI1_TEST_MUX_OUT(v)      \
        out_dword(HWIO_TCSR_EBI1_TEST_MUX_ADDR,v)
#define HWIO_TCSR_EBI1_TEST_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_EBI1_TEST_MUX_ADDR,m,v,HWIO_TCSR_EBI1_TEST_MUX_IN)
#define HWIO_TCSR_EBI1_TEST_MUX_EBI1_TEST_MUX_DEBUG_SEL_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_EBI1_TEST_MUX_EBI1_TEST_MUX_DEBUG_SEL_SHFT                                                                                                         0x0

#define HWIO_TCSR_TCSR_LDO_MISC_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000b22c)
#define HWIO_TCSR_TCSR_LDO_MISC_RMSK                                                                                                                          0xffffffff
#define HWIO_TCSR_TCSR_LDO_MISC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_LDO_MISC_ADDR, HWIO_TCSR_TCSR_LDO_MISC_RMSK)
#define HWIO_TCSR_TCSR_LDO_MISC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_LDO_MISC_ADDR, m)
#define HWIO_TCSR_TCSR_LDO_MISC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_LDO_MISC_ADDR,v)
#define HWIO_TCSR_TCSR_LDO_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_LDO_MISC_ADDR,m,v,HWIO_TCSR_TCSR_LDO_MISC_IN)
#define HWIO_TCSR_TCSR_LDO_MISC_TCSR_LDO_MISC_BMSK                                                                                                            0xffffffff
#define HWIO_TCSR_TCSR_LDO_MISC_TCSR_LDO_MISC_SHFT                                                                                                                   0x0

#define HWIO_TCSR_TCSR_WCSS_MISC_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000b23c)
#define HWIO_TCSR_TCSR_WCSS_MISC_RMSK                                                                                                                         0xffffffff
#define HWIO_TCSR_TCSR_WCSS_MISC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_WCSS_MISC_ADDR, HWIO_TCSR_TCSR_WCSS_MISC_RMSK)
#define HWIO_TCSR_TCSR_WCSS_MISC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_WCSS_MISC_ADDR, m)
#define HWIO_TCSR_TCSR_WCSS_MISC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_WCSS_MISC_ADDR,v)
#define HWIO_TCSR_TCSR_WCSS_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_WCSS_MISC_ADDR,m,v,HWIO_TCSR_TCSR_WCSS_MISC_IN)
#define HWIO_TCSR_TCSR_WCSS_MISC_TCSR_WCSS_MISC_BMSK                                                                                                          0xffffffff
#define HWIO_TCSR_TCSR_WCSS_MISC_TCSR_WCSS_MISC_SHFT                                                                                                                 0x0

#define HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b240)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_MISC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR, HWIO_TCSR_TCSR_USB_PHY_MISC_RMSK)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR, m)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR,v)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR,m,v,HWIO_TCSR_TCSR_USB_PHY_MISC_IN)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_TCSR_USB_PHY_MISC_BMSK                                                                                                    0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_MISC_TCSR_USB_PHY_MISC_SHFT                                                                                                           0x0

#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000b244)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, m)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,v)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,m,v,HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_BMSK                                                                                          0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_SHFT                                                                                                 0x0

#define HWIO_TCSR_CX_DD_CONFIG0_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000b248)
#define HWIO_TCSR_CX_DD_CONFIG0_RMSK                                                                                                                          0xffffffff
#define HWIO_TCSR_CX_DD_CONFIG0_IN          \
        in_dword_masked(HWIO_TCSR_CX_DD_CONFIG0_ADDR, HWIO_TCSR_CX_DD_CONFIG0_RMSK)
#define HWIO_TCSR_CX_DD_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_DD_CONFIG0_ADDR, m)
#define HWIO_TCSR_CX_DD_CONFIG0_OUT(v)      \
        out_dword(HWIO_TCSR_CX_DD_CONFIG0_ADDR,v)
#define HWIO_TCSR_CX_DD_CONFIG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CX_DD_CONFIG0_ADDR,m,v,HWIO_TCSR_CX_DD_CONFIG0_IN)
#define HWIO_TCSR_CX_DD_CONFIG0_CX_DD_CONFIG0_BMSK                                                                                                            0xffffffff
#define HWIO_TCSR_CX_DD_CONFIG0_CX_DD_CONFIG0_SHFT                                                                                                                   0x0

#define HWIO_TCSR_CX_DD_CONFIG1_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000b24c)
#define HWIO_TCSR_CX_DD_CONFIG1_RMSK                                                                                                                                0x1f
#define HWIO_TCSR_CX_DD_CONFIG1_IN          \
        in_dword_masked(HWIO_TCSR_CX_DD_CONFIG1_ADDR, HWIO_TCSR_CX_DD_CONFIG1_RMSK)
#define HWIO_TCSR_CX_DD_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_DD_CONFIG1_ADDR, m)
#define HWIO_TCSR_CX_DD_CONFIG1_OUT(v)      \
        out_dword(HWIO_TCSR_CX_DD_CONFIG1_ADDR,v)
#define HWIO_TCSR_CX_DD_CONFIG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CX_DD_CONFIG1_ADDR,m,v,HWIO_TCSR_CX_DD_CONFIG1_IN)
#define HWIO_TCSR_CX_DD_CONFIG1_CX_DD_CONFIG1_BMSK                                                                                                                  0x1f
#define HWIO_TCSR_CX_DD_CONFIG1_CX_DD_CONFIG1_SHFT                                                                                                                   0x0

#define HWIO_TCSR_CX_DD_STATUS_ADDR                                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000b250)
#define HWIO_TCSR_CX_DD_STATUS_RMSK                                                                                                                                 0xff
#define HWIO_TCSR_CX_DD_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_CX_DD_STATUS_ADDR, HWIO_TCSR_CX_DD_STATUS_RMSK)
#define HWIO_TCSR_CX_DD_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_DD_STATUS_ADDR, m)
#define HWIO_TCSR_CX_DD_STATUS_CX_DD_STATUS_BMSK                                                                                                                    0xff
#define HWIO_TCSR_CX_DD_STATUS_CX_DD_STATUS_SHFT                                                                                                                     0x0

#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b254)
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_RMSK                                                                                                                           0x3f
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_IN          \
        in_dword_masked(HWIO_TCSR_CX_DD_FIFO_RD_ADDR_ADDR, HWIO_TCSR_CX_DD_FIFO_RD_ADDR_RMSK)
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_DD_FIFO_RD_ADDR_ADDR, m)
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_OUT(v)      \
        out_dword(HWIO_TCSR_CX_DD_FIFO_RD_ADDR_ADDR,v)
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CX_DD_FIFO_RD_ADDR_ADDR,m,v,HWIO_TCSR_CX_DD_FIFO_RD_ADDR_IN)
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_CX_DD_FIFO_RD_ADDR_BMSK                                                                                                        0x3f
#define HWIO_TCSR_CX_DD_FIFO_RD_ADDR_CX_DD_FIFO_RD_ADDR_SHFT                                                                                                         0x0

#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b258)
#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_RMSK                                                                                                                           0xff
#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_IN          \
        in_dword_masked(HWIO_TCSR_CX_DD_FIFO_RD_DATA_ADDR, HWIO_TCSR_CX_DD_FIFO_RD_DATA_RMSK)
#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_INM(m)      \
        in_dword_masked(HWIO_TCSR_CX_DD_FIFO_RD_DATA_ADDR, m)
#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_CX_DD_FIFO_RD_DATA_BMSK                                                                                                        0xff
#define HWIO_TCSR_CX_DD_FIFO_RD_DATA_CX_DD_FIFO_RD_DATA_SHFT                                                                                                         0x0

#define HWIO_TCSR_VDDA_DD_CONFIG0_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000b25c)
#define HWIO_TCSR_VDDA_DD_CONFIG0_RMSK                                                                                                                        0xffffffff
#define HWIO_TCSR_VDDA_DD_CONFIG0_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_DD_CONFIG0_ADDR, HWIO_TCSR_VDDA_DD_CONFIG0_RMSK)
#define HWIO_TCSR_VDDA_DD_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_DD_CONFIG0_ADDR, m)
#define HWIO_TCSR_VDDA_DD_CONFIG0_OUT(v)      \
        out_dword(HWIO_TCSR_VDDA_DD_CONFIG0_ADDR,v)
#define HWIO_TCSR_VDDA_DD_CONFIG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VDDA_DD_CONFIG0_ADDR,m,v,HWIO_TCSR_VDDA_DD_CONFIG0_IN)
#define HWIO_TCSR_VDDA_DD_CONFIG0_VDDA_DD_CONFIG0_BMSK                                                                                                        0xffffffff
#define HWIO_TCSR_VDDA_DD_CONFIG0_VDDA_DD_CONFIG0_SHFT                                                                                                               0x0

#define HWIO_TCSR_VDDA_DD_CONFIG1_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000b260)
#define HWIO_TCSR_VDDA_DD_CONFIG1_RMSK                                                                                                                              0x1f
#define HWIO_TCSR_VDDA_DD_CONFIG1_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_DD_CONFIG1_ADDR, HWIO_TCSR_VDDA_DD_CONFIG1_RMSK)
#define HWIO_TCSR_VDDA_DD_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_DD_CONFIG1_ADDR, m)
#define HWIO_TCSR_VDDA_DD_CONFIG1_OUT(v)      \
        out_dword(HWIO_TCSR_VDDA_DD_CONFIG1_ADDR,v)
#define HWIO_TCSR_VDDA_DD_CONFIG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VDDA_DD_CONFIG1_ADDR,m,v,HWIO_TCSR_VDDA_DD_CONFIG1_IN)
#define HWIO_TCSR_VDDA_DD_CONFIG1_VDDA_DD_CONFIG1_BMSK                                                                                                              0x1f
#define HWIO_TCSR_VDDA_DD_CONFIG1_VDDA_DD_CONFIG1_SHFT                                                                                                               0x0

#define HWIO_TCSR_VDDA_DD_STATUS_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000b264)
#define HWIO_TCSR_VDDA_DD_STATUS_RMSK                                                                                                                               0xff
#define HWIO_TCSR_VDDA_DD_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_DD_STATUS_ADDR, HWIO_TCSR_VDDA_DD_STATUS_RMSK)
#define HWIO_TCSR_VDDA_DD_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_DD_STATUS_ADDR, m)
#define HWIO_TCSR_VDDA_DD_STATUS_VDDA_DD_STATUS_BMSK                                                                                                                0xff
#define HWIO_TCSR_VDDA_DD_STATUS_VDDA_DD_STATUS_SHFT                                                                                                                 0x0

#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000b268)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_RMSK                                                                                                                         0x3f
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_ADDR, HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_RMSK)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_ADDR, m)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_OUT(v)      \
        out_dword(HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_ADDR,v)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_ADDR,m,v,HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_IN)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_VDDA_DD_FIFO_RD_ADDR_BMSK                                                                                                    0x3f
#define HWIO_TCSR_VDDA_DD_FIFO_RD_ADDR_VDDA_DD_FIFO_RD_ADDR_SHFT                                                                                                     0x0

#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000b26c)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_RMSK                                                                                                                         0xff
#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_IN          \
        in_dword_masked(HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_ADDR, HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_RMSK)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_INM(m)      \
        in_dword_masked(HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_ADDR, m)
#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_VDDA_DD_FIFO_RD_DATA_BMSK                                                                                                    0xff
#define HWIO_TCSR_VDDA_DD_FIFO_RD_DATA_VDDA_DD_FIFO_RD_DATA_SHFT                                                                                                     0x0

#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000b204)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_RMSK                                                                                                             0xffffffff
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR, HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_RMSK)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR, m)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR,v)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR,m,v,HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_IN)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_COPSS_USB_CONTROL_WITH_JDR_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_COPSS_USB_CONTROL_WITH_JDR_SHFT                                                                                         0x0

#define HWIO_TCSR_COPSS_USB_CONTROL_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b208)
#define HWIO_TCSR_COPSS_USB_CONTROL_RMSK                                                                                                                             0x1
#define HWIO_TCSR_COPSS_USB_CONTROL_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_ADDR, HWIO_TCSR_COPSS_USB_CONTROL_RMSK)
#define HWIO_TCSR_COPSS_USB_CONTROL_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_ADDR, m)
#define HWIO_TCSR_COPSS_USB_CONTROL_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_USB_CONTROL_ADDR,v)
#define HWIO_TCSR_COPSS_USB_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_USB_CONTROL_ADDR,m,v,HWIO_TCSR_COPSS_USB_CONTROL_IN)
#define HWIO_TCSR_COPSS_USB_CONTROL_COPSS_USB_CONTROL_BMSK                                                                                                           0x1
#define HWIO_TCSR_COPSS_USB_CONTROL_COPSS_USB_CONTROL_SHFT                                                                                                           0x0

#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000b20c)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_RMSK                                                                                                                     0x1
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR, HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_RMSK)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR, m)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR,v)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR,m,v,HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_IN)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_UFS_SATA_CTRL_SEL_BMSK                                                                                                   0x1
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_UFS_SATA_CTRL_SEL_SHFT                                                                                                   0x0

#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n)                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000b160 + 0x4 * (n))
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_RMSK                                                                                                             0x8000007f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_MAXn                                                                                                                     31
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n), HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_RMSK)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INI(n))
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_EN_BMSK                                                                                      0x80000000
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_EN_SHFT                                                                                            0x1f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_SEL_BMSK                                                                                           0x7f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_SEL_SHFT                                                                                            0x0

#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n)                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b360 + 0x4 * (n))
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_RMSK                                                                                                         0x80000003
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_MAXn                                                                                                                 31
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n), HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_RMSK)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n), mask)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n),val)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n),mask,val,HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INI(n))
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_EN_BMSK                                                                              0x80000000
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_EN_SHFT                                                                                    0x1f
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_SEL_BMSK                                                                                    0x3
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_SEL_SHFT                                                                                    0x0

#define HWIO_TCSR_LDO_SLEEP_CTRL_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000c000)
#define HWIO_TCSR_LDO_SLEEP_CTRL_RMSK                                                                                                                                0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, HWIO_TCSR_LDO_SLEEP_CTRL_RMSK)
#define HWIO_TCSR_LDO_SLEEP_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,m,v,HWIO_TCSR_LDO_SLEEP_CTRL_IN)
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_BMSK                                                                                                                      0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_SHFT                                                                                                                      0x0

#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000c004)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK                                                                                                                         0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,m,v,HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_BMSK                                                                                                        0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_SHFT                                                                                                        0x0

#define HWIO_TCSR_LDO_OBIAS_CTRL_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000c008)
#define HWIO_TCSR_LDO_OBIAS_CTRL_RMSK                                                                                                                                0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, HWIO_TCSR_LDO_OBIAS_CTRL_RMSK)
#define HWIO_TCSR_LDO_OBIAS_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OBIAS_CTRL_IN)
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_BMSK                                                                                                                   0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_SHFT                                                                                                                   0x0

#define HWIO_TCSR_LDO_VREF_CONFIG_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000c00c)
#define HWIO_TCSR_LDO_VREF_CONFIG_RMSK                                                                                                                               0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, HWIO_TCSR_LDO_VREF_CONFIG_RMSK)
#define HWIO_TCSR_LDO_VREF_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_VREF_CONFIG_IN)
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_BMSK                                                                                                               0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_SHFT                                                                                                               0x0

#define HWIO_TCSR_LDO_IB_CONFIG_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000c010)
#define HWIO_TCSR_LDO_IB_CONFIG_RMSK                                                                                                                                 0x7
#define HWIO_TCSR_LDO_IB_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, HWIO_TCSR_LDO_IB_CONFIG_RMSK)
#define HWIO_TCSR_LDO_IB_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_IB_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_IB_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_IB_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_IB_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_IB_CONFIG_IN)
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_BMSK                                                                                                                   0x7
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_SHFT                                                                                                                   0x0

#define HWIO_TCSR_LDO_BGC_CONFIG_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000c014)
#define HWIO_TCSR_LDO_BGC_CONFIG_RMSK                                                                                                                                0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, HWIO_TCSR_LDO_BGC_CONFIG_RMSK)
#define HWIO_TCSR_LDO_BGC_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_BGC_CONFIG_IN)
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_BMSK                                                                                                                        0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_SHFT                                                                                                                        0x0

#define HWIO_TCSR_LDO_VREF_CTRL_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000c018)
#define HWIO_TCSR_LDO_VREF_CTRL_RMSK                                                                                                                             0x10001
#define HWIO_TCSR_LDO_VREF_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, HWIO_TCSR_LDO_VREF_CTRL_RMSK)
#define HWIO_TCSR_LDO_VREF_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CTRL_ADDR,m,v,HWIO_TCSR_LDO_VREF_CTRL_IN)
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_BMSK                                                                                                            0x10000
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_SHFT                                                                                                               0x10
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_BMSK                                                                                                                 0x1
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_SHFT                                                                                                                 0x0

#define HWIO_TCSR_LDO_LD_EN_ADDR                                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000c01c)
#define HWIO_TCSR_LDO_LD_EN_RMSK                                                                                                                              0x80000000
#define HWIO_TCSR_LDO_LD_EN_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, HWIO_TCSR_LDO_LD_EN_RMSK)
#define HWIO_TCSR_LDO_LD_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, m)
#define HWIO_TCSR_LDO_LD_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_EN_ADDR,v)
#define HWIO_TCSR_LDO_LD_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_EN_ADDR,m,v,HWIO_TCSR_LDO_LD_EN_IN)
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_BMSK                                                                                                                    0x80000000
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_SHFT                                                                                                                          0x1f

#define HWIO_TCSR_LDO_LD_CTRL_ADDR                                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000c020)
#define HWIO_TCSR_LDO_LD_CTRL_RMSK                                                                                                                              0xff00ff
#define HWIO_TCSR_LDO_LD_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, HWIO_TCSR_LDO_LD_CTRL_RMSK)
#define HWIO_TCSR_LDO_LD_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_LD_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_LD_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_CTRL_ADDR,m,v,HWIO_TCSR_LDO_LD_CTRL_IN)
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_BMSK                                                                                                                   0xff0000
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_SHFT                                                                                                                       0x10
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_BMSK                                                                                                                       0xff
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_SHFT                                                                                                                        0x0

#define HWIO_TCSR_LDO_OSC_RESETB_ADDR                                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000c024)
#define HWIO_TCSR_LDO_OSC_RESETB_RMSK                                                                                                                         0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, HWIO_TCSR_LDO_OSC_RESETB_RMSK)
#define HWIO_TCSR_LDO_OSC_RESETB_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, m)
#define HWIO_TCSR_LDO_OSC_RESETB_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_RESETB_ADDR,v)
#define HWIO_TCSR_LDO_OSC_RESETB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_RESETB_ADDR,m,v,HWIO_TCSR_LDO_OSC_RESETB_IN)
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_BMSK                                                                                                          0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_SHFT                                                                                                                0x1f

#define HWIO_TCSR_LDO_OSC_CTRL_ADDR                                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000c028)
#define HWIO_TCSR_LDO_OSC_CTRL_RMSK                                                                                                                                  0x3
#define HWIO_TCSR_LDO_OSC_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, HWIO_TCSR_LDO_OSC_CTRL_RMSK)
#define HWIO_TCSR_LDO_OSC_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OSC_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OSC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OSC_CTRL_IN)
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_BMSK                                                                                                                     0x3
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_SHFT                                                                                                                     0x0

#define HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000c02c)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK                                                                                                                        0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_EN_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_BMSK                                                                                                             0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_SHFT                                                                                                                   0x1f

#define HWIO_TCSR_LDO_DFT_CTRL_ADDR                                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000c030)
#define HWIO_TCSR_LDO_DFT_CTRL_RMSK                                                                                                                                  0x7
#define HWIO_TCSR_LDO_DFT_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, HWIO_TCSR_LDO_DFT_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_BMSK                                                                                                                   0x7
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_SHFT                                                                                                                   0x0

#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000d000)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDMSS_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_COMPILER_VDDMSS_ACC_0_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_COMPILER_VDDMSS_ACC_0_SHFT                                                                                                   0x0

#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_ADDR                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000d010)
#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_RMSK                                                                                     0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_BMSK                            0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_CUSTOM_ACC_QCRF4221_82RBYNGDB00_128X32_1_CUSTOM4P_VDDMSS_SHFT                             0x0

#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_ADDR                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000d014)
#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_RMSK                                                                                      0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_BMSK                              0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_CUSTOM_ACC_QCRF8441_82RBYNGDB00_64X32_1_CUSTOM8P_VDDMSS_SHFT                               0x0

#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_ADDR                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000d01c)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_RMSK                                                                                   0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_BMSK                        0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_CUSTOM_ACC_QCSRAM1111_12RWDNGD00_512X128_4_LLSP0810_VDDMSS_SHFT                         0x0

#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_ADDR                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000d020)
#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_RMSK                                                                                      0xff
#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_BMSK                              0xff
#define HWIO_TCSR_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_CUSTOM_QCSRAM1111_22RWDNGD02_7168X32_16_LLSP0810_VDDMSS_SHFT                               0x0

#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_ADDR                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000d024)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_RMSK                                                                               0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_BMSK                0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_CUSTOM_ACC_QCSRAM1111_22RWDNGD12_6144X32_8_LLSP0810BOLT_VDDMSS_SHFT                 0x0

#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000d080)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK                                                                                                                   0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_SHFT                                                                                                     0x0

#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000d090)
#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_COMPILER_VDDCX_WCSS_ACC_0_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_WCSS_ACC_0_COMPILER_VDDCX_WCSS_ACC_0_SHFT                                                                                           0x0

#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_ADDR                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000d0a4)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_RMSK                                                                                   0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_BMSK                        0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_CUSTOM_ACC_QCSRAM1111_32RWDNGD02_3072X128_8_LLSP0810_VDDCX_SHFT                         0x0

#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_ADDR                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000d0a8)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_RMSK                                                                                   0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_BMSK                        0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDCX_SHFT                         0x0

#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000d100)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDGFX_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_COMPILER_VDDGFX_ACC_0_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_COMPILER_VDDGFX_ACC_0_SHFT                                                                                                   0x0

#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000d110)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_RMSK                                                                                   0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR, HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_BMSK                        0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_SHFT                         0x0

#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_ADDR                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000d11c)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_RMSK                                                                                  0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_ADDR, HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_BMSK                      0xff
#define HWIO_TCSR_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_CUSTOM_ACC_QCSRAM1111_42RBYNGD02_2048X256_4_LLSP0810_VDDGFX_SHFT                       0x0

#define HWIO_TCSR_MEM_ARRY_STBY_ADDR                                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000d180)
#define HWIO_TCSR_MEM_ARRY_STBY_RMSK                                                                                                                                 0x1
#define HWIO_TCSR_MEM_ARRY_STBY_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, HWIO_TCSR_MEM_ARRY_STBY_RMSK)
#define HWIO_TCSR_MEM_ARRY_STBY_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, m)
#define HWIO_TCSR_MEM_ARRY_STBY_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ARRY_STBY_ADDR,v)
#define HWIO_TCSR_MEM_ARRY_STBY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ARRY_STBY_ADDR,m,v,HWIO_TCSR_MEM_ARRY_STBY_IN)
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_BMSK                                                                                                                 0x1
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_SHFT                                                                                                                 0x0

#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000e000)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_RMSK                                                                                                                             0x3
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR, HWIO_TCSR_MEM_ACC_SEL_VDDCX_RMSK)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR, m)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR,v)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR,m,v,HWIO_TCSR_MEM_ACC_SEL_VDDCX_IN)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_MEM_ACC_SEL_VDDCX_BMSK                                                                                                           0x3
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_MEM_ACC_SEL_VDDCX_SHFT                                                                                                           0x0

#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000f000)
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_RMSK                                                                                                                            0x3
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDGFX_ADDR, HWIO_TCSR_MEM_ACC_SEL_VDDGFX_RMSK)
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDGFX_ADDR, m)
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ACC_SEL_VDDGFX_ADDR,v)
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ACC_SEL_VDDGFX_ADDR,m,v,HWIO_TCSR_MEM_ACC_SEL_VDDGFX_IN)
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_MEM_ACC_SEL_VDDGFX_BMSK                                                                                                         0x3
#define HWIO_TCSR_MEM_ACC_SEL_VDDGFX_MEM_ACC_SEL_VDDGFX_SHFT                                                                                                         0x0

#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x000001a0)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_RMSK                                                                                                                  0x10001
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_LPASS_SB1_QRIB_XPU2_NSEN_INIT_BMSK                                                                                    0x10000
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_LPASS_SB1_QRIB_XPU2_NSEN_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_LPASS_SB1_QRIB_XPU2_EN_TZ_BMSK                                                                                            0x1
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_INIT_LPASS_SB1_QRIB_XPU2_EN_TZ_SHFT                                                                                            0x0

#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00001728)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_RMSK                                                                                                                0xffffffff
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_LPASS_SB1_QRIB_XPU2_ACR_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_ACR_LPASS_SB1_QRIB_XPU2_ACR_SHFT                                                                                               0x0

#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000172c)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                           0x10001
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                           0x10000
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                              0x10
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                         0x1
#define HWIO_TCSR_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_LPASS_SB1_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                         0x0

#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00001730)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                      0x1f001f
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB1_QRIB_VMIDMT_ACR_INIT_BMSK                                                                       0x1f0000
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB1_QRIB_VMIDMT_ACR_INIT_SHFT                                                                           0x10
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB1_QRIB_VMIDMT_VMID_INIT_BMSK                                                                          0x1f
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_INIT_ACR_INIT_LPASS_SB1_QRIB_VMIDMT_VMID_INIT_SHFT                                                                           0x0

#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001734)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_RMSK                                                                                                                      0x1
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_LPASS_SB1_QRIB_VMIDMT_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_LPASS_SB1_QRIB_VMIDMT_EN_LPASS_SB1_QRIB_VMIDMT_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000003bc)
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_RMSK                                                                                                                        0x10001
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_UFS_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_UFS_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_UFS_QRIB_XPU2_NSEN_INIT_BMSK                                                                                                0x10000
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_UFS_QRIB_XPU2_NSEN_INIT_SHFT                                                                                                   0x10
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_UFS_QRIB_XPU2_EN_TZ_BMSK                                                                                                        0x1
#define HWIO_TCSR_UFS_QRIB_XPU2_INIT_UFS_QRIB_XPU2_EN_TZ_SHFT                                                                                                        0x0

#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00001884)
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_UFS_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_UFS_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_UFS_QRIB_XPU2_ACR_BMSK                                                                                                    0xffffffff
#define HWIO_TCSR_UFS_QRIB_XPU2_ACR_UFS_QRIB_XPU2_ACR_SHFT                                                                                                           0x0

#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000188c)
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_UFS_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                                       0x10000
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_UFS_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                          0x10
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_UFS_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                                     0x1
#define HWIO_TCSR_UFS_QRIB_XPU2_VMIDEN_INIT_UFS_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                                     0x0

#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x000003c0)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_RMSK                                                                                                                    0x10001
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_UFS_ICE_QRIB_XPU2_NSEN_INIT_BMSK                                                                                        0x10000
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_UFS_ICE_QRIB_XPU2_NSEN_INIT_SHFT                                                                                           0x10
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_UFS_ICE_QRIB_XPU2_EN_TZ_BMSK                                                                                                0x1
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_INIT_UFS_ICE_QRIB_XPU2_EN_TZ_SHFT                                                                                                0x0

#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00001890)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_UFS_ICE_QRIB_XPU2_ACR_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_ACR_UFS_ICE_QRIB_XPU2_ACR_SHFT                                                                                                   0x0

#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001894)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                             0x10001
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                               0x10000
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                  0x10
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                             0x1
#define HWIO_TCSR_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_UFS_ICE_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                             0x0

#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00001214)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                            0x1f001f
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_UFS_QRIB_VMIDMT_ACR_INIT_BMSK                                                                                   0x1f0000
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_UFS_QRIB_VMIDMT_ACR_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_UFS_QRIB_VMIDMT_VMID_INIT_BMSK                                                                                      0x1f
#define HWIO_TCSR_UFS_QRIB_VMIDMT_INIT_ACR_INIT_UFS_QRIB_VMIDMT_VMID_INIT_SHFT                                                                                       0x0

#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00001218)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_RMSK                                                                                                                            0x1
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_UFS_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_UFS_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_UFS_QRIB_VMIDMT_EN_BMSK                                                                                                         0x1
#define HWIO_TCSR_UFS_QRIB_VMIDMT_EN_UFS_QRIB_VMIDMT_EN_SHFT                                                                                                         0x0

#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x000003b8)
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_RMSK                                                                                                                        0x10001
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_IPA_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_IPA_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_IPA_QRIB_XPU2_NSEN_INIT_BMSK                                                                                                0x10000
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_IPA_QRIB_XPU2_NSEN_INIT_SHFT                                                                                                   0x10
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_IPA_QRIB_XPU2_EN_TZ_BMSK                                                                                                        0x1
#define HWIO_TCSR_IPA_QRIB_XPU2_INIT_IPA_QRIB_XPU2_EN_TZ_SHFT                                                                                                        0x0

#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00001880)
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_RMSK                                                                                                                      0xffffffff
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_IPA_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_IPA_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_IPA_QRIB_XPU2_ACR_BMSK                                                                                                    0xffffffff
#define HWIO_TCSR_IPA_QRIB_XPU2_ACR_IPA_QRIB_XPU2_ACR_SHFT                                                                                                           0x0

#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001888)
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_RMSK                                                                                                                 0x10001
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IPA_QRIB_XPU2_VMIDEN_INIT_BMSK                                                                                       0x10000
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IPA_QRIB_XPU2_VMIDEN_INIT_SHFT                                                                                          0x10
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IPA_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                                                                                     0x1
#define HWIO_TCSR_IPA_QRIB_XPU2_VMIDEN_INIT_IPA_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                                                                                     0x0

#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000123c)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_RMSK                                                                                                            0x1f001f
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IN          \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_RMSK)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_ADDR, m)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,v)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_ADDR,m,v,HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IN)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IPA_QRIB_VMIDMT_ACR_INIT_BMSK                                                                                   0x1f0000
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IPA_QRIB_VMIDMT_ACR_INIT_SHFT                                                                                       0x10
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IPA_QRIB_VMIDMT_VMID_INIT_BMSK                                                                                      0x1f
#define HWIO_TCSR_IPA_QRIB_VMIDMT_INIT_ACR_INIT_IPA_QRIB_VMIDMT_VMID_INIT_SHFT                                                                                       0x0

#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00001210)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_RMSK                                                                                                                            0x1
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_IN          \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_VMIDMT_EN_ADDR, HWIO_TCSR_IPA_QRIB_VMIDMT_EN_RMSK)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_QRIB_VMIDMT_EN_ADDR, m)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_QRIB_VMIDMT_EN_ADDR,v)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_QRIB_VMIDMT_EN_ADDR,m,v,HWIO_TCSR_IPA_QRIB_VMIDMT_EN_IN)
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_IPA_QRIB_VMIDMT_EN_BMSK                                                                                                         0x1
#define HWIO_TCSR_IPA_QRIB_VMIDMT_EN_IPA_QRIB_VMIDMT_EN_SHFT                                                                                                         0x0

#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000e004)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK                                                                                                                             0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_BMSK                                                                                                           0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_SHFT                                                                                                           0x0

#define HWIO_TCSR_KPSS_AUX_CLK_EN_ADDR                                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0000e008)
#define HWIO_TCSR_KPSS_AUX_CLK_EN_RMSK                                                                                                                               0x1
#define HWIO_TCSR_KPSS_AUX_CLK_EN_IN          \
        in_dword_masked(HWIO_TCSR_KPSS_AUX_CLK_EN_ADDR, HWIO_TCSR_KPSS_AUX_CLK_EN_RMSK)
#define HWIO_TCSR_KPSS_AUX_CLK_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_KPSS_AUX_CLK_EN_ADDR, m)
#define HWIO_TCSR_KPSS_AUX_CLK_EN_OUT(v)      \
        out_dword(HWIO_TCSR_KPSS_AUX_CLK_EN_ADDR,v)
#define HWIO_TCSR_KPSS_AUX_CLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_KPSS_AUX_CLK_EN_ADDR,m,v,HWIO_TCSR_KPSS_AUX_CLK_EN_IN)
#define HWIO_TCSR_KPSS_AUX_CLK_EN_KPSS_AUX_CLK_EN_BMSK                                                                                                               0x1
#define HWIO_TCSR_KPSS_AUX_CLK_EN_KPSS_AUX_CLK_EN_SHFT                                                                                                               0x0

#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000f004)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_RMSK                                                                                                                            0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDGFX_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDGFX_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_MEM_SVS_SEL_VDDGFX_BMSK                                                                                                         0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_MEM_SVS_SEL_VDDGFX_SHFT                                                                                                         0x0

#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b218)
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_CNOC_ADDR, HWIO_TCSR_TCSR_PWRSTALL_CNOC_RMSK)
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_CNOC_ADDR, m)
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_PWRSTALL_CNOC_ADDR,v)
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_PWRSTALL_CNOC_ADDR,m,v,HWIO_TCSR_TCSR_PWRSTALL_CNOC_IN)
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_TCSR_PWRSTALL_CNOC_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_CNOC_TCSR_PWRSTALL_CNOC_SHFT                                                                                                         0x0

#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b234)
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_PNOC_ADDR, HWIO_TCSR_TCSR_PWRSTALL_PNOC_RMSK)
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_PNOC_ADDR, m)
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_PWRSTALL_PNOC_ADDR,v)
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_PWRSTALL_PNOC_ADDR,m,v,HWIO_TCSR_TCSR_PWRSTALL_PNOC_IN)
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_TCSR_PWRSTALL_PNOC_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_PNOC_TCSR_PWRSTALL_PNOC_SHFT                                                                                                         0x0

#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b210)
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_SNOC_ADDR, HWIO_TCSR_TCSR_PWRSTALL_SNOC_RMSK)
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_SNOC_ADDR, m)
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_PWRSTALL_SNOC_ADDR,v)
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_PWRSTALL_SNOC_ADDR,m,v,HWIO_TCSR_TCSR_PWRSTALL_SNOC_IN)
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_TCSR_PWRSTALL_SNOC_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_SNOC_TCSR_PWRSTALL_SNOC_SHFT                                                                                                         0x0

#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b21c)
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_MNOC_ADDR, HWIO_TCSR_TCSR_PWRSTALL_MNOC_RMSK)
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_PWRSTALL_MNOC_ADDR, m)
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_PWRSTALL_MNOC_ADDR,v)
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_PWRSTALL_MNOC_ADDR,m,v,HWIO_TCSR_TCSR_PWRSTALL_MNOC_IN)
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_TCSR_PWRSTALL_MNOC_BMSK                                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_PWRSTALL_MNOC_TCSR_PWRSTALL_MNOC_SHFT                                                                                                         0x0

#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000b220)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_RMSK                                                                                                                   0xffffffff
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_IN          \
        in_dword_masked(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR, HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_RMSK)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR, m)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR,v)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR,m,v,HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_IN)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_DDR_SS_DEBUG_BUS_SEL_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_DDR_SS_DEBUG_BUS_SEL_SHFT                                                                                                     0x0


#endif /* __TCSR_HWIO_H__ */
