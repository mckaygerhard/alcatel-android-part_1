#ifndef __APCS_HWIO_H__
#define __APCS_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    APCS_COMMON_APSS_SHARED

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8994/apcs_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: APCS_COMMON_APSS_SHARED
 *--------------------------------------------------------------------------*/

#define APCS_COMMON_APSS_SHARED_REG_BASE                                                        (TERMINATOR_BASE      + 0x00112000)

#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_ADDR(n)                                                 (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000000 + 0x4 * (n))
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_RMSK                                                    0x80000007
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_MAXn                                                            31
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_APCS_COMMON_HW_EVENT_SEL_n_ADDR(n), HWIO_APCS_COMMON_HW_EVENT_SEL_n_RMSK)
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_COMMON_HW_EVENT_SEL_n_ADDR(n), mask)
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_APCS_COMMON_HW_EVENT_SEL_n_ADDR(n),val)
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_COMMON_HW_EVENT_SEL_n_ADDR(n),mask,val,HWIO_APCS_COMMON_HW_EVENT_SEL_n_INI(n))
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_EN_BMSK                                                 0x80000000
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_EN_SHFT                                                       0x1f
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_SEL_BMSK                                                       0x7
#define HWIO_APCS_COMMON_HW_EVENT_SEL_n_SEL_SHFT                                                       0x0

#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_ADDR(n)                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000148 + 0x4 * (n))
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_RMSK                                                 0x80000007
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_MAXn                                                         13
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_INI(n)        \
        in_dword_masked(HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_ADDR(n), HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_RMSK)
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_ADDR(n), mask)
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_ADDR(n),val)
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_ADDR(n),mask,val,HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_INI(n))
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_EN_BMSK                                              0x80000000
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_EN_SHFT                                                    0x1f
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_SEL_BMSK                                                    0x7
#define HWIO_APCS_COMMON_CCI_EVNTBUS_SEL_n_SEL_SHFT                                                    0x0

#define HWIO_APCS_COMMON_SPI_MUX_SEL_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000080)
#define HWIO_APCS_COMMON_SPI_MUX_SEL_RMSK                                                       0x800001ff
#define HWIO_APCS_COMMON_SPI_MUX_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SPI_MUX_SEL_ADDR, HWIO_APCS_COMMON_SPI_MUX_SEL_RMSK)
#define HWIO_APCS_COMMON_SPI_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SPI_MUX_SEL_ADDR, m)
#define HWIO_APCS_COMMON_SPI_MUX_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SPI_MUX_SEL_ADDR,v)
#define HWIO_APCS_COMMON_SPI_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SPI_MUX_SEL_ADDR,m,v,HWIO_APCS_COMMON_SPI_MUX_SEL_IN)
#define HWIO_APCS_COMMON_SPI_MUX_SEL_EN_BMSK                                                    0x80000000
#define HWIO_APCS_COMMON_SPI_MUX_SEL_EN_SHFT                                                          0x1f
#define HWIO_APCS_COMMON_SPI_MUX_SEL_SEL_BMSK                                                        0x1ff
#define HWIO_APCS_COMMON_SPI_MUX_SEL_SEL_SHFT                                                          0x0

#define HWIO_APCS_COMMON_PPI_MUX_SEL_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000084)
#define HWIO_APCS_COMMON_PPI_MUX_SEL_RMSK                                                       0x8000007f
#define HWIO_APCS_COMMON_PPI_MUX_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_PPI_MUX_SEL_ADDR, HWIO_APCS_COMMON_PPI_MUX_SEL_RMSK)
#define HWIO_APCS_COMMON_PPI_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_PPI_MUX_SEL_ADDR, m)
#define HWIO_APCS_COMMON_PPI_MUX_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_PPI_MUX_SEL_ADDR,v)
#define HWIO_APCS_COMMON_PPI_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_PPI_MUX_SEL_ADDR,m,v,HWIO_APCS_COMMON_PPI_MUX_SEL_IN)
#define HWIO_APCS_COMMON_PPI_MUX_SEL_EN_BMSK                                                    0x80000000
#define HWIO_APCS_COMMON_PPI_MUX_SEL_EN_SHFT                                                          0x1f
#define HWIO_APCS_COMMON_PPI_MUX_SEL_SEL_BMSK                                                         0x7f
#define HWIO_APCS_COMMON_PPI_MUX_SEL_SEL_SHFT                                                          0x0

#define HWIO_APCS_COMMON_PERIPHBASE_CCI_ADDR                                                    (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000100)
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_RMSK                                                     0x1ffffff
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_IN          \
        in_dword_masked(HWIO_APCS_COMMON_PERIPHBASE_CCI_ADDR, HWIO_APCS_COMMON_PERIPHBASE_CCI_RMSK)
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_PERIPHBASE_CCI_ADDR, m)
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_PERIPHBASE_CCI_ADDR,v)
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_PERIPHBASE_CCI_ADDR,m,v,HWIO_APCS_COMMON_PERIPHBASE_CCI_IN)
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_CCI_PERIPHBASE_BMSK                                      0x1ffffff
#define HWIO_APCS_COMMON_PERIPHBASE_CCI_CCI_PERIPHBASE_SHFT                                            0x0

#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR                                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000104)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_RMSK                                                      0xffffffff
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_IN          \
        in_dword_masked(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR, HWIO_APCS_COMMON_ADDRMAPS_CCI_RMSK)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR, m)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR,v)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDR,m,v,HWIO_APCS_COMMON_ADDRMAPS_CCI_IN)
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP15_BMSK                                            0xc0000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP15_SHFT                                                  0x1e
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP14_BMSK                                            0x30000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP14_SHFT                                                  0x1c
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP13_BMSK                                             0xc000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP13_SHFT                                                  0x1a
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP12_BMSK                                             0x3000000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP12_SHFT                                                  0x18
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP11_BMSK                                              0xc00000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP11_SHFT                                                  0x16
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP10_BMSK                                              0x300000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP10_SHFT                                                  0x14
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP9_BMSK                                                0xc0000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP9_SHFT                                                   0x12
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP8_BMSK                                                0x30000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP8_SHFT                                                   0x10
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP7_BMSK                                                 0xc000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP7_SHFT                                                    0xe
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP6_BMSK                                                 0x3000
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP6_SHFT                                                    0xc
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP5_BMSK                                                  0xc00
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP5_SHFT                                                    0xa
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP4_BMSK                                                  0x300
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP4_SHFT                                                    0x8
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP3_BMSK                                                   0xc0
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP3_SHFT                                                    0x6
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP2_BMSK                                                   0x30
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP2_SHFT                                                    0x4
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP1_BMSK                                                    0xc
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP1_SHFT                                                    0x2
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP0_BMSK                                                    0x3
#define HWIO_APCS_COMMON_ADDRMAPS_CCI_ADDRMAP0_SHFT                                                    0x0

#define HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR                                                     (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000108)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_RMSK                                                      0x1ffffff
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR, HWIO_APCS_COMMON_CCI_INPUT_CFG_RMSK)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR, m)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR,v)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_INPUT_CFG_ADDR,m,v,HWIO_APCS_COMMON_CCI_INPUT_CFG_IN)
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QOSOVERRIDE_BMSK                                          0x1f00000
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QOSOVERRIDE_SHFT                                               0x14
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_STRIPING_GRANULE_BMSK                                       0xe0000
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_STRIPING_GRANULE_SHFT                                          0x11
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BUFFERABLEOVERRIDE_BMSK                                     0x1c000
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BUFFERABLEOVERRIDE_SHFT                                         0xe
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM2_BMSK                                             0x2000
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM2_SHFT                                                0xd
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM1_BMSK                                             0x1000
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM1_SHFT                                                0xc
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM0_BMSK                                              0x800
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_QVNENABLEM0_SHFT                                                0xb
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BARRIERTERMINATE_BMSK                                         0x700
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BARRIERTERMINATE_SHFT                                           0x8
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BROADCASTCACHEMAINT_BMSK                                       0xe0
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_BROADCASTCACHEMAINT_SHFT                                        0x5
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_ACCHANNELEN_BMSK                                               0x1f
#define HWIO_APCS_COMMON_CCI_INPUT_CFG_ACCHANNELEN_SHFT                                                0x0

#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000010c)
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_RMSK                                                   0xffffffff
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_CCI_CFG3_ADDR, HWIO_APCS_COMMON_SHARED_CCI_CFG3_RMSK)
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_CCI_CFG3_ADDR, m)
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SHARED_CCI_CFG3_ADDR,v)
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SHARED_CCI_CFG3_ADDR,m,v,HWIO_APCS_COMMON_SHARED_CCI_CFG3_IN)
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_COREARST_2_APCNSAW_ENABLE_BMSK                                0x1
#define HWIO_APCS_COMMON_SHARED_CCI_CFG3_COREARST_2_APCNSAW_ENABLE_SHFT                                0x0

#define HWIO_APCS_COMMON_SHARED_STATUS_0_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000110)
#define HWIO_APCS_COMMON_SHARED_STATUS_0_RMSK                                                   0xffffffff
#define HWIO_APCS_COMMON_SHARED_STATUS_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_STATUS_0_ADDR, HWIO_APCS_COMMON_SHARED_STATUS_0_RMSK)
#define HWIO_APCS_COMMON_SHARED_STATUS_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_STATUS_0_ADDR, m)
#define HWIO_APCS_COMMON_SHARED_STATUS_0_STATUS_BMSK                                            0xffffffff
#define HWIO_APCS_COMMON_SHARED_STATUS_0_STATUS_SHFT                                                   0x0

#define HWIO_APCS_COMMON_SHARED_STATUS_1_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000114)
#define HWIO_APCS_COMMON_SHARED_STATUS_1_RMSK                                                   0xffffffff
#define HWIO_APCS_COMMON_SHARED_STATUS_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_STATUS_1_ADDR, HWIO_APCS_COMMON_SHARED_STATUS_1_RMSK)
#define HWIO_APCS_COMMON_SHARED_STATUS_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_STATUS_1_ADDR, m)
#define HWIO_APCS_COMMON_SHARED_STATUS_1_STATUS_BMSK                                            0xffffffff
#define HWIO_APCS_COMMON_SHARED_STATUS_1_STATUS_SHFT                                                   0x0

#define HWIO_APCS_COMMON_WDOG_ABSENT_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000118)
#define HWIO_APCS_COMMON_WDOG_ABSENT_RMSK                                                              0x3
#define HWIO_APCS_COMMON_WDOG_ABSENT_IN          \
        in_dword_masked(HWIO_APCS_COMMON_WDOG_ABSENT_ADDR, HWIO_APCS_COMMON_WDOG_ABSENT_RMSK)
#define HWIO_APCS_COMMON_WDOG_ABSENT_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_WDOG_ABSENT_ADDR, m)
#define HWIO_APCS_COMMON_WDOG_ABSENT_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_WDOG_ABSENT_ADDR,v)
#define HWIO_APCS_COMMON_WDOG_ABSENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_WDOG_ABSENT_ADDR,m,v,HWIO_APCS_COMMON_WDOG_ABSENT_IN)
#define HWIO_APCS_COMMON_WDOG_ABSENT_WDOG1_ABSENT_BMSK                                                 0x2
#define HWIO_APCS_COMMON_WDOG_ABSENT_WDOG1_ABSENT_SHFT                                                 0x1
#define HWIO_APCS_COMMON_WDOG_ABSENT_WDOG0_ABSENT_BMSK                                                 0x1
#define HWIO_APCS_COMMON_WDOG_ABSENT_WDOG0_ABSENT_SHFT                                                 0x0

#define HWIO_APCS_COMMON_CCI_CLK_GFM_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000011c)
#define HWIO_APCS_COMMON_CCI_CLK_GFM_RMSK                                                       0x800000ff
#define HWIO_APCS_COMMON_CCI_CLK_GFM_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CLK_GFM_ADDR, HWIO_APCS_COMMON_CCI_CLK_GFM_RMSK)
#define HWIO_APCS_COMMON_CCI_CLK_GFM_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CLK_GFM_ADDR, m)
#define HWIO_APCS_COMMON_CCI_CLK_GFM_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_CLK_GFM_ADDR,v)
#define HWIO_APCS_COMMON_CCI_CLK_GFM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_CLK_GFM_ADDR,m,v,HWIO_APCS_COMMON_CCI_CLK_GFM_IN)
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_ON_BMSK                                            0x80000000
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_ON_SHFT                                                  0x1f
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_PLL_MAIN_DIV_BMSK                                            0x80
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_PLL_MAIN_DIV_SHFT                                             0x7
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_DIV_BMSK                                                 0x60
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_DIV_SHFT                                                  0x5
#define HWIO_APCS_COMMON_CCI_CLK_GFM_GFMUX_SRC_SEL_HF_BMSK                                            0x18
#define HWIO_APCS_COMMON_CCI_CLK_GFM_GFMUX_SRC_SEL_HF_SHFT                                             0x3
#define HWIO_APCS_COMMON_CCI_CLK_GFM_GFMUX_SRC_SEL_LF_BMSK                                             0x6
#define HWIO_APCS_COMMON_CCI_CLK_GFM_GFMUX_SRC_SEL_LF_SHFT                                             0x1
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_GATE_BMSK                                                 0x1
#define HWIO_APCS_COMMON_CCI_CLK_GFM_CCI_CLK_GATE_SHFT                                                 0x0

#define HWIO_APCS_COMMON_DBG_CLK_CFG_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000120)
#define HWIO_APCS_COMMON_DBG_CLK_CFG_RMSK                                                              0x7
#define HWIO_APCS_COMMON_DBG_CLK_CFG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_DBG_CLK_CFG_ADDR, HWIO_APCS_COMMON_DBG_CLK_CFG_RMSK)
#define HWIO_APCS_COMMON_DBG_CLK_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_DBG_CLK_CFG_ADDR, m)
#define HWIO_APCS_COMMON_DBG_CLK_CFG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_DBG_CLK_CFG_ADDR,v)
#define HWIO_APCS_COMMON_DBG_CLK_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_DBG_CLK_CFG_ADDR,m,v,HWIO_APCS_COMMON_DBG_CLK_CFG_IN)
#define HWIO_APCS_COMMON_DBG_CLK_CFG_DBG_CLK_DIV_BMSK                                                  0x6
#define HWIO_APCS_COMMON_DBG_CLK_CFG_DBG_CLK_DIV_SHFT                                                  0x1
#define HWIO_APCS_COMMON_DBG_CLK_CFG_DBG_CLK_SEL_BMSK                                                  0x1
#define HWIO_APCS_COMMON_DBG_CLK_CFG_DBG_CLK_SEL_SHFT                                                  0x0

#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_ADDR                                                    (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000124)
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_RMSK                                                           0x1
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_IN          \
        in_dword_masked(HWIO_APCS_COMMON_DBGPWRUPIGNORE_ADDR, HWIO_APCS_COMMON_DBGPWRUPIGNORE_RMSK)
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_DBGPWRUPIGNORE_ADDR, m)
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_DBGPWRUPIGNORE_ADDR,v)
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_DBGPWRUPIGNORE_ADDR,m,v,HWIO_APCS_COMMON_DBGPWRUPIGNORE_IN)
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_DBGPWRUPIGNORE_BIT_BMSK                                        0x1
#define HWIO_APCS_COMMON_DBGPWRUPIGNORE_DBGPWRUPIGNORE_BIT_SHFT                                        0x0

#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000130)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_RMSK                                                         0xf
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR, HWIO_APCS_COMMON_CONFIG_BPMID_SEL_RMSK)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_BPMID_SEL_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_BPMID_SEL_IN)
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_BID_SEL_BMSK                                             0x8
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_BID_SEL_SHFT                                             0x3
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_PID_SEL_BMSK                                             0x4
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_PID_SEL_SHFT                                             0x2
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_MID_APC1_SEL_BMSK                                        0x2
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_MID_APC1_SEL_SHFT                                        0x1
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_MID_APC0_SEL_BMSK                                        0x1
#define HWIO_APCS_COMMON_CONFIG_BPMID_SEL_CFG_MID_APC0_SEL_SHFT                                        0x0

#define HWIO_APCS_COMMON_CONFIG_BID_ADDR                                                        (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000134)
#define HWIO_APCS_COMMON_CONFIG_BID_RMSK                                                               0x7
#define HWIO_APCS_COMMON_CONFIG_BID_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BID_ADDR, HWIO_APCS_COMMON_CONFIG_BID_RMSK)
#define HWIO_APCS_COMMON_CONFIG_BID_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_BID_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_BID_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_BID_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_BID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_BID_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_BID_IN)
#define HWIO_APCS_COMMON_CONFIG_BID_CFG_BID_BMSK                                                       0x7
#define HWIO_APCS_COMMON_CONFIG_BID_CFG_BID_SHFT                                                       0x0

#define HWIO_APCS_COMMON_CONFIG_PID_ADDR                                                        (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000138)
#define HWIO_APCS_COMMON_CONFIG_PID_RMSK                                                              0x1f
#define HWIO_APCS_COMMON_CONFIG_PID_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_PID_ADDR, HWIO_APCS_COMMON_CONFIG_PID_RMSK)
#define HWIO_APCS_COMMON_CONFIG_PID_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_PID_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_PID_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_PID_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_PID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_PID_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_PID_IN)
#define HWIO_APCS_COMMON_CONFIG_PID_CFG_PID_BMSK                                                      0x1f
#define HWIO_APCS_COMMON_CONFIG_PID_CFG_PID_SHFT                                                       0x0

#define HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000013c)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_RMSK                                                         0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR, HWIO_APCS_COMMON_CONFIG_MID_APC1_RMSK)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_MID_APC1_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_MID_APC1_IN)
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_CFG_MID_APC1_BMSK                                            0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC1_CFG_MID_APC1_SHFT                                             0x0

#define HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000140)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_RMSK                                                         0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR, HWIO_APCS_COMMON_CONFIG_MID_APC0_RMSK)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR, m)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR,v)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CONFIG_MID_APC0_ADDR,m,v,HWIO_APCS_COMMON_CONFIG_MID_APC0_IN)
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_CFG_MID_APC0_BMSK                                            0xff
#define HWIO_APCS_COMMON_CONFIG_MID_APC0_CFG_MID_APC0_SHFT                                             0x0

#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_ADDR                                              (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000128)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_RMSK                                                     0xf
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_ADDR, HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_RMSK)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_ADDR, m)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_ADDR,v)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_ADDR,m,v,HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_IN)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_TCM_REDIRECT_SWR_CTL_BITS_BMSK                           0xf
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_TCM_REDIRECT_SWR_CTL_BITS_SHFT                           0x0

#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_ADDR                                            (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000012c)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_RMSK                                                0xffff
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_IN          \
        in_dword_masked(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_ADDR, HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_RMSK)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_ADDR, m)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_ADDR,v)
#define HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_ADDR,m,v,HWIO_APCS_COMMON_TCM_REDIRECT_SWR_CTL_2_IN)

#define HWIO_APCS_COMMON_CPU_QREQ_SEL_ADDR                                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000088)
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_RMSK                                                      0x80000007
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPU_QREQ_SEL_ADDR, HWIO_APCS_COMMON_CPU_QREQ_SEL_RMSK)
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPU_QREQ_SEL_ADDR, m)
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CPU_QREQ_SEL_ADDR,v)
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CPU_QREQ_SEL_ADDR,m,v,HWIO_APCS_COMMON_CPU_QREQ_SEL_IN)
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_EN_BMSK                                                   0x80000000
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_EN_SHFT                                                         0x1f
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_SEL_BMSK                                                         0x7
#define HWIO_APCS_COMMON_CPU_QREQ_SEL_SEL_SHFT                                                         0x0

#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000008c)
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_RMSK                                                   0x80000007
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPU_QACTIVE_SEL_ADDR, HWIO_APCS_COMMON_CPU_QACTIVE_SEL_RMSK)
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPU_QACTIVE_SEL_ADDR, m)
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CPU_QACTIVE_SEL_ADDR,v)
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CPU_QACTIVE_SEL_ADDR,m,v,HWIO_APCS_COMMON_CPU_QACTIVE_SEL_IN)
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_EN_BMSK                                                0x80000000
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_EN_SHFT                                                      0x1f
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_SEL_BMSK                                                      0x7
#define HWIO_APCS_COMMON_CPU_QACTIVE_SEL_SEL_SHFT                                                      0x0

#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000090)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_RMSK                                                  0x80000007
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_ADDR, HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_RMSK)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_ADDR, m)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_ADDR,v)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_ADDR,m,v,HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_IN)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_EN_BMSK                                               0x80000000
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_EN_SHFT                                                     0x1f
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_SEL_BMSK                                                     0x7
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_REQ_SEL_SHFT                                                     0x0

#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000094)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_RMSK                                                  0x80000007
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_ADDR, HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_RMSK)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_ADDR, m)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_ADDR,v)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_ADDR,m,v,HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_IN)
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_EN_BMSK                                               0x80000000
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_EN_SHFT                                                     0x1f
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_SEL_BMSK                                                     0x7
#define HWIO_APCS_COMMON_CPU_SPM_WAIT_ACK_SEL_SHFT                                                     0x0

#define HWIO_APCS_COMMON_MEM_SVS_ADDR                                                           (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000144)
#define HWIO_APCS_COMMON_MEM_SVS_RMSK                                                                  0x3
#define HWIO_APCS_COMMON_MEM_SVS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MEM_SVS_ADDR, HWIO_APCS_COMMON_MEM_SVS_RMSK)
#define HWIO_APCS_COMMON_MEM_SVS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MEM_SVS_ADDR, m)
#define HWIO_APCS_COMMON_MEM_SVS_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MEM_SVS_ADDR,v)
#define HWIO_APCS_COMMON_MEM_SVS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MEM_SVS_ADDR,m,v,HWIO_APCS_COMMON_MEM_SVS_IN)
#define HWIO_APCS_COMMON_MEM_SVS_MEM_SVS_C1_BIT_BMSK                                                   0x2
#define HWIO_APCS_COMMON_MEM_SVS_MEM_SVS_C1_BIT_SHFT                                                   0x1
#define HWIO_APCS_COMMON_MEM_SVS_MEM_SVS_C0_BIT_BMSK                                                   0x1
#define HWIO_APCS_COMMON_MEM_SVS_MEM_SVS_C0_BIT_SHFT                                                   0x0

#define HWIO_APCS_COMMON_CCI_HIER_CG_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001b4)
#define HWIO_APCS_COMMON_CCI_HIER_CG_RMSK                                                              0x1
#define HWIO_APCS_COMMON_CCI_HIER_CG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR, HWIO_APCS_COMMON_CCI_HIER_CG_RMSK)
#define HWIO_APCS_COMMON_CCI_HIER_CG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR, m)
#define HWIO_APCS_COMMON_CCI_HIER_CG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR,v)
#define HWIO_APCS_COMMON_CCI_HIER_CG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_HIER_CG_ADDR,m,v,HWIO_APCS_COMMON_CCI_HIER_CG_IN)
#define HWIO_APCS_COMMON_CCI_HIER_CG_HIER_CG_EN_BMSK                                                   0x1
#define HWIO_APCS_COMMON_CCI_HIER_CG_HIER_CG_EN_SHFT                                                   0x0

#define HWIO_APCS_COMMON_SHARED_SECURE_ADDR                                                     (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000009c)
#define HWIO_APCS_COMMON_SHARED_SECURE_RMSK                                                            0x3
#define HWIO_APCS_COMMON_SHARED_SECURE_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_SECURE_ADDR, HWIO_APCS_COMMON_SHARED_SECURE_RMSK)
#define HWIO_APCS_COMMON_SHARED_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SHARED_SECURE_ADDR, m)
#define HWIO_APCS_COMMON_SHARED_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SHARED_SECURE_ADDR,v)
#define HWIO_APCS_COMMON_SHARED_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SHARED_SECURE_ADDR,m,v,HWIO_APCS_COMMON_SHARED_SECURE_IN)
#define HWIO_APCS_COMMON_SHARED_SECURE_SHARED_SECURE_CLK_BMSK                                          0x2
#define HWIO_APCS_COMMON_SHARED_SECURE_SHARED_SECURE_CLK_SHFT                                          0x1
#define HWIO_APCS_COMMON_SHARED_SECURE_SHARED_SECURE_CFG_BMSK                                          0x1
#define HWIO_APCS_COMMON_SHARED_SECURE_SHARED_SECURE_CFG_SHFT                                          0x0

#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000180)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_RMSK                                                        0xff
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR, HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_RMSK)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR, m)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR,v)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_ADDR,m,v,HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_IN)
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_SPM_WAKE_UP_WDOG_ENABLE_BMSK                                0xff
#define HWIO_APCS_COMMON_SPM_WAKE_UP_WDOG_SPM_WAKE_UP_WDOG_ENABLE_SHFT                                 0x0

#define HWIO_APCS_COMMON_CCI_CBCR_ADDR                                                          (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000184)
#define HWIO_APCS_COMMON_CCI_CBCR_RMSK                                                                0x1f
#define HWIO_APCS_COMMON_CCI_CBCR_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CBCR_ADDR, HWIO_APCS_COMMON_CCI_CBCR_RMSK)
#define HWIO_APCS_COMMON_CCI_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_CBCR_ADDR, m)
#define HWIO_APCS_COMMON_CCI_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_CBCR_ADDR,v)
#define HWIO_APCS_COMMON_CCI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_CBCR_ADDR,m,v,HWIO_APCS_COMMON_CCI_CBCR_IN)
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_SEC_DIV_BMSK                                           0x18
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_SEC_DIV_SHFT                                            0x3
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_PRI_DIV_BMSK                                            0x6
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_PRI_DIV_SHFT                                            0x1
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_CBC_EN_BMSK                                             0x1
#define HWIO_APCS_COMMON_CCI_CBCR_CCI_LEAF_CLK_CBC_EN_SHFT                                             0x0

#define HWIO_APCS_COMMON_CLUST_LVL_SEL_ADDR                                                     (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000188)
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_RMSK                                                            0x1
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CLUST_LVL_SEL_ADDR, HWIO_APCS_COMMON_CLUST_LVL_SEL_RMSK)
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CLUST_LVL_SEL_ADDR, m)
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CLUST_LVL_SEL_ADDR,v)
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CLUST_LVL_SEL_ADDR,m,v,HWIO_APCS_COMMON_CLUST_LVL_SEL_IN)
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_CLUST_SELECT_BMSK                                               0x1
#define HWIO_APCS_COMMON_CLUST_LVL_SEL_CLUST_SELECT_SHFT                                               0x0

#define HWIO_APCS_COMMON_LMH_AUX_DIV_ADDR                                                       (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000018c)
#define HWIO_APCS_COMMON_LMH_AUX_DIV_RMSK                                                       0xfc00003f
#define HWIO_APCS_COMMON_LMH_AUX_DIV_IN          \
        in_dword_masked(HWIO_APCS_COMMON_LMH_AUX_DIV_ADDR, HWIO_APCS_COMMON_LMH_AUX_DIV_RMSK)
#define HWIO_APCS_COMMON_LMH_AUX_DIV_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_LMH_AUX_DIV_ADDR, m)
#define HWIO_APCS_COMMON_LMH_AUX_DIV_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_LMH_AUX_DIV_ADDR,v)
#define HWIO_APCS_COMMON_LMH_AUX_DIV_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_LMH_AUX_DIV_ADDR,m,v,HWIO_APCS_COMMON_LMH_AUX_DIV_IN)
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_CLK_OFF_BMSK                                       0x80000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_CLK_OFF_SHFT                                             0x1f
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_UPDATE_BMSK                                        0x40000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_UPDATE_SHFT                                              0x1e
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_CLK_EN_BMSK                                        0x20000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_CLK_EN_SHFT                                              0x1d
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_WR_EN_BMSK                                         0x10000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_RCG_WR_EN_SHFT                                               0x1c
#define HWIO_APCS_COMMON_LMH_AUX_DIV_A57_PS_SOFT_RST_BMSK                                        0x8000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_A57_PS_SOFT_RST_SHFT                                             0x1b
#define HWIO_APCS_COMMON_LMH_AUX_DIV_A53_PS_SOFT_RST_BMSK                                        0x4000000
#define HWIO_APCS_COMMON_LMH_AUX_DIV_A53_PS_SOFT_RST_SHFT                                             0x1a
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_AUX_DIV_RATIO_BMSK                                           0x3e
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_AUX_DIV_RATIO_SHFT                                            0x1
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_AUX_CLK_SEL_BMSK                                              0x1
#define HWIO_APCS_COMMON_LMH_AUX_DIV_LMH_AUX_CLK_SEL_SHFT                                              0x0

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000190)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK                                               0xdfffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESET_FUNC_BMSK                                    0x80000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RESET_FUNC_SHFT                                          0x1f
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_MODE_SEL_BMSK                                      0x40000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_MODE_SEL_SHFT                                            0x1e
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIGGER_SEL_BMSK                                   0x18000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIGGER_SEL_SHFT                                         0x1b
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                     0x4000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                          0x1a
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_START_CAPTURE_BMSK                                  0x2000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_START_CAPTURE_SHFT                                       0x19
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_POWER_BMSK                                       0x1000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_EN_POWER_SHFT                                            0x18
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                   0xff0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                       0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                     0xff00
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                        0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                       0xf0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                        0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_POS_BMSK                                             0xc
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_POS_SHFT                                             0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                          0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                          0x0

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000194)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK                                                 0xffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SEL_JTAG_BMSK                                        0x800000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SEL_JTAG_SHFT                                            0x17
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                       0x7e0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                           0x11
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLP_EN_BMSK                                     0x10000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLP_EN_SHFT                                        0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                       0xe000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                          0xd
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_THRSLD_BMSK                                      0x1fe0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_THRSLD_SHFT                                         0x5
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_FUNC_BMSK                                             0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_FUNC_SHFT                                              0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                            0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                            0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_STS_CLR_BMSK                                              0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_STS_CLR_SHFT                                              0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_ALARM_BMSK                                             0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_EN_ALARM_SHFT                                             0x1
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CGC_CLK_EN_BMSK                                           0x1
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CGC_CLK_EN_SHFT                                           0x0

#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR                                                 (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x00000198)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK                                                    0x3ffff
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_BMSK                                          0x3c000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_SHFT                                              0xe
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLP_NEG_STS_BMSK                                         0x2000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLP_NEG_STS_SHFT                                            0xd
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLP_POS_STS_BMSK                                         0x1000
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLP_POS_STS_SHFT                                            0xc
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_STS_BMSK                                        0x800
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_STS_SHFT                                          0xb
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_STS_BMSK                                        0x400
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_STS_SHFT                                          0xa
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACT_STS_BMSK                                         0x200
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACT_STS_SHFT                                           0x9
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                    0x100
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                      0x8
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_BMSK                                             0xff
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_SHFT                                              0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x0000019c)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK                                               0xdfffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESET_FUNC_BMSK                                    0x80000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RESET_FUNC_SHFT                                          0x1f
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_MODE_SEL_BMSK                                      0x40000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_MODE_SEL_SHFT                                            0x1e
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIGGER_SEL_BMSK                                   0x18000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIGGER_SEL_SHFT                                         0x1b
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                     0x4000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                          0x1a
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_START_CAPTURE_BMSK                                  0x2000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_START_CAPTURE_SHFT                                       0x19
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_POWER_BMSK                                       0x1000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_EN_POWER_SHFT                                            0x18
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                   0xff0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                       0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                     0xff00
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                        0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                       0xf0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                        0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_POS_BMSK                                             0xc
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_POS_SHFT                                             0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                          0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                          0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001a0)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK                                                 0xffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SEL_JTAG_BMSK                                        0x800000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SEL_JTAG_SHFT                                            0x17
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                       0x7e0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                           0x11
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLP_EN_BMSK                                     0x10000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLP_EN_SHFT                                        0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                       0xe000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                          0xd
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_THRSLD_BMSK                                      0x1fe0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_THRSLD_SHFT                                         0x5
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_FUNC_BMSK                                             0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_FUNC_SHFT                                              0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                            0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                            0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_STS_CLR_BMSK                                              0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_STS_CLR_SHFT                                              0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_ALARM_BMSK                                             0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_EN_ALARM_SHFT                                             0x1
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CGC_CLK_EN_BMSK                                           0x1
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CGC_CLK_EN_SHFT                                           0x0

#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR                                                 (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001a4)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK                                                    0x3ffff
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_BMSK                                          0x3c000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_SHFT                                              0xe
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLP_NEG_STS_BMSK                                         0x2000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLP_NEG_STS_SHFT                                            0xd
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLP_POS_STS_BMSK                                         0x1000
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLP_POS_STS_SHFT                                            0xc
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_STS_BMSK                                        0x800
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_STS_SHFT                                          0xb
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_STS_BMSK                                        0x400
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_STS_SHFT                                          0xa
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACT_STS_BMSK                                         0x200
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACT_STS_SHFT                                           0x9
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                    0x100
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                      0x8
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_BMSK                                             0xff
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_SHFT                                              0x0

#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_ADDR                                                 (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001a8)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_RMSK                                                 0xdfffffff
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_RESET_FUNC_BMSK                                      0x80000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_RESET_FUNC_SHFT                                            0x1f
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_MODE_SEL_BMSK                                        0x40000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_MODE_SEL_SHFT                                              0x1e
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIGGER_SEL_BMSK                                     0x18000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIGGER_SEL_SHFT                                           0x1b
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_EN_TRIGGER_BMSK                                       0x4000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_EN_TRIGGER_SHFT                                            0x1a
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_START_CAPTURE_BMSK                                    0x2000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_START_CAPTURE_SHFT                                         0x19
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_EN_POWER_BMSK                                         0x1000000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_EN_POWER_SHFT                                              0x18
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                                     0xff0000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                                         0x10
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                                       0xff00
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                                          0x8
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                                         0xf0
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                                          0x4
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIG_POS_BMSK                                               0xc
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIG_POS_SHFT                                               0x2
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIG_METHOD_BMSK                                            0x3
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_0_TRIG_METHOD_SHFT                                            0x0

#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ADDR                                                 (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001ac)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_RMSK                                                   0xffffff
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_SEL_JTAG_BMSK                                          0x800000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_SEL_JTAG_SHFT                                              0x17
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_FIFO_ADDR_BMSK                                         0x7e0000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_FIFO_ADDR_SHFT                                             0x11
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ALARM_SLP_EN_BMSK                                       0x10000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_ALARM_SLP_EN_SHFT                                          0x10
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_DELTA_CYCLE_BMSK                                         0xe000
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_DELTA_CYCLE_SHFT                                            0xd
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_SLOPE_THRSLD_BMSK                                        0x1fe0
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_SLOPE_THRSLD_SHFT                                           0x5
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_EN_FUNC_BMSK                                               0x10
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_EN_FUNC_SHFT                                                0x4
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_CLAMP_DIS_BMSK                                              0x8
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_CLAMP_DIS_SHFT                                              0x3
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_STS_CLR_BMSK                                                0x4
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_STS_CLR_SHFT                                                0x2
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_EN_ALARM_BMSK                                               0x2
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_EN_ALARM_SHFT                                               0x1
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_CGC_CLK_EN_BMSK                                             0x1
#define HWIO_APCS_COMMON_MX_VSENS_CONFIG_1_CGC_CLK_EN_SHFT                                             0x0

#define HWIO_APCS_COMMON_MX_VSENS_STATUS_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001b0)
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_RMSK                                                      0x3ffff
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_MX_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MX_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_VS_FSM_ST_BMSK                                            0x3c000
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_VS_FSM_ST_SHFT                                                0xe
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_SLP_NEG_STS_BMSK                                           0x2000
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_SLP_NEG_STS_SHFT                                              0xd
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_SLP_POS_STS_BMSK                                           0x1000
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_SLP_POS_STS_SHFT                                              0xc
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_ALARM_MAX_STS_BMSK                                          0x800
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_ALARM_MAX_STS_SHFT                                            0xb
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_ALARM_MIN_STS_BMSK                                          0x400
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_ALARM_MIN_STS_SHFT                                            0xa
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_ACT_STS_BMSK                                           0x200
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_ACT_STS_SHFT                                             0x9
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                                      0x100
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                                        0x8
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_DATA_BMSK                                               0xff
#define HWIO_APCS_COMMON_MX_VSENS_STATUS_FIFO_DATA_SHFT                                                0x0

#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_ADDR                                             (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001c0)
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_RMSK                                                   0xff
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_ADDR, HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_RMSK)
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_ADDR, m)
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_ADDR,v)
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_ADDR,m,v,HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_IN)
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE3_BMSK                0x80
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE3_SHFT                 0x7
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE2_BMSK                0x40
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE2_SHFT                 0x6
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE1_BMSK                0x20
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE1_SHFT                 0x5
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE0_BMSK                0x10
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST1_CORE0_SHFT                 0x4
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE3_BMSK                 0x8
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE3_SHFT                 0x3
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE2_BMSK                 0x4
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE2_SHFT                 0x2
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE1_BMSK                 0x2
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE1_SHFT                 0x1
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE0_BMSK                 0x1
#define HWIO_APCS_COMMON_CP15SDISABLE_OVERRIDE_CP15SDISABLE_OVERRIDE_CLUST0_CORE0_SHFT                 0x0

#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001c8)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_RMSK                                                         0x3
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR, HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_RMSK)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR, m)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR,v)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_ADDR,m,v,HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_IN)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_SLEEP_BMSK                                     0x2
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_SLEEP_SHFT                                     0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_WAKEUP_BMSK                                    0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C0_CLUST0_L2_SPM_WAKEUP_SHFT                                    0x0

#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR                                                  (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001cc)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_RMSK                                                         0x3
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR, HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_RMSK)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR, m)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR,v)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_ADDR,m,v,HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_IN)
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_SLEEP_BMSK                                     0x2
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_SLEEP_SHFT                                     0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_WAKEUP_BMSK                                    0x1
#define HWIO_APCS_COMMON_L2SPM_TRIGGER_C1_CLUST1_L2_SPM_WAKEUP_SHFT                                    0x0

#define HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR                                                    (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001d0)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_RMSK                                                           0x3
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR, HWIO_APCS_COMMON_CCISPM_TRIGGER_RMSK)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR, m)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR,v)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCISPM_TRIGGER_ADDR,m,v,HWIO_APCS_COMMON_CCISPM_TRIGGER_IN)
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_SLEEP_BMSK                                              0x2
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_SLEEP_SHFT                                              0x1
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_WAKEUP_BMSK                                             0x1
#define HWIO_APCS_COMMON_CCISPM_TRIGGER_CCISPM_WAKEUP_SHFT                                             0x0

#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_ADDR                                               (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001d4)
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_RMSK                                                      0x3
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_ADDR, HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_RMSK)
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_ADDR, m)
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_ADDR,v)
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_ADDR,m,v,HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_IN)
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_APBDBGCLKONACK_BMSK                                       0x2
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_APBDBGCLKONACK_SHFT                                       0x1
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_APBDBGCLKONREQ_BMSK                                       0x1
#define HWIO_APCS_COMMON_APCS_APBDBGCLKONREQ_APBDBGCLKONREQ_SHFT                                       0x0

#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_ADDR                                                   (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001d8)
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_RMSK                                                          0x3
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_IN          \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CLK_EN_ADDR, HWIO_APCS_COMMON_CCI_HIER_CLK_EN_RMSK)
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_CCI_HIER_CLK_EN_ADDR, m)
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_CCI_HIER_CLK_EN_ADDR,v)
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_CCI_HIER_CLK_EN_ADDR,m,v,HWIO_APCS_COMMON_CCI_HIER_CLK_EN_IN)
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_CCI_AHB2AHB_INACTIVE_BMSK                                     0x2
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_CCI_AHB2AHB_INACTIVE_SHFT                                     0x1
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_SNOOP_INACTIVE_BMSK                                           0x1
#define HWIO_APCS_COMMON_CCI_HIER_CLK_EN_SNOOP_INACTIVE_SHFT                                           0x0

#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_ADDR                                                    (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001dc)
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_RMSK                                                      0x3cf3cf
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MEM_ON_OFF_SEL_ADDR, HWIO_APCS_COMMON_MEM_ON_OFF_SEL_RMSK)
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MEM_ON_OFF_SEL_ADDR, m)
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MEM_ON_OFF_SEL_ADDR,v)
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MEM_ON_OFF_SEL_ADDR,m,v,HWIO_APCS_COMMON_MEM_ON_OFF_SEL_IN)
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_PERIPH_OFF_MUX_SEL_BMSK                           0x3c0000
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_PERIPH_OFF_MUX_SEL_SHFT                               0x12
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_PERIPH_ON_MUX_SEL_BMSK                              0xf000
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_PERIPH_ON_MUX_SEL_SHFT                                 0xc
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_CORE_OFF_MUX_SEL_BMSK                                0x3c0
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_CORE_OFF_MUX_SEL_SHFT                                  0x6
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_CORE_ON_MUX_SEL_BMSK                                   0xf
#define HWIO_APCS_COMMON_MEM_ON_OFF_SEL_CPU_MEM_CORE_ON_MUX_SEL_SHFT                                   0x0

#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_ADDR                                                (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001e0)
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_RMSK                                                 0x9f3e7cf
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_ADDR, HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_RMSK)
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_ADDR, m)
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_ADDR,v)
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_ADDR,m,v,HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_IN)
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_UP_CMPLT_MUX_SEL_BMSK                       0x8000000
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_UP_CMPLT_MUX_SEL_SHFT                            0x1b
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_UP_CMPLT_MUX_SEL_BMSK                      0x1e00000
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_UP_CMPLT_MUX_SEL_SHFT                           0x15
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_UP_STRTD_MUX_SEL_BMSK                        0x100000
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_UP_STRTD_MUX_SEL_SHFT                            0x14
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_UP_STRTD_MUX_SEL_BMSK                        0x3c000
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_UP_STRTD_MUX_SEL_SHFT                            0xe
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_DWN_CMPLT_MUX_SEL_BMSK                         0x2000
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_DWN_CMPLT_MUX_SEL_SHFT                            0xd
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_DWN_CMPLT_MUX_SEL_BMSK                         0x780
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_DWN_CMPLT_MUX_SEL_SHFT                           0x7
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_DWN_STRTD_MUX_SEL_BMSK                           0x40
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_L2_POWER_DWN_STRTD_MUX_SEL_SHFT                            0x6
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_DWN_STRTD_MUX_SEL_BMSK                           0xf
#define HWIO_APCS_COMMON_SPM_PWR_STATUS_SEL_CPU_POWER_DWN_STRTD_MUX_SEL_SHFT                           0x0

#define HWIO_APCS_COMMON_MISC_HWEVENT_ADDR                                                      (APCS_COMMON_APSS_SHARED_REG_BASE      + 0x000001e4)
#define HWIO_APCS_COMMON_MISC_HWEVENT_RMSK                                                      0x80000003
#define HWIO_APCS_COMMON_MISC_HWEVENT_IN          \
        in_dword_masked(HWIO_APCS_COMMON_MISC_HWEVENT_ADDR, HWIO_APCS_COMMON_MISC_HWEVENT_RMSK)
#define HWIO_APCS_COMMON_MISC_HWEVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_MISC_HWEVENT_ADDR, m)
#define HWIO_APCS_COMMON_MISC_HWEVENT_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_MISC_HWEVENT_ADDR,v)
#define HWIO_APCS_COMMON_MISC_HWEVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_MISC_HWEVENT_ADDR,m,v,HWIO_APCS_COMMON_MISC_HWEVENT_IN)
#define HWIO_APCS_COMMON_MISC_HWEVENT_MISC_HWEVENT_EN_BMSK                                      0x80000000
#define HWIO_APCS_COMMON_MISC_HWEVENT_MISC_HWEVENT_EN_SHFT                                            0x1f
#define HWIO_APCS_COMMON_MISC_HWEVENT_MISC_HWEVENT_MUX_SEL_BMSK                                        0x3
#define HWIO_APCS_COMMON_MISC_HWEVENT_MISC_HWEVENT_MUX_SEL_SHFT                                        0x0


#endif /* __APCS_HWIO_H__ */
