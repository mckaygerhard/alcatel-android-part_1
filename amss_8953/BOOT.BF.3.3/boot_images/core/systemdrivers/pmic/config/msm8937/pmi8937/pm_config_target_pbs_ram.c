/*! \file  pm_config_target_pbs_ram.c
 *  
 *  \brief  File Contains the PMIC Set Mode Driver Implementation
 *  \details Set Mode Driver implementation is responsible for setting and getting 
 *  all mode settings such as Register values, memory values, etc.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: PM8937-x.x-OTPx.x-MSM8937-05122016_b0_v009 - Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2016 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/config/msm8937/pmi8937/pm_config_target_pbs_ram.c#2 $ 
$DateTime: 2016/05/26 03:54:29 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_target_information.h"
#include "pm_config_sbl.h"
#include "pm_pbs_driver.h"

/*========================== PBS RAM LUT =============================*/

// To handle multiple PBS RAM configuration for different rev of the same PMIC or for multiple PMICs,
// a double dimension array of PBS RAM data is used. The data field of the specific command (in pm_config_target_sbl_sequence.c)
// to program PBS RAM will hold the index to the PBS RAM that needs to be used. during programming.
// Example:
// 1.sid; 2.data; 3.base_address; 4.offset;  5.reg_operation; 6.rev_id_operation; 7.rev_id;
// {0, 0x00, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_2_0},  //data = 0:  Use the 1st set of PBS RAM data if PMIC Rev ID = REV_ID_2_0
// {0, 0x01, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_1_0},  //data = 1:  Use the 2nd set of PBS RAM data if PMIC Rev ID = REV_ID_1_0

pm_pbs_ram_data_type
pm_pbs_seq [ ][PBS_RAM_DATA_SIZE] =
{
   // PBS_RAM_MSM8937.PMIC.HW.PM8937_1p0_1_0_6
   {
      //data  offset  base_addr  sid
      { 0x2C,	0x04,	0x00,	0x80},	// W#0	-	DTEST 1 PBS-RAM Trigger. Used for XO Correction Sequence in Sleep
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#1	-	Configured in PBS-OTP
      { 0x34,	0x05,	0x00,	0x80},	// W#2	-	POFF PBS-RAM Trigger
      { 0x54,	0x04,	0x00,	0x80},	// W#3	-	BUA PBS-RAM Trigger
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#4	-	
      { 0xA4,	0x04,	0x00,	0x80},	// W#5	-	Sleep Wake Trigger in RAM. PBS CLIENT used as Pre PON in OTP, has been re purposed to Sleep Wake in RAM
      { 0x0C,	0x05,	0x00,	0x80},	// W#6	-	WARM_RESET PBS-RAM Trigger
      { 0xBC,	0x05,	0x00,	0x80},	// W#7	-	DTEST 2 PBS-RAM Trigger. Used for uSD-DragonFly-Debug Support
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#8	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#9	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#10	-	
      { 0x00,	0x08,	0x50,	0x40},	// W#11	-	Read XO_STATUS1 for SLEEP_B
      { 0x40,	0x01,	0x40,	0x90},	// W#12	-	Skip next line if SLEEP_B is high, for Active state
      { 0x44,	0x04,	0x00,	0x80},	// W#13	-	Goto DO_CORRECTION sequence to correct for BBCLK
      { 0x00,	0x51,	0x59,	0x40},	// W#14	-	Read CLK_DIST_SPARE1 & store value in Local Buffer
      { 0x00,	0x5C,	0x50,	0x08},	// W#15	-	Write (Copy) the local-buffer (SPARE1) value to XO_ADJ_FINE Register
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#16	-	End of XO UNDO correction sequence
      { 0x00,	0x52,	0x59,	0x40},	// W#17	-	Read CLK_DIST_SPARE2 & store value in Local Buffer
      { 0x00,	0x5C,	0x50,	0x08},	// W#18	-	Write (Copy) the local-buffer (SPARE2) value to XO_ADJ_FINE Register
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#19	-	End of DO_CORRECTION sequence
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#20	-	
      { 0x00,	0x18,	0x1C,	0x40},	// W#21	-	check BUA INT_LATCHED_STS 
      { 0x01,	0x01,	0x01,	0x90},	// W#22	-	BATT_ALRAM INT latched
      { 0x70,	0x04,	0x00,	0x80},	// W#23	-	Goto BUA_UICC_STS Check
      { 0x90,	0x04,	0x4D,	0x89},	// W#24	-	Reset LDO14 (UIM1 LDO)
      { 0x90,	0x04,	0x4E,	0x89},	// W#25	-	Reset LDO15 (UIM2 LDO)
      { 0x90,	0x04,	0xC1,	0x88},	// W#26	-	Reset GPIO2 (UIM3)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#27	-	End of BUA sequence
      { 0x00,	0x09,	0x1C,	0x40},	// W#28	-	Read BUA_EXT_CHARGER_STATUS2 register for UICCx_Alarm detection
      { 0x01,	0x00,	0x01,	0x90},	// W#29	-	Skip next line if UICC1 alarm not detected
      { 0x90,	0x04,	0x4D,	0x89},	// W#30	-	Reset LDO14 (UIM1 LDO)
      { 0x02,	0x00,	0x02,	0x90},	// W#31	-	Skip next line if UICC2 alarm not detected
      { 0x90,	0x04,	0x4E,	0x89},	// W#32	-	Reset LDO15 (UIM2 LDO)
      { 0x04,	0x00,	0x04,	0x90},	// W#33	-	Skip next line if UICC3 alarm not detected
      { 0x90,	0x04,	0xC1,	0x88},	// W#34	-	Reset GPIO2 (UIM3)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#35	-	End of BUA sequence
      { 0xA5,	0xD0,	0x00,	0x04},	// W#36	-	Unlock SEC_ACCESS
      { 0x01,	0xDB,	0x00,	0x04},	// W#37	-	Perform LOCAL_SOFT_RESET on LDO (to reset all other voltage settings in addition to disabling it)
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#38	-	Return to the calling line
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#39	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#40	-	
      { 0x00,	0x8A,	0x08,	0x40},	// W#41	-	Read PON: AVDD_VPH register, to know, whether we are entering into or exiting from sleep
      { 0x20,	0x01,	0x20,	0x90},	// W#42	-	IFAVDD_VPH reg. bit 5= 1 => AVDD in HPM, skip next line to enter sleep
      { 0xD8,	0x04,	0x00,	0x80},	// W#43	-	Goto: BEGIN_RAM_WAKE_SEQ
      { 0x01,	0x46,	0x51,	0x00},	// W#44	-	Configure BBCLK1 to (only) follow BBCLK1_EN (CXO_EN) Pin-Control
      { 0x10,	0x40,	0xC3,	0x00},	// W#45	-	Configure BBYP to be in forced BYP mode in sleep
      { 0x00,	0x45,	0x17,	0x01},	// W#46	-	Force PFM Mode on S2
      { 0x00,	0x45,	0x1A,	0x01},	// W#47	-	Force PFM Mode on S3
      { 0x00,	0x45,	0x1D,	0x01},	// W#48	-	Force PFM Mode on S4
      { 0x11,	0x40,	0xA0,	0x00},	// W#49	-	Change MPP1 Mode from 1.25V Analog Output to Digital Output Mode (for low power consumption) [DIG_VIN already configured for 1.2V in SBL]
      { 0x00,	0x8A,	0x08,	0x00},	// W#50	-	Put AVDD Regulator into LPM
      { 0xA5,	0xD0,	0x59,	0x00},	// W#51	-	Unlock SEC_ACCESS
      { 0x3C,	0x40,	0x59,	0x00},	// W#52	-	SMPS_GP_BASE_ON_HALT
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#53	-	EOS (End on RAM_SLEEP_SEQ)
      { 0x20,	0x8A,	0x08,	0x00},	// W#54	-	Put AVDD Regulator back to HPM & Enable Auto-selection to Master BandGap
      { 0x81,	0x46,	0x51,	0x00},	// W#55	-	Configure BBCLK1 to also get SW-Enabled (it has already been enabled by BBCLK1_EN (CXO_EN) Pin that got asserted-high before starting Wake-Up)
      { 0x11,	0x40,	0xC3,	0x00},	// W#56	-	Configure BBYP to be in Boost mode back
      { 0x40,	0x45,	0x17,	0x01},	// W#57	-	Auto Mode on S2
      { 0x40,	0x45,	0x1A,	0x01},	// W#58	-	Auto Mode on S3
      { 0x40,	0x45,	0x1D,	0x01},	// W#59	-	Auto Mode on S4
      { 0x51,	0x40,	0xA0,	0x00},	// W#60	-	Revert MPP1 Mode from 1.20V Digital Output to 1.25V Analog Output Mode [DIG_VIN already configured for 1.2V in SBL]
      { 0xA5,	0xD0,	0x59,	0x00},	// W#61	-	Unlock SEC_ACCESS
      { 0x34,	0x40,	0x59,	0x00},	// W#62	-	SMPS_BASE_ON_HALT, GP_RC_19M
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#63	-	EOS (End of RAM_WAKE_SEQ)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#64	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#65	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#66	-	
      { 0x20,	0x8A,	0x08,	0x00},	// W#67	-	Put AVDD Regulator back to HPM & Enable Auto-selection to Master BandGap
      { 0x51,	0x40,	0xA0,	0x00},	// W#68	-	MPP1 to analog Output mode with 1.25V (For cases where, PMIC in sleep & warm reset occurs)
      { 0x11,	0x40,	0xC3,	0x00},	// W#69	-	setting GPIO4 in Digital Output mode with enable high to turn on the boost
      { 0xFF,	0x16,	0x08,	0x00},	// W#70	-	Writing INT_EN_CLR register to clear off interrupts
      { 0x00,	0x40,	0x50,	0x00},	// W#71	-	Restore XO VMUX to Auto - so that it chooses VREG_XO that runs on AVDD
      { 0x54,	0x03,	0x01,	0xC0},	// W#72	-	Wait for 26ms (= 852 cycles of 32kHz clock)
      { 0x40,	0x91,	0x08,	0x00},	// W#73	-	Write Sequence Complete ACK to PON
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#74	-	EOS (Finish Warm-Reset-Sequence)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#75	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#76	-	
      { 0xA5,	0xD0,	0x44,	0x01},	// W#77	-	Unlock SEC_ACCESS
      { 0x00,	0xE0,	0x44,	0x01},	// W#78	-	LDO5 INT Test access diabled
      { 0x29,	0xB2,	0xF1,	0x01},	// W#79	-	CDC_A_SPKR_DRV_CTL: Disable SPKR Drive from Tombak-Analog Codec Module
      { 0x1F,	0xC3,	0xF1,	0x01},	// W#80	-	CDC_A_BOOST_EN_CTL: Disable Boost Regulator from Tombak-Analog Codec Module
      { 0x00,	0x48,	0x5A,	0x00},	// W#81	-	Disable SMPL
      { 0x20,	0x66,	0x20,	0x01},	// W#82	-	Set OCP_CLR on S5 to clear OCP event on S5 [if occurred] 
      { 0x20,	0x66,	0x23,	0x01},	// W#83	-	Set OCP_CLR on S6 to clear OCP event on S6 [if occurred] 
      { 0xAC,	0x05,	0x4A,	0x89},	// W#84	-	[17] Disable LDO11
      { 0xAC,	0x05,	0x4B,	0x89},	// W#85	-	[16] Disable LDO12
      { 0xAC,	0x05,	0x47,	0x89},	// W#86	-	[15] Disable LDO8
      { 0xAC,	0x05,	0x4C,	0x89},	// W#87	-	[14] Disable LDO13
      { 0xAC,	0x05,	0x0A,	0x88},	// W#88	-	[10] Disable VREF_LPDDR
      { 0xAC,	0x05,	0x41,	0x89},	// W#89	-	[9] Disable LDO2
      { 0xAC,	0x05,	0x44,	0x89},	// W#90	-	[8] Disable LDO5 (This will also pull BBCLK1_EN Pin Low [unless it is already low, during PMIC Sleep], thus actual BB_CLK1 clock & LDO7 o/p will respond to their SW-Enable states)
      { 0xAC,	0x05,	0x5A,	0x88},	// W#91	-	[13] Disable Sleep Clock (L5 Shutdown already gates-off the Clock outside, this command disables the Clock-core)
      { 0xAC,	0x05,	0x51,	0x88},	// W#92	-	[12] Remove SW-Enable from BB_CLK1 Peripheral (Actual clock disable will happen here, i.e. after BBCLK1_EN Pin is Low AND SW-Control is also disabled [internal OR-gate for enabling])
      { 0xAC,	0x05,	0x46,	0x89},	// W#93	-	[11] Remove SW-Enable from LDO7 Peripheral. Actual LDO7 o/p will go down here, i.e. after this setting AND BB_CLK1 clock are disabled. Unless in Sleep, BB_CLK1 will get disabled after being SW-Disabled AND BBCLK1_EN Pin going low
      { 0xAC,	0x05,	0x50,	0x88},	// W#94	-	Disable XO, shuts down the XO core
      { 0xAC,	0x05,	0x53,	0x89},	// W#95	-	Disable LDO_XO 
      { 0xAC,	0x05,	0xA0,	0x88},	// W#96	-	[7] Disable MPP1
      { 0xAC,	0x05,	0xC3,	0x88},	// W#97	-	[3] Disable GPIO4 (to disable boost)
      { 0xAC,	0x05,	0x20,	0x89},	// W#98	-	[6] Disable S5A_6A (Disable Apc)
      { 0xAC,	0x05,	0x17,	0x89},	// W#99	-	[5]Disable S2
      { 0xAC,	0x05,	0x42,	0x89},	// W#100	-	[4] Disable LDO3
      { 0xAC,	0x05,	0x1A,	0x89},	// W#101	-	[2] Disable S3
      { 0xAC,	0x05,	0x1D,	0x89},	// W#102	-	[1] Disable S4 
      { 0x40,	0x91,	0x08,	0x00},	// W#103	-	Write Sequence Complete ACK to PON
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#104	-	EOS (Finish POFF-Sequence)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#105	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#106	-	
      { 0x00,	0x46,	0x00,	0x04},	// W#107	-	Write 0x00 to 0x46 (EN_CTL register in the peripheral)
      { 0x20,	0x00,	0x01,	0xC0},	// W#108	-	Wait 1ms (= 32 cycles of 32kHz clock)
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#109	-	RETURN: Go back to next line of the calling function
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#110	-	
      { 0x00,	0x10,	0x08,	0x40},	// W#111	-	 PON.INT_RT_STS ==> Read value & Store in Local Buffer
      { 0x02,	0x01,	0x02,	0x90},	// W#112	-	 IF RESIN_IN = 1 (i.e. Vol Down Button pressed) then SKIP Next Line
      { 0xD8,	0x05,	0x00,	0x80},	// W#113	-	 VOL- Key Not pressed ==> No DragonFly. End this SEQ immediately
      { 0x60,	0x41,	0x4B,	0x01},	// W#114	-	 LDO12.VOLTAGE_CTL2 = 0x60 ==> Set to 2.95V
      { 0x80,	0x46,	0x4B,	0x01},	// W#115	-	 LDO12: Enable (SD VDDPX_2)
      { 0x60,	0x41,	0x4A,	0x01},	// W#116	-	 LDO11.VOLTAGE_CTL2 = 0x60 ==> Set to 2.95V
      { 0x80,	0x46,	0x4A,	0x01},	// W#117	-	 LDO11: Enable (MEM_SD_MC_VDD)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#118	-	 End of uSD Drangonfly seq
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#119	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#120	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#121	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#122	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#123	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#124	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#125	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#126	-	
      { 0x00,	0x06,	0xFF,	0xF8},	// W#127	-	
   },
   // PBS_RAM_MSM8937.PMIC.HW.PMi8937_2p0_2_0_0
   {
      //data  offset  base_addr  sid
      { 0x2C,	0x04,	0x00,	0x80},	// W#0	-	
      { 0xB4,	0x04,	0x00,	0x80},	// W#1	-	
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#2	-	
      { 0xE4,	0x04,	0x00,	0x80},	// W#3	-	
      { 0x2C,	0x05,	0x00,	0x80},	// W#4	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#5	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#6	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#7	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#8	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#9	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#10	-	
      { 0x00,	0x8A,	0x08,	0x40},	// W#11	-	Read AVDD Regulator to determine if we are entering or exiting sleep
      { 0x20,	0x01,	0x20,	0x90},	// W#12	-	If AVDD_HPM_EN (bit 5) == 1 then skip over to sleep sequence
      { 0x74,	0x04,	0x00,	0x80},	// W#13	-	Else AVDD_HPM_EN == 0 sp we are waking, goto wake sequence
      { 0x00,	0x4A,	0x59,	0x00},	// W#14	-	Force SLEEP_B low
      { 0x10,	0x8A,	0x08,	0x00},	// W#15	-	AVDD LDO in LPM
      { 0x00,	0x08,	0x13,	0x40},	// W#16	-	Read PMIC power path status
      { 0xA5,	0xD0,	0x41,	0x00},	// W#17	-	Unlock Secure Access
      { 0x01,	0x00,	0x03,	0x90},	// W#18	-	Skip if not battery powered
      { 0x00,	0xF5,	0x41,	0x00},	// W#19	-	Disable ESR Estimation in SLEEP
      { 0x01,	0x01,	0x03,	0x90},	// W#20	-	Skip if battery powered
      { 0x01,	0xF5,	0x41,	0x00},	// W#21	-	Enable ESR Estimation in SLEEP
      { 0x01,	0x46,	0x59,	0x00},	// W#22	-	CR843858: Put RC Osc in HW CTL during sleep
      { 0x80,	0x44,	0x2C,	0x00},	// W#23	-	MBG in NPM
      { 0x00,	0x8C,	0x08,	0x40},	// W#24	-	Read PERPH_RB_SPARE, to check if SW wants MBG in LPM
      { 0x02,	0x01,	0x02,	0x90},	// W#25	-	Skip next line if, bit is set, to end sleep seq
      { 0x10,	0x44,	0x2C,	0x00},	// W#26	-	MBG in LPM
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#27	-	End of Sleep seq
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#28	-	Blank space for SW to try experimental changes
      { 0x80,	0x44,	0x2C,	0x00},	// W#29	-	MBG in NPM
      { 0x70,	0x8A,	0x08,	0x00},	// W#30	-	 AVDD LDO in HPM
      { 0x01,	0x4A,	0x59,	0x00},	// W#31	-	SLEEP_B to FOLLOW HW (connected HIGH)
      { 0xA5,	0xD0,	0x41,	0x00},	// W#32	-	Unlock Secure Access
      { 0x01,	0xF5,	0x41,	0x00},	// W#33	-	Enable ESR Estimation coming out of SLEEP
      { 0x80,	0x46,	0x59,	0x00},	// W#34	-	CR165419: Force RC Osc on when awake
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#35	-	End wake sequence
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#36	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#37	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#38	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#39	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#40	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#41	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#42	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#43	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#44	-	
      { 0x54,	0x03,	0x01,	0xC0},	// W#45	-	Wait 26ms
      { 0x40,	0x91,	0x08,	0x00},	// W#46	-	ACK to PON module
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#47	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#48	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#49	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#50	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#51	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#52	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#53	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#54	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#55	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#56	-	
      { 0x00,	0x8D,	0x08,	0x40},	// W#57	-	Read the dvdd_rb_spare register to determin WP vs nWP config
      { 0x01,	0x01,	0x01,	0x90},	// W#58	-	Skip next line if WP config is detected
      { 0xFC,	0x04,	0x00,	0x80},	// W#59	-	nWP config detected, so load non-Wipower settings
      { 0xA5,	0xD0,	0x12,	0x00},	// W#60	-	
      { 0x24,	0xF2,	0x12,	0x00},	// W#61	-	Set AICL threshold to 6.25V/6.75V
      { 0x1C,	0x05,	0x00,	0x80},	// W#62	-	Done loading WiPower Settings
      { 0xA5,	0xD0,	0x13,	0x00},	// W#63	-	
      { 0x00,	0xFF,	0x13,	0x00},	// W#64	-	Disable all bits in WI_PWR_OPTIONS register
      { 0xA5,	0xD0,	0x14,	0x00},	// W#65	-	
      { 0x04,	0xF3,	0x14,	0x00},	// W#66	-	Set DCIN input collapse glitch filter to 5ms falling
      { 0xA5,	0xD0,	0x14,	0x00},	// W#67	-	
      { 0x00,	0xF4,	0x14,	0x00},	// W#68	-	Set DCIN AICL debounce time to 20ms rising and 20ms falling
      { 0xA5,	0xD0,	0x16,	0x00},	// W#69	-	
      { 0x84,	0xF2,	0x16,	0x00},	// W#70	-	Disable UVLO dependent on CHG_OK and disable Charge OK function
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#71	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#72	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#73	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#74	-	
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#75	-	
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#76	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#77	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#78	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#79	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#80	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#81	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#82	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#83	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#84	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#85	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#86	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#87	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#88	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#89	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#90	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#91	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#92	-	
      { 0x01,	0x00,	0x01,	0xC0},	// W#93	-	Wait ~30us
      { 0x00,	0x54,	0x44,	0x40},	// W#94	-	 Read IMA_OPERATION_STS
      { 0x02,	0x01,	0x02,	0x90},	// W#95	-	If IACS_RDY=1 (bit1), then access is ready, skip the next line
      { 0xF4,	0xFF,	0x00,	0x84},	// W#96	-	Access not ready. Poll again.
      { 0xFF,	0xFF,	0xFF,	0x8C},	// W#97	-	IACS_READY=1
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#98	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#99	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#100	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#101	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#102	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#103	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#104	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#105	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#106	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#107	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#108	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#109	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#110	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#111	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#112	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#113	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#114	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#115	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#116	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#117	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#118	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#119	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#120	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#121	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#122	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#123	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#124	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#125	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#126	-	
      { 0x00,	0x00,	0xFF,	0xF8},	// W#127	-	
   },
};
