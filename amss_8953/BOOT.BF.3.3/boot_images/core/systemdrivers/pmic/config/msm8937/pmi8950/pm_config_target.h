/*! \file
 *  
 *  \brief  rpm_settings.c ----This file contains customizable target specific driver settings & PMIC registers.
 *  \details This file contains customizable target specific 
 * driver settings & PMIC registers. This file is generated from database functional
 * configuration information that is maintained for each of the targets.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/config/msm8937/pmi8950/pm_config_target.h#2 $ 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/26/16   rk      Dynamic loading of configi file for  msm8937_pm8937_pmi8950 platform (CR 1018364).
02/05/16   rk      [MSM8937+PMI8952] New PMIC SBL1 (PBS, SBL sequence) (CR-962684)
02/12/15   mr      Added Coincell Drv support for All target (CR-795061)
10/23/13   rk      No of LDOs changed to 20
10/08/13   rk      Added target specific settings
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#define pmi8950_NUM_OF_PMIC             2

#define pmi8950_NUM_OF_GPIO_A           8
#define pmi8950_NUM_OF_MPP_A            4
#define pmi8950_NUM_OF_VIB_A            0
#define pmi8950_NUM_OF_SMPS_A           6
#define pmi8950_NUM_OF_LDO_A            23
#define pmi8950_NUM_OF_LDO_B            1
#define pmi8950_NUM_OF_PBS_CLIENT_A     8
#define pmi8950_NUM_OF_LPG_A            1
#define pmi8950_NUM_OF_LPG_B            1
#define pmi8950_NUM_OF_GPIO_B           2
#define pmi8950_NUM_OF_MPP_B            4
#define pmi8950_NUM_OF_PBS_CLIENT_B     9
#define pmi8950_NUM_OF_VIB_B            1
#define pmi8950_NUM_OF_SMBCHG_B         1
#define pmi8950_NUM_OF_COINCELL_A       1

#define pmi8950_NUM_OF_VIB_B            1
#define pmi8950_NUM_OF_RGB_B            1

#define  pmi8950_NUM_OF_WLED_B       1
#define  pmi8950_NUM_OF_IBB_B        1
#define  pmi8950_NUM_OF_LAB_B        1
#define  pmi8950_NUM_OF_PWM          1
#define  pmi8950_NUM_OF_FG_B         1




