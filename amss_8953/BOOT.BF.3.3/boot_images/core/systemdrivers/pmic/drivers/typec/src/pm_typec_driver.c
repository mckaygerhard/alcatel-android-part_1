

#include "pm_target_information.h"
#include "CoreVerify.h"
#include "pm_typec_driver.h"
#include "pm_typec.h"

/* Static global variable to store the SMBCHG driver data */
static pm_typec_data_type *pm_typec_data_arr[PM_MAX_NUM_PMICS];
static uint8 typec_pmic_index = 0xFF;
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void
pm_typec_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_typec_data_type *typec_ptr = NULL;
    uint32 prop_id_arr[] = {PM_PROP_TYPECA_NUM}; 

    typec_ptr = pm_typec_data_arr[pmic_index];

    if (typec_ptr == NULL)
    {
        pm_malloc( sizeof(pm_typec_data_type), (void**)&typec_ptr);                                                    
        /* Assign Comm ptr */
        typec_ptr->comm_ptr = comm_ptr;

        /* type-c Register Info - Obtaining Data through dal config */
        typec_ptr->typec_register = (typec_register_ds*)pm_target_information_get_common_info((uint32)PM_PROP_TYPEC_REG);
        CORE_VERIFY_PTR(typec_ptr->typec_register);

        /* TYPEC Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));

	    typec_ptr->num_of_peripherals = pm_target_information_get_periph_count_info(prop_id_arr[pmic_index], pmic_index);
        CORE_VERIFY(typec_ptr->num_of_peripherals != 0);
/*
        smbchg_ptr->chg_range_data = (chg_range_data_type*)pm_target_information_get_specific_info((uint32)PM_PROP_SMBCHG_DATA);
        CORE_VERIFY_PTR(smbchg_ptr->chg_range_data);
*/
        pm_typec_data_arr[pmic_index] = typec_ptr;
        typec_pmic_index = pmic_index;
    }
}

pm_typec_data_type* pm_typec_get_data(uint8 pmic_index)
{
  if(pmic_index < PM_MAX_NUM_PMICS)
  {
      return pm_typec_data_arr[pmic_index];
  }

  return NULL;
}

uint8 pm_typec_get_pmic_index()
{
  return typec_pmic_index;
}

