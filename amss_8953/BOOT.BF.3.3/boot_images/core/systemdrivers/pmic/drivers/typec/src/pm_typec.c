/*! \file pm_typec.c
*
*  \brief  pm_typec.c
*  \details Implementation file for TYPEC resourece type.
*
*  &copy; Copyright 2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/23/15   mr      Updated Macro definition to PMIC specific (CR-954241)
11/09/15   pxm     Creation
===========================================================================*/

/*===========================================================================

                    INCLUDE FILES

===========================================================================*/
#include "CoreVerify.h"

#include "pm_comm.h"
#include "pm_typec_driver.h"
#include "pm_typec.h"
#include "pm_utils.h"


/*===========================================================================

                    TYPEDEF AND MACRO DECLARATIONS

===========================================================================*/
#define CHECK_RETURN_VALUE(err)\
    do {\
        if(PM_ERR_FLAG__SUCCESS != (err)) {\
            return err;\
        }\
    }while(0)
#define NULL_POINTER_CHECK(ptr)\
    do {\
        if(NULL == (ptr)) {\
            return PM_ERR_FLAG__INVALID_POINTER;\
        }\
    }while(0)


/*===========================================================================

                    FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_typec_is_vbus_valid(boolean* vbus_valid)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }
    NULL_POINTER_CHECK(vbus_valid);

    address = typec->typec_register->base_address
                + typec->typec_register->status1;

    err = pm_comm_read_byte(typec->comm_ptr->slave_id, address, &data, 0);
    CHECK_RETURN_VALUE(err);
    *vbus_valid = (PM_BIT(5) == (data & PM_BIT(5)));

    return err;
}

pm_err_flag_type pm_typec_get_plug_orientation_status(pm_typec_plug_orientation_type* status)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }
    NULL_POINTER_CHECK(status);

    address = typec->typec_register->base_address
                + typec->typec_register->status1;

    err = pm_comm_read_byte(typec->comm_ptr->slave_id, address, &data, 0);
    CHECK_RETURN_VALUE(err);

    if(data & PM_BIT(6))
    {
        *status = PM_PLUG_OPEN;
    }
    else if(data & PM_BIT(7))
    {
        *status = PM_PLUG_FLIP;
    }
    else
    {
        *status = PM_PLUG_UNFLIP;
    }

    return err;
}

pm_err_flag_type pm_typec_get_ufp_type(pm_typec_ufp_type_type* dfp_type)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }
    NULL_POINTER_CHECK(dfp_type);
    address = typec->typec_register->base_address
                + typec->typec_register->status2;

    err = pm_comm_read_byte(typec->comm_ptr->slave_id, address, &data, 0);
    CHECK_RETURN_VALUE(err);

    if(data & PM_BIT(2))
    {
        *dfp_type = PM_UFP_UNATTACHED;
    }
    else if(data & PM_BIT(3))
    {
        *dfp_type = PM_UFP_AUDIO_ADAPTER;
    }
    else if(data & PM_BIT(4))
    {
        *dfp_type = PM_UFP_DEBUG_ACCESSORY;
    }
    else if(data & PM_BIT(5))
    {
        *dfp_type = PM_UFP_ATTACHED_POWER_CABLE;
    }
    else if(data & PM_BIT(6))
    {
        *dfp_type = PM_UFP_ATTACHED;
    }
    else if(data & PM_BIT(7))
    {
        *dfp_type = PM_UFP_NOT_ATTACHED_POWER_CABLE;
    }

    return err;
}

pm_err_flag_type pm_typec_get_dfp_curr_advertise(pm_typec_current_advertise_type * current_advertise)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }
    NULL_POINTER_CHECK(current_advertise);
    address = typec->typec_register->base_address
                + typec->typec_register->status1;

    err = pm_comm_read_byte(typec->comm_ptr->slave_id, address, &data, 0);
    CHECK_RETURN_VALUE(err);

    if(data & PM_BIT(0))
    {
        *current_advertise = PM_CURR_ADV_3A;
    }
    else if(data & PM_BIT(1))
    {
        *current_advertise = PM_CURR_ADV_1P5A;
    }
    else if(data & PM_BIT(2))
    {
        *current_advertise = PM_CURR_ADV_STANDARD;
    }
    else
    {
        *current_advertise = PM_CURR_ADV_PRIVATE;
    }

    return err;
}

pm_err_flag_type pm_typec_enable(boolean enable)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }

    address = typec->typec_register->base_address
                + typec->typec_register->en_ctl1;
    err = pm_comm_write_byte_mask(typec->comm_ptr->slave_id, address, PM_BIT(7), (enable ? PM_BIT(7) : 0), 0);

    return err;
}

pm_err_flag_type pm_typec_disable_typec_command_sw(boolean enable)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }

    address = typec->typec_register->base_address
                + typec->typec_register->sw_ctl;
    err = pm_comm_write_byte_mask(typec->comm_ptr->slave_id, address, PM_BIT(3), (enable ? PM_BIT(3) : 0), 0);

    return err;
}

pm_err_flag_type pm_typec_set_port_role(pm_typec_port_role_type port_role)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(port_role >= PM_ROLE_INVALID)
    {
        return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }

    address = typec->typec_register->base_address
                + typec->typec_register->sw_ctl;
    if(PM_ROLE_DFP == port_role)
    {
        data = PM_BIT(4);
    }
    else if(PM_ROLE_UFP == port_role)
    {
        data = PM_BIT(5);
    }
    else
    {
        data = 0;
    }

    err = pm_comm_write_byte_mask(typec->comm_ptr->slave_id, address, PM_BITS(5, 4), data, 0);

    return err;
}

pm_err_flag_type pm_typec_vconn_enable(pm_typec_vconn_enable_type enable_type)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type address = 0;
    uint8 data = 0;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(enable_type >= PM_VCONN_INVALID)
    {
        return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

    if(NULL == typec)
    {
        return PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT;
    }

    address = typec->typec_register->base_address
                + typec->typec_register->sw_ctl;

    if(PM_VCONN_CONTROL_BY_HW == enable_type)
    {
        data = 0;

        err = pm_comm_write_byte_mask(typec->comm_ptr->slave_id, address, PM_BIT(7), data, 0);
    }
    else
    {
        if(PM_VCONN_ENABLE_BY_SW == enable_type)
        {
            data = PM_BITS(7, 6);
        }
        else if(PM_VCONN_DISABLE_BY_SW == enable_type)
        {
            data = PM_BIT(7);
        }

        err = pm_comm_write_byte_mask(typec->comm_ptr->slave_id, address, PM_BITS(7, 6), data, 0);
    }

    return err;
}
