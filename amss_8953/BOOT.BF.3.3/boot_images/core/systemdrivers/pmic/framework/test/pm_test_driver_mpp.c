/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM CLK DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
/* For TEST/DEBUG purpose */
pm_mpp_status_type mpp_status;
uint32 g_shunt_list_status;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
 /**
 * @name pm_test_driver_mpp_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_mpp_level_0 (uint8 pmic_index)
{
    uint8 i;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    for(i = 0; i < g_pm_test_mpp_num; i++)
    {
        memset(&mpp_status, 0, sizeof(mpp_status));

        err_flag = pm_mpp_config_digital_input(pmic_index, (pm_mpp_perph_index)i, PM_MPP__DLOGIC__LVL_VIO_4, (pm_mpp_dlogic_in_dbus_type)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_DIGITAL_INPUT, i, err_flag);
        }

        err_flag = pm_mpp_config_digital_output (pmic_index, (pm_mpp_perph_index)i, PM_MPP__DLOGIC__LVL_VIO_4, PM_MPP__DLOGIC_OUT__CTRL_MPP);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_DIGITAL_OUTPUT, i, err_flag);
        }

        err_flag = pm_mpp_config_digital_inout (pmic_index, (pm_mpp_perph_index)i, PM_MPP__DLOGIC__LVL_VIO_4, PM_MPP__DLOGIC_INOUT__PUP_30K);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_DIGITAL_INOUT, i, err_flag);
        }

        err_flag = pm_mpp_config_analog_input (pmic_index, (pm_mpp_perph_index)i, PM_MPP__AIN__CH_ABUS2);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_ANALOG_INPUT, i, err_flag);
        }

        err_flag = pm_mpp_config_analog_output (pmic_index, (pm_mpp_perph_index)i, PM_MPP__AOUT__LEVEL_VREF_1p25_Volts, PM_MPP__AOUT__SWITCH_ON_IF_MPP_HIGH);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_ANALOG_OUTPUT, i, err_flag);
        }

        err_flag = pm_mpp_config_i_sink (pmic_index, (pm_mpp_perph_index)i, PM_MPP__I_SINK__LEVEL_25mA, PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_HIGH);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_I_SINK, i, err_flag);
        }

        err_flag = pm_mpp_config_atest (pmic_index, (pm_mpp_perph_index)i, PM_MPP__CH_ATEST4);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_CONFIG_ATEST, i, err_flag);
        }

        // TODO - API is not implemented.
        // err_flag = pm_mpp_config_dtest_output(pmic_index, (pm_mpp_perph_index)i, PM_MPP__DLOGIC__LVL_VIO_2, PM_MPP__DLOGIC_OUT__DBUS2);
        // if(PM_ERR_FLAG__SUCCESS != err_flag)
        // {
            // pm_test_handle_error(pmic_index, PM_MPP_CONFIG_DTEST_OUTPUT, i, err_flag);
        // }

        err_flag = pm_mpp_set_list_mpp_with_shunt_cap(pmic_index, (pm_mpp_perph_index)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_SET_LIST_MPP_WITH_SHUNT_CAP, i, err_flag);
        }

        err_flag = pm_mpp_get_mpp_with_shunt_cap_list_status_for_device(pmic_index, (pm_mpp_perph_index)i, &g_shunt_list_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_GET_MPP_WITH_SHUNT_CAP_LIST_STATUS_FOR_DEVICE, i, err_flag);
        }

        err_flag = pm_mpp_enable(pmic_index, (pm_mpp_perph_index)i, TRUE);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_ENABLE, i, err_flag);
        }

        err_flag = pm_mpp_status_get (pmic_index, (pm_mpp_perph_index)i, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_MPP_STATUS_GET, i, err_flag);
        }
    }

    return err_flag;
}

#if 0    /* TODO */
/**
 * @name pm_test_driver_mpp_level_1
 *
 * @brief This function tests for GPIO Driver (SET-GET) APIs' successful return
 *        and cross verification on outcome.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_mpp_level_1 (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

	if(0 == g_pm_test_mpp_num)
	{
		return err_flag;
	}

    memset(&mpp_status, 0, sizeof(mpp_status));

    err_flag = pm_gpio_config_bias_voltage (pmic_index, PM_GPIO_1, PM_GPIO_VIN3);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_1, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_VIN3 != mpp_status.gpio_voltage_source)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, PM_GPIO_1, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_set_volt_source (pmic_index, PM_GPIO_2, PM_GPIO_VIN0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_VOLTAGE_SOURCE, PM_GPIO_2, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_2, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_VIN0 != mpp_status.gpio_voltage_source)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_VOLTAGE_SOURCE, PM_GPIO_2, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_config_digital_input (pmic_index, PM_GPIO_3, PM_GPIO_I_SOURCE_PULL_UP_31_5uA, PM_GPIO_VIN1,
                                                 PM_GPIO_OUT_BUFFER_MEDIUM, PM_GPIO_SOURCE_DTEST2);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_INPUT, PM_GPIO_3, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_3, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag ||
           PM_GPIO_I_SOURCE_PULL_UP_31_5uA != mpp_status.gpio_current_source_pulls || PM_GPIO_VIN1 != mpp_status.gpio_voltage_source ||
           PM_GPIO_OUT_BUFFER_MEDIUM != mpp_status.gpio_out_buffer_drive_strength || PM_GPIO_SOURCE_DTEST2 != mpp_status.gpio_source_config)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_INPUT, PM_GPIO_3, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_config_digital_output (pmic_index, PM_GPIO_4, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_PMOS,
                                                  PM_GPIO_VIN2, PM_GPIO_SOURCE_DTEST3, PM_GPIO_OUT_BUFFER_HIGH, 0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_OUTPUT, PM_GPIO_4, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_4, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag ||
           PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_PMOS != mpp_status.gpio_out_buffer_config || PM_GPIO_VIN2 != mpp_status.gpio_voltage_source ||
           PM_GPIO_OUT_BUFFER_HIGH != mpp_status.gpio_out_buffer_drive_strength || PM_GPIO_SOURCE_DTEST3 != mpp_status.gpio_source_config ||
           0 != mpp_status.gpio_invert_ext_pin)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_OUTPUT, PM_GPIO_4, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_config_digital_input_output (pmic_index, PM_GPIO_2, PM_GPIO_SOURCE_DTEST4, PM_GPIO_I_SOURCE_PULL_UP_1_5uA,
                                                        PM_GPIO_VIN3, PM_GPIO_OUT_BUFFER_CONFIG_CMOS, PM_GPIO_OUT_BUFFER_HIGH);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, PM_GPIO_2, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_2, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_SOURCE_DTEST4 != mpp_status.gpio_source_config ||
           PM_GPIO_I_SOURCE_PULL_UP_1_5uA != mpp_status.gpio_current_source_pulls || PM_GPIO_OUT_BUFFER_CONFIG_CMOS != mpp_status.gpio_out_buffer_config ||
           PM_GPIO_VIN3 != mpp_status.gpio_voltage_source || PM_GPIO_OUT_BUFFER_HIGH != mpp_status.gpio_out_buffer_drive_strength)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, PM_GPIO_2, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_set_inversion_configuration (pmic_index, PM_GPIO_4, 1);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_INVERSION_CONFIGURATION, PM_GPIO_4, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_4, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || 1 != mpp_status.gpio_invert_ext_pin)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_INVERSION_CONFIGURATION, PM_GPIO_4, err_flag);
        }

        memset(&mpp_status, 0, sizeof(mpp_status));
    }

    err_flag = pm_gpio_set_output_buffer_configuration (pmic_index, PM_GPIO_2, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, PM_GPIO_2, err_flag);
    }
    else
    {
        err_flag = pm_mpp_status_get (pmic_index, PM_GPIO_2, &mpp_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS != mpp_status.gpio_out_buffer_config)
        {
            pm_test_handle_error(pmic_index, PM_DEV_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, PM_GPIO_2, err_flag);
        }
    }

    return err_flag;
}
#endif
