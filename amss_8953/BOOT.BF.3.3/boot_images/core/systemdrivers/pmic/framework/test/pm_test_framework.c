/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                PM TEST FRAMEWORKsss

GENERAL DESCRIPTION
  This file contains Framework Driver test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/23/15   mr      Added Unit Tests for TypeC module. (CR-954241)
12/14/15   sv      Added Bootlog for testframework.
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
03/23/15   mr      Created.
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
pm_test_driver_state g_pm_test_status[PM_TEST_ARRAY_SIZE];
static uint8 g_f_cnt = 0;

uint8  g_pmic_index;
uint32 g_pm_test_clk_num;
uint32 g_pm_test_gpio_num;
uint32 g_pm_test_ldo_num;
uint32 g_pm_test_lpg_num;
uint32 g_pm_test_mpp_num;
uint32 g_pm_test_rgb_num;
uint32 g_pm_test_smps_num;
uint32 g_pm_test_typec_num;

uint32 g_pm_clk_prop_id_arr[]  = {PM_PROP_CLKA_NUM,  PM_PROP_CLKB_NUM,  PM_PROP_CLKC_NUM};
uint32 g_pm_gpio_prop_id_arr[] = {PM_PROP_GPIOA_NUM, PM_PROP_GPIOB_NUM, PM_PROP_GPIOC_NUM};
uint32 g_pm_ldo_prop_id_arr[]  = {PM_PROP_LDOA_NUM,  PM_PROP_LDOB_NUM,  PM_PROP_LDOC_NUM};
uint32 g_pm_lpg_prop_id_arr[]  = {PM_PROP_LPGA_NUM,  PM_PROP_LPGB_NUM,  PM_PROP_LPGC_NUM};
uint32 g_pm_mpp_prop_id_arr[]  = {PM_PROP_MPPA_NUM,  PM_PROP_MPPB_NUM,  PM_PROP_MPPC_NUM};
uint32 g_pm_rgb_prop_id_arr[]  = {PM_PROP_RGBA_NUM,  PM_PROP_RGBB_NUM,  PM_PROP_RGBC_NUM};
uint32 g_pm_smps_prop_id_arr[] = {PM_PROP_SMPSA_NUM, PM_PROP_SMPSB_NUM, PM_PROP_SMPSC_NUM};
uint32 g_pm_typec_prop_id_arr[] = {PM_PROP_TYPECA_NUM};

#define PM_SBL_TEST_FRMWRK_MSG_LEN  68
char test_frmWrk_message[PM_SBL_TEST_FRMWRK_MSG_LEN];


/*===========================================================================

                TEST FUNCTION DEFINITIONS

===========================================================================*/
/* Error logging function */
void pm_test_handle_error(uint8 pmic_index, pm_test_apis api, uint8 rcrs, pm_err_flag_type error)
{
    if (g_f_cnt <= PM_TEST_ARRAY_SIZE-1)
    {
        g_pm_test_status[g_f_cnt].api = api;
        g_pm_test_status[g_f_cnt].rcrs_id = rcrs;
        g_pm_test_status[g_f_cnt].err_code = error;
        g_pm_test_status[g_f_cnt].pm_index = pmic_index;
        snprintf(test_frmWrk_message, PM_SBL_TEST_FRMWRK_MSG_LEN, "PmicIndex= %d, API= %d, Resource ID=%d, Error Code=%d, Test Level=%d", g_pm_test_status[g_f_cnt].pm_index, g_pm_test_status[g_f_cnt].api, g_pm_test_status[g_f_cnt].rcrs_id, g_pm_test_status[g_f_cnt].err_code, g_pm_test_status[g_f_cnt].lvl);
        boot_log_message(test_frmWrk_message);
    }
    ++g_f_cnt;

    if (g_f_cnt < PM_TEST_ARRAY_SIZE)
    {
        g_pm_test_status[g_f_cnt].lvl = g_pm_test_status[g_f_cnt-1].lvl;
    }
}

void pm_test_get_periph_count_info(uint8 pmic_index)
{
    g_pm_test_clk_num = pm_target_information_get_periph_count_info(g_pm_clk_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_gpio_num = pm_target_information_get_periph_count_info(g_pm_gpio_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_ldo_num = pm_target_information_get_periph_count_info(g_pm_ldo_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_lpg_num = pm_target_information_get_periph_count_info(g_pm_lpg_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_mpp_num = pm_target_information_get_periph_count_info(g_pm_mpp_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_rgb_num = pm_target_information_get_periph_count_info(g_pm_rgb_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_smps_num = pm_target_information_get_periph_count_info(g_pm_smps_prop_id_arr[pmic_index], pmic_index);
    g_pm_test_typec_num = pm_target_information_get_periph_count_info(g_pm_typec_prop_id_arr[pmic_index], pmic_index);
}

/**
 * LEVEL 0 - Driver APIs' Test for successful execution.
 */
pm_err_flag_type pm_test_driver_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    g_pm_test_status[g_f_cnt].lvl = PM_TEST_DRIVER_LVL_0;

    err_flag  = pm_test_driver_ldo_level_0(pmic_index);
    err_flag |= pm_test_driver_smps_level_0(pmic_index);
    err_flag |= pm_test_driver_clk_level_0(pmic_index);
    err_flag |= pm_test_driver_gpio_level_0(pmic_index);
    //err_flag |= pm_test_driver_mpp_level_0(pmic_index);
    err_flag |= pm_test_driver_pon_level_0(pmic_index);
    //err_flag |= pm_test_driver_lpg_level_0(pmic_index);
    err_flag |= pm_test_driver_rgb_level_0(pmic_index);
    err_flag |= pm_test_driver_rtc_level_0(pmic_index);
    err_flag |= pm_test_driver_coincell_level_0(pmic_index);
    //err_flag |= pm_test_driver_vib_level_0(pmic_index);
    err_flag |= pm_test_driver_typec_level_0(pmic_index);

    return err_flag;
}

/**
 * LEVEL 1 - Driver SET-GET APIs' Tests with working params for successful execution.
 */
pm_err_flag_type pm_test_driver_level_1 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    g_pm_test_status[g_f_cnt].lvl = PM_TEST_DRIVER_LVL_1;

    err_flag  = pm_test_driver_ldo_level_1(pmic_index);
    err_flag |= pm_test_driver_smps_level_1(pmic_index);
    err_flag |= pm_test_driver_gpio_level_1(pmic_index);
    // err_flag = pm_test_driver_mpp_level_1(pmic_index);
    err_flag = pm_test_driver_pon_level_1(pmic_index);
    err_flag |= pm_test_driver_typec_level_1(pmic_index);

    return err_flag;
}

#if 0    /* TODO - Functions not implemented in BOOT Test Framework */
/**
 * LEVEL 2 - Driver SET-GET APIs' Tests with all possible params combination for successful execution.
 */
pm_err_flag_type pm_test_driver_level_2 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    g_pm_test_status[g_f_cnt].lvl = PM_TEST_DRIVER_LVL_2;

    err_flag  = pm_test_driver_ldo_level_2(pmic_index);
    err_flag |= pm_test_driver_smps_level_2(pmic_index);
    err_flag |= pm_test_driver_gpio_level_2(pmic_index);
    // err_flag = pm_test_driver_mpp_level_2(pmic_index);

    return err_flag;
}
#endif

/**
 * Main function to call all three level test functions.
 * This function is exported for explicit tests.
 */
pm_err_flag_type pm_test_framework (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    boot_log_message("pm_test_framework: Start");
    memset(g_pm_test_status, 0, sizeof(g_pm_test_status));

    for(g_pmic_index = 0; g_pmic_index <= 2; g_pmic_index++)
    {
        pm_test_get_periph_count_info(g_pmic_index);

        err_flag  = pm_test_driver_level_0(g_pmic_index);
        err_flag |= pm_test_driver_level_1(g_pmic_index);
        // err_flag |= pm_test_driver_level_2(g_pmic_index);

        // pm_test_unit_testcases();
    }

    boot_log_message("pm_test_framework: End");
    return err_flag;
}
