/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM LDO DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
/* For TEST/DEBUG purpose */  /* Same varialbes are used for SMPS Driver */
pm_pon_poff_reason_type g_poff_reason;
pm_pon_warm_reset_reason_type g_warmreset_reason;
pm_pon_soft_reset_reason_type g_softreset_reason;
uint64 g_all_reasons;
boolean g_pon_status;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
 /**
 * @name pm_test_driver_pon_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_pon_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    err_flag = pm_pon_overtemp_reset_cfg(pmic_index, PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_OVERTEMP_RESET_CFG, 1, err_flag);
    }

    err_flag = pm_pon_ps_hold_cfg (pmic_index, PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_PS_HOLD_CFG, 1, err_flag);
    }

    err_flag = pm_pon_reset_source_cfg (pmic_index, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, 3072, 1000, PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_RESET_SOURCE_CFG, 1, err_flag);
    }

    err_flag = pm_pon_reset_source_ctl (pmic_index, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, PM_ON);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_RESET_SOURCE_CTL, 1, err_flag);
    }

    err_flag = pm_pon_stage3_reset_source_cfg (pmic_index, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, 64);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_STAGE3_RESET_SOURCE_CFG, 1, err_flag);
    }

    err_flag = pm_pon_get_poff_reason (pmic_index, &g_poff_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_POFF_REASON, 1, err_flag);
    }

    err_flag = pm_pon_get_warm_reset_reason (pmic_index, &g_warmreset_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_WARM_RESET_REASON, 1, err_flag);
    }

    err_flag = pm_pon_get_soft_reset_reason (pmic_index, &g_softreset_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_SOFT_RESET_REASON, 1, err_flag);
    }

    err_flag = pm_pon_get_all_pon_reasons (pmic_index, &g_all_reasons);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_ALL_PON_REASONS, 1, err_flag);
    }

    err_flag = pm_pon_irq_enable (pmic_index, PM_PON_IRQ_KPDPWR_BARK, 1);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_IRQ_ENABLE, 1, err_flag);
    }

    err_flag = pm_pon_irq_clear (pmic_index, PM_PON_IRQ_KPDPWR_BARK);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_IRQ_CLEAR, 1, err_flag);
    }

    err_flag = pm_pon_irq_set_trigger (pmic_index, PM_PON_IRQ_KPDPWR_BARK, PM_IRQ_TRIGGER_RISING_EDGE);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_IRQ_SET_TRIGGER, 1, err_flag);
    }

    err_flag = pm_pon_irq_status (pmic_index, PM_PON_IRQ_KPDPWR_BARK, PM_IRQ_STATUS_PENDING, &g_pon_status);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_IRQ_STATUS, 1, err_flag);
    }

    err_flag = pm_pon_config_uvlo (pmic_index, 3000, 300); // range1: 2875 - 3225; range2: 175 - 425
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_CONFIG_UVLO, 1, err_flag);
    }

    err_flag = pm_pon_warm_reset_status (pmic_index, &g_pon_status);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_WARM_RESET_STATUS, 1, err_flag);
    }

    err_flag = pm_pon_warm_reset_status_clear (0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_WARM_RESET_STATUS_CLEAR, 1, err_flag);
    }

    return err_flag;
}

/**
 * @name pm_test_driver_pon_level_1
 *
 * @brief This function tests for SET-GET APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_pon_level_1 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 i = 0, j = 0;

    for(i = 0; (pm_pon_reset_cfg_type)i < PM_PON_RESET_CFG_INVALID; i++)
    {
        err_flag = pm_pon_overtemp_reset_cfg(pmic_index, (pm_pon_reset_cfg_type)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_OVERTEMP_RESET_CFG, (uint8)i, err_flag);
        }
    }

    for(i = 0; (pm_pon_reset_cfg_type)i < PM_PON_RESET_CFG_INVALID; i++)
    {
        err_flag = pm_pon_ps_hold_cfg (pmic_index, (pm_pon_reset_cfg_type)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_PS_HOLD_CFG, (uint8)i, err_flag);
        }
    }

    for(i = 0; (pm_pon_reset_source_type)i < PM_PON_RESET_SOURCE_INVALID; i++)
    {
        for(j = 0; (pm_pon_reset_cfg_type)j < PM_PON_RESET_CFG_INVALID; j++)
        {
            err_flag = pm_pon_reset_source_cfg (pmic_index, (pm_pon_reset_source_type)i, 3072, 1000, (pm_pon_reset_cfg_type)j);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_PON_RESET_SOURCE_CFG, (uint8)i, err_flag);
            }
        }
    }

    for(i = 0; (pm_pon_reset_source_type)i < PM_PON_RESET_SOURCE_INVALID; i++)
    {
        err_flag = pm_pon_reset_source_ctl (pmic_index, (pm_pon_reset_source_type)i, PM_ON);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_RESET_SOURCE_CTL, (uint8)i, err_flag);
        }
    }

    for(i = 0; (pm_pon_reset_source_type)i < PM_PON_RESET_SOURCE_INVALID; i++)
    {
        err_flag = pm_pon_stage3_reset_source_cfg (pmic_index, (pm_pon_reset_source_type)i, 64);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_STAGE3_RESET_SOURCE_CFG, (uint8)i, err_flag);
        }
    }

    err_flag = pm_pon_get_poff_reason (pmic_index, &g_poff_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_POFF_REASON, (uint8)i, err_flag);
    }

    err_flag = pm_pon_get_warm_reset_reason (pmic_index, &g_warmreset_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_WARM_RESET_REASON, (uint8)i, err_flag);
    }

    err_flag = pm_pon_get_soft_reset_reason (pmic_index, &g_softreset_reason);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_SOFT_RESET_REASON, (uint8)i, err_flag);
    }

    err_flag = pm_pon_get_all_pon_reasons (pmic_index, &g_all_reasons);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_GET_ALL_PON_REASONS, (uint8)i, err_flag);
    }

    for(i = 0; (pm_pon_irq_type)i < PM_PON_IRQ_INVALID; i++)
    {
        err_flag = pm_pon_irq_enable (pmic_index, (pm_pon_irq_type)i, 1);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_IRQ_ENABLE, (uint8)i, err_flag);
        }
    }

    for(i = 0; (pm_pon_irq_type)i < PM_PON_IRQ_INVALID; i++)
    {
        err_flag = pm_pon_irq_clear (pmic_index, (pm_pon_irq_type)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_PON_IRQ_CLEAR, (uint8)i, err_flag);
        }
    }

    for(i = 0; (pm_pon_irq_type)i < PM_PON_IRQ_INVALID; i++)
    {
        for(j = 0; (pm_irq_trigger_type)j < PM_IRQ_TRIGGER_INVALID; j++)
        {
            err_flag = pm_pon_irq_set_trigger (pmic_index, (pm_pon_irq_type)i, (pm_irq_trigger_type)j);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_PON_IRQ_SET_TRIGGER, (uint8)i, err_flag);
            }
        }
    }

    for(i = 0; (pm_pon_irq_type)i < PM_PON_IRQ_INVALID; i++)
    {
        for(j = 0; (pm_irq_status_type)j < PM_IRQ_STATUS_INVALID; j++)
        {
            err_flag = pm_pon_irq_status (pmic_index, (pm_pon_irq_type)i, (pm_irq_status_type)j, &g_pon_status);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_PON_IRQ_STATUS, (uint8)i, err_flag);
            }
        }
    }

    err_flag = pm_pon_config_uvlo (pmic_index, 3000, 300);    // range1: 2875 - 3225; range2: 175 - 425
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_CONFIG_UVLO, (uint8)i, err_flag);
    }

    err_flag = pm_pon_warm_reset_status (pmic_index, &g_pon_status);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_WARM_RESET_STATUS, (uint8)i, err_flag);
    }

    err_flag = pm_pon_warm_reset_status_clear (0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_PON_WARM_RESET_STATUS_CLEAR, (uint8)i, err_flag);
    }

    return err_flag;
}

