    /*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                PM TEST GPIO DRIVER

GENERAL DESCRIPTION
  This file contains GPIO Driver test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
08/26/14   mr      Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                 LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/
/* TARGET Specific Info - Needs to be updated for different Targets */
#define PM_SUPPORTED_GPIO_VIN     4


/*===========================================================================

                VARIABLES DEFINITIONS

===========================================================================*/
/* For TEST/DEBUG purpose */
pm_gpio_status_type gpio_status;


/*===========================================================================

                LOCAL FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_test_driver_gpio_level_0
 *
 * @brief This function tests for GPIO Driver APIs' successful return
 *        by passing working params.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note Need to read Periph register values after setting configuration.
 *
 */
pm_err_flag_type pm_test_driver_gpio_level_0 (uint8 pmic_index)
{
    uint8 i;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        memset(&gpio_status, 0, sizeof(gpio_status));
        //Venu stub start
	err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_STATUS_GET, i, err_flag);
        }
	else if( gpio_status.gpio_ext_pin_config == TRUE) // if GPIO is enabled, dont perform any GPIO configurations
	{		    
	    continue;
	}
	// Venu stub end
        err_flag = pm_gpio_config_bias_voltage (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_VIN1);    /** 0xC040 = 0x11, 0xC041 = 0x01, 0xC045 = 0x03, 0xC046 = 0x80 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, i, err_flag);
        }

        err_flag = pm_gpio_set_volt_source (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_VIN2);    /** 0xC041 = 0x02 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_VOLT_SOURCE, i, err_flag);
        }

        err_flag = pm_gpio_config_digital_input (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_I_SOURCE_PULL_UP_1_5uA, PM_GPIO_VIN3,
                                                     PM_GPIO_OUT_BUFFER_HIGH, PM_GPIO_SOURCE_DTEST1);    /** 0xC040 = 0x08, 0xC041 = 0x03, 0xC042 = 0x01, 0xC045 = 0x03, 0xC046 = 0x80 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT, i, err_flag);
        }

        err_flag = pm_gpio_config_digital_output (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS,
                                                      PM_GPIO_VIN0, PM_GPIO_SOURCE_DTEST2, PM_GPIO_OUT_BUFFER_MEDIUM, 1);    /** 0xC040 = 0x1B, 0xC041 = 0x00, 0xC045 = 0x12, 0xC046 = 0x80 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_OUTPUT, i, err_flag);
        }

        err_flag = pm_gpio_config_digital_input_output (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_SOURCE_DTEST3, PM_GPIO_I_SOURCE_PULL_DOWN_10uA,
                                                            PM_GPIO_VIN2, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS, PM_GPIO_OUT_BUFFER_MEDIUM);    /** 0xC040 = 0x2C, 0xC041 = 0x02, 0xC042 = 0x04, 0xC045 = 0x22, 0xC046 = 0x80 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, i, err_flag);
        }

        err_flag = pm_gpio_set_inversion_configuration (pmic_index, (pm_gpio_perph_index)i, 0);    /** 0xC041 = 0x*0 */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_INVERSION_CONFIGURATION, i, err_flag);
        }

        err_flag = pm_gpio_set_output_buffer_configuration (pmic_index, (pm_gpio_perph_index)i, PM_GPIO_OUT_BUFFER_CONFIG_CMOS);    /** 0xC045 = 0x0* */
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, i, err_flag);
        }

        err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_STATUS_GET, i, err_flag);
        }
    }

    return err_flag;
}

/**
 * @name pm_test_driver_gpio_level_1
 *
 * @brief This function tests for GPIO Driver (SET-GET) APIs' successful return
 *        and cross verification on outcome.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_gpio_level_1 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

	if(0 == g_pm_test_gpio_num)
	{
		return err_flag;
	}

    memset(&gpio_status, 0, sizeof(gpio_status));

    err_flag = pm_gpio_config_bias_voltage (pmic_index, PM_GPIO_1, PM_GPIO_VIN3);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_VIN3 != gpio_status.gpio_volt_source)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_set_volt_source (pmic_index, PM_GPIO_1, PM_GPIO_VIN0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_SET_VOLT_SOURCE, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_VIN0 != gpio_status.gpio_volt_source)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_VOLT_SOURCE, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_config_digital_input (pmic_index, PM_GPIO_1, PM_GPIO_I_SOURCE_PULL_UP_31_5uA, PM_GPIO_VIN1,
                                                 PM_GPIO_OUT_BUFFER_MEDIUM, PM_GPIO_SOURCE_DTEST2);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag ||
           PM_GPIO_I_SOURCE_PULL_UP_31_5uA != gpio_status.gpio_current_src_pulls || PM_GPIO_VIN1 != gpio_status.gpio_volt_source ||
           PM_GPIO_OUT_BUFFER_MEDIUM != gpio_status.gpio_out_buffer_drv_strength || PM_GPIO_SOURCE_DTEST2 != gpio_status.gpio_src_config)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_config_digital_output (pmic_index, PM_GPIO_1, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS,
                                                  PM_GPIO_VIN2, PM_GPIO_SOURCE_DTEST3, PM_GPIO_OUT_BUFFER_HIGH, 0);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_OUTPUT, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag ||
           PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS != gpio_status.gpio_out_buffer_config || PM_GPIO_VIN2 != gpio_status.gpio_volt_source ||
           PM_GPIO_OUT_BUFFER_HIGH != gpio_status.gpio_out_buffer_drv_strength || PM_GPIO_SOURCE_DTEST3 != gpio_status.gpio_src_config ||
           0 != gpio_status.gpio_invert_ext_pin_config)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_OUTPUT, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_config_digital_input_output (pmic_index, PM_GPIO_1, PM_GPIO_SOURCE_DTEST4, PM_GPIO_I_SOURCE_PULL_UP_1_5uA,
                                                        PM_GPIO_VIN3, PM_GPIO_OUT_BUFFER_CONFIG_CMOS, PM_GPIO_OUT_BUFFER_HIGH);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_SOURCE_DTEST4 != gpio_status.gpio_src_config ||
           PM_GPIO_I_SOURCE_PULL_UP_1_5uA != gpio_status.gpio_current_src_pulls || PM_GPIO_OUT_BUFFER_CONFIG_CMOS != gpio_status.gpio_out_buffer_config ||
           PM_GPIO_VIN3 != gpio_status.gpio_volt_source || PM_GPIO_OUT_BUFFER_HIGH != gpio_status.gpio_out_buffer_drv_strength)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_set_inversion_configuration (pmic_index, PM_GPIO_1, 1);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_SET_INVERSION_CONFIGURATION, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || 1 != gpio_status.gpio_invert_ext_pin_config)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_INVERSION_CONFIGURATION, PM_GPIO_1, err_flag);
        }

        memset(&gpio_status, 0, sizeof(gpio_status));
    }

    err_flag = pm_gpio_set_output_buffer_configuration (pmic_index, PM_GPIO_1, PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, PM_GPIO_1, err_flag);
    }
    else
    {
        err_flag = pm_gpio_status_get (pmic_index, PM_GPIO_1, &gpio_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag || PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN_NMOS != gpio_status.gpio_out_buffer_config)
        {
            pm_test_handle_error(pmic_index, PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, PM_GPIO_1, err_flag);
        }
    }

    return err_flag;
}

/**
 * @name pm_test_driver_gpio_level_2
 *
 * @brief This function tests for GPIO Driver (SET-GET) APIs' with cross verification on outcome.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note Covers all possible combinations for Periph configuration.
 *
 */
pm_err_flag_type pm_test_driver_gpio_level_2 (uint8 pmic_index)
{
    uint8 i = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    pm_gpio_volt_src_type vltg_src;
    pm_gpio_current_src_pulls_type i_src_pulls;
    pm_gpio_out_buffer_config_type o_buf_config;
    pm_gpio_out_buffer_drv_strength_type out_buf_drive;
    pm_gpio_src_config_type src_config;
    boolean out_inversion;

    memset(&gpio_status, 0, sizeof(gpio_status));

/** GPIO VOLTAGE SRC CONFIG-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(vltg_src = PM_GPIO_VIN0; vltg_src < PM_SUPPORTED_GPIO_VIN; vltg_src++)
        {
            err_flag = pm_gpio_config_bias_voltage (pmic_index, (pm_gpio_perph_index)i, vltg_src);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, i, err_flag);
            }
            else
            {
                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                if(PM_ERR_FLAG__SUCCESS != err_flag || vltg_src != gpio_status.gpio_volt_source)
                {
                    pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_BIAS_VOLTAGE, i, err_flag);
                }
            }

            memset(&gpio_status, 0, sizeof(gpio_status));
        }
    }

/** GPIO VOLTAGE SRC SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(vltg_src = PM_GPIO_VIN0; vltg_src < PM_SUPPORTED_GPIO_VIN; vltg_src++)
        {
            err_flag = pm_gpio_set_volt_source (pmic_index, (pm_gpio_perph_index)i, vltg_src);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_GPIO_SET_VOLT_SOURCE, i, err_flag);
            }
            else
            {
                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                if(PM_ERR_FLAG__SUCCESS != err_flag || vltg_src != gpio_status.gpio_volt_source)
                {
                    pm_test_handle_error(pmic_index, PM_GPIO_SET_VOLT_SOURCE, i, err_flag);
                }
            }

            memset(&gpio_status, 0, sizeof(gpio_status));
        }
    }

/** GPIO DIGITAL INPUT CONFIG SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(i_src_pulls = PM_GPIO_I_SOURCE_PULL_UP_30uA; i_src_pulls < PM_GPIO_CURRENT_SOURCE_PULLS_TYPE__INVALID; i_src_pulls++)
        {
            for(vltg_src = PM_GPIO_VIN0; vltg_src < PM_SUPPORTED_GPIO_VIN; vltg_src++)
            {
                for(out_buf_drive = PM_GPIO_OUT_BUFFER_LOW; out_buf_drive < PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID; out_buf_drive++)
                {
                    for(src_config = PM_GPIO_SOURCE_GND; src_config < PM_GPIO_SOURCE_CONFIG_TYPE__INVALID; src_config++)
                    {
                        err_flag = pm_gpio_config_digital_input (pmic_index, (pm_gpio_perph_index)i, i_src_pulls, vltg_src, out_buf_drive, src_config);
                        if(PM_ERR_FLAG__SUCCESS != err_flag)
                        {
                            pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT, i, err_flag);
                        }
                        else
                        {
                            err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                            if(PM_ERR_FLAG__SUCCESS != err_flag ||
                               i_src_pulls != gpio_status.gpio_current_src_pulls || vltg_src != gpio_status.gpio_volt_source ||
                               out_buf_drive != gpio_status.gpio_out_buffer_drv_strength || src_config != gpio_status.gpio_src_config)
                            {
                                pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT, i, err_flag);
                            }
                        }

                        memset(&gpio_status, 0, sizeof(gpio_status));
                    }
                }
            }
        }
    }

/** GPIO DIGITAL OUTPUT CONFIG SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(o_buf_config = PM_GPIO_OUT_BUFFER_CONFIG_CMOS; o_buf_config < PM_GPIO_OUT_BUFFER_CONFIG__INVALID; o_buf_config++)
        {
            for(vltg_src = PM_GPIO_VIN0; vltg_src < PM_SUPPORTED_GPIO_VIN; vltg_src++)
            {
                for(src_config = PM_GPIO_SOURCE_DTEST1; src_config < PM_GPIO_SOURCE_CONFIG_TYPE__INVALID; src_config++)
                {
                    for(out_buf_drive = PM_GPIO_OUT_BUFFER_LOW; out_buf_drive < PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID; out_buf_drive++)
                    {
                        for(out_inversion = 0; out_inversion < 2; out_inversion++)
                        {
                            err_flag = pm_gpio_config_digital_output (pmic_index, (pm_gpio_perph_index)i, o_buf_config, vltg_src, src_config, out_buf_drive, out_inversion);
                            if(PM_ERR_FLAG__SUCCESS != err_flag)
                            {
                                pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_OUTPUT, i, err_flag);
                            }
                            else
                            {
                                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                                if(PM_ERR_FLAG__SUCCESS != err_flag ||
                                   o_buf_config != gpio_status.gpio_out_buffer_config || vltg_src != gpio_status.gpio_volt_source ||
                                   out_buf_drive != gpio_status.gpio_out_buffer_drv_strength || src_config != gpio_status.gpio_src_config ||
                                   out_inversion != gpio_status.gpio_invert_ext_pin_config)
                                {
                                    pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_OUTPUT, i, err_flag);
                                }
                            }

                            memset(&gpio_status, 0, sizeof(gpio_status));
                        }
                    }
                }
            }
        }
    }

/** GPIO DIGITAL IN-OUT CONFIG SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(src_config = PM_GPIO_SOURCE_DTEST1; src_config < PM_GPIO_SOURCE_CONFIG_TYPE__INVALID; src_config++)
        {
            for(i_src_pulls = PM_GPIO_I_SOURCE_PULL_UP_30uA; i_src_pulls < PM_GPIO_CURRENT_SOURCE_PULLS_TYPE__INVALID; i_src_pulls++)
            {
                for(vltg_src = PM_GPIO_VIN0; vltg_src < PM_SUPPORTED_GPIO_VIN; vltg_src++)
                {
                    for(o_buf_config = PM_GPIO_OUT_BUFFER_CONFIG_CMOS; o_buf_config < PM_GPIO_OUT_BUFFER_CONFIG__INVALID; o_buf_config++)
                    {
                        for(out_buf_drive = PM_GPIO_OUT_BUFFER_LOW; out_buf_drive < PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID; out_buf_drive++)
                        {
                            err_flag = pm_gpio_config_digital_input_output (pmic_index, (pm_gpio_perph_index)i, src_config, i_src_pulls, vltg_src, o_buf_config, out_buf_drive);
                            if(PM_ERR_FLAG__SUCCESS != err_flag)
                            {
                                pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, i, err_flag);
                            }
                            else
                            {
                                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                                if(PM_ERR_FLAG__SUCCESS != err_flag || src_config != gpio_status.gpio_src_config ||
                                   i_src_pulls != gpio_status.gpio_current_src_pulls || o_buf_config != gpio_status.gpio_out_buffer_config ||
                                   vltg_src != gpio_status.gpio_volt_source || out_buf_drive != gpio_status.gpio_out_buffer_drv_strength)
                                {
                                    pm_test_handle_error(pmic_index, PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT, i, err_flag);
                                }
                            }

                            memset(&gpio_status, 0, sizeof(gpio_status));
                        }
                    }
                }
            }
        }
    }

/** GPIO INVERT CONFIG SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(out_inversion = 0; out_inversion < 2; out_inversion++)
        {
            err_flag = pm_gpio_set_inversion_configuration (pmic_index, (pm_gpio_perph_index)i, out_inversion);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_GPIO_SET_INVERSION_CONFIGURATION, i, err_flag);
            }
            else
            {
                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                if(PM_ERR_FLAG__SUCCESS != err_flag || out_inversion != gpio_status.gpio_invert_ext_pin_config)
                {
                    pm_test_handle_error(pmic_index, PM_GPIO_SET_INVERSION_CONFIGURATION, i, err_flag);
                }
            }

            memset(&gpio_status, 0, sizeof(gpio_status));
        }
    }

/** GPIO OUTPUT BUFFER CONFIG SET-GET APIs */
    for(i = 0; i < g_pm_test_gpio_num; i++)
    {
        for(o_buf_config = PM_GPIO_OUT_BUFFER_CONFIG_CMOS; o_buf_config < PM_GPIO_OUT_BUFFER_CONFIG__INVALID; o_buf_config++)
        {
            err_flag = pm_gpio_set_output_buffer_configuration (pmic_index, (pm_gpio_perph_index)i, o_buf_config);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, i, err_flag);
            }
            else
            {
                err_flag = pm_gpio_status_get (pmic_index, (pm_gpio_perph_index)i, &gpio_status);
                if(PM_ERR_FLAG__SUCCESS != err_flag || o_buf_config != gpio_status.gpio_out_buffer_config)
                {
                    pm_test_handle_error(pmic_index, PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION, i, err_flag);
                }
            }

            memset(&gpio_status, 0, sizeof(gpio_status));
        }
    }

    return err_flag;
}

