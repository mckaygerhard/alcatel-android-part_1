#ifndef PM_TEST_FRAMEWORK_H
#define PM_TEST_FRAMEWORK_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                PM TEST FRAMEWORK

GENERAL DESCRIPTION
  PMIC Framework Test header file.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/23/15   mr      Added Unit Tests for TypeC module. (CR-954241)
12/14/15   sv      Added vib driver test support.
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include <stdio.h>
#include "string.h"

#include "boot_logger.h"

#include "pm_err_flags.h"
#include "pm_target_information.h"
#include "pm_lib.h"
#include "pm_utils.h"


/*===========================================================================

                 LOCAL MACRO AND STRUCTURE DEFINITIONS

===========================================================================*/
typedef enum
{
    PM_TEST_LVL_NONE,

    /* Driver Test Level */
    PM_TEST_DRIVER_LVL_0,
    PM_TEST_DRIVER_LVL_1,
    PM_TEST_DRIVER_LVL_2,

    /* NPA Framework Test Level */
    PM_TEST_NPA_LVL_0,
    PM_TEST_NPA_LVL_1,
} pm_test_level;

typedef enum
{
    PM_API_NONE,

    /* LDO */
    PM_LDO_SW_ENABLE,
    PM_LDO_SW_ENABLE_STATUS,
    PM_LDO_SW_MODE,
    PM_LDO_SW_MODE_STATUS,
    PM_LDO_VOLT_LEVEL,
    PM_LDO_VOLT_LEVEL_STATUS,
    PM_LDO_PULL_DOWN,
    PM_LDO_PIN_CTRLED,
    PM_LDO_VREG_OK_STATUS,

    /* SMPS */
    PM_SMPS_SW_ENABLE,
    PM_SMPS_SW_ENABLE_STATUS,
    PM_SMPS_SW_MODE,
    PM_SMPS_SW_MODE_STATUS,
    PM_SMPS_VOLT_LEVEL,
    PM_SMPS_VOLT_LEVEL_STATUS,
    PM_SMPS_QUIET_MODE,
    PM_SMPS_SWITCHING_FREQ,
    PM_SMPS_SWITCHING_FREQ_STATUS,
    PM_SMPS_INDUCTOR_ILIM,
    PM_SMPS_INDUCTOR_ILIM_STATUS,
    PM_SMPS_SET_PHASE,
    PM_SMPS_GET_PHASE,
    PM_SMPS_PULL_DOWN,
    PM_SMPS_PIN_CTRLED,
    PM_SMPS_VREG_OK_STATUS,

    /* CLK */
    PM_CLK_DRV_STRENGTH,
    PM_CLK_SW_ENABLE,
    PM_CLK_PIN_CTRLED,
    PM_CLK_PULL_DOWN,

    /* GPIO */
    PM_GPIO_STATUS_GET,
    PM_GPIO_CONFIG_BIAS_VOLTAGE,
    PM_GPIO_CONFIG_DIGITAL_INPUT,
    PM_GPIO_CONFIG_DIGITAL_OUTPUT,
    PM_GPIO_CONFIG_DIGITAL_INPUT_OUTPUT,
    PM_GPIO_SET_VOLT_SOURCE,
    PM_GPIO_SET_OUTPUT_BUFFER_CONFIGURATION,
    PM_GPIO_SET_INVERSION_CONFIGURATION,

    /* MPP */
    PM_MPP_STATUS_GET,
    PM_MPP_CONFIG_DIGITAL_INPUT,
    PM_MPP_CONFIG_DIGITAL_OUTPUT,
    PM_MPP_CONFIG_DIGITAL_INOUT,
    PM_MPP_CONFIG_ANALOG_INPUT,
    PM_MPP_CONFIG_ANALOG_OUTPUT,
    PM_MPP_CONFIG_I_SINK,
    PM_MPP_CONFIG_ATEST,
    PM_MPP_CONFIG_DTEST_OUTPUT,
    PM_MPP_SET_LIST_MPP_WITH_SHUNT_CAP,
    PM_MPP_GET_MPP_WITH_SHUNT_CAP_LIST_STATUS_FOR_DEVICE,
    PM_MPP_ENABLE,

    /* PON */
    PM_PON_OVERTEMP_RESET_CFG,
    PM_PON_PS_HOLD_CFG,
    PM_PON_RESET_SOURCE_CFG,
    PM_PON_RESET_SOURCE_CTL,
    PM_PON_STAGE3_RESET_SOURCE_CFG,
    PM_PON_GET_POFF_REASON,
    PM_PON_GET_WARM_RESET_REASON,
    PM_PON_GET_SOFT_RESET_REASON,
    PM_PON_GET_ALL_PON_REASONS,
    PM_PON_IRQ_ENABLE,
    PM_PON_IRQ_CLEAR,
    PM_PON_IRQ_SET_TRIGGER,
    PM_PON_IRQ_STATUS,
    PM_PON_CONFIG_UVLO,
    PM_PON_WARM_RESET_STATUS,
    PM_PON_WARM_RESET_STATUS_CLEAR,

    /* LPG */
    PM_LPG_PWM_ENABLE,
    PM_LPG_PWM_OUTPUT_ENABLE,
    PM_LPG_PWM_SET_PWM_VALUE,
    PM_LPG_PWM_SET_PRE_DIVIDE,
    PM_LPG_PWM_CLOCK_SEL,
    PM_LPG_SET_PWM_BIT_SIZE,
    PM_LPG_PWM_SRC_SELECT,
    PM_LPG_CONFIG_PWM_TYPE,
    PM_LPG_PATTERN_CONFIG,
    PM_LPG_LUT_CONFIG_SET,
    PM_LPG_LUT_CONFIG_GET,
    PM_LPG_LUT_CONFIG_SET_ARRAY,
    PM_LPG_LUT_CONFIG_GET_ARRAY,
    PM_LPG_PWM_RAMP_GENERATOR_START,
    PM_LPG_PWM_LUT_INDEX_SET,
    PM_LPG_CONFIG_PAUSE_TIME,
    PM_LPG_GET_STATUS,

    /* RGB */
    PM_RGB_SET_VOLTAGE_SOURCE,
    PM_RGB_ENABLE,
    PM_RGB_GET_STATUS,

    /* RGB */
    PM_RTC_GET_TIME,

    /* COINCELL */
    PM_COINCELL_ENABLE,
    PM_COINCELL_GET_STATUS,
    PM_COINCELL_SET_CC_CURRENT_LIMIT_RESISTOR,
    PM_COINCELL_SET_CC_CHARGING_VOLTAGE,

    /* VIB */
    PM_VIB_SET_VOLT,
    PM_VIB_ENABLE,

    /* TYPEC */
    PM_TYPEC_ENABLE,
    PM_TYPEC_IS_VBUS_VALID,
    PM_TYPEC_GET_PLUG_ORIENTATION_STATUS,
    PM_TYPEC_GET_UFP_TYPE,
    PM_TYPEC_GET_DFP_CURR_ADVERTISE,
    PM_TYPEC_DISABLE_TYPEC_COMMAND_SW,
    PM_TYPEC_SET_PORT_ROLE,
    PM_TYPEC_VCONN_ENABLE,
    PM_TYPEC_READ_PERIPH_REG
} pm_test_apis;

#define PM_TEST_ARRAY_SIZE  24

typedef struct
{
    uint8 pm_index;
    pm_test_level lvl : 8;
    pm_test_apis api  : 8;
    uint32 rcrs_id    : 8;
    pm_err_flag_type err_code   : 8;
} __attribute__((packed)) pm_test_driver_state;


/*===========================================================================

                EXPORTED GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
extern pm_test_driver_state g_pm_test_status[PM_TEST_ARRAY_SIZE];
extern uint8 g_f_cnt;

extern uint8  g_pmic_index;
extern uint32 g_pm_test_gpio_num;
extern uint32 g_pm_test_ldo_num;
extern uint32 g_pm_test_lpg_num;
extern uint32 g_pm_test_mpp_num;
extern uint32 g_pm_test_rgb_num;
extern uint32 g_pm_test_smps_num;
extern uint32 g_pm_test_typec_num;

/* For TEST/DEBUG purpose  -  Defined in LDO Driver Test file */
extern pm_sw_mode_type g_sw_mode;
extern pm_on_off_type g_sw_enable;
extern pm_volt_level_type g_volt_lvl, g_prev_volt_lvl;
extern boolean g_vreg_ok;


/*===========================================================================

                LOCAL FUNCTION PROTOTYPES

===========================================================================*/
void pm_test_handle_error(uint8 pmic_index, pm_test_apis api, uint8 rcrs, pm_err_flag_type error);

/** PMIC Driver Level Test Functions */
pm_err_flag_type pm_test_driver_ldo_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_smps_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_clk_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_gpio_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_mpp_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_pon_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_lpg_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_rgb_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_rtc_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_coincell_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_vib_level_0 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_typec_level_0 (uint8 pmic_index);

pm_err_flag_type pm_test_driver_ldo_level_1 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_smps_level_1 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_gpio_level_1 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_mpp_level_1 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_pon_level_1 (uint8 pmic_index);
pm_err_flag_type pm_test_driver_typec_level_1 (uint8 pmic_index);

// void pm_test_unit_testcases (void);

#endif    /* PM_TEST_FRAMEWORK_H */

