/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM SMPS DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
boolean g_vib_status = 0;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_test_driver_vib_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_vib_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    /* config vibrater driver voltage to 2200 mV. vibration dirver voltage range 1200mv~3100mv*/
	uint16  pm_vib_drv_volt = 2200;
	pm_vib_mode_type vib_mode =PM_VIB_MODE__MANUAL;
	err_flag = pm_vib_set_volt(pmic_index,PM_VIB__1,pm_vib_drv_volt);
	if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_VIB_SET_VOLT, 1, err_flag);
    }  
	
	for(vib_mode = PM_VIB_MODE__MANUAL; vib_mode <PM_VIB_MODE__INVALID; vib_mode ++)
	{
		err_flag = pm_vib_enable(pmic_index,PM_VIB__1, PM_VIB_MODE__MANUAL, TRUE);
		if(PM_ERR_FLAG__SUCCESS != err_flag)
		{
			pm_test_handle_error(pmic_index, PM_VIB_ENABLE, (uint8)vib_mode, err_flag);
		}
	}
    return err_flag;
}

#if 0
/**
 * @name pm_test_driver_vib_level_1
 *
 * @brief This function tests for SET-GET APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_vib_level_1 (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}
#endif
