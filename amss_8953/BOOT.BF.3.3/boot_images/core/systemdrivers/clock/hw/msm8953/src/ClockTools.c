/*
===========================================================================
  @file ClockSBLSDCC.c

  This file provides clock initialization for starting SDCC clocks at boot.
===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/clock/hw/msm8953/src/ClockTools.c#5 $
  $DateTime: 2015/12/29 22:33:42 $
  $Author: pwbldsvc $

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"

/*=========================================================================
      Data
==========================================================================*/
struct Clock_SDCCRegAddrType
{
  uint32 nCmd;
  uint32 nApps;
  uint32 nAHB;
};

const struct Clock_SDCCRegAddrType Clock_SDCCRegs[CLK_SDC_NUM_CLKS] = 
{ 
  /* CLK_SDC_NONE: */
  {0,0,0},
    
  { /* CLK_SDC1: */
    HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR),
    HWIO_ADDR(GCC_SDCC1_APPS_CBCR),
    HWIO_ADDR(GCC_SDCC1_AHB_CBCR)
  },
  { /* CLK_SDC2: */
    HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR),
    HWIO_ADDR(GCC_SDCC2_APPS_CBCR),
    HWIO_ADDR(GCC_SDCC2_AHB_CBCR)
  }
};

/* ============================================================================
**  Function : Clock_SetSDCClockFrequencyExt
** ============================================================================
*/
/*!
    Configure SDC clock to a specific perf level.

    @param eClockPerfLevel   -  [IN] Clock frequency level
           eClock            -  [IN] SDC clock to configure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
uint32 Clock_SetSDCClockFrequencyExt( uint32 nFreqKHz, ClockSDCType eClock)
{
  ClockConfigMuxType SDCCCfg = {0};
  const ClockSDCCfgType *pCfg = NULL;
  uint32 n;
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  /* Check for a valid SDC */
  if(eClock >= CLK_SDC_NUM_CLKS) return 0;
  if (Clock_SDCCRegs[eClock].nCmd == 0) return 0;

  /* Copy the config to the local so the nCMDCGRAddr can be modified. */
  pCfg = (eClock == CLK_SDC1)? (cfg->SDC1_Ext_Cfg) : (cfg->SDC2_Ext_Cfg);

  /* Find the nearest frequency that matches the request */
  for(n = 0; pCfg[n].nFrequency != 0; n++)
  {
    if(pCfg[n].nFrequency >= nFreqKHz) break;
  }
  if(pCfg[n].nFrequency == 0) return 0;

  /* A configuration is found.  Set it */
  SDCCCfg = pCfg[n].Cfg; /* copy struct and fix it */
  SDCCCfg.nCMDCGRAddr = Clock_SDCCRegs[eClock].nCmd;

  /*
   * Enable access to the PCNOC.
   */
  Clock_ToggleClock(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);   

  if( ! Clock_EnableSource( SDCCCfg.eSource )) return 0;
  if( ! Clock_ConfigMux(&SDCCCfg)) return 0;
  if( ! Clock_ToggleClock(Clock_SDCCRegs[eClock].nApps, CLK_TOGGLE_ENABLE)) return 0;
  if( ! Clock_ToggleClock(Clock_SDCCRegs[eClock].nAHB, CLK_TOGGLE_ENABLE)) return 0;

  return pCfg[n].nFrequency;
  }

/* ============================================================================
**  Function : Clock_SetSDCClockFrequency
** ============================================================================
*/
/*!
    Configure SDC clock to a specific perf level.

    @param eClockPerfLevel   -  [IN] SDC Clock perf level
           eClock            -  [IN] SDC clock to configure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetSDCClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockSDCType eClock
)
{
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  uint32 nFreq = 0;

  if(ePerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;
 
  nFreq = Clock_SetSDCClockFrequencyExt( cfg->SDC_Cfg[ePerfLevel], eClock);
  if( nFreq == 0) return FALSE;
  return TRUE;

} /* END Clock_SetSDCClockFrequency */


/* ============================================================================
**  Function : Clock_InitUSB
** ============================================================================
*/
/*!
    Configure USB clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_InitUSB(void)
{
/* USB configuration USB_Cfg : 133.33MHz */
  ClockConfigMuxType USB_Cfg = {
    HWIO_ADDR(GCC_USB30_MASTER_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    12, /* nDiv2x */
    0,0,0 /* M/N:D */
  };

  /* USB configuration USB_MockCfg : 60 MHz */
  ClockConfigMuxType USB_MockCfg =
  {
    HWIO_ADDR(GCC_USB30_MOCK_UTMI_CMD_RCGR),
    MUX_USB, SRC_GPLL6_DIV2,  /* eSource */
    18, /* nDiv2x */
    0,0,0 /* M/N:D */
  };

  /* USB configuration USB_AuxCfg : 19.2 MHz */
  ClockConfigMuxType USB_AuxCfg =
  {
    HWIO_ADDR(GCC_USB3_AUX_CMD_RCGR),
    MUX_GCC, SRC_CXO,  /* eSource */
    2, /* nDiv2x */
    0,0,0 /* M/N:D */
  };

  if( ! Clock_ConfigMux(&USB_Cfg)) return FALSE;
  if( HWIO_INF(GCC_PCNOC_USB3_AXI_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_PCNOC_USB3_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB30_MASTER_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_MASTER_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB30_SLEEP_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }

  if(! Clock_EnableSource(USB_MockCfg.eSource )) return FALSE;

  if( ! Clock_ConfigMux(&USB_MockCfg)) return FALSE;
  if( HWIO_INF(GCC_USB30_MOCK_UTMI_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }

  if( HWIO_INF(GCC_USB_PHY_CFG_AHB_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }

  if( ! Clock_ConfigMux(&USB_AuxCfg)) return FALSE;
  if( HWIO_INF(GCC_USB3_AUX_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB3_AUX_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }

  /*
   * On 8953, do not wait for the PIPE clock to turn ON. This clock is sourced
   * from the USB3 PHY clock, which is not turned ON for HS modes.
   */
  if( HWIO_INF(GCC_USB3_PIPE_CBCR, CLK_OFF))
  {
    HWIO_OUTF(GCC_USB3_PIPE_CBCR, CLK_ENABLE, 1);
  } 
  
  return TRUE;

} /* END Clock_InitUSB */

/* ============================================================================
**  Function : Clock_DisableUSB
** ============================================================================
*/
/*!
    Disable USB clocks.

    @param None.
    @return
    TRUE -- Disable was successful.
    FALSE -- Disable failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_DisableUSB(void)
{
	
 if( ! Clock_ToggleClock(HWIO_GCC_USB3_PIPE_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;  
  if( ! Clock_ToggleClock(HWIO_GCC_USB3_AUX_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;  
  if( ! Clock_ToggleClock(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_MASTER_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_PCNOC_USB3_AXI_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;   

 return TRUE;
  
} /* END Clock_DisableUSB */


/* ============================================================================
**  Function : Clock_Usb30SwitchPipeClk
** ============================================================================
   */
/*!
    Switch to USB3_PIPE_CLK.

    @param None.
    @return
    TRUE -- Switch was successful.
    FALSE -- Switch failed.
   
    @dependencies
    None.

    @sa None
*/
boolean Clock_Usb30SwitchPipeClk(void)
{
  /* On 9x55, 8953 the PCIE PIPE clock is sourced from the USB PHY always */
      	return TRUE;
  }


/* ============================================================================
**  Function : Clock_Usb30EnableSWCollapse
** ============================================================================
*/
/*!
    Enable SW Collapse for USB30

    @param Enable/Disable.
    @return
    TRUE always

    @dependencies
    None.

    @sa None
   */
boolean Clock_Usb30EnableSWCollapse(boolean enable)
{
  /* Enable/Disable SW PowerCollapse for USB30 */
    HWIO_OUTF(GCC_USB30_GDSCR, SW_COLLAPSE, enable);
  return TRUE;
}
  
/* ============================================================================
**  Function : Clock_Usb30GetSWCollapse
** ============================================================================
*/
/*!
    Returns the status of SW Collapse for USB30

    @param None
    @return
    TRUE if enabled
    FALSE if disabled

    @dependencies
    None.

    @sa None
*/
boolean Clock_Usb30GetSWCollapse(void)
{
  uint8 sw_collapse = HWIO_INF(GCC_USB30_GDSCR, SW_COLLAPSE);
  return (sw_collapse ? TRUE : FALSE);
	
}
