/**
 * @file:  SpmiBare.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/platform/config/bear/default/SpmiBare.h#1 $
 * $Change: 8504943 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */
#ifndef SPMIBARE_H
#define	SPMIBARE_H

#define SPMI_BARE_OWNER_NUMBER     0
#define SPMI_BARE_PMIC_ARB_ADDRESS 0x02000000

#endif

