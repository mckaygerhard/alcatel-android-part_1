/**
 * @file: SpmiOsLogs.h
 * 
 * @brief: This module implements logging functionality for the SPMI driver
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/platform/os/bare/SpmiOsLogs.h#1 $
 * $Change: 8504943 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#ifndef __SPMI_OS_LOGS_H_
#define __SPMI_OS_LOGS_H_


#define SPMI_OS_LOG_FATAL(msg, args...) 
    
#define SPMI_OS_LOG_ERROR(msg, args...) 
    
#define SPMI_OS_LOG_WARNING(msg, args...) 
    
#define SPMI_OS_LOG_INFO(msg, args...) 
    
#define SPMI_OS_LOG_VERBOSE(msg, args...) 

#define SPMI_OS_LOG_TRACE(msg, args...)
    
#define SPMI_OS_LOG_TRACE_RAW(msg, args...)

#endif
