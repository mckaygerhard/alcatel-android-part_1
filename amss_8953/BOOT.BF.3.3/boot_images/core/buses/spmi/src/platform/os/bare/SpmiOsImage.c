/**
 * @file:  SpmiOsEmmc.c
 * 
 * Copyright (c) 2013-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/09/29 22:03:08 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/platform/os/bare/SpmiOsImage.c#2 $
 * $Change: 9128872 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 07/09/15  Multiple bus support
 * 11/3/14  Automatic channel assignment
 * 10/1/13  Initial Version
 */

#include "SpmiOs.h"
#include "SpmiBare.h"
#include "busywait.h"
#include "SpmiSwio.h"

//******************************************************************************
static uint16 dbgChannels[] = {SWIO_MAX_CHANNELS_SUPPORTED - 1};
// Public API Functions
//******************************************************************************

SpmiOs_ReservedChannels* SpmiOs_GetReservedChannels(uint8 busId)
{
   static SpmiOs_ReservedChannels debugChannels = {
        .numReserved = sizeof(dbgChannels) / sizeof(dbgChannels[0]),
        .reserved = dbgChannels
    };
	
	return &debugChannels;
}

void SpmiOs_Wait(uint32 us)
{
    busywait( us );
}

boolean SpmiOs_HandleTransactionError(Spmi_Result* rslt, PmicArbCmd cmd, uint8 busId,
                                      uint8 slaveId, uint16 address, uint8 tries)
{
    return FALSE;
}

Spmi_Result SpmiOs_GetOwnerId(uint8* owner)
{
    *owner = SPMI_BARE_OWNER_NUMBER;
    return SPMI_SUCCESS;
}

Spmi_Result SpmiOs_GetPmicArbBaseAddr(uint8 busId, void** addr)
{
    *addr = (void*) SPMI_BARE_PMIC_ARB_ADDRESS;
    return SPMI_SUCCESS;
}



Spmi_Result SpmiOs_Malloc(uint32 size, void** buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

Spmi_Result SpmiOs_Free(void* buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

Spmi_Result SpmiOs_RegisterISR(SpmiOs_IsrPtr isr, uint8 busId)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}



SpmiOs_ClkStatus SpmiOs_GetSerialClkState(uint8 busId)
{
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}

SpmiOs_ClkStatus SpmiOs_GetAhbClkState(uint8 busId)
{
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}
uint64 SpmiOs_GetTimeTick(void)
{
    return 0;
}
