/**
 * @file:  SpmiCfg.h
 * @brief: This module provides configuration options for the SPMI controller
 * 
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/core/SpmiCfgInternal.h#1 $
 * $Change: 8504943 $ 
 * 
 *                              Edit History
 * Date      Description
 * --------  -------------------------------------------------------------------
 * 5/26/15  Multiple bus support
 * 11/14/14  Initial Version
 */
#ifndef SPMICFGINTERNAL_H
#define	SPMICFGINTERNAL_H

#include "SpmiBusCfg.h"

//******************************************************************************
// Public API Functions
//******************************************************************************

boolean SpmiBusCfg_InDynamicChannelMode(uint8 busId);
Spmi_Result SpmiBusCfg_ConfigureChannel(uint8 busId, SpmiBusCfg_ChannelCfg* chanCfg, boolean autoAssignChan);

#endif
