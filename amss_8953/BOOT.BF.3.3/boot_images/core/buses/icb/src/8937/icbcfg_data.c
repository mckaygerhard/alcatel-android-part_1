/*==============================================================================

FILE:      icbcfg_data.c

DESCRIPTION: This file implements the ICB Configuration driver.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

$Header: //components/rel/boot.bf/3.3/boot_images/core/buses/icb/src/8937/icbcfg_data.c#6 $ 
$DateTime: 2016/03/18 00:07:23 $
$Author: pwbldsvc $
$Change: 10096522 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2016/03/14  pra    Adding the support for 8940.
2015/11/27  ddk    Updated BIMC calculator settings.
2015/05/27  sds    Update BKE context allocation and recommended settings
2015/05/27  rc     To map the SNOC to the 512MB region 
2014/11/07  rc     Branched for 8952
2014/09/12  sds    Update danger context allocation
2014/06/19  sds    Update to v0.9 8936 spreadsheet
2014/05/20  sds    Change the hardware base address to a uint8_t*.
2014/05/20  sds    Allow 3GB memory maps
2014/01/27  sds    Branched for 8936
2013/12/11  sds    First pass at DDR settings spreadsheet
2013/10/09  sds    Branched for 8916
2013/02/26  sds    Changed how available DDR regions are handled.
2013/02/04  jc     Updated masters and slaves to match TO
2012/12/06  jc     8x10 implementation
2012/03/26  av     Created
 
        Copyright (c) 2012-2016 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "icbcfg.h"
#include "icbcfgi.h"
#include "icbcfg_hwio.h"
#include "HALbimc.h"
#include "HALbimcHwioGeneric.h"

/*---------------------------------------------------------------------------*/
/*          Macro and constant definitions                                   */
/*---------------------------------------------------------------------------*/
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))

/* BIMC register value macros */
#define SLAVE_SEGMENT(slave,index,addr_base,addr_mask)  \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_BASEm_LOWER_ADDR((uint8_t *)BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_BASEm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_BASEm_LOWER_RMSK, \
   (addr_base) }, \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_MASKm_LOWER_ADDR((uint8_t *)BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   (addr_mask) }
   
#define SLAVE_SEGMENT_MASK(slave,index,addr_mask)  \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_MASKm_LOWER_ADDR(BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   (addr_mask) }

#define BIMC_DEFAULT(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_DEFAULT_SEGMENT_ADDR(BIMC_BASE), \
   BIMC_DEFAULT_SEGMENT_RMSK, \
   BIMC_DEFAULT_SEGMENT_RMSK, \
   (value) }

#define ARB_MODE(slave,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_S_ARB_MODE_ADDR((uint8_t *)BIMC_BASE,slave), \
   BIMC_S_ARB_MODE_RMSK, \
   BIMC_S_ARB_MODE_RMSK, \
   (mode) }

#define SWAY_GATHERING(slave,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_S_SWAY_GATHERING_ADDR(BIMC_BASE,slave), \
   BIMC_S_SWAY_GATHERING_RMSK, \
   BIMC_S_SWAY_GATHERING_RMSK, \
   (mode) }

/* Slave indexes */
#define SLAVE_DDR_CH0 0
#define SLAVE_SNOC    1

#define SYS_SWAY 0
#define DEFAULT_SWAY 1

#define MPORT_MODE(master,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_MODE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_MODE_RMSK, \
   BIMC_M_MODE_RMSK, \
   (mode) }

#define MPORT_PIPE0_GATHERING(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_PIPE0_GATHERING_ADDR(BIMC_BASE,master), \
   BIMC_M_PIPE0_GATHERING_RMSK, \
   BIMC_M_PIPE0_GATHERING_RMSK, \
   (value) }

#define MPORT_RD_TRACKING_INDEX(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_RD_TRACKING_INDEX_ADDR(BIMC_BASE,master), \
   BIMC_M_RD_TRACKING_INDEX_RMSK, \
   BIMC_M_RD_TRACKING_INDEX_RMSK, \
   (value) }

#define MPORT_WR_TRACKING_INDEX(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_WR_TRACKING_INDEX_ADDR(BIMC_BASE,master), \
   BIMC_M_WR_TRACKING_INDEX_RMSK, \
   BIMC_M_WR_TRACKING_INDEX_RMSK, \
   (value) }

#define MPORT_BKE_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE_ENABLE_RMSK, \
   BIMC_M_BKE_ENABLE_RMSK, \
   (value) }

#define MPORT_PL_OVERRIDE(master, value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_PRIORITYLVL_OVERRIDE_ADDR(BIMC_BASE,master), \
   BIMC_M_PRIORITYLVL_OVERRIDE_RMSK, \
   BIMC_M_PRIORITYLVL_OVERRIDE_RMSK, \
   (value) }

#define MPORT_BKE1_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE1_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE1_ENABLE_RMSK, \
   BIMC_M_BKE1_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE2_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE2_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE2_ENABLE_RMSK, \
   BIMC_M_BKE2_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_ENABLE_RMSK, \
   BIMC_M_BKE3_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_PERIOD(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_PERIOD_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_COUNT(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_COUNT_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_MEDIUM(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_MEDIUM_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_LOW(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_LOW_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   (value) }

#define MPORT_BKE3_HEALTH_0(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_HEALTH_0_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   (value) }

/* Master indexes */
#define MASTER_APP   0
#define MASTER_DSP   1
#define MASTER_GPU   2
#define MASTER_MMSS0 3
#define MASTER_MMSS1 4
#define MASTER_SYS   5
#define MASTER_TCU   6

#define QOS_CTRL(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_CTRL_ADDR(BIMC_BASE), \
   BIMC_QOS_CTRL_RMSK, \
   BIMC_QOS_CTRL_RMSK, \
   (value) }

#define QOS_CFG(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_CFG_ADDR(BIMC_BASE), \
   BIMC_QOS_CFG_RMSK, \
   BIMC_QOS_CFG_RMSK, \
   (value) }

#define QOS_TIMEOUT_CNT_LOW_URGENCY(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_ADDR(BIMC_BASE), \
   BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_RMSK, \
   BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_RMSK, \
   (value) }

#define QOS_TIMEOUT_CNT_HIGH_URGENCY(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_ADDR(BIMC_BASE), \
   BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_RMSK, \
   BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_RMSK, \
   (value) }

#define QOS_FSSH_URGENCY_SEL(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_FSSH_URGENCY_SEL_ADDR(BIMC_BASE), \
   BIMC_QOS_FSSH_URGENCY_SEL_RMSK, \
   BIMC_QOS_FSSH_URGENCY_SEL_RMSK, \
   (value) }

#define SYS_SWAY_READ_COMMAND_OVERRIDE  0x00450250
#define SYS_SWAY_WRITE_COMMAND_OVERRIDE 0x00450260

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/*---------------------------------------------------------------------------*/
/*          Properties data for device ID  = "icbcfg/boot"                   */
/*---------------------------------------------------------------------------*/

/* ICBcfg Boot Configuration Data*/

icbcfg_data_type icbcfg_boot_data[] = 
{
    /* Add configuration data using
      ICBCFG_HWIO_*() or
      ICBCFG_RAW_*() macros below
      .
      .                          
      .                          */
    MPORT_PL_OVERRIDE(MASTER_APP, 0x001),
    MPORT_PL_OVERRIDE(MASTER_DSP, 0x301),
    MPORT_PL_OVERRIDE(MASTER_GPU, 0x001),
    MPORT_PL_OVERRIDE(MASTER_MMSS0, 0x201),
    MPORT_PL_OVERRIDE(MASTER_TCU, 0x201),

    BIMC_DEFAULT(0x80030003),	
    SWAY_GATHERING(SYS_SWAY,0x3),

    ICBCFG_RAW_DW(SYS_SWAY_READ_COMMAND_OVERRIDE, 0x00010040),
    ICBCFG_RAW_DW(SYS_SWAY_WRITE_COMMAND_OVERRIDE, 0x00010040),

    /* Only map the bottom 256 MB to SNOC */
    SLAVE_SEGMENT_MASK(SLAVE_SNOC, 0, 0xF0000000),

    ARB_MODE(SLAVE_DDR_CH0, 0x00000001),

    MPORT_MODE(MASTER_APP,   0x10),
    MPORT_MODE(MASTER_DSP,   0x12),
    MPORT_MODE(MASTER_GPU,   0x12),
    MPORT_MODE(MASTER_MMSS0, 0x12),
    MPORT_MODE(MASTER_MMSS1, 0x12),
    MPORT_MODE(MASTER_SYS,   0x20000012),
    MPORT_MODE(MASTER_TCU,   0x2012),


    MPORT_BKE3_GRANT_PERIOD(MASTER_APP, 0x14),
    MPORT_BKE3_GRANT_COUNT(MASTER_APP, 0x64),
    MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_APP, 0xFFCE),
    MPORT_BKE3_THRESHOLD_LOW(MASTER_APP, 0xFF9C),
    MPORT_BKE3_HEALTH_0(MASTER_APP, 0x80000000),

    MPORT_BKE_ENABLE(MASTER_APP,  0x01000000),
    MPORT_BKE1_ENABLE(MASTER_APP, 0x02000000),
    MPORT_BKE2_ENABLE(MASTER_APP, 0x04000000),
    MPORT_BKE3_ENABLE(MASTER_APP, 0x38000001),

    MPORT_BKE3_GRANT_PERIOD(MASTER_GPU, 0x14),
    MPORT_BKE3_GRANT_COUNT(MASTER_GPU, 0x64),
    MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_GPU, 0xFFCE),
    MPORT_BKE3_THRESHOLD_LOW(MASTER_GPU, 0xFF9C),
    MPORT_BKE3_HEALTH_0(MASTER_GPU, 0x80000000),

    MPORT_BKE_ENABLE(MASTER_GPU,  0x01000000),
    MPORT_BKE1_ENABLE(MASTER_GPU, 0x02000000),
    MPORT_BKE2_ENABLE(MASTER_GPU, 0x04000000),
    MPORT_BKE3_ENABLE(MASTER_GPU, 0x38000001),

    QOS_CFG(0x0000606),
    QOS_TIMEOUT_CNT_LOW_URGENCY(0x00600060),
    QOS_TIMEOUT_CNT_HIGH_URGENCY(0x00270027),
    QOS_FSSH_URGENCY_SEL(0x00000010),
    QOS_CTRL(0x00000001),
};

icbcfg_prop_type icbcfg_boot_prop = 
{
    /* Length of the config  data array */
    ARRAY_SIZE(icbcfg_boot_data),
    /* Pointer to config data array */ 
    icbcfg_boot_data                                    
};

/* DDR map information. */
uint32 map_ddr_region_count = 1; 
icbcfg_mem_region_type map_ddr_regions[1] =
{
  { 0x10000000ULL, 0x100000000ULL },
};

uint32 channel_map[1] = { SLAVE_DDR_CH0 };

HAL_bimc_InfoType bimc_hal_info =
{
  (uint8_t *)BIMC_BASE, /* Base address */
  19200,     /* QoS frequency */
  {
    0,
    0,
    0,
    0,
    0,
    0,
    3, /**< Number of segments for address decode. */
  }
};

/* Make sure the config region is always prohibited when "resetting" */
HAL_bimc_SlaveSegmentType safe_reset_seg =
{
  true,
  0x00000000ULL,                 /* start of config region */
  0x10000000ULL,                 /* 256 MB */
  BIMC_SEGMENT_TYPE_SUBTRACTIVE,
  BIMC_INTERLEAVE_NONE,
};

/* MSM8937 - ALL */
icbcfg_device_config_type msm8937_all =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8937,  /**< Chip family */
  false,                       /**< Exact match for version? */
  0,                           /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop,           /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  3,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  NULL,                        /**< L2 TCM unmap configuration */
};

/* MSM8940 - ALL */
icbcfg_device_config_type msm8940_all =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8940,  /**< Chip family */
  false,                       /**< Exact match for version? */
  0,                           /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop,           /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  3,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  NULL,                        /**< L2 TCM unmap configuration */
};

/* Definitions list */
icbcfg_device_config_type *configs[] =
{
  &msm8937_all,
  &msm8940_all,
};

/* Exported target definitions */
icbcfg_info_type icbcfg_info =
{
  ARRAY_SIZE(configs),
  configs,
};
