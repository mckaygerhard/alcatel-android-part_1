#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/buses/i2c/config/i2c_boot_8953.xml"





















#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/ClockBoot.h"










 













 



 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/services/com_dtypes.h"

















 
















 






 








 
#line 58 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/services/com_dtypes.h"



 





















 



 


typedef  unsigned char      boolean;      




#line 107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/services/com_dtypes.h"


typedef  unsigned int      uint32;       




typedef  unsigned short     uint16;       




typedef  unsigned char      uint8;        




typedef  signed int         int32;        




typedef  signed short       int16;        




typedef  signed char        int8;         




typedef  unsigned long      uintnt;      

 





 

typedef  unsigned char      byte;          



typedef  unsigned short     word;          
typedef  unsigned int       dword;         

typedef  unsigned char      uint1;         
typedef  unsigned short     uint2;         
typedef  unsigned int       uint4;         

typedef  signed char        int1;          
typedef  signed short       int2;          
typedef  signed int         int4;          

typedef  signed int         sint31;        
typedef  signed short       sint15;        
typedef  signed char        sint7;         

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32  Word32 ;
typedef int16  Word16 ;
typedef uint8  UWord8 ;
typedef int8   Word8 ;
typedef int32  Vect32 ;


   

    typedef long long     int64;        



    typedef  unsigned long long  uint64;       
#line 205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/services/com_dtypes.h"







#line 32 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/ClockBoot.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/power/voltage_level.h"






 

















 

typedef enum
{
  RAIL_VOLTAGE_LEVEL_OFF           = 0,
  
  RAIL_VOLTAGE_LEVEL_RETENTION     = 16,
  RAIL_VOLTAGE_LEVEL_RETENTION_HIGH = 32,
  
  RAIL_VOLTAGE_LEVEL_SVS_MIN       = 48,
  RAIL_VOLTAGE_LEVEL_SVS_LOW       = 64,
  RAIL_VOLTAGE_LEVEL_SVS           = 128,
  RAIL_VOLTAGE_LEVEL_SVS_HIGH      = 192,
  
  RAIL_VOLTAGE_LEVEL_NOMINAL       = 256,
  RAIL_VOLTAGE_LEVEL_NOMINAL_HIGH  = 320,
  
  RAIL_VOLTAGE_LEVEL_TURBO         = 384,
  RAIL_VOLTAGE_LEVEL_TURBO_HIGH    = 448,
  
  RAIL_VOLTAGE_LEVEL_SUPER_TURBO    = 512,

  RAIL_VOLTAGE_LEVEL_MAX           = 0x7FFFFFF
} rail_voltage_level;




#line 33 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/ClockBoot.h"



 



 










 
typedef enum
{
  CLOCK_BOOT_PERF_NONE,
  CLOCK_BOOT_PERF_MIN,
  CLOCK_BOOT_PERF_NOMINAL,
  CLOCK_BOOT_PERF_MAX,
  CLOCK_BOOT_PERF_DEFAULT,
  CLOCK_BOOT_PERF_NUM,
  CLOCK_BOOT_PERF_32BITS = 0x7FFFFFFF
} ClockBootPerfLevelType;



 
typedef enum
{
   CLK_SDC_NONE,
   CLK_SDC1,
   CLK_SDC2,
   CLK_SDC3,
   CLK_SDC4,
   CLK_SDC5,
   CLK_SDC_NUM_CLKS
} ClockSDCType;



 
typedef enum
{
   CLK_BLSP_UART_NONE,
   CLK_BLSP1_UART1_APPS,
   CLK_BLSP1_UART2_APPS,
   CLK_BLSP1_UART3_APPS,
   CLK_BLSP1_UART4_APPS,
   CLK_BLSP1_UART5_APPS,
   CLK_BLSP1_UART6_APPS,
   CLK_BLSP2_UART1_APPS,
   CLK_BLSP2_UART2_APPS,
   CLK_BLSP2_UART3_APPS,
   CLK_BLSP2_UART4_APPS,
   CLK_BLSP2_UART5_APPS,
   CLK_BLSP2_UART6_APPS,
   CLK_BLSP_UART_NUM_CLKS
} ClockUARTType;




 
typedef enum
{
   CLK_BLSP_QUP_I2C_NONE,
   CLK_BLSP1_QUP1_I2C_APPS,
   CLK_BLSP1_QUP2_I2C_APPS,
   CLK_BLSP1_QUP3_I2C_APPS,
   CLK_BLSP1_QUP4_I2C_APPS,
   CLK_BLSP1_QUP5_I2C_APPS,
   CLK_BLSP1_QUP6_I2C_APPS,
   CLK_BLSP2_QUP1_I2C_APPS,
   CLK_BLSP2_QUP2_I2C_APPS,
   CLK_BLSP2_QUP3_I2C_APPS,
   CLK_BLSP2_QUP4_I2C_APPS,
   CLK_BLSP_QUP_I2C_NUM_CLKS
} ClockQUPI2CType;

typedef enum
{
   
  CLOCK_RESOURCE_QUERY_INVALID         = 0,

  



 
  CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,

  



 
  CLOCK_RESOURCE_QUERY_MIN_FREQ_KHZ, 

  



 
  CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,

  

 
  CLOCK_RESOURCE_QUERY_THRESHOLD_FREQ_KHZ,

  

 
  CLOCK_RESOURCE_QUERY_DDR_SOURCE,

} ClockQueryType;






 

typedef enum {
  SRC_CXO,
  SRC_GPLL0,
  SRC_GPLL1,
  SRC_USB3_PIPE_CLK = 2,
  SRC_GPLL2,
  SRC_GPLL3,
  SRC_GPLL4,
  SRC_GPLL5,
  SRC_BIMCPLL,
  SRC_GPLL6,
  SRC_GPLL6_DIV2,
  SRC_MMPLL0,
  SRC_MMPLL1,
  SRC_MMPLL2,
  SRC_MMPLL3,
  SRC_LPAPLL0,
  SRC_LPAPLL1,
  SRC_WCNPLL,
  SRC_MPLL0,
  SRC_MPLL1,
  SRC_MPLL2,
  SRC_KPPLL0,
  SRC_KPPLL1,
  SRC_KPPLL2,
  SRC_KPPLL3,
  SRC_KPL2PLL,
  SRC_A7PLL,
  SRC_A53PLL,
  SRC_A53PWRPLL,
  SRC_A53PERFPLL,
  SRC_A53CCIPLL,
  SRC_RAW,
  SRC_INVALID = 0x7FFFFFFF,
} ClockSourceType;












 
#line 228 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/ClockBoot.h"






 


typedef enum
{
   CLK_APPS,
   CLK_DDR,
   CLK_NUM,
   CLOCK_CLOCK_TYPE_32BITS = 0x7FFFFFFF
} ClockType;


typedef struct
{
  uint32 pll_dec_start;
  uint32 pll_div_frac_start3;
  uint32 pll_plllock_cmp1;
  uint32 pll_plllock_cmp2;
  uint32 pll_vco_count1;
  uint32 pll_vco_count2;
  uint32 pll_pll_lpf2_postdiv;
  uint32 pll_kvco_code;
} ClockDSFConfigType;





 
typedef struct 
{
  uint32             nFreqKHz;
  rail_voltage_level eVRegLevel;
  uint32             eMode;
  ClockDSFConfigType  sDSFConfig;
}ClockPlanType;



 


 
boolean Clock_PreDDRInit( uint32 ddr_type );




 


 
boolean Clock_PreDDRInitEx( uint32 ddr_type );




 
















 

boolean Clock_Init(ClockBootPerfLevelType eSysPerfLevel,
                    ClockBootPerfLevelType eCPUPerfLevel);




 














 

boolean Clock_SetSysPerfLevel(ClockBootPerfLevelType eSysPerfLevel);




 














 

boolean Clock_SetCPUPerfLevel(ClockBootPerfLevelType eCPUPerfLevel);




 













 
boolean Clock_SetL2PerfLevel(ClockBootPerfLevelType eL2PerfLevel);




 













 
boolean Clock_SetSDCClockFrequency(ClockBootPerfLevelType ePerfLevel,
                                    ClockSDCType eClock);





 













 
uint32 Clock_SetSDCClockFrequencyExt( uint32 nFreqKHz, ClockSDCType eClock);




 













 
boolean Clock_SetUARTClockFrequency(ClockBootPerfLevelType ePerfLevel,
                                    ClockUARTType eClock);





 












 
boolean Clock_DisableUARTClock(ClockUARTType eClock);





 













 
boolean Clock_SetI2CClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockQUPI2CType eClock
);





 












 
boolean Clock_DisableI2CClock(ClockQUPI2CType eClock);




 













 
void Clock_InitForDownloadMode(void);




 












 
boolean Clock_InitUSB(void);




 












 

boolean Clock_InitCrypto(void);




 












 
boolean Clock_ExitBoot(void);




 












 
boolean Clock_DebugInit(void);




 












 
boolean Clock_DisableUSB(void);




 












 
uint32 Clock_DDRSpeed(void);



 












 
boolean Clock_I2CInit(void);



 












 
boolean Clock_EnableQPICForDownloadMode( void );



 

void Clock_BIMCQuery(ClockQueryType nQuery,void* pResource);

void Clock_SwitchBusVoltage(rail_voltage_level eVoltageLevel);

boolean Clock_SetBIMCSpeed(uint32 nFreqKHz );



 













 
boolean Clock_ExtBuckGPIOMisc( void );




 












 
boolean Clock_Usb30SwitchPipeClk(void);





 











 
boolean Clock_Usb30EnableSWCollapse(boolean enable);




 












 
boolean Clock_Usb30GetSWCollapse(void);



 













 
boolean Clock_EnableQPICForDownloadMode( void );




 














 

boolean Clock_GetClockFrequency(ClockType  eClock,  uint32 * pnFrequencyHz);




 













 
boolean Clock_LMhInit( uint32 nA57uV );




 











 
boolean Clock_LMhDeInit( void );




 









 
uint32 Clock_LMhPreCalibration( uint32 nFreqKHz );




 











 
boolean Clock_InitVSense( void );




 











 
boolean Clock_ShutdownVSense( void );

#line 23 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/buses/i2c/config/i2c_boot_8953.xml"

<driver name="I2C">
    <global_def> 
      <var_seq name="i2cqup1_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c022, 0x2001c032,end 
      </var_seq>

      <var_seq name="i2cqup2_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c063, 0x2001c073,end 
      </var_seq>
      
      <var_seq name="i2cqup3_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c0a2, 0x2001c0b2,end 
      </var_seq>    

      <var_seq name="i2cqup4_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c0e3, 0x2001c0f3,end 
      </var_seq>

      <var_seq name="i2cqup5_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c123, 0x2001c133,end 
      </var_seq>
      
      <var_seq name="i2cqup6_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c163, 0x2001c173,end 
      </var_seq>
	  
	  <var_seq name="i2cqup7_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c873, 0x2001c883,end 
      </var_seq>
	  
	  <var_seq name="i2cqup8_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c621, 0x2001c631,end 
      </var_seq> 	  
    </global_def>

    <device id=DALDEVICEID_I2C_DEVICE_1>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP1_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B5000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup1_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_2>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP2_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B6000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup2_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_3>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP3_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B7000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup3_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_4>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP4_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B8000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup4_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
    
    <device id=DALDEVICEID_I2C_DEVICE_5>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP2_QUP1_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x7AF5000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup5_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
    
    <device id=DALDEVICEID_I2C_DEVICE_6>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP2_QUP2_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x7AF6000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup6_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
	
	<device id=DALDEVICEID_I2C_DEVICE_7>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP2_QUP3_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x7AF7000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup7_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
	
	<device id=DALDEVICEID_I2C_DEVICE_8>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP2_QUP4_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x7AF8000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup8_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
</driver>
 
