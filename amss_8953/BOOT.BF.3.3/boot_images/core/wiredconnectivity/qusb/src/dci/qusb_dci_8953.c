//============================================================================
/**
 * @file        qusb_dci_8953.c
 * @author   rthoorpu
 * @date      15-June-2015
 *
 * @brief       QUSB (Qualcomm High-Speed USB) DCI (Device-Controller-Interface) PHY specific handling.
 *
 * @details      There might be different PHY types for different targets.
 *               When using 3-wired Full-Speed PHY the USB Core (Link) cannot conntrol the D+/D-.
 * 
 * @note 
 * 
 * @ref         Design Ware Controller spec "DWC_usb3_databook.pdf".
 *
 *              Copyright (c) 2013 QUALCOMM Technologies Incorporated.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 * 
 */
//============================================================================

// ===========================================================================
// 
//                            EDIT HISTORY FOR FILE
//   This section contains comments describing changes made to the module.
//   Notice that changes are listed in reverse chronological order.
// 
// $Header: //components/rel/boot.bf/3.3/boot_images/core/wiredconnectivity/qusb/src/dci/qusb_dci_8953.c#20 $$DateTime: 2016/04/14 08:01:34 $$Author: pwbldsvc $
// 
// when          who     what, where, why
// ----------   -----    ----------------------------------------------------------
// 
// ===========================================================================

//----------------------------------------------------------------------------
// Include Files
//----------------------------------------------------------------------------
#include "qusb_hwio_dependency_8953.h"
#include "ClockBoot.h"

#include "comdef.h"                 // common defines - basic types as uint32 etc
#include "qusb_dci_api.h"         // DCI API Prototypes
#include "qusb_dci_private.h"     // dQH_t
#include "qusb_log.h" 
#include "pm_smbchg_usb_chgpth.h"  //For detecting the VBus from PMIC
#include "pm_smbchg_misc.h"       //For detecting the charger type (SDP/CDP/DCP)
#include "pm_smbchg_chgr.h"
#include "pm_typec.h"
#include "DDIPlatformInfo.h"
#include "DDITlmm.h"
#include "fs_hotplug.h"

//----------------------------------------------------------------------------
// Preprocessor Definitions and Constants
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// Type Declarations
//----------------------------------------------------------------------------

typedef enum
{
  FS_HOTPLUG_FAILURE = -1,
  FS_HOTPLUG_SUCCESS =  0,
} FS_HOTPLUG_STATUS;
//----------------------------------------------------------------------------
// Global Data Definitions
//----------------------------------------------------------------------------
core_info_t dci_core_info[DCI_MAX_CORES];

#define QUSB_DLOAD_INFO_ADDR_IN_IMEM 	            (SHARED_IMEM_USB_BASE)
//defines for enabling the 1.5k pull up 
#define QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_EN     (HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN   (HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS   (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_SHFT)
#define QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE          (0x0 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE_SHFT)
#define QUSB2PHY_PORT_UTMI_CTRL1_OP_NON_DRV_MODE  (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE_SHFT)

#define QUSB_LINESTATE_CHECK_DELAY                (0x5)
#define QUSB_LINESTATE_CHECK_RETRY_CNT            (10000)   // 50msec is the total wait time to deal with a bad cable
#define QUSB_CHARGER_DETECT_RETRY_TIMEOUT         (100)
#define QUSB_CC_DET_RETRY_CNT                     (6)

#define USB_GPIO_SSMUX_TOGGLE                     (139)



// If a 300 ohm SMB charger resistor on D+/D- which corresponds to HSTX_TRIM offset of value 0x2
// Subtract QUSB_HSTX_TRIM_OFFSET to make amplitude overshoot by 400mV to account for longer cables and hubs.
// We are keeping this value as 0x0 by default. Post SOD, this can be tweaked as needed.
#define QUSB_HSTX_TRIM_OFFSET                     (0x0)

// Used to check QUSB2 PHY PLL STATUS
#define QUSB_HS_PHY_PLL_MAX_CNT           (20000)
#define QUSB_HS_PHY_PLL_BMSK              (0x1)
//----------------------------------------------------------------------------
// Static Variable Definitions
//----------------------------------------------------------------------------
static boolean qusb_forced_download_feature_supported = TRUE;  

typedef struct qusb_dal_info{
DalDeviceHandle          *tlmm;
DALGpioSignalType         pin_config;
DalPlatformInfoPlatformInfoType  platform_info;
}qusb_dal_info_t;

static qusb_dal_info_t qusb_ssmux_dal_info;

//============================================================================
// QUSB Super-Speed PHY Configuration Array
//============================================================================
#define QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT    (108)

// Values according to Sec.3.2.2 of QMP_USB3_PHY_HPG (Feb28, 14)
static const uint32 qusb_dci_ss_phy_cfg_address_common[QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT] =
{
  //-------------------Differential Clock Pre Initialization 0 - 7-------------------------------
  QUSB_HWIO_ADDR_EMPTY,                                               // 0  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 1  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 2  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 3  : (0x00)
  //---------------------------------------------------------------------------------------------
  QUSB_HWIO_ADDR_EMPTY,                                               // 4  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 5  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 6  : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 7  : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,                  // 8  : (0x08)
  HWIO_USB3PHY_QSERDES_COM_CLK_SELECT_ADDR,                           // 9  : (0x30)
  HWIO_USB3PHY_QSERDES_COM_SYS_CLK_CTRL_ADDR,                         // 10 : (0x06)
  QUSB_HWIO_ADDR_EMPTY,                                               // 11 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_RESETSM_CNTRL_ADDR,                        // 12 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_RESETSM_CNTRL2_ADDR,                       // 13 : (0x08)
  HWIO_USB3PHY_QSERDES_TX_LANE_MODE_ADDR,                             // 14 : (0x06)
  HWIO_USB3PHY_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                     // 15 : (0x01)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                            // 16 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_DEC_START_MODE0_ADDR,                      // 17 : (0x82)
  QUSB_HWIO_ADDR_EMPTY,                                               // 18 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 19 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,                // 20 : (0x55)
  HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,                // 21 : (0x55)
  HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,                // 22 : (0x03)
  QUSB_HWIO_ADDR_EMPTY,                                               // 23 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_CP_CTRL_MODE0_ADDR,                        // 24 : (0x0B)
  HWIO_USB3PHY_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                      // 25 : (0x16)
  HWIO_USB3PHY_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                      // 26 : (0x28)
  QUSB_HWIO_ADDR_EMPTY,                                               // 27 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,                // 28 : (0x80)
  HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN1_MODE0_ADDR,                // 29 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_CORECLK_DIV_ADDR,                          // 30 : (0x0A)
  QUSB_HWIO_ADDR_EMPTY,                                               // 31 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                      // 32 : (0x15)
  HWIO_USB3PHY_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                      // 33 : (0x34)
  HWIO_USB3PHY_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                      // 34 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 35 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_LOCK_CMP_EN_ADDR,                          // 36 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_CORE_CLK_EN_ADDR,                          // 37 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_ADDR,                         // 38 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_ADDR,                         // 39 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_BG_TIMER_ADDR,                             // 40 : (0x0A)
  HWIO_USB3PHY_QSERDES_COM_SSC_EN_CENTER_ADDR,                        // 41 : (0x01)
  HWIO_USB3PHY_QSERDES_COM_SSC_PER1_ADDR,                             // 42 : (0x31)
  HWIO_USB3PHY_QSERDES_COM_SSC_PER2_ADDR,                             // 43 : (0x01)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_SSC_ADJ_PER1_ADDR,                         // 44 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_SSC_ADJ_PER2_ADDR,                         // 45 : (0x00)
  HWIO_USB3PHY_QSERDES_COM_SSC_STEP_SIZE1_ADDR,                       // 46 : (0xDE)
  HWIO_USB3PHY_QSERDES_COM_SSC_STEP_SIZE2_ADDR,                       // 47 : (0x07)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_COM_PLL_IVCO_ADDR,                             // 48 : (0x0F)
  HWIO_USB3PHY_QSERDES_COM_BG_TRIM_ADDR,                              // 49 : (0x0F)
  HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_INITVAL_ADDR,                    // 50 : (0x80)
  QUSB_HWIO_ADDR_EMPTY,                                               // 51 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,                 // 52 : (0x0B)
  HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,                 // 53 : (0x02)
  HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_ADDR,                 // 54 : (0x6C)
  HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_ADDR,                 // 55 : (0xBB)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_RX_RX_EQ_OFFSET_ADAPTOR_CNTRL1_ADDR,           // 56 : (0x77)
  HWIO_USB3PHY_QSERDES_RX_RX_OFFSET_ADAPTOR_CNTRL2_ADDR,              // 57 : (0x80)
  HWIO_USB3PHY_QSERDES_RX_SIGDET_CNTRL_ADDR,                          // 58 : (0x03)
  HWIO_USB3PHY_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,                 // 59 : (0x16)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_RX_UCDR_SO_SATURATION_AND_ENABLE_ADDR,         // 60 : (0x75)
  HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_COUNT_LOW_ADDR,               // 61 : (0x00)
  HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_COUNT_HIGH_ADDR,              // 62 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 63 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_RX_UCDR_FO_GAIN_ADDR,                          // 64 : (0x0A)
  HWIO_USB3PHY_QSERDES_RX_UCDR_SO_GAIN_ADDR,                          // 65 : (0x06)
  HWIO_USB3PHY_QSERDES_RX_SIGDET_ENABLES_ADDR,                        // 66 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 67 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,      // 68 : (0x45)
  HWIO_USB3PHY_QSERDES_TX_RCV_DETECT_LVL_2_ADDR,                      // 69 : (0x12)
  HWIO_USB3PHY_QSERDES_COM_CMN_CONFIG_ADDR,                           // 70 : (0x06)
  HWIO_USB3PHY_QSERDES_TX_RES_CODE_LANE_OFFSET_ADDR,                  // 71 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNTRL2_ADDR,                         // 72 : (0x83)
  HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNTRL1_ADDR,                         // 73 : (0x02)
  QUSB_HWIO_ADDR_EMPTY,                                               // 74 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                               // 75 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNT_VAL_L_ADDR,                      // 76 : (0x09)
  HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNT_VAL_H_TOL_ADDR,                  // 77 : (0xA2)
  HWIO_USB3PHY_PCIE_USB3_PCS_FLL_MAN_CODE_ADDR,                       // 78 : (0x85)
  QUSB_HWIO_ADDR_EMPTY,                                               // 79 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG1_ADDR,                // 80 : (0xD1)
  HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG2_ADDR,                // 81 : (0x1F)
  HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG3_ADDR,                // 82 : (0x47)
  QUSB_HWIO_ADDR_EMPTY,                                               // 83 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_TXMGN_V0_ADDR,                           // 84 : (0x9F)
  HWIO_USB3PHY_PCIE_USB3_PCS_TXDEEMPH_M6DB_V0_ADDR,                   // 85 : (0x17)
  HWIO_USB3PHY_PCIE_USB3_PCS_TXDEEMPH_M3P5DB_V0_ADDR,                 // 86 : (0x0F)
  QUSB_HWIO_ADDR_EMPTY,                                               // 87 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_RXEQTRAINING_WAIT_TIME_ADDR,             // 88 : (0x75)
  HWIO_USB3PHY_PCIE_USB3_PCS_RXEQTRAINING_RUN_TIME_ADDR,              // 89 : (0x13)
  HWIO_USB3PHY_PCIE_USB3_PCS_LFPS_TX_ECSTART_EQTLOCK_ADDR,            // 90 : (0x86)
  HWIO_USB3PHY_PCIE_USB3_PCS_PWRUP_RESET_DLY_TIME_AUXCLK_ADDR,        // 91 : (0x04)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_TSYNC_RSYNC_TIME_ADDR,                   // 92 : (0x44)
  HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_P1U2_L_ADDR,               // 93 : (0xE7)
  HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_P1U2_H_ADDR,               // 94 : (0x03)
  QUSB_HWIO_ADDR_EMPTY,                                               // 95 : (0x00)
  //---------------------------------------------------------------------------------------------
  HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_U3_L_ADDR,                 // 96 : (0x40)
  HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_U3_H_ADDR,                 // 97 : (0x00)
  HWIO_USB3PHY_PCIE_USB3_PCS_RX_SIGDET_LVL_ADDR,                      // 98 : (0x88)
  QUSB_HWIO_ADDR_EMPTY,                                               // 99 : (0x00)
  //---------------------------------------------------------------------------------------------
  QUSB_HWIO_ADDR_END,                                                 // 100: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 101: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 102: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 103: (0x00)
  //---------------------------------------------------------------------------------------------
  QUSB_HWIO_ADDR_END,                                                 // 104: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 105: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 106: (0x00)
  QUSB_HWIO_ADDR_END,                                                 // 107: (0x00)

};

__align(4)
static const uint8 qusb_dci_ss_phy_cfg_value_common[QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT] =
{
  //-------------------Differential Clock Pre Initialization 0 - 7-------------------------------
  /* QUSB_HWIO_ADDR_EMPTY,                                               0 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               1 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               2 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               3 */ (0x00),
  //---------------------------------------------------------------------------------------------
  /* QUSB_HWIO_ADDR_EMPTY,                                               4 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               5 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               6 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               7 */ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,                  8 */ (0x08),
  /* HWIO_USB3PHY_QSERDES_COM_CLK_SELECT_ADDR,                           9 */ (0x30),
  /* HWIO_USB3PHY_QSERDES_COM_SYS_CLK_CTRL_ADDR,                         10*/ (0x06),
  /* QUSB_HWIO_ADDR_EMPTY,                                               11*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_RESETSM_CNTRL_ADDR,                        12*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_RESETSM_CNTRL2_ADDR,                       13*/ (0x08),
  /* HWIO_USB3PHY_QSERDES_TX_LANE_MODE_ADDR,                             14*/ (0x06),
  /* HWIO_USB3PHY_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                     15*/ (0x01),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_HSCLK_SEL_ADDR,                            16*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_DEC_START_MODE0_ADDR,                      17*/ (0x82),
  /* QUSB_HWIO_ADDR_EMPTY,                                               18*/ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               19*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,                20*/ (0x55),
  /* HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,                21*/ (0x55),
  /* HWIO_USB3PHY_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,                22*/ (0x03),
  /* QUSB_HWIO_ADDR_EMPTY,                                               23*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_CP_CTRL_MODE0_ADDR,                        24*/ (0x0B),
  /* HWIO_USB3PHY_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                      25*/ (0x16),
  /* HWIO_USB3PHY_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                      26*/ (0x28),
  /* QUSB_HWIO_ADDR_EMPTY,                                               27*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,                28*/ (0x80),
  /* HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN1_MODE0_ADDR,                29*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_CORECLK_DIV_ADDR,                          30*/ (0x0A),
  /* QUSB_HWIO_ADDR_EMPTY,                                               31*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                      32*/ (0x15),
  /* HWIO_USB3PHY_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                      33*/ (0x34),
  /* HWIO_USB3PHY_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                      34*/ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               35*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_LOCK_CMP_EN_ADDR,                          36*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_CORE_CLK_EN_ADDR,                          37*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_ADDR,                         38*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_ADDR,                         39*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_BG_TIMER_ADDR,                             40*/ (0x0A),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_EN_CENTER_ADDR,                        41*/ (0x01),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_PER1_ADDR,                             42*/ (0x31),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_PER2_ADDR,                             43*/ (0x01),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_SSC_ADJ_PER1_ADDR,                         44*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_ADJ_PER2_ADDR,                         45*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_STEP_SIZE1_ADDR,                       46*/ (0xDE),
  /* HWIO_USB3PHY_QSERDES_COM_SSC_STEP_SIZE2_ADDR,                       47*/ (0x07),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_COM_PLL_IVCO_ADDR,                             48*/ (0x0F),
  /* HWIO_USB3PHY_QSERDES_COM_BG_TRIM_ADDR,                              49*/ (0x0F),
  /* HWIO_USB3PHY_QSERDES_COM_INTEGLOOP_INITVAL_ADDR,                    50*/ (0x80),
  /* QUSB_HWIO_ADDR_EMPTY,                                               51*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,                 52*/ (0x0B),
  /* HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,                 53*/ (0x02),
  /* HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_ADDR,                 54*/ (0x6C),
  /* HWIO_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_ADDR,                 55*/ (0xBB),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_RX_RX_EQ_OFFSET_ADAPTOR_CNTRL1_ADDR,           56*/ (0x77),
  /* HWIO_USB3PHY_QSERDES_RX_RX_OFFSET_ADAPTOR_CNTRL2_ADDR,              57*/ (0x80),
  /* HWIO_USB3PHY_QSERDES_RX_SIGDET_CNTRL_ADDR,                          58*/ (0x03),
  /* HWIO_USB3PHY_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,                 59*/ (0x16),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_SO_SATURATION_AND_ENABLE_ADDR,         60*/ (0x75),
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_COUNT_LOW_ADDR,               61*/ (0x00),
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_COUNT_HIGH_ADDR,              62*/ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               63*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_FO_GAIN_ADDR,                          64*/ (0x0A),
  /* HWIO_USB3PHY_QSERDES_RX_UCDR_SO_GAIN_ADDR,                          65*/ (0x06),
  /* HWIO_USB3PHY_QSERDES_RX_SIGDET_ENABLES_ADDR,                        66*/ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               67*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,      68*/ (0x45),
  /* HWIO_USB3PHY_QSERDES_TX_RCV_DETECT_LVL_2_ADDR,                      69*/ (0x12),
  /* HWIO_USB3PHY_QSERDES_COM_CMN_CONFIG_ADDR,                           70*/ (0x06),
  /* HWIO_USB3PHY_QSERDES_TX_RES_CODE_LANE_OFFSET_ADDR,                  71*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNTRL2_ADDR,                         72*/ (0x83),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNTRL1_ADDR,                         73*/ (0x02),
  /* QUSB_HWIO_ADDR_EMPTY,                                               74*/ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                               75*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNT_VAL_L_ADDR,                      76*/ (0x09),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_FLL_CNT_VAL_H_TOL_ADDR,                  77*/ (0xA2),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_FLL_MAN_CODE_ADDR,                       78*/ (0x85),
  /* QUSB_HWIO_ADDR_EMPTY,                                               79*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG1_ADDR,                80*/ (0xD1),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG2_ADDR,                81*/ (0x1F),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_LOCK_DETECT_CONFIG3_ADDR,                82*/ (0x47),
  /* QUSB_HWIO_ADDR_EMPTY,                                               83*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_TXMGN_V0_ADDR,                           84*/ (0x9F),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_TXDEEMPH_M6DB_V0_ADDR,                   85*/ (0x17),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_TXDEEMPH_M3P5DB_V0_ADDR,                 86*/ (0x0F),
  /* QUSB_HWIO_ADDR_EMPTY,                                               87*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RXEQTRAINING_WAIT_TIME_ADDR,             88*/ (0x75),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RXEQTRAINING_RUN_TIME_ADDR,              89*/ (0x13),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_LFPS_TX_ECSTART_EQTLOCK_ADDR,            90*/ (0x86),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_PWRUP_RESET_DLY_TIME_AUXCLK_ADDR,        91*/ (0x04),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_TSYNC_RSYNC_TIME_ADDR,                   92*/ (0x44),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_P1U2_L_ADDR,               93*/ (0xE7),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_P1U2_H_ADDR,               94*/ (0x03),
  /* QUSB_HWIO_ADDR_EMPTY,                                               95*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_U3_L_ADDR,                 96*/ (0x40),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RCVR_DTCT_DLY_U3_H_ADDR,                 97*/ (0x00),
  /* HWIO_USB3PHY_PCIE_USB3_PCS_RX_SIGDET_LVL_ADDR,                      98*/ (0x88),
  /* QUSB_HWIO_ADDR_EMPTY,                                               99*/ (0x00),
  //---------------------------------------------------------------------------------------------
  /* QUSB_HWIO_ADDR_END,                                                 100*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 101*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 102*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 103*/(0x00),
  //---------------------------------------------------------------------------------------------
  /* QUSB_HWIO_ADDR_END,                                                 104*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 105*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 106*/(0x00),
  /* QUSB_HWIO_ADDR_END,                                                 107*/(0x00),
};

#define QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT          (128)
#define QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX                       (8)

// initialization without reading the foundry information
static uint32 qusb_dci_ss_phy_foundry_id = 0;

// Array keeps tracking of starting index of override programming sequence
// per foundry
static const uint32 qusb_dci_ss_phy_cfg_override_start_index[QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX] =
{ 
    0,    // 0
    16,   // 1
    32,   // 2
    48,   // 3
    64,   // 4
    80,   // 5
    96,   // 6
    112,  // 7
};

// Between 2 foundries, there must be QUSB_HWIO_ADDR_END as sentinel value to
// prevent going over the boundry.
static const uint32 qusb_dci_ss_phy_cfg_address_override[QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT] = 
{
    //  0
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 16
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 32
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 48
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 64
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 80
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 96
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 112
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END, QUSB_HWIO_ADDR_END,
    // 128
};
__align(4)
static const uint8 qusb_dci_ss_phy_cfg_value_override[QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT]    = 
{
  //  0
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 16
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 32
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 48
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 64
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 80
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 96
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 112
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  // 128
};

//============================================================================
// QUSB High-Speed PHY Configuration Array
//============================================================================

#define QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT      (12)

// It is recommended to set the CSR bit "r_power_down = 1'b1", program any 
// hardware tuning (such as mentioned in #1 above)/config bits/etc., then 
// release "r_power_down = 1'b0" once again, in the QUSB2PHY_POWER_DOWN 
// register.

static const uint32 qusb_dci_hs_phy_cfg_address[QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE1_ADDR,                       // 0  : (0xF8)
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE2_ADDR,                       // 1  : (0xB3)
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE3_ADDR,                       // 2  : (0x83)
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE4_ADDR,                       // 3  : (0xC0)
  //---------------------------------------------------------------------------
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TEST2_ADDR,                       // 4  : (0x14)
  HWIO_QUSB2PHY_QUSB2PHY_PLL_TUNE_ADDR,                         // 5  : (0x30)
  HWIO_QUSB2PHY_QUSB2PHY_PLL_USER_CTL1_ADDR,                    // 6  : (0x79)
  HWIO_QUSB2PHY_QUSB2PHY_PLL_USER_CTL2_ADDR,                    // 7  : (0x21)
  //---------------------------------------------------------------------------
  HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE5_ADDR,                       // 8  : (0x00)
  HWIO_QUSB2PHY_QUSB2PHY_PLL_PWR_CTL_ADDR,                      // 9  : (0x00)
  HWIO_QUSB2PHY_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,                 // 10 : (0x9F)
  QUSB_HWIO_ADDR_END,                                           // 11 : (0x00)
};

static const uint8 qusb_dci_hs_phy_cfg_value[QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  /*HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE1_ADDR,                      0  */ (0xF8),
  /*HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE2_ADDR,                      1  */ (0xB3),
  /*HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE3_ADDR,                      2  */ (0x83),
  /*HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE4_ADDR,                      3  */ (0xC0),
  //---------------------------------------------------------------------------
  /* HWIO_QUSB2PHY_QUSB2PHY_PORT_TEST2_ADDR,                     4  */ (0x14),
  /* HWIO_QUSB2PHY_QUSB2PHY_PLL_TUNE_ADDR,                       5  */ (0x30),
  /* HWIO_QUSB2PHY_QUSB2PHY_PLL_USER_CTL1_ADDR,                  6  */ (0x79),
  /* HWIO_QUSB2PHY_QUSB2PHY_PLL_USER_CTL2_ADDR,                  7  */ (0x21),
  //----------------------------------------------------------------------------
  /* HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE5_ADDR,                     8  */ (0x00),
  /* HWIO_QUSB2PHY_QUSB2PHY_PLL_PWR_CTL_ADDR,                    9  */ (0x00),
  /* HWIO_QUSB2PHY_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,               10 */ (0x9F),
  /* QUSB_HWIO_ADDR_END,                                         11 */ (0x00),
};


//----------------------------------------------------------------------------
// Static Function Declarations and Definitions
//----------------------------------------------------------------------------
static void qusb_dci_phy_reg_array_process
(
  const uint32 *address_array, 
  const uint8  *value_array, 
  uint32        start_index, 
  uint32        array_entry_cnt
);

//----------------------------------------------------------------------------
// Externalized Function Definitions
//----------------------------------------------------------------------------
// ===========================================================================
/**
 * @function    qusb_get_phy_clk_type
 * 
 * @brief   API is used to get the PHY reference clock type. Basically there 
 *          are two types of reference clocks Differential Clock and Single 
 *          Ended type. 
 *
 * @param   None.
 * 
 * @return  phy_ref_clk_t 0: Differential Clock 
 *                        1: Single Ended Clock 
 * 
 */
// ===========================================================================
static phy_ref_clk_t qusb_get_phy_clk_type(void)
{
  phy_ref_clk_t ref_clk_type; 
  ref_clk_type = QUSB_CML_DIFF_REF_CLK; 
  ref_clk_type = (phy_ref_clk_t)(HWIO_FEATURE_CONFIG5_INM(
                                 HWIO_FEATURE_CONFIG5_DIFFERENTIAL_SINGLE_ENDED_PHY_CLK_BMSK)>>
                                 HWIO_FEATURE_CONFIG5_DIFFERENTIAL_SINGLE_ENDED_PHY_CLK_SHFT );
  return ref_clk_type; 
}

//============================================================================
/**
 * @function    qusb_get_shared_imem_address
 * 
 * @brief   	Gets the shared imem address based on the hardware version
 * 
 * @note    	None          
 *  
 * @param   	None
 *
 * @return 		uint32 A 32-Bit Address
 *
 * @ref 
 *
 */
// ===========================================================================
uint32 qusb_get_shared_imem_address()
{
  return QUSB_DLOAD_INFO_ADDR_IN_IMEM;
}

//----------------------------------------------------------------------------
// Static Function Declarations and Definitions
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Externalized Function Definitions
//----------------------------------------------------------------------------
//============================================================================
/**
 * @function  qusb_is_charger_type_sdp_or_cdp
 *
 * @brief     This API determines whether the Cable Connected is USB SDP
 *
 * @Note :    This API would check if the cable type connected is SDP/CDP
 *            This API reads the data from the PMI Chip and then provides
 *            the result to USB driver
 *
 * @param     None
 *
 * @return boolean ( TRUE if SDP or CDP, else FALSE )
 *
 *
 */
//============================================================================
static boolean qusb_is_charger_type_sdp_or_cdp(void)
{
  pm_smbchg_misc_src_detect_type charger_type = PM_SMBCHG_MISC_SRC_DETECT_INVALID;
  boolean is_chg_type_sdp_or_cdp = FALSE;
  pm_err_flag_type pm_err;
  uint32 charger_detect_retry;
  
  
  for(charger_detect_retry = 0; charger_detect_retry < QUSB_CHARGER_DETECT_RETRY_TIMEOUT;
      charger_detect_retry++)
  {
    pm_err = pm_smbchg_misc_chgr_port_detected(1, &charger_type);
    if(PM_ERR_FLAG__SUCCESS != pm_err)
    {
      qusb_uart_log("usb: chg_port_detect failed");
      return FALSE;
    }
    if(PM_SMBCHG_MISC_SRC_DETECT_INVALID != charger_type)
    {
      break;
    }
    qusb_dci_delay_ms(5);
    
  }

  switch (charger_type)
  {
    case PM_SMBCHG_MISC_SRC_DETECT_CDP:
      qusb_uart_log("usb:: cdp");
      is_chg_type_sdp_or_cdp = TRUE;
      break;
      
    case PM_SMBCHG_MISC_SRC_DETECT_DCP:
      qusb_uart_log("usb:: dcp");
      is_chg_type_sdp_or_cdp = FALSE;
      break;
      
    case PM_SMBCHG_MISC_SRC_DETECT_OTHER_CHARGING_PORT:
      qusb_uart_log("usb:: other_chg_port");
      is_chg_type_sdp_or_cdp = FALSE;
      break;
      
    case PM_SMBCHG_MISC_SRC_DETECT_SDP:
      qusb_uart_log("usb:: sdp");
      is_chg_type_sdp_or_cdp = TRUE;
      break;
      
    case PM_SMBCHG_MISC_SRC_DETECT_INVALID:
      qusb_uart_log("usb:: invalid_chg");
      is_chg_type_sdp_or_cdp = FALSE;
      break;
      
    default:
      is_chg_type_sdp_or_cdp = FALSE;
      
  }
  return(is_chg_type_sdp_or_cdp);
  
}


//============================================================================
/**
 * @function  qhsusb_dci_is_vbus_valid
 *
 * @brief     This API determines whether the VBus is High or Low. 
 *
 * @Note :    This API can determine whether the VBus is High or Low. This API
 *            depends on PMIC API to get the VBus Status. Please check with 
 *            PMIC team whether the API is available before porting to new 
 *            targets
 *
 * @param     None   
 *
 * @return boolean ( TRUE if Valid (or High), else FALSE )
 *
 *
 */
//============================================================================
boolean qusb_dci_is_vbus_valid(void)
{
    boolean is_vbus_valid = FALSE;
    pm_smbchg_usb_chgpth_input_sts_type input_type;
    
#ifdef FEATURE_FIREHOSE
    return TRUE; 
#else 
  /* check for the vbus with index '1' ( for internal PMI )  */
  if(PM_ERR_FLAG__SUCCESS == pm_smbchg_usb_chgpth_input_sts(1, PM_SMBCHG_CHAR_TYPE_USB, &input_type))
  {
    if(PM_SMBCHG_5V_9V_CHGR_DETECTED == input_type)
    {
      is_vbus_valid = TRUE;
    }    
    else
    {
      is_vbus_valid = FALSE;  
    }
  }  
  else
  {
    /* PMI not present */  
    is_vbus_valid = FALSE;
    qusb_uart_log("usb:No PMI");
  }

  return(is_vbus_valid);
#endif    
}


// ===========================================================================
/**
 * @function    qusb_hs_phy_refclk_enable
 * 
 * @brief   This function will be used to enable / disable HS PHY reference clock.
 * 
 * @param  TRUE or FALSE depending on enable or disable.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_hs_phy_refclk_enable(boolean enable)
{
  HWIO_GCC_QUSB_REF_CLK_EN_OUTM(HWIO_GCC_QUSB_REF_CLK_EN_CLK_ENABLE_BMSK,
    enable << HWIO_GCC_QUSB_REF_CLK_EN_CLK_ENABLE_SHFT);

  HWIO_GCC_QUSB_REF_CLK_EN_OUTM(HWIO_GCC_QUSB_REF_CLK_EN_HW_CONTROL_BMSK,
    enable << HWIO_GCC_QUSB_REF_CLK_EN_HW_CONTROL_SHFT);
}


// ===========================================================================
/**
 * @function    qusb_dci_enable_usb30_clocks
 * 
 * @brief   This function will be used to turn ON the USB3.0 clocks
 * 
 * @param   None
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
boolean qusb_dci_enable_usb30_clocks(void)
{
  boolean var = TRUE; 
#if !defined(FEATURE_FIREHOSE_QUSB)
  var = Clock_InitUSB();
#endif
  return (var);
}

// ===========================================================================
/**
 * @function    qusb_dci_disable_usb30_clocks
 * 
 * @brief   This function will be used to turn OFF the USB3.0 clocks
 * 
 * @param   None
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
boolean qusb_dci_disable_usb30_clocks(void)
{
  boolean var = TRUE; 
#if !defined(FEATURE_FIREHOSE_QUSB)
  var = Clock_DisableUSB();
#endif
  
  qusb_hs_phy_refclk_enable(FALSE);
  
  return (var);
}

// ===========================================================================
/**
 * @function    qusb_dci_enable_usb30_power
 * 
 * @brief   This function enables the power domain for SNPS controller
 *             On Jacala, SNPS core is not power collapsible  but GDSCR controlls the clock gating
 *
 * @param   TRUE: It enables the s/w Power Collapse 
 *          FALSE:  IT disables the s/w Power Collapse
 * 
 * @details  Used for enabling the power domain for SNPS controller
 * 
 * @return  TRUE or FALSE depending on success or failure.
 * 
 */
// ===========================================================================
void qusb_dci_enable_usb30_power_collapse(boolean enable)
{
  Clock_Usb30EnableSWCollapse(enable); 
  return; 
}
// ===========================================================================
/**
 * @function    qusb_dci_is_usb30_sw_collapsed
 * 
 * @brief   This function reads back if  GDSCR is gating clocks to USB core.
 * 
 * @details Used fr cheking if USB core was initialized in PBL.
 *          If not then single enumeration cannot continue, 
 *          USB core needs to be re-enumerated.
 * 
 * @return  TRUE or FALSE depending on status of SW collapse bit.
 * 
 */
// ===========================================================================
boolean qusb_dci_is_usb30_sw_collapsed(void)
{
  return Clock_Usb30GetSWCollapse();
}

// ===========================================================================
/**
 * @function    qusb_dci_get_hardware_id
 * 
 * @brief   This function will be used for getting the SNPS h/w id.
 * 
 * @param   None
 * 
 * @return  uint32 - hardware id. 
 * 
 */
// ===========================================================================
uint32 qusb_dci_get_hardware_id(void)
{
  uint32 hw_id =0x0;
  hw_id = HWIO_USB30_GSNPSID_INM(HWIO_USB30_GSNPSID_SYNOPSYSID_BMSK);
  return hw_id;
}

// ===========================================================================
/**
 * @function    qusb_dci_hs_phy_update_hstx_trim
 * 
 * @brief   This function will update TUNE2 HSTX_TRIM register bits if feature is enabled.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_dci_hs_phy_update_hstx_trim(void)
{
  uint8 hstx_trim_val = 0x0; 
  
  // Update non-zero HSTX_TRIM value read from fuse.
  hstx_trim_val = 
  HWIO_OEM_CONFIG0_INM(HWIO_OEM_CONFIG0_USB2PHY_HSTX_CALIB_BMSK) 
                                            >> HWIO_OEM_CONFIG0_USB2PHY_HSTX_CALIB_SHFT;

  if(hstx_trim_val)
  {
      HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE2_OUTM(
                                            HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_BMSK, 
                                            (hstx_trim_val - QUSB_HSTX_TRIM_OFFSET) << 
                                            HWIO_QUSB2PHY_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_SHFT);
  }
}

// ------------------------------------------------------------------------------------------------
// Core Initialization APIs
// ------------------------------------------------------------------------------------------------
// ===========================================================================
/**
 * @function    qusb_hs_phy_init
 * 
 * @brief   API used to initialize the High Speed PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
boolean qusb_hs_phy_init(void)
{
  uint32 pll_status = 0;
  uint32 pll_status_cnt = 0;
  /* Keep the PHY in power down mode */
  HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_OUTM(HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
                                             (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));
 
  qusb_dci_phy_reg_array_process(
        qusb_dci_hs_phy_cfg_address, 
        qusb_dci_hs_phy_cfg_value, 
        0, 
        QUSB_DCI_HS_PHY_CFG_ARRAY_ENTRY_CNT);
  
  
  // Overwrite HSTX TRIM calibration value
  qusb_dci_hs_phy_update_hstx_trim();

  /* Bring Out  the PHY from power down mode */
  HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_OUTM(HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
                                             (0x0 << HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

  /* The delay is required for PHY PLL capacitor to get charged / ramp up */
  qusb_dci_delay_us(150);

  //Tune the value in PHY CSR to ensure it can work with Single Ended Reference Clock
  if(QUSB_SINGLE_ENDED_REF_CLK == qusb_get_phy_clk_type())
  {
    HWIO_QUSB2PHY_QUSB2PHY_PLL_TEST_OUT(0x80); 
  }
  else
  {
    qusb_hs_phy_refclk_enable(TRUE);
  }

  /* Wait maximum of ~200ms for PLL to lock */
  for (pll_status_cnt = 0; (0 == pll_status) && (pll_status_cnt < QUSB_HS_PHY_PLL_MAX_CNT); pll_status_cnt++)
  {
    qusb_dci_delay_us(10);

    pll_status = (HWIO_QUSB2PHY_QUSB2PHY_PLL_STATUS_INM(HWIO_QUSB2PHY_QUSB2PHY_PLL_STATUS_PLL_STATUS_BMSK)) 
      >> HWIO_QUSB2PHY_QUSB2PHY_PLL_STATUS_PLL_STATUS_SHFT;

    // Only LSB is PLL status, rest of bits are reserved
    pll_status &= QUSB_HS_PHY_PLL_BMSK;
  }

  if (0 == pll_status)
  {
    qusb_error_log(DCI_HS_PHY_PLL_FAILURE_LOG, 0, pll_status_cnt);
    qusb_uart_log("usb: PLL lock failed");
    return FALSE;
  }

  return TRUE;
}

// ===========================================================================
/**
 * @function    qusb_dci_configure_device_mode
 * 
 * @brief   This is used for configuring the Device config Register post reset.
 * 
 * @param   qusb_max_speed_required_t Maximum speed at which the device can operate
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_configure_device_mode(qusb_max_speed_required_t speed_required)
{
  // Configure the Minimum negotiatiated Speed to  HS: 0x0 , SS: 0x4
  if(speed_required == QUSB_MAX_SPEED_SUPER)
  {
    /* Maximum Speed Configured is Super Speed */
    HWIO_USB30_DCFG_OUTM(HWIO_USB30_DCFG_DEVSPD_BMSK, (QUSB_SUPER_SPEED << HWIO_USB30_DCFG_DEVSPD_SHFT));
  }
    else if (speed_required == QUSB_MAX_SPEED_HIGH)
  {
    /* Maximum Speed Configured is High Speed */
    HWIO_USB30_DCFG_OUTM(HWIO_USB30_DCFG_DEVSPD_BMSK, (QUSB_HIGH_SPEED << HWIO_USB30_DCFG_DEVSPD_SHFT));
  }

  /*set the device address to 0x0 after reset*/ 
  HWIO_USB30_DCFG_OUTM(HWIO_USB30_DCFG_DEVADDR_BMSK,(0x0 << HWIO_USB30_DCFG_DEVADDR_SHFT)); 

  /*LPM Capability EN: 0x1, DIS:0x0*/
  HWIO_USB30_DCFG_OUTM(HWIO_USB30_DCFG_LPMCAP_BMSK,(0x0 << HWIO_USB30_DCFG_LPMCAP_SHFT));

  /* Enable Device mode operation */
  HWIO_USB30_GCTL_OUTM(HWIO_USB30_GCTL_PRTCAPDIR_BMSK, (0x2 << HWIO_USB30_GCTL_PRTCAPDIR_SHFT )); 

  return;
}


// ===========================================================================
/**
 * @function    qusb_dci_configure_usb30
 * 
 * @brief   API used for PHY configuration in device Mode for USB3.0 
 *  
 * @details This API is used for configuring based on the HPG specifications 
 * 			Please follow the same steps for configuration 
 * 
 * @param   qusb_max_speed_required_t Maximum speed at which the device should
 *          operate. 
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_configure_usb30(qusb_max_speed_required_t speed_required)
{

  // Set DELAYP1TRANS to 0 
  HWIO_USB30_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_OUTMI(0, HWIO_USB30_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_DELAYP1TRANS_BMSK,
                               ((uint32) 0x0 << HWIO_USB30_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_DELAYP1TRANS_SHFT));
 
  // Controller Configuration to Peripheral mode 
  qusb_dci_configure_device_mode(speed_required);

  return;
}

// ===========================================================================
/**
 * @function    qusb_dci_usb30_gcc_reset
 * 
 * @brief   API used for resetting the Link and PHYs using GCC control
 *  
 * @details This API is used for resetting the Link and PHYs using clock control 
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_usb30_gcc_reset(void)
{
  // Reset SNPS Link controller 
  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  // Reset QUSB (USB 2.0) and QMP (USB 3.0) PHYs 
  HWIO_GCC_QUSB2_PHY_BCR_OUTM(HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_QUSB2_PHY_BCR_OUTM(HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_SHFT));

  return; 
}

// ===========================================================================
/**
 * @function    qusb_dci_select_utmi_clk
 * 
 * @brief   This is used for configuring the core to UTMI clock instead of pipe
 *          clock.  This needs to be called when there is no SS USB PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_select_utmi_clk(void)
{
  qusb_error_log(QUSB_DCI_SELECT_UTMI_CLK_LOG, 0, 0);
  
  // If operating without SS PHY, follow this sequence to disable 
  // pipe clock requirement
  HWIO_USB30_GENERAL_CFG_OUTM(HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
    0x1 << HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);
  
  qusb_dci_delay_us(100);
  
  HWIO_USB30_GENERAL_CFG_OUTM(HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_SEL_BMSK,
    0x1 << HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_SEL_SHFT);
  
  qusb_dci_delay_us(100);
  
  HWIO_USB30_GENERAL_CFG_OUTM(HWIO_USB30_GENERAL_CFG_PIPE3_PHYSTATUS_SW_BMSK,
    0x1 << HWIO_USB30_GENERAL_CFG_PIPE3_PHYSTATUS_SW_SHFT);
  
  qusb_dci_delay_us(100);
  
  HWIO_USB30_GENERAL_CFG_OUTM(HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
    0x0 << HWIO_USB30_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);      
}

// ===========================================================================
/**
 * @function    qusb_ss_phy_init
 * 
 * @brief   API used for initializing the SS PHY 
 *  
 * @details This is used for initializing the SNPS controller PHY and QMP PHY 
 * 			Configure the QMP PHY as per Table 4-1 of the QMP PHY HPG 
 * 
 * @param   None.
 * 
 * @return  TRUE if successful else FALSE.
 * 
 */
// ===========================================================================
boolean qusb_ss_phy_init(void)
{
  uint32 qusb_timeout=0x0;

  qusb_error_log(QUSB_SS_PHY_INIT_LOG, 0, 0);

  /*Put the PHY and CSR blocks into RESET*/
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  /*Put the PHY block into RESET*/
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);

  /*Release the CSR block from RESET*/
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);
  
  /* Release the PHY from Power Down mode */
  HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_OUTM(HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, (0x1 << HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));
  
  //Tune the value in PHY CSR to ensure it can work with Single Ended Reference Clock
  // or differential reference clock
  if(QUSB_SINGLE_ENDED_REF_CLK == qusb_get_phy_clk_type())
  {
    HWIO_USB3PHY_QSERDES_COM_SYSCLK_EN_SEL_OUT(0x1A);
  }
  else
  {
    HWIO_USB3PHY_QSERDES_COM_SYSCLK_EN_SEL_OUT(0x14);
  }

  // Configure the QMP PHY as per Table 4-1 of the QMP PHY HPG with default entry
  qusb_dci_phy_reg_array_process(
        qusb_dci_ss_phy_cfg_address_common, 
        qusb_dci_ss_phy_cfg_value_common,
        0,
        QUSB_DCI_SS_PHY_COMMON_CFG_ARRAY_ENTRY_CNT);
  
  // Read foundry specific information and program addition overrides
  // qusb_dci_ss_phy_foundry_id = pbl_get_foundry_id();
  if (qusb_dci_ss_phy_foundry_id >= QUSB_DCI_SS_PHY_FOUNDRY_CNT_MAX)
  {
    qusb_error_log(INVALID_QUSB_DCI_SS_PHY_FOUNDRY_ID_LOG, 0, qusb_dci_ss_phy_foundry_id);
  }
  else
  {
    qusb_dci_phy_reg_array_process(
          qusb_dci_ss_phy_cfg_address_override, 
          qusb_dci_ss_phy_cfg_value_override, 
          qusb_dci_ss_phy_cfg_override_start_index[qusb_dci_ss_phy_foundry_id],
          QUSB_DCI_SS_PHY_OVERRIDE_CFG_ARRAY_ENTRY_CNT);
  }

  /* Release the PHY block from Reset */
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_dci_delay_us(100);
  
  
  /* Release the PHY from SW Reset state */
  HWIO_USB3PHY_PCIE_USB3_PCS_SW_RESET_OUTM(HWIO_USB3PHY_PCIE_USB3_PCS_SW_RESET_SW_RESET_BMSK, (0x0 << HWIO_USB3PHY_PCIE_USB3_PCS_SW_RESET_SW_RESET_SHFT));
  
  /* To Ensure a proper Fll to occur, Start PHY operation should happen first */
  /* Start PHY operation (begin with internal power-up & calibration) */
  HWIO_USB3PHY_PCIE_USB3_PCS_START_CONTROL_OUT(0x03);

  /* Poll PHYSTATUS field of Register USB3_PHY_PCS_STATUS to Go LOW after reset*/
  while(HWIO_USB3PHY_PCIE_USB3_PCS_PCS_STATUS_INM(HWIO_USB3PHY_PCIE_USB3_PCS_PCS_STATUS_PHYSTATUS_BMSK))
  {
    qusb_dci_delay_us(100);
    if (qusb_timeout++ > 100)
    {
      DCI_ASSERT(DCI_SS_PHY_RESET_ERROR_LOG);
      return FALSE;
    }
  } 

  return TRUE;
}


// ===========================================================================
/**
 * @function    qusb_dci_deinit_hs_phy
 * 
 * @brief   API used to de-initialize the High Speed PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_deinit_hs_phy(void)
{
  //Clear the D+ Pull Up Assertion
  HWIO_USB30_HS_PHY_CTRL_OUTM(HWIO_USB30_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x0 << HWIO_USB30_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
  HWIO_USB30_HS_PHY_CTRL_OUTM(HWIO_USB30_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x0 << HWIO_USB30_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));
}
// ===========================================================================
/**
 * @function    qusb_dci_enable_vbus_valid
 * 
 * @brief       API used to enable VBUS using s/w control
 * 
 * @param   qusb_max_speed_required_t - Maximum Speed required to be configured
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dci_enable_vbus_valid(qusb_max_speed_required_t speed_required)
{

  HWIO_USB30_HS_PHY_CTRL_OUTM(HWIO_USB30_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x1 << HWIO_USB30_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
  HWIO_USB30_HS_PHY_CTRL_OUTM(HWIO_USB30_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x1 << HWIO_USB30_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));

  /* If we want to enable SUPER SPEED then also set LANE PWR PRESENT bit */
  if(speed_required == QUSB_MAX_SPEED_SUPER)
  {
    HWIO_USB30_SS_PHY_CTRL_OUTM(HWIO_USB30_SS_PHY_CTRL_LANE0_PWR_PRESENT_BMSK, (0x1 << HWIO_USB30_SS_PHY_CTRL_LANE0_PWR_PRESENT_SHFT));
  }

  return;
}
// ===========================================================================
/**
 * @function    qusb_dci_phy_reg_array_process
 * 
 * @brief   This function reads from array which define list of hwio writes for
 *          USB PHY
 * 
 * @param   address_array   - array holding address of HW register
 *          value_array     - array holding values to be written to HW register
 *          start_index     - starting index for array processing
 *          array_entry_cnt - number of entries in the array
 * 
 * @return  None
 *
 * @Note    When only a portion of the array needs to be parsed, the 
 *          array_entry_cnt should be end_index to be written + 1. When the 
 *          entire array needs to be parsed, you can give the array size
 * 
 */
// ===========================================================================
static void qusb_dci_phy_reg_array_process
(
  const uint32 *address_array, 
  const uint8  *value_array, 
  uint32        start_index, 
  uint32        array_entry_cnt
)
{
  uint32 index = start_index;

  if ( (NULL == address_array)
      || (NULL == value_array)
      || (0 == array_entry_cnt) )
  {
    qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS__FAIL_LOG, 0, (uint32)address_array);
  }
  else
  {
    for (; index < array_entry_cnt; index++)
    {
      if (QUSB_HWIO_ADDR_END == address_array[index])
      {
        break;
      }

      if (QUSB_HWIO_ADDR_EMPTY == address_array[index])
      {
        continue;
      }

      out_dword(address_array[index], value_array[index]);
    }
  }
  //qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS__START_LOG, 0, start_index);
  //qusb_error_log(QUSB_DCI_PHY_REG_ARRAY_PROCESS____END_LOG, 0, index);
}
// ===========================================================================
/**
 * @function    qusb_dci_check_for_pbl_dload
 * 
 * @brief   This function will check if D+ is grounded. And if it is connected
 *          to ground, then we go into EDL.
 * 
 * @param   None
 * 
 * @return  TRUE: if it is connected to GND
 *          FALSE: if it is not connected to GND. 
 * 
 */
// ===========================================================================
boolean qusb_dci_check_for_pbl_dload(void)
{
  uint32 linestate = 0x0; 
  int32 retries = 0x0;
  uint32 enable_fs_xvr = 0x0;
  boolean select_utmi_config = TRUE; // Fall back to HS Only UTMI config by default.
  
  if(FALSE == qusb_forced_download_feature_supported)
  {
    qusb_log(QUSB_SKIP_PBL_DLOAD_LOG, 0, 0xFF);
    return FALSE; 
  }
  
  /** Bring the controller out of Power Collapse**/
  qusb_dci_enable_usb30_power_collapse(FALSE);
  
  //Disable the clocks 
  qusb_dci_disable_usb30_clocks(); 
  
  //hard reset the USB Link and PHY using the GCC reset
  qusb_dci_usb30_gcc_reset();
  
  //enable the clocks now 
  qusb_dci_enable_usb30_clocks();

  // Write 0x11 to AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG so that 
  // writes and reads to/from the PHY use one wait state.
  HWIO_AHB2PHY_TOP_CFG_OUT(0x11);

  // Initialize SS PHY if present
  if(qusb_dci_ss_phy_present())
  {
    // Fall back to HS only UTMI config if SS PHY initialization fails.
    select_utmi_config = (FALSE == qusb_ss_phy_init());
  }

  if(select_utmi_config)
  {
    qusb_dci_select_utmi_clk();
  }

  //Initialize the HS_PHY
  if( FALSE == qusb_hs_phy_init())
  {
    /* Keep the PHY in power down mode */
    HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));
    
    return FALSE;
  }
  
  //Enable the VBUS
  qusb_dci_enable_vbus_valid(QUSB_MAX_SPEED_HIGH);

  if(FALSE == qusb_dci_is_vbus_valid())
  {
    qusb_log(QUSB_SKIP_PBL_DLOAD_VBUS_LOW_LOG,0,0);
    return FALSE;
  }
  else
  {
    //This is the case where Vbus is valid and
    //we want to ensure that charger type is CDP/SDP.
    if(FALSE == qusb_is_charger_type_sdp_or_cdp())
    {
      return FALSE;
    }
  }
  
  // Enable UTMI_TEST_MUX_SEL
  HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
    0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);

  
  //Disable the D+ Pull down 
  HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_BMSK, 
                                            (0x0 <<HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_SHFT )); 
  
  //Select the FS transceiver and enable the 1.5k pull-up. 
  enable_fs_xvr = (QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_EN | QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN | 
                   QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS | QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE); 
  HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_OUT(enable_fs_xvr);
  
  do
  {
    //wait for 5 microseconds 
    qusb_dci_delay_us(QUSB_LINESTATE_CHECK_DELAY); 
    
    //read the linestate
    linestate = HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_STATUS_INM(HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_STATUS_LINESTATE_BMSK); 
    
    //if D+ is still high, then the device should not enter the EDL Mode 
    if(linestate != 0x0)
    {
      break; 
    }
    
    retries++; 
   
  }while (retries < QUSB_LINESTATE_CHECK_RETRY_CNT);
  
  //disable the 1.5K Pull up resistor
  HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_OUT(HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_BMSK);
  
  // Disable UTMI_TEST_MUX_SEL
  HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
    0x0 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);

  qusb_dci_delay_us(10);

  /* Keep the PHY in power down mode */
  HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_OUTM(
    HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
    (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

  if(linestate != 0x0)
  {
    qusb_log(QUSB_SKIP_PBL_DLOAD_LOG, 0, linestate);
    return FALSE; 
  }
  //D+ is connected to the GND
  qusb_log(QUSB_ENTER_PBL_DLOAD_LOG, 0, linestate); 
  qusb_uart_log("usb:force_eDL");
  
  return TRUE;
}

// ===========================================================================
/**
 * @function    qusb_dci_check_ss_phy_presence
 * 
 * @brief   This function will check if 0.9V analog supply to SS PHY is on.
 * 
 * @param   None
 * 
 * @return  TRUE:  If analog supply to SS PHY on.
 *          FALSE: If analog supply to SS PHY off.
 * 
 */
// ===========================================================================
boolean qusb_dci_ss_phy_present(void)
{
  uint32 ac_jtag_outp, ac_jtag_outn;
  boolean ss_phy_present = FALSE;
  
  //1.  Release pcieusb3phy_reset (pcieusb3phy_phy_reset can remain asserted).
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));

  //2.  Write 0x01 to PCIE_USB3_PHY_POWER_DOWN_CONTROL to bring the PHY out of powerdown mode.
  HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_OUTM(HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, (0x1 << HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));

  //3.  Write 0x01 to QSERDES_RX_AC_JTAG_RESET to enable set/reset of the ac_jtag receiver circuit.
  HWIO_USB3PHY_QSERDES_RX_AC_JTAG_RESET_OUTM(HWIO_USB3PHY_QSERDES_RX_AC_JTAG_RESET_AC_JTAG_RESET_BMSK, (0x1 << HWIO_USB3PHY_QSERDES_RX_AC_JTAG_RESET_AC_JTAG_RESET_SHFT));
  
  //4.  Write 0x02 to QSERDES_RX_AC_JTAG_INITP and 0x03 to QSERDES_RX_AC_JTAG_INITN to set/reset ac_jtag_outp/n.
  HWIO_USB3PHY_QSERDES_RX_AC_JTAG_INITP_OUT(0x02);
  HWIO_USB3PHY_QSERDES_RX_AC_JTAG_INITN_OUT(0x03);
  
  //5.  Read QSERDES_RX_AC_JTAG_OUTP (expect value is 0x01) and  QSERDES_RX_AC_JTAG_OUTN (expect value is 0x00).
  ac_jtag_outp = HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_INM(HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_BMSK) >> HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_SHFT;
  ac_jtag_outn = HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_INM(HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_BMSK) >> HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_SHFT;
  if ((ac_jtag_outp == 0x01) && (ac_jtag_outn == 0x00))
  {
    //6.  Write 0x03 to QSERDES_RX_AC_JTAG_INITP and 0x02 to QSERDES_RX_AC_JTAG_INITN to reset/set ac_jtag_outp/n.
    HWIO_USB3PHY_QSERDES_RX_AC_JTAG_INITP_OUT(0x03);
    HWIO_USB3PHY_QSERDES_RX_AC_JTAG_INITN_OUT(0x02);
   
    //7.  Read QSERDES_RX_AC_JTAG_OUTP (expect value is 0x00) and  QSERDES_RX_AC_JTAG_OUTN (expect value is 0x01).
    ac_jtag_outp = HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_INM(HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_BMSK) >> HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTP_AC_JTAG_OUTP_STATUS_SHFT;
    ac_jtag_outn = HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_INM(HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_BMSK) >> HWIO_USB3PHY_QSERDES_RX_AC_JTAG_OUTN_AC_JTAG_OUTN_STATUS_SHFT;
    if ((ac_jtag_outp == 0x00) && (ac_jtag_outn == 0x01))
    {
      ss_phy_present = TRUE;
    }
  }

  //If any of the reads at step 5 or step 7 do not match the expected values, then it can be assumed that the 0.9V analog supply is off.
  //If this is the case, then the PHY should be placed back in powerdown mode by writing 0x00 to PCIE_USB3_PHY_POWER_DOWN_CONTROL.
  if(FALSE == ss_phy_present)
  {
    HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_OUTM(HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK, (0x0 << HWIO_USB3PHY_PCIE_USB3_PCS_POWER_DOWN_CONTROL_SW_PWRDN_B_SHFT));
  }

  qusb_error_log(QUSB_SS_PHY_PRESENT, 0, ss_phy_present);
  return ss_phy_present;
}
// ===========================================================================
/**
 * @function    qusb_dci_enable_ref_clk
 * 
 * @brief   This API will enable the reference clocks for the QUSB2 PHY and 
 *          QMP PHY. Although the ref_clk is set to be enabled on PON_RESET,
 *          however if it is not, we would like to enable. The clock could be
 *          turned OFF in HLOS driver in power saving mode.
 * 
 * @param   None
 * 
 * @return  None
 * 
 */
// ===========================================================================
void qusb_dci_enable_ref_clk(void)
{
  uint32 is_ss_ref_clk_off = 0x0; 
  uint32 is_hs_ref_clk_off = 0x0;

  is_hs_ref_clk_off = (HWIO_GCC_QUSB_REF_CLK_EN_INM(HWIO_GCC_QUSB_REF_CLK_EN_CLK_OFF_BMSK) >> HWIO_GCC_QUSB_REF_CLK_EN_CLK_OFF_SHFT); 
  is_ss_ref_clk_off = (HWIO_GCC_USB_SS_REF_CLK_EN_INM(HWIO_GCC_USB_SS_REF_CLK_EN_CLK_OFF_BMSK) >> HWIO_GCC_USB_SS_REF_CLK_EN_CLK_OFF_SHFT);

  if(is_hs_ref_clk_off)
  {
    HWIO_GCC_QUSB_REF_CLK_EN_OUTM(HWIO_GCC_QUSB_REF_CLK_EN_CLK_ENABLE_BMSK, (0x1 << HWIO_GCC_QUSB_REF_CLK_EN_CLK_ENABLE_SHFT ));
  }
  if(is_ss_ref_clk_off)
  {
    HWIO_GCC_USB_SS_REF_CLK_EN_OUTM(HWIO_GCC_USB_SS_REF_CLK_EN_CLK_ENABLE_BMSK, (0x1 << HWIO_GCC_USB_SS_REF_CLK_EN_CLK_ENABLE_SHFT)); 
  }
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_hs_phy_gcc_reset
 * 
 * @brief   API used for resetting High Speed QUSB2 PHY using GCC control
 *  
 * @details This API is used for resetting High Speed QUSB2 PHY using GCC control
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
#ifndef FEATURE_FIREHOSE_QUSB
static void qusb_hs_phy_gcc_reset(void)
{
  HWIO_GCC_QUSB2_PHY_BCR_OUTM(HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_BMSK,
                             (0x1 << HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_SHFT));
  qusb_dci_delay_us(100);

  HWIO_GCC_QUSB2_PHY_BCR_OUTM(HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_BMSK,
                             (0x0 << HWIO_GCC_QUSB2_PHY_BCR_BLK_ARES_SHFT));
}
#endif /*FEATURE_FIREHOSE_QUSB*/

// ===========================================================================
/**
 * @function    qusb_dci_hs_phy_nondrive_mode_set
 * 
 * @brief        This API will set the PHY in Non Drive mode. This can be called 
 *               by PMI before running the APSD in SBL.
 *
 *               This will avoid USB PHY interferance when performing the 
 *               charger detection.
 *
 * @param   None
 * 
 * @return  None
 * 
 * @Note    This API will always be called during the boot. Now for the 
 *          Firhose case, when this API is called, it would be avoided
 *          based on the feature flag FEATURE_FIREHOSE_QUSB
 * 
 */
// ===========================================================================
void qusb_dci_hs_phy_nondrive_mode_set(void)
{
#ifndef FEATURE_FIREHOSE_QUSB
  uint32 enable_fs_xvr = 0x0;

  qusb_uart_log("usb: hs_phy_nondrive_start");

  /** Bring the controller out of Power Collapse**/
  qusb_dci_enable_usb30_power_collapse(FALSE);
 
  qusb_hs_phy_gcc_reset();
  
  Clock_InitUSB();

  qusb_dci_delay_us(10);

  // Write 0x11 to AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG so that 
  // writes and reads to/from the PHY use one wait state.
  // This is essential to operate at nominal frequency with lower CX rail voltages.
  HWIO_AHB2PHY_TOP_CFG_OUT(0x11);

  qusb_dci_delay_us(10);

  if(qusb_hs_phy_init())
  {

    //Disable the D+ Pull down 
    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_BMSK, 
                                            (0x0 <<HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_SHFT ));
    // Disable the D- Pull down 
    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
      HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_BMSK,
      (0x0 <<HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_SHFT));

    // Select the FS transceiver and put into non-drive mode
    // Putting phy to suspend as SUSPEND_N = 0 (power on default)
    enable_fs_xvr = (QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN
                   | QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS
                   | QUSB2PHY_PORT_UTMI_CTRL1_OP_NON_DRV_MODE);

    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL1_OUT(enable_fs_xvr);
 
    // Enable UTMI_TEST_MUX_SEL
    HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
      HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
      0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);
  }  

  /* Keep the PHY in power down mode */
  HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_OUTM(
    HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
    (0x1 << HWIO_QUSB2PHY_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

  qusb_dci_delay_us(10);

  Clock_DisableUSB();
  
  /** Set  the Power Collapse switch **/
  qusb_dci_enable_usb30_power_collapse(TRUE);

  qusb_uart_log("usb: hs_phy_nondrive_finish");
#endif /* FEATURE_FIREHOSE_QUSB */
}

static void qusb_ssmux_switch_gpio_init(void)
{
   DALResult                         res;
   DalDeviceHandle                  *platform_info_device;

   res = DAL_DeviceAttach(DALDEVICEID_TLMM, &qusb_ssmux_dal_info.tlmm);
   if(res != DAL_SUCCESS)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     qusb_uart_log("usb:DAL attach failed");
     return;
   }

   if(NULL == qusb_ssmux_dal_info.tlmm)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     return;
   }

   res = DalDevice_Open(qusb_ssmux_dal_info.tlmm, DAL_OPEN_SHARED);
   if(res != DAL_SUCCESS)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     qusb_uart_log("usb:DAL open failed");
     return;
   }

   qusb_ssmux_dal_info.pin_config = DAL_GPIO_CFG(
     (uint32)USB_GPIO_SSMUX_TOGGLE, 
      0, 
      DAL_GPIO_OUTPUT, 
      DAL_GPIO_NO_PULL, 
      DAL_GPIO_4MA);
    
   res = DalTlmm_ConfigGpio(qusb_ssmux_dal_info.tlmm, qusb_ssmux_dal_info.pin_config, DAL_TLMM_GPIO_ENABLE);
   
   if(res != DAL_SUCCESS)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     qusb_uart_log("usb:config gpio failed");
     return;
   }
    
   // Get the platform info 
   res = DAL_PlatformInfoDeviceAttach(DALDEVICEID_PLATFORMINFO, &platform_info_device);
   if (res != DAL_SUCCESS)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     qusb_uart_log("usb:Device attach failed");
     return;
   }
   
   res = DalPlatformInfo_GetPlatformInfo(platform_info_device, &qusb_ssmux_dal_info.platform_info);
   if (res != DAL_SUCCESS)
   {
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     qusb_uart_log("usb:platform info failed");
     return;
   }

   qusb_log(TLMM_DAL_GPIO_CFG_COMPLETE_LOG, 0, 0);
}

void qusb_dci_typec_lane_detect(void)
{
   DALResult                       res;
   DALGpioValueType                gpio_type; 
   pm_err_flag_type                pm_error; 
   uint8                           cc_retry = 0;
   pm_typec_plug_orientation_type  cc_status = PM_PLUG_OPEN;

   qusb_log(TYPC_LANE_DETECT_LOG, 0, 0);

   //Initialize the SS mux switch GIPO
   qusb_ssmux_switch_gpio_init();

   //Check for cable connect/vbus valid
   while(FALSE == qusb_dci_is_vbus_valid());

   //Wait for the PMIC CC detection to finish
   //PM should complete CC within 450ms, we wait a little longer and bail out
   while((cc_status == PM_PLUG_OPEN) && (cc_retry < QUSB_CC_DET_RETRY_CNT))  
   {
     pm_error =  pm_typec_get_plug_orientation_status(&cc_status);
     if(PM_ERR_FLAG__SUCCESS != pm_error)
     {
       qusb_uart_log("usb: PMIC CC error");
       return;
     }

     qusb_dci_delay_ms(100);
     cc_retry++;
   }

   //CC failed, skip the SS mux setting
   if(cc_status == PM_PLUG_OPEN)
   {
     qusb_log(TYPC_CC_DETECT_FAILED_LOG, 0, 0);
     qusb_uart_log("usb: PMIC CC Failed");
     return;
   }

   //Toggle the GPIO
   if(NULL != qusb_ssmux_dal_info.tlmm)
   {
     //For QRD,        GPIO->  FLIP = LOW,  UNFLIP = HIGH
     //For MTP/CDP, GPIO -> FLIP = HIGH,  UNFLIP = LOW
     if(qusb_ssmux_dal_info.platform_info.platform == DALPLATFORMINFO_TYPE_QRD)
     {
       gpio_type = (cc_status == PM_PLUG_FLIP) ? DAL_GPIO_LOW_VALUE : DAL_GPIO_HIGH_VALUE;
     }
     else
     {
       gpio_type = (cc_status == PM_PLUG_FLIP) ? DAL_GPIO_HIGH_VALUE : DAL_GPIO_LOW_VALUE;
     }
    
     res = DalTlmm_GpioOut(qusb_ssmux_dal_info.tlmm,
                           qusb_ssmux_dal_info.pin_config, gpio_type);
     if(res != DAL_SUCCESS)
     {
       qusb_uart_log("usb:GpioOut failed");
       qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
     }
   }
   else
   {
     qusb_uart_log("usb:No Tllm Handle");
     qusb_log(TLMM_DAL_FAIL_LOG, 0, __LINE__);
   } 

   qusb_log(TYPC_LANE_DETECT_COMPLETE_LOG, 0, 0);
}
//============================================================================
/**
 * @function  qhsusb_dci_read_serial_number
 *
 * @brief     Gets serial number from emmc
 *
 * @Note :    This API needs to get the serial number from MSM for UFS.
 *            Currently this API just supports getting for EMMC
 *
 * @param     None
 *
 * @return    uint32 A 32-Bit serial number
 *
 *
 */
//============================================================================
uint32 qusb_dci_read_serial_number(void)
{
  struct hotplug_device *hdev;
  struct hotplug_device_info dev_info;
  const struct hotplug_guid sbl1_hotplug_id =
      /*{DEA0BA2C-CBDD-4805-B4F9-F428251C3E98}*/
  {0xDEA0BA2C, 0xCBDD, 0x4805, { 0xB4, 0xF9, 0xF4, 0x28, 0x25, 0x1C, 0x3E, 0x98}};
  uint32 serial_number = 0;
  
  //For bear family targets, the supported type is only MMC_FLASH
  hdev = hotplug_open_device_by_gpt_partition_type(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EMBEDDED_DEVICES_ONLY, &sbl1_hotplug_id);
  if(NULL == hdev)
  {
    qusb_log(QUSB_DCI_HOTPLUG_FAILURE_LOG, 0, __LINE__);
    return 0;
  }
  if(FS_HOTPLUG_SUCCESS == hotplug_dev_get_device_info(hdev, &dev_info))
  {
    serial_number = dev_info.prod_serial_num;
  }
  else
  {
    qusb_log(QUSB_DCI_HOTPLUG_FAILURE_LOG, 0 , __LINE__);
    return 0;
  }
  qusb_log(QUSB_DCI_SERIAL_NUMBER_LOG, 0, serial_number);
  return serial_number;
}


