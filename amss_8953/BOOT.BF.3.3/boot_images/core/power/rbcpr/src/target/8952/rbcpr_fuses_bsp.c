/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "rbcpr_fuses.h"
#include "rbcpr_qfprom.h"
#include "HALhwio.h"

//TBD to get the fuses mapping 
const rbcpr_bsp_fuse_config_t rbcpr_bsp_fuse_config =
{
    .rail_fuse_config = (rbcpr_bsp_fuse_rail_config_t[])
    {    
        {   //Vdd_Mx
            .rail_name = "vddmx",
            .number_of_fuses = 5,
            .individual_fuse_configs = (rbcpr_bsp_individual_fuse_config_t[])
            {
			    {
				    // SVS fuse not defined , but railway corner exist
                    .fuse_type = RBCPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR2_TARG_VOLT_SVSPLUS)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 7, 
                               .mask           = 0xF80, 
                            }
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_SVS_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR2_TARG_VOLT_SVSPLUS)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 7, 
                               .mask           = 0xF80, 
                            }							
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR2_TARG_VOLT_NOM_4_3),
							RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR2_TARG_VOLT_NOM_2_0)
                        },
                },
				{
                    .fuse_type = RBCPR_FUSE_NOMINAL_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR2_TARG_VOLT_NOMPLUS)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 2, 
                               .mask           = 0x7c, 
                            }
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR2_TARG_VOLT_TURBO)
                        },
                },
            },
        },    
        {   //Vdd_Cx
            .rail_name = "vddcx",
            .number_of_fuses = 5,
            .individual_fuse_configs = (rbcpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = RBCPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_SVS)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 0x11, 
                               .mask           = 0x3E0000, 
                            }
                        },
                },
				{
                    .fuse_type = RBCPR_FUSE_SVS_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_SVS)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 0x11, 
                               .mask           = 0x3E0000, 
                            }
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_NOM)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 0xC, 
                               .mask           = 0x1F000, 
                            }
                        },
                },
				{
                    .fuse_type = RBCPR_FUSE_NOMINAL_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            //RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_NOM)
							{   
                               .fuse_address   = HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 
                               .offset         = 0xC, 
                               .mask           = 0x1F000, 
                            }
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW2_LSB, CPR0_TARG_VOLT_TUR)
                        },
                },
            },
        },
		{   //Vdd_Mss
            .rail_name = "vddmss",
            .number_of_fuses = 5,
            .individual_fuse_configs = (rbcpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = RBCPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_LSB, CPR1_TARG_VOLT_SVS_4),
							RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR1_TARG_VOLT_SVS_3_0)
						},
                },
				{
                    .fuse_type = RBCPR_FUSE_SVS_PLUS,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_LSB, CPR1_TARG_VOLT_SVS_4),
 						    RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR1_TARG_VOLT_SVS_3_0)
						},
                },
                {
                    .fuse_type = RBCPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR1_TARG_VOLT_NOM)
						},
                },
				{
                    .fuse_type = RBCPR_FUSE_NOMINAL_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR1_TARG_VOLT_NOM)
                        },
                },
                {
                    .fuse_type = RBCPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const rbpcr_bsp_part_individual_fuse_config_t[])
                        {
                            RBCPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR1_TARG_VOLT_TUR)
                        },
                },
            },
        },
    },
    .number_of_fused_rails = 3,
};
