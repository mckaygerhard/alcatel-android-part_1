/*===========================================================================
  	
  	FILE:         rbcpr_bsp.c


	DESCRIPTION:  Per target rbcpr bsp configurations
  	
=============================================================================
	
Edit History

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/rbcpr/src/target/8952/rbcpr_bsp.c#7 $
$Date: 2015/11/25 $
	 
when       who     what, where, why
--------   ---     --------------------------------------------------------
  
===========================================================================
Copyright (c) 2014 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary and Confidential.
===========================================================================*/

 /* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "rbcpr.h"
#include "railway.h"
#include "rbcpr_fuses.h"
#include "CoreVerify.h"


#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))
void rbcpr_pin_mx_voltage_to_global_ceiling(rbcpr_corner_params_type* mx_corner_params, struct rbcpr_bsp_rail_info* mx_rail);

rbcpr_bsp_rail_params_type gf_tn3_cpr_settings[] =
{
     {   // VDDMX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 1050000,
		            },
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 1150000,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1062500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1137500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1187500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_OPEN_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
        .number_of_corners_to_settle_at = 1,
    },
    {   // VDDCX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .rosc_target[0] = 228,
		                .rosc_target[1] = 276,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 259,
		                .rosc_target[5] = 213,
		                .rosc_target[6] = 413,
		                .rosc_target[7] = 370,
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 900000,
						.static_margin  = 50000,
                        .static_margin_adjust = 0,
		            },
                },
				{
                .corner_params =
		            {
		                .rosc_target[0] = 330,
		                .rosc_target[1] = 381,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 356,
		                .rosc_target[5] = 306,
		                .rosc_target[6] = 531,
		                .rosc_target[7] = 485,
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 975000,
						.static_margin  = 37500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 408,
		                .rosc_target[1] = 457,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 426,
		                .rosc_target[5] = 377,
		                .rosc_target[6] = 612,
		                .rosc_target[7] = 563,
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1037500,
						.static_margin  = 25000,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
					{
		                .rosc_target[0] = 463,
		                .rosc_target[1] = 511,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 474,
		                .rosc_target[5] = 429,
		                .rosc_target[6] = 668,
		                .rosc_target[7] = 619,
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1075000,
						.static_margin  = 12500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 527,
		                .rosc_target[1] = 574,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 532,
		                .rosc_target[5] = 488,
		                .rosc_target[6] = 735,
		                .rosc_target[7] = 684,
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1125000,
						.static_margin  = 12500,
                        .static_margin_adjust = 0,
		            },
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_CLOSED_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
		.voltage_scaling_vector = 
		    {
			    1310,
			    1280,
			    0,
			    0,
			    1170,
			    1200,
			    1360,
			    1320,
			},
        .number_of_corners_to_settle_at = 1,
    },
};
rbcpr_bsp_rail_params_type tsmc_tn3_cpr_settings[] =
{
     {   // VDDMX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 1050000,
		            },
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 1150000,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1062500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1137500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1187500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_OPEN_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
        .number_of_corners_to_settle_at = 1,
    },
    {   // VDDCX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .rosc_target[0] = 280,
		                .rosc_target[1] = 286,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 227,
		                .rosc_target[5] = 245,
		                .rosc_target[6] = 401,
		                .rosc_target[7] = 413,
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 900000,
                        .static_margin  = 62500,
                        .static_margin_adjust = 0,
		            },
                },
				{
                .corner_params =
		            {
		                .rosc_target[0] = 412,
		                .rosc_target[1] = 415,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 342,
		                .rosc_target[5] = 360,
		                .rosc_target[6] = 541,
		                .rosc_target[7] = 544,
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 975000,
						.static_margin  = 37500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 507,
		                .rosc_target[1] = 508,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 427,
		                .rosc_target[5] = 442,
		                .rosc_target[6] = 637,
		                .rosc_target[7] = 633,
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1037500,
						.static_margin  = 87500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
					{
		                .rosc_target[0] = 573,
		                .rosc_target[1] = 571,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 486,
		                .rosc_target[5] = 499,
		                .rosc_target[6] = 702,
		                .rosc_target[7] = 692,
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1075000,
						.static_margin  = 87500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 656,
		                .rosc_target[1] = 651,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 561,
		                .rosc_target[5] = 570,
		                .rosc_target[6] = 783,
		                .rosc_target[7] = 765,
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1125000,
						.static_margin  = 75000,
                        .static_margin_adjust = 0,
		            },
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_CLOSED_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
		.voltage_scaling_vector = 
		    {
			    1280,
			    1280,
			       0,
			       0,
			    1120,
			    1120,
			    1440,
			    1360,
			},
        .number_of_corners_to_settle_at = 1,
    },
};




rbcpr_bsp_rail_params_type tsmc_tn3_cpr_settings_v2[] =
{
     {   // VDDMX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 1050000,
		            },
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 1150000,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1062500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
				{
                .corner_params =
		            {
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1137500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
                {
                .corner_params =
		            {
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1187500,
		            },
				.fuse_function = rbcpr_pin_mx_voltage_to_global_ceiling,
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_OPEN_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
        .number_of_corners_to_settle_at = 1,
    },
    {   // VDDCX
        .target_params = (rbcpr_corner_params_and_fuse_type[])
            {
                {
                .corner_params =
		            {
		                .rosc_target[0] = 280,
		                .rosc_target[1] = 286,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 227,
		                .rosc_target[5] = 245,
		                .rosc_target[6] = 401,
		                .rosc_target[7] = 413,
		                .corner         = RAILWAY_SVS_SOC,
		                .fuse           = RBCPR_FUSE_SVS,
		                .voltage_ceil   = 1050000,
		                .voltage_floor  = 900000,
                        .static_margin  = 25000,
                        .static_margin_adjust = 0,
		            },
                },
				{
                .corner_params =
		            {
		                .rosc_target[0] = 412,
		                .rosc_target[1] = 415,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 342,
		                .rosc_target[5] = 360,
		                .rosc_target[6] = 541,
		                .rosc_target[7] = 544,
		                .corner         = RAILWAY_SVS_HIGH,
		                .fuse           = RBCPR_FUSE_SVS_PLUS,
		                .voltage_ceil   = 1150000,
		                .voltage_floor  = 975000,
						.static_margin  = 0,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 507,
		                .rosc_target[1] = 508,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 427,
		                .rosc_target[5] = 442,
		                .rosc_target[6] = 637,
		                .rosc_target[7] = 633,
		                .corner         = RAILWAY_NOMINAL,
		                .fuse           = RBCPR_FUSE_NOMINAL,
		                .voltage_ceil   = 1225000,
		                .voltage_floor  = 1037500,
						.static_margin  = 12500,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
					{
		                .rosc_target[0] = 573,
		                .rosc_target[1] = 571,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 486,
		                .rosc_target[5] = 499,
		                .rosc_target[6] = 702,
		                .rosc_target[7] = 692,
		                .corner         = RAILWAY_NOMINAL_HIGH,
		                .fuse           = RBCPR_FUSE_NOMINAL_PLUS,
		                .voltage_ceil   = 1287500,
		                .voltage_floor  = 1075000,
						.static_margin  = 0,
                        .static_margin_adjust = 0,
		            },
                },
                {
                .corner_params =
		            {
		                .rosc_target[0] = 656,
		                .rosc_target[1] = 651,
		                .rosc_target[2] = 0,
		                .rosc_target[3] = 0,
		                .rosc_target[4] = 561,
		                .rosc_target[5] = 570,
		                .rosc_target[6] = 783,
		                .rosc_target[7] = 765,
		                .corner         = RAILWAY_SUPER_TURBO,
		                .fuse           = RBCPR_FUSE_TURBO,
		                .voltage_ceil   = 1350000,
		                .voltage_floor  = 1125000,
						.static_margin  = 0,
                        .static_margin_adjust = 0,
		            },
                },
            },
        .rbcpr_enablement=RBCPR_ENABLED_CLOSED_LOOP,
        .number_of_target_params=5,
        .corners_to_settle_at = (railway_corner[])
            {
                RAILWAY_SUPER_TURBO,
            },
		.voltage_scaling_vector = 
		    {
			    1280,
			    1280,
			       0,
			       0,
			    1120,
			    1120,
			    1440,
			    1360,
			},
        .number_of_corners_to_settle_at = 1,
    },
};

rbcpr_bsp_revision_array_type hw_version_specific_settings_8952 =
{
    .hw_version_count = 2,
    .hw_version_array = (rbcpr_versioned_bsp_rail_params_type[])
    {
		{
		    .fab_process_id_count = 2,
    		.rbcpr_bsp_params_fab = (rbcpr_fab_id_settings[])
            {
                {
                    .bsp_params = (rbcpr_bsp_rail_params_type *)&gf_tn3_cpr_settings,
                    .process_id = 0,
				    .foundry_id = 1,
                },
                {
                    .bsp_params = (rbcpr_bsp_rail_params_type *)&tsmc_tn3_cpr_settings,
                    .process_id = 0,
				    .foundry_id = 0,
                },
		    },
		    .supported_hw_after_version= DALCHIPINFO_VERSION(1, 0), 
        },

		{
		    .fab_process_id_count = 2,
    		.rbcpr_bsp_params_fab = (rbcpr_fab_id_settings[])
            {
                {
                    .bsp_params = (rbcpr_bsp_rail_params_type *)&gf_tn3_cpr_settings,
                    .process_id = 0,
				    .foundry_id = 1,
                },
                {
                    .bsp_params = (rbcpr_bsp_rail_params_type *)&tsmc_tn3_cpr_settings_v2,
                    .process_id = 0,
				    .foundry_id = 0,
                },
		    },
		    .supported_hw_after_version= DALCHIPINFO_VERSION(1, 1), 
        },
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8952,
};


const rbcpr_supported_hw_array_type rbcpr_supported_hw_array =
{
    .hw_version_specific_settings = (rbcpr_bsp_revision_array_type*[])
    {
        &hw_version_specific_settings_8952,
    },
    .hw_version_specific_settings_count = 1,
};

const rbcpr_bsp_rail_const_info rbcpr_rail_const_info[] =
{
    {   //VddMx
    .rail_name                      = "vddmx",
    .pmic_step_size_uv              = 12500, 
    .efuse_target_voltage_multipler = 10000,
    },
    {   //VddCx
    .rail_name                           = "vddcx",
    .step_quot                           = 26,
    .pmic_step_size_uv                   = 12500,
    .up_threshold                        = 0,
    .dn_threshold                        = 2,
    .consecutive_up                      = 0,
    .consecutive_dn                      = 2,
	.sensors_to_mask                     = (const uint8 []){19,20},
    .number_of_sensors_to_mask           = 2,
    .gcnt                                = 19,
    .count_mode                          = HAL_RBCPR_STAGGERED,
    .idle_clocks                         = 15,
    .efuse_target_voltage_multipler      = 10000,
    },
};

rbcpr_bsp_type rbcpr_bsp_data = 
{
    .rails = (rbcpr_bsp_rail_info[])
    {
	    {   //VddMx
             .rail_const_info = &rbcpr_rail_const_info[0],
        },
        {   //VddCx
             .rail_const_info = &rbcpr_rail_const_info[1],
        },
    },
    .num_rails = 2,
};

static rbcpr_checksum_config_t rbcpr_checksum_configs[] =
{
    /* MX */
    {
        .rail_name = "vddmx",
    },
    /* CX */
    {
        .rail_name = "vddcx",
    },
};

rbcpr_checksum_config_arr_t rbcpr_checksum_config_data =
{
    .configs     = rbcpr_checksum_configs,
    .num_configs = sizeof(rbcpr_checksum_configs)/sizeof(rbcpr_checksum_configs[0]),
};


uint32 rbcpr_open_loop_ceiling(const rbcpr_bsp_rail_info* rail_info, rbcpr_fuse_type fuse, int voltage_offset_steps )
{
    rbcpr_corner_params_type* corner_params = NULL;

    const int32 voltage_offset = voltage_offset_steps * rail_info->rail_const_info->efuse_target_voltage_multipler;
    
    for(int i=0; i<rail_info->bsp_data->number_of_target_params; i++)
    {
        if(rail_info->bsp_data->target_params[i].corner_params.fuse == fuse)
        {
           corner_params = &rail_info->bsp_data->target_params[i].corner_params;
		   break;
        }
    }

    CORE_VERIFY(corner_params);
	
	uint32 open_loop_ceiling = voltage_offset + corner_params->voltage_ceil;
	
	open_loop_ceiling = MIN(corner_params->voltage_ceil, MAX(open_loop_ceiling, corner_params->voltage_floor));
	
    //Now round up to the PMIC step size.
    if(open_loop_ceiling % rail_info->rail_const_info->pmic_step_size_uv)
    {
        open_loop_ceiling += (rail_info->rail_const_info->pmic_step_size_uv - (open_loop_ceiling % rail_info->rail_const_info->pmic_step_size_uv));
    }
    
    CORE_VERIFY((corner_params->voltage_ceil % rail_info->rail_const_info->pmic_step_size_uv) == 0);      //This should always be true if the config is correct.
    
    open_loop_ceiling = MIN(corner_params->voltage_ceil, open_loop_ceiling);
    
    return open_loop_ceiling;
}


void rbcpr_pin_mx_voltage_to_global_ceiling(rbcpr_corner_params_type* mx_corner_params, struct rbcpr_bsp_rail_info* mx_rail)
{
    uint32 global_open_loop_ceiling = 0;
    int cx_voltage_offset_steps=0, mx_voltage_offset_steps=0, mss_voltage_offset_steps=0;
    rbcpr_bsp_rail_info* cx_rail = &rbcpr_bsp_data.rails[1];

    // read CX open LOOP fuse recommondation	
    cx_voltage_offset_steps = rbcpr_fuses_get_fuse_value("vddcx", mx_corner_params->fuse);

    // read MX open LOOP fuse recommondation	
    mx_voltage_offset_steps = rbcpr_fuses_get_fuse_value("vddmx", mx_corner_params->fuse);  
		
    // read MX open LOOP fuse recommondation	
    mss_voltage_offset_steps = rbcpr_fuses_get_fuse_value("vddmss", mx_corner_params->fuse);  
	
    cx_voltage_offset_steps = MAX(cx_voltage_offset_steps, mss_voltage_offset_steps);
    global_open_loop_ceiling = rbcpr_open_loop_ceiling(cx_rail, mx_corner_params->fuse, cx_voltage_offset_steps);
    
    global_open_loop_ceiling = MAX(global_open_loop_ceiling, rbcpr_open_loop_ceiling(mx_rail, mx_corner_params->fuse, mx_voltage_offset_steps));
	
    //Now round global_open_loop_ceiling up to the PMIC step size.
    if(global_open_loop_ceiling % mx_rail->rail_const_info->pmic_step_size_uv)
    {
        global_open_loop_ceiling += (mx_rail->rail_const_info->pmic_step_size_uv - (global_open_loop_ceiling % mx_rail->rail_const_info->pmic_step_size_uv));
    }
    
    CORE_VERIFY(global_open_loop_ceiling <= mx_corner_params->voltage_ceil);
    
    railway_set_corner_voltage(mx_rail->railway_rail_id, mx_corner_params->corner, global_open_loop_ceiling);

    mx_corner_params->voltage_ceil = global_open_loop_ceiling;
}


/*===========================================================================
FUNCTION: rbcpr_init_rosc_target

DESCRIPTION: initializes the rosc_target values based on static margin if closed loop

RETURN VALUE: None
===========================================================================*/
void rbcpr_bsp_init_rosc_target(rbcpr_bsp_rail_info* rail)
{
    if (rail->bsp_data->rbcpr_enablement != RBCPR_ENABLED_CLOSED_LOOP)
	    return;
	
    for (int i=0; i < rail->bsp_data->number_of_target_params; i++)	
    {
		for (int j=0; j < RBCPR_ROSC_COUNT; j++)
		{
			// divide it by 1000000, because voltage delta is in uv and voltage scaling factor (kv) is also multiplied by 1000 to avoid floating point arthmatic
			int32 calc_value = ((rail->bsp_data->target_params[i].corner_params.static_margin) * (rail->bsp_data->voltage_scaling_vector[j]));
            calc_value = (int32)calc_value/1000000;
			
			// adjust precision from above calculation
			rail->bsp_data->target_params[i].corner_params.rosc_target[j] +=  calc_value + rail->bsp_data->target_params[i].corner_params.static_margin_adjust;
			
		}
    }
}






