/**
 * @file:  cpr_target_hwio.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/15 23:28:14 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/target/8953/cpr_target_hwio.h#1 $
 * $Change: 9594144 $
 *
 */
#ifndef CPR_TARGET_HWIO_H
#define CPR_TARGET_HWIO_H

#include "msmhwiobase.h"
#include "cpr_hwio.h"
#include "cpr_fuses_hwio.h"

#endif /* CPR_TARGET_HWIO_H */
