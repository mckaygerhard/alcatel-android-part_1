/**
 * @file:  cpr_isr.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/15 23:28:14 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/common/src/cpr_isr.c#1 $
 * $Change: 9594144 $
 */
#include "cpr_cfg.h"
#include "cpr_enablement.h"
#include "cpr_rail.h"

uint32 cpr_isr_get_interrupt(cpr_domain_id railId)
{
    cpr_rail* rail = cpr_get_rail( railId );
    return rail->interruptId;
}

void cpr_isr_process(cpr_domain_id railId)
{
    cpr_rail_isr( cpr_get_rail( railId ) );
}
