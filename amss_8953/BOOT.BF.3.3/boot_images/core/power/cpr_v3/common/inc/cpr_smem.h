/**
 * @file:  cpr_smem.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 01:39:35 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/common/inc/cpr_smem.h#2 $
 * $Change: 9714790 $
 */
#ifndef CPR_SMEM_H
#define CPR_SMEM_H

#include "cpr_cfg.h"
#include "cpr_data.h"

void cpr_smem_deserialize_config(cpr_rail* rail, cpr_rail_state* state);

//append incidicates to the driver if we are starting at the beginning of the smem section or adding on to prior sections
void cpr_smem_serialize_config(cpr_rail_state* state, boolean append);

#endif
