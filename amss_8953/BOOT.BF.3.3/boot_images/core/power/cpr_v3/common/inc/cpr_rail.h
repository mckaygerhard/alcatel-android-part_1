/**
 * @file:  cpr_rail.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/18 01:21:37 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/common/inc/cpr_rail.h#2 $
 * $Change: 10096987 $
 */
#ifndef CPR_RAIL_H
#define	CPR_RAIL_H

#include "cpr_data.h"
#include "cpr_cfg.h"

void cpr_rail_init(cpr_rail* rail);
boolean cpr_rail_process_results(cpr_rail* rail, cpr_results* rslts);
void cpr_rail_update_target_quotients(cpr_rail* rail, cpr_rail_state* railState);
void cpr_rail_set_initial_voltages(cpr_rail* rail, boolean useFloorFuses, boolean useCeilingFuses);
void cpr_rail_enable(cpr_rail* rail, cpr_mode_settings* modeState, cpr_submode_settings* submodeState, boolean changeOveride);
void cpr_rail_disable(cpr_rail* rail);
void cpr_rail_register_isr(cpr_rail* rail);
void cpr_rail_isr(void* ctx);

#endif

