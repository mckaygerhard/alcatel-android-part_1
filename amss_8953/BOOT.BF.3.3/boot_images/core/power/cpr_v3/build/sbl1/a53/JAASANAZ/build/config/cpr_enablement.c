#include "cpr_enablement.h"
#include "cpr_image_target_init.h"
#include "cpr_cfg.h"
#include "cpr_target_hwio.h"
#include "cpr_logs.h"

//hash value of enablement.cfg
const char cpr_enablement_cfg_hash_value[] = "07DD1376B9C6DD512CC9C59A5FDA8158";


static cpr_controller cpr3_controller = {
    .hal      = { .base = 0x00048000 },
    .refClk  = "gcc_rbcpr_clk",
    .ahbClk  = "gcc_rbcpr_ahb_clk",
    .stepQuotMin = 14,
    .stepQuotMax = 17,
    .bypassSensors       = NULL,
    .bypassSensorsCount  = 0,
    .disableSensors      = (uint8[]){ 19,20,24,25,28,29 },
    .disableSensorsCount = 6,
};


static cpr_rail cpr_rail_cx_cfg_v3 = {
    .id  = CPR_RAIL_CX,
    .name = "CX",
    .hal = { .base = 0x00048000, .thread = 0, .type = CPR_CONTROLLER_TYPE_SW_CL_ONLY },
    .interruptId = 62,
    .halRailCfg = {
        .upThresh = 0,
        .dnThresh = 2,
        .consecUp = 0,
        .consecDn = 2,
        .verCfg.v3 = { .sensors = NULL, .sensorsCount = 0 }
    },
    .settleModes      = (cpr_voltage_mode[]) { CPR_VOLTAGE_MODE_SUPER_TURBO, CPR_VOLTAGE_MODE_NOMINAL_L1, CPR_VOLTAGE_MODE_NOMINAL },
    .settleModesCount = 3,
};


static cpr_rail cpr_rail_mx_cfg_v3 = {
    .id  = CPR_RAIL_MX,
    .name = "MX",
};


static cpr_enablement cpr_rail_cx_enablement_0 =
{
    .id       = CPR_RAIL_CX,
    .funcs    = &CPR_INIT_CLOSED_LOOP_SETTLE_ONLY,
    .versions = { .versions = (cpr_version[]){ {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(1,0), CPR_CHIPINFO_VERSION(255,255)} },
                  .count = 1 },
    .dynamicfloor       = true,
    .enabletempadj      = false,
    .testMargin         = 0,
    .fuseMultiplier     = 10000,
    .stepSize           = 8000,
    .thermalAdjustment  = { 0, 0, 0, 0 },
};


static cpr_enablement cpr_rail_mx_enablement_0 =
{
    .id       = CPR_RAIL_MX,
    .funcs    = &CPR_INIT_OPEN_LOOP,
    .versions = { .versions = (cpr_version[]){ {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(1,0), CPR_CHIPINFO_VERSION(255,255)} },
                  .count = 1 },
    .dynamicfloor       = false,
    .enabletempadj      = false,
    .testMargin         = 0,
    .fuseMultiplier     = 10000,
    .stepSize           = 8000,
    .thermalAdjustment  = { 0, 0, 0, 0 },
};

cpr_rail* cpr_get_rail(cpr_domain_id railId)
{
    switch(railId)
    {
        case CPR_RAIL_CX: return &cpr_rail_cx_cfg_v3;
        case CPR_RAIL_MX: return &cpr_rail_mx_cfg_v3;
        default:
            CPR_LOG_FATAL("Unsupported railId 0x%x", railId);
            break;
    }

    return NULL;
}

uint32 cpr_get_rail_idx(cpr_domain_id railId)
{
    switch(railId)
    {
        case CPR_RAIL_CX: return 0;
        case CPR_RAIL_MX: return 1;
        default:
            CPR_LOG_FATAL("Unsupported railId 0x%x", railId);
            break;
    }

    return 0;
}

cpr_rail* cpr_rails[] = { &cpr_rail_cx_cfg_v3, &cpr_rail_mx_cfg_v3 };
cpr_controller* cpr_controllers[] = { &cpr3_controller };
cpr_enablement* cpr_enablements[] = { &cpr_rail_cx_enablement_0, &cpr_rail_mx_enablement_0 };

cpr_misc_cfg cpr_misc = {
    .cprRev = {.count = 1, .data = (struct raw_fuse_data[]) {  { HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR_GLOBAL_RC_SHFT, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR_GLOBAL_RC_BMSK } } }
};

