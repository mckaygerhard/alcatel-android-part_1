/**
 * @file:  cpr_image_init.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/08/18 01:42:32 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/target/8953/cpr_image_target_init.c#4 $
 * $Change: 11173862 $
 */
#include <string.h>
#include "cpr_logs.h"
#include "cpr_data.h"
#include "cpr_rail.h"
#include "cpr_smem.h"
#include "cpr_image.h"
#include "cpr_measurements.h"
#include "cpr_cfg.h"
#include "cpr_hal.h"
#include "cpr_utils.h"
#include "cpr_image_target_init.h"

#define HWIO_QFPROM_RAW_PTE1_VENUS_UPLIFT_BMSK                                                         0x800000
#define HWIO_QFPROM_RAW_PTE1_VENUS_UPLIFT_SHFT                                                         0x17

//******************************************************************************
// Local Helper Functions
//******************************************************************************

extern void cpr_hal_write_temp_margin_init_registers(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement);

static cpr_cfg_funcs* init_open_loop(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Initializing open loop on %s", rail->name );
    cpr_rail_set_initial_voltages( rail, false, false );
    return NULL;
}

static cpr_cfg_funcs* cpr_init_settle_only(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Settling on %s", rail->name );
    /*
     * Set the SDELTA table up for the specific rail. Assume we are at turbo mode to start.
     */
    // Temperature adjustment feature init for CPR4.
    cpr_hal_write_temp_margin_init_registers(rail->vp->modesCount-1, &rail->hal, rail->vp, rail->enablement);
    
    for(int i = 0; i < rail->settleModesCount; i++) {
        cpr_measurements_settle( rail, rail->settleModes[i] );
    }

    return NULL;
}

static cpr_cfg_funcs* init_closed_loop_and_settle(cpr_rail* rail)
{
    init_open_loop(rail);

    CPR_LOG_TRACE( "Configuring closed loop on %s", rail->name );

    cpr_rail_update_target_quotients( rail, &cpr_info.railStates[rail->railIdx] );
    cpr_hal_init_rail_hw( &rail->hal, &rail->halRailCfg );

    cpr_init_settle_only(rail);

    return NULL;
}

static cpr_cfg_funcs* custom_turbo_static_margin(cpr_rail* rail)
{
    cpr_fuse venus_fmax_uplift = {.count = 1, .data = (struct raw_fuse_data[]) {  { HWIO_QFPROM_RAW_PTE1_ADDR, HWIO_QFPROM_RAW_PTE1_VENUS_UPLIFT_SHFT , HWIO_QFPROM_RAW_PTE1_VENUS_UPLIFT_BMSK } } };
    if (cpr_utils_decode_fuse_value(&venus_fmax_uplift, 1, false))
    {
        //Adding 25 mV static margin only for Turbo corner for Venus Fmax uplift
        cpr_voltage_plan *vp = rail->vp;
        uint32 modeIdx = rail->vp->idxLookupFunc( CPR_VOLTAGE_MODE_SUPER_TURBO );

        cpr_margin_data* marginData = cpr_utils_get_margins( vp->modes[modeIdx].margins );
        marginData->closedLoop += 25;
    }
    return NULL;
}

//******************************************************************************
// Default Enablement Structures
//******************************************************************************

cpr_cfg_funcs CPR_INIT_NONE           = {.cMode = CPR_CONTROL_NONE,           .init = NULL,             .enable = NULL, .custom_fn = NULL};
cpr_cfg_funcs CPR_INIT_OPEN_LOOP      = {.cMode = CPR_CONTROL_OPEN_LOOP,      .init = init_open_loop,   .enable = NULL, .custom_fn = NULL};
cpr_cfg_funcs CPR_INIT_CLOSED_LOOP_SETTLE_ONLY    = {.cMode = CPR_CONTROL_SW_CLOSED_LOOP, .init = init_closed_loop_and_settle, .enable = NULL, .custom_fn = custom_turbo_static_margin};
