/**
 * @file:  cpr_image_defs.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/15 23:28:14 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/inc/cpr_image_defs.h#1 $
 * $Change: 9594144 $
 */
#ifndef CPR_IMAGE_DEFS_H
#define	CPR_IMAGE_DEFS_H

#include "HALhwio.h"
#include "DDIChipInfo.h"
#include "CoreVerify.h"
#include "msmhwiobase.h"


#define CPR_CHIPINFO_VERSION DALCHIPINFO_VERSION
#define CPR_ASSERT(x) CORE_VERIFY(x)

#endif

