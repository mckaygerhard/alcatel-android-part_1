#include "cpr_image_target.h"
#include "cpr_logs.h"


railway_corner cpr_image_target_mode_to_corner(cpr_voltage_mode mode)
{
    railway_corner corner = RAILWAY_NO_REQUEST;

    switch(mode)
    {
        case CPR_VOLTAGE_MODE_OFF               : corner = RAILWAY_NO_REQUEST;         break;
        case CPR_VOLTAGE_MODE_RETENTION         : corner = RAILWAY_RETENTION;          break;
        case CPR_VOLTAGE_MODE_LOW_SVS           : corner = RAILWAY_SVS_LOW;            break;
        case CPR_VOLTAGE_MODE_SVS               : corner = RAILWAY_SVS_SOC;            break;
        case CPR_VOLTAGE_MODE_SVS_L1            : corner = RAILWAY_SVS_HIGH;           break;
        case CPR_VOLTAGE_MODE_NOMINAL           : corner = RAILWAY_NOMINAL;            break;
		case CPR_VOLTAGE_MODE_NOMINAL_L1        : corner = RAILWAY_NOMINAL_HIGH;       break;        
        case CPR_VOLTAGE_MODE_TURBO             : corner = RAILWAY_TURBO;              break;
        case CPR_VOLTAGE_MODE_SUPER_TURBO       : corner = RAILWAY_SUPER_TURBO;        break;
        case CPR_VOLTAGE_MODE_SUPER_TURBO_NO_CPR: corner = RAILWAY_SUPER_TURBO_NO_CPR; break;
        default:
            CPR_LOG_FATAL("Railway does not support %u mode",mode); break;
    }

    return corner;
}

cpr_voltage_mode cpr_image_target_corner_to_mode(railway_corner corner)
{
    cpr_voltage_mode mode = CPR_VOLTAGE_MODE_OFF;

    switch(corner)
    {
        case RAILWAY_NO_REQUEST        : mode = CPR_VOLTAGE_MODE_OFF;                break;
        case RAILWAY_RETENTION         : mode = CPR_VOLTAGE_MODE_RETENTION;          break;
        case RAILWAY_SVS_LOW           : mode = CPR_VOLTAGE_MODE_LOW_SVS;            break;
        case RAILWAY_SVS_SOC           : mode = CPR_VOLTAGE_MODE_SVS;                break;
        case RAILWAY_SVS_HIGH          : mode = CPR_VOLTAGE_MODE_SVS_L1;             break;
        case RAILWAY_NOMINAL           : mode = CPR_VOLTAGE_MODE_NOMINAL;            break;
		case RAILWAY_NOMINAL_HIGH      : mode = CPR_VOLTAGE_MODE_NOMINAL_L1;         break;
        case RAILWAY_TURBO             : mode = CPR_VOLTAGE_MODE_TURBO;              break;
        case RAILWAY_SUPER_TURBO       : mode = CPR_VOLTAGE_MODE_SUPER_TURBO;        break;
        case RAILWAY_SUPER_TURBO_NO_CPR: mode = CPR_VOLTAGE_MODE_SUPER_TURBO_NO_CPR; break;
        default:
            CPR_LOG_FATAL("CPR does not support %u corner",corner); break;
    }

    return mode;
}

