/**
 * @file:  cpr_image_logs.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 01:39:35 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/inc/cpr_image_logs.h#2 $
 * $Change: 9714790 $
 */
#ifndef CPR_IMAGE_LOGS_H
#define CPR_IMAGE_LOGS_H

#include <stdio.h>
#include <stdbool.h>
#include "CoreVerify.h"
#include "boot_logger.h"

#define CPR_LOG_SIZE 256

extern char cpr_image_log_buf[CPR_LOG_SIZE];


#define CPR_IMAGE_LOG_INIT() //ULogFront_RealTimeInit(&cprLogHandle, \
                                                    CPR_LOG_NAME, \
                                                    CPR_LOG_SIZE, \
                                                    ULOG_MEMORY_LOCAL, \
                                                    ULOG_LOCK_OS)

#define GET_COUNT_MACRO(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,COUNT,...) COUNT
#define DO_ULOG(...) //ULogFront_RealTimePrintf(cprLogHandle, GET_COUNT_MACRO(__VA_ARGS__,10,9,8,7,6,5,4,3,2,1,0), __VA_ARGS__)

#define CPR_IMAGE_LOG_FATAL(msg, args...)     //do { DO_ULOG("Fatal: "msg, ##args); CPR_ASSERT(false); } while(false)
#define CPR_IMAGE_LOG_ERROR(msg, args...)     //DO_ULOG("Error: "msg, ##args)
#define CPR_IMAGE_LOG_WARNING(msg, args...)   //DO_ULOG("Warn: "msg, ##args)
#define CPR_IMAGE_LOG_INFO(msg, args...)      //DO_ULOG("Info: "msg, ##args)
#define CPR_IMAGE_LOG_VERBOSE(msg, args...)   //DO_ULOG("Verbose: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE(msg, args...)     //DO_ULOG("Trace: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE_RAW(msg, args...) //CPR_IMAGE_LOG_TRACE(msg, ##args)

//TODO: After BU enable proper CPR logging 
#if 0
#define CPR_IMAGE_LOG_INIT()

#define DO_LOG(...) \
do {\
    snprintf(cpr_image_log_buf, CPR_LOG_SIZE-1, __VA_ARGS__);\
    boot_log_message(cpr_image_log_buf);\
} while(0)

#if CPR_LOG_FATAL_ASSERT_ENABLED
#define CPR_IMAGE_LOG_FATAL(msg, args...)     do { DO_LOG("Fatal: "msg, ##args); CPR_ASSERT(false); } while(false)
#else
#define CPR_IMAGE_LOG_FATAL(msg, args...)     DO_LOG("FATAL: "msg, ##args)
#endif
#define CPR_IMAGE_LOG_ERROR(msg, args...)     DO_LOG("ERROR: "msg, ##args)
#define CPR_IMAGE_LOG_WARNING(msg, args...)   DO_LOG("WARN : "msg, ##args)
#define CPR_IMAGE_LOG_INFO(msg, args...)      DO_LOG("INFO : "msg, ##args)
#define CPR_IMAGE_LOG_VERBOSE(msg, args...)   DO_LOG("VERBOSE: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE(msg, args...)     DO_LOG("TRACE: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE_RAW(msg, args...) CPR_IMAGE_LOG_TRACE(msg, ##args)
#endif

#if 0
#define CPR_IMAGE_STATIC_LOG_INIT() cpr_image_static_log_update_all()

#define CPR_IMAGE_STATIC_LOG_UPDATE_ALL() cpr_image_static_log_update_all()

#define CPR_IMAGE_STATIC_LOG_CPR_INFO(cpr_info)\
    CPR_IMAGE_LOG_INFO(\
            "CPR Info: (ChipVersion:0x%08X) (CprRev:0x%08X) (Foundry:%u) (DataInit:%u) (CfgInit:%u) (FullInit:%u) (EnabCfg Hash:%s) (VP Hash:%s)",\
            (cpr_info)->chipVersion,\
            (cpr_info)->cprRev,\
            (cpr_info)->foundry,\
            (cpr_info)->dataInit,\
            (cpr_info)->cfgInit,\
            (cpr_info)->fullInit,\
            (cpr_info)->cfgInfo->hashValue,\
            (cpr_info)->vpInfo->hashValue)

#define CPR_IMAGE_STATIC_LOG_RAIL_INFO(rail_state)\
    CPR_IMAGE_LOG_INFO(\
            "Rail Info: (CtrlMode:%u) (AgingMargin:%d, ThermMargin:%d, TestMargin:%d) (DisableVotes:0x%X) (PrevMode:%d, ActiveMode:%d)",\
            (rail_state)->cMode,\
            (rail_state)->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_AGING],\
            (rail_state)->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_THERMAL],\
            (rail_state)->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_TEST],\
            (rail_state)->disableVotes,\
            ((rail_state)->previousMode) ? (rail_state)->previousMode->mode : 0,\
            ((rail_state)->activeMode) ? (rail_state)->activeMode->mode : 0)

#define CPR_IMAGE_STATIC_LOG_MODE_INFO(rail_state, mode_setting, submode_setting)\
    CPR_IMAGE_LOG_INFO(\
            "Mode %u Info: (FuseStep:%d, FuseOffset:%d) (EnableCount:%u) (Floor:%u, Ceiling:%u) (Min:%u, Current:%u)",\
            (mode_setting)->mode,\
            (mode_setting)->decodedFuseSteps,\
            (mode_setting)->decodedFuseOffset,\
            (mode_setting)->enableCount,\
            (submode_setting)->floor,\
            (submode_setting)->ceiling,\
            (submode_setting)->min,\
            (submode_setting)->current)

#define CPR_IMAGE_STATIC_LOG_ISR_INFO(rail_state, mode_setting, submode_setting)\
    CPR_IMAGE_LOG_INFO(\
            "Mode %u ISR Info: (UpInt:%u, DownInt:%u) (HitFloor:%u, HitCeiling:%u, Ignored:%u)",\
            (mode_setting)->mode,\
            (submode_setting)->debug.up,\
            (submode_setting)->debug.down,\
            (submode_setting)->debug.floor,\
            (submode_setting)->debug.ceiling,\
            (submode_setting)->debug.ignored)

void cpr_image_static_log_update_all(void);
#endif

#endif

