
/**
 * @file:  cpr_image.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/04/11 09:05:18 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/src/cpr_image.c#10 $
 * $Change: 10240660 $
 */
#include <stdlib.h>
#include "cpr_image.h"
#include "cpr_image_defs.h"
#include "cpr_image_target.h"
#include "cpr_cfg.h"
#include "cpr_data.h"
#include "cpr_v3.h"
#include "railway.h"
#include "CoreVerify.h"
#include "DALSys.h"
#include "cpr_smem.h"
#include "smem.h"
#include "DDIChipInfo.h"
#include "cpr_utils.h"
#include "cpr_clockhwio.h"

#include <stringl/stringl.h>


void RBCPRTaskISR(void* task);
void* RBCPRTaskInit(cpr_image_isr func, void* ctx, uint32_t client_interrupt_id);

//calls cpr_init as boot still uses rbcpr_init for CPR initialization
void rbcpr_init (void)
{
  cpr_init ();
}

//******************************************************************************
// Global Data
//******************************************************************************

typedef struct
{
    cpr_domain_id   cpr_rail_id;
    const char*     railway_name;
} rail_name_map;

const static rail_name_map railway_name_map[] =
{
    { CPR_RAIL_MX,       "vddmx"      },
    { CPR_RAIL_CX,       "vddcx"      },
};

typedef struct
{
    boolean         init;
    int32           railway_rail_id;
    railway_voter_t voter;
} railway_handle;


static railway_handle handles[CPR_NUM_RAILS];

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static const char* cpr_rpm_railway_rail_name(cpr_domain_id rail_id)
{
    for(int i = 0; i < (sizeof(railway_name_map) / sizeof(rail_name_map)); i++) {
        if(rail_id == railway_name_map[i].cpr_rail_id) {
            return railway_name_map[i].railway_name;
        }
    }

    CPR_ASSERT( 0 );
}

static railway_handle* get_railway_handle(cpr_domain_id railId)
{
    uint32 railIdx = cpr_get_rail_idx( railId );

    if(!handles[railIdx].init)
    {
        handles[railIdx].railway_rail_id = rail_id( cpr_rpm_railway_rail_name( railId ) );
        CPR_ASSERT( handles[railIdx].railway_rail_id != RAIL_NOT_SUPPORTED_BY_RAILWAY );

        handles[railIdx].init = true;
    }

    return &handles[railIdx];
}

/*
 * function: cpr_image_do_set_corner_voltage
 * 
 * Make sure voltages for modes are in increasing order
 */
static void cpr_image_do_set_corner_voltage(int railIdx, int modeIdx, uint32 microvolts)
{

	cpr_mode_settings *modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx];
	
    if(modeIdx>0)
    {
		cpr_mode_settings *prev_modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx-1];
		uint32 prev_corner_voltage = prev_modeSettings->subModes->current;
		
		/* Recurse until previous mode voltage is less than microvolts
		 * to satisfy voltages are in increasing order.
		 */
		if(prev_corner_voltage > microvolts)
			cpr_image_do_set_corner_voltage(railIdx, (modeIdx-1), microvolts); //Should recurse such that voltages for modes can't overlap
    }

    if(modeIdx < (cpr_info.railStates[railIdx].modeSettingsCount-1) )
    {
		cpr_mode_settings *next_modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx+1];
		uint32 next_corner_voltage = next_modeSettings->subModes->current;
		
		/* Recurse until next mode voltage is greater than microvolts
		 * to satisfy voltages are in increasing order.
		 */
		if(next_corner_voltage < microvolts)
			cpr_image_do_set_corner_voltage(railIdx, (modeIdx+1), microvolts); //Should recurse such that voltages for modes can't overlap
    }

	modeSettings->subModes->current = microvolts;
}

void cpr_image_set_rail_mode_voltage(cpr_rail* rail, cpr_voltage_mode mode, uint32 recommendation)
{
	uint32  railIdx     =  rail->railIdx;
	uint32 	modeIdx 	=  rail->vp->idxLookupFunc(mode);
	cpr_image_do_set_corner_voltage(railIdx, modeIdx, recommendation);
    
    //Now verify that the voltages are increasing with the corner
    for(int i=0; i< cpr_info.railStates[railIdx].modeSettingsCount-1 ; i++)
    {
		cpr_mode_settings *curr_modeSettings = &cpr_info.railStates[railIdx].modeSettings[i];
		cpr_mode_settings *next_modeSettings = &cpr_info.railStates[railIdx].modeSettings[i+1];
        CORE_VERIFY( curr_modeSettings->subModes->current <= next_modeSettings->subModes->current);
    }
	
}

/*static void cpr_image_pre_switch(const railway_settings *settings, void* ctx)
{
    cpr_domain_id railId = (cpr_domain_id)(uintptr_t)ctx;
    cpr_pre_state_switch( railId );
}

static void cpr_image_post_switch(const railway_settings *settings, void* ctx)
{
    cpr_domain_id railId = (cpr_domain_id)(uintptr_t)ctx;
    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {(cpr_voltage_mode)settings->mode}};
    cpr_post_state_switch( railId, &info, settings->microvolts );
}

static void cpr_isr() __irq
{
    uint32 interrupt = interrupt_current_isr();
    interrupt_clear( interrupt );

    for(int i = 0; i < CPR_NUM_RAILS; i++)
    {
        if(isr_data[i].interrupt == interrupt)
        {
            RBCPRTaskISR( isr_data[i].task );
            return;
        }
    }

    CPR_ASSERT( 0 );
}*/

//******************************************************************************
// Public API Functions
//******************************************************************************

void* cpr_image_malloc(uint32 size)
{
    void* buf;
    if(DALSYS_Malloc( size, &buf ) != DAL_SUCCESS) {
        CPR_ASSERT( 0 );
    }

    memset( buf, 0, size );
    return buf;
}

void cpr_image_free(void* buf)
{
    DALSYS_Free( buf );
}

void cpr_image_memscpy(void* dst, void* src, uint32 size)
{
    memscpy(dst, size, src, size);
}

void cpr_image_register_thermal_cb(cpr_therm_cb cb)
{

}

void cpr_image_register_isr(cpr_domain_id railId, uint32 interrupt, cpr_image_isr isr, void* ctx)
{

}

void cpr_image_enable_clock(const char* clkId)
{
	     //enable the block 
    HWIO_OUTF(GCC_RBCPR_CBCR, CLK_ENABLE, 0x1);
    HWIO_OUTF(GCC_RBCPR_AHB_CBCR, CLK_ENABLE, 0x1);
    
    while(HWIO_INF(GCC_RBCPR_CBCR, CLK_OFF));
    while(HWIO_INF(GCC_RBCPR_AHB_CBCR, CLK_OFF));
	
	    //enable the block 
    HWIO_OUTF(GCC_RBCPR_MX_CBCR, CLK_ENABLE, 0x1);
    HWIO_OUTF(GCC_RBCPR_MX_AHB_CBCR, CLK_ENABLE, 0x1);
    
    while(HWIO_INF(GCC_RBCPR_MX_CBCR, CLK_OFF));
    while(HWIO_INF(GCC_RBCPR_MX_AHB_CBCR, CLK_OFF));

}

uint32 cpr_image_get_chip_version()
{
    return DalChipInfo_ChipVersion();
}

cpr_foundry_id cpr_image_get_foundry()
{
    /*cpr_foundry_id cpr_foundry;
    uint32 chipInfo_foundry = Chipinfo_GetFoundryId();

    switch(chipInfo_foundry)
    {
        case(CHIPINFO_FOUNDRYID_TSMC):
            cpr_foundry = CPR_FOUNDRY_TSMC;
            break;
        case(CHIPINFO_FOUNDRYID_GF):
            cpr_foundry = CPR_FOUNDRY_GF;
            break;
        case(CHIPINFO_FOUNDRYID_SS):
            cpr_foundry = CPR_FOUNDRY_SS;
            break;
        case(CHIPINFO_FOUNDRYID_IBM):
            cpr_foundry = CPR_FOUNDRY_IBM;
            break;
        case(CHIPINFO_FOUNDRYID_UMC):
            cpr_foundry = CPR_FOUNDRY_UMC;
            break;
        default:
            CPR_ASSERT( 0 ); // Chip foundry is not valid
    }

    return cpr_foundry;*/
    return CPR_FOUNDRY_SS;
}

boolean cpr_image_set_rail_mode(cpr_domain_id railId, cpr_domain_info* info)
{
    railway_handle* hdl = get_railway_handle( railId );

    if(!hdl->voter) {
        hdl->voter = railway_create_voter( hdl->railway_rail_id, RAILWAY_CPR_SETTLING_VOTER );
    }

    railway_corner_vote( hdl->voter, (railway_corner) info->u.mode );
    railway_transition_rails( );
    return true;
}

boolean cpr_image_rail_transition_voltage(cpr_domain_id railId)
{
    railway_transition_rails();
    
    return true;
}

void cpr_image_enter_sleep(void)
{
}

void cpr_image_exit_sleep(void)
{
}

void cpr_image_open_remote_cfg(void** cfg, uint32* size)
{
    *cfg = smem_get_addr( SMEM_CPR_CONFIG, size );

    if(!*cfg)
    {
        *size = 0x600;     //Reserve 1K for now
        *cfg = smem_alloc(SMEM_CPR_CONFIG, *size);
    }

    if(*cfg)
    {
        CPR_LOG_TRACE("smem addr: 0x%p", *cfg);
    }
    else
    {
        /*
         * KW does not like CPR_LOG_FATAL() 
         */
        CPR_LOG_FATAL("Failed to get smem addr");
        CPR_ASSERT(0);
    }
}

void cpr_image_close_remote_cfg()
{

}

void cpr_image_wait(uint32 us)
{
    DALSYS_BusyWait( us );
}

//Function to be called after SMEM is initialized to push out the CPR settings
//to SMEM. These settings are to be picked up by the RPM CPR driver during boot.
//Must be done before the RPM FW execution begins.
void cpr_externalize_state()
{
    CPR_LOG_INFO("Exporting CPR state");

    cpr_smem_serialize_config( cpr_utils_get_rail_state( CPR_RAIL_CX   ), false );
    cpr_smem_serialize_config( cpr_utils_get_rail_state( CPR_RAIL_MX   ), true );

    CPR_LOG_INFO("--- Done with CPR state export ---");
}

void rbcpr_core_dump()
{
}

uint32 cpr_image_get_eldo_voltage(cpr_domain_id railId, cpr_domain_info* info)
{
    return 0;
}

boolean cpr_image_can_resume_control(cpr_domain_id railId)
{
    return true;
}

void cpr_image_enable_measurements(void)
{
}

void cpr_image_disable_measurements(void)
{
}
uint32 rbcpr_cx_mx_settings_checksum(void) 
{ 
   return 0;
   //return cpr_cx_mx_settings_checksum (); //enable after DDR team tests it
}

void rbcpr_externalize_state (void)
{
  cpr_externalize_state ();
}

