/**
 * @file:  cpr_image_target_init.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 01:39:35 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/target/8953/cpr_image_target_init.h#2 $
 * $Change: 9714790 $
 */
#ifndef CPR_IMAGE_TARGET_INIT_H
#define CPR_IMAGE_TARGET_INIT_H

#include "cpr_cfg.h"

//******************************************************************************
// Default Enablement Structures
//******************************************************************************

extern cpr_cfg_funcs CPR_INIT_NONE;                              /* used to disable CPR */
extern cpr_cfg_funcs CPR_INIT_OPEN_LOOP;                         /* used to enable open loop CPR */
extern cpr_cfg_funcs CPR_INIT_CLOSED_LOOP_SETTLE_ONLY;           /* used to initialize closed loop CPR and settle the rails only (for SBL) */

#endif

