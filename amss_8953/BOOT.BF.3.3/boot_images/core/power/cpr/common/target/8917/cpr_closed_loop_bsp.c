/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_closed_loop.h"
#include "cpr_defs.h"
#include "msmhwiobase.h"

#define RBCPR_WRAPPER_BASE           0x48000

cpr_closed_loop_rail_config_t cx_config = 
{
    .rail_id =                  CPR_RAIL_CX,
    .ref_clk_resource           = "gcc_rbcpr_clk",
    .ahb_clk_resource           = "gcc_rbcpr_ahb_clk",
    .hal_handle =               {RBCPR_WRAPPER_BASE},
    .aging_sensor_ids =         (const uint8[]){0},  //defined in CPR_aging_parameters
    .aging_sensor_count =       0,    // 0 means aging is disabled
	.aging_ro_kv_x100 =         289,
    .aging_margin_limit =       12.5,
    .aging_voltage_mode =       CPR_VOLTAGE_MODE_SUPER_TURBO,
	.sensors_to_mask    =       (const uint8[]){2,3},
    .sensors_to_mask_count =    2,
    .step_quot =                26,
    .interrupt_id =             62,   // top_cpr_irq[0]
    .up_threshold =             0,
    .dn_threshold =             2,
    .consecutive_up =           0,
    .consecutive_dn =           2,
};

const cpr_closed_loop_config_t cpr_bsp_closed_loop_config =
{
    .closed_loop_config = (const cpr_closed_loop_rail_config_t*[])
    {
        &cx_config,
    },
    .closed_loop_config_count = 1,
};
