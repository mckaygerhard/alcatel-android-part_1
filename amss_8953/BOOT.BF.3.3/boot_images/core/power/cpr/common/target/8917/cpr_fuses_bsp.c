/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_fuses.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"
#include "HALhwio.h"

// -----------------------------------------------------------------
// 8937 Fuses 
// -----------------------------------------------------------------
const cpr_bsp_fuse_versioned_config_t cpr_bsp_fuse_config_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),
        },
        .foundry_range_count = 1,
    },
    .rail_fuse_config = (cpr_bsp_fuse_rail_config_t[])
    {
        {   //Vdd_Mx
            .rail_id = CPR_RAIL_MX,
            .number_of_fuses = 4,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB , CPR2_TARG_VOLT_SVSPLUS)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB  , CPR2_TARG_VOLT_NOM_1),
							CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_LSB  , CPR2_TARG_VOLT_NOM_0),
							
                        },
                },
				{
                    .fuse_type = CPR_FUSE_NOMINAL_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB , CPR2_TARG_VOLT_NOMPLUS)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_LSB , CPR2_TARG_VOLT_TURBO)
                        },
                },
            },
        },
        {   //Vdd_Cx
            .rail_id = CPR_RAIL_CX,
            .number_of_fuses = 5, 
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR0_TARG_VOLT_SVS)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_MSB, CPR0_TARG_VOLT_NOM)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW3_LSB, CPR0_TARG_VOLT_TUR)
                        },
                },
				{
                    .fuse_type = CPR_FUSE_NOMINAL_OFFSET,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW0_LSB, CPR0_OFFSET_NOM)
                        },
                },
				{
                    .fuse_type = CPR_FUSE_TURBO_OFFSET,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW0_LSB, CPR0_OFFSET_TUR)
                        },
                },
            }, 
        },
        {   //Vdd_Mss
            .rail_id = CPR_RAIL_MSS,
            .number_of_fuses = 3,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_MSB , CPR1_TARG_VOLT_SVS)
                        },
                },           
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_MSB , CPR1_TARG_VOLT_NOM)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_MSB , CPR1_TARG_VOLT_TUR)
                        },
                },
            },
        },
    },
    .number_of_fused_rails = 3,
};

const cpr_config_bsp_fuse_config_t cpr_bsp_fuse_config =
{
    .versioned_fuses = (const cpr_bsp_fuse_versioned_config_t*[])
    {
		&cpr_bsp_fuse_config_8917,
    },
    .versioned_fuses_count = 1,
};
