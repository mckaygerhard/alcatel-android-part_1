/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_voltage_ranges.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"

// Mx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8976_mx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                                Ceiling, Floor,  Fuse-Ref, max_floor_to_ceiling, Corr-factor, Fuse-Type, 			Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,                 975000,  975000, 975000,    	 	0,          		0,		CPR_FUSE_SVS,			CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,              1040000, 975000, 1040000,  	 	0,          		0,		CPR_FUSE_SVS_PLUS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,             1075000, 975000, 1075000,   	 	0,         			0,		CPR_FUSE_NOMINAL,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,          1140000, 995000, 1140000,     	0,          		0,		CPR_FUSE_NOMINAL_PLUS,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO,         1190000, 1075000,1190000,     	0,          		0,		CPR_FUSE_TURBO,			CPR_FUSE_NO_FUSE}, 		
		{CPR_VOLTAGE_MODE_SUPER_TURBO_L1,  	   1190000, 1190000,1190000,     	0,          		0,		CPR_FUSE_NO_FUSE,		CPR_FUSE_NO_FUSE}, 
    },
    .voltage_level_count = 6,
};

static const cpr_config_rail_voltage_ranges_t mx_voltage_ranges =
{
    .rail_id = CPR_RAIL_MX,
    .disable_fused_floor = true,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8976_mx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// Cx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8976_cx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,  Floor, Fuse-Ref, max_floor_to_ceiling, Corr-factor,  Fuse-Type, 			Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,          950000, 825000,  950000, 		0, 					0,		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1012500, 850000, 1012500, 		0,	 				0,		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1050000, 887500, 1050000, 		0, 					0,		CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1112500, 950000, 1112500, 		0, 					0,		CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1162500, 975000, 1162500, 		0, 					0,		CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t cx_voltage_ranges =
{
    .rail_id = CPR_RAIL_CX,
	.disable_fused_floor = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8976_cx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// MSS Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8976_mss_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,  Floor, Fuse-Ref, max_floor_to_ceiling, Corr-factor,  Fuse-Type,			Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,          950000, 825000,  950000, 		0, 				0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1012500, 850000, 1012500, 		0, 				0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1050000, 887500, 1050000, 		0, 				0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1112500, 950000, 1112500, 		0, 				0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1162500, 975000, 1162500, 		0, 				0,			CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t mss_voltage_ranges =
{
    .rail_id = CPR_RAIL_MSS,
	.disable_fused_floor = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8976_mss_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// GFX Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8976_gfx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,  Floor, Fuse-Ref, max_floor_to_ceiling, Corr-factor,	Fuse-Type,			Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,          950000, 795000,  950000, 		0, 				0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1010000, 835000, 1010000, 		0,				0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1050000, 855000, 1050000, 		0, 				0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1110000, 920000, 1110000, 		0, 				0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1165000, 945000, 1165000, 		0,				0,			CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t gfx_voltage_ranges =
{
    .rail_id = CPR_RAIL_GFX,
	.disable_fused_floor = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8976_gfx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};
const cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config =
{
    .rail_voltage_ranges = (const cpr_config_rail_voltage_ranges_t*[])
    {
		&mx_voltage_ranges,
		&cx_voltage_ranges,
        &mss_voltage_ranges,
		&gfx_voltage_ranges,
    },
    .rail_voltage_ranges_count = 4,
};

// Rail PMIC step-sizes

const cpr_config_misc_info_t cpr_bsp_misc_info_config =
{
    .cpr_rev_fuse = {HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, 12, (1<<14) | (1<<13) | (1<<12)}, //0x000A425C 14:12 CPR_GLOBAL_RC
    .rail_info = (const cpr_config_misc_rail_info_t[])
    {
        //Rail,             PMIC Step-Size
        {CPR_RAIL_MX,                 5000},
        {CPR_RAIL_CX,                12500},
		{CPR_RAIL_MSS,               12500},
		{CPR_RAIL_GFX,                5000},
    },
    .rail_info_count = 4,
};
