/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_target_quotients.h"
#include "cpr_device_hw_version.h"

static const cpr_target_quotient_versioned_config_t tsmc_cx_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   1327,   1254,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   1206,   1144,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,        970,    933,   1098,   1042,    550,   596,     401,    452},
        {CPR_VOLTAGE_MODE_SVS,           807,    778,    930,    881,    432,   479,     299,    348},
    },
    .ro_kv_x_100 = {                     250,    220,    250,    220,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t umc_cx_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   1327,   1254,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   1206,   1144,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,        970,    933,   1098,   1042,    550,   596,     401,    452},
        {CPR_VOLTAGE_MODE_SVS,           807,    778,    930,    881,    432,   479,     299,    348},
    },
    .ro_kv_x_100 = {                     250,    220,    250,    220,    160,   180,     120,    140},
};
static const cpr_target_quotient_rail_config_t cx_quotient_config =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_cx_quotients,
		&umc_cx_quotients,
    },
    .versioned_target_quotient_config_count = 2,
};

static const cpr_target_quotient_versioned_config_t tsmc_mx_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   1327,   1254,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   1206,   1144,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,       1010,    971,   1138,   1080,    579,   626,     428,    479},
        {CPR_VOLTAGE_MODE_SVS,          1010,    971,   1138,   1080,    579,   626,     428,    479},
    },
    .ro_kv_x_100 = {                     250,    220,    250,    220,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t umc_mx_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   0,      0,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   0,      0,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   0,      0,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,       1010,    971,   0,      0,    579,   626,     428,    479},
        {CPR_VOLTAGE_MODE_SVS,          1010,    971,   0,      0,    579,   626,     428,    479},
    },
    .ro_kv_x_100 = {                     250,    220,   0,      0,    160,   180,     120,    140},
};
static const cpr_target_quotient_rail_config_t mx_quotient_config =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_mx_quotients,
		&umc_mx_quotients,
    },
    .versioned_target_quotient_config_count = 2,
};

const cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &cx_quotient_config,
        &mx_quotient_config,
    },
    .rail_config_count = 2,
};
