/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_enablement.h"
#include "CoreVerify.h"
#include "cpr_voltage_ranges.h"
#include "cpr_closed_loop.h"

extern cpr_config_rail_voltage_ranges_t cx_voltage_ranges;
extern cpr_closed_loop_rail_config_t cx_config ;

static cpr_enablement_config_t cpr_enablement_find_target(void)
{
	cpr_enablement_config_t* cpr_enablement_config = NULL;
	for(int32 i=0; i< cpr_bsp_hw_enablement_type.cpr_bsp_hw_enablement_type_count; i++)
	{
		cpr_enablement_config = cpr_bsp_hw_enablement_type.cpr_enablement_config[i];
		if(cpr_device_target_matches_this_device(cpr_enablement_config->supported_chipset))
		{
			break;
		}
	}
	CORE_VERIFY(cpr_enablement_config);
	
	return (*cpr_enablement_config);
}


static const cpr_enablement_versioned_rail_config_t* cpr_enablement_find_device_config(const cpr_enablement_rail_config_t* enablement_config)
{
    cpr_enablement_versioned_rail_config_t ** init_params;
    for(int i=0; i<enablement_config->versioned_rail_config_count; i++)
    {
        const cpr_enablement_versioned_rail_config_t* versioned_enablement_config = enablement_config->versioned_rail_config[i];
        const cpr_config_hw_version_range* hw_versions = &versioned_enablement_config->hw_versions;

        if(cpr_device_hw_version_matches_this_device(hw_versions))
        {
                return versioned_enablement_config;
        }
    }   
 
    // This implies that foundry specifc CPR configuration is not added yet. 
    // So we will use open loop values to start with.
    init_params = (cpr_enablement_versioned_rail_config_t**)&enablement_config->versioned_rail_config[0];
    (*init_params)->enablement_init_params = &CPR_ENABLE_OPEN_LOOP;
    return (enablement_config->versioned_rail_config[0]);
 }

uint32 cpr_enablement_number_of_rails(void)
{
	const cpr_enablement_config_t cpr_bsp_enablement_config = cpr_enablement_find_target();
    return cpr_bsp_enablement_config.rail_enablement_config_count;
}

void cpr_enablement_rail_info(uint32 index, cpr_rail_id_t* rail_id, const cpr_enablement_versioned_rail_config_t** rail_enablement_versioned_config)
{	
	const cpr_enablement_config_t cpr_bsp_enablement_config = cpr_enablement_find_target();
    CORE_VERIFY(index<cpr_bsp_enablement_config.rail_enablement_config_count);

    const cpr_enablement_rail_config_t* rail_enablement_config = cpr_bsp_enablement_config.rail_enablement_config[index];
    *rail_id = rail_enablement_config->rail_id;
    *rail_enablement_versioned_config = cpr_enablement_find_device_config(rail_enablement_config);
}

const cpr_enablement_versioned_rail_config_t* cpr_enablement_find_versioned_rail_config(cpr_rail_id_t cpr_rail_id)
{
	const cpr_enablement_config_t cpr_bsp_enablement_config = cpr_enablement_find_target();
    for(int i = 0; i < cpr_bsp_enablement_config.rail_enablement_config_count; i++)
    {
        if(cpr_bsp_enablement_config.rail_enablement_config[i]->rail_id == cpr_rail_id)
        {
            return cpr_enablement_find_device_config(cpr_bsp_enablement_config.rail_enablement_config[i]);
        }
    }
    return NULL;
}
void cpr_enablement_from_xml(int32 rail_id, cpr_enablement_versioned_rail_config_t** rail_enablement_config)
{
       unsigned int xml_read; //to check whether to use the values from XML or not
       DALSYS_PROPERTY_HANDLE_DECLARE(xml_read_devcfg);
       DALSYSPropertyVar prop;
   
       //Get the handle for xml_read in XML using DAL
       CORE_DAL_VERIFY(DALSYS_GetDALPropertyHandleStr("/dev/core/power/cpr",xml_read_devcfg)); 
       CORE_DAL_VERIFY(DALSYS_GetPropertyValue( xml_read_devcfg, "XML_READ", 0,&prop));
       xml_read = (unsigned int)prop.Val.dwVal;
       if (xml_read == 0) //If '0' values from XML will not be used for configuration
           return;
		   
       if (rail_id == 0x100) //check for the rail
           {
           unsigned int mx_cpr_config; //to hold mx configuration from XML
           DALSYS_PROPERTY_HANDLE_DECLARE(mx_cpr_DevCfg);
           DALSYSPropertyVar prop;
   
           //Get the handle for mx cpr configuration in XML using DAL
           CORE_DAL_VERIFY(DALSYS_GetDALPropertyHandleStr("/dev/core/power/cpr",mx_cpr_DevCfg)); 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( mx_cpr_DevCfg, "MX_CPR_ENABLEMENT", 0,&prop));
           mx_cpr_config = (unsigned int)prop.Val.dwVal;
           
           //Based on the value in XML, switch between the various CPR configurations  
   	       switch(mx_cpr_config){
           case 0  :
               (*rail_enablement_config)->enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE;
               break;
           case 1  :
               (*rail_enablement_config)->enablement_init_params = &CPR_ENABLE_OPEN_LOOP;
               break;
           default :
		       break;
                  }
            }
   
        if (rail_id == 0x101) //check for the rail
           {
           unsigned int cx_cpr_config; //to hold cx configuration from XML
           int32 static_margin;
		   unsigned int fused_floor_disable;
		   unsigned int open_loop_adjustment;
		   unsigned int disable_aging;
		   DALSYS_PROPERTY_HANDLE_DECLARE(cx_cpr_DevCfg);
           DALSYSPropertyVar prop;
  
           //Get the handle for cx cpr configuration in XML using DAL
           CORE_DAL_VERIFY(DALSYS_GetDALPropertyHandleStr("/dev/core/power/cpr",cx_cpr_DevCfg));
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue(cx_cpr_DevCfg, "CX_CPR_ENABLEMENT", 0,&prop));
           cx_cpr_config = (unsigned int)prop.Val.dwVal;          
		   cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config = cpr_voltage_range_find_target();		   
		  
    
	       //Based on the value in XML, switch between the various cpr configurations 
   	       switch(cx_cpr_config){
               case 0  :
                   (*rail_enablement_config)->enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE;
                   break;
               case 1  :
                   (*rail_enablement_config)->enablement_init_params = &CPR_ENABLE_OPEN_LOOP;
                   break;
               case 2  : 
                   (*rail_enablement_config)->enablement_init_params = &CPR_ENABLE_CLOSED_LOOP; 
	               break;
	           default :
		           break;
                     }
	
           //updating the SVS static margin based on the values from XML  
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "SVS_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
	            (*rail_enablement_config)->supported_level[0].static_margin_mv += static_margin; 
      
	       //updating the SVS_HIGH static margin based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "SVS_HIGH_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
                (*rail_enablement_config)->supported_level[1].static_margin_mv += static_margin; 
      
	       //updating the NOM static margin based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "NOM_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
                (*rail_enablement_config)->supported_level[2].static_margin_mv += static_margin; 
  
           //updating the NOM_HIGH static margin based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "NOM_HIGH_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
                (*rail_enablement_config)->supported_level[3].static_margin_mv += static_margin; 
      
	       //updating the TURBO static margin based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "TURBO_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
                (*rail_enablement_config)->supported_level[4].static_margin_mv += static_margin;

           //updating the SUPER TURBO HIGH static margin based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "SUPER_TURBO_HIGH_STATIC_MARGIN", 0,&prop));
           static_margin = (int32)prop.Val.dwVal;
		   
		   if ( static_margin != 0xDEADBEFF)
                (*rail_enablement_config)->supported_level[5].static_margin_mv += static_margin;
           
           //finding whether to disable fused floor or not based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "FUSED_FLOOR", 0,&prop));
           fused_floor_disable = (uint32)prop.Val.dwVal;
		   
	       //Based on the value in XML, switch between the various configurations 
   	       switch(fused_floor_disable){
               case 0  :
                   cpr_bsp_voltage_ranges_config.rail_voltage_ranges[1]->disable_fused_floor = true;
                   break;
               case 1  :
                   cpr_bsp_voltage_ranges_config.rail_voltage_ranges[1]->disable_fused_floor = false;
                   break;
               default :
		           break;
                     }
		   
		   //finding whether to disable Open Loop adjustment or not based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "OPEN_LOOP_ADJUSTMENT", 0,&prop));
           open_loop_adjustment = (uint32)prop.Val.dwVal;
		   
		   switch(open_loop_adjustment){
               case 0  :
                   cpr_bsp_voltage_ranges_config.rail_voltage_ranges[1]->openloop_adjustment = false;
                   break;
               case 1  :
                   cpr_bsp_voltage_ranges_config.rail_voltage_ranges[1]->openloop_adjustment = true;
                   break;
               default :
		           break;
                     }
		   
		   //finding whether to enable or disable aging based on the values from XML 
           CORE_DAL_VERIFY(DALSYS_GetPropertyValue( cx_cpr_DevCfg, "AGING_DISABLE", 0,&prop));
           disable_aging = (uint32)prop.Val.dwVal;
	       
		   //Based on the value in XML, switch between the various configurations 
   	       if(disable_aging ==1 )
               cx_config.aging_sensor_count = 0;
		   else
		       return;             
		           
		    }

        else
           return;
}
