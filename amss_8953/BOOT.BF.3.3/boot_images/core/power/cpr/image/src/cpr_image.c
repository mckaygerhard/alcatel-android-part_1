/*===========================================================================
                              rbcpr.c

SERVICES:

DESCRIPTION:

INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include <stdlib.h>
#include "CoreVerify.h"
#include "cpr_sbl.h"
#include "cpr.h"
#include "DDIChipInfo.h"

#include <stdbool.h>
#include "cpr_voltage_ranges.h"
#include "cpr_target_quotients.h"
#include "cpr_enablement.h"
#include "cpr_image.h"
#include "DALHeap.h"
#include <stringl/stringl.h>
// #include "LoaderUtils.h" 
#include "cpr_clock_hwio.h"


//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------

size_t cpr_image_memscpy(void *dst, size_t dst_size, const void *src, size_t src_size)
{
    return memscpy(dst, dst_size, src, src_size);
}

cpr_image_closed_loop_rail_t cpr_image_alloc_image_rail_state(void)
{
    cpr_image_closed_loop_rail_t image_rail_state = (cpr_image_closed_loop_rail_t)cpr_image_malloc(sizeof(cpr_image_closed_loop_rail_s));
    CORE_VERIFY_PTR(image_rail_state);
    memset(image_rail_state, 0, sizeof(cpr_image_closed_loop_rail_s));
    return image_rail_state;
}

typedef struct
{
    cpr_rail_id_t   cpr_rail_id;
    const char*     railway_name;
} cpr_rpm_railway_rail_name_mapping;

const static cpr_rpm_railway_rail_name_mapping cpr_railway_name_map[] = 
{
    { CPR_RAIL_MX,       "vddmx"    },
    { CPR_RAIL_CX,       "vddcx"    },
    { CPR_RAIL_GFX,      "vddgfx"   },
    { CPR_RAIL_VDDA_EBI, "vdda_ebi" },
};

const char* cpr_rpm_railway_rail_name(cpr_rail_id_t rail_id)
{
    for(int i=0; i<(sizeof(cpr_railway_name_map)/sizeof(cpr_rpm_railway_rail_name_mapping)); i++)
    {
        if(rail_id == cpr_railway_name_map[i].cpr_rail_id)
        {
            return cpr_railway_name_map[i].railway_name;
        }
    }
    
    CORE_VERIFY(0);
    // return NULL;
}

typedef struct cpr_image_rail_voltage_control_handle_s
{
    int32           railway_rail_id;
    railway_voter_t railway_voter;
    
} cpr_image_rail_voltage_control_handle_s;

cpr_image_rail_voltage_control_handle_t cpr_image_alloc_voltage_control_handle(cpr_rail_id_t cpr_rail_id)
{
    cpr_image_rail_voltage_control_handle_t control_handle = (cpr_image_rail_voltage_control_handle_t)cpr_image_malloc(sizeof(cpr_image_rail_voltage_control_handle_s));
    CORE_VERIFY_PTR(control_handle);
    memset(control_handle, 0, sizeof(cpr_image_rail_voltage_control_handle_s));
    
    control_handle->railway_rail_id = rail_id(cpr_rpm_railway_rail_name(cpr_rail_id));
    CORE_VERIFY(control_handle->railway_rail_id!=RAIL_NOT_SUPPORTED_BY_RAILWAY);
    
    return control_handle;
}

void cpr_image_set_rail_mode_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode, uint32 voltage_uv)
{
    CORE_VERIFY_PTR(voltage_control_handle);
    railway_set_corner_voltage(voltage_control_handle->railway_rail_id, (railway_corner)voltage_mode, voltage_uv);
}

uint32 cpr_image_get_rail_mode_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode)
{
    CORE_VERIFY_PTR(voltage_control_handle);
    return railway_get_corner_voltage(voltage_control_handle->railway_rail_id, (railway_corner)voltage_mode);
}

const cpr_corner_params_t* cpr_image_get_current_corner_params(cpr_closed_loop_rail_t rail)
{
    railway_settings settings;
    railway_get_current_settings(rail->voltage_control_handle->railway_rail_id, &settings);
    return cpr_corner_params(rail, settings.mode);
}

void cpr_image_rail_transition_voltage(cpr_image_rail_voltage_control_handle_t voltage_control_handle)
{
    railway_transition_rails();
}

void cpr_image_set_rail_mode(cpr_image_rail_voltage_control_handle_t voltage_control_handle, cpr_voltage_mode_t voltage_mode)
{
    if(!voltage_control_handle->railway_voter)
    {
        voltage_control_handle->railway_voter = railway_create_voter(voltage_control_handle->railway_rail_id, RAILWAY_CPR_SETTLING_VOTER);
    }
    
    railway_corner_vote(voltage_control_handle->railway_voter, (railway_corner)voltage_mode);
    railway_transition_rails();
}

cpr_voltage_mode_t cpr_image_get_rail_mode(cpr_image_rail_voltage_control_handle_t voltage_control_handle)
{
    //Check that we went to the correct voltage
    railway_settings settings;
    railway_get_current_settings(voltage_control_handle->railway_rail_id, &settings);
    return (cpr_voltage_mode_t)settings.mode;
}

void cpr_image_enable_clocks(cpr_closed_loop_rail_t rail)
{
    //enable the block 
    HWIO_OUTF(GCC_RBCPR_CBCR, CLK_ENABLE, 0x1);
    HWIO_OUTF(GCC_RBCPR_AHB_CBCR, CLK_ENABLE, 0x1);
    
    while(HWIO_INF(GCC_RBCPR_CBCR, CLK_OFF));
    while(HWIO_INF(GCC_RBCPR_AHB_CBCR, CLK_OFF));
}

#define CPR_INVALID_CORNER -1

static void cpr_init_corner_lookup(cpr_closed_loop_rail_t rail)
{
    for(int i=0; i<RAILWAY_CORNERS_COUNT; i++)
    {
        rail->image_rail_state->corner_lookup_index[i] = CPR_INVALID_CORNER;
    }

    for(int i=0; i<rail->target_params_count; i++)
    {
        railway_corner corner = (railway_corner)rail->target_params[i].voltage_mode;
        CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
        rail->image_rail_state->corner_lookup_index[corner] = i;
    }
}

cpr_corner_params_t* cpr_corner_params(cpr_closed_loop_rail_t rail, railway_corner corner)
{
    CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
    int corner_lookup = rail->image_rail_state->corner_lookup_index[corner];
    CORE_VERIFY(corner_lookup != CPR_INVALID_CORNER);
    return &rail->target_params[corner_lookup];
}


void cpr_image_clear_interrupt_at_interrupt_controller(uint32 interrupt_id)
{
}

void cpr_image_busywait(uint32 us)
{
    DALSYS_BusyWait( us );
}

void cpr_image_prepare_for_voltage_settling(void)
{
//    railway_unlock();
}

void cpr_image_init_image_closed_loop(cpr_closed_loop_rail_t rail)
{
    cpr_init_corner_lookup(rail);
}

uint32 cpr_image_get_chip_info_version(void)
{
    return DalChipInfo_ChipVersion();
}

cpr_foundry_id cpr_image_get_foundry_info(void)
{

    DalChipInfoFoundryIdType foundryID = DalChipInfo_FoundryId();
    
    switch(foundryID)
    {
        case(DALCHIPINFO_FOUNDRYID_TSMC):
            return CPR_FOUNDRY_TSMC;
        case(DALCHIPINFO_FOUNDRYID_GF):
            return CPR_FOUNDRY_GF;
        case(DALCHIPINFO_FOUNDRYID_SS):
            return CPR_FOUNDRY_SS;
        case(DALCHIPINFO_FOUNDRYID_IBM):
            return CPR_FOUNDRY_IBM;
        case(DALCHIPINFO_FOUNDRYID_UMC):
            return CPR_FOUNDRY_UMC;
        case(DALCHIPINFO_FOUNDRYID_SMIC):
            return CPR_FOUNDRY_SMIC;
        default:
            break;
    }

    CORE_VERIFY(0); // Chip foundry is not valid

}

uint32 rbcpr_cx_mx_settings_checksum(void) { return 0;}
void cpr_image_cancel_task(cpr_closed_loop_rail_t rail) {}

void cpr_image_prepare_to_access_smem(void)
{
}

void cpr_image_smem_access_complete(void)
{
}

void* cpr_image_malloc(uint32 size)
{
    void* ret = NULL;
    DALResult result = DALSYS_Malloc(size, &ret);
    CORE_VERIFY(result == DAL_SUCCESS);
    return ret;
}

void cpr_init_initial_voltage_config(void);
void cpr_init_closed_loop_init(void);

void cpr_init(void)
{
	cpr_init_initial_voltage_config();
	cpr_init_closed_loop_init();
}

void rbcpr_init (void)
{
  cpr_init ();
}

extern void cpr_externalize_state (void);

void rbcpr_externalize_state (void)
{
  cpr_externalize_state ();
}
