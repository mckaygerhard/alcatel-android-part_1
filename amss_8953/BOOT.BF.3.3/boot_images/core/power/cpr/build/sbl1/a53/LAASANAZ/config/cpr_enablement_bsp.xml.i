<driver name="NULL">
  <device id="/dev/core/power/cpr">
    <!--check whether to use the values from xml or not-->
    <props name= "XML_READ" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>  
	<!--CX-->
    <props name= "CX_CPR_ENABLEMENT" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--MX-->
	<props name= "MX_CPR_ENABLEMENT" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--SVS-->
	<props name= "SVS_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--SVS_HIGH-->
	<props name= "SVS_HIGH_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--NOMINAL-->
	<props name= "NOM_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--NOMINAL_HIGH-->
	<props name= "NOM_HIGH_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--SUPER_TURBO-->
	<props name= "TURBO_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--SUPER_TURBO HIGH-->
	<props name= "SUPER_TURBO_HIGH_STATIC_MARGIN" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--DISABLING FUSED FLOOR-->
	<props name= "FUSED_FLOOR" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--DISABLING OPEN LOOP ADJUSTMENT-->
	<props name= "OPEN_LOOP_ADJUSTMENT" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>
	<!--AGING ENABLE OR DISABLE-->
	<props name= "AGING_DISABLE" type=DALPROP_ATTR_TYPE_UINT32>
      0xDEADBEFF
    </props>		
  </device>
</driver>
