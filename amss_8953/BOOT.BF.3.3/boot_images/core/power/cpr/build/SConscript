# =========================================================

Import('env')
env = env.Clone()
#env.Append(CCFLAGS = " -DRPM_CPR_BUILD")

if env['MSM_ID'] == '8940':
	env['MSM_ID'] = '8937'
	
if env['MSM_ID'] == '8920':
	env['MSM_ID'] = '8917'
   
RBCPRROOT = "${BUILD_ROOT}/core/power/cpr"

CPR_HAL_PATH = RBCPRROOT + '/common/hal'
CPR_HAL_COMMON_INCLUDE = CPR_HAL_PATH + '/inc'

CPR_HAL_VERSION = 'v2'

CPR_IMAGE_INCPATH = RBCPRROOT + '/image/inc'
COMMONINCPATH = RBCPRROOT + '/common/inc'

TARGET_SPECIFIC_INC_PATH = RBCPRROOT + '/src/target/${MSM_ID}'
CPR_HAL_VERSIONED_INC_PATH =  CPR_HAL_PATH + '/' + CPR_HAL_VERSION + '/inc'
HAL_TARGET_SPECIFIC_INC_PATH =  CPR_HAL_PATH + '/' + CPR_HAL_VERSION + '/target/${MSM_ID}'

env.VariantDir('${BUILDPATH}', RBCPRROOT, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'POWER',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DEBUGTRACE',
   'MPROC',
   'BOOT',
   'SERVICES',
   # needs to be last as it may contain wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)
env.RequirePublicApi(['RFA'], 'pmic')

env.PublishPrivateApi('CPR',[CPR_IMAGE_INCPATH, COMMONINCPATH, TARGET_SPECIFIC_INC_PATH, 
                             HAL_TARGET_SPECIFIC_INC_PATH, CPR_HAL_COMMON_INCLUDE, CPR_HAL_VERSIONED_INC_PATH])

if env.has_key('HWIO_IMAGE'):
    env.AddHWIOFile('HWIO', [
        {
            'filename': '${INC_ROOT}/core/power/cpr/common/hal/target/${MSM_ID}/HAL_cpr_hwio.h',
            'modules': ['rbcpr_wrapper'],
            'output-offsets': True,
            'explicit-addressing': True,
            'header':
                '/*\n'
                ' * HWIO base definitions\n'
                ' */\n'
                '#include "msmhwiobase.h"\n\n'
        },
    ])

    env.AddHWIOFile('HWIO', [
        {
            'filename': '${INC_ROOT}/core/power/cpr/common/src/target/${MSM_ID}/cpr_qfprom.h',
            'modules': ['security_control_core'],
            'header':
                '#include "msmhwiobase.h"\n\n'
        },
    ])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
if env['MSM_ID'] in ['8994']:
    env.Append(CPPDEFINES = 'SMEM_64BIT_ALIGNMENT')

CPR_C_SOURCES = [
    '${BUILDPATH}/image/src/cpr_image.c',
    '${BUILDPATH}/common/src/cpr_fuses.c',
    '${BUILDPATH}/common/src/cpr_voltage_ranges.c',
    '${BUILDPATH}/common/src/cpr_target_quotients.c',
    '${BUILDPATH}/common/src/cpr_device_hw_version.c',
    '${BUILDPATH}/common/src/cpr_enablement.c',
    '${BUILDPATH}/common/src/cpr_open_loop.c',
    '${BUILDPATH}/common/src/cpr_closed_loop.c',
    '${BUILDPATH}/common/src/cpr.c',
    '${BUILDPATH}/common/src/cpr_checksum.c',
    '${BUILDPATH}/common/src/cpr_smem.c',
    '${BUILDPATH}/common/target/${MSM_ID}/cpr_fuses_bsp.c',
    '${BUILDPATH}/common/target/${MSM_ID}/cpr_voltage_ranges_bsp.c',
    '${BUILDPATH}/common/target/${MSM_ID}/cpr_enablement_bsp.c',
    '${BUILDPATH}/common/target/${MSM_ID}/cpr_target_quotients_bsp.c',
    '${BUILDPATH}/common/target/${MSM_ID}/cpr_closed_loop_bsp.c',
]

CPR_STUB_SOURCES = [
    '${BUILDPATH}/src/cpr_stubs.c',
]

CPR_HAL_VERSION_SOURCES = [
    '${BUILDPATH}/common/hal/' + CPR_HAL_VERSION + '/src/HAL_cpr.c',
    '${BUILDPATH}/common/hal/' + CPR_HAL_VERSION + '/src/HAL_cpr_closed_loop.c',
]

#------------------------------------------------------------------------------
# Adding device config data
#------------------------------------------------------------------------------
if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   # Select appropriate xml config file based on target. May not change on
   # single core targets
   env.AddDevCfgInfo(DEVCFG_IMG, {
         'devcfg_xml' : '${BUILD_ROOT}/core/power/cpr/config/cpr_enablement_bsp.xml'	
      }
   )
if 'USES_QDSS_SWE' in env:
   QDSS_IMG = ['QDSS_EN_IMG']
   events = [['PLACE_HOLDER=670', 'cpr_pre_swith_entry: (rail %d) (corner %d) (microvolts %d)'],
             ['RBCPR_CORNER_UPDATE_REC', 'cpr_corner_update_rec: (rail: %d) (corner: %d) (step up(2)/down(1): %d)'],
             ['RBCPR_CORNER_UPDATE_ACT', 'cpr_corner_update_act: (rail: %d) (hit floor? %d) (hit ceiling? %d) (result microvolts: %d)'],
             ['RBCPR_ISR', 'cpr_isr: (task 0x%x)'],
             ['RBCPR_LAST=689','NULL'],
            ]
   env.AddSWEInfo(QDSS_IMG, events)
   
#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
c_env = env.Clone()
c_env.Append(CCFLAGS = " --c99")
#c_env.Append(ARMCC_OPT = "${ARM_OPT_SIZE} ${ARM_OPT_2}")

if env['MSM_ID'] in ['8976', '8937', '8917']:
    c_env.AddLibrary(['CORE_CPR'],'${BUILDPATH}/cpr_c.lib', CPR_C_SOURCES)
    c_env.AddLibrary(['CORE_CPR'],'${BUILDPATH}/cpr_hal_' + CPR_HAL_VERSION + '_v2' + '.lib', CPR_HAL_VERSION_SOURCES)
    #env.AddLibrary(['CORE_CPR'],'${BUILDPATH}/cpr_cpp_v2.lib', CPR_CPP_SOURCES)
