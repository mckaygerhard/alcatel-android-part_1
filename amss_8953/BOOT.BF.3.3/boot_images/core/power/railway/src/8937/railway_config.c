/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/railway/src/8937/railway_config.c#2 $
$Date: 2016/04/21 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2012 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "railway_config.h"
#include "pm_ldo.h"
#include "pm_smps.h"


/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

//
// BEGIN config data; should migrate to the system enumeration data method
//
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .rail_type = RAILWAY_RAIL_TYPE_MX,
            .vreg_name = "vddmx",

            //LDO3A
            .pmic_rail_type = RAILWAY_LDO_TYPE,
            .pmic_chip_id   = 0,
            .pmic_peripheral_index = PM_LDO_3,

            .pmic_step_size = 12500,

            .initial_corner = RAILWAY_NOMINAL,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                750000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_LOW
                1050000,                // RAILWAY_SVS_SOC
				1162500,                // RAILWAY_SVS_HIGH           
                1225000,                // RAILWAY_NOMINAL
                1287500,                // RAILWAY_NOMINAL_HIGH
                1350000,                // RAILWAY_TURBO
                1350000,                // RAILWAY_SUPER_TURBO
                1350000,                // RAILWAY_SUPER_TURBO_NO_CPR
				1400000,                // RAILWAY_SUPER_TURBO_HIGH
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_SVS_HIGH,
                RAILWAY_NOMINAL,
                RAILWAY_NOMINAL_HIGH,
                RAILWAY_SUPER_TURBO,
				RAILWAY_SUPER_TURBO_HIGH,
            },
            .supported_corners_count = 7,
        },

        // VDDCX
        {
            .rail_type = RAILWAY_RAIL_TYPE_LOGIC,
            .vreg_name = "vddcx",

            // SMPS2A
            .pmic_rail_type = RAILWAY_SMPS_TYPE,
            .pmic_chip_id   = 0,
            .pmic_peripheral_index = PM_SMPS_2,

            .pmic_step_size = 12500,

            .initial_corner = RAILWAY_NOMINAL,

            .default_uvs = (const uint32[])
            {
                0,                      // RAILWAY_NO_REQUEST
                700000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_LOW
                1050000,                // RAILWAY_SVS_SOC
         	    1162500,                // RAILWAY_SVS_HIGH
                1225000,                // RAILWAY_NOMINAL
                1287500,                // RAILWAY_NOMINAL_HIGH
                1350000,                // RAILWAY_TURBO
                1350000,                // RAILWAY_SUPER_TURBO
                1350000,                // RAILWAY_SUPER_TURBO_NO_CPR
				1400000,                // RAILWAY_SUPER_TURBO_HIGH
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_SVS_HIGH,
                RAILWAY_NOMINAL,
                RAILWAY_NOMINAL_HIGH,
                RAILWAY_SUPER_TURBO,
				RAILWAY_SUPER_TURBO_HIGH,
            },
            .supported_corners_count = 7,
        },
    },

    .num_rails = 2,
};
//
// END config data
//

const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

void railway_target_init(void)
{
}

