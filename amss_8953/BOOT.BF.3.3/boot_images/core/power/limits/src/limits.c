/*===========================================================================

                    LIMITS STUBS DEFINITIONS

DESCRIPTION
  Contains limits drivers public driver.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/limits/src/limits.c#1 $
$DateTime: 2015/07/02 04:11:47 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/20/14   dra     Added tz call to set current limit
09/16/14   clm     Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "lmh_limits.h"
#include "boot_fastcall_tz.h"
#include "DDIChipInfo.h"
#include "DALFramework.h"
#include "boot_dload.h"

/*===========================================================================
                 CONSTANT / DEFINE DECLARATIONS
===========================================================================*/
#define LMH_SET_CURRENT_LIMIT_CMD_ID        0x00001305
#define LMH_SET_CURRENT_LIMIT_CMD_PARAM_ID  0x3

typedef enum
{
    GLOBAL_CURRENT_LIMIT = 0,
    PROFILE_CURRENT_LIMIT = 1,
    RAIL_CURRENT_LIMIT = 2,
    LMH_LIMIT_TYPE_MAX,
    LIMIT_TYPE_SIZE = 0xFFFFFFE, //force to size of uint32
} LMH_limit_type;

typedef enum
{
    LMH_8A_LIMIT  =  8000,
    LMH_9A_LIMIT  =  9000,
    LMH_10A_LIMIT = 10000,
    RAIL_LIMITS_SIZE = 0xFFFFFFE, //force to size of uint32
} LMH_rail_limits;

typedef enum
{
    LMH_PROFILE_DEFAULT  = 0,
    LMH_PROFILE_ALT_1    = 1,
    LMH_PROFILES_MAX,
    PROFILES_SIZE = 0xFFFFFFE, //force to size of uint32
} LMH_profiles;

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================

**  Function :  limits_late_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver late init
*
* @return
*  None.
*
* @par Dependencies
*    Function must only be called post qsee init
*
*/

void limits_late_init(void)
{
  //DalChipInfoVersionType version = DalChipInfo_ChipVersion();
  //Add customization config calls
  /*
  boot_fastcall_tz_3arg(LMH_SET_CURRENT_LIMIT_CMD_ID,
                       LMH_SET_CURRENT_LIMIT_CMD_PARAM_ID, 
                       LMH_9A_LIMIT, 
                       RAIL_CURRENT_LIMIT, 
                       LMH_PROFILE_DEFAULT);
  */
}

