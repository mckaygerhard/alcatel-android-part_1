/*===========================================================================

                    LIMITS TARGET DEFINITIONS

DESCRIPTION
  Contains limits drivers target implementation.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/limits/src/8976/limits_target.c#4 $
$DateTime: 2015/10/12 23:58:18 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/12/15   dra     Added additional debug info, fixed storing bad data
12/10/14   dra     Ensure CPU4 is off when driver exists.
09/16/14   clm     Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "msmhwio.h"
#include "HALhwio.h"
#include "boot_comdef.h"
#include "fs_hotplug.h"
#include "boot_flash_dev_if.h"
#include "boot_sdcc.h"
#include "string.h"
#include "DDIChipInfo.h"
#include "TsensBoot.h"
#include "ClockBoot.h"
#include "boot_dload.h" //provides HWIO macros for CPU ALIAS
#include "lmh_llm_hwio.h"
#include "checksum.h"
#include "lmh_limits.h"
#include "boot_logger.h"
#include "boot_logger_timer.h"
#include <math.h>
#include "DALFramework.h"
#include "busywait.h"
#include <stdarg.h>

//---------------------------------------------------------------------------
// Constant / Define Declarations
//---------------------------------------------------------------------------
/* Switches */
//pkoleti - TBD
#define LMH_USES_PARTITION
//#define LMH_DEBUG_ENABLE

#define LMH_FAILED_CLOCK_INIT     0x494B4C43  /* CLKI */
#define LMH_FAILED_OPAMP          0x4D41504F  /* OPAM */
#define LMH_FAILED_TEMP_HIGH      0x484D4554  /* TEMH */
#define LMH_FAILED_GET_TEMP       0x474D4554  /* TEMG */
#define LMH_FAILED_CHANGE_CLOCK   0x434B4C43  /* CLKC */
#define LMH_FAILED_ENDPOINT       0x50444E45  /* ENDP */
#define LMH_FAILED                1
#define LMH_SUCCESS               0

/*TBD MAX is HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OVERRIDE_VALUE_BMSK
                                   Value subject to LSB effects as well.
  511 -> 511 * 4mA(LSB) = 2044mA
 */
#define LMH_OVERRIDE_CURRENT  511 
                                   
/* Partition parameters */
#define MAX_PARTITION_SIZE_BYTES  1024        /* Allocated size of partition */
#define LMH_MAGIC_NUM             0x20349812  /* Cookie to validate partition */
#define LMH_VERSION               0x00010000  /* Current Limits partition version */
#define PAYLOAD_SIZE              256         /* Size of any payload block */
#define LMH_MAX_ADC_NUM           32          /* Additional ADC's for future use */
#define RESERVED_BYTES            32
#define DEBUG_BYTES               128

/* ADC parameters */
#define TRIM_MAX_CORES 4
#define CORES_PER_ADC 1         /* number of cores eac adc measures */
#define LMH_TRIM_CORE_SEL 0x1   /* bitmask to select 1 or 2 core trim */
#define LMH_NUM_TRIM_CORES 0x1  /* number of cores to trim */
#define LMH_SAMPLES 128         /* number of samples per measurement */

/* Default programming */
//pkoleti - need slope and intercept for all 8 cs - TBD
#define DEFAULT_ADC_0_SLOPE     0xFC    /* default characterized slope */
#define DEFAULT_ADC_0_INTERCEPT 0xFF5C    /* default characterized intercept */
#define DEFAULT_ADC_1_SLOPE     0xFC    /* default characterized slope */
#define DEFAULT_ADC_1_INTERCEPT 0xFF5C    /* default characterized intercept */
#define DEFAULT_ADC_2_SLOPE     0xFC    /* default characterized slope */
#define DEFAULT_ADC_2_INTERCEPT 0xFF5C    /* default characterized intercept */
#define DEFAULT_ADC_3_SLOPE     0xFC   /* default characterized slope */
#define DEFAULT_ADC_3_INTERCEPT 0xFF5C    /* default characterized intercept */

/* Measurement parameters */
#define TRIM_VOLTAGE 900000     /* 0.9V - voltage for trim process */
#define M_COUNT 6               /* number of measurements to perform */
#define CDYN_CORE 1.50          /* 1.50nF */
#define CDYN_L2 1.455           /* 1.455nF */
#define G_HERTZ 1000000.0       /* in kHz*/
#define V_TRIM (((float)TRIM_VOLTAGE)/1000000.0)
#define LSB_mA 1.0              /* LSB in mA for digital current*/
#define K_1 65.0                /* Temp acceleration */
#define K_0 0.61                /* Voltage acceleration */
#define V_IDDQ 0.98              /* Voltage of IDDQ measurement */
#define T_IDDQ 32               /* 30C, Temp of IDDQ measurement */

//For Eldarion: cpu_4:4, cpu_5:5, cpu_6:6, cpu_7:7
#define CORE_4_TSENSE_ID 4     /* tsense sensor index for core 4 (A72-0) */
#define CORE_5_TSENSE_ID 5     /* tsense sensor index for core 5 (A72-1) */
#define CORE_6_TSENSE_ID 6     /* tsense sensor index for core 6 (A72-2) */
#define CORE_7_TSENSE_ID 7      /* tsense sensor index for core 7 (A72-3) */
#define LMH_SENSOR_INTERCEPT_ADJ 4        /* Divisor intercept */
#define LMH_SENSOR_SLOPE_ADJ  8 /* multiplication factor for Slop */

#define TRIM_TEMP_THRESHOLD 85  /* Temp threshold to fail trim */

//CAL REGION
//pkoleti - TBD
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_A_BG_CURRENTTRIM_BMSK    (0x1f<<0xa)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_A_BG_CURRENTTRIM_SHFT    0xa
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_A_ADC_TRIM_BMSK          (0xf<<0xf)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_A_ADC_TRIM_SHFT          0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SLOPE_BMSK               (0x3ff<<0x13)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SLOPE_SHFT               0x13
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_INTERCEPT_0_2_BMSK       (0x7<<0x1d)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_INTERCEPT_0_2_SHFT       0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_3_6_BMSK       (0xf<<10)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_3_6_SHFT       0x10
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_7_BMSK         (0x1<<0xf)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_7_SHFT         0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_8_9_BMSK       (0x3<<0xc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_INTERCEPT_8_9_SHFT       0xc

/* Logging defines */
#define BUF_SIZE    100       /* Max size of an single message */
#define LOG_ERROR   (0x1)     /* ERROR Messages */
#define LOG_WARN    (0x1<<1)  /* WARNING messages */
#define LOG_INFO    (0x1<<2)  /* Internal INFO message */
#define LOG_MSG     (0x1<<3)  /* General messages */
#define LOG_PROFILE (0x1<<4)  /* Profiling messages */
#define LOG_CUST    (0x1<<5)  /* Log messages ok for customers to see */

/* Timing defines */
#define LMH_XO_FREQUENCY_IN_KHZ   19200
#define LMH_COOLDOWN_DELAY        LMH_XO_FREQUENCY_IN_KHZ*1000

#define ISENSE_SLOPE_OFFSET 2

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------

PACKED struct adc_info
{
  uint32 slope;
  uint32 offset;
} PACKED_POST;

typedef PACKED struct cal_info_v1 {
  struct adc_info info[LMH_MAX_ADC_NUM];
  uint8  reserved[RESERVED_BYTES];
  uint8  debug[DEBUG_BYTES];
} PACKED_POST cal_info_v1_t;

PACKED union payload
{
  struct cal_info_v1 v1;
} PACKED_POST;

PACKED struct partition_data
{
  uint32 magic_num;
  uint32 version;
  uint16 checksum;
  union payload payload;
} PACKED_POST;

struct fuse_data 
{
  boot_boolean valid;
  uint32 data;
};

struct fuse_set
{
  struct fuse_data iddq_mult;
  struct fuse_data iddq_core[TRIM_MAX_CORES];
  struct fuse_data worst_core;
  struct fuse_data bg_current_trim;
  struct fuse_data adc_trim;
  struct fuse_data slope;
  struct fuse_data intercept[TRIM_MAX_CORES];
};

//4 cores data is required - for eldarion
struct trim_struct {
  uint32 corei_dign[TRIM_MAX_CORES][M_COUNT];
  uint32 corei_dig_SSQ[TRIM_MAX_CORES];
  uint32 corei_dig_SU[TRIM_MAX_CORES];
  float corei_tot_SU[TRIM_MAX_CORES];
  float corei_dig_tot_SU[TRIM_MAX_CORES];
  uint32 corei_temp[TRIM_MAX_CORES];
  uint32 m_freq[M_COUNT];
};

#ifdef LMH_DEBUG_ENABLE
struct lmh_debug_data_struct
{
  double core_dyn_pw[M_COUNT];
  float core_static_pw[TRIM_MAX_CORES][M_COUNT];
  float slope[TRIM_MAX_CORES];
  float intercept[TRIM_MAX_CORES];
};

static struct lmh_debug_data_struct lmh_debug_data;

/* Used to keep symbol for trim_data in the elf */
struct lmh_debug_data_struct *lmh_debug_data_p = &lmh_debug_data;

#endif

//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------
static volatile boolean bSBLLMhDisabled = 0;
static volatile boolean bUseDefaultSlopeIntercept = 0;
static volatile boolean bEnableReTrim = 0;
static uint32 perf_core_id = 4;

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------
/*define GUID for DDR params partition*/
//pkoleti - TBD
#ifdef LMH_USES_PARTITION
static struct hotplug_guid limits_params_partition_id =
      /*{10A0C19C-516A-5444-5CE3-664C3226A794}*/
      { 0x10A0C19C, 0x516A, 0x5444, { 0x5C, 0xE3, 0x66, 0x4C, 0x32, 0x26, 0xA7, 0x94 } };
#endif

static struct partition_data part_data;

const static checksum_data_t checksum_arr[] =
{
  {
	(uint8*)&(part_data.payload),
	sizeof(union payload)
  },
};

static struct fuse_set fuse_info;

static struct trim_struct trim_data;

/* Used to keep symbol for trim_data in the elf */
const struct trim_struct *trim_data_p = &trim_data;

/* Trim frequencies */
/* pkoleti - need to check what are the frquencies required on Eldarion - TBD */
uint32 freq[M_COUNT] =
{
    892800,
    1190400,
    1382400,
    1497000,
    1612800,
    1747200
};

#if 0
uint32 freq[M_COUNT] =
{
    0,
    148000,
    302000,
    585000,
    892800,
    1190400,
    1382400,
    1497000,
    1612800,
    1747200
};
#endif

volatile static uint32 log_bitmask = LOG_ERROR;//|LOG_WARN|LOG_INFO|LOG_MSG|LOG_PROFILE|LOG_CUST;

char buf[BUF_SIZE];

//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// External References
//---------------------------------------------------------------------------

#ifdef FEATURE_BOOT_EXTERN_LIMITS_COMPLETED
	extern boolean Clock_LMhInit( uint32 nA57uV );
	extern uint32 Clock_LMhPreCalibration( uint32 nFreqKHz );
	extern boolean Clock_LMhDeInit( void );

#else
	static __inline boolean Clock_LMhInit( uint32 nA57uV ){ return TRUE; }
	static __inline uint32 Clock_LMhPreCalibration( uint32 nFreqKHz ){ return nFreqKHz; }
	static __inline boolean Clock_LMhDeInit( void ){ return TRUE; }
#endif

/*--------------------------------------------------------------------------
                          MACRO DEFINITIONS
---------------------------------------------------------------------------*/

#define LMH_CS_CALIBRATION_RESULT_COREn_0_IN(n) \
    in_dword(HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_ADDR(0) + (0x10*n))
    
#define HWIO_OUT_B(reg,val) \
    do { \
        HWIO_OUT( reg,val); \
        memory_barrier(); \
    } while(0)
    
#define HWIO_OUTF_B(reg,fe,val) \
    do { \
        HWIO_OUTF( reg, fe, val); \
        memory_barrier(); \
    } while(0)

#define CPU_ON_core(core) \
    do { \
        HWIO_OUTF( APCS_ALIAS##core##_APC_PWR_GATE_CTL, HS_EN_FEW, 1); \
        busywait(1); \
        HWIO_OUTF( APCS_ALIAS##core##_APC_PWR_GATE_CTL, HS_EN_REST, 1); \
        busywait(1); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x79); \
        busywait(2); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x7D); \
        busywait(2); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x3D); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x3C); \
        busywait(1); \
    }while(0)

#define CPU_OFF_core(core) \
    do { \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x3D); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x7D); \
        busywait(2); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x79); \
        busywait(2); \
        HWIO_OUT_B( APCS_ALIAS##core##_CPU_PWR_CTL, 0x773); \
        HWIO_OUTF_B( APCS_ALIAS##core##_APC_PWR_GATE_CTL, HS_EN_REST, 0); \
        HWIO_OUTF_B( APCS_ALIAS##core##_APC_PWR_GATE_CTL, HS_EN_FEW, 0); \
    }while(0)

#define BITS(high_bit, size, var) \
    (var&(((0x1<<size)-1)<<high_bit))>>((high_bit+1)-size)
    
#define LMH_LOG_MESSAGE(type, fmt, ...) \
    lmh_log_message(type, fmt, ##__VA_ARGS__)

#define LMH_LOG_ERROR(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_ERROR, fmt, ##__VA_ARGS__)
    
#define LMH_LOG_WARN(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_WARN, fmt, ##__VA_ARGS__)
    
#define LMH_LOG_INFO(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_INFO, fmt, ##__VA_ARGS__)

#define LMH_LOG_MSG(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_MSG, fmt, ##__VA_ARGS__)
    
#define LMH_LOG_PROFILE(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_PROFILE, fmt, ##__VA_ARGS__)
    
#define LMH_LOG_CUST(fmt, ...) \
    LMH_LOG_MESSAGE(LOG_CUST, fmt, ##__VA_ARGS__)
    
/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

void lmh_log_message(uint32 type, const char* fmt, ...);

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================
**  Function :  lmh_set_thermal_bin
** ==========================================================================
*/
/*!
*
* @brief
*   This function will reset the thermal limits based on a part power bin
*
* @par Dependencies
*   efuse data loaded
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
/* pkoleti - this function is not being called, check the relavance on eldarion */
void lmh_set_thermal_bin(void)
{
  uint32 power, iddq_sum=0;
  uint32 i;
  uint32 cpr_fuse;
  float cpr_volt;
  
  for (i=0; i<TRIM_MAX_CORES; i++)
  {
    iddq_sum+=fuse_info.iddq_core[i].data;
  }
  /* Get open-loop cpr voltage */
  cpr_fuse = in_dword(0xFC4B8458)&(0xFC); //APC1 CPR6_TARG_VOLT_SVS
  cpr_volt = 1.225-((BITS(5,1,cpr_fuse))?(0):((BITS(4,5,cpr_fuse))*0.1))-.100;
  
  /* Find part power */
  power = ( pow(1.1,((cpr_volt-0.9)/0.025)) )*7.552*cpr_volt*(iddq_sum)-7757.58+14777*cpr_volt+7033.33*pow((cpr_volt-1.05),2);
  //85C_Dhrystone_4C+L2 = (1.1^((VOLT-0.9)/0.025))*7.552*VOLT*(IDDq_4X_Fuses)-7757.58+14777*VOLT+7033.33*(VOLT-1.05)^2
  /* Set thermal limits */
  //nothing to set currently
  /* Store calculated power so that TZ can read it. */
  HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_OUTI(0, power);
  
  LMH_LOG_PROFILE("LMH POWER=%u", power);
}

/*===========================================================================
**  Function :  lmh_convert_to_two_comp
** ==========================================================================
*/
/*!
*
* @brief
*   This function will convert a float to a uint32 in 2's comp if negative
*
* @par Dependencies
*   None
*
* @retval
*   uint32 binary encoded decimal
*
* @par Side Effects
*   None
*
*/
uint32 lmh_convert_to_two_comp(float value, float adjust)
{
    uint32 ret;
    
    if (value < 0)
    {
        value = value * -1;
        ret = ((~((uint32)(value/adjust)))&0xFFFF)+1;
    }
    else
    {
        ret = (uint32)(value/adjust);
    }
    return ret;
}

/*===========================================================================
**  Function :  lmh_convert_to_BCD
** ==========================================================================
*/
/*!
*
* @brief
*   This function will convert a float to a uint32 in whole.dec format
*
* @par Dependencies
*   None
*
* @retval
*   uint32 binary encoded decimal
*
* @par Side Effects
*   None
*
*/
uint32 lmh_convert_to_BCD(uint32 whole,uint32 dec,float value, float adjust)
{
    uint32 whole_part, step, dec_part;
    uint32 mask=0;
    
    value = value/adjust;
    
    if (whole > 0 && whole < 32) {
        mask = (0x1<<whole)-1;
    }
    
    whole_part = (((uint32)value)&mask)<<dec;
    dec_part = 0;
    value = value-(float)whole_part;
    for (step=0; step<dec; step++)
    {
        value = value*2;
        if ((uint32)value)
        {
            dec_part+=0x1<<(dec-(step+1));
        }
        value = value-(float)((uint32)value);
    }
    return whole_part+dec_part;
}

/*===========================================================================
**  Function :  lmh_get_time
** ==========================================================================
*/
/*!
*
* @brief
*   This function will return the current # of XO_TICKS as a UINT64
*   Handles lower word overflow.
*
* @par Dependencies
*   None
*
* @retval
*   uint64 time
*
* @par Side Effects
*   None
*
*/
/* pkoleti - TBD */
//APCS_QTMR0_F0V1_QTMR_V1
#define QTimerBase 0xB121000
uint64 lmh_get_time(void)
{
  uint32 upper,  lower;
  upper = in_dword(QTimerBase+4);
  lower = in_dword(QTimerBase);
  while (upper != in_dword(QTimerBase+4))
  {
    upper = in_dword(QTimerBase+4);
    lower = in_dword(QTimerBase);
  }
  return (((uint64)upper<<32)+(uint64)lower);
}

/*===========================================================================
**  Function :  lmh_log_message
** ==========================================================================
*/
/*!
*
* @brief
*   This function will print a message if logging in enabled
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void lmh_log_message(uint32 type, const char* fmt, ...)
{
  if (type & log_bitmask)
  {
    va_list vargs;
    va_start(vargs, fmt);
    //pkoleti - compilation issue - TBD
    //vsnprintf(buf, BUF_SIZE-1, fmt, vargs);
    va_end(vargs);
    boot_log_message(buf);
  }
}


static void lmh_cpu_on(uint32 core)
{
  switch(core)
  {
    case 4:
      CPU_ON_core(4);
      break;
    case 5:
      CPU_ON_core(5);
      break;
    case 6:
      CPU_ON_core(6);
      break;
    case 7:
      CPU_ON_core(7);
      break;
    default:
      break;
  }
}

static void lmh_cpu_off(uint32 core)
{
  switch(core)
  {
    case 4:
      CPU_OFF_core(4);
        break;
    case 5:
     CPU_OFF_core(5);
      break;
    case 6:
      CPU_OFF_core(6);
      break;
    case 7:
      CPU_OFF_core(7);
      break;
    default:
      break;
  }
}

/*===========================================================================
**  Function :  lmh_enable_l2
** ==========================================================================
*/
/*!
*
* @brief
*   This function will configure and turn on the L2, as needed for trim
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void lmh_enable_l2(void)
{
  //turn on L2 by magic numbers
  /* Close Few of the head-switches for L2SCU logic  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x0010F700);
  busywait(2);

  /* Close Rest of the head-switches for L2SCU logic  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x0410F700);
  busywait(2);

  /* Assert PRESETDBG */
  HWIO_OUT_B(APCS_ALIAS1_PWR_CTL_OVERRIDE,0x00400000);
  busywait(2);

  /* Deassert L2/SCU Memory Clamp    */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04103700);
  
  /* Assert L2 memory slp_nret_n */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04103703);
  busywait(4);

  /* Assert L2 memory slp_ret_n  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04101703);
  busywait(4);

  /* Assert L2 memory wl_en_clk */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04101783);
  busywait(1);

  /* De-Assert L2 memory slp_ret_n  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04101703);
  
  /* Enable clocks using software CLK EN  */
  HWIO_OUT_B(APCS_ALIAS1_CORE_CBCR, 0x00000001);
  
  /* De-assert L2/SCU logic Clamp  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04101603);
  busywait(2);

  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x14101603);
    
  //De-assert PRESETDBG 
  //HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL_OVERRIDE,0x00000000);
  //De-assert L2/SCU Logic Reset; 
  //HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04100203);
  //busywait(54);
  //Set PMIC_APC_ON - notdone on  8994
  //HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x14100203);
  //HWIO_OUT_B(APCS_ALIAS1_CORE_CBCR,0x00000003 );
  

  /* turn on 1 A72 to enable the sensors */
  lmh_cpu_on(perf_core_id);   
}


static void lmh_disable_l2(void)
{

  //turn off L2 by magic numbers
  /* Disable clocks by clearing software CLK EN */
  HWIO_OUT_B(APCS_ALIAS1_CORE_CBCR, 0x00000000);

  /* Enable L2/SCU logic Clamp  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04101703);

  /* De-assert L2 memory slp_ret_n */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04103703);
  busywait(4);

  /* De-ssert L2 memory slp_nret_n */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x04103700);
  busywait(4);

 /* Assert L2/SCU Memory Clamp */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x0410F700);

  /* De-Assert PRESETDBG */
  HWIO_OUT_B(APCS_ALIAS1_PWR_CTL_OVERRIDE,0x00000000);
  busywait(2);

  /* Open Rest of the head-switches for L2SCU logic  */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x0010F700);
  busywait(2);

  /* Open Few of the head-switches for L2 SCU logic */
  HWIO_OUT_B(APCS_ALIAS1_L2_PWR_CTL,0x0010FD00);

}


/*===========================================================================
**  Function :  lmh_set_sensor_bypass
** ==========================================================================
*/
/*!
*
* @brief
*   This function will set override current sensor if any sensor init errors
*   encountered.
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void lmh_set_sensor_bypass(void)
{
  /* Set current override */
  HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTI(0, LMH_OVERRIDE_CURRENT);
  HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTI(1, LMH_OVERRIDE_CURRENT);
  HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTI(2, LMH_OVERRIDE_CURRENT);
  HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTI(3, LMH_OVERRIDE_CURRENT);

  HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_OUT(0x0000000F);

  HWIO_APCS_CS_AMP_PERIOD_CTRL_OUT(0x0);
}

/*===========================================================================
**  Function :  lmh_calibrate_opamp
** ==========================================================================
*/
/*!
*
* @brief
*   This function will calibrate op amp.
*
* @par Dependencies
*   None
*
* @retval
*   LMH_SUCCESS or LMH_FAIL
*
* @par Side Effects
*   None
*
*/
uint32 lmh_calibrate_opamp(void)
{
  uint8 retry_cnt = 0;
  uint32 ret_val;
  int i;
  
  do{
    ret_val = LMH_SUCCESS;
  
    /* SW_TRIM must be cleared for retrim  */
    HWIO_OUTF(APCS_CS_AMP_CALIBRATION_CONTROL1,AMP_SW_TRIM_START, 0x0);

    /* pkoleti - HPG recommends SW programs the register 
     CS_AMP_CALIBRATION_CONTROL1 with 0x 14B */
  //HWIO_OUTF(APCS_CS_AMP_CALIBRATION_CONTROL1,AMP_TRIM_TIMER,0x5);     //using default
  //HWIO_OUTF(APCS_CS_AMP_CALIBRATION_CONTROL1,OFFSET, 0x5);            //using default
  HWIO_OUTF(APCS_CS_AMP_CALIBRATION_CONTROL1,AMP_SW_TRIM_START, 0x1);

  while (HWIO_INF(APCS_CS_SENSOR_GENERAL_STATUS,SS_AMPTRIM_DONE) != 0x1)
  {;}

    //Check Status - TBD
    /* pkoleti - on 8994, thre exists one OPAMP for 2 cores, they why checking 8 opAMPs here */
  for (i = 0; i < (HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_MAXi + 1); i++)
  {
      /* pkoleti  - HPG says, [4:1] bit filed contain 3'b0000 
         if calibaration is passed - TBD */
    if (HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_INI(i) & 0x1E)
    {
        ret_val = LMH_FAILED;
    }
  }
  }while(ret_val == LMH_FAILED && ++retry_cnt < 4 );
  
  if(ret_val == LMH_FAILED)
  {
    return  ret_val;
  }

  /* OPTIONAL: Override trim settings - not adding unless needed */
  /*
  //program AMP_SW_OV and AMP_SW_OFFSET_CANCELLATION
  HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_OUT(i,0x0);
  */
  /* pkoleti - HPG mentieons 0x303 - cross check once - TBD*/
  HWIO_APCS_CS_ADC_DATA_SAMPLE_OUT(0x00000303);

  /* OPTIONAL: Override ADC trim fuses - not adding unless needed */
  

  HWIO_APCS_CS_AMP_CALIBRATION_DONE_OUT(0x1);

  while (HWIO_INF(APCS_CS_SENSOR_GENERAL_STATUS,SENSOR_RDY) != 0x1)
  {;}

  return LMH_SUCCESS;
}

/*===========================================================================
**  Function :  lmh_sensor_init
** ==========================================================================
*/
/*!
*
* @brief
*   Setup the sensor.
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void lmh_sensor_init(void)
{
  int i = 0;
  
  /* base control programming */
  /* Isense team recommended to have BHS sternchg 1(0x3f) */
  HWIO_APCS_CS_BHS_STRENGTH_OUT(0x3F); //full strength
  for(i = 0; i <= HWIO_APCS_CS_A_SENSOR_CTRL_i_MAXi; i++)
  {
    HWIO_APCS_CS_A_SENSOR_CTRL_i_OUTI(i, 0x0170080);  
  }

  /* OPTIONAL: LDO settings - not adding unless needed */
  //HWIO_APCS_CS_LDO_TIMER_OUT(0x28);                      //default
  //HWIO_APCS_CS_CLAMP_TIMER_OUT(0x28);                    //default
  //HWIO_APCS_CS_BANDGAP_TIMER_OUT(0x28);                //new setting
  //HWIO_APCS_CS_AMP_TIMER_OUT(0x28);                //new setting
  //HWIO_APCS_CS_PD_TIMER_OUT(0x28);                //new setting
  //HWIO_APCS_CS_ADC_TIMER_OUT(0x28);                //new setting

  //HWIO_APCS_CS_BANDGAP_TIMER_OUT(0x500);                //new setting

  /* Setup ISENSE */
  /* ADC Analog control */
  /* CS_A_ADC_CTRL_CORE_i - is not requied on Eldaroin as these are
     removed from ISENSE2 */
  
  /* Bandgap control removed on Eldarion */
  
  /* Enable sensors */
  HWIO_APCS_CS_ENABLE_REG_OUT(0xF);                     //turns on all ISENSE

  /* wait for sensors to turn on */
  while (HWIO_INF(APCS_CS_SENSOR_GENERAL_STATUS, CS_PWR_ON_STATUS) != 0x1)
  {;}
}


/*===========================================================================
FUNCTION: void lmh_trim_endpoint(void)

DESCRIPTION:

RETURN VALUE: uint32
===========================================================================*/
uint32 lmh_trim_endpoint(void)
{
    static int iteration = 0;
    uint32 nCnt = 0;
    //volatile uint64_t start, end, delta;
    
    for( nCnt = 0; nCnt < TRIM_MAX_CORES; nCnt++ ){
    
    HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_OUT(0x0);
    HWIO_OUTF_B(APCS_CS_ENDPOINT_CALIBRATION_CTRL, TRIM_NUM_AVG, LMH_SAMPLES);
      HWIO_OUTF_B(APCS_CS_ENDPOINT_CALIBRATION_CTRL, TRIM_ENABLE, LMH_TRIM_CORE_SEL<<nCnt);
    
      if (iteration != 0 || nCnt != 0) {
        /* 1st time core4 is truned on by lmh_enable_l2 so not required to
          to turn on here */
        lmh_cpu_on(4 + nCnt);
    }
    
      if (iteration != 0 || freq[0] != 0) {
         /* UNGATE CLOCKS - only do this after the first measurement
         if first frequency to be applied is zero */
         HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_OUT(0x1<<nCnt);
        busywait(1);
    }
    
      //start measurement
      HWIO_OUTF_B(APCS_CS_ENDPOINT_CALIBRATION_CTRL, CAL_ENABLE, 0x1);

    while (HWIO_INF(APCS_CS_CALIBRATION_STATUS, DONE_FLAG) != 0x1) {;}
    
      //GATE CLOCKS
    HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_OUT(0x0);
    
      lmh_cpu_off(4 + nCnt);
    if (HWIO_INF(APCS_CS_CALIBRATION_STATUS, TERMINATION) != 0x0) {
        return LMH_FAILED;
    }
		
    }  // for( nCnt = 0; nCnt < TRIM_MAX_CORES; nCnt++ )

    iteration++;
    return LMH_SUCCESS;
}
/*===========================================================================
**  Function :  load_efuse_data
** ==========================================================================
*/
/*!
*
* @brief
*   This function will load efuse data
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
//pkolet - TBD
static void load_efuse_data(void)
{
  uint32 read_buf;
  //pkoleti - compilation issued
  //boot_boolean valid;
  uint32 i;
  
  memset(&fuse_info, 0x0, sizeof(struct fuse_set));

  read_buf = 0;
  read_buf = HWIO_INF(QFPROM_CORR_CALIB_ROW11_MSB, MULTIPLICATION_FACTOR);
  fuse_info.iddq_mult.data = read_buf;
  fuse_info.iddq_mult.valid = TRUE;
  
  read_buf = HWIO_INF(QFPROM_CORR_CALIB_ROW11_MSB, A72_WORST_CORE_NUM);
  fuse_info.worst_core.data = read_buf;
  fuse_info.worst_core.valid = TRUE;
  
  //Fill the core iddq values and roll over on 0x11
  fuse_info.iddq_core[((read_buf) & (0x3))].data = 
    HWIO_INF(QFPROM_RAW_CALIB_ROW11_LSB, IDDQ_A72_MAX_ACTIVE_MEAS_FUSE_BITS) * 
    fuse_info.iddq_mult.data;

  fuse_info.iddq_core[((read_buf) & (0x3))].valid = TRUE;

  fuse_info.iddq_core[((++read_buf) & (0x3))].data = 
    HWIO_INF(QFPROM_RAW_CALIB_ROW11_LSB, IDDQ_A72_MAX_P1_ACTIVE_MEAS_FUSE_BITS) *
    fuse_info.iddq_mult.data;
  fuse_info.iddq_core[((read_buf) & (0x3))].valid = TRUE;  

  fuse_info.iddq_core[((++read_buf) & (0x3))].data = 
    HWIO_INF(QFPROM_RAW_CALIB_ROW11_LSB, IDDQ_A72_MAX_P2_ACTIVE_MEAS_FUSE_BITS) *
    fuse_info.iddq_mult.data;
  fuse_info.iddq_core[((read_buf) & (0x3))].valid = TRUE;  

  fuse_info.iddq_core[((++read_buf) & (0x3))].data = 
    HWIO_INF(QFPROM_RAW_CALIB_ROW11_LSB, IDDQ_A72_MAX_P3_ACTIVE_MEAS_FUSE_BITS) *
    fuse_info.iddq_mult.data;
  fuse_info.iddq_core[((read_buf) & (0x3))].valid = TRUE;

#if 0
  fuse_info.bg_current_trim.data = 
    HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, CS_A_BG_CURRENTTRIM);
  fuse_info.bg_current_trim.valid = TRUE;
  
  //BMASK 0xF stored in CS_A_BG_CURRENTRIM
  //BMASK 0x10 stored in CS_A_BG_CTRL[6]
  if (fuse_info.bg_current_trim.data == 0)
    fuse_info.bg_current_trim.valid = FALSE;
  
  //Represented in APCS_CS_A_ADC_TRIM
  fuse_info.adc_trim.data = 
    HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, CS_A_ADC_TRIM);
  fuse_info.adc_trim.valid = TRUE;
  
  if (fuse_info.adc_trim.data == 0)
    fuse_info.adc_trim.valid = FALSE;
  
  fuse_info.slope.data = 
    HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, CS_SLOPE);
  fuse_info.slope.valid = TRUE;
  
  if (fuse_info.slope.data == 0)
    fuse_info.slope.valid = FALSE;

  fuse_info.intercept[0].data = 
     HWIO_INF(QFPROM_RAW_CALIB_ROW0_MSB, CS_SENSOR_PARAM_CORE_0_INTERCEPT_2_0) |
      (HWIO_INF(QFPROM_RAW_CALIB_ROW1_LSB, CS_SENSOR_PARAM_CORE_0_INTERCEPT_3) << 0x3 ) |
     (HWIO_INF(QFPROM_RAW_CALIB_ROW1_LSB, CS_SENSOR_PARAM_CORE_0_INTERCEPT_9_4) << 0x4 );
  fuse_info.intercept[0].valid = TRUE;

  fuse_info.intercept[1].data = 
     HWIO_INF(QFPROM_RAW_CALIB_ROW1_LSB, CS_SENSOR_PARAM_CORE_1_INTERCEPT);
  fuse_info.intercept[1].valid = TRUE;

  fuse_info.intercept[2].data = 
     HWIO_INF(QFPROM_RAW_CALIB_ROW1_LSB, CS_SENSOR_PARAM_CORE_2_INTERCEPT_6_0) |
     (HWIO_INF(QFPROM_RAW_CALIB_ROW1_MSB, CS_SENSOR_PARAM_CORE_2_INTERCEPT_9_7) << 0x7 );
  fuse_info.intercept[2].valid = TRUE;

  fuse_info.intercept[3].data = 
     HWIO_INF(QFPROM_RAW_CALIB_ROW1_MSB, CS_SENSOR_PARAM_CORE_3_INTERCEPT);
  fuse_info.intercept[3].valid = TRUE;
#endif  

  for (i = 0; i < TRIM_MAX_CORES; i++)
  {
    if (fuse_info.intercept[i].data == 0)
      fuse_info.intercept[i].valid = FALSE;	
  }
   
  for (i = 0; i < TRIM_MAX_CORES; i++)
  {
    if (fuse_info.iddq_core[i].data == 0)
    {
      fuse_info.iddq_core[i].valid = FALSE;
      bUseDefaultSlopeIntercept = TRUE;
    }  
  }
 
   
}

/*===========================================================================
**  Function :  lmh_store_measurement
** ==========================================================================
*/
/*!
*
* @brief
*   This function stores the current measured trim values
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

static void lmh_store_measurement(uint32 measurement)
{
    //core dynamic current = Cdyn(core)*Vtrim*Ftrim*#cores trimmed*1000mA
    double dyn_core_mA = (CDYN_CORE*V_TRIM*(((double)(trim_data.m_freq[measurement]))/G_HERTZ)*LMH_NUM_TRIM_CORES)*1000;
    float core_tot_local=0; /* total power for core pair */
    double iddq;
    double temp;
    uint32 core,t;
    
    LMH_LOG_PROFILE("-Measurement[%u]", measurement);
    LMH_LOG_PROFILE("->Voltage=%uV[/1000]", (uint32)(V_TRIM*1000.0));
    LMH_LOG_PROFILE("->Freq=%uGHz[/1000]", (uint32)((((float)(trim_data.m_freq[measurement]))/G_HERTZ)*1000.0));
    LMH_LOG_PROFILE("->Idyn_core[4mA]=%u[/1000]", (uint32)((dyn_core_mA/LSB_mA)*1000.0));
    
#ifdef LMH_DEBUG_ENABLE
    lmh_debug_data_p->core_dyn_pw[measurement] = dyn_core_mA;
#endif

    /* pkoleti -  For Eldarion, 2 ADCs per core - just re-visit the code again. */
    for (t=0; t<TRIM_MAX_CORES; t+=CORES_PER_ADC) { 
       /* few targets may have 1 ADC for 2 cores */
       core = t/CORES_PER_ADC; //space saving
       iddq = (double)(fuse_info.iddq_core[t].data); //first core on ADC
       temp = (double)trim_data.corei_temp[t];
        
       core_tot_local  = (float)((iddq)*pow(10,(V_TRIM-V_IDDQ)/K_0)*pow(10,(temp-T_IDDQ)/K_1));

#ifdef LMH_DEBUG_ENABLE
    lmh_debug_data_p->core_static_pw[t][measurement] = core_tot_local;
#endif

        core_tot_local += (float)(dyn_core_mA);
        core_tot_local = core_tot_local/LSB_mA;
        
        trim_data.corei_dign[core][measurement] = LMH_CS_CALIBRATION_RESULT_COREn_0_IN(t);
        
        trim_data.corei_tot_SU[core] += core_tot_local;
        trim_data.corei_dig_tot_SU[core] += core_tot_local*trim_data.corei_dign[core][measurement];
        trim_data.corei_dig_SU[core] += trim_data.corei_dign[core][measurement];
        trim_data.corei_dig_SSQ[core] += (trim_data.corei_dign[core][measurement])*(trim_data.corei_dign[core][measurement]);
        
        LMH_LOG_PROFILE("--ADC[%u]", core);
        LMH_LOG_PROFILE("-->Ptotal[4mA]=%u[/1000]", (uint32)((core_tot_local)*1000.0));
        LMH_LOG_PROFILE("---Core[%u]", t);
       LMH_LOG_PROFILE("--->IDDQ=%umA[/1000]", (uint32)(iddq*1000.0));
       LMH_LOG_PROFILE("--->temp=%uC[/1000]", (uint32)(temp*1000.0));
        LMH_LOG_PROFILE("--->digital=%u", LMH_CS_CALIBRATION_RESULT_COREn_0_IN(t));
    }
}

/*===========================================================================
**  Function :  store_temp_data
** ==========================================================================
*/
/*!
*
* @brief
*   This function stores the current measured temp values
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/

static uint32 store_temp_data(void)
{
    int32 core_temp;
    if (Tsens_GetTemp(CORE_4_TSENSE_ID, &(core_temp)) != TSENS_SUCCESS) {
      return LMH_FAILED;
    }
    trim_data.corei_temp[0] = (uint32)core_temp;
    if (Tsens_GetTemp(CORE_5_TSENSE_ID, &(core_temp)) != TSENS_SUCCESS) {
      return LMH_FAILED;
    }
    trim_data.corei_temp[1] = (uint32)core_temp;
    if (Tsens_GetTemp(CORE_6_TSENSE_ID, &(core_temp)) != TSENS_SUCCESS) {
      return LMH_FAILED;
    }
    trim_data.corei_temp[2] = (uint32)core_temp;
    if (Tsens_GetTemp(CORE_7_TSENSE_ID, &(core_temp)) != TSENS_SUCCESS) {
      return LMH_FAILED;
    }
    trim_data.corei_temp[3] = (uint32)core_temp;
    return LMH_SUCCESS;
}

/*===========================================================================
**  Function :  lmh_config_default_slope_intercept
** ==========================================================================
*/
/*!
*
* @brief
*   This function configures default slope and intercept values.
*
* @par Dependencies
*   None
*
*
* @par Side Effects
*   None
*
*/
static void lmh_config_default_slope_intercept( void )
{
    /* Default slope and intercept values
    	  
    The values ATE calcuted are slope = 31.5 and intercept = -665.
    The ISense has 13 bit interface to LMH so it can represent upto 4096 values.
    So we need to scale the LSB to accommodate the current range we are expecting
    to measure. Since we are targeting about 9A, we would need 4 mA per LSB. This 
    would allow measuring upto 16A with 4096 values.

    So the equation is y = (mx+b)/4.
    Since we are trying to represent a fractional slope, we can represent it as 
    numerator / denominator. Choosing the denomintoar as 8 allows us to represent 
    slope values with .125 (i.e. 1/8) resolution. But we would need to multiply 
    numerator by 8 too. I.e m = 31.5*8/8 = 252/8
		
    Similarly we need to multiply the intercept by 8 (shift by 3) to nullify
    the effect of the devide by 8. Since we are laso devideing the entire equation 
    by 4 to scale the LSB, the final denominator becomes 32
    (i.e *8 for .125 fractional reolution, and *4 for LSB alignment  = * 32). 
    This is a shift by 5

    y = (31.5*8 *x -665 * 8 ) / (8 * 4)    = (252*x  655 * 8)/ (32)
    1.	SLOPE = 0xFC
    2.	INTERCEPT=0xFD67
    3.	APCS_CS_DECIMAL_ALIGN  [CURRENTRESULT_OFFSET_VALUE]=5,
    4.	APCS_CS_DECIMAL_ALIGN  INTERCEPT_OFFSET_VALUE]=3
	
    The intercept range that can be programmed directly is -512 to 511. 
    To achieve a larger value, we have to multiply the offset using intercept_offset
    CSRs. So by having an addition multiplier of 4 on the offset 
    (increase from 3 to 5), the supported offset range can be extended to
    -2048 to 2044.
	
	so new values would be:
    y = (31.5*8 *x - (665/4) * 4 * 8 ) / (8 * 4)    = (252*x  (655/4)* 4 * 8)/ (32)
    1.	SLOPE = 0xFC
    2.	INTERCEPT=0xFF5C (2'complement of 655/4 = 164)
    3.	APCS_CS_DECIMAL_ALIGN  [CURRENTRESULT_OFFSET_VALUE]=5,
    4.	APCS_CS_DECIMAL_ALIGN  INTERCEPT_OFFSET_VALUE]=5
    */	
	
    int i=0;
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE, DEFAULT_ADC_0_SLOPE);
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, DEFAULT_ADC_0_INTERCEPT);

    i=1;
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE, DEFAULT_ADC_1_SLOPE);
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, DEFAULT_ADC_1_INTERCEPT);

    i=2;
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE, DEFAULT_ADC_2_SLOPE);
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, DEFAULT_ADC_2_INTERCEPT);

    i=3;
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE, DEFAULT_ADC_3_SLOPE);
    HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, DEFAULT_ADC_3_INTERCEPT);
}
/*===========================================================================
**  Function :  lmh_trim
** ==========================================================================
*/
/*!
*
* @brief
*   This function will perform current sensor trim and override current sensor
*   if any sensor init errors encountered.
*
* @par Dependencies
*   None
*
* @retval
*   LMH_SUCCESS or LMH_FAIL
*
* @par Side Effects
*   None
*
*/
static uint32 lmh_trim(cal_info_v1_t *data, boot_boolean valid)
{
  uint32 i;
  
  //uint32 set_freq = 0x0;
  //uint32 start_freq = 0x0;
  uint32 ret_val = LMH_SUCCESS;
  uint32 slope_int, intercept_int;
  
  float tot_sum, dig_ssq, dig_sum, dig_tot_sum, det;
  float slope, intercept;
  
  LMH_LOG_MSG("LMH: Sensor trim start.");
  do {
    /* 
      Enable APC1 Rail -- 
      if this fails we need to fail boot as the rail to access LMH won't be on
    */
    if (Clock_LMhInit(TRIM_VOLTAGE) == FALSE) {
      ret_val = LMH_FAILED_CLOCK_INIT;
      LMH_LOG_PROFILE("!!CLOCK VOLTAGE FAILED!!");
      break;
    }

    lmh_enable_l2();
	
    load_efuse_data();
    lmh_sensor_init();
    
#if 0    
    LMH_LOG_PROFILE("## fused slope %u", fuse_info.slope.data);
    for(i = 0; i < TRIM_MAX_CORES; i++)
    {
      LMH_LOG_PROFILE("## fused intercept for core %d: %u", i, fuse_info.intercept[i].data);
    }//for(i = 0; i < TRIM_MAX_CORES; i++)
#endif
    
    for (i = 0; i < (HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi + 1); i++) {
      LMH_LOG_PROFILE("## stored slope[%u] %u", i, data->info[i].slope);
      LMH_LOG_PROFILE("## stored intercept[%u] %u", i, data->info[i].offset);
    }//for (i = 0; i < (HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi + 1); i++)

    /* Calibrate opamp */
    if (lmh_calibrate_opamp() == LMH_FAILED) {
      ret_val = LMH_FAILED_OPAMP;
      LMH_LOG_PROFILE("!!OPAMP CAL FAILED!!");
      break;
    }

    /* Use NV data */
    if (valid && 1 != bEnableReTrim)
    {
      LMH_LOG_MSG("LMH USING PARTITION DATA");
      for (i = 0; i < (HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi + 1); i++) {
        HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE,
                        (data->info[i].slope));
        HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, 
                        (data->info[i].offset));
      }
      ret_val = LMH_SUCCESS;
      break;
    }//if (valid)
    
    if(bUseDefaultSlopeIntercept)
    {
        LMH_LOG_MSG("LMH USING Default slope and intercept DATA");
	lmh_config_default_slope_intercept();
	ret_val = LMH_SUCCESS;
	break;
    }
  
    /* Check temperature and fail if cores are not below TRIM_TEMP_THRESHOLD */
    /* Get temp data */
    if (store_temp_data() == LMH_FAILED) {
      ret_val = LMH_FAILED_GET_TEMP;
      LMH_LOG_PROFILE("!!GET TEMP FAILED!!");
      break;
    }
    /* Check core temp */
    for (i = 0; i < (HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi + 1); i++) {
      if (trim_data.corei_temp[i] > TRIM_TEMP_THRESHOLD)
      {
        LMH_LOG_PROFILE("LMH: FAIL TRIM - TOO HOT!");
        ret_val = LMH_FAILED_TEMP_HIGH;
        break;
      }
    }//for (i = 0; i < (HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi + 1); i++)
    
    if (ret_val != LMH_SUCCESS) {
      break;
    }
    
    LMH_LOG_PROFILE("LMH: start endpoint trim");
    LMH_LOG_PROFILE("># Trim Cores=%u", LMH_NUM_TRIM_CORES);
    LMH_LOG_PROFILE(">Cdyn_core[nF]=%unF[/1000]", (uint32)((float)CDYN_CORE*1000.0));
    LMH_LOG_PROFILE(">Cdyn_L2[nF]=%unF", (uint32)((float)CDYN_L2*1000.0));
    LMH_LOG_PROFILE(">IDDQ measurement voltage=%uV", (uint32)(V_IDDQ*1000.0));
    LMH_LOG_PROFILE(">IDDQ measurement temp=%uC", T_IDDQ);

    /* Take endpoint measurement */
    for (i = 0; i<M_COUNT; i++) {
      /* ENABLE THE PLL */
      if (i > 0 || 0 != freq[i] ) { /* no clock on first test if freq is zero*/
        trim_data.m_freq[i] = Clock_LMhPreCalibration(freq[i]);
        if (trim_data.m_freq[i] == FALSE) {
          ret_val = LMH_FAILED_CHANGE_CLOCK;
          LMH_LOG_PROFILE("!!CLOCK CHANGE FAILED!!");
          break;
        }
      }
      
      /* Get temp data */
      if (store_temp_data() == LMH_FAILED) {
        ret_val = LMH_FAILED_GET_TEMP;
        LMH_LOG_PROFILE("!!GET TEMP FAILED!!");
        break;
      }
      
      /* Take measurement */
      if (lmh_trim_endpoint() == LMH_FAILED) {
        ret_val = LMH_FAILED_ENDPOINT;
        break;
      }
      /* Store measurement */
      lmh_store_measurement(i);
    }//for (i = 0; i<M_COUNT; i++)

    if (ret_val != LMH_SUCCESS) break;
    
    /* Calculate slope+offset */
    for (i=0; i<TRIM_MAX_CORES; i+=CORES_PER_ADC) {
      uint32 core=i/CORES_PER_ADC; /* space saving */
      
      LMH_LOG_PROFILE("-ADC[%u]", core);
      
      tot_sum = trim_data.corei_tot_SU[core];
      dig_ssq = (float)trim_data.corei_dig_SSQ[core];
      dig_sum = (float)trim_data.corei_dig_SU[core];
      dig_tot_sum = trim_data.corei_dig_tot_SU[core];
      
      det = 1.0/((dig_ssq*M_COUNT)-(dig_sum*dig_sum));
      
      slope = (det * ( (M_COUNT*dig_tot_sum)-(dig_sum*tot_sum) ));
      intercept = (det * ( (dig_ssq*tot_sum)-(dig_sum*dig_tot_sum) ));
      
#ifdef LMH_DEBUG_ENABLE
      lmh_debug_data_p->slope[i] = slope;
      lmh_debug_data_p->intercept[i] = intercept;

#endif
      //only on 8976 - based on profiling data, refer description above
      slope_int = (slope - ISENSE_SLOPE_OFFSET) * LMH_SENSOR_SLOPE_ADJ;
      intercept_int = lmh_convert_to_two_comp(intercept, LMH_SENSOR_INTERCEPT_ADJ);
      
      //slope_int = lmh_convert_to_BCD(0,10,slope, LMH_SENSOR_ADJ);
      
      if (intercept < 0)
      {
        LMH_LOG_PROFILE("->INTERCEPT NEGATIVE");
      }
      
      HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, SLOPE, slope_int);
      HWIO_OUTFI(APCS_CS_SENSOR_PARAM_CORE_i, i, INTERCEPT, intercept_int);
      
      LMH_LOG_CUST("->Calc slope=%u[/1000]", (uint32)(slope*1000));
      LMH_LOG_CUST("->Calc intercept=%u[/1000]", (uint32)((intercept<0?intercept*-1:intercept)*1000));
      LMH_LOG_CUST("->PROG slope=%X[hex]", slope_int);
      LMH_LOG_CUST("->PROG intercept=%X[hex]", intercept_int);
      data->info[i].slope = slope_int;
      data->info[i].offset = intercept_int;
    }//for (i=0; i<TRIM_MAX_CORES; i+=CORES_PER_ADC)

  } while (0);
  
  if(perf_core_id == 4)
  {
  /* if CORE 4 is still on, turn it off. */
  if (HWIO_IN( APCS_ALIAS4_CPU_PWR_CTL) == 0x3C)
  {
    CPU_OFF_core(4);
    }
  }
  else
  {
    lmh_cpu_off(perf_core_id);
  }
  /* End ENDPOINT Trim */
  HWIO_OUTF(APCS_CS_ENDPOINT_CALIBRATION_CTRL, CAL_ENABLE, 0x0);
  /* Settings this bit by SW, will transition the HW from trim mode to functional mode. */
  HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_OUT(0x1);
  
  /* Finalize programming */
  if (ret_val == LMH_FAILED_OPAMP) {
    /* On OP AMP trim failure, cannot trust ODCM, bypass instead */
    lmh_set_sensor_bypass();
    LMH_LOG_MSG("LMH: OP AMP Trim error.");
  } else {
    /* Select trimmed data as fuses are not configured */
    if ( (ret_val != LMH_SUCCESS) || (1 == bUseDefaultSlopeIntercept) ) {
	lmh_config_default_slope_intercept();
    }//if ( (ret_val != LMH_SUCCESS) || (1 == bUseDefaultSlopeIntercept) )
      
      HWIO_OUTF(APCS_CS_DECIMAL_ALIGN, CURRENTRESULT_OFFSET_VALUE, 5);
    HWIO_OUTF(APCS_CS_DECIMAL_ALIGN, INTERCEPT_OFFSET_VALUE, 5);
    
    /* Set fuse override */
    HWIO_OUTF(APCS_CS_SW_OV_FUSE_EN, INTERCEPTTRIM_EN, 0x1);
    HWIO_OUTF(APCS_CS_SW_OV_FUSE_EN, SLOPETRIM_EN, 0x1);
    
    /* configure ISense2 low power features */
    HWIO_OUTF(APCS_CS_NEW_FEATURE_ENABLES, EN_PER_CS_POR, 0x1);
    HWIO_OUTF(APCS_CS_NEW_FEATURE_ENABLES, EN_RESTART_ON_BHSEN, 0x1);
    HWIO_OUTF(APCS_CS_NEW_FEATURE_ENABLES, EN_EARLY_BHSEN, 0x1);
    
    /* Configure Periodic Trim */
    //TODO: Enable periodic trim once given go ahead
    //HWIO_APCS_CS_AMP_PERIOD_CTRL_OUT(0x001001A4);
    HWIO_OUTF(APCS_CS_AMP_PERIOD_CTRL, TRIM_ENABLE, 0x0);;
  }

  /* Switch to CXO before clock off to return to reset state */
  Clock_LMhPreCalibration(19200);
  
  /* L2 Clock Gate */
    lmh_disable_l2();

  /* Turn off trim clocks */
  Clock_LMhDeInit();
  LMH_LOG_MSG("LMH: Sensor trim done.");
  
  //thermal limit adjustment based on part binning
  //lmh_set_thermal_bin();
  
  return ret_val;
}

/*===========================================================================
**  Function :  validate_data_from_partition
** ==========================================================================
*/
/*!
*
* @brief
*   This function will validate partition data
*
* @par Dependencies
*   None
*
* @retval
*   TRUE or FALSE
*
* @par Side Effects
*   None
*
*/
static boot_boolean validate_data_from_partition
(
  struct partition_data *buf
)
{
#ifdef LMH_USES_PARTITION
  uint32 i;

  if (buf == NULL)
    return FALSE;
    
  if (buf->magic_num != LMH_MAGIC_NUM)
    return FALSE;

  if (buf->version != LMH_VERSION)
    return FALSE;

  if (checksum16(checksum_arr, sizeof(checksum_arr)/sizeof(checksum_arr[0])) != buf->checksum)
    return FALSE;
  
  /* Verify that payload data is valid */
  for (i = 0; i < TRIM_MAX_CORES; i++)
  {
    if (buf->payload.v1.info[i].slope != 0 || buf->payload.v1.info[i].offset != 0)
    {
      continue;
    }
    return FALSE;
  }

  return TRUE;
#else
  return FALSE;
#endif
}

/*===========================================================================
**  Function :  load_data_from_partition
** ==========================================================================
*/
/*!
*
* @brief
*   This function will read the limits data from limits partition
*
* @par Dependencies
*   Must be called before qsee init
*
* @retval
*   0 on SUCCESS
*
* @par Side Effects
*   None
*
*/
static int load_data_from_partition
(
  uint8 *data_buf,
  uint32 data_size
)
{
#ifdef LMH_USES_PARTITION
  boot_boolean success = FALSE;
  boot_flash_trans_if_type *trans_if = NULL;
  int ret = 0;

  /* Verify inputs are valid  */
  //TBD

  LMH_LOG_MSG("LMH: READING FROM PARTITION.");
  
  boot_flash_configure_target_image((uint8*)&limits_params_partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);
  
  if (trans_if == NULL)
  {
    LMH_LOG_ERROR("UNABLE TO OPEN LMH PARTITION FOR READING");
#ifndef NO_LMH_PARTITION_ABORT
    BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR );
#endif
    return 1;
  }

  /* Allow the address range of the ddr_training_data_buf to be written to */
  boot_clobber_add_local_hole( boot_flash_trans_get_clobber_tbl_ptr( trans_if ),
                               data_buf, data_size );

  success = boot_flash_trans_read(trans_if,
                                  data_buf,
                                  0,
                                  data_size);

  BL_VERIFY(success, BL_ERR_SBL);

  /* close partition */
  boot_flash_dev_close_image(&trans_if);
  return ret;
#else
  return 0;
#endif
}

/*===========================================================================
**  Function :  save_data_to_partition
** ==========================================================================
*/
/*!
*
* @brief
*   This function will save ddr training data to ddr partition
*
* @param[in] ddr_training_data_ptr Pointer to ddr training data
*
* @param[in] ddr_training_data_size size of ddr training data
*
* @par Dependencies
*   Must be called after ddr_post_init
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static void save_data_to_partition
(
  uint8* data_ptr,
  uint32 data_size
)
{
#ifdef LMH_USES_PARTITION
  boot_flash_trans_if_type *trans_if = NULL;
  boot_boolean success = FALSE;

  LMH_LOG_MSG("LMH: SAVING TO PARTITION.");
  
  /* Write DDR training data to storage device */
  boot_flash_configure_target_image((uint8*)&limits_params_partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  if (trans_if == NULL)
  {
    LMH_LOG_ERROR("UNABLE TO OPEN LMH PARTITION FOR WRITING");
#ifndef NO_LMH_PARTITION_ABORT
    BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR );
#endif
    return;
  }
  //BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR );

  success = dev_sdcc_write_bytes(data_ptr,
                                 0,
                                 data_size,
                                 GEN_IMG);

  BL_VERIFY(success, BL_ERR_SBL);

  /* close partition  */
  boot_flash_dev_close_image(&trans_if);
#endif
}

/*===========================================================================

**  Function :  limits_early_init

** ==========================================================================
*/
/*!
*
*    Must performs limits driver init in secure execution environment
*
* @return
*  None.
*
* @par Dependencies
*  None
*
*
*/

void limits_early_init(uint32 flag)
{
  DalChipInfoVersionType version = DalChipInfo_ChipVersion();
  DalChipInfoIdType chip_id = DalChipInfo_ChipId();
  boot_boolean valid = FALSE;
  uint32 ret_val = LMH_FAILED;
  cal_info_v1_t *payload;
  uint64 start_time,end_time;
  uint32 i;
  
  if(bSBLLMhDisabled == 1)
  {
	return;
  }

  if(chip_id != DALCHIPINFO_ID_MSM8976 && chip_id != DALCHIPINFO_ID_APQ8076)
  {
    /* Use default slope and intercept values */
    bUseDefaultSlopeIntercept = 1;
	
    /* find out which cores are fused out */
    if(HWIO_INF(QFPROM_CORR_FEAT_CONFIG_ROW2_MSB, APPS_CFGCPUPRESENT_N_4))
    {
      if(HWIO_INF(QFPROM_CORR_FEAT_CONFIG_ROW2_MSB, APPS_CFGCPUPRESENT_N_5))
      {
        if(HWIO_INF(QFPROM_CORR_FEAT_CONFIG_ROW2_MSB, APPS_CFGCPUPRESENT_N_6))
        {
          perf_core_id = 7;
        }
        else
        {
          perf_core_id = 6;
        }
      }
      else
      {
        perf_core_id = 5;
      }
    }
  }
  
  LMH_LOG_INFO("LMH START: %lu", lmh_get_time());
  start_time=lmh_get_time();
  /* Init partition data */
  memset(&part_data, 0x0, sizeof(part_data));

  if (load_data_from_partition((uint8*)&part_data, sizeof(part_data)) == 0)
  {
    valid = validate_data_from_partition(&part_data);
  }
  else
  {
    valid = FALSE;
  }
  
  /* invalidate partition if *PERFORM_SW_TRIM* flag is set */
  if ((flag&0x1))
  {
    valid = FALSE;
    log_bitmask |= LOG_CUST;
  }

  payload = &part_data.payload.v1;
  ret_val = lmh_trim(payload, valid);
  
  /* store partition write count */
  ((uint32*)part_data.payload.v1.debug)[0]++;
  
  /* capture debug data on failure */
  if (ret_val != LMH_SUCCESS) 
  {
    /* find next debug slot */
    for (i = 2; i < (DEBUG_BYTES/sizeof(uint32))-2; i+=2)
    {
      if ( ((uint32*)part_data.payload.v1.debug)[i] == 0 && 
            ((uint32*)part_data.payload.v1.debug)[i+1] == 0) 
      {
        break;
      }
    }
    /* store boot on which failure occurred */
    ((uint32*)part_data.payload.v1.debug)[i] = ((uint32*)part_data.payload.v1.debug)[0];
    /* store failure code */
    ((uint32*)part_data.payload.v1.debug)[i+1] = ret_val;
    
    /* ensure partition invalidation */
    part_data.payload.v1.info[0].slope = 0;
    part_data.payload.v1.info[0].offset = 0;
  }
  
  if ( valid == FALSE || ret_val != LMH_SUCCESS )
  {
    part_data.magic_num = LMH_MAGIC_NUM;
    part_data.version = LMH_VERSION;
    
    /* store unique MSM ID */
    //((uint32*)part_data.debug)[1] = 
    
    /* Calculate checksum */
    part_data.checksum = checksum16(checksum_arr, 
                                    sizeof(checksum_arr) / 
                                    sizeof(checksum_arr[0]));
                                    
    /* Store data to partition */
    save_data_to_partition((uint8*)&part_data, sizeof(part_data));
  }
  end_time=lmh_get_time();
  LMH_LOG_INFO("LMH DELTA: %lu", end_time-start_time);
  LMH_LOG_INFO("LMH END: %lu", end_time);
}
