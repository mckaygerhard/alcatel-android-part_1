#ifndef __LMH_LLM_HWIO_H__
#define __LMH_LLM_HWIO_H__
/*
===========================================================================
*/
/**
  @file lmh_llm_hwio.h
  @brief Auto-generated HWIO interface include file.


  Reference chip release:
    MSM8956 (Eldarion) [eldarion_v1.0_p3q3r41_MTO]

  This file contains HWIO register definitions for the following modules:
    APCS_LMH_LITE
    APCS_LLM_ELESSAR
    APCS_LLM_ELESSAR_NONSEC
    APCS_LMH_LITE_NONSEC
    APCS_ISENSE_CONTROLLER

  'Include' filters applied: <none>
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/power/limits/src/8976/lmh_llm_hwio.h#1 $
  $DateTime: 2015/07/02 04:11:47 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"



/*----------------------------------------------------------------------------
 * MODULE: APCS_LMH_LITE
 *--------------------------------------------------------------------------*/

#define APCS_LMH_LITE_REG_BASE                                                (A53SS_BASE      + 0x001d8000)

#define HWIO_APCS_LMH_VER_ADDR                                                (APCS_LMH_LITE_REG_BASE      + 0x0000039c)
#define HWIO_APCS_LMH_VER_RMSK                                                0xffffffff
#define HWIO_APCS_LMH_VER_IN          \
        in_dword_masked(HWIO_APCS_LMH_VER_ADDR, HWIO_APCS_LMH_VER_RMSK)
#define HWIO_APCS_LMH_VER_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_VER_ADDR, m)
#define HWIO_APCS_LMH_VER_VERSION_BMSK                                        0xffffffff
#define HWIO_APCS_LMH_VER_VERSION_SHFT                                               0x0

#define HWIO_APCS_LMH_ACCESS_CTRL_ADDR                                        (APCS_LMH_LITE_REG_BASE      + 0x00000398)
#define HWIO_APCS_LMH_ACCESS_CTRL_RMSK                                          0x1fffff
#define HWIO_APCS_LMH_ACCESS_CTRL_IN          \
        in_dword_masked(HWIO_APCS_LMH_ACCESS_CTRL_ADDR, HWIO_APCS_LMH_ACCESS_CTRL_RMSK)
#define HWIO_APCS_LMH_ACCESS_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_ACCESS_CTRL_ADDR, m)
#define HWIO_APCS_LMH_ACCESS_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_LMH_ACCESS_CTRL_ADDR,v)
#define HWIO_APCS_LMH_ACCESS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LMH_ACCESS_CTRL_ADDR,m,v,HWIO_APCS_LMH_ACCESS_CTRL_IN)
#define HWIO_APCS_LMH_ACCESS_CTRL_ACCESS_CTRL_BMSK                              0x1fffff
#define HWIO_APCS_LMH_ACCESS_CTRL_ACCESS_CTRL_SHFT                                   0x0

#define HWIO_APCS_LMH_PWRDN_MODE_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000394)
#define HWIO_APCS_LMH_PWRDN_MODE_RMSK                                                0x3
#define HWIO_APCS_LMH_PWRDN_MODE_IN          \
        in_dword_masked(HWIO_APCS_LMH_PWRDN_MODE_ADDR, HWIO_APCS_LMH_PWRDN_MODE_RMSK)
#define HWIO_APCS_LMH_PWRDN_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_PWRDN_MODE_ADDR, m)
#define HWIO_APCS_LMH_PWRDN_MODE_OUT(v)      \
        out_dword(HWIO_APCS_LMH_PWRDN_MODE_ADDR,v)
#define HWIO_APCS_LMH_PWRDN_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LMH_PWRDN_MODE_ADDR,m,v,HWIO_APCS_LMH_PWRDN_MODE_IN)
#define HWIO_APCS_LMH_PWRDN_MODE_A57_CNSTRNT_RSP_CLKON_BMSK                          0x2
#define HWIO_APCS_LMH_PWRDN_MODE_A57_CNSTRNT_RSP_CLKON_SHFT                          0x1
#define HWIO_APCS_LMH_PWRDN_MODE_A53_CNSTRNT_RSP_CLKON_BMSK                          0x1
#define HWIO_APCS_LMH_PWRDN_MODE_A53_CNSTRNT_RSP_CLKON_SHFT                          0x0

#define HWIO_APCS_LMH_CLK_RATIO_ADDR                                          (APCS_LMH_LITE_REG_BASE      + 0x00000004)
#define HWIO_APCS_LMH_CLK_RATIO_RMSK                                               0x1ff
#define HWIO_APCS_LMH_CLK_RATIO_IN          \
        in_dword_masked(HWIO_APCS_LMH_CLK_RATIO_ADDR, HWIO_APCS_LMH_CLK_RATIO_RMSK)
#define HWIO_APCS_LMH_CLK_RATIO_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_CLK_RATIO_ADDR, m)
#define HWIO_APCS_LMH_CLK_RATIO_OUT(v)      \
        out_dword(HWIO_APCS_LMH_CLK_RATIO_ADDR,v)
#define HWIO_APCS_LMH_CLK_RATIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LMH_CLK_RATIO_ADDR,m,v,HWIO_APCS_LMH_CLK_RATIO_IN)
#define HWIO_APCS_LMH_CLK_RATIO_DIV_EN_BMSK                                        0x100
#define HWIO_APCS_LMH_CLK_RATIO_DIV_EN_SHFT                                          0x8
#define HWIO_APCS_LMH_CLK_RATIO_DIV_CLK_RATIO_BMSK                                  0xff
#define HWIO_APCS_LMH_CLK_RATIO_DIV_CLK_RATIO_SHFT                                   0x0

#define HWIO_APCS_A57_PS_N_ADDR                                               (APCS_LMH_LITE_REG_BASE      + 0x00000008)
#define HWIO_APCS_A57_PS_N_RMSK                                                     0xff
#define HWIO_APCS_A57_PS_N_IN          \
        in_dword_masked(HWIO_APCS_A57_PS_N_ADDR, HWIO_APCS_A57_PS_N_RMSK)
#define HWIO_APCS_A57_PS_N_INM(m)      \
        in_dword_masked(HWIO_APCS_A57_PS_N_ADDR, m)
#define HWIO_APCS_A57_PS_N_OUT(v)      \
        out_dword(HWIO_APCS_A57_PS_N_ADDR,v)
#define HWIO_APCS_A57_PS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_A57_PS_N_ADDR,m,v,HWIO_APCS_A57_PS_N_IN)
#define HWIO_APCS_A57_PS_N_PS_N_BMSK                                                0xff
#define HWIO_APCS_A57_PS_N_PS_N_SHFT                                                 0x0

#define HWIO_APCS_A53_PS_N_ADDR                                               (APCS_LMH_LITE_REG_BASE      + 0x0000000c)
#define HWIO_APCS_A53_PS_N_RMSK                                                     0xff
#define HWIO_APCS_A53_PS_N_IN          \
        in_dword_masked(HWIO_APCS_A53_PS_N_ADDR, HWIO_APCS_A53_PS_N_RMSK)
#define HWIO_APCS_A53_PS_N_INM(m)      \
        in_dword_masked(HWIO_APCS_A53_PS_N_ADDR, m)
#define HWIO_APCS_A53_PS_N_OUT(v)      \
        out_dword(HWIO_APCS_A53_PS_N_ADDR,v)
#define HWIO_APCS_A53_PS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_A53_PS_N_ADDR,m,v,HWIO_APCS_A53_PS_N_IN)
#define HWIO_APCS_A53_PS_N_PS_N_BMSK                                                0xff
#define HWIO_APCS_A53_PS_N_PS_N_SHFT                                                 0x0

#define HWIO_APCS_LMH_MISC_CTRL1_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000010)
#define HWIO_APCS_LMH_MISC_CTRL1_RMSK                                         0xffffffff
#define HWIO_APCS_LMH_MISC_CTRL1_IN          \
        in_dword_masked(HWIO_APCS_LMH_MISC_CTRL1_ADDR, HWIO_APCS_LMH_MISC_CTRL1_RMSK)
#define HWIO_APCS_LMH_MISC_CTRL1_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_MISC_CTRL1_ADDR, m)
#define HWIO_APCS_LMH_MISC_CTRL1_OUT(v)      \
        out_dword(HWIO_APCS_LMH_MISC_CTRL1_ADDR,v)
#define HWIO_APCS_LMH_MISC_CTRL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LMH_MISC_CTRL1_ADDR,m,v,HWIO_APCS_LMH_MISC_CTRL1_IN)
#define HWIO_APCS_LMH_MISC_CTRL1_MISC_BMSK                                    0xffffffff
#define HWIO_APCS_LMH_MISC_CTRL1_MISC_SHFT                                           0x0

#define HWIO_APCS_LMH_MISC_CTRL2_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000014)
#define HWIO_APCS_LMH_MISC_CTRL2_RMSK                                         0xffffffff
#define HWIO_APCS_LMH_MISC_CTRL2_IN          \
        in_dword_masked(HWIO_APCS_LMH_MISC_CTRL2_ADDR, HWIO_APCS_LMH_MISC_CTRL2_RMSK)
#define HWIO_APCS_LMH_MISC_CTRL2_INM(m)      \
        in_dword_masked(HWIO_APCS_LMH_MISC_CTRL2_ADDR, m)
#define HWIO_APCS_LMH_MISC_CTRL2_OUT(v)      \
        out_dword(HWIO_APCS_LMH_MISC_CTRL2_ADDR,v)
#define HWIO_APCS_LMH_MISC_CTRL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LMH_MISC_CTRL2_ADDR,m,v,HWIO_APCS_LMH_MISC_CTRL2_IN)
#define HWIO_APCS_LMH_MISC_CTRL2_MISC_BMSK                                    0xffffffff
#define HWIO_APCS_LMH_MISC_CTRL2_MISC_SHFT                                           0x0

#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_ADDR                                    (APCS_LMH_LITE_REG_BASE      + 0x00000020)
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_RMSK                                           0x7
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_IN          \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_ENABLE_ADDR, HWIO_APCS_PMIC_CNSTRNT_ENABLE_RMSK)
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_ENABLE_ADDR, m)
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_CNSTRNT_ENABLE_ADDR,v)
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_CNSTRNT_ENABLE_ADDR,m,v,HWIO_APCS_PMIC_CNSTRNT_ENABLE_IN)
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_OP_MODE_BMSK                                   0x6
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_OP_MODE_SHFT                                   0x1
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_ENABLE_BMSK                                    0x1
#define HWIO_APCS_PMIC_CNSTRNT_ENABLE_ENABLE_SHFT                                    0x0

#define HWIO_APCS_PMIC_CNSTRNT_STATUS_ADDR                                    (APCS_LMH_LITE_REG_BASE      + 0x00000024)
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_RMSK                                           0x7
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_STATUS_ADDR, HWIO_APCS_PMIC_CNSTRNT_STATUS_RMSK)
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_STATUS_ADDR, m)
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_OP_MODE_BMSK                                   0x6
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_OP_MODE_SHFT                                   0x1
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_ENABLE_BMSK                                    0x1
#define HWIO_APCS_PMIC_CNSTRNT_STATUS_ENABLE_SHFT                                    0x0

#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_ADDR(i)                              (APCS_LMH_LITE_REG_BASE      + 0x00000028 + 0x4 * (i))
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_RMSK                                     0x1fff
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_MAXi                                         16
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_INI(i)        \
        in_dword_masked(HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_ADDR(i), HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_RMSK)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_ADDR(i), mask)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_OUTI(i,val)    \
        out_dword(HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_ADDR(i),val)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_ADDR(i),mask,val,HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_INI(i))
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_IB_LIMIT_BMSK                            0x1fff
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_IB_IB_LIMIT_SHFT                               0x0

#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_ADDR(i)                               (APCS_LMH_LITE_REG_BASE      + 0x00000068 + 0x4 * (i))
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_RMSK                                  0xffffffff
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_MAXi                                          16
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_INI(i)        \
        in_dword_masked(HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_ADDR(i), HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_RMSK)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_ADDR(i), mask)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_OUTI(i,val)    \
        out_dword(HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_ADDR(i),val)
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_ADDR(i),mask,val,HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_INI(i))
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_1_BMSK                              0xff000000
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_1_SHFT                                    0x18
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_2_BMSK                                0xff0000
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_2_SHFT                                    0x10
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_3_BMSK                                  0xff00
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_3_SHFT                                     0x8
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_4_BMSK                                    0xff
#define HWIO_APCS_PMIC_RESP_TBL_ROW_i_M_M_4_SHFT                                     0x0

#define HWIO_APCS_PMIC_DELTA_M_ADDR                                           (APCS_LMH_LITE_REG_BASE      + 0x000000ac)
#define HWIO_APCS_PMIC_DELTA_M_RMSK                                               0xffff
#define HWIO_APCS_PMIC_DELTA_M_IN          \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_ADDR, HWIO_APCS_PMIC_DELTA_M_RMSK)
#define HWIO_APCS_PMIC_DELTA_M_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_ADDR, m)
#define HWIO_APCS_PMIC_DELTA_M_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_DELTA_M_ADDR,v)
#define HWIO_APCS_PMIC_DELTA_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_DELTA_M_ADDR,m,v,HWIO_APCS_PMIC_DELTA_M_IN)
#define HWIO_APCS_PMIC_DELTA_M_DELTA_M_UNTHRTL_BMSK                               0xff00
#define HWIO_APCS_PMIC_DELTA_M_DELTA_M_UNTHRTL_SHFT                                  0x8
#define HWIO_APCS_PMIC_DELTA_M_DELTA_M_THRTL_BMSK                                   0xff
#define HWIO_APCS_PMIC_DELTA_M_DELTA_M_THRTL_SHFT                                    0x0

#define HWIO_APCS_PMIC_SW_OVRD_MN_ADDR                                        (APCS_LMH_LITE_REG_BASE      + 0x000000b0)
#define HWIO_APCS_PMIC_SW_OVRD_MN_RMSK                                            0xffff
#define HWIO_APCS_PMIC_SW_OVRD_MN_IN          \
        in_dword_masked(HWIO_APCS_PMIC_SW_OVRD_MN_ADDR, HWIO_APCS_PMIC_SW_OVRD_MN_RMSK)
#define HWIO_APCS_PMIC_SW_OVRD_MN_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_SW_OVRD_MN_ADDR, m)
#define HWIO_APCS_PMIC_SW_OVRD_MN_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_SW_OVRD_MN_ADDR,v)
#define HWIO_APCS_PMIC_SW_OVRD_MN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_SW_OVRD_MN_ADDR,m,v,HWIO_APCS_PMIC_SW_OVRD_MN_IN)
#define HWIO_APCS_PMIC_SW_OVRD_MN_M_BMSK                                          0xff00
#define HWIO_APCS_PMIC_SW_OVRD_MN_M_SHFT                                             0x8
#define HWIO_APCS_PMIC_SW_OVRD_MN_N_BMSK                                            0xff
#define HWIO_APCS_PMIC_SW_OVRD_MN_N_SHFT                                             0x0

#define HWIO_APCS_PMIC_ERR_MODE_MN_ADDR                                       (APCS_LMH_LITE_REG_BASE      + 0x000000b4)
#define HWIO_APCS_PMIC_ERR_MODE_MN_RMSK                                           0xffff
#define HWIO_APCS_PMIC_ERR_MODE_MN_IN          \
        in_dword_masked(HWIO_APCS_PMIC_ERR_MODE_MN_ADDR, HWIO_APCS_PMIC_ERR_MODE_MN_RMSK)
#define HWIO_APCS_PMIC_ERR_MODE_MN_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_ERR_MODE_MN_ADDR, m)
#define HWIO_APCS_PMIC_ERR_MODE_MN_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_ERR_MODE_MN_ADDR,v)
#define HWIO_APCS_PMIC_ERR_MODE_MN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_ERR_MODE_MN_ADDR,m,v,HWIO_APCS_PMIC_ERR_MODE_MN_IN)
#define HWIO_APCS_PMIC_ERR_MODE_MN_M_BMSK                                         0xff00
#define HWIO_APCS_PMIC_ERR_MODE_MN_M_SHFT                                            0x8
#define HWIO_APCS_PMIC_ERR_MODE_MN_N_BMSK                                           0xff
#define HWIO_APCS_PMIC_ERR_MODE_MN_N_SHFT                                            0x0

#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_ADDR                                (APCS_LMH_LITE_REG_BASE      + 0x000000bc)
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_RMSK                                0xffffffff
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_IN          \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_ADDR, HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_RMSK)
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_ADDR, m)
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_ADDR,v)
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_ADDR,m,v,HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_IN)
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_THRSHLD_BMSK                        0xffffffff
#define HWIO_APCS_PMIC_THRTTL_CNT_THRSHLD_THRSHLD_SHFT                               0x0

#define HWIO_APCS_PMIC_WINDOW_SIZE_ADDR                                       (APCS_LMH_LITE_REG_BASE      + 0x000000c0)
#define HWIO_APCS_PMIC_WINDOW_SIZE_RMSK                                       0xffffffff
#define HWIO_APCS_PMIC_WINDOW_SIZE_IN          \
        in_dword_masked(HWIO_APCS_PMIC_WINDOW_SIZE_ADDR, HWIO_APCS_PMIC_WINDOW_SIZE_RMSK)
#define HWIO_APCS_PMIC_WINDOW_SIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_WINDOW_SIZE_ADDR, m)
#define HWIO_APCS_PMIC_WINDOW_SIZE_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_WINDOW_SIZE_ADDR,v)
#define HWIO_APCS_PMIC_WINDOW_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_WINDOW_SIZE_ADDR,m,v,HWIO_APCS_PMIC_WINDOW_SIZE_IN)
#define HWIO_APCS_PMIC_WINDOW_SIZE_WINDOW_SIZE_BMSK                           0xffffffff
#define HWIO_APCS_PMIC_WINDOW_SIZE_WINDOW_SIZE_SHFT                                  0x0

#define HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_ADDR                                 (APCS_LMH_LITE_REG_BASE      + 0x000000c4)
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_RMSK                                        0x1
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_ADDR,v)
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_RST_BMSK                                    0x1
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_RST_RST_SHFT                                    0x0

#define HWIO_APCS_PMIC_CNSTRNT_RST_ADDR                                       (APCS_LMH_LITE_REG_BASE      + 0x000000c8)
#define HWIO_APCS_PMIC_CNSTRNT_RST_RMSK                                              0x1
#define HWIO_APCS_PMIC_CNSTRNT_RST_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_CNSTRNT_RST_ADDR,v)
#define HWIO_APCS_PMIC_CNSTRNT_RST_RST_BMSK                                          0x1
#define HWIO_APCS_PMIC_CNSTRNT_RST_RST_SHFT                                          0x0

#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_ADDR                                    (APCS_LMH_LITE_REG_BASE      + 0x000000cc)
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_RMSK                                        0xffff
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_IN          \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_MIN_MAX_ADDR, HWIO_APCS_PMIC_THRTTL_MIN_MAX_RMSK)
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_MIN_MAX_ADDR, m)
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_MAX_M_BMSK                                  0xff00
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_MAX_M_SHFT                                     0x8
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_MIN_M_BMSK                                    0xff
#define HWIO_APCS_PMIC_THRTTL_MIN_MAX_MIN_M_SHFT                                     0x0

#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000d0)
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_RMSK                                   0xffffffff
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_IN          \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_CNT_STAT_ADDR, HWIO_APCS_PMIC_THRTTL_CNT_STAT_RMSK)
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_THRTTL_CNT_STAT_ADDR, m)
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_OVERFLOW_BMSK                          0x80000000
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_OVERFLOW_SHFT                                0x1f
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_THRTTL_CNT_BMSK                        0x7fffffff
#define HWIO_APCS_PMIC_THRTTL_CNT_STAT_THRTTL_CNT_SHFT                               0x0

#define HWIO_APCS_PMIC_ACCUM_M_UPPER_ADDR                                     (APCS_LMH_LITE_REG_BASE      + 0x000000d4)
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_RMSK                                     0xffffffff
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_IN          \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_M_UPPER_ADDR, HWIO_APCS_PMIC_ACCUM_M_UPPER_RMSK)
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_M_UPPER_ADDR, m)
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_OVERFLOW_BMSK                            0x80000000
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_OVERFLOW_SHFT                                  0x1f
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_M_UPPER_BMSK                             0x7fffffff
#define HWIO_APCS_PMIC_ACCUM_M_UPPER_M_UPPER_SHFT                                    0x0

#define HWIO_APCS_PMIC_ACCUM_M_LOWER_ADDR                                     (APCS_LMH_LITE_REG_BASE      + 0x000000d8)
#define HWIO_APCS_PMIC_ACCUM_M_LOWER_RMSK                                     0xffffffff
#define HWIO_APCS_PMIC_ACCUM_M_LOWER_IN          \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_M_LOWER_ADDR, HWIO_APCS_PMIC_ACCUM_M_LOWER_RMSK)
#define HWIO_APCS_PMIC_ACCUM_M_LOWER_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_M_LOWER_ADDR, m)
#define HWIO_APCS_PMIC_ACCUM_M_LOWER_M_LOWER_BMSK                             0xffffffff
#define HWIO_APCS_PMIC_ACCUM_M_LOWER_M_LOWER_SHFT                                    0x0

#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_ADDR                                  (APCS_LMH_LITE_REG_BASE      + 0x000000dc)
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_RMSK                                  0xffffffff
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_IN          \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_ADDR, HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_RMSK)
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_ADDR, m)
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_OVERFLOW_BMSK                         0x80000000
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_OVERFLOW_SHFT                               0x1f
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_THRTTL_CNT_BMSK                       0x7fffffff
#define HWIO_APCS_PMIC_ACCUM_THRTTL_CNT_THRTTL_CNT_SHFT                              0x0

#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_ADDR                                (APCS_LMH_LITE_REG_BASE      + 0x000000e0)
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_RMSK                                0xffffffff
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_IN          \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_ADDR, HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_RMSK)
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_ADDR, m)
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_OVERFLOW_BMSK                       0x80000000
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_OVERFLOW_SHFT                             0x1f
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_TIMER_TICK_BMSK                     0x7fffffff
#define HWIO_APCS_PMIC_CNSTRNT_TIMER_STAT_TIMER_TICK_SHFT                            0x0

#define HWIO_APCS_PMIC_THRSHLD_MAPPING_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000e4)
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_RMSK                                     0xffffff
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_IN          \
        in_dword_masked(HWIO_APCS_PMIC_THRSHLD_MAPPING_ADDR, HWIO_APCS_PMIC_THRSHLD_MAPPING_RMSK)
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_THRSHLD_MAPPING_ADDR, m)
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_THRSHLD_MAPPING_ADDR,v)
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_THRSHLD_MAPPING_ADDR,m,v,HWIO_APCS_PMIC_THRSHLD_MAPPING_IN)
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_111_BMSK                          0xe00000
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_111_SHFT                              0x15
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_110_BMSK                          0x1c0000
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_110_SHFT                              0x12
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_101_BMSK                           0x38000
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_101_SHFT                               0xf
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_100_BMSK                            0x7000
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_100_SHFT                               0xc
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_011_BMSK                             0xe00
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_011_SHFT                               0x9
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_010_BMSK                             0x1c0
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_010_SHFT                               0x6
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_001_BMSK                              0x38
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_001_SHFT                               0x3
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_000_BMSK                               0x7
#define HWIO_APCS_PMIC_THRSHLD_MAPPING_T3T2T1_000_SHFT                               0x0

#define HWIO_APCS_PMIC_SPARE_RESPONSE0_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000e8)
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_RMSK                                       0xffff
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_IN          \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE0_ADDR, HWIO_APCS_PMIC_SPARE_RESPONSE0_RMSK)
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE0_ADDR, m)
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_SPARE_RESPONSE0_ADDR,v)
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_SPARE_RESPONSE0_ADDR,m,v,HWIO_APCS_PMIC_SPARE_RESPONSE0_IN)
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_SPARE_RESP0_N_BMSK                         0xff00
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_SPARE_RESP0_N_SHFT                            0x8
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_SPARE_RESP0_M_BMSK                           0xff
#define HWIO_APCS_PMIC_SPARE_RESPONSE0_SPARE_RESP0_M_SHFT                            0x0

#define HWIO_APCS_PMIC_SPARE_RESPONSE1_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000ec)
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_RMSK                                       0xffff
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_IN          \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE1_ADDR, HWIO_APCS_PMIC_SPARE_RESPONSE1_RMSK)
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE1_ADDR, m)
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_SPARE_RESPONSE1_ADDR,v)
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_SPARE_RESPONSE1_ADDR,m,v,HWIO_APCS_PMIC_SPARE_RESPONSE1_IN)
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_SPARE_RESP1_N_BMSK                         0xff00
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_SPARE_RESP1_N_SHFT                            0x8
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_SPARE_RESP1_M_BMSK                           0xff
#define HWIO_APCS_PMIC_SPARE_RESPONSE1_SPARE_RESP1_M_SHFT                            0x0

#define HWIO_APCS_PMIC_SPARE_RESPONSE2_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000f0)
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_RMSK                                       0xffff
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_IN          \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE2_ADDR, HWIO_APCS_PMIC_SPARE_RESPONSE2_RMSK)
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE2_ADDR, m)
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_SPARE_RESPONSE2_ADDR,v)
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_SPARE_RESPONSE2_ADDR,m,v,HWIO_APCS_PMIC_SPARE_RESPONSE2_IN)
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_SPARE_RESP2_N_BMSK                         0xff00
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_SPARE_RESP2_N_SHFT                            0x8
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_SPARE_RESP2_M_BMSK                           0xff
#define HWIO_APCS_PMIC_SPARE_RESPONSE2_SPARE_RESP2_M_SHFT                            0x0

#define HWIO_APCS_PMIC_SPARE_RESPONSE3_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000000f4)
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_RMSK                                       0xffff
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_IN          \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE3_ADDR, HWIO_APCS_PMIC_SPARE_RESPONSE3_RMSK)
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_SPARE_RESPONSE3_ADDR, m)
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_SPARE_RESPONSE3_ADDR,v)
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_SPARE_RESPONSE3_ADDR,m,v,HWIO_APCS_PMIC_SPARE_RESPONSE3_IN)
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_SPARE_RESP3_N_BMSK                         0xff00
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_SPARE_RESP3_N_SHFT                            0x8
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_SPARE_RESP3_M_BMSK                           0xff
#define HWIO_APCS_PMIC_SPARE_RESPONSE3_SPARE_RESP3_M_SHFT                            0x0

#define HWIO_APCS_PMIC_TIMER_ENABLE_ADDR                                      (APCS_LMH_LITE_REG_BASE      + 0x000000f8)
#define HWIO_APCS_PMIC_TIMER_ENABLE_RMSK                                             0x1
#define HWIO_APCS_PMIC_TIMER_ENABLE_IN          \
        in_dword_masked(HWIO_APCS_PMIC_TIMER_ENABLE_ADDR, HWIO_APCS_PMIC_TIMER_ENABLE_RMSK)
#define HWIO_APCS_PMIC_TIMER_ENABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_TIMER_ENABLE_ADDR, m)
#define HWIO_APCS_PMIC_TIMER_ENABLE_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_TIMER_ENABLE_ADDR,v)
#define HWIO_APCS_PMIC_TIMER_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_TIMER_ENABLE_ADDR,m,v,HWIO_APCS_PMIC_TIMER_ENABLE_IN)
#define HWIO_APCS_PMIC_TIMER_ENABLE_ENABLE_BMSK                                      0x1
#define HWIO_APCS_PMIC_TIMER_ENABLE_ENABLE_SHFT                                      0x0

#define HWIO_APCS_PMIC_FIFO_ENTRY_ADDR                                        (APCS_LMH_LITE_REG_BASE      + 0x000000fc)
#define HWIO_APCS_PMIC_FIFO_ENTRY_RMSK                                               0x3
#define HWIO_APCS_PMIC_FIFO_ENTRY_IN          \
        in_dword_masked(HWIO_APCS_PMIC_FIFO_ENTRY_ADDR, HWIO_APCS_PMIC_FIFO_ENTRY_RMSK)
#define HWIO_APCS_PMIC_FIFO_ENTRY_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_FIFO_ENTRY_ADDR, m)
#define HWIO_APCS_PMIC_FIFO_ENTRY_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_FIFO_ENTRY_ADDR,v)
#define HWIO_APCS_PMIC_FIFO_ENTRY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_FIFO_ENTRY_ADDR,m,v,HWIO_APCS_PMIC_FIFO_ENTRY_IN)
#define HWIO_APCS_PMIC_FIFO_ENTRY_ENTRY_USED_BMSK                                    0x3
#define HWIO_APCS_PMIC_FIFO_ENTRY_ENTRY_USED_SHFT                                    0x0

#define HWIO_APCS_PMIC_INTR_TABLE_ADDR                                        (APCS_LMH_LITE_REG_BASE      + 0x00000100)
#define HWIO_APCS_PMIC_INTR_TABLE_RMSK                                              0xff
#define HWIO_APCS_PMIC_INTR_TABLE_IN          \
        in_dword_masked(HWIO_APCS_PMIC_INTR_TABLE_ADDR, HWIO_APCS_PMIC_INTR_TABLE_RMSK)
#define HWIO_APCS_PMIC_INTR_TABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_INTR_TABLE_ADDR, m)
#define HWIO_APCS_PMIC_INTR_TABLE_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_INTR_TABLE_ADDR,v)
#define HWIO_APCS_PMIC_INTR_TABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_INTR_TABLE_ADDR,m,v,HWIO_APCS_PMIC_INTR_TABLE_IN)
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_111_BMSK                                   0x80
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_111_SHFT                                    0x7
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_110_BMSK                                   0x40
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_110_SHFT                                    0x6
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_101_BMSK                                   0x20
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_101_SHFT                                    0x5
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_100_BMSK                                   0x10
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_100_SHFT                                    0x4
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_011_BMSK                                    0x8
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_011_SHFT                                    0x3
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_010_BMSK                                    0x4
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_010_SHFT                                    0x2
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_001_BMSK                                    0x2
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_001_SHFT                                    0x1
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_000_BMSK                                    0x1
#define HWIO_APCS_PMIC_INTR_TABLE_T3T2T1_000_SHFT                                    0x0

#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_ADDR(i)                               (APCS_LMH_LITE_REG_BASE      + 0x00000124 + 0x4 * (i))
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_RMSK                                      0x3fff
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_MAXi                                          12
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_INI(i)        \
        in_dword_masked(HWIO_APCS_PMIC_THRESHOLDi_LIMIT_ADDR(i), HWIO_APCS_PMIC_THRESHOLDi_LIMIT_RMSK)
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_PMIC_THRESHOLDi_LIMIT_ADDR(i), mask)
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_OUTI(i,val)    \
        out_dword(HWIO_APCS_PMIC_THRESHOLDi_LIMIT_ADDR(i),val)
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_PMIC_THRESHOLDi_LIMIT_ADDR(i),mask,val,HWIO_APCS_PMIC_THRESHOLDi_LIMIT_INI(i))
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_LIMIT_BMSK                                0x3fff
#define HWIO_APCS_PMIC_THRESHOLDi_LIMIT_LIMIT_SHFT                                   0x0

#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_ADDR                               (APCS_LMH_LITE_REG_BASE      + 0x00000158)
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_RMSK                                      0x1
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_IN          \
        in_dword_masked(HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_ADDR, HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_RMSK)
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_ADDR, m)
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_ADDR,v)
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_ADDR,m,v,HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_IN)
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_SEL_BMSK                                  0x1
#define HWIO_APCS_PMIC_THRESHOLD_LIMIT_SEL_SEL_SHFT                                  0x0

#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_ADDR                             (APCS_LMH_LITE_REG_BASE      + 0x0000015c)
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_RMSK                             0xffffffff
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_IN          \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_ADDR, HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_RMSK)
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_ADDR, m)
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_ADDR,v)
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_ADDR,m,v,HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_IN)
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_TIMER_BMSK                       0xffffffff
#define HWIO_APCS_PMIC_DELTA_M_STEP_UP_TIMER_TIMER_SHFT                              0x0

#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_ADDR                           (APCS_LMH_LITE_REG_BASE      + 0x00000160)
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_RMSK                           0xffffffff
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_IN          \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_ADDR, HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_RMSK)
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_ADDR, m)
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_ADDR,v)
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_ADDR,m,v,HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_IN)
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_TIMER_BMSK                     0xffffffff
#define HWIO_APCS_PMIC_DELTA_M_STEP_DOWN_TIMER_TIMER_SHFT                            0x0

#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ADDR(i)                      (APCS_LMH_LITE_REG_BASE      + 0x00000164 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_RMSK                                0x7
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_MAXi                                  3
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_INI(i))
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_MODE_BMSK                           0x6
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_MODE_SHFT                           0x1
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ENABLE_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A53_COREi_ENABLE_SHFT                         0x0

#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ADDR(i)                      (APCS_LMH_LITE_REG_BASE      + 0x00000174 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_RMSK                                0x7
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_MAXi                                  3
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_INI(i))
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_MODE_BMSK                           0x6
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_MODE_SHFT                           0x1
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ENABLE_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_ENABLE_A57_COREi_ENABLE_SHFT                         0x0

#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_ADDR(i)                     (APCS_LMH_LITE_REG_BASE      + 0x00000184 + 0x4 * (i))
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_RMSK                        0xffffffff
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_MAXi                                 3
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_ADDR(i), HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_RMSK)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_INI(i))
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_1_BMSK                    0xff000000
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_1_SHFT                          0x18
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_2_BMSK                      0xff0000
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_2_SHFT                          0x10
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_3_BMSK                        0xff00
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_3_SHFT                           0x8
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_4_BMSK                          0xff
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A53_COREi_M_4_SHFT                           0x0

#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_ADDR(i)                     (APCS_LMH_LITE_REG_BASE      + 0x00000194 + 0x4 * (i))
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_RMSK                        0xffffffff
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_MAXi                                 3
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_ADDR(i), HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_RMSK)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_INI(i))
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_1_BMSK                    0xff000000
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_1_SHFT                          0x18
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_2_BMSK                      0xff0000
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_2_SHFT                          0x10
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_3_BMSK                        0xff00
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_3_SHFT                           0x8
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_4_BMSK                          0xff
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A53_COREi_M_4_SHFT                           0x0

#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_ADDR(i)                     (APCS_LMH_LITE_REG_BASE      + 0x000001a4 + 0x4 * (i))
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_RMSK                        0xffffffff
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_MAXi                                 3
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_ADDR(i), HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_RMSK)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_INI(i))
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_1_BMSK                    0xff000000
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_1_SHFT                          0x18
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_2_BMSK                      0xff0000
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_2_SHFT                          0x10
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_3_BMSK                        0xff00
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_3_SHFT                           0x8
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_4_BMSK                          0xff
#define HWIO_APCS_THRML_RESP_TBL_HIVIOL_A57_COREi_M_4_SHFT                           0x0

#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_ADDR(i)                     (APCS_LMH_LITE_REG_BASE      + 0x000001b4 + 0x4 * (i))
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_RMSK                        0xffffffff
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_MAXi                                 3
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_ADDR(i), HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_RMSK)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_ADDR(i),mask,val,HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_INI(i))
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_1_BMSK                    0xff000000
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_1_SHFT                          0x18
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_2_BMSK                      0xff0000
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_2_SHFT                          0x10
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_3_BMSK                        0xff00
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_3_SHFT                           0x8
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_4_BMSK                          0xff
#define HWIO_APCS_THRML_RESP_TBL_LOVIOL_A57_COREi_M_4_SHFT                           0x0

#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_ADDR(i)                   (APCS_LMH_LITE_REG_BASE      + 0x000001c4 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_RMSK                             0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_MAXi                               3
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_RST_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A53_COREi_RST_SHFT                         0x0

#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_ADDR(i)                   (APCS_LMH_LITE_REG_BASE      + 0x000001d4 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_RMSK                             0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_MAXi                               3
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_RST_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_RST_A57_COREi_RST_SHFT                         0x0

#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_ADDR(i)                         (APCS_LMH_LITE_REG_BASE      + 0x000001e4 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_RMSK                                   0x1
#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_MAXi                                     3
#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_RST_BMSK                               0x1
#define HWIO_APCS_THRML_CNSTRNT_RST_A53_COREi_RST_SHFT                               0x0

#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_ADDR(i)                         (APCS_LMH_LITE_REG_BASE      + 0x000001f4 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_RMSK                                   0x1
#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_MAXi                                     3
#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_OUTI(i,val)    \
        out_dword(HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_ADDR(i),val)
#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_RST_BMSK                               0x1
#define HWIO_APCS_THRML_CNSTRNT_RST_A57_COREi_RST_SHFT                               0x0

#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_ADDR(i)                      (APCS_LMH_LITE_REG_BASE      + 0x00000204 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_RMSK                                0x7
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_MAXi                                  3
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_OP_MODE_BMSK                        0x6
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_OP_MODE_SHFT                        0x1
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_ENABLE_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A53_COREi_ENABLE_SHFT                         0x0

#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_ADDR(i)                      (APCS_LMH_LITE_REG_BASE      + 0x00000214 + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_RMSK                                0x7
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_MAXi                                  3
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_OP_MODE_BMSK                        0x6
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_OP_MODE_SHFT                        0x1
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_ENABLE_BMSK                         0x1
#define HWIO_APCS_THRML_CNSTRNT_STATUS_A57_COREi_ENABLE_SHFT                         0x0

#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_ADDR(i)                          (APCS_LMH_LITE_REG_BASE      + 0x00000224 + 0x4 * (i))
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_RMSK                                    0x3
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_MAXi                                      3
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_FSM_STATUS_A53_COREi_ADDR(i), HWIO_APCS_THRML_FSM_STATUS_A53_COREi_RMSK)
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_FSM_STATUS_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_STATE_BMSK                              0x3
#define HWIO_APCS_THRML_FSM_STATUS_A53_COREi_STATE_SHFT                              0x0

#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_ADDR(i)                          (APCS_LMH_LITE_REG_BASE      + 0x00000234 + 0x4 * (i))
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_RMSK                                    0x3
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_MAXi                                      3
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_FSM_STATUS_A57_COREi_ADDR(i), HWIO_APCS_THRML_FSM_STATUS_A57_COREi_RMSK)
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_FSM_STATUS_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_STATE_BMSK                              0x3
#define HWIO_APCS_THRML_FSM_STATUS_A57_COREi_STATE_SHFT                              0x0

#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_ADDR(i)                        (APCS_LMH_LITE_REG_BASE      + 0x00000264 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_RMSK                           0xffffffff
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_MAXi                                    3
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_ADDR(i), HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_OVERFLOW_BMSK                  0x80000000
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_OVERFLOW_SHFT                        0x1f
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_TIMER_CNT_BMSK                 0x7fffffff
#define HWIO_APCS_THRML_TIME_HI_PERF_A53_COREi_TIMER_CNT_SHFT                        0x0

#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_ADDR(i)                        (APCS_LMH_LITE_REG_BASE      + 0x00000274 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_RMSK                           0xffffffff
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_MAXi                                    3
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_ADDR(i), HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_OVERFLOW_BMSK                  0x80000000
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_OVERFLOW_SHFT                        0x1f
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_TIMER_CNT_BMSK                 0x7fffffff
#define HWIO_APCS_THRML_TIME_HI_PERF_A57_COREi_TIMER_CNT_SHFT                        0x0

#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_ADDR(i)                        (APCS_LMH_LITE_REG_BASE      + 0x00000284 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_RMSK                           0xffffffff
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_MAXi                                    3
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_ADDR(i), HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_OVERFLOW_BMSK                  0x80000000
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_OVERFLOW_SHFT                        0x1f
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_TIMER_CNT_BMSK                 0x7fffffff
#define HWIO_APCS_THRML_TIME_LO_PERF_A53_COREi_TIMER_CNT_SHFT                        0x0

#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_ADDR(i)                        (APCS_LMH_LITE_REG_BASE      + 0x00000294 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_RMSK                           0xffffffff
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_MAXi                                    3
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_ADDR(i), HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_OVERFLOW_BMSK                  0x80000000
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_OVERFLOW_SHFT                        0x1f
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_TIMER_CNT_BMSK                 0x7fffffff
#define HWIO_APCS_THRML_TIME_LO_PERF_A57_COREi_TIMER_CNT_SHFT                        0x0

#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_ADDR(i)                       (APCS_LMH_LITE_REG_BASE      + 0x000002a4 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_RMSK                          0xffffffff
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_MAXi                                   3
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_ADDR(i), HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_OVERFLOW_BMSK                 0x80000000
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_OVERFLOW_SHFT                       0x1f
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_TIMER_CNT_BMSK                0x7fffffff
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A53_COREi_TIMER_CNT_SHFT                       0x0

#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_ADDR(i)                       (APCS_LMH_LITE_REG_BASE      + 0x000002b4 + 0x4 * (i))
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_RMSK                          0xffffffff
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_MAXi                                   3
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_ADDR(i), HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_RMSK)
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_OVERFLOW_BMSK                 0x80000000
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_OVERFLOW_SHFT                       0x1f
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_TIMER_CNT_BMSK                0x7fffffff
#define HWIO_APCS_THRML_TIME_UNTHRTTL_A57_COREi_TIMER_CNT_SHFT                       0x0

#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_ADDR                                (APCS_LMH_LITE_REG_BASE      + 0x000002c4)
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_RMSK                                0xffffffff
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_IN          \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_ADDR, HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_ADDR, m)
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_ADDR,v)
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_ADDR,m,v,HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_IN)
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_MAX_CNT_BMSK                        0xffffffff
#define HWIO_APCS_THRML_CNSTRNT_HYS_TIMER_MAX_CNT_SHFT                               0x0

#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_ADDR                             (APCS_LMH_LITE_REG_BASE      + 0x000002c8)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_RMSK                                   0xff
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_IN          \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_ADDR, HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_ADDR, m)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_OUT(v)      \
        out_dword(HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_ADDR,v)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_ADDR,m,v,HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_IN)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE3_BMSK                         0x80
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE3_SHFT                          0x7
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE2_BMSK                         0x40
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE2_SHFT                          0x6
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE1_BMSK                         0x20
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE1_SHFT                          0x5
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE0_BMSK                         0x10
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A57_CORE0_SHFT                          0x4
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE3_BMSK                          0x8
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE3_SHFT                          0x3
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE2_BMSK                          0x4
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE2_SHFT                          0x2
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE1_BMSK                          0x2
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE1_SHFT                          0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE0_BMSK                          0x1
#define HWIO_APCS_THRML_CNSTRNT_TIMER_ENABLE_A53_CORE0_SHFT                          0x0

#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_ADDR(i)                  (APCS_LMH_LITE_REG_BASE      + 0x000002cc + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_RMSK                     0xffffffff
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_MAXi                              3
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_OVERFLOW_BMSK            0x80000000
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_OVERFLOW_SHFT                  0x1f
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_TIMER_TICK_BMSK          0x7fffffff
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A53_COREi_TIMER_TICK_SHFT                 0x0

#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_ADDR(i)                  (APCS_LMH_LITE_REG_BASE      + 0x000002dc + 0x4 * (i))
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_RMSK                     0xffffffff
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_MAXi                              3
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_INI(i)        \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_ADDR(i), HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_RMSK)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_ADDR(i), mask)
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_OVERFLOW_BMSK            0x80000000
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_OVERFLOW_SHFT                  0x1f
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_TIMER_TICK_BMSK          0x7fffffff
#define HWIO_APCS_THRML_CNSTRNT_TIMER_STAT_A57_COREi_TIMER_TICK_SHFT                 0x0

#define HWIO_APCS_THRML_SW_OVRD_MN_A53_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000002f0)
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_RMSK                                       0xffff
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_IN          \
        in_dword_masked(HWIO_APCS_THRML_SW_OVRD_MN_A53_ADDR, HWIO_APCS_THRML_SW_OVRD_MN_A53_RMSK)
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_SW_OVRD_MN_A53_ADDR, m)
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_OUT(v)      \
        out_dword(HWIO_APCS_THRML_SW_OVRD_MN_A53_ADDR,v)
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_SW_OVRD_MN_A53_ADDR,m,v,HWIO_APCS_THRML_SW_OVRD_MN_A53_IN)
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_M_BMSK                                     0xff00
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_M_SHFT                                        0x8
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_N_BMSK                                       0xff
#define HWIO_APCS_THRML_SW_OVRD_MN_A53_N_SHFT                                        0x0

#define HWIO_APCS_THRML_SW_OVRD_MN_A57_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000002f4)
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_RMSK                                       0xffff
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_IN          \
        in_dword_masked(HWIO_APCS_THRML_SW_OVRD_MN_A57_ADDR, HWIO_APCS_THRML_SW_OVRD_MN_A57_RMSK)
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_SW_OVRD_MN_A57_ADDR, m)
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_OUT(v)      \
        out_dword(HWIO_APCS_THRML_SW_OVRD_MN_A57_ADDR,v)
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_SW_OVRD_MN_A57_ADDR,m,v,HWIO_APCS_THRML_SW_OVRD_MN_A57_IN)
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_M_BMSK                                     0xff00
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_M_SHFT                                        0x8
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_N_BMSK                                       0xff
#define HWIO_APCS_THRML_SW_OVRD_MN_A57_N_SHFT                                        0x0

#define HWIO_APCS_THRML_INTR_DELAY_A53_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000002f8)
#define HWIO_APCS_THRML_INTR_DELAY_A53_RMSK                                   0xffffffff
#define HWIO_APCS_THRML_INTR_DELAY_A53_IN          \
        in_dword_masked(HWIO_APCS_THRML_INTR_DELAY_A53_ADDR, HWIO_APCS_THRML_INTR_DELAY_A53_RMSK)
#define HWIO_APCS_THRML_INTR_DELAY_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_INTR_DELAY_A53_ADDR, m)
#define HWIO_APCS_THRML_INTR_DELAY_A53_OUT(v)      \
        out_dword(HWIO_APCS_THRML_INTR_DELAY_A53_ADDR,v)
#define HWIO_APCS_THRML_INTR_DELAY_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_INTR_DELAY_A53_ADDR,m,v,HWIO_APCS_THRML_INTR_DELAY_A53_IN)
#define HWIO_APCS_THRML_INTR_DELAY_A53_DELAY_BMSK                             0xffffffff
#define HWIO_APCS_THRML_INTR_DELAY_A53_DELAY_SHFT                                    0x0

#define HWIO_APCS_THRML_INTR_DELAY_A57_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x000002fc)
#define HWIO_APCS_THRML_INTR_DELAY_A57_RMSK                                   0xffffffff
#define HWIO_APCS_THRML_INTR_DELAY_A57_IN          \
        in_dword_masked(HWIO_APCS_THRML_INTR_DELAY_A57_ADDR, HWIO_APCS_THRML_INTR_DELAY_A57_RMSK)
#define HWIO_APCS_THRML_INTR_DELAY_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_INTR_DELAY_A57_ADDR, m)
#define HWIO_APCS_THRML_INTR_DELAY_A57_OUT(v)      \
        out_dword(HWIO_APCS_THRML_INTR_DELAY_A57_ADDR,v)
#define HWIO_APCS_THRML_INTR_DELAY_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_INTR_DELAY_A57_ADDR,m,v,HWIO_APCS_THRML_INTR_DELAY_A57_IN)
#define HWIO_APCS_THRML_INTR_DELAY_A57_DELAY_BMSK                             0xffffffff
#define HWIO_APCS_THRML_INTR_DELAY_A57_DELAY_SHFT                                    0x0

#define HWIO_APCS_THRML_DELTA_M_A57_ADDR                                      (APCS_LMH_LITE_REG_BASE      + 0x00000300)
#define HWIO_APCS_THRML_DELTA_M_A57_RMSK                                          0xffff
#define HWIO_APCS_THRML_DELTA_M_A57_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_A57_ADDR, HWIO_APCS_THRML_DELTA_M_A57_RMSK)
#define HWIO_APCS_THRML_DELTA_M_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_A57_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_A57_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_A57_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_A57_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_A57_IN)
#define HWIO_APCS_THRML_DELTA_M_A57_DELTA_M_THRTL_BMSK                            0xff00
#define HWIO_APCS_THRML_DELTA_M_A57_DELTA_M_THRTL_SHFT                               0x8
#define HWIO_APCS_THRML_DELTA_M_A57_DELTA_M_UNTHRTL_BMSK                            0xff
#define HWIO_APCS_THRML_DELTA_M_A57_DELTA_M_UNTHRTL_SHFT                             0x0

#define HWIO_APCS_THRML_DELTA_M_A53_ADDR                                      (APCS_LMH_LITE_REG_BASE      + 0x00000304)
#define HWIO_APCS_THRML_DELTA_M_A53_RMSK                                          0xffff
#define HWIO_APCS_THRML_DELTA_M_A53_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_A53_ADDR, HWIO_APCS_THRML_DELTA_M_A53_RMSK)
#define HWIO_APCS_THRML_DELTA_M_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_A53_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_A53_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_A53_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_A53_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_A53_IN)
#define HWIO_APCS_THRML_DELTA_M_A53_DELTA_M_THRTL_BMSK                            0xff00
#define HWIO_APCS_THRML_DELTA_M_A53_DELTA_M_THRTL_SHFT                               0x8
#define HWIO_APCS_THRML_DELTA_M_A53_DELTA_M_UNTHRTL_BMSK                            0xff
#define HWIO_APCS_THRML_DELTA_M_A53_DELTA_M_UNTHRTL_SHFT                             0x0

#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_ADDR                        (APCS_LMH_LITE_REG_BASE      + 0x00000308)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_RMSK                        0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_ADDR, HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_RMSK)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_IN)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_TIMER_BMSK                  0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A57_TIMER_SHFT                         0x0

#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_ADDR                      (APCS_LMH_LITE_REG_BASE      + 0x0000030c)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_RMSK                      0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_ADDR, HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_RMSK)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_IN)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_TIMER_BMSK                0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A57_TIMER_SHFT                       0x0

#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_ADDR                        (APCS_LMH_LITE_REG_BASE      + 0x00000310)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_RMSK                        0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_ADDR, HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_RMSK)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_IN)
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_TIMER_BMSK                  0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_UP_TIMER_A53_TIMER_SHFT                         0x0

#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_ADDR                      (APCS_LMH_LITE_REG_BASE      + 0x00000314)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_RMSK                      0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_IN          \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_ADDR, HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_RMSK)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_ADDR, m)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_OUT(v)      \
        out_dword(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_ADDR,v)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_ADDR,m,v,HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_IN)
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_TIMER_BMSK                0xffffffff
#define HWIO_APCS_THRML_DELTA_M_STEP_DOWN_TIMER_A53_TIMER_SHFT                       0x0

#define HWIO_APCS_INTR_MASK_ADDR                                              (APCS_LMH_LITE_REG_BASE      + 0x0000031c)
#define HWIO_APCS_INTR_MASK_RMSK                                                 0x7ffff
#define HWIO_APCS_INTR_MASK_IN          \
        in_dword_masked(HWIO_APCS_INTR_MASK_ADDR, HWIO_APCS_INTR_MASK_RMSK)
#define HWIO_APCS_INTR_MASK_INM(m)      \
        in_dword_masked(HWIO_APCS_INTR_MASK_ADDR, m)
#define HWIO_APCS_INTR_MASK_OUT(v)      \
        out_dword(HWIO_APCS_INTR_MASK_ADDR,v)
#define HWIO_APCS_INTR_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_INTR_MASK_ADDR,m,v,HWIO_APCS_INTR_MASK_IN)
#define HWIO_APCS_INTR_MASK_CS_INTR_MSK_BMSK                                     0x40000
#define HWIO_APCS_INTR_MASK_CS_INTR_MSK_SHFT                                        0x12
#define HWIO_APCS_INTR_MASK_A57_THRML_CNTR_INTR_MSK_BMSK                         0x3c000
#define HWIO_APCS_INTR_MASK_A57_THRML_CNTR_INTR_MSK_SHFT                             0xe
#define HWIO_APCS_INTR_MASK_A53_THRML_CNTR_INTR_MSK_BMSK                          0x3c00
#define HWIO_APCS_INTR_MASK_A53_THRML_CNTR_INTR_MSK_SHFT                             0xa
#define HWIO_APCS_INTR_MASK_A57_PMIC_CNTR_INT_MSK_BMSK                             0x200
#define HWIO_APCS_INTR_MASK_A57_PMIC_CNTR_INT_MSK_SHFT                               0x9
#define HWIO_APCS_INTR_MASK_A57_CORE3_INTR_MSK_BMSK                                0x100
#define HWIO_APCS_INTR_MASK_A57_CORE3_INTR_MSK_SHFT                                  0x8
#define HWIO_APCS_INTR_MASK_A57_CORE2_INTR_MSK_BMSK                                 0x80
#define HWIO_APCS_INTR_MASK_A57_CORE2_INTR_MSK_SHFT                                  0x7
#define HWIO_APCS_INTR_MASK_A57_CORE1_INTR_MSK_BMSK                                 0x40
#define HWIO_APCS_INTR_MASK_A57_CORE1_INTR_MSK_SHFT                                  0x6
#define HWIO_APCS_INTR_MASK_A57_CORE0_INTR_MSK_BMSK                                 0x20
#define HWIO_APCS_INTR_MASK_A57_CORE0_INTR_MSK_SHFT                                  0x5
#define HWIO_APCS_INTR_MASK_A53_CORE3_INTR_MSK_BMSK                                 0x10
#define HWIO_APCS_INTR_MASK_A53_CORE3_INTR_MSK_SHFT                                  0x4
#define HWIO_APCS_INTR_MASK_A53_CORE2_INTR_MSK_BMSK                                  0x8
#define HWIO_APCS_INTR_MASK_A53_CORE2_INTR_MSK_SHFT                                  0x3
#define HWIO_APCS_INTR_MASK_A53_CORE1_INTR_MSK_BMSK                                  0x4
#define HWIO_APCS_INTR_MASK_A53_CORE1_INTR_MSK_SHFT                                  0x2
#define HWIO_APCS_INTR_MASK_A53_CORE0_INTR_MSK_BMSK                                  0x2
#define HWIO_APCS_INTR_MASK_A53_CORE0_INTR_MSK_SHFT                                  0x1
#define HWIO_APCS_INTR_MASK_PMIC_INTR_MSK_BMSK                                       0x1
#define HWIO_APCS_INTR_MASK_PMIC_INTR_MSK_SHFT                                       0x0

#define HWIO_APCS_PS_CONFIG0_A53_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x0000032c)
#define HWIO_APCS_PS_CONFIG0_A53_RMSK                                         0xffffffff
#define HWIO_APCS_PS_CONFIG0_A53_IN          \
        in_dword_masked(HWIO_APCS_PS_CONFIG0_A53_ADDR, HWIO_APCS_PS_CONFIG0_A53_RMSK)
#define HWIO_APCS_PS_CONFIG0_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CONFIG0_A53_ADDR, m)
#define HWIO_APCS_PS_CONFIG0_A53_OUT(v)      \
        out_dword(HWIO_APCS_PS_CONFIG0_A53_ADDR,v)
#define HWIO_APCS_PS_CONFIG0_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CONFIG0_A53_ADDR,m,v,HWIO_APCS_PS_CONFIG0_A53_IN)
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL3_BMSK                               0xffe00000
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL3_SHFT                                     0x15
#define HWIO_APCS_PS_CONFIG0_A53_MN_VAL_SWMODE_BMSK                             0x100000
#define HWIO_APCS_PS_CONFIG0_A53_MN_VAL_SWMODE_SHFT                                 0x14
#define HWIO_APCS_PS_CONFIG0_A53_SW_BYP_OVRD_BMSK                                0x80000
#define HWIO_APCS_PS_CONFIG0_A53_SW_BYP_OVRD_SHFT                                   0x13
#define HWIO_APCS_PS_CONFIG0_A53_LD_SW_PAT_BMSK                                  0x40000
#define HWIO_APCS_PS_CONFIG0_A53_LD_SW_PAT_SHFT                                     0x12
#define HWIO_APCS_PS_CONFIG0_A53_OVRD_VALU_BMSK                                  0x3c000
#define HWIO_APCS_PS_CONFIG0_A53_OVRD_VALU_SHFT                                      0xe
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL2_BMSK                                   0x3800
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL2_SHFT                                      0xb
#define HWIO_APCS_PS_CONFIG0_A53_SW_OVRD_BMSK                                      0x400
#define HWIO_APCS_PS_CONFIG0_A53_SW_OVRD_SHFT                                        0xa
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_FRC_SWL_BMSK                                 0x200
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_FRC_SWL_SHFT                                   0x9
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL_BMSK                                     0x1c0
#define HWIO_APCS_PS_CONFIG0_A53_DBG_CTRL_SHFT                                       0x6
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_CNTR_BMSK                                     0x20
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_CNTR_SHFT                                      0x5
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_MN_BMSK                                       0x10
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_MN_SHFT                                        0x4
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_BYP_BMSK                                       0x8
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_BYP_SHFT                                       0x3
#define HWIO_APCS_PS_CONFIG0_A53_DIV_MODE_BMSK                                       0x4
#define HWIO_APCS_PS_CONFIG0_A53_DIV_MODE_SHFT                                       0x2
#define HWIO_APCS_PS_CONFIG0_A53_DIDT_CTRL_BMSK                                      0x2
#define HWIO_APCS_PS_CONFIG0_A53_DIDT_CTRL_SHFT                                      0x1
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_SRC_BMSK                                       0x1
#define HWIO_APCS_PS_CONFIG0_A53_CTRL_SRC_SHFT                                       0x0

#define HWIO_APCS_PS_CONFIG1_A53_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000330)
#define HWIO_APCS_PS_CONFIG1_A53_RMSK                                         0xffffffff
#define HWIO_APCS_PS_CONFIG1_A53_IN          \
        in_dword_masked(HWIO_APCS_PS_CONFIG1_A53_ADDR, HWIO_APCS_PS_CONFIG1_A53_RMSK)
#define HWIO_APCS_PS_CONFIG1_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CONFIG1_A53_ADDR, m)
#define HWIO_APCS_PS_CONFIG1_A53_OUT(v)      \
        out_dword(HWIO_APCS_PS_CONFIG1_A53_ADDR,v)
#define HWIO_APCS_PS_CONFIG1_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CONFIG1_A53_ADDR,m,v,HWIO_APCS_PS_CONFIG1_A53_IN)
#define HWIO_APCS_PS_CONFIG1_A53_SPARE_BMSK                                   0xff000000
#define HWIO_APCS_PS_CONFIG1_A53_SPARE_SHFT                                         0x18
#define HWIO_APCS_PS_CONFIG1_A53_SW_CTRL_N_BMSK                                 0xff0000
#define HWIO_APCS_PS_CONFIG1_A53_SW_CTRL_N_SHFT                                     0x10
#define HWIO_APCS_PS_CONFIG1_A53_SW_CTRL_M_BMSK                                   0xff00
#define HWIO_APCS_PS_CONFIG1_A53_SW_CTRL_M_SHFT                                      0x8
#define HWIO_APCS_PS_CONFIG1_A53_SWALW_SEQ_BMSK                                     0xff
#define HWIO_APCS_PS_CONFIG1_A53_SWALW_SEQ_SHFT                                      0x0

#define HWIO_APCS_PS_A53_CFG_VALID_ADDR                                       (APCS_LMH_LITE_REG_BASE      + 0x00000334)
#define HWIO_APCS_PS_A53_CFG_VALID_RMSK                                              0x1
#define HWIO_APCS_PS_A53_CFG_VALID_IN          \
        in_dword_masked(HWIO_APCS_PS_A53_CFG_VALID_ADDR, HWIO_APCS_PS_A53_CFG_VALID_RMSK)
#define HWIO_APCS_PS_A53_CFG_VALID_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_A53_CFG_VALID_ADDR, m)
#define HWIO_APCS_PS_A53_CFG_VALID_OUT(v)      \
        out_dword(HWIO_APCS_PS_A53_CFG_VALID_ADDR,v)
#define HWIO_APCS_PS_A53_CFG_VALID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_A53_CFG_VALID_ADDR,m,v,HWIO_APCS_PS_A53_CFG_VALID_IN)
#define HWIO_APCS_PS_A53_CFG_VALID_VALID_BMSK                                        0x1
#define HWIO_APCS_PS_A53_CFG_VALID_VALID_SHFT                                        0x0

#define HWIO_APCS_PS_CONFIG0_A57_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000338)
#define HWIO_APCS_PS_CONFIG0_A57_RMSK                                         0xffffffff
#define HWIO_APCS_PS_CONFIG0_A57_IN          \
        in_dword_masked(HWIO_APCS_PS_CONFIG0_A57_ADDR, HWIO_APCS_PS_CONFIG0_A57_RMSK)
#define HWIO_APCS_PS_CONFIG0_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CONFIG0_A57_ADDR, m)
#define HWIO_APCS_PS_CONFIG0_A57_OUT(v)      \
        out_dword(HWIO_APCS_PS_CONFIG0_A57_ADDR,v)
#define HWIO_APCS_PS_CONFIG0_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CONFIG0_A57_ADDR,m,v,HWIO_APCS_PS_CONFIG0_A57_IN)
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL3_BMSK                               0xffe00000
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL3_SHFT                                     0x15
#define HWIO_APCS_PS_CONFIG0_A57_MN_VAL_SWMODE_BMSK                             0x100000
#define HWIO_APCS_PS_CONFIG0_A57_MN_VAL_SWMODE_SHFT                                 0x14
#define HWIO_APCS_PS_CONFIG0_A57_SW_BYP_OVRD_BMSK                                0x80000
#define HWIO_APCS_PS_CONFIG0_A57_SW_BYP_OVRD_SHFT                                   0x13
#define HWIO_APCS_PS_CONFIG0_A57_LD_SW_PAT_BMSK                                  0x40000
#define HWIO_APCS_PS_CONFIG0_A57_LD_SW_PAT_SHFT                                     0x12
#define HWIO_APCS_PS_CONFIG0_A57_OVRD_VALU_BMSK                                  0x3c000
#define HWIO_APCS_PS_CONFIG0_A57_OVRD_VALU_SHFT                                      0xe
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL2_BMSK                                   0x3800
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL2_SHFT                                      0xb
#define HWIO_APCS_PS_CONFIG0_A57_SW_OVRD_BMSK                                      0x400
#define HWIO_APCS_PS_CONFIG0_A57_SW_OVRD_SHFT                                        0xa
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_FRC_SWL_BMSK                                 0x200
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_FRC_SWL_SHFT                                   0x9
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL_BMSK                                     0x1c0
#define HWIO_APCS_PS_CONFIG0_A57_DBG_CTRL_SHFT                                       0x6
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_CNTR_BMSK                                     0x20
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_CNTR_SHFT                                      0x5
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_MN_BMSK                                       0x10
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_MN_SHFT                                        0x4
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_BYP_BMSK                                       0x8
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_BYP_SHFT                                       0x3
#define HWIO_APCS_PS_CONFIG0_A57_DIV_MODE_BMSK                                       0x4
#define HWIO_APCS_PS_CONFIG0_A57_DIV_MODE_SHFT                                       0x2
#define HWIO_APCS_PS_CONFIG0_A57_DIDT_CTRL_BMSK                                      0x2
#define HWIO_APCS_PS_CONFIG0_A57_DIDT_CTRL_SHFT                                      0x1
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_SRC_BMSK                                       0x1
#define HWIO_APCS_PS_CONFIG0_A57_CTRL_SRC_SHFT                                       0x0

#define HWIO_APCS_PS_CONFIG1_A57_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x0000033c)
#define HWIO_APCS_PS_CONFIG1_A57_RMSK                                         0xffffffff
#define HWIO_APCS_PS_CONFIG1_A57_IN          \
        in_dword_masked(HWIO_APCS_PS_CONFIG1_A57_ADDR, HWIO_APCS_PS_CONFIG1_A57_RMSK)
#define HWIO_APCS_PS_CONFIG1_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CONFIG1_A57_ADDR, m)
#define HWIO_APCS_PS_CONFIG1_A57_OUT(v)      \
        out_dword(HWIO_APCS_PS_CONFIG1_A57_ADDR,v)
#define HWIO_APCS_PS_CONFIG1_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CONFIG1_A57_ADDR,m,v,HWIO_APCS_PS_CONFIG1_A57_IN)
#define HWIO_APCS_PS_CONFIG1_A57_SPARE_BMSK                                   0xff000000
#define HWIO_APCS_PS_CONFIG1_A57_SPARE_SHFT                                         0x18
#define HWIO_APCS_PS_CONFIG1_A57_SW_CTRL_N_BMSK                                 0xff0000
#define HWIO_APCS_PS_CONFIG1_A57_SW_CTRL_N_SHFT                                     0x10
#define HWIO_APCS_PS_CONFIG1_A57_SW_CTRL_M_BMSK                                   0xff00
#define HWIO_APCS_PS_CONFIG1_A57_SW_CTRL_M_SHFT                                      0x8
#define HWIO_APCS_PS_CONFIG1_A57_SWALW_SEQ_BMSK                                     0xff
#define HWIO_APCS_PS_CONFIG1_A57_SWALW_SEQ_SHFT                                      0x0

#define HWIO_APCS_PS_A57_CFG_VALID_ADDR                                       (APCS_LMH_LITE_REG_BASE      + 0x00000340)
#define HWIO_APCS_PS_A57_CFG_VALID_RMSK                                              0x1
#define HWIO_APCS_PS_A57_CFG_VALID_IN          \
        in_dword_masked(HWIO_APCS_PS_A57_CFG_VALID_ADDR, HWIO_APCS_PS_A57_CFG_VALID_RMSK)
#define HWIO_APCS_PS_A57_CFG_VALID_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_A57_CFG_VALID_ADDR, m)
#define HWIO_APCS_PS_A57_CFG_VALID_OUT(v)      \
        out_dword(HWIO_APCS_PS_A57_CFG_VALID_ADDR,v)
#define HWIO_APCS_PS_A57_CFG_VALID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_A57_CFG_VALID_ADDR,m,v,HWIO_APCS_PS_A57_CFG_VALID_IN)
#define HWIO_APCS_PS_A57_CFG_VALID_VALID_BMSK                                        0x1
#define HWIO_APCS_PS_A57_CFG_VALID_VALID_SHFT                                        0x0

#define HWIO_APCS_PS_STATUS0_A53_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000344)
#define HWIO_APCS_PS_STATUS0_A53_RMSK                                         0xffffffff
#define HWIO_APCS_PS_STATUS0_A53_IN          \
        in_dword_masked(HWIO_APCS_PS_STATUS0_A53_ADDR, HWIO_APCS_PS_STATUS0_A53_RMSK)
#define HWIO_APCS_PS_STATUS0_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_STATUS0_A53_ADDR, m)
#define HWIO_APCS_PS_STATUS0_A53_PS_STATUS_BMSK                               0xffffffff
#define HWIO_APCS_PS_STATUS0_A53_PS_STATUS_SHFT                                      0x0

#define HWIO_APCS_PS_STATUS0_A57_ADDR                                         (APCS_LMH_LITE_REG_BASE      + 0x00000348)
#define HWIO_APCS_PS_STATUS0_A57_RMSK                                         0xffffffff
#define HWIO_APCS_PS_STATUS0_A57_IN          \
        in_dword_masked(HWIO_APCS_PS_STATUS0_A57_ADDR, HWIO_APCS_PS_STATUS0_A57_RMSK)
#define HWIO_APCS_PS_STATUS0_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_STATUS0_A57_ADDR, m)
#define HWIO_APCS_PS_STATUS0_A57_PS_STATUS_BMSK                               0xffffffff
#define HWIO_APCS_PS_STATUS0_A57_PS_STATUS_SHFT                                      0x0

#define HWIO_APCS_PS_CTRL_A53_ADDR                                            (APCS_LMH_LITE_REG_BASE      + 0x0000034c)
#define HWIO_APCS_PS_CTRL_A53_RMSK                                                   0x3
#define HWIO_APCS_PS_CTRL_A53_IN          \
        in_dword_masked(HWIO_APCS_PS_CTRL_A53_ADDR, HWIO_APCS_PS_CTRL_A53_RMSK)
#define HWIO_APCS_PS_CTRL_A53_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CTRL_A53_ADDR, m)
#define HWIO_APCS_PS_CTRL_A53_OUT(v)      \
        out_dword(HWIO_APCS_PS_CTRL_A53_ADDR,v)
#define HWIO_APCS_PS_CTRL_A53_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CTRL_A53_ADDR,m,v,HWIO_APCS_PS_CTRL_A53_IN)
#define HWIO_APCS_PS_CTRL_A53_BYPASS_BMSK                                            0x2
#define HWIO_APCS_PS_CTRL_A53_BYPASS_SHFT                                            0x1
#define HWIO_APCS_PS_CTRL_A53_ENABLE_BMSK                                            0x1
#define HWIO_APCS_PS_CTRL_A53_ENABLE_SHFT                                            0x0

#define HWIO_APCS_PS_CTRL_A57_ADDR                                            (APCS_LMH_LITE_REG_BASE      + 0x00000350)
#define HWIO_APCS_PS_CTRL_A57_RMSK                                                   0x3
#define HWIO_APCS_PS_CTRL_A57_IN          \
        in_dword_masked(HWIO_APCS_PS_CTRL_A57_ADDR, HWIO_APCS_PS_CTRL_A57_RMSK)
#define HWIO_APCS_PS_CTRL_A57_INM(m)      \
        in_dword_masked(HWIO_APCS_PS_CTRL_A57_ADDR, m)
#define HWIO_APCS_PS_CTRL_A57_OUT(v)      \
        out_dword(HWIO_APCS_PS_CTRL_A57_ADDR,v)
#define HWIO_APCS_PS_CTRL_A57_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_PS_CTRL_A57_ADDR,m,v,HWIO_APCS_PS_CTRL_A57_IN)
#define HWIO_APCS_PS_CTRL_A57_BYPASS_BMSK                                            0x2
#define HWIO_APCS_PS_CTRL_A57_BYPASS_SHFT                                            0x1
#define HWIO_APCS_PS_CTRL_A57_ENABLE_BMSK                                            0x1
#define HWIO_APCS_PS_CTRL_A57_ENABLE_SHFT                                            0x0

#define HWIO_APCS_DEBUG_TS_REQEST_ADDR                                        (APCS_LMH_LITE_REG_BASE      + 0x00000354)
#define HWIO_APCS_DEBUG_TS_REQEST_RMSK                                        0x3fffffff
#define HWIO_APCS_DEBUG_TS_REQEST_IN          \
        in_dword_masked(HWIO_APCS_DEBUG_TS_REQEST_ADDR, HWIO_APCS_DEBUG_TS_REQEST_RMSK)
#define HWIO_APCS_DEBUG_TS_REQEST_INM(m)      \
        in_dword_masked(HWIO_APCS_DEBUG_TS_REQEST_ADDR, m)
#define HWIO_APCS_DEBUG_TS_REQEST_OUT(v)      \
        out_dword(HWIO_APCS_DEBUG_TS_REQEST_ADDR,v)
#define HWIO_APCS_DEBUG_TS_REQEST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_DEBUG_TS_REQEST_ADDR,m,v,HWIO_APCS_DEBUG_TS_REQEST_IN)
#define HWIO_APCS_DEBUG_TS_REQEST_BOOST_MODE_PKT_SEL_BMSK                     0x20000000
#define HWIO_APCS_DEBUG_TS_REQEST_BOOST_MODE_PKT_SEL_SHFT                           0x1d
#define HWIO_APCS_DEBUG_TS_REQEST_SID_PKT_SEL_BMSK                            0x10000000
#define HWIO_APCS_DEBUG_TS_REQEST_SID_PKT_SEL_SHFT                                  0x1c
#define HWIO_APCS_DEBUG_TS_REQEST_TPDM_CMB_SECURE_EN_BMSK                      0x8000000
#define HWIO_APCS_DEBUG_TS_REQEST_TPDM_CMB_SECURE_EN_SHFT                           0x1b
#define HWIO_APCS_DEBUG_TS_REQEST_PRD_RETRIM_DBG_EN_BMSK                       0x4000000
#define HWIO_APCS_DEBUG_TS_REQEST_PRD_RETRIM_DBG_EN_SHFT                            0x1a
#define HWIO_APCS_DEBUG_TS_REQEST_DEBUG_ENABLE_BMSK                            0x2000000
#define HWIO_APCS_DEBUG_TS_REQEST_DEBUG_ENABLE_SHFT                                 0x19
#define HWIO_APCS_DEBUG_TS_REQEST_PERIOD_BMSK                                  0x1fffffe
#define HWIO_APCS_DEBUG_TS_REQEST_PERIOD_SHFT                                        0x1
#define HWIO_APCS_DEBUG_TS_REQEST_ENABLE_BMSK                                        0x1
#define HWIO_APCS_DEBUG_TS_REQEST_ENABLE_SHFT                                        0x0

#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_ADDR                                   (APCS_LMH_LITE_REG_BASE      + 0x00000358)
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_RMSK                                   0xffffffff
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_IN          \
        in_dword_masked(HWIO_APCS_DEBUG_PACKET_BW_CTRL_ADDR, HWIO_APCS_DEBUG_PACKET_BW_CTRL_RMSK)
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_DEBUG_PACKET_BW_CTRL_ADDR, m)
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_DEBUG_PACKET_BW_CTRL_ADDR,v)
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_DEBUG_PACKET_BW_CTRL_ADDR,m,v,HWIO_APCS_DEBUG_PACKET_BW_CTRL_IN)
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_RATE_BMSK                              0xffffffff
#define HWIO_APCS_DEBUG_PACKET_BW_CTRL_RATE_SHFT                                     0x0

#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_ADDR                                 (APCS_LMH_LITE_REG_BASE      + 0x0000035c)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_RMSK                                       0x1f
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_IN          \
        in_dword_masked(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_ADDR, HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_RMSK)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_INM(m)      \
        in_dword_masked(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_ADDR, m)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_OUT(v)      \
        out_dword(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_ADDR,v)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_ADDR,m,v,HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_IN)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_CLK_CTRL_ENABLE_BMSK                       0x1f
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_CLK_CTRL_ENABLE_SHFT                        0x0

#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ADDR                           (APCS_LMH_LITE_REG_BASE      + 0x00000360)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_RMSK                           0xffffffff
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ADDR, HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_RMSK)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ADDR, m)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ADDR,v)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ADDR,m,v,HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_IN)
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ENABLE_BMSK                    0x80000000
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_ENABLE_SHFT                          0x1f
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_TIMER_BMSK                     0x7fffffff
#define HWIO_APCS_CLK_CTRL_FOR_CURR_TRIM_TIMER_TIMER_SHFT                            0x0

#define HWIO_APCS_BHS_STRENGTH_STATUS_ADDR                                    (APCS_LMH_LITE_REG_BASE      + 0x00000364)
#define HWIO_APCS_BHS_STRENGTH_STATUS_RMSK                                          0x3f
#define HWIO_APCS_BHS_STRENGTH_STATUS_IN          \
        in_dword_masked(HWIO_APCS_BHS_STRENGTH_STATUS_ADDR, HWIO_APCS_BHS_STRENGTH_STATUS_RMSK)
#define HWIO_APCS_BHS_STRENGTH_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_BHS_STRENGTH_STATUS_ADDR, m)
#define HWIO_APCS_BHS_STRENGTH_STATUS_BHS_STRENGTH_BMSK                             0x3f
#define HWIO_APCS_BHS_STRENGTH_STATUS_BHS_STRENGTH_SHFT                              0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ISENSE_CONTROLLER
 *--------------------------------------------------------------------------*/

#define APCS_ISENSE_CONTROLLER_REG_BASE                                                  (A53SS_BASE      + 0x001d8400)

#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_ADDR(i)                                     (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000000 + 0x4 * (i))
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_RMSK                                              0x3f
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_MAXi                                                 3
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_ADDR(i), HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_RMSK)
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_ADDR(i), mask)
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_ADC_STATUS_BMSK                                   0x20
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_ADC_STATUS_SHFT                                    0x5
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_CLK_STATUS_BMSK                                   0x10
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_CLK_STATUS_SHFT                                    0x4
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_BYPASS_BMSK                                 0x8
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_BYPASS_SHFT                                 0x3
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_LDOBHS_ENABLE_BMSK                          0x4
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_LDOBHS_ENABLE_SHFT                          0x2
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_HALT_BMSK                                   0x2
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_HALT_SHFT                                   0x1
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_ENABLE_BMSK                                 0x1
#define HWIO_APCS_CS_CURRENT_SENSOR_STATUS_i_SENSOR_ENABLE_SHFT                                 0x0

#define HWIO_APCS_CS_CALIBRATION_STATUS_ADDR                                             (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000010)
#define HWIO_APCS_CS_CALIBRATION_STATUS_RMSK                                                    0x7
#define HWIO_APCS_CS_CALIBRATION_STATUS_IN          \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_STATUS_ADDR, HWIO_APCS_CS_CALIBRATION_STATUS_RMSK)
#define HWIO_APCS_CS_CALIBRATION_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_STATUS_ADDR, m)
#define HWIO_APCS_CS_CALIBRATION_STATUS_TERMINATION_BMSK                                        0x6
#define HWIO_APCS_CS_CALIBRATION_STATUS_TERMINATION_SHFT                                        0x1
#define HWIO_APCS_CS_CALIBRATION_STATUS_DONE_FLAG_BMSK                                          0x1
#define HWIO_APCS_CS_CALIBRATION_STATUS_DONE_FLAG_SHFT                                          0x0

#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_ADDR(i)                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000014 + 0x4 * (i))
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_RMSK                                          0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_MAXi                                              3
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_ADDR(i), HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_RMSK)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_ADDR(i), mask)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_DIGITAL_CODE_BMSK                             0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE0_i_DIGITAL_CODE_SHFT                               0x0

#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_ADDR(i)                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000024 + 0x4 * (i))
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_RMSK                                          0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_MAXi                                              3
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_ADDR(i), HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_RMSK)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_ADDR(i), mask)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_DIGITAL_CODE_BMSK                             0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE1_i_DIGITAL_CODE_SHFT                               0x0

#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_ADDR(i)                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000034 + 0x4 * (i))
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_RMSK                                          0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_MAXi                                              3
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_ADDR(i), HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_RMSK)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_ADDR(i), mask)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_DIGITAL_CODE_BMSK                             0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE2_i_DIGITAL_CODE_SHFT                               0x0

#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_ADDR(i)                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000044 + 0x4 * (i))
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_RMSK                                          0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_MAXi                                              3
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_ADDR(i), HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_RMSK)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_ADDR(i), mask)
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_DIGITAL_CODE_BMSK                             0x3ff
#define HWIO_APCS_CS_CALIBRATION_RESULT_CORE3_i_DIGITAL_CODE_SHFT                               0x0

#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_ADDR(i)                         (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000054 + 0x4 * (i))
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_RMSK                            0xffffffff
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_MAXi                                     3
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_ADDR(i), HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_RMSK)
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_ADDR(i), mask)
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_OVER_FLOW_BMSK                  0x80000000
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_OVER_FLOW_SHFT                        0x1f
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_ACCUMULATED_VALUE_BMSK          0x7fffffff
#define HWIO_APCS_CS_CURRENT_SENSOR_ACCUMULATED_OUTPUT_i_ACCUMULATED_VALUE_SHFT                 0x0

#define HWIO_APCS_CS_TIMER_STATUS_ADDR                                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000064)
#define HWIO_APCS_CS_TIMER_STATUS_RMSK                                                   0xffffffff
#define HWIO_APCS_CS_TIMER_STATUS_IN          \
        in_dword_masked(HWIO_APCS_CS_TIMER_STATUS_ADDR, HWIO_APCS_CS_TIMER_STATUS_RMSK)
#define HWIO_APCS_CS_TIMER_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_TIMER_STATUS_ADDR, m)
#define HWIO_APCS_CS_TIMER_STATUS_OVER_FLOW_BMSK                                         0x80000000
#define HWIO_APCS_CS_TIMER_STATUS_OVER_FLOW_SHFT                                               0x1f
#define HWIO_APCS_CS_TIMER_STATUS_TIMER_VALUE_BMSK                                       0x7fffffff
#define HWIO_APCS_CS_TIMER_STATUS_TIMER_VALUE_SHFT                                              0x0

#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_ADDR                                          (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000068)
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_RMSK                                              0x7fff
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_IN          \
        in_dword_masked(HWIO_APCS_CS_SENSOR_GENERAL_STATUS_ADDR, HWIO_APCS_CS_SENSOR_GENERAL_STATUS_RMSK)
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_SENSOR_GENERAL_STATUS_ADDR, m)
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_PERIODIC_ERR_BMSK                         0x4000
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_PERIODIC_ERR_SHFT                            0xe
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_AMPTRIM_WARMRST_ERR_BMSK                  0x2000
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_AMPTRIM_WARMRST_ERR_SHFT                     0xd
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_AMPTRIM_ERR_BMSK                          0x1000
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_OVERALL_AMPTRIM_ERR_SHFT                             0xc
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_AMPTRIM_DONE_BMSK                               0x800
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_AMPTRIM_DONE_SHFT                                 0xb
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_CS_PWR_ON_STATUS_BMSK                              0x400
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_CS_PWR_ON_STATUS_SHFT                                0xa
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_POR_FSM_STATUS_BMSK                                0x3c0
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_POR_FSM_STATUS_SHFT                                  0x6
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_DONE_CAL_BMSK                                    0x20
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_DONE_CAL_SHFT                                     0x5
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_IN_CAL_BMSK                                      0x10
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SS_IN_CAL_SHFT                                       0x4
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_HB_CNT_ON_BMSK                                       0x8
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_HB_CNT_ON_SHFT                                       0x3
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SENSOR_RDY_BMSK                                      0x4
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_SENSOR_RDY_SHFT                                      0x2
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_BG_EN_STATUS_BMSK                                    0x2
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_BG_EN_STATUS_SHFT                                    0x1
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_LDO_STATUS_BMSK                                      0x1
#define HWIO_APCS_CS_SENSOR_GENERAL_STATUS_LDO_STATUS_SHFT                                      0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_ADDR(i)                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000074 + 0x4 * (i))
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_RMSK                                            0x3f
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_MAXi                                               7
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_ADDR(i), HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_ADDR(i), mask)
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_PASS_CAL_BMSK                        0x20
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_PASS_CAL_SHFT                         0x5
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OUT_OF_RANGE_ERR_BMSK                       0x10
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OUT_OF_RANGE_ERR_SHFT                        0x4
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_CHECK_TIMEOUT_ERR_BMSK                       0x8
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_CHECK_TIMEOUT_ERR_SHFT                       0x3
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_CHECK_MAX_ERR_BMSK                    0x4
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_CHECK_MAX_ERR_SHFT                    0x2
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_CHECK_MIN_ERR_BMSK                    0x2
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_AMP_OFFSET_CHECK_MIN_ERR_SHFT                    0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_DONE_BMSK                                        0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS1_i_DONE_SHFT                                        0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_ADDR(i)                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000098 + 0x4 * (i))
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_RMSK                                      0xffe00fff
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_MAXi                                               7
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_ADDR(i), HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_ADDR(i), mask)
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_RESERVE_BMSK                              0xffe00000
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_RESERVE_SHFT                                    0x15
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_TRIM_FSM_STATE_BMSK                            0xf00
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_TRIM_FSM_STATE_SHFT                              0x8
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_TRIM_OFFSET_CANCELLATION_BMSK                   0xff
#define HWIO_APCS_CS_AMP_CALIBRATION_STATUS2_i_TRIM_OFFSET_CANCELLATION_SHFT                    0x0

#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_ADDR(i)                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000000bc + 0x4 * (i))
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_RMSK                                              0xf
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_MAXi                                                7
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_ADDR(i), HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_RMSK)
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_ADDR(i), mask)
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_PERIODIC_ERR_BMSK                                 0x8
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_PERIODIC_ERR_SHFT                                 0x3
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_FSM_STATE_BMSK                                    0x7
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_STATUS_i_FSM_STATE_SHFT                                    0x0

#define HWIO_APCS_CS_ENABLE_REG_ADDR                                                     (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000480)
#define HWIO_APCS_CS_ENABLE_REG_RMSK                                                            0xf
#define HWIO_APCS_CS_ENABLE_REG_IN          \
        in_dword_masked(HWIO_APCS_CS_ENABLE_REG_ADDR, HWIO_APCS_CS_ENABLE_REG_RMSK)
#define HWIO_APCS_CS_ENABLE_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_ENABLE_REG_ADDR, m)
#define HWIO_APCS_CS_ENABLE_REG_OUT(v)      \
        out_dword(HWIO_APCS_CS_ENABLE_REG_ADDR,v)
#define HWIO_APCS_CS_ENABLE_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_ENABLE_REG_ADDR,m,v,HWIO_APCS_CS_ENABLE_REG_IN)
#define HWIO_APCS_CS_ENABLE_REG_ENABLE_CS_BMSK                                                  0xf
#define HWIO_APCS_CS_ENABLE_REG_ENABLE_CS_SHFT                                                  0x0

#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_ADDR                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000484)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RMSK                                      0xffffffff
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_IN          \
        in_dword_masked(HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_ADDR, HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RMSK)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_ADDR, m)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_ADDR,v)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_ADDR,m,v,HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_IN)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RESERVE_1_BMSK                            0xffe00000
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RESERVE_1_SHFT                                  0x15
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_NUM_AVG_BMSK                           0x1ff000
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_NUM_AVG_SHFT                                0xc
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_OFFSET_BMSK                               0xc00
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_OFFSET_SHFT                                 0xa
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RESERVE_0_BMSK                                 0x3e0
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_RESERVE_0_SHFT                                   0x5
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_ENABLE_BMSK                                0x1e
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_TRIM_ENABLE_SHFT                                 0x1
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_CAL_ENABLE_BMSK                                  0x1
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_CTRL_CAL_ENABLE_SHFT                                  0x0

#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_ADDR(i)                                         (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000494 + 0x4 * (i))
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_RMSK                                            0xffffffff
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_MAXi                                                     3
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_SENSOR_PARAM_CORE_i_ADDR(i), HWIO_APCS_CS_SENSOR_PARAM_CORE_i_RMSK)
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_SENSOR_PARAM_CORE_i_ADDR(i), mask)
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_SENSOR_PARAM_CORE_i_ADDR(i),val)
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_SENSOR_PARAM_CORE_i_ADDR(i),mask,val,HWIO_APCS_CS_SENSOR_PARAM_CORE_i_INI(i))
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_SLOPE_BMSK                                      0xffff0000
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_SLOPE_SHFT                                            0x10
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_INTERCEPT_BMSK                                      0xffff
#define HWIO_APCS_CS_SENSOR_PARAM_CORE_i_INTERCEPT_SHFT                                         0x0

#define HWIO_APCS_CS_HALT_REG_ADDR                                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004a4)
#define HWIO_APCS_CS_HALT_REG_RMSK                                                       0xffffffff
#define HWIO_APCS_CS_HALT_REG_IN          \
        in_dword_masked(HWIO_APCS_CS_HALT_REG_ADDR, HWIO_APCS_CS_HALT_REG_RMSK)
#define HWIO_APCS_CS_HALT_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_HALT_REG_ADDR, m)
#define HWIO_APCS_CS_HALT_REG_OUT(v)      \
        out_dword(HWIO_APCS_CS_HALT_REG_ADDR,v)
#define HWIO_APCS_CS_HALT_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_HALT_REG_ADDR,m,v,HWIO_APCS_CS_HALT_REG_IN)
#define HWIO_APCS_CS_HALT_REG_RESERVE_BMSK                                               0xfffffff0
#define HWIO_APCS_CS_HALT_REG_RESERVE_SHFT                                                      0x4
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_3_BMSK                                               0x8
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_3_SHFT                                               0x3
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_2_BMSK                                               0x4
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_2_SHFT                                               0x2
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_1_BMSK                                               0x2
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_1_SHFT                                               0x1
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_0_BMSK                                               0x1
#define HWIO_APCS_CS_HALT_REG_HALT_CS_CORE_0_SHFT                                               0x0

#define HWIO_APCS_CS_BACKGND_CURRENT_ADDR                                                (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004a8)
#define HWIO_APCS_CS_BACKGND_CURRENT_RMSK                                                0xffffffff
#define HWIO_APCS_CS_BACKGND_CURRENT_IN          \
        in_dword_masked(HWIO_APCS_CS_BACKGND_CURRENT_ADDR, HWIO_APCS_CS_BACKGND_CURRENT_RMSK)
#define HWIO_APCS_CS_BACKGND_CURRENT_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_BACKGND_CURRENT_ADDR, m)
#define HWIO_APCS_CS_BACKGND_CURRENT_OUT(v)      \
        out_dword(HWIO_APCS_CS_BACKGND_CURRENT_ADDR,v)
#define HWIO_APCS_CS_BACKGND_CURRENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_BACKGND_CURRENT_ADDR,m,v,HWIO_APCS_CS_BACKGND_CURRENT_IN)
#define HWIO_APCS_CS_BACKGND_CURRENT_RESERVE_BMSK                                        0xffffe000
#define HWIO_APCS_CS_BACKGND_CURRENT_RESERVE_SHFT                                               0xd
#define HWIO_APCS_CS_BACKGND_CURRENT_CURRENT_CONSUMED_VALUE_BMSK                             0x1fff
#define HWIO_APCS_CS_BACKGND_CURRENT_CURRENT_CONSUMED_VALUE_SHFT                                0x0

#define HWIO_APCS_CS_TIMER_RESET_ADDR                                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004ac)
#define HWIO_APCS_CS_TIMER_RESET_RMSK                                                           0x1
#define HWIO_APCS_CS_TIMER_RESET_OUT(v)      \
        out_dword(HWIO_APCS_CS_TIMER_RESET_ADDR,v)
#define HWIO_APCS_CS_TIMER_RESET_RESET_BMSK                                                     0x1
#define HWIO_APCS_CS_TIMER_RESET_RESET_SHFT                                                     0x0

#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_ADDR                                          (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004b0)
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_RMSK                                          0xffffffff
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_IN          \
        in_dword_masked(HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_ADDR, HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_RMSK)
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_ADDR, m)
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_OUT(v)      \
        out_dword(HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_ADDR,v)
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_ADDR,m,v,HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_IN)
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_RESERVE_BMSK                                  0xfffff000
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_RESERVE_SHFT                                         0xc
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_NUM_SAMPLES_BMSK                                   0xfff
#define HWIO_APCS_CS_CTRL_AGGREGATE_WINDOW_NUM_SAMPLES_SHFT                                     0x0

#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_ADDR(i)                                        (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004b4 + 0x4 * (i))
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_RMSK                                           0xffffffff
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_MAXi                                                    3
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_ADDR(i), HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_RMSK)
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_ADDR(i), mask)
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_ADDR(i),val)
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_ADDR(i),mask,val,HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_INI(i))
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_RESERVE_BMSK                                   0xfffff800
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_RESERVE_SHFT                                          0xb
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OVERRIDE_VALUE_BMSK                                 0x7ff
#define HWIO_APCS_CS_SENSOR_BYPASS_DATA_i_OVERRIDE_VALUE_SHFT                                   0x0

#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_ADDR                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004c4)
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_RMSK                                                 0xff
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_IN          \
        in_dword_masked(HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_ADDR, HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_RMSK)
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_ADDR, m)
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_OUT(v)      \
        out_dword(HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_ADDR,v)
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_ADDR,m,v,HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_IN)
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_3_BYPASS_COREOFF_BMSK                           0x80
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_3_BYPASS_COREOFF_SHFT                            0x7
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_2_BYPASS_COREOFF_BMSK                           0x40
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_2_BYPASS_COREOFF_SHFT                            0x6
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_1_BYPASS_COREOFF_BMSK                           0x20
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_1_BYPASS_COREOFF_SHFT                            0x5
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_0_BYPASS_COREOFF_BMSK                           0x10
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_0_BYPASS_COREOFF_SHFT                            0x4
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_3_BYPASS_EN_BMSK                                 0x8
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_3_BYPASS_EN_SHFT                                 0x3
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_2_BYPASS_EN_BMSK                                 0x4
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_2_BYPASS_EN_SHFT                                 0x2
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_1_BYPASS_EN_BMSK                                 0x2
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_1_BYPASS_EN_SHFT                                 0x1
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_0_BYPASS_EN_BMSK                                 0x1
#define HWIO_APCS_CS_SENSOR_BYPASS_ENABLE_CORE_0_BYPASS_EN_SHFT                                 0x0

#define HWIO_APCS_CS_TIMER_ENABLE_ADDR                                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004c8)
#define HWIO_APCS_CS_TIMER_ENABLE_RMSK                                                          0x1
#define HWIO_APCS_CS_TIMER_ENABLE_IN          \
        in_dword_masked(HWIO_APCS_CS_TIMER_ENABLE_ADDR, HWIO_APCS_CS_TIMER_ENABLE_RMSK)
#define HWIO_APCS_CS_TIMER_ENABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_TIMER_ENABLE_ADDR, m)
#define HWIO_APCS_CS_TIMER_ENABLE_OUT(v)      \
        out_dword(HWIO_APCS_CS_TIMER_ENABLE_ADDR,v)
#define HWIO_APCS_CS_TIMER_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_TIMER_ENABLE_ADDR,m,v,HWIO_APCS_CS_TIMER_ENABLE_IN)
#define HWIO_APCS_CS_TIMER_ENABLE_ENABLE_BMSK                                                   0x1
#define HWIO_APCS_CS_TIMER_ENABLE_ENABLE_SHFT                                                   0x0

#define HWIO_APCS_CS_CGC_DISABLE_ADDR                                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004cc)
#define HWIO_APCS_CS_CGC_DISABLE_RMSK                                                           0x1
#define HWIO_APCS_CS_CGC_DISABLE_IN          \
        in_dword_masked(HWIO_APCS_CS_CGC_DISABLE_ADDR, HWIO_APCS_CS_CGC_DISABLE_RMSK)
#define HWIO_APCS_CS_CGC_DISABLE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_CGC_DISABLE_ADDR, m)
#define HWIO_APCS_CS_CGC_DISABLE_OUT(v)      \
        out_dword(HWIO_APCS_CS_CGC_DISABLE_ADDR,v)
#define HWIO_APCS_CS_CGC_DISABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_CGC_DISABLE_ADDR,m,v,HWIO_APCS_CS_CGC_DISABLE_IN)
#define HWIO_APCS_CS_CGC_DISABLE_DISABLE_BMSK                                                   0x1
#define HWIO_APCS_CS_CGC_DISABLE_DISABLE_SHFT                                                   0x0

#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_ADDR                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000538)
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_RMSK                                                  0x3
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_IN          \
        in_dword_masked(HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_ADDR, HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_RMSK)
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_ADDR, m)
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OUT(v)      \
        out_dword(HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_ADDR,v)
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_ADDR,m,v,HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_IN)
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OV_VALUE_BMSK                                         0x2
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OV_VALUE_SHFT                                         0x1
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OV_ENABLE_BMSK                                        0x1
#define HWIO_APCS_CS_A_LDO_CLAMP_OV_DEBUG_OV_ENABLE_SHFT                                        0x0

#define HWIO_APCS_CS_LDO_TIMER_ADDR                                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x0000053c)
#define HWIO_APCS_CS_LDO_TIMER_RMSK                                                      0xffffffff
#define HWIO_APCS_CS_LDO_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_LDO_TIMER_ADDR, HWIO_APCS_CS_LDO_TIMER_RMSK)
#define HWIO_APCS_CS_LDO_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_LDO_TIMER_ADDR, m)
#define HWIO_APCS_CS_LDO_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_LDO_TIMER_ADDR,v)
#define HWIO_APCS_CS_LDO_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_LDO_TIMER_ADDR,m,v,HWIO_APCS_CS_LDO_TIMER_IN)
#define HWIO_APCS_CS_LDO_TIMER_VALUE_BMSK                                                0xffffffff
#define HWIO_APCS_CS_LDO_TIMER_VALUE_SHFT                                                       0x0

#define HWIO_APCS_CS_CLAMP_TIMER_ADDR                                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000540)
#define HWIO_APCS_CS_CLAMP_TIMER_RMSK                                                    0xffffffff
#define HWIO_APCS_CS_CLAMP_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_CLAMP_TIMER_ADDR, HWIO_APCS_CS_CLAMP_TIMER_RMSK)
#define HWIO_APCS_CS_CLAMP_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_CLAMP_TIMER_ADDR, m)
#define HWIO_APCS_CS_CLAMP_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_CLAMP_TIMER_ADDR,v)
#define HWIO_APCS_CS_CLAMP_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_CLAMP_TIMER_ADDR,m,v,HWIO_APCS_CS_CLAMP_TIMER_IN)
#define HWIO_APCS_CS_CLAMP_TIMER_VALUE_BMSK                                              0xffffffff
#define HWIO_APCS_CS_CLAMP_TIMER_VALUE_SHFT                                                     0x0

#define HWIO_APCS_CS_BANDGAP_TIMER_ADDR                                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000544)
#define HWIO_APCS_CS_BANDGAP_TIMER_RMSK                                                  0xffffffff
#define HWIO_APCS_CS_BANDGAP_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_BANDGAP_TIMER_ADDR, HWIO_APCS_CS_BANDGAP_TIMER_RMSK)
#define HWIO_APCS_CS_BANDGAP_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_BANDGAP_TIMER_ADDR, m)
#define HWIO_APCS_CS_BANDGAP_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_BANDGAP_TIMER_ADDR,v)
#define HWIO_APCS_CS_BANDGAP_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_BANDGAP_TIMER_ADDR,m,v,HWIO_APCS_CS_BANDGAP_TIMER_IN)
#define HWIO_APCS_CS_BANDGAP_TIMER_VALUE_BMSK                                            0xffffffff
#define HWIO_APCS_CS_BANDGAP_TIMER_VALUE_SHFT                                                   0x0

#define HWIO_APCS_CS_AMP_TIMER_ADDR                                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000548)
#define HWIO_APCS_CS_AMP_TIMER_RMSK                                                      0xffffffff
#define HWIO_APCS_CS_AMP_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_TIMER_ADDR, HWIO_APCS_CS_AMP_TIMER_RMSK)
#define HWIO_APCS_CS_AMP_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_TIMER_ADDR, m)
#define HWIO_APCS_CS_AMP_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_TIMER_ADDR,v)
#define HWIO_APCS_CS_AMP_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_TIMER_ADDR,m,v,HWIO_APCS_CS_AMP_TIMER_IN)
#define HWIO_APCS_CS_AMP_TIMER_VALUE_BMSK                                                0xffffffff
#define HWIO_APCS_CS_AMP_TIMER_VALUE_SHFT                                                       0x0

#define HWIO_APCS_CS_PD_TIMER_ADDR                                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x0000054c)
#define HWIO_APCS_CS_PD_TIMER_RMSK                                                       0xffffffff
#define HWIO_APCS_CS_PD_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_PD_TIMER_ADDR, HWIO_APCS_CS_PD_TIMER_RMSK)
#define HWIO_APCS_CS_PD_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_PD_TIMER_ADDR, m)
#define HWIO_APCS_CS_PD_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_PD_TIMER_ADDR,v)
#define HWIO_APCS_CS_PD_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_PD_TIMER_ADDR,m,v,HWIO_APCS_CS_PD_TIMER_IN)
#define HWIO_APCS_CS_PD_TIMER_VALUE_BMSK                                                 0xffffffff
#define HWIO_APCS_CS_PD_TIMER_VALUE_SHFT                                                        0x0

#define HWIO_APCS_CS_ADC_TIMER_ADDR                                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000550)
#define HWIO_APCS_CS_ADC_TIMER_RMSK                                                      0xffffffff
#define HWIO_APCS_CS_ADC_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_ADC_TIMER_ADDR, HWIO_APCS_CS_ADC_TIMER_RMSK)
#define HWIO_APCS_CS_ADC_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_ADC_TIMER_ADDR, m)
#define HWIO_APCS_CS_ADC_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_ADC_TIMER_ADDR,v)
#define HWIO_APCS_CS_ADC_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_ADC_TIMER_ADDR,m,v,HWIO_APCS_CS_ADC_TIMER_IN)
#define HWIO_APCS_CS_ADC_TIMER_VALUE_BMSK                                                0xffffffff
#define HWIO_APCS_CS_ADC_TIMER_VALUE_SHFT                                                       0x0

#define HWIO_APCS_CS_HALT_TIMER_ADDR                                                     (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000554)
#define HWIO_APCS_CS_HALT_TIMER_RMSK                                                     0xffffffff
#define HWIO_APCS_CS_HALT_TIMER_IN          \
        in_dword_masked(HWIO_APCS_CS_HALT_TIMER_ADDR, HWIO_APCS_CS_HALT_TIMER_RMSK)
#define HWIO_APCS_CS_HALT_TIMER_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_HALT_TIMER_ADDR, m)
#define HWIO_APCS_CS_HALT_TIMER_OUT(v)      \
        out_dword(HWIO_APCS_CS_HALT_TIMER_ADDR,v)
#define HWIO_APCS_CS_HALT_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_HALT_TIMER_ADDR,m,v,HWIO_APCS_CS_HALT_TIMER_IN)
#define HWIO_APCS_CS_HALT_TIMER_VALUE_BMSK                                               0xffffffff
#define HWIO_APCS_CS_HALT_TIMER_VALUE_SHFT                                                      0x0

#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_ADDR                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000558)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_RMSK                                             0x1
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_IN          \
        in_dword_masked(HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_ADDR, HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_RMSK)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_ADDR, m)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_OUT(v)      \
        out_dword(HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_ADDR,v)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_ADDR,m,v,HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_IN)
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_DONE_BMSK                                        0x1
#define HWIO_APCS_CS_ENDPOINT_CALIBRATION_DONE_DONE_SHFT                                        0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_ADDR                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x0000055c)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_RMSK                                        0x13fffff
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_ADDR, HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_ADDR, m)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_ADDR,v)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_ADDR,m,v,HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_IN)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_SW_WRM_TRIM_START_BMSK                  0x1000000
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_SW_WRM_TRIM_START_SHFT                       0x18
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_TRIM_TIMER_BMSK                          0x3fffc0
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_TRIM_TIMER_SHFT                               0x6
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_OFFSET_BMSK                                      0x3e
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_OFFSET_SHFT                                       0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_SW_TRIM_START_BMSK                            0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL1_AMP_SW_TRIM_START_SHFT                            0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_ADDR                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000560)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_RMSK                                         0xffffff
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_ADDR, HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_ADDR, m)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_ADDR,v)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_ADDR,m,v,HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_IN)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_TRIM_MAX_BMSK                         0xff0000
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_TRIM_MAX_SHFT                             0x10
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_SLOW_TRIM_MIN_BMSK                      0xff00
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_SLOW_TRIM_MIN_SHFT                         0x8
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_FAST_TRIM_MIN_BMSK                        0xff
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL2_AMP_SW_FAST_TRIM_MIN_SHFT                         0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_ADDR(i)                                     (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000564 + 0x4 * (i))
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_RMSK                                            0x3fff
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_MAXi                                                 7
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_ADDR(i), HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_ADDR(i), mask)
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_ADDR(i),val)
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_ADDR(i),mask,val,HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_INI(i))
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_TRIM_FSM_OV_STATE_BMSK                      0x3c00
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_TRIM_FSM_OV_STATE_SHFT                         0xa
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_TRIM_FSM_OV_BMSK                             0x200
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_TRIM_FSM_OV_SHFT                               0x9
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_SW_OFFSET_CANCELLATION_BMSK                  0x1fe
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_SW_OFFSET_CANCELLATION_SHFT                    0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_SW_OV_BMSK                                     0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_DEBUG_i_AMP_SW_OV_SHFT                                     0x0

#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_ADDR                                            (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000590)
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_RMSK                                                   0x1
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_IN          \
        in_dword_masked(HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_ADDR, HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_RMSK)
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_ADDR, m)
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_OUT(v)      \
        out_dword(HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_ADDR,v)
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_ADDR,m,v,HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_IN)
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_OV_ENABLE_BMSK                                         0x1
#define HWIO_APCS_CS_FORCE_ADC_TRIM_DONE_OV_ENABLE_SHFT                                         0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_ADDR                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000594)
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_RMSK                                                  0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_DONE_ADDR, HWIO_APCS_CS_AMP_CALIBRATION_DONE_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_DONE_ADDR, m)
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_CALIBRATION_DONE_ADDR,v)
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_CALIBRATION_DONE_ADDR,m,v,HWIO_APCS_CS_AMP_CALIBRATION_DONE_IN)
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_SW_OPAMP_CAL_DONE_BMSK                                0x1
#define HWIO_APCS_CS_AMP_CALIBRATION_DONE_SW_OPAMP_CAL_DONE_SHFT                                0x0

#define HWIO_APCS_CS_GPIO_DEBUG_SEL_ADDR                                                 (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005a4)
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_RMSK                                                        0xf
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_IN          \
        in_dword_masked(HWIO_APCS_CS_GPIO_DEBUG_SEL_ADDR, HWIO_APCS_CS_GPIO_DEBUG_SEL_RMSK)
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_GPIO_DEBUG_SEL_ADDR, m)
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_OUT(v)      \
        out_dword(HWIO_APCS_CS_GPIO_DEBUG_SEL_ADDR,v)
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_GPIO_DEBUG_SEL_ADDR,m,v,HWIO_APCS_CS_GPIO_DEBUG_SEL_IN)
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_VALUE_BMSK                                                  0xf
#define HWIO_APCS_CS_GPIO_DEBUG_SEL_VALUE_SHFT                                                  0x0

#define HWIO_APCS_CS_DECIMAL_ALIGN_ADDR                                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005a8)
#define HWIO_APCS_CS_DECIMAL_ALIGN_RMSK                                                        0xff
#define HWIO_APCS_CS_DECIMAL_ALIGN_IN          \
        in_dword_masked(HWIO_APCS_CS_DECIMAL_ALIGN_ADDR, HWIO_APCS_CS_DECIMAL_ALIGN_RMSK)
#define HWIO_APCS_CS_DECIMAL_ALIGN_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DECIMAL_ALIGN_ADDR, m)
#define HWIO_APCS_CS_DECIMAL_ALIGN_OUT(v)      \
        out_dword(HWIO_APCS_CS_DECIMAL_ALIGN_ADDR,v)
#define HWIO_APCS_CS_DECIMAL_ALIGN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DECIMAL_ALIGN_ADDR,m,v,HWIO_APCS_CS_DECIMAL_ALIGN_IN)
#define HWIO_APCS_CS_DECIMAL_ALIGN_CURRENTRESULT_OFFSET_VALUE_BMSK                             0xf0
#define HWIO_APCS_CS_DECIMAL_ALIGN_CURRENTRESULT_OFFSET_VALUE_SHFT                              0x4
#define HWIO_APCS_CS_DECIMAL_ALIGN_INTERCEPT_OFFSET_VALUE_BMSK                                  0xf
#define HWIO_APCS_CS_DECIMAL_ALIGN_INTERCEPT_OFFSET_VALUE_SHFT                                  0x0

#define HWIO_APCS_CS_ADC_DATA_SAMPLE_ADDR                                                (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005ac)
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_RMSK                                                    0x1fff
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_IN          \
        in_dword_masked(HWIO_APCS_CS_ADC_DATA_SAMPLE_ADDR, HWIO_APCS_CS_ADC_DATA_SAMPLE_RMSK)
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_ADC_DATA_SAMPLE_ADDR, m)
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_OUT(v)      \
        out_dword(HWIO_APCS_CS_ADC_DATA_SAMPLE_ADDR,v)
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_ADC_DATA_SAMPLE_ADDR,m,v,HWIO_APCS_CS_ADC_DATA_SAMPLE_IN)
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_ADC_RDY_PERIOD_BMSK                                     0x1fe0
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_ADC_RDY_PERIOD_SHFT                                        0x5
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_ADC_VALID_QUALIFIER_BMSK                                  0x10
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_ADC_VALID_QUALIFIER_SHFT                                   0x4
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_CYLCE_DELAY_VALUE_BMSK                                     0xf
#define HWIO_APCS_CS_ADC_DATA_SAMPLE_CYLCE_DELAY_VALUE_SHFT                                     0x0

#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_ADDR                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005b8)
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_RMSK                                      0x3fffff
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_ADDR, HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_RMSK)
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_ADDR, m)
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_ADDR,v)
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_ADDR,m,v,HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_IN)
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_PLUS_DISTANCE_BMSK                        0x3fc000
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_PLUS_DISTANCE_SHFT                             0xe
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_MINUS_DISTANCE_BMSK                         0x3fc0
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_MINUS_DISTANCE_SHFT                            0x6
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_OFFSET_DISTANCE_BMSK                          0x3f
#define HWIO_APCS_CS_AMP_WARM_RESET_TRIM_CONTROL_OFFSET_DISTANCE_SHFT                           0x0

#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_ADDR(i)                                     (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005bc + 0x4 * (i))
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_RMSK                                               0xf
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_MAXi                                                 7
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_ADDR(i), HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_RMSK)
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_ADDR(i), mask)
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_ADDR(i),val)
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_ADDR(i),mask,val,HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_INI(i))
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OV_STATE_BMSK                                      0xe
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OV_STATE_SHFT                                      0x1
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OV_EN_BMSK                                         0x1
#define HWIO_APCS_CS_AMP_PERIOD_TRIM_DEBUG_i_OV_EN_SHFT                                         0x0

#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_ADDR                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005e0)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_RMSK                                           0xffff
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_ADDR, HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_RMSK)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_ADDR, m)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_ADDR,v)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_ADDR,m,v,HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_IN)
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_AMP_TRIM_MAX_CHECK_VALUE_BMSK                  0xff00
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_AMP_TRIM_MAX_CHECK_VALUE_SHFT                     0x8
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_AMP_TRIM_MIN_CHECK_VALUE_BMSK                    0xff
#define HWIO_APCS_CS_AMP_CALIBRATION_CONTROL3_AMP_TRIM_MIN_CHECK_VALUE_SHFT                     0x0

#define HWIO_APCS_CS_REVISION_ADDR                                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000100)
#define HWIO_APCS_CS_REVISION_RMSK                                                       0xffffffff
#define HWIO_APCS_CS_REVISION_IN          \
        in_dword_masked(HWIO_APCS_CS_REVISION_ADDR, HWIO_APCS_CS_REVISION_RMSK)
#define HWIO_APCS_CS_REVISION_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_REVISION_ADDR, m)
#define HWIO_APCS_CS_REVISION_VERSION_BMSK                                               0xffffffff
#define HWIO_APCS_CS_REVISION_VERSION_SHFT                                                      0x0

#define HWIO_APCS_CS_DYN_FSM_ADDR                                                        (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000104)
#define HWIO_APCS_CS_DYN_FSM_RMSK                                                               0x3
#define HWIO_APCS_CS_DYN_FSM_IN          \
        in_dword_masked(HWIO_APCS_CS_DYN_FSM_ADDR, HWIO_APCS_CS_DYN_FSM_RMSK)
#define HWIO_APCS_CS_DYN_FSM_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYN_FSM_ADDR, m)
#define HWIO_APCS_CS_DYN_FSM_STATE_BMSK                                                         0x3
#define HWIO_APCS_CS_DYN_FSM_STATE_SHFT                                                         0x0

#define HWIO_APCS_CS_A_SENSOR_STATUS_i_ADDR(i)                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000140 + 0x4 * (i))
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_RMSK                                              0xffffffff
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_MAXi                                                       7
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_STATUS_i_ADDR(i), HWIO_APCS_CS_A_SENSOR_STATUS_i_RMSK)
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_STATUS_i_ADDR(i), mask)
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_CS_STATUS_BMSK                                    0xffffffff
#define HWIO_APCS_CS_A_SENSOR_STATUS_i_CS_STATUS_SHFT                                           0x0

#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_ADDR(i)                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000180 + 0x4 * (i))
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_RMSK                                              0x307
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_MAXi                                                  7
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_ADDR(i), HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_RMSK)
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_ADDR(i), mask)
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_OTEST_ERR_BMSK                                    0x200
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_OTEST_ERR_SHFT                                      0x9
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_DTEST_ERR_BMSK                                    0x100
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_DTEST_ERR_SHFT                                      0x8
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_OTEST_PASS_BMSK                                     0x4
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_OTEST_PASS_SHFT                                     0x2
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_DTEST_PASS_BMSK                                     0x2
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_DTEST_PASS_SHFT                                     0x1
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_ATEST_PASS_BMSK                                     0x1
#define HWIO_APCS_CS_A_SENSOR_TEST_STATUS_i_ATEST_PASS_SHFT                                     0x0

#define HWIO_APCS_CS_A_SENSOR_CTRL_i_ADDR(i)                                             (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000600 + 0x4 * (i))
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_RMSK                                                0xffffffff
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_MAXi                                                         7
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_CTRL_i_ADDR(i), HWIO_APCS_CS_A_SENSOR_CTRL_i_RMSK)
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_CTRL_i_ADDR(i), mask)
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_A_SENSOR_CTRL_i_ADDR(i),val)
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_A_SENSOR_CTRL_i_ADDR(i),mask,val,HWIO_APCS_CS_A_SENSOR_CTRL_i_INI(i))
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_CS_CONTROL_BMSK                                     0xffffffff
#define HWIO_APCS_CS_A_SENSOR_CTRL_i_CS_CONTROL_SHFT                                            0x0

#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ADDR(i)                                        (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000640 + 0x4 * (i))
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_RMSK                                            0x3073f3f
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_MAXi                                                    7
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ADDR(i), HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_RMSK)
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ADDR(i), mask)
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ADDR(i),val)
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ADDR(i),mask,val,HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_INI(i))
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_NEG_BMSK                                  0x2000000
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_NEG_SHFT                                       0x19
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_POS_BMSK                                  0x1000000
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_POS_SHFT                                       0x18
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_OTEST_EN_BMSK                                     0x40000
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_OTEST_EN_SHFT                                        0x12
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_DTEST_EN_BMSK                                     0x20000
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_DTEST_EN_SHFT                                        0x11
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_EN_BMSK                                     0x10000
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST_EN_SHFT                                        0x10
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST1_CTRL_BMSK                                   0x3f00
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST1_CTRL_SHFT                                      0x8
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST0_CTRL_BMSK                                     0x3f
#define HWIO_APCS_CS_A_SENSOR_TEST_CTRL_i_ATEST0_CTRL_SHFT                                      0x0

#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_ADDR                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000680)
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_RMSK                                               0xffff
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_IN          \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_ADDR, HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_RMSK)
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_ADDR, m)
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_OUT(v)      \
        out_dword(HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_ADDR,v)
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_ADDR,m,v,HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_IN)
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_SETTLE_TIME_DATA_BMSK                              0xff00
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_SETTLE_TIME_DATA_SHFT                                 0x8
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_SETTLE_TIME_REF_BMSK                                 0xff
#define HWIO_APCS_CS_A_SENSOR_SETTLE_TIME_SETTLE_TIME_REF_SHFT                                  0x0

#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_ADDR(i)                                       (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000006c0 + 0x4 * (i))
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_RMSK                                          0xffffffff
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_MAXi                                                   7
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_ADDR(i), HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_RMSK)
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_ADDR(i), mask)
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_ADDR(i),val)
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_ADDR(i),mask,val,HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_INI(i))
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_CS_CTRL_BMSK                                  0xffffffff
#define HWIO_APCS_CS_A_SENSOR_SPARE_CTRL_i_CS_CTRL_SHFT                                         0x0

#define HWIO_APCS_CS_DYNAMIC_MODE_ADDR                                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000700)
#define HWIO_APCS_CS_DYNAMIC_MODE_RMSK                                                   0xffff7fff
#define HWIO_APCS_CS_DYNAMIC_MODE_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_MODE_ADDR, HWIO_APCS_CS_DYNAMIC_MODE_RMSK)
#define HWIO_APCS_CS_DYNAMIC_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_MODE_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_MODE_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_MODE_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_MODE_IN)
#define HWIO_APCS_CS_DYNAMIC_MODE_BHS_STRENGTH_SETTLE_TIME_BMSK                          0xff000000
#define HWIO_APCS_CS_DYNAMIC_MODE_BHS_STRENGTH_SETTLE_TIME_SHFT                                0x18
#define HWIO_APCS_CS_DYNAMIC_MODE_BHS_STRENGTH_STEPPING_TIME_BMSK                          0xff0000
#define HWIO_APCS_CS_DYNAMIC_MODE_BHS_STRENGTH_STEPPING_TIME_SHFT                              0x10
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_FSM_OV_STATE_BMSK                                  0x6000
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_FSM_OV_STATE_SHFT                                     0xd
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_FSM_OV_EN_BMSK                                     0x1000
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_FSM_OV_EN_SHFT                                        0xc
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_MODE_BMSK                                           0xfff
#define HWIO_APCS_CS_DYNAMIC_MODE_DYNAMIC_MODE_SHFT                                             0x0

#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_ADDR                                           (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000704)
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_RMSK                                             0x3f3f3f
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_ADDR, HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_RMSK)
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_IN)
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_QUAD_CORE_BMSK                      0x3f0000
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_QUAD_CORE_SHFT                          0x10
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_TRI_CORE_BMSK                         0x3f00
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_TRI_CORE_SHFT                            0x8
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_DUAL_CORE_BMSK                          0x3f
#define HWIO_APCS_CS_DYNAMIC_BHS_STRENGTH_BHS_STRENGTH_DUAL_CORE_SHFT                           0x0

#define HWIO_APCS_CS_DYNAMIC_GAIN_ADDR                                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000708)
#define HWIO_APCS_CS_DYNAMIC_GAIN_RMSK                                                    0x7030303
#define HWIO_APCS_CS_DYNAMIC_GAIN_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_GAIN_ADDR, HWIO_APCS_CS_DYNAMIC_GAIN_RMSK)
#define HWIO_APCS_CS_DYNAMIC_GAIN_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_GAIN_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_GAIN_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_GAIN_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_GAIN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_GAIN_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_GAIN_IN)
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_QUAD_CORE_BMSK                                  0x4000000
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_QUAD_CORE_SHFT                                       0x1a
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_TRI_CORE_BMSK                                   0x2000000
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_TRI_CORE_SHFT                                        0x19
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_DUAL_CORE_BMSK                                  0x1000000
#define HWIO_APCS_CS_DYNAMIC_GAIN_BHS_SEL_DUAL_CORE_SHFT                                       0x18
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_QUAD_CORE_BMSK                                       0x30000
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_QUAD_CORE_SHFT                                          0x10
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_TRI_CORE_BMSK                                          0x300
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_TRI_CORE_SHFT                                            0x8
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_DUAL_CORE_BMSK                                           0x3
#define HWIO_APCS_CS_DYNAMIC_GAIN_GAIN_DUAL_CORE_SHFT                                           0x0

#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_ADDR(i)                                             (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000710 + 0x4 * (i))
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_RMSK                                                0x3fffffff
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_MAXi                                                         3
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_SLOPE_i_ADDR(i), HWIO_APCS_CS_DYNAMIC_SLOPE_i_RMSK)
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_SLOPE_i_ADDR(i), mask)
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_DYNAMIC_SLOPE_i_ADDR(i),val)
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_SLOPE_i_ADDR(i),mask,val,HWIO_APCS_CS_DYNAMIC_SLOPE_i_INI(i))
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_QUAD_CORE_BMSK                                0x3ff00000
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_QUAD_CORE_SHFT                                      0x14
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_TRI_CORE_BMSK                                    0xffc00
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_TRI_CORE_SHFT                                        0xa
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_DUAL_CORE_BMSK                                     0x3ff
#define HWIO_APCS_CS_DYNAMIC_SLOPE_i_SLOPE_DUAL_CORE_SHFT                                       0x0

#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_ADDR(i)                                         (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000720 + 0x4 * (i))
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_RMSK                                            0x3fffffff
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_MAXi                                                     3
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_ADDR(i), HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_RMSK)
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_ADDR(i), mask)
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_ADDR(i),val)
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_ADDR(i),mask,val,HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INI(i))
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_QUAD_CORE_BMSK                        0x3ff00000
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_QUAD_CORE_SHFT                              0x14
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_TRI_CORE_BMSK                            0xffc00
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_TRI_CORE_SHFT                                0xa
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_DUAL_CORE_BMSK                             0x3ff
#define HWIO_APCS_CS_DYNAMIC_INTERCEPT_i_INTERCEPT_DUAL_CORE_SHFT                               0x0

#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_ADDR                                         (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000730)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_RMSK                                         0x1fff1fff
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_ADDR, HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_RMSK)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_IN)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_BKGND_CURRENT_TRI_CORE_BMSK                  0x1fff0000
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_BKGND_CURRENT_TRI_CORE_SHFT                        0x10
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_BKGND_CURRENT_DUAL_CORE_BMSK                     0x1fff
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT1_BKGND_CURRENT_DUAL_CORE_SHFT                        0x0

#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_ADDR                                         (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000734)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_RMSK                                             0x1fff
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_ADDR, HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_RMSK)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_IN)
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_BKGND_CURRENT_QUAD_CORE_BMSK                     0x1fff
#define HWIO_APCS_CS_DYNAMIC_BKGND_CURRENT2_BKGND_CURRENT_QUAD_CORE_SHFT                        0x0

#define HWIO_APCS_CS_DYNAMIC_OFFSET_ADDR                                                 (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000738)
#define HWIO_APCS_CS_DYNAMIC_OFFSET_RMSK                                                  0xfff0fff
#define HWIO_APCS_CS_DYNAMIC_OFFSET_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_OFFSET_ADDR, HWIO_APCS_CS_DYNAMIC_OFFSET_RMSK)
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_OFFSET_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_OFFSET_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_OFFSET_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_OFFSET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_OFFSET_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_OFFSET_IN)
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_QUAD_CORE_BMSK                   0xf000000
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_QUAD_CORE_SHFT                        0x18
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_TRI_CORE_BMSK                     0xf00000
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_TRI_CORE_SHFT                         0x14
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_DUAL_CORE_BMSK                     0xf0000
#define HWIO_APCS_CS_DYNAMIC_OFFSET_CURRENTRESULT_OFFSET_DUAL_CORE_SHFT                        0x10
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_QUAD_CORE_BMSK                           0xf00
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_QUAD_CORE_SHFT                             0x8
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_TRI_CORE_BMSK                             0xf0
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_TRI_CORE_SHFT                              0x4
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_DUAL_CORE_BMSK                             0xf
#define HWIO_APCS_CS_DYNAMIC_OFFSET_INTERCEPT_OFFSET_DUAL_CORE_SHFT                             0x0

#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_ADDR                                            (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x0000073c)
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_RMSK                                                0xffff
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_IN          \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_ADDR, HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_RMSK)
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_ADDR, m)
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_OUT(v)      \
        out_dword(HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_ADDR,v)
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_ADDR,m,v,HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_IN)
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_QUAD_CORE_BMSK                          0xf000
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_QUAD_CORE_SHFT                             0xc
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_TRI_CORE_BMSK                            0xf00
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_TRI_CORE_SHFT                              0x8
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_DUAL_CORE_BMSK                            0xf0
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_DUAL_CORE_SHFT                             0x4
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_BMSK                                       0xf
#define HWIO_APCS_CS_DYNAMIC_INPUT_SHIFT_INPUT_SHIFT_SHFT                                       0x0

#define HWIO_APCS_CS_TEST_COUNTER_i_ADDR(i)                                              (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000740 + 0x4 * (i))
#define HWIO_APCS_CS_TEST_COUNTER_i_RMSK                                                 0xffffffff
#define HWIO_APCS_CS_TEST_COUNTER_i_MAXi                                                          7
#define HWIO_APCS_CS_TEST_COUNTER_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_TEST_COUNTER_i_ADDR(i), HWIO_APCS_CS_TEST_COUNTER_i_RMSK)
#define HWIO_APCS_CS_TEST_COUNTER_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_TEST_COUNTER_i_ADDR(i), mask)
#define HWIO_APCS_CS_TEST_COUNTER_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_TEST_COUNTER_i_ADDR(i),val)
#define HWIO_APCS_CS_TEST_COUNTER_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_TEST_COUNTER_i_ADDR(i),mask,val,HWIO_APCS_CS_TEST_COUNTER_i_INI(i))
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_EN_BMSK                                     0x80000000
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_EN_SHFT                                           0x1f
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_INV_BMSK                                    0x40000000
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_INV_SHFT                                          0x1e
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_STEP_BMSK                                   0x3ff00000
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_STEP_SHFT                                         0x14
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_END_BMSK                                       0xffc00
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_END_SHFT                                           0xa
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_START_BMSK                                       0x3ff
#define HWIO_APCS_CS_TEST_COUNTER_i_TEST_CNT_START_SHFT                                         0x0

#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_ADDR                                            (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000760)
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_RMSK                                                  0x3f
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_IN          \
        in_dword_masked(HWIO_APCS_CS_NEW_FEATURE_ENABLES_ADDR, HWIO_APCS_CS_NEW_FEATURE_ENABLES_RMSK)
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_NEW_FEATURE_ENABLES_ADDR, m)
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_OUT(v)      \
        out_dword(HWIO_APCS_CS_NEW_FEATURE_ENABLES_ADDR,v)
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_NEW_FEATURE_ENABLES_ADDR,m,v,HWIO_APCS_CS_NEW_FEATURE_ENABLES_IN)
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_STREAMING_BMSK                                     0x20
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_STREAMING_SHFT                                      0x5
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_EARLY_BHSEN_BMSK                                   0x10
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_EARLY_BHSEN_SHFT                                    0x4
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_RESTART_ON_BHSEN_BMSK                               0x8
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_RESTART_ON_BHSEN_SHFT                               0x3
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_PER_CS_POR_BMSK                                     0x4
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_PER_CS_POR_SHFT                                     0x2
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_OFFSET_BMSK                                         0x2
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_OFFSET_SHFT                                         0x1
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_RESULT_CLEAR_BMSK                                   0x1
#define HWIO_APCS_CS_NEW_FEATURE_ENABLES_EN_RESULT_CLEAR_SHFT                                   0x0

#define HWIO_APCS_CS_DTEST_RANGE_ADDR                                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000764)
#define HWIO_APCS_CS_DTEST_RANGE_RMSK                                                     0x7ff07ff
#define HWIO_APCS_CS_DTEST_RANGE_IN          \
        in_dword_masked(HWIO_APCS_CS_DTEST_RANGE_ADDR, HWIO_APCS_CS_DTEST_RANGE_RMSK)
#define HWIO_APCS_CS_DTEST_RANGE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_DTEST_RANGE_ADDR, m)
#define HWIO_APCS_CS_DTEST_RANGE_OUT(v)      \
        out_dword(HWIO_APCS_CS_DTEST_RANGE_ADDR,v)
#define HWIO_APCS_CS_DTEST_RANGE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_DTEST_RANGE_ADDR,m,v,HWIO_APCS_CS_DTEST_RANGE_IN)
#define HWIO_APCS_CS_DTEST_RANGE_DTEST_MAX_BMSK                                           0x7ff0000
#define HWIO_APCS_CS_DTEST_RANGE_DTEST_MAX_SHFT                                                0x10
#define HWIO_APCS_CS_DTEST_RANGE_DTEST_MIN_BMSK                                               0x7ff
#define HWIO_APCS_CS_DTEST_RANGE_DTEST_MIN_SHFT                                                 0x0

#define HWIO_APCS_CS_OTEST_RANGE_ADDR                                                    (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000768)
#define HWIO_APCS_CS_OTEST_RANGE_RMSK                                                     0x7ff07ff
#define HWIO_APCS_CS_OTEST_RANGE_IN          \
        in_dword_masked(HWIO_APCS_CS_OTEST_RANGE_ADDR, HWIO_APCS_CS_OTEST_RANGE_RMSK)
#define HWIO_APCS_CS_OTEST_RANGE_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_OTEST_RANGE_ADDR, m)
#define HWIO_APCS_CS_OTEST_RANGE_OUT(v)      \
        out_dword(HWIO_APCS_CS_OTEST_RANGE_ADDR,v)
#define HWIO_APCS_CS_OTEST_RANGE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_OTEST_RANGE_ADDR,m,v,HWIO_APCS_CS_OTEST_RANGE_IN)
#define HWIO_APCS_CS_OTEST_RANGE_OTEST_MAX_BMSK                                           0x7ff0000
#define HWIO_APCS_CS_OTEST_RANGE_OTEST_MAX_SHFT                                                0x10
#define HWIO_APCS_CS_OTEST_RANGE_OTEST_MIN_BMSK                                               0x7ff
#define HWIO_APCS_CS_OTEST_RANGE_OTEST_MIN_SHFT                                                 0x0

#define HWIO_APCS_CS_SW_OV_FUSE_EN_ADDR                                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005a0)
#define HWIO_APCS_CS_SW_OV_FUSE_EN_RMSK                                                         0xc
#define HWIO_APCS_CS_SW_OV_FUSE_EN_IN          \
        in_dword_masked(HWIO_APCS_CS_SW_OV_FUSE_EN_ADDR, HWIO_APCS_CS_SW_OV_FUSE_EN_RMSK)
#define HWIO_APCS_CS_SW_OV_FUSE_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_SW_OV_FUSE_EN_ADDR, m)
#define HWIO_APCS_CS_SW_OV_FUSE_EN_OUT(v)      \
        out_dword(HWIO_APCS_CS_SW_OV_FUSE_EN_ADDR,v)
#define HWIO_APCS_CS_SW_OV_FUSE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_SW_OV_FUSE_EN_ADDR,m,v,HWIO_APCS_CS_SW_OV_FUSE_EN_IN)
#define HWIO_APCS_CS_SW_OV_FUSE_EN_INTERCEPTTRIM_EN_BMSK                                        0x8
#define HWIO_APCS_CS_SW_OV_FUSE_EN_INTERCEPTTRIM_EN_SHFT                                        0x3
#define HWIO_APCS_CS_SW_OV_FUSE_EN_SLOPETRIM_EN_BMSK                                            0x4
#define HWIO_APCS_CS_SW_OV_FUSE_EN_SLOPETRIM_EN_SHFT                                            0x2

#define HWIO_APCS_CS_AMP_PERIOD_CTRL_ADDR                                                (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000005b4)
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_RMSK                                                0x83ffffff
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_IN          \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_CTRL_ADDR, HWIO_APCS_CS_AMP_PERIOD_CTRL_RMSK)
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_AMP_PERIOD_CTRL_ADDR, m)
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_CS_AMP_PERIOD_CTRL_ADDR,v)
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_AMP_PERIOD_CTRL_ADDR,m,v,HWIO_APCS_CS_AMP_PERIOD_CTRL_IN)
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_MODE_NEW_BMSK                                  0x80000000
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_MODE_NEW_SHFT                                        0x1f
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_RUNTIME_CNT_VALUE_BMSK                               0x3ff0000
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_RUNTIME_CNT_VALUE_SHFT                                    0x10
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_MODE_BMSK                                          0x8000
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_MODE_SHFT                                             0xf
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_CNT_VALUE_BMSK                                     0x7ffe
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_CNT_VALUE_SHFT                                        0x1
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_ENABLE_BMSK                                           0x1
#define HWIO_APCS_CS_AMP_PERIOD_CTRL_TRIM_ENABLE_SHFT                                           0x0

#define HWIO_APCS_CS_ADC_FSM_STATUS_i_ADDR(i)                                            (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000200 + 0x4 * (i))
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_RMSK                                                      0x7
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_MAXi                                                        7
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_ADC_FSM_STATUS_i_ADDR(i), HWIO_APCS_CS_ADC_FSM_STATUS_i_RMSK)
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_ADC_FSM_STATUS_i_ADDR(i), mask)
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_STATUS_BMSK                                               0x7
#define HWIO_APCS_CS_ADC_FSM_STATUS_i_STATUS_SHFT                                               0x0

#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADDR(i)                                             (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000770 + 0x4 * (i))
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_RMSK                                                      0x7f
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_MAXi                                                         7
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_INI(i)        \
        in_dword_masked(HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADDR(i), HWIO_APCS_CS_ADC_FSM_DEBUG_i_RMSK)
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADDR(i), mask)
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADDR(i),val)
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADDR(i),mask,val,HWIO_APCS_CS_ADC_FSM_DEBUG_i_INI(i))
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_START_SENSORS_OV_VALUE_BMSK                           0x40
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_START_SENSORS_OV_VALUE_SHFT                            0x6
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_CS_EN_OV_VALUE_BMSK                                   0x20
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_CS_EN_OV_VALUE_SHFT                                    0x5
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_OV_EN_BMSK                                            0x10
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_ADC_OV_EN_SHFT                                             0x4
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OV_STATE_BMSK                                              0xe
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OV_STATE_SHFT                                              0x1
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OV_ENABLE_BMSK                                             0x1
#define HWIO_APCS_CS_ADC_FSM_DEBUG_i_OV_ENABLE_SHFT                                             0x0

#define HWIO_APCS_CS_BHS_STRENGTH_ADDR                                                   (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x00000598)
#define HWIO_APCS_CS_BHS_STRENGTH_RMSK                                                        0x13f
#define HWIO_APCS_CS_BHS_STRENGTH_IN          \
        in_dword_masked(HWIO_APCS_CS_BHS_STRENGTH_ADDR, HWIO_APCS_CS_BHS_STRENGTH_RMSK)
#define HWIO_APCS_CS_BHS_STRENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_BHS_STRENGTH_ADDR, m)
#define HWIO_APCS_CS_BHS_STRENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CS_BHS_STRENGTH_ADDR,v)
#define HWIO_APCS_CS_BHS_STRENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_BHS_STRENGTH_ADDR,m,v,HWIO_APCS_CS_BHS_STRENGTH_IN)
#define HWIO_APCS_CS_BHS_STRENGTH_BHS_SEL_BMSK                                                0x100
#define HWIO_APCS_CS_BHS_STRENGTH_BHS_SEL_SHFT                                                  0x8
#define HWIO_APCS_CS_BHS_STRENGTH_VALUE_BMSK                                                   0x3f
#define HWIO_APCS_CS_BHS_STRENGTH_VALUE_SHFT                                                    0x0

#define HWIO_APCS_CS_POR_DEBUG_ADDR                                                      (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004f8)
#define HWIO_APCS_CS_POR_DEBUG_RMSK                                                            0xff
#define HWIO_APCS_CS_POR_DEBUG_IN          \
        in_dword_masked(HWIO_APCS_CS_POR_DEBUG_ADDR, HWIO_APCS_CS_POR_DEBUG_RMSK)
#define HWIO_APCS_CS_POR_DEBUG_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_POR_DEBUG_ADDR, m)
#define HWIO_APCS_CS_POR_DEBUG_OUT(v)      \
        out_dword(HWIO_APCS_CS_POR_DEBUG_ADDR,v)
#define HWIO_APCS_CS_POR_DEBUG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_POR_DEBUG_ADDR,m,v,HWIO_APCS_CS_POR_DEBUG_IN)
#define HWIO_APCS_CS_POR_DEBUG_RESTART_ON_EARLY_APC1_PWRDWN_BMSK                               0x80
#define HWIO_APCS_CS_POR_DEBUG_RESTART_ON_EARLY_APC1_PWRDWN_SHFT                                0x7
#define HWIO_APCS_CS_POR_DEBUG_RESTART_ON_APC0_ENABLE_BMSK                                     0x40
#define HWIO_APCS_CS_POR_DEBUG_RESTART_ON_APC0_ENABLE_SHFT                                      0x6
#define HWIO_APCS_CS_POR_DEBUG_AMP_EN_BMSK                                                     0x20
#define HWIO_APCS_CS_POR_DEBUG_AMP_EN_SHFT                                                      0x5
#define HWIO_APCS_CS_POR_DEBUG_SW_OV_BMSK                                                      0x10
#define HWIO_APCS_CS_POR_DEBUG_SW_OV_SHFT                                                       0x4
#define HWIO_APCS_CS_POR_DEBUG_FORCE_HB_EN_BMSK                                                 0x8
#define HWIO_APCS_CS_POR_DEBUG_FORCE_HB_EN_SHFT                                                 0x3
#define HWIO_APCS_CS_POR_DEBUG_FORCE_CLK_EN_BMSK                                                0x4
#define HWIO_APCS_CS_POR_DEBUG_FORCE_CLK_EN_SHFT                                                0x2
#define HWIO_APCS_CS_POR_DEBUG_BG_EN_BMSK                                                       0x2
#define HWIO_APCS_CS_POR_DEBUG_BG_EN_SHFT                                                       0x1
#define HWIO_APCS_CS_POR_DEBUG_LDO_EN_BMSK                                                      0x1
#define HWIO_APCS_CS_POR_DEBUG_LDO_EN_SHFT                                                      0x0

#define HWIO_APCS_CS_POR_DEBUG_FSM_ADDR                                                  (APCS_ISENSE_CONTROLLER_REG_BASE      + 0x000004fc)
#define HWIO_APCS_CS_POR_DEBUG_FSM_RMSK                                                       0xf7f
#define HWIO_APCS_CS_POR_DEBUG_FSM_IN          \
        in_dword_masked(HWIO_APCS_CS_POR_DEBUG_FSM_ADDR, HWIO_APCS_CS_POR_DEBUG_FSM_RMSK)
#define HWIO_APCS_CS_POR_DEBUG_FSM_INM(m)      \
        in_dword_masked(HWIO_APCS_CS_POR_DEBUG_FSM_ADDR, m)
#define HWIO_APCS_CS_POR_DEBUG_FSM_OUT(v)      \
        out_dword(HWIO_APCS_CS_POR_DEBUG_FSM_ADDR,v)
#define HWIO_APCS_CS_POR_DEBUG_FSM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CS_POR_DEBUG_FSM_ADDR,m,v,HWIO_APCS_CS_POR_DEBUG_FSM_IN)
#define HWIO_APCS_CS_POR_DEBUG_FSM_POR_FSM_SEL_BMSK                                           0xf00
#define HWIO_APCS_CS_POR_DEBUG_FSM_POR_FSM_SEL_SHFT                                             0x8
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_VALUE_EARLY_APC1_PWRDWN_BMSK                             0x40
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_VALUE_EARLY_APC1_PWRDWN_SHFT                              0x6
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_VALUE_BMSK                                               0x20
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_VALUE_SHFT                                                0x5
#define HWIO_APCS_CS_POR_DEBUG_FSM_STATE_VALUE_BMSK                                            0x1e
#define HWIO_APCS_CS_POR_DEBUG_FSM_STATE_VALUE_SHFT                                             0x1
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_EN_BMSK                                                   0x1
#define HWIO_APCS_CS_POR_DEBUG_FSM_OV_EN_SHFT                                                   0x0


/*----------------------------------------------------------------------------
 * MODULE: APCS_LLM_ELESSAR
 *--------------------------------------------------------------------------*/

#define APCS_LLM_ELESSAR_REG_BASE                                    (A53SS_BASE      + 0x001d9000)

#define HWIO_APCS_LM_HW_VERSION_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000000)
#define HWIO_APCS_LM_HW_VERSION_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_HW_VERSION_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_HW_VERSION_REG_ADDR, HWIO_APCS_LM_HW_VERSION_REG_RMSK)
#define HWIO_APCS_LM_HW_VERSION_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_HW_VERSION_REG_ADDR, m)
#define HWIO_APCS_LM_HW_VERSION_REG_MAJOR_BMSK                       0xf0000000
#define HWIO_APCS_LM_HW_VERSION_REG_MAJOR_SHFT                             0x1c
#define HWIO_APCS_LM_HW_VERSION_REG_MINOR_BMSK                        0xfff0000
#define HWIO_APCS_LM_HW_VERSION_REG_MINOR_SHFT                             0x10
#define HWIO_APCS_LM_HW_VERSION_REG_STEP_BMSK                            0xffff
#define HWIO_APCS_LM_HW_VERSION_REG_STEP_SHFT                               0x0

#define HWIO_APCS_LM_HW_CFG_REG_ADDR                                 (APCS_LLM_ELESSAR_REG_BASE      + 0x00000004)
#define HWIO_APCS_LM_HW_CFG_REG_RMSK                                  0x3ffffff
#define HWIO_APCS_LM_HW_CFG_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_HW_CFG_REG_ADDR, HWIO_APCS_LM_HW_CFG_REG_RMSK)
#define HWIO_APCS_LM_HW_CFG_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_HW_CFG_REG_ADDR, m)
#define HWIO_APCS_LM_HW_CFG_REG_NU_TTLOMS_BMSK                        0x3c00000
#define HWIO_APCS_LM_HW_CFG_REG_NU_TTLOMS_SHFT                             0x16
#define HWIO_APCS_LM_HW_CFG_REG_NU_TTLCR_BMSK                          0x3c0000
#define HWIO_APCS_LM_HW_CFG_REG_NU_TTLCR_SHFT                              0x12
#define HWIO_APCS_LM_HW_CFG_REG_NU_TSHLD_BMSK                           0x3e000
#define HWIO_APCS_LM_HW_CFG_REG_NU_TSHLD_SHFT                               0xd
#define HWIO_APCS_LM_HW_CFG_REG_NU_FIFO_BMSK                             0x1e00
#define HWIO_APCS_LM_HW_CFG_REG_NU_FIFO_SHFT                                0x9
#define HWIO_APCS_LM_HW_CFG_REG_NU_TS_BMSK                                0x1e0
#define HWIO_APCS_LM_HW_CFG_REG_NU_TS_SHFT                                  0x5
#define HWIO_APCS_LM_HW_CFG_REG_NU_CHILD_BMSK                              0x1f
#define HWIO_APCS_LM_HW_CFG_REG_NU_CHILD_SHFT                               0x0

#define HWIO_APCS_LM_CR0_REG_ADDR                                    (APCS_LLM_ELESSAR_REG_BASE      + 0x00000008)
#define HWIO_APCS_LM_CR0_REG_RMSK                                        0x7fff
#define HWIO_APCS_LM_CR0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_CR0_REG_ADDR, HWIO_APCS_LM_CR0_REG_RMSK)
#define HWIO_APCS_LM_CR0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_CR0_REG_ADDR, m)
#define HWIO_APCS_LM_CR0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_CR0_REG_ADDR,v)
#define HWIO_APCS_LM_CR0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_CR0_REG_ADDR,m,v,HWIO_APCS_LM_CR0_REG_IN)
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTI_ENCODE_BMSK                     0x7000
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTI_ENCODE_SHFT                        0xc
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTI_ACTIVE_BMSK                      0x800
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTI_ACTIVE_SHFT                        0xb
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTO_ENCODE_BMSK                      0x700
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTO_ENCODE_SHFT                        0x8
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTO_ACTIVE_BMSK                       0x80
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_CTO_ACTIVE_SHFT                        0x7
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_EVC_RUN_BMSK                          0x40
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_EVC_RUN_SHFT                           0x6
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_FIFO_BMSK                             0x20
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_FIFO_SHFT                              0x5
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_TTLCRX_BMSK                           0x10
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_TTLCRX_SHFT                            0x4
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_BMSK                                   0x8
#define HWIO_APCS_LM_CR0_REG_CR0_DBG_SHFT                                   0x3
#define HWIO_APCS_LM_CR0_REG_CR0_TOGGLE_TTLCR_BMSK                          0x4
#define HWIO_APCS_LM_CR0_REG_CR0_TOGGLE_TTLCR_SHFT                          0x2
#define HWIO_APCS_LM_CR0_REG_CR0_TOGGLE_TSHLD_BMSK                          0x2
#define HWIO_APCS_LM_CR0_REG_CR0_TOGGLE_TSHLD_SHFT                          0x1
#define HWIO_APCS_LM_CR0_REG_CR0_RUN_BMSK                                   0x1
#define HWIO_APCS_LM_CR0_REG_CR0_RUN_SHFT                                   0x0

#define HWIO_APCS_LM_TEMP_LIMIT_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000018)
#define HWIO_APCS_LM_TEMP_LIMIT_REG_RMSK                                  0xfff
#define HWIO_APCS_LM_TEMP_LIMIT_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TEMP_LIMIT_REG_ADDR, HWIO_APCS_LM_TEMP_LIMIT_REG_RMSK)
#define HWIO_APCS_LM_TEMP_LIMIT_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TEMP_LIMIT_REG_ADDR, m)
#define HWIO_APCS_LM_TEMP_LIMIT_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TEMP_LIMIT_REG_ADDR,v)
#define HWIO_APCS_LM_TEMP_LIMIT_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TEMP_LIMIT_REG_ADDR,m,v,HWIO_APCS_LM_TEMP_LIMIT_REG_IN)
#define HWIO_APCS_LM_TEMP_LIMIT_REG_TEMP_LIMIT_BMSK                       0xfff
#define HWIO_APCS_LM_TEMP_LIMIT_REG_TEMP_LIMIT_SHFT                         0x0

#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_ADDR                           (APCS_LLM_ELESSAR_REG_BASE      + 0x0000001c)
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_RMSK                               0x3fff
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC_CTRL_REG_ADDR, HWIO_APCS_LM_DBG_EVC_CTRL_REG_RMSK)
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC_CTRL_REG_ADDR, m)
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_DBG_EVC_CTRL_REG_ADDR,v)
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_DBG_EVC_CTRL_REG_ADDR,m,v,HWIO_APCS_LM_DBG_EVC_CTRL_REG_IN)
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC1_INDEX_BMSK                    0x3e00
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC1_INDEX_SHFT                       0x9
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC1_SEL_BMSK                       0x180
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC1_SEL_SHFT                         0x7
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC0_INDEX_BMSK                      0x7c
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC0_INDEX_SHFT                       0x2
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC0_SEL_BMSK                         0x3
#define HWIO_APCS_LM_DBG_EVC_CTRL_REG_EVC0_SEL_SHFT                         0x0

#define HWIO_APCS_LM_DBG_EVC0_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000020)
#define HWIO_APCS_LM_DBG_EVC0_REG_RMSK                               0xffffffff
#define HWIO_APCS_LM_DBG_EVC0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC0_REG_ADDR, HWIO_APCS_LM_DBG_EVC0_REG_RMSK)
#define HWIO_APCS_LM_DBG_EVC0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC0_REG_ADDR, m)
#define HWIO_APCS_LM_DBG_EVC0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_DBG_EVC0_REG_ADDR,v)
#define HWIO_APCS_LM_DBG_EVC0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_DBG_EVC0_REG_ADDR,m,v,HWIO_APCS_LM_DBG_EVC0_REG_IN)
#define HWIO_APCS_LM_DBG_EVC0_REG_EVC0_CNT_BMSK                      0xffffffff
#define HWIO_APCS_LM_DBG_EVC0_REG_EVC0_CNT_SHFT                             0x0

#define HWIO_APCS_LM_DBG_EVC1_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000024)
#define HWIO_APCS_LM_DBG_EVC1_REG_RMSK                               0xffffffff
#define HWIO_APCS_LM_DBG_EVC1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC1_REG_ADDR, HWIO_APCS_LM_DBG_EVC1_REG_RMSK)
#define HWIO_APCS_LM_DBG_EVC1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBG_EVC1_REG_ADDR, m)
#define HWIO_APCS_LM_DBG_EVC1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_DBG_EVC1_REG_ADDR,v)
#define HWIO_APCS_LM_DBG_EVC1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_DBG_EVC1_REG_ADDR,m,v,HWIO_APCS_LM_DBG_EVC1_REG_IN)
#define HWIO_APCS_LM_DBG_EVC1_REG_EVC1_CNT_BMSK                      0xffffffff
#define HWIO_APCS_LM_DBG_EVC1_REG_EVC1_CNT_SHFT                             0x0

#define HWIO_APCS_LM_DBGP2_REG_ADDR                                  (APCS_LLM_ELESSAR_REG_BASE      + 0x0000002c)
#define HWIO_APCS_LM_DBGP2_REG_RMSK                                         0x3
#define HWIO_APCS_LM_DBGP2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBGP2_REG_ADDR, HWIO_APCS_LM_DBGP2_REG_RMSK)
#define HWIO_APCS_LM_DBGP2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBGP2_REG_ADDR, m)
#define HWIO_APCS_LM_DBGP2_REG_IDLE_FULL_BMSK                               0x2
#define HWIO_APCS_LM_DBGP2_REG_IDLE_FULL_SHFT                               0x1
#define HWIO_APCS_LM_DBGP2_REG_BITCELLS_ON_AP_BMSK                          0x1
#define HWIO_APCS_LM_DBGP2_REG_BITCELLS_ON_AP_SHFT                          0x0

#define HWIO_APCS_LM_DBGP3_REG_ADDR                                  (APCS_LLM_ELESSAR_REG_BASE      + 0x00000030)
#define HWIO_APCS_LM_DBGP3_REG_RMSK                                      0x7fff
#define HWIO_APCS_LM_DBGP3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBGP3_REG_ADDR, HWIO_APCS_LM_DBGP3_REG_RMSK)
#define HWIO_APCS_LM_DBGP3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBGP3_REG_ADDR, m)
#define HWIO_APCS_LM_DBGP3_REG_ODCM_IN_BMSK                              0x7fff
#define HWIO_APCS_LM_DBGP3_REG_ODCM_IN_SHFT                                 0x0

#define HWIO_APCS_LM_DBGP4_REG_ADDR                                  (APCS_LLM_ELESSAR_REG_BASE      + 0x00000034)
#define HWIO_APCS_LM_DBGP4_REG_RMSK                                  0xffffffff
#define HWIO_APCS_LM_DBGP4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBGP4_REG_ADDR, HWIO_APCS_LM_DBGP4_REG_RMSK)
#define HWIO_APCS_LM_DBGP4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBGP4_REG_ADDR, m)
#define HWIO_APCS_LM_DBGP4_REG_DPM_IN_BMSK                           0xffffffff
#define HWIO_APCS_LM_DBGP4_REG_DPM_IN_SHFT                                  0x0

#define HWIO_APCS_LM_ODCM_LIM_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000038)
#define HWIO_APCS_LM_ODCM_LIM_REG_RMSK                                   0x3fff
#define HWIO_APCS_LM_ODCM_LIM_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_ODCM_LIM_REG_ADDR, HWIO_APCS_LM_ODCM_LIM_REG_RMSK)
#define HWIO_APCS_LM_ODCM_LIM_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_ODCM_LIM_REG_ADDR, m)
#define HWIO_APCS_LM_ODCM_LIM_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_ODCM_LIM_REG_ADDR,v)
#define HWIO_APCS_LM_ODCM_LIM_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_ODCM_LIM_REG_ADDR,m,v,HWIO_APCS_LM_ODCM_LIM_REG_IN)
#define HWIO_APCS_LM_ODCM_LIM_REG_ODCM_LIM_BMSK                          0x3fff
#define HWIO_APCS_LM_ODCM_LIM_REG_ODCM_LIM_SHFT                             0x0

#define HWIO_APCS_LM_TTL_WORD_OUT_REG_ADDR                           (APCS_LLM_ELESSAR_REG_BASE      + 0x0000003c)
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_RMSK                               0x1fff
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTL_WORD_OUT_REG_ADDR, HWIO_APCS_LM_TTL_WORD_OUT_REG_RMSK)
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTL_WORD_OUT_REG_ADDR, m)
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTL_WORD_OUT_REG_ADDR,v)
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTL_WORD_OUT_REG_ADDR,m,v,HWIO_APCS_LM_TTL_WORD_OUT_REG_IN)
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_TTL_WORD_OUT_BMSK                  0x1fff
#define HWIO_APCS_LM_TTL_WORD_OUT_REG_TTL_WORD_OUT_SHFT                     0x0

#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_ADDR                          (APCS_LLM_ELESSAR_REG_BASE      + 0x00000044)
#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_RMSK                              0xffff
#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_DBGP5_P2C_DST_REG_ADDR, HWIO_APCS_LM_DBGP5_P2C_DST_REG_RMSK)
#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_DBGP5_P2C_DST_REG_ADDR, m)
#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_MSG_DST_BMSK                      0xffff
#define HWIO_APCS_LM_DBGP5_P2C_DST_REG_MSG_DST_SHFT                         0x0

#define HWIO_APCS_LM_TEST_REG_ADDR                                   (APCS_LLM_ELESSAR_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_LM_TEST_REG_RMSK                                   0xffffffff
#define HWIO_APCS_LM_TEST_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TEST_REG_ADDR, HWIO_APCS_LM_TEST_REG_RMSK)
#define HWIO_APCS_LM_TEST_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TEST_REG_ADDR, m)
#define HWIO_APCS_LM_TEST_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TEST_REG_ADDR,v)
#define HWIO_APCS_LM_TEST_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TEST_REG_ADDR,m,v,HWIO_APCS_LM_TEST_REG_IN)
#define HWIO_APCS_LM_TEST_REG_TEST_VALUE_BMSK                        0xffffffff
#define HWIO_APCS_LM_TEST_REG_TEST_VALUE_SHFT                               0x0

#define HWIO_APCS_LM_TTLOMS0_REG_ADDR                                (APCS_LLM_ELESSAR_REG_BASE      + 0x000000a0)
#define HWIO_APCS_LM_TTLOMS0_REG_RMSK                                0xffffffff
#define HWIO_APCS_LM_TTLOMS0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLOMS0_REG_ADDR, HWIO_APCS_LM_TTLOMS0_REG_RMSK)
#define HWIO_APCS_LM_TTLOMS0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLOMS0_REG_ADDR, m)
#define HWIO_APCS_LM_TTLOMS0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLOMS0_REG_ADDR,v)
#define HWIO_APCS_LM_TTLOMS0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLOMS0_REG_ADDR,m,v,HWIO_APCS_LM_TTLOMS0_REG_IN)
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG7_SEL_BMSK                0xf0000000
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG7_SEL_SHFT                      0x1c
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG6_SEL_BMSK                 0xf000000
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG6_SEL_SHFT                      0x18
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG5_SEL_BMSK                  0xf00000
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG5_SEL_SHFT                      0x14
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG4_SEL_BMSK                   0xf0000
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG4_SEL_SHFT                      0x10
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG3_SEL_BMSK                    0xf000
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG3_SEL_SHFT                       0xc
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG2_SEL_BMSK                     0xf00
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG2_SEL_SHFT                       0x8
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG1_SEL_BMSK                      0xf0
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG1_SEL_SHFT                       0x4
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG0_SEL_BMSK                       0xe
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SIG0_SEL_SHFT                       0x1
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SW_VALID_BMSK                       0x1
#define HWIO_APCS_LM_TTLOMS0_REG_TTLOMS_SW_VALID_SHFT                       0x0

#define HWIO_APCS_LM_TTLOMS0_REG_CONT_ADDR                           (APCS_LLM_ELESSAR_REG_BASE      + 0x000000a4)
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_RMSK                              0xfffff
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLOMS0_REG_CONT_ADDR, HWIO_APCS_LM_TTLOMS0_REG_CONT_RMSK)
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLOMS0_REG_CONT_ADDR, m)
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLOMS0_REG_CONT_ADDR,v)
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLOMS0_REG_CONT_ADDR,m,v,HWIO_APCS_LM_TTLOMS0_REG_CONT_IN)
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG12_SEL_BMSK             0xf0000
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG12_SEL_SHFT                0x10
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG11_SEL_BMSK              0xf000
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG11_SEL_SHFT                 0xc
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG10_SEL_BMSK               0xf00
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG10_SEL_SHFT                 0x8
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG9_SEL_BMSK                 0xf0
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG9_SEL_SHFT                  0x4
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG8_SEL_BMSK                  0xf
#define HWIO_APCS_LM_TTLOMS0_REG_CONT_TTLOMS_SIG8_SEL_SHFT                  0x0

#define HWIO_APCS_LM_TTLOMS1_REG_ADDR                                (APCS_LLM_ELESSAR_REG_BASE      + 0x000000a8)
#define HWIO_APCS_LM_TTLOMS1_REG_RMSK                                0xffffffff
#define HWIO_APCS_LM_TTLOMS1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLOMS1_REG_ADDR, HWIO_APCS_LM_TTLOMS1_REG_RMSK)
#define HWIO_APCS_LM_TTLOMS1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLOMS1_REG_ADDR, m)
#define HWIO_APCS_LM_TTLOMS1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLOMS1_REG_ADDR,v)
#define HWIO_APCS_LM_TTLOMS1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLOMS1_REG_ADDR,m,v,HWIO_APCS_LM_TTLOMS1_REG_IN)
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG7_SEL_BMSK                0xf0000000
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG7_SEL_SHFT                      0x1c
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG6_SEL_BMSK                 0xf000000
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG6_SEL_SHFT                      0x18
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG5_SEL_BMSK                  0xf00000
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG5_SEL_SHFT                      0x14
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG4_SEL_BMSK                   0xf0000
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG4_SEL_SHFT                      0x10
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG3_SEL_BMSK                    0xf000
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG3_SEL_SHFT                       0xc
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG2_SEL_BMSK                     0xf00
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG2_SEL_SHFT                       0x8
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG1_SEL_BMSK                      0xf0
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG1_SEL_SHFT                       0x4
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG0_SEL_BMSK                       0xe
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SIG0_SEL_SHFT                       0x1
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SW_VALID_BMSK                       0x1
#define HWIO_APCS_LM_TTLOMS1_REG_TTLOMS_SW_VALID_SHFT                       0x0

#define HWIO_APCS_LM_TTLOMS1_REG_CONT_ADDR                           (APCS_LLM_ELESSAR_REG_BASE      + 0x000000ac)
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_RMSK                              0xfffff
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLOMS1_REG_CONT_ADDR, HWIO_APCS_LM_TTLOMS1_REG_CONT_RMSK)
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLOMS1_REG_CONT_ADDR, m)
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLOMS1_REG_CONT_ADDR,v)
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLOMS1_REG_CONT_ADDR,m,v,HWIO_APCS_LM_TTLOMS1_REG_CONT_IN)
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG12_SEL_BMSK             0xf0000
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG12_SEL_SHFT                0x10
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG11_SEL_BMSK              0xf000
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG11_SEL_SHFT                 0xc
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG10_SEL_BMSK               0xf00
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG10_SEL_SHFT                 0x8
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG9_SEL_BMSK                 0xf0
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG9_SEL_SHFT                  0x4
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG8_SEL_BMSK                  0xf
#define HWIO_APCS_LM_TTLOMS1_REG_CONT_TTLOMS_SIG8_SEL_SHFT                  0x0

#define HWIO_APCS_LM_TSIDS0_REG_ADDR                                 (APCS_LLM_ELESSAR_REG_BASE      + 0x000000b0)
#define HWIO_APCS_LM_TSIDS0_REG_RMSK                                       0xff
#define HWIO_APCS_LM_TSIDS0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSIDS0_REG_ADDR, HWIO_APCS_LM_TSIDS0_REG_RMSK)
#define HWIO_APCS_LM_TSIDS0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSIDS0_REG_ADDR, m)
#define HWIO_APCS_LM_TSIDS0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSIDS0_REG_ADDR,v)
#define HWIO_APCS_LM_TSIDS0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSIDS0_REG_ADDR,m,v,HWIO_APCS_LM_TSIDS0_REG_IN)
#define HWIO_APCS_LM_TSIDS0_REG_TSIDS_ID1_BMSK                             0xf0
#define HWIO_APCS_LM_TSIDS0_REG_TSIDS_ID1_SHFT                              0x4
#define HWIO_APCS_LM_TSIDS0_REG_TSIDS_ID0_BMSK                              0xf
#define HWIO_APCS_LM_TSIDS0_REG_TSIDS_ID0_SHFT                              0x0

#define HWIO_APCS_LM_TSENS0_REG_ADDR                                 (APCS_LLM_ELESSAR_REG_BASE      + 0x000000c0)
#define HWIO_APCS_LM_TSENS0_REG_RMSK                                     0x1fff
#define HWIO_APCS_LM_TSENS0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSENS0_REG_ADDR, HWIO_APCS_LM_TSENS0_REG_RMSK)
#define HWIO_APCS_LM_TSENS0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSENS0_REG_ADDR, m)
#define HWIO_APCS_LM_TSENS0_REG_TSENS_RT_VALID_BMSK                      0x1000
#define HWIO_APCS_LM_TSENS0_REG_TSENS_RT_VALID_SHFT                         0xc
#define HWIO_APCS_LM_TSENS0_REG_TSENS_VALUE_BMSK                          0xfff
#define HWIO_APCS_LM_TSENS0_REG_TSENS_VALUE_SHFT                            0x0

#define HWIO_APCS_LM_TSENS1_REG_ADDR                                 (APCS_LLM_ELESSAR_REG_BASE      + 0x000000c4)
#define HWIO_APCS_LM_TSENS1_REG_RMSK                                     0x1fff
#define HWIO_APCS_LM_TSENS1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSENS1_REG_ADDR, HWIO_APCS_LM_TSENS1_REG_RMSK)
#define HWIO_APCS_LM_TSENS1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSENS1_REG_ADDR, m)
#define HWIO_APCS_LM_TSENS1_REG_TSENS_RT_VALID_BMSK                      0x1000
#define HWIO_APCS_LM_TSENS1_REG_TSENS_RT_VALID_SHFT                         0xc
#define HWIO_APCS_LM_TSENS1_REG_TSENS_VALUE_BMSK                          0xfff
#define HWIO_APCS_LM_TSENS1_REG_TSENS_VALUE_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R0_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000100)
#define HWIO_APCS_LM_TTLCR0_R0_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R0_REG_ADDR, HWIO_APCS_LM_TTLCR0_R0_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R0_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R0_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R0_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R0_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R0_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R0_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R0_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R0_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R0_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R0_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R0_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R0_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R0_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R0_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R0_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R0_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R0_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R0_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R0_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R0_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000104)
#define HWIO_APCS_LM_TTLCR1_R0_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R0_REG_ADDR, HWIO_APCS_LM_TTLCR1_R0_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R0_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R0_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R0_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R0_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R0_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R0_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R0_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R0_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R0_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000108)
#define HWIO_APCS_LM_TTLCR2_R0_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R0_REG_ADDR, HWIO_APCS_LM_TTLCR2_R0_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R0_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R0_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R0_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R0_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R0_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R0_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R0_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R0_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R0_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R0_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R0_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R0_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R0_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R0_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R0_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R0_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R1_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000110)
#define HWIO_APCS_LM_TTLCR0_R1_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R1_REG_ADDR, HWIO_APCS_LM_TTLCR0_R1_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R1_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R1_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R1_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R1_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R1_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R1_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R1_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R1_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R1_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R1_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R1_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R1_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R1_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R1_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R1_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R1_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R1_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R1_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R1_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R1_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000114)
#define HWIO_APCS_LM_TTLCR1_R1_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R1_REG_ADDR, HWIO_APCS_LM_TTLCR1_R1_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R1_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R1_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R1_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R1_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R1_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R1_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R1_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R1_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R1_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000118)
#define HWIO_APCS_LM_TTLCR2_R1_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R1_REG_ADDR, HWIO_APCS_LM_TTLCR2_R1_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R1_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R1_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R1_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R1_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R1_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R1_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R1_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R1_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R1_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R1_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R1_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R1_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R1_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R1_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R1_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R1_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R2_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000120)
#define HWIO_APCS_LM_TTLCR0_R2_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R2_REG_ADDR, HWIO_APCS_LM_TTLCR0_R2_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R2_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R2_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R2_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R2_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R2_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R2_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R2_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R2_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R2_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R2_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R2_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R2_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R2_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R2_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R2_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R2_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R2_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R2_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R2_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R2_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R2_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000124)
#define HWIO_APCS_LM_TTLCR1_R2_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R2_REG_ADDR, HWIO_APCS_LM_TTLCR1_R2_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R2_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R2_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R2_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R2_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R2_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R2_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R2_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R2_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R2_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R2_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000128)
#define HWIO_APCS_LM_TTLCR2_R2_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R2_REG_ADDR, HWIO_APCS_LM_TTLCR2_R2_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R2_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R2_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R2_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R2_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R2_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R2_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R2_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R2_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R2_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R2_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R2_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R2_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R2_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R2_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R2_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R2_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R2_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R3_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000130)
#define HWIO_APCS_LM_TTLCR0_R3_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R3_REG_ADDR, HWIO_APCS_LM_TTLCR0_R3_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R3_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R3_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R3_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R3_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R3_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R3_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R3_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R3_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R3_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R3_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R3_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R3_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R3_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R3_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R3_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R3_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R3_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R3_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R3_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R3_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R3_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000134)
#define HWIO_APCS_LM_TTLCR1_R3_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R3_REG_ADDR, HWIO_APCS_LM_TTLCR1_R3_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R3_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R3_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R3_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R3_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R3_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R3_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R3_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R3_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R3_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R3_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000138)
#define HWIO_APCS_LM_TTLCR2_R3_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R3_REG_ADDR, HWIO_APCS_LM_TTLCR2_R3_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R3_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R3_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R3_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R3_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R3_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R3_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R3_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R3_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R3_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R3_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R3_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R3_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R3_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R3_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R3_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R3_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R3_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R4_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000140)
#define HWIO_APCS_LM_TTLCR0_R4_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R4_REG_ADDR, HWIO_APCS_LM_TTLCR0_R4_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R4_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R4_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R4_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R4_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R4_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R4_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R4_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R4_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R4_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R4_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R4_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R4_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R4_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R4_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R4_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R4_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R4_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R4_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R4_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R4_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R4_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R4_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000144)
#define HWIO_APCS_LM_TTLCR1_R4_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R4_REG_ADDR, HWIO_APCS_LM_TTLCR1_R4_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R4_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R4_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R4_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R4_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R4_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R4_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R4_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R4_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R4_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R4_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R4_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000148)
#define HWIO_APCS_LM_TTLCR2_R4_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R4_REG_ADDR, HWIO_APCS_LM_TTLCR2_R4_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R4_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R4_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R4_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R4_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R4_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R4_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R4_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R4_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R4_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R4_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R4_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R4_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R4_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R4_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R4_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R4_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R4_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R4_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R5_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000150)
#define HWIO_APCS_LM_TTLCR0_R5_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R5_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R5_REG_ADDR, HWIO_APCS_LM_TTLCR0_R5_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R5_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R5_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R5_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R5_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R5_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R5_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R5_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R5_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R5_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R5_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R5_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R5_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R5_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R5_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R5_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R5_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R5_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R5_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R5_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R5_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R5_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R5_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R5_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000154)
#define HWIO_APCS_LM_TTLCR1_R5_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R5_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R5_REG_ADDR, HWIO_APCS_LM_TTLCR1_R5_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R5_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R5_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R5_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R5_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R5_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R5_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R5_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R5_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R5_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R5_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R5_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R5_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000158)
#define HWIO_APCS_LM_TTLCR2_R5_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R5_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R5_REG_ADDR, HWIO_APCS_LM_TTLCR2_R5_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R5_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R5_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R5_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R5_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R5_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R5_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R5_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R5_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R5_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R5_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R5_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R5_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R5_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R5_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R5_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R5_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R5_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R5_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R5_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R6_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000160)
#define HWIO_APCS_LM_TTLCR0_R6_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R6_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R6_REG_ADDR, HWIO_APCS_LM_TTLCR0_R6_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R6_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R6_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R6_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R6_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R6_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R6_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R6_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R6_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R6_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R6_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R6_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R6_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R6_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R6_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R6_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R6_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R6_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R6_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R6_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R6_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R6_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R6_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R6_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000164)
#define HWIO_APCS_LM_TTLCR1_R6_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R6_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R6_REG_ADDR, HWIO_APCS_LM_TTLCR1_R6_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R6_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R6_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R6_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R6_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R6_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R6_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R6_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R6_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R6_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R6_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R6_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R6_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000168)
#define HWIO_APCS_LM_TTLCR2_R6_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R6_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R6_REG_ADDR, HWIO_APCS_LM_TTLCR2_R6_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R6_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R6_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R6_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R6_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R6_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R6_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R6_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R6_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R6_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R6_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R6_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R6_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R6_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R6_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R6_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R6_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R6_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R6_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R6_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R7_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000170)
#define HWIO_APCS_LM_TTLCR0_R7_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R7_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R7_REG_ADDR, HWIO_APCS_LM_TTLCR0_R7_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R7_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R7_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R7_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R7_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R7_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R7_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R7_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R7_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R7_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R7_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R7_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R7_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R7_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R7_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R7_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R7_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R7_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R7_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R7_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R7_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R7_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R7_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R7_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000174)
#define HWIO_APCS_LM_TTLCR1_R7_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R7_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R7_REG_ADDR, HWIO_APCS_LM_TTLCR1_R7_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R7_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R7_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R7_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R7_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R7_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R7_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R7_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R7_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R7_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R7_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R7_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R7_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000178)
#define HWIO_APCS_LM_TTLCR2_R7_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R7_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R7_REG_ADDR, HWIO_APCS_LM_TTLCR2_R7_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R7_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R7_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R7_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R7_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R7_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R7_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R7_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R7_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R7_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R7_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R7_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R7_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R7_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R7_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R7_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R7_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R7_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R7_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R7_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R8_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000180)
#define HWIO_APCS_LM_TTLCR0_R8_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R8_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R8_REG_ADDR, HWIO_APCS_LM_TTLCR0_R8_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R8_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R8_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R8_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R8_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R8_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R8_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R8_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R8_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R8_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R8_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R8_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R8_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R8_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R8_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R8_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R8_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R8_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R8_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R8_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R8_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R8_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R8_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R8_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000184)
#define HWIO_APCS_LM_TTLCR1_R8_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R8_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R8_REG_ADDR, HWIO_APCS_LM_TTLCR1_R8_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R8_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R8_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R8_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R8_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R8_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R8_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R8_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R8_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R8_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R8_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R8_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R8_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000188)
#define HWIO_APCS_LM_TTLCR2_R8_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R8_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R8_REG_ADDR, HWIO_APCS_LM_TTLCR2_R8_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R8_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R8_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R8_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R8_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R8_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R8_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R8_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R8_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R8_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R8_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R8_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R8_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R8_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R8_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R8_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R8_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R8_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R8_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R8_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R9_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000190)
#define HWIO_APCS_LM_TTLCR0_R9_REG_RMSK                              0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R9_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R9_REG_ADDR, HWIO_APCS_LM_TTLCR0_R9_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R9_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R9_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R9_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R9_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R9_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R9_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R9_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R9_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR0_R9_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR0_R9_REG_HYS_CNTR_SEL_BMSK                      0x100
#define HWIO_APCS_LM_TTLCR0_R9_REG_HYS_CNTR_SEL_SHFT                        0x8
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_INT_DEF_BMSK                         0x80
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_INT_DEF_SHFT                          0x7
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_INT_BMSK                             0x40
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_INT_SHFT                              0x6
#define HWIO_APCS_LM_TTLCR0_R9_REG_HW_INT_DEF_BMSK                         0x20
#define HWIO_APCS_LM_TTLCR0_R9_REG_HW_INT_DEF_SHFT                          0x5
#define HWIO_APCS_LM_TTLCR0_R9_REG_HW_INT_BMSK                             0x10
#define HWIO_APCS_LM_TTLCR0_R9_REG_HW_INT_SHFT                              0x4
#define HWIO_APCS_LM_TTLCR0_R9_REG_PULSE_TYPE_BMSK                          0x8
#define HWIO_APCS_LM_TTLCR0_R9_REG_PULSE_TYPE_SHFT                          0x3
#define HWIO_APCS_LM_TTLCR0_R9_REG_XLINK_CLR_BMSK                           0x4
#define HWIO_APCS_LM_TTLCR0_R9_REG_XLINK_CLR_SHFT                           0x2
#define HWIO_APCS_LM_TTLCR0_R9_REG_XLINK_MASK_BMSK                          0x2
#define HWIO_APCS_LM_TTLCR0_R9_REG_XLINK_MASK_SHFT                          0x1
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR0_R9_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR1_R9_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000194)
#define HWIO_APCS_LM_TTLCR1_R9_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR1_R9_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R9_REG_ADDR, HWIO_APCS_LM_TTLCR1_R9_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R9_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R9_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R9_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R9_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R9_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R9_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R9_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R9_REG_CLEAR_MASK_BMSK                   0xffff0000
#define HWIO_APCS_LM_TTLCR1_R9_REG_CLEAR_MASK_SHFT                         0x10
#define HWIO_APCS_LM_TTLCR1_R9_REG_TSHLD_MASK_BMSK                       0xffff
#define HWIO_APCS_LM_TTLCR1_R9_REG_TSHLD_MASK_SHFT                          0x0

#define HWIO_APCS_LM_TTLCR2_R9_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000198)
#define HWIO_APCS_LM_TTLCR2_R9_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TTLCR2_R9_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R9_REG_ADDR, HWIO_APCS_LM_TTLCR2_R9_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R9_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R9_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R9_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R9_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R9_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R9_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R9_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R9_REG_HYS_CNT_VAL_BMSK                  0xfffff000
#define HWIO_APCS_LM_TTLCR2_R9_REG_HYS_CNT_VAL_SHFT                         0xc
#define HWIO_APCS_LM_TTLCR2_R9_REG_SAT_CNT_VAL_BMSK                       0xfc0
#define HWIO_APCS_LM_TTLCR2_R9_REG_SAT_CNT_VAL_SHFT                         0x6
#define HWIO_APCS_LM_TTLCR2_R9_REG_RSVD_BMSK                               0x38
#define HWIO_APCS_LM_TTLCR2_R9_REG_RSVD_SHFT                                0x3
#define HWIO_APCS_LM_TTLCR2_R9_REG_HYS_CNT_ACTV_BMSK                        0x4
#define HWIO_APCS_LM_TTLCR2_R9_REG_HYS_CNT_ACTV_SHFT                        0x2
#define HWIO_APCS_LM_TTLCR2_R9_REG_RT_ACTIV_BMSK                            0x2
#define HWIO_APCS_LM_TTLCR2_R9_REG_RT_ACTIV_SHFT                            0x1
#define HWIO_APCS_LM_TTLCR2_R9_REG_RT_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TTLCR2_R9_REG_RT_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TTLCR0_R10_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001a0)
#define HWIO_APCS_LM_TTLCR0_R10_REG_RMSK                             0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R10_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R10_REG_ADDR, HWIO_APCS_LM_TTLCR0_R10_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R10_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R10_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R10_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R10_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R10_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R10_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R10_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R10_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR0_R10_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR0_R10_REG_HYS_CNTR_SEL_BMSK                     0x100
#define HWIO_APCS_LM_TTLCR0_R10_REG_HYS_CNTR_SEL_SHFT                       0x8
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_INT_DEF_BMSK                        0x80
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_INT_DEF_SHFT                         0x7
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_INT_BMSK                            0x40
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_INT_SHFT                             0x6
#define HWIO_APCS_LM_TTLCR0_R10_REG_HW_INT_DEF_BMSK                        0x20
#define HWIO_APCS_LM_TTLCR0_R10_REG_HW_INT_DEF_SHFT                         0x5
#define HWIO_APCS_LM_TTLCR0_R10_REG_HW_INT_BMSK                            0x10
#define HWIO_APCS_LM_TTLCR0_R10_REG_HW_INT_SHFT                             0x4
#define HWIO_APCS_LM_TTLCR0_R10_REG_PULSE_TYPE_BMSK                         0x8
#define HWIO_APCS_LM_TTLCR0_R10_REG_PULSE_TYPE_SHFT                         0x3
#define HWIO_APCS_LM_TTLCR0_R10_REG_XLINK_CLR_BMSK                          0x4
#define HWIO_APCS_LM_TTLCR0_R10_REG_XLINK_CLR_SHFT                          0x2
#define HWIO_APCS_LM_TTLCR0_R10_REG_XLINK_MASK_BMSK                         0x2
#define HWIO_APCS_LM_TTLCR0_R10_REG_XLINK_MASK_SHFT                         0x1
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR0_R10_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TTLCR1_R10_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001a4)
#define HWIO_APCS_LM_TTLCR1_R10_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR1_R10_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R10_REG_ADDR, HWIO_APCS_LM_TTLCR1_R10_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R10_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R10_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R10_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R10_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R10_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R10_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R10_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R10_REG_CLEAR_MASK_BMSK                  0xffff0000
#define HWIO_APCS_LM_TTLCR1_R10_REG_CLEAR_MASK_SHFT                        0x10
#define HWIO_APCS_LM_TTLCR1_R10_REG_TSHLD_MASK_BMSK                      0xffff
#define HWIO_APCS_LM_TTLCR1_R10_REG_TSHLD_MASK_SHFT                         0x0

#define HWIO_APCS_LM_TTLCR2_R10_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001a8)
#define HWIO_APCS_LM_TTLCR2_R10_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR2_R10_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R10_REG_ADDR, HWIO_APCS_LM_TTLCR2_R10_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R10_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R10_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R10_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R10_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R10_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R10_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R10_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R10_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR2_R10_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR2_R10_REG_SAT_CNT_VAL_BMSK                      0xfc0
#define HWIO_APCS_LM_TTLCR2_R10_REG_SAT_CNT_VAL_SHFT                        0x6
#define HWIO_APCS_LM_TTLCR2_R10_REG_RSVD_BMSK                              0x38
#define HWIO_APCS_LM_TTLCR2_R10_REG_RSVD_SHFT                               0x3
#define HWIO_APCS_LM_TTLCR2_R10_REG_HYS_CNT_ACTV_BMSK                       0x4
#define HWIO_APCS_LM_TTLCR2_R10_REG_HYS_CNT_ACTV_SHFT                       0x2
#define HWIO_APCS_LM_TTLCR2_R10_REG_RT_ACTIV_BMSK                           0x2
#define HWIO_APCS_LM_TTLCR2_R10_REG_RT_ACTIV_SHFT                           0x1
#define HWIO_APCS_LM_TTLCR2_R10_REG_RT_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR2_R10_REG_RT_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TTLCR0_R11_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001b0)
#define HWIO_APCS_LM_TTLCR0_R11_REG_RMSK                             0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R11_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R11_REG_ADDR, HWIO_APCS_LM_TTLCR0_R11_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R11_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R11_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R11_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R11_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R11_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R11_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R11_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R11_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR0_R11_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR0_R11_REG_HYS_CNTR_SEL_BMSK                     0x100
#define HWIO_APCS_LM_TTLCR0_R11_REG_HYS_CNTR_SEL_SHFT                       0x8
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_INT_DEF_BMSK                        0x80
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_INT_DEF_SHFT                         0x7
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_INT_BMSK                            0x40
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_INT_SHFT                             0x6
#define HWIO_APCS_LM_TTLCR0_R11_REG_HW_INT_DEF_BMSK                        0x20
#define HWIO_APCS_LM_TTLCR0_R11_REG_HW_INT_DEF_SHFT                         0x5
#define HWIO_APCS_LM_TTLCR0_R11_REG_HW_INT_BMSK                            0x10
#define HWIO_APCS_LM_TTLCR0_R11_REG_HW_INT_SHFT                             0x4
#define HWIO_APCS_LM_TTLCR0_R11_REG_PULSE_TYPE_BMSK                         0x8
#define HWIO_APCS_LM_TTLCR0_R11_REG_PULSE_TYPE_SHFT                         0x3
#define HWIO_APCS_LM_TTLCR0_R11_REG_XLINK_CLR_BMSK                          0x4
#define HWIO_APCS_LM_TTLCR0_R11_REG_XLINK_CLR_SHFT                          0x2
#define HWIO_APCS_LM_TTLCR0_R11_REG_XLINK_MASK_BMSK                         0x2
#define HWIO_APCS_LM_TTLCR0_R11_REG_XLINK_MASK_SHFT                         0x1
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR0_R11_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TTLCR1_R11_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001b4)
#define HWIO_APCS_LM_TTLCR1_R11_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR1_R11_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R11_REG_ADDR, HWIO_APCS_LM_TTLCR1_R11_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R11_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R11_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R11_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R11_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R11_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R11_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R11_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R11_REG_CLEAR_MASK_BMSK                  0xffff0000
#define HWIO_APCS_LM_TTLCR1_R11_REG_CLEAR_MASK_SHFT                        0x10
#define HWIO_APCS_LM_TTLCR1_R11_REG_TSHLD_MASK_BMSK                      0xffff
#define HWIO_APCS_LM_TTLCR1_R11_REG_TSHLD_MASK_SHFT                         0x0

#define HWIO_APCS_LM_TTLCR2_R11_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001b8)
#define HWIO_APCS_LM_TTLCR2_R11_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR2_R11_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R11_REG_ADDR, HWIO_APCS_LM_TTLCR2_R11_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R11_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R11_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R11_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R11_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R11_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R11_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R11_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R11_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR2_R11_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR2_R11_REG_SAT_CNT_VAL_BMSK                      0xfc0
#define HWIO_APCS_LM_TTLCR2_R11_REG_SAT_CNT_VAL_SHFT                        0x6
#define HWIO_APCS_LM_TTLCR2_R11_REG_RSVD_BMSK                              0x38
#define HWIO_APCS_LM_TTLCR2_R11_REG_RSVD_SHFT                               0x3
#define HWIO_APCS_LM_TTLCR2_R11_REG_HYS_CNT_ACTV_BMSK                       0x4
#define HWIO_APCS_LM_TTLCR2_R11_REG_HYS_CNT_ACTV_SHFT                       0x2
#define HWIO_APCS_LM_TTLCR2_R11_REG_RT_ACTIV_BMSK                           0x2
#define HWIO_APCS_LM_TTLCR2_R11_REG_RT_ACTIV_SHFT                           0x1
#define HWIO_APCS_LM_TTLCR2_R11_REG_RT_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR2_R11_REG_RT_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TTLCR0_R12_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001c0)
#define HWIO_APCS_LM_TTLCR0_R12_REG_RMSK                             0xfffff1ff
#define HWIO_APCS_LM_TTLCR0_R12_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R12_REG_ADDR, HWIO_APCS_LM_TTLCR0_R12_REG_RMSK)
#define HWIO_APCS_LM_TTLCR0_R12_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR0_R12_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR0_R12_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR0_R12_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR0_R12_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR0_R12_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR0_R12_REG_IN)
#define HWIO_APCS_LM_TTLCR0_R12_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR0_R12_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR0_R12_REG_HYS_CNTR_SEL_BMSK                     0x100
#define HWIO_APCS_LM_TTLCR0_R12_REG_HYS_CNTR_SEL_SHFT                       0x8
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_INT_DEF_BMSK                        0x80
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_INT_DEF_SHFT                         0x7
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_INT_BMSK                            0x40
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_INT_SHFT                             0x6
#define HWIO_APCS_LM_TTLCR0_R12_REG_HW_INT_DEF_BMSK                        0x20
#define HWIO_APCS_LM_TTLCR0_R12_REG_HW_INT_DEF_SHFT                         0x5
#define HWIO_APCS_LM_TTLCR0_R12_REG_HW_INT_BMSK                            0x10
#define HWIO_APCS_LM_TTLCR0_R12_REG_HW_INT_SHFT                             0x4
#define HWIO_APCS_LM_TTLCR0_R12_REG_PULSE_TYPE_BMSK                         0x8
#define HWIO_APCS_LM_TTLCR0_R12_REG_PULSE_TYPE_SHFT                         0x3
#define HWIO_APCS_LM_TTLCR0_R12_REG_XLINK_CLR_BMSK                          0x4
#define HWIO_APCS_LM_TTLCR0_R12_REG_XLINK_CLR_SHFT                          0x2
#define HWIO_APCS_LM_TTLCR0_R12_REG_XLINK_MASK_BMSK                         0x2
#define HWIO_APCS_LM_TTLCR0_R12_REG_XLINK_MASK_SHFT                         0x1
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR0_R12_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TTLCR1_R12_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001c4)
#define HWIO_APCS_LM_TTLCR1_R12_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR1_R12_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R12_REG_ADDR, HWIO_APCS_LM_TTLCR1_R12_REG_RMSK)
#define HWIO_APCS_LM_TTLCR1_R12_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR1_R12_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR1_R12_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR1_R12_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR1_R12_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR1_R12_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR1_R12_REG_IN)
#define HWIO_APCS_LM_TTLCR1_R12_REG_CLEAR_MASK_BMSK                  0xffff0000
#define HWIO_APCS_LM_TTLCR1_R12_REG_CLEAR_MASK_SHFT                        0x10
#define HWIO_APCS_LM_TTLCR1_R12_REG_TSHLD_MASK_BMSK                      0xffff
#define HWIO_APCS_LM_TTLCR1_R12_REG_TSHLD_MASK_SHFT                         0x0

#define HWIO_APCS_LM_TTLCR2_R12_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x000001c8)
#define HWIO_APCS_LM_TTLCR2_R12_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TTLCR2_R12_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R12_REG_ADDR, HWIO_APCS_LM_TTLCR2_R12_REG_RMSK)
#define HWIO_APCS_LM_TTLCR2_R12_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TTLCR2_R12_REG_ADDR, m)
#define HWIO_APCS_LM_TTLCR2_R12_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TTLCR2_R12_REG_ADDR,v)
#define HWIO_APCS_LM_TTLCR2_R12_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TTLCR2_R12_REG_ADDR,m,v,HWIO_APCS_LM_TTLCR2_R12_REG_IN)
#define HWIO_APCS_LM_TTLCR2_R12_REG_HYS_CNT_VAL_BMSK                 0xfffff000
#define HWIO_APCS_LM_TTLCR2_R12_REG_HYS_CNT_VAL_SHFT                        0xc
#define HWIO_APCS_LM_TTLCR2_R12_REG_SAT_CNT_VAL_BMSK                      0xfc0
#define HWIO_APCS_LM_TTLCR2_R12_REG_SAT_CNT_VAL_SHFT                        0x6
#define HWIO_APCS_LM_TTLCR2_R12_REG_RSVD_BMSK                              0x38
#define HWIO_APCS_LM_TTLCR2_R12_REG_RSVD_SHFT                               0x3
#define HWIO_APCS_LM_TTLCR2_R12_REG_HYS_CNT_ACTV_BMSK                       0x4
#define HWIO_APCS_LM_TTLCR2_R12_REG_HYS_CNT_ACTV_SHFT                       0x2
#define HWIO_APCS_LM_TTLCR2_R12_REG_RT_ACTIV_BMSK                           0x2
#define HWIO_APCS_LM_TTLCR2_R12_REG_RT_ACTIV_SHFT                           0x1
#define HWIO_APCS_LM_TTLCR2_R12_REG_RT_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TTLCR2_R12_REG_RT_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TSHLD0_R0_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000200)
#define HWIO_APCS_LM_TSHLD0_R0_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R0_REG_ADDR, HWIO_APCS_LM_TSHLD0_R0_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R0_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R0_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R0_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R0_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R0_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R0_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R0_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R0_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R0_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R0_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R0_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R0_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R0_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R0_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R0_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R0_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R0_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R0_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R0_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000204)
#define HWIO_APCS_LM_TSHLD1_R0_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R0_REG_ADDR, HWIO_APCS_LM_TSHLD1_R0_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R0_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R0_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R0_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R0_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R0_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R0_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R0_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R0_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R0_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R0_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R0_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R0_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R1_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000208)
#define HWIO_APCS_LM_TSHLD0_R1_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R1_REG_ADDR, HWIO_APCS_LM_TSHLD0_R1_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R1_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R1_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R1_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R1_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R1_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R1_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R1_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R1_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R1_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R1_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R1_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R1_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R1_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R1_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R1_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R1_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R1_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R1_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R1_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x0000020c)
#define HWIO_APCS_LM_TSHLD1_R1_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R1_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R1_REG_ADDR, HWIO_APCS_LM_TSHLD1_R1_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R1_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R1_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R1_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R1_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R1_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R1_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R1_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R1_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R1_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R1_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R1_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R1_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R1_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R1_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R2_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000210)
#define HWIO_APCS_LM_TSHLD0_R2_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R2_REG_ADDR, HWIO_APCS_LM_TSHLD0_R2_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R2_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R2_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R2_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R2_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R2_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R2_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R2_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R2_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R2_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R2_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R2_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R2_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R2_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R2_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R2_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R2_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R2_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R2_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R2_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R2_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000214)
#define HWIO_APCS_LM_TSHLD1_R2_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R2_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R2_REG_ADDR, HWIO_APCS_LM_TSHLD1_R2_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R2_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R2_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R2_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R2_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R2_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R2_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R2_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R2_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R2_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R2_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R2_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R2_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R2_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R2_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R3_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000218)
#define HWIO_APCS_LM_TSHLD0_R3_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R3_REG_ADDR, HWIO_APCS_LM_TSHLD0_R3_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R3_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R3_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R3_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R3_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R3_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R3_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R3_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R3_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R3_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R3_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R3_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R3_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R3_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R3_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R3_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R3_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R3_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R3_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R3_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R3_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x0000021c)
#define HWIO_APCS_LM_TSHLD1_R3_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R3_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R3_REG_ADDR, HWIO_APCS_LM_TSHLD1_R3_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R3_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R3_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R3_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R3_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R3_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R3_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R3_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R3_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R3_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R3_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R3_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R3_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R3_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R3_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R4_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000220)
#define HWIO_APCS_LM_TSHLD0_R4_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R4_REG_ADDR, HWIO_APCS_LM_TSHLD0_R4_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R4_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R4_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R4_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R4_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R4_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R4_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R4_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R4_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R4_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R4_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R4_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R4_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R4_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R4_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R4_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R4_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R4_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R4_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R4_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R4_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R4_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000224)
#define HWIO_APCS_LM_TSHLD1_R4_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R4_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R4_REG_ADDR, HWIO_APCS_LM_TSHLD1_R4_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R4_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R4_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R4_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R4_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R4_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R4_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R4_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R4_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R4_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R4_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R4_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R4_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R4_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R4_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R4_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R5_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000228)
#define HWIO_APCS_LM_TSHLD0_R5_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R5_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R5_REG_ADDR, HWIO_APCS_LM_TSHLD0_R5_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R5_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R5_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R5_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R5_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R5_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R5_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R5_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R5_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R5_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R5_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R5_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R5_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R5_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R5_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R5_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R5_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R5_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R5_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R5_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R5_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R5_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R5_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x0000022c)
#define HWIO_APCS_LM_TSHLD1_R5_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R5_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R5_REG_ADDR, HWIO_APCS_LM_TSHLD1_R5_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R5_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R5_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R5_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R5_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R5_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R5_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R5_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R5_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R5_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R5_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R5_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R5_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R5_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R5_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R5_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R6_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000230)
#define HWIO_APCS_LM_TSHLD0_R6_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R6_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R6_REG_ADDR, HWIO_APCS_LM_TSHLD0_R6_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R6_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R6_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R6_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R6_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R6_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R6_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R6_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R6_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R6_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R6_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R6_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R6_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R6_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R6_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R6_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R6_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R6_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R6_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R6_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R6_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R6_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R6_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000234)
#define HWIO_APCS_LM_TSHLD1_R6_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R6_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R6_REG_ADDR, HWIO_APCS_LM_TSHLD1_R6_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R6_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R6_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R6_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R6_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R6_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R6_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R6_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R6_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R6_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R6_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R6_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R6_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R6_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R6_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R6_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R7_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000238)
#define HWIO_APCS_LM_TSHLD0_R7_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R7_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R7_REG_ADDR, HWIO_APCS_LM_TSHLD0_R7_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R7_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R7_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R7_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R7_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R7_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R7_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R7_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R7_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R7_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R7_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R7_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R7_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R7_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R7_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R7_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R7_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R7_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R7_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R7_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R7_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R7_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R7_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x0000023c)
#define HWIO_APCS_LM_TSHLD1_R7_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R7_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R7_REG_ADDR, HWIO_APCS_LM_TSHLD1_R7_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R7_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R7_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R7_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R7_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R7_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R7_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R7_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R7_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R7_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R7_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R7_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R7_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R7_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R7_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R7_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R8_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000240)
#define HWIO_APCS_LM_TSHLD0_R8_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R8_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R8_REG_ADDR, HWIO_APCS_LM_TSHLD0_R8_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R8_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R8_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R8_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R8_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R8_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R8_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R8_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R8_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R8_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R8_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R8_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R8_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R8_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R8_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R8_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R8_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R8_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R8_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R8_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R8_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R8_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R8_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000244)
#define HWIO_APCS_LM_TSHLD1_R8_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R8_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R8_REG_ADDR, HWIO_APCS_LM_TSHLD1_R8_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R8_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R8_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R8_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R8_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R8_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R8_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R8_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R8_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R8_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R8_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R8_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R8_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R8_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R8_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R8_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R9_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x00000248)
#define HWIO_APCS_LM_TSHLD0_R9_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD0_R9_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R9_REG_ADDR, HWIO_APCS_LM_TSHLD0_R9_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R9_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R9_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R9_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R9_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R9_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R9_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R9_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R9_REG_P_BMSK                            0xfff00000
#define HWIO_APCS_LM_TSHLD0_R9_REG_P_SHFT                                  0x14
#define HWIO_APCS_LM_TSHLD0_R9_REG_RSLT_BMSK                            0xfff00
#define HWIO_APCS_LM_TSHLD0_R9_REG_RSLT_SHFT                                0x8
#define HWIO_APCS_LM_TSHLD0_R9_REG_RSVD_BMSK                               0xf0
#define HWIO_APCS_LM_TSHLD0_R9_REG_RSVD_SHFT                                0x4
#define HWIO_APCS_LM_TSHLD0_R9_REG_RT_CROSS_BMSK                            0x8
#define HWIO_APCS_LM_TSHLD0_R9_REG_RT_CROSS_SHFT                            0x3
#define HWIO_APCS_LM_TSHLD0_R9_REG_RT_VALID_BMSK                            0x4
#define HWIO_APCS_LM_TSHLD0_R9_REG_RT_VALID_SHFT                            0x2
#define HWIO_APCS_LM_TSHLD0_R9_REG_GTLT_BMSK                                0x2
#define HWIO_APCS_LM_TSHLD0_R9_REG_GTLT_SHFT                                0x1
#define HWIO_APCS_LM_TSHLD0_R9_REG_SW_VALID_BMSK                            0x1
#define HWIO_APCS_LM_TSHLD0_R9_REG_SW_VALID_SHFT                            0x0

#define HWIO_APCS_LM_TSHLD1_R9_REG_ADDR                              (APCS_LLM_ELESSAR_REG_BASE      + 0x0000024c)
#define HWIO_APCS_LM_TSHLD1_R9_REG_RMSK                              0xffffffff
#define HWIO_APCS_LM_TSHLD1_R9_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R9_REG_ADDR, HWIO_APCS_LM_TSHLD1_R9_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R9_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R9_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R9_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R9_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R9_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R9_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R9_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R9_REG_FSEL_BMSK                         0xf0000000
#define HWIO_APCS_LM_TSHLD1_R9_REG_FSEL_SHFT                               0x1c
#define HWIO_APCS_LM_TSHLD1_R9_REG_SHIFT_BMSK                         0xf000000
#define HWIO_APCS_LM_TSHLD1_R9_REG_SHIFT_SHFT                              0x18
#define HWIO_APCS_LM_TSHLD1_R9_REG_I_BMSK                              0xfff000
#define HWIO_APCS_LM_TSHLD1_R9_REG_I_SHFT                                   0xc
#define HWIO_APCS_LM_TSHLD1_R9_REG_D_BMSK                                 0xfff
#define HWIO_APCS_LM_TSHLD1_R9_REG_D_SHFT                                   0x0

#define HWIO_APCS_LM_TSHLD0_R10_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000250)
#define HWIO_APCS_LM_TSHLD0_R10_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD0_R10_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R10_REG_ADDR, HWIO_APCS_LM_TSHLD0_R10_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R10_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R10_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R10_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R10_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R10_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R10_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R10_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R10_REG_P_BMSK                           0xfff00000
#define HWIO_APCS_LM_TSHLD0_R10_REG_P_SHFT                                 0x14
#define HWIO_APCS_LM_TSHLD0_R10_REG_RSLT_BMSK                           0xfff00
#define HWIO_APCS_LM_TSHLD0_R10_REG_RSLT_SHFT                               0x8
#define HWIO_APCS_LM_TSHLD0_R10_REG_RSVD_BMSK                              0xf0
#define HWIO_APCS_LM_TSHLD0_R10_REG_RSVD_SHFT                               0x4
#define HWIO_APCS_LM_TSHLD0_R10_REG_RT_CROSS_BMSK                           0x8
#define HWIO_APCS_LM_TSHLD0_R10_REG_RT_CROSS_SHFT                           0x3
#define HWIO_APCS_LM_TSHLD0_R10_REG_RT_VALID_BMSK                           0x4
#define HWIO_APCS_LM_TSHLD0_R10_REG_RT_VALID_SHFT                           0x2
#define HWIO_APCS_LM_TSHLD0_R10_REG_GTLT_BMSK                               0x2
#define HWIO_APCS_LM_TSHLD0_R10_REG_GTLT_SHFT                               0x1
#define HWIO_APCS_LM_TSHLD0_R10_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TSHLD0_R10_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TSHLD1_R10_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000254)
#define HWIO_APCS_LM_TSHLD1_R10_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD1_R10_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R10_REG_ADDR, HWIO_APCS_LM_TSHLD1_R10_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R10_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R10_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R10_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R10_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R10_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R10_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R10_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R10_REG_FSEL_BMSK                        0xf0000000
#define HWIO_APCS_LM_TSHLD1_R10_REG_FSEL_SHFT                              0x1c
#define HWIO_APCS_LM_TSHLD1_R10_REG_SHIFT_BMSK                        0xf000000
#define HWIO_APCS_LM_TSHLD1_R10_REG_SHIFT_SHFT                             0x18
#define HWIO_APCS_LM_TSHLD1_R10_REG_I_BMSK                             0xfff000
#define HWIO_APCS_LM_TSHLD1_R10_REG_I_SHFT                                  0xc
#define HWIO_APCS_LM_TSHLD1_R10_REG_D_BMSK                                0xfff
#define HWIO_APCS_LM_TSHLD1_R10_REG_D_SHFT                                  0x0

#define HWIO_APCS_LM_TSHLD0_R11_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000258)
#define HWIO_APCS_LM_TSHLD0_R11_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD0_R11_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R11_REG_ADDR, HWIO_APCS_LM_TSHLD0_R11_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R11_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R11_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R11_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R11_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R11_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R11_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R11_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R11_REG_P_BMSK                           0xfff00000
#define HWIO_APCS_LM_TSHLD0_R11_REG_P_SHFT                                 0x14
#define HWIO_APCS_LM_TSHLD0_R11_REG_RSLT_BMSK                           0xfff00
#define HWIO_APCS_LM_TSHLD0_R11_REG_RSLT_SHFT                               0x8
#define HWIO_APCS_LM_TSHLD0_R11_REG_RSVD_BMSK                              0xf0
#define HWIO_APCS_LM_TSHLD0_R11_REG_RSVD_SHFT                               0x4
#define HWIO_APCS_LM_TSHLD0_R11_REG_RT_CROSS_BMSK                           0x8
#define HWIO_APCS_LM_TSHLD0_R11_REG_RT_CROSS_SHFT                           0x3
#define HWIO_APCS_LM_TSHLD0_R11_REG_RT_VALID_BMSK                           0x4
#define HWIO_APCS_LM_TSHLD0_R11_REG_RT_VALID_SHFT                           0x2
#define HWIO_APCS_LM_TSHLD0_R11_REG_GTLT_BMSK                               0x2
#define HWIO_APCS_LM_TSHLD0_R11_REG_GTLT_SHFT                               0x1
#define HWIO_APCS_LM_TSHLD0_R11_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TSHLD0_R11_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TSHLD1_R11_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x0000025c)
#define HWIO_APCS_LM_TSHLD1_R11_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD1_R11_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R11_REG_ADDR, HWIO_APCS_LM_TSHLD1_R11_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R11_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R11_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R11_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R11_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R11_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R11_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R11_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R11_REG_FSEL_BMSK                        0xf0000000
#define HWIO_APCS_LM_TSHLD1_R11_REG_FSEL_SHFT                              0x1c
#define HWIO_APCS_LM_TSHLD1_R11_REG_SHIFT_BMSK                        0xf000000
#define HWIO_APCS_LM_TSHLD1_R11_REG_SHIFT_SHFT                             0x18
#define HWIO_APCS_LM_TSHLD1_R11_REG_I_BMSK                             0xfff000
#define HWIO_APCS_LM_TSHLD1_R11_REG_I_SHFT                                  0xc
#define HWIO_APCS_LM_TSHLD1_R11_REG_D_BMSK                                0xfff
#define HWIO_APCS_LM_TSHLD1_R11_REG_D_SHFT                                  0x0

#define HWIO_APCS_LM_TSHLD0_R12_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000260)
#define HWIO_APCS_LM_TSHLD0_R12_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD0_R12_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R12_REG_ADDR, HWIO_APCS_LM_TSHLD0_R12_REG_RMSK)
#define HWIO_APCS_LM_TSHLD0_R12_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD0_R12_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD0_R12_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD0_R12_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD0_R12_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD0_R12_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD0_R12_REG_IN)
#define HWIO_APCS_LM_TSHLD0_R12_REG_P_BMSK                           0xfff00000
#define HWIO_APCS_LM_TSHLD0_R12_REG_P_SHFT                                 0x14
#define HWIO_APCS_LM_TSHLD0_R12_REG_RSLT_BMSK                           0xfff00
#define HWIO_APCS_LM_TSHLD0_R12_REG_RSLT_SHFT                               0x8
#define HWIO_APCS_LM_TSHLD0_R12_REG_RSVD_BMSK                              0xf0
#define HWIO_APCS_LM_TSHLD0_R12_REG_RSVD_SHFT                               0x4
#define HWIO_APCS_LM_TSHLD0_R12_REG_RT_CROSS_BMSK                           0x8
#define HWIO_APCS_LM_TSHLD0_R12_REG_RT_CROSS_SHFT                           0x3
#define HWIO_APCS_LM_TSHLD0_R12_REG_RT_VALID_BMSK                           0x4
#define HWIO_APCS_LM_TSHLD0_R12_REG_RT_VALID_SHFT                           0x2
#define HWIO_APCS_LM_TSHLD0_R12_REG_GTLT_BMSK                               0x2
#define HWIO_APCS_LM_TSHLD0_R12_REG_GTLT_SHFT                               0x1
#define HWIO_APCS_LM_TSHLD0_R12_REG_SW_VALID_BMSK                           0x1
#define HWIO_APCS_LM_TSHLD0_R12_REG_SW_VALID_SHFT                           0x0

#define HWIO_APCS_LM_TSHLD1_R12_REG_ADDR                             (APCS_LLM_ELESSAR_REG_BASE      + 0x00000264)
#define HWIO_APCS_LM_TSHLD1_R12_REG_RMSK                             0xffffffff
#define HWIO_APCS_LM_TSHLD1_R12_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R12_REG_ADDR, HWIO_APCS_LM_TSHLD1_R12_REG_RMSK)
#define HWIO_APCS_LM_TSHLD1_R12_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TSHLD1_R12_REG_ADDR, m)
#define HWIO_APCS_LM_TSHLD1_R12_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_TSHLD1_R12_REG_ADDR,v)
#define HWIO_APCS_LM_TSHLD1_R12_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TSHLD1_R12_REG_ADDR,m,v,HWIO_APCS_LM_TSHLD1_R12_REG_IN)
#define HWIO_APCS_LM_TSHLD1_R12_REG_FSEL_BMSK                        0xf0000000
#define HWIO_APCS_LM_TSHLD1_R12_REG_FSEL_SHFT                              0x1c
#define HWIO_APCS_LM_TSHLD1_R12_REG_SHIFT_BMSK                        0xf000000
#define HWIO_APCS_LM_TSHLD1_R12_REG_SHIFT_SHFT                             0x18
#define HWIO_APCS_LM_TSHLD1_R12_REG_I_BMSK                             0xfff000
#define HWIO_APCS_LM_TSHLD1_R12_REG_I_SHFT                                  0xc
#define HWIO_APCS_LM_TSHLD1_R12_REG_D_BMSK                                0xfff
#define HWIO_APCS_LM_TSHLD1_R12_REG_D_SHFT                                  0x0

#define HWIO_APCS_LM_FIFO0_R0_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000300)
#define HWIO_APCS_LM_FIFO0_R0_REG_RMSK                               0xffff07ff
#define HWIO_APCS_LM_FIFO0_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_FIFO0_R0_REG_ADDR, HWIO_APCS_LM_FIFO0_R0_REG_RMSK)
#define HWIO_APCS_LM_FIFO0_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_FIFO0_R0_REG_ADDR, m)
#define HWIO_APCS_LM_FIFO0_R0_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_FIFO0_R0_REG_ADDR,v)
#define HWIO_APCS_LM_FIFO0_R0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_FIFO0_R0_REG_ADDR,m,v,HWIO_APCS_LM_FIFO0_R0_REG_IN)
#define HWIO_APCS_LM_FIFO0_R0_REG_ENTRY0_RT_VALID_BMSK               0x80000000
#define HWIO_APCS_LM_FIFO0_R0_REG_ENTRY0_RT_VALID_SHFT                     0x1f
#define HWIO_APCS_LM_FIFO0_R0_REG_ENTRY0_BMSK                        0x7fff0000
#define HWIO_APCS_LM_FIFO0_R0_REG_ENTRY0_SHFT                              0x10
#define HWIO_APCS_LM_FIFO0_R0_REG_ACC_CNT_BMSK                            0x7c0
#define HWIO_APCS_LM_FIFO0_R0_REG_ACC_CNT_SHFT                              0x6
#define HWIO_APCS_LM_FIFO0_R0_REG_GROUP_BMSK                               0x3c
#define HWIO_APCS_LM_FIFO0_R0_REG_GROUP_SHFT                                0x2
#define HWIO_APCS_LM_FIFO0_R0_REG_ACC_CLK_SEL_BMSK                          0x2
#define HWIO_APCS_LM_FIFO0_R0_REG_ACC_CLK_SEL_SHFT                          0x1
#define HWIO_APCS_LM_FIFO0_R0_REG_USE_ACC_BMSK                              0x1
#define HWIO_APCS_LM_FIFO0_R0_REG_USE_ACC_SHFT                              0x0

#define HWIO_APCS_LM_FIFO1_R0_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000304)
#define HWIO_APCS_LM_FIFO1_R0_REG_RMSK                               0xffffffff
#define HWIO_APCS_LM_FIFO1_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_FIFO1_R0_REG_ADDR, HWIO_APCS_LM_FIFO1_R0_REG_RMSK)
#define HWIO_APCS_LM_FIFO1_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_FIFO1_R0_REG_ADDR, m)
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY2_RT_VALID_BMSK               0x80000000
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY2_RT_VALID_SHFT                     0x1f
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY2_BMSK                        0x7fff0000
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY2_SHFT                              0x10
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY1_RT_VALID_BMSK                   0x8000
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY1_RT_VALID_SHFT                      0xf
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY1_BMSK                            0x7fff
#define HWIO_APCS_LM_FIFO1_R0_REG_ENTRY1_SHFT                               0x0

#define HWIO_APCS_LM_FIFO2_R0_REG_ADDR                               (APCS_LLM_ELESSAR_REG_BASE      + 0x00000308)
#define HWIO_APCS_LM_FIFO2_R0_REG_RMSK                               0xffffffff
#define HWIO_APCS_LM_FIFO2_R0_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_FIFO2_R0_REG_ADDR, HWIO_APCS_LM_FIFO2_R0_REG_RMSK)
#define HWIO_APCS_LM_FIFO2_R0_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_FIFO2_R0_REG_ADDR, m)
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY4_RT_VALID_BMSK               0x80000000
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY4_RT_VALID_SHFT                     0x1f
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY4_BMSK                        0x7fff0000
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY4_SHFT                              0x10
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY3_RT_VALID_BMSK                   0x8000
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY3_RT_VALID_SHFT                      0xf
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY3_BMSK                            0x7fff
#define HWIO_APCS_LM_FIFO2_R0_REG_ENTRY3_SHFT                               0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_LLM_ELESSAR_NONSEC
 *--------------------------------------------------------------------------*/

#define APCS_LLM_ELESSAR_NONSEC_REG_BASE                (A53SS_BASE      + 0x001da000)

#define HWIO_APCS_LM_ISTR_REG_ADDR                      (APCS_LLM_ELESSAR_NONSEC_REG_BASE      + 0x0000000c)
#define HWIO_APCS_LM_ISTR_REG_RMSK                             0x1
#define HWIO_APCS_LM_ISTR_REG_IN          \
        in_dword_masked(HWIO_APCS_LM_ISTR_REG_ADDR, HWIO_APCS_LM_ISTR_REG_RMSK)
#define HWIO_APCS_LM_ISTR_REG_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_ISTR_REG_ADDR, m)
#define HWIO_APCS_LM_ISTR_REG_OUT(v)      \
        out_dword(HWIO_APCS_LM_ISTR_REG_ADDR,v)
#define HWIO_APCS_LM_ISTR_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_ISTR_REG_ADDR,m,v,HWIO_APCS_LM_ISTR_REG_IN)
#define HWIO_APCS_LM_ISTR_REG_MSTR_ISTR_BMSK                   0x1
#define HWIO_APCS_LM_ISTR_REG_MSTR_ISTR_SHFT                   0x0

#define HWIO_APCS_LM_TEST_REG2_ADDR                     (APCS_LLM_ELESSAR_NONSEC_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_LM_TEST_REG2_RMSK                     0xffffffff
#define HWIO_APCS_LM_TEST_REG2_IN          \
        in_dword_masked(HWIO_APCS_LM_TEST_REG2_ADDR, HWIO_APCS_LM_TEST_REG2_RMSK)
#define HWIO_APCS_LM_TEST_REG2_INM(m)      \
        in_dword_masked(HWIO_APCS_LM_TEST_REG2_ADDR, m)
#define HWIO_APCS_LM_TEST_REG2_OUT(v)      \
        out_dword(HWIO_APCS_LM_TEST_REG2_ADDR,v)
#define HWIO_APCS_LM_TEST_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_LM_TEST_REG2_ADDR,m,v,HWIO_APCS_LM_TEST_REG2_IN)
#define HWIO_APCS_LM_TEST_REG2_TEST_VALUE_BMSK          0xffffffff
#define HWIO_APCS_LM_TEST_REG2_TEST_VALUE_SHFT                 0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_LMH_LITE_NONSEC
 *--------------------------------------------------------------------------*/

#define APCS_LMH_LITE_NONSEC_REG_BASE                           (A53SS_BASE      + 0x001db000)

#define HWIO_APCS_INTR_STAT_ADDR                              (APCS_LMH_LITE_NONSEC_REG_BASE      + 0x00000000)
#define HWIO_APCS_INTR_STAT_RMSK                                 0x7ffff
#define HWIO_APCS_INTR_STAT_IN          \
        in_dword_masked(HWIO_APCS_INTR_STAT_ADDR, HWIO_APCS_INTR_STAT_RMSK)
#define HWIO_APCS_INTR_STAT_INM(m)      \
        in_dword_masked(HWIO_APCS_INTR_STAT_ADDR, m)
#define HWIO_APCS_INTR_STAT_CS_INTR_BMSK                         0x40000
#define HWIO_APCS_INTR_STAT_CS_INTR_SHFT                            0x12
#define HWIO_APCS_INTR_STAT_A57_THRML_CNTR_INTR_BMSK             0x3c000
#define HWIO_APCS_INTR_STAT_A57_THRML_CNTR_INTR_SHFT                 0xe
#define HWIO_APCS_INTR_STAT_A53_THRML_CNTR_INTR_BMSK              0x3c00
#define HWIO_APCS_INTR_STAT_A53_THRML_CNTR_INTR_SHFT                 0xa
#define HWIO_APCS_INTR_STAT_A57_PMIC_CNTR_INTR_BMSK                0x200
#define HWIO_APCS_INTR_STAT_A57_PMIC_CNTR_INTR_SHFT                  0x9
#define HWIO_APCS_INTR_STAT_A57_THRML_INTR_BMSK                    0x1e0
#define HWIO_APCS_INTR_STAT_A57_THRML_INTR_SHFT                      0x5
#define HWIO_APCS_INTR_STAT_A53_THRML_INTR_BMSK                     0x1e
#define HWIO_APCS_INTR_STAT_A53_THRML_INTR_SHFT                      0x1
#define HWIO_APCS_INTR_STAT_PMIC_INTR_BMSK                           0x1
#define HWIO_APCS_INTR_STAT_PMIC_INTR_SHFT                           0x0

#define HWIO_APCS_INTR_STAT_RST_ADDR                          (APCS_LMH_LITE_NONSEC_REG_BASE      + 0x00000004)
#define HWIO_APCS_INTR_STAT_RST_RMSK                             0x7ffff
#define HWIO_APCS_INTR_STAT_RST_OUT(v)      \
        out_dword(HWIO_APCS_INTR_STAT_RST_ADDR,v)
#define HWIO_APCS_INTR_STAT_RST_RST_BMSK                         0x7ffff
#define HWIO_APCS_INTR_STAT_RST_RST_SHFT                             0x0

#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_ADDR(i)                (APCS_LMH_LITE_NONSEC_REG_BASE      + 0x00000004 + 0x4 * (i))
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_RMSK                     0xffffff
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_MAXi                           32
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_INI(i)        \
        in_dword_masked(HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_ADDR(i), HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_RMSK)
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_ADDR(i), mask)
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_ADDR(i),val)
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_ADDR(i),mask,val,HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_INI(i))
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_M_RANGE_BMSK             0xff0000
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_M_RANGE_SHFT                 0x10
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_SID_H_BMSK                 0xf000
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_SID_H_SHFT                    0xc
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_SID_L_BMSK                  0xf00
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_SID_L_SHFT                    0x8
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_REPEAT_H_BMSK                0xf0
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_REPEAT_H_SHFT                 0x4
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_REPEAT_L_BMSK                 0xf
#define HWIO_APCS_A53_M_TO_SID_LUT_ROW_i_REPEAT_L_SHFT                 0x0

#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_ADDR(i)                (APCS_LMH_LITE_NONSEC_REG_BASE      + 0x00000084 + 0x4 * (i))
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_RMSK                     0xffffff
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_MAXi                           32
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_INI(i)        \
        in_dword_masked(HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_ADDR(i), HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_RMSK)
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_INMI(i,mask)    \
        in_dword_masked(HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_ADDR(i), mask)
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_OUTI(i,val)    \
        out_dword(HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_ADDR(i),val)
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_OUTMI(i,mask,val) \
        out_dword_masked_ns(HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_ADDR(i),mask,val,HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_INI(i))
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_M_RANGE_BMSK             0xff0000
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_M_RANGE_SHFT                 0x10
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_SID_H_BMSK                 0xf000
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_SID_H_SHFT                    0xc
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_SID_L_BMSK                  0xf00
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_SID_L_SHFT                    0x8
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_REPEAT_H_BMSK                0xf0
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_REPEAT_H_SHFT                 0x4
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_REPEAT_L_BMSK                 0xf
#define HWIO_APCS_A57_M_TO_SID_LUT_ROW_i_REPEAT_L_SHFT                 0x0


/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS4_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS4_APSS_ACS_REG_BASE                                                       (A53SS_BASE      + 0x00088000)

#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR                                                   (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_RMSK                                                   0x20007fff
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS4_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                   0x20000000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                         0x1d
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                          0x4000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                             0xe
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WORDLINE_EN_BMSK                                        0x2000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WORDLINE_EN_SHFT                                           0xd
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_CLK_EN_BMSK                                             0x1000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_CLK_EN_SHFT                                                0xc
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_RST_DIS_BMSK                                             0x800
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_RST_DIS_SHFT                                               0xb
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                       0x400
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                         0xa
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_LDO_BYPASS_OPEN_BMSK                                        0x200
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_LDO_BYPASS_OPEN_SHFT                                          0x9
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_LDO_DISABLE_BMSK                                            0x100
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_LDO_DISABLE_SHFT                                              0x8
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                            0x80
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                             0x7
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_GATE_CLK_BMSK                                                0x40
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_GATE_CLK_SHFT                                                 0x6
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_COREPOR_RST_BMSK                                             0x20
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_COREPOR_RST_SHFT                                              0x5
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_RST_BMSK                                                0x10
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_RST_SHFT                                                 0x4
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                              0x8
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                              0x3
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                           0x4
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                           0x2
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                           0x2
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                           0x1
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CLAMP_BMSK                                                    0x1
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CLAMP_SHFT                                                    0x0

#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR                                              (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_RMSK                                                     0x3
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_FEW_SHFT                                           0x0

#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR                                           (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_RMSK                                                  0x3
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                           0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS5_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS5_APSS_ACS_REG_BASE                                                       (A53SS_BASE      + 0x00098000)

#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR                                                   (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_RMSK                                                   0x20007fff
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS5_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                   0x20000000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                         0x1d
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                          0x4000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                             0xe
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WORDLINE_EN_BMSK                                        0x2000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WORDLINE_EN_SHFT                                           0xd
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_CLK_EN_BMSK                                             0x1000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_CLK_EN_SHFT                                                0xc
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_RST_DIS_BMSK                                             0x800
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_RST_DIS_SHFT                                               0xb
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                       0x400
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                         0xa
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_LDO_BYPASS_OPEN_BMSK                                        0x200
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_LDO_BYPASS_OPEN_SHFT                                          0x9
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_LDO_DISABLE_BMSK                                            0x100
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_LDO_DISABLE_SHFT                                              0x8
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                            0x80
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                             0x7
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_GATE_CLK_BMSK                                                0x40
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_GATE_CLK_SHFT                                                 0x6
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_COREPOR_RST_BMSK                                             0x20
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_COREPOR_RST_SHFT                                              0x5
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_RST_BMSK                                                0x10
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_RST_SHFT                                                 0x4
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                              0x8
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                              0x3
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                           0x4
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                           0x2
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                           0x2
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                           0x1
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CLAMP_BMSK                                                    0x1
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CLAMP_SHFT                                                    0x0


#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR                                              (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_RMSK                                                     0x3
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_FEW_SHFT                                           0x0

#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR                                           (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_RMSK                                                  0x3
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                           0x0


/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS6_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS6_APSS_ACS_REG_BASE                                                       (A53SS_BASE      + 0x000a8000)

#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR                                                   (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_RMSK                                                   0x20007fff
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS6_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                   0x20000000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                         0x1d
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                          0x4000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                             0xe
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WORDLINE_EN_BMSK                                        0x2000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WORDLINE_EN_SHFT                                           0xd
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_CLK_EN_BMSK                                             0x1000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_CLK_EN_SHFT                                                0xc
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_RST_DIS_BMSK                                             0x800
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_RST_DIS_SHFT                                               0xb
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                       0x400
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                         0xa
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_LDO_BYPASS_OPEN_BMSK                                        0x200
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_LDO_BYPASS_OPEN_SHFT                                          0x9
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_LDO_DISABLE_BMSK                                            0x100
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_LDO_DISABLE_SHFT                                              0x8
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                            0x80
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                             0x7
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_GATE_CLK_BMSK                                                0x40
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_GATE_CLK_SHFT                                                 0x6
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_COREPOR_RST_BMSK                                             0x20
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_COREPOR_RST_SHFT                                              0x5
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_RST_BMSK                                                0x10
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_RST_SHFT                                                 0x4
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                              0x8
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                              0x3
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                           0x4
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                           0x2
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                           0x2
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                           0x1
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CLAMP_BMSK                                                    0x1
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CLAMP_SHFT                                                    0x0

#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR                                              (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_RMSK                                                     0x3
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_FEW_SHFT                                           0x0

#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR                                           (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_RMSK                                                  0x3
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                           0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS7_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS7_APSS_ACS_REG_BASE                                                       (A53SS_BASE      + 0x000b8000)

#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR                                                   (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_RMSK                                                   0x20007fff
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS7_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                   0x20000000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                         0x1d
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                          0x4000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                             0xe
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WORDLINE_EN_BMSK                                        0x2000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WORDLINE_EN_SHFT                                           0xd
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_CLK_EN_BMSK                                             0x1000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_CLK_EN_SHFT                                                0xc
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_RST_DIS_BMSK                                             0x800
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_RST_DIS_SHFT                                               0xb
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                       0x400
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                         0xa
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_LDO_BYPASS_OPEN_BMSK                                        0x200
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_LDO_BYPASS_OPEN_SHFT                                          0x9
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_LDO_DISABLE_BMSK                                            0x100
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_LDO_DISABLE_SHFT                                              0x8
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                            0x80
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                             0x7
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_GATE_CLK_BMSK                                                0x40
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_GATE_CLK_SHFT                                                 0x6
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_COREPOR_RST_BMSK                                             0x20
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_COREPOR_RST_SHFT                                              0x5
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_RST_BMSK                                                0x10
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_RST_SHFT                                                 0x4
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                              0x8
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                              0x3
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                           0x4
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                           0x2
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                           0x2
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                           0x1
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CLAMP_BMSK                                                    0x1
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CLAMP_SHFT                                                    0x0

#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR                                              (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_RMSK                                                     0x3
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_FEW_SHFT                                           0x0

#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR                                           (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_RMSK                                                  0x3
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_REST_BMSK                                          0x2
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_REST_SHFT                                          0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                           0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                           0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS1_APSS_GLB
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS1_APSS_GLB_REG_BASE                                                     (A53SS_BASE      + 0x00011000)

#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK                                              0x45101f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_BMSK                                    0x400000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_SHFT                                        0x16
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_R_BMSK                                   0x40000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_R_SHFT                                      0x12
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_BMSK                                0x10000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_SHFT                                   0x10
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_R_BMSK                               0x1000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_R_SHFT                                  0xc
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_BMSK                                           0x1f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_SHFT                                            0x0


#define HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK                                                  0x37ffff07
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                  0x20000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                        0x1d
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_BMSK                                      0x10000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_SHFT                                            0x1c
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_BMSK                                     0x4000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_SHFT                                          0x1a
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_BMSK                                         0x3ff0000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_SHFT                                              0x10
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_BMSK                                    0x8000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_SHFT                                       0xf
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_CLAMP_BMSK                                   0x4000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_CLAMP_SHFT                                      0xe
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_RET_SLP_BMSK                                     0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_RET_SLP_SHFT                                        0xd
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_BMSK                                            0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_SHFT                                               0xc
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_BMSK                                        0x800
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_SHFT                                          0xb
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_BMSK                                             0x400
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_SHFT                                               0xa
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_BMSK                                          0x200
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_SHFT                                            0x9
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_BMSK                                           0x100
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_SHFT                                             0x8
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_BMSK                                              0x4
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_SHFT                                              0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_BMSK                                            0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SCU_ARRAY_HS_SHFT                                            0x1
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_BMSK                                             0x1
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000018)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK                                               0x1f1f37ff
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_BMSK                                 0x10000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_SHFT                                       0x1c
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_BMSK                                    0x8000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_SHFT                                         0x1b
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_BMSK                                  0x4000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_SHFT                                       0x1a
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_BMSK                                    0x2000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_SHFT                                         0x19
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_BMSK                                    0x1000000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_SHFT                                         0x18
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_BMSK                                     0x1f0000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_SHFT                                         0x10
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_BMSK                                       0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_SHFT                                          0xd
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_BMSK                                     0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_SHFT                                        0xc
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_RET_SLP_STS_BMSK                                     0x400
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_RET_SLP_STS_SHFT                                       0xa
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_BMSK                                          0x200
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_STS_SHFT                                            0x9
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_BMSK                                        0x100
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_SHFT                                          0x8
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_ARRAY_HS_STS_BMSK                                     0xff
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_ARRAY_HS_STS_SHFT                                      0x0

#define HWIO_APCS_ALIAS1_CORE_CBCR_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS1_CORE_CBCR_RMSK                                                   0x80000003
#define HWIO_APCS_ALIAS1_CORE_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, HWIO_APCS_ALIAS1_CORE_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_CORE_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_CORE_CBCR_IN)
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_APCS_ALIAS1_CORE_CBCR_HW_CTL_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_CORE_CBCR_HW_CTL_SHFT                                                   0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_CLK_ENABLE_SHFT                                               0x0

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                        (SECURITY_CONTROL_BASE      + 0x00000000)

#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000000 + 0x4 * (n))
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_MAXn                                                          71
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_SHFT                                          0x0

#define HWIO_QFPROM_RAW_JTAG_ID_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000120)
#define HWIO_QFPROM_RAW_JTAG_ID_RMSK                                                          0xffffffff
#define HWIO_QFPROM_RAW_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, HWIO_QFPROM_RAW_JTAG_ID_RMSK)
#define HWIO_QFPROM_RAW_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_RAW_JTAG_ID_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_JTAG_ID_ADDR,v)
#define HWIO_QFPROM_RAW_JTAG_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_JTAG_ID_ADDR,m,v,HWIO_QFPROM_RAW_JTAG_ID_IN)
#define HWIO_QFPROM_RAW_JTAG_ID_SPARE0_BMSK                                                   0xe0000000
#define HWIO_QFPROM_RAW_JTAG_ID_SPARE0_SHFT                                                         0x1d
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_BMSK                                             0x10000000
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_SHFT                                                   0x1c
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_BMSK                                                0xff00000
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_SHFT                                                     0x14
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_BMSK                                                     0xfffff
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_SHFT                                                         0x0

#define HWIO_QFPROM_RAW_PTE1_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000124)
#define HWIO_QFPROM_RAW_PTE1_RMSK                                                             0xffffffff
#define HWIO_QFPROM_RAW_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, HWIO_QFPROM_RAW_PTE1_RMSK)
#define HWIO_QFPROM_RAW_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, m)
#define HWIO_QFPROM_RAW_PTE1_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE1_ADDR,v)
#define HWIO_QFPROM_RAW_PTE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE1_ADDR,m,v,HWIO_QFPROM_RAW_PTE1_IN)
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_BMSK                              0xfc000000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_SHFT                                    0x1a
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_BMSK                                              0x2000000
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_SHFT                                                   0x19
#define HWIO_QFPROM_RAW_PTE1_SPARE2_BMSK                                                       0x1800000
#define HWIO_QFPROM_RAW_PTE1_SPARE2_SHFT                                                            0x17
#define HWIO_QFPROM_RAW_PTE1_BONE_PILE_BMSK                                                     0x400000
#define HWIO_QFPROM_RAW_PTE1_BONE_PILE_SHFT                                                         0x16
#define HWIO_QFPROM_RAW_PTE1_A53_ACC_SETTINGS_ID_BMSK                                           0x300000
#define HWIO_QFPROM_RAW_PTE1_A53_ACC_SETTINGS_ID_SHFT                                               0x14
#define HWIO_QFPROM_RAW_PTE1_SPARE1_BMSK                                                         0xfe000
#define HWIO_QFPROM_RAW_PTE1_SPARE1_SHFT                                                             0xd
#define HWIO_QFPROM_RAW_PTE1_WAFER_BIN_BMSK                                                       0x1c00
#define HWIO_QFPROM_RAW_PTE1_WAFER_BIN_SHFT                                                          0xa
#define HWIO_QFPROM_RAW_PTE1_METAL_REV_BMSK                                                        0x300
#define HWIO_QFPROM_RAW_PTE1_METAL_REV_SHFT                                                          0x8
#define HWIO_QFPROM_RAW_PTE1_SPARE0_BMSK                                                            0xff
#define HWIO_QFPROM_RAW_PTE1_SPARE0_SHFT                                                             0x0

#define HWIO_QFPROM_RAW_SERIAL_NUM_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000128)
#define HWIO_QFPROM_RAW_SERIAL_NUM_RMSK                                                       0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, HWIO_QFPROM_RAW_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_RAW_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,v)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,m,v,HWIO_QFPROM_RAW_SERIAL_NUM_IN)
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_PTE2_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000012c)
#define HWIO_QFPROM_RAW_PTE2_RMSK                                                             0xffffffff
#define HWIO_QFPROM_RAW_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, HWIO_QFPROM_RAW_PTE2_RMSK)
#define HWIO_QFPROM_RAW_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, m)
#define HWIO_QFPROM_RAW_PTE2_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE2_ADDR,v)
#define HWIO_QFPROM_RAW_PTE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE2_ADDR,m,v,HWIO_QFPROM_RAW_PTE2_IN)
#define HWIO_QFPROM_RAW_PTE2_WAFER_ID_BMSK                                                    0xf8000000
#define HWIO_QFPROM_RAW_PTE2_WAFER_ID_SHFT                                                          0x1b
#define HWIO_QFPROM_RAW_PTE2_DIE_X_BMSK                                                        0x7f80000
#define HWIO_QFPROM_RAW_PTE2_DIE_X_SHFT                                                             0x13
#define HWIO_QFPROM_RAW_PTE2_DIE_Y_BMSK                                                          0x7f800
#define HWIO_QFPROM_RAW_PTE2_DIE_Y_SHFT                                                              0xb
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_BMSK                                                       0x700
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_SHFT                                                         0x8
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_BMSK                                                   0xe0
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_SHFT                                                    0x5
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_BMSK                                                         0x1c
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_SHFT                                                          0x2
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_BMSK                                                         0x3
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_SHFT                                                         0x0

#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000130)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RSVD0_BMSK                                             0xf8000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RSVD0_SHFT                                                   0x1b
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE26_BMSK                                            0x4000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE26_SHFT                                                 0x1a
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE25_BMSK                                            0x2000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE25_SHFT                                                 0x19
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE24_BMSK                                            0x1000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE24_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE23_BMSK                                             0x800000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE23_SHFT                                                 0x17
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE22_BMSK                                             0x400000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE22_SHFT                                                 0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                       0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                            0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                               0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_BMSK                                                 0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_SHFT                                                    0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_BMSK                                               0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_SHFT                                                  0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB12_BMSK                                               0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB12_SHFT                                                  0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                           0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                             0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                 0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CM_FEAT_CONFIG_BMSK                                         0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CM_FEAT_CONFIG_SHFT                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                             0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                              0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                         0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                          0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                         0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                          0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                          0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                          0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                               0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                               0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_BMSK                                                      0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_SHFT                                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CRI_CM_PRIVATE_BMSK                                           0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CRI_CM_PRIVATE_SHFT                                           0x0

#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000134)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RSVD0_BMSK                                             0xf8000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RSVD0_SHFT                                                   0x1b
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE26_BMSK                                            0x4000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE26_SHFT                                                 0x1a
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE25_BMSK                                            0x2000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE25_SHFT                                                 0x19
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE24_BMSK                                            0x1000000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE24_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE23_BMSK                                             0x800000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE23_SHFT                                                 0x17
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE22_BMSK                                             0x400000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE22_SHFT                                                 0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                       0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                            0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                               0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_BMSK                                                 0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_SHFT                                                    0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_BMSK                                               0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_SHFT                                                  0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB12_BMSK                                               0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB12_SHFT                                                  0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                           0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                             0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                 0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CM_FEAT_CONFIG_BMSK                                         0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CM_FEAT_CONFIG_SHFT                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                             0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                              0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                         0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                          0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                         0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                          0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                          0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                          0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                               0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                               0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_BMSK                                                      0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_SHFT                                                      0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CRI_CM_PRIVATE_BMSK                                           0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CRI_CM_PRIVATE_SHFT                                           0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000138)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                               0xfc000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                     0x1a
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                            0x3fff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                  0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                              0xffe
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                                0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                                0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                                0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000013c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                      0xfffc0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                            0x12
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                  0x3ffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                      0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000140)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                             0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000144)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                            0x18
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                     0xfff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                          0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                               0xff0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                 0x4
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                             0xf
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                             0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000148)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                          0xffff0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                                0x10
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                              0xffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000014c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                       0xffffff00
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                              0x8
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                       0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000150)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RSVD0_BMSK                                        0xe0000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RSVD0_SHFT                                              0x1d
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_DISABLE_POLICY_BMSK                         0x10000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_DISABLE_POLICY_SHFT                               0x1c
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG26_SECURE_BMSK                            0x8000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG26_SECURE_SHFT                                 0x1b
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG25_SECURE_BMSK                            0x4000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG25_SECURE_SHFT                                 0x1a
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG24_SECURE_BMSK                            0x2000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG24_SECURE_SHFT                                 0x19
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG23_SECURE_BMSK                            0x1000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG23_SECURE_SHFT                                 0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG22_SECURE_BMSK                             0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG22_SECURE_SHFT                                 0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                    0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                            0x3c0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                                0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                              0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                 0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                              0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                 0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE1_BMSK                                           0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE1_SHFT                                              0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                               0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                  0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                               0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                  0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                          0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                             0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                  0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                    0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                              0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                                0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                     0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                       0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                               0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                 0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                          0xe0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                           0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                 0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                               0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                               0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                        0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                        0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                     0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000154)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                       0xf8000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                             0x1b
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SDCC5_SCM_FORCE_EFUSE_KEY_BMSK                     0x4000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SDCC5_SCM_FORCE_EFUSE_KEY_SHFT                          0x1a
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_BMSK                        0x2000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_SHFT                             0x19
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_BMSK                           0x1000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_SHFT                                0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_BMSK                            0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_SHFT                                0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                       0x780000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                           0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                            0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                               0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                             0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                                0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                              0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                 0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                                0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                   0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                   0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                             0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                                0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                              0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                 0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                    0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                      0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                    0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                      0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                              0x80
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                               0x7
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                  0x40
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                   0x6
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                  0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                   0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                   0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                   0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000158)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                               0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                     0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                        0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                           0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000015c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                   0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                       0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                          0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000160)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_0_BMSK                        0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_0_SHFT                              0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_BMSK                               0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_SHFT                                     0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_1_BMSK                        0x20000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_1_SHFT                              0x1d
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MDP_EFUSE_LTC0_DISABLE_BMSK                      0x10000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MDP_EFUSE_LTC0_DISABLE_SHFT                            0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_NIDNT_DISABLE_BMSK                                0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_NIDNT_DISABLE_SHFT                                     0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_DISABLE_HEVC_BMSK                           0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_DISABLE_HEVC_SHFT                                0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_BMSK                       0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_SHFT                            0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_BMSK                          0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_SHFT                               0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_BMSK                           0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_SHFT                               0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_22_BMSK                                      0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_22_SHFT                                          0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_NAV_DISABLE_BMSK                                   0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_NAV_DISABLE_SHFT                                       0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_20_BMSK                                      0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_20_SHFT                                          0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_SPARE_DISABLE_BMSK                            0xe0000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_SPARE_DISABLE_SHFT                               0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_UIM2_DISABLE_BMSK                             0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_UIM2_DISABLE_SHFT                                0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_UIM1_DISABLE_BMSK                              0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_UIM1_DISABLE_SHFT                                 0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SMMU_TBU_BYPASS_DISABLE_BMSK                         0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SMMU_TBU_BYPASS_DISABLE_SHFT                            0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_DSDA_DISABLE_BMSK                              0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_DSDA_DISABLE_SHFT                                 0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX3_DISABLE_BMSK                             0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX3_DISABLE_SHFT                                0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX2_DISABLE_BMSK                              0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX2_DISABLE_SHFT                                0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_GLOBAL_DISABLE_BMSK                             0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_GLOBAL_DISABLE_SHFT                               0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_9_BMSK                                          0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_9_SHFT                                            0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_8_BMSK                                          0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_8_SHFT                                            0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_7_BMSK                                           0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPARE_7_SHFT                                            0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_ABOVE_CAT1_DISABLE_BMSK                      0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_ABOVE_CAT1_DISABLE_SHFT                       0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_TDSCDMA_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_TDSCDMA_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_HSPA_DISABLE_BMSK                                0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_HSPA_DISABLE_SHFT                                 0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_WCDMA_DISABLE_BMSK                                0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_WCDMA_DISABLE_SHFT                                0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_DO_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_DO_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_1X_DISABLE_BMSK                                   0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MODEM_1X_DISABLE_SHFT                                   0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000164)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                 0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                       0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CM_FEAT_CONFIG_DISABLE_BMSK                      0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CM_FEAT_CONFIG_DISABLE_SHFT                            0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_GFX_FREQ_LIMIT_FUSE_BMSK                         0x30000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_GFX_FREQ_LIMIT_FUSE_SHFT                               0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SYSBARDISABLE_BMSK                                0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SYSBARDISABLE_SHFT                                     0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE6_BMSK                                       0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE6_SHFT                                            0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DDR_FREQ_LIMIT_EN_BMSK                            0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DDR_FREQ_LIMIT_EN_SHFT                                 0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VFE_DISABLE_BMSK                                  0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VFE_DISABLE_SHFT                                       0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY1_BMSK                       0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY1_SHFT                           0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY0_BMSK                       0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY0_SHFT                           0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_DISABLE_BMSK                             0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_DISABLE_SHFT                                 0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE5_BMSK                                        0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE5_SHFT                                            0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_SCM_DISABLE_BMSK                              0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDCC5_SCM_DISABLE_SHFT                                 0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CCI_FREQ_SCALE_BMSK                                 0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CCI_FREQ_SCALE_SHFT                                    0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_CLOCKCFG_BMSK                                  0x38000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_CLOCKCFG_SHFT                                      0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_BMSK                                     0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_SHFT                                        0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_UL_CA_DISABLE_BMSK                          0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_UL_CA_DISABLE_SHFT                             0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_ABOVE_CAT4_DISABLE_BMSK                     0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_ABOVE_CAT4_DISABLE_SHFT                        0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_HSUPA_DC_DISABLE_BMSK                      0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_HSUPA_DC_DISABLE_SHFT                        0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_VP9D_BMSK                               0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_VP9D_SHFT                                 0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE2_BMSK                                           0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE2_SHFT                                             0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_265E_BMSK                               0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_265E_SHFT                                 0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_4KX2KD_BMSK                              0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_4KX2KD_SHFT                               0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CSID_2_DISABLE_BMSK                                    0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_CSID_2_DISABLE_SHFT                                     0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_CFG_BMSK                                 0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_CFG_SHFT                                  0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_DELAY_BMSK                               0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_DELAY_SHFT                                0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE1_BMSK                                             0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE1_SHFT                                             0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_L2_CACHE_UPPER_BANK_DISABLE_BMSK                   0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APPS_L2_CACHE_UPPER_BANK_DISABLE_SHFT                   0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000168)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                      0xfc000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                            0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_BMSK                0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_SHFT                     0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_BMSK                0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_SHFT                     0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                     0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                         0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_LPASS_NIDEN_DISABLE_BMSK                        0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_LPASS_NIDEN_DISABLE_SHFT                            0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_LPASS_DBGEN_DISABLE_BMSK                        0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_LPASS_DBGEN_DISABLE_SHFT                            0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                       0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                           0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                         0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                            0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                          0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                             0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                           0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                              0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                           0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                              0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                         0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                            0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                          0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                             0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                           0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                              0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                           0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                              0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                         0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                           0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                             0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                               0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                         0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                          0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                              0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                               0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                               0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                               0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                        0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000016c)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                      0xfe000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                            0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                     0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                          0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                             0xfff800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                           0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                             0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                          0x3e0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                            0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                            0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                             0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000170)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK            0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                   0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000174)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_7_BMSK                      0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_7_SHFT                            0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_6_BMSK                      0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_6_SHFT                            0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_5_BMSK                      0x20000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_5_SHFT                            0x1d
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_4_BMSK                      0x10000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_4_SHFT                            0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_GCC_DISABLE_BOOT_FSM_BMSK                         0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_GCC_DISABLE_BOOT_FSM_SHFT                              0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                           0x7800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                                0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                             0x780000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                 0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_3_BMSK                         0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_3_SHFT                            0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_2_BMSK                         0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_2_SHFT                            0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_1_BMSK                         0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_1_SHFT                            0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_CSR_BMSK                              0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_CSR_SHFT                                 0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_AARCH64_ENABLE_BMSK                             0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_AARCH64_ENABLE_SHFT                                0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                             0x3000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                                0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                               0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                 0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                          0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                            0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                                0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                  0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                        0x180
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                          0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                               0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                                0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                 0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                  0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000178)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_LSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000017c)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW0_MSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000180)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_LSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000184)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW1_MSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000188)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_LSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_ADDR                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000018c)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_RSVD0_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_CM_FEAT_CONFIG_ROW2_MSB_RSVD0_SHFT                                           0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000190 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000194 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001d0)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                  0xff000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                        0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                    0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                        0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                      0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                         0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                        0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                         0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001d4)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_BMSK                                     0x80000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_SHFT                                           0x1f
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_BMSK                                  0x7f000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_SHFT                                        0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                    0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                        0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                      0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                         0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                        0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                         0x0

#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001d8)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_SPARE0_BMSK                                          0xc0000000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_SPARE0_SHFT                                                0x1e
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR6_AGING_SENSOR_0_BMSK                             0x3f000000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR6_AGING_SENSOR_0_SHFT                                   0x18
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR5_AGING_SENSOR_0_BMSK                               0xfc0000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR5_AGING_SENSOR_0_SHFT                                   0x12
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR2_AGING_SENSOR_0_BMSK                                0x3f000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR2_AGING_SENSOR_0_SHFT                                    0xc
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR1_AGING_SENSOR_0_BMSK                                  0xfc0
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR1_AGING_SENSOR_0_SHFT                                    0x6
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR0_AGING_SENSOR_0_BMSK                                   0x3f
#define HWIO_QFPROM_RAW_CALIB12_ROW0_LSB_CPR0_AGING_SENSOR_0_SHFT                                    0x0

#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001dc)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_SPARE0_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_SPARE0_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_FEC_VALUE_BMSK                                       0x7f000000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_FEC_VALUE_SHFT                                             0x18
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR_LOCAL_RC_BMSK                                      0xe00000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR_LOCAL_RC_SHFT                                          0x15
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_NOM_TARGET_BMSK                                   0x1f0000
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_NOM_TARGET_SHFT                                       0x10
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_TURBO_TARGET_BMSK                                   0xf800
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_TURBO_TARGET_SHFT                                      0xb
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR1_SVS2_TARGET_BMSK                                     0x7c0
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR1_SVS2_TARGET_SHFT                                       0x6
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_SVS_TARGET_BMSK                                       0x3e
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_CPR3_SVS_TARGET_SHFT                                        0x1
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_SPARE1_BMSK                                                 0x1
#define HWIO_QFPROM_RAW_CALIB12_ROW0_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001e0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_MAXn                                                          3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001e4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_MAXn                                                          3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_BMSK                                       0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_SHFT                                             0x18
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                        0xffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                             0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000200)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000204)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_BMSK                                       0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_SHFT                                             0x18
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_UNUSED_BMSK                                            0xffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_UNUSED_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000208)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_1_8_0_BMSK                        0xff800000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_1_8_0_SHFT                              0x17
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_0_BMSK                              0x7fe000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_0_SHFT                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CSI_PHY_BMSK                                               0x1f00
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_CSI_PHY_SHFT                                                  0x8
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_DSI_PHY_BMSK                                                 0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_DSI_PHY_SHFT                                                  0x4
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_DSIPHY_PLL_BMSK                                               0xf
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_DSIPHY_PLL_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000020c)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_2_0_BMSK                0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_2_0_SHFT                    0x15
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_3_BMSK                              0x1ff800
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_3_SHFT                                   0xb
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_2_BMSK                                 0x7fe
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_2_SHFT                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_1_9_BMSK                                 0x1
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_1_9_SHFT                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000210)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_6_0_BMSK              0xfe000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_6_0_SHFT                    0x19
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_1_INTERCEPT_BMSK                   0x1ff8000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_1_INTERCEPT_SHFT                         0xf
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_9_4_BMSK                  0x7e00
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_9_4_SHFT                     0x9
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IDDQ_A53_ACTIVE_MEAS_BMSK                                   0x1fe
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IDDQ_A53_ACTIVE_MEAS_SHFT                                     0x1
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_3_BMSK                       0x1
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_3_SHFT                       0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000214)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE1_BMSK                                              0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE1_SHFT                                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APC1_LDO_VREF_TRIM_BMSK                                  0x3e0000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APC1_LDO_VREF_TRIM_SHFT                                      0x11
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_AMP_COMP_BMSK                                             0x1e000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_AMP_COMP_SHFT                                                 0xd
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_3_INTERCEPT_BMSK                      0x1ff8
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_3_INTERCEPT_SHFT                         0x3
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_9_7_BMSK                     0x7
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_9_7_SHFT                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000218)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS1_POINT2_BMSK                                     0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS1_POINT2_SHFT                                           0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS1_POINT1_BMSK                                      0x3f00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS1_POINT1_SHFT                                           0x14
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS0_POINT2_BMSK                                        0xfc000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS0_POINT2_SHFT                                            0xe
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS0_POINT1_BMSK                                         0x3f00
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS0_POINT1_SHFT                                            0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS_BASE0_BMSK                                             0xff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_TSENS_BASE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000021c)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS3_POINT2_BMSK                                       0xfc0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS3_POINT2_SHFT                                           0x12
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS3_POINT1_BMSK                                        0x3f000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS3_POINT1_SHFT                                            0xc
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS2_POINT2_BMSK                                          0xfc0
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS2_POINT2_SHFT                                            0x6
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS2_POINT1_BMSK                                           0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_TSENS2_POINT1_SHFT                                            0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000220)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS5_POINT2_BMSK                                     0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS5_POINT2_SHFT                                           0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS5_POINT1_BMSK                                      0x3f00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS5_POINT1_SHFT                                           0x14
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS4_POINT2_BMSK                                        0xfc000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS4_POINT2_SHFT                                            0xe
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS4_POINT1_BMSK                                         0x3f00
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS4_POINT1_SHFT                                            0x8
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS_BASE1_BMSK                                             0xff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TSENS_BASE1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000224)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS7_POINT2_BMSK                                       0xfc0000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS7_POINT2_SHFT                                           0x12
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS7_POINT1_BMSK                                        0x3f000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS7_POINT1_SHFT                                            0xc
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS6_POINT2_BMSK                                          0xfc0
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS6_POINT2_SHFT                                            0x6
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS6_POINT1_BMSK                                           0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TSENS6_POINT1_SHFT                                            0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000228)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS10_POINT1_4_0_BMSK                                0xf8000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS10_POINT1_4_0_SHFT                                      0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS9_POINT2_BMSK                                      0x7e00000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS9_POINT2_SHFT                                           0x15
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS9_POINT1_BMSK                                       0x1f8000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS9_POINT1_SHFT                                            0xf
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS8_POINT2_BMSK                                         0x7e00
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS8_POINT2_SHFT                                            0x9
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS8_POINT1_BMSK                                          0x1f8
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS8_POINT1_SHFT                                            0x3
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS_CALIB_BMSK                                              0x7
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_TSENS_CALIB_SHFT                                              0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000022c)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_SPARE_6_0_BMSK                                    0xfe0000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_SPARE_6_0_SHFT                                        0x11
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_RANGE_CORR_BMSK                                    0x10000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_RANGE_CORR_SHFT                                       0x10
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_AVEG_CORR_BMSK                                      0x8000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_AVEG_CORR_SHFT                                         0xf
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_RPOLY_CAL_BMSK                                      0x7f80
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TXDAC0_RPOLY_CAL_SHFT                                         0x7
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TSENS10_POINT2_BMSK                                          0x7e
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TSENS10_POINT2_SHFT                                           0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TSENS10_POINT1_5_BMSK                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_TSENS10_POINT1_5_SHFT                                         0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000230)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M3_1_0_BMSK                                       0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M3_1_0_SHFT                                             0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M2_BMSK                                           0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M2_SHFT                                                 0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M1_BMSK                                            0x7000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M1_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M0_BMSK                                             0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B0M0_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B0_BMSK                                                0x1c0000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B0_SHFT                                                    0x12
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_B_BMSK                                                0x30000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_B_SHFT                                                   0x10
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CAP_B_BMSK                                                 0xc000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CAP_B_SHFT                                                    0xe
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_B_BMSK                                                 0x3000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_B_SHFT                                                    0xc
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_MODEM_TXDAC_FUSEFLAG_BMSK                                   0x800
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_MODEM_TXDAC_FUSEFLAG_SHFT                                     0xb
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_RANGE_CORR_BMSK                                      0x400
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_RANGE_CORR_SHFT                                        0xa
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_AVEG_CORR_BMSK                                       0x200
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_AVEG_CORR_SHFT                                         0x9
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_RPOLY_CAL_BMSK                                       0x1fe
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC1_RPOLY_CAL_SHFT                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC0_SPARE_7_BMSK                                           0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_TXDAC0_SPARE_7_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000234)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B2M0_0_BMSK                                           0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B2M0_0_SHFT                                               0x17
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B2_BMSK                                                0x700000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B2_SHFT                                                    0x14
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_B1_BMSK                                              0xc0000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_B1_SHFT                                                 0x12
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M3_BMSK                                              0x38000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M3_SHFT                                                  0xf
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M2_BMSK                                               0x7000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M2_SHFT                                                  0xc
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M1_BMSK                                                0xe00
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M1_SHFT                                                  0x9
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M0_BMSK                                                0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B1M0_SHFT                                                  0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B1_BMSK                                                    0x38
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B1_SHFT                                                     0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_B0_BMSK                                                  0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_B0_SHFT                                                  0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B0M3_2_BMSK                                                0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B0M3_2_SHFT                                                0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000238)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_GNSS_ADC_CALIB_1_0_BMSK                                0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_GNSS_ADC_CALIB_1_0_SHFT                                      0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_B3_BMSK                                           0x30000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_B3_SHFT                                                 0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M3_BMSK                                            0xe000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M3_SHFT                                                 0x19
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M2_BMSK                                            0x1c00000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M2_SHFT                                                 0x16
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M1_BMSK                                             0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M1_SHFT                                                 0x13
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M0_BMSK                                              0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B3M0_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B3_BMSK                                                  0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B3_SHFT                                                     0xd
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_B2_BMSK                                               0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_B2_SHFT                                                  0xb
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M3_BMSK                                                0x700
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M3_SHFT                                                  0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M2_BMSK                                                 0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M2_SHFT                                                  0x5
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M1_BMSK                                                 0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M1_SHFT                                                  0x2
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M0_2_1_BMSK                                              0x3
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B2M0_2_1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000023c)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SPARE1_BMSK                                              0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SPARE1_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_VOLTAGE_SENSOR_CALIB_BMSK                                 0xfffc0
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_VOLTAGE_SENSOR_CALIB_SHFT                                     0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_GNSS_ADC_CALIB_7_2_BMSK                                      0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_GNSS_ADC_CALIB_7_2_SHFT                                       0x0

#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000240)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW7_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW7_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_SPARE0_BMSK                                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_SPARE0_SHFT                                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_SVS_TARGET_BMSK                                   0x3e000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_SVS_TARGET_SHFT                                         0x19
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_NOMINAL_TARGET_BMSK                                0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_NOMINAL_TARGET_SHFT                                     0x14
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_TURBO_TARGET_BMSK                                    0xf8000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR1_TURBO_TARGET_SHFT                                        0xf
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_SVS_TARGET_BMSK                                       0x7c00
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_SVS_TARGET_SHFT                                          0xa
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_NOMINAL_TARGET_BMSK                                    0x3e0
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_NOMINAL_TARGET_SHFT                                      0x5
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_TURBO_TARGET_BMSK                                       0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR0_TURBO_TARGET_SHFT                                        0x0

#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000244)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW7_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE2_BMSK                                              0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE2_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_SVS2_TARGET_BMSK                                    0x7c0000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_SVS2_TARGET_SHFT                                        0x12
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_SVS_TARGET_BMSK                                      0x3e000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_SVS_TARGET_SHFT                                          0xd
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_NOMINAL_TARGET_BMSK                                   0x1f00
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_NOMINAL_TARGET_SHFT                                      0x8
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_TURBO_TARGET_BMSK                                       0xf8
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_CPR2_TURBO_TARGET_SHFT                                        0x3
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE1_BMSK                                                   0x7
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000248)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW8_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW8_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_SVS_TARGET_BMSK                                   0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_SVS_TARGET_SHFT                                         0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_NOMINAL_TARGET_BMSK                                0x3f00000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_NOMINAL_TARGET_SHFT                                     0x14
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_TURBO_TARGET_BMSK                                    0xfc000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_TURBO_TARGET_SHFT                                        0xe
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_SVS_ROSEL_BMSK                                        0x3800
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_SVS_ROSEL_SHFT                                           0xb
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_NOM_ROSEL_BMSK                                         0x700
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_NOM_ROSEL_SHFT                                           0x8
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_TUR_ROSEL_BMSK                                          0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR5_TUR_ROSEL_SHFT                                           0x5
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR3_SVS2_TARGET_BMSK                                        0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CPR3_SVS2_TARGET_SHFT                                         0x0

#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000024c)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW8_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_SVS_QUOT_VMIN_BMSK                                  0xff0000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_SVS_QUOT_VMIN_SHFT                                      0x10
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_NOMINAL_QUOT_VMIN_BMSK                                0xff00
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_NOMINAL_QUOT_VMIN_SHFT                                   0x8
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_TURBO_QUOT_VMIN_BMSK                                    0xff
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CPR5_TURBO_QUOT_VMIN_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000250)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW9_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW9_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR6_NOM_ROSEL_1_0_BMSK                                0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR6_NOM_ROSEL_1_0_SHFT                                      0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR6_TUR_ROSEL_BMSK                                    0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR6_TUR_ROSEL_SHFT                                          0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_SVS_OFFSET_BMSK                                    0x7e00000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_SVS_OFFSET_SHFT                                         0x15
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_NOMINAL_OFFSET_BMSK                                 0x1f8000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_NOMINAL_OFFSET_SHFT                                      0xf
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_TURBO_OFFSET_BMSK                                     0x7f00
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_TURBO_OFFSET_SHFT                                        0x8
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_SVS2_QUOT_VMIN_BMSK                                     0xff
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CPR5_SVS2_QUOT_VMIN_SHFT                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000254)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW9_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SPARE1_BMSK                                              0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SPARE1_SHFT                                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_SVS_TARGET_BMSK                                     0x3f0000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_SVS_TARGET_SHFT                                         0x10
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_NOMINAL_TARGET_BMSK                                   0xfc00
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_NOMINAL_TARGET_SHFT                                      0xa
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_TURBO_TARGET_BMSK                                      0x3f0
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_TURBO_TARGET_SHFT                                        0x4
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_SVS_ROSEL_BMSK                                           0xe
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_SVS_ROSEL_SHFT                                           0x1
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_NOM_ROSEL_2_BMSK                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CPR6_NOM_ROSEL_2_SHFT                                         0x0

#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000258)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW10_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW10_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_NOMINAL_OFFSET_0_BMSK                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_NOMINAL_OFFSET_0_SHFT                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_TURBO_OFFSET_BMSK                                0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_TURBO_OFFSET_SHFT                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_SVS_QUOT_VMIN_BMSK                                 0xff0000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_SVS_QUOT_VMIN_SHFT                                     0x10
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_NOMINAL_QUOT_VMIN_BMSK                               0xff00
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_NOMINAL_QUOT_VMIN_SHFT                                  0x8
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_TURBO_QUOT_VMIN_BMSK                                   0xff
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CPR6_TURBO_QUOT_VMIN_SHFT                                    0x0

#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000025c)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW10_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW10_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE1_BMSK                                             0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE1_SHFT                                                 0x17
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TXDAC1_SPARE_BMSK                                       0x7f8000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TXDAC1_SPARE_SHFT                                            0xf
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR_GLOBAL_RC_BMSK                                        0x7000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR_GLOBAL_RC_SHFT                                           0xc
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE2_BMSK                                                0x800
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_SPARE2_SHFT                                                  0xb
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR6_SVS_OFFSET_BMSK                                       0x7e0
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR6_SVS_OFFSET_SHFT                                         0x5
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR6_NOMINAL_OFFSET_5_1_BMSK                                0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_CPR6_NOMINAL_OFFSET_5_1_SHFT                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000260)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW11_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_1_0_BMSK                0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_1_0_SHFT                      0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P3_ACTIVE_MEAS_FUSE_BITS_BMSK            0x3f000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P3_ACTIVE_MEAS_FUSE_BITS_SHFT                  0x18
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P2_ACTIVE_MEAS_FUSE_BITS_BMSK              0xfc0000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P2_ACTIVE_MEAS_FUSE_BITS_SHFT                  0x12
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P1_ACTIVE_MEAS_FUSE_BITS_BMSK               0x3f000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_P1_ACTIVE_MEAS_FUSE_BITS_SHFT                   0xc
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_ACTIVE_MEAS_FUSE_BITS_BMSK                    0xfc0
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_MAX_ACTIVE_MEAS_FUSE_BITS_SHFT                      0x6
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_L2_ACTIVE_MEAS_FUSE_BITS_BMSK                      0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IDDQ_A72_L2_ACTIVE_MEAS_FUSE_BITS_SHFT                       0x0

#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000264)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW11_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_SPARE1_BMSK                                             0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_SPARE1_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_REV_CTRL_BMSK                                      0x180000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_REV_CTRL_SHFT                                          0x13
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_A72_WORST_CORE_NUM_BMSK                                  0x60000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_A72_WORST_CORE_NUM_SHFT                                     0x11
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_MULTIPLICATION_FACTOR_BMSK                               0x18000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_MULTIPLICATION_FACTOR_SHFT                                   0xf
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_MX_ACTIVE_MEAS_FUSE_BITS_BMSK                        0x7e00
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_MX_ACTIVE_MEAS_FUSE_BITS_SHFT                           0x9
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_GFX_ACTIVE_SUM_MEAS_FUSE_BITS_BMSK                    0x1f8
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_GFX_ACTIVE_SUM_MEAS_FUSE_BITS_SHFT                      0x3
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_4_2_BMSK                       0x7
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_4_2_SHFT                       0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000268 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_MAXn                                                      16
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000026c + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_MAXn                                                      16
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                     0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002f0)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002f4)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_BMSK                                   0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_SHFT                                         0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002f8)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002fc)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_BMSK                                   0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_SHFT                                         0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000300 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_MAXn                                                       31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000304 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_MAXn                                                       31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_BMSK                                        0x80000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_SHFT                                              0x1f
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_BMSK                                     0x7f000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_SHFT                                           0x18
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                          0xfe0000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                              0x11
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                       0x1fffe
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                           0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                             0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                             0x0

#define HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000400)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_LSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RSVD0_BMSK                                                 0xf8000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RSVD0_SHFT                                                       0x1b
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                        0x4000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                             0x1a
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                        0x2000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                             0x19
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                        0x1000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                             0x18
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                         0x800000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                             0x17
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                         0x400000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                             0x16
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                         0x200000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                             0x15
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                         0x100000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                             0x14
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                          0x80000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                             0x13
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                          0x40000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                             0x12
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                          0x20000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                             0x11
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                          0x10000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                             0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                           0x8000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                              0xf
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                           0x4000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                              0xe
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                           0x2000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                              0xd
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                           0x1000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                              0xc
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                            0x800
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                              0xb
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                            0x400
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                              0xa
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                             0x200
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                               0x9
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                             0x100
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                               0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                              0x80
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                               0x7
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                              0x40
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                               0x6
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                              0x20
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                               0x5
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                              0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                               0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                               0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                               0x3
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                               0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                               0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                               0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                               0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                               0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                               0x0

#define HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000404)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_MSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                     0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                            0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000408)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_BMSK                                           0xffffff00
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_SHFT                                                  0x8
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SAFE_SWITCH_BMSK                                            0xff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SAFE_SWITCH_SHFT                                             0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000040c)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000410)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000414)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000418)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000041c)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000420)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000424)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000428)
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG22_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG22_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG22_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG22_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG22_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG22_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG22_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000042c)
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG22_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG22_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG22_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG22_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG22_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG22_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG22_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000430)
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG23_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG23_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG23_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG23_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG23_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG23_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG23_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000434)
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG23_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG23_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG23_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG23_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG23_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG23_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG23_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000438)
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG24_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG24_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG24_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG24_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG24_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG24_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG24_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000043c)
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG24_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG24_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG24_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG24_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG24_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG24_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG24_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000440)
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG25_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG25_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG25_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG25_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG25_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG25_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG25_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000444)
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG25_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG25_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG25_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG25_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG25_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG25_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG25_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000448)
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG26_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG26_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG26_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG26_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG26_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG26_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG26_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000044c)
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG26_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG26_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG26_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG26_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG26_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG26_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG26_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_MAXn                                                             39
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                         0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                0x0

#define HWIO_ACC_IR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002000)
#define HWIO_ACC_IR_RMSK                                                                            0x1f
#define HWIO_ACC_IR_OUT(v)      \
        out_dword(HWIO_ACC_IR_ADDR,v)
#define HWIO_ACC_IR_INSTRUCTION_BMSK                                                                0x1f
#define HWIO_ACC_IR_INSTRUCTION_SHFT                                                                 0x0

#define HWIO_ACC_DR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002004)
#define HWIO_ACC_DR_RMSK                                                                      0xffffffff
#define HWIO_ACC_DR_IN          \
        in_dword_masked(HWIO_ACC_DR_ADDR, HWIO_ACC_DR_RMSK)
#define HWIO_ACC_DR_INM(m)      \
        in_dword_masked(HWIO_ACC_DR_ADDR, m)
#define HWIO_ACC_DR_OUT(v)      \
        out_dword(HWIO_ACC_DR_ADDR,v)
#define HWIO_ACC_DR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_ACC_DR_ADDR,m,v,HWIO_ACC_DR_IN)
#define HWIO_ACC_DR_DR_BMSK                                                                   0xffffffff
#define HWIO_ACC_DR_DR_SHFT                                                                          0x0

#define HWIO_ACC_VERID_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002008)
#define HWIO_ACC_VERID_RMSK                                                                       0xffff
#define HWIO_ACC_VERID_IN          \
        in_dword_masked(HWIO_ACC_VERID_ADDR, HWIO_ACC_VERID_RMSK)
#define HWIO_ACC_VERID_INM(m)      \
        in_dword_masked(HWIO_ACC_VERID_ADDR, m)
#define HWIO_ACC_VERID_FWVERID_BMSK                                                               0xff00
#define HWIO_ACC_VERID_FWVERID_SHFT                                                                  0x8
#define HWIO_ACC_VERID_HWVERID_BMSK                                                                 0xff
#define HWIO_ACC_VERID_HWVERID_SHFT                                                                  0x0

#define HWIO_ACC_FEATSETn_ADDR(n)                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002010 + 0x4 * (n))
#define HWIO_ACC_FEATSETn_RMSK                                                                0xffffffff
#define HWIO_ACC_FEATSETn_MAXn                                                                         7
#define HWIO_ACC_FEATSETn_INI(n)        \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), HWIO_ACC_FEATSETn_RMSK)
#define HWIO_ACC_FEATSETn_INMI(n,mask)    \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), mask)
#define HWIO_ACC_FEATSETn_FEAT_BMSK                                                           0xffffffff
#define HWIO_ACC_FEATSETn_FEAT_SHFT                                                                  0x0

#define HWIO_ACC_STATE_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002038)
#define HWIO_ACC_STATE_RMSK                                                                          0x7
#define HWIO_ACC_STATE_IN          \
        in_dword_masked(HWIO_ACC_STATE_ADDR, HWIO_ACC_STATE_RMSK)
#define HWIO_ACC_STATE_INM(m)      \
        in_dword_masked(HWIO_ACC_STATE_ADDR, m)
#define HWIO_ACC_STATE_ACC_READY_BMSK                                                                0x4
#define HWIO_ACC_STATE_ACC_READY_SHFT                                                                0x2
#define HWIO_ACC_STATE_ACC_LOCKED_BMSK                                                               0x2
#define HWIO_ACC_STATE_ACC_LOCKED_SHFT                                                               0x1
#define HWIO_ACC_STATE_ACC_STOP_BMSK                                                                 0x1
#define HWIO_ACC_STATE_ACC_STOP_SHFT                                                                 0x0

#define HWIO_QFPROM_BLOW_TIMER_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_RMSK                                                                0xfff
#define HWIO_QFPROM_BLOW_TIMER_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, HWIO_QFPROM_BLOW_TIMER_RMSK)
#define HWIO_QFPROM_BLOW_TIMER_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, m)
#define HWIO_QFPROM_BLOW_TIMER_OUT(v)      \
        out_dword(HWIO_QFPROM_BLOW_TIMER_ADDR,v)
#define HWIO_QFPROM_BLOW_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BLOW_TIMER_ADDR,m,v,HWIO_QFPROM_BLOW_TIMER_IN)
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_BMSK                                                     0xfff
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_SHFT                                                       0x0

#define HWIO_QFPROM_TEST_CTRL_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_RMSK                                                                   0xf
#define HWIO_QFPROM_TEST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, HWIO_QFPROM_TEST_CTRL_RMSK)
#define HWIO_QFPROM_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, m)
#define HWIO_QFPROM_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_TEST_CTRL_ADDR,v)
#define HWIO_QFPROM_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_TEST_CTRL_ADDR,m,v,HWIO_QFPROM_TEST_CTRL_IN)
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_BMSK                                                       0x8
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_SHFT                                                       0x3
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_BMSK                                                        0x4
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_SHFT                                                        0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_BMSK                                                        0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_SHFT                                                        0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_BMSK                                                  0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_SHFT                                                  0x0

#define HWIO_QFPROM_ACCEL_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002044)
#define HWIO_QFPROM_ACCEL_RMSK                                                                     0xfff
#define HWIO_QFPROM_ACCEL_IN          \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, HWIO_QFPROM_ACCEL_RMSK)
#define HWIO_QFPROM_ACCEL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, m)
#define HWIO_QFPROM_ACCEL_OUT(v)      \
        out_dword(HWIO_QFPROM_ACCEL_ADDR,v)
#define HWIO_QFPROM_ACCEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_ACCEL_ADDR,m,v,HWIO_QFPROM_ACCEL_IN)
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_BMSK                                                     0x800
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_SHFT                                                       0xb
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_BMSK                                                   0x700
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_SHFT                                                     0x8
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_BMSK                                                         0xff
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_SHFT                                                          0x0

#define HWIO_QFPROM_BLOW_STATUS_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_RMSK                                                                 0x3
#define HWIO_QFPROM_BLOW_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, HWIO_QFPROM_BLOW_STATUS_RMSK)
#define HWIO_QFPROM_BLOW_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, m)
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_BMSK                                                   0x2
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_SHFT                                                   0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_BMSK                                                     0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_SHFT                                                     0x0

#define HWIO_QFPROM_ROM_ERROR_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_RMSK                                                                   0x1
#define HWIO_QFPROM_ROM_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, HWIO_QFPROM_ROM_ERROR_RMSK)
#define HWIO_QFPROM_ROM_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, m)
#define HWIO_QFPROM_ROM_ERROR_ERROR_BMSK                                                             0x1
#define HWIO_QFPROM_ROM_ERROR_ERROR_SHFT                                                             0x0

#define HWIO_QFPROM_BIST_CTRL_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002050)
#define HWIO_QFPROM_BIST_CTRL_RMSK                                                                  0x7f
#define HWIO_QFPROM_BIST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, HWIO_QFPROM_BIST_CTRL_RMSK)
#define HWIO_QFPROM_BIST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, m)
#define HWIO_QFPROM_BIST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_BIST_CTRL_ADDR,v)
#define HWIO_QFPROM_BIST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BIST_CTRL_ADDR,m,v,HWIO_QFPROM_BIST_CTRL_IN)
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_BMSK                                                      0x7c
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_SHFT                                                       0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_BMSK                                                        0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_SHFT                                                        0x1
#define HWIO_QFPROM_BIST_CTRL_START_BMSK                                                             0x1
#define HWIO_QFPROM_BIST_CTRL_START_SHFT                                                             0x0

#define HWIO_QFPROM_BIST_ERROR_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002054)
#define HWIO_QFPROM_BIST_ERROR_RMSK                                                           0xffffffff
#define HWIO_QFPROM_BIST_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, HWIO_QFPROM_BIST_ERROR_RMSK)
#define HWIO_QFPROM_BIST_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, m)
#define HWIO_QFPROM_BIST_ERROR_ERROR_BMSK                                                     0xffffffff
#define HWIO_QFPROM_BIST_ERROR_ERROR_SHFT                                                            0x0

#define HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002060 + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_RMSK                                                      0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_MAXn                                                               7
#define HWIO_QFPROM_HASH_SIGNATUREn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), HWIO_QFPROM_HASH_SIGNATUREn_RMSK)
#define HWIO_QFPROM_HASH_SIGNATUREn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), mask)
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_BMSK                                           0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_SHFT                                                  0x0

#define HWIO_HW_KEY_STATUS_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002080)
#define HWIO_HW_KEY_STATUS_RMSK                                                                     0x7f
#define HWIO_HW_KEY_STATUS_IN          \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, HWIO_HW_KEY_STATUS_RMSK)
#define HWIO_HW_KEY_STATUS_INM(m)      \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, m)
#define HWIO_HW_KEY_STATUS_FUSE_SENSE_DONE_BMSK                                                     0x40
#define HWIO_HW_KEY_STATUS_FUSE_SENSE_DONE_SHFT                                                      0x6
#define HWIO_HW_KEY_STATUS_CRI_CM_BOOT_DONE_BMSK                                                    0x20
#define HWIO_HW_KEY_STATUS_CRI_CM_BOOT_DONE_SHFT                                                     0x5
#define HWIO_HW_KEY_STATUS_KDF_DONE_BMSK                                                            0x10
#define HWIO_HW_KEY_STATUS_KDF_DONE_SHFT                                                             0x4
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_BMSK                                                     0x8
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_SHFT                                                     0x3
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_BMSK                                                    0x4
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_SHFT                                                    0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_BMSK                                         0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_SHFT                                         0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_BMSK                                         0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_SHFT                                         0x0

#define HWIO_RESET_JDR_STATUS_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002084)
#define HWIO_RESET_JDR_STATUS_RMSK                                                                   0x3
#define HWIO_RESET_JDR_STATUS_IN          \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, HWIO_RESET_JDR_STATUS_RMSK)
#define HWIO_RESET_JDR_STATUS_INM(m)      \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, m)
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_BMSK                                                       0x2
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_SHFT                                                       0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_BMSK                                              0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_SHFT                                              0x0

#define HWIO_FEC_ESR_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002090)
#define HWIO_FEC_ESR_RMSK                                                                         0x3fff
#define HWIO_FEC_ESR_IN          \
        in_dword_masked(HWIO_FEC_ESR_ADDR, HWIO_FEC_ESR_RMSK)
#define HWIO_FEC_ESR_INM(m)      \
        in_dword_masked(HWIO_FEC_ESR_ADDR, m)
#define HWIO_FEC_ESR_OUT(v)      \
        out_dword(HWIO_FEC_ESR_ADDR,v)
#define HWIO_FEC_ESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_FEC_ESR_ADDR,m,v,HWIO_FEC_ESR_IN)
#define HWIO_FEC_ESR_CORR_SW_ACC_BMSK                                                             0x2000
#define HWIO_FEC_ESR_CORR_SW_ACC_SHFT                                                                0xd
#define HWIO_FEC_ESR_CORR_SECURE_CHANNEL_BMSK                                                      0x800
#define HWIO_FEC_ESR_CORR_SECURE_CHANNEL_SHFT                                                        0xb
#define HWIO_FEC_ESR_CORR_BOOT_ROM_BMSK                                                            0x400
#define HWIO_FEC_ESR_CORR_BOOT_ROM_SHFT                                                              0xa
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_BMSK                                                          0x200
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_SHFT                                                            0x9
#define HWIO_FEC_ESR_CORR_MULT_BMSK                                                                0x100
#define HWIO_FEC_ESR_CORR_MULT_SHFT                                                                  0x8
#define HWIO_FEC_ESR_CORR_SEEN_BMSK                                                                 0x80
#define HWIO_FEC_ESR_CORR_SEEN_SHFT                                                                  0x7
#define HWIO_FEC_ESR_ERR_SW_ACC_BMSK                                                                0x40
#define HWIO_FEC_ESR_ERR_SW_ACC_SHFT                                                                 0x6
#define HWIO_FEC_ESR_ERR_SECURE_CHANNEL_BMSK                                                        0x10
#define HWIO_FEC_ESR_ERR_SECURE_CHANNEL_SHFT                                                         0x4
#define HWIO_FEC_ESR_ERR_BOOT_ROM_BMSK                                                               0x8
#define HWIO_FEC_ESR_ERR_BOOT_ROM_SHFT                                                               0x3
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_BMSK                                                             0x4
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_SHFT                                                             0x2
#define HWIO_FEC_ESR_ERR_MULT_BMSK                                                                   0x2
#define HWIO_FEC_ESR_ERR_MULT_SHFT                                                                   0x1
#define HWIO_FEC_ESR_ERR_SEEN_BMSK                                                                   0x1
#define HWIO_FEC_ESR_ERR_SEEN_SHFT                                                                   0x0

#define HWIO_FEC_EAR_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002094)
#define HWIO_FEC_EAR_RMSK                                                                     0xffffffff
#define HWIO_FEC_EAR_IN          \
        in_dword_masked(HWIO_FEC_EAR_ADDR, HWIO_FEC_EAR_RMSK)
#define HWIO_FEC_EAR_INM(m)      \
        in_dword_masked(HWIO_FEC_EAR_ADDR, m)
#define HWIO_FEC_EAR_CORR_ADDR_BMSK                                                           0xffff0000
#define HWIO_FEC_EAR_CORR_ADDR_SHFT                                                                 0x10
#define HWIO_FEC_EAR_ERR_ADDR_BMSK                                                                0xffff
#define HWIO_FEC_EAR_ERR_ADDR_SHFT                                                                   0x0

#define HWIO_QFPROM0_MATCH_STATUS_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002098)
#define HWIO_QFPROM0_MATCH_STATUS_RMSK                                                        0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, HWIO_QFPROM0_MATCH_STATUS_RMSK)
#define HWIO_QFPROM0_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_BMSK                                                   0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_SHFT                                                          0x0

#define HWIO_QFPROM1_MATCH_STATUS_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000209c)
#define HWIO_QFPROM1_MATCH_STATUS_RMSK                                                        0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, HWIO_QFPROM1_MATCH_STATUS_RMSK)
#define HWIO_QFPROM1_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_BMSK                                                   0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_SHFT                                                          0x0

#define HWIO_FEAT_PROV_OUTn_ADDR(n)                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000020a0 + 0x4 * (n))
#define HWIO_FEAT_PROV_OUTn_RMSK                                                              0xffffffff
#define HWIO_FEAT_PROV_OUTn_MAXn                                                                       3
#define HWIO_FEAT_PROV_OUTn_INI(n)        \
        in_dword_masked(HWIO_FEAT_PROV_OUTn_ADDR(n), HWIO_FEAT_PROV_OUTn_RMSK)
#define HWIO_FEAT_PROV_OUTn_INMI(n,mask)    \
        in_dword_masked(HWIO_FEAT_PROV_OUTn_ADDR(n), mask)
#define HWIO_FEAT_PROV_OUTn_FEAT_PROV_OUT_VALUE_BMSK                                          0xffffffff
#define HWIO_FEAT_PROV_OUTn_FEAT_PROV_OUT_VALUE_SHFT                                                 0x0

#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000020b0 + 0x4 * (n))
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_RMSK                                                0xffffffff
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_MAXn                                                         3
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_INI(n)        \
        in_dword_masked(HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n), HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_RMSK)
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_INMI(n,mask)    \
        in_dword_masked(HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n), mask)
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_SEC_CTRL_MISC_CONFIG_STATUS_VALUE_BMSK              0xffffffff
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_SEC_CTRL_MISC_CONFIG_STATUS_VALUE_SHFT                     0x0

#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004000 + 0x4 * (n))
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_MAXn                                                         71
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_SHFT                                         0x0

#define HWIO_QFPROM_CORR_JTAG_ID_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004120)
#define HWIO_QFPROM_CORR_JTAG_ID_RMSK                                                         0xffffffff
#define HWIO_QFPROM_CORR_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, HWIO_QFPROM_CORR_JTAG_ID_RMSK)
#define HWIO_QFPROM_CORR_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_CORR_JTAG_ID_SPARE0_BMSK                                                  0xe0000000
#define HWIO_QFPROM_CORR_JTAG_ID_SPARE0_SHFT                                                        0x1d
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_BMSK                                            0x10000000
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_SHFT                                                  0x1c
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_BMSK                                               0xff00000
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_SHFT                                                    0x14
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_BMSK                                                    0xfffff
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_SHFT                                                        0x0

#define HWIO_QFPROM_CORR_PTE1_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004124)
#define HWIO_QFPROM_CORR_PTE1_RMSK                                                            0xffffffff
#define HWIO_QFPROM_CORR_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, HWIO_QFPROM_CORR_PTE1_RMSK)
#define HWIO_QFPROM_CORR_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, m)
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_BMSK                             0xfc000000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_SHFT                                   0x1a
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_BMSK                                             0x2000000
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_SHFT                                                  0x19
#define HWIO_QFPROM_CORR_PTE1_SPARE2_BMSK                                                      0x1800000
#define HWIO_QFPROM_CORR_PTE1_SPARE2_SHFT                                                           0x17
#define HWIO_QFPROM_CORR_PTE1_BONE_PILE_BMSK                                                    0x400000
#define HWIO_QFPROM_CORR_PTE1_BONE_PILE_SHFT                                                        0x16
#define HWIO_QFPROM_CORR_PTE1_A53_ACC_SETTINGS_ID_BMSK                                          0x300000
#define HWIO_QFPROM_CORR_PTE1_A53_ACC_SETTINGS_ID_SHFT                                              0x14
#define HWIO_QFPROM_CORR_PTE1_SPARE1_BMSK                                                        0xfe000
#define HWIO_QFPROM_CORR_PTE1_SPARE1_SHFT                                                            0xd
#define HWIO_QFPROM_CORR_PTE1_WAFER_BIN_BMSK                                                      0x1c00
#define HWIO_QFPROM_CORR_PTE1_WAFER_BIN_SHFT                                                         0xa
#define HWIO_QFPROM_CORR_PTE1_METAL_REV_BMSK                                                       0x300
#define HWIO_QFPROM_CORR_PTE1_METAL_REV_SHFT                                                         0x8
#define HWIO_QFPROM_CORR_PTE1_SPARE0_BMSK                                                           0xff
#define HWIO_QFPROM_CORR_PTE1_SPARE0_SHFT                                                            0x0

#define HWIO_QFPROM_CORR_SERIAL_NUM_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004128)
#define HWIO_QFPROM_CORR_SERIAL_NUM_RMSK                                                      0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, HWIO_QFPROM_CORR_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_CORR_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_PTE2_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000412c)
#define HWIO_QFPROM_CORR_PTE2_RMSK                                                            0xffffffff
#define HWIO_QFPROM_CORR_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, HWIO_QFPROM_CORR_PTE2_RMSK)
#define HWIO_QFPROM_CORR_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, m)
#define HWIO_QFPROM_CORR_PTE2_WAFER_ID_BMSK                                                   0xf8000000
#define HWIO_QFPROM_CORR_PTE2_WAFER_ID_SHFT                                                         0x1b
#define HWIO_QFPROM_CORR_PTE2_DIE_X_BMSK                                                       0x7f80000
#define HWIO_QFPROM_CORR_PTE2_DIE_X_SHFT                                                            0x13
#define HWIO_QFPROM_CORR_PTE2_DIE_Y_BMSK                                                         0x7f800
#define HWIO_QFPROM_CORR_PTE2_DIE_Y_SHFT                                                             0xb
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_BMSK                                                      0x700
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_SHFT                                                        0x8
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_BMSK                                                  0xe0
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_SHFT                                                   0x5
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_BMSK                                                        0x1c
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_SHFT                                                         0x2
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_BMSK                                                        0x3
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_SHFT                                                        0x0

#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004130)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RSVD0_BMSK                                            0xf8000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RSVD0_SHFT                                                  0x1b
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE26_BMSK                                           0x4000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE26_SHFT                                                0x1a
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE25_BMSK                                           0x2000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE25_SHFT                                                0x19
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE24_BMSK                                           0x1000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE24_SHFT                                                0x18
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE23_BMSK                                            0x800000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE23_SHFT                                                0x17
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE22_BMSK                                            0x400000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE22_SHFT                                                0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_BMSK                                            0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_SHFT                                                0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_BMSK                                            0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_SHFT                                                0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_BMSK                                             0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_SHFT                                                0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_BMSK                                             0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_SHFT                                                0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                      0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                           0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                              0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_BMSK                                                0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_SHFT                                                   0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_BMSK                                              0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_SHFT                                                 0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB12_BMSK                                              0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB12_SHFT                                                 0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                          0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                            0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                  0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                  0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CM_FEAT_CONFIG_BMSK                                        0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CM_FEAT_CONFIG_SHFT                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                            0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                             0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                        0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                         0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                        0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                         0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                         0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                         0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                              0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                              0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_BMSK                                                     0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_SHFT                                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CRI_CM_PRIVATE_BMSK                                          0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CRI_CM_PRIVATE_SHFT                                          0x0

#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004134)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RSVD0_BMSK                                            0xf8000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RSVD0_SHFT                                                  0x1b
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE26_BMSK                                           0x4000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE26_SHFT                                                0x1a
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE25_BMSK                                           0x2000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE25_SHFT                                                0x19
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE24_BMSK                                           0x1000000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE24_SHFT                                                0x18
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE23_BMSK                                            0x800000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE23_SHFT                                                0x17
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE22_BMSK                                            0x400000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE22_SHFT                                                0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_BMSK                                            0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_SHFT                                                0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_BMSK                                            0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_SHFT                                                0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_BMSK                                             0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_SHFT                                                0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_BMSK                                             0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_SHFT                                                0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                      0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                           0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                              0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_BMSK                                                0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_SHFT                                                   0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_BMSK                                              0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_SHFT                                                 0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB12_BMSK                                              0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB12_SHFT                                                 0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                          0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                            0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                  0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                  0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CM_FEAT_CONFIG_BMSK                                        0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CM_FEAT_CONFIG_SHFT                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                            0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                             0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                        0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                         0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                        0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                         0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                         0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                         0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                              0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                              0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_BMSK                                                     0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_SHFT                                                     0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CRI_CM_PRIVATE_BMSK                                          0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CRI_CM_PRIVATE_SHFT                                          0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004138)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                              0xfc000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                    0x1a
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                           0x3fff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                 0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                             0xffe
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                               0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                               0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                               0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000413c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                     0xfffc0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                           0x12
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                 0x3ffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                     0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004140)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                            0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004144)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                     0xff000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                           0x18
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                    0xfff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                         0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                              0xff0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                0x4
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                            0xf
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                            0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004148)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                         0xffff0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                               0x10
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                             0xffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000414c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                      0xffffff00
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                             0x8
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                     0xff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                      0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004150)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RSVD0_BMSK                                       0xe0000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RSVD0_SHFT                                             0x1d
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DEBUG_DISABLE_POLICY_BMSK                        0x10000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DEBUG_DISABLE_POLICY_SHFT                              0x1c
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG26_SECURE_BMSK                           0x8000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG26_SECURE_SHFT                                0x1b
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG25_SECURE_BMSK                           0x4000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG25_SECURE_SHFT                                0x1a
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG24_SECURE_BMSK                           0x2000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG24_SECURE_SHFT                                0x19
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG23_SECURE_BMSK                           0x1000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG23_SECURE_SHFT                                0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG22_SECURE_BMSK                            0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG22_SECURE_SHFT                                0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                               0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                   0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                           0x3c0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                               0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                             0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                             0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE1_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE1_SHFT                                             0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                              0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                 0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                              0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                 0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                         0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                            0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                 0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                   0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                             0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                               0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                    0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                      0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                              0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                         0xe0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                          0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                               0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                              0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                              0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                       0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                       0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                    0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                    0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004154)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                      0xf8000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                            0x1b
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SDCC5_SCM_FORCE_EFUSE_KEY_BMSK                    0x4000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SDCC5_SCM_FORCE_EFUSE_KEY_SHFT                         0x1a
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_BMSK                       0x2000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_SHFT                            0x19
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_BMSK                          0x1000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_SHFT                               0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_BMSK                           0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_SHFT                               0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                      0x780000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                          0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                           0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                              0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                            0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                               0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                             0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                               0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                  0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                               0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                  0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                            0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                               0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                             0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                               0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                 0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                               0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                 0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                   0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                     0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                   0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                     0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                             0x80
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                              0x7
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                 0x40
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                  0x6
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                 0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                  0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                  0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                  0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                  0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                  0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                  0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                  0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004158)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                              0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                    0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                       0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                          0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000415c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                  0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                      0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                         0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004160)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_0_BMSK                       0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_0_SHFT                             0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_BMSK                              0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_SHFT                                    0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_1_BMSK                       0x20000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RESOLUTION_LIMITER_1_SHFT                             0x1d
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MDP_EFUSE_LTC0_DISABLE_BMSK                     0x10000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MDP_EFUSE_LTC0_DISABLE_SHFT                           0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_NIDNT_DISABLE_BMSK                               0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_NIDNT_DISABLE_SHFT                                    0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_DISABLE_HEVC_BMSK                          0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_DISABLE_HEVC_SHFT                               0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_BMSK                      0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_SHFT                           0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_BMSK                         0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_SHFT                              0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_BMSK                          0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_SHFT                              0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_22_BMSK                                     0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_22_SHFT                                         0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_NAV_DISABLE_BMSK                                  0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_NAV_DISABLE_SHFT                                      0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_20_BMSK                                     0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_20_SHFT                                         0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_SPARE_DISABLE_BMSK                           0xe0000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_SPARE_DISABLE_SHFT                              0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_UIM2_DISABLE_BMSK                            0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_UIM2_DISABLE_SHFT                               0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_UIM1_DISABLE_BMSK                             0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_UIM1_DISABLE_SHFT                                0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SMMU_TBU_BYPASS_DISABLE_BMSK                        0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SMMU_TBU_BYPASS_DISABLE_SHFT                           0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_DSDA_DISABLE_BMSK                             0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_DSDA_DISABLE_SHFT                                0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX3_DISABLE_BMSK                            0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX3_DISABLE_SHFT                               0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX2_DISABLE_BMSK                             0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_BBRX2_DISABLE_SHFT                               0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_GLOBAL_DISABLE_BMSK                            0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_GLOBAL_DISABLE_SHFT                              0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_9_BMSK                                         0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_9_SHFT                                           0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_8_BMSK                                         0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_8_SHFT                                           0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_7_BMSK                                          0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPARE_7_SHFT                                           0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_ABOVE_CAT1_DISABLE_BMSK                     0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_ABOVE_CAT1_DISABLE_SHFT                      0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_TDSCDMA_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_TDSCDMA_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_HSPA_DISABLE_BMSK                               0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_HSPA_DISABLE_SHFT                                0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_LTE_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_WCDMA_DISABLE_BMSK                               0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_WCDMA_DISABLE_SHFT                               0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_DO_DISABLE_BMSK                                  0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_DO_DISABLE_SHFT                                  0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_1X_DISABLE_BMSK                                  0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MODEM_1X_DISABLE_SHFT                                  0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004164)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                      0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CM_FEAT_CONFIG_DISABLE_BMSK                     0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CM_FEAT_CONFIG_DISABLE_SHFT                           0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_GFX_FREQ_LIMIT_FUSE_BMSK                        0x30000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_GFX_FREQ_LIMIT_FUSE_SHFT                              0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SYSBARDISABLE_BMSK                               0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SYSBARDISABLE_SHFT                                    0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE6_BMSK                                      0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE6_SHFT                                           0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DDR_FREQ_LIMIT_EN_BMSK                           0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DDR_FREQ_LIMIT_EN_SHFT                                0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VFE_DISABLE_BMSK                                 0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VFE_DISABLE_SHFT                                      0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY1_BMSK                      0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY1_SHFT                          0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY0_BMSK                      0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_FORCE_HW_KEY0_SHFT                          0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_DISABLE_BMSK                            0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_ICE_DISABLE_SHFT                                0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE5_BMSK                                       0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE5_SHFT                                           0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_SCM_DISABLE_BMSK                             0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDCC5_SCM_DISABLE_SHFT                                0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CCI_FREQ_SCALE_BMSK                                0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CCI_FREQ_SCALE_SHFT                                   0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_CLOCKCFG_BMSK                                 0x38000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_CLOCKCFG_SHFT                                     0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_BMSK                                    0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_SHFT                                       0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_UL_CA_DISABLE_BMSK                         0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_UL_CA_DISABLE_SHFT                            0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_ABOVE_CAT4_DISABLE_BMSK                    0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_LTE_ABOVE_CAT4_DISABLE_SHFT                       0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_HSUPA_DC_DISABLE_BMSK                     0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_HSUPA_DC_DISABLE_SHFT                       0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_VP9D_BMSK                              0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_VP9D_SHFT                                0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE2_BMSK                                          0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE2_SHFT                                            0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_265E_BMSK                              0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_265E_SHFT                                0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_4KX2KD_BMSK                             0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENUS_DISABLE_4KX2KD_SHFT                              0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CSID_2_DISABLE_BMSK                                   0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_CSID_2_DISABLE_SHFT                                    0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_CFG_BMSK                                0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_CFG_SHFT                                 0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_DELAY_BMSK                              0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_BOOT_FSM_DELAY_SHFT                               0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE1_BMSK                                            0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE1_SHFT                                            0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_L2_CACHE_UPPER_BANK_DISABLE_BMSK                  0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APPS_L2_CACHE_UPPER_BANK_DISABLE_SHFT                  0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004168)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                     0xfc000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                           0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_BMSK               0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_SHFT                    0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_BMSK               0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_SHFT                    0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                    0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                        0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_LPASS_NIDEN_DISABLE_BMSK                       0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_LPASS_NIDEN_DISABLE_SHFT                           0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_LPASS_DBGEN_DISABLE_BMSK                       0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_LPASS_DBGEN_DISABLE_SHFT                           0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                      0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                          0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                        0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                           0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                         0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                            0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                          0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                             0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                          0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                             0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                        0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                           0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                         0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                            0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                          0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                             0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                          0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                             0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                               0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                 0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                               0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                 0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                        0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                          0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                            0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                              0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                        0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                         0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                             0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                              0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                             0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                              0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                              0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                              0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                  0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                  0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                       0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000416c)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                     0xfe000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                           0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                    0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                         0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                            0xfff800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                 0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                          0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                            0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                         0x3e0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                           0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                           0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                            0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004170)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK           0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                  0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004174)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_7_BMSK                     0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_7_SHFT                           0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_6_BMSK                     0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_6_SHFT                           0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_5_BMSK                     0x20000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_5_SHFT                           0x1d
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_4_BMSK                     0x10000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_4_SHFT                           0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_GCC_DISABLE_BOOT_FSM_BMSK                        0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_GCC_DISABLE_BOOT_FSM_SHFT                             0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                          0x7800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                               0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                            0x780000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_3_BMSK                        0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_3_SHFT                           0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_2_BMSK                        0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_2_SHFT                           0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_1_BMSK                        0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_CFGCPUPRESENT_N_1_SHFT                           0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_CSR_BMSK                             0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_CSR_SHFT                                0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_AARCH64_ENABLE_BMSK                            0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_AARCH64_ENABLE_SHFT                               0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                            0x3000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                               0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                              0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                         0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                           0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                               0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                 0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                       0x180
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                         0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                              0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                               0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                           0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                            0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                 0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004178)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_LSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000417c)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW0_MSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004180)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_LSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004184)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW1_MSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004188)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_LSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_ADDR                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000418c)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_RSVD0_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_CM_FEAT_CONFIG_ROW2_MSB_RSVD0_SHFT                                          0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004190 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                          3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004194 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                          3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                              0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                          3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                          3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                              0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041d0)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                 0xff000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                       0x18
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                   0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                       0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                     0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                        0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                       0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                        0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041d4)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK                                             0xffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                   0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                       0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                     0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                        0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                       0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                        0x0

#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_ADDR                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041d8)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_SPARE0_BMSK                                         0xc0000000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_SPARE0_SHFT                                               0x1e
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR6_AGING_SENSOR_0_BMSK                            0x3f000000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR6_AGING_SENSOR_0_SHFT                                  0x18
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR5_AGING_SENSOR_0_BMSK                              0xfc0000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR5_AGING_SENSOR_0_SHFT                                  0x12
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR2_AGING_SENSOR_0_BMSK                               0x3f000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR2_AGING_SENSOR_0_SHFT                                   0xc
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR1_AGING_SENSOR_0_BMSK                                 0xfc0
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR1_AGING_SENSOR_0_SHFT                                   0x6
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR0_AGING_SENSOR_0_BMSK                                  0x3f
#define HWIO_QFPROM_CORR_CALIB12_ROW0_LSB_CPR0_AGING_SENSOR_0_SHFT                                   0x0

#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_ADDR                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041dc)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_RMSK                                                  0xffffff
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR_LOCAL_RC_BMSK                                     0xe00000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR_LOCAL_RC_SHFT                                         0x15
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_NOM_TARGET_BMSK                                  0x1f0000
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_NOM_TARGET_SHFT                                      0x10
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_TURBO_TARGET_BMSK                                  0xf800
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_TURBO_TARGET_SHFT                                     0xb
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR1_SVS2_TARGET_BMSK                                    0x7c0
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR1_SVS2_TARGET_SHFT                                      0x6
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_SVS_TARGET_BMSK                                      0x3e
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_CPR3_SVS_TARGET_SHFT                                       0x1
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_SPARE1_BMSK                                                0x1
#define HWIO_QFPROM_CORR_CALIB12_ROW0_MSB_SPARE1_SHFT                                                0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041e0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_MAXn                                                         3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                            0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041e4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK                                                  0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_MAXn                                                         3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                       0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                            0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004200)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                            0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004204)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK                                                  0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_BMSK                     0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_SHFT                          0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004208)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_1_8_0_BMSK                       0xff800000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_1_8_0_SHFT                             0x17
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_0_BMSK                             0x7fe000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CS_SENSOR_PARAM_CORE_0_SHFT                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CSI_PHY_BMSK                                              0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_CSI_PHY_SHFT                                                 0x8
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_DSI_PHY_BMSK                                                0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_DSI_PHY_SHFT                                                 0x4
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_DSIPHY_PLL_BMSK                                              0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_DSIPHY_PLL_SHFT                                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000420c)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_2_0_BMSK               0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_2_0_SHFT                   0x15
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_3_BMSK                             0x1ff800
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_3_SHFT                                  0xb
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_2_BMSK                                0x7fe
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_2_SHFT                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_1_9_BMSK                                0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_CS_SENSOR_PARAM_CORE_1_9_SHFT                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004210)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_6_0_BMSK             0xfe000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_6_0_SHFT                   0x19
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_1_INTERCEPT_BMSK                  0x1ff8000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_1_INTERCEPT_SHFT                        0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_9_4_BMSK                 0x7e00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_9_4_SHFT                    0x9
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IDDQ_A53_ACTIVE_MEAS_BMSK                                  0x1fe
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IDDQ_A53_ACTIVE_MEAS_SHFT                                    0x1
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_3_BMSK                      0x1
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CS_SENSOR_PARAM_CORE_0_INTERCEPT_3_SHFT                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004214)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE1_BMSK                                             0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE1_SHFT                                                 0x16
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APC1_LDO_VREF_TRIM_BMSK                                 0x3e0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APC1_LDO_VREF_TRIM_SHFT                                     0x11
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_AMP_COMP_BMSK                                            0x1e000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_AMP_COMP_SHFT                                                0xd
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_3_INTERCEPT_BMSK                     0x1ff8
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_3_INTERCEPT_SHFT                        0x3
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_9_7_BMSK                    0x7
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CS_SENSOR_PARAM_CORE_2_INTERCEPT_9_7_SHFT                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004218)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS1_POINT2_BMSK                                    0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS1_POINT2_SHFT                                          0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS1_POINT1_BMSK                                     0x3f00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS1_POINT1_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS0_POINT2_BMSK                                       0xfc000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS0_POINT2_SHFT                                           0xe
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS0_POINT1_BMSK                                        0x3f00
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS0_POINT1_SHFT                                           0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS_BASE0_BMSK                                            0xff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_TSENS_BASE0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000421c)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS3_POINT2_BMSK                                      0xfc0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS3_POINT2_SHFT                                          0x12
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS3_POINT1_BMSK                                       0x3f000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS3_POINT1_SHFT                                           0xc
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS2_POINT2_BMSK                                         0xfc0
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS2_POINT2_SHFT                                           0x6
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS2_POINT1_BMSK                                          0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_TSENS2_POINT1_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004220)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS5_POINT2_BMSK                                    0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS5_POINT2_SHFT                                          0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS5_POINT1_BMSK                                     0x3f00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS5_POINT1_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS4_POINT2_BMSK                                       0xfc000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS4_POINT2_SHFT                                           0xe
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS4_POINT1_BMSK                                        0x3f00
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS4_POINT1_SHFT                                           0x8
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS_BASE1_BMSK                                            0xff
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_TSENS_BASE1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004224)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS7_POINT2_BMSK                                      0xfc0000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS7_POINT2_SHFT                                          0x12
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS7_POINT1_BMSK                                       0x3f000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS7_POINT1_SHFT                                           0xc
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS6_POINT2_BMSK                                         0xfc0
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS6_POINT2_SHFT                                           0x6
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS6_POINT1_BMSK                                          0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_TSENS6_POINT1_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004228)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS10_POINT1_4_0_BMSK                               0xf8000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS10_POINT1_4_0_SHFT                                     0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS9_POINT2_BMSK                                     0x7e00000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS9_POINT2_SHFT                                          0x15
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS9_POINT1_BMSK                                      0x1f8000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS9_POINT1_SHFT                                           0xf
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS8_POINT2_BMSK                                        0x7e00
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS8_POINT2_SHFT                                           0x9
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS8_POINT1_BMSK                                         0x1f8
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS8_POINT1_SHFT                                           0x3
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS_CALIB_BMSK                                             0x7
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_TSENS_CALIB_SHFT                                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000422c)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_SPARE_6_0_BMSK                                   0xfe0000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_SPARE_6_0_SHFT                                       0x11
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_RANGE_CORR_BMSK                                   0x10000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_RANGE_CORR_SHFT                                      0x10
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_AVEG_CORR_BMSK                                     0x8000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_AVEG_CORR_SHFT                                        0xf
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_RPOLY_CAL_BMSK                                     0x7f80
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TXDAC0_RPOLY_CAL_SHFT                                        0x7
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TSENS10_POINT2_BMSK                                         0x7e
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TSENS10_POINT2_SHFT                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TSENS10_POINT1_5_BMSK                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_TSENS10_POINT1_5_SHFT                                        0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004230)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M3_1_0_BMSK                                      0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M3_1_0_SHFT                                            0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M2_BMSK                                          0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M2_SHFT                                                0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M1_BMSK                                           0x7000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M1_SHFT                                                0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M0_BMSK                                            0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B0M0_SHFT                                                0x15
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B0_BMSK                                               0x1c0000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B0_SHFT                                                   0x12
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_B_BMSK                                               0x30000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_B_SHFT                                                  0x10
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CAP_B_BMSK                                                0xc000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CAP_B_SHFT                                                   0xe
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_B_BMSK                                                0x3000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_B_SHFT                                                   0xc
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_MODEM_TXDAC_FUSEFLAG_BMSK                                  0x800
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_MODEM_TXDAC_FUSEFLAG_SHFT                                    0xb
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_RANGE_CORR_BMSK                                     0x400
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_RANGE_CORR_SHFT                                       0xa
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_AVEG_CORR_BMSK                                      0x200
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_AVEG_CORR_SHFT                                        0x9
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_RPOLY_CAL_BMSK                                      0x1fe
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC1_RPOLY_CAL_SHFT                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC0_SPARE_7_BMSK                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_TXDAC0_SPARE_7_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004234)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B2M0_0_BMSK                                          0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B2M0_0_SHFT                                              0x17
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B2_BMSK                                               0x700000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B2_SHFT                                                   0x14
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_B1_BMSK                                             0xc0000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_B1_SHFT                                                0x12
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M3_BMSK                                             0x38000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M3_SHFT                                                 0xf
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M2_BMSK                                              0x7000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M2_SHFT                                                 0xc
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M1_BMSK                                               0xe00
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M1_SHFT                                                 0x9
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M0_BMSK                                               0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B1M0_SHFT                                                 0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B1_BMSK                                                   0x38
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B1_SHFT                                                    0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_B0_BMSK                                                 0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_B0_SHFT                                                 0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B0M3_2_BMSK                                               0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B0M3_2_SHFT                                               0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004238)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_GNSS_ADC_CALIB_1_0_BMSK                               0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_GNSS_ADC_CALIB_1_0_SHFT                                     0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_B3_BMSK                                          0x30000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_B3_SHFT                                                0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M3_BMSK                                           0xe000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M3_SHFT                                                0x19
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M2_BMSK                                           0x1c00000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M2_SHFT                                                0x16
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M1_BMSK                                            0x380000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M1_SHFT                                                0x13
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M0_BMSK                                             0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B3M0_SHFT                                                0x10
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B3_BMSK                                                 0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B3_SHFT                                                    0xd
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_B2_BMSK                                              0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_B2_SHFT                                                 0xb
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M3_BMSK                                               0x700
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M3_SHFT                                                 0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M2_BMSK                                                0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M2_SHFT                                                 0x5
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M1_BMSK                                                0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M1_SHFT                                                 0x2
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M0_2_1_BMSK                                             0x3
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B2M0_2_1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000423c)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SPARE1_BMSK                                             0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SPARE1_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_VOLTAGE_SENSOR_CALIB_BMSK                                0xfffc0
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_VOLTAGE_SENSOR_CALIB_SHFT                                    0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_GNSS_ADC_CALIB_7_2_BMSK                                     0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_GNSS_ADC_CALIB_7_2_SHFT                                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004240)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW7_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_SPARE0_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_SPARE0_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_SVS_TARGET_BMSK                                  0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_SVS_TARGET_SHFT                                        0x19
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_NOMINAL_TARGET_BMSK                               0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_NOMINAL_TARGET_SHFT                                    0x14
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_TURBO_TARGET_BMSK                                   0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR1_TURBO_TARGET_SHFT                                       0xf
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_SVS_TARGET_BMSK                                      0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_SVS_TARGET_SHFT                                         0xa
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_NOMINAL_TARGET_BMSK                                   0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_NOMINAL_TARGET_SHFT                                     0x5
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_TURBO_TARGET_BMSK                                      0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR0_TURBO_TARGET_SHFT                                       0x0

#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004244)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SPARE2_BMSK                                             0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SPARE2_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_SVS2_TARGET_BMSK                                   0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_SVS2_TARGET_SHFT                                       0x12
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_SVS_TARGET_BMSK                                     0x3e000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_SVS_TARGET_SHFT                                         0xd
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_NOMINAL_TARGET_BMSK                                  0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_NOMINAL_TARGET_SHFT                                     0x8
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_TURBO_TARGET_BMSK                                      0xf8
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_CPR2_TURBO_TARGET_SHFT                                       0x3
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SPARE1_BMSK                                                  0x7
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004248)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW8_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_SVS_TARGET_BMSK                                  0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_SVS_TARGET_SHFT                                        0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_NOMINAL_TARGET_BMSK                               0x3f00000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_NOMINAL_TARGET_SHFT                                    0x14
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_TURBO_TARGET_BMSK                                   0xfc000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_TURBO_TARGET_SHFT                                       0xe
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_SVS_ROSEL_BMSK                                       0x3800
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_SVS_ROSEL_SHFT                                          0xb
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_NOM_ROSEL_BMSK                                        0x700
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_NOM_ROSEL_SHFT                                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_TUR_ROSEL_BMSK                                         0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR5_TUR_ROSEL_SHFT                                          0x5
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR3_SVS2_TARGET_BMSK                                       0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CPR3_SVS2_TARGET_SHFT                                        0x0

#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000424c)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_SVS_QUOT_VMIN_BMSK                                 0xff0000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_SVS_QUOT_VMIN_SHFT                                     0x10
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_NOMINAL_QUOT_VMIN_BMSK                               0xff00
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_NOMINAL_QUOT_VMIN_SHFT                                  0x8
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_TURBO_QUOT_VMIN_BMSK                                   0xff
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CPR5_TURBO_QUOT_VMIN_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004250)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW9_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR6_NOM_ROSEL_1_0_BMSK                               0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR6_NOM_ROSEL_1_0_SHFT                                     0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR6_TUR_ROSEL_BMSK                                   0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR6_TUR_ROSEL_SHFT                                         0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_SVS_OFFSET_BMSK                                   0x7e00000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_SVS_OFFSET_SHFT                                        0x15
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_NOMINAL_OFFSET_BMSK                                0x1f8000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_NOMINAL_OFFSET_SHFT                                     0xf
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_TURBO_OFFSET_BMSK                                    0x7f00
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_TURBO_OFFSET_SHFT                                       0x8
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_SVS2_QUOT_VMIN_BMSK                                    0xff
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CPR5_SVS2_QUOT_VMIN_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004254)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SPARE1_BMSK                                             0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SPARE1_SHFT                                                 0x16
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_SVS_TARGET_BMSK                                    0x3f0000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_SVS_TARGET_SHFT                                        0x10
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_NOMINAL_TARGET_BMSK                                  0xfc00
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_NOMINAL_TARGET_SHFT                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_TURBO_TARGET_BMSK                                     0x3f0
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_TURBO_TARGET_SHFT                                       0x4
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_SVS_ROSEL_BMSK                                          0xe
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_SVS_ROSEL_SHFT                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_NOM_ROSEL_2_BMSK                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CPR6_NOM_ROSEL_2_SHFT                                        0x0

#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004258)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_NOMINAL_OFFSET_0_BMSK                           0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_NOMINAL_OFFSET_0_SHFT                                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_TURBO_OFFSET_BMSK                               0x7f000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_TURBO_OFFSET_SHFT                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_SVS_QUOT_VMIN_BMSK                                0xff0000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_SVS_QUOT_VMIN_SHFT                                    0x10
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_NOMINAL_QUOT_VMIN_BMSK                              0xff00
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_NOMINAL_QUOT_VMIN_SHFT                                 0x8
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_TURBO_QUOT_VMIN_BMSK                                  0xff
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CPR6_TURBO_QUOT_VMIN_SHFT                                   0x0

#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000425c)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_SPARE1_BMSK                                            0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_SPARE1_SHFT                                                0x17
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TXDAC1_SPARE_BMSK                                      0x7f8000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TXDAC1_SPARE_SHFT                                           0xf
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR_GLOBAL_RC_BMSK                                       0x7000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR_GLOBAL_RC_SHFT                                          0xc
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_SPARE2_BMSK                                               0x800
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_SPARE2_SHFT                                                 0xb
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR6_SVS_OFFSET_BMSK                                      0x7e0
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR6_SVS_OFFSET_SHFT                                        0x5
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR6_NOMINAL_OFFSET_5_1_BMSK                               0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_CPR6_NOMINAL_OFFSET_5_1_SHFT                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004260)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_1_0_BMSK               0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_1_0_SHFT                     0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P3_ACTIVE_MEAS_FUSE_BITS_BMSK           0x3f000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P3_ACTIVE_MEAS_FUSE_BITS_SHFT                 0x18
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P2_ACTIVE_MEAS_FUSE_BITS_BMSK             0xfc0000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P2_ACTIVE_MEAS_FUSE_BITS_SHFT                 0x12
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P1_ACTIVE_MEAS_FUSE_BITS_BMSK              0x3f000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_P1_ACTIVE_MEAS_FUSE_BITS_SHFT                  0xc
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_ACTIVE_MEAS_FUSE_BITS_BMSK                   0xfc0
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_MAX_ACTIVE_MEAS_FUSE_BITS_SHFT                     0x6
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_L2_ACTIVE_MEAS_FUSE_BITS_BMSK                     0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IDDQ_A72_L2_ACTIVE_MEAS_FUSE_BITS_SHFT                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004264)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_SPARE1_BMSK                                            0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_SPARE1_SHFT                                                0x15
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_REV_CTRL_BMSK                                     0x180000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_REV_CTRL_SHFT                                         0x13
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_A72_WORST_CORE_NUM_BMSK                                 0x60000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_A72_WORST_CORE_NUM_SHFT                                    0x11
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_MULTIPLICATION_FACTOR_BMSK                              0x18000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_MULTIPLICATION_FACTOR_SHFT                                  0xf
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_MX_ACTIVE_MEAS_FUSE_BITS_BMSK                       0x7e00
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_MX_ACTIVE_MEAS_FUSE_BITS_SHFT                          0x9
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_GFX_ACTIVE_SUM_MEAS_FUSE_BITS_BMSK                   0x1f8
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_GFX_ACTIVE_SUM_MEAS_FUSE_BITS_SHFT                     0x3
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_4_2_BMSK                      0x7
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IDDQ_CX_ACTIVE_MEAS_FUSE_BITS_4_2_SHFT                      0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n)                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004268 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_MAXn                                                     16
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n)                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000426c + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_MAXn                                                     16
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                    0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042f0)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042f4)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK                                              0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_BMSK                                    0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042f8)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042fc)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK                                              0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_BMSK                                    0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_SHFT                                         0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004300 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_MAXn                                                      31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004304 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK                                                0xffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_MAXn                                                      31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                         0xfe0000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                             0x11
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                      0x1fffe
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                          0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                            0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                            0x0

#define HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004400)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RSVD0_BMSK                                                0xf8000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RSVD0_SHFT                                                      0x1b
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                       0x4000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                            0x1a
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                       0x2000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                            0x19
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                       0x1000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                            0x18
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                        0x800000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                            0x17
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                        0x400000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                            0x16
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                        0x200000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                            0x15
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                        0x100000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                            0x14
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                         0x80000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                            0x13
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                         0x40000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                            0x12
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                         0x20000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                            0x11
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                         0x10000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                            0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                             0xf
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                          0x4000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                             0xe
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                          0x2000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                             0xd
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                          0x1000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                             0xc
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                           0x800
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                             0xb
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                           0x400
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                             0xa
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                            0x200
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                              0x9
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                            0x100
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                              0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                             0x80
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                              0x7
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                             0x40
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                              0x6
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                             0x20
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                              0x5
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                             0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                              0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                              0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                              0x3
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                              0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                              0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                              0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                              0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                              0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                              0x0

#define HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004404)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                    0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                           0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004408)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG18_LSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_BMSK                                          0xffffff00
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_SHFT                                                 0x8
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SAFE_SWITCH_BMSK                                           0xff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SAFE_SWITCH_SHFT                                            0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000440c)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004410)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004414)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004418)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000441c)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004420)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004424)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004428)
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG22_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000442c)
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG22_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004430)
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG23_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004434)
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG23_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004438)
#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG24_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG24_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG24_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG24_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000443c)
#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG24_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG24_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG24_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG24_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004440)
#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG25_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG25_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG25_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG25_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004444)
#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG25_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG25_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG25_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG25_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004448)
#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG26_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG26_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG26_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_SPARE0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG26_LSB_SPARE0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000444c)
#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG26_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG26_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG26_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_SPARE1_BMSK                                            0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG26_MSB_SPARE1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n)                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004800 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_MAXn                                                            39
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                        0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                               0x0

#define HWIO_SEC_CTRL_HW_VERSION_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006000)
#define HWIO_SEC_CTRL_HW_VERSION_RMSK                                                         0xffffffff
#define HWIO_SEC_CTRL_HW_VERSION_IN          \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, HWIO_SEC_CTRL_HW_VERSION_RMSK)
#define HWIO_SEC_CTRL_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, m)
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_BMSK                                                   0xf0000000
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_SHFT                                                         0x1c
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_BMSK                                                    0xfff0000
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_SHFT                                                         0x10
#define HWIO_SEC_CTRL_HW_VERSION_STEP_BMSK                                                        0xffff
#define HWIO_SEC_CTRL_HW_VERSION_STEP_SHFT                                                           0x0

#define HWIO_FEATURE_CONFIG0_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006004)
#define HWIO_FEATURE_CONFIG0_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG0_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, HWIO_FEATURE_CONFIG0_RMSK)
#define HWIO_FEATURE_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, m)
#define HWIO_FEATURE_CONFIG0_RESOLUTION_LIMITER_0_BMSK                                        0x80000000
#define HWIO_FEATURE_CONFIG0_RESOLUTION_LIMITER_0_SHFT                                              0x1f
#define HWIO_FEATURE_CONFIG0_DSI_1_DISABLE_BMSK                                               0x40000000
#define HWIO_FEATURE_CONFIG0_DSI_1_DISABLE_SHFT                                                     0x1e
#define HWIO_FEATURE_CONFIG0_RESOLUTION_LIMITER_1_BMSK                                        0x20000000
#define HWIO_FEATURE_CONFIG0_RESOLUTION_LIMITER_1_SHFT                                              0x1d
#define HWIO_FEATURE_CONFIG0_MDP_EFUSE_LTC0_DISABLE_BMSK                                      0x10000000
#define HWIO_FEATURE_CONFIG0_MDP_EFUSE_LTC0_DISABLE_SHFT                                            0x1c
#define HWIO_FEATURE_CONFIG0_NIDNT_DISABLE_BMSK                                                0x8000000
#define HWIO_FEATURE_CONFIG0_NIDNT_DISABLE_SHFT                                                     0x1b
#define HWIO_FEATURE_CONFIG0_VENUS_DISABLE_HEVC_BMSK                                           0x4000000
#define HWIO_FEATURE_CONFIG0_VENUS_DISABLE_HEVC_SHFT                                                0x1a
#define HWIO_FEATURE_CONFIG0_SECURE_CHANNEL_DISABLE_BMSK                                       0x2000000
#define HWIO_FEATURE_CONFIG0_SECURE_CHANNEL_DISABLE_SHFT                                            0x19
#define HWIO_FEATURE_CONFIG0_VP8_DECODER_DISABLE_BMSK                                          0x1000000
#define HWIO_FEATURE_CONFIG0_VP8_DECODER_DISABLE_SHFT                                               0x18
#define HWIO_FEATURE_CONFIG0_VP8_ENCODER_DISABLE_BMSK                                           0x800000
#define HWIO_FEATURE_CONFIG0_VP8_ENCODER_DISABLE_SHFT                                               0x17
#define HWIO_FEATURE_CONFIG0_SPARE_22_BMSK                                                      0x400000
#define HWIO_FEATURE_CONFIG0_SPARE_22_SHFT                                                          0x16
#define HWIO_FEATURE_CONFIG0_NAV_DISABLE_BMSK                                                   0x200000
#define HWIO_FEATURE_CONFIG0_NAV_DISABLE_SHFT                                                       0x15
#define HWIO_FEATURE_CONFIG0_SPARE_20_BMSK                                                      0x100000
#define HWIO_FEATURE_CONFIG0_SPARE_20_SHFT                                                          0x14
#define HWIO_FEATURE_CONFIG0_MODEM_SPARE_DISABLE_BMSK                                            0xe0000
#define HWIO_FEATURE_CONFIG0_MODEM_SPARE_DISABLE_SHFT                                               0x11
#define HWIO_FEATURE_CONFIG0_MODEM_UIM2_DISABLE_BMSK                                             0x10000
#define HWIO_FEATURE_CONFIG0_MODEM_UIM2_DISABLE_SHFT                                                0x10
#define HWIO_FEATURE_CONFIG0_MODEM_UIM1_DISABLE_BMSK                                              0x8000
#define HWIO_FEATURE_CONFIG0_MODEM_UIM1_DISABLE_SHFT                                                 0xf
#define HWIO_FEATURE_CONFIG0_SMMU_TBU_BYPASS_DISABLE_BMSK                                         0x4000
#define HWIO_FEATURE_CONFIG0_SMMU_TBU_BYPASS_DISABLE_SHFT                                            0xe
#define HWIO_FEATURE_CONFIG0_MODEM_DSDA_DISABLE_BMSK                                              0x2000
#define HWIO_FEATURE_CONFIG0_MODEM_DSDA_DISABLE_SHFT                                                 0xd
#define HWIO_FEATURE_CONFIG0_MODEM_BBRX3_DISABLE_BMSK                                             0x1000
#define HWIO_FEATURE_CONFIG0_MODEM_BBRX3_DISABLE_SHFT                                                0xc
#define HWIO_FEATURE_CONFIG0_MODEM_BBRX2_DISABLE_BMSK                                              0x800
#define HWIO_FEATURE_CONFIG0_MODEM_BBRX2_DISABLE_SHFT                                                0xb
#define HWIO_FEATURE_CONFIG0_MODEM_GLOBAL_DISABLE_BMSK                                             0x400
#define HWIO_FEATURE_CONFIG0_MODEM_GLOBAL_DISABLE_SHFT                                               0xa
#define HWIO_FEATURE_CONFIG0_SPARE_9_BMSK                                                          0x200
#define HWIO_FEATURE_CONFIG0_SPARE_9_SHFT                                                            0x9
#define HWIO_FEATURE_CONFIG0_SPARE_8_BMSK                                                          0x100
#define HWIO_FEATURE_CONFIG0_SPARE_8_SHFT                                                            0x8
#define HWIO_FEATURE_CONFIG0_SPARE_7_BMSK                                                           0x80
#define HWIO_FEATURE_CONFIG0_SPARE_7_SHFT                                                            0x7
#define HWIO_FEATURE_CONFIG0_MODEM_LTE_ABOVE_CAT1_DISABLE_BMSK                                      0x40
#define HWIO_FEATURE_CONFIG0_MODEM_LTE_ABOVE_CAT1_DISABLE_SHFT                                       0x6
#define HWIO_FEATURE_CONFIG0_MODEM_TDSCDMA_DISABLE_BMSK                                             0x20
#define HWIO_FEATURE_CONFIG0_MODEM_TDSCDMA_DISABLE_SHFT                                              0x5
#define HWIO_FEATURE_CONFIG0_MODEM_HSPA_DISABLE_BMSK                                                0x10
#define HWIO_FEATURE_CONFIG0_MODEM_HSPA_DISABLE_SHFT                                                 0x4
#define HWIO_FEATURE_CONFIG0_MODEM_LTE_DISABLE_BMSK                                                  0x8
#define HWIO_FEATURE_CONFIG0_MODEM_LTE_DISABLE_SHFT                                                  0x3
#define HWIO_FEATURE_CONFIG0_MODEM_WCDMA_DISABLE_BMSK                                                0x4
#define HWIO_FEATURE_CONFIG0_MODEM_WCDMA_DISABLE_SHFT                                                0x2
#define HWIO_FEATURE_CONFIG0_MODEM_DO_DISABLE_BMSK                                                   0x2
#define HWIO_FEATURE_CONFIG0_MODEM_DO_DISABLE_SHFT                                                   0x1
#define HWIO_FEATURE_CONFIG0_MODEM_1X_DISABLE_BMSK                                                   0x1
#define HWIO_FEATURE_CONFIG0_MODEM_1X_DISABLE_SHFT                                                   0x0

#define HWIO_FEATURE_CONFIG1_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006008)
#define HWIO_FEATURE_CONFIG1_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG1_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, HWIO_FEATURE_CONFIG1_RMSK)
#define HWIO_FEATURE_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, m)
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                                 0x80000000
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                                       0x1f
#define HWIO_FEATURE_CONFIG1_CM_FEAT_CONFIG_DISABLE_BMSK                                      0x40000000
#define HWIO_FEATURE_CONFIG1_CM_FEAT_CONFIG_DISABLE_SHFT                                            0x1e
#define HWIO_FEATURE_CONFIG1_GFX_FREQ_LIMIT_FUSE_BMSK                                         0x30000000
#define HWIO_FEATURE_CONFIG1_GFX_FREQ_LIMIT_FUSE_SHFT                                               0x1c
#define HWIO_FEATURE_CONFIG1_SYSBARDISABLE_BMSK                                                0x8000000
#define HWIO_FEATURE_CONFIG1_SYSBARDISABLE_SHFT                                                     0x1b
#define HWIO_FEATURE_CONFIG1_SPARE6_BMSK                                                       0x4000000
#define HWIO_FEATURE_CONFIG1_SPARE6_SHFT                                                            0x1a
#define HWIO_FEATURE_CONFIG1_DDR_FREQ_LIMIT_EN_BMSK                                            0x2000000
#define HWIO_FEATURE_CONFIG1_DDR_FREQ_LIMIT_EN_SHFT                                                 0x19
#define HWIO_FEATURE_CONFIG1_VFE_DISABLE_BMSK                                                  0x1000000
#define HWIO_FEATURE_CONFIG1_VFE_DISABLE_SHFT                                                       0x18
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_FORCE_HW_KEY1_BMSK                                       0x800000
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_FORCE_HW_KEY1_SHFT                                           0x17
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_FORCE_HW_KEY0_BMSK                                       0x400000
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_FORCE_HW_KEY0_SHFT                                           0x16
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_DISABLE_BMSK                                             0x200000
#define HWIO_FEATURE_CONFIG1_SDCC5_ICE_DISABLE_SHFT                                                 0x15
#define HWIO_FEATURE_CONFIG1_SPARE5_BMSK                                                        0x100000
#define HWIO_FEATURE_CONFIG1_SPARE5_SHFT                                                            0x14
#define HWIO_FEATURE_CONFIG1_SDCC5_SCM_DISABLE_BMSK                                              0x80000
#define HWIO_FEATURE_CONFIG1_SDCC5_SCM_DISABLE_SHFT                                                 0x13
#define HWIO_FEATURE_CONFIG1_CCI_FREQ_SCALE_BMSK                                                 0x40000
#define HWIO_FEATURE_CONFIG1_CCI_FREQ_SCALE_SHFT                                                    0x12
#define HWIO_FEATURE_CONFIG1_APPS_CLOCKCFG_BMSK                                                  0x38000
#define HWIO_FEATURE_CONFIG1_APPS_CLOCKCFG_SHFT                                                      0xf
#define HWIO_FEATURE_CONFIG1_DCC_DISABLE_BMSK                                                     0x4000
#define HWIO_FEATURE_CONFIG1_DCC_DISABLE_SHFT                                                        0xe
#define HWIO_FEATURE_CONFIG1_MSMC_LTE_UL_CA_DISABLE_BMSK                                          0x2000
#define HWIO_FEATURE_CONFIG1_MSMC_LTE_UL_CA_DISABLE_SHFT                                             0xd
#define HWIO_FEATURE_CONFIG1_MSMC_LTE_ABOVE_CAT4_DISABLE_BMSK                                     0x1000
#define HWIO_FEATURE_CONFIG1_MSMC_LTE_ABOVE_CAT4_DISABLE_SHFT                                        0xc
#define HWIO_FEATURE_CONFIG1_MSMC_MODEM_HSUPA_DC_DISABLE_BMSK                                      0x800
#define HWIO_FEATURE_CONFIG1_MSMC_MODEM_HSUPA_DC_DISABLE_SHFT                                        0xb
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_VP9D_BMSK                                               0x400
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_VP9D_SHFT                                                 0xa
#define HWIO_FEATURE_CONFIG1_SPARE2_BMSK                                                           0x200
#define HWIO_FEATURE_CONFIG1_SPARE2_SHFT                                                             0x9
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_265E_BMSK                                               0x100
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_265E_SHFT                                                 0x8
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_4KX2KD_BMSK                                              0x80
#define HWIO_FEATURE_CONFIG1_VENUS_DISABLE_4KX2KD_SHFT                                               0x7
#define HWIO_FEATURE_CONFIG1_CSID_2_DISABLE_BMSK                                                    0x40
#define HWIO_FEATURE_CONFIG1_CSID_2_DISABLE_SHFT                                                     0x6
#define HWIO_FEATURE_CONFIG1_APPS_BOOT_FSM_CFG_BMSK                                                 0x20
#define HWIO_FEATURE_CONFIG1_APPS_BOOT_FSM_CFG_SHFT                                                  0x5
#define HWIO_FEATURE_CONFIG1_APPS_BOOT_FSM_DELAY_BMSK                                               0x18
#define HWIO_FEATURE_CONFIG1_APPS_BOOT_FSM_DELAY_SHFT                                                0x3
#define HWIO_FEATURE_CONFIG1_SPARE1_BMSK                                                             0x6
#define HWIO_FEATURE_CONFIG1_SPARE1_SHFT                                                             0x1
#define HWIO_FEATURE_CONFIG1_APPS_L2_CACHE_UPPER_BANK_DISABLE_BMSK                                   0x1
#define HWIO_FEATURE_CONFIG1_APPS_L2_CACHE_UPPER_BANK_DISABLE_SHFT                                   0x0

#define HWIO_FEATURE_CONFIG2_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000600c)
#define HWIO_FEATURE_CONFIG2_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG2_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, HWIO_FEATURE_CONFIG2_RMSK)
#define HWIO_FEATURE_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, m)
#define HWIO_FEATURE_CONFIG2_SPARE1_BMSK                                                      0xfc000000
#define HWIO_FEATURE_CONFIG2_SPARE1_SHFT                                                            0x1a
#define HWIO_FEATURE_CONFIG2_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_BMSK                                0x2000000
#define HWIO_FEATURE_CONFIG2_CAMCC_VFE_1_EFUSE_LIM_RES_8MP_SHFT                                     0x19
#define HWIO_FEATURE_CONFIG2_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_BMSK                                0x1000000
#define HWIO_FEATURE_CONFIG2_CAMCC_VFE_0_EFUSE_LIM_RES_8MP_SHFT                                     0x18
#define HWIO_FEATURE_CONFIG2_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                                     0x800000
#define HWIO_FEATURE_CONFIG2_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                                         0x17
#define HWIO_FEATURE_CONFIG2_QC_LPASS_NIDEN_DISABLE_BMSK                                        0x400000
#define HWIO_FEATURE_CONFIG2_QC_LPASS_NIDEN_DISABLE_SHFT                                            0x16
#define HWIO_FEATURE_CONFIG2_QC_LPASS_DBGEN_DISABLE_BMSK                                        0x200000
#define HWIO_FEATURE_CONFIG2_QC_LPASS_DBGEN_DISABLE_SHFT                                            0x15
#define HWIO_FEATURE_CONFIG2_QC_DAP_DEVICEEN_DISABLE_BMSK                                       0x100000
#define HWIO_FEATURE_CONFIG2_QC_DAP_DEVICEEN_DISABLE_SHFT                                           0x14
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPNIDEN_DISABLE_BMSK                                         0x80000
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPNIDEN_DISABLE_SHFT                                            0x13
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPIDEN_DISABLE_BMSK                                          0x40000
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPIDEN_DISABLE_SHFT                                             0x12
#define HWIO_FEATURE_CONFIG2_QC_DAP_NIDEN_DISABLE_BMSK                                           0x20000
#define HWIO_FEATURE_CONFIG2_QC_DAP_NIDEN_DISABLE_SHFT                                              0x11
#define HWIO_FEATURE_CONFIG2_QC_DAP_DBGEN_DISABLE_BMSK                                           0x10000
#define HWIO_FEATURE_CONFIG2_QC_DAP_DBGEN_DISABLE_SHFT                                              0x10
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPNIDEN_DISABLE_BMSK                                         0x8000
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPNIDEN_DISABLE_SHFT                                            0xf
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPIDEN_DISABLE_BMSK                                          0x4000
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPIDEN_DISABLE_SHFT                                             0xe
#define HWIO_FEATURE_CONFIG2_QC_APPS_NIDEN_DISABLE_BMSK                                           0x2000
#define HWIO_FEATURE_CONFIG2_QC_APPS_NIDEN_DISABLE_SHFT                                              0xd
#define HWIO_FEATURE_CONFIG2_QC_APPS_DBGEN_DISABLE_BMSK                                           0x1000
#define HWIO_FEATURE_CONFIG2_QC_APPS_DBGEN_DISABLE_SHFT                                              0xc
#define HWIO_FEATURE_CONFIG2_QC_SPARE1_DISABLE_BMSK                                                0x800
#define HWIO_FEATURE_CONFIG2_QC_SPARE1_DISABLE_SHFT                                                  0xb
#define HWIO_FEATURE_CONFIG2_QC_SPARE0_DISABLE_BMSK                                                0x400
#define HWIO_FEATURE_CONFIG2_QC_SPARE0_DISABLE_SHFT                                                  0xa
#define HWIO_FEATURE_CONFIG2_QC_VENUS_0_DBGEN_DISABLE_BMSK                                         0x200
#define HWIO_FEATURE_CONFIG2_QC_VENUS_0_DBGEN_DISABLE_SHFT                                           0x9
#define HWIO_FEATURE_CONFIG2_QC_RPM_DAPEN_DISABLE_BMSK                                             0x100
#define HWIO_FEATURE_CONFIG2_QC_RPM_DAPEN_DISABLE_SHFT                                               0x8
#define HWIO_FEATURE_CONFIG2_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                                         0x80
#define HWIO_FEATURE_CONFIG2_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                                          0x7
#define HWIO_FEATURE_CONFIG2_QC_RPM_DBGEN_DISABLE_BMSK                                              0x40
#define HWIO_FEATURE_CONFIG2_QC_RPM_DBGEN_DISABLE_SHFT                                               0x6
#define HWIO_FEATURE_CONFIG2_QC_WCSS_DBGEN_DISABLE_BMSK                                             0x20
#define HWIO_FEATURE_CONFIG2_QC_WCSS_DBGEN_DISABLE_SHFT                                              0x5
#define HWIO_FEATURE_CONFIG2_QC_MSS_NIDEN_DISABLE_BMSK                                              0x10
#define HWIO_FEATURE_CONFIG2_QC_MSS_NIDEN_DISABLE_SHFT                                               0x4
#define HWIO_FEATURE_CONFIG2_QC_MSS_DBGEN_DISABLE_BMSK                                               0x8
#define HWIO_FEATURE_CONFIG2_QC_MSS_DBGEN_DISABLE_SHFT                                               0x3
#define HWIO_FEATURE_CONFIG2_QDI_SPMI_DISABLE_BMSK                                                   0x4
#define HWIO_FEATURE_CONFIG2_QDI_SPMI_DISABLE_SHFT                                                   0x2
#define HWIO_FEATURE_CONFIG2_SM_BIST_DISABLE_BMSK                                                    0x2
#define HWIO_FEATURE_CONFIG2_SM_BIST_DISABLE_SHFT                                                    0x1
#define HWIO_FEATURE_CONFIG2_TIC_DISABLE_BMSK                                                        0x1
#define HWIO_FEATURE_CONFIG2_TIC_DISABLE_SHFT                                                        0x0

#define HWIO_FEATURE_CONFIG3_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006010)
#define HWIO_FEATURE_CONFIG3_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG3_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, HWIO_FEATURE_CONFIG3_RMSK)
#define HWIO_FEATURE_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, m)
#define HWIO_FEATURE_CONFIG3_SEC_TAP_ACCESS_DISABLE_BMSK                                      0xfe000000
#define HWIO_FEATURE_CONFIG3_SEC_TAP_ACCESS_DISABLE_SHFT                                            0x19
#define HWIO_FEATURE_CONFIG3_TAP_CJI_CORE_SEL_DISABLE_BMSK                                     0x1000000
#define HWIO_FEATURE_CONFIG3_TAP_CJI_CORE_SEL_DISABLE_SHFT                                          0x18
#define HWIO_FEATURE_CONFIG3_TAP_INSTR_DISABLE_BMSK                                             0xfff800
#define HWIO_FEATURE_CONFIG3_TAP_INSTR_DISABLE_SHFT                                                  0xb
#define HWIO_FEATURE_CONFIG3_SPARE1_BMSK                                                           0x400
#define HWIO_FEATURE_CONFIG3_SPARE1_SHFT                                                             0xa
#define HWIO_FEATURE_CONFIG3_MODEM_PBL_PATCH_VERSION_BMSK                                          0x3e0
#define HWIO_FEATURE_CONFIG3_MODEM_PBL_PATCH_VERSION_SHFT                                            0x5
#define HWIO_FEATURE_CONFIG3_APPS_PBL_PATCH_VERSION_BMSK                                            0x1f
#define HWIO_FEATURE_CONFIG3_APPS_PBL_PATCH_VERSION_SHFT                                             0x0

#define HWIO_FEATURE_CONFIG4_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006014)
#define HWIO_FEATURE_CONFIG4_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG4_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, HWIO_FEATURE_CONFIG4_RMSK)
#define HWIO_FEATURE_CONFIG4_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, m)
#define HWIO_FEATURE_CONFIG4_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK                            0xffffffff
#define HWIO_FEATURE_CONFIG4_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                                   0x0

#define HWIO_FEATURE_CONFIG5_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006018)
#define HWIO_FEATURE_CONFIG5_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG5_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, HWIO_FEATURE_CONFIG5_RMSK)
#define HWIO_FEATURE_CONFIG5_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, m)
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_7_BMSK                                      0x80000000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_7_SHFT                                            0x1f
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_6_BMSK                                      0x40000000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_6_SHFT                                            0x1e
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_5_BMSK                                      0x20000000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_5_SHFT                                            0x1d
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_4_BMSK                                      0x10000000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_4_SHFT                                            0x1c
#define HWIO_FEATURE_CONFIG5_GCC_DISABLE_BOOT_FSM_BMSK                                         0x8000000
#define HWIO_FEATURE_CONFIG5_GCC_DISABLE_BOOT_FSM_SHFT                                              0x1b
#define HWIO_FEATURE_CONFIG5_MODEM_PBL_PLL_CTRL_BMSK                                           0x7800000
#define HWIO_FEATURE_CONFIG5_MODEM_PBL_PLL_CTRL_SHFT                                                0x17
#define HWIO_FEATURE_CONFIG5_APPS_PBL_PLL_CTRL_BMSK                                             0x780000
#define HWIO_FEATURE_CONFIG5_APPS_PBL_PLL_CTRL_SHFT                                                 0x13
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_3_BMSK                                         0x40000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_3_SHFT                                            0x12
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_2_BMSK                                         0x20000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_2_SHFT                                            0x11
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_1_BMSK                                         0x10000
#define HWIO_FEATURE_CONFIG5_APPS_CFGCPUPRESENT_N_1_SHFT                                            0x10
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_CSR_BMSK                                              0x8000
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_CSR_SHFT                                                 0xf
#define HWIO_FEATURE_CONFIG5_APPS_AARCH64_ENABLE_BMSK                                             0x4000
#define HWIO_FEATURE_CONFIG5_APPS_AARCH64_ENABLE_SHFT                                                0xe
#define HWIO_FEATURE_CONFIG5_APPS_PBL_BOOT_SPEED_BMSK                                             0x3000
#define HWIO_FEATURE_CONFIG5_APPS_PBL_BOOT_SPEED_SHFT                                                0xc
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_ROM_BMSK                                               0x800
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_ROM_SHFT                                                 0xb
#define HWIO_FEATURE_CONFIG5_MSA_ENA_BMSK                                                          0x400
#define HWIO_FEATURE_CONFIG5_MSA_ENA_SHFT                                                            0xa
#define HWIO_FEATURE_CONFIG5_FORCE_MSA_AUTH_EN_BMSK                                                0x200
#define HWIO_FEATURE_CONFIG5_FORCE_MSA_AUTH_EN_SHFT                                                  0x9
#define HWIO_FEATURE_CONFIG5_SPARE_8_7_BMSK                                                        0x180
#define HWIO_FEATURE_CONFIG5_SPARE_8_7_SHFT                                                          0x7
#define HWIO_FEATURE_CONFIG5_MODEM_BOOT_FROM_ROM_BMSK                                               0x40
#define HWIO_FEATURE_CONFIG5_MODEM_BOOT_FROM_ROM_SHFT                                                0x6
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_DISABLE_BMSK                                            0x20
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_DISABLE_SHFT                                             0x5
#define HWIO_FEATURE_CONFIG5_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                                 0x1f
#define HWIO_FEATURE_CONFIG5_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                                  0x0

#define HWIO_OEM_CONFIG0_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000601c)
#define HWIO_OEM_CONFIG0_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG0_IN          \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, HWIO_OEM_CONFIG0_RMSK)
#define HWIO_OEM_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, m)
#define HWIO_OEM_CONFIG0_RSVD0_BMSK                                                           0xe0000000
#define HWIO_OEM_CONFIG0_RSVD0_SHFT                                                                 0x1d
#define HWIO_OEM_CONFIG0_DEBUG_DISABLE_POLICY_BMSK                                            0x10000000
#define HWIO_OEM_CONFIG0_DEBUG_DISABLE_POLICY_SHFT                                                  0x1c
#define HWIO_OEM_CONFIG0_SPARE_REG26_SECURE_BMSK                                               0x8000000
#define HWIO_OEM_CONFIG0_SPARE_REG26_SECURE_SHFT                                                    0x1b
#define HWIO_OEM_CONFIG0_SPARE_REG25_SECURE_BMSK                                               0x4000000
#define HWIO_OEM_CONFIG0_SPARE_REG25_SECURE_SHFT                                                    0x1a
#define HWIO_OEM_CONFIG0_SPARE_REG24_SECURE_BMSK                                               0x2000000
#define HWIO_OEM_CONFIG0_SPARE_REG24_SECURE_SHFT                                                    0x19
#define HWIO_OEM_CONFIG0_SPARE_REG23_SECURE_BMSK                                               0x1000000
#define HWIO_OEM_CONFIG0_SPARE_REG23_SECURE_SHFT                                                    0x18
#define HWIO_OEM_CONFIG0_SPARE_REG22_SECURE_BMSK                                                0x800000
#define HWIO_OEM_CONFIG0_SPARE_REG22_SECURE_SHFT                                                    0x17
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_BMSK                                                   0x400000
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_SHFT                                                       0x16
#define HWIO_OEM_CONFIG0_ROOT_CERT_TOTAL_NUM_BMSK                                               0x3c0000
#define HWIO_OEM_CONFIG0_ROOT_CERT_TOTAL_NUM_SHFT                                                   0x12
#define HWIO_OEM_CONFIG0_SPARE_REG19_SECURE_BMSK                                                 0x20000
#define HWIO_OEM_CONFIG0_SPARE_REG19_SECURE_SHFT                                                    0x11
#define HWIO_OEM_CONFIG0_SPARE_REG18_SECURE_BMSK                                                 0x10000
#define HWIO_OEM_CONFIG0_SPARE_REG18_SECURE_SHFT                                                    0x10
#define HWIO_OEM_CONFIG0_SPARE1_BMSK                                                              0x8000
#define HWIO_OEM_CONFIG0_SPARE1_SHFT                                                                 0xf
#define HWIO_OEM_CONFIG0_SPARE_REG21_SECURE_BMSK                                                  0x4000
#define HWIO_OEM_CONFIG0_SPARE_REG21_SECURE_SHFT                                                     0xe
#define HWIO_OEM_CONFIG0_SPARE_REG20_SECURE_BMSK                                                  0x2000
#define HWIO_OEM_CONFIG0_SPARE_REG20_SECURE_SHFT                                                     0xd
#define HWIO_OEM_CONFIG0_WDOG_EN_BMSK                                                             0x1000
#define HWIO_OEM_CONFIG0_WDOG_EN_SHFT                                                                0xc
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_BMSK                                                     0x800
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_SHFT                                                       0xb
#define HWIO_OEM_CONFIG0_ALT_SD_PORT_FOR_BOOT_BMSK                                                 0x400
#define HWIO_OEM_CONFIG0_ALT_SD_PORT_FOR_BOOT_SHFT                                                   0xa
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                                        0x200
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                                          0x9
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_BMSK                                                  0x100
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_SHFT                                                    0x8
#define HWIO_OEM_CONFIG0_FAST_BOOT_BMSK                                                             0xe0
#define HWIO_OEM_CONFIG0_FAST_BOOT_SHFT                                                              0x5
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_BMSK                                                   0x10
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_SHFT                                                    0x4
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_BMSK                                                    0x8
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_SHFT                                                    0x3
#define HWIO_OEM_CONFIG0_SPARE_BMSK                                                                  0x4
#define HWIO_OEM_CONFIG0_SPARE_SHFT                                                                  0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_BMSK                                                           0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_SHFT                                                           0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_BMSK                                                        0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_SHFT                                                        0x0

#define HWIO_OEM_CONFIG1_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006020)
#define HWIO_OEM_CONFIG1_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG1_IN          \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, HWIO_OEM_CONFIG1_RMSK)
#define HWIO_OEM_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, m)
#define HWIO_OEM_CONFIG1_SPARE1_BMSK                                                          0xf8000000
#define HWIO_OEM_CONFIG1_SPARE1_SHFT                                                                0x1b
#define HWIO_OEM_CONFIG1_SDCC5_SCM_FORCE_EFUSE_KEY_BMSK                                        0x4000000
#define HWIO_OEM_CONFIG1_SDCC5_SCM_FORCE_EFUSE_KEY_SHFT                                             0x1a
#define HWIO_OEM_CONFIG1_A5X_ISDB_DBGEN_DISABLE_BMSK                                           0x2000000
#define HWIO_OEM_CONFIG1_A5X_ISDB_DBGEN_DISABLE_SHFT                                                0x19
#define HWIO_OEM_CONFIG1_LPASS_NIDEN_DISABLE_BMSK                                              0x1000000
#define HWIO_OEM_CONFIG1_LPASS_NIDEN_DISABLE_SHFT                                                   0x18
#define HWIO_OEM_CONFIG1_LPASS_DBGEN_DISABLE_BMSK                                               0x800000
#define HWIO_OEM_CONFIG1_LPASS_DBGEN_DISABLE_SHFT                                                   0x17
#define HWIO_OEM_CONFIG1_ANTI_ROLLBACK_FEATURE_EN_BMSK                                          0x780000
#define HWIO_OEM_CONFIG1_ANTI_ROLLBACK_FEATURE_EN_SHFT                                              0x13
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_BMSK                                               0x40000
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_SHFT                                                  0x12
#define HWIO_OEM_CONFIG1_DAP_SPNIDEN_DISABLE_BMSK                                                0x20000
#define HWIO_OEM_CONFIG1_DAP_SPNIDEN_DISABLE_SHFT                                                   0x11
#define HWIO_OEM_CONFIG1_DAP_SPIDEN_DISABLE_BMSK                                                 0x10000
#define HWIO_OEM_CONFIG1_DAP_SPIDEN_DISABLE_SHFT                                                    0x10
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_BMSK                                                   0x8000
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_SHFT                                                      0xf
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_BMSK                                                   0x4000
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_SHFT                                                      0xe
#define HWIO_OEM_CONFIG1_APPS_SPNIDEN_DISABLE_BMSK                                                0x2000
#define HWIO_OEM_CONFIG1_APPS_SPNIDEN_DISABLE_SHFT                                                   0xd
#define HWIO_OEM_CONFIG1_APPS_SPIDEN_DISABLE_BMSK                                                 0x1000
#define HWIO_OEM_CONFIG1_APPS_SPIDEN_DISABLE_SHFT                                                    0xc
#define HWIO_OEM_CONFIG1_APPS_NIDEN_DISABLE_BMSK                                                   0x800
#define HWIO_OEM_CONFIG1_APPS_NIDEN_DISABLE_SHFT                                                     0xb
#define HWIO_OEM_CONFIG1_APPS_DBGEN_DISABLE_BMSK                                                   0x400
#define HWIO_OEM_CONFIG1_APPS_DBGEN_DISABLE_SHFT                                                     0xa
#define HWIO_OEM_CONFIG1_SPARE1_DISABLE_BMSK                                                       0x200
#define HWIO_OEM_CONFIG1_SPARE1_DISABLE_SHFT                                                         0x9
#define HWIO_OEM_CONFIG1_SPARE0_DISABLE_BMSK                                                       0x100
#define HWIO_OEM_CONFIG1_SPARE0_DISABLE_SHFT                                                         0x8
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_BMSK                                                 0x80
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_SHFT                                                  0x7
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_BMSK                                                     0x40
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_SHFT                                                      0x6
#define HWIO_OEM_CONFIG1_RPM_WCSS_NIDEN_DISABLE_BMSK                                                0x20
#define HWIO_OEM_CONFIG1_RPM_WCSS_NIDEN_DISABLE_SHFT                                                 0x5
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_BMSK                                                     0x10
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_SHFT                                                      0x4
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_BMSK                                                     0x8
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_SHFT                                                     0x3
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_BMSK                                                      0x4
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_SHFT                                                      0x2
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_BMSK                                                      0x2
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_SHFT                                                      0x1
#define HWIO_OEM_CONFIG1_ALL_DEBUG_DISABLE_BMSK                                                      0x1
#define HWIO_OEM_CONFIG1_ALL_DEBUG_DISABLE_SHFT                                                      0x0

#define HWIO_OEM_CONFIG2_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006024)
#define HWIO_OEM_CONFIG2_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG2_IN          \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, HWIO_OEM_CONFIG2_RMSK)
#define HWIO_OEM_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, m)
#define HWIO_OEM_CONFIG2_OEM_PRODUCT_ID_BMSK                                                  0xffff0000
#define HWIO_OEM_CONFIG2_OEM_PRODUCT_ID_SHFT                                                        0x10
#define HWIO_OEM_CONFIG2_OEM_HW_ID_BMSK                                                           0xffff
#define HWIO_OEM_CONFIG2_OEM_HW_ID_SHFT                                                              0x0

#define HWIO_OEM_CONFIG3_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006028)
#define HWIO_OEM_CONFIG3_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG3_IN          \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, HWIO_OEM_CONFIG3_RMSK)
#define HWIO_OEM_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, m)
#define HWIO_OEM_CONFIG3_PERIPH_VID_BMSK                                                      0xffff0000
#define HWIO_OEM_CONFIG3_PERIPH_VID_SHFT                                                            0x10
#define HWIO_OEM_CONFIG3_PERIPH_PID_BMSK                                                          0xffff
#define HWIO_OEM_CONFIG3_PERIPH_PID_SHFT                                                             0x0

#define HWIO_BOOT_CONFIG_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000602c)
#define HWIO_BOOT_CONFIG_RMSK                                                                      0x3ff
#define HWIO_BOOT_CONFIG_IN          \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, HWIO_BOOT_CONFIG_RMSK)
#define HWIO_BOOT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, m)
#define HWIO_BOOT_CONFIG_APPS_AARCH64_ENABLE_BMSK                                                  0x200
#define HWIO_BOOT_CONFIG_APPS_AARCH64_ENABLE_SHFT                                                    0x9
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_BMSK                                                    0x100
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_SHFT                                                      0x8
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_BMSK                                                   0xc0
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_SHFT                                                    0x6
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_BMSK                                                    0x20
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_SHFT                                                     0x5
#define HWIO_BOOT_CONFIG_RSVD_BMSK                                                                  0x10
#define HWIO_BOOT_CONFIG_RSVD_SHFT                                                                   0x4
#define HWIO_BOOT_CONFIG_FAST_BOOT_BMSK                                                              0xe
#define HWIO_BOOT_CONFIG_FAST_BOOT_SHFT                                                              0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_BMSK                                                                0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_SHFT                                                                0x0

#define HWIO_SECURE_BOOTn_ADDR(n)                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006030 + 0x4 * (n))
#define HWIO_SECURE_BOOTn_RMSK                                                                     0x1ff
#define HWIO_SECURE_BOOTn_MAXn                                                                         7
#define HWIO_SECURE_BOOTn_INI(n)        \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), HWIO_SECURE_BOOTn_RMSK)
#define HWIO_SECURE_BOOTn_INMI(n,mask)    \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), mask)
#define HWIO_SECURE_BOOTn_FUSE_SRC_BMSK                                                            0x100
#define HWIO_SECURE_BOOTn_FUSE_SRC_SHFT                                                              0x8
#define HWIO_SECURE_BOOTn_RSVD_7_BMSK                                                               0x80
#define HWIO_SECURE_BOOTn_RSVD_7_SHFT                                                                0x7
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_BMSK                                                       0x40
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_SHFT                                                        0x6
#define HWIO_SECURE_BOOTn_AUTH_EN_BMSK                                                              0x20
#define HWIO_SECURE_BOOTn_AUTH_EN_SHFT                                                               0x5
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_BMSK                                                      0x10
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_SHFT                                                       0x4
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_BMSK                                                     0xf
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_SHFT                                                     0x0

#define HWIO_OVERRIDE_0_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006058)
#define HWIO_OVERRIDE_0_RMSK                                                                  0x1fffffff
#define HWIO_OVERRIDE_0_IN          \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, HWIO_OVERRIDE_0_RMSK)
#define HWIO_OVERRIDE_0_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, m)
#define HWIO_OVERRIDE_0_OUT(v)      \
        out_dword(HWIO_OVERRIDE_0_ADDR,v)
#define HWIO_OVERRIDE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_0_ADDR,m,v,HWIO_OVERRIDE_0_IN)
#define HWIO_OVERRIDE_0_TX_DISABLE_BMSK                                                       0x10000000
#define HWIO_OVERRIDE_0_TX_DISABLE_SHFT                                                             0x1c
#define HWIO_OVERRIDE_0_RSVD_27_2_BMSK                                                         0xffffffc
#define HWIO_OVERRIDE_0_RSVD_27_2_SHFT                                                               0x2
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_BMSK                                                     0x2
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_SHFT                                                     0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_BMSK                                               0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_SHFT                                               0x0

#define HWIO_OVERRIDE_1_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000605c)
#define HWIO_OVERRIDE_1_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_1_IN          \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, HWIO_OVERRIDE_1_RMSK)
#define HWIO_OVERRIDE_1_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, m)
#define HWIO_OVERRIDE_1_OUT(v)      \
        out_dword(HWIO_OVERRIDE_1_ADDR,v)
#define HWIO_OVERRIDE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_1_ADDR,m,v,HWIO_OVERRIDE_1_IN)
#define HWIO_OVERRIDE_1_OVERRIDE_1_BMSK                                                       0xffffffff
#define HWIO_OVERRIDE_1_OVERRIDE_1_SHFT                                                              0x0

#define HWIO_OVERRIDE_2_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006060)
#define HWIO_OVERRIDE_2_RMSK                                                                   0x1fffc00
#define HWIO_OVERRIDE_2_IN          \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, HWIO_OVERRIDE_2_RMSK)
#define HWIO_OVERRIDE_2_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, m)
#define HWIO_OVERRIDE_2_OUT(v)      \
        out_dword(HWIO_OVERRIDE_2_ADDR,v)
#define HWIO_OVERRIDE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_2_ADDR,m,v,HWIO_OVERRIDE_2_IN)
#define HWIO_OVERRIDE_2_OVRID_A5X_ISDB_DBGEN_DISABLE_BMSK                                      0x1000000
#define HWIO_OVERRIDE_2_OVRID_A5X_ISDB_DBGEN_DISABLE_SHFT                                           0x18
#define HWIO_OVERRIDE_2_OVRID_LPASS_NIDEN_DISABLE_BMSK                                          0x800000
#define HWIO_OVERRIDE_2_OVRID_LPASS_NIDEN_DISABLE_SHFT                                              0x17
#define HWIO_OVERRIDE_2_OVRID_LPASS_DBGEN_DISABLE_BMSK                                          0x400000
#define HWIO_OVERRIDE_2_OVRID_LPASS_DBGEN_DISABLE_SHFT                                              0x16
#define HWIO_OVERRIDE_2_OVRID_DAP_DEVICEEN_DISABLE_BMSK                                         0x200000
#define HWIO_OVERRIDE_2_OVRID_DAP_DEVICEEN_DISABLE_SHFT                                             0x15
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_BMSK                                            0x100000
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_SHFT                                                0x14
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_BMSK                                             0x80000
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_SHFT                                                0x13
#define HWIO_OVERRIDE_2_OVRID_APPS_NIDEN_DISABLE_BMSK                                            0x40000
#define HWIO_OVERRIDE_2_OVRID_APPS_NIDEN_DISABLE_SHFT                                               0x12
#define HWIO_OVERRIDE_2_OVRID_APPS_DBGEN_DISABLE_BMSK                                            0x20000
#define HWIO_OVERRIDE_2_OVRID_APPS_DBGEN_DISABLE_SHFT                                               0x11
#define HWIO_OVERRIDE_2_OVRID_SPARE1_DISABLE_BMSK                                                0x10000
#define HWIO_OVERRIDE_2_OVRID_SPARE1_DISABLE_SHFT                                                   0x10
#define HWIO_OVERRIDE_2_OVRID_SPARE0_DISABLE_BMSK                                                 0x8000
#define HWIO_OVERRIDE_2_OVRID_SPARE0_DISABLE_SHFT                                                    0xf
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_BMSK                                          0x4000
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_SHFT                                             0xe
#define HWIO_OVERRIDE_2_OVRID_RPM_DAPEN_DISABLE_BMSK                                              0x2000
#define HWIO_OVERRIDE_2_OVRID_RPM_DAPEN_DISABLE_SHFT                                                 0xd
#define HWIO_OVERRIDE_2_OVRID_RPM_WCSS_NIDEN_DISABLE_BMSK                                         0x1000
#define HWIO_OVERRIDE_2_OVRID_RPM_WCSS_NIDEN_DISABLE_SHFT                                            0xc
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_BMSK                                               0x800
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_SHFT                                                 0xb
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_BMSK                                              0x400
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_SHFT                                                0xa

#define HWIO_OVERRIDE_3_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006064)
#define HWIO_OVERRIDE_3_RMSK                                                                        0x1f
#define HWIO_OVERRIDE_3_IN          \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, HWIO_OVERRIDE_3_RMSK)
#define HWIO_OVERRIDE_3_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, m)
#define HWIO_OVERRIDE_3_OUT(v)      \
        out_dword(HWIO_OVERRIDE_3_ADDR,v)
#define HWIO_OVERRIDE_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_3_ADDR,m,v,HWIO_OVERRIDE_3_IN)
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_BMSK                                              0x10
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_SHFT                                               0x4
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_BMSK                                                0x8
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_SHFT                                                0x3
#define HWIO_OVERRIDE_3_OVRID_APPS_SPNIDEN_DISABLE_BMSK                                              0x4
#define HWIO_OVERRIDE_3_OVRID_APPS_SPNIDEN_DISABLE_SHFT                                              0x2
#define HWIO_OVERRIDE_3_OVRID_APPS_SPIDEN_DISABLE_BMSK                                               0x2
#define HWIO_OVERRIDE_3_OVRID_APPS_SPIDEN_DISABLE_SHFT                                               0x1
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_BMSK                                                  0x1
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_SHFT                                                  0x0

#define HWIO_OVERRIDE_4_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006068)
#define HWIO_OVERRIDE_4_RMSK                                                                         0x3
#define HWIO_OVERRIDE_4_IN          \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, HWIO_OVERRIDE_4_RMSK)
#define HWIO_OVERRIDE_4_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, m)
#define HWIO_OVERRIDE_4_OUT(v)      \
        out_dword(HWIO_OVERRIDE_4_ADDR,v)
#define HWIO_OVERRIDE_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_4_ADDR,m,v,HWIO_OVERRIDE_4_IN)
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_BMSK                                                 0x2
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_SHFT                                                 0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_BMSK                                                 0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_SHFT                                                 0x0

#define HWIO_CAPT_SEC_GPIO_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000606c)
#define HWIO_CAPT_SEC_GPIO_RMSK                                                                  0x1ffff
#define HWIO_CAPT_SEC_GPIO_IN          \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, HWIO_CAPT_SEC_GPIO_RMSK)
#define HWIO_CAPT_SEC_GPIO_INM(m)      \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, m)
#define HWIO_CAPT_SEC_GPIO_OUT(v)      \
        out_dword(HWIO_CAPT_SEC_GPIO_ADDR,v)
#define HWIO_CAPT_SEC_GPIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CAPT_SEC_GPIO_ADDR,m,v,HWIO_CAPT_SEC_GPIO_IN)
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_BMSK                                              0x10000
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_SHFT                                                 0x10
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_BMSK                                               0x8000
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_SHFT                                                  0xf
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FORCE_MSA_AUTH_EN_BMSK                                0x4000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FORCE_MSA_AUTH_EN_SHFT                                   0xe
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_AUTH_EN_BMSK                                       0x2000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_AUTH_EN_SHFT                                          0xd
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_PK_HASH_IN_FUSE_BMSK                               0x1000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_PK_HASH_IN_FUSE_SHFT                                  0xc
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_AUTH_EN_BMSK                                       0x800
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_AUTH_EN_SHFT                                         0xb
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_PK_HASH_IN_FUSE_BMSK                               0x400
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_PK_HASH_IN_FUSE_SHFT                                 0xa
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_ALL_USE_SERIAL_NUM_BMSK                                0x200
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_ALL_USE_SERIAL_NUM_SHFT                                  0x9
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_PK_HASH_INDEX_SRC_BMSK                                 0x100
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_PK_HASH_INDEX_SRC_SHFT                                   0x8
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_PBL_BOOT_SPEED_BMSK                                0xc0
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_PBL_BOOT_SPEED_SHFT                                 0x6
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_BOOT_FROM_ROM_BMSK                                 0x20
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_BOOT_FROM_ROM_SHFT                                  0x5
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_AARCH64_ENABLE_BMSK                                0x10
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_AARCH64_ENABLE_SHFT                                 0x4
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FAST_BOOT_BMSK                                           0xe
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FAST_BOOT_SHFT                                           0x1
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_WDOG_DISABLE_BMSK                                        0x1
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_WDOG_DISABLE_SHFT                                        0x0

#define HWIO_APP_PROC_CFG_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006070)
#define HWIO_APP_PROC_CFG_RMSK                                                                 0x1ffffff
#define HWIO_APP_PROC_CFG_IN          \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, HWIO_APP_PROC_CFG_RMSK)
#define HWIO_APP_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, m)
#define HWIO_APP_PROC_CFG_OUT(v)      \
        out_dword(HWIO_APP_PROC_CFG_ADDR,v)
#define HWIO_APP_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APP_PROC_CFG_ADDR,m,v,HWIO_APP_PROC_CFG_IN)
#define HWIO_APP_PROC_CFG_LPASS_DBG_NIDEN_BMSK                                                 0x1000000
#define HWIO_APP_PROC_CFG_LPASS_DBG_NIDEN_SHFT                                                      0x18
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_BMSK                                                   0x800000
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_SHFT                                                       0x17
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_BMSK                                                    0x400000
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_SHFT                                                        0x16
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_BMSK                                                    0x200000
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_SHFT                                                        0x15
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_BMSK                                                  0x100000
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_SHFT                                                      0x14
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC3_BMSK                                                    0xf0000
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC3_SHFT                                                       0x10
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC2_BMSK                                                     0xf000
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC2_SHFT                                                        0xc
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC1_BMSK                                                      0xf00
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC1_SHFT                                                        0x8
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC0_BMSK                                                       0xf0
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC0_SHFT                                                        0x4
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_BMSK                                                        0x8
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_SHFT                                                        0x3
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_BMSK                                                      0x4
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_SHFT                                                      0x2
#define HWIO_APP_PROC_CFG_APPS_CP15_DISABLE_BMSK                                                     0x2
#define HWIO_APP_PROC_CFG_APPS_CP15_DISABLE_SHFT                                                     0x1
#define HWIO_APP_PROC_CFG_APPS_CFG_NMFI_BMSK                                                         0x1
#define HWIO_APP_PROC_CFG_APPS_CFG_NMFI_SHFT                                                         0x0

#define HWIO_MSS_PROC_CFG_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006074)
#define HWIO_MSS_PROC_CFG_RMSK                                                                       0x1
#define HWIO_MSS_PROC_CFG_IN          \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, HWIO_MSS_PROC_CFG_RMSK)
#define HWIO_MSS_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, m)
#define HWIO_MSS_PROC_CFG_OUT(v)      \
        out_dword(HWIO_MSS_PROC_CFG_ADDR,v)
#define HWIO_MSS_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_PROC_CFG_ADDR,m,v,HWIO_MSS_PROC_CFG_IN)
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_BMSK                                                         0x1
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_SHFT                                                         0x0

#define HWIO_QFPROM_CLK_CTL_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006078)
#define HWIO_QFPROM_CLK_CTL_RMSK                                                                     0x1
#define HWIO_QFPROM_CLK_CTL_IN          \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, HWIO_QFPROM_CLK_CTL_RMSK)
#define HWIO_QFPROM_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, m)
#define HWIO_QFPROM_CLK_CTL_OUT(v)      \
        out_dword(HWIO_QFPROM_CLK_CTL_ADDR,v)
#define HWIO_QFPROM_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CLK_CTL_ADDR,m,v,HWIO_QFPROM_CLK_CTL_IN)
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_BMSK                                                            0x1
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_SHFT                                                            0x0

#define HWIO_JTAG_ID_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000607c)
#define HWIO_JTAG_ID_RMSK                                                                     0xffffffff
#define HWIO_JTAG_ID_IN          \
        in_dword_masked(HWIO_JTAG_ID_ADDR, HWIO_JTAG_ID_RMSK)
#define HWIO_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_JTAG_ID_ADDR, m)
#define HWIO_JTAG_ID_JTAG_ID_BMSK                                                             0xffffffff
#define HWIO_JTAG_ID_JTAG_ID_SHFT                                                                    0x0

#define HWIO_OEM_ID_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006080)
#define HWIO_OEM_ID_RMSK                                                                      0xffffffff
#define HWIO_OEM_ID_IN          \
        in_dword_masked(HWIO_OEM_ID_ADDR, HWIO_OEM_ID_RMSK)
#define HWIO_OEM_ID_INM(m)      \
        in_dword_masked(HWIO_OEM_ID_ADDR, m)
#define HWIO_OEM_ID_OEM_ID_BMSK                                                               0xffff0000
#define HWIO_OEM_ID_OEM_ID_SHFT                                                                     0x10
#define HWIO_OEM_ID_OEM_PRODUCT_ID_BMSK                                                           0xffff
#define HWIO_OEM_ID_OEM_PRODUCT_ID_SHFT                                                              0x0

#define HWIO_TEST_BUS_SEL_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006084)
#define HWIO_TEST_BUS_SEL_RMSK                                                                       0xf
#define HWIO_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, HWIO_TEST_BUS_SEL_RMSK)
#define HWIO_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, m)
#define HWIO_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TEST_BUS_SEL_ADDR,v)
#define HWIO_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TEST_BUS_SEL_ADDR,m,v,HWIO_TEST_BUS_SEL_IN)
#define HWIO_TEST_BUS_SEL_TEST_EN_BMSK                                                               0x8
#define HWIO_TEST_BUS_SEL_TEST_EN_SHFT                                                               0x3
#define HWIO_TEST_BUS_SEL_TEST_SELECT_BMSK                                                           0x7
#define HWIO_TEST_BUS_SEL_TEST_SELECT_SHFT                                                           0x0

#define HWIO_SPDM_DYN_SECURE_MODE_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006088)
#define HWIO_SPDM_DYN_SECURE_MODE_RMSK                                                               0x1
#define HWIO_SPDM_DYN_SECURE_MODE_IN          \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, HWIO_SPDM_DYN_SECURE_MODE_RMSK)
#define HWIO_SPDM_DYN_SECURE_MODE_INM(m)      \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, m)
#define HWIO_SPDM_DYN_SECURE_MODE_OUT(v)      \
        out_dword(HWIO_SPDM_DYN_SECURE_MODE_ADDR,v)
#define HWIO_SPDM_DYN_SECURE_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_SPDM_DYN_SECURE_MODE_ADDR,m,v,HWIO_SPDM_DYN_SECURE_MODE_IN)
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_BMSK                                                   0x1
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_SHFT                                                   0x0

#define HWIO_OVERRIDE_5_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000608c)
#define HWIO_OVERRIDE_5_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_5_IN          \
        in_dword_masked(HWIO_OVERRIDE_5_ADDR, HWIO_OVERRIDE_5_RMSK)
#define HWIO_OVERRIDE_5_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_5_ADDR, m)
#define HWIO_OVERRIDE_5_OUT(v)      \
        out_dword(HWIO_OVERRIDE_5_ADDR,v)
#define HWIO_OVERRIDE_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_5_ADDR,m,v,HWIO_OVERRIDE_5_IN)
#define HWIO_OVERRIDE_5_RSVD_31_0_BMSK                                                        0xffffffff
#define HWIO_OVERRIDE_5_RSVD_31_0_SHFT                                                               0x0

#define HWIO_OVERRIDE_6_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006090)
#define HWIO_OVERRIDE_6_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_6_IN          \
        in_dword_masked(HWIO_OVERRIDE_6_ADDR, HWIO_OVERRIDE_6_RMSK)
#define HWIO_OVERRIDE_6_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_6_ADDR, m)
#define HWIO_OVERRIDE_6_OUT(v)      \
        out_dword(HWIO_OVERRIDE_6_ADDR,v)
#define HWIO_OVERRIDE_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_6_ADDR,m,v,HWIO_OVERRIDE_6_IN)
#define HWIO_OVERRIDE_6_RSVD_31_0_BMSK                                                        0xffffffff
#define HWIO_OVERRIDE_6_RSVD_31_0_SHFT                                                               0x0

#define HWIO_PTE0_ADDR                                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060a0)
#define HWIO_PTE0_RMSK                                                                        0xffffffff
#define HWIO_PTE0_IN          \
        in_dword_masked(HWIO_PTE0_ADDR, HWIO_PTE0_RMSK)
#define HWIO_PTE0_INM(m)      \
        in_dword_masked(HWIO_PTE0_ADDR, m)
#define HWIO_PTE0_SPARE0_BMSK                                                                 0xe0000000
#define HWIO_PTE0_SPARE0_SHFT                                                                       0x1d
#define HWIO_PTE0_MACCHIATO_EN_BMSK                                                           0x10000000
#define HWIO_PTE0_MACCHIATO_EN_SHFT                                                                 0x1c
#define HWIO_PTE0_FEATURE_ID_BMSK                                                              0xff00000
#define HWIO_PTE0_FEATURE_ID_SHFT                                                                   0x14
#define HWIO_PTE0_JTAG_ID_BMSK                                                                   0xfffff
#define HWIO_PTE0_JTAG_ID_SHFT                                                                       0x0

#define HWIO_PTE1_ADDR                                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060a4)
#define HWIO_PTE1_RMSK                                                                        0xffffffff
#define HWIO_PTE1_IN          \
        in_dword_masked(HWIO_PTE1_ADDR, HWIO_PTE1_RMSK)
#define HWIO_PTE1_INM(m)      \
        in_dword_masked(HWIO_PTE1_ADDR, m)
#define HWIO_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_BMSK                                         0xfc000000
#define HWIO_PTE1_IDDQ_MSS_ACTIVE_MEAS_FUSE_BITS_SHFT                                               0x1a
#define HWIO_PTE1_PROCESS_NODE_ID_BMSK                                                         0x2000000
#define HWIO_PTE1_PROCESS_NODE_ID_SHFT                                                              0x19
#define HWIO_PTE1_SPARE2_BMSK                                                                  0x1800000
#define HWIO_PTE1_SPARE2_SHFT                                                                       0x17
#define HWIO_PTE1_BONE_PILE_BMSK                                                                0x400000
#define HWIO_PTE1_BONE_PILE_SHFT                                                                    0x16
#define HWIO_PTE1_A53_ACC_SETTINGS_ID_BMSK                                                      0x300000
#define HWIO_PTE1_A53_ACC_SETTINGS_ID_SHFT                                                          0x14
#define HWIO_PTE1_SPARE1_BMSK                                                                    0xfe000
#define HWIO_PTE1_SPARE1_SHFT                                                                        0xd
#define HWIO_PTE1_WAFER_BIN_BMSK                                                                  0x1c00
#define HWIO_PTE1_WAFER_BIN_SHFT                                                                     0xa
#define HWIO_PTE1_METAL_REV_BMSK                                                                   0x300
#define HWIO_PTE1_METAL_REV_SHFT                                                                     0x8
#define HWIO_PTE1_SPARE0_BMSK                                                                       0xff
#define HWIO_PTE1_SPARE0_SHFT                                                                        0x0

#define HWIO_SERIAL_NUM_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060a8)
#define HWIO_SERIAL_NUM_RMSK                                                                  0xffffffff
#define HWIO_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_SERIAL_NUM_ADDR, HWIO_SERIAL_NUM_RMSK)
#define HWIO_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_SERIAL_NUM_ADDR, m)
#define HWIO_SERIAL_NUM_SERIAL_NUM_BMSK                                                       0xffffffff
#define HWIO_SERIAL_NUM_SERIAL_NUM_SHFT                                                              0x0

#define HWIO_PTE2_ADDR                                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060ac)
#define HWIO_PTE2_RMSK                                                                        0xffffffff
#define HWIO_PTE2_IN          \
        in_dword_masked(HWIO_PTE2_ADDR, HWIO_PTE2_RMSK)
#define HWIO_PTE2_INM(m)      \
        in_dword_masked(HWIO_PTE2_ADDR, m)
#define HWIO_PTE2_WAFER_ID_BMSK                                                               0xf8000000
#define HWIO_PTE2_WAFER_ID_SHFT                                                                     0x1b
#define HWIO_PTE2_DIE_X_BMSK                                                                   0x7f80000
#define HWIO_PTE2_DIE_X_SHFT                                                                        0x13
#define HWIO_PTE2_DIE_Y_BMSK                                                                     0x7f800
#define HWIO_PTE2_DIE_Y_SHFT                                                                         0xb
#define HWIO_PTE2_FOUNDRY_ID_BMSK                                                                  0x700
#define HWIO_PTE2_FOUNDRY_ID_SHFT                                                                    0x8
#define HWIO_PTE2_LOGIC_RETENTION_BMSK                                                              0xe0
#define HWIO_PTE2_LOGIC_RETENTION_SHFT                                                               0x5
#define HWIO_PTE2_SPEED_BIN_BMSK                                                                    0x1c
#define HWIO_PTE2_SPEED_BIN_SHFT                                                                     0x2
#define HWIO_PTE2_MX_RET_BIN_BMSK                                                                    0x3
#define HWIO_PTE2_MX_RET_BIN_SHFT                                                                    0x0

#define HWIO_ANTI_ROLLBACK_1_0_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060b0)
#define HWIO_ANTI_ROLLBACK_1_0_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_1_0_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_1_0_ADDR, HWIO_ANTI_ROLLBACK_1_0_RMSK)
#define HWIO_ANTI_ROLLBACK_1_0_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_1_0_ADDR, m)
#define HWIO_ANTI_ROLLBACK_1_0_PIL_SUBSYSTEM0_BMSK                                            0xfc000000
#define HWIO_ANTI_ROLLBACK_1_0_PIL_SUBSYSTEM0_SHFT                                                  0x1a
#define HWIO_ANTI_ROLLBACK_1_0_TZ_BMSK                                                         0x3fff000
#define HWIO_ANTI_ROLLBACK_1_0_TZ_SHFT                                                               0xc
#define HWIO_ANTI_ROLLBACK_1_0_SBL1_BMSK                                                           0xffe
#define HWIO_ANTI_ROLLBACK_1_0_SBL1_SHFT                                                             0x1
#define HWIO_ANTI_ROLLBACK_1_0_RPMB_KEY_PROVISIONED_BMSK                                             0x1
#define HWIO_ANTI_ROLLBACK_1_0_RPMB_KEY_PROVISIONED_SHFT                                             0x0

#define HWIO_ANTI_ROLLBACK_1_1_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060b4)
#define HWIO_ANTI_ROLLBACK_1_1_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_1_1_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_1_1_ADDR, HWIO_ANTI_ROLLBACK_1_1_RMSK)
#define HWIO_ANTI_ROLLBACK_1_1_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_1_1_ADDR, m)
#define HWIO_ANTI_ROLLBACK_1_1_APPSBL0_BMSK                                                   0xfffc0000
#define HWIO_ANTI_ROLLBACK_1_1_APPSBL0_SHFT                                                         0x12
#define HWIO_ANTI_ROLLBACK_1_1_PIL_SUBSYSTEM1_BMSK                                               0x3ffff
#define HWIO_ANTI_ROLLBACK_1_1_PIL_SUBSYSTEM1_SHFT                                                   0x0

#define HWIO_ANTI_ROLLBACK_2_0_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060b8)
#define HWIO_ANTI_ROLLBACK_2_0_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_2_0_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_2_0_ADDR, HWIO_ANTI_ROLLBACK_2_0_RMSK)
#define HWIO_ANTI_ROLLBACK_2_0_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_2_0_ADDR, m)
#define HWIO_ANTI_ROLLBACK_2_0_APPSBL1_BMSK                                                   0xffffffff
#define HWIO_ANTI_ROLLBACK_2_0_APPSBL1_SHFT                                                          0x0

#define HWIO_ANTI_ROLLBACK_2_1_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060bc)
#define HWIO_ANTI_ROLLBACK_2_1_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_2_1_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_2_1_ADDR, HWIO_ANTI_ROLLBACK_2_1_RMSK)
#define HWIO_ANTI_ROLLBACK_2_1_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_2_1_ADDR, m)
#define HWIO_ANTI_ROLLBACK_2_1_ROOT_CERT_PK_HASH_INDEX_BMSK                                   0xff000000
#define HWIO_ANTI_ROLLBACK_2_1_ROOT_CERT_PK_HASH_INDEX_SHFT                                         0x18
#define HWIO_ANTI_ROLLBACK_2_1_HYPERVISOR_BMSK                                                  0xfff000
#define HWIO_ANTI_ROLLBACK_2_1_HYPERVISOR_SHFT                                                       0xc
#define HWIO_ANTI_ROLLBACK_2_1_RPM_BMSK                                                            0xff0
#define HWIO_ANTI_ROLLBACK_2_1_RPM_SHFT                                                              0x4
#define HWIO_ANTI_ROLLBACK_2_1_APPSBL2_BMSK                                                          0xf
#define HWIO_ANTI_ROLLBACK_2_1_APPSBL2_SHFT                                                          0x0

#define HWIO_ANTI_ROLLBACK_3_0_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c0)
#define HWIO_ANTI_ROLLBACK_3_0_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_3_0_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_3_0_ADDR, HWIO_ANTI_ROLLBACK_3_0_RMSK)
#define HWIO_ANTI_ROLLBACK_3_0_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_3_0_ADDR, m)
#define HWIO_ANTI_ROLLBACK_3_0_MSS_BMSK                                                       0xffff0000
#define HWIO_ANTI_ROLLBACK_3_0_MSS_SHFT                                                             0x10
#define HWIO_ANTI_ROLLBACK_3_0_MBA_BMSK                                                           0xffff
#define HWIO_ANTI_ROLLBACK_3_0_MBA_SHFT                                                              0x0

#define HWIO_ANTI_ROLLBACK_3_1_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c4)
#define HWIO_ANTI_ROLLBACK_3_1_RMSK                                                           0xffffffff
#define HWIO_ANTI_ROLLBACK_3_1_IN          \
        in_dword_masked(HWIO_ANTI_ROLLBACK_3_1_ADDR, HWIO_ANTI_ROLLBACK_3_1_RMSK)
#define HWIO_ANTI_ROLLBACK_3_1_INM(m)      \
        in_dword_masked(HWIO_ANTI_ROLLBACK_3_1_ADDR, m)
#define HWIO_ANTI_ROLLBACK_3_1_SPARE0_BMSK                                                    0xffffff00
#define HWIO_ANTI_ROLLBACK_3_1_SPARE0_SHFT                                                           0x8
#define HWIO_ANTI_ROLLBACK_3_1_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                                   0xff
#define HWIO_ANTI_ROLLBACK_3_1_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                                    0x0

#define HWIO_PK_HASH_0_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c8)
#define HWIO_PK_HASH_0_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_0_IN          \
        in_dword_masked(HWIO_PK_HASH_0_ADDR, HWIO_PK_HASH_0_RMSK)
#define HWIO_PK_HASH_0_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_0_ADDR, m)
#define HWIO_PK_HASH_0_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_0_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_1_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060cc)
#define HWIO_PK_HASH_1_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_1_IN          \
        in_dword_masked(HWIO_PK_HASH_1_ADDR, HWIO_PK_HASH_1_RMSK)
#define HWIO_PK_HASH_1_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_1_ADDR, m)
#define HWIO_PK_HASH_1_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_1_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_2_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d0)
#define HWIO_PK_HASH_2_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_2_IN          \
        in_dword_masked(HWIO_PK_HASH_2_ADDR, HWIO_PK_HASH_2_RMSK)
#define HWIO_PK_HASH_2_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_2_ADDR, m)
#define HWIO_PK_HASH_2_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_2_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_3_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d4)
#define HWIO_PK_HASH_3_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_3_IN          \
        in_dword_masked(HWIO_PK_HASH_3_ADDR, HWIO_PK_HASH_3_RMSK)
#define HWIO_PK_HASH_3_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_3_ADDR, m)
#define HWIO_PK_HASH_3_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_3_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_4_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d8)
#define HWIO_PK_HASH_4_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_4_IN          \
        in_dword_masked(HWIO_PK_HASH_4_ADDR, HWIO_PK_HASH_4_RMSK)
#define HWIO_PK_HASH_4_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_4_ADDR, m)
#define HWIO_PK_HASH_4_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_4_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_5_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060dc)
#define HWIO_PK_HASH_5_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_5_IN          \
        in_dword_masked(HWIO_PK_HASH_5_ADDR, HWIO_PK_HASH_5_RMSK)
#define HWIO_PK_HASH_5_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_5_ADDR, m)
#define HWIO_PK_HASH_5_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_5_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_6_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060e0)
#define HWIO_PK_HASH_6_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_6_IN          \
        in_dword_masked(HWIO_PK_HASH_6_ADDR, HWIO_PK_HASH_6_RMSK)
#define HWIO_PK_HASH_6_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_6_ADDR, m)
#define HWIO_PK_HASH_6_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_6_HASH_DATA0_SHFT                                                               0x0

#define HWIO_PK_HASH_7_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060e4)
#define HWIO_PK_HASH_7_RMSK                                                                   0xffffffff
#define HWIO_PK_HASH_7_IN          \
        in_dword_masked(HWIO_PK_HASH_7_ADDR, HWIO_PK_HASH_7_RMSK)
#define HWIO_PK_HASH_7_INM(m)      \
        in_dword_masked(HWIO_PK_HASH_7_ADDR, m)
#define HWIO_PK_HASH_7_HASH_DATA0_BMSK                                                        0xffffffff
#define HWIO_PK_HASH_7_HASH_DATA0_SHFT                                                               0x0

#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006100)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_RMSK                                            0xffffffff
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_IN          \
        in_dword_masked(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_ADDR, HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_RMSK)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_INM(m)      \
        in_dword_masked(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_ADDR, m)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_OUT(v)      \
        out_dword(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_ADDR,v)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_ADDR,m,v,HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_IN)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION31_STICKY_BIT_BMSK                        0x80000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION31_STICKY_BIT_SHFT                              0x1f
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION30_STICKY_BIT_BMSK                        0x40000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION30_STICKY_BIT_SHFT                              0x1e
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION29_STICKY_BIT_BMSK                        0x20000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION29_STICKY_BIT_SHFT                              0x1d
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION28_STICKY_BIT_BMSK                        0x10000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION28_STICKY_BIT_SHFT                              0x1c
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION27_STICKY_BIT_BMSK                         0x8000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION27_STICKY_BIT_SHFT                              0x1b
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION26_STICKY_BIT_BMSK                         0x4000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION26_STICKY_BIT_SHFT                              0x1a
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION25_STICKY_BIT_BMSK                         0x2000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION25_STICKY_BIT_SHFT                              0x19
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION24_STICKY_BIT_BMSK                         0x1000000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION24_STICKY_BIT_SHFT                              0x18
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION23_STICKY_BIT_BMSK                          0x800000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION23_STICKY_BIT_SHFT                              0x17
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION22_STICKY_BIT_BMSK                          0x400000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION22_STICKY_BIT_SHFT                              0x16
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION21_STICKY_BIT_BMSK                          0x200000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION21_STICKY_BIT_SHFT                              0x15
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION20_STICKY_BIT_BMSK                          0x100000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION20_STICKY_BIT_SHFT                              0x14
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION19_STICKY_BIT_BMSK                           0x80000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION19_STICKY_BIT_SHFT                              0x13
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION18_STICKY_BIT_BMSK                           0x40000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION18_STICKY_BIT_SHFT                              0x12
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION17_STICKY_BIT_BMSK                           0x20000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION17_STICKY_BIT_SHFT                              0x11
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION16_STICKY_BIT_BMSK                           0x10000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION16_STICKY_BIT_SHFT                              0x10
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION15_STICKY_BIT_BMSK                            0x8000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION15_STICKY_BIT_SHFT                               0xf
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION14_STICKY_BIT_BMSK                            0x4000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION14_STICKY_BIT_SHFT                               0xe
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION13_STICKY_BIT_BMSK                            0x2000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION13_STICKY_BIT_SHFT                               0xd
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION12_STICKY_BIT_BMSK                            0x1000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION12_STICKY_BIT_SHFT                               0xc
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION11_STICKY_BIT_BMSK                             0x800
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION11_STICKY_BIT_SHFT                               0xb
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION10_STICKY_BIT_BMSK                             0x400
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION10_STICKY_BIT_SHFT                               0xa
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION9_STICKY_BIT_BMSK                              0x200
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION9_STICKY_BIT_SHFT                                0x9
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION8_STICKY_BIT_BMSK                              0x100
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION8_STICKY_BIT_SHFT                                0x8
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION7_STICKY_BIT_BMSK                               0x80
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION7_STICKY_BIT_SHFT                                0x7
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION6_STICKY_BIT_BMSK                               0x40
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION6_STICKY_BIT_SHFT                                0x6
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION5_STICKY_BIT_BMSK                               0x20
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION5_STICKY_BIT_SHFT                                0x5
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION4_STICKY_BIT_BMSK                               0x10
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION4_STICKY_BIT_SHFT                                0x4
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION3_STICKY_BIT_BMSK                                0x8
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION3_STICKY_BIT_SHFT                                0x3
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION2_STICKY_BIT_BMSK                                0x4
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION2_STICKY_BIT_SHFT                                0x2
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION1_STICKY_BIT_BMSK                                0x2
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION1_STICKY_BIT_SHFT                                0x1
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION0_STICKY_BIT_BMSK                                0x1
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT0_REGION0_STICKY_BIT_SHFT                                0x0

#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006104)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_RMSK                                            0xffffffff
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_IN          \
        in_dword_masked(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_ADDR, HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_RMSK)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_ADDR, m)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_OUT(v)      \
        out_dword(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_ADDR,v)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_ADDR,m,v,HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_IN)
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_RSVD0_BMSK                                      0xffffe000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_RSVD0_SHFT                                             0xd
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION44_STICKY_BIT_BMSK                            0x1000
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION44_STICKY_BIT_SHFT                               0xc
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION43_STICKY_BIT_BMSK                             0x800
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION43_STICKY_BIT_SHFT                               0xb
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION42_STICKY_BIT_BMSK                             0x400
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION42_STICKY_BIT_SHFT                               0xa
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION41_STICKY_BIT_BMSK                             0x200
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION41_STICKY_BIT_SHFT                               0x9
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION40_STICKY_BIT_BMSK                             0x100
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION40_STICKY_BIT_SHFT                               0x8
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION39_STICKY_BIT_BMSK                              0x80
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION39_STICKY_BIT_SHFT                               0x7
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION38_STICKY_BIT_BMSK                              0x40
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION38_STICKY_BIT_SHFT                               0x6
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION37_STICKY_BIT_BMSK                              0x20
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION37_STICKY_BIT_SHFT                               0x5
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION36_STICKY_BIT_BMSK                              0x10
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION36_STICKY_BIT_SHFT                               0x4
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION35_STICKY_BIT_BMSK                               0x8
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION35_STICKY_BIT_SHFT                               0x3
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION34_STICKY_BIT_BMSK                               0x4
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION34_STICKY_BIT_SHFT                               0x2
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION33_STICKY_BIT_BMSK                               0x2
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION33_STICKY_BIT_SHFT                               0x1
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION32_STICKY_BIT_BMSK                               0x1
#define HWIO_QFPROM_WRITE_DISABLE_STICKY_BIT1_REGION32_STICKY_BIT_SHFT                               0x0

#endif /* __LMH_LLM_HWIO_H__ */
