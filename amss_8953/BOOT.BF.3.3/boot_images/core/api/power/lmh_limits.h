/*===========================================================================

                    LIMITS API DEFINITIONS

DESCRIPTION
  Contains limits drivers stubs for images that do not require them.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

#ifndef __LIMITS_H__
#define __LIMITS_H__

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "boot_comdef.h"

#define LIMITS_DEFAULT_FLAG      0
#define LIMITS_ALWAYS_TRIM_FLAG  (0x1<<0)

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================

**  Function :  limits_early_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver init in secure execution environment
*
* @return
*  None.
*
* @par Dependencies
*  None
*
*
*/

void limits_early_init(uint32 flag);

/*===========================================================================

**  Function :  limits_late_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver late init
*
* @return
*  None.
*
* @par Dependencies
*    Function to be called post qsee init
*
*/

void limits_late_init(void);

#ifdef __cplusplus
};
#endif // __cplusplus

#endif // __LIMITS_H__

