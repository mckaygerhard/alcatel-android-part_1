#ifndef PM_SMBCHG_BAT_IF_H
#define PM_SMBCHG_BAT_IF_H

/*! \file
*  \n
*  \brief  pm_uefi_smbchg_bat_if.h PMIC-SMBCHG MODULE RELATED DECLARATION 
*  \details  This header file contains functions and variable declarations 
*  to support Qualcomm PMIC SMBCHG OTG (Switch-Mode Battery Charger) module. The 
*  Switched-Mode Battery Charger (SMBCHG OTG) module includes a buck regulated 
*  battery charger with integrated switches. The SMBCHG OTG module, along with the 
*  Over Voltage Proection (OVP) module will majorly be used by charger 
*  appliation for charging Li-Ion batteries with high current (up to 2A).
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies, All Rights Reserved
*/

/* =======================================================================
                                Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/api/systemdrivers/pmic/pm_smbchg_bat_if.h#3 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
09/25/15   pxm     Added the function pm_smbchg_bat_if_chg_led()
12/02/14   aab     pm_smbchg_bat_if_set_low_batt_volt_source()
10/15/14   aab     Added pm_smbchg_bat_if_set_bat_missing_detection_src()
04/09/14   aab     Initial version. 
========================================================================== */
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/


/*! \struct pm_smbchg_bat_if_cmd_chg_type
   \brief Struct for writing to battery interface command control
 */
typedef enum pm_smbchg_bat_if_cmd_chg_type
{
   PM_SMBCHG_BAT_IF_CMD__OTG_EN,               /** <OTG Enable, 0 = Disable, 1 = Enable                                                             >*/
   PM_SMBCHG_BAT_IF_CMD__EN_BAT_CHG,           /** <Enable Battery Charging                                                                         >*/
   PM_SMBCHG_BAT_IF_CMD__FC_COM,               /** <Fast Charge Command, FALSE = Force pre-charge current, TRUE = Allow Fast Charge                 >*/
   PM_SMBCHG_BAT_IF_CMD__STAT_OUTPUT,          /** <0 = STAT output enabled (still allow IRQs) 1 = Turn off STAT pin (still allow IRQs)             >*/
   PM_SMBCHG_BAT_IF_CMD__THERM_NTC_I_OVERRIDE, /** <0 = Thermistor Monitor per config setting 1 = Force thermistor monitor disable (override config)>*/
   PM_SMBCHG_BAT_IF_CMD__WIRELESS_CHG_DIS,     /** <Wireless charging 0 = enabled, 1 = disabled                                                     >*/
   PM_SMBCHG_BAT_IF_CMD__INVALID               /** <INVALID                                                                                         >*/
}pm_smbchg_bat_if_cmd_chg_type;


/*! \enum pm_smbchg_bat_if_low_bat_thresh_type
   \brief enum for different level of low battery threshold 
 */
typedef enum pm_smbchg_bat_if_low_bat_thresh_type
{
    PM_SMBCHG_BAT_IF_LOW_BATTERY_DISABLED,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P5,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P6,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P7,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P8,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P9,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P0,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P1,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P7,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_2P88,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P00,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P10,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P25,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P35,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P46,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P58,
    PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_INVALID
}pm_smbchg_bat_if_low_bat_thresh_type;



/*! \enum pm_smbchg_bat_if_aicl_threshold_type
   \brief enum for AICL threshold configuration
 */  
typedef enum pm_smbchg_bat_if_aicl_threshold_type
{ 
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_AICL_THRESHOLD_6P25V_OR_6P75V,
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_AICL_THRESHOLD_4P25V_OR_4P40V,
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_AICL_THRESHOLD_INVALID
}pm_smbchg_bat_if_aicl_threshold_type ; 



/*! \enum pm_smbchg_bat_if_dcd_timeout_delay_type
   \brief enum for DCD timeout delay configuration
 */  
typedef enum pm_smbchg_bat_if_dcd_timeout_delay_type
{ 
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_DCD_TIMEOUT_DELAY_600MS,
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_DCD_TIMEOUT_DELAY_300MS,
   PM_SMBCHG_BAT_IF_VBL_SEL_CFG_DCD_TIMEOUT_DELAY_INVALID
}pm_smbchg_bat_if_dcd_timeout_delay_type ; 



/*! \struct pm_smbchg_bat_if_cmd_chg_type
   \brief Struct for configuring battery missing configuration
 */
typedef struct pm_smbchg_bat_if_batt_missing_cfg_type
{
   uint32  batt_removal_det_time_usec;       /** <Battery removal detection time: 80,160,320,640 usec > */
   boolean batt_bat_get_override_en;         /** <Battery FET Configuration 0 = Normal operation 1 = override (turn off FET) 0x0 : BATT_FET_NORMAL 0x1 : BATT_FET_OVERRIDE > */
   boolean batt_missing_input_plugin_en;     /** <Battery Missing on Input Plug-In 0 = Disabled 1 = Enabled 0x0 : BMA_PLUG_IN_DIS 0x1 : BMA_PLUG_IN_EN > */
   boolean batt_missing_2p6s_poller_en;      /** <Battery Missing 2.6s Poller 0 = Disabled 1 = Enabled 0x0 : BMA_POLLER_DIS 0x1 : BMA_POLLER_EN > */
   boolean batt_missing_algo_en;             /** <Battery Missing Algorithm 0 = Disabled 1 = Enabled 0x0 : BMA_DIS 0x1 : BMA_EN > */
   boolean use_therm_pin_for_batt_missing_src;
   boolean use_bmd_pin_for_batt_missing_src;
}pm_smbchg_bat_if_batt_missing_cfg_type;

/*! \enum pm_smbchg_bat_miss_detect_src_type
   \brief enum for battery missing detection sources
 */
typedef enum pm_smbchg_bat_miss_detect_src_type
{
   PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_THERM_PIN,
   PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_BMD_PIN,
   PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_THERM_BMD_PIN,
   PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_NONE,
   PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_INVALID,
}pm_smbchg_bat_miss_detect_src_type;


/*! \enum pm_smbchg_bat_if_dcin_icl_type
   \brief enum for configuring dcin input current limit mode
 */
typedef enum pm_smbchg_bat_if_dcin_icl_type
{
   PM_SMBCHG_BAT_IF_DCIN_PASS_THROUGH_MODE, /** < pass through mode and ~9.8v                         >*/
   PM_SMBCHG_BAT_IF_DCIN_LOW_VOLT_MODE,     /** <low volt mode, DCIN less than 6.5v, DIV2_EN =1       >*/
   PM_SMBCHG_BAT_IF_DCIN_HIGH_VOLT_MODE,    /** <high volt mode DCIN >= 6.5v and max 9v, , DIV2_EN =1 >*/
   PM_SMBCHG_BAT_IF_DCIN_INVALID_MODE,      /** <INVALID mode                                         >*/
}pm_smbchg_bat_if_dcin_icl_type;


/*! \enum pm_smbchg_irq_bit_field_type
   \brief different types of irq bit fields of by smbch_bat_if irq module
 */
typedef enum {
   PM_SMBCHG_BAT_IF_HOT_BAT_HARD_LIM,
   PM_SMBCHG_BAT_IF_HOT_BAT_SOFT_LIM,
   PM_SMBCHG_BAT_IF_COLD_BAT_HARD_LIM,
   PM_SMBCHG_BAT_IF_COLD_BAT_SOFT_LIM,
   PM_SMBCHG_BAT_IF_BAT_OV,
   PM_SMBCHG_BAT_IF_BAT_LOW,
   PM_SMBCHG_BAT_IF_BAT_MISSING,
   PM_SMBCHG_BAT_IF_BAT_TERM_MISSING,
   PM_SMBCHG_BAT_IF_IRQ_INVALID
}pm_smbchg_bat_if_irq_type;

/*===========================================================================

                 SMBCHG BAT_IF DRIVER FUNCTION PROTOTYPES

===========================================================================*/


/*Returns battery presence status*/
/**
 * @brief This function returns battery presence status.
 * 
 * @details
 *  This function returns battery presence status..
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]bat_present.  TRUE: Present, FALSE: not present 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_bat_pres_status(uint32 device_index, boolean *bat_present);


/*This API clears the dead battery timer*/
/**
 * @brief This function clears the dead battery timer
 * 
 * @details
 *  This function clears the dead battery timer.
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_clear_dead_bat_timer(uint32 device_index);



/**
 * @brief This function writes to battery interface command control to enable/disable wireless charging,
 *         thermal current override, stat output, fast charge, battery charging, OTG .
 * 
 * @details
 *  This function writes to battery interface command control to enable/disable wireless charging,
 *  thermal current override, stat output, fast charge, battery charging, OTG .
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]cmd_chg_cfg.  Refer pm_smbchg_bat_if_cmd_chg_type 
 *                          for more info
 * @param[in]boolean       TRUE: enable, FALSE: disable
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_config_chg_cmd(uint32 device_index, pm_smbchg_bat_if_cmd_chg_type cmd_chg_cfg, boolean enable);



 /**
 * @brief This function reads to battery interface command control to enable/disable wireless charging,
 *         thermal current override, stat output, fast charge, battery charging, OTG .
 * 
 * @details
 *  This function reads to battery interface command control to
 *  enable/disable wireless charging, thermal current override,
 *  stat output, fast charge, battery charging, OTG .
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]cmd_chg_cfg.  Refer pm_smbchg_bat_if_cmd_chg_type 
 *                          for more info
 * @param[out]boolean       TRUE: enable, FALSE: disable
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_chg_cmd(uint32 device_index, pm_smbchg_bat_if_cmd_chg_type cmd_chg_cfg, boolean *enable);

/*Sets low battery voltag threshold*/
/**
 * @brief This function sets low battery voltag threshold.
 * 
 * @details
 *  Sets low battery voltag threshold .
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]low_bat_threshold.  Refer pm_smbchg_bat_if_low_bat_thresh_type 
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_low_batt_volt_threshold(uint32 device_index, pm_smbchg_bat_if_low_bat_thresh_type low_bat_threshold);


/**
 * @brief This function reads low battery voltag threshold.
 * 
 * @details
 *  Gets low battery voltag threshold .
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]low_bat_threshold.  Refer pm_smbchg_bat_if_low_bat_thresh_type 
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_low_bat_volt_threshold(uint32 device_index, pm_smbchg_bat_if_low_bat_thresh_type *low_bat_threshold, uint32 *low_bat_value);




/**
 * @brief This function sets AICL threshold
 * 
 * @details
 *  AICL (Automatic input Current Limit) threshold is used by AICL algorithm to automatically and safely maximize the current drawn from an AC/DC adapter or USB input.
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]low_bat_src.  Refer pm_smbchg_bat_if_aicl_threshold_type 
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_aicl_threshold(uint32 device_index, pm_smbchg_bat_if_aicl_threshold_type aicl_threshold);


/**
 * @brief This function sets DCD time out delay
 * 
 * @details
 *  DCD(Data Contact Detect)  delay is used in Charger Detection Hardware.  DCD hardware detects when the data pins have made contact during charger attach event.
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]low_bat_src.  Refer pm_smbchg_bat_if_dcd_timeout_delay_type 
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_dcd_timeout_delay(uint32 device_index, pm_smbchg_bat_if_dcd_timeout_delay_type dcd_timeout_delay);




/**
 * @brief This function configures battery missing parameters 
 * 
 * @details
 *  This API sets battery missing detection configuration like select if the
 *  battery missing monitoring should only happen at the beginning of a charge cycle
 *  or every 3 seconds
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] batt_missing_cfg.   Refer pm_smbchg_bat_if_batt_missing_cfg_type 
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_bat_missing_cfg(uint32 device_index, pm_smbchg_bat_if_batt_missing_cfg_type *batt_missing_cfg);


/**
 * @brief This function reads battery missing config
 * 
 * @details
 *  This API reads battery missing detection configuration like
 *  select if the battery missing monitoring should only happen
 *  at the beginning of a charge cycle or every 3 seconds
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[out] batt_missing_cfg.   Refer 
 *                                pm_smbchg_bat_if_batt_missing_cfg_type
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_bat_missing_cfg(uint32 device_index, pm_smbchg_bat_if_batt_missing_cfg_type *batt_missing_cfg);


/**
 * @brief This function sets battery missing detection source
 * 
 * @details
 *  This API sets battery missing detection source
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[out] batt_missing_cfg.   Refer 
 *                                pm_smbchg_bat_miss_detect_src_type
 *                                for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_bat_missing_detection_src(uint32 device_index, pm_smbchg_bat_miss_detect_src_type batt_missing_det_src);

/**
 * @brief This function sets the minimum system voltage
 * 
 * @details
 *  This API sets the minimum system voltage. And below this
 *  voltage system and battery are not connected
 *  together.Valid values are 3150, 3450 and 3600 milli volt 
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] min_sys_millivolt   Valid value is 3150 mv to 
 *                                3600mV
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_min_sys_volt(uint32 device_index, uint32 min_sys_millivolt);


/**
 * @brief This function reads the minimum system voltage
 * 
 * @details
 *  This API reads the minimum system voltage. And below this
 *  voltage system and battery are not connected together.Valid
 *  values are 3150, 3450 and 3600 milli volt
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[out] min_sys_millivolt   Valid value is 3150 mv to 
 *                                3600mV
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_min_sys_volt(uint32 device_index, uint32 *min_sys_millivolt);



/** 
 * @brief This function configures the current limit for pass 
 *        through mode, low volt mode and high volt mode
 * 
 * @details
 *  This API configures the DCIN input current limit for pass
 *  through mode, low volt mode and high volt mode in milliamp
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] dcin_icl_type        refer enum pm_smbchg_bat_if_dcin_icl_type 
 *                                 for more info
 *      
 * @param[in] current_ma           valid value 300 to 2000 mAmp 
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_set_dcin_input_current_limit(uint32 device_index, pm_smbchg_bat_if_dcin_icl_type dcin_icl_type, uint32 current_ma);

/*This API reads the configured current limit for pass through mode, low volt mode and high volt mode in milliamp*/
/** 
 * @brief This function returns the current limit for pass 
 *        through mode, low volt mode and high volt mode
 * 
 * @details
 *  This API returns the DCIN input current limit for pass
 *  through mode, low volt mode and high volt mode in milliamp
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] dcin_icl_type        refer enum pm_smbchg_bat_if_dcin_icl_type 
 *                                 for more info
 *      
 * @param[out]current_ma           valid value 300 to 2000 mAmp 
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_bat_if_get_dcin_input_current_limit(uint32 device_index, pm_smbchg_bat_if_dcin_icl_type dcin_icl_type, uint32 *current_ma);



/**
* @brief This function configures WI PWR timer.
* 
* @details
*  This API configures WI PWR timer. div2 falling edge values:
*  0, 150, 250, 500 usec. wipwr_irq_tmr_us values 1000,
*  1500,2000,2500,3000,4000,4500 usec*
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] div2_falling_edge_time_us   valid values are 0, 150, 250, 500usec 
* @param[in] wipwr_irq_tmr_us               values 1000, 1500,2000,2500,3000,4000,4500 usec 
*                                
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_smbchg_bat_if_set_wi_pwr_tmr(uint32 device_index, uint32 div2_falling_edge_time_us, uint32 wipwr_irq_tmr_us);

/**
* @brief This function configures WI PWR timer.
* 
* @details
*  This API configures WI PWR timer. div2 falling edge values:
*  0, 150, 250, 500 usec. wipwr_irq_tmr_us values 1000,
*  1500,2000,2500,3000,4000,4500 usec*
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] div2_falling_edge_time_us   valid values are 0, 150, 250, 500usec 
* @param[in] wipwr_irq_tmr_us               values 1000, 1500,2000,2500,3000,4000,4500 usec 
*                                
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_smbchg_bat_if_get_wi_pwr_tmr(uint32 device_index, uint32 *div2_falling_edge_time_us, uint32 *wipwr_irq_tmr_us);

/**
* @brief This function configures the SMBCHG for led 
* 
* @details
*  This function configures the SMBCHG for led 
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1.  
* 
* @param[in]boolean	   TRUE: enable, FALSE: disable
*                                
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_smbchg_bat_if_chg_led(uint32 device_index, boolean enable);

/**
* @brief This function configures the SMBCHG for irq 
* 
* @details
*  This function configures the SMBCHG for irq 
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] irq                 pm_smbchg_bat_if_irq_type
*                                
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_smbchg_bat_if_irq_status(uint32 device_index, pm_smbchg_bat_if_irq_type irq, pm_irq_status_type type, boolean *status);



#endif /* PM_SMBCHG_BAT_IF_H*/
