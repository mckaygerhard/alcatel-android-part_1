#ifndef PM_TYPEC_H
#define PM_TYPEC_H

/*! \file
*  \n
*  \brief  pm_typec.h PMIC-TYPEC MODULE RELATED DECLARATION 
*  \details  This header file contains functions and variable declarations 
*  to support Qualcomm PMIC TYPE C module. 
*  \n &copy; Copyright 2015 QUALCOMM Technologies, All Rights Reserved
*/

/* =======================================================================
                                Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/09/15   pxm     Create
========================================================================== */
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

typedef enum
{
    PM_PLUG_INVALID,
    PM_PLUG_OPEN,
    PM_PLUG_UNFLIP, // that means CC1 is connected
    PM_PLUG_FLIP,   // that means CC2 is connected
} pm_typec_plug_orientation_type;

/* For the types that different source (UFP) attached (we are in DFP).
 */
typedef enum
{
    PM_UFP_INVALID,
    PM_UFP_UNATTACHED,                  // OPEN OPEN
    PM_UFP_AUDIO_ADAPTER,               // RA RA
    PM_UFP_DEBUG_ACCESSORY,             // RD RD
    PM_UFP_ATTACHED_POWER_CABLE,        // RD RA
    PM_UFP_ATTACHED,                    // RD OPEN
    PM_UFP_NOT_ATTACHED_POWER_CABLE,    // RA OPEN
} pm_typec_ufp_type_type;


/*
 * For detecting the current limit source can provide.
 */
typedef enum
{
    PM_CURR_ADV_INVALID,
    PM_CURR_ADV_STANDARD,
    PM_CURR_ADV_1P5A,
    PM_CURR_ADV_3A,
    PM_CURR_ADV_PRIVATE,
} pm_typec_current_advertise_type;

typedef enum
{
    PM_ROLE_DRP,
    PM_ROLE_DFP,
    PM_ROLE_UFP,
    PM_ROLE_INVALID,
} pm_typec_port_role_type;

typedef enum
{
    PM_VCONN_CONTROL_BY_HW,
    PM_VCONN_ENABLE_BY_SW,
    PM_VCONN_DISABLE_BY_SW,
    PM_VCONN_INVALID,
} pm_typec_vconn_enable_type;

/*===========================================================================

                 TYPEC DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/*
 * @Brief To get the pmic device index where TYPEC module exists.
 * @return the pmic device index of type-c peripheral.
 *            0xFF: type-c peripheral is not found
 */
uint8 pm_typec_get_pmic_index(void);

/*
 * @brief To identify VBUS_VALID status via GPIO7. We can also use it to identify whether DFP/DRP is attached.
 * @param vbus_valid return whether VBUS is provided by device attached.
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_is_vbus_valid(boolean* vbus_valid);

/*
 * @Brief This function used to detect cable orientation detection.
 * @param status PM_PLUG_OPEN: Nothing attached
 *                       PM_PLUG_UNFLIP: Connected to CC1, unflip
 *                       PM_PLUG_FLIP: Connected to CC2, flipped
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_get_plug_orientation_status(pm_typec_plug_orientation_type* status);

/*
 * @Biref detecting UFP type attached. We are in DFP mode now.
 * @param dfp_type 
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_get_ufp_type(pm_typec_ufp_type_type* dfp_type);

/*
 * @Biref when a DFP is attached, we can get the current limit it can provide.
 * @param current_advertise PM_CURR_ADV_STANDARD: current limit <= 1A
 *                                  PM_CURR_ADV_1P5A: 1.5A standard
 *                                  PM_CURR_ADV_3A: 3A standard
 *                                  PM_CURR_ADV_PRIVATE: the others. NOT sure whether it is supported now.
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_get_dfp_curr_advertise(pm_typec_current_advertise_type * current_advertise);

/*
 * @Biref enable peripheral. Already been set in pon sequence table.
 * @param 
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_enable(boolean enable);

/*
 * @Biref send typec_disable_cmd command
 * @param 
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_disable_typec_command_sw(boolean enable);

/*
 * @Brief Explicitly set working role: drp/dfp/ufp
 * @param port_role PM_ROLE_DRP,
 *             PM_ROLE_DFP,
 *             PM_ROLE_UFP,
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_set_port_role(pm_typec_port_role_type port_role);

/*
 * @Brief set control source of VCONN, enable/disable vconn by software
 * @param enable_type PM_VCONN_CONTROL_BY_HW, change control source to hardware (auto by FSM)
 *             PM_VCONN_ENABLE_BY_SW, enable vconn by sw
 *             PM_VCONN_DISABLE_BY_SW, disable vconn by sw
 * @return PM_ERR_FLAG__SUCCESS no error.
 *             PM_ERR_FLAG__TYPEC_PERIPERAL_ABSENT type-C is not supported
 */
pm_err_flag_type pm_typec_vconn_enable(pm_typec_vconn_enable_type enable_type);



#endif /* PM_TYPEC_H*/

