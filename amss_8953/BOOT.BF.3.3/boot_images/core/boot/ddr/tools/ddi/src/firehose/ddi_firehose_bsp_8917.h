/*==================================================================
 *
 * FILE:        ddi_firehose_bsp_8917.h
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2013 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose_bsp_8917.h#1 $ 
 *   $DateTime: 2016/04/21 08:39:38 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2016-03-14   qbz      Create for 8917 DDI
 */

#ifndef DDI_FIREHOSE_BSP_8917_H
#define DDI_FIREHOSE_BSP_8917_H

#include "com_dtypes.h"

#define DDI_PLATFORM      "MSM8917"
#define DDI_VERSION       14

#endif
