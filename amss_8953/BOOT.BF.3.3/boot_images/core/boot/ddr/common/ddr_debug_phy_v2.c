/**
 * @file boot_ddr_debug.c
 * @brief
 * Implementation for DDR Debug Mode.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/common/ddr_debug_phy_v2.c#4 $
$DateTime: 2016/04/21 08:39:38 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/13/14   yps    Mask watch dog reset funciton on 8936 platform
09/23/14   yps    Porting code from 8916 to 8936 platform
09/17/13   sl      Added more DDR tuning options.
09/13/13   sl      Fixed pslew/nslew order.
08/29/13   sl      Use USB APIs instead of Boot APIs.
08/20/13   sl      Added DDR tuning.
08/07/13   sr      Added watchdog reset support.
06/18/13   sl      Initial version.
================================================================================
                     Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_debug_phy_v2.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "qhsusb_al_bulk.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "HAL_SEQ_DDR.h"
#include "ddr_test.h"
#include "mpm_hwio.h"
#include "boot_extern_clk_interface.h"
#include "HALhwio.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_power_interface.h"
#include "ddr_debug_common.h"
#include "ddi_firehose.h"
#include "ddr_phy_config.h"
#include "ddrss_seq_hwiobase.h"
#include "ddr_phy_ebi.h"
#include "ddr_params.h"
#include "DALSys.h"
#include "ddr_phy_ddrss.h"
#include "ddi_firehose.h"

/*==============================================================================
                                  MACROS
==============================================================================*/
/* Length of transportation buffer in characters */
//#define BUFFER_LENGTH   512


/* Watchdog bark and bite time */
#define WATCHDOG_BARK_TIME_SCLK  0x7FFF
#define WATCHDOG_BITE_TIME_SCLK  0xFFFF

/* DDR tuning parameters */
#define DDR_TUNING_LOOP_COUNT  128
#define DQ_ROUT_MAX            0x7
#define DQ_PSLEW_MAX           0x3
#define DQ_NSLEW_MAX           0x3

#define RESET_DEBUG_SW_ENTRY_ENABLE  0x1
#define BIT_ERROR_STATISTICS

#define CA_PATTERN_NUM 4

static clock_info_t g_clkInfo = {0, NULL};

extern uint16 ca_training_pattern[CA_PATTERN_NUM][6];

//#define SWEEP_WRITE_CDC_ONLY
uint32 failures[4];
uint32 error_statistics =0;
extern char pattern_name[][20];
/*==============================================================================
                                  DATA
==============================================================================*/
/* Echo and message buffers: must be addressable by USB and 4-byte aligned */


/* DDR tuning cookies */
extern void (*volatile reset_cb_func)(boolean);
extern void DDRSS_dq_set_all_dq_cdc_rd90_to_coarse_fine( uint32 _inst_,SDRAM_INTERFACE  interface, uint16 coarse, uint16 fine );
extern void DDRSS_dq_set_all_dq_cdc_wr90_to_coarse_fine( uint32 _inst_,SDRAM_INTERFACE  interface, uint16 coarse, uint16 fine );
extern 	uint32 DDRSS_RCW_training( SDRAM_INTERFACE channel_select, uint32 chip_select, uint32 pattern_mode, uint32 clk_freq_in_khz, uint32 max_loop_cnt);
extern boolean ddr_do_phy_training( void );

__attribute__((section("DDR_DEBUG_TUNING_COOKIE")))
uint32 dq_rout, dq_pslew, dq_nslew, read_cdc_delay, write_cdc_delay;

int32 clock_index = -1;

boolean rdcdc_scan = FALSE;
uint8 coarse_sweep,fine_sweep;

uint32 ddrphy_dq_ddi_cfg[][2] = 
{
  HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG0_ADDR(0)                      , DDRPHY_DQ_PAD_CFG0_RECVAL ,
  //HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG1_ADDR(0)                      , DDRPHY_DQ_PAD_CFG1_RECVAL ,
  //HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG2_ADDR(0)                      , DDRPHY_DQ_PAD_CFG2_RECVAL ,
  //HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG3_ADDR(0)                      , DDRPHY_DQ_PAD_CFG3_RECVAL ,
  // SHOWS END OF SETTING
  END_OF_SETTING                                                 , END_OF_SETTING
};

extern uint32 (*ddrphy_dq_mision_mode_cfg_ptr)[2];
/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
void ddr_debug_output(const char *msg)
{
} /* ddr_debug_output */

void ddr_tuning_log_out(uint32 failures[4],uint32 tempresult[4],uint16 CDC_DELAY_START,uint16 WRITE_CDC_DELAY_MAX)
{
  memset(str,0,BUFFER_LENGTH);
  if(0 == failures[0] && 0 == failures[1] && 0 == failures[2] && 0 == failures[3]) {
    snprintf(str, BUFFER_LENGTH,
             "%4u, %4u, %4u, "
             "%3u, %3u, %3u, %3u.[log]%s\r\n",
             dq_rout, read_cdc_delay, write_cdc_delay,
             failures[0], failures[1], failures[2], failures[3],"[stress test]PASS");        
  }
  else {
    snprintf(str, BUFFER_LENGTH,
             "%4u, %4u, %4u, "
             "%3u, %3u, %3u, %3u.[log]%s\r\n",
             dq_rout, read_cdc_delay, write_cdc_delay,
             failures[0], failures[1], failures[2], failures[3],str_error_log);
  }
  ddi_firehose_print_log(str);
  memset(str_error_log,0,BUFFER_LENGTH);
  tempresult[0]= failures[0];
  tempresult[1]= failures[1];
  tempresult[2]= failures[2];
  tempresult[3]= failures[3];
}

void HAL_SDRAM_PHY_Update_Drive_Strength(uint32 base, SDRAM_INTERFACE interface, uint32 rout, uint32 pslew, uint32 nslew)
{
  ddr_interface_state ddr_status;

  ddr_status = ddr_get_status();
  ddrphy_dq_mision_mode_cfg_ptr = ddrphy_dq_ddi_cfg;
    
  ddrphy_dq_ddi_cfg[0][1]= (ddrphy_dq_ddi_cfg[0][1]&~HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG0_DQ_ROUT_BMSK)|(rout<<HWIO_DDRPHY_DQ_DDRPHY_DQ_PAD_CFG0_DQ_ROUT_SHFT);
    
  if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE) {
    //chn0 dq config
    HWIO_OUTX(	SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_SWMAN_OFFSET, AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CH0_DQs);
    EBI1_PHY_Set_Config_dq( SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_BROADCAST_SWMAN_OFFSET );
    HWIO_OUTX(  SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_SWMAN_OFFSET, AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE);
  }

  if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE) {
    //chn1 dq config
    HWIO_OUTX(  SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_SWMAN_OFFSET, AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CH1_DQs) ; 
    EBI1_PHY_Set_Config_dq( SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_BROADCAST_SWMAN_OFFSET );
    HWIO_OUTX(	SEQ_DDRSS_EBI1_PHY_OFFSET + SEQ_EBI1_PHY_CFG_EBI1_AHB2PHY_SWMAN_OFFSET, AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE); 
  }

  ddrphy_dq_mision_mode_cfg_ptr = ddrphy_dq_mision_mode_cfg;
} /* HAL_SDRAM_PHY_Update_Drive_Strength */

void  ddr_write_pattern(const uint32 base,uint32 test_size,uint32 *val)
{
  volatile uint32 * base_addr = (uint32 *)base;
  uint32 i = 0;
  uint32 value;
  uint32 ddi_ddr_freq;
  ddr_interface_state ddr_status;

  ddi_ddr_freq = -1 == clock_index ? 
                   g_clkInfo.ddr_fmax :
                   g_clkInfo.ClockFreqKHz[clock_index];
  
  value = *val;
  ddr_status = ddr_get_status();

  EBI1_PHY_CFG_phy_init(SEQ_DDRSS_EBI1_PHY_OFFSET, 0, 0, dynamic_legacy, 0 );
  HAL_SDRAM_PHY_Update_Drive_Strength(0,SDRAM_INTERFACE_0,dq_rout,dq_pslew,dq_nslew);

  /* Clock API to scale to low speed */
  if(rdcdc_scan == TRUE) {
    /* Clock API to scale to low speed */
    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddr_status.clk_speed , SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddr_status.clk_speed , SDRAM_INTERFACE_1);

    boot_clock_set_bimcspeed(ddr_status.clk_speed / DDR_CLOCK_RATIO);

    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddr_status.clk_speed, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddr_status.clk_speed, SDRAM_INTERFACE_1);
  }
  else {
    /* clock api to restore to high speed */
    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

    boot_clock_set_bimcspeed(ddi_ddr_freq / DDR_CLOCK_RATIO);

    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

    DDRSS_dq_set_all_dq_cdc_wr90_to_coarse_fine(0, SDRAM_INTERFACE_0, coarse_sweep, fine_sweep);
  }

  switch(PATTERN)
  {
    case BITFLIP_PATTERN:
    case CHECK_BOARD_PATTERN:
    case BITSPREAD_PATTERN:
    case SOLID_BITS_PATTERN:
      for (i = 0; i <= test_size; i++) {
        base_addr[i] = (i % 2) == 0 ? value : ~value;
      }
      break;
    case WORKING_ONE_PATTERN:
    case WORKING_ZERO_PATTERN:
      for (i = 0; i <= test_size; i++) {
        base_addr[i] = value;
      }
      break;
    case SEQ_INR_PARTTERN:
      for (i = 0; i <= test_size; ++i) {
        base_addr[i] = value;
        ++value;
      }
      break;
    case CUSTOMER_PARTTERN:
      break;
    default:
      break;
  }
}

boolean ddr_read_pattern(const uint32 base,uint32 test_size,uint32 *val)
{
  volatile uint32 * base_addr = (uint32 *)base;
  uint32 i = 0;
  uint32 value;
  uint32 temp_value = 0;
  uint32 expect_value = 0;
  uint32 result = 0;
  uint32 idx = 0;
  uint32 ddi_ddr_freq;
  ddr_interface_state ddr_status;

  ddi_ddr_freq = -1 == clock_index ? 
                 g_clkInfo.ddr_fmax :
                 g_clkInfo.ClockFreqKHz[clock_index];

  ddr_status = ddr_get_status();

  //char pattern_name[20];
  value = *val;

  EBI1_PHY_CFG_phy_init(SEQ_DDRSS_EBI1_PHY_OFFSET, 0, 0, dynamic_legacy, 0 );
  HAL_SDRAM_PHY_Update_Drive_Strength(0,SDRAM_INTERFACE_0,dq_rout,dq_pslew,dq_nslew);
  if(rdcdc_scan == TRUE) {
    /* clock api to restore to high speed */
    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

    boot_clock_set_bimcspeed(ddi_ddr_freq / DDR_CLOCK_RATIO);
    
    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

    DDRSS_dq_set_all_dq_cdc_rd90_to_coarse_fine(0, SDRAM_INTERFACE_0, coarse_sweep, 2);
    DDRSS_RCW_training(SDRAM_INTERFACE_0, 1, 1, ddi_ddr_freq, 1);
    DDRSS_dq_set_all_dq_cdc_rd90_to_coarse_fine(0, SDRAM_INTERFACE_0, coarse_sweep, fine_sweep);
  }
  else {
    /* Clock API to scale to low speed */
    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddr_status.clk_speed , SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_pre_clock_switch(0, ddr_status.clk_speed , SDRAM_INTERFACE_1);

    boot_clock_set_bimcspeed(ddr_status.clk_speed / DDR_CLOCK_RATIO);

    if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddr_status.clk_speed, SDRAM_INTERFACE_0);
    if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
      ddr_post_clock_switch(0, ddr_status.clk_speed, SDRAM_INTERFACE_1);
  }

  switch(PATTERN)
  {
    case BITFLIP_PATTERN:
    case CHECK_BOARD_PATTERN:
    case BITSPREAD_PATTERN:
    case SOLID_BITS_PATTERN:  
      for (i = 0; i <= test_size; i++) {
        temp_value = base_addr[i];
        expect_value = (i % 2) == 0 ? value : ~value;
        if (temp_value != expect_value) {
          memset(str_error_log,0,BUFFER_LENGTH);
          if((temp_value!=expect_value)) {
            result = temp_value ^ expect_value;
#ifdef BIT_ERROR_STATISTICS
            error_statistics|=result;
            if(rdcdc_scan == TRUE)
              snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x",
                       pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
            else
              snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
                       pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
#else
            if(rdcdc_scan == TRUE)
              snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error data:0x%08x, expect data:0x%08x", 
                       pattern_name[PATTERN],(base_addr+i), temp_value, expect_value);
            else
              snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error data:0x%08x, expect data:0x%08x", 
                       pattern_name[PATTERN],(base_addr+i), temp_value, expect_value);
#endif
          }

          for (idx = 0; idx < 4; idx++) {
            if( result & (0xFF << (idx*8)) ) failures[idx]++;
          }
          //return FALSE;
        }
      }
      break;
    case WORKING_ONE_PATTERN:
    case WORKING_ZERO_PATTERN:
      /* verify */
      for (i = 0; i <= test_size; i++) {
        temp_value = base_addr[i];
        if (temp_value != value) {
          result = temp_value ^ value;
#ifdef BIT_ERROR_STATISTICS
          error_statistics |= result;
          if(rdcdc_scan == TRUE)
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
                     pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
          else
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
                     pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
#else
          if(rdcdc_scan == TRUE)
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
                     pattern_name[PATTERN],base_addr + i, base_addr[i], ((i % 2) == 0 ? value : ~value));
          else
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
                     pattern_name[PATTERN],base_addr + i, base_addr[i], ((i % 2) == 0 ? value : ~value));
#endif
          for (idx = 0; idx < 4; idx++) {
            if ( result & (0xFF << (idx*8)) ) failures[idx]++;
          }
          //return FALSE;
        }
      }
      break;
    case SEQ_INR_PARTTERN:
      for (i = 0; i <= test_size; ++i) {
        temp_value = base_addr[i]; 
        if (temp_value != value) {
          result = temp_value ^ value;
#ifdef BIT_ERROR_STATISTICS
          error_statistics|=result;
          if(rdcdc_scan == TRUE)
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
                     pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
          else
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
                     pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
#else
          if(rdcdc_scan == TRUE)
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
                     pattern_name[PATTERN], base_addr + i, base_addr[i], value);
          else
            snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
                     pattern_name[PATTERN],  base_addr + i, base_addr[i], value);
#endif
          for (idx = 0; idx < 4; idx++) {
            if ( result & (0xFF << (idx*8)) )  failures[idx]++;
          }
          //return FALSE;
        }
        ++value;
      }
      break;
    default:
      break;
  }

  if(failures[0]||failures[1]||failures[2]||failures[3]) return FALSE;
  /* verify */
  return TRUE;
}

void* ddr_debug_get_clock_info(void)
{
  uint32 nNumLevel = 0, ii = 0;
  ClockPlanType *pClockPlan;

  if(0 != g_clkInfo.ClockLevels && NULL != g_clkInfo.ClockLevels) return &g_clkInfo;

  /* Call BIMCQuery to get the Num Perf Levels */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);

  /* Call BIMCQuery to get the Clock Plan */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);

  g_clkInfo.ClockLevels = nNumLevel;

  g_clkInfo.ClockFreqKHz = (uint32*)malloc(nNumLevel * sizeof(uint32));
  if(NULL == g_clkInfo.ClockFreqKHz) return NULL;

  for(ii = 0; ii < nNumLevel; ii++){
    g_clkInfo.ClockFreqKHz[ii] = pClockPlan[ii].nFreqKHz * DDR_CLOCK_RATIO;
  }

  g_clkInfo.mc_fmax = pClockPlan[nNumLevel - 1].nFreqKHz;
  g_clkInfo.ddr_fmax = pClockPlan[nNumLevel - 1].nFreqKHz * DDR_CLOCK_RATIO;

  // invalid clock_index input, reset to -1
  if(clock_index < 0 || clock_index > g_clkInfo.ClockLevels - 1){
    clock_index = -1;
  }

  DALSYS_Free(pClockPlan);
  
  return &g_clkInfo;
}

void ddr_debug_tuning_init()
{
  uint32 ddr_training_partition_size;

  if(NULL == ddr_params_get_training_data(&ddr_training_partition_size))
    ddr_params_set_training_data((void*)SCL_DDR_TRAINING_DATA_BUF_BASE, ddr_training_partition_size);

  ddr_debug_get_clock_info();
}

void ddr_debug_do_tuning(int lineType, int rwType, boolean is_training)
{
}

boolean ddr_debug_dq_cdc_sweep(void* inArgs)
{
  arg_dqshmoo_p tmpDQArgs;
  uint32 ddi_ddr_freq;
  uint8 max_coarse=0, loop_count=0;
  uint16 training_period_1ps=0;
  static uint32 tempresult[4]={0xFF};
  ddr_size_info ddr_info;

  if(NULL == inArgs) return FALSE;
  tmpDQArgs = (arg_dqshmoo_p)inArgs;

  // not good for following codes, may change in future
  clock_index = tmpDQArgs->cur_clocklevel;
  dq_rout = tmpDQArgs->cur_rout;
  start_address = tmpDQArgs->cur_startaddr;
  test_size = tmpDQArgs->cur_testsize;

  ddr_debug_tuning_init();

  // init dq & ca setting as trained data
  ddr_do_phy_training();
  
  ddi_ddr_freq = -1 == clock_index ?
                 g_clkInfo.ddr_fmax :
                 g_clkInfo.ClockFreqKHz[clock_index];
                 
  ddr_info = ddr_get_size();

  training_period_1ps = 1000000000 / ddi_ddr_freq;
  max_coarse = (((training_period_1ps / 2) + fine_lut[DESKEW_CDC_INITIAL_VALUE] + 100) / COARSE_STEP) + 3;

  snprintf(str, BUFFER_LENGTH, "$Clock(%4u, 0, %4u)\r\n", ddi_ddr_freq, ddi_ddr_freq);
  ddi_firehose_print_log(str);

  snprintf(str, BUFFER_LENGTH, "$Vref(%4u, 0, %4u)\r\n", tmpDQArgs->cur_vref, tmpDQArgs->cur_vref);
  ddi_firehose_print_log(str);

  if(ddr_info.sdram0_cs0!=0){

    //read scan
    if(0 == tmpDQArgs->cur_rwtype || 1 == tmpDQArgs->cur_rwtype){
      // for all & read only
      rdcdc_scan = TRUE;
      for(coarse_sweep = 0; coarse_sweep < max_coarse; coarse_sweep++){

        for(fine_sweep = 0; fine_sweep < MAX_FINE_STEP; fine_sweep++){

          for(loop_count = 0; loop_count < 1; loop_count++){

            failures[0] = failures[1] = failures[2] = failures[3] = 0;
            error_statistics = 0;
            ddr_function_defect_test((uint32)ddr_info.sdram0_cs0_addr, test_size);
            read_cdc_delay = cdc_lut[coarse_sweep*MAX_FINE_STEP+fine_sweep]; 
            write_cdc_delay = 0xffff;
            ddr_tuning_log_out(failures,tempresult,cdc_lut[0],cdc_lut[FAIL_HISTOGRAM_SIZE-1]);
          } // loop_count
        } // fine_sweep
      } // coarse_sweep
    }

    //write scan
    if(0 == tmpDQArgs->cur_rwtype || 2 == tmpDQArgs->cur_rwtype){
      // for all & read only
      rdcdc_scan = FALSE;
      for(coarse_sweep = 0; coarse_sweep < max_coarse; coarse_sweep++){

        for(fine_sweep = 0; fine_sweep < MAX_FINE_STEP; fine_sweep++){

          for(loop_count = 0; loop_count < 1; loop_count++){

            failures[0] = failures[1] = failures[2] = failures[3] = 0;
            error_statistics = 0;
            ddr_function_defect_test((uint32)ddr_info.sdram0_cs0_addr, test_size);
            read_cdc_delay = 0xffff;
            write_cdc_delay = cdc_lut[coarse_sweep*MAX_FINE_STEP+fine_sweep];
            ddr_tuning_log_out(failures,tempresult,cdc_lut[0],cdc_lut[FAIL_HISTOGRAM_SIZE-1]);
          } // loop_count
        } // fine_sweep
      } // coarse_sweep
    }
  }

  return TRUE;
}

uint16 DDRSS_get_ca_exp_pattern_ddi(uint32 _inst_, uint16 ca_pat_rise, uint16 ca_pat_fall, uint16 ca_train_mapping)
{
  uint16 i;
  uint16 index0;
  uint16 index1;
  uint16 exp_pattern_ph0;
  uint16 exp_pattern_ph1;

  index0 = 0;
  index1 = 0;
  exp_pattern_ph0 = 0;
  exp_pattern_ph1 = 0;

  for (i = 0; i <= 9;  i ++) {
    if (i != 4 && i != 9 && ca_train_mapping == 0) {
      exp_pattern_ph0 = exp_pattern_ph0 | (((ca_pat_rise >> i) & 0x1) << index0);
      exp_pattern_ph0 = exp_pattern_ph0 | (((ca_pat_fall >> i) & 0x1) << index0 + 1);
      index0 = index0 + 2;
    }
    else {
      if(ca_train_mapping == 1 && (i == 4 || i == 9)) {
        exp_pattern_ph1 = exp_pattern_ph1 | (((ca_pat_rise >> i) & 0x1) << index1);
        exp_pattern_ph1 = exp_pattern_ph1 | (((ca_pat_fall >> i) & 0x1) << index1 + 1);
        index1 = index1 + 8;
      }
    }
  }

  if(ca_train_mapping == 0) {
    return exp_pattern_ph0;
  }
  else {
    return exp_pattern_ph1;
  }
}

void DDRSS_dcc_cal_ddi( uint32 _inst_, uint32 chnl)
{
  uint32 which_jcpll = 0;

  /// Detect which_jcpll
  if (chnl == 0) {
    which_jcpll = HWIO_INXF (_inst_ + SEQ_DDRSS_EBI1_PHY_CH0_DDR_CC_OFFSET, DDR_CC_JCPLL_STATUS, ACTIVE_JCPLL);
  }
#ifdef CHNL_CNT_2  
  if (chnl == 1) {
    which_jcpll = HWIO_INXF (_inst_ + SEQ_DDRSS_EBI1_PHY_CH1_DDR_CC_OFFSET, DDR_CC_JCPLL_STATUS, ACTIVE_JCPLL);
  }
#endif

  ///  Turn-on PHY clocks in debug mode
  BIMC_Enable_CLK_To_PHY (_inst_ + SEQ_DDRSS_BIMC_OFFSET, (SDRAM_INTERFACE) chnl );
  
  ///  Perform DCC calibration for first jcpll
  EBI1_PHY_CFG_dcc_init(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET,
                        chnl,
                        which_jcpll);
  ///  Turn-off PHY clocks in debug mode
  BIMC_Disable_CLK_To_PHY (_inst_ + SEQ_DDRSS_BIMC_OFFSET, (SDRAM_INTERFACE) chnl );
  
}

boolean ddr_debug_ca_cdc_sweep(void* inArgs)
{
  arg_cashmoo_p tmpCArgs;
  uint32 ddi_ddr_freq;

  // settings
  uint32 _inst_=0;
  SDRAM_INTERFACE interface=SDRAM_INTERFACE_0;
  uint16 chip_select=SDRAM_CS0;
  uint16 max_loop_count=2;
  
  // local variables
  uint8 pattern=0;
  uint8 ca_mapping=0;
  uint8 site=0;
  uint8 loop_count=0;
  uint8 coarse_sweep=0;
  uint8 fine_sweep=0;

  char str[128];
  char str_detail_log[128];
  boolean isPreviousPass=FALSE;
  
  uint8 fail_bit[32]={0};
  uint8 fail_byte[4]={0};
  uint8 fail_cnt=0;
  uint32 fail_mask_bit=0x1;
  uint32 fail_mask_byte=0xff;

  uint16 fail = 0;
  uint16 rank, first_rank, last_rank;
  uint16 safe_ca_cdc_wr90_delays[4] = {0x18, 0x20, 0x30, 0x40};
  uint16 deskew_delay_value=DESKEW_CDC_INITIAL_VALUE;
  uint16 ca_training_expected=0;
  uint16 cs_rise_pat=0;
  uint16 cs_fall_pat=0;
  uint16 ca_training_pattern_result_read=0;
  uint16 ca_cdc=0;

  uint32 debug_msg_cnt=0;
  ddr_interface_state ddr_status;

  if(NULL == inArgs) return FALSE;
  tmpCArgs = (arg_cashmoo_p)inArgs;

  clock_index = tmpCArgs->cur_clocklevel;

  ddr_debug_tuning_init();

  ddi_ddr_freq = -1 == clock_index ? 
                 g_clkInfo.ddr_fmax :
                 g_clkInfo.ClockFreqKHz[clock_index];

  ddr_status = ddr_get_status();

  BIMC_Disable_All_Periodic(EBI1_BIMC_BASE, interface, chip_select); 

  /* PLL 0 */
  if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
    ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
  if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
    ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

  boot_clock_set_bimcspeed(ddi_ddr_freq / DDR_CLOCK_RATIO);

  if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
    ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
  if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
    ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

  DDRSS_dcc_cal_ddi(0,interface);
  
  /* PLL 1 */
  if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
    ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
  if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
    ddr_pre_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

  boot_clock_set_bimcspeed(ddi_ddr_freq / DDR_CLOCK_RATIO);	

  if(ddr_status.sdram0_cs0 != DDR_UNAVAILABLE)
    ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_0);
  if(ddr_status.sdram1_cs0 != DDR_UNAVAILABLE)
    ddr_post_clock_switch(0, ddi_ddr_freq, SDRAM_INTERFACE_1);

  DDRSS_dcc_cal_ddi(0,interface);

  snprintf(str, BUFFER_LENGTH, "$Clock(%4u, 0, %4u)\r\n", ddi_ddr_freq, ddi_ddr_freq);
  ddi_firehose_print_log(str);

  snprintf(str, BUFFER_LENGTH, "$Vref(%4u, 0, %4u)\r\n", tmpCArgs->cur_vref, tmpCArgs->cur_vref);
  ddi_firehose_print_log(str);
    
  if ( chip_select == SDRAM_RANK_BOTH ) {//dual rank
    first_rank = SDRAM_RANK_CS0;
    last_rank  = SDRAM_RANK_CS1;
  }
  else {//single  rank ( either SDRAM_RANK_CS0 or SDRAM_RANK_CS1
    first_rank = chip_select;
    last_rank  = chip_select;
  }

  if(max_loop_count >= (256/CA_PATTERN_NUM)) max_loop_count = (256/CA_PATTERN_NUM) - 1;

  for ( rank=first_rank ; rank <=last_rank ; rank++) {
    EBI1_PHY_CFG_ebi_set_exit_dq_for_ca_training(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET, interface, 1);

    for(site = 0; site < SITE_PER_CA_CNT;  site ++) {
      EBI1_PHY_CFG_ebi_ca_training_seq_set_initial_cdc_perbit_deskew_delay(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET,
                                                                           interface,
                                                                           site,
                                                                           deskew_delay_value);
    }

    for(pattern = 0; pattern < CA_PATTERN_NUM;  pattern ++) {
      for(ca_mapping = 0; ca_mapping < 2; ca_mapping ++) {
        for(site = 0; site < SITE_PER_CA_CNT; site++) {
          EBI1_PHY_CFG_ebi_set_ca_cdc_wr90(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET, interface, site, safe_ca_cdc_wr90_delays);
        }

        BIMC_Set_CA_Training_Pattern(_inst_ + SEQ_DDRSS_BIMC_OFFSET, 
                                     interface,
                                     rank, 
                                     ca_mapping,
                                     ca_training_pattern[pattern][PRECS_RISE], 
                                     ca_training_pattern[pattern][PRECS_FALL], 
                                     ca_training_pattern[pattern][CS_RISE], 
                                     ca_training_pattern[pattern][CS_FALL], 
                                     ca_training_pattern[pattern][POSTCS_RISE], 
                                     ca_training_pattern[pattern][POSTCS_FALL]);
        cs_rise_pat = ca_training_pattern[pattern][CS_RISE];
        cs_fall_pat = ca_training_pattern[pattern][CS_FALL];
        ca_training_expected = DDRSS_get_ca_exp_pattern_ddi(_inst_ , cs_rise_pat, cs_fall_pat, ca_mapping);

        // scan from left, but will not break
        for(coarse_sweep = 0x0; coarse_sweep < MAX_COARSE_STEP; coarse_sweep++) {
          for(fine_sweep = 0x0; fine_sweep < MAX_FINE_STEP; fine_sweep++) {
            fail = 0;
            for(fail_cnt = 0; fail_cnt<32; fail_cnt++) fail_bit[fail_cnt] = 0;
            for(fail_cnt = 0; fail_cnt<4; fail_cnt++) fail_byte[fail_cnt] = 0;

            for(site = 0; site < SITE_PER_CA_CNT; site++) {
              EBI1_PHY_CFG_ebi_ca_training_seq_set_initial_cdc_wr90_delay(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET,
                                                                          interface,
                                                                          site,
                                                                          coarse_sweep,
                                                                          fine_sweep);
            }

            for(loop_count = 0; loop_count < max_loop_count; loop_count++) {
              BIMC_CA_Training_Toggle_Pattern(_inst_ + SEQ_DDRSS_BIMC_OFFSET, interface);
              for(site = 0; site < SITE_PER_CA_CNT;  site ++) {
                ca_training_pattern_result_read = EBI1_PHY_CFG_ebi_read_dq_training_status_for_ca_training(
                                                                          _inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET,
                                                                          interface,
                                                                          site);
                if(ca_mapping == 1) ca_training_pattern_result_read &= 0x0303;

                if(ca_mapping == 0) {
                  if((ca_training_pattern_result_read & 0x0003) != (ca_training_expected & 0x0003)) fail |= 0x0001;
                  if((ca_training_pattern_result_read & 0x000C) != (ca_training_expected & 0x000C)) fail |= 0x0002;
                  if((ca_training_pattern_result_read & 0x0030) != (ca_training_expected & 0x0030)) fail |= 0x0004;
                  if((ca_training_pattern_result_read & 0x00C0) != (ca_training_expected & 0x00C0)) fail |= 0x0008;
                  if((ca_training_pattern_result_read & 0x0300) != (ca_training_expected & 0x0300)) fail |= 0x0020;
                  if((ca_training_pattern_result_read & 0x0C00) != (ca_training_expected & 0x0C00)) fail |= 0x0040;
                  if((ca_training_pattern_result_read & 0x3000) != (ca_training_expected & 0x3000)) fail |= 0x0080;
                  if((ca_training_pattern_result_read & 0xC000) != (ca_training_expected & 0xC000)) fail |= 0x0100;
                }

                if (ca_mapping != 0){
                  if((ca_training_pattern_result_read & 0x0003) != (ca_training_expected & 0x0003)) fail |= 0x0010;
                  if((ca_training_pattern_result_read & 0x0300) != (ca_training_expected & 0x0300)) fail |= 0x0200;
                }
              }
            }

            for(fail_cnt = 0; fail_cnt < 32; fail_cnt++) {
              fail_mask_bit = 0x1 << fail_cnt;
              if(fail & fail_mask_bit) fail_bit[fail_cnt]++;

              fail_mask_byte = 0xff << (fail_cnt / 8) * 8;
              if( (fail & fail_mask_bit) & fail_mask_byte) fail_byte[fail_cnt/8]++;
            }

            ca_cdc = cdc_lut[coarse_sweep*MAX_FINE_STEP+fine_sweep];

            //ddi_firehose_print_logUart("coarse<%d>, fine<%d>, cdc<%d>", coarse_sweep, fine_sweep, ca_cdc );

            if(0 == fail_byte[0] && 0 == fail_byte[1] && 0 == fail_byte[2] && 0 == fail_byte[3]) {// pass
              if(!(0 == fine_sweep && 0 == coarse_sweep) && !(MAX_FINE_STEP-1 == fine_sweep && MAX_COARSE_STEP-1 == coarse_sweep)){
                //not the first one & not the last one
                if(TRUE == isPreviousPass) continue;
              }

              snprintf(str_detail_log,BUFFER_LENGTH, "[msg%u][Pattern%u]pass", debug_msg_cnt, pattern);
              snprintf(str, BUFFER_LENGTH, "%4u,%4u,%4u,%3u,%3u,%3u,%3u.[log]%s\r\n",
                       0xffff, ca_cdc, 0xffff, fail_byte[0], fail_byte[1], fail_byte[2], fail_byte[3], str_detail_log);
              isPreviousPass = TRUE;
            }
            else {
              snprintf(str_detail_log, BUFFER_LENGTH, "[msg%u][Pattern%u]failed, error result:0x%08x, expect result:0x%08x", debug_msg_cnt, pattern, fail, ALL_CA_PASS_PAT);
              snprintf(str, BUFFER_LENGTH, "%4u,%4u,%4u,%3u,%3u,%3u,%3u.[log]%s\r\n",
                       0xffff, ca_cdc, 0xffff, fail_byte[0], fail_byte[1], fail_byte[2], fail_byte[3], str_detail_log);
              isPreviousPass = FALSE;
            }
            debug_msg_cnt++;

            ddi_firehose_print_log(str);
            memset(str,0,128);
            memset(str_detail_log,0,128);
          }
        }
      }
    }

    // exit ca training mode
    for(site = 0; site < SITE_PER_CA_CNT; site++) {
      EBI1_PHY_CFG_ebi_set_ca_cdc_wr90(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET, interface, site, safe_ca_cdc_wr90_delays);
    }

    BIMC_Exit_CA_Training(_inst_ + SEQ_DDRSS_BIMC_OFFSET, interface, rank);
    EBI1_PHY_CFG_ebi_set_exit_dq_for_ca_training(_inst_ + SEQ_DDRSS_EBI1_PHY_OFFSET, interface, 0);
  }

  return TRUE;
}

