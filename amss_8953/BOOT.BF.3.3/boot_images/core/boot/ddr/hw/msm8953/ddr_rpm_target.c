/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/msm8953/ddr_rpm_target.c#6 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/02/15   sc      Shared driver changes
06/20/14   tw      added ddr_pre_init api to capture any target specific 
                   workarounds that needs to be applied prior to ddr init
05/28/14   tw      cleaned up sbl <-> ddr driver dependencies around ddr training
                   implementation of cx\mx\cpr hash to force retraining
03/12/14   sr      Initial version.
================================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/

/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HAL_SNS_DDR.h"
#include "ddr_config.h"
#include "ClockBoot.h"
#include "icbcfg.h"
#include "railway.h"
#include "ddrss_init_sdi.h"

#include "msmhwiobase.h"
#include "msmhwioreg.h"
#include <stddef.h>
#include "bimc.h"
#include "HALhwio.h"
#include "bimc_seq_hwioreg.h"
#include "pm.h"
#include "ddr_external.h"
#include "ddr_func_tbl.h"


#define ONE_TIME_TRAINING FALSE
/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF

/*==============================================================================
                                  DATA
==============================================================================*/

__attribute__((section("ddr_func_ptr"))) ddr_func ddr_func_tbl;

__attribute__((section("ddrsns_struct"))) DDR_STRUCT ddrsns_struct;
DDR_STRUCT *ddrsns_share_data = &(ddrsns_struct);

/* DDR interface status, keeps track of active interfaces and their status */
extern ddr_interface_state ddr_status;

extern uint32 ddr_bus_width;

extern ddr_info ddr_physical_size;
extern ddr_size_info ddr_system_size;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/


/* ============================================================================
**  Function : ddr_get_params
** ============================================================================
*/
/**
*   @brief
*   Get DDR device parameters.
*
*   @param[in]  interface_name  Interface to get DDR device parameters for
*
*   @return
*   DDR device parameters
*
*   @dependencies
*   None
*
*   @sa
*   None
*
*   @sa
*   None
*/
ddr_device_params *ddr_get_params(DDR_CHANNEL interface_name)
{
  ddr_device_params *device_params;

  device_params = (interface_name == DDR_CH0) ? &(ddrsns_share_data->cdt_params[0])
                                                        : &(ddrsns_share_data->cdt_params[1]);

  return device_params;
} /* ddr_get_params */

/* =============================================================================
**  Function : sys_debug_bric_settings
** =============================================================================
*/
/**
*   @brief
*   function called to update the BRIC register settings.
*
*   @param [in] None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void sys_debug_bric_settings(void)
{
  uint32 total_ddr_size;

  total_ddr_size = ddrsns_share_data->ddr_size_info.ddr0_cs0_mb
	                      + ddrsns_share_data->ddr_size_info.ddr0_cs1_mb
			      + ddrsns_share_data->ddr_size_info.ddr1_cs0_mb
			      + ddrsns_share_data->ddr_size_info.ddr1_cs1_mb;

  // Disable DDR Slave segments
  HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_A_LOWER,0,0x00000000);
  HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_B_LOWER,0,0x00000000);
  HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_C_LOWER,0,0x00000000);

  // To shrink SNOC to 256 MB
  HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_A_LOWER,1,0xF0000000);
	  
  if (total_ddr_size <= 2048)
  {
     HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_A_LOWER,0,0x80000000);
     HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_A_LOWER,0,0x80000001);
	 HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_MASK_LOWER,0,0x80000000);
	 HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_BASE_LOWER,0,0x80000001);
     HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_OP1,0,0x0F80001);
  }
  else if((ddrsns_share_data->ddr_size_info.ddr0_cs0_mb == 2048) && (ddrsns_share_data->ddr_size_info.ddr0_cs1_mb == 1024))
  {
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_A_LOWER,0,0xC0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_A_LOWER,0,0x40000001);

    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_B_LOWER,0,0x80000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_B_LOWER,0,0x80000001);

	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_MASK_LOWER,0,0xC0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_BASE_LOWER,0,0xC0000001);
    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_OP1,0,0x0F40001);
  }
  else if((ddrsns_share_data->ddr_size_info.ddr0_cs0_mb == 2048) && (ddrsns_share_data->ddr_size_info.ddr0_cs1_mb == 2048))
  {
    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_A_LOWER,0,0x00000001);

    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_B_LOWER,0,0xF0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_B_LOWER,0,0x3);
  }
  else if((ddrsns_share_data->ddr_size_info.ddr0_cs0_mb == 1536) && (ddrsns_share_data->ddr_size_info.ddr0_cs1_mb == 1536))
  {
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_A_LOWER,0,0xC0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_A_LOWER,0,0x40000001);

    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_MASK_B_LOWER,0,0x80000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_SEGMENTN_ADDR_BASE_B_LOWER,0,0x80000001);

	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_MASK_LOWER,0,0xE0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_BASE_LOWER,0,0x60000001);
    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_OP1,0,0x0FA0001);

    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_MASK_LOWER,1,0xE0000000);
	HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_ADDR_BASE_LOWER,1,0xE0000001);
    HWIO_OUTXI(0x00402000, BRIC_GLOBAL2_BRIC_REMAPN_OP1,1,0x0F40001); 
   }
}


/* =============================================================================
**  Function : ddr_restore_from_wdog_reset
** =============================================================================
*/
/**
*   @brief
*   function called to take ddr out of self refresh following a wdog reset
*
*   @param [in] clk_speed_khz  clock speed of ddr in khz
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_restore_from_wdog_reset( uint32 clk_speed_khz )
{
  struct ddr_device_params_common *ddr_params_ch0, *ddr_params_ch1;
  DDR_CHIPSELECT ch0_chip_sel = DDR_CS_NONE, ch1_chip_sel = DDR_CS_NONE;

  /* Get DDR device parameters */
  ddr_params_ch0 = &(ddr_get_params(DDR_CH0)->common);
  ddr_params_ch1 = &(ddr_get_params(DDR_CH1)->common);
  
  if(ddr_params_ch0->num_banks_cs0 != 0)
  {
    ch0_chip_sel |= DDR_CS0;
  }
  if(ddr_params_ch0->num_banks_cs1 != 0)
  {
    ch0_chip_sel |= DDR_CS1;
  }
  if(ddr_params_ch1->num_banks_cs0 != 0)
  {
    ch1_chip_sel |= DDR_CS0;
  }
  if(ddr_params_ch1->num_banks_cs1 != 0)
  {
    ch1_chip_sel |= DDR_CS1;
  }
  /* reset to APSS view */
  ddrsns_share_data->base_addr.bimc_base_addr = EBI1_BIMC_BASE;
  ddrsns_share_data->base_addr.ddrss_base_addr = EBI1_PHY_CFG_BASE;

  if(ch0_chip_sel != SDRAM_CS_NONE)
  {	
    HAL_DDR_Init_SDI(ddrsns_share_data, DDR_CH0);
  }
  /* BRIC Register settings */
  sys_debug_bric_settings();  
}

// =============================================================================
//  Function : HAL_DDR_Get_DDR_Struct_Addr
// =============================================================================
void* HAL_DDR_Get_DDR_Struct_Addr()
{
	uint32 func_addr = (uint32) &HAL_DDR_Get_DDR_Struct_Addr;
	void *addr = (void*) SCL_SYSTEM_DDR_DATA_BASE;
	if(func_addr & SCL_RPM_CODE_RAM_BASE)
		return addr;
   else
      return (void*)((uint8*)addr - SCL_RPM_CODE_RAM_BASE);
}

/* =============================================================================
**  Function : HAL_DDR_Get_CDT_ptr
** =============================================================================
*/
/**
*   @brief
*   To get CDT params pointer in RPM
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
ddr_device_params* HAL_DDR_Get_CDT_ptr()
{
	uint32 func_addr = (uint32)&HAL_DDR_Get_CDT_ptr;
	ddr_device_params* addr = ((DDR_STRUCT *)SCL_SYSTEM_DDR_DATA_BASE)->cdt_params;
	if(func_addr & SCL_RPM_CODE_RAM_BASE)
		return addr;
   else
      return (ddr_device_params*)((uint8 *)addr - SCL_RPM_CODE_RAM_BASE);
}

/* =============================================================================
**  Function : HAL_DDR_Get_Status_Reg_Addr
** =============================================================================
*/
/*
*   @brief
*   To Get list of status register addresses to be dumped during
*   abort, Pre/post clock switch from RPM
*
*   @retval  Array of register addresses
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void* HAL_DDR_Get_Status_Reg_Addr()
{
    uint32 func_addr = (uint32)&HAL_DDR_Get_Status_Reg_Addr;
	void *addr = (((DDR_STRUCT *)SCL_SYSTEM_DDR_DATA_BASE)->status_reg_addr);
	if(func_addr & SCL_RPM_CODE_RAM_BASE)
	  return addr;
    else
      return (void *)((uint8 *)addr - SCL_RPM_CODE_RAM_BASE);
}

 

