#ifndef __MPM_HWIO_H__
#define __MPM_HWIO_H__
/*
===========================================================================
*/
/**
  @file mpm_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8953 (Jacala) [jacala_v1.1_p3q3r2]
 
  This file contains HWIO register definitions for the following modules:
    MPM2_PSHOLD

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/msm8953/mpm_hwio.h#2 $
  $DateTime: 2016/02/24 02:43:57 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: MPM2_PSHOLD
 *--------------------------------------------------------------------------*/

#define MPM2_PSHOLD_REG_BASE                                                    (MPM2_MPM_BASE      + 0x0000b000)
#define MPM2_PSHOLD_REG_BASE_PHYS                                               (MPM2_MPM_BASE_PHYS + 0x0000b000)
#define MPM2_PSHOLD_REG_BASE_OFFS                                               0x0000b000

#define HWIO_MPM2_MPM_PS_HOLD_ADDR(x)                                           ((x) + 0x00000000)
#define HWIO_MPM2_MPM_PS_HOLD_PHYS(x)                                           ((x) + 0x00000000)
#define HWIO_MPM2_MPM_PS_HOLD_OFFS                                              (0x00000000)
#define HWIO_MPM2_MPM_PS_HOLD_RMSK                                                     0x1
#define HWIO_MPM2_MPM_PS_HOLD_IN(x)      \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR(x), HWIO_MPM2_MPM_PS_HOLD_RMSK)
#define HWIO_MPM2_MPM_PS_HOLD_INM(x, m)      \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR(x), m)
#define HWIO_MPM2_MPM_PS_HOLD_OUT(x, v)      \
        out_dword(HWIO_MPM2_MPM_PS_HOLD_ADDR(x),v)
#define HWIO_MPM2_MPM_PS_HOLD_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PS_HOLD_ADDR(x),m,v,HWIO_MPM2_MPM_PS_HOLD_IN(x))
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_BMSK                                              0x1
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_SHFT                                              0x0

#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR(x)                             ((x) + 0x00000004)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_PHYS(x)                             ((x) + 0x00000004)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OFFS                                (0x00000004)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK                                       0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN(x)      \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR(x), HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_INM(x, m)      \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR(x), m)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUT(x, v)      \
        out_dword(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR(x),v)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR(x),m,v,HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN(x))
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_BMSK                 0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_SHFT                 0x0

#define HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR(x)                                     ((x) + 0x00000008)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_PHYS(x)                                     ((x) + 0x00000008)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OFFS                                        (0x00000008)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_IN(x)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR(x), HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_INM(x, m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR(x), m)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUT(x, v)      \
        out_dword(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR(x),v)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR(x),m,v,HWIO_MPM2_MPM_SSCAON_CONFIG_IN(x))
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_SHFT                                 0x0

#define HWIO_MPM2_MPM_SSCAON_STATUS_ADDR(x)                                     ((x) + 0x0000000c)
#define HWIO_MPM2_MPM_SSCAON_STATUS_PHYS(x)                                     ((x) + 0x0000000c)
#define HWIO_MPM2_MPM_SSCAON_STATUS_OFFS                                        (0x0000000c)
#define HWIO_MPM2_MPM_SSCAON_STATUS_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_IN(x)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR(x), HWIO_MPM2_MPM_SSCAON_STATUS_RMSK)
#define HWIO_MPM2_MPM_SSCAON_STATUS_INM(x, m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR(x), m)
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_SHFT                                 0x0


#endif /* __MPM_HWIO_H__ */
