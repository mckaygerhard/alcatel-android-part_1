/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_wrlvl.c#2 $
$DateTime: 2016/05/18 04:11:19 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

/***************************************************************************************************
   Description: The DDRSS Write Leveling Routine performs the LPDDR4 write leveling function.
   Write Leveling is used to determine the skew at the DRAM device between the CK and DQS strobe.
   Training for cs (rank) 1 is not supported.  The trained cs 0 values are populated to 
   rank 1 when rank 0 is trained.   
 
***************************************************************************************************/
#include "ddrss.h"

void DDRSS_wrlvl( DDR_STRUCT *ddr, 
                  uint8 ch, 
                  uint8 cs, 
                  uint32 clk_freq_khz, 
                  uint8  wrlvl_clk_freq_idx,
                  training_params_t *training_params_ptr)
{  

  //============================================================
  // WRLVL Main Code Thread
  //============================================================

  //-------------------------------------------------------------------------------------
  // Check for CLK->DQS negative skew and update CA PHY WRLVL CDCs
  //-------------------------------------------------------------------------------------

  // CA is called by the ddrss_boot_training_lpddr3 outer loop
  //
  //DDRSS_wrlvl_ca(ddr,ch,cs,clk_freq_khz,training_params_ptr, wrlvl_clk_freq_idx);

  //-------------------------------------------------------------------------------------
  // Loop and repeat the Write Leveling procedure until all DQ PHYs are done or error
  //-------------------------------------------------------------------------------------

  DDRSS_wrlvl_dqs(ddr,ch,cs,clk_freq_khz,training_params_ptr,wrlvl_clk_freq_idx);

  //-------------------------------------------------------------------------------------
  // Scale the WRLVL parameters to lower frequencies
  //-------------------------------------------------------------------------------------

  //DDRSS_wrlvl_scale(ddr,ch,cs,clk_freq_khz);

} // DDR_PHY_wrlvl


void DDRSS_wrlvl_ca(DDR_STRUCT *ddr,
                    uint8 ch, 
                    uint8 cs, 
                    uint32 clk_freq_khz, 
                    training_params_t *training_params_ptr, 
                    uint8  wrlvl_clk_freq_idx )
{
  //-------------------------------------------------------------------------------------
  // Check for DQS->CLK skew. If the CLK is early, add delay to the CA PHY WRLVL CDC
  // There are two DQ PHYs for each CA PHY.  Both DQ PHYs for each CA PHY must start with
  // a zero (fail) Write Level value.  
  //-------------------------------------------------------------------------------------

  uint8  wrlvl_ca_done                 = 0;  // CA wrlvl routine status
  uint8  bisc_status                   = 0;  // Device WRLVL result
  uint8  feedback_percent              = 0;  // Local feedback percent
  uint8  ca                            = 0;  // CA Loop counter
  uint8  loopcnt                       = 0;  // CA Loop counter
  uint8  dq                            = 0;  // DQ Loop counter
  uint8  done                          = 0;  // Loop done counter
  uint16 period                        = 0;  // Period calculated from frequency
  uint16 T4                            = 0;  // 1/4 Period
  uint16 T34                           = 0;  // 3/4 Period
  uint8  wrlvl_max_coarse_cdc          = 0;  // Coarse CDC Saturation Value
  uint8  wrlvl_max_loopcnt             = 0;  // Coarse CDC Saturation Value
  uint8  curr_coarse                   = 0;
  uint8  curr_fine                     = 0;
  uint8  curr_cdc_value                = 0;
  
  uint8     byte_one_cnt[NUM_DQ_PCH]          = {0};
  uint8    byte_zero_cnt[NUM_DQ_PCH]          = {0};
  uint8     byte_ca_done[NUM_DQ_PCH]          = {0};   // CA status of the dq bytes
  uint8         ca_delay[NUM_CA_4WRLVL_PCH]   = {0};   // CA Write level coarse delay
  uint8         ca_retmr[NUM_CA_4WRLVL_PCH]   = {0};   // CA retmr value
  
  uint32 reg_offset_dpe                   = 0;     // BIMC DPE
  uint16 ca_delay_ps                      = 0;     // Coarse delay in picoseconds
  uint8 clk_idx                           = 0;
  uint8 current_clk_inx                   = 0;
  
    uint32 reg_offset_ddr_ca[NUM_CA_PCH]  = {0}; 
    uint32 reg_offset_ddr_dq[NUM_DQ_PCH]  = {0};

  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
   // Set DDR Base address registers
    reg_offset_ddr_ca[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_ca[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[2]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[3]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;
  // Set DQ0 PHY BASE for the channel and cs
  reg_offset_dpe    = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
  // Set CDC Saturation Limits from the training params data structure
  wrlvl_max_coarse_cdc   = training_params_ptr->wrlvl.max_coarse_cdc;
  wrlvl_max_loopcnt      = training_params_ptr->wrlvl.max_loopcnt;
  // Calculate the period based on the start frequency
  // period = 1000000000/clk_freq_khz;
  period = ddrss_lib_uidiv(1000000000, clk_freq_khz);
  // Calculate T4, T3/4
//T4  = period / 4;
  T4  = ddrss_lib_uidiv(period , 4);
  T34 = (3 * T4);
  // Calculate the current clock index
  for (clk_idx = ddrss_lib_uidiv(sizeof(freq_range),4); clk_idx > 0; clk_idx--)
  {
     if (clk_freq_khz >= freq_range[clk_idx])
        break;
  }
  current_clk_inx = clk_idx + 1; 
  // Turn on continuous GCC clock per channel (turned on during training by default)
  HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
  HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 1);
  for (ca=0;ca<NUM_CA_4WRLVL_PCH;ca++) 
  {
   // Enable software override to DQS/DQ pad OEs.
    for (dq = 0; dq < NUM_DQ_PCH; dq++) 
    {
      // Turn on continuous DDRCC clock (per byte lane to drive the DQS pulses)
      HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG     , DISABLE_PHY       , 0);
      HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG     , DISABLE_PHY_BYPASS, 1);
      HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG     , TRAFFIC_CGC_EN    , 1);
#ifdef TARGET_8992_COMPATIBLE
#else
      HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG    , BIST_TOP_CGC_EN   , 1);     
#endif
      // Enable PAD SW override 
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG    , SW_PAD_MODE_DQS    , 1);
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG    , SW_PAD_MODE_DQ     , 0x3FF);
      // Disable PAD OE 
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQS, 0);
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQ , 0);
      //select BIST in datapath
      HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG     , WR_DP_SRC_SEL      ,  0x1);
      // Enable DQS PAD OE 
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQS, 1);
      // Enable DQ PAD IE
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG      , SW_PAD_MODE_IE_DQ,   0x3FF);
      HWIO_OUTXF (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG      , SW_PAD_ENABLE_IE_DQ, 0x3FF);
      // Set SW Enable for DQS pulses
      HWIO_OUTX  (reg_offset_ddr_dq[dq],DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x5);
    }
    // Initialize CA Rank 0 WRLVL CDC to zero delay
    DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_ca[ca],
                               0, 
                               1, // coarse
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_ca[ca],
                               0, 
                               0, // fine
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_wrlvl_retmr(reg_offset_ddr_ca[ca],
                                0, // cs
                                0, //half cycle
                                0,
                                0);// retmr

  // Loop until all DQs are done or error
  while (wrlvl_ca_done < NUM_DQ_PCH) 
  {
    // Read the bisc_status (Write Level result) from each DQ PHY pair
    for (dq=0;dq<NUM_DQ_PCH;dq++) 
    {
      if (ca_delay[ca] >= wrlvl_max_coarse_cdc) 
      {
        byte_ca_done[dq] = 1;
      }
      else 
      {
        if (byte_ca_done[dq] == 0) 
        {
          byte_one_cnt[dq]  = 0;
          byte_zero_cnt[dq] = 0;

          for (loopcnt = wrlvl_max_loopcnt;loopcnt > 0;loopcnt-- ) 
          {
            // Send two DQS pulses to all DQ PHYs
            HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);
            HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x1);     
            HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);     

            bisc_status =  DDR_PHY_hal_sta_wrlvl_training(reg_offset_ddr_dq[dq]);
  
            // Collapse the status to a single bit
            bisc_status = (bisc_status & 0x00000001);

            //  Check the bisc status and increment the one or zero count
            if (bisc_status == 1) 
            {
               byte_one_cnt[dq]++;
            } 
            else 
            {
               byte_zero_cnt[dq]++;
            }
          }
          // Calculate the threshold in percent for Write Level pass (histogram)
          if ((byte_one_cnt[dq] + byte_zero_cnt[dq]) != 0) 
          {
            feedback_percent =  ddrss_lib_uidiv((byte_one_cnt[dq] * 100) , (byte_one_cnt[dq] + byte_zero_cnt[dq]));
          }
          else 
          {
            feedback_percent = 0;
          }

          // Check bisc status.  If equal to one (pass), increment the CA WRLVL delay (CA CLK and Data)
          if (feedback_percent != 0) 
          {
            ca_delay[ca]++;
  
            DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_ca[ca],
                                           ca_delay[ca], 
                                           1, // coarse
                                           1, // hp_mode
                                           0 );

										   
//=================================================================================================										   
  curr_cdc_value = HWIO_INX ( reg_offset_ddr_ca[0] , DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ) ;
    // Parse the CA CDC for the safe mode
  curr_coarse =  curr_cdc_value        & 0x1F;
  curr_fine   = (curr_cdc_value >> 5 ) & 0x1F;
  //curr_coarse =  curr_coarse;
  //curr_fine   =  curr_fine  ;
//=================================================================================================										   
										   
										   
										   
            // Check and update the retmr in the CA 
            ca_delay_ps = ( ca_delay[ca] + curr_coarse ) * COARSE_STEP_IN_PS + (curr_fine*FINE_STEP_IN_PS) ;
          
            // Increment the retimer when the delay exceeds the T/4 period boundaries
            if (ca_delay_ps <= T4) 
            {
              ca_retmr[ca] = retimer_map[0];
            }
            else if ((ca_delay_ps > T4) && (ca_delay_ps <= T34)) 
            {
              ca_retmr[ca]  = retimer_map[1];
            }
            else if (ca_delay_ps  > T34) 
            {
              ca_retmr[ca] = retimer_map[2];
            }
            DDR_PHY_hal_cfg_wrlvl_retmr(reg_offset_ddr_ca[ca],
                                        0,
                                        ca_retmr[ca],
                                        0, // half delay
                                        0); //full delay
          }
          else 
          {
           byte_ca_done[dq] = 1;
          }
        } // if byte_ca_done
      } // else byte_ca_done
    } // for dq

    // Aggregate all of the byte status to determine the ca done state
    wrlvl_ca_done = 0;
    for (done = 0; done < NUM_DQ_PCH; done++) 
    {
      wrlvl_ca_done = wrlvl_ca_done + byte_ca_done[done];
    }
  } // while wrlvl_ca_done
  // De activate WRLVL test modes 
  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Disable PAD OE 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQ , 0);
    //De-select BIST in datapath                                           
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , WR_DP_SRC_SEL       , 0);
    // Disable DQS PAD OE                                                  
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG      , SW_PAD_ENABLE_OE_DQS, 0);
    // Disable DQ PAD IE                                                   
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG      , SW_PAD_MODE_IE_DQ   , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG      , SW_PAD_ENABLE_IE_DQ , 0);
    // Disble WRLVL pulse control
    HWIO_OUTX  (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x0);
    // Disable PAD SW override 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG    , SW_PAD_MODE_DQS     , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG    , SW_PAD_MODE_DQ      , 0);
    // Turn off continuous DDRCC clock (per byte lane)                     
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , DISABLE_PHY         , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , DISABLE_PHY_BYPASS  , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , TRAFFIC_CGC_EN      , 0);
#ifdef TARGET_8992_COMPATIBLE                                              
#else
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG     , BIST_TOP_CGC_EN     , 0);     
#endif

  }
    
  } // for (ca 
	
	





  for (ca=0;ca<NUM_CA_PCH;ca++) 
  {
    // Save the trained WRLVL
    DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_ca[ca],
                               ca_delay[0], 
                               1, // coarse
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_wrlvl_retmr(reg_offset_ddr_ca[ca],
                                0, // cs
                                ca_retmr[0],
                                0,//half cycle
                                0);// full_cycle


    // Write trained CA WRLVL values to the ext registers
    DDR_PHY_hal_cfg_wrlvlext_ctl_update(reg_offset_ddr_ca[ca], 
                                        current_clk_inx, 
                                        0,  //cs 
                                        ca_retmr[0], 
                                        0,  // half cycle
                                        0); // full cycle

    DDR_PHY_hal_cfg_cdcext_wrlvl_update(reg_offset_ddr_ca[ca],
                                        current_clk_inx, 
                                        0, //cs
                                        0, //fine
                                        ca_delay[0]); //coarse

    //  Store register values into the training data structure
    training_data_ptr->results.wrlvl.ca_dqs_retmr       [wrlvl_clk_freq_idx][ch][0][ca] = ca_retmr[0];
    training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][ca] = ca_delay[0];
    training_data_ptr->results.wrlvl.ca_dqs_half_cycle  [wrlvl_clk_freq_idx][ch][0][ca] = 0;
    training_data_ptr->results.wrlvl.ca_dqs_full_cycle  [wrlvl_clk_freq_idx][ch][0][ca] = 0;

    ddr_printf(DDR_NORMAL,"    WRLVL ch %d rank %d CA %d coarse = %d\n",ch,cs,ca,ca_delay[0]);

  } // for (ca 













  ddr_printf(DDR_NORMAL,"\n");

  // Turn off continuous GCC clock per channel (turned on during training by default)
    HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);
    HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 1);

}// DDRSS_wrlvl_ca


void DDRSS_wrlvl_dqs(DDR_STRUCT *ddr, 
                     uint8 ch, 
                     uint8 cs, 
                     uint32 clk_freq_khz, 
                     training_params_t *training_params_ptr, 
                     uint8  wrlvl_clk_freq_idx)
{
  //-----------------------------------------------------------------------------
  //  For the selected frequency, measure dqs->clk delay (wrlvl_max_loopcnt) 
  //  times for each wrlvl delay value. if the one count
  //  is greater than the zero count, the wrlvl training is finished.
  //  If not, the coarse CDC register is incremented. When the coarse CDC passes, 
  //  it is decremented and the loop repeats and increments the fine CDC delay.
  //  This loop is kept continued until either the one count is bigger than the
  //  zero count or when the wrlvl delay value reaches its maximum limit (MAX_CDC).
  //--------------------------------------------------------------------------------------

  // Local Variables
  uint8  dq                           = 0;     // dq loop counter
  uint8  done                         = 0;     // done loop counter
  uint8  loopcnt                      = 0;     // number of dq measurements
  uint8  feedback_percent             = 50;    // Percent of feedback ones in sample
  uint16 coarse_dqs_delay_ps          = 0;     // Write level coarse delay in ps
  uint16 fine_dqs_delay_ps            = 0;     // Write Level fine delay in ps
  uint8  wrlvl_dqs_done               = 0;     // Status of the wrlvl routine
  uint8  bisc_status                  = 0;     // Status of the wrlvl result

  uint16     byte_one_cnt[NUM_DQ_PCH] = {0};   // Count of 1 in dq0 measurement
  uint16    byte_zero_cnt[NUM_DQ_PCH] = {0};   // Count of 0 in dq0 measurement
  uint8  fine_dqs_started[NUM_DQ_PCH] = {0};   // Status of the dq bytes
  uint8     byte_dqs_done[NUM_DQ_PCH] = {0};   // Status of the dq bytes
  uint8    fine_dqs_delay[NUM_DQ_PCH] = {0};   // Write Level fine delay
  uint8  coarse_dqs_delay[NUM_DQ_PCH] = {0};   // Write level coarse delay
  uint8   coarse_dqs_done[NUM_DQ_PCH] = {0};   // Write level coarse delay status
  uint8         dqs_retmr[NUM_DQ_PCH] = {0};   // Local dqs re-timer variable
  
  uint8  wrlvl_max_loopcnt            = 0;     // Maximum measurement loop count
  uint8  wrlvl_max_coarse_cdc         = 0;     // Coarse CDC saturation limit
  uint8  wrlvl_max_fine_cdc           = 0;     // Fine CDC saturation limit
  uint8  wrlvl_feedback_percent       = 0;     // Feedback histogram pass percent
  uint16 period                       = 0;     // Period derived from the frequency
  uint16 T4                           = 0;     // 1/4 Period
  uint16 T34                          = 0;     // 3/4 Period
  uint32 reg_offset_dpe               = 0;     // BIMC DPE
  uint8  wrlvl_coarse_cdc_step        = 0;     // Coarse CDC step
  uint8  wrlvl_fine_cdc_step          = 0;     // Fine CDC step
  uint8 clk_idx                        = 0;
  uint8 current_clk_inx                = 0;
    uint32 reg_offset_ddr_dq[NUM_DQ_PCH]  = {0};

  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

 // Set DQ0 PHY BASE for the channel and cs
  reg_offset_dpe    = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
   // Set DDR Base address registers
    reg_offset_ddr_dq[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[2]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[3]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;

  // Calculate the period based on the start frequency
  // period = 1000000000/clk_freq_khz;
  period = ddrss_lib_uidiv(1000000000, clk_freq_khz);

  // Calculate T/4 and 3T/4
  T4  =      ddrss_lib_uidiv(period  , 4);
  T34 = (3 * T4);

  // Calculate the current clock index
  for (clk_idx = ddrss_lib_uidiv(sizeof(freq_range),4); clk_idx > 0; clk_idx--)
  {
     if (clk_freq_khz >= freq_range[clk_idx])
        break;
  }
  current_clk_inx = clk_idx + 1; 

  wrlvl_max_loopcnt      = training_params_ptr->wrlvl.max_loopcnt;
  wrlvl_max_coarse_cdc   = training_params_ptr->wrlvl.max_coarse_cdc;
  wrlvl_max_fine_cdc     = training_params_ptr->wrlvl.max_fine_cdc;
  wrlvl_feedback_percent = training_params_ptr->wrlvl.feedback_percent;
  wrlvl_coarse_cdc_step  = training_params_ptr->wrlvl.coarse_cdc_step;
  wrlvl_fine_cdc_step    = training_params_ptr->wrlvl.fine_cdc_step;
  
  // Turn on continuous GCC clock per channel (turned on during training by default)
    HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
    HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 1);

 // Enable software override to DQS/DQ pad OEs.
  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Turn on continuous DDRCC clock (per byte lane)
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , DISABLE_PHY        , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , DISABLE_PHY_BYPASS , 1);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG      , TRAFFIC_CGC_EN     , 1);
#ifdef TARGET_8992_COMPATIBLE
#else
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG     , BIST_TOP_CGC_EN    , 1);     
#endif
    // Enable PAD SW override 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG   , SW_PAD_MODE_DQS    , 1);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG   , SW_PAD_MODE_DQ     , 0x3FF);
    // Disable PAD OE 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG     , SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG     , SW_PAD_ENABLE_OE_DQ , 0);
    //select BIST in datapath                                             
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG     , WR_DP_SRC_SEL       , 0x1);
    // Enable DQS PAD OE                                                  
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG     , SW_PAD_ENABLE_OE_DQS, 1);
    // Enable DQ PAD IE                                                   
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG     , SW_PAD_MODE_IE_DQ   , 0x3FF);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG     , SW_PAD_ENABLE_IE_DQ , 0x3FF);
    // Set SW Enable for DQS pulses
    HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x5);
  }

  // Loop through each DQ until Done or Error
  while (wrlvl_dqs_done < NUM_DQ_PCH) 
  {
         
    // Setup all of the DQ PHYs
    for (dq = 0; dq < NUM_DQ_PCH; dq++) 
    {
      // Update WRLVL CDC
      if (byte_dqs_done[dq] == 0) 
      {
        if (fine_dqs_started[dq]) 
        {
          DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_dq[dq],
                                          fine_dqs_delay[dq], 
                                          0, // fine 
                                          1, // hp_mode
                                          cs );
        }
        else 
        {
          DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_dq[dq],
                                        coarse_dqs_delay[dq], 
                                        1, // coarse 
                                        1, // hp_mode
                                        cs );
        }

        coarse_dqs_delay_ps = coarse_dqs_delay[dq] * COARSE_STEP_IN_PS;
        fine_dqs_delay_ps   = fine_dqs_delay[dq]   * FINE_STEP_IN_PS;

        // Increment the retimer when the delay exceeds the T/4 period boundaries
        if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T4) 
        {
          dqs_retmr[dq] = retimer_map[0];
        }
        else if (((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T4) && ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T34)) 
        {
          dqs_retmr[dq]  = retimer_map[1];
        }
        else if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T34) 
        {
          dqs_retmr[dq] = retimer_map[2];
        }

        DDR_PHY_hal_cfg_wrlvl_retmr(reg_offset_ddr_dq[dq],
                                    cs,
                                    dqs_retmr[dq],
                                    0,//half cycle
                                    0);// full_cycle
      }

      //  Repeat the WRLVL test for max_loopcount to make a 1 dimensional histogram
      for (loopcnt = wrlvl_max_loopcnt;loopcnt > 0;loopcnt-- ) 
      {
        // Update the status of bytes that are not done
        if (byte_dqs_done[dq] == 0) 
        {
          // Send two Write Level DQS pulses to all PHYs
          HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);
          HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x1);          
          HWIO_OUTX (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);          

          bisc_status = DDR_PHY_hal_sta_wrlvl_training(reg_offset_ddr_dq[dq]);
        
          // Collapse the status to a single bit
          bisc_status = (bisc_status & 0x00000001);

          //  Check the bisc status and increment the one or zero count
          if (bisc_status == 1) 
          {
             byte_one_cnt[dq]++;
          } 
          else 
          {
             byte_zero_cnt[dq]++;
          }

        } // if (!byte_dqs_done
      } // for (loopcnt
    } //  End of loop for a given wrlvl delay value

    
    //=========================================================================
    // Process the Write Level Loop Result
    //=========================================================================
    for (dq = 0; dq < NUM_DQ_PCH; dq++) 
    {
      //-------------------------------------------------------------------------
      // Write Level Result Process Flow Control
      //-------------------------------------------------------------------------

      // Process the DQ if it is not done
      if (byte_dqs_done[dq] == 0) 
      {
      // Calculate the threshold in percent for Write Level pass (histogram)
        if ((byte_one_cnt[dq] + byte_zero_cnt[dq]) != 0) 
        {
          feedback_percent =  ddrss_lib_uidiv((byte_one_cnt[dq] * 100) , (byte_one_cnt[dq] + byte_zero_cnt[dq]));
        }
        else 
        {
          feedback_percent = 0;
        }
  
        // Generate an Error and terminate if CDC overflows
        if ((coarse_dqs_delay[dq] >= (wrlvl_max_coarse_cdc - 1)) && (fine_dqs_delay[dq] >= wrlvl_max_fine_cdc)) 
        {
          byte_dqs_done[dq]   = 1;
        }
        // Coarse completed and the fine one count is greater than the zero count: wrlvl is complete
        else if ((coarse_dqs_done[dq] == 1) && (fine_dqs_started[dq] == 1) && 
                 (feedback_percent >= wrlvl_feedback_percent)) 
        {
          byte_dqs_done[dq] = 1;
        }
        // Course is complete and fine has not started : subtract one from coarse and start fine
        else if ((fine_dqs_started[dq] == 0) && 
                 ((feedback_percent >= wrlvl_feedback_percent) || (coarse_dqs_delay[dq] == wrlvl_max_coarse_cdc))) 
        {
          coarse_dqs_done[dq] = 1;
          fine_dqs_started[dq] = 1;
          if (coarse_dqs_delay[dq] != 0) 
          {
            coarse_dqs_delay[dq] -= wrlvl_coarse_cdc_step;

            // Update decremented coarse CDC
            DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_dq[dq],
                                          coarse_dqs_delay[dq], 
                                          1, // coarse 
                                          1, // hp_mode
                                          cs );
          }
        }
        // Coarse is complete and the one count is NOT greater than the zero count : increment fine
        else if ((coarse_dqs_done[dq] == 1) && 
                 (feedback_percent < wrlvl_feedback_percent)) 
        {
          fine_dqs_delay[dq] += wrlvl_fine_cdc_step;
        }
        else 
        {
        // Increment coarse delay 
          coarse_dqs_delay[dq] += wrlvl_coarse_cdc_step;
        } 


      }// if !byte_dqs_done

      // Reset byte counters for the next iteration
      byte_one_cnt[dq]  = 0;
      byte_zero_cnt[dq] = 0;

    } // for (dq

    // Aggregate all of the byte status to determine the done state
    wrlvl_dqs_done = 0;
    for (done = 0; done < NUM_DQ_PCH; done++) 
    {
      wrlvl_dqs_done = wrlvl_dqs_done + byte_dqs_done[done];
    }

  } // while (wrlvl_dqs_done<) 

  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Write trained WRLVL values to the ext registers
    DDR_PHY_hal_cfg_wrlvlext_ctl_update(reg_offset_ddr_dq[dq], 
                                        current_clk_inx, 
                                        cs, 
                                        dqs_retmr[dq], 
                                        0,  // half cycle
                                        0); // full cycle

    DDR_PHY_hal_cfg_cdcext_wrlvl_update(reg_offset_ddr_dq[dq],
                                        current_clk_inx, 
                                        cs,
                                        fine_dqs_delay[dq],
                                        coarse_dqs_delay[dq]);


    // Write the trained WRLVL registers
    DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_dq[dq],
                                    fine_dqs_delay[dq], 
                                    0, // fine 
                                    1, // hp_mode
                                    cs );

    DDR_PHY_hal_cfg_cdc_slave_wrlvl(reg_offset_ddr_dq[dq],
                                    coarse_dqs_delay[dq], 
                                    1, // coarse 
                                    1, // hp_mode
                                    cs );

    //  Store register values into the training data structure
    training_data_ptr->results.wrlvl.dq_dqs_retmr       [wrlvl_clk_freq_idx][ch][cs][dq]   = dqs_retmr[dq];
    training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][cs][dq]   = coarse_dqs_delay[dq];
    training_data_ptr->results.wrlvl.dq_fine_dqs_delay  [wrlvl_clk_freq_idx][ch][cs][dq]   = fine_dqs_delay[dq];
    training_data_ptr->results.wrlvl.dq_dqs_half_cycle  [wrlvl_clk_freq_idx][ch][cs][dq]   = 0;
    training_data_ptr->results.wrlvl.dq_dqs_full_cycle  [wrlvl_clk_freq_idx][ch][cs][dq]   = 0;

    // Disable PAD OE 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG  , SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG  , SW_PAD_ENABLE_OE_DQ , 0);
    //De-select BIST in datapath                                       
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG  , WR_DP_SRC_SEL       , 0x0);
    // Disable DQ PAD IE                                               
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG  , SW_PAD_MODE_IE_DQ   , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG  , SW_PAD_ENABLE_IE_DQ , 0);
    // Disble WRLVL pulse control
    HWIO_OUTX  (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x0);
    // Disable PAD SW override 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQS   , 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQ    , 0);
    // Turn off continuous DDRCC clock (per byte lane)
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG  , DISABLE_PHY          , 0); 
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG  , DISABLE_PHY_BYPASS, 0);
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG  , TRAFFIC_CGC_EN, 0); 

#ifdef TARGET_8992_COMPATIBLE
#else
    HWIO_OUTXF (reg_offset_ddr_dq[dq], DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG, BIST_TOP_CGC_EN, 0);     
#endif

    ddr_printf(DDR_NORMAL,"    WRLVL ch %d rank %d DQ %d coarse = %d  fine = %d\n",ch,cs,dq,coarse_dqs_delay[dq],fine_dqs_delay[dq]);
  } // for (dq
  ddr_printf(DDR_NORMAL,"\n");

  // Turn off continuous GCC clock per channel (turned on during training by default)
    HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);
    HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 1);

} // DDRSS_wrlvl_dqs
    

void DDRSS_wrlvl_scale(DDR_STRUCT *ddr,
                             uint8 ch, 
                             uint8 cs, 
                             uint32 clk_freq_khz)
{
//---------------------------------------------------------------------
// Scale the WRLVL results to lower frequencies
// Requires extension of the data structure for save/restore of 
// additional frequencies
//---------------------------------------------------------------------

  uint8   dq                      = 0;// loop counter
  uint8   ca                      = 0;// loop counter
  uint8   scaling_wrlvl_clk_freq_idx = 0;
  uint8   retimer_cal             = 0;
  uint8   final_ca_coarse_delay   = 0;
  uint8   final_dq_coarse_delay   = 0;
  uint8   final_dq_fine_delay     = 0;
  uint32  f_range                 = 0;
  uint32  period                  = 0;
  uint8 clk_idx                   = 0;
  uint8 current_clk_inx           = 0;
    uint32 reg_offset_ddr_ca[NUM_CA_PCH]  = {0}; 
    uint32 reg_offset_ddr_dq[NUM_DQ_PCH]  = {0};
  
  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
    // Set DDR Base address registers
    reg_offset_ddr_ca[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_ca[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[2]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[3]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;


  // Calculate the current clock index
  for (clk_idx = ddrss_lib_uidiv(sizeof(freq_range),4); clk_idx > 0; clk_idx--)
  {
     if (clk_freq_khz >= freq_range[clk_idx])
        break;
  }
  current_clk_inx = clk_idx + 1; 

    for (ca=0;ca<NUM_CA_4WRLVL_PCH;ca++) 
    {
      //Scale WRLVL results
      for (scaling_wrlvl_clk_freq_idx =0; scaling_wrlvl_clk_freq_idx < current_clk_inx; scaling_wrlvl_clk_freq_idx++)
      {
        // Load the ca wrlvl delay from the data structure
        final_ca_coarse_delay = training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[scaling_wrlvl_clk_freq_idx][ch][0][ca];

        DDR_PHY_hal_cfg_cdcext_wrlvl_update(reg_offset_ddr_ca[ca],
                                                                    scaling_wrlvl_clk_freq_idx, 
                                                                    0, //cs
                                                                    0, //fine
                                                                    final_ca_coarse_delay); //coarse

        f_range = DDRSS_Get_Freq_Range (ddr, scaling_wrlvl_clk_freq_idx);                                                       

        period = CONVERT_CYC_TO_PS / f_range;
            
        retimer_cal = DDRSS_Retimer_Calc(ddr, final_ca_coarse_delay, 0, period);
            
        training_data_ptr->results.wrlvl.ca_dqs_retmr[scaling_wrlvl_clk_freq_idx][ch][0][ca] = retimer_cal;
            
        DDR_PHY_hal_cfg_wrlvlext_retimer_update(reg_offset_ddr_ca[ca], 
                                   scaling_wrlvl_clk_freq_idx, 
                                   0, 
                                   retimer_cal); 
      }
    }

    for (dq=0;dq<NUM_DQ_PCH;dq++) 
    {
      for (scaling_wrlvl_clk_freq_idx =0; scaling_wrlvl_clk_freq_idx < current_clk_inx; scaling_wrlvl_clk_freq_idx++)
      {
        // Load the dq wrlvl delays from the data structure
        final_dq_coarse_delay = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[scaling_wrlvl_clk_freq_idx][ch][cs][dq];
        final_dq_fine_delay   = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[scaling_wrlvl_clk_freq_idx][ch][cs][dq];

        DDR_PHY_hal_cfg_cdcext_wrlvl_update(reg_offset_ddr_dq[dq],
                                                                    scaling_wrlvl_clk_freq_idx, 
                                                                    cs,
                                                                    final_dq_fine_delay, //fine
                                                                    final_dq_coarse_delay); //coarse

        f_range = DDRSS_Get_Freq_Range (ddr, scaling_wrlvl_clk_freq_idx);                                                       

        period = ddrss_lib_uidiv(CONVERT_CYC_TO_PS , f_range);
                
        retimer_cal = DDRSS_Retimer_Calc(ddr, final_dq_coarse_delay, final_dq_fine_delay, period);
                
        training_data_ptr->results.wrlvl.dq_dqs_retmr[scaling_wrlvl_clk_freq_idx][ch][cs][dq] = retimer_cal;
                
        DDR_PHY_hal_cfg_wrlvlext_retimer_update(reg_offset_ddr_dq[dq], 
                                   scaling_wrlvl_clk_freq_idx, 
                                   cs, 
                                   retimer_cal); 
      }
    } // for (dq
} // DDR_PHY_wrlvl_scale

// EOF
