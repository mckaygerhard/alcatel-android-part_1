//===========================================================================
//  Copyright (c) 2014 - 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
//  QUALCOMM Proprietary and Confidential. 
//===========================================================================
////////////////////////////////////////////////////////////////////////////////////////////////

#include "ddr_phy.h"
#include "ddrss.h"


void DDR_PHY_hal_cfg_ca_dq_in( uint32 _inst_ )
{
	uint32 tmp;

	///  program Software IE enables of DQ and DQs pads of DQ1 instance 
	tmp = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG );
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG_SW_PAD_MODE_DQS_BMSK)) | ((0x1u) << HWIO_DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG_SW_PAD_MODE_DQS_SHFT ) ;
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG_SW_PAD_MODE_DQ_BMSK)) | ((0x3FFu) << HWIO_DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG_SW_PAD_MODE_DQ_SHFT ) ;
	HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, tmp );
	///  SW Override for the DQ PAD mode
	tmp = HWIO_INX (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG );
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG_SW_PAD_ENABLE_IE_DQS_BMSK)) | ((0x1u) << HWIO_DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG_SW_PAD_ENABLE_IE_DQS_SHFT ) ;
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG_SW_PAD_ENABLE_IE_DQ_BMSK)) | ((0x3FFu) << HWIO_DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG_SW_PAD_ENABLE_IE_DQ_SHFT ) ;
	HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, tmp );
	///  SW Override for the DQ PAD IE
	///  set DQS/DQSN floating during CA VREF
	tmp = HWIO_INX (_inst_, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG );
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_N_DQS_BMSK)) | ((0x0u) << HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_N_DQS_SHFT ) ;
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_DQS_BMSK)) | ((0x0u) << HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_DQS_SHFT ) ;
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_BMSK)) | ((0x0u) << HWIO_DDR_PHY_DDRPHY_PAD_CNTL_0_CFG_PULL_SHFT ) ;
	HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG, tmp );
	HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x1);
	///  disable FPM to keep the above settings for IO pads
	///  also DQ1 should be in CA_TRAINING_MODE
	HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, CA_TRAINING_MODE, 0x1);
	///  put DQ1 in CA_TRAINING_MODE
	///  set the following CSR to disable ODT
	HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_PAD_DQ_0_CFG, ODT_EN, 0x0);
	HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_PAD_DQS_0_CFG, ODT_EN, 0x0);
	///  enable FPM for frequency switch in the future steps
	HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, FPM_BYPASS, 0x0);
	///  enable FPM
}

void DDR_PHY_hal_cfg_dq_ca_training_entry( uint32 _inst_ )
{
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG   , CA_TRAINING_MODE    , 0x1   );

        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_MODE_IE_DQS  , 0x1   );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_MODE_IE_DQ   , 0x3FF );

        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_ENABLE_IE_DQS, 0x1   );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_ENABLE_IE_DQ , 0x3FF );

        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG , SW_PAD_MODE_DQS     , 0x1   );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG , SW_PAD_MODE_DQ      , 0x3FF );

}
void DDR_PHY_hal_cfg_dq_ca_training_exit( uint32 _inst_ )
{
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG   , CA_TRAINING_MODE    , 0x0 );
                    
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_MODE_IE_DQS  , 0x0 );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_MODE_IE_DQ   , 0x0 );
                    
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_ENABLE_IE_DQS, 0x0 );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG   , SW_PAD_ENABLE_IE_DQ , 0x0 );
                    
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG , SW_PAD_MODE_DQS     , 0x0 );
        HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG , SW_PAD_MODE_DQ      , 0x0 );

}

void DDR_PHY_hal_cfg_ca_ca( uint32 _inst_ )
{
	uint32 tmp;

	///  program CA phy to use wr90 clk and not tmin clk
	tmp = HWIO_INX (_inst_, DDR_PHY_DDRPHY_TOP_CTRL_4_CFG );
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_TOP_CTRL_4_CFG_SW_ODDR_SDR_MODE_EN_BMSK)) | ((0x1u) << HWIO_DDR_PHY_DDRPHY_TOP_CTRL_4_CFG_SW_ODDR_SDR_MODE_EN_SHFT ) ;
	tmp = (tmp & ~(HWIO_DDR_PHY_DDRPHY_TOP_CTRL_4_CFG_SW_ODDR_SDR_MODE_BMSK)) | ((0x0u) << HWIO_DDR_PHY_DDRPHY_TOP_CTRL_4_CFG_SW_ODDR_SDR_MODE_SHFT ) ;
	HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_TOP_CTRL_4_CFG, tmp );
	///  Software SDR mode. 0 = DDR Mode, 1 = SDR Mode
	///  Set initial CA CDC T/4 and per-bit deskew value coarse delay = 0, fine delay 
	///  setting for fine delay of wr CDC, rank 0
	DDR_PHY_hal_cfg_cdc_slave_wr(_inst_,
	                              0x0,
	                              1,
	                              1,
	                              0);
	///  setting for coarse delay of wr CDC, rank 0
	DDR_PHY_hal_cfg_cdc_slave_wr(_inst_,
	                              0x0,
	                              0,
	                              1,
	                              0);
	///  setting for fine delay of wr CDC, rank 1
    DDR_PHY_hal_cfg_cdc_slave_wr(_inst_,
	                              0x0,
	                              1,
	                              1,
	                              1);
	///  setting for coarse delay of wr CDC, rank 1
    DDR_PHY_hal_cfg_cdc_slave_wr(_inst_,
                                  0x0,
                                  0,
                                  1,
                                  1);


    HWIO_OUTXF2 (_inst_, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG, DQS_HALF_CYCLE_R0, DQS_HALF_CYCLE_R1, 0x0, 0x0);  //for Vref only CA training, don't push clock anymore. 

	///  Generate trigger pulse in order to latch the above value
}

// ***********************************************************************
/// DDRPHY BISC STA READ
/// seq_msg(INFO, 0, "DDRPHY CA VREF STA READ");
/// -----------------------------------------------------------------
///     HALs for BISC STA READ
/// -----------------------------------------------------------------
// ***********************************************************************
uint32 DDR_PHY_hal_sta_ca( uint32 _inst_ )
{

	///  only the lower 6 bits are feedback data from DRAM for CA training
	///  but due to bit remapping, all 10 bits need to be saved. Then the
	///  correct remap needs to take place before the comparison can happen.
	return ((HWIO_INXF (_inst_, DDR_PHY_DDRPHY_BISC_TRAINING_STA, TRAINING_STATUS) & 0x3FF));
}




void DDR_PHY_hal_cfg_ca_dq_retmr( uint32 _inst_, uint8 rank, uint32 dq_retmr )
{

	if (rank) {
		///  rank 1
		HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R1, dq_retmr);
	} else {
		///  rank 0
		HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R0, dq_retmr);
	}
     HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
	 HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
}



void DDR_PHY_hal_cfg_ca_dq_retmr_bcm( uint32 _inst_, uint8 rank, uint32 dq_retmr, uint32 bcm
 )
{

	if (rank) {
		///  rank 1
		HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R1, dq_retmr);
	} else {
		///  rank 0
		HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQ_RETMR_R0, dq_retmr);
	}
     HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
	 HWIO_OUTXF (_inst_, DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
}



