/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                              EDIT HISTORY
$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/header/ddr_phy_technology.h#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $

==============================================================================*/
#include "ddrss.h"
//#define NUM_VREF_TRAINING_FREQS     1
//#define NUM_VREF_DQ_PCH             1
#define NUM_CA_PCH                  2
#define PINS_PER_PHY                9
#define PINS_PER_PHY_CONNECTED_CA  10
#define NUM_CA_PHY_BIT             10
#define NUM_DQ_PHY_BIT             10

#define PERBIT_CDC_MAX_VALUE        0xF 
#define COARSE_STEP_IN_PS           50
#define FINE_STEP_IN_PS             8
#define FINE_STEPS_PER_COARSE       6   //50/8.
#define CA_PATTERN_NUM              4 

#define COARSE_CDC_MAX_VALUE        0x18
#define COARSE_CDC                  (COARSE_CDC_MAX_VALUE + 1)

#define FINE_CDC_MAX_VALUE          0xF
#define FINE_CDC                    (FINE_CDC_MAX_VALUE + 1)
#define FINE_RD_CDC                 0x10

#define F_RANGE_0                   250000
#define F_RANGE_1                   450000
#define F_RANGE_2                   790000
#define F_RANGE_3                   900000
#define F_RANGE_4                   1066000
#define F_RANGE_5                   1066000
#define F_RANGE_6                   1066000
#define F_RANGE_7                   1066000

#define HISTOGRAM_SIZE            (COARSE_CDC_MAX_VALUE * FINE_STEPS_PER_COARSE)
#define COARSE_L                   0
#define FINE_L                     1
#define DELAY_L                    2
#define CDC_L                      3


