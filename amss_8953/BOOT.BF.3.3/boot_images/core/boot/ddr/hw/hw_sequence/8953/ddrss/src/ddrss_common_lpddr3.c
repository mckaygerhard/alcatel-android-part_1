/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_common_lpddr3.c#3 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#include "ddrss.h"
#include "target_config.h"
#include <string.h>


uint32 freq_range[5] = {F_RANGE_0, F_RANGE_1, F_RANGE_2, F_RANGE_3, F_RANGE_4};

uint32 global_bit_mask               =  0xFFFFFFFF;
uint64 global_beat_mask[4]           = {0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF
                                       };

extern uint8  bit_remapping_phy2bimc_DQ[NUM_CH][NUM_DQ_PCH][PINS_PER_PHY] ;
extern uint8  bit_remapping_bimc2phy_DQ[NUM_CH][NUM_DQ_PCH][PINS_PER_PHY_CONNECTED_NO_DBI];
 
uint8  fail_flag_per_byte[4] = {0};
uint8  fail_flag_per_bit[40] = {0};

//====================================================================================================
// LPDDR3
//====================================================================================================

//Updated to single channel
uint8  byte_remapping_table[NUM_CH][NUM_DQ_PCH] = {{0, 1, 2, 3}};

uint32 training_address[2][2] = {{0}};

// Size of the array write_in_pattern[] in number of uint32s. 
//This number is supplied to memory read/write functions used during DDR PHY training.

#define DQ_TRAINING_PATTERN_SIZE (sizeof(write_in_pattern) / sizeof(uint32))

uint32 write_in_pattern[] __attribute__((aligned(8))) = {
0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE, 0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D, 
0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE, 0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D,
0x22222222, 0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0x0f0f0f0f, 0xf0f0f0f0, 0x00000000, 0xffffffff, 
0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0x0f0f0f0f, 0xf0f0f0f0, 0x00000000, 0xffffffff,
0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 
0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
0x66666666, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 
0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 
0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000,
0x55555555, 0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0xf0f0f0f0, 0x0f0f0f0f, 0xffffffff, 0x00000000, 
0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0xf0f0f0f0, 0x0f0f0f0f, 0xffffffff, 0x00000000,
0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 
0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808,
0x55555555, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xAAAAAAAA,
0x55555555, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xAAAAAAAA, 0x55555555, 
0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xAAAAAAAA,

0x3789D91E, 0x19E69FC4, 0x484E3BE0, 0xCD6014C4, 0x77D1D4B1, 0xAB30DF90, 0xCB1F041D, 0xFA3D26AB, 
0x63E23084, 0x03DD0851, 0x18577023, 0x913FB1B5, 0xE81D6955, 0xCF7163D4, 0x2A452F75 ,0x01E6629F,
0x797B2280, 0x9ACE7CD7, 0xA06B1A1D, 0xBE79D5DB, 0xEBBFF686, 0x6EBB85FF, 0xBF31749A, 0x29F2E6C7, 
0x8C91AAFC, 0xAB1AAB75, 0xAC48D44E, 0x504C17D0, 0xB1C5B12A, 0xAC329613, 0xBA5D149B ,0x592054F1,
0x753B266D, 0x20944DEB, 0x3220F4C2, 0xDFDF25CD, 0x80D363CB, 0x63EEBF1B, 0x4C1A820C, 0x1E54639A, 
0x127F2554, 0xEB3C2935, 0xCDAAFAEF, 0xC84B4320, 0x2A0B9168, 0xA4CC98BF, 0xF3609880 ,0xD79CAE43,
0x713BB864, 0x72FA35F8, 0x6DED7DF2, 0x23A354E0, 0xCB34F01A, 0xB035D5A0, 0x86C06BD6, 0xEFAB7C43, 
0x74DFBE03, 0xC1E91236, 0x09CAD86F, 0x1C0D78EC, 0xDA6B8136, 0xA18EA81C, 0xEA4D7192 ,0x8B331A3B,
0x81810414, 0xA4555EFD, 0x31722982, 0xFC0E7445, 0x883A76E5, 0x4A9718FF, 0x9577C0A3, 0x49F2D41B, 
0x5D8D9AC4, 0xB48E6E84, 0x4451DBED, 0x0C03D7D7, 0x398540DD, 0xBDC56DB1, 0x29555DDF ,0x47B64FF0,
0x3DF87F32, 0x4697A951, 0x78FFBEF0, 0xA71F8CBD, 0xC449842F, 0x036EDD7C, 0x4E020585, 0xFB39FB64, 
0x4FF1CEC2, 0x7D3A52FB, 0xF8A2D60B, 0x1FD68B15, 0x092A6BD6, 0x2386376D, 0xCCFC9C10 ,0xB8257024,
0xC1A1FDED, 0x186DD915, 0xA0AC5EFE, 0x5363D067, 0x2507A9F2, 0xADD4BC69, 0xFC266ABB, 0x835F251B, 
0x9FA61759, 0x071198C9, 0x1000275A, 0xC7883CC7, 0xF3C7AB76, 0x20E052FD, 0xE3AA85F3 ,0x042B74AA
};

//================================================================================================//
// Memory write
//================================================================================================//
void DDRSS_mem_write (DDR_STRUCT *ddr, uint8 ch, uint8 cs)
{
   ddr_mem_copy (write_in_pattern, (uint32 *)(size_t)training_address[ch][cs], DQ_TRAINING_PATTERN_SIZE, 1);
}

uint16 DDRSS_dq_remapping_lpddr3 (uint16 pattern, uint8 remaping_LUT[10])
{
    uint8  bit;
    uint16 return_data = 0;    
    uint16 bit_data = 0;
        
    for (bit = 0; bit <= 9; bit ++)
    {
      if (remaping_LUT[bit] != PHYBIT_NC)
        {
          bit_data = (pattern >> bit) & 0x1;
          return_data |= (bit_data << remaping_LUT[bit]);        
        }
    }
    return return_data;
}

uint16 DDRSS_ca_remapping_lpddr3 (uint16 pattern, uint8 remaping_LUT[10])
{
    uint8  bit;
    uint16 return_data = 0;    
    uint8  bit_data = 0;
    
    for (bit = 0; bit < 10; bit ++)
    {
        bit_data = (pattern >> bit) & 0x1;
        return_data |= (bit_data << remaping_LUT[bit]);        
    }
    
    return return_data;
}

//================================================================================================//
// Memory read with results per bit
// This function will do a memory read and report pass/fail per BIT against the expected pattern
//================================================================================================//
uint8 *DDRSS_mem_read_per_bit_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 wr_rd, uint8 byte_lane , uint8 phase)
{
   uint32 read_back_pattern[DQ_TRAINING_PATTERN_SIZE] = {0};
   uint32 mask_per_bit    = 0;
   uint32 beat            = 0;
   uint32 compare_pattern = 0;
   uint8  beat64          = 0;
   uint64 beat_mask       = 0;
   uint8  beat_mask_ptr   = 0;
   uint8  bit             = 0;
   uint8  phy_bit         = 0;
   uint8  byte            = 0;
   uint8  byte_remap      = 0;
   uint8  bit_mask_offset = 0;
   uint8  loop  = 0;
   uint8  (*fail_flag_per_bit_ptr) = fail_flag_per_bit;

   // Compare results and return pass/fail per BIT
   mask_per_bit = 0x01;

   // Reset fail_flag_per_bit_ptr
   for (bit = 0; bit < 40; bit++)
   {
     fail_flag_per_bit_ptr[bit]  = 0;
   }

   for (loop = 0; loop < read_test_loop_cnt; loop++)
   {
      //ddr_mem_read 
      ddr_mem_copy ((uint32 *)(size_t)training_address[ch][cs], read_back_pattern, DQ_TRAINING_PATTERN_SIZE, 1);
      compare_pattern = 0x00000000;

      for (beat = 0; beat < DQ_TRAINING_PATTERN_SIZE; beat++)
      {
        beat64        = beat>>6;
        beat_mask     = global_beat_mask[beat64];
        beat_mask_ptr = (beat_mask>>(beat & 0x3F)) & 0x1;

        if (((phase == (beat & 0x1)) || (phase == 0x3)) & beat_mask_ptr)
        {
         // compare_pattern records the position of failed bits
         compare_pattern |= (read_back_pattern[beat] ^ write_in_pattern[beat]) & global_bit_mask;
        }

         // If failure occurs on all bits, jump out of the loop to save time 
         if (compare_pattern == 0xFFFFFFFF)
         break;
      }

      for (byte = 0; byte < NUM_DQ_PCH; byte++)
      {
         // In RD training, wr_rd = 1, all bytes are processed
         // In WR training, wr_rd = 0, only the specific byte is processed
         if (wr_rd | (byte == byte_lane))
         {
            byte_remap = byte_remapping_table[ch][byte];

            for (bit = 0; bit < PINS_PER_PHY_CONNECTED_NO_DBI; bit++)
            {
               phy_bit         = (byte*PINS_PER_PHY) + bit_remapping_bimc2phy_DQ[ch][byte][bit]; 
               bit_mask_offset = (byte_remap*PINS_PER_PHY_CONNECTED_NO_DBI) + bit;
               mask_per_bit    = 0x1 << bit_mask_offset;

               // Only accumulates the specific bit compare results
               fail_flag_per_bit_ptr[phy_bit] = (((compare_pattern & mask_per_bit) == 0) ? 0 : 1);
            }
         }
      } //byte loop
   }
   return fail_flag_per_bit_ptr;
}

uint8 *DDRSS_mem_read_per_byte_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 phase)
{
   uint32 read_back_pattern[DQ_TRAINING_PATTERN_SIZE] __attribute__((aligned(8))) =  {0};

   uint32 mask_per_byte   = 0;
   uint32 beat            = 0;
   uint32 compare_pattern = 0;
   uint8  beat64          = 0;
   uint64 beat_mask       = 0;
   uint8  beat_mask_ptr   = 0;
   uint8  byte            = 0;
   uint8  byte_remap      = 0;
   uint8  loop            = 0;
   uint8  (*fail_flag_per_byte_ptr) = fail_flag_per_byte;

   // Compare results and return pass/fail per BYTE
   mask_per_byte = 0xFF;

   // Reset fail_flag_per_byte_ptr
   for (byte = 0; byte < NUM_DQ_PCH; byte++)
   {
      fail_flag_per_byte_ptr[byte]  = 0;
   }

   for (loop = 0; loop < read_test_loop_cnt; loop++)
   {
      //ddr_mem_read 
      ddr_mem_copy ((uint32 *)(size_t)training_address[ch][cs], read_back_pattern, DQ_TRAINING_PATTERN_SIZE, 1);
      compare_pattern = 0x00000000;

      for (beat = 0; beat < DQ_TRAINING_PATTERN_SIZE; beat++)
      {
        beat64        = beat>>6;
        beat_mask     = global_beat_mask[beat64];
        beat_mask_ptr = (beat_mask>>(beat & 0x3F)) & 0x1;

        if (((phase == (beat & 0x1)) || (phase == 0x3)) & beat_mask_ptr)
        {
          // compare_pattern records the position of failed bits
          compare_pattern |= (read_back_pattern[beat] ^ write_in_pattern[beat]) & global_bit_mask;

          // If failure occurs on all bytes, jump out of the loop to save time
          if (compare_pattern == 0xFFFFFFFF)
          break;
        }
      }

      for (byte = 0; byte < NUM_DQ_PCH; byte++)
      {
         byte_remap = byte_remapping_table[ch][byte];

         // Only accumulates the specific byte compare results
         fail_flag_per_byte_ptr[byte_remap] |= (((compare_pattern & mask_per_byte) == 0) ? 0 : 1);
         // Mask shift for the next byte
         mask_per_byte = mask_per_byte << 8;
      }
   }

   return fail_flag_per_byte_ptr;
}

// --------------------------------------------------
// Update PHY CGC settings after training for power optimization 
// --------------------------------------------------
void DDRSS_update_CGC_optimization(DDR_STRUCT *ddr, uint32 cgc_bypass)
{
	uint8  ch                = 0;
    uint8  ca                = 0;
	uint8  dq                = 0;
    uint32 dq0_ddr_phy_base  = 0;
    uint32 ca0_ddr_phy_base  = 0;
	ca0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);
	dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
	if (cgc_bypass==0)
	{
		for (ca = 0; ca < NUM_CA_PCH; ca++) 
            {
                HWIO_OUTX (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMCDCWR_TRAFFIC_BYP_CFG,0x00070000);
			}
           for (dq = 0; dq < NUM_DQ_PCH; dq++) 
            {
                HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMCDCWR_TRAFFIC_BYP_CFG,0x00070000); 
			}
	}
	else if(cgc_bypass==1)
	{
		   for (ca = 0; ca < NUM_CA_PCH; ca++) 
            {
                HWIO_OUTX (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMCDCWR_TRAFFIC_BYP_CFG,0x0037FFFF);
			}
           for (dq = 0; dq < NUM_DQ_PCH; dq++) 
            {
                HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMCDCWR_TRAFFIC_BYP_CFG,0x0037FFFF); 
			}
	}
}

