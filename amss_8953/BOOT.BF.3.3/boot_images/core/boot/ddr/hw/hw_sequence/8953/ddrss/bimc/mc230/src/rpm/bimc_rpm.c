/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/rpm/bimc_rpm.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"
#include "ddrss.h"

//================================================================================================//
//ZQ Cal function for lpddr3
// ZQ Cal is used on a per rank basis, chip_select_both is not supported
//================================================================================================//
void BIMC_ZQ_Calibration (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch      = 0;
   uint8 ch_1hot = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      ch_1hot = CH_1HOT(ch);
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK,
                                      (div_ceil((ddr->cdt_params[ch].lpddr.tZQCL * 100), XO_PERIOD_IN_PS)));
         if (chip_select & DDR_CS0) {

             HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LONG, DDR_CS0, 1);
             while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LONG));

         }
         if (chip_select & DDR_CS1) {

             HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LONG, DDR_CS1, 1);
             while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LONG));

         }
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0);//reset MANUAL_1 timer
      }
   }
}

//================================================================================================//
//Mode register read function only returns a single MR value, it reads on per channel per rank basis
//Both-channel option is not supported for DDR_CHANNEL
//================================================================================================//
uint32 BIMC_MR_Read (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr)
{
   uint32  read_value = 0;
   uint32 reg_offset_shke = 0;
   
   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(CH_INX(channel));

   HWIO_OUTXF (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MR_addr);

   HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_READ, chip_select, 1);
   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_READ));

   if(chip_select == DDR_CS0) {
      read_value = HWIO_INX (reg_offset_shke, SHKE_MREG_RDATA_RANK0_L);
   }
   if(chip_select == DDR_CS1) {
      read_value = HWIO_INX (reg_offset_shke, SHKE_MREG_RDATA_RANK1_L);
   }


   return read_value;
}//BIMC_MR_Read

//================================================================================================//
//Timer function is called before triggering DRAM_MANUAL_0 command
//WAIT_TIMER_DOMAIN has 2 options: XO clock(19.2MHz) and Timer clock(32KHz), enum defined in bimc.h
//================================================================================================//
void BIMC_Wait_Timer_Setup (DDR_STRUCT *ddr, DDR_CHANNEL channel, BIMC_Wait_Timer_Domain one_xo_zero_timer_clk, uint32 timer_value)
{
   uint8  ch = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      if ((channel >> ch) & 0x1)
      {
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, one_xo_zero_timer_clk, timer_value);
      }
   }

}

//================================================================================================//
// BIMC_Enter_Self_Refresh
// disable HW self refresh and enter SW self refresh
//================================================================================================//
void BIMC_Enter_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;
   uint32 reg_offset_scmo = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch);

      if ((channel >> ch) & 0x1)
      {
         // Disable rank and disable HW self refresh
         if (chip_select & DDR_CS0)  {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  0/*rank index 0*/, RANK_EN, 0);
            BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 0, 0);
         }
         if (chip_select & DDR_CS1)  {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  1/*rank index 1*/, RANK_EN, 0);
            BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 1, 0);
         }

         // Enter SW self refresh
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ENTER_SELF_REFRESH_IDLE, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE));

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH) == 0);
      }
   }
}


//================================================================================================//
// BIMC_Exit_Self_Refresh
// exit SW self refresh and enable HW self refresh
//================================================================================================//
void BIMC_Exit_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;
   uint32 reg_offset_scmo = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch);

      if ((channel >> ch) & 0x1)
      {
         // Enable rank
         if (chip_select & DDR_CS0) {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  0/*rank0*/, RANK_EN, 1);
         }
         if (chip_select & DDR_CS1) {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  1/*rank1*/, RANK_EN, 1);
         }


         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXIT_SELF_REFRESH, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXIT_SELF_REFRESH));

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH));

         //force a manual ZQCAL after Power Collapse
         BIMC_ZQ_Calibration(ddr, CH_1HOT(ch), chip_select);

         //force a refresh rate update
         if (chip_select & DDR_CS0)
         {
            BIMC_MR_Read(ddr, CH_1HOT(ch), DDR_CS0, JEDEC_MR_4);
            // Enable HW self refresh
            BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 0, 1);
         }
         if (chip_select & DDR_CS1) 
         {
            BIMC_MR_Read(ddr, CH_1HOT(ch), DDR_CS1, JEDEC_MR_4);
            // Enable HW self refresh
            BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 1, 1);
         }
      }
   }
}

//================================================================================================//
// Enter SW self refresh and ensure RANK0/1 is in self refresh state
//================================================================================================//
void BIMC_Enter_Power_Collapse (DDR_STRUCT *ddr, DDR_CHANNEL channel)

{
   uint32 reg_offset_dpe = 0;
   uint32 reg_offset_global0 = 0;
   uint8  ch = 0;

   reg_offset_global0 = ddr->base_addr.bimc_base_addr + REG_OFFSET_GLOBAL0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);

      if ((channel >> ch) & 0x1)
      {

         BIMC_Enter_Self_Refresh (ddr, CH_1HOT(ch), (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);

         // Check if it's in self refresh state.
         if (ddr->cdt_params[ch].common.populated_chipselect & DDR_CS0) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_0_SR) == 0);
         }

         if (ddr->cdt_params[ch].common.populated_chipselect & DDR_CS1) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_1_SR) == 0);
         }
         // Disable DCD to improve DEHR save and restore latency
         HWIO_OUTXFI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_LP_CLK_DIV_CFG, ch, CLK_DIV_ENA, 0);

      }
   }

}

//================================================================================================//
// Power collapse exit sequence for retention mode requires exiting SW self refresh
// Timing parameters and self refresh status in BIMC are already retained by retention registers
//================================================================================================//
void BIMC_Exit_Power_Collapse (DDR_STRUCT *ddr, DDR_CHANNEL channel)
{

   uint8  ch = 0;
   uint32 reg_offset_global0 = 0;

   reg_offset_global0 = ddr->base_addr.bimc_base_addr + REG_OFFSET_GLOBAL0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      if ((channel >> ch) & 0x1)
      {
         BIMC_Exit_Self_Refresh (ddr, CH_1HOT(ch), (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
         // Enable DCD which was disabled during power collapse entry
         HWIO_OUTXFI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_LP_CLK_DIV_CFG, ch, CLK_DIV_ENA, 1);
      }
   }

}

//================================================================================================//
// Power collapse pre-exit sequence
//================================================================================================//
void BIMC_Pre_Exit_Power_Collapse (DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
   uint32 reg_offset_dpe = 0;
   uint32 reg_offset_shke = 0;
   uint8 ch=0;
   
   for (ch = 0; ch < NUM_CH; ch++) 
   {
   	DDR_CHIPSELECT chip_select;
   
   	reg_offset_dpe  = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
   	reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
   
   	//chip_select = DDR_CS_NONE;
	if ((channel >> ch) & 0x1)
      	{
   		/*if(HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH) == 1)  
   		{
    			chip_select = (DDR_CHIPSELECT) HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL);
   		}*/
   	chip_select = (DDR_CHIPSELECT) ddr->cdt_params[ch].common.populated_chipselect; 

   if (chip_select != DDR_CS_NONE)
   {
     // Kick off timing parameter calculation and wait until done
     HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, RECALC_PS_PARAMS, 0x1);
     while (HWIO_INXF (reg_offset_dpe, DPE_MEMC_STATUS_1, CYC_CALC_VALID));
     
	 // Load timing registers
	 HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
	
    
    	HWIO_OUTX (reg_offset_shke, SHKE_CONFIG, 0x00148001);

     	//Enable ranks
    	if (chip_select & DDR_CS0) {
        	HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_EN, 1);
      	}
      	if (chip_select & DDR_CS1) {
        	HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_EN, 1);
      	}

	//HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, NUM_BANKS, ddr->cdt_params[ch].common.num_banks_cs0);

	 // Toggle SHKE_CONFIG[RANK0/1_INITCOMPLETE] as workaround for -> HW issue:TREFI not loaded correctly
	  // disable rank init complete signal
	  if (chip_select & DDR_CS0) {
        HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 0);
      }
      if (chip_select & DDR_CS1) {
        HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 0);
      }
	  
	  // Wait for 1us (at least 3 XO cycles)
	  seq_wait(1, US);
	  
	  // Restore SHKE_CONFIG expect SHKE_CONFIG[RANK0/1_INITCOMPLETE]
	  if (chip_select & DDR_CS0) {
        HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 1);
      }
      if (chip_select & DDR_CS1) {
        HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 1);
      }
	 
     /* Put DDR into self refresh with EBI1 clamped to restore controller state */
   			BIMC_Enter_Self_Refresh ( ddr, CH_1HOT(ch), chip_select);
	  }
	}
   }
}

//================================================================================================//
// Enable/Disable HW activity based self refresh
//================================================================================================//
void BIMC_HW_Self_Refresh_Ctrl (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 enable)
{

   uint32 reg_offset_shke = 0;

   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
   // Check if this feature is enabled in the DDR_STRUCT
   if (ddr->extended_cdt_runtime.hw_self_refresh_enable == 1) {
      if (cs == 0) {
         HWIO_OUTXF (reg_offset_shke,SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, enable);
      }
      if (cs == 1) {
         HWIO_OUTXF (reg_offset_shke, SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, enable);
      }
   }
}

//================================================================================================//
// Temperature controlled minimim frequency is dictated by the refresh rate at which DRAM
// device has to operate
//================================================================================================//
uint32 BIMC_Temp_Ctrl_Min_Freq (DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
 
   uint8  ch = 0;
   uint32 reg_offset_dpe = 0;
   uint32 rank0_temp_info[2] = {0};
   uint32 rank1_temp_info[2] = {0};
   uint32 max_temp_info = 0;
   uint32 min_freq = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);

      if ((channel >> ch) & 0x1)
      {
         if (ddr->cdt_params[ch].common.populated_chipselect & DDR_CS0) {
            rank0_temp_info[ch] = HWIO_INXF (reg_offset_dpe, DPE_INTERRUPT_STATUS, RANK0_TEMP_INFO);
         }

         if (ddr->cdt_params[ch].common.populated_chipselect & DDR_CS1) {
            rank1_temp_info[ch] = HWIO_INXF (reg_offset_dpe, DPE_INTERRUPT_STATUS, RANK1_TEMP_INFO);
         }

      }
   }

   if ((rank0_temp_info[0] >= rank1_temp_info[0]) &&
       (rank0_temp_info[0] >= rank0_temp_info[1]) &&
       (rank0_temp_info[0] >= rank1_temp_info[1]))
   {
      max_temp_info = rank0_temp_info[0];
   }
   else if ((rank0_temp_info[1] >= rank0_temp_info[0]) &&
            (rank0_temp_info[1] >= rank1_temp_info[0]) &&
            (rank0_temp_info[1] >= rank1_temp_info[1]))
   {
      max_temp_info = rank0_temp_info[1];
   }
   else if ((rank1_temp_info[0] >= rank0_temp_info[0]) &&
            (rank1_temp_info[0] >= rank0_temp_info[1]) &&
            (rank1_temp_info[0] >= rank1_temp_info[1]))
   {
      max_temp_info = rank1_temp_info[0];
   }
   else if ((rank1_temp_info[1] >= rank0_temp_info[0]) &&
            (rank1_temp_info[1] >= rank0_temp_info[1]) &&
            (rank1_temp_info[1] >= rank1_temp_info[0]))
   {
      max_temp_info = rank1_temp_info[1];
   }

   // Check for refresh rate greater than 1x
   if (max_temp_info > 0x3)
   {
      // Check for 0.5x refresh rate
      if (max_temp_info == 0x4)
      {
         // 0.5x
         min_freq = 50000;
      }
      else
      {
         // 0.25x or higher
         min_freq = 100000;
      }
   }
   else
   {
      // 1x or lower
      min_freq = 19200;
   }

   return min_freq;

}

