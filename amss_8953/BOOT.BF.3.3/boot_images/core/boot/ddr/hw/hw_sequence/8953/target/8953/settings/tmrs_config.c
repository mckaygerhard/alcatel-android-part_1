/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/settings/tmrs_config.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/11/14   arindamm     Initial creation for 8994 V1-only code.
================================================================================*/
#include "ddrss.h"


#ifdef DSF_LPDDR3_TMRS
uint32 num_of_tests = 4;
uint32 dram_tmrs_array[4][20] =  {
					{
						0 /* Array[0] */
					},
     				{
						0 /* Array[1] */
					},
					{
						0 /* Array[2] */
					},
					{
						0 /* Array[3] */
					}
		
                              };
#endif

