/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/sdi/ddrss_init_sdi.h#5 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/
#include "bimc_seq_hwioreg_sdi.h"
#include "ddr_ss_seq_hwioreg_sdi.h"
#include "seq_hwio.h"
#include "HAL_SNS_DDR.h"
#define TARGET_DDR_SYSTEM_FIRMWARE_VERSION     48
//#define TARGET_SILICON                         1 
//Replaced TARGET_SILICON with FEATURE_RUMI_BOOT based on SW request.
// If FEATURE_RUMI_BOOT is defined , it implies an emulation build.
// If FEATURE_RUMI_BOOT is not defined, it implies a real-silicon build.
//define FEATURE_RUMI_BOOT 1		//define this for RUMI
#define TARGET_BIMC_ARCH_VERSION               2
#define TARGET_BIMC_CORE_MAJOR_VERSION         3
#define TARGET_BIMC_CORE_MINOR_VERSION         0
#define TARGET_PHY_CORE_MAJOR_VERSION          4
#define TARGET_PHY_CORE_MINOR_VERSION          0
#define DDR_BASE                        0x80000000

#define REG_OFFSET_GLOBAL0              (SEQ_BIMC_GLOBAL0_OFFSET)
#define REG_OFFSET_GLOBAL1              (SEQ_BIMC_GLOBAL1_OFFSET)
#define REG_OFFSET_SCMO(uint8)          ((uint8 == 0) ? \
                                        (SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET) : \
                                        (SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET))
#define REG_OFFSET_DPE(uint8)           ((uint8 == 0) ? \
                                        (SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET)  : \
                                        (SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET))
#define REG_OFFSET_SHKE(uint8)          ((uint8 == 0) ? \
                                        (SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET) : \
                                        (SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET))
#define CH_1HOT(uint8)                  ((uint8 == 0) ? \
                                        DDR_CH0 : \
                                        DDR_CH1)
#define CS_1HOT(uint8)                  ((uint8 == 0) ? \
                                        DDR_CS0 : \
                                        DDR_CS1)
#define CH_INX(DDR_CHANNEL)             ((DDR_CHANNEL == DDR_CH0) ? \
                                        0: \
                                        1)
#define CS_INX(DDR_CHIPSELECT)          ((DDR_CHIPSELECT == DDR_CS0) ? \
                                        0 : \
                                        1)
#define     BIMC_CH_OFFSET  (SEQ_BIMC_BIMC_S_DDR0_OFFSET-SEQ_BIMC_BIMC_S_DDR0_OFFSET)

#define NUM_CH                    1 // Number of DDR channels
#define NUM_CS                    2 // Number of ranks (chip selects)
#define NUM_DQ_PCH                4 // Number of DQ PHYs
#define NUM_CA_4WRLVL_PCH                1 // Number of CA PHYs Per Channel
#define NUM_PLL                   2 // Number of PLLs in DDRCC

#define DDR_PHY_OFFSET           0x1000     // DDR PHY Address offset (4k Bytes)
#define CA0_DDR_PHY_OFFSET       0x0000
#define CA1_DDR_PHY_OFFSET       0x1000
#define DQ0_DDR_PHY_OFFSET       0x2000
#define DQ1_DDR_PHY_OFFSET       0x3000
#define DQ2_DDR_PHY_OFFSET       0x4000
#define DQ3_DDR_PHY_OFFSET       0x5000
#define DDR_CC_OFFSET            0x6000
#define REG_OFFSET_DDR_PHY_CH(ch) ((ch == 0)  ? \
                                  0x0 : \
                                  0x3800)
#define BROADCAST_BASE SEQ_DDR_SS_DDRSS_AHB2PHY_BROADCAST_SWMAN1_OFFSET
#define JEDEC_MR_0   0x0
#define JEDEC_MR_1   0x1
#define JEDEC_MR_2   0x2
#define JEDEC_MR_3   0x3
#define JEDEC_MR_4   0x4
#define JEDEC_MR_5   0x5
#define JEDEC_MR_6   0x6
#define JEDEC_MR_7   0x7
#define JEDEC_MR_8   0x8
#define JEDEC_MR_9   0x9
#define JEDEC_MR_11  0xB
#define JEDEC_MR_12  0xC
#define JEDEC_MR_13  0xD
#define JEDEC_MR_14  0xE
#define JEDEC_MR_15  0xF
#define JEDEC_MR_16  0x10
#define JEDEC_MR_17  0x11
#define JEDEC_MR_18  0x12
#define JEDEC_MR_19  0x13
#define JEDEC_MR_20  0x14
#define JEDEC_MR_22  0x16
#define JEDEC_MR_23  0x17
#define JEDEC_MR_24  0x18
#define JEDEC_MR_32  0x20
#define JEDEC_MR_40  0x28

//initialization functions
void BIMC_Config_sdi(DDR_STRUCT *ddr);
void DDR_PHY_CC_Config_sdi (DDR_STRUCT *ddr);
void BIMC_Pre_Init_Setup_sdi (DDR_STRUCT *ddr, DDR_CHANNEL ChannelSel, DDR_CHIPSELECT chip_select);
void BIMC_Memory_Device_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL ChannelSel, DDR_CHIPSELECT chip_select);
void BIMC_Memory_Device_Init_Lpddr3_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel ,DDR_CHIPSELECT chip_select);
void BIMC_Post_Init_Setup_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);


void Set_config (uint32 offset, uint32 config_base[][2] );
void BIMC_Exit_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void BIMC_Enter_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void BIMC_HW_Self_Refresh_Ctrl_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 enable);
void BIMC_ZQ_Calibration_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);

/* =============================================================================
**  Function : HAL_DDR_Init_SDI
** =============================================================================
*/
/**
*   @brief
*   Initialize DDR controller, as well as DDR device.
*
*   @param[in]  ddr          Pointer to ddr conifiguration struct
*   @param[in]  channel      channel to initialize for
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean HAL_DDR_Init_SDI(DDR_STRUCT *ddr, DDR_CHANNEL channel);
                       

 
