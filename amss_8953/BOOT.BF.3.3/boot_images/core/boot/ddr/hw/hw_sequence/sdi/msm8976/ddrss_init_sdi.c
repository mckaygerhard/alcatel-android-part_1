/**
 * @file  bimc_config_8936.c
 * @brief  the bimc configurations for MSM8936, including the static setting,
 * read latency, write latency, MR values based on different frequency plans
 * 
 */
/*==============================================================================
                                EDIT HISTORY

$Header $ $DateTime $
$Author: pwbldsvc $
================================================================================
when         who         what, where, why
----------   --------     -------------------------------------------------------------
08/18/15   sc      SDI path fixes propogated from Sahi
04/02/15   sc      Initial version.
================================================================================
                   Copyright 2013-2015 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                                             INCLUDES
==============================================================================*/

#include "ddrss_init_sdi.h"
#include "bimc_config.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HALhwio.h"
#include "ddrss_seq_hwiobase.h"
#include "ddrss_seq_hwioreg.h"
#include "bimc_struct.h"

#ifndef SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET
#define SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET SEQ_DDRSS_BIMC_BIMC_S_DDR0_SHKE_OFFSET
#endif

#define REG_OFFSET_SHKE_ABSOLUTE(INTERFACE) ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_DDRSS_BIMC_BIMC_S_DDR0_SHKE_OFFSET : \
    SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET)

#ifndef SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET
#define SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET SEQ_DDRSS_EBI1_PHY_CH0_CA_DDRPHY_CA_OFFSET
#endif

#define REG_BASE_DDRPHY_CA(INTERFACE) ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_DDRSS_EBI1_PHY_CH0_CA_DDRPHY_CA_OFFSET: \
    SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET)
	
void HAL_SDRAM_PHY_Disable_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_Manual_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_Enable_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_CA_Manual_IO_Cal(SDRAM_INTERFACE interface);

/*==============================================================================
                                           VARIABLE DEFINITIONS                     
==============================================================================*/

uint32 LPDDR3_bimc_global0_config_SDI[][2] = { 
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_2X_MODE_ADDR(0,0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_2X_MODE_ADDR(0,1), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_PERIOD_ADDR(0,0), 0x10000430},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_PERIOD_ADDR(0,1), 0x10000430},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PHY_CLK_SEL_ADDR(0,0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PHY_CLK_SEL_ADDR(0,1), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_SCMON_LOCAL_CGC_THRESHOLD_ADDR(0,0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_SCMON_LOCAL_CGC_THRESHOLD_ADDR(0,1), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DPEN_LOCAL_CGC_THRESHOLD_ADDR(0,0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DPEN_LOCAL_CGC_THRESHOLD_ADDR(0,1), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORT_CORE_DOMAIN_LOCAL_CGC_THRESHOLD_ADDR(0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORTN_CLK_LOCAL_CGC_THRESHOLD_ADDR(0,0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORTN_CLK_LOCAL_CGC_THRESHOLD_ADDR(0,1), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORTN_CLK_LOCAL_CGC_THRESHOLD_ADDR(0,2), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORTN_CLK_LOCAL_CGC_THRESHOLD_ADDR(0,3), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_MPORTN_CLK_LOCAL_CGC_THRESHOLD_ADDR(0,4), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_SWAY_LOCAL_CGC_THRESHOLD_ADDR(0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_SCMON_CGC_CFG_ADDR(0,0), 0x0100000A},
    {HWIO_BIMC_MISC_GLOBAL_CSR_SCMON_CGC_CFG_ADDR(0,1), 0x0100000A},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DPEN_CGC_CFG_ADDR(0,0), 0x0100000A},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DPEN_CGC_CFG_ADDR(0,1), 0x0100000A},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_0_ADDR(0,0), 0x100000C8},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_0_ADDR(0,1), 0x100000C8},
  /* emulation phy on RUMI does n't support pipelining and hence
     timing params/ configuration needs to be updated accordingly*/
  #ifdef FEATURE_RUMI_BOOT
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_1_ADDR(0,0), 0x00000100},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_1_ADDR(0,1), 0x00000100},
  #else
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_1_ADDR(0,0), 0x00000105},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_1_ADDR(0,1), 0x00000105},
  #endif
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_2_ADDR(0,0), 0x10000010},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_2_ADDR(0,1), 0x10000010},
  #ifdef FEATURE_RUMI_BOOT
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_3_ADDR(0,0), 0x00000002},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_3_ADDR(0,1), 0x00000002},
  #else
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_3_ADDR(0,0), 0x00000007},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAL_CFG_3_ADDR(0,1), 0x00000007},
  #endif
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CAL_ENA_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CAL_ENA_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_CAPHY_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_CAPHY_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DQPHY_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DQPHY_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLK_READY_OVERRIDE_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLK_READY_OVERRIDE_ADDR(0,1), 0x00000000},
  #ifdef FEATURE_RUMI_BOOT
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND_ADDR(0,0), 0x00000001},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND_ADDR(0,1), 0x00000001},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND_ADDR(0,0), 0x00000001},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND_ADDR(0,1), 0x00000001},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND_ADDR(0,0), 0x00000001},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND_ADDR(0,1), 0x00000001},
  #else
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND_ADDR(0,0), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND_ADDR(0,1), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND_ADDR(0,0), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND_ADDR(0,1), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND_ADDR(0,0), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND_ADDR(0,1), 0x00000008},
  #endif
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PHY_PIPE_CLK_CTRL_ADDR(0,0), 0x00000007},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PHY_PIPE_CLK_CTRL_ADDR(0,1), 0x00000007},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PIPE_CGC_EN_EXTEND_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PIPE_CGC_EN_EXTEND_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_CTRL_ADDR(0,0), 0x00000010},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_CTRL_ADDR(0,1), 0x00000010},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PIPESTAGES_TIMER_ADDR(0,0), 0x00000008},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_PIPESTAGES_TIMER_ADDR(0,1), 0x00000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EN_HYSTERESIS_ADDR(0,0), 0x00000002},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EN_HYSTERESIS_ADDR(0,1), 0x00000002},  
  {HWIO_BIMC_MISC_GLOBAL_CSR_DFIN_LP_CFG_ADDR(0,0), 0x0000000C},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DFIN_LP_CFG_ADDR(0,1), 0x0000000C},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_CFG_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_CFG_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_CTRL_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_CTRL_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_CTRL_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_CTRL_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL_ADDR(0,0), 0x00000002},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL_ADDR(0,1), 0x00000002},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL_ADDR(0,0), 0x00000002},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL_ADDR(0,1), 0x00000002},  
  #ifdef FEATURE_RUMI_BOOT
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSPC_CFG_ADDR(0,0), 0x80000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSPC_CFG_ADDR(0,1), 0x80000000},  
  #else
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSPC_CFG_ADDR(0,0), 0x80000008},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSPC_CFG_ADDR(0,1), 0x80000008},
  #endif
  {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_REQ_CFG_N_ADDR(0,0), 0x00000500},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_REQ_CFG_N_ADDR(0,1), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_REQ_CFG_N_ADDR(0,2), 0x00000500},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_REQ_CFG_N_ADDR(0,3), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_DBG_CFG_ADDR(0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_DBG_CTL_ADDR(0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_DT_AGG_CGC_CFG_ADDR(0), 0x00000000},
    {HWIO_BIMC_MISC_GLOBAL_CSR_QOS_FSSH_CTRL_ADDR(0), 0x00000001},
    {HWIO_BIMC_MISC_GLOBAL_CSR_QOS_FSSH_FREQ_THRESHOLD_URGENCY_BAND0_ADDR(0), 0x000009C4},
    {HWIO_BIMC_MISC_GLOBAL_CSR_QOS_FSSH_URGENCY_SEL_ADDR(0), 0x00000010},
    {0x0, 0x0} 
#endif
};
 
uint32 LPDDR3_bimc_scmo_config_SDI[][2] = {
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
    {HWIO_SCMO_CFG_INTERRUPT_CLEAR_ADDR(0), 0x00000000},
    {HWIO_SCMO_CFG_INTERRUPT_ENABLE_ADDR(0), 0x00000000},
    {HWIO_SCMO_CFG_CLOCK_CTRL_ADDR(0), 0x00111111},
    {HWIO_SCMO_CFG_SLV_INTERLEAVE_CFG_ADDR(0), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_BASE_CSN_ADDR(0,0), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_BASE_CSN_ADDR(0,1), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_MAP_CSN_ADDR(0,0), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_MAP_CSN_ADDR(0,1), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_MASK_CSN_ADDR(0,0), 0x00000000},
    {HWIO_SCMO_CFG_ADDR_MASK_CSN_ADDR(0,1), 0x00000000},
    {HWIO_SCMO_CFG_GLOBAL_MON_CFG_ADDR(0), 0x00000013},
    {HWIO_SCMO_CFG_CMD_BUF_CFG_ADDR(0), 0x00000001},
    {HWIO_SCMO_CFG_RCH_SELECT_ADDR(0), 0x00000002},
    {HWIO_SCMO_CFG_RCH_BKPR_CFG_ADDR(0), 0x30307070},
    {HWIO_SCMO_CFG_WCH_BUF_CFG_ADDR(0), 0x00000001},
    {HWIO_SCMO_CFG_FLUSH_CFG_ADDR(0), 0x00320F08},
    {HWIO_SCMO_CFG_FLUSH_CMD_ADDR(0), 0x00000000},
    {HWIO_SCMO_CFG_CMD_OPT_CFG0_ADDR(0), 0x00021110},
    {HWIO_SCMO_CFG_CMD_OPT_CFG1_ADDR(0), 0x010A0B1F},
    {HWIO_SCMO_CFG_CMD_OPT_CFG2_ADDR(0), 0x00000804},
    {HWIO_SCMO_CFG_CMD_OPT_CFG3_ADDR(0), 0x0004001F},
    {HWIO_SCMO_CFG_CMD_OPT_CFG4_ADDR(0), 0x00000011},
    {0x0, 0x0} 
#endif
};

uint32 LPDDR3_bimc_dpe_config_SDI[][2] = { 
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
    {HWIO_ABHN_DPE_CONFIG_0_ADDR(0), 0x11020A11},
    {HWIO_ABHN_DPE_CONFIG_1_ADDR(0), 0x32000808},
    {HWIO_ABHN_DPE_CONFIG_2_ADDR(0), 0x00810800},
    {HWIO_ABHN_DPE_CONFIG_3_ADDR(0), 0x04161012},
    {HWIO_ABHN_DPE_CONFIG_4_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CONFIG_5_ADDR(0), 0x7FFFFFC7},
    {HWIO_ABHN_DPE_CONFIG_6_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CONFIG_7_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CONFIG_DQ_MAP_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_ODT_CONFIG_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_ODT_CONFIG_1_ADDR(0), 0x00000100},
    {HWIO_ABHN_DPE_CA_TRAIN_PRE_CS_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CA_TRAIN_CS_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CA_TRAIN_POST_CS_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_RCW_CTRL_ADDR(0), 0x000000AA},
    {HWIO_ABHN_DPE_PWR_CTRL_0_ADDR(0), 0x01110060},
    {HWIO_ABHN_DPE_OPT_CTRL_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_OPT_CTRL_1_ADDR(0), 0x00001080},
    {HWIO_ABHN_DPE_INTERRUPT_ENABLE_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_INTERRUPT_CLR_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_INTERRUPT_SET_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_ELEVATE_PRI_RD_ADDR(0), 0x40404000},
    {HWIO_ABHN_DPE_ELEVATE_PRI_WR_ADDR(0), 0x80808000},
    {HWIO_ABHN_DPE_TIMER_0_ADDR(0), 0x02302000},
    {HWIO_ABHN_DPE_TIMER_1_ADDR(0), 0x00002140},
    {HWIO_ABHN_DPE_TIMER_2_ADDR(0), 0x0000DAF4},
    {HWIO_ABHN_DPE_TIMER_3_ADDR(0), 0x00000432},
    {HWIO_ABHN_DPE_MRW_BEFORE_FREQ_SWITCH_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_BEFORE_FREQ_SWITCH_1_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_BEFORE_FREQ_SWITCH_2_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_BEFORE_FREQ_SWITCH_3_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_AFTER_FREQ_SWITCH_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_AFTER_FREQ_SWITCH_1_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_AFTER_FREQ_SWITCH_2_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_AFTER_FREQ_SWITCH_3_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_MRW_FREQ_SWITCH_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_0_ADDR(0), 0x000031A4},
    {HWIO_ABHN_DPE_DRAM_TIMING_1_ADDR(0), 0x409630B4},
    {HWIO_ABHN_DPE_DRAM_TIMING_2_ADDR(0), 0x404B2064},
    {HWIO_ABHN_DPE_DRAM_TIMING_3_ADDR(0), 0x08340384},
    {HWIO_ABHN_DPE_DRAM_TIMING_4_ADDR(0), 0x0000404B},
    {HWIO_ABHN_DPE_DRAM_TIMING_5_ADDR(0), 0x60D230B4},
    {HWIO_ABHN_DPE_DRAM_TIMING_6_ADDR(0), 0x81F4304B},
    {HWIO_ABHN_DPE_DRAM_TIMING_7_ADDR(0), 0x00000384},
    {HWIO_ABHN_DPE_DRAM_TIMING_8_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_9_ADDR(0), 0x00003096},
    {HWIO_ABHN_DPE_DRAM_TIMING_10_ADDR(0), 0x28982898},
    {HWIO_ABHN_DPE_DRAM_TIMING_11_ADDR(0), 0x304B304B},
    {HWIO_ABHN_DPE_DRAM_TIMING_12_ADDR(0), 0x304B304B},
    {HWIO_ABHN_DPE_DRAM_TIMING_13_ADDR(0), 0x40044004},
    {HWIO_ABHN_DPE_DRAM_TIMING_14_ADDR(0), 0x00001046},
    {HWIO_ABHN_DPE_DRAM_TIMING_15_ADDR(0), 0x5000D096},
    {HWIO_ABHN_DPE_DRAM_TIMING_16_ADDR(0), 0x00040E08},
    {HWIO_ABHN_DPE_DRAM_TIMING_17_ADDR(0), 0x00000E10},
    {HWIO_ABHN_DPE_DRAM_TIMING_18_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_19_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_20_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_21_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_22_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_DRAM_TIMING_23_ADDR(0), 0x0A122301},
    {HWIO_ABHN_DPE_TESTBUS_CTRL_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_SPARE_REG_ADDR(0), 0x00000000},
    {HWIO_ABHN_DPE_CGC_CTRL_ADDR(0), 0x0001FFFF},
    {HWIO_ABHN_DPE_DERATE_TEMP_CTRL_ADDR(0), 0x00000000},  
    {HWIO_ABHN_DPE_WR_DATA_CTRL_ADDR(0), 0x00000001},
    {0x0, 0x0} 
#endif
};

uint32 LPDDR3_bimc_shke_config_SDI[][2] = {  
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
    {HWIO_ABHN_SHKE_CONFIG_ADDR(0), 0x001F8001},
    {HWIO_ABHN_SHKE_CGC_CNTL_ADDR(0), 0x00000001},
    {HWIO_ABHN_SHKE_DRAM_MANUAL_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_DRAM_MANUAL_1_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_MREG_ADDR_WDATA_CNTL_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_DRAM_DEBUG_CMD_0_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_DRAM_DEBUG_CMD_1_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_DRAM_DEBUG_CMD_2_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_PERIODIC_MRR_ADDR(0), 0x00404000},
    {HWIO_ABHN_SHKE_DERATE_REF_RANK0_ADDR(0), 0x00000003},
    {HWIO_ABHN_SHKE_DERATE_REF_RANK1_ADDR(0), 0x00000003},
    {HWIO_ABHN_SHKE_PERIODIC_ZQCAL_ADDR(0), 0x00000800},
    {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_ADDR(0), 0x00000049},
    {HWIO_ABHN_SHKE_SELF_REFRESH_CNTL_ADDR(0), 0x00032000},
    {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_1_ADDR(0), 0x00000049},
    {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_2_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_TESTBUS_CNTL_ADDR(0), 0x00000000},
    {HWIO_ABHN_SHKE_SPARE_REG_ADDR(0), 0x00000000},
    {0x0, 0x0} 
#endif
};


  
/*==============================================================================
                                  FUNCTIONS
==============================================================================*/ 

/* =============================================================================
 **  Function : BIMC_GLOBAL0_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_GLOBAL0 static settings
 **/
void 
BIMC_GLOBAL0_Set_Config_SDI( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;

    if (LPDDR3_bimc_global0_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_global0_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_global0_config_SDI[reg][0] + reg_base, LPDDR3_bimc_global0_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : BIMC_SCMO_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SCMO static settings
 **/
void 
BIMC_SCMO_Set_Config_SDI( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg; 
 
    if (LPDDR3_bimc_scmo_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_scmo_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_scmo_config_SDI[reg][0] + reg_base, LPDDR3_bimc_scmo_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : BIMC_DPE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_DPE static settings
 **/
void 
BIMC_DPE_Set_Config_SDI ( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;

    if (LPDDR3_bimc_dpe_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_dpe_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_dpe_config_SDI[reg][0] + reg_base, LPDDR3_bimc_dpe_config_SDI[reg][1]);
      }
    }

}

/* =============================================================================
 **  Function : BIMC_SHKE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SHKE static settings
 **/
void 
BIMC_SHKE_Set_Config_SDI ( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;
  
    if (LPDDR3_bimc_shke_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_shke_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_shke_config_SDI[reg][0] + reg_base, LPDDR3_bimc_shke_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : HAL_SDRAM_SDI_Init
 ** =============================================================================
 */
/**
 *   @brief
 *   Initialize DDR controller, as well as DDR device.
 **/
void HAL_SDRAM_SDI_Init(SDRAM_INTERFACE interface, SDRAM_CHIPSELECT chip_select, uint32 clk_freq_in_khz)
{

  uint32 clk_period_in_ps;
  uint32 shke_config;
  
  clk_period_in_ps = ddr_divide_func(1000000000, clk_freq_in_khz);

  /* for wdog reset path save the shke_config */
  shke_config = in_dword(REG_OFFSET_SHKE_ABSOLUTE(interface) + HWIO_ABHN_SHKE_CONFIG_ADDR(0));

  #ifndef FEATURE_RUMI_BOOT
  /* PHY Initialization */
  /* clk_mode = 1, odt_en = 0 */
  EBI1_PHY_CFG_phy_init(SEQ_DDRSS_EBI1_PHY_OFFSET, 0, interface, dynamic_legacy, 0 );
  EBI1_PHY_CFG_DDR_CC_Pre_Init_Setup(SEQ_DDRSS_EBI1_PHY_OFFSET,interface);
  #endif 
  
  BIMC_Pre_Init_Setup_SDI (SEQ_DDRSS_BIMC_OFFSET, interface, clk_freq_in_khz, clk_period_in_ps);
  
  ABHN_SHKE_Disable_All_Periodic(REG_OFFSET_SHKE_ABSOLUTE(interface), chip_select);
  
  #ifndef FEATURE_RUMI_BOOT
  HAL_SDRAM_PHY_CA_Manual_IO_Cal(interface);
  #endif

  out_dword(REG_OFFSET_SHKE_ABSOLUTE(interface) + HWIO_ABHN_SHKE_CONFIG_ADDR(0),shke_config);
  
  /* hack to get DDR out of self refresh manually */
  HWIO_ABHN_SHKE_DRAM_MANUAL_0_OUTM(REG_OFFSET_SHKE_ABSOLUTE(interface), 0x2000, 0x2000);

  BIMC_Post_Init_Setup (SEQ_DDRSS_BIMC_OFFSET,interface,chip_select,0);  

  HAL_SDRAM_Exit_Self_Refresh(interface, chip_select);

} /* HAL_SDRAM_SDI_Init */

/* =============================================================================
 **  Function : BIMC_Pre_Init_Setup_SDI
 ** =============================================================================
 */
/**
 *   @brief
 *      one-time settings for SCMO/DPE/SHKE/BIMC_MISC; values are provided in bimc_config.c parsing from SWI
 *      AC Timing Parameters is update by SW CDT at the end
 *      ODT is on (now hard-coded for initialzing ODT for rank 0)
 **/
void
BIMC_Pre_Init_Setup_SDI (uint32 reg_base, SDRAM_INTERFACE interface,
    uint32 clk_freq_in_khz, uint32 clk_period_in_ps)
{
  uint32 clk_mode = 0;

  if(interface == SDRAM_INTERFACE_0) {
    BIMC_GLOBAL0_Set_Config_SDI( reg_base + SEQ_BIMC_GLOBAL0_OFFSET, interface) ;       
  }

  clk_mode = Get_BIMC_LUT (interface, clk_freq_in_khz, 5);


  BIMC_MISC_GLOBAL_CSR_Mode_Switch(reg_base + SEQ_BIMC_GLOBAL0_OFFSET, interface, clk_mode);    
  BIMC_MISC_GLOBAL_CSR_Update_Clock_Period (reg_base + SEQ_BIMC_GLOBAL0_OFFSET,
      interface, CLK_PERIOD_RESOLUTION_1PS, clk_period_in_ps);

  BIMC_SCMO_Set_Config_SDI(reg_base + REG_OFFSET_SCMO(interface), interface);

  BIMC_DPE_Set_Config_SDI(reg_base+ REG_OFFSET_DPE(interface), interface);

  BIMC_SHKE_Set_Config_SDI(reg_base+ REG_OFFSET_SHKE(interface), interface);

  BIMC_Update_AC_Parameters(reg_base, interface, clk_freq_in_khz, clk_period_in_ps);
}
