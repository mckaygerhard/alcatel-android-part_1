/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/bimc_data.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"

//================================================================================================//
// Geometry Tables
// Number of rows and columns for each device density
//================================================================================================//
uint8   lpddr_geometry_table[][2][2] = {
  /*      X32     |       X16     */
  /* nrows, ncols |  nrows, ncols */
#if DSF_EMULATION_WORKAROUND_MRR_8953 
    {{14,    10},    {14,    10}},   // Rsvd
#else	
    {{0,	  0},    {0,     0}},
#endif	
    {{0,      0},    {0,     0}},   // Rsvd
	{{0,      0},    {0,     0}},   // Rsvd
	{{0,      0},    {0,     0}},   // Rsvd
	{{13,     9},    {13,    10}},  //  1 Gb, Not in spec 
    {{14,     9},    {14,    10}},  //  2 Gb, Not in Spec 
    {{14,    10},    {14,    11}},  //  4 Gb
    {{15,    10},    {15,    11}},  //  8 Gb
    {{15,    11},    {15,    12}},  // 16 Gb
	{{15,    12},    {15,    12}},  // 32 Gb, TBD in spec
    {{0,      0},    {0,     0}},   // Rsvd
    {{0,      0},    {0,     0}},   // Rsvd
    {{0,      0},    {0,     0}},   // Rsvd
    {{15,    11},    {15,    12}},   // 12 Gb
	{{15,    10},    {15,    11}}   // 6 Gb

};

uint16   lpddr_size_table[][2] = {
  /* size_mb, addr_offset_bit*/
#if DSF_EMULATION_WORKAROUND_MRR_8953 
	{0x200,       29},   // Rsvd
#else	
	{0,            0},   // Rsvd
#endif
	{0,            0},   // Rsvd
	{0,            0},   // Rsvd
	{0,            0},   // Rsvd
	{0x80,        27},  //  1 Gb 
    {0x100,       28},  //  2 Gb 
    {0x200,       29},  //  4 Gb 
    {0x400,       30},  //  8 Gb 
    {0x800,       31},  // 16 Gb 
	{0x1000,      31},  // 32 Gb //check
    {0,            0},   // Rsvd
    {0,            0},   // Rsvd
    {0,            0},   // Rsvd
    {0x600,       31},   // 12 Gb 
	{0x300,       30},   // 6 Gb 
};

uint16   lpddr_timing_table[][2] = {
  /* trfc, txsr in 100ps*/
#if DSF_EMULATION_WORKAROUND_MRR_8953 
	{2100, 2200},  // Rsvd
#else	
	{0,       0},  // Rsvd
#endif
	{0,       0},  // Rsvd
	{0,       0},  // Rsvd
	{0,       0},  // Rsvd
    {1300, 1400},  //  1 Gb 
    {1300, 1400},  //  2 Gb 
    {1300, 1400},  //  4 Gb 
    {2100, 2200},  //  8 Gb 
    {2100, 2200},  // 16 Gb, trfc TBD 
    {2100, 2200},  // 32 Gb, trfc TBD 
	{0,       0},  // Rsvd
	{0,       0},  // Rsvd
	{0,       0},  // Rsvd
    {2100, 2200},  // 12 Gb, trfc TBD 
    {2100, 2200}   // 6 Gb 
};

uint32 interface_width[]                        = {32,16};


