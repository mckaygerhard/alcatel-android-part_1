/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/header/ddrss_training.h#2 $
$DateTime: 2016/06/21 00:04:59 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#ifndef __DDRSS_TRAINING_H__
#define __DDRSS_TRAINING_H__

#include "ddr_phy_technology.h"

typedef enum
{
    TRAINING_TYPE_CA   = 0,
    TRAINING_TYPE_WR_DQDQS  = 1,
    TRAINING_TYPE_RD_DQDQS  = 2,
    TRAINING_TYPE_MAX       = 0x7f
} TRAINING_TYPE;

#define PHYBIT_NC           0xFF
#define PHYBIT_DBI          0xFE

#define CONVERT_CYC_TO_PS       1000000000
#define TRAINING_NUM_FREQ_LPDDR3  2  //
#define TRAINING_START_PRFS       4  //8953 has 5 prfs bands
#define DDR_NUM_CLOCK_LEVELS      11     //Need to match with ddr_num_clock_levels in ddr_misc
#define SCALING_STOP_PRFS        2
#define RX_DCC                      10
#define PHASE                       2
#define CLK_DCC_OFFSET_RANGE        7     //2*3(range) + 1
#define PINS_PER_PHY_CONNECTED_NO_DBI    8     
#define PINS_PER_PHY_CONNECTED_WITH_DBI  9



#define MP_HP    2

#define FEATURE_DISABLE            0
#define FEATURE_ENABLE             1
#define DCC_TRAINING_WRLVL_WR90_IO 7
#define DCC_TRAINING_WR90          2
#define DCC_TRAINING_WRLVL         1
#define DCC_TRAINING_WR90_IO       6

extern uint32 freq_range[5]; 
extern uint8 bit_remapping_phy2bimc_DQ [NUM_CH][NUM_DQ_PCH][PINS_PER_PHY];
extern uint8 retimer_map[NUM_RETIMER_LEVELS];
extern uint8 retimer_ref[2][NUM_RETIMER_LEVELS];

/***************************************************************************************************
 Training Params Struct
 ***************************************************************************************************/
typedef struct
{
   struct
   {
      uint8 max_loopcnt;
   } dcc;

   struct
   {
      uint8 max_loopcnt;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 fine_training_enable;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
   } ca;

   struct
   {
      uint8 max_loopcnt;
      uint8 max_coarse_cdc;
      uint8 max_fine_cdc;
      uint8 coarse_cdc_step;
      uint8 fine_cdc_step;
      uint8 feedback_percent;
   } wrlvl;

   struct
   {
       uint8  max_loopcnt;
       uint8  max_coarse_cdc;
       uint8  max_fine_cdc;
       uint8  coarse_cdc_step;
       uint8  fine_cdc_step;
   } rcw;

   struct
   {
      uint8 max_loopcnt;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 fine_training_enable;
      uint8 fine_cdc_start_value;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
      uint8 pbit_start_value;
   } rd_dqdqs;

   struct
   {
      uint8 max_loopcnt;
      uint8 coarse_cdc_start_value;
      uint8 coarse_cdc_max_value;
      uint8 coarse_cdc_step;
      uint8 fine_training_enable;
      uint8 fine_cdc_top_freq_start_value;
      uint8 fine_cdc_start_value;
      uint8 fine_cdc_top_freq_max_value;
      uint8 fine_cdc_max_value;
      uint8 fine_cdc_step;
      uint8 pbit_start_value;
   } wr_dqdqs;

} training_params_t;


/***************************************************************************************************
 Training data struct
 This is the organization of ddr->flash_params.training_data struct
 ***************************************************************************************************/
struct training_results
{
   struct
   {
      uint16 wrlvl_stat_dq [NUM_CH][NUM_DQ_PCH];        // [2][4] * 2 = 16 bytes
      uint16 t90_stat_dq   [NUM_CH][NUM_DQ_PCH];        // [2][4] * 2 = 16 bytes     
      uint16 iodqs_stat_dq [NUM_CH][NUM_DQ_PCH];        // [2][4] * 2 = 16 bytes
      uint16 iodq_stat_dq  [NUM_CH][NUM_DQ_PCH];        // [2][4] * 2 = 16 bytes

      uint16 wrlvl_stat_ca [NUM_CH][NUM_CA_PCH];   // [2][1] * 2 =  2 bytes
      uint16 t90_stat_ca   [NUM_CH][NUM_CA_PCH];   // [2][1] * 2 =  2 bytes
      uint16 iodqs_stat_ca [NUM_CH][NUM_CA_PCH];   // [2][1] * 2 =  2 bytes
      uint16 iodq_stat_ca  [NUM_CH][NUM_CA_PCH];   // [2][1] * 2 =  2 bytes

   } dcc;  // 72 bytes

   struct
   {


      uint8 coarse_cdc     [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 8 bytes
      uint8 fine_cdc       [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 8 bytes
      uint8 dqs_retmr      [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 8 bytes
      uint8 dqs_half_cycle [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 8 bytes
      uint8 dqs_full_cycle [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 8 bytes
      uint8 perbit_delay                             [NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY_CONNECTED_CA];  // [2][2][4][2][10] = 160 bytes
   } ca; // 200 bytes

   struct
   {
      uint8  dq_dqs_retmr       [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];        // [2][2][2][4] = 32 bytes
      uint8  dq_coarse_dqs_delay[TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];        // [2][2][2][4] = 32 bytes
      uint8  dq_fine_dqs_delay  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];        // [2][2][2][4] = 32 bytes
      uint8  dq_dqs_half_cycle  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];        // [2][2][2][4] = 32 bytes
      uint8  dq_dqs_full_cycle  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];        // [2][2][2][4] = 32 bytes

      uint8  ca_dqs_retmr       [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 16 bytes
      uint8  ca_coarse_dqs_delay[TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 16 bytes
      uint8  ca_dqs_half_cycle  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 16 bytes
      uint8  ca_dqs_full_cycle  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_CA_PCH];  // [2][2][2][2] = 16 bytes
   } wrlvl; // 224 bytes

   struct
   {
      uint8 bimc_tDQSCK [DDR_NUM_CLOCK_LEVELS][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [11][2][2][4] = 176 bytes
   } rcw; // 176 bytes

   struct
   {
      uint8 coarse_cdc  [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];               // [2][2][2][4] =  32 bytes
      uint8 fine_cdc    [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH];               // [2][2][2][4] =  32 bytes
      uint8 perbit_delay                          [NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY]; // [2][2][4][9] = 144 bytes
      uint8 rd_dcc      [NUM_CH][NUM_DQ_PCH];                                                 // 8953: [2][4] =   8 bytes.
   } rd_dqdqs; // 216 bytes

   struct
   {
      uint8 coarse_cdc                             [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 fine_cdc                               [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 coarse_max_eye_left_boundary_cdc_value [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 coarse_max_eye_right_boundary_cdc_value[TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 dqs_retmr                              [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 dqs_half_cycle                         [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 dqs_full_cycle                         [TRAINING_NUM_FREQ_LPDDR3][NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
     // uint8 perbit_ave                                                       [NUM_CH][NUM_CS][NUM_DQ_PCH]; // [2][2][2][4] = 32 bytes
      uint8 perbit_delay                                                     [NUM_CH][NUM_CS][NUM_DQ_PCH][PINS_PER_PHY]; // [2][2][2][4][9] = 496 bytes
   } wr_dqdqs; // 752 bytes

};// 72 + 200 + 224 + 176 + 216 + 752 = 1640 bytes

typedef struct
{
   struct training_results results;
} training_data;

void DDRSS_set_training_params (training_params_t *training_params_ptr);

#define COARSE_CDC_MAX_VALUE    0x18
#define COARSE_CDC              (COARSE_CDC_MAX_VALUE + 1)
#define FINE_CDC                (FINE_CDC_MAX_VALUE + 1)


/***************************************************************************************************
 CA  training
 ***************************************************************************************************/
#define FSP_OP_BOOT                   0x0
#define FSP_OP_HIGH                   0x1

#define DCC_TRAINING_SEL              7         // Selecting all three DCC training flows
#define CA_PATTERN_NUM                4

extern uint8 ca_training_pattern[CA_PATTERN_NUM][6];
 
typedef struct
{
  uint8 best_cdc_value;
  uint8 best_fine_cdc_value;
  uint16 best_dcc_value; 
  uint8 max_eye_right_boundary_cdc_value;
  uint8 max_eye_left_boundary_cdc_value;
  uint8 all_fail_flag;
} best_eye_struct;

#define  STRT 0
#define  STOP 1
#define  MIDP 2
#define  EYEW 3

typedef struct {
       uint8 coarse_fail_count_table       [NUM_CA_PCH][COARSE_CDC];
       uint8 left_fine_fail_count_table    [NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][FINE_CDC];
       uint8 right_fine_fail_count_table   [NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][FINE_CDC];
       uint8 left_boundary_fine_cdc_value  [NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA];
       uint8 right_boundary_fine_cdc_value [NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA];
	   uint8          failcount_histogram            [3][NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][HISTOGRAM_SIZE]; //19200 : 4 * 2 * 2 * 10 * (24 * 5) = 19200 //2800B. 
	   uint8          pass_reg                          [NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][4];                                     //19200 : 4 * 2 * 2 * 10 * (24 * 5) = 19200 //2800B. 

} ddrss_ca_vref_local_vars;


/***************************************************************************************************
 Write Leveling
 ***************************************************************************************************/
// Structure for wrlvl convert routine
typedef struct {
  uint32 dqs_retmr;
  uint32 coarse_dqs_delay;
  uint32 fine_dqs_delay;
  uint32 dqs_full_cycle;
  uint32 dqs_half_cycle;
} wrlvl_params_struct;


/***************************************************************************************************
 Write and Read training
 ***************************************************************************************************/
#define HP_MODE      1
#define SWEEP_LEFT   0
#define SWEEP_RIGHT  1

// Structure containing what would otherwise be local variables used by the ddrss_rd_dqdqs(), ddrss_wr_dqdqs() functions and its callees.
typedef struct {
    struct {
        uint8 fine_left_boundary_cdc_value[NUM_DQ_PCH];
        uint8 fine_right_boundary_cdc_value[NUM_DQ_PCH];
    } ddrss_rdwr_fine_cdc_boundary;


//    struct {
//	   uint8 failcount_histogram     [NUM_DQ_PCH][NUM_CS][COARSE_CDC * FINE_STEPS_PER_COARSE][3][SLICES_PER_PHY];//19200 : 4 * 2 * 2 * 10 * (24 * 5) = 19200 //2800B. 
//	   uint8 failcount_histogram     [NUM_CA_PCH][NUM_CS][COARSE_CDC * FINE_STEPS_PER_COARSE][3][PINS_PER_PHY_CONNECTED_CA];//19200 : 4 * 2 * 2 * 10 * (24 * 5) = 19200 //2800B. 
//    } share;

    struct {
        uint8 coarse_dq_passband_info[NUM_DQ_PCH][COARSE_CDC];
        uint8 coarse_dq_half_cycle[NUM_DQ_PCH];                                    //8953: 4 * 56  = 224B.
        uint8 coarse_dq_full_cycle[NUM_DQ_PCH];                                    //8953: 4 * 56  = 224B.
        uint8 coarse_dq_dcc_info[NUM_DQ_PCH][NUM_CS][RX_DCC];//[COARSE_CDC * FINE_STEPS_PER_COARSE];//8953: 4 * 2 * 9 * (25 * 7) = 12600 //2800B. 
        uint8 coarse_dq_dcc_schmoo   [NUM_DQ_PCH][NUM_CS][PHASE][HISTOGRAM_SIZE];//8953: 4 * 2 * 9 * (25 * 7) = 12600 //2800B. 
        uint8 dcc_width[NUM_DQ_PCH][NUM_CS][RX_DCC];                                            //8953: 4 * 2 * 9 = 72B.
        uint8 best_dcc[NUM_DQ_PCH][NUM_CS];                                                     //8953: 4 * 2 = 8B.
        uint8 rd_dcc_ctl[NUM_DQ_PCH][NUM_CS][RX_DCC];                                           //8953: 4 * 2 * 9 = 72B.
        uint8 clk_dcc_width[NUM_DQ_PCH][NUM_CS][CLK_DCC_OFFSET_RANGE][PHASE];                   //8953: 4 * 2 * 9 * 2 = 144B.
        uint8 clk_dq_dcd[NUM_DQ_PCH][NUM_CS][CLK_DCC_OFFSET_RANGE];                             //8953: 4 * 2 * 9 = 72B.
        uint8 coarse_fail_count_table[NUM_DQ_PCH];                                              //8953: 4 * 56 = 224B.
    } coarse_schmoo;

    struct {
        uint8 fine_dq_passband_info_left[NUM_DQ_PCH][FINE_CDC];
        uint8 fine_dq_passband_info_right[NUM_DQ_PCH][FINE_CDC];
    } fine_schmoo;

} ddrss_rdwr_dqdqs_local_vars;



//RCW related

#define   RCW_COARSE_DELAY                            1
#define   RCW_FINE_DELAY                              0


#endif /* __DDRSS_TRAINING_H__ */
