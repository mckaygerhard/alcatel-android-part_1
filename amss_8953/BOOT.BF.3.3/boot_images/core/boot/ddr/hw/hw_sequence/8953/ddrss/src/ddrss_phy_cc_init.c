/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_phy_cc_init.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/30/14   jeasley      Create separate CA and DQ PHY settings routines
05/20/14   jeasley      Correct SW handshake and Broadcast
05/08/14   jeasley      Move initialization of polling variables to the inside of the loop.
05/06/14   jeasley      Invert the polarity of sw_handshake_complete poll.
05/06/14   jeasley      Updated DDR_PHY_CC_init to use broadcast.
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
//===========================================================================
// PHY structures.
//===========================================================================
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_DQS_ODT_ENABLE_BMSK 0x00000040
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_DQS_ODT_ENABLE_SHFT        0x6

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ODT_IN_DQS_BMSK 0x0000001c
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ODT_IN_DQS_SHFT        0x2

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_DQ_ODT_ENABLE_BMSK 0x00000020
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_DQ_ODT_ENABLE_SHFT        0x5

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ODT_IN_DQ_H_BMSK 0x00000003
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ODT_IN_DQ_H_SHFT        0x0

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODT_IN_DQ_L_BMSK 0x80000000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODT_IN_DQ_L_SHFT       0x1f

#define HWIO_DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_4_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_4_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG_HP_COARSE_R0_SHFT        0x0
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG_HP_COARSE_R0_BMSK 0x0000001f
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG_HP_COARSE_R0_SHFT        0x0

#define HWIO_DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_4_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_4_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG_HP_COARSE_R1_SHFT        0xa
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG_HP_COARSE_R1_BMSK 0x00007c00
#define HWIO_DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG_HP_COARSE_R1_SHFT        0xa

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_BMSK 0x00007000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQS_CNTL_SHFT        0xc

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_BMSK 0x00000e00
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_PULL_UP_DQ_CNTL_SHFT        0x9

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_BMSK 0x000001c0
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQS_SHFT        0x6

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQ_BMSK 0x00000038
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ODRV_DQ_SHFT        0x3

#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_DRIVER_VOH_MODE_BMSK 0x00018000
#define HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_DRIVER_VOH_MODE_SHFT        0xf

extern struct ecdt_msm_drive_strength_input_struct     msm_drive_strength_ca;
extern struct ecdt_msm_drive_strength_input_struct     msm_drive_strength_dq;
extern struct ecdt_msm_odt_input_struct                msm_odt;
extern struct ecdt_msm_rd_t2_input_struct              msm_rd_t2;
extern struct ecdt_dram_soc_odt_input_struct           dram_soc_odt[];
extern struct ecdt_msm_drv_str_odt_rdt2_input_struct   msm_drv_str_odt_rdt2[];
uint32 prfs_offset_LO[8] =
{
    0,
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG_ADDR(0)
};

uint32 prfs_offset_HI[8] =
{
    0,
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0),
    HWIO_DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG_ADDR(0)- HWIO_DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG_ADDR(0)
};

//================================================================================================//
// DDR PHY and CC one-time settings
//================================================================================================//
void DDR_PHY_CC_Config(DDR_STRUCT *ddr)
{
   uint32   reg_ddrss_base = 0;

   reg_ddrss_base = ddr->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all DQ PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_DQs);
   ddr_phy_dq_set_config(ddr, (reg_ddrss_base + BROADCAST_BASE), ddr_phy_dq_config_base, ddr_phy_dq_config_delta);

   // Enable broadcast mode for all CA PHYs on both channels
   // Enable broadcast mode CA0 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA0);
   ddr_phy_ca0_set_config(ddr,(reg_ddrss_base + BROADCAST_BASE), ddr_phy_ca0_config_base, ddr_phy_ca0_config_delta);

   // Enable broadcast mode CA1 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA1);
   ddr_phy_ca1_set_config(ddr,(reg_ddrss_base + BROADCAST_BASE), ddr_phy_ca1_config_base, ddr_phy_ca1_config_delta);

   // Enable broadcast mode for all CCs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CCs);
   ddr_cc_set_config(ddr,(reg_ddrss_base + BROADCAST_BASE), ddr_cc_config_base,  ddr_cc_config_delta);

   // Disable broadcast mode
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE);
}


//================================================================================================//
// DDR PHY and CC Initialization
//================================================================================================//
void DDR_PHY_CC_init (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 clk_freq_khz)
{
   uint8    ch = 0;
   uint32   reg_offset_ddr_phy;
   uint8    ca0_master;
   uint8    ca1_master;
   uint32   iocal_done = 0x0;
//   uint32   sw_handshake_complete = 0x1;

   uint32   reg_ddrss_base = 0;

   reg_ddrss_base = ddr->base_addr.ddrss_base_addr;

   for (ch = 0; ch < NUM_CH; ch++)
   {
      reg_offset_ddr_phy = reg_ddrss_base + REG_OFFSET_DDR_PHY_CH(ch);

      if ((channel >> ch) & 0x1)
         {

          //turn on LVDS terminiation for DQ0 and DQ3 PHY
          
          
         // Initialize polling variables
         iocal_done            = 0x0;
//        sw_handshake_complete = 0x1;

         // ----------------------------------------------------------------------------------------
         // PHY one time setting for both CA and DQ.
         // DDR_PHY_hal_cfg_init (uint32 _inst_, uint32 clk_freq_khz, uint8 lpddr4)
         // master_phy: 1 = CA PHY with calibration master inside, 0 = DQ PHY or CA PHY without calibratoin master.
         // clk_freq_khz: boot clock frequency
         // ----------------------------------------------------------------------------------------
		// master PHY for all update requests 
		 //Only CA0 is master for both channels	
		   ca0_master = 1;     //In channel 0, CA0 is the master PHY marcro. 
         ca1_master = 0;     //In channel 1, CA0 is the master PHY marcro. 

         // Initiate IO Calibration for the DDR PHY CA Master 
         DDR_PHY_hal_cfg_sw_iocal (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, ca0_master ); 
         DDR_PHY_hal_cfg_sw_iocal (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, ca1_master );

         // Enable broadcast mode for 4 DQ PHYs 
         HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_DQs << (ch * 7));

         // Initiate DQ Calibration with PHY DQ broadcast
         DDR_PHY_hal_cfg_sw_iocal (reg_ddrss_base + BROADCAST_BASE, 0x0 );

         /// poll for IOCAL_DONE to be asserted for all PHYs
         while (iocal_done == 0x0) {
           iocal_done = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 
                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 
                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 
                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 
                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 
                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)));
         }

        // Load the register settings by doing a SW freq switch
        DDRSS_ddr_phy_sw_freq_switch(ddr,clk_freq_khz, ch);
               
       }
   }
}
//================================================================================================//
// PHY eCDT.
//================================================================================================//
void DDR_PHY_CC_eCDT_Override(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt, DDR_CHANNEL channel)
{
    uint8  ch           = 0;
    uint8  prfs_index   = 0;
    uint8  freq_index   = 0;
    uint8  dq_index     = 0;
    uint8  ca_index     = 0;

    uint32 reg_offset_ddr_phy_ca[NUM_CA_PCH] = {0};
    uint32 reg_offset_ddr_phy_dq[NUM_DQ_PCH] = {0};
    

    for (ch = 0; ch < 2; ch++)
    {    
        if ((channel >> ch) & 0x1)
        { 
            reg_offset_ddr_phy_dq[0] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            reg_offset_ddr_phy_dq[1] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
            reg_offset_ddr_phy_dq[2] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
            reg_offset_ddr_phy_dq[3] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;
            reg_offset_ddr_phy_ca[0] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
            reg_offset_ddr_phy_ca[1] = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
 
            for (freq_index = 0; freq_index < NUM_ECDT_PRFS_BANDS; freq_index++)
            {               
                prfs_index = DDRSS_Get_Freq_Index (ddr, ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[freq_index].frequency_in_kHz);        
                
                // Array bounds exceed check for KlockWork error suppression - simply return. 
                // DSF behavior unpredictable after this point. This condition is not expected to happen since 
                // it will only occur when frequency_in_kHz exceeds the highest frequency in the supported frequency plan.
                if(prfs_index >= NUM_ECDT_PRFS_BANDS)
                {
                    return;
                }
                
                // DQ byte_lane loop.
                for(dq_index = 0; dq_index < NUM_DQ_PCH; dq_index++)
                {
                    if(ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.apply_override == 1)
                    {
                        // Note: The pull-up values for both DQS and DQ are from the same element in the structure.
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, PULL_UP_DQS_CNTL, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.pull_up_drive_strength);
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, PULL_UP_DQ_CNTL, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.pull_up_drive_strength);
                                            
                        // Note: The pull-down values for both DQS and DQ are from the same element in the structure.
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, ODRV_DQS, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.pull_down_drive_strength);
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, ODRV_DQ, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.pull_down_drive_strength);
                        
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, DRIVER_VOH_MODE, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_dq.vOH);                        
                    }
                    if(ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.apply_override == 1)
                    {
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_HI[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG, DQS_ODT_ENABLE, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.dqs_odt_enable);                        
                        
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_HI[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG, ODT_IN_DQS, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.dqs_odt);
                        
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_HI[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG, DQ_ODT_ENABLE, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.dq_odt_enable);
    
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_HI[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG, ODT_IN_DQ_H, 
                                    (ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.dq_odt >> 1));
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, ODT_IN_DQ_L, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_odt.dq_odt);
                    }
                    if(ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_rd_t2.apply_override == 1)
                    {
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_index * 4, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG, HP_COARSE_R0, 
                                ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_rd_t2.rd_t2_coarse_cdc);
                        HWIO_OUTXF(reg_offset_ddr_phy_dq[dq_index] + prfs_index * 4, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG, HP_COARSE_R1, 
                                ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_rd_t2.rd_t2_coarse_cdc);
                    }
                } // DQ byte_lane loop.
                
                // CA byte_lane loop.
                for(ca_index = 0; ca_index < NUM_CA_PCH; ca_index++)
                {
                    if(ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.apply_override == 1)
                    {
                        // Note: The pull-up values for both DQS and DQ are from the same element in the structure.
                        HWIO_OUTXF(reg_offset_ddr_phy_ca[ca_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, PULL_UP_DQS_CNTL, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.pull_up_drive_strength);
                        HWIO_OUTXF(reg_offset_ddr_phy_ca[ca_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, PULL_UP_DQ_CNTL, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.pull_up_drive_strength);
                        
                        // Note: The pull-down values for both DQS and DQ are from the same element in the structure.
                        HWIO_OUTXF(reg_offset_ddr_phy_ca[ca_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, ODRV_DQS, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.pull_down_drive_strength);
                        HWIO_OUTXF(reg_offset_ddr_phy_ca[ca_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, ODRV_DQ, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.pull_down_drive_strength);
                                            
                        HWIO_OUTXF(reg_offset_ddr_phy_ca[ca_index] + prfs_offset_LO[prfs_index], DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG, DRIVER_VOH_MODE, 
                                    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[prfs_index].msm_drive_strength_ca.vOH);
                    }
                } // CA byte_lane loop.
            
            } // freq_index loop.
            
        }        
    } // ch.
 
}

