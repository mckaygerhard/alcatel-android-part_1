/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/bimc_common.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"

//================================================================================================//
//Mode Register write can use for both channels and both ranks
//================================================================================================//
void BIMC_MR_Write (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr, uint32 MR_data)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      if ((channel >> ch) & 0x1)
      {
         HWIO_OUTXF2 (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MREG_WDATA, MR_addr, MR_data);
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_WRITE, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_WRITE));
      }
   }

}//BIMC_MR_Write

//================================================================================================//
//Extended Mode Register write can use for both channels and both ranks
//Extended MRW is used only in lpddr3 initialization
//================================================================================================//
void BIMC_Extended_MR_Write (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr, uint32 MR_data)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      if ((channel >> ch) & 0x1)
      {
         HWIO_OUTXF2 (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MREG_WDATA, MR_addr, MR_data);
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXTND_MODE_REGISTER_WRITE, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXTND_MODE_REGISTER_WRITE));
      }
   }

}//BIMC_Extended_MR_Write

//================================================================================================//
// enable signals controls whether enable or disable auto refresh
//================================================================================================//
void BIMC_Auto_Refresh_Ctrl (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint8 enable)
{
   uint8  ch        = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         if (chip_select & DDR_CS0) {
            HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK0, enable);
         }
         if (chip_select & DDR_CS1) {
            HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK1, enable);
         }
      }
   }
}


//================================================================================================//
// Periodic events controlled through this function are
// 1. Auto refresh
// 2. HW self refresh (activity based)
// 3. ZQ calibration
// 4. Temperature status read
//================================================================================================//
void BIMC_All_Periodic_Ctrl (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint8 enable)
{
   uint8  rank_ctrl = 0;
   uint8  ch        = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
        // Auto refresh and HW activity based self refresh
        if (chip_select & DDR_CS0) {
           HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK0,    enable);
           BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 0, enable);
        }
        if (chip_select & DDR_CS1) {
           HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK1,    enable);
           BIMC_HW_Self_Refresh_Ctrl (ddr, ch, 1, enable);
        }

        // Periodic ZQ calibration	 
        rank_ctrl = HWIO_INXF (reg_offset_shke, SHKE_PERIODIC_ZQCAL, RANK_SEL);
        rank_ctrl = enable ? (rank_ctrl | chip_select) : (rank_ctrl & ~chip_select);
        HWIO_OUTXF  (reg_offset_shke, SHKE_PERIODIC_ZQCAL, RANK_SEL, rank_ctrl);

        // Periodic temperature status read (SRR)
        if (ddr->extended_cdt_runtime.MR4_polling_enable == 1) 
        {
        rank_ctrl = HWIO_INXF  (reg_offset_shke, SHKE_PERIODIC_MRR, MRR_RANK_SEL);
        rank_ctrl = enable ? (rank_ctrl | chip_select) : (rank_ctrl & ~chip_select);
        HWIO_OUTXF  (reg_offset_shke, SHKE_PERIODIC_MRR, MRR_RANK_SEL, rank_ctrl);
        }

      }
   }
}


