/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/sdi/ddrss_init_sdi.c#2 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
11/07/14   nevedeth     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss_init_sdi.h"
#include "target_config.h"
boolean HAL_DDR_Init_SDI (DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
   DDR_CHANNEL ch_1hot = DDR_CH_NONE;
   uint32 reg_offset_shke;
   uint8 ch = 0;

   // BIMC one-time settings
   BIMC_Config_sdi(ddr);
   // DDR PHY and CC one-time settings
   #ifndef FEATURE_RUMI_BOOT
   DDR_PHY_CC_Config_sdi(ddr);
   #endif
   BIMC_Pre_Init_Setup_sdi (ddr, channel, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
   
   //*MPM2_MPM_DDR_PHY_FREEZEIO_EBI1 = 0x0;
   // Disable clamps.
   #ifndef FEATURE_RUMI_BOOT
   ddr_mpm_config_ebi1_freeze_io(0);
   #endif
   for (ch = 0; ch < NUM_CH; ch++)
   {
      ch_1hot = CH_1HOT(ch);
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
       // If it is a watchdog reset we just need to do a warm-bootup . 
       // 'WDOG_SELF_RFSH' when set indicates that the memory controller is currently in self refresh due to a watchdog reset event
       // and requires a manual 'EXIT_SELF_REFRESH' trigger to exit from the forced self refresh state.
       if ( (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, WDOG_SELF_RFSH) == 1) ) {
           //set rank enable and rank init complete signals
           HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_EN, 1);
           HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 1);
           if (ddr->cdt_params[ch].common.populated_chipselect & DDR_CS1)
           {
             HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_EN, 1);
             HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 1);
           }
           BIMC_Exit_Self_Refresh_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
        } 
        else {    // If not a watchdog reset, we need to do a cold boot style init
           BIMC_Memory_Device_Init_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);   
        }
		 
		 BIMC_Post_Init_Setup_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
                /* Do Mode register writes with RL/WL of 6/3 for XO mode as it cannot be determined which freq
				   was operational during watch dog reset*/	
				HWIO_OUTXF2 (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MREG_WDATA, JEDEC_MR_2, 0x04);
         		HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_WRITE, 0x3, 1);
         		while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_WRITE));
      }
   }

   return TRUE;
}

uint32 bimc_global0_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND, 0), 0x00000002},
  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND, 0), 0x00000002},
  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND, 0), 0x00000002},
  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_PERIOD, 0), 0x1000CB74},    	// Time period for 19.2MHZ XO clock in 1ps CLK_PERIOD_RESOLUTION
  {0x0, 0x0}
#endif
};
  
uint32 bimc_scmo_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_BUF_CFG), 0x00000001},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_OPT_CFG0), 0x00021110},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_OPT_CFG1), 0x210A0B1F},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_OPT_CFG2), 0x00000884},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_OPT_CFG3), 0x0004001F},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_CMD_OPT_CFG4), 0x00000011},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_FLUSH_CFG), 0x007F0F08},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_GLOBAL_MON_CFG), 0x00000013},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_RCH_BKPR_CFG), 0x30307070},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_RCH_SELECT), 0x00000002},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CFG_WCH_BUF_CFG), 0x00000001},
  {0x0, 0x0}
#endif
};

uint32 bimc_dpe_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_0), 0x23020A11},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_1), 0x00000808},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_10), 0x00010011},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_12), 0x0000C0E0},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_2), 0x00810800},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_3), 0x040F0012},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_5), 0x7FFFFFC7},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_6), 0x00000050},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_8), 0x05070507},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_9), 0x80003000},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_0), 0x000031A4},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_1), 0x409630B4},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_2), 0x204B2064},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_3), 0x05140258},  
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_4), 0x0000404B},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_5), 0x60D260B4},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_6), 0x81F4304B},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_7), 0x00000384},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_9), 0x00003096},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_10), 0x25782578},  
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_11), 0x304B304B},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_12), 0x30FF304B},  
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_13), 0x40146028},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_14), 0x00000035},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_15), 0x0000F0E6},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_16), 0x00040603},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_17), 0x00000E10},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_23), 0x0A112311},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_28), 0x0000000C},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_OPT_CTRL_1), 0x00001080},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_PWR_CTRL_0), 0x00010060},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_CTRL), 0x000000AA},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_0), 0x02302000},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_1), 0x00002140},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_2), 0x0000DAF4},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_3), 0x00000455},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_5), 0x00000000},
  {0x0, 0x0}
#endif
};

uint32 bimc_shke_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_AUTO_REFRESH_CNTL), 0x00000049},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_AUTO_REFRESH_CNTL_1), 0x00000049},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_CONFIG), 0x00148001},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_PERIODIC_ZQCAL), 0x00000800},
  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_SELF_REFRESH_CNTL), 0x00032000},
  {0x0, 0x0}
#endif
};

//*********************************************************************************
// ddr_phy_dq_config_sdi and ddr_phy_ca_config_sdi updated fro Jacala based on 
// ddr_phy_config.c and SWC 
// some CSR has been removed compare to Estel and left as default
// (1/4/2016) amoussav 
//*********************************************************************************

uint32 ddr_phy_dq_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001C090},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG), 0x00032001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG), 0x29466198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG), 0x04200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG)    , 0x2B7857D0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG)        , 0x09100191},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_UPDATE_INTF_CFG)      , 0x00000000}, 
   {0x0, 0x0}
#endif
};

uint32 ddr_phy_ca_config_sdi[][2] =
{
#if defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED) || defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_LITE_COMPLETED) 
    {0x0, 0x0} 
#else
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG)        , 0x0001C090},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG)        , 0x00000001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG)    , 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG)       , 0x29466198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG)       , 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG)       , 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG)       , 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG)  , 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG)     , 0x2B7857D0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG)         , 0x09100191},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG)   , 0x1293CCF3},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG)   , 0x1293CCF3},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG) , 0x27366198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_UPDATE_INTF_CFG)       , 0x00000000},
   {0x0, 0x0} 
#endif
};
//*********************************************************************************



uint32 ddr_cc_config_sdi[][2] =
{
   {0x0, 0x0} 
};

void Set_config (uint32 offset, uint32 config_base[][2] )
{
  uint32 reg = 0;
  uint8  ch = 0;
  uint32 ch_offset = offset;
  /* Populate base config */
  if (config_base != NULL)
  {
     if (config_base != bimc_global0_config_sdi)
     {
        for (ch=0; ch < NUM_CH; ch++)
        {
           ch_offset += BIMC_CH_OFFSET*ch;
           for (reg = 0; config_base[reg][0] != 0; reg++)
           {
             out_dword(config_base[reg][0] + ch_offset, config_base[reg][1]);
           }
        }
        ch_offset = offset;
     }
     else {
        for (reg = 0; config_base[reg][0] != 0; reg++)
        {
          out_dword(config_base[reg][0] + offset, config_base[reg][1]);
        }
     }
  }
}


//================================================================================================//
// BIMC One-Time Settings
//================================================================================================//
void BIMC_Config_sdi(DDR_STRUCT *ddr)
{
   uint32   reg_bimc_base = 0;

   reg_bimc_base = ddr->base_addr.bimc_base_addr;

   Set_config(reg_bimc_base, bimc_global0_config_sdi);
   Set_config(reg_bimc_base, bimc_scmo_config_sdi);
   Set_config(reg_bimc_base, bimc_dpe_config_sdi);
   Set_config(reg_bimc_base, bimc_shke_config_sdi);
}

//================================================================================================//
// DDR PHY and CC one-time settings
//================================================================================================//
void DDR_PHY_CC_Config_sdi(DDR_STRUCT *ddr)
{
   uint32   reg_ddrss_base = 0;

   reg_ddrss_base = ddr->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all DQ PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_DQs);
   Set_config((reg_ddrss_base + BROADCAST_BASE), ddr_phy_dq_config_sdi);

   // Enable broadcast mode for all CA PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CAs);
   Set_config((reg_ddrss_base + BROADCAST_BASE), ddr_phy_ca_config_sdi);

   // Enable broadcast mode for all CCs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CCs);
   Set_config((reg_ddrss_base + BROADCAST_BASE), ddr_cc_config_sdi);

   // Disable broadcast mode
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE);
}


//================================================================================================//
// Update AC timing parameters from CDT, recalculate and load DPE timing actual registers
//================================================================================================//
void BIMC_Pre_Init_Setup_sdi(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8  ch              = 0;
   uint32 reg_offset_dpe  = 0;

   for (ch = 0; ch < NUM_CH; ch++)
   {
      reg_offset_dpe  = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);

      if ((channel >> ch) & 0x1)
      {
		 // Kick off timing parameter calculation and wait until done
         HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, RECALC_PS_PARAMS, 0x1);
         while (HWIO_INXF (reg_offset_dpe, DPE_MEMC_STATUS_1, CYC_CALC_VALID));

         // Load all the calculated settings into DPE actual registers
         HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
      }
   }
}


//================================================================================================//
// Device Initialization
// Select LPDDR3 or LPDDR4 initialization routines for enabling CK and CKE.
// Does ZQ calibration and RL/WL programming
//================================================================================================//
void BIMC_Memory_Device_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch = 0x0;

   BIMC_Memory_Device_Init_Lpddr3_sdi (ddr, channel, chip_select);

   for (ch = 0; ch < NUM_CH; ch++)
   {
      if ((channel >> ch) & 0x1)
      {
         //force a manual ZQCAL after Power Collapse
         BIMC_ZQ_Calibration_sdi (ddr, CH_1HOT(ch), chip_select);
      }
   }
}

//================================================================================================//
// LPDDR3 Device Initialization
// Does device initialization steps of enabling CK and CKE sequentially
//================================================================================================//
void BIMC_Memory_Device_Init_Lpddr3_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel ,DDR_CHIPSELECT chip_select)
{
   uint8 ch      = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++)
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         // Ensure CKE OFF for 100ns(2 tck) before reset_n de-asserts
         // CKE is off, set CKE off again to trigger timer, so it's just a fake timer.
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x02);


         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));

         // Turn CK on and wait 5tck
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x05);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_ON));

         // Turn CKE on and keep 200 us
         // Convert tINIT3 to XO clock cycle(0.052us)
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0xF06);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

         // Reset manual_1 timer
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x00);

         // Reset MRW, reset value is 0 at boot freq(10~55MHz), 0xFC at high freq
         //BIMC_Extended_MR_Write ((DDR_CHANNEL)ch_1hot, chip_select, 0x3F/*Addr=MR63*/, 0xFC/*opcode*/);		 
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MREG_WDATA, 0x3F, 0xFC);
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXTND_MODE_REGISTER_WRITE, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXTND_MODE_REGISTER_WRITE));

         // Wait tINIT4 1us after MRW reset
         // Convert tINIT4 to XO clock cycle(0.052us)
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x14);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

         // Reset manual_1 timer
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x00); 

      }
   }
}

//================================================================================================//
// BIMC DRAM Address Setting up
//================================================================================================//

void BIMC_DDR_Addr_Setup_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs)
{
   uint32 addr_base_9_2   = 0;
   uint32 size_in_mb      = 0;
   uint32 addr_mask       = 0;
   uint32 num_rows        = 0;
   uint32 num_cols        = 0;
   uint32 num_banks       = 0;
   uint32 reg_offset_scmo = 0;
   uint32 base_addr    = 0xFFFFFFFF;

   if (cs == 0)  // Rank 0
   {
      num_rows  = ddr->cdt_params[ch].common.num_rows_cs0;
      num_cols  = ddr->cdt_params[ch].common.num_cols_cs0;
      num_banks = ddr->cdt_params[ch].common.num_banks_cs0; 
	  if (ch == 0) {
	     base_addr = ddr->ddr_size_info.ddr0_cs0_addr;
		 size_in_mb = ddr->ddr_size_info.ddr0_cs0_mb;
      } else {
         base_addr = ddr->ddr_size_info.ddr1_cs0_addr;
		 size_in_mb = ddr->ddr_size_info.ddr1_cs0_mb;	
      }		 
   }
   else
   {
      num_rows  = ddr->cdt_params[ch].common.num_rows_cs1;
      num_cols  = ddr->cdt_params[ch].common.num_cols_cs1;
      num_banks = ddr->cdt_params[ch].common.num_banks_cs1;
	  if (ch == 0) {
	     base_addr = ddr->ddr_size_info.ddr0_cs1_addr;
		 size_in_mb = ddr->ddr_size_info.ddr0_cs1_mb;
	  } else {
         base_addr = ddr->ddr_size_info.ddr1_cs1_addr;  
		 size_in_mb = ddr->ddr_size_info.ddr1_cs1_mb;
      }
   }
	
   switch(size_in_mb){
      case 8192 : addr_mask = 0x80;
         break;
      case 4096 : addr_mask = 0xc0;
         break;
      case 2048 : addr_mask = 0xe0;
         break;
      case 1536 : addr_mask = 0xe0;
         break;  
      case 1024 : addr_mask = 0xf0;
         break;
	  case 768 : addr_mask = 0xf0;
         break;
      case 512  : addr_mask = 0xf8;
         break;
      case 256  : addr_mask = 0xfc;
         break;
      case 128  : addr_mask = 0xfe;
         break;
      case 64   : addr_mask = 0xff;
         break;
      default   : addr_mask = 0x00;
         break;
   }

   reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch);
   HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  cs, BANK_SIZE, (num_banks) >> 3);// 0x0:BANKS_4
                                                                                                     // 0x1:BANKS_8
   HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  cs, ROW_SIZE,  (num_rows) - 13); // 0x0:ROWS_13
                                                                                                     // 0x1:ROWS_14
                                                                                                     // 0x2:ROWS_15
                                                                                                     // 0x3:ROWS_16
   HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  cs, COL_SIZE,  (num_cols) - 8);  // 0x0:COLS_8
                                                                                                     // 0x1:COLS_9
                                                                                                     // 0x2:COLS_10
                                                                                                     // 0x3:COLS_11

   // Convert base addr to [9:2] for SCMO base CSR. Divide base address by 64MB which is the
   // minimum supported density (right shift by 26)
   addr_base_9_2 = (base_addr >> 26);
   HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_BASE_CSN, cs, ADDR_BASE, addr_base_9_2);
   HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MASK_CSN, cs, ADDR_MASK, addr_mask);

   if (num_banks != 0) {
      HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN, cs, RANK_EN, 1);
   }
}


//================================================================================================//
// BIMC Post Initialization sequence  //
// Channel=DDR_CH_BOTH is not supported in this function
//================================================================================================//
void BIMC_Post_Init_Setup_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 interface_width_index_cs0 = 0;
   uint8 interface_width_index_cs1 = 0;
   uint8 ch_inx                    = 0;
   uint32 reg_offset_dpe           = 0;
   uint32 reg_offset_scmo          = 0;
   uint32 reg_offset_shke          = 0;

   ch_inx          = CH_INX(channel);
   reg_offset_dpe  = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch_inx);
   reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch_inx);
   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch_inx);


   if (chip_select & DDR_CS0)
   {
      if (ddr->cdt_params[ch_inx].common.interleave_en & DDR_CS0_INTERLEAVE)
      {
         HWIO_OUTXF (reg_offset_scmo, SCMO_CFG_SLV_INTERLEAVE_CFG , INTERLEAVE_CS0, 1);
      }

      switch(ddr->cdt_params[ch_inx].common.interface_width_cs0)
      {
         case 64 : interface_width_index_cs0 = 0x3;
            break;
         case 32 : interface_width_index_cs0 = 0x2;
            break;
         case 16 : interface_width_index_cs0 = 0x1;
            break;
         default : interface_width_index_cs0 = 0x0;
            break;
      }

      HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_0, DEVICE_CFG_RANK0, interface_width_index_cs0);

      BIMC_DDR_Addr_Setup_sdi (ddr, ch_inx, CS_INX(DDR_CS0));

      // Bank number 4: 0x00; Bank number 8: 0x01
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, NUM_BANKS, (ddr->cdt_params[ch_inx].common.num_banks_cs0) >> 3 );

   }

   if (chip_select & DDR_CS1)
   {
      if (ddr->cdt_params[ch_inx].common.interleave_en & DDR_CS1_INTERLEAVE)
      {
         HWIO_OUTXF (reg_offset_scmo, SCMO_CFG_SLV_INTERLEAVE_CFG , INTERLEAVE_CS1, 1);
      }

      switch(ddr->cdt_params[ch_inx].common.interface_width_cs1)
      {
         case 64 : interface_width_index_cs1 = 0x3;
            break;
         case 32 : interface_width_index_cs1 = 0x2;
            break;
         case 16 : interface_width_index_cs1 = 0x1;
            break;
         default : interface_width_index_cs1 = 0x0;
            break;
      }

      HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_0, DEVICE_CFG_RANK1, interface_width_index_cs1);

      BIMC_DDR_Addr_Setup_sdi (ddr, ch_inx, CS_INX(DDR_CS1));

      //Bank number 4: 0x00; Bank number 8: 0x01
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, NUM_BANKS, (ddr->cdt_params[ch_inx].common.num_banks_cs1) >> 3 );
   }

   // bank count for both ranks
   HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_1, NUM_BANKS_RANK0, NUM_BANKS_RANK1,
                ddr->cdt_params[ch_inx].common.num_banks_cs0, ddr->cdt_params[ch_inx].common.num_banks_cs1);

   HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

   // Set rank init complete signal
   if (chip_select & DDR_CS0) {
      HWIO_OUTXF2 (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, RANK0_EN, 1, 1);
   }
   if (chip_select & DDR_CS1) {
      HWIO_OUTXF2 (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, RANK1_EN, 1, 1);
   }
   
   // Enable periodic functions: auto refresh,periodic ZQCAL
   // Auto refresh
   if (chip_select & DDR_CS0) {
      HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK0, 0x01);
   }
   if (chip_select & DDR_CS1) {
      HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK1, 0x01);
   }

   // Periodic ZQ calibration
     HWIO_OUTXF  (reg_offset_shke, SHKE_PERIODIC_ZQCAL, RANK_SEL, chip_select);

}

//================================================================================================//
// BIMC_Exit_Self_Refresh_sdi
// exit SW self refresh and enable HW self refresh
//================================================================================================//
void BIMC_Exit_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;
   uint32 reg_offset_scmo = 0;

   for (ch = 0; ch < NUM_CH; ch++)
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch);

      if ((channel >> ch) & 0x1)
      {
         //enable rank
         if (chip_select & DDR_CS0) {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  0/*rank0*/, RANK_EN, 1);
         }
         if (chip_select & DDR_CS1) {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  1/*rank1*/, RANK_EN, 1);
         }

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXIT_SELF_REFRESH, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXIT_SELF_REFRESH));

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH));

         //force a manual ZQCAL after Power Collapse
         BIMC_ZQ_Calibration_sdi (ddr, CH_1HOT(ch), chip_select);

         if (chip_select & DDR_CS0) {
            //force a refresh rate update
            HWIO_OUTXF (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, JEDEC_MR_4);
            HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_READ, DDR_CS0, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_READ));

            //BIMC_HW_Enter_Self_Refresh
            HWIO_OUTXF (reg_offset_shke, SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, 1);
         }
         if (chip_select & DDR_CS1) {
            //force a refresh rate update
            HWIO_OUTXF (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, JEDEC_MR_4);
            HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_READ, DDR_CS1, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_READ));

            //BIMC_HW_Enter_Self_Refresh
            HWIO_OUTXF (reg_offset_shke, SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, 1);
         }
      }
   }
}


void BIMC_Enter_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch = 0;
   uint32 reg_offset_shke = 0;
   uint32 reg_offset_scmo = 0;

   for (ch = 0; ch < NUM_CH; ch++)
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      reg_offset_scmo = ddr->base_addr.bimc_base_addr + REG_OFFSET_SCMO(ch);
      if ((channel >> ch) & 0x1)
      {
         if (chip_select & DDR_CS0)  {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  0/*rank index 0*/, RANK_EN, 0);
            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 0, 0);
         }
         if (chip_select & DDR_CS1)  {
            HWIO_OUTXFI (reg_offset_scmo, SCMO_CFG_ADDR_MAP_CSN,  1/*rank index 1*/, RANK_EN, 0);
            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 1, 0);
         }
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ENTER_SELF_REFRESH_IDLE, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE));
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH) == 0);
      }
   }
}
void BIMC_HW_Self_Refresh_Ctrl_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 enable)
{
   uint32 reg_offset_shke = 0;

   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

   if (ddr->misc.misc_cfg[0] == 1) {
      if (cs == 0) {
         HWIO_OUTXF (reg_offset_shke, SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, enable);
      }
      if (cs == 1) {
         HWIO_OUTXF (reg_offset_shke, SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, enable);
      }
   }
}

//================================================================================================//
//ZQ Cal function for lpddr3
// ZQ Cal is used on a per rank basis, chip_select_both is not supported
//================================================================================================//
void BIMC_ZQ_Calibration_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8 ch      = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0x7);
         if (chip_select & DDR_CS0) {

             HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LONG, DDR_CS0, 1);
             while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LONG));

         }
         if (chip_select & DDR_CS1) {

             HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LONG, DDR_CS1, 1);
             while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LONG));

         }
         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, 0x1, 0);  //reset MANUAL_1 timer 
      }
   }
}
