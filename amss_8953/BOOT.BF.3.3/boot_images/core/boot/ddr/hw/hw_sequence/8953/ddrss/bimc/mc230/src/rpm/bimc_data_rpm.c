/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/rpm/bimc_data_rpm.c#2 $
$DateTime: 2016/06/21 00:04:59 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"


//================================================================================================================================//
// Updates the CLKEN_WR_EXTEND, CLKEN_RD_EXTEND, TRAFFIC_WR_EXTEND, TRAFFIC_RD_EXTEND valus in DPE config_8 based on frequency
//================================================================================================================================//
uint32 dpe_config_8[]= {
   0x05070507 ,  /* 0       < F <= 19.2 MHz   */
   0x05070507 ,  /* 19.2    < F <= 100.8MHz   */
   0x05070507 ,  /* 100.8   < F <= 211.2MHz   */
   0x05070507 ,  /* 211.2   < F <= 278.4MHz   */
   0x05070507 ,  /* 278.4   < F <= 384  MHz   */
   0x05070507 ,  /* 384     < F <= 422.4MHz   */
   0x05070507 ,  /* 422.4   < F <= 556.8MHz   */
   0x05070507 ,  /* 556.8   < F <= 672  MHz  */
   0x05070507 ,  /* 672     < F <= 768  MHz  */
   0x05070507 ,  /* 768     < F <= 806.4MHz  */
   0x05070507 ,  /* 806.4   < F <= 844.8MHz  */
   0x05070507    /* 844.8   < F <= 931.2MHz  */
};

//================================================================================================//
// Read and Write Latency Tables
// RL, WL and corresponding MR2 values
//================================================================================================//

struct ecdt_dram_latency_runtime_struct RL_WL_lpddr_struct[] =
{
   /*RL,  WL, MR2  ,  freq*/
   { 6 ,  3,  0x04 , 400000 },  /* 0   < F <= 400MHz */
   { 8 ,  4,  0x06 , 533000 },  /* 400 < F <= 533MHz */
   { 9 ,  5,  0x07 , 600000 },  /* 533 < F <= 600MHz */
   { 10,  6,  0x08 , 667000 },  /* 600 < F <= 667MHz */
   { 11,  6,  0x09 , 733000 },  /* 667 < F <= 733MHz */
   { 12,  6,  0xa  , 800000 },  /* 733 < F <= 800MHz */
   { 14,  8,  0xc  , 933000 },  /* 800 < F <= 933MHz */
   { 16,  8,  0xe  , 1066000},  /* 933 < F <= 1066MHz */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0},  /* reserved */
   {  0,  0,    0  ,       0}   /* reserved */
};

struct ecdt_bimc_freq_switch_params_runtime_struct bimc_freq_switch_params_struct[] =
{
   /* MR3,  freq_switch_params_freq_range */
   // {0x03,    211200}, /*  10  < F <= 400MHz, WR-pre =1, DBI_WR=1          */
   // {0x01,    422400}, /* 400  < F <= 750MHz, PU-CAL=Vddq/2.5              */
   // {0x01,    844800}, /* 750  < F <= 1000MHz,                             */
   // {0x02,    931200}, /* 1000 < F <= 1295MHz CA-ODT=Rzq/2, DQ-ODT=Rzq/3   */
   // {   0,         0}, /* 1295 < F <= 1570MHz RD-PRE=1,  DQ ODT = 80ohm    */
   // {   0,         0}, /* 1570 < F <= 1890MHz WR-PST =1, DQ ODT = 60ohm    */
   // {   0,         0}, /* reserved                                         */
   // {   0,         0}  /* reserved                                         */
   
   /* MR3,  freq_switch_params_freq_range */
   {0x01,    211200}, /*  10  < F <= 400MHz, WR-pre =1, DBI_WR=1          */
   {0x01,    422400}, /* 400  < F <= 750MHz, PU-CAL=Vddq/2.5              */
   {0x01,    844800}, /* 750  < F <= 1000MHz,                             */
   {0x01,    931200}, /* 1000 < F <= 1295MHz CA-ODT=Rzq/2, DQ-ODT=Rzq/3   */
   {   0,         0}, /* 1295 < F <= 1570MHz RD-PRE=1,  DQ ODT = 80ohm    */
   {   0,         0}, /* 1570 < F <= 1890MHz WR-PST =1, DQ ODT = 60ohm    */
   {   0,         0}, /* reserved                                         */
   {   0,         0}  /* reserved                                         */
};


uint8 RL_WL_freq_range_table_size               = sizeof(RL_WL_lpddr_struct)/sizeof(struct ecdt_dram_latency_runtime_struct);
uint8 bimc_freq_switch_params_table_size = sizeof(bimc_freq_switch_params_struct)/sizeof(struct ecdt_bimc_freq_switch_params_runtime_struct);
//================================================================================================//
// Based on device RL/WL/ODTLon frequency band, get an index for selecting in RL/WL/ODTLon table
//================================================================================================//
uint8 BIMC_RL_WL_Freq_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 clk_idx;
   uint8 RL_WL_freq_range_table_size_mapped;

   //Map to the current active processor address map for accessibility
   RL_WL_freq_range_table_size_mapped = *((uint8 *)ddr_external_shared_addr((uint32 *)&RL_WL_freq_range_table_size));

   for (clk_idx = 0; (clk_idx < RL_WL_freq_range_table_size_mapped/*NUM_ECDT_DRAM_LATENCY_STRUCTS*/); clk_idx++)
   {
      if (clk_freq_khz <= ddr->extended_cdt_runtime.dram_latency[clk_idx].rl_wl_freq_in_kHz)
         break;
   }

   return clk_idx;
}

//================================================================================================//
// Based on Freq_Switch_Params band, get an index for selecting in Freq_Switch_Params table
//================================================================================================//
uint8 BIMC_Freq_Switch_Params_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 clk_idx = 0;
   uint8 bimc_freq_switch_params_table_size_mapped;

   //Map to the current active processor address map for accessibility
   bimc_freq_switch_params_table_size_mapped = *((uint8 *)ddr_external_shared_addr((uint32 *)&bimc_freq_switch_params_table_size));
   
   for (clk_idx = 0; clk_idx < bimc_freq_switch_params_table_size_mapped; clk_idx++)
   {
       if (clk_freq_khz <= ddr->extended_cdt_runtime.bimc_freq_switch[clk_idx].freq_switch_range_in_kHz)
           break;
   }
      
   return clk_idx;
}


