Instructions:
-------------

1. Open DDR calculator excel
2. Enable Macros
3. Run Macro to convert to CSV output file
4. CD to Test Folder
5. Run Perl script to translate CSV to ddr_config.c and ddr_config.h file
6. Copy ddr_config.c and ddr_config.h file to settings folder



