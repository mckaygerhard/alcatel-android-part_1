/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_wr_dqdqs_lpddr3.c#3 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#include "ddrss.h"
#include <string.h>

// -------------------------------------------------------------------------
// DDR PHY WR DQ-DQS Training.
// -------------------------------------------------------------------------
boolean DDRSS_wr_dqdqs_lpddr3 (DDR_STRUCT *ddr, 
                               uint8 ch, 
                               uint8 cs, 
                               training_params_t *training_params_ptr,
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 curr_training_freq_khz,
                               uint8  training_ddr_freq_indx,
                               uint8  prfs_index)
{    
    uint32          dq0_ddr_phy_base = 0; 
    uint8                        bit = 0;
    uint8                  byte_lane = 0;
    uint8                  retimer   = 0;

    uint8    fine_left_start_cdc_value[NUM_DQ_PCH] = {0};    
    uint8   fine_right_start_cdc_value[NUM_DQ_PCH] = {0}; 
    uint8           coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8           fine_wrlvl_delay  [NUM_DQ_PCH] = {0};

    uint8 max_training_freq_index                  = TRAINING_NUM_FREQ_LPDDR3 - 1;

    best_eye_struct wr_best_eye_coarse[NUM_DQ_PCH];

    // Training data structure pointer 
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
   
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;



    ddr_printf (DDR_NORMAL,"    WR Current clock  = %d PRFS_index = %d\n\n",curr_training_freq_khz ,prfs_index);
   
    // Initialize perbit and fine settings before WR training start.
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {   
      // Initialize the WRLVL delays
#if  DSF_WRLVL_TRAINING_EN
      coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
      fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
#endif
      // Initialize the per bit training values for coarse training
      if(training_ddr_freq_indx == max_training_freq_index)  
      {
          for(bit = 0; bit < PINS_PER_PHY; bit++)
          {
              DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                              bit, 
                                              1,  /* 1 for TX. */
                                              cs, 
                                              training_params_ptr->wr_dqdqs.pbit_start_value);
          }
      }

      // Initialize the best fine CDC to the start value
      wr_best_eye_coarse[byte_lane].best_fine_cdc_value = training_params_ptr->wr_dqdqs.fine_cdc_start_value;

      // Initialize the fine CDC values before training
      DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    training_params_ptr->wr_dqdqs.fine_cdc_start_value,
                                    0, 
                                    1, 
                                    cs);

    } // byte_lane 
    // Calibrate the WR per-bit at the highest frequency
    if(training_ddr_freq_indx == max_training_freq_index)  
    {

    // ---------------------------------------------------------------------------------
    // Coarse Training to find the right boundary (left edge of DQ)
    // ---------------------------------------------------------------------------------
        DDRSS_WR_CDC_Coarse_Schmoo (ddr, 
                                ch, 
                                cs, 
                                training_data_ptr, 
                                training_params_ptr, 
                                wr_best_eye_coarse, 
                                local_vars, 
                                curr_training_freq_khz,
                                training_ddr_freq_indx);

        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
        {   
      // Abort the routine of no coarse eye found
            if(wr_best_eye_coarse[byte_lane].all_fail_flag == 1)
            {
            return FALSE;
            }

        }// byte_lane
    ddr_printf(DDR_NORMAL,"\n");

    // ---------------------------------------------------------------------------------
    // Calibrate or restore the WR per-bit alignment 
    // ---------------------------------------------------------------------------------
    // Calibrate the WR per-bit at the highest frequency

      // Use the setup edge (actual left, code right) for per-bit calibration 
      for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {            
        fine_right_start_cdc_value[byte_lane] = wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value;
        ddr_printf (DDR_NORMAL,"    WR Byte %d Perbit Edge %d\n",byte_lane,fine_right_start_cdc_value[byte_lane]);
      }

      // WR per-bit calibration
      DDRSS_wr_pbit_schmoo(ddr, 
                           ch, 
                           cs, 
                           training_params_ptr, 
                           local_vars,
                           fine_right_start_cdc_value,
                           curr_training_freq_khz,
                           training_ddr_freq_indx);

    } 
    else
    {
      // Restore the previous max frequency per-bit values
      for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {            
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
            DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                            bit, 
                                            1,  /* 1 for TX. */
                                            cs, 
                                            training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][bit]);
        }
      }
    }


    ddr_printf(DDR_NORMAL,"\n");
     // ---------------------------------------------------------------------------------
    // Coarse Training to find the data eye boundaries
    // ---------------------------------------------------------------------------------
    DDRSS_WR_CDC_Coarse_Schmoo (ddr, 
                                ch, 
                                cs, 
                                training_data_ptr, 
                                training_params_ptr, 
                                wr_best_eye_coarse, 
                                local_vars, 
                                curr_training_freq_khz,
                                training_ddr_freq_indx);

    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {   
      // Abort the routine of no coarse eye found
      if(wr_best_eye_coarse[byte_lane].all_fail_flag == 1)
      {
        return FALSE;
      }
      ddr_printf(DDR_NORMAL,"    WR Byte %u Coarse L<->R Edge: %d <-> %d  : Width = %d \n",
                   byte_lane,
                   wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value,
                   wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value,
                   (wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value -
                    wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value));
    }// byte_lane
    // ---------------------------------------------------------------------------------
    // Fine Training.    
    // ---------------------------------------------------------------------------------
    if(training_params_ptr->wr_dqdqs.fine_training_enable == 1)
    {
      ddr_printf (DDR_NORMAL,"\n");

      for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {            
        // Coarse start CDC value for Fine search (code left, actual right) (small CDC)
        if (wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value != 0) 
        {
           wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value--;
        }
        fine_left_start_cdc_value[byte_lane] = wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value;

        // At max frequency, the (code left, actual right) boundary is algned to the Coarse CDC by the Per-bit
        //if(training_ddr_freq_indx != max_training_freq_index)  
        //{
          // If the Coarse boundary sum is odd, subtract one (code right, actual left)
          if (ddrss_lib_modulo((wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value + 
                 wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value) , 2) == 1)
          {
            wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value--;
          }
          fine_right_start_cdc_value[byte_lane] = wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value; 
          ddr_printf (DDR_NORMAL,"    WR Byte %d Fine start   Coarse Left = %d Coarse Right = %d\n",
                     byte_lane,fine_left_start_cdc_value[byte_lane],fine_right_start_cdc_value[byte_lane]);
        //}
        //else
        //{
        //    ddr_printf (DDR_NORMAL,"    WR Byte %d Fine adjusted Coarse Left = %d Coarse Right = Not Trained\n",
        //             byte_lane,fine_left_start_cdc_value[byte_lane],fine_right_start_cdc_value[byte_lane]);
        //}    
      } // byte_lane
      ddr_printf (DDR_NORMAL,"\n");
        
      // Fine Training search the left and right coarse boundaries
      DDRSS_WR_CDC_1D_Schmoo (ddr, 
                              ch, 
                              cs, 
                              training_data_ptr,
                              training_params_ptr, 
                              local_vars,
                              wr_best_eye_coarse, 
                              fine_left_start_cdc_value,
                              fine_right_start_cdc_value,
                              curr_training_freq_khz,
                              training_ddr_freq_indx);

    } // if (fine_training_enable

    // Write the Coarse and Fine training results to the registers and data structures
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      // Calculate the coarse center from the coarse search
      wr_best_eye_coarse[byte_lane].best_cdc_value = (wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value + 
                                                      wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value)/2;
  
      // If the coarse boundary sum is odd, add the equivalent half of course with fine steps
      if (ddrss_lib_modulo((wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value + 
             wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value) , 2) == 1)
      {
        wr_best_eye_coarse[byte_lane].best_fine_cdc_value += (FINE_STEPS_PER_COARSE/2);
      }



      // Write the left and right boundaries to the results data structure 
      training_data_ptr->results.wr_dqdqs.coarse_max_eye_left_boundary_cdc_value[training_ddr_freq_indx][ch][cs][byte_lane]  = 
                                 wr_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value;
      training_data_ptr->results.wr_dqdqs.coarse_max_eye_right_boundary_cdc_value[training_ddr_freq_indx][ch][cs][byte_lane] = 
                                 wr_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value;   
 
      // Write the trained coarse center to the results data structure
      training_data_ptr->results.wr_dqdqs.coarse_cdc [training_ddr_freq_indx][ch][cs][byte_lane] =
                         wr_best_eye_coarse[byte_lane].best_cdc_value;
            // Write the trained fine center offset to the results data structure
      training_data_ptr->results.wr_dqdqs.fine_cdc [training_ddr_freq_indx][ch][cs][byte_lane] = 
	                     wr_best_eye_coarse[byte_lane].best_fine_cdc_value; 

      // Calculate the retimer
      retimer = DDRSS_CDC_Retimer (ddr, 
                                   cs, 
                                   training_data_ptr->results.wr_dqdqs.coarse_cdc [training_ddr_freq_indx][ch][cs][byte_lane],
                                   training_data_ptr->results.wr_dqdqs.fine_cdc [training_ddr_freq_indx][ch][cs][byte_lane], 
                                   coarse_wrlvl_delay[byte_lane],
                                   fine_wrlvl_delay[byte_lane],
                                   (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                   curr_training_freq_khz);  

      // Write the trained retimer to the results data structure
      training_data_ptr->results.wr_dqdqs.dqs_retmr [training_ddr_freq_indx][ch][cs][byte_lane] = retimer;



      // Write the trained coarse center to the active CDC register
       DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                     training_data_ptr->results.wr_dqdqs.coarse_cdc [training_ddr_freq_indx][ch][cs][byte_lane], 
                                     1, 
                                     1, 
                                     cs);  // 1 for coarse_delay_mode. 1 for hp_mode.

      // Write the trained fine offset to the active CDC register
       DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                     training_data_ptr->results.wr_dqdqs.fine_cdc [training_ddr_freq_indx][ch][cs][byte_lane], 
                                     0, 
                                     1, 
                                     cs);  // 0 for fine_delay_mode. 1 for hp_mode.

      ddr_printf(DDR_NORMAL,"    WR Byte %d Center CDC Coarse = %d  Fine = %d\n",byte_lane,
                               training_data_ptr->results.wr_dqdqs.coarse_cdc [training_ddr_freq_indx][ch][cs][byte_lane],
                               training_data_ptr->results.wr_dqdqs.fine_cdc [training_ddr_freq_indx][ch][cs][byte_lane]);
    
      //---------------------------------------------------------------------------------------------------------------
      // Write the Ext registers and scale the trained results for lower bands
      //---------------------------------------------------------------------------------------------------------------

      // Write the current band EXT registers
      DDR_PHY_hal_cfg_cdcext_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                      cs, 
                                      training_data_ptr->results.wr_dqdqs.coarse_cdc [training_ddr_freq_indx][ch][cs][byte_lane],
                                      1/*coarse*/, 
                                      HP_MODE, 
                                      prfs_index
                                      );
    
      DDR_PHY_hal_cfg_cdcext_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                       cs, 
                                       training_data_ptr->results.wr_dqdqs.fine_cdc [training_ddr_freq_indx][ch][cs][byte_lane],
                                       0/*fine*/,   
                                       HP_MODE, 
                                       prfs_index
                                       );
    
      DDR_PHY_hal_cfg_wrlvlext_ctl_update((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                           prfs_index, 
                                           cs, 
                                           training_data_ptr->results.wr_dqdqs.dqs_retmr [training_ddr_freq_indx][ch][cs][byte_lane],
                                           0, // half cycle
                                           0 // full cycle
                                           );
 
     // Scale CDC to lower frequency bands
     /*Scaling lower frequencies*/      


   
     // End of Scaling lower frequency 
 
 } // byte_lane
 if (training_ddr_freq_indx == 0)
 { 
    Scale_wr_dqdqs_lpddr3 (  ddr, 
                             ch,
                             cs,
                             training_ddr_freq_indx,
                             curr_training_freq_khz, 
                             prfs_index - 1);
 }
  return TRUE;   
}//DDRSS_wr_dqdqs_lpddr3 







// -------------------------------------------------------------------------
// DDR PHY WR DQ-DQS Training Subroutines
// -------------------------------------------------------------------------

void DDRSS_WR_CDC_Coarse_Schmoo (DDR_STRUCT *ddr, 
                                 uint8 ch, 
                                 uint8 cs,                                     
                                 training_data *training_data_ptr,
                                 training_params_t *training_params_ptr,
                                 best_eye_struct *best_eye_ptr,
                                 ddrss_rdwr_dqdqs_local_vars *local_vars,
                                 uint32 clk_freq_khz,
                                 uint8 training_ddr_freq_indx)
{
    uint32   dq0_ddr_phy_base = 0;
    uint8           byte_lane = 0;
    uint8          loop_count = 0;
    uint8    coarse_cdc_value = 0;
  //uint8 clk_idx             = 0;
  //uint8 current_clk_inx     = 0;

    uint8     read_loop_count = 1;  // We [WR-RD]once together, for a max_loopcnt number of times.
    uint8     *compare_result; 
    uint8     coarse_dq_error_count[NUM_DQ_PCH] = {0};
    uint8        coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8        fine_wrlvl_delay  [NUM_DQ_PCH] = {0};
    
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
       best_eye_ptr[byte_lane].all_fail_flag  = 0; // Assume all sane values to begin with.
       best_eye_ptr[byte_lane].best_cdc_value = 0; // Assume initial value = 0, for initial retimer() setup.

      // Initialize the WRLVL delays
#if  DSF_WRLVL_TRAINING_EN
      coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
      fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
#endif
    }
    
    memset(local_vars->coarse_schmoo.coarse_dq_passband_info, 
           training_params_ptr->wr_dqdqs.max_loopcnt + 1, 
           NUM_DQ_PCH * COARSE_CDC);

    // WR Coarse CDC search
        for(coarse_cdc_value  = training_params_ptr->wr_dqdqs.coarse_cdc_start_value; 
            coarse_cdc_value <= training_params_ptr->wr_dqdqs.coarse_cdc_max_value; 
            coarse_cdc_value += training_params_ptr->wr_dqdqs.coarse_cdc_step)
        {
            for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
              DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                            coarse_cdc_value, 
                                            1, 
                                            1, 
                                            cs);
            
              DDRSS_CDC_Retimer (ddr, 
                                 cs, 
                                 coarse_cdc_value,
                                 0, 
                                 coarse_wrlvl_delay[byte_lane],
                                 fine_wrlvl_delay[byte_lane],
                                 (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                 clk_freq_khz);  // 0 for fine_delay.                
              
              coarse_dq_error_count[byte_lane] = 0;
            }
            
            for(loop_count = training_params_ptr->wr_dqdqs.max_loopcnt; loop_count > 0 ; loop_count--)
            {                 
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs);
                
                // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, read_loop_count,0x3);
                
                // Accumulate the error for all byte_lanes, based on loop_count number of times.
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
                {
                    coarse_dq_error_count[byte_lane] += compare_result[byte_lane];
                }
            } // LOOP_COUNT 
            
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
            { 
                // Populate the histogram with the result found.
                local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value] = coarse_dq_error_count[byte_lane]; 
            } 
           
    } // CDC_LOOP

    // Print the Coarse search histograms
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
    { 
      ddr_printf (DDR_NORMAL,"    WR Byte %d Coarse Search :",byte_lane);
      for (coarse_cdc_value=0;coarse_cdc_value<=training_params_ptr->wr_dqdqs.coarse_cdc_max_value;coarse_cdc_value++) 
      {
        ddr_printf (DDR_NORMAL,"%d ",local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value]);
      }
      ddr_printf (DDR_NORMAL,"\n");
    }
    ddr_printf (DDR_NORMAL,"\n");
    
    // Construct 1D histogram for the 4 bytelanes: [DQ][CDC]. 
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value  = 0xFF;
      best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value = 0x0;

      for (coarse_cdc_value=0;
           coarse_cdc_value<training_params_ptr->wr_dqdqs.coarse_cdc_max_value;
           coarse_cdc_value++)
      {
        if ((local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value] == 0) &&
            (best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value > coarse_cdc_value))
        {
          best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value = coarse_cdc_value;
        }

        if (local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value] == 0)
        {
          best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value = coarse_cdc_value;
        }
      }
    }// byte_lane
}//DDRSS_WR_CDC_Coarse_Schmoo

void DDRSS_wr_pbit_schmoo (DDR_STRUCT *ddr, 
                           uint8 ch, 
                           uint8 cs, 
                           training_params_t *training_params_ptr, 
                           ddrss_rdwr_dqdqs_local_vars *local_vars,
                           uint8 *coarse_cdc_right_start_value,
                           uint32 clk_freq_khz,
                           uint8 training_ddr_freq_indx)
{
    uint8 byte_lane                    = 0;
    uint32 dq0_ddr_phy_base            = 0;
    uint8  bit                         = 0;
    uint8  loopcnt                     = 0;
    uint8  perbit                      = 0;
  //uint8 clk_idx                      = 0;
  //uint8 current_clk_inx              = 0;

    uint8 *compare_result;
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};     
    uint8 perbit_cdc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};
    uint8        coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8        fine_wrlvl_delay  [NUM_DQ_PCH] = {0};
    uint16 perbit_sum                           = 0;

    extern uint8 dm_bit;
    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);    

    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    // Initialize the fail flag, best cdc, coarse and fine 
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {   
      DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    coarse_cdc_right_start_value[byte_lane],
                                     1, 
                                     1, 
                                     cs);  // 1 for coarse_delay_mode. 1 for hp_mode.

      // Initialize the WRLVL delays
#if   DSF_WRLVL_TRAINING_EN
      coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
      fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
#endif

      DDRSS_CDC_Retimer (ddr, 
                         cs, 
                         coarse_cdc_right_start_value[byte_lane],
                         training_params_ptr->wr_dqdqs.fine_cdc_top_freq_start_value,
                         coarse_wrlvl_delay[byte_lane],
                         fine_wrlvl_delay[byte_lane],
                         (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                         clk_freq_khz);                                  

    }
   ddr_printf (DDR_NORMAL ,"\n");
    
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        //Reset perbit_cdc
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
            perbit_cdc [byte_lane][bit] = 0x0;
        }

        for (perbit = 0; perbit <= PERBIT_CDC_MAX_VALUE; perbit++)
        {
            for(bit = 0; bit < PINS_PER_PHY; bit++)  //PHY bit
            {
              // Sweep all of the perbit at once
              DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                             bit, 
                                             1,   // 1 for TX.
                                             cs, 
                                             perbit);
            }
            
            // Reset the compare results
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                compare_result_acc[byte_lane][bit]  = 0x0;
            }
        
            for(loopcnt = training_params_ptr->wr_dqdqs.max_loopcnt; loopcnt > 0 ; loopcnt--)
            {         
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs); 

                compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, byte_lane, 0x0);
        
                for (bit = 0; bit < PINS_PER_PHY; bit++)
                {
                    // fine training accumulated results for every other bit.
                    compare_result_acc [byte_lane][bit] += 
                    compare_result [(byte_lane*PINS_PER_PHY) + bit];                        
                }
            }
            
            // Search for Pass -> Fail to calibrate per-bit
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
              //ddr_printf(DDR_NORMAL,"  WR Byte %d Bit %d perbit %d Result = %d\n",byte_lane,bit,perbit,compare_result_acc[byte_lane][bit]);
              if ((compare_result_acc[byte_lane][bit] == 0x0) && (perbit >= perbit_cdc[byte_lane][bit]))
              { 
                // perbit_cdc is in PHY order
                  perbit_cdc[byte_lane][bit] = perbit ; 
              }   
            } // bit loop
        } // perbit loop
    } // byte loop


    // Populate the PHY per-bit lanes with the trained values
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {  
      perbit_sum = 0;
      for (bit = 0; bit < PINS_PER_PHY; bit ++)
      {
        DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       bit, 
                                       1,   // 1 for TX.
                                       cs, 
                                       perbit_cdc[byte_lane][bit]);
        if (bit == dm_bit)
        {
            // training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = 0x8;
        } 
        else
        {
             training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = perbit_cdc[byte_lane][bit]; 
             perbit_sum += perbit_cdc[byte_lane][bit] ;
        }       

      }
      // Average the perbit values and write it to dm_bit
      training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][dm_bit] = perbit_sum / (PINS_PER_PHY - 1) ;
      for (bit = 0; bit < PINS_PER_PHY; bit ++)
      {
        ddr_printf(DDR_NORMAL,"    Byte %d PHY bit %d WR DCC Perbit = %d\n",
                                byte_lane,bit, training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][bit]);
      }
      ddr_printf(DDR_NORMAL,"\n");
    }
    
}//DDRSS_wr_pbit_schmoo                

void DDRSS_WR_CDC_1D_Schmoo (DDR_STRUCT *ddr, 
                             uint8 ch, 
                             uint8 cs, 
                             training_data *training_data_ptr,
                             training_params_t *training_params_ptr, 
                             ddrss_rdwr_dqdqs_local_vars *local_vars,
                             best_eye_struct *best_eye_ptr,
                             uint8 *coarse_cdc_left_start_value,
                             uint8 *coarse_cdc_right_start_value,
                             uint32 clk_freq_khz,
                             uint8 training_ddr_freq_indx)                                
{
    uint32   dq0_ddr_phy_base   = 0;
    uint8           byte_lane   = 0;
    //uint8       max_training_freq_index = TRAINING_NUM_FREQ_LPDDR3 -1;
    uint8     left_boundary_eye_cdc_value [NUM_DQ_PCH] = {0};
    uint8    right_boundary_eye_cdc_value [NUM_DQ_PCH] = {0};

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;   



    // Update Coarse CDC value
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    coarse_cdc_left_start_value[byte_lane], 
                                    1, 
                                    1, 
                                    cs);   // 1 for coarse_delay_mode. 1 for hp_mode.
    }

    // Find the fine value in the vicinity of C(Left) (Actual right)
    DDRSS_WR_CDC_1D_Fine_Schmoo (ddr, 
                                 ch, 
                                 cs, 
                                 training_data_ptr, 
                                 training_params_ptr, 
                                 local_vars, 
                                 left_boundary_eye_cdc_value,
                                 right_boundary_eye_cdc_value,
                                 coarse_cdc_left_start_value,
                                 clk_freq_khz, 
                                 training_ddr_freq_indx, 
                                 SWEEP_LEFT);

     // At the maximum frequency, the (code right, actual left) coarse boundary is aligned with per-bit
     //if (training_ddr_freq_indx != max_training_freq_index)
     //{
       // Update Coarse CDC value
       for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
       {
         DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       coarse_cdc_right_start_value[byte_lane], 
                                       1, 
                                       1, 
                                       cs);   // 1 for coarse_delay_mode. 1 for hp_mode.
       }

       // Calculate the fine value in the vicinity of C(Right)
       DDRSS_WR_CDC_1D_Fine_Schmoo (ddr,  
                                    ch,  
                                    cs,  
                                    training_data_ptr,  
                                    training_params_ptr,  
                                    local_vars,  
                                    left_boundary_eye_cdc_value,
                                    right_boundary_eye_cdc_value,
                                    coarse_cdc_right_start_value,
                                    clk_freq_khz,  
                                    training_ddr_freq_indx,  
                                    SWEEP_RIGHT);

       // Calculate the total fine offset
       for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
       {
         best_eye_ptr[byte_lane].best_fine_cdc_value = ddrss_lib_uidiv((left_boundary_eye_cdc_value[byte_lane] + 
                                                                        right_boundary_eye_cdc_value[byte_lane]),2);
       }
     //}
}//DDRSS_WR_CDC_1D_Schmoo 

void DDRSS_WR_CDC_1D_Fine_Schmoo (DDR_STRUCT *ddr, 
                                  uint8 ch, 
                                  uint8 cs, 
                                  training_data *training_data_ptr,
                                  training_params_t *training_params_ptr, 
                                  ddrss_rdwr_dqdqs_local_vars *local_vars,
                                  uint8 *left_boundary_eye_cdc_value,
                                  uint8 *right_boundary_eye_cdc_value,
                                  uint8 *coarse_cdc_value,
                                  uint32 clk_freq_khz,
                                  uint8 training_ddr_freq_indx,// current prfs index
                                  uint8 direction /*0 for Cl and 1 for Cr*/)                                
{
    uint32   dq0_ddr_phy_base = 0;
    uint8           byte_lane = 0;
    uint8          loop_count = 0;
    uint8      fine_cdc_value = 0;
    uint8  fine_cdc_max_value = 0;
    uint8      fine_cdc_index = 0;
    uint8               index = 0; 
  //uint8 clk_idx             = 0;
  //uint8 current_clk_inx     = 0;
    
    uint8     *compare_result;
    
    uint8     dq_error_count[NUM_DQ_PCH] = {0};
    uint8 coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8 fine_wrlvl_delay  [NUM_DQ_PCH] = {0};

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    fine_cdc_max_value = training_params_ptr->wr_dqdqs.fine_cdc_max_value;

    // Pre-set the passband histogram
    if (direction == SWEEP_LEFT)
    {
      memset(local_vars->fine_schmoo.fine_dq_passband_info_left, 
             training_params_ptr->wr_dqdqs.max_loopcnt + 1, 
            (NUM_DQ_PCH * (FINE_CDC)));
    }
    else 
    {
      memset(local_vars->fine_schmoo.fine_dq_passband_info_right, 
             training_params_ptr->wr_dqdqs.max_loopcnt + 1, 
            (NUM_DQ_PCH * (FINE_CDC)));
    }

#if  DSF_WRLVL_TRAINING_EN
    // Initialize the WRLVL delays
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
      fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
    }
#endif

    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Fine CDC Schmoo loop
        for(fine_cdc_value  = training_params_ptr->wr_dqdqs.fine_cdc_start_value;
            fine_cdc_value <= training_params_ptr->wr_dqdqs.fine_cdc_max_value;
            fine_cdc_value += training_params_ptr->wr_dqdqs.fine_cdc_step)
        {
            // Update Fine CDC
            DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           fine_cdc_value, 
                                           0, 
                                           1, 
                                           cs);   // 0 for fine_delay_mode. 1 for hp_mode.  
            DDRSS_CDC_Retimer (ddr, 
                                 cs, 
                                 coarse_cdc_value[byte_lane],
                                 fine_cdc_value, 
                                 coarse_wrlvl_delay[byte_lane],
                                 fine_wrlvl_delay[byte_lane],
                                 (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                 clk_freq_khz);  // 0 for fine_delay. 

            fine_cdc_index = ddrss_lib_uidiv(fine_cdc_value, training_params_ptr->wr_dqdqs.fine_cdc_step);
            dq_error_count[byte_lane] = 0;
            
            for(loop_count = training_params_ptr->wr_dqdqs.max_loopcnt; loop_count > 0 ; loop_count--)
            {                 
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs);
                
                // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x3);
                
                // Accumulate the error for all byte_lanes, based on loop_count number of times.
                dq_error_count[byte_lane] += compare_result[byte_lane];    
            }
        
            // Populate the histogram every coarse and fine cdc with the result found.
            if (direction == SWEEP_LEFT)
            {
              local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][fine_cdc_index] = dq_error_count[byte_lane];
            }
            else
            {
              local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][fine_cdc_index] = dq_error_count[byte_lane];
            }
           
          }

        // Print the fine histograms
        if (direction == SWEEP_LEFT)
        {
          ddr_printf (DDR_NORMAL,"    WR Byte %d Left Fine :",byte_lane );
          for(index = 0; index <= fine_cdc_max_value; index++)
          {
            ddr_printf(DDR_NORMAL,"%d ", local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][index]);
          }
          ddr_printf (DDR_NORMAL,"\n");
        }
        if (direction == SWEEP_RIGHT)
        {
          ddr_printf (DDR_NORMAL,"    WR Byte %d Right Fine :",byte_lane );
          for(index = 0; index <= fine_cdc_max_value; index++)
          {
            ddr_printf(DDR_NORMAL,"%d ", local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][index]);
          }
          ddr_printf (DDR_NORMAL,"\n");
        }

        // Find the fine boundary.
        if (direction == SWEEP_LEFT)
        {
          for(index = 0; index <= fine_cdc_max_value; index++)
          {
            // Find the left (actual right) fine edge. F->P transition.
            if(local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][index] == 0)
            {
                left_boundary_eye_cdc_value[byte_lane] = index;
                break;
            }
          }
        }

        if (direction == SWEEP_RIGHT)
        {
          for(index = 0; index <= fine_cdc_max_value; index++)
          {
            // Find the right (actual left) fine edge. F->P transition.
            if((local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][index] != 0) && (index != 0))
            {
                right_boundary_eye_cdc_value[byte_lane] = index - 1;
                break;
            }
          }
        }
    } // byte_lane
    printf ("\n");
}//DDRSS_WR_CDC_1D_Fine_Schmoo 
/* This API performs write scaling based on the last trained frequency*/
void Scale_wr_dqdqs_lpddr3 (DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index)
{
    uint8 byte_lane                              =  0;
    uint8 clk_idx                                =  0;
    uint8 coarse_wrlvl_delay[NUM_DQ_PCH]         = {0};
    uint8 fine_wrlvl_delay  [NUM_DQ_PCH]         = {0};
    uint32 dq0_ddr_phy_base                      =  0;
    uint8 scale_prfs_indx                        =  0; 
    int   scale_freq_indx                        =  0; 
    int32 scale_offset_in_ps[NUM_DQ_PCH]        = {0};
    uint32 scale_center_in_ps                    =  0;  
    uint32 scale_wr_coarse_cdc                   =  0;  
    uint32 scale_wr_fine_cdc                     =  0;
    uint32 scale_retimer                         =  0;
    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) //calculate scale offset bytewise
    {
        scale_offset_in_ps[byte_lane] = ((training_data_ptr->results.wr_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane] * COARSE_STEP_IN_PS)
                                      +
                                      (training_data_ptr->results.wr_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane] * FINE_STEP_IN_PS))
                                      -
                                      ((1000000000 / trained_clk_freq_khz)/4); // determine the offset for scaling based on the last trained values.
    }
    clk_idx = ddr->misc.ddr_num_clock_levels - 1;//max clock frequency index (corresponding to 932 MHz).
    for(scale_prfs_indx = scale_start_prfs_index; scale_prfs_indx >= SCALING_STOP_PRFS; scale_prfs_indx--)//Scale all the scaling bands
    {
        //Determine the scaling frequency in khz
        for(scale_freq_indx = clk_idx; scale_freq_indx >= 0; scale_freq_indx--)
        {
            if (ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz <= freq_range[scale_prfs_indx])
                break;
        }
        ddr_printf(DDR_NORMAL,"\n  Scaling frequency = %u , prfs_level = %d\n",ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz,
                                                                scale_prfs_indx);
        clk_idx = scale_freq_indx;

        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
        {
        #if  DSF_WRLVL_TRAINING_EN
            coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
            fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][byte_lane];
        #endif
            scale_center_in_ps  = scale_offset_in_ps[byte_lane]
                                 +
                                 ddrss_lib_uidiv(ddrss_lib_uidiv(1000000000 , ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz),4);
            scale_wr_coarse_cdc = ddrss_lib_uidiv(scale_center_in_ps ,COARSE_STEP_IN_PS);
            scale_wr_fine_cdc   = ddrss_lib_uidiv(ddrss_lib_modulo(scale_center_in_ps, COARSE_STEP_IN_PS), FINE_STEP_IN_PS);
            scale_retimer = DDRSS_CDC_Retimer(ddr, 
                                              cs, 
                                              scale_wr_coarse_cdc,
                                              scale_wr_fine_cdc, 
                                              coarse_wrlvl_delay[byte_lane],
                                              fine_wrlvl_delay[byte_lane],
                                              (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                              ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz); 
            DDR_PHY_hal_cfg_cdcext_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                             cs,
                                             scale_wr_coarse_cdc,
                                             1,//coarse 
                                             HP_MODE,
                                             scale_prfs_indx
                                             ); 
            DDR_PHY_hal_cfg_cdcext_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                             cs,
                                             scale_wr_fine_cdc,
                                             0,//fine
                                             HP_MODE,
                                             scale_prfs_indx  
                                             );
            DDR_PHY_hal_cfg_wrlvlext_ctl_update((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                                 scale_prfs_indx,
                                                 cs,
                                                 scale_retimer,
                                                 0, // half cycle
                                                 0 // full cycle
                                                 );
            ddr_printf(DDR_NORMAL,"    WR Byte %u Data Eye Center Coarse = %d Fine = %d \n",
                      byte_lane,
                      scale_wr_coarse_cdc,
                      scale_wr_fine_cdc);

        }
              
    }
    
}
//End of write scaling

