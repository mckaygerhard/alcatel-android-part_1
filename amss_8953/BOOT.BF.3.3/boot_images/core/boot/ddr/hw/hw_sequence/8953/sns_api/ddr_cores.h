#ifndef __DDR_CORES_H__
#define __DDR_CORES_H__

/*=============================================================================

                                DDR CORES
                                Header File
GENERAL DESCRIPTION
 This file defines the DDR controller and DDR PHY core data structures


Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/sns_api/ddr_cores.h#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
12/01/13   dp      Initial version.
==============================================================================*/
/*==========================================================================
                               INCLUDE FILES
===========================================================================*/
//#include "comdef.h"
#include "HALcomdef.h"

/*==============================================================================
                                  TYPES
==============================================================================*/

/**********************/
/*** DDR Controller ***/
/**********************/

struct ddr_ctlr
{
  struct
  {
   uint32 arch;
	uint32 major;
	uint32 minor;
  } version;
};

/**********************/
/*** DDR PHY  ***/
/**********************/

struct ddr_phy
{
	struct
	{
	  uint32 major;
	  uint32 minor;
	} version;
};

#endif /* __DDR_CORES_H__ */
