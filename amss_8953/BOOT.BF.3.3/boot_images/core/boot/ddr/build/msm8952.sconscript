#===============================================================================
#
# DDR Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright 2009-2015 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/build/msm8952.sconscript#3 $
#  $DateTime: 2016/03/21 06:24:55 $
#  $Author: pwbldsvc $
#  $Change: 10109066 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 03/10/16   yps     updated 8952 DDI files
# 05/28/15   yps     Added DDI support for MSM8952
# 03/24/15   sk      updated 8952 config files
# 03/02/15   sk      updated RPM_SHARED_IMEM_DDR_PARAM_BASE as IMEM address location
# 10/11/14   sk      Initial Version.
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

env.PublishPrivateApi('DDR', [
  "${INC_ROOT}/core/boot/ddr/hw/msm8952/",
  "${INC_ROOT}/core/boot/ddr/common/params/v2/",
  "${INC_ROOT}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/",
  "${INC_ROOT}/core/boot/ddr/hw/hw_sequence/PHY/v2.5/",
  "${INC_ROOT}/core/boot/ddr/hw/hw_sequence/sdi/msm8952/",
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
  'BOOT',
  'SERVICES',
  'SYSTEMDRIVERS',
  'DAL',
  'KERNEL',
  'BUSES',
  'POWER',
]

if 'USES_BOOT_DDR_DEBUG_MODE' in env:
  CBSP_API += [
    'WIREDCONNECTIVITY',
  ]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.Append(CPPDEFINES = [
  "SHARED_IMEM_DDR_PARAM_BASE=0x08600190",
  "RPM_SHARED_IMEM_DDR_PARAM_BASE=0x68600190", 
  "SHARED_IMEM_DDR_TRAINING_COOKIE=0x08600020"
])

env.Append(CPPDEFINES = [
  "FEATURE_LPDDR3"
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
DDR_DRIVERS_SOURCES =[
  '${BUILDPATH}/core/boot/ddr/common/ddr_sync.c',
  '${BUILDPATH}/core/boot/ddr/common/params/v2/ddr_params.c',
  '${BUILDPATH}/core/boot/ddr/common/ddr_seq_drivers.c',
  '${BUILDPATH}/core/boot/ddr/hw/msm8952/ddr_target.c',
  '${BUILDPATH}/core/boot/ddr/hw/msm8952/ddr_rpm_target.c',  
  '${BUILDPATH}/core/boot/ddr/hw/msm8952/ddr_external_api.c',
]

DDR_RPM_MESSAGE_SOURCES = [
]

DDR_TEST_SOURCES = [
  '${BUILDPATH}/core/boot/ddr/common/ddr_test.c',
]

if 'USES_BOOT_DDR_DEBUG_MODE' in env:
  DDR_TEST_SOURCES += [
    '${BUILDPATH}/core/boot/ddr/common/ddr_debug_common.c',
    '${BUILDPATH}/core/boot/ddr/common/ddr_debug_phy_v2.c',
    '${BUILDPATH}/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose.c',
    '${BUILDPATH}/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose_bsp_8952.c',
    '${BUILDPATH}/core/storage/tools/deviceprogrammer_ddr/src/firehose/deviceprogrammer_xml_parser.c',
    '${BUILDPATH}/core/storage/tools/deviceprogrammer_ddr/src/firehose/deviceprogrammer_utils.c',
    '${BUILDPATH}/core/storage/tools/deviceprogrammer_ddr/src/firehose/deviceprogrammer_security.c',
  ]
if 'USES_BOOT_DDR_DEBUG_MODE' in env:
 env.PublishPrivateApi('DDR', [
	  "${INC_ROOT}/core/storage/tools/deviceprogrammer_ddr/src/firehose",
	  "${INC_ROOT}/core/api/kernel/libstd/stringl",
	  "${INC_ROOT}/core/securemsm/secboot/api",
	  "${INC_ROOT}/core/api/securemsm/secboot",
	  "${INC_ROOT}/core/securemsm/cryptodrivers/ce/shared/inc",
	  "${INC_ROOT}/core/storage/tools/deviceprogrammer_ddr/src/bsp",
	  "${INC_ROOT}/core/boot/ddr/tools/ddi/src/firehose",
	  "${INC_ROOT}/core/api/debugtools",
	])
DDR_HAL_SOURCES = [
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_ddrss_wrapper.c',
]

DDR_SCALE_SOURCES = [
#BIMC SCALe
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_mc_shke.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_mc_dpe.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_mc_scmo.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_global.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/ebi.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/bimc_config_8952.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/ddrcc_config_8952.c', 
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/BIMC/v2.2/ddrss_rcw.c',   

#PHY SCALe
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/PHY/v2.5/ddr_phy_ca.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/PHY/v2.5/ddr_phy_dq.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/PHY/v2.5/ddr_phy_ebi.c',
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/PHY/v2.5/ddr_phy_ddrss.c',
  
#SDI SCALe
  '${BUILDPATH}/core/boot/ddr/hw/hw_sequence/sdi/msm8952/ddrss_init_sdi.c',
]

if 'BUILD_BOOT_CHAIN' in env:
  DDR_HAL_SOURCES += []

ddr_drivers_lib = env.Library('${BUILDPATH}/DDR_DRIVERS', DDR_DRIVERS_SOURCES)
ddr_test_lib = env.Library('${BUILDPATH}/DDR_TEST', DDR_TEST_SOURCES)
ddr_hal_lib = env.Library('${BUILDPATH}/DDR_HAL', DDR_HAL_SOURCES)
ddr_scale_lib = env.Library('${BUILDPATH}/DDR_SCALE', DDR_SCALE_SOURCES)

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddLibsToImage('DDR_BOOT_DRIVER', [ddr_drivers_lib, ddr_test_lib, ddr_hal_lib,ddr_scale_lib])
env.AddLibsToImage('RPM_IMAGE', [ddr_drivers_lib, ddr_hal_lib])
