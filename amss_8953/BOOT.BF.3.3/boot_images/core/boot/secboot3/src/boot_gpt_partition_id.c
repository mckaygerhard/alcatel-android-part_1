/**
 * @file boot_gpt_partition_id.c
 * @brief
 * Source file contains the GUID for sbl2,sbl3,tz,rpm and appsbl
 *
 */

/*==========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_gpt_partition_id.c#3 $
$DateTime: 2016/08/11 06:07:47 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ---------------------------------------------------------
08/09/16   sj	   Added the GUID of logdump partition for FR37098	
07/09/15   aus     Added GUID for DEVCFG partition
05/19/15   lm      Added GUID for APDP partition
03/21/14   ck      Added GUID for SEC partition
10/17/13   ck      Split TZ into QSEE and QHEE and created GUID's for them.
03/19/13   dh      Added GUID for ram dump parition
12/05/12   jz      Added GUID for CDT partition
07/20/12   dh      Add GUID for ddr training parameter partition
03/22/12   dh      Add GUID for wdog debug image
04/05/11   plc     Add GUID info for SBL1 
03/24/11   dh      Initial creation 

============================================================================
                     Copyright 2014 Qualcomm Technologies Incorporated.
                            All Rights Reserved.
                    Qualcomm Confidential and Proprietary
===========================================================================*/

/*==========================================================================
                             INCLUDE FILES
===========================================================================*/
#include "boot_comdef.h"
#include "fs_hotplug.h"

/*define GUID for SBL1*/
/*This is the GUID that PBL expects to identify and load the first SBL*/
struct hotplug_guid sbl1_partition_id = 
      /*{DEA0BA2C-CBDD-4805-B4F9-F428251C3E98}*/
      { 0xDEA0BA2C, 0xCBDD, 0x4805, { 0xB4, 0xF9, 0xF4, 0x28, 0x25, 0x1C, 0x3E, 0x98 } };

/*define GUID for SBL2*/
struct hotplug_guid sbl2_partition_id = 
      /*{8C6B52AD-8A9E-4398-AD09-AE916E53AE2D}*/
      { 0x8C6B52AD, 0x8A9E, 0x4398, { 0xAD, 0x09, 0xAE, 0x91, 0x6E, 0x53, 0xAE, 0x2D } };

/*define GUID for SBL3*/       
struct hotplug_guid sbl3_partition_id = 
      /*{05E044DF-92F1-4325-B69E-374A82E97D6E}*/
      { 0x05E044DF, 0x92F1, 0x4325, { 0xB6, 0x9E, 0x37, 0x4A, 0x82, 0xE9, 0x7D, 0x6E } };
      
/*define GUID for APPSBL*/      
struct hotplug_guid appsbl_partition_id = 
      /*{400FFDCD-22E0-47E7-9A23-F16ED9382388}*/
      { 0x400FFDCD, 0x22E0, 0x47E7, { 0x9A, 0x23, 0xF1, 0x6E, 0xD9, 0x38, 0x23, 0x88 } };
      
/*define GUID for QSEE*/
struct hotplug_guid qsee_partition_id = 
      /*{A053AA7F-40B8-4B1C-BA08-2F68AC71A4F4}*/
      { 0xA053AA7F, 0x40B8, 0x4B1C, { 0xBA, 0x08, 0x2F, 0x68, 0xAC, 0x71, 0xA4, 0xF4 } };

/*define GUID for QHEE*/
struct hotplug_guid qhee_partition_id = 
      /*{E1A6A689-0C8D-4CC6-B4E8-55A4320FBD8A}*/
      { 0xE1A6A689, 0x0C8D, 0x4CC6, { 0xB4, 0xE8, 0x55, 0xA4, 0x32, 0x0F, 0xBD, 0x8A } };
 
/*define GUID for RPM*/      
struct hotplug_guid rpm_partition_id = 
      /*{098DF793-D712-413D-9D4E-89D711772228}*/
      { 0x098DF793, 0xD712, 0x413D, { 0x9D, 0x4E, 0x89, 0xD7, 0x11, 0x77, 0x22, 0x28 } };
      
/*define GUID for wdog debug image*/      
struct hotplug_guid wdt_partition_id = 
      /*{D4E0D938-B7FA-48C1-9D21-BC5ED5C4B203}*/
      { 0xD4E0D938, 0xB7FA, 0x48C1, { 0x9D, 0x21, 0xBC, 0x5E, 0xD5, 0xC4, 0xB2, 0x03 } };
      
/*define GUID for DDR params partiton*/      
struct hotplug_guid ddr_params_partition_id =
      /*{20A0C19C-286A-42FA-9CE7-F64C3226A794}*/
      { 0x20A0C19C, 0x286A, 0x42FA, { 0x9C, 0xE7, 0xF6, 0x4C, 0x32, 0x26, 0xA7, 0x94 } };
      
/*define GUID for CDT partition*/
struct hotplug_guid cdt_partition_id =
      /*{a19f205f-ccd8-4b6d-8f1e-2d9bc24cffb1}*/
      { 0xA19F205F, 0xCCD8, 0x4B6D, { 0x8F, 0x1E, 0x2D, 0x9B, 0xC2, 0x4C, 0xFF, 0xB1 } };

/*define GUID for Ram Dump partition*/
struct hotplug_guid ram_dump_partition_id =
      /*{66C9B323-F7FC-48B6-BF96-6F32E335A428}*/
      { 0x66C9B323, 0xF7FC, 0x48B6, { 0xBF, 0x96, 0x6F, 0x32, 0xE3, 0x35, 0xA4, 0x28 } };

struct hotplug_guid log_dump_partition_id =
	  /*{5AF80809-AABB-4943-9168-CDFC38742598 } */	
	   { 0x5AF80809, 0xAABB, 0x4943, { 0x91, 0x68, 0xCD, 0xFC, 0x38, 0x74, 0x25, 0x98 } };	
   


/*define GUID for SEC partition*/
struct hotplug_guid sec_partition_id =
      /*{303E6AC3-AF15-4C54-9E9B-D9A8FBECF401}*/
      { 0x303E6AC3, 0xAF15, 0x4C54, { 0x9E, 0x9B, 0xD9, 0xA8, 0xFB, 0xEC, 0xF4, 0x01 } };

/*define GUID for DPO Debug Policy Override partition*/
struct hotplug_guid dpo_partition_id =
      /*{11406F35-1173-4869-807B-27DF71802812}*/
      { 0x11406F35, 0x1173, 0x4869, { 0x80, 0x7B, 0x27, 0xDF, 0x71, 0x80, 0x28, 0x12 } };
      
/*define GUID for APDP Apps Policy Debug partition*/
struct hotplug_guid apdp_partition_id = 
      /*{E6E98DA2-E22A-4D12-AB33-169E7DEAA507}*/
      { 0xE6E98DA2, 0xE22A, 0x4D12, { 0xAB, 0x33, 0x16, 0x9E, 0x7D, 0xEA, 0xA5, 0x07 } };

/*define GUID for DEVCFG partition*/
struct hotplug_guid devcfg_partition_id =
      /*{F65D4B16-343D-4E25-AAFC-BE99B6556A6D}*/
      { 0xF65D4B16, 0x343D, 0x4E25, { 0xAA, 0xFC, 0xBE, 0x99, 0xB6, 0x55, 0x6A, 0x6D } };

/*define GUID for traceability partition*/
struct hotplug_guid traceability_partition_id =
      /*{92A4552D-A9A3-47D0-AC0D-1A12E09EEA3C}*/
      { 0x92A4552D, 0xA9A3, 0x47D0, { 0xAC, 0x0D, 0x1A, 0x12, 0xE0, 0x9E, 0xEA, 0x3C } };

/*define GUID for userdata partition*/
struct hotplug_guid userdata_partition_id =
      /*{1B81E7E6-F50D-419B-A739-2AEEF8DA3335}*/
      { 0x1B81E7E6, 0xF50D, 0x419B, { 0xA7, 0x39, 0x2A, 0xEE, 0xF8, 0xDA, 0x33, 0x35 } };

