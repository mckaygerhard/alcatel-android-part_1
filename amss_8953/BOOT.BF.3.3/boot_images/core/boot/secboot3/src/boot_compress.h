#ifndef BOOT_COMPRESS_H
#define BOOT_COMPRESS_H

/*=============================================================================

                               Boot Compress
                               Header File
GENERAL DESCRIPTION
  This file provides the APIs for compression of crash dumps.
  
EXTERNALIZED FUNCTIONS
  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  Make sure to use boot_compress_init() before calling boot_compress_input()
  and to finish with boot_compress_deinit().

Copyright 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/23/15   ak      Initial version

=============================================================================*/
#include "boot_comdef.h"

#define DC_HEADER_SIZE 5 /* Words */

#define PAGE_SIZE 4096
#define PAGES_PER_CHUNK 16
#define PADDING_BAD_RATIO 256
#define PADDING_SZ_HEADER 1
#define CHUNK_SIZE (PAGE_SIZE * PAGES_PER_CHUNK)

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/


/*===========================================================================

**  Function :  boot_compress_get_header

** ==========================================================================
*/
/*!
* 
* @brief  
*   This routine modifies a 16-byte header that marks the start of a compressed
*   file. This should be called and transmitted before writing any compressed
*   output.
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
#ifdef FEATURE_SD_RAMDUMP_COMPRESSION
	void boot_compress_get_header
	(
		uint32 block_size, 
		uint32 header[DC_HEADER_SIZE]
	);
#else
	static __inline void boot_compress_get_header
	(
		uint32 block_size, 
		uint32 header[DC_HEADER_SIZE]
	)
	{
	
	}
#endif


/*===========================================================================

**  Function :  boot_compress_input

** ==========================================================================
*/
/*!
* 
* @brief  
*   This routine compresses a word-aligned uncompressed buffer of in_len 
*   words into a word-aligned output buffer. This function runs from DDR.
*
* @par Dependencies
*   boot_compress_init needs to be called first.
*
* @retval
*   uint32 size of the compressed output in words
* 
* @par Side Effects
*   None
* 
*/
#ifdef FEATURE_SD_RAMDUMP_COMPRESSION
	uint32 boot_compress_input
	(
		uint32 * uncompressed, 
		uint32 * compressed, uint32 in_len
	);
#else
	static __inline uint32 boot_compress_input
	(
		uint32 * uncompressed, 
		uint32 * compressed, uint32 in_len
	)
	{
		return 0;
	}
#endif //FEATURE_SD_RAMDUMP_COMPRESSION
#endif  /* BOOT_COMPRESS_H */
