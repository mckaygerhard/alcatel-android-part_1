/*===========================================================================

                    BOOT EXTERN LIMITS DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external limits drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_extern_limits_interface.c#1 $
$DateTime: 2015/07/02 04:11:47 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/20/14   dra     Updated late init definition
09/16/14   clm     Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_extern_limits_interface.h"
#include "lmh_limits.h"


/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================

**  Function :  boot_limits_early_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver init in SBL secure execution environment
*
* @return
*  None.
*
* @par Dependencies
*    boot_Tsens_Init must be called once prior to this function
*    TBD
*
*/

void boot_limits_early_init(bl_shared_data_type *bl_shared_data)
{
  limits_early_init(LIMITS_DEFAULT_FLAG);
}

/*===========================================================================

**  Function :  boot_limits_late_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver late init
*
* @return
*  None.
*
* @par Dependencies
*    Function to be called post qsee init
*
*/

void boot_limits_late_init(bl_shared_data_type *bl_shared_data)
{
  limits_late_init();
}

