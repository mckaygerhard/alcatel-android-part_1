#ifndef BOOT_EXTERN_VSENSE_INTERFACE_H
#define BOOT_EXTERN_VSENSE_INTERFACE_H 

/*===========================================================================

                    BOOT EXTERN VSENSE DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external vsense drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2011-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    

$DateTime: 2015/12/02 08:25:58 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/23/14   aks      Adding boot_vsense_init_rpm for SMEM access.
===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/

#include "vsense.h"
#include "boot_sbl_shared.h"

/*===========================================================================

**  Function :  boot_vsense_init

** ==========================================================================
*/
/*!
*
*  This function calibrates the voltage sensors on the rails.
*  It should be invoked from the apps boot loader code.
*
* 
*
* @par Dependencies Shared memory must be ready.
*
* @return  FALSE if initialization failed.
*
* @par Side Effects  None.
*
*/

#ifdef FEATURE_BOOT_EXTERN_VSENSE_COMPLETED
  boolean boot_vsense_init(void);
#else
  static inline boolean boot_vsense_init(void)
  {
    return TRUE;
  }
#endif
/*===========================================================================

**  Function :  boot_vsense_copy_to_smem

** ==========================================================================
*/
/*!
*
*  This function copies the calibrated data for the voltage sensor to 
*  shared memory.
*  It should be invoked from the apps boot loader code.
*
* 
*
* @par Dependencies Shared memory must be ready.
*
* @return  FALSE if initialization failed.
*
* @par Side Effects  None.
*
*/
#ifdef FEATURE_BOOT_EXTERN_VSENSE_COMPLETED
  void boot_vsense_copy_to_smem(bl_shared_data_type *bl_shared_data);
#else
  static inline void boot_vsense_copy_to_smem(bl_shared_data_type *bl_shared_data)
  {
    return ;
  }
#endif

#endif //BOOT_EXTERN_VSENSE_INTERFACE_H

