/*===========================================================================

                    BOOT EXTERN PMIC DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external pmic drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2013-2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_extern_pmic_interface.c#6 $
$DateTime: 2015/12/02 07:52:44 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/02/15   sve     Added boot_pm_pon_wdog_cfg,boot_pm_pon_wdog_enable & boot_pm_pon_wdog_disable API
09/11/15   sc      Add boot_pm_smps_sw_mode API
03/18/15   lm      Added boot_pm_rtc_get_time API
02/03/15   rk      Changed PMIC PS_HOLD config API call for multi-PMIC support.
01/06/15   pxm     Add generic PMIC LED API
05/22/13   yp      Add boot_pm_vib_on and boot_pm_vib_off
05/22/13   aus     Updated RAM dump LED to support multiple targets
04/02/13   dh      Add boot_pm_pon_ps_hold_cfg.
                   Change LED light to blue and brightness to high
03/29/13   plc     Add LED Functions
02/26/13   dh      Add boot_pm_init_smem
04/18/12   dh      change boot_pm_get_power_on_status to boot_pm_dev_get_power_on_reason
09/08/11   dh      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_extern_pmic_interface.h"
#include "boot_target.h"
#include "pm_led.h"
#include "boot_logger.h"
/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 
/*===========================================================================

**  Function :  boot_pm_device_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_device_init(void)
{
  return pm_device_init();
}

/*===========================================================================

**  Function :  boot_pm_dev_get_power_on_reason

** ==========================================================================
*/
/*!
 * @brief  This function returns the phone power-on reason. Typically used in boot
    during early bootup and stored in memory for later access.
 *
 * INPUT PARAMETERS
 * @param pmic_device_index When the target has more than
 *          one pmic, this is the index of the PMIC in which
 *          the power on module is physically located. The device
 *          index starts at zero.
 *@param pwr_on_reason
 *    - pointer to 64-bit unsigned integer that stores the all PON reasons
 *   including PON power on, Warm Reset Reason and POFF_REASON,
 *   SOFT_RESET_REASON.
 *    PON_REASON
 *    PON_WARM_RESET_REASON
 *    PON_POFF_REASON
 *    PON_SOFT_RESET_REASON
 *
 * @return pm_err_flag_type.
 *         PM_ERR_FLAG__PAR1_OUT_OF_RANGE     = Input Parameter one is out of range.
 *         PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this 
 *                                       version of the PMIC.
 *         PM_ERR_FLAG__SBI_OPT_ERR           = The SBI driver failed to communicate
 *                                       with the PMIC.
 *         PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 *@par Dependencies
 *      and pm_init() must have been called.
===========================================================================*/
pm_err_flag_type boot_pm_dev_get_power_on_reason(unsigned pmic_device_index, uint64* pwr_on_reason)
{
  return pm_pon_get_all_pon_reasons(pmic_device_index, pwr_on_reason);
}


/*===========================================================================

**  Function :  boot_pm_driver_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_driver_init(void)
{
  return pm_driver_init();
}


/*===========================================================================

**  Function :  boot_pm_driver_init

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic device initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_sbl_chg_init(void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  err_flag = pm_sbl_chg_init();

  if(PM_ERR_FLAG__CHARGER_MODULE_ABSENT == err_flag)
  {
    // For the case PMI8950 absence, it's by design for some customers using external charger. So return success.
    err_flag = PM_ERR_FLAG__SUCCESS;
    boot_log_message("charger module absent, it's by design if using external charger");
  }

  return err_flag;
}


/*===========================================================================

**  Function :  boot_pm_init_smem

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic smem initialization
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_init_smem(void)
{
  return pm_smem_init();
}


/*===========================================================================

**  Function :  boot_pm_enable_led

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic led functions to turn on default LED
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_enable_led(boolean current_led_status)
{
    pm_led_type led_default = PM_LED_TYPE_INVALID;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    led_default = pm_led_get_default_led();
    do
    {
    if(PM_LED_TYPE_INVALID != led_default)
       {
             // For the cases that LED hw unavailable or NOT specified in pm_config_target.c, we just return 0
            err_flag = pm_led_enable(pm_led_get_default_led(), current_led_status);
            if(err_flag != PM_ERR_FLAG__SUCCESS)
            {
                break;
            }
            err_flag = pm_led_enable(PM_LED_KPD, current_led_status);
        }
    } while(0);

    return err_flag;
}


/*===========================================================================

**  Function :  boot_pm_pon_ps_hold_cfg

** ==========================================================================
*/
/**
 * @brief Configures PMIC to act on MSM PS_HOLD toggle. This
 *        is an app level API which handles all the required
 *        PS_HOLD config for all the applicable PMICs
 *        internally.
 *  
 * @details Configure PMIC to act on MSM PS_HOLD state. 
 * 
 * @param ps_hold_cfg 
 *          PMAPP_PS_HOLD_CFG_WARM_RESET,
 *          PMAPP_PS_HOLD_CFG_HARD_RESET,
 *          PMAPP_PS_HOLD_CFG_NORMAL_SHUTDOWN.
 *   
 *                   
 * @return pm_err_flag_type PM_ERR_FLAG__SUCCESS = SUCCESS else 
 *         ERROR.
 */
pm_err_flag_type boot_pm_pon_ps_hold_cfg
(
  pmapp_ps_hold_cfg_type ps_hold_cfg
)
{
  return pmapp_ps_hold_cfg(ps_hold_cfg);
}

#ifdef FEATURE_BOOT_PON_VIBRATION

/*===========================================================================

**  Function :  boot_pm_vib_on

** ==========================================================================
*/
/*! 
 * @brief This function initializes vibration driver voltage and turn on it
 * @return Error flag.
 *
 */ 

pm_err_flag_type boot_pm_vib_on(void)
{
  pm_err_flag_type ret_val = PM_ERR_FLAG__SUCCESS;
  /* config vibrater driver voltage to 2200 mV. vibration dirver voltage range 1200mv~3100mv*/
  uint16  pm_vib_drv_volt = 2200; 

  ret_val = pm_vib_set_volt(0,PM_VIB__1,pm_vib_drv_volt);
  if (ret_val == PM_ERR_FLAG__SUCCESS)
  {
  	ret_val = pm_vib_enable(0,PM_VIB__1,PM_VIB_MODE__MANUAL,TRUE);
  }
	
  return ret_val;
    
}

/*===========================================================================

**  Function :  boot_pm_vib_off

** ==========================================================================
*/
/*! 
 * @brief This function turn off vibration
 * @return Error flag.
 *
 */ 

pm_err_flag_type boot_pm_vib_off(void)
{
  pm_err_flag_type ret_val = PM_ERR_FLAG__SUCCESS;
  /*Reset state is 0x0000 */
  uint16  pm_vib_drv_volt = 0;

  ret_val =pm_vib_enable(0,PM_VIB__1,PM_VIB_MODE__MANUAL,FALSE);
  if (ret_val == PM_ERR_FLAG__SUCCESS)
  {
  	ret_val = pm_vib_set_volt(0,PM_VIB__1,pm_vib_drv_volt); 
  }
  return ret_val;
}

#endif

#ifndef PMIC_PON_DISABLED
/*===========================================================================

**  Function :  boot_pm_pon_warm_reset_status

** ==========================================================================
*/
/*! 
 * @brief This function provides warm reset status information
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_pon_warm_reset_status
(
  unsigned pmic_device_index, 
  boolean *status
)
{
  return pm_pon_warm_reset_status(pmic_device_index, status);
}


/*===========================================================================

**  Function :  boot_pm_pon_warm_reset_status_clear

** ==========================================================================
*/
/*! 
 * @brief This function clears the warm reset status
 * @return Error flag.
 *
 */ 
pm_err_flag_type boot_pm_pon_warm_reset_status_clear
(
  unsigned pmic_device_index
)
{
  return pm_pon_warm_reset_status_clear(pmic_device_index);
}
#endif

/*===========================================================================

**  Function :  boot_pm_rtc_get_time

** ==========================================================================
*/
/*!
 *@brief  This function returns the current time of the RTC (in secs).
 *@return  Error flag.
 *
 */
pm_err_flag_type boot_pm_rtc_get_time
(
	uint8 pmic_device_index,
	uint32 *time_ptr
)
{
	return pm_rtc_get_time(pmic_device_index,time_ptr);
}
 
/*===========================================================================

**  Function :  boot_pm_smps_sw_mode

** ==========================================================================
*/
/*!
 *@brief  Switch between NPM, LPM, and other modes of a regulator
 *
 *INPUT PARAMETERS
 *@param pmic_chip: Primary: 0. Secondary: 1
 *@param smps_peripheral_index: smps_peripheral_index:
 *                Starts from 0 (for first SMPS peripheral)
 *@param sw_mode - Select the different mode of a regulator. Example: NPM, LPM and AUTO
 *
 *
 *@return pm_err_flag_type.
 *
 */
pm_err_flag_type boot_pm_smps_sw_mode
(
	uint8 pmic_chip,
	uint8 smps_peripheral_index,
	pm_sw_mode_type sw_mode
)
{
	return pm_smps_sw_mode(pmic_chip, smps_peripheral_index, sw_mode);
}

/*===========================================================================

**  Function :  boot_pm_pon_wdog_cfg

** ==========================================================================
*/
/*!
 *@brief  This function will configure the wdog with the given timer value 
 * and with the given reset configuration type.
 *
 *INPUT PARAMETERS
 *@param pmic_device_index
 *@param s1_timer
 *@param s2_timer
 *@param reset_cfg_type
 *
 *
 *@return pm_err_flag_type.
 *
 *
 @par Dependencies
 *api boot_pm_device_init() must have been called before calling this API
 *
 */
pm_err_flag_type boot_pm_pon_wdog_cfg
(
	uint8 pmic_device_index, 
	uint32 s1_timer, 
	uint32 s2_timer, 
	pm_pon_reset_cfg_type reset_cfg_type
)
{
	return pm_pon_wdog_cfg(pmic_device_index,s1_timer, s2_timer, reset_cfg_type);
}

/*===========================================================================

**  Function :  boot_pm_pon_wdog_enable

** ==========================================================================
*/
/*!
 *@brief  This function will enable the wdog with the given pmic_device_index.
 *
 * INPUT PARAMETERS
 *@param pmic_device_index
 *@param enable flag
 *
 *@return pm_err_flag_type
 *
 *
 *@par Dependencies
 * api boot_pm_pon_wdog_cfg() must have been called before calling this API
 *
 */
pm_err_flag_type boot_pm_pon_wdog_enable
(
	uint8 pmic_device_index,
	pm_on_off_type enable
)
{
	return pm_pon_wdog_enable(pmic_device_index,enable);
}
 
/*===========================================================================

**  Function :  boot_pm_pon_wdog_disable

** ==========================================================================
*/
/*!
 *@brief  This function will disable the wdog with the given pmic_device_index.
 *
 *INPUT PARAMETERS
 *@param pmic_device_index
 *@param disable flag
 *
 *@return pm_err_flag_type.
 *
 *@par Dependencies
 * api boot_pm_pon_wdog_cfg() must have been called before calling this API
 *
 */
pm_err_flag_type boot_pm_pon_wdog_disable
(
	uint8 pmic_device_index,
	pm_on_off_type disable
)
{
	return pm_pon_wdog_enable(pmic_device_index,disable);
}
