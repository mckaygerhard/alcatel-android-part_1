/*=============================================================================

                             Boot Authenticator

GENERAL DESCRIPTION
  This module performs SBL binary image authenticating, utilizing the
  functions provided by the PBL fucntion pointers.

Copyright 2014-2016 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_authenticator.c#5 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/11/16   bd      Move TPM Hash's to SMEM
04/11/16   bd      Store hash values only if enough space available, return else
04/11/16   bd      change remaining_block_length to U16
05/19/15   lm      Added boot_auth_image_hashtable() & boot_auth_compute_verify_hash()
10/03/15   aus     Validate cert chain ptr and signature ptr only for 32 bit ELF images
11/26/14   lm      Added error check in boot_store_tpm_hash_block API
11/17/14   sk      Added to verify condition check for integer overflow and underflow.
11/12/14   mohan   Added check code to verify integer overflow
08/21/14   jz      Cleaned up obsoleted code
06/30/14   tj	   Added Additonal Conditional Check for Integer overflow in boot_auth_load_header
06/25/14   tj      Make sure that signature & cert ptr falls in the valid range
				   and should not overlapping.
06/10/14   jz      Check auth_enabled befor calling PBL secboot driver,
                   fixed boot_is_auth_enabled to check return status in the caller
04/29/14   ck      Added SAFE 1.0 logic and fixed secboot_authenticate function pointer assignment
04/29/14   ck      Renamed boot_sbl_auth_image to boot_auth_image as all images are verified
04/01/14   ck      Updated pbl_secboot_verified_info_type to secboot_verified_info_type
                   as Bear PBL uses common secboot lib now.
12/10/13   ck      Replaced return value of boot_is_auth_enabled to void since secboot
                   status is checked inside of function
12/06/13   ck      Removed bl_shared_data_type parameter from boot_is_auth_enabled
08/06/13   aus     Added support for new secboot_verified_info structure
05/07/13   dh      In boot_secboot_ftbl_init, Copy the pbl secboot function 
                   pointers to sbl local function tables individually to avoid
                   mismatch between pbl secboot function table structure and
                   sbl secboot function table structure
04/29/12   kedara  Update boot_auth_load_mba_header to check secure boot status
04/15/12   kedara  Added boot_auth_load_mba_header ,boot_is_msa_enabled_in_hw.
02/13/13   dh      SBL keeps its own copy of secboot function table. Based on 
                   FEATURE_BOOT_EXTERN_SECBOOT_COMPLETED flag we copy over 
                   PBL secboot function table or local secboot lib function table.
12/06/12   dh      Change SHARED_IMEM_TPM_HASH_REGION_BASE to SHARED_IMEM_TPM_HASH_REGION_OFFSET
11/28/12   dh      Add roll back version protection logic 
11/19/12   dh      add boot_store_tpm_hash_block to store tpm hash to shared imem
11/08/12   kedara  Support loading hash segment to seperate buffer.
10/17/12   dh      Check return value of secboot_init
09/26/12   kedara  Added boot_get_sbl_auth_verified_info
09/21/12   kedara  Modified auth function to return a value instead of
                   entering error handler
04/12/12   dh      Rewrote boot_pbl_auth_image to use latest pbl secboot API
12/01/11   dh      ported Rollback prevention version check.
10/27/11   dh      Added boot_auth_init_qd_certs
10/18/11   dh      Added boot_get_ou_field_info
09/01/11   dh      remove boot_pbl_auth_sbl2_image
08/10/11   kpa     Moved api supporting QDST to boot_auth_qd_cert.c
06/28/11   kpa     Added BOOT_MSM_HW_ID define to be used in QDST.
02/11/11   dh      Added a size check for signature_size in boot_pbl_auth_image
01/28/11   dxiang  Define boot_auth_image() utilizing PBL Function Pointers
11/28/10   plc     Added support for Secure-Boot with Qualcomm Development 
                   certificates
10/01/10   plc     Initial revision.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include BOOT_PBL_H
#include "boot_authenticator.h"
#include "boot_sbl_if.h"
#include "boot_util.h"
#include "miheader.h"
#include "boot_logger.h"
#include "boot_target.h"
#include "boot_rollback_version.h"
#include "secboot_hw.h"
#include "secboot.h"
#include "secboot_util.h"
#include "boot_error_if.h"
#include "boot_shared_imem_cookie.h"
#include "boot_extern_secboot_interface.h"
#include "boot_extern_sec_img_interface.h"
#include "boot_extern_smem_interface.h"
//#include "sbl1_mc.h"
#include "boot_smem.h"

/*=========================================================================== 
 
                                    GLOBALS
 
===========================================================================*/


static __align(32)	sec_img_auth_verified_info_s  sbl_auth_verified_info;



/* SBL's local copy of secboot function tables */
static secboot_hw_ftbl_type sbl_secboot_hw_ftbl;
static secboot_ftbl_type sbl_secboot_ftbl;

/*===========================================================================

                              FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

**  Function : boot_secboot_ftbl_init

** ==========================================================================
*/
/*!
* 
* @brief
*    This function initializes SBL's own copy of secboot function tables
*
* @param[in] bl_shared_data Pointer to shared data passed between functions
*
* @par Dependencies
*    None
* 
* @retval
*    bl_error_type Returns error code in case of error
*                  Returns BL_ERR_NONE in case of no error
* 
* 
*/
secboot_hw_etype boot_secboot_ftbl_init(bl_shared_data_type *bl_shared_data)
{
  secboot_hw_etype status = E_SECBOOT_HW_FAILURE;

#ifndef FEATURE_BOOT_EXTERN_SECBOOT_COMPLETED

  secboot_ftbl_type *pbl_secboot_ftbl_ptr = NULL;
  secboot_hw_ftbl_type *pbl_secboot_hw_ftbl_ptr = NULL;
  
  /* If we don't have a local secboot lib compiled in, use pbl's secboot lib */
  if( bl_shared_data != NULL &&
      bl_shared_data->sbl_shared_data != NULL &&
      bl_shared_data->sbl_shared_data->pbl_shared_data != NULL &&
      bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data != NULL)
  {
    pbl_secboot_hw_ftbl_ptr = &bl_shared_data->sbl_shared_data->
                              pbl_shared_data->secboot_shared_data->pbl_secboot_hw_ftbl;
    pbl_secboot_ftbl_ptr = &bl_shared_data->sbl_shared_data->
                              pbl_shared_data->secboot_shared_data->pbl_secboot_ftbl;
                              
    /*Copy the pbl secboot function pointer to sbl local function tables*/                          
    sbl_secboot_hw_ftbl.secboot_hw_is_auth_enabled = pbl_secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled;

    sbl_secboot_ftbl.secboot_init = pbl_secboot_ftbl_ptr->secboot_init;
    sbl_secboot_ftbl.secboot_deinit = pbl_secboot_ftbl_ptr->secboot_deinit;
    sbl_secboot_ftbl.secboot_authenticate = pbl_secboot_ftbl_ptr->secboot_authenticate;
                       
    status = E_SECBOOT_HW_SUCCESS;
  }

#else

  /* If we have a local secboot lib compiled in, use sbl's secboot lib */
  if(E_SECBOOT_SUCCESS == boot_secboot_get_ftbl(&sbl_secboot_ftbl) &&
     E_SECBOOT_HW_SUCCESS == boot_secboot_hw_get_ftbl(&sbl_secboot_hw_ftbl))
  {
    status = E_SECBOOT_HW_SUCCESS;
  }
  
#endif

  return status;
}

/*===========================================================================

**  Function : boot_is_auth_enabled

** ==========================================================================
*/
/*!
* 
* @brief
*    This function determines if secboot is enabled 
*
* @param[out] is_auth_enabled Pointer to a boolean which will be set 
*                             to true if secboot is enabled, false if not
*
* @par Dependencies
*    None
* 
* @retval
*    bl_error_boot_type Returns error code in case of error
*                  Returns BL_ERR_NONE in case of no error
* 
* 
*/
secboot_hw_etype boot_is_auth_enabled(boot_boolean *is_auth_enabled)
{
  secboot_hw_etype sec_hw_err = E_SECBOOT_HW_SUCCESS;
  secboot_hw_ftbl_type *sbl_secboot_hw_ftbl_ptr = &sbl_secboot_hw_ftbl;


  if(sbl_secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled != NULL)
  {    
    sec_hw_err = sbl_secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled(SECBOOT_HW_APPS_CODE_SEGMENT,
                                                                     (uint32 *)is_auth_enabled);
  }
   	return sec_hw_err;
}


/*===========================================================================
**  Function : boot_get_sbl_auth_verified_info
** ==========================================================================
*/
/*!
* 
* @brief
*   This function returns the address of the sbl_auth_verified_info data 
*   structure. This field is populated during authentication routines, and 
*   may be required by other modules. The secboot_verified_info_type corresponds
*   to the last authenticated image.
*
* @param[in] bl_shared_data - Pointer to shared data passed between functions
* 
* @par Dependencies
*   sbl_auth_verified_info is populated post authentication
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
secboot_verified_info_type * boot_get_sbl_auth_verified_info
(
  bl_shared_data_type *bl_shared_data
)
{
  return &sbl_auth_verified_info.v_info;
}


/*===========================================================================
**  Function :  boot_store_tpm_hash_block
** ==========================================================================
*/
/*!
* 
* @brief
*   Store hash of loaded image into shared imem location if authentication is enabled
* 
* @param[in] bl_shared_data - Pointer to shared data passed between functions
*
* @param[in] sbl1_auth_verified_info image hash info
*
* @par Dependencies
*   None
*   
* @retval
*   Updated pointer value if ok.
*   NULL if error.
* 
* @par Side Effects
*   None
* 
*/
void boot_store_tpm_hash_block
(
  bl_shared_data_type *bl_shared_data, 
  secboot_verified_info_type *sbl1_auth_verified_info
)
{
#if (defined(SHARED_IMEM_TPM_HASH_REGION_SIZE) && defined(SHARED_IMEM_TPM_HASH_REGION_OFFSET))

  /* 
     Store the hash of the image last authenticated on to shared imem.
     Hash block pointer is updated to next available
     free location. Along with the hash, hash block also contains other info
     about image such as image address.
  */
   
  /* First uint32 represents how many blocks there are */
  uint32 tpm_hash_imem_base = SHARED_IMEM_TPM_HASH_REGION_OFFSET + (uint32)boot_shared_imem_cookie_ptr;
  uint32 *num_of_hashes = NULL;
  uint16 remaining_block_length = 0;
  uint32 *tpm_hash_block_ptr = bl_shared_data->
                               sbl_shared_data->
                               next_avail_tpm_hash_blk_ptr;
  boot_boolean is_auth_enabled = FALSE;
  uint32 tpm_hash_region_size = 0;
  static boot_boolean move_hash_post_smem_init = FALSE;
  boot_boolean smem_init_done = boot_smem_init_get_status();
  uint32 tpm_hash_base = 0;	
  
  /* First determine if secboot is enabled */
  boot_is_auth_enabled(&is_auth_enabled);
   
  if(is_auth_enabled == TRUE)
  {
    /* Validate hash pointer and available space */
    BL_VERIFY(( tpm_hash_block_ptr != NULL ), BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT );	
      
    if(smem_init_done == FALSE)
    {
      tpm_hash_base = tpm_hash_imem_base;
      tpm_hash_region_size = SHARED_IMEM_TPM_HASH_REGION_SIZE;
    }
    else if((smem_init_done == TRUE) && (move_hash_post_smem_init == FALSE)) 		
    {
      /* first call post SMEM init 
         If smem_init_done is TRUE on very first call to this function,
         then tpm_hash_block_ptr - tpm_hash_imem_base = 4(number of hashes) */       
      tpm_hash_region_size = SHARED_SMEM_TPM_HASH_REGION_SIZE;
      tpm_hash_base = (uint32)smem_alloc( SMEM_XBL_LOADER_CORE_INFO, tpm_hash_region_size);
      BL_VERIFY(( tpm_hash_base > 0 ), BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT );			

      qmemcpy((void *)tpm_hash_base, (void *)tpm_hash_imem_base, 
          (uint32)tpm_hash_block_ptr - tpm_hash_imem_base);	
          
      tpm_hash_block_ptr = (uint32 *)(tpm_hash_base 
              + ((uint32)tpm_hash_block_ptr - tpm_hash_imem_base));

      move_hash_post_smem_init = TRUE;
    }
    else /*  post SMEM init */
    {
      tpm_hash_region_size = SHARED_SMEM_TPM_HASH_REGION_SIZE;
      tpm_hash_base = (uint32)smem_alloc( SMEM_XBL_LOADER_CORE_INFO, tpm_hash_region_size);
      BL_VERIFY(( tpm_hash_base > 0 ), BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT );
    }

    num_of_hashes = (uint32 *)tpm_hash_base;  

    BL_VERIFY(( tpm_hash_base < (uint32)tpm_hash_block_ptr ),
                BL_ERR_TPM_HASH_BLK_STR_FAIL|BL_ERROR_GROUP_BOOT );

    /* Verify there is enough space remain to store one hash block */
    remaining_block_length = (tpm_hash_base + tpm_hash_region_size) - 
                            (uint32)tpm_hash_block_ptr;	

    BL_VERIFY(remaining_block_length >= sizeof(secboot_image_hash_info_type),
              BL_ERR_TPM_HASH_BLK_STR_FAIL|BL_ERROR_GROUP_BOOT);	

    /* Store hash to shared imem */
    qmemcpy(tpm_hash_block_ptr, 
            &sbl1_auth_verified_info->image_hash_info, 
            sizeof(secboot_image_hash_info_type));

    /* Update pointer to point to next available location */
    bl_shared_data->sbl_shared_data->next_avail_tpm_hash_blk_ptr = 
              (uint32 *)((uint32)tpm_hash_block_ptr + sizeof(secboot_image_hash_info_type));
    *num_of_hashes += 1;
  }

#endif  
  
}/*  boot_store_tpm_hash_block */


/*===========================================================================

**  Function : boot_is_msa_enabled_in_hw

** ==========================================================================
*/
/*!
* 
*  @brief
*    This function Checks if MSA [/subsystem self authentication (SSA)]
*    feature is enabled in hardware.
*
*  @param[in] 
*    None
*
*  @par Dependencies
*    Should be called only after boot_secboot_ftbl_init
* 
*  @retval
*    TRUE if MSA/SSA Feature is enabled in hw / qfprom fuses else
*    FALSE
* 
*  @par Side Effects
*    None
* 
*/
boot_boolean boot_is_msa_enabled_in_hw ( void )
{
  boot_boolean is_auth_enabled = FALSE;

  secboot_hw_etype sec_hw_err = E_SECBOOT_HW_SUCCESS;
  
  secboot_hw_ftbl_type *sbl_secboot_hw_ftbl_ptr = &sbl_secboot_hw_ftbl;
  
  BL_VERIFY((sbl_secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled != NULL),
             BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT );

  sec_hw_err = sbl_secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled(SECBOOT_HW_MBA_CODE_SEGMENT,
                                                                 (uint32 *)&is_auth_enabled);
  BL_VERIFY((E_SECBOOT_HW_SUCCESS == sec_hw_err),
             (uint16)sec_hw_err|BL_ERROR_GROUP_SECURITY );

  return is_auth_enabled;
}

/*===========================================================================
**  Function :  boot_auth_image_hashtable
** ==========================================================================
*/
/*!
* 
* @brief
*   This function authenticates the image metadata (hashtable)
*
* @par Dependencies
*   None 
* 
* @retval
*   SEC_IMG_AUTH_SUCCESS - If ELF images is verified successfully
*   ERROR VALUES - In case of failure.
* 
*/
sec_img_auth_error_type boot_auth_image_hashtable (uint32 image_id,
              sec_img_auth_elf_info_type *sbl_elf_info, 
              sec_img_auth_whitelist_area_param_t *sbl_white_list_param)
{
  sec_img_auth_error_type sec_img_auth_status = SEC_IMG_AUTH_FAILURE;
	
  sec_img_auth_status = boot_sec_img_auth_verify_metadata(image_id,
                                                     sbl_elf_info,
                                                     sbl_white_list_param,
                                                     &sbl_auth_verified_info);

  return sec_img_auth_status;
}
	
	/*===========================================================================
**  Function :  boot_auth_compute_verify_hash
** ==========================================================================
*/
/*!
* 
* @brief
*   This function loads the data segment into memory using the information
*   from a specific program header and input destination address.  
*
* @param[in] image_id -  image identification number 
*
* @par Dependencies
*   None 
* 
* @retval
*   boolean status - SEC_IMG_AUTH_SUCESS if authentication was successful
*                    EROR VALUES - In case of failure.
* 
*/
uint32 boot_auth_compute_verify_hash( uint32 image_id )
{
  sec_img_auth_error_type sec_img_auth_status = SEC_IMG_AUTH_FAILURE;
  uint32 cert_img_version;
  
  sec_img_auth_status = boot_sec_img_auth_hash_elf_segments(image_id, &sbl_auth_verified_info);

  if( sec_img_auth_status == SEC_IMG_AUTH_SUCCESS )
  {
   cert_img_version = sbl_auth_verified_info.v_info.sw_id >> 32;
   BL_VERIFY((boot_rollback_update_img_version((secboot_sw_type)image_id, cert_img_version) == BL_ERR_NONE),
             BL_ERR_CODE_ROLLBACK_VERSION_VERIFY_FAIL|BL_ERROR_GROUP_BOOT);	
  }
  return sec_img_auth_status;
}
