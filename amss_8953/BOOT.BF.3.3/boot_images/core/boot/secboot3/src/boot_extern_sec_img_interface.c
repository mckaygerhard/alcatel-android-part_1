/*===========================================================================

                    BOOT EXTERN SECURE IMAGE AUTH DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external image authentication drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2014-2015 by Qualcomm Technologies, Incorporated.  
All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_extern_sec_img_interface.c#3 $
$DateTime: 2015/11/02 06:39:49 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/19/15   lm      Ported from 8994
01/28/15   plc     Fixing KW error by passing securemsm version of 
                   sbl_verified_info* to sec_img_auth_init()
12/23/14   plc     Added logic to track authentication time in boot log stats
11/13/14   wg      Added support for Debug Policy Image loading
09/12/14   plc     Add support for engineering certificates
05/05/14   wg      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/

#include "boot_extern_sec_img_interface.h"
#include "boot_statistics.h"
#include "boot_util.h"
#ifdef DEBUG_POLICY_V2
#include "secboot_debug_policy_v2.h"
#else
#include "secboot_debug_policy.h"
#endif

/*===========================================================================
                           DEFINITIONS
===========================================================================*/


/*===========================================================================
                      FUNCTION DEFINITIONS
===========================================================================*/ 

/**
 * @brief This function authenticates image Metadata
 *      
 *
 * @param[in,out] sec_img_id           	SW Image ID
 *                sec_img_data          Pointer to ELF header         
 *                                     
 * 				  
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_verify_metadata(uint32 sec_img_id, 
							 const sec_img_auth_elf_info_type *sec_img_data,
							 sec_img_auth_whitelist_area_param_t *whitelist_area,
							 sec_img_auth_verified_info_s *v_info)
{
  sec_img_auth_error_type retval = SEC_IMG_AUTH_FAILURE;
  
  /* Start the authentication timer */
  boot_statistics_auth_read_start();

  retval = sec_img_auth_verify_metadata(sec_img_id, 
							 sec_img_data,
							 whitelist_area,
							 v_info);
							
  /* Stop the authentication timer */							
  boot_statistics_auth_read_stop();
  
  return retval;
}


/**
 * @brief This function authenticates an ELF images hash segments
 *      
 *
 * @param[in,out] sec_img_id              SW Image ID
 *                v_info                  Pointer to location for verifed info
 *                                   
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_hash_elf_segments(uint32 sec_img_id,
								  sec_img_auth_verified_info_s *v_info)
{
  sec_img_auth_error_type retval = SEC_IMG_AUTH_FAILURE;
  
  /* Start the authentication timer */
  boot_statistics_auth_read_start();  

  retval = sec_img_auth_hash_elf_segments(sec_img_id, v_info);
  
  /* Stop the authentication timer */							
  boot_statistics_auth_read_stop();

  return retval;
}								  
								
								  
/**
 * @brief This function validates the ELF image
 *      
 *
 * @param[in,out] elf_hdr               Pointer to ELF header
 *                                    
 *                        
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_validate_elf(const void *elf_hdr)
{
  return sec_img_auth_validate_elf(elf_hdr);
}


/**
 * @brief This function checks if the image segment is valid
 *
 *
 * @param[in,out] format              File format
 *                entry               Pointer to hash segment to be checked
 *                     
 *
 * @return \c TRUE if the segment is valid, \c FALSE otherwise.
 *
 * @sideeffects  None
 *
 *
 */
boolean boot_sec_img_auth_is_valid_segment(uint32 format, const void *entry)
{
  return sec_img_auth_is_valid_segment(format, entry);
}


/**
 * @brief This function checks whether authentication is enabled
 *      
 *
 * @param[in,out] code_seg            Type of code segment descriptor
 *                *is_auth_enabled    Pointer to store auth_en flag. 
 *                                                           
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
sec_img_auth_error_type boot_sec_img_auth_is_auth_enabled(uint32 code_seg, 
																									uint32 *is_auth_enabled)
{
  return sec_img_auth_is_auth_enabled(code_seg, is_auth_enabled);
}

/**
 * @brief send pbl debug information to sec img auth library
 *
 * @param[in] bl_shared_data - pbl shared info
 */
void boot_sec_img_auth_init(bl_shared_data_type * bl_shared_data)
{
  sec_img_auth_error_type status = SEC_IMG_AUTH_INVALID_ARG;
  secboot_verified_info_type sbl_verified_info;

  /* get the enable_debug flag of sbl cert */
  if( (NULL != bl_shared_data)
     && (NULL != bl_shared_data->sbl_shared_data) 
     && (NULL != bl_shared_data->sbl_shared_data->pbl_shared_data)
     && (NULL != bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data))
  {
    /* Since pbl is passing secboot_verified_info_type pbl_verified_info, but secboot wants secboot_verified_info, 
       we manually copy entries into secboot_verified_info type to avoid KW errors.  To be safe we set 
       the extra "enable_crash_dump" and "version_id" fields as well. */
    qmemset(&sbl_verified_info.version_id,
          0x0, sizeof(sbl_verified_info.version_id));

    /* Copy fields from PBL-provided verified info into securemsm format verified info */
    qmemscpy(&sbl_verified_info.sw_id, 
            sizeof(sbl_verified_info.sw_id), 
		    &bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.sw_id, 
            sizeof(bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.sw_id));
    qmemscpy(&sbl_verified_info.msm_hw_id, 
            sizeof(sbl_verified_info.msm_hw_id), 
		    &bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.msm_hw_id,
            sizeof(bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.msm_hw_id));
    qmemscpy(&sbl_verified_info.enable_debug, 
            sizeof(sbl_verified_info.enable_debug),
		    &bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.enable_debug,
            sizeof(bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.enable_debug));
    qmemscpy(&sbl_verified_info.image_hash_info, 
            sizeof(sbl_verified_info.image_hash_info),
		    &bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.image_hash_info,
            sizeof(bl_shared_data->sbl_shared_data->pbl_shared_data->secboot_shared_data->pbl_verified_info.image_hash_info));

    qmemset(&sbl_verified_info.enable_crash_dump,
            0x0, sizeof(sbl_verified_info.enable_crash_dump));
  
    status = sec_img_auth_init(&sbl_verified_info);
    BL_VERIFY((SEC_IMG_AUTH_SUCCESS == status),(uint16)status|BL_ERROR_GROUP_SECURITY);
  }
}

/**
 * @brief This function checks whether Debug Policy Override Flag is set
 *      
 *
 * @param[in,out] *dpo_buffer         Debug Policy Override Flag Buffer               
 *                                                           
 *
 * @return SEC_IMG_AUTH_SUCCESS on success. Appropriate error code on failure.
 *
 * @sideeffects  None
 *
 *
 */
boolean boot_sec_img_auth_is_dp_disable(uint8 *dpo_flag)
{
  return sec_is_dp_disable(dpo_flag);
}


/**
 * @brief This function returns the sizeof Debug Policy Override structure
 *      
 *
 * @param[in,out] 
 *                                                                                          
 *
 * @return sizeof(dbg_policy_override).if defined, otherwise TZBSP_DPO_BUFFER_SIZE 
 *
 * @sideeffects  None
 *
 *
 */
uint32 boot_sec_img_auth_get_dpo_size(void)
{
  return sizeof(dbg_policy_override);
}
