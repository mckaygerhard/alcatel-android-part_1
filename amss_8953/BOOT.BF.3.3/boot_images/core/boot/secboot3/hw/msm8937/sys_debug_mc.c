/*=============================================================================
                        System Debug Main Control Function

GENERAL DESCRIPTION     
   This module defines objects for the main control loop 
   of the system debug image.
  
	Copyright 2014-2016 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who   what, where, why
--------   ---   --------------------------------------------------
06/15/16   sc    Force SDI pass#1 execution for reset_status=0x0
01/20/16   sk    Enable secure WDG to recover from lockup in SDI path 
01/04/16   lm    Set debugui_ctl register after dumping debugUI in SDI
11/16/15   sk    Updating necessary clocks in SDI path
09/29/15   sk    Enable necessary clock in SDI path
11/06/14   sk    Added changes for 8952 SDI image
07/22/14   lm    Aligned with V1.1 cpu dump table format
07/14/14   lm    Added changes to support for 8936
06/13/14   lm    Added a logic to find the cpu number instead of hard coding
				 the cpu number
06/07/14   lm    Enable QDSS clock by writing GCC reg
05/29/14   lm    Changed the core number due to performance cluster for 8936
05/23/14   ck    Added additional QDSS clocks and initialization
05/21/14   lm    Added flatfile change updates
03/13/14   ck    Added Debug UI clocks to sys_debug_enable_clocks
03/06/14   ck    Added sys_debug_enable_clocks function
02/26/14   ck    Added QDSS unlock logic
02/18/14   ck    Added call to boot_ddr_restore_from_wdog_reset
02/18/14   ck    Removed duplicate copy of 32 to 64 bit switch functions.
                 Single copy is stored in SDI code space and shared.
02/06/14   ck    Added sys_debug_switch_to_aarch64
01/02/14   ck    Initial release for Bear family 

===========================================================================*/
#include "sys_debug.h"
#include "boot_msm.h"
#include "msmhwioreg.h"
#include "boot_elf_header.h"
#include "boothw_target.h"
#include "boot_extern_ddr_interface.h"
#include "boot_target.h"


/* SYSTEM_DEBUG_QSEE_INFO is memory specified in the scatter file for SBL
   and system debug to share information about where QSEE was loaded and
   if it's a 32 bit or 64 bit image. */
extern uint32 Image$$SYSTEM_DEBUG_QSEE_INFO$$Base;
extern uint32 get_cur_cluster_num(void);

/*Reset status IMEM location*/
#define  SYSTEM_DEBUG_GCC_RESET_STATUS_IMEM_LOC   0x8600764
#define  SYSTEM_DEBUG_MARKER_ADDR                 0x8600B14
#define  SYSTEM_DEBUG_SBL_ENTRY_MAGIC             0x5B15D1EE 
#define  SYSTEM_DEBUG_SBL_DDR_OSR_START_MAGIC     0x5B15D1AA
#define  SYSTEM_DEBUG_SBL_DDR_OSR_END_MAGIC       0x5B15D1DD
#define  SYSTEM_DEBUG_SBL_INVALID_QSEE_ENTRY      0x5B15D1BB
#define  SYSTEM_DEBUG_SBL_JUMP_TO_TZ              0x5B15D1CC

#define SYSTEM_DEBUG_FORCE_RESET_STATUS_VALUE     0x3E

#define  SYSTEM_DEBUG_WDT_DELAY_MS      30000 //30secs
#define  SYSTEM_DEBUG_TIMER_MIN(a,b)    ((a)>(b)?(b):(a))
#define  SYSTEM_DEBUG_TIMER_MAX(a,b)    ((a)>(b)?(a):(b))

#define SYSTEM_DEBUG_MARKER_SET(val) 				 \
	do{										         \
		*((uint32*)(SYSTEM_DEBUG_MARKER_ADDR)) = (val);	 \
	}while(0)

static uint64 ms_to_ticks_sec_wdt(uint64 ms)
{
  /* Secure watchdog runs as 32KHz. */
  return (32768ULL * ms) >> 10;
}

/*===========================================================================
**  Function :  sys_debug_enable_clocks
** ==========================================================================
*/
/*!
* 
* @brief
*   During abnormal reset some clocks are turned off that need to be reenabled
*   for proper operation.  Since full clock driver cannot be placed into
*   RPM Code RAM this function will enable the minimum set of clocks needed.
*
* @param[in] 
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
* 
*/
void sys_debug_enable_clocks(void)
{

  /* Needed by Debug UI */
  HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);

  HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);

  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, QDSS_DAP_CLK_ENA, 0x1);


  /* Needed by DDR */
  HWIO_GCC_PCNOC_DDR_CFG_CBCR_OUT(HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_OUT(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_EN_BMSK);

  HWIO_GCC_BIMC_XO_CBCR_OUT(HWIO_GCC_BIMC_XO_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_BIMC_CFG_AHB_CBCR_OUT(HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK |
                                 HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_DDR_DIM_CFG_CBCR_OUT(HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_BIMC_SLEEP_CBCR_OUT(HWIO_GCC_BIMC_SLEEP_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_DDR_DIM_SLEEP_CBCR_OUT(HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_ENABLE_BMSK);

  HWIO_GCC_DDR_CMD_RCGR_OUT(HWIO_GCC_DDR_CMD_RCGR_UPDATE_BMSK);

  HWIO_GCC_BIMC_DDR_CPLL0_CBCR_OUT(HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK);  
  HWIO_GCC_BIMC_DDR_CPLL1_CBCR_OUT(HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK);
  HWIO_GCC_BIMC_DDR_CH0_CBCR_OUT(HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK);
}

/*===========================================================================
**  Function :  sys_debug_wdt_start
** ==========================================================================
*/
/*!
* 
* @brief
*   SDI secure WDOG start
*
* @param[in] 
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
* 
*/
void sys_debug_wdt_start()
{
  uint32 bark_timeout;
  uint32 bite_timeout = SYSTEM_DEBUG_WDT_DELAY_MS;
 
 
  bite_timeout = ms_to_ticks_sec_wdt(bite_timeout);
  bark_timeout = bite_timeout + 10;

  /* Timeouts have a ceiling value */
  bark_timeout = SYSTEM_DEBUG_TIMER_MIN(bark_timeout,
                                        HWIO_MPM2_WDOG_BARK_VAL_REG_WDOG_BARK_VAL_BMSK);
  bite_timeout = SYSTEM_DEBUG_TIMER_MIN(bite_timeout, 
                                        HWIO_MPM2_WDOG_BITE_VAL_REG_WDOG_BITE_VAL_BMSK);
  /* Make sure the watchdog is disabled */
  HWIO_OUTF(MPM2_WDOG_CTL_REG, WDOG_EN, 0);

  /* Program bark and bite timeouts. */
  HWIO_OUTF(MPM2_WDOG_BARK_VAL_REG, WDOG_BARK_VAL, bark_timeout);
  while(HWIO_INF(MPM2_WDOG_BARK_VAL_REG, SYNC_STATUS) == 0);

  HWIO_OUTF(MPM2_WDOG_BITE_VAL_REG, WDOG_BITE_VAL, bite_timeout);
  while(HWIO_INF(MPM2_WDOG_BITE_VAL_REG, SYNC_STATUS) == 0);

  HWIO_OUTF(MPM2_WDOG_CTL_REG, WDOG_CLK_EN, 1);

  HWIO_OUTF(MPM2_WDOG_CTL_REG, WDOG_EN, 1);

  /* Reset to make sure the count starts at 0. */
  HWIO_OUT(MPM2_WDOG_RESET_REG, 1);
}

/*===========================================================================
**  Function :  sys_debug_disable
** ==========================================================================
*/
/*!
* 
* @brief
*   Disables SDI functionality
*
* @param[in] 
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
* 
*/
void sys_debug_disable()
{
	HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x0);
	HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x0);
	HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x0);
	HWIO_OUTF(GCC_RESET_DEBUG, RESET_ACCESS_FIRST_PASS, 0x0);
	HWIO_OUTF(GCC_RESET_DEBUG, RESET_DEBUG_FIRST_PASS, 0x0);
	MSM_CLEAR_BIT(TCSR_RESET_DEBUG_SW_ENTRY, 0x0);
	HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x0);
	MSM_CLEAR_BIT(GCC_RESET_STATUS,0x0);
}

/*===========================================================================
**  Function :  sys_debug_main_ctl
** ==========================================================================
*/
/*!
* 
* @brief
* 
* @param[in] 
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
* 
*/
void sys_debug_main_ctl(sys_debug_cpu_ctxt_regs_type * ctxt_regs)
{
   
  sys_debug_qsee_info_type * qsee_info =
    (sys_debug_qsee_info_type *)&Image$$SYSTEM_DEBUG_QSEE_INFO$$Base;
	
  uint32 reset_status = 0xFFFF;
  
  /*Update SDI SBL entry debug marker*/
  SYSTEM_DEBUG_MARKER_SET(SYSTEM_DEBUG_SBL_ENTRY_MAGIC);
  
    /*Read the reset_status value*/
	reset_status = HWIO_IN(GCC_RESET_STATUS);
	/*Save reset status value to SDI IMEM location*/
  *((uint32*)(SYSTEM_DEBUG_GCC_RESET_STATUS_IMEM_LOC)) = reset_status;
  
  /* Disable SDI and Enable WDOG to recover from any hangs in Warm reset path */
  sys_debug_disable();
#if 0
  sys_debug_wdt_start();
#endif
  /* Enable clocks that were disabled during abnormal reset
     that are needed.  This must be done first. */
  sys_debug_enable_clocks();


  /* Unlock and initialize QDSS Coresight component so the PC and Reset Mode
     can be accessed.  No reason to verify if unlock was successful or not as
     code must continue. */
  HWIO_QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS_OUT(DEBUG_UI_UNLOCK_CODE);


  HWIO_QDSS_WRAPPER_DEBUG_UI_CTL_MASK_OUT(0);


  HWIO_APCS_BANKED_GCC_DBG_CLK_ON_REQ_OUT(1);
  
  
  /* Save Reset PC */
  /* For 8936 quadcore support,only cluster and cpu numbers have changed but 
     the register's remain same */
  ctxt_regs->pc = HWIO_QDSS_WRAPPER_DEBUG_UI_DATA_n_INI(4);
 
  SYSTEM_DEBUG_MARKER_SET(SYSTEM_DEBUG_SBL_DDR_OSR_START_MAGIC);
  /* Bring DDR out of self refresh */
  ddr_restore_from_wdog_reset(DDR_XO_SPEED_IN_KHZ); 
  
  /*Update DDR exit self-refresh end debug marker*/
  SYSTEM_DEBUG_MARKER_SET(SYSTEM_DEBUG_SBL_DDR_OSR_END_MAGIC);
  
  /* At TZ monitor entry reset_status register value decides the SDI or Normal warm/cold boot path. 
    If reset status value is non-zero, then enters SDI path, otherwise normal boot.
    So,if reset status=0x0, then force TZ SDI pass#1 execution by updating the GCC_RESET_STATUS=0x3E(all status bits set).
    And in TZ sdi save the rest status as 0x0 in IMEM location,instead 0x3E.
	*/
  if (reset_status == 0x0)
  {
    /*Disable watchdog debug before updating reset_status register, otherwise we see 
	  unknown behavior due to status register write*/
	sys_debug_disable();
   
	HWIO_OUT(GCC_RESET_STATUS, SYSTEM_DEBUG_FORCE_RESET_STATUS_VALUE);
  }
   
   /*Ensure QSEE entry and ELF class are correct*/
   if ( ((uint32)qsee_info->entry_point == NULL) ||
        !((qsee_info->elf_class == ELF_CLASS_64) || 
		  (qsee_info->elf_class == ELF_CLASS_32))||
		!( qsee_info->entry_point <= 0xFFFFFFFF ) 
	  )
	{
		 /*Update the SDI debug marker*/
		SYSTEM_DEBUG_MARKER_SET(SYSTEM_DEBUG_SBL_INVALID_QSEE_ENTRY);
		
		/*Pass#1 can't be continued. Loop here- WD would be triggered soon*/
		while(1);
	}
	
	/*Try access TZ entry address before switch to TZ in 64bit mode.
	  This confirms exit self refresh done and TZ code is accessible.
	  TODO: Add vector table in SBL and dump DDR registers in SDI RPM code ram in exception handler.*/
	if ( *((uint32 *)((uint32)qsee_info->entry_point )))
	{
	    /*Update SDI debug marker*/
		SYSTEM_DEBUG_MARKER_SET(SYSTEM_DEBUG_SBL_JUMP_TO_TZ);
	}
	
  /* Jump to QSEE passing the address of the internal dump structure.
     The entry point and elf class have already been validated during cold
     boot.  Even if they were bad there is nowhere to go from here.
     Entry point must be 32 bits as the boot remapper is used.  Again this
     was already validated during cold boot.*/
  if (qsee_info->elf_class == ELF_CLASS_64)
  {
    boot_switch_to_aarch64((uint32)qsee_info->entry_point,
                           (uint32)ctxt_regs);
  }
  else
  {
    ((void (*)())(uint32)(qsee_info->entry_point))(ctxt_regs); 
  }

} /* sys_debug_main_ctl() */


void sys_debug_undefined_c_handler(void)
{
}


void sys_debug_swi_c_handler(void)
{
}


void sys_debug_prefetch_abort_c_handler(void)
{
}


void sys_debug_data_abort_c_handler(void)
{
}


void sys_debug_reserved_c_handler(void)
{
}


void sys_debug_irq_c_handler(void)
{
}


void sys_debug_fiq_c_handler(void)
{
}
