


ARM Macro Assembler    Page 1 


    1 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
    2 00000000         ;
    3 00000000         ;                              SBL1
    4 00000000         ;
    5 00000000         ; GENERAL DESCRIPTION
    6 00000000         ;   This file bootstraps the processor. The Start-up Pri
                       mary Bootloader
    7 00000000         ;   (SBL1) performs the following functions:
    8 00000000         ;
    9 00000000         ;      - Set up the hardware to continue boot process.
   10 00000000         ;      - Initialize DDR memory
   11 00000000         ;      - Load Trust-Zone OS
   12 00000000         ;      - Load RPM firmware
   13 00000000         ;      - Load APPSBL and continue boot process
   14 00000000         ;
   15 00000000         ;   The SBL1 is written to perform the above functions w
                       ith optimal speed.
   16 00000000         ;   It also attempts to minimize the execution time and 
                       hence reduce boot time.
   17 00000000         ;
   18 00000000         ; Copyright 2014-2015 by Qualcomm Technologies, Incorpor
                       ated.All Rights Reserved.
   19 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   20 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   21 00000000         ;
   22 00000000         ;                           EDIT HISTORY FOR FILE
   23 00000000         ;
   24 00000000         ; This section contains comments describing changes made
                        to the module.
   25 00000000         ; Notice that changes are listed in reverse chronologica
                       l order.
   26 00000000         ;
   27 00000000         ; $Header: 
   28 00000000         ;
   29 00000000         ; when       who     what, where, why
   30 00000000         ; --------   ---     -----------------------------------
                       ---------------------
   31 00000000         ; 02/27/15   aus     Fixed to check for correct abort mo
                       de
   32 00000000         ; 07/14/14   lm      Added sbl1_external_abort_enable fu
                       ntion
   33 00000000         ; 07/01/14   sk      Added sbl_save_regs function
   34 00000000         ; 05/01/14   ck      Added logic to assign stacks based 
                       on processor mode
   35 00000000         ; 03/19/14   ck      Fixed stack base issue.  Now using 
                       proper address which is "Limit" of SBL1_STACK 
   36 00000000         ; 03/07/14   ck      Removed -4 logic from check_for_nes
                       ting as bear SBL has its own vector table
   37 00000000         ; 03/03/14   ck      Updated vector table with branches 
                       as VBAR is being used in Bear family 
   38 00000000         ; 11/15/12   dh      Add boot_read_l2esr for 8974
   39 00000000         ; 08/31/12   dh      Correct the stack base in check_for
                       _nesting, remove unused code
   40 00000000         ; 07/16/12   dh      Remove watchdog reset code
   41 00000000         ; 02/06/12   dh      Update start up code
   42 00000000         ; 01/31/12   dh      Initial revision for 8974



ARM Macro Assembler    Page 2 


   43 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   44 00000000         ;=======================================================
                       =====================
   45 00000000         ;
   46 00000000         ;                            MODULE INCLUDES
   47 00000000         ;
   48 00000000         ;=======================================================
                       =====================
   49 00000000         ;=======================================================
                       =====================
   50 00000000         ;
   51 00000000         ;                             MODULE DEFINES
   52 00000000         ;
   53 00000000         ;=======================================================
                       =====================
   54 00000000         ;
   55 00000000 00000013 
                       Mode_SVC
                               EQU              0x13
   56 00000000 00000017 
                       Mode_ABT
                               EQU              0x17
   57 00000000 0000001B 
                       Mode_UND
                               EQU              0x1b
   58 00000000 00000010 
                       Mode_USR
                               EQU              0x10
   59 00000000 00000011 
                       Mode_FIQ
                               EQU              0x11
   60 00000000 00000012 
                       Mode_IRQ
                               EQU              0x12
   61 00000000 0000001F 
                       Mode_SYS
                               EQU              0x1F
   62 00000000 00000080 
                       I_Bit   EQU              0x80
   63 00000000 00000040 
                       F_Bit   EQU              0x40
   64 00000000 00000100 
                       A_Bit   EQU              0x100
   65 00000000         ;=======================================================
                       =====================
   66 00000000         ;
   67 00000000         ;                             MODULE IMPORTS
   68 00000000         ;
   69 00000000         ;=======================================================
                       =====================
   70 00000000         ; Import the external symbols that are referenced in thi
                       s module.
   71 00000000                 IMPORT           |Image$$SBL1_SVC_STACK$$ZI$$Lim
it|
   72 00000000                 IMPORT           |Image$$SBL1_UND_STACK$$ZI$$Lim
it|
   73 00000000                 IMPORT           |Image$$SBL1_ABT_STACK$$ZI$$Lim
it|



ARM Macro Assembler    Page 3 


   74 00000000                 IMPORT           boot_undefined_instruction_c_ha
ndler
   75 00000000                 IMPORT           boot_swi_c_handler
   76 00000000                 IMPORT           boot_prefetch_abort_c_handler
   77 00000000                 IMPORT           boot_data_abort_c_handler
   78 00000000                 IMPORT           boot_reserved_c_handler
   79 00000000                 IMPORT           boot_irq_c_handler
   80 00000000                 IMPORT           boot_fiq_c_handler
   81 00000000                 IMPORT           boot_nested_exception_c_handler
   82 00000000                 IMPORT           sbl1_main_ctl
   83 00000000                 IMPORT           boot_crash_dump_regs_ptr
   84 00000000         ;=======================================================
                       =====================
   85 00000000         ;
   86 00000000         ;                             MODULE EXPORTS
   87 00000000         ;
   88 00000000         ;=======================================================
                       =====================
   89 00000000         ; Export the external symbols that are referenced in thi
                       s module.
   90 00000000                 EXPORT           sbl_loop_here
   91 00000000                 EXPORT           boot_read_l2esr
   92 00000000         ; Export the symbols __main and _main to prevent the lin
                       ker from
   93 00000000         ; including the standard runtime library and startup rou
                       tine.
   94 00000000                 EXPORT           __main
   95 00000000                 EXPORT           _main
   96 00000000                 EXPORT           sbl_save_regs
   97 00000000                 EXPORT           sbl1_external_abort_enable
   98 00000000         ;=======================================================
                       =====================
   99 00000000         ;
  100 00000000         ;                             MODULE DATA AREA
  101 00000000         ;
  102 00000000         ;=======================================================
                       =====================
  103 00000000         ;
  104 00000000         ; Area that defines the register data structure
  105 00000000         ;
  106 00000000                 AREA             SAVE_REGS, DATA
  107 00000000        7 
                       arm_core_t
                               RN               r7
  108 00000000                 MAP              0,arm_core_t
  109 00000000 00000000 
                       svc_r0  FIELD            4
  110 00000000 00000004 
                       svc_r1  FIELD            4
  111 00000000 00000008 
                       svc_r2  FIELD            4
  112 00000000 0000000C 
                       svc_r3  FIELD            4
  113 00000000 00000010 
                       svc_r4  FIELD            4
  114 00000000 00000014 
                       svc_r5  FIELD            4
  115 00000000 00000018 
                       svc_r6  FIELD            4



ARM Macro Assembler    Page 4 


  116 00000000 0000001C 
                       svc_r7  FIELD            4
  117 00000000 00000020 
                       svc_r8  FIELD            4
  118 00000000 00000024 
                       svc_r9  FIELD            4
  119 00000000 00000028 
                       svc_r10 FIELD            4
  120 00000000 0000002C 
                       svc_r11 FIELD            4
  121 00000000 00000030 
                       svc_r12 FIELD            4
  122 00000000 00000034 
                       svc_sp  FIELD            4
  123 00000000 00000038 
                       svc_lr  FIELD            4
  124 00000000 0000003C 
                       svc_spsr
                               FIELD            4
  125 00000000 00000040 
                       svc_pc  FIELD            4
  126 00000000 00000044 
                       cpsr    FIELD            4
  127 00000000 00000048 
                       sys_sp  FIELD            4
  128 00000000 0000004C 
                       sys_lr  FIELD            4
  129 00000000 00000050 
                       irq_sp  FIELD            4
  130 00000000 00000054 
                       irq_lr  FIELD            4
  131 00000000 00000058 
                       irq_spsr
                               FIELD            4
  132 00000000 0000005C 
                       abt_sp  FIELD            4
  133 00000000 00000060 
                       abt_lr  FIELD            4
  134 00000000 00000064 
                       abt_spsr
                               FIELD            4
  135 00000000 00000068 
                       udf_sp  FIELD            4
  136 00000000 0000006C 
                       udf_lr  FIELD            4
  137 00000000 00000070 
                       udf_spsr
                               FIELD            4
  138 00000000 00000074 
                       fiq_r8  FIELD            4
  139 00000000 00000078 
                       fiq_r9  FIELD            4
  140 00000000 0000007C 
                       fiq_r10 FIELD            4
  141 00000000 00000080 
                       fiq_r11 FIELD            4
  142 00000000 00000084 
                       fiq_r12 FIELD            4
  143 00000000 00000088 



ARM Macro Assembler    Page 5 


                       fiq_sp  FIELD            4
  144 00000000 0000008C 
                       fiq_lr  FIELD            4
  145 00000000 00000090 
                       fiq_spsr
                               FIELD            4
  146 00000000                 PRESERVE8
  147 00000000                 AREA             SBL1_VECTORS, CODE, READONLY, A
LIGN=4
  148 00000000                 CODE32
  149 00000000         unused_reset_vector
  150 00000000 EAFFFFFE        B                0x00000000
  151 00000004         undefined_instruction_vector
  152 00000004 EAFFFFFE        B                sbl1_undefined_instruction_nest
ed_handler
  153 00000008         swi_vector
  154 00000008 EAFFFFFE        B                boot_swi_c_handler
  155 0000000C         prefetch_abort_vector
  156 0000000C EAFFFFFE        B                sbl1_prefetch_abort_nested_hand
ler
  157 00000010         data_abort_vector
  158 00000010 EAFFFFFE        B                sbl1_data_abort_nested_handler
  159 00000014         reserved_vector
  160 00000014 EAFFFFFE        B                boot_reserved_c_handler
  161 00000018         irq_vector
  162 00000018 EAFFFFFE        B                boot_irq_c_handler
  163 0000001C         fiq_vector
  164 0000001C EAFFFFFE        B                boot_fiq_c_handler
  165 00000020         ;=======================================================
                       =====================
  166 00000020         ; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
  167 00000020         ;=======================================================
                       =====================
  168 00000020                 AREA             SBL1_ENTRY, CODE, READONLY, ALI
GN=4
  169 00000000                 CODE32
  170 00000000                 ENTRY
  171 00000000         __main
  172 00000000         _main
  173 00000000         ;=======================================================
                       =====================
  174 00000000         ;   We contiue to disable interrupt and watch dog until 
                       we jump to SBL3
  175 00000000         ;=======================================================
                       =====================
  176 00000000         sbl1_entry
  177 00000000         ;Change to Supervisor Mode
  178 00000000 E321F0D3        msr              CPSR_c, #Mode_SVC:OR:I_Bit:OR:F
_Bit
  179 00000004         ; Save the passing parameter from PBL
  180 00000004 E1A07000        mov              r7, r0
  181 00000008         ; Set VBAR (Vector Base Address Register) to SBL vector 
                       table
  182 00000008 E59F01E0        ldr              r0, =0x08005000
  183 0000000C EE0C0F10        MCR              p15, 0, r0, c12, c0, 0
  184 00000010         ; Setup the supervisor mode stack
  185 00000010 E59F01DC        ldr              r0, =|Image$$SBL1_SVC_STACK$$ZI
$$Limit|
  186 00000014 E1A0D000        mov              r13, r0



ARM Macro Assembler    Page 6 


  187 00000018         ; Switch to IRQ
  188 00000018 E321F0D2        msr              CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F
_Bit
  189 0000001C E1A0D000        mov              r13, r0
  190 00000020         ; Switch to FIQ
  191 00000020 E321F0D1        msr              CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F
_Bit
  192 00000024 E1A0D000        mov              r13, r0
  193 00000028         ; Switch to undefined mode and setup the undefined mode 
                       stack
  194 00000028 E321F0DB        msr              CPSR_c, #Mode_UND:OR:I_Bit:OR:F
_Bit
  195 0000002C E59F01C4        ldr              r0, =|Image$$SBL1_UND_STACK$$ZI
$$Limit|
  196 00000030 E1A0D000        mov              r13, r0
  197 00000034         ; Switch to abort mode and setup the abort mode stack
  198 00000034 E321F0D7        msr              CPSR_c, #Mode_ABT:OR:I_Bit:OR:F
_Bit
  199 00000038 E59F01BC        ldr              r0, =|Image$$SBL1_ABT_STACK$$ZI
$$Limit|
  200 0000003C E1A0D000        mov              r13, r0
  201 00000040         ; Return to supervisor mode
  202 00000040 E321F0D3        msr              CPSR_c, #Mode_SVC:OR:I_Bit:OR:F
_Bit
  203 00000044         ; Restore the passing parameter
  204 00000044 E1A00007        mov              r0, r7
  205 00000048         ; ------------------------------------------------------
                       ------------
  206 00000048         ; Call functions external to perform SBL1 function.
  207 00000048         ; It should never return.
  208 00000048         ; ------------------------------------------------------
                       ------------
  209 00000048 E59F51B0        ldr              r5, =sbl1_main_ctl
  210 0000004C E12FFF35        blx              r5
  211 00000050         ; For safety
  212 00000050 EB000048        bl               loophere    ; never returns, ke
                                                            ep lr in r14 for de
                                                            bug
  213 00000054         ;=======================================================
                       =====================
  214 00000054         ; sbl_save_regs
  215 00000054         ;
  216 00000054         ; PROTOTYPE
  217 00000054         ;   void sbl_save_regs();
  218 00000054         ;
  219 00000054         ; ARGS
  220 00000054         ;   None
  221 00000054         ;
  222 00000054         ; DESCRIPTION
  223 00000054         ;   Configure VBAR, vector table base register.
  224 00000054         ;   
  225 00000054         ;=======================================================
                       =====================    
  226 00000054         sbl_save_regs
  227 00000054         ; Save CPSR
  228 00000054 E92D0040        stmfd            sp!, {r6}
  229 00000058 E10F6000        mrs              r6, CPSR
  230 0000005C E92D0080        stmfd            sp!, {r7}
  231 00000060         ; Switch to SVC mode



ARM Macro Assembler    Page 7 


  232 00000060 E321F0D3        msr              CPSR_c, #Mode_SVC:OR:I_Bit:OR:F
_Bit
  233 00000064         ; Capture User Mode r0-r14 (no SPSR)
  234 00000064         ; Registers are stored in svc structure for backwards co
                       mpatibility
  235 00000064 E59F7198        ldr              arm_core_t,=boot_crash_dump_reg
s_ptr
  236 00000068 E5977000        ldr              arm_core_t, [r7]
  237 0000006C E5870000        str              r0,  svc_r0
  238 00000070 E5871004        str              r1,  svc_r1
  239 00000074 E5872008        str              r2,  svc_r2
  240 00000078 E587300C        str              r3,  svc_r3
  241 0000007C E5874010        str              r4,  svc_r4
  242 00000080 E5875014        str              r5,  svc_r5
  243 00000084         ; Store r6 later after restoring it from the stack
  244 00000084         ; Store r7 later after restoring it from the stack
  245 00000084 E5878020        str              r8,  svc_r8
  246 00000088 E5879024        str              r9,  svc_r9
  247 0000008C E587A028        str              r10, svc_r10
  248 00000090 E587B02C        str              r11, svc_r11
  249 00000094 E587C030        str              r12, svc_r12
  250 00000098 E587E038        str              r14, svc_lr
  251 0000009C         ; Store SP value
  252 0000009C E587D034        str              sp, svc_sp
  253 000000A0         ; Store SPSR
  254 000000A0 E14F0000        mrs              r0, SPSR
  255 000000A4 E587003C        str              r0, svc_spsr
  256 000000A8         ; Store the PC for restoration later
  257 000000A8 E59F0158        ldr              r0, =sbl_save_regs
  258 000000AC E5870040        str              r0, svc_pc
  259 000000B0         ; Save SYS mode registers
  260 000000B0 E321F0DF        msr              CPSR_c, #Mode_SYS:OR:I_Bit:OR:F
_Bit
  261 000000B4 E587D048        str              r13, sys_sp
  262 000000B8 E587E04C        str              r14, sys_lr
  263 000000BC         ; Save IRQ mode registers
  264 000000BC E321F0D2        msr              CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F
_Bit
  265 000000C0 E587D050        str              r13, irq_sp
  266 000000C4 E587E054        str              r14, irq_lr
  267 000000C8 E14F0000        mrs              r0, SPSR
  268 000000CC E5870058        str              r0, irq_spsr
  269 000000D0         ; Save ABT mode registers
  270 000000D0 E321F0D7        msr              CPSR_c, #Mode_ABT:OR:I_Bit:OR:F
_Bit
  271 000000D4 E587D05C        str              r13, abt_sp
  272 000000D8 E587E060        str              r14, abt_lr
  273 000000DC E14F0000        mrs              r0, SPSR
  274 000000E0 E5870064        str              r0, abt_spsr
  275 000000E4         ; Save UND mode registers
  276 000000E4 E321F0DB        msr              CPSR_c, #Mode_UND:OR:I_Bit:OR:F
_Bit
  277 000000E8 E587D068        str              r13, udf_sp
  278 000000EC E587E06C        str              r14, udf_lr
  279 000000F0 E14F0000        mrs              r0, SPSR
  280 000000F4 E5870070        str              r0, udf_spsr
  281 000000F8         ; Save FIQ mode registers
  282 000000F8 E321F0D1        msr              CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F
_Bit



ARM Macro Assembler    Page 8 


  283 000000FC E5878074        str              r8,  fiq_r8
  284 00000100 E5879078        str              r9,  fiq_r9
  285 00000104 E587A07C        str              r10, fiq_r10
  286 00000108 E587B080        str              r11, fiq_r11
  287 0000010C E587C084        str              r12, fiq_r12
  288 00000110 E587D088        str              r13, fiq_sp
  289 00000114 E587E08C        str              r14, fiq_lr
  290 00000118 E14F0000        mrs              r0, SPSR
  291 0000011C E5870090        str              r0, fiq_spsr
  292 00000120         ; Switch back to original mode using r6 which holds the 
                       cpsr
  293 00000120 E121F006        msr              CPSR_c, r6
  294 00000124         ; Store CPSR
  295 00000124 E5876044        str              r6, cpsr
  296 00000128         ; Restore r7 value 
  297 00000128 E8BD0004        ldmfd            sp!, {r2}
  298 0000012C E587201C        str              r2, svc_r7
  299 00000130         ; Restore r6 value
  300 00000130 E8BD0002        ldmfd            sp!, {r1}
  301 00000134 E5871018        str              r1, svc_r6
  302 00000138 E1A06001        mov              r6, r1
  303 0000013C E1A07002        mov              r7, r2
  304 00000140         ; Finished so return    
  305 00000140 E12FFF1E        bx               lr
  306 00000144         ;=======================================================
                       ===============
  307 00000144         ; Called by sbl1_error_handler only. We clean up the reg
                       isters and loop
  308 00000144         ; here until JTAG is connected.
  309 00000144         ;=======================================================
                       ===============
  310 00000144         sbl_loop_here
  311 00000144 E3A00000        mov              r0,#0
  312 00000148 E3A01000        mov              r1,#0
  313 0000014C E3A02000        mov              r2,#0
  314 00000150 E3A03000        mov              r3,#0
  315 00000154 E3A04000        mov              r4,#0
  316 00000158 E3A05000        mov              r5,#0
  317 0000015C E3A06000        mov              r6,#0
  318 00000160 E3A07000        mov              r7,#0
  319 00000164 E3A08000        mov              r8,#0
  320 00000168 E3A09000        mov              r9,#0
  321 0000016C E3A0A000        mov              r10,#0
  322 00000170 E3A0B000        mov              r11,#0
  323 00000174 E3A0C000        mov              r12,#0
  324 00000178         loophere
  325 00000178 EAFFFFFE        b                loophere
  326 0000017C         ;=======================================================
                       ===============
  327 0000017C         ; SBL1 exception handlers that can have nested calls to 
                       them.  These
  328 0000017C         ; handlers check for nesting and if it is the first exce
                       ption they
  329 0000017C         ; call a "C" exception handler that calls the SBL1 error
                        handler.
  330 0000017C         ; If it is a nested exception, the "C" exception handler
                        is not
  331 0000017C         ; re-entered and the JTAG interface is enabled immediate
                       ly. Nesting



ARM Macro Assembler    Page 9 


  332 0000017C         ; is only a concern for undefined instruction and abort 
                       exceptions.
  333 0000017C         ; Note, a separate exception handler is used for each ex
                       ception to
  334 0000017C         ; provide additional debug information (see sbl1_error_h
                       andler.c for
  335 0000017C         ; more information).
  336 0000017C         ;=======================================================
                       ===============
  337 0000017C         sbl1_undefined_instruction_nested_handler
  338 0000017C E59F5088        ldr              r5,=boot_undefined_instruction_
c_handler
  339 00000180 EA000003        b                check_for_nesting
  340 00000184         sbl1_prefetch_abort_nested_handler
  341 00000184 E59F5084        ldr              r5,=boot_prefetch_abort_c_handl
er
  342 00000188 EA000001        b                check_for_nesting
  343 0000018C         sbl1_data_abort_nested_handler
  344 0000018C E59F5080        ldr              r5,=boot_data_abort_c_handler
  345 00000190 EAFFFFFF        b                check_for_nesting
  346 00000194         ;=======================================================
                       ===============
  347 00000194         ; Checks for nested exceptions and then calls the "C" ex
                       ception
  348 00000194         ; handler pointed to by R5 if this is the first time thi
                       s exception
  349 00000194         ; has occurred, otherwise it calls the "C" nested except
                       ion handler
  350 00000194         ; that just enables JTAG debug access.  The mode stack p
                       ointer is used
  351 00000194         ; to determine if a nested exception or a second abort e
                       xception has
  352 00000194         ; occurred.  This is accomplished by comparing the mode 
                       stack pointer
  353 00000194         ; to the top of the stack that was initially assigned to
                        the stack.
  354 00000194         ; If they are equal, it is a first time exception.
  355 00000194         ;=======================================================
                       ===============
  356 00000194         check_for_nesting
  357 00000194         ; Initial stack base depends on the current processor mo
                       de
  358 00000194         ; Mode will either be ABT or UND.  Load proper stack bas
                       e.
  359 00000194 E10F7000        mrs              r7, cpsr
  360 00000198 E207701F        and              r7, r7, #Mode_SYS ; Use Mode_SY
                                                            S for mode bitmask 
                                                            as all bits are hig
                                                            h
  361 0000019C E3570017        cmp              r7, #Mode_ABT
  362 000001A0 059F6054        ldreq            r6,=|Image$$SBL1_ABT_STACK$$ZI$
$Limit|
  363 000001A4 E357001B        cmp              r7, #Mode_UND
  364 000001A8 059F6048        ldreq            r6,=|Image$$SBL1_UND_STACK$$ZI$
$Limit|
  365 000001AC E1A0700D        mov              r7, r13     ; Save current stac
                                                            k ptr
  366 000001B0 E1560007        cmp              r6, r7      ; Compare initial a
                                                            nd actual



ARM Macro Assembler    Page 10 


  367 000001B4 012FFF35        blxeq            r5          ; First time except
                                                            ion
  368 000001B8 E59F5058        ldr              r5,=boot_nested_exception_c_han
dler 
                                                            ; This is a nested 
                                                            exception
  369 000001BC E12FFF35        blx              r5
  370 000001C0         boot_read_l2esr
  371 000001C0 E3A01F81        mov              r1, #0x204  ;Indirect address o
                                                            f L2ESR
  372 000001C4 F57FF06F        isb
  373 000001C8 EE6F1FD0        mcr              p15,3,r1,c15,c0,6 ;Write the L2
                                                            CPUCPSELR with the 
                                                            indirect address of
                                                             the L2ESR
  374 000001CC F57FF06F        isb
  375 000001D0 EE7F0FF0        mrc              p15,3,r0,c15,c0,7 
                                                            ;store L2ESR to r0
  376 000001D4 F57FF06F        isb
  377 000001D8 E12FFF1E        bx               lr
  378 000001DC         ; void sbl1_external_abort_enable(uint32 flags)
  379 000001DC         sbl1_external_abort_enable
                               FUNCTION
  380 000001DC E2000D07        and              r0, r0, #F_Bit:OR:I_Bit:OR:A_Bi
t 
                                                            ; Only care about A
                                                            /I/F bits.
  381 000001E0 E10F1000        mrs              r1, cpsr    ; Read the status r
                                                            egister.
  382 000001E4 E1C11000        bic              r1, r1, r0  ; Clear requested A
                                                            /I/F bits
  383 000001E8 E123F001        msr              cpsr_cx, r1 ; Write control & e
                                                            xtension field
  384 000001EC E12FFF1E        bx               lr
  385 000001F0                 ENDFUNC
  386 000001F0                 END
              08005000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
              00000000 
Command Line: --cpu=7-A.security --apcs=/noswst/interwork --no_unaligned_access
 -o/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0
22933/b/boot_images/core/boot/secboot3/hw/build/sbl1/a53/LAASANAZ/msm8952/sbl1/
sbl1.o -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_2016
0818_022933/b/boot_images/core/boot/secboot3/hw/build -I/local/mnt/workspace/CR
MBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/build/
cust -I. -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20
160818_022933/b/boot_images/core/api/boot -I/local/mnt/workspace/CRMBuilds/BOOT
.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/boot/qfpr
om -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818
_022933/b/boot_images/core/buses/api/spmi -I/local/mnt/workspace/CRMBuilds/BOOT
.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/buses/api/icb



ARM Macro Assembler    Page 11 


 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0
22933/b/boot_images/core/buses/api/uart -I/local/mnt/workspace/CRMBuilds/BOOT.B
F.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/buses/api/i2c -
I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022
933/b/boot_images/core/api/dal -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-001
77-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/services -I/local/mn
t/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot
_images/core/api/storage -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M89
17LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers -I/local/mnt
/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_
images/core/api/systemdrivers/pmic -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3
-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/hw
io/msm8917 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_
20160818_022933/b/boot_images/core/api/wiredconnectivity -I/local/mnt/workspace
/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/cor
e/api/securemsm -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANA
ZB-1_20160818_022933/b/boot_images/core/securemsm/secboot/api -I/local/mnt/work
space/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_image
s/core/api/kernel/libstd/stringl -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-0
0177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/securemsm/secimgau
th -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818
_022933/b/boot_images/core/api/hwengines -I/local/mnt/workspace/CRMBuilds/BOOT.
BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/power -I/l
ocal/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933
/b/boot_images/core/power/utils/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.
3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/power/cpr/rpm -
I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022
933/b/boot_images/core/boot/secboot3/src -I/local/mnt/workspace/CRMBuilds/BOOT.
BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/ddr/commo
n -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_
022933/b/boot_images/core/boot/ddr/hw/msm8917 -I/local/mnt/workspace/CRMBuilds/
BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/ddr/
hw/phy -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_2016
0818_022933/b/boot_images/core/boot/ddr/hw/controller -I/local/mnt/workspace/CR
MBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/b
oot/secboot3/hw/msm8917 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M891
7LAAAANAZB-1_20160818_022933/b/boot_images/core/dal/src -I/local/mnt/workspace/
CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core
/dal/config -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1
_20160818_022933/b/boot_images/core/services/utils/src -I/local/mnt/workspace/C
RMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/
storage/flash/src/dal -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917L
AAAANAZB-1_20160818_022933/b/boot_images/core/storage/flash/src/hal -I/local/mn
t/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot
_images/core/storage/flash/tools/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3
.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/flash/inc 
-I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_02
2933/b/boot_images/core/storage/sdcc/src -I/local/mnt/workspace/CRMBuilds/BOOT.
BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/sdcc/s
rc/hal -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_2016
0818_022933/b/boot_images/core/storage/sdcc/src/bsp -I/local/mnt/workspace/CRMB
uilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/sto
rage/efs/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-
1_20160818_022933/b/boot_images/core/storage/hfat/inc -I/local/mnt/workspace/CR
MBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/s
torage/hotplug/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAA
ANAZB-1_20160818_022933/b/boot_images/core/storage/tools/deviceprogrammer_ddr/s
rc/firehose -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1
_20160818_022933/b/boot_images/core/systemdrivers/tlmm/inc -I/local/mnt/workspa



ARM Macro Assembler    Page 12 


ce/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/c
ore/systemdrivers/tlmm/src -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M
8917LAAAANAZB-1_20160818_022933/b/boot_images/core/wiredconnectivity/qhsusb/inc
 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0
22933/b/boot_images/core/wiredconnectivity/qusb/inc -I/local/mnt/workspace/CRMB
uilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/sec
uremsm/cryptodrivers/ce/shared/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3
-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/cryptodriv
ers/ce/test/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANA
ZB-1_20160818_022933/b/boot_images/core/securemsm/cryptodrivers/prng/shared/inc
 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0
22933/b/boot_images/core/securemsm/cryptodrivers/prng/test/inc -I/local/mnt/wor
kspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_imag
es/core/securemsm/secdbgplcy/api -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-0
0177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/cryptodriver
s/prng/shared/src -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAA
NAZB-1_20160818_022933/b/boot_images/core/api/securemsm/secboot -I/local/mnt/wo
rkspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_ima
ges/core/api/securemsm/seccfg -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-0017
7-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/secmath/shared/
inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_2016081
8_022933/b/boot_images/core/securemsm/fuseprov/chipset/msm8917/inc -I/local/mnt
/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_
images/core/hwengines/mhi -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8
917LAAAANAZB-1_20160818_022933/b/boot_images/core/hwengines/pcie -I/local/mnt/w
orkspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_im
ages/core/boot/secboot3/hw/msm8917/sbl1 --list=/local/mnt/workspace/CRMBuilds/B
OOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secbo
ot3/hw/build/sbl1/a53/LAASANAZ/msm8952/sbl1/sbl1.o.lst --sitelicense /local/mnt
/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_
images/core/boot/secboot3/hw/build/sbl1/a53/LAASANAZ/msm8952/sbl1/sbl1.o.i
