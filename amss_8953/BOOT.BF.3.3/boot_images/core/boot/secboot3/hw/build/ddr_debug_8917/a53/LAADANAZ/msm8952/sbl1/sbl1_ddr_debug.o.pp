#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1_ddr_debug.S"
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                              SBL1 DDR Debug
;
; GENERAL DESCRIPTION
;   This file bootstraps the processor. The Start-up
;   (SBL1_ddr_debug) performs the following functions:
;
;      - Set up the hardware to continue boot process.
;      - Initialize DDR memory
;      - Copies SBL1 to CODERAM
;      - Sets up stack in WDOG reset path .
;      - Jumps to OCIMEM to execute WDOG reset path
;
;   The SBL1_ddr_debug is written to perform the above functions with optimal speed.
;   It also attempts to minimize the execution time and hence reduce boot time.
;
; Copyright 2013 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header: 
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 07/20/15   yps     Porting code to 8952 platform.
; 07/14/14   yps     Added sbl1_external_abort_enable funtion for DDI build
; 05/14/14   yps     Porting code to 8916 platform.
; 09/13/13   sl      Cleaned up exception handlers.
; 07/17/13   sr      Initial Version.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"














 

















 






 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"




 






















 


























 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/hwio/msm8917/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 58 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 85 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 277 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 309 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 459 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 473 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 503 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 631 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 768 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 782 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 796 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1008 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1036 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1334 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1456 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1500 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1532 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1660 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1744 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1758 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1772 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 1996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2156 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2198 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2292 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2352 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2366 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2418 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2490 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2506 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2522 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2580 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2682 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2718 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2732 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2820 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2878 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2928 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 2990 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3166 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3270 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3398 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3412 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3456 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3484 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3712 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3726 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3796 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3822 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3960 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 3996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4016 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4052 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4068 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4140 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4158 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4200 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4214 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4230 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4246 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4272 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4304 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4378 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4404 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4436 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4450 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4478 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4536 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4568 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4582 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4686 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4742 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4934 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4960 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4978 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 4992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5034 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5092 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5138 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5166 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5198 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5242 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5270 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5284 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5298 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5374 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5446 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5462 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5488 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5506 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5520 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5548 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5568 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5582 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5602 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5616 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5636 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5670 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5690 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5706 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5720 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5772 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5828 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5888 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5930 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5958 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 5986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6002 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6040 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6260 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6352 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6370 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6390 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6432 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6446 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6482 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6514 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6530 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6546 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6562 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6594 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6628 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6644 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6678 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6696 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6712 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6744 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6776 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6790 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6836 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6852 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6888 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6924 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6944 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 6980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7002 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7018 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7044 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7060 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7168 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7188 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7230 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7370 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7384 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7400 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7444 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7490 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7504 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7520 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7550 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7580 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7594 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7624 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7684 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7704 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7776 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7800 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7872 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7920 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7930 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 7996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8034 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8058 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8142 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8156 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8198 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8212 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8226 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8268 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8352 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8366 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8380 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8394 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8408 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8424 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8444 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8480 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8508 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8566 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8616 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8674 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 8976 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9492 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9550 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9636 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9694 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9838 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9852 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9880 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9978 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 9992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10060 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10090 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10158 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10176 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10190 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10260 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10278 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10292 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10320 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10336 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10380 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10394 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10408 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10456 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10484 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10588 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10604 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10620 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10636 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10664 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10692 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10756 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10872 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10934 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10950 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10966 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 10998 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11040 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11058 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11158 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11228 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11274 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11344 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11358 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11372 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11458 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11478 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11514 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11530 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11588 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11650 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11676 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11692 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11708 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11744 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11774 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11790 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11876 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11906 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11938 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 11998 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12012 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12078 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12094 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12140 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12154 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12200 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12236 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12312 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12342 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12372 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12446 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12488 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12502 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12518 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12560 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12606 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12620 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12650 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12676 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12694 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12708 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12722 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12736 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12792 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12838 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12852 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12940 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 12998 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13070 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13084 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13144 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13158 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13178 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13268 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13312 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13380 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13400 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13436 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13452 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13480 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13590 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13606 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13622 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13648 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13708 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13792 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13818 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13876 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 13998 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14012 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14146 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14188 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14268 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14278 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14364 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 14385 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 14416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 14433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14447 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14515 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14703 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14759 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14815 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14891 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14927 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14941 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14951 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14971 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 14997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15053 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15065 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 15095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15225 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15799 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 15994 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16161 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16497 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16523 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16595 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16624 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16743 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16785 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16799 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16831 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16849 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16881 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16927 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 16991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17023 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17052 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17060 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17074 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17111 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17246 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17258 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17316 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17348 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17375 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17385 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17599 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17971 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 17991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18001 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18053 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18063 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18083 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18146 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18187 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18249 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18435 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18503 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18575 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18586 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18617 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18637 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18648 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18747 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18927 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 18962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19030 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19092 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19242 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19364 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19431 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19655 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19707 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19721 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19781 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19819 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19923 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 19943 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 




#line 20082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20144 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20190 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20316 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20384 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20404 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20448 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20472 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20486 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20552 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20622 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20666 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20686 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20774 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20842 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20884 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20894 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20914 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 20991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21045 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21703 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21759 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21789 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21805 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21819 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21861 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21885 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21927 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21941 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21955 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 21997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22060 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22081 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22535 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22559 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22583 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22593 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22613 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22637 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22645 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22733 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22923 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22947 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 22989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23001 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 





#line 23050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23092 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23176 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23260 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23344 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23550 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23656 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23722 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23755 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23770 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23785 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23800 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23815 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 23985 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24034 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24049 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 





#line 24066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24087 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 





#line 24155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24201 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24286 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 





#line 24328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"



 





#line 24420 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24431 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24522 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24552 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24622 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 24662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/msmhwioreg.h"

#line 42 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"




 









 









  



 







 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 







 









  



 




 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/dal/HALcomdef.h"





























 




 
#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/dal/HALcomdef.h"



#line 34 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"





 




  







 









 








 





  













 
























 















 





























 


















 







































 


































 
#line 265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"
 











 


 



   


 





 
#line 330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"

#line 355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"








 






 
#line 377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"


#line 406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"

 



#line 39 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"





 










 


 






 









#line 91 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"






 
#line 130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"









 


























 
#line 174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"


 



#line 43 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"













 






















 





 
#line 203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"






 
















 
#line 233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"


#line 44 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"





 

 




 



 




 



 





 





























#line 46 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1_ddr_debug.S"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/boot_target.h"















 



































 





 




#line 1 "./custlaadanaza.h"







 

#line 1 "./targlaadanaza.h"







 

#line 138 "./targlaadanaza.h"




#line 12 "./custlaadanaza.h"


#line 20 "./custlaadanaza.h"




#line 64 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/boot_target.h"



 





 




#line 105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/boot_target.h"






 






  


 






 






 





 









 




 







 









 






 


  




 





 





 




 




 




 





 




 




 




 




 




 




                                 


                                 
                                 


                                 
                               


                                 
                               


                                 
 


                                 
 


                             
                                 



 






 





                             


                             
#line 303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8917/boot_target.h"




  
 






 



                              


 



   


 


 




 


 


#line 47 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1_ddr_debug.S"





;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_SVC                EQU    0x13
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1b
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40

;============================================================================
; MACRO mdsb
;
; ARGS
;   NONE
;
; DESCRIPTION
;   Performs a data synchronization barrier, either using the ARMv7 instruction
;   or the legacy coprocessor instruction.
;
; NOTES
;   For reference see ARM DDI 0406A-03 section A3.8.3.
;============================================================================
   MACRO
   mdsb
   IF {ARCHITECTURE} = "7-A" :LOR: {ARCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
      dsb                        ; RVDS >= 3.0 supports ARMv7 instructions
   ELSE
      IF {CONFIG} = 32
         DCI 0xF57FF040 :OR: SY  ; ARMv7 A1 Opcode
      ELSE
         DCI 0xF3BF8F40 :OR: SY  ; ARMv7 T1 Opcode
      ENDIF
      ;mcr    p15, 0, r0, c7, c10, 4  ; Legacy Data Write Barrier
   ENDIF
   MEND

;============================================================================
; MACRO misb
;
; ARGS
;   NONE
;
; DESCRIPTION
;   Performs an instruction synchronization barrier, either using the ARMv7
;   instruction or the legacy coprocessor instruction.
;
; NOTES
;   For reference see ARM DDI 0406A-03 section A3.8.3.
;============================================================================
   MACRO
   misb
   IF {ARCHITECTURE} = "7-A" :LOR: {ARCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
      isb                        ; RVDS >= 3.0 supports ARMv7 instructions
   ELSE
      IF {CONFIG} = 32
         DCI 0xF57FF060 :OR: SY  ; ARMv7 A1 Opcode
      ELSE
         DCI 0xF3BF8F60 :OR: SY  ; ARMv7 T1 Opcode
      ENDIF
      ;mcr    p15, 0, r0, c7, c5, 4   ; Legacy Pre-Fetch Flush
   ENDIF
   MEND


;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT ddr_debug_init
    IMPORT sbl1_wdogpath_main_ctl
    IMPORT sbl1_ddr_debug_main_ctl


;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sbl_loop_here
    EXPORT ddr_debug_invalidate_tlb

    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT   __main
    EXPORT   _main
    EXPORT  sbl_save_regs
    EXPORT  sbl1_external_abort_enable

;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

    PRESERVE8
    AREA    SBL1_INDIRECT_VECTOR_TABLE, CODE, READONLY, ALIGN=4
    CODE32
unused_reset_vector
    B     0x00000000
undefined_instruction_vector
    B     loophere
swi_vector
    B     loophere
prefetch_abort_vector
    B     loophere
data_abort_vector
    B     loophere
reserved_vector
    B     loophere
irq_vector
    B     loophere
fiq_vector
    B     loophere


;============================================================================
; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
;============================================================================

    AREA  SBL1_ENTRY, CODE, READONLY, ALIGN=4
    CODE32
    ENTRY

__main
_main

sbl1_entry

    ;Change to Supervisor Mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Save the passing parameter from PBL
    mov     r7, r0

    ; Set VBAR (Vector Base Address Register) to SBL vector table
    ldr     r0, =0x08006000
    MCR     p15, 0, r0, c12, c0, 0
	
    ; Setup the supervisor mode stack
    ldr     r0, =(0x08006000 + 0x0050000)
    mov     r13, r0

    ; Switch to undefined mode and setup the undefined mode stack
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    mov     r13, r0

    ; Switch to abort mode and setup the abort mode stack
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    mov     r13, r0

    ; Return to supervisor mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Restore the passing parameter
    mov     r0, r7
    
    ldr    r5, =ddr_debug_init
    blx    r5

    ldr    r5,=sbl1_ddr_debug_main_ctl
    blx    r5

    b loophere

;============================================================================
; ddr_debug_invalidate_tlb
;
; ARGS
;   NONE
;
; DESCRIPTION
;   Invalidates the entire Translation Look-aside Buffer (TLB) as a unified
;   operation (Data and Instruction). Invalidates all unlocked entries in the
;   TLB. Causes the prefetch buffer to be flushed. All following instructions
;   are fetched after the TLB invalidation.
;   We should do this before we enable to MMU.
;============================================================================
ddr_debug_invalidate_tlb
   ; Call memory barrier operations to ensure completion of all cache
   ; maintenance & branch predictor operations appearing in program
   ; order, before these instructions
   mdsb
   misb
   mov     r0 , #0
   mcr     p15, 0, r0, c8, c7, 0
   mdsb
   misb
   bx lr

;============================================================================
; sbl_save_regs
;
; PROTOTYPE
;   void sbl_save_regs();
;
; ARGS
;   None
;
; DESCRIPTION
;   Configure VBAR, vector table base register.
;   
;============================================================================    
sbl_save_regs	
 
    ; Finished so return    
    bx lr


; void sbl1_external_abort_enable(uint32 flags)
sbl1_external_abort_enable FUNCTION

    bx      lr
    ENDFUNC
;======================================================================
; Called by sbl1_error_handler only. We clean up the registers and loop
; here until JTAG is connected.
;======================================================================
sbl_loop_here
    mov r0,#0
    mov r1,#0
    mov r2,#0
    mov r3,#0
    mov r4,#0
    mov r5,#0
    mov r6,#0
    mov r7,#0
    mov r8,#0
    mov r9,#0
    mov r10,#0
    mov r11,#0
    mov r12,#0
loophere
    b loophere

   END
