#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sys_debug.S"
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;             System Debug Image Initialization Routine
;
; GENERAL DESCRIPTION
;	This file contains initialization code for the system_debug image.
;       This performs a dump of the CPU core information as well.
;
; Copyright 2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; when         who   what, where, why
; ----------   ---   --------------------------------------------------------
; 07/22/2014   lm    Aligned with V1.1 cpu dump table structure
; 06/13/2014   lm    Added get_cur_cluster_num API
; 03/13/2014   ck    Save r0 and r1 values that were stored by PBL 
; 03/03/2014   ck    Pull PS_HOLD high at start because PBL is not doing it
; 02/18/2014   ck    Removed duplicate 32 to 64 bit switch logic
;                    Single copy is stored in SDI code space and shared
; 02/06/2014   ck    Added A53 32 to 64 bit logic switch logic 
; 01/02/2014   ck    Initial release for Bear family
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"




 



















 


























 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/hwio/msm8937/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 55 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 82 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 166 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 212 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 226 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 242 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 274 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 292 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 334 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 348 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 390 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 400 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 426 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 447 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 501 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 540 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 548 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 558 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 586 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 600 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 618 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 632 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1153 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1167 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1479 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1493 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1535 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1563 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1703 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1759 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1947 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 1989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2083 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2177 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2385 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2417 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2531 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2565 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2611 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2629 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2719 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2885 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 2985 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3001 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3017 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3033 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3123 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3287 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3563 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3631 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3733 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3747 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3831 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3943 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 3995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4285 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4361 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4375 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4417 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4493 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4535 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4565 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4739 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4785 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4799 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4955 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 4997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5049 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5065 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5137 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5217 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5349 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5495 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5509 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5523 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5595 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5613 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5655 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5669 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5759 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5891 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 5987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6091 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6109 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6123 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6319 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6411 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6493 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6579 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6595 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6611 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6631 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6885 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6981 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 6995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7065 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7083 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7147 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7177 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7311 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7367 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7599 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7613 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7687 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7823 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7947 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 7999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8045 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8089 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8493 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8739 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8763 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8781 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8805 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8881 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8951 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 8993 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9035 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9049 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9063 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9091 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9147 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9161 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9217 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9711 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9789 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9947 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 9997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10183 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10315 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10369 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10601 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10629 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10795 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10825 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 10995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11177 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11191 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11263 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11277 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11551 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11565 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11593 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11621 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11739 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11823 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 11993 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12035 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12335 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12349 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12535 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12579 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12599 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12683 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12781 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12831 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12943 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 12989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13069 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13161 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13191 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13287 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13347 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13479 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13493 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13509 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13551 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13583 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13611 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13815 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 13989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14033 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14075 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14089 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14183 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14555 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14613 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 14989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15235 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 15276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 15307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 15328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15342 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15358 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15372 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15392 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15420 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15436 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15452 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15472 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15486 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15500 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15514 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15598 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15612 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15682 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15696 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15808 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15822 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15836 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15850 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15864 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15878 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15906 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15920 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 15996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16018 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16046 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16076 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16118 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16144 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16158 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 16207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16461 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16923 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16938 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 16995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17017 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17087 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17263 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17423 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17468 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17497 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17530 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17563 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17637 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17652 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17880 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17891 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 17997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18018 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18036 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18092 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18236 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18264 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18274 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18418 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18664 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18706 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18739 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18804 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18822 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18888 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18924 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 18972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19074 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19140 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19244 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19288 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19298 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19309 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19320 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19360 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19370 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19380 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19390 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19400 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19420 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19440 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19593 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19687 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19914 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19928 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 19986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20076 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20090 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20142 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 




#line 20165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20287 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20531 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20555 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20725 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20749 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 20997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21012 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21030 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21044 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21058 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21180 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21230 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21358 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21374 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21436 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21450 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21478 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21492 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21506 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21532 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21548 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21574 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21590 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21612 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21650 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21672 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21694 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21720 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21748 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21762 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21778 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21792 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21806 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21820 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21834 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21878 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21936 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21950 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 21996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22052 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22094 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22108 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22366 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22424 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22440 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22480 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22568 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22612 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22656 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22670 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22684 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22694 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22704 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22718 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22748 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22762 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22776 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22796 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22806 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22836 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22880 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22894 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22928 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22938 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 22996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23034 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23044 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23078 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23092 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23120 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 





#line 23181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23244 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23286 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23349 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23370 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23412 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23517 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23559 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23580 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23601 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23622 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23664 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23812 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23844 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23916 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23976 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 23991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24002 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24083 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24180 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"



 





#line 24197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24246 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 24263 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/msmhwioreg.h"

#line 38 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sys_debug.S"
	

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SVC                EQU    0x13
Mode_MON		EQU    0x16
Mode_ABT                EQU    0x17
Mode_HYP                EQU    0x1A
Mode_UND                EQU    0x1B
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40

; Secure Configuration Register Bits
SCR_NS_BIT                  EQU     0x01    ; Non-Secure (NS) bit
SCR_IRQ_BIT                 EQU     0x02    ; IRQ bit
SCR_FIQ_BIT                 EQU     0x04    ; FIQ bit
SCR_FW_BIT                  EQU     0x10    ; F Bit writable (FW)
SCR_AW_BIT                  EQU     0x20    ; A Bit writable (AW)



;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT sys_debug_undefined_c_handler
    IMPORT sys_debug_swi_c_handler
    IMPORT sys_debug_prefetch_abort_c_handler
    IMPORT sys_debug_data_abort_c_handler
    IMPORT sys_debug_reserved_c_handler
    IMPORT sys_debug_irq_c_handler
    IMPORT sys_debug_fiq_c_handler

    ; Main Entry point of C code
    IMPORT sys_debug_main_ctl
;============================================================================
;                             MODULE EXPORTS
;============================================================================

	EXPORT get_cur_cluster_num
	
;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================


    PRESERVE8
    AREA    SYSTEM_DEBUG_VECTOR_TABLE, CODE, READWRITE
    CODE32
unused_reset_vector
    DCD     0x00000000 
undefined_instruction_vector
    DCD     sys_debug_undefined_c_handler
swi_vector
    DCD     sys_debug_swi_c_handler
prefetch_abort_vector
    DCD     sys_debug_prefetch_abort_c_handler
data_abort_vector
    DCD     sys_debug_data_abort_c_handler
reserved_vector
    DCD     sys_debug_reserved_c_handler
irq_vector
    DCD     sys_debug_irq_c_handler
fiq_vector
    DCD     sys_debug_fiq_c_handler

;==========================================================================
;
;		get_cur_cluster_num
;
;==========================================================================
get_cur_cluster_num 
    mrc     p15, 0, r0, c0, c0, 5 
    and     r0, r0, #0xFF00         ; AFFL1 is the cluster number. 
    asr     r0, r0, #8 
    bx      lr 

;============================================================================
; Qualcomm System Debug Image ENTRY POINT
;============================================================================

    AREA  SYSTEM_DEBUG_ENTRY, CODE, READONLY, ALIGN=4
    CODE32
    
; This function is written specifically to match the sys_debug_cpu_ctxt_regs
; structure.  If the structure changes this function must change.
; The structure has 64 bit values so each value must be stored individually.
; 
sys_debug_entry
    ; We have two registers to play with 	
    ; r0, r1 have been saved by the PBL

    ; Always start in monitor mode.
    msr	CPSR_c,	#Mode_MON:OR:I_Bit:OR:F_Bit

    ; PS_HOLD needs to be pulled high to fix hardware issue
    ldr r0, =((0x004a0000 + 0x0000b000) + 0x00000000)
    ldr r1, =0x1
    str r1, [r0]
	
    ; Keep r0 as the base address of the structure and use r1 as current address
    ldr r0, =sys_debug_cpu_ctxt_base
    mov r1, r0
	
    ; Store r2 - r12 taking into account the 64 bit structure
    ; r0 and r1 will be saved later.  These were stored by PBL. 
    add r1, r1, #8
    add r1, r1, #8
    str r2, [r1]
    add r1, r1, #8
    str r3, [r1]
    add r1, r1, #8
    str r4, [r1]
    add r1, r1, #8
    str r5, [r1]
    add r1, r1, #8
    str r6, [r1]
    add r1, r1, #8
    str r7, [r1]
    add r1, r1, #8
    str r8, [r1]
    add r1, r1, #8
    str r9, [r1]
    add r1, r1, #8
    str r10, [r1]
    add r1, r1, #8
    str r11, [r1]
    add r1, r1, #8
    str r12, [r1]
    add r1, r1, #8

    ; Switch to SYS mode so we can get r13 and r14 from USR mode.
    ; SYS mode uses the same registers as USR but maintains privliges.
    cps #Mode_SYS
    str r13, [r1]	
    add r1, r1, #8
    str r14, [r1]
    add r1, r1, #8

    ; HYP mode only exists in NS world.
    ; Skipping collection of r13 HYP for now.
    add r1, r1, #8

    ; Store r14 and r13 for IRQ Mode
    cps #Mode_IRQ
    str r14, [r1]
    add r1, r1, #8
    str r13, [r1]
    add r1, r1, #8

    ; Store r14 and r13 for SVC Mode
    cps #Mode_SVC
    str r14, [r1]
    add r1, r1, #8
    str r13, [r1]
    add r1, r1, #8

    ; Store r14 and r13 for ABT Mode
    cps #Mode_ABT
    str r14, [r1]
    add r1, r1, #8
    str r13, [r1]
    add r1, r1, #8
	
    ; Store r14 and r13 for UND Mode
    cps #Mode_UND
    str r14, [r1]
    add r1, r1, #8
    str r13, [r1]
    add r1, r1, #8

    ; Store r8 - r14 for FIQ Mode
    cps #Mode_FIQ
    str r8, [r1]
    add r1, r1, #8
    str r9, [r1]
    add r1, r1, #8
    str r10, [r1]
    add r1, r1, #8
    str r11, [r1]
    add r1, r1, #8
    str r12, [r1]
    add r1, r1, #8
    str r13, [r1]
    add r1, r1, #8
    str r14, [r1]
    add r1, r1, #8

    ; Skip the next entry in the struct.  This is PC and will be filled in later
    add r1, r1, #8
    ; As CPSR doesn't get retained after a secure wdog bite /abnormal reset
    ; we dont need to update this cpsr field. cpsr field comes into significance
    ; in case of non secure wdog bitewhere TZ updates this field
    add r1, r1, #8

    ; Store r13 and r14 for MON Mode
    cps #Mode_MON
    str r13, [r1]
    add r1, r1, #8
    str r14, [r1]
    add r1, r1, #8

    ; HYP mode only exists in NS world.
    ; Skipping collection of r14 HYP for now.
    add r1, r1, #8
		
    ; Switch back to SVC Mode before continuing
    cps #Mode_SVC

    ; Save r0 and r1 that was stored by PBL
    ; r0 was stored to the Process id register
    MRC p15, 0, r2, c13, c0, 2
    ; r1 was stored to TPIDRPRW
    MRC p15, 0, r3, c13, c0, 4
    ; Reset structure pointer back to beginning 
    mov r1, r0
    str r2, [r1]
    add r1, r1, #8
    str r3, [r1]
	
    ; Set the stack pointer
    ldr r13, =sys_debug_cpu_stack_top

    ; We have saved all the registers for this CPU.
    ; Now we can branch to the main loop passing the ctxt structure address
    ; We will not return
    ldr r5,  =sys_debug_main_ctl
    blx r5

    ; For safety	
    bl loophere  ; never returns, keep lr in r14 for debug	

	
loophere
    bx r14

	
;=======================================================================
;
;		Data for the module 
;
;=======================================================================
    AREA SYSTEM_DEBUG_DATA , DATA, READWRITE
; Allocate memory for sys_debug_cpu_ctxt_type
; Round up big so if structure changes it does not overflow
sys_debug_cpu_ctxt_base      SPACE 0x200

; Alocate memory in RW data area for the stack 
sys_debug_cpu_stack_bottom   SPACE 0x110
sys_debug_cpu_stack_top      SPACE 0x4

   
   END
