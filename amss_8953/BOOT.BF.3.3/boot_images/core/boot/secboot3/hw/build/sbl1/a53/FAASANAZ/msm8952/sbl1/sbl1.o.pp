#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1.S"
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                              SBL1
;
; GENERAL DESCRIPTION
;   This file bootstraps the processor. The Start-up Primary Bootloader
;   (SBL1) performs the following functions:
;
;      - Set up the hardware to continue boot process.
;      - Initialize DDR memory
;      - Load Trust-Zone OS
;      - Load RPM firmware
;      - Load APPSBL and continue boot process
;
;   The SBL1 is written to perform the above functions with optimal speed.
;   It also attempts to minimize the execution time and hence reduce boot time.
;
; Copyright 2014-2015 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header: 
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 02/27/15   aus     Fixed to check for correct abort mode
; 07/14/14   lm      Added sbl1_external_abort_enable funtion
; 07/01/14   sk      Added sbl_save_regs function
; 05/01/14   ck      Added logic to assign stacks based on processor mode
; 03/19/14   ck      Fixed stack base issue.  Now using proper address which is "Limit" of SBL1_STACK 
; 03/07/14   ck      Removed -4 logic from check_for_nesting as bear SBL has its own vector table
; 03/03/14   ck      Updated vector table with branches as VBAR is being used in Bear family 
; 11/15/12   dh      Add boot_read_l2esr for 8974
; 08/31/12   dh      Correct the stack base in check_for_nesting, remove unused code
; 07/16/12   dh      Remove watchdog reset code
; 02/06/12   dh      Update start up code
; 01/31/12   dh      Initial revision for 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"














 

















 






 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"




 






















 


























 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/hwio/msm8937/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 58 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 85 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 277 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 309 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 459 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 473 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 503 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 631 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 768 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 782 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 796 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1008 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1036 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1334 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1456 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1500 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1532 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1660 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1744 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1758 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1772 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 1996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2156 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2184 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2198 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2292 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2352 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2366 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2418 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2490 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2506 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2522 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2564 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2580 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2682 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2718 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2732 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2820 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2878 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2928 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 2990 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3166 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3270 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3398 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3412 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3456 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3484 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3698 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3712 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3726 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3796 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3822 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3960 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3974 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 3990 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4078 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4176 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4208 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4236 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4308 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4368 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4398 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4440 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4458 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4472 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4486 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4500 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4514 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4530 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4546 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4590 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4604 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4618 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4632 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4678 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4704 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4722 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4736 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4764 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4778 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4836 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4910 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4920 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4978 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 4994 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5030 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5138 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5210 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5254 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5270 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5342 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5370 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5446 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5488 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5502 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5518 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5560 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5606 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5620 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5650 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5666 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5692 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5782 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5798 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5842 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5884 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5938 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 5990 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6074 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6108 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6284 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6322 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6392 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6408 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6458 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6472 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6518 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6560 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6576 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6612 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6662 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6706 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6726 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6742 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6768 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6782 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6802 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6818 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6834 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6850 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6914 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6930 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6944 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 6980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7188 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7260 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7280 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7316 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7352 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7390 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7418 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7490 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7526 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7600 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7614 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7628 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7684 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7716 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7732 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7752 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7768 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7788 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7804 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7840 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7876 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7938 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 7996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8012 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8132 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8146 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8176 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8236 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8312 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8342 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8356 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8478 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8502 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8526 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8550 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8574 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8626 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8650 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8674 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8712 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8726 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8770 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8800 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8828 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8842 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8856 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8884 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8940 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 8996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9046 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9168 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9326 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9412 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9462 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9522 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9600 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9708 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9844 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 9952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10060 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10146 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10348 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10434 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10448 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10462 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10476 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10540 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10562 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10576 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10590 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10604 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10630 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10704 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10756 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10774 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10788 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10802 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10832 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10876 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10890 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10934 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10960 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10978 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 10992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11040 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11068 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11140 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11154 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11322 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11344 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11358 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11560 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11576 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11624 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11656 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11682 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11742 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11756 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11800 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11870 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11916 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 11986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12000 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12170 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12242 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12258 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12278 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12344 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12408 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12434 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12450 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12492 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12508 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12540 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12592 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12622 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12638 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12674 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12704 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12754 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12770 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12800 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12876 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12958 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12972 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 12988 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13002 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13018 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13068 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13084 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13114 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13144 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13190 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13308 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13322 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13336 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13366 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13408 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13426 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13440 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13468 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13482 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13598 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13614 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13672 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13686 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13756 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13774 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13788 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13802 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13872 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13890 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13918 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13976 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 13992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14006 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14040 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14228 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14264 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14284 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14342 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14412 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14536 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14598 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14614 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14640 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14666 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14750 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14906 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 14968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15044 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15070 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15088 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15210 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15336 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15368 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 15389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 15420 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 15437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15503 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15565 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15595 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15637 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15651 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15707 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15721 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15749 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15763 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15805 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15819 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15861 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 15987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16001 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16065 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16075 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16141 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16225 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 16323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16385 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16789 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16861 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 16927 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17177 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17279 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17425 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17495 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17551 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17725 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17768 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17825 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17854 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 17987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18001 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18111 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18304 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18316 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18375 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18476 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18488 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18536 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18546 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18763 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18971 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 18995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19201 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19417 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19468 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19479 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19733 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19759 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19818 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19849 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19880 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 19999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20049 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20069 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20089 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20109 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20180 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20396 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20426 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20620 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20683 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20743 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20781 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 20993 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 




#line 21314 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21360 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21374 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21390 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21420 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21436 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21450 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21480 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21524 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21538 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21552 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21566 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21582 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21612 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21678 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21746 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21790 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21810 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21834 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21914 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 21984 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22246 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22311 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22325 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22353 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22517 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22611 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22645 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22683 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22815 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 22995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23017 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23033 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23187 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23217 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23369 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23411 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23425 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23488 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23509 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23558 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23616 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23658 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23716 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23732 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23772 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23860 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23904 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23976 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 23996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24030 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24040 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24052 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24090 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24108 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24116 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24126 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24140 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24180 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24204 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24214 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24228 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24272 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24282 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24292 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24320 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24360 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24374 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24462 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 





#line 24491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24575 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24617 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24638 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24659 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24722 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24743 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24764 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24785 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24806 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24890 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24974 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 24991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25008 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25154 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25226 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25256 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25286 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25312 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25353 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25426 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25490 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"



 





#line 25507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25539 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 25573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/msmhwioreg.h"

#line 42 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"




 









 









  



 







 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 







 









  



 




 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/dal/HALcomdef.h"





























 




 
#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/dal/HALcomdef.h"



#line 34 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"





 




  







 









 








 





  













 
























 















 





























 


















 







































 


































 
#line 265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"
 











 


 



   


 





 
#line 330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"

#line 355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"








 






 
#line 377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"


#line 406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/HALhwio.h"

 



#line 39 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"





 










 


 






 









#line 91 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"






 
#line 130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"









 


























 
#line 174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/systemdrivers/msmhwio.h"


 



#line 43 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"













 






















 





 
#line 203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"






 
















 
#line 233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"


#line 44 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src/boot_msm.h"





 

 




 



 




 



 





 





























#line 53 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1.S"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/boot_target.h"















 



































 





 




#line 1 "./custfaasanaza.h"







 

#line 1 "./targfaasanaza.h"







 

#line 163 "./targfaasanaza.h"




#line 12 "./custfaasanaza.h"


#line 20 "./custfaasanaza.h"




#line 64 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/boot_target.h"



 





 




#line 105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/boot_target.h"






 






  


 






 






 





 









 




 







 









 






 


  




 





 





 




 




 




 





 




 




 




 




 




 




                                 


                                 
                                 


                                 
                               


                                 
                               


                                 
 


                                 
 


                             
                                 



 






 





                             


                             
#line 303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/boot_target.h"




  
 






 



                              


 



   


 


 




 


 


#line 54 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/msm8952/sbl1/sbl1.S"

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_SVC                EQU    0x13
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1b
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40
A_Bit					EQU    0x100

;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT |Image$$SBL1_SVC_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_UND_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_ABT_STACK$$ZI$$Limit|
    IMPORT boot_undefined_instruction_c_handler
    IMPORT boot_swi_c_handler
    IMPORT boot_prefetch_abort_c_handler
    IMPORT boot_data_abort_c_handler
    IMPORT boot_reserved_c_handler
    IMPORT boot_irq_c_handler
    IMPORT boot_fiq_c_handler
    IMPORT boot_nested_exception_c_handler
    IMPORT sbl1_main_ctl
    IMPORT boot_crash_dump_regs_ptr

;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sbl_loop_here
    EXPORT boot_read_l2esr
   
    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT   __main
    EXPORT   _main
    EXPORT  sbl_save_regs
    EXPORT  sbl1_external_abort_enable
;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

;
; Area that defines the register data structure
;
    AREA    SAVE_REGS, DATA

arm_core_t RN     r7 
           MAP    0,arm_core_t

svc_r0     FIELD  4
svc_r1     FIELD  4
svc_r2     FIELD  4
svc_r3     FIELD  4
svc_r4     FIELD  4
svc_r5     FIELD  4
svc_r6     FIELD  4
svc_r7     FIELD  4
svc_r8     FIELD  4
svc_r9     FIELD  4
svc_r10    FIELD  4
svc_r11    FIELD  4
svc_r12    FIELD  4
svc_sp     FIELD  4
svc_lr     FIELD  4
svc_spsr   FIELD  4
svc_pc     FIELD  4
cpsr       FIELD  4
sys_sp     FIELD  4
sys_lr     FIELD  4
irq_sp     FIELD  4
irq_lr     FIELD  4
irq_spsr   FIELD  4
abt_sp     FIELD  4
abt_lr     FIELD  4
abt_spsr   FIELD  4
udf_sp     FIELD  4
udf_lr     FIELD  4
udf_spsr   FIELD  4
fiq_r8     FIELD  4
fiq_r9     FIELD  4
fiq_r10    FIELD  4
fiq_r11    FIELD  4
fiq_r12    FIELD  4
fiq_sp     FIELD  4
fiq_lr     FIELD  4
fiq_spsr   FIELD  4

    PRESERVE8
    AREA    SBL1_VECTORS, CODE, READONLY, ALIGN=4
    CODE32
unused_reset_vector
    B     0x00000000
undefined_instruction_vector
    B     sbl1_undefined_instruction_nested_handler
swi_vector
    B     boot_swi_c_handler
prefetch_abort_vector
    B     sbl1_prefetch_abort_nested_handler
data_abort_vector
    B     sbl1_data_abort_nested_handler
reserved_vector
    B     boot_reserved_c_handler
irq_vector
    B     boot_irq_c_handler
fiq_vector
    B     boot_fiq_c_handler


;============================================================================
; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
;============================================================================

    AREA  SBL1_ENTRY, CODE, READONLY, ALIGN=4
    CODE32
    ENTRY
    
__main
_main

;============================================================================
;   We contiue to disable interrupt and watch dog until we jump to SBL3
;============================================================================
sbl1_entry

    ;Change to Supervisor Mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Save the passing parameter from PBL
    mov     r7, r0

    ; Set VBAR (Vector Base Address Register) to SBL vector table
    ldr     r0, =0x08005000
    MCR     p15, 0, r0, c12, c0, 0
	
    ; Setup the supervisor mode stack
    ldr     r0, =|Image$$SBL1_SVC_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to IRQ
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0

    ; Switch to FIQ
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0	
	
    ; Switch to undefined mode and setup the undefined mode stack
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_UND_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to abort mode and setup the abort mode stack
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Return to supervisor mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Restore the passing parameter
    mov     r0, r7
    
    ; ------------------------------------------------------------------
    ; Call functions external to perform SBL1 function.
    ; It should never return.
    ; ------------------------------------------------------------------
    ldr    r5, =sbl1_main_ctl
    blx    r5

    ; For safety
    bl loophere  ; never returns, keep lr in r14 for debug


;============================================================================
; sbl_save_regs
;
; PROTOTYPE
;   void sbl_save_regs();
;
; ARGS
;   None
;
; DESCRIPTION
;   Configure VBAR, vector table base register.
;   
;============================================================================    
sbl_save_regs	
    ; Save CPSR
    stmfd   sp!, {r6}
    mrs     r6, CPSR

    stmfd   sp!, {r7}

    ; Switch to SVC mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Capture User Mode r0-r14 (no SPSR)
    ; Registers are stored in svc structure for backwards compatibility
    ldr     arm_core_t,=boot_crash_dump_regs_ptr
    ldr     arm_core_t, [r7]
    str     r0,  svc_r0
    str     r1,  svc_r1
    str     r2,  svc_r2
    str     r3,  svc_r3
    str     r4,  svc_r4
    str     r5,  svc_r5
    ; Store r6 later after restoring it from the stack
    ; Store r7 later after restoring it from the stack
    str     r8,  svc_r8
    str     r9,  svc_r9
    str     r10, svc_r10
    str     r11, svc_r11    
    str     r12, svc_r12
    str     r14, svc_lr

    ; Store SP value
    str     sp, svc_sp

    ; Store SPSR
    mrs     r0, SPSR
    str     r0, svc_spsr

    ; Store the PC for restoration later
    ldr     r0, =sbl_save_regs
    str     r0, svc_pc

    ; Save SYS mode registers
    msr     CPSR_c, #Mode_SYS:OR:I_Bit:OR:F_Bit
    str     r13, sys_sp
    str     r14, sys_lr

    ; Save IRQ mode registers
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    str     r13, irq_sp
    str     r14, irq_lr
    mrs     r0, SPSR
    str     r0, irq_spsr    

    ; Save ABT mode registers
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    str     r13, abt_sp
    str     r14, abt_lr
    mrs     r0, SPSR
    str     r0, abt_spsr  

    ; Save UND mode registers
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    str     r13, udf_sp
    str     r14, udf_lr
    mrs     r0, SPSR
    str     r0, udf_spsr  

    ; Save FIQ mode registers
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    str     r8,  fiq_r8
    str     r9,  fiq_r9
    str     r10, fiq_r10
    str     r11, fiq_r11    
    str     r12, fiq_r12    
    str     r13, fiq_sp
    str     r14, fiq_lr
    mrs     r0, SPSR
    str     r0, fiq_spsr  

    ; Switch back to original mode using r6 which holds the cpsr
    msr     CPSR_c, r6

    ; Store CPSR
    str     r6, cpsr
    
    ; Restore r7 value 
    ldmfd   sp!, {r2}
    str     r2, svc_r7

    ; Restore r6 value
    ldmfd   sp!, {r1}
    str     r1, svc_r6
    mov     r6, r1
    mov     r7, r2	

    ; Finished so return    
    bx lr

;======================================================================
; Called by sbl1_error_handler only. We clean up the registers and loop
; here until JTAG is connected.
;======================================================================
sbl_loop_here
    mov r0,#0
    mov r1,#0
    mov r2,#0
    mov r3,#0
    mov r4,#0
    mov r5,#0
    mov r6,#0
    mov r7,#0
    mov r8,#0
    mov r9,#0
    mov r10,#0
    mov r11,#0
    mov r12,#0
loophere
    b loophere


;======================================================================
; SBL1 exception handlers that can have nested calls to them.  These
; handlers check for nesting and if it is the first exception they
; call a "C" exception handler that calls the SBL1 error handler.
; If it is a nested exception, the "C" exception handler is not
; re-entered and the JTAG interface is enabled immediately. Nesting
; is only a concern for undefined instruction and abort exceptions.
; Note, a separate exception handler is used for each exception to
; provide additional debug information (see sbl1_error_handler.c for
; more information).
;======================================================================

	
sbl1_undefined_instruction_nested_handler
    ldr r5,=boot_undefined_instruction_c_handler
    b   check_for_nesting

sbl1_prefetch_abort_nested_handler
    ldr r5,=boot_prefetch_abort_c_handler
    b   check_for_nesting

sbl1_data_abort_nested_handler
    ldr r5,=boot_data_abort_c_handler
    b   check_for_nesting

;======================================================================
; Checks for nested exceptions and then calls the "C" exception
; handler pointed to by R5 if this is the first time this exception
; has occurred, otherwise it calls the "C" nested exception handler
; that just enables JTAG debug access.  The mode stack pointer is used
; to determine if a nested exception or a second abort exception has
; occurred.  This is accomplished by comparing the mode stack pointer
; to the top of the stack that was initially assigned to the stack.
; If they are equal, it is a first time exception.
;======================================================================
check_for_nesting

    ; Initial stack base depends on the current processor mode
    ; Mode will either be ABT or UND.  Load proper stack base.
    mrs r7, cpsr
    and r7, r7, #Mode_SYS ; Use Mode_SYS for mode bitmask as all bits are high
    cmp r7, #Mode_ABT
    ldreq r6,=|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    cmp r7, #Mode_UND
    ldreq r6,=|Image$$SBL1_UND_STACK$$ZI$$Limit|

    mov r7, r13                            ; Save current stack ptr
    cmp r6, r7                             ; Compare initial and actual
    blxeq r5                               ; First time exception
    ldr r5,=boot_nested_exception_c_handler; This is a nested exception
    blx r5

boot_read_l2esr
    mov r1, #0x204    ;Indirect address of L2ESR
    isb
    mcr p15,3,r1,c15,c0,6 ;Write the L2CPUCPSELR with the indirect address of the L2ESR
    isb
    mrc p15,3,r0,c15,c0,7 ;store L2ESR to r0
    isb
    bx lr
    
; void sbl1_external_abort_enable(uint32 flags)
sbl1_external_abort_enable FUNCTION
    and     r0, r0, #F_Bit:OR:I_Bit:OR:A_Bit 	; Only care about A/I/F bits.
    mrs     r1, cpsr                            ; Read the status register.
    bic     r1, r1, r0                          ; Clear requested A/I/F bits
    msr     cpsr_cx, r1                         ; Write control & extension field
    bx      lr
    ENDFUNC	
    END
