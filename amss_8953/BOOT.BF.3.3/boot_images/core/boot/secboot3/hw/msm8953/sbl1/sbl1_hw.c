/*=============================================================================

                         SBL1 Hardware Initialization

GENERAL DESCRIPTION
  This file does basic hardware initialization at power up.

Copyright 2014-2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/hw/msm8953/sbl1/sbl1_hw.c#8 $

when       who       what, where, why
--------   ---       ----------------------------------------------------------
11/09/15   shrey     Put HS USB PHY into Non-Drive mode to not interfere with charger detection by PMI
09/16/15   aus       After DDR training set reset_required field for QSEE if reset from QSEE is enabled
07/30/15   sds       Call boot_ICB_Config_PostInit() after training.
05/05/15   sk        Added DDR sanity test feature
04/01/15   aus       Initialize DDR heap before device programmer execution
02/25/15   tj        Disable PMIC init for Device Programmer DDR
03/18/2015 lm        Added boot_pm_rtc_get_time API
11/21/14   sk        Changes for creating two elfs for RUMI and ASIC
09/23/14   mvanim    Added pmic api call after pmic init to dump PMIC_RTC status
07/24/14   sk        Added sbl1_save_ddr_training_data function to save DDR trained data into EMMC
06/20/14   lm        Fixed dead/weak battery charging by moving boot_pm_driver_init()
					 after ddr_init and cleaned the code.
06/18/14   SK        Moved boot_ddr_test func out of condition check.
06/06/14   lm        Moved boot_pm_init_driver func before calling boot_pre_ddr_clock_init call
05/20/14   lm        Call boot_railway_init() for DDR training
03/21/14   ck        Added logic to save reset_status_register to imem cookies before clearing
03/18/14   ck        Updated boot_hw_reset calls as they now take a reset type parameter
03/14/14   ck        Moved boot_busywait_init to sbl1_main_ctl as it needs to be done before boot logger init
03/03/14   ck        Removing SpmiInit and SpmiCfg from sbl1_hw_init per SPMI team as PMIC driver does this now
02/18/14   ck        Added logic to sbl1_hw_init to clear PMIC_ABNORMAL_RESIN_STATUS
10/17/13   ck        Temp zeroing of ddr training memory location until ddr team delivers
08/07/13   sr        Removed boot_ddr_debug call .
07/26/13   rj        Calling i2C init to fix eeprom init issue
07/11/13   rj        Calling boot_pre_ddr_clock_init after CDT configuration is 
                     done to dynamically detect ddr and setup the clock plan
06/18/13   sl        Call boot_ddr_debug()
06/06/13   dh        Backup gcc reset status then clear it
06/14/13   dh        Only load ddr training data if sbl is not in dload mode
05/21/13   yp        Turn on vibrator when device power up in normal mode.
04/04/13   dh        Move boot_DALSYS_InitMod to early sbl1_main_ctl
04/02/13   dh        Use boot_dload_is_dload_mode_set instead of uefi dump cookie
                     to determin if device is in dload mode
04/11/13   sl        Relocate boot_ddr_test() call
04/03/13   sl        Call Clock API to get DDR speed
03/21/13   dh        Add memory barrier in boot_ddr_post_init
03/07/12   jz        Cleanup logging
12/13/12   jz        Change clock frequency back to MAX after DDR training is done
12/04/12   dh        Move boot_Tsens_Init to sbl1_hw_init
11/26/12   dh        Skip DDR training if UEFI dload cookie is set
11/12/12   sl        Enable DDR Test Framework
11/05/12   dh        Move thermal management code to common file
10/09/12   dh        Add boot_ICB_Segments_Init right after ddr init to configure
                     bimc slave address range
                     Add boot_SpmiCfg_Init before boot_SpmiBus_Init
10/09/12   dh        Only enable fast debug feature if FEATURE_BOOT_FAST_DEBUG 
                     is defined
09/26/12   jz        Compile out boot_debug_mode_enter for Virtio testing
09/25/12   dh        Added pm_driver_init to allow PMIC API usage and pm_oem_init
	  	  	         to allow customer to call PMIC API in boot for
	  	             their desirable PMIC configurations.
08/30/12   dh        Add boot log for clock and pmic functions 
08/16/12   AJC       Added boot_debug_mode_enter for FASTDEBUG feature
08/01/12   dh        Add sbl1_hw_deinit
07/23/12   dh        Add sbl1_set_ddr_training_data and sbl1_wait_for_ddr_training
07/16/12   dh        Move spmi and pmic init to sbl1_hw_init
06/18/12   dh        Switch to boot external driver api for tsensor and bus API
06/11/12   dh        Add sbl1_check_device_temp, check temp at the beginning of
                     sbl1_hw_init
06/08/12   dh        Add ICB_Config_Init in sbl1_hw_init
05/29/12   dh        Move boot_clock_init to sbl1_hw_init_secondary
                     Add boot_busywait_init in sbl1_hw_init
                     Remove gpio init from sbl1_hw_init_secondary since it must be 
                     called after smem is available.
                     Rename sbl1_clk_regime_ram_dump_init to sbl1_hw_dload_init
05/08/12   dh        Add clock init api
10/18/11   dh        Initial revision
=============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_comdef.h"
#include "boot_sbl_if.h"
#include "sbl1_hw.h"
#include "sbl1_mc.h"
#include "boothw_target.h"
#include "boot_dload.h"
#include "boot_logger.h"
#include "boot_config_data.h"
#include "boot_util.h"
#include "boot_thermal_management.h"
#include "boot_extern_clk_interface.h"
#include "boot_extern_ddr_interface.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_dal_interface.h"
#include "boot_extern_tlmm_interface.h"
#include "boot_extern_bus_interface.h"
#include "boot_extern_tsensor_interface.h"
#include "boot_extern_power_interface.h"
#include "boot_extern_usb_interface.h"
#include "boot_shared_imem_cookie.h"
#include "string.h"
#include "boot_cache_mmu.h"
#include "msmhwioreg.h"
#include "boot_target.h"
#include "boot_extern_bam_interface.h"
#include "ddr_drivers.h"

#include "tct.h"
#include "DDITlmm.h"
#include "pm_pon.h"
#include "boot_extern_busywait_interface.h"
#include "boot_flash_dev_if.h"
#include "boot_extern_bam_interface.h"
#include "boot_extern_vsense_interface.h"
#include "boot_sdcc.h"
#include <stdio.h>

/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/

/* backed up GCC_RESET_STATUS for ramdump */
static uint32 reset_status_register = 0;
/* backed up PMIC timer status for ramdump */
static uint32 pmic_rtc_value= 0;

/* For initializing the heap before jump to device programmer */
extern byte boot_external_heap[BOOT_EXTERNAL_HEAP_SIZE];

extern uint8 traceability_partition_id[];
#ifdef FEATURE_TCTNB_FORCE_DLOAD_CABLE
/* If need reset device by qsee,don't clear force download flag in traceability*/
static boot_boolean need_reset_device_by_qsee = FALSE;
#define NPI_GPIO                   63    /*London NPI_DLOAD is GPIO 63*/
#define RESTART_REASON_ADDR_OFFSET 0x65C
#endif

/*=============================================================================

                              FUNCTION DEFINITIONS

=============================================================================*/
void sbl1_save_ddr_training_data(bl_shared_data_type *bl_shared_data)
{
  uint32 ddr_params_training_data_size = 0;
  void* ddr_training_data_ptr = NULL;

    memory_barrier();
        
    ddr_training_data_ptr = 
      boot_ddr_params_get_training_data(&ddr_params_training_data_size);
        
    BL_VERIFY((ddr_training_data_ptr != NULL) &&
                     (ddr_params_training_data_size != 0),
                     BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT);
        
    /*Save the updated training data to storage device */
    sbl1_save_ddr_training_data_to_partition(ddr_training_data_ptr, 
                                                                   ddr_params_training_data_size);
                                                
    if(RESET_FROM_QSEE_ENABLED)
    {                                         
       /* After training data is saved, instruct QSEE to perform a reset */
       bl_shared_data->sbl_qsee_interface.reset_required = (uint32) RESET_DEVICE_BY_QSEE;
#ifdef FEATURE_TCTNB_FORCE_DLOAD_CABLE
       need_reset_device_by_qsee = TRUE;
#endif
    }
    else
    {
       /* Do a reset after training data is saved */
       boot_hw_reset(BOOT_HARD_RESET_TYPE);
    }
}

/*===========================================================================

**  Function :  sbl1_ddr_init

** ==========================================================================
*/
/*!
* 
* @brief
*   Initialize DDR device.
* * 
* @par Dependencies 
*  Following Api's need to be called first:
*    sbl1_hw_init : To have ddr clocks setup.
*    sbl1_ddr_set_params : To have DDR config parameters setup.
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_ddr_init(bl_shared_data_type *bl_shared_data)
{
  ddr_info sbl1_ddr_info;
  
#ifndef FEATURE_RUMI_BOOT
  pm_err_flag_type pm_driver_init,sbl_chg_init;
  boolean ddr_save_training_required = FALSE;
#endif
  icbcfg_error_type icb_config_post_init;
  //icbcfg_remap_info_type remap1 = {0x0, 0x20000000, 0x60000000, ICBCFG_REMAP_INTERLEAVE_DEFAULT, FALSE};
  //icbcfg_remap_info_type remap2 = {0x20000000, 0x20000000, 0xE0000000, ICBCFG_REMAP_INTERLEAVE_DEFAULT, FALSE};
  /* Initialize DDR */
#ifdef FEATURE_RUMI_BOOT
  boot_ddr_initialize_device(19200);
#else
  boot_ddr_initialize_device(boot_clock_get_ddr_speed());
#endif

  boot_ddr_post_init();

  /* Configure DDR slave segments address range */
  sbl1_ddr_info = boot_ddr_get_info();
  boot_ICB_Segments_Init( "/dev/icbcfg/boot", &sbl1_ddr_info);
	 
  #if 0
  /* Incase of Single channel + dual rank + 1536 MB per each rank.
  Base address to be aligned with size at BIMC and so remapping at buses to fit into memory map
  Address 0x0 is being mapped to 0x6000 0000 for a block of 0.5GB,
  Address 0x2000 0000 is being mapped to 0xE000 0000 for a block of 0.5 GB.
  */
  if (sbl1_ddr_info.ddr_size.sdram0_cs0_addr == 0x0)
  {
     if ((sbl1_ddr_info.ddr_size.sdram0_cs0 == 1536) && (sbl1_ddr_info.ddr_size.sdram0_cs1 == 1536)
             && (sbl1_ddr_info.ddr_size.sdram1_cs0 == 0) &&  (sbl1_ddr_info.ddr_size.sdram1_cs1 == 0 )) {
      boot_ICB_RemapEx("/dev/icbcfg/boot", &sbl1_ddr_info, 0, &remap1);
      boot_ICB_RemapEx("/dev/icbcfg/boot", &sbl1_ddr_info, 1, &remap2);
     }
  }
  #endif

  boot_ddr_remapper();

#ifndef FEATURE_RUMI_BOOT
  if(boot_dload_is_dload_mode_set() == FALSE)
  {
   /* perform sanity ddr test */
   boot_log_message("do ddr sanity test, Start");
   boot_log_start_timer();
   boot_ddr_test(boot_clock_get_ddr_speed());
   boot_log_stop_timer("do ddr sanity test, Delta");
  }
#endif

  /* PMIC driver must be initialized after DDR init for dead/weak battery charging
     and before ddr training as its uses PMIC API's */
   /*compile out pmic and clock api for rumi */
#if (!defined(FEATURE_RUMI_BOOT))
  /* PMIC image not loaded for Device Programmer */

  if (bl_shared_data->build_type != SBL_BUILD_DEVICEPROGRAMMER_DDR) {
    /* Initialize PMIC Driver - Allowing API usage */
  
  boot_log_message("pm_driver_init, Start");
  boot_log_start_timer(); 
  
  /* Initialize PMIC Driver - Allowing API usage */
  BL_VERIFY((pm_driver_init=boot_pm_driver_init()) == PM_ERR_FLAG__SUCCESS, (uint16)pm_driver_init|BL_ERROR_GROUP_PMIC);
  BL_VERIFY(((sbl_chg_init=boot_pm_sbl_chg_init()) == PM_ERR_FLAG__SUCCESS),(uint16)sbl_chg_init|BL_ERROR_GROUP_PMIC);
  
  boot_log_stop_timer("pm_driver_init, Delta"); 

  }  
#endif /* FEATURE_RUMI_BOOT*/

#ifndef FEATURE_RUMI_BOOT
  if (boot_ddr_params_is_training_on_rpm_required() == FALSE &&
      boot_dload_is_dload_mode_set() == FALSE)
  {
    ddr_save_training_required = boot_ddr_do_ddr_training();    
	if ((boot_pbl_get_flash_type() != NO_FLASH) && ddr_save_training_required ) {
	 sbl1_save_ddr_training_data(bl_shared_data); 
	}
    boot_ddr_test(boot_clock_get_ddr_speed());
  }
#endif

  if (bl_shared_data->build_type == SBL_BUILD_DEVICEPROGRAMMER_DDR) {
  /* Add the DDR heap to DAL */
  boot_DALSYS_HeapInit(boot_external_heap, BOOT_EXTERNAL_HEAP_SIZE, TRUE);
  }

  /*ICB post config api will configure any settings done after DDR/memmap is finalized. */
  BL_VERIFY((icb_config_post_init=boot_ICB_Config_PostInit("/dev/icbcfg/boot")) == ICBCFG_SUCCESS,(uint16)icb_config_post_init|BL_ERROR_GROUP_ICB);
 
} /* sbl1_ddr_init() */

#ifndef BOOT_PRE_SILICON
#ifdef  FEATURE_BOOT_FAST_DEBUG
/*===========================================================================

**  Function :  sbl1_enter_fastdebug

** ==========================================================================
*/
/*!
* 
* @brief
*   Enters debug mode if PMIC GPIO key is pressed while booting up
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
static volatile boolean enter_fastdebug = FALSE;
static void  boot_debug_mode_enter (void)
{
  
	uint8 data;
	uint32 addr, readlen;
	
	// On 8974, GPIO4 is used for the focus key
	// Use a pull up value of 30uA and a voltage source of VIN0

    data = 0x2;
	addr = 0xC341;
	SpmiBus_Write = boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1);
	BL_VERIFY((SPMI_BUS_SUCCESS == SpmiBus_Write ), (uint16)SpmiBus_Write|BL_ERROR_GROUP_SPMI);

  	data = 0x0;
    addr = 0xC340;
	SpmiBus_Write = boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1);
	BL_VERIFY((SPMI_BUS_SUCCESS == SpmiBus_Write ), (uint16)SpmiBus_Write|BL_ERROR_GROUP_SPMI);


	// Slave & Priority  = 0
	data = 0x0;
	addr = 0xC342;
	SpmiBus_Write = boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1);
	BL_VERIFY((SPMI_BUS_SUCCESS == SpmiBus_Write ), (uint16)SpmiBus_Write|BL_ERROR_GROUP_SPMI);

	// Then enable the GPIO
	data = 0x80;
	addr = 0xC346;
	SpmiBus_Write = boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1);
	BL_VERIFY((SPMI_BUS_SUCCESS == SpmiBus_Write ), (uint16)SpmiBus_Write|BL_ERROR_GROUP_SPMI);

	// Now poll the value
	addr = 0xC308;
	if (SPMI_BUS_SUCCESS == boot_SpmiBus_ReadLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1, &readlen))
	{
		// If GPIO is pressed, bit 0 will be 0 (active low)
		if (!(data & 0x1))
		{
			enter_fastdebug = TRUE;
			while(enter_fastdebug);
		}
	}
}
#endif
#endif

	
/*===========================================================================

**  Function :  sbl1_hw_init

** ==========================================================================
*/
/*!
* 
* @brief
*   This function performs hardware initialization.
* 
*   Only common hardware that is needed for flash
*   initialization is initialized here.
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_hw_init(bl_shared_data_type *bl_shared_data)
{  
  TsensResultType tsens_init;
#ifndef FEATURE_RUMI_BOOT
  pm_err_flag_type pm_device_init;
#endif
#ifndef BOOT_PRE_SILICON
#ifdef FEATURE_BOOT_FAST_DEBUG
  /* Check if we are going to enter debug mode */
  boot_debug_mode_enter();
#endif
#endif
  /* Initialize BAM interface */
  boot_bam_drv_init();

  /* Initialize temperature sensor */
  BL_VERIFY((tsens_init=boot_Tsens_Init()) == TSENS_SUCCESS,(uint16)tsens_init|BL_ERROR_GROUP_TSENS);
  
  
  
#if (!defined(FEATURE_RUMI_BOOT))
  /* Calling here to ensure eeprom init goes fine for CDT read */
  boot_pre_i2c_clock_init();
  
  if (bl_shared_data->build_type != SBL_BUILD_DEVICEPROGRAMMER_DDR) {
  boot_log_message("pm_device_init, Start");
  boot_log_start_timer();
  
  /* Initialize the pmic */
  BL_VERIFY((pm_device_init=boot_pm_device_init()) == PM_ERR_FLAG__SUCCESS, (uint16)pm_device_init|BL_ERROR_GROUP_PMIC);  
  
  boot_log_stop_timer("pm_device_init, Delta");
  }

  /* Only execute for the default path
   ** Putting the HS USB PHY into NONDRIVE mode to not interfere with the autonomous charger 
   ** detection by the PMI HW 
   */
  boot_usb_al_hs_phy_nondrive_mode_set();


#endif /*FEATURE_RUMI_BOOT*/

/* Check the temperature */
  boot_check_device_temp();

  /* There is a HW bug in 8916 where PMIC_ABNORMAL_RESIN_STATUS is set accidently.
     Just clear the bit regardless. */
  HWIO_GCC_RESET_STATUS_OUTM(HWIO_GCC_RESET_STATUS_PMIC_ABNORMAL_RESIN_STATUS_BMSK,
                             0);

} /* sbl1_hw_init() */


/*===========================================================================

**  Function :  sbl1_hw_deinit

** ==========================================================================
*/
/*!
* 
* @brief
*   This function deinit the hardware that's not needed beyond sbl1
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_hw_deinit()
{

  /* Call clock exit boot to disable unneeded clock*/
  BL_VERIFY(boot_clock_exit_boot() == TRUE,BL_ERR_DISABLE_CLOCK_FAIL|BL_ERROR_GROUP_BOOT);
  
} /* sbl1_hw_deinit() */


/*===========================================================================

**  Function :  sbl1_hw_dload_init

** ==========================================================================
*/
/*!
* 
* @brief
*   This function initializes the necessary clocks for dload
*   We only do so when auth is disabled 
* 
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_hw_dload_init(bl_shared_data_type *bl_shared_data)
{
  boot_dload_clock_init();
}


/*===========================================================================

**  Function :  sbl1_hw_init_secondary

** ==========================================================================
*/
/*!
* 
* @brief
*   This function performs secondary hardware initialization.
* 
*   DDR is initialized here. This needs to be done after trustzone is loaded.
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_hw_init_secondary( bl_shared_data_type *bl_shared_data )
{ 

  /*compile out pmic and clock api for rumi */
#if (!defined(FEATURE_RUMI_BOOT))
    pm_err_flag_type pm_vib_on;
  /* First save gcc reset status to shared IMEM, then clear it */
  reset_status_register = HWIO_IN(GCC_RESET_STATUS);

  if (boot_shared_imem_cookie_ptr)
  {
    boot_shared_imem_cookie_ptr->reset_status_register = reset_status_register;
  }

  HWIO_OUT(GCC_RESET_STATUS,0);

  if(!boot_dload_is_dload_mode_set())
  {
  	/*turn on vibrator*/
 	 BL_VERIFY((pm_vib_on=boot_pm_vib_on())== PM_ERR_FLAG__SUCCESS,(uint16)pm_vib_on|BL_ERROR_GROUP_PMIC);
  }

  boot_log_message("clock_init, Start");
  boot_log_start_timer();  
  
  BL_VERIFY((boot_clock_init() == TRUE), BL_ERR_CPU_CLK_INIT_FAIL|BL_ERROR_GROUP_BOOT);
  
  boot_log_stop_timer("clock_init, Delta");
  
  boot_pm_rtc_get_time(0,&pmic_rtc_value); 
  
#endif /*FEATURE_RUMI_BOOT*/
   
} /* sbl1_hw_init_secondary() */


/*===========================================================================

**  Function :  sbl1_hw_get_reset_status

** ==========================================================================
*/
/*!
* 
* @brief
*   This function returns the value of reset status register saved from this boot
* 
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
uint32 sbl1_hw_get_reset_status( void )
{
   return reset_status_register;
}  /* sbl1_hw_get_reset_status */

/*===========================================================================

**  Function :  sbl1_hw_get_pmictimer_status

** ==========================================================================
*/
/*!
* 
* @brief
*   This function returns the value of pmic timer status register saved from this boot
* 
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
uint32 sbl1_hw_get_pmictimer_status( void )
{
   return pmic_rtc_value;
}  /* sbl1_hw_get_pmictimer_status */


/*===========================================================================

**  Function :  sbl1_ddr_set_params

** ==========================================================================
*/
/*!
* 
* @brief
*   Set DDR configuration parameters
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* @par Dependencies
*  	This Api needs to be called :
*  Before: sbl1_ddr_init
*  After: boot_config_data_table_init
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_ddr_set_params(  bl_shared_data_type *bl_shared_data  )
{
  uint32 ddr_cdt_params_size = 0;
  struct cdt_info *config_data_table_info;
  uint8 *ddr_cdt_params_ptr;
  ddr_info ddr_type_info;
  icbcfg_error_type icb_config_Init;
  boot_log_message("sbl1_ddr_set_params, Start");
  boot_log_start_timer();  
  
  /* Extract ddr data block from configuration data table(CDT) */
  config_data_table_info = 
                    bl_shared_data->sbl_shared_data->config_data_table_info;
                    
  ddr_cdt_params_ptr = 
                    boot_get_config_data_block(config_data_table_info->cdt_ptr,
                                               CONFIG_DATA_BLOCK_INDEX_V1_DDR,
                                               &ddr_cdt_params_size);
  
  /* Make sure cdt param size is valid */
  BL_VERIFY((ddr_cdt_params_size <= CONFIG_DATA_TABLE_MAX_SIZE) &&
            (ddr_cdt_params_ptr != NULL),  BL_ERR_CDT_PARAMS_SIZE_INVALID|BL_ERROR_GROUP_BOOT );

  /* Copy CDT DDR paramters to shared IMEM */
  qmemcpy((void*)SHARED_IMEM_BOOT_CDT_BASE,
          ddr_cdt_params_ptr,
          ddr_cdt_params_size);
  
  /* Pass cdt parameter to DDR driver */
  BL_VERIFY(boot_ddr_set_params((void*)SHARED_IMEM_BOOT_CDT_BASE, ddr_cdt_params_size) == TRUE, BL_ERR_DDR_SET_PARAMS_FAIL|BL_ERROR_GROUP_DDR);
  
  /* Pass ddr training data to DDR driver */
  sbl1_load_ddr_training_data();

  /* Get the ddr type by reading the cdt */
  ddr_type_info = boot_ddr_get_info();

  /* Initialize the railway driver */
  /* NOTE: this driver API is wrapped in boot_extern_power_interface.h, 
      which needs to be enabled by turning on BOOT_EXTERN_POWER_COMPLETED in target.builds */
  boot_railway_init();
 
  boot_log_message("cpr_init, Start");
  boot_log_start_timer();

  /* Call CPR init to settle the voltages to the recommended levels*/
  boot_cpr_init();
 
  boot_log_stop_timer("cpr_init, Delta");

  boot_log_message("Pre_DDR_clock_init, Start");
  boot_log_start_timer();
  /* Initialize the pre ddr clock */ 
  BL_VERIFY((boot_pre_ddr_clock_init(ddr_type_info.ddr_type) == TRUE), BOOT_PRE_DDR_CLK_INIT_FAIL|BL_ERROR_GROUP_BOOT);
  
  boot_log_stop_timer("Pre_DDR_clock_init, Delta");

  /*ICB config api will configure address range for NOCs */
  BL_VERIFY((icb_config_Init=boot_ICB_Config_Init("/dev/icbcfg/boot")) == ICBCFG_SUCCESS, (uint16)icb_config_Init|BL_ERROR_GROUP_ICB);
  boot_log_stop_timer("sbl1_ddr_set_params, Delta");
  
} /* sbl1_ddr_set_params() */



/*===========================================================================

**  Function :  sbl1_load_ddr_training_data

** ==========================================================================
*/
/*!
* 
* @brief
*   If ddr training is required, read the ddr training data from partition
*   and pass it to ddr driver.
* 
* @param[in] 
*   None 
*     
* @par Dependencies
*   Must be called before sbl1_ddr_init
*   
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_load_ddr_training_data()
{

  uint32 ddr_params_training_data_size = 0;
  boot_boolean success = FALSE;
  uint8 *ddr_training_data_ptr = (uint8 *)SCL_DDR_TRAINING_DATA_BUF_BASE;
//  uint8 *ddr_training_data_ptr = (uint8 *)0;
  
  /*Read the ddr partition if ddr training is required and not in dload mode */
  if((boot_ddr_params_is_training_required() == TRUE) &&
     (!boot_dload_is_dload_mode_set()))
  {
    /* Get the size of training data */
    boot_ddr_params_get_training_data(&ddr_params_training_data_size);
   
   /* Read the ddr training data out, we use
     SCL_DDR_TRAINING_DATA_BUF_BASE which is tz base as a temporary buffer */
    sbl1_load_ddr_training_data_from_partition(ddr_training_data_ptr,
                                               ddr_params_training_data_size);
    
    BL_VERIFY(ddr_training_data_ptr != NULL, BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT);
    
    /* Pass the training data to DDR driver */
    success = boot_ddr_params_set_training_data((void *)ddr_training_data_ptr,
                                               ddr_params_training_data_size);

    BL_VERIFY(success, BL_ERR_PASS_DDR_TRAINING_DATA_TO_DDR_DRIVER_FAIL|BL_ERROR_GROUP_DDR);
  }
}


/*===========================================================================

**  Function :  sbl1_wait_for_ddr_training

** ==========================================================================
*/
/*!
* 
* @brief
*   Wait for RPM to execute DDR training sequence. 
*   If ddr training parameter data is updated, save it to storage device.
* 
* @param[in] 
*   None 
*     
* @par Dependencies
*   Must be called after sbl1_ddr_init
*   
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_wait_for_ddr_training(void)
{
  uint32 ddr_params_training_data_size = 0;
  void* ddr_training_data_ptr = NULL;
  
  /* Check if DDR parameter training is required */
  if (boot_ddr_params_is_training_required() == TRUE)
  {
    /* Only perform DDR training if SBL is not in dload mode */
    if(!boot_dload_is_dload_mode_set())
    { 
      boot_log_message("sbl1_wait_for_ddr_training, Start");
      boot_log_start_timer();
      
      memory_barrier();

      /* If boot_ddr_post_init returns TRUE then ddr training data is updated, 
        we need to save it to storage device */
      if(boot_ddr_post_init() == TRUE)
      {
        memory_barrier();
        
        ddr_training_data_ptr = 
            boot_ddr_params_get_training_data(&ddr_params_training_data_size);
        
        BL_VERIFY((ddr_training_data_ptr != NULL) &&
                  (ddr_params_training_data_size != 0),
                  BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT);
        
        /*Save the updated training data to storage device */
        sbl1_save_ddr_training_data_to_partition(ddr_training_data_ptr, 
                                                ddr_params_training_data_size);
                                                
        /* Do a reset after training data is saved */
        boot_hw_reset(BOOT_HARD_RESET_TYPE);
      }

      /* Speed back up to max frequency */
      boot_clock_set_cpu_perf_level(CLOCK_BOOT_PERF_MAX);
      boot_clock_set_L2_perf_level(CLOCK_BOOT_PERF_MAX);
    
      boot_ddr_test(0);

      boot_log_stop_timer("sbl1_wait_for_ddr_training, Delta");
    }
  }
}

/*===========================================================================

**  Function :  sbl1_cable_dload_check

** ==========================================================================
*/

// emergency cable download
#ifdef FEATURE_TCTNB_FORCE_DLOAD_CABLE
#define TRACEABILITY_PARTITION_OFFSET 0x175
void sbl1_cable_dload_check(bl_shared_data_type *bl_shared_data)
{
    uint32 pwr_mode = 0;
    DalDeviceHandle *htlmm;

    DALGpioSignalType gpio_cfg = DAL_GPIO_CFG(NPI_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA);

    uint32 *boot_restart_reason_ptr = (uint32*)(SHARED_IMEM_BOOT_BASE + RESTART_REASON_ADDR_OFFSET);
    uint8 is_usb_charge;
    DALGpioValueType value;
    char log_message[50];
    uint32 clear = 0x00000000; /*clear power on mode sign*/
    uint32 reason = 0x776655cc;

    boot_log_message("cable_dload_check, Start");
    boot_log_start_timer();

    /* get status of usb */
    boot_busywait(5*100*1000);/*wait 0.5s*/
    pm_comm_read_byte(2, 0x1310, &is_usb_charge, 0);

    //snprintf(log_message,50,"is_usb_charge 0x%x",is_usb_charge);
    //boot_log_message(log_message);
    is_usb_charge = is_usb_charge >> 2 ;

    /*get boot mode*/
    boot_read_flash_partition(traceability_partition_id,
		                      (void*)&pwr_mode,
		                      TRACEABILITY_PARTITION_OFFSET,
		                      sizeof(uint32));
    //snprintf(log_message,50,"traceability pwr_mode is 0x%x",pwr_mode);
    //boot_log_message(log_message);

    /* USB power on */
    if (is_usb_charge ){

         // Create a TLMM handle
         DAL_DeviceAttach(DALDEVICEID_TLMM, &htlmm);

         DalDevice_Open(htlmm, DAL_OPEN_SHARED);

         // Enable GPIO
         DalTlmm_ConfigGpio(htlmm, gpio_cfg, DAL_TLMM_GPIO_ENABLE);

        /*delay 100ms*/
        DALSYS_BusyWait(100000);

        /* GPIO operation */
        DalTlmm_GpioIn(htlmm, gpio_cfg, &value);

         // Disable GPIO
         DalTlmm_ConfigGpio(htlmm, gpio_cfg, DAL_TLMM_GPIO_DISABLE);

         // Remove the handle
         DalDevice_Close(htlmm);
         DAL_DeviceDetach(htlmm);

       /* check NPI_DOWNLOAD AND sahara or mprg reboot  */
        if(( DAL_GPIO_LOW_VALUE == value )&&(pwr_mode != 0x776655cc)) {
	      boot_write_flash_partition(traceability_partition_id,
			                       &reason,
			                       TRACEABILITY_PARTITION_OFFSET,
			                       sizeof(uint32));
          boot_dload_transition_pbl_forced_dload();
        }

    }

	if(pwr_mode == 0x776655cc){

	    snprintf(log_message,50,"pwr_mode is 0x%x clear",pwr_mode);
            boot_log_message(log_message);
            *boot_restart_reason_ptr = 0x776655cc;
            if (need_reset_device_by_qsee) {
                boot_log_message("device need reset by qsee,don't clear dload flag!");
            } else {
		boot_write_flash_partition(traceability_partition_id,
	                               &clear,
			                       TRACEABILITY_PARTITION_OFFSET,
			                       sizeof(uint32));
            }
	}
	boot_log_message("cable_dload_check, End");
}
#endif //FEATURE_TCTNB_FORCE_DLOAD_CABLE

void boot_write_flash_partition
(
  uint8 * partition_id,           /*partition id define in boot_gpt_partition_id.c*/
  void* buf,                      /*data buffer*/
  uint32 byte_offset,             /*offset in partition*/
  uint32 size                     /*size to read*/
)
{
  boot_flash_trans_if_type *trans_if = NULL;
  /*boot_boolean success = FALSE;*/


  /* Write DDR training data to storage device */
  boot_flash_configure_target_image(partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR_PASSED|BL_ERROR_GROUP_BOOT );

  /*success = */dev_sdcc_write_bytes(buf,
                                 byte_offset,
                                 size,
                                 GEN_IMG);

  /*BL_VERIFY(success, BL_ERR_SBL); not report error*/

  /* close parition*/
  boot_flash_dev_close_image(&trans_if);
}


void boot_read_flash_partition
(
  uint8 * partition_id,           /*partition id define in boot_gpt_partition_id.c*/
  void* buf,                      /*data buffer*/
  uint32 byte_offset,             /*offset in partition*/
  uint32 size                     /*size to read*/
)
{
  boot_flash_trans_if_type *trans_if = NULL;
  /*boot_boolean success = FALSE; not report error*/

  boot_flash_configure_target_image(partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  /*BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR );*/

  boot_clobber_add_local_hole( boot_flash_trans_get_clobber_tbl_ptr( trans_if ),
                               buf, size );

  /*success = */boot_flash_trans_read( trans_if,
                                   buf,
                                   byte_offset,
                                   size);
  /*BL_VERIFY( success, BL_ERR_ELF_FLASH );*/

  /* close parition*/
  boot_flash_dev_close_image(&trans_if);
}
