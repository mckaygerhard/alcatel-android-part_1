/*=============================================================================

                              SBL1 DDR Debug Main Control

GENERAL DESCRIPTION
  This file contains the main control for  DDR Debug SBL1 execution. Since this implements
  the main control functionality, it may be dependent on all modules of the
  system.

Copyright 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/
/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/hw/msm8953/sbl1/sbl1_ddr_debug_mc.c#3 $ 
$DateTime: 2016/04/21 08:39:38 $ 
$Author: pwbldsvc $

when         who     what, where, why
----------   ---     ----------------------------------------------------------
03/10/16     yps     rebase DDI code
07/20/15     yps     Proting DDI to 8952 platform.
05/14/14     yps     proting code to 8916 platform.
09/13/13     sl      Removed PBL dependencies.
08/29/13     sl      Updated scl section names.
07/17/13     sr      Initial version
===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_target.h"
#include "boot_util.h"
#include "boot_config_data.h"
#include "boot_cache_mmu.h"
#include "boot_extern_dal_interface.h"
#include "boot_extern_bus_interface.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_busywait_interface.h"
#include "boot_extern_clk_interface.h"
#include "boot_extern_ddr_interface.h"
#include "boot_extern_bam_interface.h"
#include "boot_extern_power_interface.h"
#include "sbl1_mc.h"
#include "boot_logger.h"
#include "boot_error_if.h"
/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
/*External Abort mask bit */
#define CPSR_EA_BIT     (1<<8)

/* Internal heap memory, inside the SBL1 image*/
#define INTERNAL_HEAP_SIZE  20480
#pragma arm section zidata = "BOOT_INTERNAL_HEAP"
static byte internal_heap[INTERNAL_HEAP_SIZE];
#pragma arm section zidata

extern uint8 config_data_table[CONFIG_DATA_TABLE_MAX_SIZE];

extern uint32  Load$$DDR_DEBUG_WDOG$$Base;
extern uint32  Image$$DDR_DEBUG_WDOG$$Base;
extern uint32  Image$$DDR_DEBUG_WDOG$$Length;

extern uint32  Load$$DDR_DEBUG_CODE$$Base;
extern uint32  Image$$DDR_DEBUG_CODE$$Base;
extern uint32  Image$$DDR_DEBUG_CODE$$Length;

extern uint32  Load$$DDR_DEBUG_RO$$Base;
extern uint32  Image$$DDR_DEBUG_RO$$Base;
extern uint32  Image$$DDR_DEBUG_RO$$Length;

extern uint32  Load$$DDR_DEBUG_RW$$Base;
extern uint32  Image$$DDR_DEBUG_RW$$Base;
extern uint32  Image$$DDR_DEBUG_RW$$Length;


extern uint32  Load$$DDR_DEBUG_RW_COPY$$Base;
extern uint32  Image$$DDR_DEBUG_RW_COPY$$Base;
extern uint32  Image$$DDR_DEBUG_RW_COPY$$Length;

extern uint32  Load$$DDR_DEBUG_ZI$$Base;
extern uint32  Image$$DDR_DEBUG_ZI$$Base;
extern uint32  Image$$DDR_DEBUG_ZI$$ZI$$Length;


static boot_log_init_data boot_log_data =
{
  (void *)SBL1_LOG_BUF_START,
  SBL1_LOG_BUF_SIZE,
  (void *)SBL1_LOG_META_INFO_START,
  SBL1_LOG_META_INFO_SIZE,
  NULL
};


/*=============================================================================
                              FUNCTION DEFINITIONS
=============================================================================*/

/*===========================================================================
**	Function :	sbl1_wdogpath_main_ctl
** ==========================================================================
*/
/*!
* 
* @brief
*	This function does the basic/needed initializations for both ( coldboot / warmboot ) paths .
*		- it sets up the rw base , by coping the backup from RW_COPY base 
*		- it also setsup the ddr for ddr_debug_mode 
*		- Initializes ram
*		- And so on...
* 
* @param  None
*  
* @par Dependencies
*	None
* 
* @retval
*	None
* 
* @par Side Effects
*	This function never returns.
* 
*/
void sbl1_wdogpath_main_ctl(void)
{
  const void *cdt_ptr;
  uint32 cdt_size;
  ddr_info info;
  DALResult bsy_wait_init;
  icbcfg_error_type icb_config_Init;
//#ifndef FEATURE_RUMI_BOOT
	pm_err_flag_type pm_init;
//#endif
  //icbcfg_remap_info_type remap = {0x0, 0x20000000, 0x60000000, ICBCFG_REMAP_INTERLEAVE_DEFAULT, FALSE};
  //ddr_info sbl1_ddr_info;

  /* Zero out the zero-init data segment. */
  qmemset( (void *)&Image$$DDR_DEBUG_ZI$$Base,
		   0,
		   (uint32)&Image$$DDR_DEBUG_ZI$$ZI$$Length );
#ifdef SBL1_DISABLE_D_CACHE
	mmu_flush_cache_and_disable();
	mmu_enable_instruction_cache();
#endif
#ifndef FEATURE_FIREHOSE_QUSB

  /* Enabling external abort */
  sbl1_external_abort_enable(CPSR_EA_BIT);
#endif

  /*Configure Domain access control register */
  mmu_set_dacr(DACR_ALL_DOMAIN_CLIENTS);

  /* Enable qdss workaround*/
  boot_clock_debug_init();
  /* Initialize busywait module */
  BL_VERIFY((bsy_wait_init=boot_busywait_init()) == DAL_SUCCESS, (uint16)bsy_wait_init|BL_ERROR_GROUP_BUSYWAIT);


  /*initialize boot logger*/
  boot_log_init(&boot_log_data);



  /* Initialize clocks */
//  BL_VERIFY((boot_clock_init() == TRUE), BL_ERR_SBL);
 // BL_VERIFY((boot_clock_init() == TRUE), BL_ERR_CPU_CLK_INIT_FAIL|BL_ERROR_GROUP_BOOT);

  boot_DALSYS_HeapInit(internal_heap, INTERNAL_HEAP_SIZE, FALSE);

  boot_DALSYS_InitMod(NULL);


  /* Initialize BAM interface */
  boot_bam_drv_init();



//#if (!defined(FEATURE_RUMI_BOOT))
  /* Initialize PMIC */
  boot_log_message("pm_device_init, Start");
  boot_log_start_timer();

  BL_VERIFY((pm_init=boot_pm_device_init()) == PM_ERR_FLAG__SUCCESS, (uint16)pm_init|BL_ERROR_GROUP_PMIC);  
  boot_log_stop_timer("pm_device_init, Delta");
//#endif
  /* There is a HW bug in 8916 where PMIC_ABNORMAL_RESIN_STATUS is set accidently.
     Just clear the bit regardless. */
  HWIO_GCC_RESET_STATUS_OUTM(HWIO_GCC_RESET_STATUS_PMIC_ABNORMAL_RESIN_STATUS_BMSK,
                             0);

  /* Get DDR CDT parameters */
  cdt_ptr = boot_get_config_data_block(config_data_table,
									   CONFIG_DATA_BLOCK_INDEX_V1_DDR,
									   &cdt_size);

  /* Make sure CDT parameters are valid */
  BL_VERIFY((cdt_size <= CONFIG_DATA_TABLE_MAX_SIZE) &&
            (cdt_ptr != NULL),  BL_ERR_CDT_PARAMS_SIZE_INVALID|BL_ERROR_GROUP_BOOT );

  /* Copy CDT DDR parameters to shared IMEM */
  //qmemcpy( (void*)SHARED_IMEM_BOOT_CDT_BASE, cdt_ptr, cdt_size );

  /* Pass CDT parameters to DDR drivers */
  BL_VERIFY(boot_ddr_set_params((void*)cdt_ptr, cdt_size) == TRUE, BL_ERR_DDR_SET_PARAMS_FAIL|BL_ERROR_GROUP_DDR);
  /* Initialize the railway driver */
  /* NOTE: this driver API is wrapped in boot_extern_power_interface.h, 
      which needs to be enabled by turning on BOOT_EXTERN_POWER_COMPLETED in target.builds */
  boot_railway_init();
  boot_log_message("cpr_init, Start");
  boot_log_start_timer();

  /* Call CPR init to settle the voltages to the recommended levels*/
  boot_cpr_init();
 
  boot_log_stop_timer("cpr_init, Delta");

  /* Initialize DDR clock according to DDR type */
  BL_VERIFY((boot_pre_ddr_clock_init(boot_ddr_get_info().ddr_type) == TRUE), BOOT_PRE_DDR_CLK_INIT_FAIL|BL_ERROR_GROUP_BOOT);

  /* Initialize buses */
  BL_VERIFY((icb_config_Init=boot_ICB_Config_Init("/dev/icbcfg/boot")) == ICBCFG_SUCCESS, (uint16)icb_config_Init|BL_ERROR_GROUP_ICB);

  /* Initialize DDR */
  boot_ddr_initialize_device(boot_clock_get_ddr_speed());

  boot_ddr_post_init();

  /* Configure DDR slave segments address range */
  info = boot_ddr_get_info();

  /* Initialize bus segments according to DDR size info */
  boot_ICB_Segments_Init("/dev/icbcfg/boot", &info);
  boot_ddr_remapper();
  
  boot_log_message("pm_driver_init, Start");
  boot_log_start_timer(); 
  
  /* Initialize PMIC Driver - Allowing API usage */
  BL_VERIFY((pm_init=boot_pm_driver_init()) == PM_ERR_FLAG__SUCCESS, (uint16)pm_init|BL_ERROR_GROUP_PMIC);
  BL_VERIFY(((pm_init=boot_pm_sbl_chg_init()) == PM_ERR_FLAG__SUCCESS),(uint16)pm_init|BL_ERROR_GROUP_PMIC);
  
  boot_log_stop_timer("pm_driver_init, Delta"); 

  /* Go to DDR Debug Mode */
  boot_ddr_debug();

} /* sbl1_wdogpath_main_ctl() */

/*===========================================================================
**  Function :  sbl1_ddr_debug_main_ctl
** ==========================================================================
*/
/*!
* 
* @brief
*   This function gets called from .s (startup ) , it disable the cache as the sbl1
*   code now runs from CODERAM instead of L2Cache .SBL1 code needs to be run from CODERAM
*   as L2 cache gets flushed after watch dog reset .
* 
* @param  None
*  
* @par Dependencies
*   None
* 
* @retval
*   None
* 
* @par Side Effects
*   This function never returns.
* 
*/
void sbl1_ddr_debug_main_ctl()
{
  cache_mmu_disable();
  sbl1_wdogpath_main_ctl();
}
