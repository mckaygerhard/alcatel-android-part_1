import argparse
import array
import os
import struct
import sys

### Begin compress/decompress helper functions ###
def pushCompressedBits(bits,numBits,compressedWordStream):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    temp = bits << numCompressedPartialBits                                 
    numCompressedPartialBits += numBits                                                                                        
    compressedPartialWord = compressedPartialWord | temp   
    if numCompressedPartialBits >= 32:                                                                               
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)
        numCompressedWords += 1                                              
        compressedPartialWord = compressedPartialWord >> 32   
        numCompressedPartialBits -= 32              

def finalizeCompressedBits(compressedWordStream,compressedPartialWord,numCompressedPartialBits):  
    global numCompressedWords
    if numCompressedPartialBits > 0:                                                                                                                                                                                                    
        compressedWordStream.append(compressedPartialWord & 0xFFFFFFFF)  
        numCompressedWords += 1   

def checkAnchor(anchor,val,compressed):
    global anchors
    anchor_val = anchors[anchor]; 
    if anchors[anchor] == val: 
        pushCompressedBits((anchor << 2) + 1,2 + 2,compressed)
        return 1
    elif ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)):
        pushCompressedBits(((val&0x3FF)<<(2+2))+((anchor<<2)+2),2+2+10,compressed)
        anchors[anchor] = val; 
        return 1
    else:
        return 0

def __GET_BITS(__hold,n):
     return __hold & ((1 << n) - 1)

def __SKIP_BITS_W_CHECK(compressed,size,n): 
    global __numAvailBits   
    global __hold
    global __in_index
    __numAvailBits -= n                                             
    __hold = __hold >> n                                                  
    if __numAvailBits <= 32: 
        if __in_index != size:
            __hold |= compressed[__in_index] << __numAvailBits              
            __in_index += 1                                                                         
            __numAvailBits += 32 
### End compress/decompress helper functions ###


##
# @brief	Decompress an input stream compressed by deltaCompress v2
# @param	compressed - input stream
# @param	decompressed - output stream
# @param	size - the maximum output size
#
def deltaUncompress(compressed, uncompressed, size):
    global __numAvailBits   
    global __hold
    global __in_index
    __anchors = [0,0,0,0]
    anchorIndex = 3
    __numAvailBits = 0
    __hold = 0
    __in_index = 0
    out_index = 0
    if size == 0:
        return (0, 0)
    compressed_size = len(compressed)
    __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    if size != 1:
        __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    while out_index < size and __numAvailBits > 0:
        code = __GET_BITS(__hold,2)
        if code == 0:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            uncompressed.append(0)
            out_index += 1
        elif code == 1:
            anchor = __GET_BITS(__hold>>2,2)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2)
            uncompressed.append(__anchors[anchor])
            out_index += 1
        elif code == 2:
            anchor = __GET_BITS(__hold>>2,2)
            delta = __GET_BITS(__hold>>(2+2),10)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2+10)
            val = (__anchors[anchor] & 0xFFFFFC00) | delta
            uncompressed.append(val)
            out_index += 1
            __anchors[anchor] = val
        elif code == 3:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            val = __hold & 0xFFFFFFFF
            __SKIP_BITS_W_CHECK(compressed,compressed_size,32)
            uncompressed.append(val)
            out_index += 1
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            __anchors[anchorIndex] = val
    while __numAvailBits >= 32:
    	__in_index -= 1 
    	__numAvailBits -= 32
    return (out_index, __in_index)


##
# @brief	Compress an input stream with deltaCompress v2.1 algorithm
# @param	uncompressed - input stream
# @param	compressed - output stream
# @param	size - maximum size of input block
#
def deltaCompress (uncompressed,compressed,size):
    global numCompressedWords
    global compressedPartialWord
    global numCompressedPartialBits
    global anchors
    anchors = [0,0,0,0]
    numCompressedWords = 0
    compressedPartialWord = 0
    numCompressedPartialBits = 0
    anchorIndex = 3
    # @warning anandj In HW implementation, compressed data starts at first word
    for i in range(min(size, len(uncompressed))):
        val = uncompressed[i]
        if (val == 0):
            pushCompressedBits(0,2,compressed)
        else:
            anchor = anchorIndex
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if checkAnchor(anchor,val,compressed) == 1:
                continue
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            anchors[anchorIndex] = val
            pushCompressedBits(3,2,compressed)
            pushCompressedBits(val,32,compressed)
    finalizeCompressedBits(compressed,compressedPartialWord,numCompressedPartialBits)
    
    return numCompressedWords


##
# @brief	Decompress all .RWC in a given directory.
# @param	Absolute path to directory
#
def decompress_dir(path, dlt, rec):
	# try:
	for file in os.listdir(path):
		if file.endswith(".RWC"):
			decompress_file(os.path.abspath(os.path.join(path, file)), dlt)
		elif rec and os.path.isdir(file):
			decompress_dir(os.path.abspath(os.path.join(path, file)), dlt, rec)


##
# @brief	Apply deltaUncompress v2.1 to an input file.
# @param	filepath - path to input file
# @param	dlt - should the input file be deleted once done?
#
def decompress_file(filepath, dlt):
	# Open the appropriate files.
	try:
		print '\n\n' + 'Opening ' + filepath + ' for decompression'
		comp_f = open(filepath, 'rb')
	except:
		print 'Failure opening ' + filepath
		return

	d_filepath = filepath.replace('.RWC', '.BIN')
	try:
		print 'Destination: ' + d_filepath
		decomp_f = open(d_filepath, 'wb')
	except:
		print 'Failure opening ' + d_filepath
		return

	# Read the header. First is the magic number.
	magic_num = comp_f.read(4)
	magic_num = struct.unpack('<I', magic_num)[0]
	if magic_num != 0x00435752: 
		print 'Invalid header, skipping file'
		return
	# Chunk size
	chunk_sz = comp_f.read(4)
	chunk_sz = struct.unpack('<I', chunk_sz)[0]
	# Get the version number.
	version_list = [0x01, 0x00, 0x02, 0x00]
	for val in version_list:
		version = comp_f.read(1)
		version = struct.unpack('<B', version)[0]
		if version != val:
			print 'Invalid version number, skipping file'
			return
	# Reserved bytes
	skip_offset = comp_f.read(4)
	skip_size = comp_f.read(4)
	skip_offset = struct.unpack('<I', skip_offset)[0]
	skip_size = struct.unpack('<I', skip_size)[0]

	decompressed = array.array('I')
	# Decompress the file in chunks.
	print "Beginning decompression..."
	while True:
		if skip_size != 0 and skip_offset/4 == len(decompressed):
			print "Filling skipped region with 0s"
			for i in range(0, skip_size/4):
				decompressed.append(0)
		compressed = array.array('I')
		comp_chunk_len = comp_f.read(4)
		if(len(str(comp_chunk_len)) == 0):
			break
		comp_chunk_len = struct.unpack('<I', comp_chunk_len)[0]

		# Get the compressed data.
		try:
			compressed.fromfile(comp_f, comp_chunk_len)
		except:
			# Reached EOF, but still decompress. Break in the next iteration.
			pass
		deltaUncompress(compressed, decompressed, chunk_sz / 4)
		# Decompression can be slow, so periodically print our progress.
		if (len(decompressed) % 0x800000) == 0:
			print str(4 * len(decompressed)) + ' bytes decompressed'
		
	decompressed.tofile(decomp_f)
	
	print 'Final decompressed size: ' + str(4 * len(decompressed)) + ' bytes'
	comp_f.close()
	decomp_f.close()

	if(dlt):
		os.remove(filepath)

##
# @brief	Compress .BIN files in a directory
# @param	path - path to directory
# @param	dlt - should the input file be deleted once done?
#				  (Directories not deleted)
# @param	rec - should we recurse within other directories?
#
def compress_dir(path, dlt, rec):
	for file in os.listdir(path):
		if file.endswith(".BIN"):
			compress_file(os.path.abspath(os.path.join(path,file)), dlt)
		elif rec and os.path.isdir(file):
			compress_dir(os.path.abspath(os.path.join(path,file)), dlt, rec)

##
# @brief	Compress one file with deltaCompress v2.1
# @param	path - path to compressed file
# @param	dlt - should the input file be deleted once done?
#
def compress_file(path, dlt):
	# Open the appropriate files.
	try:
		print '\n\n' + 'Opening ' + path + ' for compression'
		uncomp_f = open(path, 'rb')
	except:
		print 'Failure opening ' + path
		return

	d_path = path.replace('.BIN', '.RWC')
	try:
		print 'Destination: ' + d_path
		comp_f = open(d_path, 'wb')
	except:
		print 'Failure opening ' + d_path
		return

	compressed = array.array('I')
	block_size = 65536

	# Set up and add the header.
	compressed.append(0x00435752)	# "RWC" - Magic Number
	compressed.append(block_size)	# Maximum size of input blocks
	compressed.append(0x00020001)	# Algorithm and System revisions (flipped for Endianness)
	compressed.append(0)			# Reserved

	print 'Compressing...'
	counter = 1
	while True:
		copied_bytes = 0
		uncompressed = []
		# Read up to one block from the file.
		try:
			uncompressed = array.array('I')
			uncompressed.fromfile(uncomp_f, block_size / 4)
		except:
			pass
		copied_bytes = 4 * len(uncompressed)
		# Avoid writing extra 0s.
		if copied_bytes == 0:
			break
		# Compress the file.
		length_idx = len(compressed)
		comp_length = deltaCompress(uncompressed, compressed, copied_bytes / 4)
		# Include the length of the compressed block.
		compressed.insert(length_idx, comp_length)
		# Reached the end of the file? Done compressing.
		if copied_bytes < block_size:
			break
		# Clear the uncompressed list.
		del uncompressed
		# Print periodically to show that we are still alive.
		if (len(compressed) >  (counter * 0x200000)):
			print str(4 * len(compressed)) + ' bytes compressed'
			counter += 1

	print 'Final compressed size: ' + str(4 * len(compressed)) + ' bytes'

	# Write to the destination file.
	compressed.tofile(comp_f)

	comp_f.close()
	uncomp_f.close()


##
# @brief	Get command line arguments.
#
def get_args():
	parser = argparse.ArgumentParser(description='Python tool for RWC v2 Compression')
	parser.add_argument('-c', '--compress', help='Compress (default Decompress)', action='store_true', required=False)
	parser.add_argument('-p', '--path', type=str, help='Relative Path', required=False, default='.')
	parser.add_argument('-d', '--delete', help='Delete source .RWC or .BIN after use', action='store_true', required=False)
	parser.add_argument('-r', '--recurse', help='Recurse through files and folders', action='store_true', required=False)
	return parser.parse_args()

##
# @brief	Entry point for compression/decompression script.
#
def main():
	# Get command line arguments.
	args = get_args()
	path = args.path
	dlt  = args.delete
	rec  = args.recurse
	comp = args.compress

	# Get the absolute filepath and check if it's a directory.
	path = os.path.abspath(path)
	is_dir = os.path.isdir(path)

	# Compress or decompress? File or directory?
	if comp:
		if is_dir:
			compress_dir(path, dlt, rec)
		else:
			compress_file(path, dlt)
	else:
		if is_dir:
			decompress_dir(path, dlt, rec)
		else:
			decompress_file(path, dlt)


if __name__ == "__main__":
	main()