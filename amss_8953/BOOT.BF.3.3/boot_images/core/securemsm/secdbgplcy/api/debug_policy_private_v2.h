/**
@file debug_policy_private.h
@brief Debug policy implementation definitions/routines

This file defines a private definition of debug policy implementation

*/

/*=============================================================================
                              EDIT HISTORY
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


  $Header: //components/rel/boot.bf/3.3/boot_images/core/securemsm/secdbgplcy/api/debug_policy_private_v2.h#2 $
  $DateTime: 2016/03/30 05:34:58 $
  $Author: pwbldsvc $

 when           who         what, where, why
 --------       ---         --------------------------------------------------
 2014/07/08     st          Initial version
=============================================================================*/

#ifndef DEBUG_POLICY_H
#define DEBUG_POLICY_H

#include "secboot_debug_policy_v2.h"
/**
 * @brief 
 *        Check whether the current image should be authenticated against the debug policy roots
 *
 * @param[in] sw_type is the current image ID being authenticated.
 *                @retval TRUE if the image should be authenticated against the debug policy, FALSE otherwise
 *
 */
boolean is_dbg_policy_rot_for_image( uint32 sw_type );

/**
 * @brief
 * When TZ is initialized we can no longer access the policy
 * from the shared region. We therefore need to keep a local
 * copy. Before copying, verify it.
* 
 * @param[in] dp The debug policy
 * @retval TRUE if the policy is valid, bound and was copied locally
*/
boolean copy_debug_policy(dbg_policy_t* dp);

/**
* @brief
 * Process the debug policy and apply any relevant settings
 */
void apply_dbg_policy(void);

/**
 * @brief
 * Return the debug policy
 * @retval NULL if the debug policy has not been validated
*/
dbg_policy_t* get_dbg_policy(void);

boolean is_debug_policy_flag_set ( uint32 flag );

boolean is_dbg_policy_valid(dbg_policy_t *dbgp_ptr);

boolean is_sec_dbg_skip_serial_number(void);

#endif
