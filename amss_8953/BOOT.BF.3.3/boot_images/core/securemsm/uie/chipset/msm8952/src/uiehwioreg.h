#ifndef __UIEHWIOREG_H__
#define __UIEHWIOREG_H__
/*
===========================================================================
*/
/**
  @file uiehwioreg.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    SECURITY_CONTROL_CORE

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/securemsm/uie/chipset/msm8952/src/uiehwioreg.h#1 $
  $DateTime: 2015/11/03 22:24:09 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

//#define SECURITY_CONTROL_BASE                                       0x86500000

#define SECURITY_CONTROL_CORE_REG_BASE                                                                             (SECURITY_CONTROL_BASE      + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_PHYS                                                                        (SECURITY_CONTROL_BASE_PHYS + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_OFFS                                                                        0x00000000

#define HWIO_OEM_CONFIG0_IMAGE_ENCRYPTION_ENABLE_BMSK                                                                 0x20000000
#define HWIO_OEM_CONFIG0_ADDR                                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000601C)
#define HWIO_OEM_CONFIG0_PHYS                                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000601C)
#define HWIO_OEM_CONFIG0_OFFS                                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x0000601C)
#define HWIO_OEM_CONFIG0_RMSK                                                                                      0xffffffff
#define HWIO_OEM_CONFIG0_IN          \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, HWIO_OEM_CONFIG0_RMSK)
		
#define HWIO_OEM_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, m)
//spare register 21 LSB
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004420)		
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004420)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004420)

#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, m)
//spare register 21 MSB		
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004424)		
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004424)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004424)

#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, m)
//spare register 22	LSB	
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004428)		
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004428)
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004428)

#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG22_LSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG22_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR, m)
		
//spare register 22	MSB	
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000442C)		
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000442C)
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x0000442C)

#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG22_MSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG22_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR, m)
		
//spare register 23	LSB	
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004430)		
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004430)
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004430)

#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG23_LSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG23_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR, m)
		
//spare register 22	MSB	
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004434)		
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004434)
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_OFFS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004434)

#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_RMSK                                                                      0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG23_MSB_RMSK)
		
#define HWIO_QFPROM_CORR_SPARE_REG23_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG23_MSB_ADDR, m)

//FEC ESR Register
#define HWIO_FEC_ESR_ADDR                                                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002090)
#define HWIO_FEC_ESR_PHYS                                                                                          (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002090)
#define HWIO_FEC_ESR_OFFS                                                                                          (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00002090)
#define HWIO_FEC_ESR_RMSK                                                                                               0xfff

#define HWIO_FEC_ESR_IN          \
        in_dword_masked(HWIO_FEC_ESR_ADDR, HWIO_FEC_ESR_RMSK)
#define HWIO_FEC_ESR_INM(m)      \
        in_dword_masked(HWIO_FEC_ESR_ADDR, m)
	
#define HWIO_FEC_ESR_OUT(v)      \
        out_dword(HWIO_FEC_ESR_ADDR,v)
#define HWIO_FEC_ESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_FEC_ESR_ADDR,m,v,HWIO_FEC_ESR_IN)
		
#define HWIO_FEC_ESR_CORR_SW_ACC_BMSK                                                                                   0x800
#define HWIO_FEC_ESR_CORR_SW_ACC_SHFT                                                                                     0xb
#define HWIO_FEC_ESR_CORR_HDCP_BMSK                                                                                     0x400
#define HWIO_FEC_ESR_CORR_HDCP_SHFT                                                                                       0xa
#define HWIO_FEC_ESR_CORR_BOOT_ROM_BMSK                                                                                 0x200
#define HWIO_FEC_ESR_CORR_BOOT_ROM_SHFT                                                                                   0x9
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_BMSK                                                                               0x100
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_SHFT                                                                                 0x8
#define HWIO_FEC_ESR_CORR_MULT_BMSK                                                                                      0x80
#define HWIO_FEC_ESR_CORR_MULT_SHFT                                                                                       0x7

#define HWIO_FEC_ESR_CORR_SEEN_BMSK                                                                                      0x80
#define HWIO_FEC_ESR_CORR_SEEN_SHFT                                                                                       0x7

#define HWIO_FEC_ESR_ERR_SW_ACC_BMSK                                                                                     0x20
#define HWIO_FEC_ESR_ERR_SW_ACC_SHFT                                                                                      0x5
#define HWIO_FEC_ESR_ERR_HDCP_BMSK                                                                                       0x10
#define HWIO_FEC_ESR_ERR_HDCP_SHFT                                                                                        0x4
#define HWIO_FEC_ESR_ERR_BOOT_ROM_BMSK                                                                                    0x8
#define HWIO_FEC_ESR_ERR_BOOT_ROM_SHFT                                                                                    0x3
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_BMSK                                                                                  0x4
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_SHFT                                                                                  0x2
#define HWIO_FEC_ESR_ERR_MULT_BMSK                                                                                        0x2
#define HWIO_FEC_ESR_ERR_MULT_SHFT                                                                                        0x1
#define HWIO_FEC_ESR_ERR_SEEN_BMSK                                                                                        0x1
#define HWIO_FEC_ESR_ERR_SEEN_SHFT                                                                                        0x0


#define HWIO_FEC_EAR_ADDR                                                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002084)
#define HWIO_FEC_EAR_PHYS                                                                                          (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002084)
#define HWIO_FEC_EAR_OFFS                                                                                          (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00002084)
#define HWIO_FEC_EAR_RMSK                                                                                          0xffffffff
#define HWIO_FEC_EAR_IN          \
        in_dword_masked(HWIO_FEC_EAR_ADDR, HWIO_FEC_EAR_RMSK)
#define HWIO_FEC_EAR_INM(m)      \
        in_dword_masked(HWIO_FEC_EAR_ADDR, m)
#define HWIO_FEC_EAR_CORR_ADDR_BMSK                                                                                0xffff0000
#define HWIO_FEC_EAR_CORR_ADDR_SHFT                                                                                      0x10
#define HWIO_FEC_EAR_ERR_ADDR_BMSK                                                                                     0xffff
#define HWIO_FEC_EAR_ERR_ADDR_SHFT                                                                                        0x0

#endif /* __UIEHWIO_H__ */
