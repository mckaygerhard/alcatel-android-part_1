/*===========================================================================
                Unified Image Encryption (UIE) L1 Fuse Key

GENERAL DESCRIPTION
  Chipset-specific key source retrieval

Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                                 INCLUDE FILES
===========================================================================*/
#include <stddef.h>
#include <string.h>
#include "uie.h"
#include "uie_priv.h"
#include "uie_env.h"
#include "uiehwioreg.h"
#include "HALhwio.h"

#define SECURITY_CONTROL_CORE_REG_FEC_OFFSET  0x2000
#define SECURITY_CONTROL_CORE_REG_CORR_OFFSET 0x4000
#define SECURITY_CONTROL_CORE_REG_SW_BASE_OFFSET 0x6000

#define BREAKIF_SETSTATUS(cond, status_code) \
  if (cond) { status = status_code; break; }

/*
 * The 128-bit L1 fuse key is represented via three corrected fuse rows.
 * Each corrected row consists of 4 least significant bytes
 * and 3 most significant bytes (1 byte was used for forward error correction).
 * Of the third row, only two least significant bytes are used.
 */
typedef struct __attribute__((__packed__))
{
  uint8 row0_lsb[4];
  uint8 row0_msb[3];
  uint8 row1_lsb[4];
  uint8 row1_msb[3];
  uint8 row2_partial_lsb[2];
} uie_fuse_key_t;

bool uie_is_image_encryption_fuse_enabled(void)
{
  bool ret = true;

  uie_map_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                    SECURITY_CONTROL_CORE_REG_SW_BASE_OFFSET, 0x1000);

  if(HWIO_OEM_CONFIG0_INM(HWIO_OEM_CONFIG0_IMAGE_ENCRYPTION_ENABLE_BMSK))
  {
    ret = true;
  }

  uie_unmap_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                      SECURITY_CONTROL_CORE_REG_SW_BASE_OFFSET, 0x1000);

  return ret;
}

int uie_get_l1_fuse_key(uie_key_src_t key_source, uint8 *key, uint32 keylen)
{
  int status = UIE_STATUS_SUCCESS;
  uint32 mrc_index = 0;
  uie_fuse_key_t *fuse_key = (uie_fuse_key_t*)key;
  
  do
  {
    BREAKIF_SETSTATUS(key == NULL || keylen != UIE_KEY_LEN ||
                      key_source != UIE_KEY_SRC_OTP_OEM,
                      UIE_STATUS_INVALID_PARAM);
					  
	uie_map_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                      SECURITY_CONTROL_CORE_REG_FEC_OFFSET, 0x1000);
    uie_map_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                      SECURITY_CONTROL_CORE_REG_CORR_OFFSET, 0x1000);

    mrc_index = HWIO_INM(QFPROM_CORR_ANTI_ROLLBACK_2_MSB,HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK);

    UIE_LOG_MSG("UIE mrc_index: %d", mrc_index);

    if (mrc_index == 0)
    {
	  UIE_LOG_MSG("UIE no mrc_index support");
      UIE_MEMSCPY(
        fuse_key->row0_lsb, sizeof(fuse_key->row0_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, 4);
      UIE_MEMSCPY(
        fuse_key->row0_msb, sizeof(fuse_key->row0_msb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, 3);
      UIE_MEMSCPY(
        fuse_key->row1_lsb, sizeof(fuse_key->row1_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG22_LSB_ADDR, 4);
      UIE_MEMSCPY(
        fuse_key->row1_msb, sizeof(fuse_key->row1_msb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG22_MSB_ADDR, 3);
      UIE_MEMSCPY(
        fuse_key->row2_partial_lsb, sizeof(fuse_key->row2_partial_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG23_LSB_ADDR, 2);
    } 
    else
    {
      UIE_MEMSCPY(
        fuse_key->row0_lsb, sizeof(fuse_key->row0_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG24_LSB_ADDR, 4);
      UIE_MEMSCPY(
        fuse_key->row0_msb, sizeof(fuse_key->row0_msb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG24_MSB_ADDR, 3);
      UIE_MEMSCPY(
        fuse_key->row1_lsb, sizeof(fuse_key->row1_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG25_LSB_ADDR, 4);
      UIE_MEMSCPY(
        fuse_key->row1_msb, sizeof(fuse_key->row1_msb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG25_MSB_ADDR, 3);
      UIE_MEMSCPY(
        fuse_key->row2_partial_lsb, sizeof(fuse_key->row2_partial_lsb),
        (void*)HWIO_QFPROM_CORR_SPARE_REG26_LSB_ADDR, 2);
	}

    BREAKIF_SETSTATUS(uie_fec_error_occurred(), UIE_STATUS_KEY_READ_FAIL);
  } while(0);

  uie_unmap_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                      SECURITY_CONTROL_CORE_REG_FEC_OFFSET, 0x1000);
  uie_unmap_fuse_area(SECURITY_CONTROL_CORE_REG_BASE +
                      SECURITY_CONTROL_CORE_REG_CORR_OFFSET, 0x1000);
  return status;
}

uint16 uie_get_fec()
{
  // Get the FEC out
  return HWIO_FEC_ESR_INM(HWIO_FEC_ESR_CORR_SEEN_SHFT);
}

void uie_clear_fec()
{
  // Writing a 0x1 clears the FEC
  HWIO_FEC_ESR_OUT(0x1);
}
