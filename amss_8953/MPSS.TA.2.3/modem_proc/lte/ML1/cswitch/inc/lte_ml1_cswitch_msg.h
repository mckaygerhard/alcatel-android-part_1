#ifndef LTE_ML1_CSWITCH_MSG_H
#define LTE_ML1_CSWITCH_MSG_H
/*!
  @file lte_ml1_cswitch_msg.h

  @brief
   A header describing the messages to the cswitch module.

  Details...
  
*/

/*==============================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/lte.mpss/6.4/ML1/cswitch/inc/lte_ml1_cswitch_msg.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
11/11/15   bm      creation

==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <msgr.h>
#include <msgr_lte.h>
#include <lte_ml1_schdlr_obj.h>
#include <lte_ml1_schdlr_types.h>
#include "lte_ml1_comdef.h"
#include "lte_ml1_cswitch.h"
#include "lte_ml1_tunemgr.h"
#include "lte_ml1_rfmgr.h"
#include "lte_ml1_rfmgr_lte_d.h"


#ifdef FEATURE_CSWITCH_MODULE

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  lte_ml1_schdlr_timeline_mask_t    timeline_mask;
  void                              *user_data_ptr;
} lte_ml1_cswitch_sched_start_cb_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  void                              *user_data_ptr;
  lte_ml1_schdlr_sched_result_s     result;
  lte_ml1_schdlr_obj_state_e        obj_state_prior_abort;
} lte_ml1_cswitch_sched_abort_cb_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  lte_ml1_cswitch_handle_type       handle;
} lte_ml1_cswitch_switch_back_ind_s;


typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  lte_ml1_tunemgr_cb_data_s        tune_done_cb;
  void  						               *user_data;
} lte_ml1_cswitch_tune_done_cb_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  lte_ml1_cc_id_t                  cc_id;
  lte_ml1_rfmgr_trm_acquire_e      acquire_result;
  lte_ml1_trm_rfmgr_res_mask_t     acquired_res;
  void                             *user_data;
} lte_ml1_cswitch_trm_grant_cb_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;

  lte_ml1_rfmgr_lte_d_req_result_s  cb_info;
} lte_ml1_cswitch_lted_grant_cb_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;
} lte_ml1_cswitch_ss_disable_done_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;
} lte_ml1_cswitch_tune_done_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;
  lte_ml1_common_result_e           csw_result;
} lte_ml1_cswitch_csw_done_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type              msgr_hdr;
  void                              *user_data_ptr;
} lte_ml1_cswitch_chest_done_ind_s;

/*! @brief suspend timer expiry indication 
*/
typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type             msgr_hdr;
  /*! Timer type */
  lte_ml1_common_timer_e           timer_type;

} lte_ml1_cswitch_guard_timer_expiry_ind_s;

typedef struct
{
  /*! Message router header */
  msgr_hdr_struct_type             msgr_hdr;
  /*! Internal request handle */
  lte_ml1_cswitch_handle_type      handle;

} lte_ml1_cswitch_cancel_ind_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
enum
{

  /*=================== Internal Messages ==============================*/

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, SCHED_START,
                   0x00, lte_ml1_cswitch_sched_start_cb_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, SCHED_ABORT,
                   0x01, lte_ml1_cswitch_sched_abort_cb_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, SWITCH_BACK,
                   0x02, lte_ml1_cswitch_sched_switch_back_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, TUNE_DONE_CB,
                   0x03, lte_ml1_cswitch_tune_done_cb_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, TRM_GRANT,
                   0x04, lte_ml1_cswitch_trm_grant_cb_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, LTED_GRANT,
                   0x05, lte_ml1_cswitch_sched_lted_grant_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, SS_DISABLE_DONE,
                   0x06, lte_ml1_cswitch_ss_disable_done_ind_s),  
                    
  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, TUNE_DONE,
                   0x07, lte_ml1_cswitch_tune_done_ind_s),  
  
  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, CSW_DONE,
                   0x08, lte_ml1_cswitch_csw_done_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, CHEST_DONE,
                   0x09, lte_ml1_cswitch_chest_done_ind_s),

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, GUARD_TIMER_EXPIRY,
                   0x0A, lte_ml1_cswitch_guard_timer_expiry_ind_s), 

  MSGR_DEFINE_UMID(LTE, ML1_CSWITCH, IND, CANCEL,
                   0x0B, lte_ml1_cswitch_cancel_ind_s),
  
};

#endif /*#ifdef FEATURE_CSWITCH_MODULE*/

#endif /* LTE_ML1_CSWITCH_MSG_H */
