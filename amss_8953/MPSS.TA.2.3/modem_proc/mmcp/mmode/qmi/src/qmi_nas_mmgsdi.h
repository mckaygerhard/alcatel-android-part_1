#ifndef _DS_QMI_NAS_MMGSDI_H_
#define _DS_QMI_NAS_MMGSDI_H_

/*===========================================================================

                         DS_QMI_NAS_MMGSDI.H

DESCRIPTION

 The Qualcomm Network Access Services MMGSDI Interface header file.

Copyright (c) 2010 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/mmcp.mpss/7.3.1/mmode/qmi/src/qmi_nas_mmgsdi.h#1 $ $DateTime: 2016/03/24 12:47:03 $ $Author: pwbldsvc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
06/03/10    hs     Initial version
===========================================================================*/

#endif // !_DS_QMI_NAS_MMGSDI_H_

