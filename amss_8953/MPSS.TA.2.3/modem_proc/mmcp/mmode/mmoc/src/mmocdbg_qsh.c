/*====*====*===*====*====*====*====*====*====*====*====*====*====*====*====*
               M U L T I M O D E   C O N T R O L L E R ( MMoC )

                 Q S H  L O G G I N G  F I L E

GENERAL DESCRIPTION
  The MMoC is responsible for logging the below critical imformation for debugging purposes .  
  the state information of MMOC , Which can describe the states of different protocols .
  It also have the data about the current transaction being processed . 


EXTERNALIZED FUNCTIONS

  Command Interface:

  Others:

REGIONAL FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  The MMoC task should be started before these APIs are called.


Copyright (c) 2002 - 2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/mmcp.mpss/7.3.1/mmode/mmoc/src/mmocdbg_qsh.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/09/15   KC      Initial release.

===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "mmoc_qsh_ext.h"   
#include "mmoci.h"   


#if defined(FEATURE_QSH_DUMP)
void mmocdbg_copy_subs_data
(
mmoc_dbg_subs_data_s_type* subs_data_ptr,
 mmoc_subs_data_type_e subs_type
 )MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void mmocdbg_copy_subsc_chgd
(
mmoc_dbg_cmd_subsc_chgd_s_type* subsc_chgd_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_copy_redir_ind_info
(
mmoc_dbg_redir_ind_trans_info_s_type* redir_ind_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_copy_gen_prot_cmd_param
(
mmoc_dbg_prot_gen_cmd_param_s_type* gen_cmd_param_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_copy_gen_prot_cmd
(
mmoc_dbg_gen_cmd_trans_info_s_type* gen_cmd_trans_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_copy_ho_ind_info
(
mmoc_dbg_ho_ind_trans_info_s_type* ho_ind_info_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_copy_tran_info
(
mmoc_dbg_trans_info_s_type* tran_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_dump_state_info
(
mmoc_dbg_state_info_s_type* dump_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void  mmocdbg_qsh_dump_dbg_buffer
(
mmoc_debug_buffer_dbg_s_type* dump_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;


void mmocdbg_qsh_copy_dump
(
qsh_client_cb_params_s *cb_params_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;

void  mmocdbg_qsh_cb
(
qsh_client_cb_params_s *cb_params_ptr
)MMOC_API_KEEP_UNCOMPRESSED_IN_ELF;






/*===========================================================================

FUNCTION mmocdbg_copy_subs_data

DESCRIPTION
  Dumps the SUBS tran info 

DEPENDENCIES
  mmocdbg_print_message()

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void mmocdbg_copy_subs_data
(mmoc_dbg_subs_data_s_type* subs_data_ptr,
 mmoc_subs_data_type_e subs_type)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
  mmoc_subs_data_s_type* src_subs_data_ptr = NULL;
  
  if(subs_type == MMOC_DBG_SUBS_CDMA)
  {
    src_subs_data_ptr = &mmoc_state_ptr->trans_info.subsc_chgd.cdma;
  }
  else if(subs_type == MMOC_DBG_SUBS_GW)
  {
    src_subs_data_ptr = &mmoc_state_ptr->trans_info.subsc_chgd.gw;
  }
  else if(subs_type == MMOC_DBG_SUBS_GW_HYBR)
  {
    src_subs_data_ptr = &mmoc_state_ptr->trans_info.subsc_chgd.gw_hybr;
  }
  else if(subs_type == MMOC_DBG_SUBS_GW_HYBR_3)
  {
    src_subs_data_ptr = &mmoc_state_ptr->trans_info.subsc_chgd.gw_hybr_3;
  }

  if(src_subs_data_ptr)
  {
    subs_data_ptr->as_id = src_subs_data_ptr->as_id;
    subs_data_ptr->nv_context = src_subs_data_ptr->nv_context;
    subs_data_ptr->session_type = src_subs_data_ptr->session_type;
    subs_data_ptr->is_subs_avail = src_subs_data_ptr->is_subs_avail;
    subs_data_ptr->is_perso_locked = src_subs_data_ptr->is_perso_locked;
    subs_data_ptr->is_subsc_chg = src_subs_data_ptr->is_subsc_chg;
    subs_data_ptr->ss = src_subs_data_ptr->ss;
    subs_data_ptr->orig_mode = src_subs_data_ptr->orig_mode;
    subs_data_ptr->mode_pref = src_subs_data_ptr->mode_pref;
    subs_data_ptr->band_pref = src_subs_data_ptr->band_pref;
    subs_data_ptr->roam_pref = src_subs_data_ptr->roam_pref;
    subs_data_ptr->hybr_pref = src_subs_data_ptr->hybr_pref;
    subs_data_ptr->lte_band_pref = src_subs_data_ptr->lte_band_pref;
    subs_data_ptr->tds_band_pref = src_subs_data_ptr->tds_band_pref;
    subs_data_ptr->subs_capability = src_subs_data_ptr->subs_capability;
    subs_data_ptr->curr_ue_mode = src_subs_data_ptr->curr_ue_mode;
    subs_data_ptr->is_ue_mode_substate_srlte = src_subs_data_ptr->is_ue_mode_substate_srlte;
  }
}


/*===========================================================================

FUNCTION mmocdbg_copy_subsc_chgd

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES
  mmocdbg_print_message()

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void mmocdbg_copy_subsc_chgd
(mmoc_dbg_cmd_subsc_chgd_s_type* subsc_chgd_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
  
  subsc_chgd_ptr->chg_type = mmoc_state_ptr->trans_info.subsc_chgd.chg_type;
  subsc_chgd_ptr->prot_subsc_chg = mmoc_state_ptr->trans_info.subsc_chgd.prot_subsc_chg;
  subsc_chgd_ptr->nam = mmoc_state_ptr->trans_info.subsc_chgd.nam;
  subsc_chgd_ptr->hybr_gw_subs_chg = mmoc_state_ptr->trans_info.subsc_chgd.hybr_gw_subs_chg;
  subsc_chgd_ptr->hybr_3_gw_subs_chg = mmoc_state_ptr->trans_info.subsc_chgd.hybr_3_gw_subs_chg;
  subsc_chgd_ptr->ds_pref = mmoc_state_ptr->trans_info.subsc_chgd.ds_pref;
  subsc_chgd_ptr->active_ss = mmoc_state_ptr->trans_info.subsc_chgd.active_ss;
  subsc_chgd_ptr->device_mode = mmoc_state_ptr->trans_info.subsc_chgd.device_mode;
  

  mmocdbg_copy_subs_data(&subsc_chgd_ptr->cdma,MMOC_DBG_SUBS_CDMA);
  mmocdbg_copy_subs_data(&subsc_chgd_ptr->gw,MMOC_DBG_SUBS_GW);
  mmocdbg_copy_subs_data(&subsc_chgd_ptr->gw_hybr,MMOC_DBG_SUBS_GW_HYBR);
  mmocdbg_copy_subs_data(&subsc_chgd_ptr->gw_hybr_3,MMOC_DBG_SUBS_GW_HYBR_3);
}

/*===========================================================================

FUNCTION mmocdbg_copy_redir_ind_info

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void mmocdbg_copy_redir_ind_info
(mmoc_dbg_redir_ind_trans_info_s_type* redir_ind_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
  mmoc_redir_ind_trans_info_s_type* 
    src_redir_ind_ptr = &mmoc_state_ptr->trans_info.redir_ind_info;

  redir_ind_ptr->ss = src_redir_ind_ptr->ss;
  redir_ind_ptr->prev_prot_state = src_redir_ind_ptr->prev_prot_state;
  redir_ind_ptr->cmd_info.actd_reason = src_redir_ind_ptr->cmd_info.actd_reason;
  redir_ind_ptr->cmd_info.prot_state = src_redir_ind_ptr->cmd_info.prot_state;
  
}


/*===========================================================================

FUNCTION mmocdbg_copy_gen_prot_cmd_param

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void mmocdbg_copy_gen_prot_cmd_param
(mmoc_dbg_prot_gen_cmd_param_s_type* gen_cmd_param_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();

  prot_gen_cmd_s_type* 
    src_gen_cmd_param_ptr = &mmoc_state_ptr->trans_info.gen_prot_cmd.cmd_info;

  prot_gen_cmd_e_type                  cmd_type = 
      mmoc_state_ptr->trans_info.gen_prot_cmd.cmd_info.cmd_type;

  if(cmd_type == PROT_GEN_CMD_PREF_SYS_CHGD)
  {
    gen_cmd_param_ptr->pref_sys_chgd.pref_reas = src_gen_cmd_param_ptr->param.pref_sys_chgd.pref_reas;
    gen_cmd_param_ptr->pref_sys_chgd.orig_mode = src_gen_cmd_param_ptr->param.pref_sys_chgd.orig_mode;
    gen_cmd_param_ptr->pref_sys_chgd.mode_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.mode_pref;
    gen_cmd_param_ptr->pref_sys_chgd.band_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.band_pref;
    gen_cmd_param_ptr->pref_sys_chgd.lte_band_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.lte_band_pref;
    gen_cmd_param_ptr->pref_sys_chgd.tds_band_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.tds_band_pref;
    gen_cmd_param_ptr->pref_sys_chgd.prl_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.prl_pref;
    gen_cmd_param_ptr->pref_sys_chgd.roam_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.roam_pref;
    gen_cmd_param_ptr->pref_sys_chgd.hybr_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.hybr_pref;
    gen_cmd_param_ptr->pref_sys_chgd.otasp_band = src_gen_cmd_param_ptr->param.pref_sys_chgd.otasp_band;
    gen_cmd_param_ptr->pref_sys_chgd.otasp_blksys = src_gen_cmd_param_ptr->param.pref_sys_chgd.otasp_blksys;
    gen_cmd_param_ptr->pref_sys_chgd.avoid_type = src_gen_cmd_param_ptr->param.pref_sys_chgd.avoid_type;
    gen_cmd_param_ptr->pref_sys_chgd.avoid_time = src_gen_cmd_param_ptr->param.pref_sys_chgd.avoid_time;

    gen_cmd_param_ptr->pref_sys_chgd.domain_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.domain_pref;
    gen_cmd_param_ptr->pref_sys_chgd.acq_order_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.acq_order_pref;
    gen_cmd_param_ptr->pref_sys_chgd.pref_update_reas = src_gen_cmd_param_ptr->param.pref_sys_chgd.pref_update_reas;

    gen_cmd_param_ptr->pref_sys_chgd.addl_action.action = 
       src_gen_cmd_param_ptr->param.pref_sys_chgd.addl_action.action;
    gen_cmd_param_ptr->pref_sys_chgd.addl_action.ue_mode = 
       src_gen_cmd_param_ptr->param.pref_sys_chgd.addl_action.ue_mode;
    gen_cmd_param_ptr->pref_sys_chgd.addl_action.is_ue_mode_substate_srlte = 
       src_gen_cmd_param_ptr->param.pref_sys_chgd.addl_action.is_ue_mode_substate_srlte;

    gen_cmd_param_ptr->pref_sys_chgd.user_mode_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.user_mode_pref;

    gen_cmd_param_ptr->pref_sys_chgd.rat_acq_order.version = 
      src_gen_cmd_param_ptr->param.pref_sys_chgd.rat_acq_order.version;
    gen_cmd_param_ptr->pref_sys_chgd.rat_acq_order.num_rat = 
      src_gen_cmd_param_ptr->param.pref_sys_chgd.rat_acq_order.num_rat;

    memscpy(gen_cmd_param_ptr->pref_sys_chgd.rat_acq_order.acq_sys_mode,
            sizeof(gen_cmd_param_ptr->pref_sys_chgd.rat_acq_order.acq_sys_mode),
            src_gen_cmd_param_ptr->param.pref_sys_chgd.rat_acq_order.acq_sys_mode,
            sizeof(src_gen_cmd_param_ptr->param.pref_sys_chgd.rat_acq_order.acq_sys_mode));

    gen_cmd_param_ptr->pref_sys_chgd.sys_sel_pref_req_id = src_gen_cmd_param_ptr->param.pref_sys_chgd.sys_sel_pref_req_id;
    gen_cmd_param_ptr->pref_sys_chgd.camp_mode_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.camp_mode_pref;
    gen_cmd_param_ptr->pref_sys_chgd.csg_id = src_gen_cmd_param_ptr->param.pref_sys_chgd.csg_id;
    gen_cmd_param_ptr->pref_sys_chgd.csg_rat = src_gen_cmd_param_ptr->param.pref_sys_chgd.csg_rat;
    gen_cmd_param_ptr->pref_sys_chgd.voice_domain_pref = src_gen_cmd_param_ptr->param.pref_sys_chgd.voice_domain_pref;

  }
  else if(cmd_type == PROT_GEN_CMD_GET_NETWORKS_GW)
  {
    gen_cmd_param_ptr->gw_get_net.mode_pref = src_gen_cmd_param_ptr->param.gw_get_net.mode_pref;
    gen_cmd_param_ptr->gw_get_net.band_pref = src_gen_cmd_param_ptr->param.gw_get_net.band_pref;
    gen_cmd_param_ptr->gw_get_net.lte_band_pref = src_gen_cmd_param_ptr->param.gw_get_net.lte_band_pref;
    gen_cmd_param_ptr->gw_get_net.tds_band_pref = src_gen_cmd_param_ptr->param.gw_get_net.tds_band_pref;
    gen_cmd_param_ptr->gw_get_net.network_list_type = src_gen_cmd_param_ptr->param.gw_get_net.network_list_type;
  }
  if(cmd_type == PROT_GEN_CMD_GW_SIM_STATE)
  {
    gen_cmd_param_ptr->gw_sim_state = src_gen_cmd_param_ptr->param.gw_sim_state;  
  }

}




/*===========================================================================

FUNCTION mmocdbg_copy_gen_prot_cmd

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void  mmocdbg_copy_gen_prot_cmd
(mmoc_dbg_gen_cmd_trans_info_s_type* gen_cmd_trans_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
  mmoc_gen_cmd_trans_info_s_type* 
    src_gen_cmd_trans_ptr = &mmoc_state_ptr->trans_info.gen_prot_cmd;

  gen_cmd_trans_ptr->is_activate_main = src_gen_cmd_trans_ptr->is_activate_main;

  gen_cmd_trans_ptr->cmd_info.cmd_type = src_gen_cmd_trans_ptr->cmd_info.cmd_type;
  gen_cmd_trans_ptr->cmd_info.trans_id = src_gen_cmd_trans_ptr->cmd_info.trans_id;
  gen_cmd_trans_ptr->cmd_info.ss = src_gen_cmd_trans_ptr->cmd_info.ss;
  gen_cmd_trans_ptr->cmd_info.prot_state = src_gen_cmd_trans_ptr->cmd_info.prot_state;

  mmocdbg_copy_gen_prot_cmd_param(&gen_cmd_trans_ptr->cmd_info.param);
  
  
}

/*===========================================================================

FUNCTION mmocdbg_copy_ho_ind_info

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void  mmocdbg_copy_ho_ind_info
(mmoc_dbg_ho_ind_trans_info_s_type* ho_ind_info_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
  mmoc_redir_ind_trans_info_s_type* 
    src_ho_ind_info_ptr = &mmoc_state_ptr->trans_info.redir_ind_info;

  ho_ind_info_ptr->ss = src_ho_ind_info_ptr->ss;
  ho_ind_info_ptr->prev_prot_state = src_ho_ind_info_ptr->prev_prot_state;
  ho_ind_info_ptr->cmd_info.actd_reason = src_ho_ind_info_ptr->cmd_info.actd_reason;
  ho_ind_info_ptr->cmd_info.prot_state = src_ho_ind_info_ptr->cmd_info.prot_state;
}


/*===========================================================================

FUNCTION mmocdbg_copy_tran_info

DESCRIPTION
  Dumps the MMOC tran info 

DEPENDENCIES

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void  mmocdbg_copy_tran_info
(mmoc_dbg_trans_info_s_type* tran_ptr)
{
  mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();

  mmocdbg_copy_subsc_chgd(&tran_ptr->subsc_chgd);
  mmocdbg_copy_gen_prot_cmd(&tran_ptr->gen_prot_cmd);
  mmocdbg_copy_redir_ind_info(&tran_ptr->redir_ind_info);

  mmocdbg_copy_ho_ind_info(&tran_ptr->ho_ind_info);

  tran_ptr->gen_deact_rpt.reason = mmoc_state_ptr->trans_info.gen_deact_rpt.reason;
  tran_ptr->gen_deact_rpt.trans_id = mmoc_state_ptr->trans_info.gen_deact_rpt.trans_id;


  tran_ptr->suspend_ss.ss = mmoc_state_ptr->trans_info.suspend_ss.ss;
  tran_ptr->suspend_ss.is_suspend = mmoc_state_ptr->trans_info.suspend_ss.is_suspend;
  tran_ptr->suspend_ss.ignore_protocol_activate = mmoc_state_ptr->trans_info.suspend_ss.ignore_protocol_activate;
  tran_ptr->suspend_ss.susp_reason = mmoc_state_ptr->trans_info.suspend_ss.susp_reason;

  tran_ptr->deact_from_dormant.ss = mmoc_state_ptr->trans_info.deact_from_dormant.ss;
  tran_ptr->deact_from_dormant.prot = mmoc_state_ptr->trans_info.deact_from_dormant.prot;
  tran_ptr->deact_from_dormant.is_activate_main = mmoc_state_ptr->trans_info.deact_from_dormant.is_activate_main;
}


/*===========================================================================

FUNCTION mmocdbg_dump_state_info

DESCRIPTION
  Dumps the MMOC state & mmoc_debug_buffer 

DEPENDENCIES
  mmocdbg_print_message()

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void  mmocdbg_dump_state_info
(mmoc_dbg_state_info_s_type* dump_ptr)
{
   
   mmoc_state_info_s_type* mmoc_state_ptr = mmoc_get_state_info_ptr();
   dump_ptr->curr_trans = mmoc_state_ptr->curr_trans;
   dump_ptr->trans_state = mmoc_state_ptr->trans_state;
 
 
   mmocdbg_copy_tran_info(&dump_ptr->trans_info);
 
   memscpy(dump_ptr->prot_state,sizeof(dump_ptr->prot_state),
            mmoc_state_ptr->prot_state,sizeof(mmoc_state_ptr->prot_state));
   
   dump_ptr->is_sd_initialized = mmoc_state_ptr->is_sd_initialized;
   dump_ptr->is_sd_init_called = mmoc_state_ptr->is_sd_init_called;
   dump_ptr->trans_id = mmoc_state_ptr->trans_id;
   dump_ptr->curr_nam = mmoc_state_ptr->curr_nam;
   dump_ptr->is_gwl_subs_avail = mmoc_state_ptr->is_gwl_subs_avail;
   dump_ptr->is_gw_hybr_subs_avail = mmoc_state_ptr->is_gw_hybr_subs_avail;
   dump_ptr->is_gw_hybr_3_subs_avail = mmoc_state_ptr->is_gw_hybr_3_subs_avail;
   dump_ptr->is_cdma_subs_avail = mmoc_state_ptr->is_cdma_subs_avail;
   dump_ptr->curr_oprt_mode = mmoc_state_ptr->curr_oprt_mode;
   dump_ptr->true_oprt_mode = mmoc_state_ptr->true_oprt_mode;
   dump_ptr->is_ph_stat_sent = mmoc_state_ptr->is_ph_stat_sent;
   dump_ptr->deact_req_index = mmoc_state_ptr->deact_req_index;
   dump_ptr->insanity_count = mmoc_state_ptr->insanity_count;
 
   dump_ptr->is_standby_sleep = mmoc_state_ptr->is_standby_sleep;
   dump_ptr->prot_subsc_chg = mmoc_state_ptr->prot_subsc_chg;
   dump_ptr->is_redir_allowed = mmoc_state_ptr->is_redir_allowed;
 
   memscpy(dump_ptr->prot_dormant,sizeof(dump_ptr->prot_dormant),
            mmoc_state_ptr->prot_dormant,sizeof(mmoc_state_ptr->prot_dormant));
 
   dump_ptr->hybr_gw_subs_chg = mmoc_state_ptr->hybr_gw_subs_chg;
   dump_ptr->hybr_3_gw_subs_chg = mmoc_state_ptr->hybr_3_gw_subs_chg;
   dump_ptr->prev_standby_pref = mmoc_state_ptr->prev_standby_pref;
   dump_ptr->standby_pref = mmoc_state_ptr->standby_pref;
   dump_ptr->prev_active_ss = mmoc_state_ptr->prev_active_ss;
   dump_ptr->active_ss = mmoc_state_ptr->active_ss;
   dump_ptr->device_mode = mmoc_state_ptr->device_mode;
   dump_ptr->prev_device_mode = mmoc_state_ptr->prev_device_mode;
   dump_ptr->max_sanity_time_multiple = mmoc_state_ptr->max_sanity_time_multiple;
   dump_ptr->is_scan_permission = mmoc_state_ptr->is_scan_permission;
   dump_ptr->hdr_deact_activate_ss = mmoc_state_ptr->hdr_deact_activate_ss;
 
   memscpy(dump_ptr->is_gwl_deact_sent,sizeof(dump_ptr->is_gwl_deact_sent),
            mmoc_state_ptr->is_gwl_deact_sent,sizeof(mmoc_state_ptr->is_gwl_deact_sent));
 
   memscpy(dump_ptr->is_suspend,sizeof(dump_ptr->is_suspend),
            mmoc_state_ptr->is_suspend,sizeof(mmoc_state_ptr->is_suspend));
 
   dump_ptr->is_buffer_auto_deact_ind = mmoc_state_ptr->is_buffer_auto_deact_ind;
   dump_ptr->last_stop_req_sent_timestamp = mmoc_state_ptr->last_stop_req_sent_timestamp;
   dump_ptr->onebuild_feature = mmoc_state_ptr->onebuild_feature;
 
   memscpy(dump_ptr->subs_feature,sizeof(dump_ptr->subs_feature),
            mmoc_state_ptr->subs_feature,sizeof(mmoc_state_ptr->subs_feature));
 
   memscpy(dump_ptr->subs_capability,sizeof(dump_ptr->subs_capability),
            mmoc_state_ptr->subs_capability,sizeof(mmoc_state_ptr->subs_capability));
   dump_ptr->ss_with_ps = mmoc_state_ptr->ss_with_ps;
   dump_ptr->dual_switch_ss = mmoc_state_ptr->dual_switch_ss;
   dump_ptr->main_last_prot_state = mmoc_state_ptr->main_last_prot_state;
   dump_ptr->emerg_susp_ss_mask = mmoc_state_ptr->emerg_susp_ss_mask;
#ifdef FEATURE_MMODE_SC_SVLTE
   dump_ptr->pri_slot = mmoc_state_ptr->pri_slot;
#endif   
} /* mmocdbg_dump_state_info */



/*===========================================================================

FUNCTION mmocdbg_qsh_dump_dbg_buffer

DESCRIPTION
  Dumps the mmoc_debug_buffer 

DEPENDENCIES
  mmocdbg_print_message()

RETURNS
  None

SIDE EFFECTS
  None

===========================================================================*/
void  mmocdbg_qsh_dump_dbg_buffer
(mmoc_debug_buffer_dbg_s_type* dump_ptr)
{
  mmoc_debug_buffer_s_type *mmoc_debug_buffer_ptr = mmoc_get_dbg_buff_ptr();
  uint8 cnt = 0;
  uint8 rpt_cnt = 0;
  dump_ptr->dbg_buf_idx = mmoc_debug_buffer_ptr->dbg_buf_idx;

  for(cnt=0;cnt<MMOC_DBG_MAX_DEBUG_BUFFER_SIZE;cnt++)
  {
    dump_ptr->dbg_buf[cnt].trans_id = mmoc_debug_buffer_ptr->dbg_buf[cnt].trans_id;
    dump_ptr->dbg_buf[cnt].trans_name = mmoc_debug_buffer_ptr->dbg_buf[cnt].trans_name;

    memscpy(dump_ptr->dbg_buf[cnt].addl_info,
            sizeof(dump_ptr->dbg_buf[cnt].addl_info) , 
            mmoc_debug_buffer_ptr->dbg_buf[cnt].addl_info,
            sizeof(mmoc_debug_buffer_ptr->dbg_buf[cnt].addl_info));
    
    for(rpt_cnt=0;rpt_cnt<MMOC_DBG_MAX_RPT_COUNT;rpt_cnt++)
    {
      dump_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].rpt_name = 
          mmoc_debug_buffer_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].rpt_name;

      dump_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].task_name = 
          mmoc_debug_buffer_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].task_name;
      
      memscpy(dump_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].prot_state, 
          sizeof(dump_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].prot_state),
          mmoc_debug_buffer_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].prot_state,
          sizeof(mmoc_debug_buffer_ptr->dbg_buf[cnt].rpt_queue[rpt_cnt].prot_state));
    }
  }
    
    
}

/*===========================================================================

FUNCTION mmocdbg_qsh_copy_dump

DESCRIPTION
  Copies the DUMP to the pointer sent by QSH .

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void mmocdbg_qsh_copy_dump(qsh_client_cb_params_s *cb_params_ptr)
{
  qsh_client_dump_collect_s *dump_params_ptr = &cb_params_ptr->action_params.dump_collect;

  if(dump_params_ptr->dump_iovec.size_bytes >= 
       sizeof(mmoc_qsh_dump_tag_mini_s_type))
  {
    if(QSH_DUMP_TAG_ENABLED(dump_params_ptr->dump_tag_mask,MMOC_QSH_DUMP_TAG_MINI))
    {
      qsh_client_action_done_s action_done;
      mmoc_qsh_dump_tag_mini_s_type *mini_dump = (mmoc_qsh_dump_tag_mini_s_type*)dump_params_ptr->dump_iovec.addr;
      
      qsh_client_dump_tag_hdr_init(&mini_dump->hdr,
                                   MMOC_QSH_DUMP_TAG_MINI,
                                   sizeof(mmoc_qsh_dump_tag_mini_s_type));
      
      mmocdbg_dump_state_info(&mini_dump->state_info);
      
      mmocdbg_qsh_dump_dbg_buffer(&mini_dump->dbg_buffer);

      qsh_client_action_done_init(&action_done);
      action_done.cb_params_ptr = cb_params_ptr; 
      action_done.params.dump_collect.size_written_bytes 
                = sizeof(mmoc_qsh_dump_tag_mini_s_type);
      action_done.action_mode_done = QSH_ACTION_MODE_DONE_SYNC;
      qsh_client_action_done(&action_done);
    }
  }
  
}


/*===========================================================================

FUNCTION mmocdbg_qsh_cb

DESCRIPTION
  Processes the QSH request .
  Currently we handle only QSH_ACTION_DUMP_COLLECT .
DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

Note
use MMOC_API_KEEP_UNCOMPRESSED_IN_ELF ,
which keeps the API in uncompressed section in th elf 
 this will be used by QSH dump collection ,
since after the crash happened uncompressed functions 
compressed functions wont be accesible 
===========================================================================*/
void  mmocdbg_qsh_cb(qsh_client_cb_params_s *cb_params_ptr)
{
  if(cb_params_ptr->action & QSH_ACTION_DUMP_COLLECT)
  {
    mmocdbg_qsh_copy_dump(cb_params_ptr);
  }
}

/*===========================================================================

FUNCTION mmocdbg_qsh_init

DESCRIPTION
  Initilize CM QSH interface.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/

void  mmocdbg_qsh_init()
{
  qsh_client_reg_s qsh_client;

  qsh_client_reg_init(&qsh_client);

  qsh_client.client = QSH_CLT_MMOC;
  qsh_client.major_ver = MMOC_QSH_MAJOR_VER;
  qsh_client.minor_ver = MMOC_QSH_MINOR_VER;
  qsh_client.client_cb_ptr = mmocdbg_qsh_cb;
  qsh_client.cb_action_support_mask = QSH_ACTION_DUMP_COLLECT; 

  qsh_client.dump_info.max_size_bytes = sizeof(mmoc_qsh_dump_tag_mini_s_type);
  
  qsh_client_reg(&qsh_client);
}



#endif
