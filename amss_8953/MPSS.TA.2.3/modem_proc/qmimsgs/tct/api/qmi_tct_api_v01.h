#ifndef TCT_SERVICE_H
#define TCT_SERVICE_H
/**
  @file qmi_tct_api_v01.h
  
  @brief This is the public header file which defines the tct service Data structures.

  This header file defines the types and structures that were defined in 
  tct. It contains the constant values defined, enums, structures,
  messages, and service message IDs (in that order) Structures that were 
  defined in the IDL as messages contain mandatory elements, optional 
  elements, a combination of mandatory and optional elements (mandatory 
  always come before optionals in the structure), or nothing (null message)
   
  An optional element in a message is preceded by a uint8_t value that must be
  set to true if the element is going to be included. When decoding a received
  message, the uint8_t values will be set to true or false by the decode
  routine, and should be checked before accessing the values that they
  correspond to. 
   
  Variable sized arrays are defined as static sized arrays with an unsigned
  integer (32 bit) preceding it that must be set to the number of elements
  in the array that are valid. For Example:
   
  uint32_t test_opaque_len;
  uint8_t test_opaque[16];
   
  If only 4 elements are added to test_opaque[] then test_opaque_len must be
  set to 4 before sending the message.  When decoding, the _len value is set 
  by the decode routine and should be checked so that the correct number of 
  elements in the array will be accessed. 

*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  

  
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====* 
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY 
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 5.1 
   It was generated on: Tue Dec 29 2015
   From IDL File: qmi_tct_api_v01.idl */

/** @defgroup tct_qmi_consts Constant values defined in the IDL */
/** @defgroup tct_qmi_msg_ids Constant values for QMI message IDs */
/** @defgroup tct_qmi_enums Enumerated types used in QMI messages */
/** @defgroup tct_qmi_messages Structures sent as QMI messages */
/** @defgroup tct_qmi_aggregates Aggregate types used in QMI messages */
/** @defgroup tct_qmi_accessor Accessor for QMI service object */
/** @defgroup tct_qmi_version Constant values for versioning information */

#include <stdint.h>
#include "qmi_idl_lib.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup tct_qmi_version 
    @{ 
  */ 
/** Major Version Number of the IDL used to generate this file */
#define TCT_V01_IDL_MAJOR_VERS 0x01
/** Revision Number of the IDL used to generate this file */
#define TCT_V01_IDL_MINOR_VERS 0x00
/** Major Version Number of the qmi_idl_compiler used to generate this file */
#define TCT_V01_IDL_TOOL_VERS 0x05
/** Maximum Defined Message ID */
#define TCT_V01_MAX_MESSAGE_ID 0x0030;
/** 
    @} 
  */


/** @addtogroup tct_qmi_consts 
    @{ 
  */
#define QMI_RETROFIT_NV_ITEM_DATA_MAX_SIZE_V01 4096
#define FS_PATH_MAX_V01 1025
#define QMI_NV_DIAG_ITEM_SIZE_V01 1024
#define QMI_NV_DIAG_FILE_PATH_V01 120
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Request Message; Test tct qmi server */
typedef struct {

  /* Mandatory */
  /*  Ping */
  char ping[4];
}test_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Response Message; Test tct qmi server */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  char pong[4];
}test_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Indication Message; Test tct qmi server */
typedef struct {

  /* Mandatory */
  /*  Interruption */
  char hello[5];
}test_ind_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Request Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Master message */
  char efs_path[FS_PATH_MAX_V01];
}retrofit_nv_read_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Response Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  uint32_t nv_data_len;

  /* Mandatory */
  uint8_t result;

  /* Mandatory */
  uint8_t nv_data[QMI_RETROFIT_NV_ITEM_DATA_MAX_SIZE_V01];
  /**<    Length of data
        - Type: Unsigned integer  */
}retrofit_nv_read_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Request Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Master message */
  uint32_t item;

  /* Mandatory */
  uint8_t file[QMI_NV_DIAG_FILE_PATH_V01];

  /* Mandatory */
  uint16_t item_size;

  /* Mandatory */
  uint16_t compress_size;

  /* Mandatory */
  uint32_t operation;

  /* Mandatory */
  uint8_t value[QMI_NV_DIAG_ITEM_SIZE_V01];

  /* Mandatory */
  uint32_t result;
}nvdiag_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Response Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  uint32_t item;

  /* Mandatory */
  uint8_t file[QMI_NV_DIAG_FILE_PATH_V01];

  /* Mandatory */
  uint16_t item_size;

  /* Mandatory */
  uint16_t compress_size;

  /* Mandatory */
  uint32_t operation;

  /* Mandatory */
  uint8_t value[QMI_NV_DIAG_ITEM_SIZE_V01];

  /* Mandatory */
  uint32_t result;
}nvdiag_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Request Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Master message */
  uint8_t path[FS_PATH_MAX_V01];

  /* Mandatory */
  uint32_t result;
}getlist_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup tct_qmi_messages
    @{
  */
/** Response Message; CIQ master/slave bridge */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  uint32_t num;

  /* Mandatory */
  uint8_t fileList[FS_PATH_MAX_V01];

  /* Mandatory */
  uint32_t result;
}getlist_resp_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup tct_qmi_msg_ids
    @{
  */
#define QMI_PING_REQ_V01 0x0001
#define QMI_PING_RESP_V01 0x0001
#define QMI_HELLO_IND_V01 0x0001
#define QMI_RETROFIT_NV_READ_REQ_V01 0x0010
#define QMI_RETROFIT_NV_READ_RESP_V01 0x0010
#define QMI_NV_DIAG_REQ_V01 0x0020
#define QMI_NV_DIAG_RESP_V01 0x0020
#define QMI_GETLIST_REQ_V01 0x0030
#define QMI_GETLIST_RESP_V01 0x0030
/**
    @}
  */

/* Service Object Accessor */
/** @addtogroup wms_qmi_accessor 
    @{
  */
/** This function is used internally by the autogenerated code.  Clients should use the
   macro tct_get_service_object_v01( ) that takes in no arguments. */
qmi_idl_service_object_type tct_get_service_object_internal_v01
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version );
 
/** This macro should be used to get the service object */ 
#define tct_get_service_object_v01( ) \
          tct_get_service_object_internal_v01( \
            TCT_V01_IDL_MAJOR_VERS, TCT_V01_IDL_MINOR_VERS, \
            TCT_V01_IDL_TOOL_VERS )
/** 
    @} 
  */


#ifdef __cplusplus
}
#endif
#endif

