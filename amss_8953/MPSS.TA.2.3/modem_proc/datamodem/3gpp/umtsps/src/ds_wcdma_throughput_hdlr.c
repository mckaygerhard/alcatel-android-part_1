/*!
  @file
  ds_wcdma_throughput_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.

*/



/*===========================================================================

  Copyright (c) 2009-2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/data.mpss/3.4.3.1/3gpp/umtsps/src/ds_wcdma_throughput_hdlr.c#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------

==============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"


#include "ds_wcdma_throughput_hdlr.h"
#include "rlcdsapif.h"
#ifdef FEATURE_SEGMENT_LOADING
#include "IWCDMA.h"
/*---------------------------------------------------------------------------
WCDMA interface tables
---------------------------------------------------------------------------*/

extern interface_t *ds_3gpp_ps_wcdma_tbl;
#endif /* FEATURE_SEGMENT_LOADING */


/*===========================================================================
FUNCTION  DS_WCDMA_CHECK_AND_UPDATE_L1_TPUT_STATUS

DESCRIPTION
  This function checks the current status of WCDMA L1 reporting status and
  sends (start/stop) WCDMA L1 request if required

  If report frequency is 0, this function and reporting is going on
  this function will send to stop report request .
  If reporting frequcny is positive and reporting is stop, this function
  will send start request and return TRUE

PARAMETERS
  subs_id - sys_modem_as_id_e_type (Subscription ID)
  need_to_report - TRUE if reporting needs to start, FALSE otherwise

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/

void ds_wcdma_check_and_update_l1_tput_status
(
   sys_modem_as_id_e_type subs_id,
   boolean need_to_report
)
{
#if 0
  boolean l1_response = FALSE;
  wcdma_qual_estimation_report_state report_state = STOP_REPORTING;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DS_3GPP_MSG1_ERROR("Invalid CM sub ID %d",subs_id);
    return;
  }

  if (need_to_report == FALSE) 
  {
    report_state = STOP_REPORTING;
  }
  else
  {
    report_state = START_REPORTING;
  }
#ifdef FEATURE_DATA_WCDMA_DOWNLINK_STATS
#ifdef FEATURE_SEGMENT_LOADING
  if(ds_3gpp_ps_wcdma_tbl != NULL)
  {
    l1_response = IWCDMA_wcdma_qual_estimation_change_report_state(
       ds_3gpp_ps_wcdma_tbl, report_state, subs_id);
  }
  else
  {
    DS_3GPP_MSG0_MED("WCDMA segment is not loaded");
  }
#else
  l1_response = wcdma_qual_estimation_change_report_state(report_state, subs_id);
#endif
  DS_3GPP_MSG3_HIGH("wcdma l1 response is %d for report_state: %d and subs_id %d",
                    l1_response, report_state, subs_id);
#endif
#else
    DS_3GPP_MSG0_LOW("WCDMA DOWNLINK STATS is not enabled");
#endif
}


/*===========================================================================
FUNCTION  DS_WCDMA_GET_ACTUAL_REPORT_INTERVAL

DESCRIPTION
  This function checks with WCDMA l1 for actual reporting interval based on
  requested interval from client

PARAMETERS 
  requested_interval - client requested interval 
  subs_id - sys_modem_as_id_e_type (Subscription ID)

DEPENDENCIES
  None.

RETURN VALUE
  actual interval

SIDE EFFECTS
  None.

===========================================================================*/

uint32 ds_wcdma_get_actual_report_interval //Set
(
   sys_modem_as_id_e_type subs_id,
   uint32 requested_interval
)
{
  uint32 actual_interval = 0;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return actual_interval;
  }

#ifdef FEATURE_DATA_WCDMA_DOWNLINK_STATS
#ifdef FEATURE_SEGMENT_LOADING
  if(ds_3gpp_ps_wcdma_tbl != NULL)
  {
    actual_interval = (uint32)IWCDMA_wcdma_qual_estimation_set_report_period(
               ds_3gpp_ps_wcdma_tbl, (uint16)requested_interval, subs_id);
  }
  else
  {
    actual_interval = DS3G_INVALID_ACTUAL_INTERVAL;
    DS_3GPP_MSG0_LOW("WCDMA segment is not loaded");
  }
#else
  actual_interval = (uint32)wcdma_qual_estimation_set_report_period(
      (uint16)requested_interval, subs_id);
#endif
#else
    DS_3GPP_MSG0_LOW("WCDMA DOWNLINK STATS is not enabled");
#endif

  DS_3GPP_MSG3_HIGH("wcdma actual_interval is %d for requested_interval: %d and subs_id %d",
                    actual_interval, requested_interval, subs_id);

  return actual_interval;
}
