
/*!
  @file
  ds_umts_tlb_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2015-2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/data.mpss/3.4.3.1/3gpp/umtsps/src/ds_umts_tlb_hdlr.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/10/15   ss       Initial version
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"
#include "sys.h"
#include <stringl/stringl.h>
#include "cm.h"

#include "ps_iface.h"
#include "ps_ifacei.h"
#include "ds_3gpp_pdn_context.h"
#include "ds_3gpp_bearer_context.h"
#include "ds_3gpp_tlb_rm_hdlr.h"
#include "ds_umts_bearer_context.h"
#include "ds_umts_tlb_hdlr.h"
#include "dsumtspdpreg.h"
#include "ds3gcmif.h"
#include "ds_eps_pdn_context.h"

/* UMTS TLB Global variables */
/*----------------------------------------------------------------------------- 
  Array of pointers to instances of Test Loopback Mode Info Block.
------------------------------------------------------------------------------*/
static ds_umts_tlb_per_subs_info_type 
         *ds_umts_tlb_info_p[DS_3GPP_TLB_MAX_INSTANCES] = {NULL};

/*---------------------------------------------------------------------------- 
  Defining the iface ev that eps_tlb is interested in
  ---------------------------------------------------------------------------*/
#define DS_UMTS_TLB_IFACE_EV_CNT 2
static ps_iface_event_enum_type iface_ev_p[DS_UMTS_TLB_IFACE_EV_CNT] = 
  {IFACE_DOWN_EV,IFACE_UP_EV};


#define DS_UMTS_TLB_PHYS_LINK_EV_CNT 2
static ps_iface_event_enum_type phys_link_ev_p[DS_UMTS_TLB_PHYS_LINK_EV_CNT] = 
  {PHYS_LINK_FLOW_ENABLED_EV,PHYS_LINK_FLOW_DISABLED_EV};



#define DS_UMTS_TLB_BRIDGE_IFACE(iface_ptr, bridge_iface_ptr, tlb_hdl, index) \
	bridge_iface_ptr = ps_iface_bridge_iface(iface_ptr); \
	if (PS_IFACE_IS_VALID(bridge_iface_ptr) &&           \
		(bridge_iface_ptr ==                              \
	  	&(tlb_hdl->rmsm_state.rm_lo_b_iface[index]))) \
    {                                                              \
      DS_3GPP_MSG0_HIGH("UM iface ptr already bridged to lo_rm_iface"); \
	  return;                                                           \
    }                                                                   \
	else                       \
    {                                                                   \
      ds_umts_tlb_bridge_um_rm(bridge_iface_ptr, iface_ptr,      \
	  	                       &(tlb_hdl->rmsm_state.rm_lo_b_iface[index])); \
    }

#define DS_UMTS_TLB_IS_IFACE_READY(iface_ptr, iface_ready) \
  if ( (!PS_IFACE_IS_VALID(iface_ptr)) || \
  	   (PS_IFACE_IS_VALID(iface_ptr) && \
        (IFACE_UP == iface_ptr->iface_private.state || \
         IFACE_DOWN == iface_ptr->iface_private.state)) ) \
  { \
    iface_ready = TRUE; \
  }

#define DS_UMTS_TLB_IS_NSAPI_MASK_SET(nsapi_mask, nsapi) \
    (nsapi_mask & ((uint16)(1 << nsapi)))

#define DS_UMTS_TLB_SET_NSAPI_MASK(nsapi_mask, nsapi) \
	(nsapi_mask |= (1 << nsapi))

static void ds_umts_tlb_handle_tlb_activation
(
  sys_modem_as_id_e_type subs_id
);

static void ds_umts_tlb_free_hndl
(
  ds_umts_tlb_per_subs_info_type *tlb_hdl
);

static void ds_umts_tlb_bridge_um_rm
(
  ps_iface_type *current_bridge_iface_ptr,
  ps_iface_type *this_iface_ptr,
  ps_iface_type *new_bridge_iface_ptr
);

void ds_umts_tlb_handle_tlb_deactivation
(
  sys_modem_as_id_e_type subs_id
);

void ds_umts_tlb_handle_lpm_mode
(
  sys_modem_as_id_e_type subs_id
);

void ds_umts_tlb_reset_rm_loopback_mode
(
  ds_umts_tlb_per_subs_info_type *tlb_hndl
);

void ds_umts_tlb_um_iface_down_event_hdlr
(
  ps_iface_type  *iface_ptr,
  void           *tlb_hndl
);

void ds_umts_tlb_um_iface_up_event_hdlr
(
  ps_iface_type  *iface_ptr,
  void           *tlb_hndl
);

void ds_umts_tlb_um_iface_evt_hdlr
(
  ds_3gpp_tlb_iface_event_type iface_evt_info,
  ds_umts_tlb_info_type_s      *tlb_hndl
);

void ds_umts_tlb_iface_evt_cb
(
  ps_iface_type			     *this_iface_ptr,
  ps_iface_event_enum_type   event,
  ps_iface_event_info_u_type event_info,
  void					     *user_data_ptr
);

void ds_umts_tlb_process_open_tlb
(
  sys_modem_as_id_e_type subs_id
);

int ds_umts_tlb_receive_data_from_network
(
  void				   *tlb_hndl,
  ps_iface_type		   *this_iface_ptr,
  dsm_item_type		   **item_ptr,
  ps_meta_info_type	   *meta_info_ptr,
  sys_modem_as_id_e_type subs_id
);

void ds_umts_tlb_process_rx_data
(
  ps_iface_type          *rm_iface_ptr
);

ds_umts_tlb_per_subs_info_type *ds_umts_tlb_get_hndl
(
  sys_modem_as_id_e_type  subs_id,
  boolean                 free_hndl
)
{
  uint8  index=0;
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  
  for (index=0; index < DS_3GPP_TLB_MAX_INSTANCES; index++)
  {
    tlb_hdl = ds_umts_tlb_info_p[index];

    if (TRUE == free_hndl)
    {
	  if ( (NULL != tlb_hdl) &&
	       (FALSE == tlb_hdl->in_use) )
	  {
	    break;
	  }
    }
	else
    {
      if ( (NULL != tlb_hdl) &&
	       (subs_id == tlb_hdl->subs_id) )
	  {
	    break;
	  }
    }

	tlb_hdl = NULL;
	  
  }

  return tlb_hdl;
}

static void ds_umts_tlb_free_hndl
(
  ds_umts_tlb_per_subs_info_type *tlb_hdl
)
{
  if (NULL == tlb_hdl)
  {
    DS_3GPP_MSG0_ERROR("Invalid tlb handle passed");
	return;
  }

  tlb_hdl->in_use = FALSE;
  tlb_hdl->subs_id = SYS_MODEM_AS_ID_NONE;
  ds3gpp_enter_global_crit_section();
  tlb_hdl->umts_tlb_info.test_mode_active = FALSE;
  tlb_hdl->umts_tlb_info.tlb_mode = CM_TEST_CONTROL_TYPE_NO_TEST;
  tlb_hdl->umts_tlb_info.nsapi_mask = 0;
  tlb_hdl->umts_tlb_info.tlb_mode_closed = FALSE;
  tlb_hdl->umts_tlb_info.wm_reg_complete = FALSE;
  ds3gpp_leave_global_crit_section();
}

void ds_umts_tlb_handle_oprt_mode_ev
(
  sys_oprt_mode_e_type        oprt_mode,
  sys_modem_as_id_e_type      subs_id,
  cm_test_control_type_e_type test_control_type
)
{
  ds_umts_tlb_per_subs_info_type  *tlb_hndl;

  tlb_hndl = ds_umts_tlb_get_hndl(subs_id, FALSE);

  DS_3GPP_MSG3_HIGH("UMTS TLB: handle oprt_mode: %d, subs_id: %d, test_control_type: %d",
  	                 oprt_mode, subs_id, test_control_type);

  if (SYS_OPRT_MODE_NET_TEST_GW == oprt_mode)
  {
    if (NULL == tlb_hndl)
    {
      ds_umts_tlb_handle_tlb_activation(subs_id);
    }
  }
  else if (SYS_OPRT_MODE_NET_TEST_GW != oprt_mode &&
  	       CM_TEST_CONTROL_TYPE_NO_TEST == test_control_type)
  {
    if (NULL != tlb_hndl &&
		tlb_hndl->umts_tlb_info.test_mode_active == TRUE)
    {
      ds_umts_tlb_handle_tlb_deactivation(subs_id);
    }
  }
  else if (SYS_OPRT_MODE_LPM == oprt_mode ||
  	       SYS_OPRT_MODE_OFFLINE == oprt_mode)
  {
    ds_umts_tlb_handle_lpm_mode(subs_id);
  }
}

void ds_umts_tlb_handle_test_control_ev
(
  cm_test_control_type_e_type mode,
  sys_modem_as_id_e_type	  subs_id
)
{
  DS_3GPP_MSG2_HIGH("Received test Control ev: %d for subs: %d",
  	                 mode, subs_id);
  switch (mode)
  {
    case CM_TEST_CONTROL_TYPE_LB_MODE4:
      ds_umts_tlb_process_close_tlb(subs_id);
	  break;

	case CM_TEST_CONTROL_TYPE_OPEN_LB_MODE4:
      ds_umts_tlb_process_open_tlb(subs_id);
	  break;

    default:
      DS_3GPP_MSG1_HIGH("Received Loopback mode:%d, not processing",
	  	                 mode);
	  break;
  }
}


static void ds_umts_tlb_handle_tlb_activation
(
  sys_modem_as_id_e_type subs_id
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  DS_3GPP_MSG3_HIGH("Test Loopback activation on subs_id: %d", subs_id, 0, 0);

  if (NULL != (tlb_hdl = ds_umts_tlb_get_hndl(subs_id, TRUE)))
  {
    tlb_hdl->in_use = TRUE;
	tlb_hdl->subs_id = subs_id;
	tlb_hdl->umts_tlb_info.test_mode_active = TRUE;
	DS_3GPP_MSG1_HIGH("Setting subs_id: %d in tlb hndl", tlb_hdl->subs_id);
  }
  else
  {
    DS_3GPP_MSG0_ERROR("Couldn't find a free umts_tlb_hndl");
  }
}

static void ds_umts_tlb_bridge_um_rm
(
  ps_iface_type *current_bridge_iface_ptr,
  ps_iface_type *this_iface_ptr,
  ps_iface_type *new_bridge_iface_ptr
)
{
  if (!PS_IFACE_IS_VALID(this_iface_ptr))
  {
    DS_3GPP_MSG0_ERROR("Invalid iface_ptr passed");
	return;
  }

  if (PS_IFACE_IS_VALID(current_bridge_iface_ptr))
  {
    DS_3GPP_MSG3_HIGH("Current bridge iface: 0x%x:%d is valid",
		               current_bridge_iface_ptr->name,
		               current_bridge_iface_ptr->instance, 0);

	/* Reset current Bridge iface */
	ps_iface_set_bridge(current_bridge_iface_ptr, NULL);
	ps_iface_set_bridge(this_iface_ptr, NULL);
  }

  ps_iface_set_bridge(new_bridge_iface_ptr, this_iface_ptr); //RM to UM
  ps_iface_set_bridge(this_iface_ptr, new_bridge_iface_ptr); // UM to RM
}

void ds_umts_tlb_handle_tlb_deactivation
(
  sys_modem_as_id_e_type subs_id
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  DS_3GPP_MSG3_HIGH("Test Loopback deactivation on subs_id: %d", subs_id, 0, 0);

  if ( (NULL != (tlb_hdl = ds_umts_tlb_get_hndl(subs_id, FALSE))) &&
       (tlb_hdl->umts_tlb_info.test_mode_active == TRUE) )
  {
    if (tlb_hdl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4)
    {
      DS_3GPP_MSG0_HIGH("Loopback Mode4 deactivation");
      ds_3gpp_tlb_teardown_um_iface(&(tlb_hdl->rmsm_state),                                                                        ds_umts_tlb_um_iface_down_event_hdlr);
    }
    else
    {
      DS_3GPP_MSG0_HIGH("Other loopback Mode deactivation");
      ds_umts_tlb_free_hndl(tlb_hdl);
    }
  }
}

void ds_umts_tlb_handle_lpm_mode
(
  sys_modem_as_id_e_type subs_id
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  DS_3GPP_MSG3_HIGH("Test Loopback cleanup on subs_id: %d", subs_id, 0, 0);

  if (NULL != (tlb_hdl = ds_umts_tlb_get_hndl(subs_id, FALSE)))
  {
    if (tlb_hdl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4)
    {
      ds_umts_tlb_reset_rm_loopback_mode(tlb_hdl);
    }
    ds_umts_tlb_free_hndl(tlb_hdl);
  }
  else
  {
    DS_3GPP_MSG0_ERROR("Couldn't find a free umts_tlb_hndl");
  }
}

void ds_umts_tlb_register_uplink_wm
(
  ps_iface_type                  *iface_ptr,
  ds_umts_tlb_per_subs_info_type *tlb_hndl
)
{
  uint8                bearer_index;
  ps_iface_type        *base_iface_ptr;
  ds_3gpp_iface_s      *ds3gpp_iface_p;
  ds_pdn_context_s     *pdn_cntxt_p;
  ds_bearer_context_s  *bearer_cntxt_p;
  
  if (!PS_IFACE_IS_VALID(iface_ptr) || (NULL == tlb_hndl))
  {
    DS_3GPP_MSG0_ERROR("Invalid iface passed");
	return;
  }

  base_iface_ptr = ps_iface_get_base_iface(iface_ptr);

  if (!PS_IFACE_IS_VALID(base_iface_ptr))
  {
    DS_3GPP_MSG0_ERROR("Invalid base iface ptr");
	return;
  }

  ds3gpp_iface_p = (ds_3gpp_iface_s *)(base_iface_ptr->client_data_ptr);

  if (NULL == ds3gpp_iface_p)
  {
    DS_3GPP_MSG0_ERROR("Invalid DS iface_ptr");
	return;
  }

  pdn_cntxt_p = (ds_pdn_context_s *)(ds3gpp_iface_p->client_data_ptr);

  if (!ds_3gpp_pdn_cntx_validate_pdn_context(pdn_cntxt_p))
  {
    DS_3GPP_MSG0_ERROR("Invalid PDN context from ds iface");
	return;
  }

  for (bearer_index = 0; 
       bearer_index < DS_3GPP_MAX_BEARER_CONTEXT_PER_PDN_CONTEXT; bearer_index++)
  {
    bearer_cntxt_p = pdn_cntxt_p->ds_pdn_context_dyn_p-> \
		                 bearer_context_ptr_tbl[bearer_index];

	if (ds_bearer_cntx_validate_bearer_context(bearer_cntxt_p) &&
		(bearer_cntxt_p->ds_bearer_context_dyn_p->state == DS_BEARER_CONTEXT_STATE_UP ||
		 bearer_cntxt_p->ds_bearer_context_dyn_p->state == DS_BEARER_CONTEXT_STATE_UP_DORMANT ||
		 bearer_cntxt_p->ds_bearer_context_dyn_p->state == DS_BEARER_CONTEXT_STATE_UP_DORMANT_REESTAB))
    {
      DS_3GPP_MSG3_HIGH("Bearer inst: %d is in state: %d, register uplink WM, nsapi: %d",
	  	                 bearer_cntxt_p->ds_bearer_context_dyn_p->index,
	  	                 bearer_cntxt_p->ds_bearer_context_dyn_p->state,
	  	                 bearer_cntxt_p->ds_bearer_context_dyn_p->nsapi);
	  
	  ds3gpp_enter_global_crit_section();
	  if (!DS_UMTS_TLB_IS_NSAPI_MASK_SET(tlb_hndl->umts_tlb_info.nsapi_mask,  
	  	                                bearer_cntxt_p->ds_bearer_context_dyn_p->nsapi))
	  {
	    DS_3GPP_MSG2_HIGH("nsapi: %d mask not set, nsapi_curr_mask: %d register WM", 
			               bearer_cntxt_p->ds_bearer_context_dyn_p->nsapi,
			               tlb_hndl->umts_tlb_info.nsapi_mask);
	    DS_UMTS_TLB_SET_NSAPI_MASK(tlb_hndl->umts_tlb_info.nsapi_mask,
			                       bearer_cntxt_p->ds_bearer_context_dyn_p->nsapi);
		tlb_hndl->umts_tlb_info.wm_reg_complete = FALSE;
	    ds3gpp_leave_global_crit_section();

        DS_UMTS_BEARER_CTXT_VF_CALL_NO_ARG_PER_BEARER(register_wm, bearer_cntxt_p);
      }
	  else
	  {
	    DS_3GPP_MSG1_HIGH("nsapi: %d mask already set, don't register WM again", 
			               bearer_cntxt_p->ds_bearer_context_dyn_p->nsapi);
	    ds3gpp_leave_global_crit_section();
	  }
    }
  }
  
}

void ds_umts_tlb_wm_reg_cnf
(
  uint8                  nsapi,
  sys_modem_as_id_e_type subs_id
)
{
  boolean       post_close_ready = FALSE;
  ds_cmd_type   *cmd_ptr;
  static uint16 nsapi_local_mask;
  ds_umts_tlb_per_subs_info_type *tlb_hndl;

  if (NULL == (tlb_hndl = ds_umts_tlb_get_hndl(subs_id,FALSE)))
  {
    DS_3GPP_MSG1_ERROR("Invalid tlb_hndl for subs_id: %d", subs_id);
	return;
  }
  
  ds3gpp_enter_global_crit_section();
  DS_UMTS_TLB_SET_NSAPI_MASK(nsapi_local_mask, nsapi);

  if (tlb_hndl->umts_tlb_info.nsapi_mask == nsapi_local_mask)
  {
    DS_3GPP_MSG1_HIGH("All WM reg cnf recieved. nsapi: %d",
		               nsapi_local_mask);
    tlb_hndl->umts_tlb_info.wm_reg_complete = TRUE;
	post_close_ready = TRUE;
	nsapi_local_mask = 0;
  }
  ds3gpp_leave_global_crit_section();

  if (post_close_ready)
  {
    cmd_ptr = ds_allocate_cmd_buf(sizeof(sys_modem_as_id_e_type));
    if( (cmd_ptr == NULL) || (cmd_ptr->cmd_payload_ptr == NULL) )
    {
      ASSERT(0);
      return;
    }

	cmd_ptr->hdr.cmd_id = DS_CMD_3GPP_UMTS_LB_CLOSE_READY;
	memscpy(cmd_ptr->cmd_payload_ptr, sizeof(sys_modem_as_id_e_type), 
		    &subs_id, sizeof(sys_modem_as_id_e_type));
	ds_put_cmd(cmd_ptr);
  }
}

void ds_umts_tlb_post_close_ready_ind
(
  sys_modem_as_id_e_type subs_id
)
{
  ps_iface_type             *v4_iface_ptr;
  ps_iface_type             *v6_iface_ptr;
  boolean                    v4_iface_ready = FALSE;
  boolean                    v6_iface_ready = FALSE;
  ds_umts_tlb_per_subs_info_type *tlb_hndl;

  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DS_3GPP_MSG1_ERROR("Invalid subs_id passed: %d", subs_id);
	return;
  }

  if (NULL == (tlb_hndl = ds_umts_tlb_get_hndl(subs_id, FALSE)))
  {
    DS_3GPP_MSG1_ERROR("Invalid tlb_hndl for subs_id: %d", subs_id);
	return;
  }

#ifdef FEATURE_WCDMA  
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_WCDMA, receive_data_from_network, \
                        ds_umts_tlb_receive_data_from_network);
  
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_WCDMA, process_rx_data, \
                        ds_umts_tlb_process_rx_data);  
#endif /* FEATURE_WCDMA */
  
#ifdef FEATURE_GSM_GPRS  
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_GSM, receive_data_from_network, \
                        ds_umts_tlb_receive_data_from_network);
  
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_GSM, process_rx_data, \
                        ds_umts_tlb_process_rx_data);  
#endif /* FEATURE_GSM_GPRS */
  
#ifdef FEATURE_TDSCDMA 
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_TDS, receive_data_from_network, \
                        ds_umts_tlb_receive_data_from_network);
  
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_WCDMA, process_rx_data, \
                        ds_umts_tlb_process_rx_data);
#endif /* FEATURE_TDSCDMA */

  v4_iface_ptr = tlb_hndl->rmsm_state.um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE];
  v6_iface_ptr = tlb_hndl->rmsm_state.um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE];

  DS_UMTS_TLB_IS_IFACE_READY(v4_iface_ptr, v4_iface_ready)

  DS_UMTS_TLB_IS_IFACE_READY(v6_iface_ptr, v6_iface_ready)

  if (v4_iface_ready && v6_iface_ready)
  {
    tlb_hndl->umts_tlb_info.tlb_mode_closed = TRUE;
    ds3g_send_tlb_close_ready_ind(TRUE, subs_id);
  }
  else
  {
    DS_3GPP_MSG0_MED("Not posting TLB CLOSED IND. "
                    "Waiting for companion iface to come up");
  }

}

void ds_umts_tlb_um_iface_up_event_hdlr
(
  ps_iface_type  *iface_ptr,
  void           *tlb_hndl
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  ps_iface_type                  *bridge_iface_ptr = NULL;
  uint8                           instance = DS_3GPP_TLB_IPV4_IFACE;
  uint8                     other_instance = DS_3GPP_TLB_IPV4_IFACE;
  ps_iface_type             *other_iface_ptr = NULL;
  ps_iface_state_enum_type  iface_state;

  tlb_hdl = (ds_umts_tlb_per_subs_info_type *)tlb_hndl;

  if (!(PS_IFACE_IS_VALID(iface_ptr)) ||
  	  NULL == tlb_hdl)
  {
    DS_3GPP_MSG0_ERROR("iface_ptr or tlb_inst passed is invalid");
	return;
  }
  
  /* Find the instance form um iface pointer */
  DS3GPPTLB_GET_INST_FROM_UM_IFACE(iface_ptr,instance,tlb_hdl);

  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_3GPP_MSG0_HIGH("Not a loopback UM iface, return without processing");
    return;
  }
  
    /* Get the UM iface ptr (s), its the logical iface */
    /* Unbridge exisiting association */
    /* Bridge with loopback rm_iface */
    /* Send CLose Ready Ind */

  if (ps_iface_addr_family_is_v4(iface_ptr))
  {
    DS_UMTS_TLB_BRIDGE_IFACE(iface_ptr, bridge_iface_ptr, 
		                     tlb_hdl, DS_3GPP_TLB_IPV4_IFACE)
  }

  if (ps_iface_addr_family_is_v6(iface_ptr))
  {
    DS_UMTS_TLB_BRIDGE_IFACE(iface_ptr, bridge_iface_ptr, 
		                     tlb_hdl, DS_3GPP_TLB_IPV6_IFACE)
  }

  /* Setup RM */
  if (!ds_3gpp_tlb_setup_rm(&(tlb_hdl->rmsm_state), 
  	                        instance,
  	                        phys_link_ev_p,
  	                        DS_UMTS_TLB_PHYS_LINK_EV_CNT,
  	                        ds_umts_tlb_rm_rx_data_cb))
  {
    DS_3GPP_MSG0_ERROR("RM setup failed, returning");
	return;
  }

  ds_umts_tlb_register_uplink_wm(iface_ptr, tlb_hdl);

  /* Check for the other um instance, if it is in UP/DOWN state
     send the CLOSE ready indication */
  if(instance == DS_3GPP_TLB_IPV4_IFACE)
  {
    other_instance = DS_3GPP_TLB_IPV6_IFACE;
  }
  other_iface_ptr = tlb_hdl->rmsm_state.um_iface_ptr[other_instance];
  iface_state = ps_iface_state(other_iface_ptr);

  /* Send close loopback only if 
      1. if not already sent or
      2. other iface state is either down or UP
  */
  ds3gpp_enter_global_crit_section();
  if( (tlb_hdl->umts_tlb_info.wm_reg_complete == TRUE) &&
  	  (tlb_hdl->umts_tlb_info.tlb_mode_closed == FALSE) &&
  	 ((other_iface_ptr == NULL) ||
     (IFACE_UP == iface_state) || (IFACE_DOWN == iface_state)) )
  {
    /* Inform TLB that DS is ready for incoming packets */
	tlb_hdl->umts_tlb_info.tlb_mode_closed = TRUE;
   
    ds3g_send_tlb_close_ready_ind(TRUE, tlb_hdl->subs_id);
  }
  ds3gpp_leave_global_crit_section();
  
}

void ds_umts_tlb_reset_rm_loopback_mode
(
  ds_umts_tlb_per_subs_info_type *tlb_hndl
)
{
  ds_3gpp_rm_tlb_state_type_s  *rmsm_state_ptr;
  
  if (NULL == tlb_hndl)
  {
    DS_3GPP_MSG0_ERROR("Invalid umts tlb handle passed");\
    return;
  }

  
  rmsm_state_ptr = &(tlb_hndl->rmsm_state);

  ds_3gpp_tlb_hdlr_cleanup_rm(rmsm_state_ptr, phys_link_ev_p, 
  	                          DS_UMTS_TLB_PHYS_LINK_EV_CNT, tlb_hndl->subs_id);
  
  /* De-register for UM Iface UP and down events */
  ds_3gpp_tlb_hdlr_dereg_um_events(rmsm_state_ptr, iface_ev_p, 
                                   DS_UMTS_TLB_IFACE_EV_CNT, ds_umts_tlb_iface_evt_cb);
}


void ds_umts_tlb_um_iface_down_event_hdlr
(
  ps_iface_type  *iface_ptr,
  void           *tlb_hndl
)
{
  uint8                                 instance;
  ds_umts_tlb_per_subs_info_type        *tlb_hdl;
  ps_iface_state_enum_type               iface_state = IFACE_STATE_INVALID;
  ds_3gpp_rm_tlb_state_type_s           *rmsm_state_ptr = NULL;
  ps_iface_type                        * other_iface_ptr = NULL;
  ps_iface_type                        * bridge_iface_ptr = NULL;
  ps_iface_down_mh_event_info_type       down_info={0};
  
  tlb_hdl = (ds_umts_tlb_per_subs_info_type *)tlb_hndl;

  if (!(PS_IFACE_IS_VALID(iface_ptr)) ||
  	  NULL == tlb_hdl)
  {
    DS_3GPP_MSG0_ERROR("iface_ptr or tlb_inst passed is invalid");
	return;
  }

  DS_3GPP_MSG3_HIGH("UM iface down event for iface: 0x%x:%d",
  	                 iface_ptr->name,
  	                 iface_ptr->instance,
  	                 0);
  
  /* Find the instance form um iface pointer */
  DS3GPPTLB_GET_INST_FROM_UM_IFACE(iface_ptr,instance,tlb_hdl);

  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_3GPP_MSG0_HIGH("Not a loopback UM iface, return without processing");
    return;
  }

  DS_3GPP_MSG0_HIGH("A loopback UM iface, process down ev on it");

  if(instance == DS_3GPP_TLB_IPV4_IFACE)
  {
    other_iface_ptr = tlb_hdl->rmsm_state.um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE];
  }
  else 
  {
    other_iface_ptr = tlb_hdl->rmsm_state.um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE];
  }

  if(NULL != other_iface_ptr)
  {
    iface_state = ps_iface_state(other_iface_ptr);
  }

  rmsm_state_ptr = &(tlb_hdl->rmsm_state);

  /* 1. Remove the bridged iface
     2. De-reg all Iface and Physlink events
     3. Empty the RX Queue
     4. Send Physlink Down indication
     5. Send RM Iface down indication
     */
  if (tlb_hdl->umts_tlb_info.test_mode_active == TRUE &&
  	  tlb_hdl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4)
  {
    /* Check if RM iface pointer is already bridged, 
       if so we have handled the UM iface up event as part 
       of IFACE bring up. No further action needed.
    */
    bridge_iface_ptr = ps_iface_bridge_iface(
                                &(rmsm_state_ptr->rm_lo_b_iface[instance]));
    if( NULL != bridge_iface_ptr)
    {
      ps_iface_set_bridge( &(rmsm_state_ptr->rm_lo_b_iface[instance]),
                             NULL );
      ds_3gpp_tlb_hdlr_dereg_rm_events(
                           &(rmsm_state_ptr->rm_lo_phys_link[instance]),
                             rmsm_state_ptr,
                             phys_link_ev_p,
                             DS_UMTS_TLB_PHYS_LINK_EV_CNT);
      dsm_empty_queue(&(rmsm_state_ptr->rm_lo_rx_wmk[instance]));
      ps_iface_phys_link_down_ind(&(rmsm_state_ptr->rm_lo_b_iface[instance]));
      ps_iface_down_ind( &(rmsm_state_ptr->rm_lo_b_iface[instance]),
                            &down_info );
      /* Clear UM iface pointer so that reset loopback mode B Will not be 
         called for this already cleaned UM Iface.
      */
      rmsm_state_ptr->um_iface_ptr[instance] = NULL;
   }

   if( (other_iface_ptr != NULL) && (IFACE_UP == iface_state) )
   {
     ds3gpp_enter_global_crit_section();
     if ((tlb_hdl->umts_tlb_info.tlb_mode_closed == FALSE ) &&
   	     (tlb_hdl->umts_tlb_info.wm_reg_complete == TRUE ) )
     {
       DS_3GPP_MSG0_HIGH("Other iface is UP, post close ready");
       tlb_hdl->umts_tlb_info.tlb_mode_closed = TRUE;
	   ds3gpp_leave_global_crit_section();
       ds3g_send_tlb_close_ready_ind(TRUE, tlb_hdl->subs_id);
     }
	 else
     {
       ds3gpp_leave_global_crit_section();
     }
   }
   else if ((other_iface_ptr == NULL) || (IFACE_DOWN == iface_state))
   {
     /* If both Iface's are down, consider it as equivalent to  
             LB_DEACT_IND and clean up the Loopback state.
       */
     DS_3GPP_MSG1_HIGH("Both IFACE's down UMTS LB mode (%d)",
                        tlb_hdl->umts_tlb_info.tlb_mode);
  
     ds_umts_tlb_reset_rm_loopback_mode(tlb_hdl);
     ds_umts_tlb_free_hndl(tlb_hdl);
   }
   else
   {
     DS_3GPP_MSG1_HIGH("Other iface is in %d state, wait for it", iface_state);
   }
   /* Update the UM iface pointer as NULL only after RM de-registration 
   */  
   rmsm_state_ptr->um_iface_ptr[instance] = NULL;
 }

  
}


void ds_umts_tlb_process_close_tlb
(
  sys_modem_as_id_e_type  subs_id
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hdl = NULL;
  ds_pdn_context_s               *pdn_cntxt_p = NULL;
  ds_bearer_context_s            *bearer_cntxt_p = NULL;
  uint16                          pdp_profile_num = 0; 
  ds_umts_pdp_context_type        *pdp_context_data = NULL;
  sys_sys_mode_e_type             bearer_mode = SYS_SYS_MODE_NONE;
  boolean                         result = FALSE;
  boolean                         bringup_result = FALSE;

  
  DS_3GPP_MSG0_HIGH("Process close loopback mode for mode4");
  

   /* Initialise RM loopback instance */
   if (NULL != (tlb_hdl = ds_umts_tlb_get_hndl(subs_id, FALSE)))
   {
     ds_3gpp_tlb_init_rm(&(tlb_hdl->rmsm_state), (void *)tlb_hdl);
   }
   else
   {
     DS_3GPP_MSG1_ERROR("Cannot find UMTS tlb hndl for subs_id: %d", subs_id);\
	 return;
   }

  /* Check if a PDN Context is active already
        Get pdp_profile from it
        Register for UM iface events
        Bringup UM iface */

  ds3gpp_enter_global_crit_section();
  if (tlb_hdl->umts_tlb_info.test_mode_active == TRUE &&
  	  tlb_hdl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4 &&
  	  tlb_hdl->umts_tlb_info.tlb_mode_closed == FALSE)
  {
    tlb_hdl->umts_tlb_info.tlb_mode_closed = TRUE;
    ds3gpp_leave_global_crit_section();
	/* Post Close ready*/
	ds3g_send_tlb_close_ready_ind(TRUE, tlb_hdl->subs_id);
	return;
  }
  ds3gpp_leave_global_crit_section();
    

  do 
  {
    pdn_cntxt_p = ds_3gpp_pdn_cntxt_get_any_active_pdn(subs_id);

	if (ds_3gpp_pdn_cntx_validate_pdn_context(pdn_cntxt_p))
    {
      if (pdn_cntxt_p->ds_pdn_context_dyn_p->state == DS_PDN_CONTEXT_STATE_COMING_UP ||
	  	  pdn_cntxt_p->ds_pdn_context_dyn_p->state == DS_PDN_CONTEXT_STATE_UP)
      {
		pdp_profile_num = (uint16)(pdn_cntxt_p->ds_pdn_context_dyn_p->pdp_profile_num);
		DS_3GPP_MSG3_HIGH("PDN cntxt: 0x%x is in %d state", 
			               pdn_cntxt_p,
			               pdn_cntxt_p->ds_pdn_context_dyn_p->state,
			               pdp_profile_num);

		bearer_cntxt_p = (ds_bearer_context_s *)
			             (pdn_cntxt_p->ds_pdn_context_dyn_p->def_bearer_context_ptr);
		
        if (ds_bearer_cntx_validate_bearer_context(bearer_cntxt_p))
        {
          bearer_mode = bearer_cntxt_p->ds_bearer_context_dyn_p->call_mode;
        }
      }
    }

	if (pdp_profile_num == 0)
	{
#ifdef FEATURE_DATA_LTE
      pdp_profile_num = ds_eps_get_attach_prof(subs_id);
#else
      (void)ds_umts_get_default_profile_number_per_subs(
                                            DS_UMTS_EMBEDDED_PROFILE_FAMILY,
                                            subs_id,
                                            &pdp_profile_num);
#endif /* FEATURE_DATA_LTE */
	}

	if (bearer_mode == SYS_SYS_MODE_NONE)
    {
      bearer_mode = ds3gpp_get_current_network_mode(subs_id);
    }

	tlb_hdl->rmsm_state.current_bearer_mode = bearer_mode;

    pdp_context_data = (ds_umts_pdp_context_type *)
		               modem_mem_alloc(sizeof(ds_umts_pdp_context_type), 
		                               MODEM_MEM_CLIENT_DATA);
	if (NULL != pdp_context_data)
	{
	  pdp_context_data->pdp_type = DS_UMTS_PDP_MAX;

	  if (DS_UMTS_PDP_SUCCESS != ds_umts_get_pdp_profile_context_info_per_subs(
	  	                                 pdp_profile_num,
	  	                                 dsumts_subs_mgr_get_subs_id(subs_id),
	  	                                 pdp_context_data))
      {
        DS_3GPP_MSG3_ERROR("Cannot retrieve pdp_context for profile: %d",
			                pdp_profile_num, 0, 0);
		break;
      }

	  DS_3GPP_MSG3_HIGH("pdp_type for profile_num %d is %d",
	  	                 pdp_profile_num, pdp_context_data->pdp_type, 0);

	  /*---------------------------------------------------------------------
		Register IFACE_UP and DOWN events with UM packet interface
	  ---------------------------------------------------------------------*/
      result = ds_3gpp_tlb_hdlr_reg_um_events(&(tlb_hdl->rmsm_state),
												subs_id,
												iface_ev_p,
												DS_UMTS_TLB_IFACE_EV_CNT,
												ds_umts_tlb_iface_evt_cb);
	  
	  /*---------------------------------------------------------------------
		Bring up UM iface
	  ---------------------------------------------------------------------*/
	  if (result != FALSE)
	  {
		bringup_result = ds_3gpp_tlb_bringup_um_iface(
									 pdp_profile_num,
									 pdp_context_data->pdp_type, 
									 subs_id, 
									 &(tlb_hdl->rmsm_state), 
									 ds_umts_tlb_um_iface_up_event_hdlr);

		if (TRUE == bringup_result)
		{
		  ds3gpp_enter_global_crit_section();
		  tlb_hdl->umts_tlb_info.tlb_mode = CM_TEST_CONTROL_TYPE_LB_MODE4;
		  ds3gpp_leave_global_crit_section();
		}
		else
		{
		  DS_3GPP_MSG0_ERROR("Iface bringup failure, clear TLB hndl");
          ds_umts_tlb_reset_rm_loopback_mode(tlb_hdl);
	      ds_umts_tlb_free_hndl(tlb_hdl);
		}
	  }

	  
	}
	else
    {
      DS_3GPP_MSG0_ERROR("Memory allocation for pdp_context failed, return");
	  break;
    }
	
  }while(0);
  
  if (NULL != pdp_context_data)
  {
    modem_mem_free(pdp_context_data, MODEM_MEM_CLIENT_DATA);
  }
}

/*==========================================================================
FUNCTION ds_umts_tlb_get_hndl_from_rm_iface

DESCRIPTION
  This function is used to fetch the TLB Info handle from the Rm Iface
 
PARAMETERS 
  rm_iface_ptr - Pointer to the Rm Iface 
 
DEPENDENCIES
  None.

RETURN VALUE
  Handle to the TLB Information
  
SIDE EFFECTS
  None.
===========================================================================*/
ds_umts_tlb_per_subs_info_type *ds_umts_tlb_get_hndl_from_rm_iface
(
  ps_iface_type* rm_iface_ptr
)
{
  int                            i = 0, j = 0;
  ds_umts_tlb_per_subs_info_type *hndl = NULL;
  /*---------------------------------------------------------------------*/

  for (i=0; i< DS3GSUBSMGR_SUBS_ID_MAX;i++)
  {
    hndl = ds_umts_tlb_get_hndl(ds3gsubsmgr_subs_id_ds3g_to_cm((ds3gsubsmgr_subs_id_e_type)i), 
		                        FALSE);

    if (NULL != hndl)
    {
      for (j=0; j< DS_3GPP_MAX_IFACE_PER_TLB; j++)
      {
        if (&(hndl->rmsm_state.rm_lo_b_iface[j]) == rm_iface_ptr)
        {
          return hndl;
        }
      }
    }
  }
  return NULL;
} /* ds_eps_tlb_get_subs_id_from_rm_iface */

boolean ds_umts_tlb_is_loopback_closed
(
  ds_umts_tlb_per_subs_info_type *hndl
)
{
  if (NULL == hndl)
  {
    DS_3GPP_MSG0_ERROR("Invalid hndl passed");
	return FALSE;
  }
  
  //Enter critical section
   ds3gpp_enter_global_crit_section();
  if (hndl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4 &&
  	  hndl->umts_tlb_info.tlb_mode_closed == TRUE)
  {
    ds3gpp_leave_global_crit_section();
    return TRUE;
  }
  ds3gpp_leave_global_crit_section();

  return FALSE;
}

boolean ds_umts_tlb_rm_rx_data_cb
(
  ps_sig_enum_type sig,
  void			   *user_data_p
)
{
  dsm_item_type				  *item_ptr;
  ps_iface_type				  *rm_iface_ptr;
  ds_3gpp_rm_tlb_state_type_s  *rmsm_tlb_state_ptr ;
  uint8						   instance = DS_3GPP_MAX_IFACE_PER_TLB;
  ds_umts_tlb_per_subs_info_type *hndl = NULL; 
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  rm_iface_ptr = (ps_iface_type *) user_data_p;
  ASSERT(rm_iface_ptr != NULL);
  
  hndl = ds_umts_tlb_get_hndl_from_rm_iface(rm_iface_ptr);
  
  if (NULL == hndl)
  {
    DS_3GPP_MSG0_ERROR("Invalid Subs Info handle");
    return TRUE;
  }
  
  rmsm_tlb_state_ptr = &(hndl->rmsm_state);
  
  DS3GPPTLB_GET_INST_FROM_RM_IFACE(rm_iface_ptr,instance,hndl);
  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_3GPP_MSG0_HIGH("Not a loopback RM iface ,return without processing");
    return TRUE;
  }

  /* if MODE is not mode4 , return TRUE */
  if(hndl->umts_tlb_info.tlb_mode != CM_TEST_CONTROL_TYPE_LB_MODE4 ||
  	 FALSE == ds_umts_tlb_is_loopback_closed(hndl))
  {
    item_ptr = (dsm_item_type *)dsm_dequeue(
  				   &(rmsm_tlb_state_ptr->rm_lo_rx_wmk[instance]));
    if (item_ptr != NULL)
    {
  	/*	We need to free DSM item., and no action taken */
  	dsm_free_packet(&item_ptr);
  	DS_3GPP_MSG0_HIGH("Mode4,discarding data, MODE4 not closed");
    }
    return FALSE;
  }
  /* Check if its a valid IP packet */

  /* Check if rm_flow is enabled */

  /* Dequeue the packet from the rm_lo_wmk */
  item_ptr = (dsm_item_type *)dsm_dequeue(
            &(rmsm_tlb_state_ptr->rm_lo_rx_wmk[instance]));
  if (item_ptr != NULL)
  {
    //DS_3GPP_MSG1_HIGH("ip rm rx data cb is called for instance[%d]", instance);

    (void)ps_phys_link_input(&
                  (rmsm_tlb_state_ptr->rm_lo_phys_link[instance]), 
                   &item_ptr,
                   NULL);
    return FALSE;
  }

  return TRUE;

  /* ps_iface_input on the bridged iface/ ps_phys_link_input */
}

void ds_umts_tlb_process_rx_data
(
  ps_iface_type          *rm_iface_ptr
)
{
  uint8  instance;
  ds_umts_tlb_per_subs_info_type *tlb_hndl;

  tlb_hndl = (ds_umts_tlb_per_subs_info_type *)
		            ds_umts_tlb_get_hndl_from_rm_iface(rm_iface_ptr);
	
  if (NULL == tlb_hndl ||
      tlb_hndl->umts_tlb_info.test_mode_active != TRUE ||
      tlb_hndl->umts_tlb_info.tlb_mode != CM_TEST_CONTROL_TYPE_LB_MODE4)
  {
    DS_3GPP_MSG0_HIGH("Discarding data, MODE 4 not active");
    return; 
  }
  
  DS3GPPTLB_GET_INST_FROM_RM_IFACE(rm_iface_ptr,instance, tlb_hndl);
  
  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_LTE_MSG0_HIGH("Not a loopback RM iface ,return without processing");
    return;
  }
  
  PS_SET_SIGNAL( tlb_hndl->rmsm_state.rx_sig[instance]);
}


int ds_umts_tlb_receive_data_from_network
(
  void				   *tlb_hndl,
  ps_iface_type		   *this_iface_ptr,
  dsm_item_type		   **item_ptr,
  ps_meta_info_type	   *meta_info_ptr,
  sys_modem_as_id_e_type subs_id
)
{
  uint8 instance;
  ds_umts_tlb_per_subs_info_type *hndl;

  hndl = (ds_umts_tlb_per_subs_info_type *)tlb_hndl;

  if (NULL == hndl ||
      hndl->umts_tlb_info.test_mode_active != TRUE ||
      hndl->umts_tlb_info.tlb_mode != CM_TEST_CONTROL_TYPE_LB_MODE4)
  {
    dsm_free_packet(item_ptr);
    PS_META_INFO_FREE(&meta_info_ptr);
    DS_3GPP_MSG0_HIGH("Discarding data, MODE 4 not active");
    return -1;
  }

  /*-------------------------------------------------------------------------
    Enqueue the packet on RX watermark and set a signal to PS to pick it up
    or send it to a registered handler
  -------------------------------------------------------------------------*/
  DS3GPPTLB_GET_INST_FROM_RM_IFACE(this_iface_ptr,instance,hndl);

  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_LTE_MSG0_HIGH("Not a loopback RM iface ,return without processing");
    dsm_free_packet(item_ptr);
    PS_META_INFO_FREE(&meta_info_ptr);
    return -1;
  }
  dsm_enqueue(&(hndl->rmsm_state.rm_lo_rx_wmk[instance]),item_ptr);

  PS_META_INFO_FREE(&meta_info_ptr);

  return 0;
  
}

int ds_umts_tlb_setup_rm_data_path
(
  ps_iface_type  *this_iface_ptr,
  ps_iface_type  *bridge_iface_ptr,
  sys_modem_as_id_e_type subs_id
)
{
  uint8  instance;
  ds_umts_tlb_per_subs_info_type *umts_tlb_hndl;
  ds_3gpp_rm_tlb_state_type_s    *rm_tlb_state_ptr;

  umts_tlb_hndl = ds_umts_tlb_get_hndl(subs_id, FALSE);

  if (NULL == umts_tlb_hndl ||
      umts_tlb_hndl->umts_tlb_info.test_mode_active != TRUE ||
      umts_tlb_hndl->umts_tlb_info.tlb_mode != CM_TEST_CONTROL_TYPE_LB_MODE4)
  {
    return -1; 
  }

  DS3GPPTLB_GET_INST_FROM_UM_IFACE(bridge_iface_ptr,instance, umts_tlb_hndl);
  
  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_LTE_MSG0_HIGH("Not a loopback UM iface ,return without processing");
    return -1;
  }

  rm_tlb_state_ptr = &(umts_tlb_hndl->rmsm_state);
  
  /*-------------------------------------------------------------------------
   Set the handler for the downlink signal for the call.
   -------------------------------------------------------------------------*/
  (void)ps_set_sig_handler(
                  rm_tlb_state_ptr->rx_sig[instance],
                  ds_umts_tlb_rm_rx_data_cb, 
                  (void *)this_iface_ptr);
  
  /*-------------------------------------------------------------------------  
    Enable it.
    -------------------------------------------------------------------------*/
  ps_enable_sig(rm_tlb_state_ptr->rx_sig[instance]);  
  return 0;
  
}


void ds_umts_tlb_um_iface_evt_hdlr
(
  ds_3gpp_tlb_iface_event_type iface_evt_info,
  ds_umts_tlb_info_type_s      *tlb_hndl
)
{
  DS_3GPP_MSG1_HIGH( "Recd Iface event %d ",iface_evt_info.event);

  switch(iface_evt_info.event)
  {
    case  IFACE_DOWN_EV:
      ds_umts_tlb_um_iface_down_event_hdlr(iface_evt_info.um_iface_ptr, tlb_hndl);
      break;
    case  IFACE_UP_EV:
      ds_umts_tlb_um_iface_up_event_hdlr(iface_evt_info.um_iface_ptr, (void *)tlb_hndl);
      break;

    default:
      DS_3GPP_MSG1_ERROR("Invalid Event type :%d",iface_evt_info.event);
  }
}

void ds_umts_tlb_hdlr_cmd
(
  const ds_cmd_type *cmd_ptr
)
{
  ds_3gpp_tlb_cmd_type          tlb_cmd;
  ds_3gpp_tlb_cmd_info_type    *data_ptr = NULL;
  sys_modem_as_id_e_type        subs_id;
  ds_umts_tlb_ev_e_type         cmd_type;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ASSERT(cmd_ptr != NULL);
  ASSERT(cmd_ptr->cmd_payload_ptr != NULL);

  switch (cmd_ptr->hdr.cmd_id)
  {
    case DS_CMD_3GPP_UMTS_LB_CLOSE_READY:
      subs_id = *((sys_modem_as_id_e_type *)(cmd_ptr->cmd_payload_ptr));
      ds_umts_tlb_post_close_ready_ind(subs_id);
	  break;

	case DS_CMD_3GPP_UMTS_LB_MODE4:
      data_ptr = (ds_3gpp_tlb_cmd_info_type*)cmd_ptr->cmd_payload_ptr;
      cmd_type = (ds_umts_tlb_ev_e_type)data_ptr->event;
      tlb_cmd = data_ptr->event_info;
  
      switch(cmd_type)
      {
        case DS_UMTS_TLB_UM_IFACE_EV:
          ds_umts_tlb_um_iface_evt_hdlr(tlb_cmd.iface_evt_info, 
                                     (ds_umts_tlb_info_type_s *)data_ptr->hndl);
          break;
  
        default:
          DS_LTE_MSG1_ERROR("Invalid command type posted %d",(int)cmd_type);
          break;
      }
	  break;

	default:
      DS_3GPP_MSG1_ERROR("Invalid command posted %d", cmd_ptr->hdr.cmd_id);
	  break;
  }
}

void ds_umts_tlb_iface_evt_cb
(
  ps_iface_type			     *this_iface_ptr,
  ps_iface_event_enum_type   event,
  ps_iface_event_info_u_type event_info,
  void					     *user_data_ptr
)
{
  ds_cmd_type                      *cmd_ptr = NULL;                   
  ds_3gpp_tlb_cmd_type             tlb_event_info;
  uint8                             instance;
  ds_3gpp_tlb_cmd_info_type        *data_ptr = NULL;
  ds_umts_tlb_per_subs_info_type    *hndl = NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_3GPP_MSG2_HIGH( "Recd event %d for um iface 0x%x", event, this_iface_ptr);

  hndl = (ds_umts_tlb_per_subs_info_type*)user_data_ptr;

  if (NULL == hndl)
  {
    DS_3GPP_MSG1_ERROR( "Invalid hndl, Ignoring event %d", event);
    return;
  }

  /* Find the instance form um iface pointer */
  DS3GPPTLB_GET_INST_FROM_UM_IFACE(this_iface_ptr,instance, hndl);

  /* Check if this is a valid instance */
  if(!(IS_DS3GPPTLB_VALID_INSTANCE(instance)))
  {
    DS_3GPP_MSG0_MED("Not processing IFACE_EV, invalid iface");
    return;
  }

  switch( event )
  {
    case IFACE_DOWN_EV:
      if( event_info.iface_down_info.state != IFACE_DOWN )
      {
        DS_3GPP_MSG0_HIGH( "UM iface down, posting DOWN_EV");
      }
      else
      {
        DS_3GPP_MSG1_MED( "Ignoring event %d", event);
        return;
      }
      break;

    case IFACE_UP_EV:
      DS_3GPP_MSG0_HIGH( "Um Iface up, posting IFACE_UP_EV" );
      break;

    default:
    	 DS_3GPP_MSG1_MED( "Ignoring event %d", event);
      return;
  }

  cmd_ptr = ds_allocate_cmd_buf(sizeof(ds_3gpp_tlb_cmd_info_type));
  if( (cmd_ptr == NULL) || (cmd_ptr->cmd_payload_ptr == NULL) )
  {
    ASSERT(0);
    return;
  }
  cmd_ptr->hdr.cmd_id = DS_CMD_3GPP_UMTS_LB_MODE4;
  data_ptr = (ds_3gpp_tlb_cmd_info_type*)cmd_ptr->cmd_payload_ptr;
  data_ptr->event = (int)DS_UMTS_TLB_UM_IFACE_EV;
  data_ptr->hndl = (ds_umts_tlb_per_subs_info_type *)hndl;
  tlb_event_info.iface_evt_info.event = event;
  tlb_event_info.iface_evt_info.um_iface_ptr = this_iface_ptr;
  data_ptr->event_info = tlb_event_info;
  ds_put_cmd(cmd_ptr);
}

void ds_umts_tlb_process_open_tlb
(
  sys_modem_as_id_e_type subs_id
)
{
  ds_umts_tlb_per_subs_info_type *tlb_hndl;
  /* TLB Loop Open*/

  tlb_hndl = ds_umts_tlb_get_hndl(subs_id, FALSE);

  if (NULL == tlb_hndl)
  {
    DS_3GPP_MSG1_ERROR("Received open tlb when handle is not allocated for subs: %d",
		                 subs_id);
	return;
  }

  DS_3GPP_MSG3_HIGH("Process open_tlb, test_mode_active: %d, tlb_mode: %d",
  	                 tlb_hndl->umts_tlb_info.test_mode_active,
  	                 tlb_hndl->umts_tlb_info.tlb_mode, 0);
  //Critical section
  ds3gpp_enter_global_crit_section();
  if (tlb_hndl->umts_tlb_info.test_mode_active == TRUE &&
  	  tlb_hndl->umts_tlb_info.tlb_mode == CM_TEST_CONTROL_TYPE_LB_MODE4)
  {
    DS_3GPP_MSG1_HIGH("Received open tlb for subs: %d", subs_id);
	tlb_hndl->umts_tlb_info.tlb_mode_closed = FALSE;
  }
  ds3gpp_leave_global_crit_section();
}

void ds_umts_tlb_init(void)
{
  uint8  index=0;
  
  /* Initialise UMTS TLB handle for each subs */
  for (index = 0; index < DS_3GPP_TLB_MAX_INSTANCES; index++)
  {
    if (NULL == ds_umts_tlb_info_p[index])
    {
      ds_umts_tlb_info_p[index] = (ds_umts_tlb_per_subs_info_type *)
	  	                           modem_mem_alloc(sizeof(ds_umts_tlb_per_subs_info_type),
	  	                                           MODEM_MEM_CLIENT_DATA);
      if(ds_umts_tlb_info_p[index] == NULL)
      {
        ASSERT(0);
        continue;
      }
	  memset(ds_umts_tlb_info_p[index], 0, sizeof(ds_umts_tlb_per_subs_info_type));
	  
	  ds_umts_tlb_info_p[index]->in_use = FALSE;
	  ds_umts_tlb_info_p[index]->subs_id = SYS_MODEM_AS_ID_NONE;
	  ds_umts_tlb_info_p[index]->umts_tlb_info.test_mode_active = FALSE;
	  ds_umts_tlb_info_p[index]->umts_tlb_info.tlb_mode = CM_TEST_CONTROL_TYPE_NONE;
	  ds_umts_tlb_info_p[index]->rmsm_state.init = FALSE;
	  ds_umts_tlb_info_p[index]->umts_tlb_info.wm_reg_complete = FALSE;
    }
  }

#ifdef FEATURE_WCDMA
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_WCDMA, setup_rm_data_path, \
                        ds_umts_tlb_setup_rm_data_path);
#endif /* FEATURE_WCDMA */

#ifdef FEATURE_GSM_GPRS
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_GSM, setup_rm_data_path, \
                        ds_umts_tlb_setup_rm_data_path);
#endif /* FEATURE_GSM_GPRS */

#ifdef FEATURE_TDSCDMA
  DS_BEARER_CTXT_VF_REG(SYS_SYS_MODE_TDS, setup_rm_data_path, \
                        ds_umts_tlb_setup_rm_data_path);
#endif /* FEATURE_TDSCDMA */
  /**/
}

boolean ds_umts_tlb_is_wm_reg_allowed
(
  sys_modem_as_id_e_type subs_id
)
{
  boolean                          ret_val = TRUE;
  ds_umts_tlb_per_subs_info_type  *tlb_hndl = NULL;

  tlb_hndl = ds_umts_tlb_get_hndl(subs_id, FALSE);

  if (NULL == tlb_hndl)
  {
    DS_3GPP_MSG1_HIGH("UMTS tlb hndl not allocated for subs: %d", subs_id);
  }
  else 
  {
    if ((tlb_hndl->umts_tlb_info.test_mode_active == TRUE) &&
        (tlb_hndl->umts_tlb_info.tlb_mode != CM_TEST_CONTROL_TYPE_LB_MODE4) )
    {
      ret_val = FALSE;
    }

    DS_3GPP_MSG2_HIGH("UMTS tlb test_mode_active: %d. mode: %d",
                       tlb_hndl->umts_tlb_info.test_mode_active,
  	               tlb_hndl->umts_tlb_info.tlb_mode);
  }

  return ret_val;
}
