/*!
  @file
  ds_eps_throughput_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.

*/



/*===========================================================================

  Copyright (c) 2009-2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/data.mpss/3.4.3.1/3gpp/lteps/src/ds_eps_throughput_hdlr.c#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------

==============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "datamodem_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_DATA_LTE

#include "ds_eps_pdn_context.h"
#include "lte_mac.h"
#include "ds_eps_throughput_hdlr.h"
#include "ds_3gpp_nv_manager.h"


/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/
ds_eps_downlink_throughput_estimation_s ds_eps_dl_tput_est_tbl[DS_SUBSCRIPTION_MAX] = {{NULL}};

#ifdef FEATURE_DATA_RAVE_SUPPORT 
ds_eps_uplink_throughput_estimation_s  ds_eps_ul_tput_est_tbl[DS3GSUBSMGR_SUBS_ID_MAX] = {{NULL}};
#endif

static boolean ds_eps_achievable_ml1_is_connected[DS3GSUBSMGR_SUBS_ID_COUNT] = {FALSE};


static ds_eps_achievable_tput_stats  ds_eps_achievable_tput_stats_info[DS_EPS_ULDL_TPUT_LOGGING_COUNT];

static uint8 achievable_index_logging = 0;

/*===========================================================================
FUNCTION DS_EPS_THROUGHPUT_HDLR_INIT

DESCRIPTION
  This function is used to initialize the 
  eps throughput handler module
  
PARAMETERS

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds_eps_throughput_hdlr_init()
{
  ds3gsubsmgr_subs_id_e_type   subs_indx;
  for(subs_indx = DS3GSUBSMGR_SUBS_ID_MIN;
      subs_indx < DS3GSUBSMGR_SUBS_ID_MAX;
      subs_indx++)
  {
    ds_eps_achievable_ml1_is_connected[subs_indx] = TRUE;
  }
}
/*===========================================================================
FUNCTION DS_EPS_DL_TPUT_LOGGING

DESCRIPTION
  This funciton is used to output log information for the following log
  packet: LOG_DS_LTE_DL_TPUT_C.
  
PARAMETERS 
  subs_id - subscription on which logging is done 
  time_diff - Time difference since last calculation
  ml1_bytes - total ml1 bytes given to DS during time frame
  pdcp_bytes - total pcdp bytes given to DS during time frame

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds_eps_dl_tput_logging
(
  sys_modem_as_id_e_type subs_id,
  uint64                 time_diff,
  uint64                 ml1_bytes,
  uint64                 pdcp_bytes
);

/*===========================================================================
FUNCTION  DS_EPS_CHECK_AND_UPDATE_ML1_STATUS

DESCRIPTION
  This function checks the current status of ML1 reporting status and
  sends (start/stop) ML1 request if required

  If report frequency is 0, this function and reporting is going on
  this function will send to stop report request and return FALSE.
  If reporting frequcny is positive and reporting is stop, this function
  will send start request and return TRUE

PARAMETERS
  subs_id - sys_modem_as_id_e_type (Subscription ID)

DEPENDENCIES
  None.

RETURN VALUE
  TRUE is reporting is expected after this, FALSE otherwise

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_check_and_update_ml1_status
(
   sys_modem_as_id_e_type subs_id,
   boolean need_to_report,
   boolean update_t_accumulate
)
{
  lte_cphy_dl_tput_estm_report_req_s ds_lte_dl_tput_req;
  uint32 report_interval = 0 ;
  ds3gsubsmgr_subs_id_e_type  subs_index = DS3GSUBSMGR_SUBS_ID_INVALID;

  /*------------------------------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }
  
  subs_index = ds3gsubsmgr_subs_id_cm_to_ds3g( subs_id );

  report_interval = ds3gtputmgr_get_est_dl_tput_actual_interval(subs_index);

  ds_lte_dl_tput_req.t_accumulate = 
      (uint16)ds_eps_dl_estimated_throughput_get_running_t_accumulate(subs_id);

  DS_3GPP_MSG3_LOW("report_frequency: %d for subs_id: %d status: %d ", 
          report_interval, subs_id, ds_eps_dl_tput_est_tbl[subs_index].status);

  if (need_to_report == FALSE &&
      ((ds_eps_dl_tput_est_tbl[subs_index].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_START) ||
      (ds_eps_dl_tput_est_tbl[subs_index].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_START_WAIT_FOR_ML1)))
  {
    ds_lte_dl_tput_req.subs_id = subs_id;
    ds_lte_dl_tput_req.req_type = LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_STOP;
    DS_3GPP_MSG3_HIGH("Sending LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ with req type %d"
                       "and t_accumulate %d for  CM sub ID %d",ds_lte_dl_tput_req.req_type,
                       ds_lte_dl_tput_req.t_accumulate, ds_lte_dl_tput_req.subs_id);
    (void) dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_dl_tput_req),
       sizeof(lte_cphy_dl_tput_estm_report_req_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    );
    ds_eps_dl_tput_est_tbl[subs_index].status = DS_EPS_DL_TPUT_ESTM_STATUS_STOP_WAIT_FOR_ML1;
    ds_eps_send_last_report_and_reset_info(subs_id);
  }
  else if (need_to_report == TRUE &&
       ((ds_eps_dl_tput_est_tbl[subs_index].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_STOP) ||
      (ds_eps_dl_tput_est_tbl[subs_index].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_STOP_WAIT_FOR_ML1))) 
  {
    ds_lte_dl_tput_req.subs_id = subs_id;
    ds_lte_dl_tput_req.req_type = LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_START;
 
    ds_eps_dl_tput_est_tbl[subs_index].ds_eps_filtered_dl_throughput_est = 0;
    ds_eps_dl_tput_est_tbl[subs_index].ds_eps_filtered_confidence_level = 0;

    ds_eps_dl_tput_est_tbl[subs_index].status = DS_EPS_DL_TPUT_ESTM_STATUS_START_WAIT_FOR_ML1;

    DS_3GPP_MSG3_HIGH("Sending LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ with req type %d "
                       "and t_accumulate %d for  CM sub ID %d",ds_lte_dl_tput_req.req_type,
                       ds_lte_dl_tput_req.t_accumulate, ds_lte_dl_tput_req.subs_id);
    
    (void) dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_dl_tput_req),
       sizeof(lte_cphy_dl_tput_estm_report_req_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    );
    ds_eps_bearer_cntxt_reset_to_current_dl_byte_count(subs_id);
    }
  else if (update_t_accumulate == TRUE &&
           ((ds_eps_dl_tput_est_tbl[subs_id].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_START_WAIT_FOR_ML1) ||
      (ds_eps_dl_tput_est_tbl[subs_id].status == 
       DS_EPS_DL_TPUT_ESTM_STATUS_START)))
  {
    ds_lte_dl_tput_req.subs_id = subs_id;
    ds_lte_dl_tput_req.req_type = LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_UPDATE;
 
    ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est = 0;
    ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_confidence_level = 0;

    ds_eps_dl_tput_est_tbl[subs_id].status = DS_EPS_DL_TPUT_ESTM_STATUS_START_WAIT_FOR_ML1;

    DS_3GPP_MSG3_HIGH("Sending LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ with req type %d "
                       "and t_accumulate %d for  CM sub ID %d",ds_lte_dl_tput_req.req_type,
                       ds_lte_dl_tput_req.t_accumulate, ds_lte_dl_tput_req.subs_id);
    
    (void) dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_DL_TPUT_ESTM_REPORT_REQ,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_dl_tput_req),
       sizeof(lte_cphy_dl_tput_estm_report_req_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    );
    ds_eps_dl_tput_estimation_reset(subs_id);
    ds_eps_bearer_cntxt_reset_to_current_dl_byte_count(subs_id);
  }
}/*ds_eps_check_and_update_ml1_status*/

/*===========================================================================
FUNCTION  DS_EPS_DL_TPUT_ESTIMATION_INIT

DESCRIPTION
  This function initializes default values of downlink throughput internal
  structures.


PARAMETERS
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/

void ds_eps_dl_tput_estimation_init(void)
{
   uint8 index = 0;
   /*------------------------------------------------------------------------*/
   for( index = 0 ; index < DS_SUBSCRIPTION_MAX; index++)
   {
      ds_eps_dl_tput_est_tbl[index].ds_eps_filtered_confidence_level = DS3G_INVALID_DL_CONFIDENCE;
      ds_eps_dl_tput_est_tbl[index].ds_eps_filtered_dl_throughput_est = DS3G_INVALID_DL_TPUT;
      ds_eps_dl_tput_est_tbl[index].time_since_last_estimatation = 0;
      ds_eps_dl_tput_est_tbl[index].status = DS_EPS_DL_TPUT_ESTM_STATUS_STOP;
      ds_eps_dl_tput_est_tbl[index].ds_eps_ignore_historical_data = TRUE;
      ds_eps_dl_tput_est_tbl[index].ds_eps_is_ml1_active = FALSE;
   }
}/*ds_eps_dl_tput_estimation_init*/


#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION  DS_EPS_UL_TPUT_ESTIMATION_INIT

DESCRIPTION
  This function initializes default values of uplink throughput internal
  structures.


PARAMETERS
  None.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_ul_tput_estimation_init()
{
  int i=0;
  for(i=0; i<DS3GSUBSMGR_SUBS_ID_MAX;i++)
  {
     memset(&ds_eps_ul_tput_est_tbl[i],0,
            sizeof(ds_eps_uplink_throughput_estimation_s));

  }
}
#endif

/*===========================================================================
FUNCTION  DS_EPS_DL_TPUT_ESTIMATION_RESET

DESCRIPTION
  This function resets default values of downlink throughput internal
  structures.


PARAMETERS
  subs_id - Subscription ID to be reset

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_tput_estimation_reset
(
   sys_modem_as_id_e_type subs_id
)
{
  time_type ds_eps_time_now;
  uint64 current_time = 0;
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  time_get_ms( ds_eps_time_now );
  current_time = QW_CVT_Q2N(ds_eps_time_now); 
  ds_eps_dl_tput_est_tbl[subs_id].time_since_last_estimatation = current_time;

  DS_3GPP_MSG1_HIGH("ds_eps_dl_tput_estimation_reset for ID %d",subs_id);
  ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_confidence_level = DS3G_INVALID_DL_CONFIDENCE;
  ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est = DS3G_INVALID_DL_TPUT;
  ds_eps_dl_tput_est_tbl[subs_id].status = DS_EPS_DL_TPUT_ESTM_STATUS_STOP;
  ds_eps_dl_tput_est_tbl[subs_id].ds_eps_ignore_historical_data = TRUE;
}/*ds_eps_dl_tput_estimation_reset*/


/*===========================================================================
FUNCTION  DS_EPS_DL_TPUT_PROCESS_ESTIMATION_INDICATION

DESCRIPTION
  This function processes the message that is received from LTE ML1 through
  message router


PARAMETERS
  lte_cphy_dl_tput_estm_report_ind_s - Estimation report message from ML1

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_tput_process_estimation_indication
(
   lte_cphy_dl_tput_estm_report_ind_s *est_report
)
{
   uint32 alpha = 0;
   uint32 confidence_level = DS_EPS_MIN_DL_TPUT_CONFIDENCE_LEVEL;

   uint64 dl_est_total_byte_count = 0;
   uint64 pdcp_total_bytes_count = 0;
   uint64 time_diff = 0;
   uint64 current_time = 0;
   uint32 report_interval = 0;
   time_type dl_time_type_now;
   /*------------------------------------------------------------------------*/
   
   if (est_report == NULL)
   {
     DS_3GPP_MSG0_ERROR("est_report passed is NULL");
     return;
   }

   if (!ds3gsubsmgr_is_subs_id_valid(est_report->subs_id))
   {
     return;
   }

   if (est_report->moved_to_idle) 
   {
      ds_eps_dl_tput_est_tbl[est_report->subs_id].ds_eps_is_ml1_active = FALSE;
      DS_3GPP_MSG0_HIGH("ML1 moved out of LTE ");
   }
   else
   {
     if (ds_eps_dl_tput_est_tbl[est_report->subs_id].ds_eps_is_ml1_active == FALSE) 
     {
        ds_eps_dl_tput_est_tbl[est_report->subs_id].ds_eps_is_ml1_active = TRUE;
        DS_3GPP_MSG0_HIGH("ML1 moved in to LTE ");
        ds_3gpp_downlink_throughput_hdlr(est_report->subs_id, FALSE);
        return;
     }
   }

   report_interval = ds3gtputmgr_get_est_dl_tput_actual_interval(
      (ds3gsubsmgr_subs_id_e_type)est_report->subs_id);

   if (report_interval == 0 ||
       report_interval == DS3G_INVALID_ACTUAL_INTERVAL )
   {
     DS_3GPP_MSG1_ERROR("Report Frequency is 0 for subs id %d",est_report->subs_id);
     return;
   }

   DS_3GPP_MSG1_HIGH("ds_eps_dl_tput_estimation_update for  ID %d",est_report->subs_id);

   time_get_ms( dl_time_type_now );

   current_time = QW_CVT_Q2N(dl_time_type_now);

   if (est_report->data_freeze == FALSE) 
   {
      DS_3GPP_MSG0_HIGH("est_report->data_freeze is not set");
      alpha = ((ds_eps_dl_estimated_throughput_get_running_t_accumulate(est_report->subs_id)* 
                 DS_EPS_MAX_DL_TPUT_ALPHA) / 
                 (report_interval));

      if (alpha < DS_EPS_MIN_DL_TPUT_ALPHA) 
      {
         alpha = DS_EPS_MIN_DL_TPUT_ALPHA;
      }
       DS_3GPP_MSG1_HIGH("est_alpha %d", alpha);
      DS_3GPP_MSG1_HIGH("est_report->confidence_level %d", est_report->confidence_level);
      if (est_report->confidence_level == LTE_CPHY_DL_TPUT_ESTM_CONFIDENCE_LEVEL_HIGH) 
      {
         confidence_level = DS_EPS_MAX_DL_TPUT_CONFIDENCE_LEVEL;
      }
      else if (est_report->confidence_level == LTE_CPHY_DL_TPUT_ESTM_CONFIDENCE_LEVEL_MEDIUM) 
      {
         confidence_level = DS_EPS_MID_DL_TPUT_CONFIDENCE_LEVEL;
      }
      
      pdcp_total_bytes_count = 
                     ds_eps_bearer_cntxt_get_total_new_dl_byte_count(est_report->subs_id);

      DS_3GPP_MSG1_HIGH("ML1 bytes %lu, ", est_report->additional_avail_bytes);
      DS_3GPP_MSG1_HIGH("PDCP bytes %lu, ", pdcp_total_bytes_count);
      dl_est_total_byte_count = est_report->additional_avail_bytes + pdcp_total_bytes_count;

      time_diff = (current_time - ds_eps_dl_tput_est_tbl[est_report->subs_id].
                           time_since_last_estimatation);

      ds_eps_calculate_update_dl_tput_estimation(est_report->subs_id, dl_est_total_byte_count, 
                                              confidence_level, time_diff, alpha);

      ds_eps_dl_tput_logging(est_report->subs_id,
                             time_diff,
                             est_report->additional_avail_bytes,
                             pdcp_total_bytes_count);
   }
   else
   {
      DS_3GPP_MSG0_HIGH("est_report->data_freeze is set");
   }

   ds_eps_dl_tput_est_tbl[est_report->subs_id].time_since_last_estimatation =  current_time;
}

/*===========================================================================
FUNCTION  DS_EPS_CALCULATE_UPDATE_DL_TPUT_ESTIMATION

DESCRIPTION
  This function calculates and updates the filtered values of estimation


PARAMETERS
  subs_id - Subscription ID
  new_byte_count - new byte count ML1 and PDCP combined
  confidence_level - Confidence level from ML1 (0/500/1000)
  time_difference - bwteen new and old report
  alpha - 0-1000 (for preseving decimal)
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_calculate_update_dl_tput_estimation
(
  sys_modem_as_id_e_type subs_id,
  uint64 new_byte_count,
  uint32 confidence_level,
  uint64 time_difference,
  uint32 alpha
)
{

  uint64 new_tput = 0;
  uint64 old_tput = 0;
  uint32 old_confidence = 0;
  uint32 new_confidence = 0;
  uint32 one_minus_alpha = 0;
  /*---------------------------------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  if(ds_eps_dl_tput_est_tbl[subs_id].ds_eps_ignore_historical_data == TRUE)
  {
    DS_3GPP_MSG1_ERROR("First indication for subs_id %d. ingnore historyical data"
                       ,subs_id);
    alpha = DS_EPS_MAX_DL_TPUT_ALPHA;
    ds_eps_dl_tput_est_tbl[subs_id].ds_eps_ignore_historical_data = FALSE;
  }
  
  one_minus_alpha =  DS_EPS_MAX_DL_TPUT_ALPHA - alpha;
  old_tput = ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est;
  old_confidence = ds_eps_dl_tput_est_tbl[subs_id].
             ds_eps_filtered_confidence_level;

  DS_3GPP_MSG1_HIGH("old_confidence  %lu, ", old_confidence);
  old_confidence = one_minus_alpha * old_confidence;
  DS_3GPP_MSG1_HIGH("old_confidence  weighted %lu, ", old_confidence);

  DS_3GPP_MSG1_HIGH("confidence_level %lu, ", confidence_level);
  confidence_level = alpha *  confidence_level;
  DS_3GPP_MSG1_HIGH("confidence_level  weighted %lu, ", confidence_level);

  new_confidence = (confidence_level + old_confidence)/DS_EPS_MAX_DL_TPUT_ALPHA;

  ds_eps_dl_tput_est_tbl[subs_id].
             ds_eps_filtered_confidence_level = new_confidence;

  DS_3GPP_MSG1_HIGH("filtered_confidence %lu, ", new_confidence);


  DS_3GPP_MSG1_HIGH("old_tput bps %lu, ", old_tput);
  old_tput = one_minus_alpha * old_tput;
  DS_3GPP_MSG1_HIGH("old_tput bps weighted %lu, ", old_tput);

  DS_3GPP_MSG1_HIGH("Total bytes %lu, ", new_byte_count); 
  DS_3GPP_MSG1_HIGH("time_difference %lu, ", time_difference); 
  new_tput = (new_byte_count * DS_EPS_SEC_TO_MS_MULTIPLIER)/time_difference;
  DS_3GPP_MSG1_HIGH("new_tput bps %lu, ", new_tput);
  new_tput = alpha *  new_tput;
  DS_3GPP_MSG1_HIGH("new_tput bps weighted %lu, ", new_tput);

  new_tput = (new_tput + old_tput)/DS_EPS_MAX_DL_TPUT_ALPHA;

  ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est = new_tput;
  DS_3GPP_MSG1_HIGH("filtered_estimation %lu, ", 
          ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est);


}/*ds_eps_calculate_update_dl_tput_estimation*/


/*===========================================================================
FUNCTION  DS_EPS_GET_CURRENT_TPUT_VALUES

DESCRIPTION
  This function prepares the current estimation report in PS format


PARAMETERS
  subs_id - Subscription ID
  ps_sys_dl_throughput_info_type - the PS structure to send the indication
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_get_current_tput_values
(
   sys_modem_as_id_e_type subs_id,
   ps_sys_dl_throughput_info_type *dl_tput_info_type
)
{
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  if (dl_tput_info_type == NULL) 
  {
    DS_3GPP_MSG0_ERROR("dl_tput_info_type passed is NULL");
    return;
  }

  if(DS3G_INVALID_DL_CONFIDENCE == ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_confidence_level)
  {
    dl_tput_info_type->confidence_level = (uint8)DS3G_MAX_DL_CONFIDENCE;
  }
  else
  {
  dl_tput_info_type->confidence_level = 
     (uint8)((ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_confidence_level*7)/DS_EPS_MAX_DL_TPUT_CONFIDENCE_LEVEL);
  }

  DS_3GPP_MSG1_HIGH("Current confidence_level %d, ", dl_tput_info_type->confidence_level); 

  if (DS3G_INVALID_DL_TPUT == ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est)
  {
    dl_tput_info_type->downlink_allowed_rate = (uint32)DS3G_INVALID_DL_TPUT;
  }
  else
  {
  dl_tput_info_type->downlink_allowed_rate = 
     (uint32)(ds_eps_dl_tput_est_tbl[subs_id].ds_eps_filtered_dl_throughput_est*8/(DS_EPS_KBPS_TO_BPS_MULTIPLIER));
  }

  DS_3GPP_MSG1_HIGH("Current downlink_allowed_rate %d, ", 
      dl_tput_info_type->downlink_allowed_rate); 
}/*ds_eps_get_current_tput_values*/


/*===========================================================================
FUNCTION  DS_EPS_DL_ESTIMATED_THROUGHPUT_SET_RUNNING_T_ACCUMULATE

DESCRIPTION
  This function gets t_accumulate nv value from 3gpp nv manager and provides
  callers


PARAMETERS
  subs_id - Subscription ID
  t_accumulate - t_accumulate to set as running t_accumulate
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_estimated_throughput_set_running_t_accumulate
(
   sys_modem_as_id_e_type subs_id,
   uint32 t_accumuate
)
{
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  ds_eps_dl_tput_est_tbl[subs_id].ds_eps_running_t_accumuate = t_accumuate;
}

/*===========================================================================
FUNCTION  DS_EPS_DL_ESTIMATED_THROUGHPUT_GET_RUNNING_T_ACCUMULATE

DESCRIPTION
  This function gets t_accumulate nv value from 3gpp nv manager and provides
  callers


PARAMETERS
  subs_id - Subscription ID
 

DEPENDENCIES
  None.

RETURN VALUE
  uint32 - running t_accumulate of the subscription
  0 - if there is an error

SIDE EFFECTS
  None.

===========================================================================*/
uint32 ds_eps_dl_estimated_throughput_get_running_t_accumulate
(
   sys_modem_as_id_e_type subs_id
)
{
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return 0;
  }

  return ds_eps_dl_tput_est_tbl[subs_id].ds_eps_running_t_accumuate;
}


/*===========================================================================
FUNCTION  DS_EPS_DL_ESTIMATED_IS_ML1_ACTIVE

DESCRIPTION
  This function gets ml1_active status


PARAMETERS
  subs_id - Subscription ID
 

DEPENDENCIES
  None.

RETURN VALUE
  1 - if ml1 is active
  0 - otherwise

SIDE EFFECTS
  None.

===========================================================================*/
uint32 ds_eps_dl_estimated_is_ml1_active
(
   sys_modem_as_id_e_type subs_id
)
{
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return 0;
  }
  DS_LTE_MSG2_LOW("is_ml1_active for subs %d: ",subs_id, 
                   ds_eps_dl_tput_est_tbl[subs_id].ds_eps_is_ml1_active);
  return ds_eps_dl_tput_est_tbl[subs_id].ds_eps_is_ml1_active;
}/*ds_eps_dl_estimated_is_ml1_active*/

/*===========================================================================
FUNCTION  DS_EPS_SET_DL_TPUT_REPORT_FREQUENCY

DESCRIPTION
  This function gets t_accumulate nv value from 3gpp nv manager and provides
  callers


PARAMETERS
  subs_id - Subscription ID
  *t_accumulate - inputer value to be stored pointer
 

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_estimated_throughput_t_accumuate
(
   sys_modem_as_id_e_type subs_id,
   uint32* t_accumulate
)
{
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }
  if (t_accumulate == NULL) 
  {
    DS_3GPP_MSG0_ERROR("t_accumulate passed is NULL");
    return;
  }
  *t_accumulate = ds_3gpp_nv_manager_get_dl_tput_t_accumulate(subs_id);
}


/*===========================================================================
FUNCTION DS_EPS_LTE_CPHY_DL_THROUGHPUT_ESTM_IND_HDLR

DESCRIPTION
  This function is used to handle the message from lTE ML1. This message is
  sent every t_accumulate time from ML1 when requested

  
PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_cphy_dl_throughput_estm_ind_hdlr
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  lte_cphy_dl_tput_estm_report_ind_s  *ds_lte_dl_tput_estm_report_ptr = NULL;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */


  if((msgr_type != LTE_CPHY_DL_TPUT_ESTM_REPORT_IND) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_cphy_dl_throughput_ind_hdlr: Invalid "
                      "message %d or dsmsg_ptr %d",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }


  ds_lte_dl_tput_estm_report_ptr = (lte_cphy_dl_tput_estm_report_ind_s *) dsmsg_ptr;
  DS_LTE_MSG1_HIGH("ds_eps_lte_cphy_dl_throughput_estm_ind_hdlr: received for subs_id "
                      " %d ",ds_lte_dl_tput_estm_report_ptr->subs_id);
  
  ds_eps_dl_tput_process_estimation_indication(ds_lte_dl_tput_estm_report_ptr);

  return TRUE;
}/*ds_eps_lte_cphy_dl_throughput_estm_ind_hdlr*/


/*===========================================================================
FUNCTION DS_EPS_LTE_CPHY_DL_THROUGHPUT_ESTM_IND_HDLR

DESCRIPTION
  This function is used to handle the message from lTE ML1. This message is
  received in reply to requsts every t_accumulate time from ML1 when requested

  
PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type        subs_id = SYS_MODEM_AS_ID_NONE;
  lte_cphy_dl_tput_estm_report_rsp_s *ds_lte_tput_estm_report_rsp_ptr = NULL;
  lte_cphy_dl_tput_estm_report_type_e response_type = LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_START;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  if((msgr_type != LTE_CPHY_DL_TPUT_ESTM_REPORT_RSP) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr: Invalid "
                      "message %d or dsmsg_ptr:0x%x",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }

  ds_lte_tput_estm_report_rsp_ptr = (lte_cphy_dl_tput_estm_report_rsp_s *) dsmsg_ptr;

  DS_LTE_MSG1_HIGH("ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr: received for subs_id "
                      " %d ",ds_lte_tput_estm_report_rsp_ptr->subs_id);

  subs_id =  ds_lte_tput_estm_report_rsp_ptr->subs_id;
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return FALSE;
  }

  response_type = ds_lte_tput_estm_report_rsp_ptr->rsp_type;
  
  if (response_type == LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_START ||
      response_type == LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_UPDATE) 
  {
     if(ds_eps_dl_tput_est_tbl[subs_id].status == 
        DS_EPS_DL_TPUT_ESTM_STATUS_START_WAIT_FOR_ML1)
     {
       DS_LTE_MSG3_HIGH("Received reponse type %d received while status is %d "
                          "for subs_id(CM) %d",response_type, 
                          ds_eps_dl_tput_est_tbl[subs_id].status, subs_id);
       ds_eps_dl_tput_est_tbl[subs_id].status = DS_EPS_DL_TPUT_ESTM_STATUS_START;
     }
     else
     {
        DS_LTE_MSG3_ERROR("Unexpected reponse type %d received while status is %d "
                          "for subs_id(CM) %d",response_type, 
                          ds_eps_dl_tput_est_tbl[subs_id].status, subs_id);
     }
  }
  else if (response_type == LTE_CPHY_DL_TPUT_ESTM_REPORT_TYPE_STOP) 
  {
     if(ds_eps_dl_tput_est_tbl[subs_id].status == 
        DS_EPS_DL_TPUT_ESTM_STATUS_STOP_WAIT_FOR_ML1)
     {
       DS_LTE_MSG3_HIGH("Received reponse type %d received while status is %d "
                          "for subs_id(CM) %d",response_type, 
                          ds_eps_dl_tput_est_tbl[subs_id].status, subs_id);
       ds_eps_dl_tput_est_tbl[subs_id].status = DS_EPS_DL_TPUT_ESTM_STATUS_STOP;
     }
     else
     {
        DS_LTE_MSG3_ERROR("Unexpected reponse type %d received while status is %d "
                          "for subs_id(CM) %d",response_type, 
                          ds_eps_dl_tput_est_tbl[subs_id].status, subs_id);
     }
  }
  
  

  return TRUE;
}/*ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr*/


/*===========================================================================
FUNCTION DS_EPS_SEND_LAST_REPORT_AND_RESET_INFO

DESCRIPTION
  This function sends the report and clears the info. No further information
  will not be stored/reported until LTE is back

  
PARAMETERS
  subs_id - Subscription

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 

void ds_eps_send_last_report_and_reset_info
(
  sys_modem_as_id_e_type subs_id
)
{
  ps_sys_dl_throughput_info_type dl_tput_info;
  /*------------------------------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  DS_3GPP_MSG1_HIGH("ds_eps_s_send_last_report_and_stop_reporting_timer  "
                     "last report tput for subs: %d",subs_id);

  ds_eps_get_current_tput_values (subs_id, &dl_tput_info);
  dl_tput_info.is_suspended = TRUE;
  ds3g_post_downlink_throughput_estimation_ind(dl_tput_info, subs_id);
  ds_eps_dl_tput_estimation_reset(subs_id);
}/*DS_EPS_SEND_LAST_REPORT_AND_RESET_INFO*/



/*===========================================================================
FUNCTION DS_EPS_DL_TPUT_TIMER_EXPIRE_HDLR

DESCRIPTION
  This function is called when the reporting timer expires and 3GPP MH will
  send filtered report to client through PS

PARAMETERS
  subs_id - Subscription for which report is being generated

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_tput_timer_expire_hdlr
(
   sys_modem_as_id_e_type subs_id
)
{
  ps_sys_dl_throughput_info_type dl_tput_info;
  uint32  report_interval = 0;
  boolean continue_reporting = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }
  report_interval = ds3gtputmgr_get_estimated_dl_tput_requested_interval(
      (ds3gsubsmgr_subs_id_e_type)subs_id);
  if (report_interval > 0 && 
      ds_eps_dl_estimated_is_ml1_active(subs_id) &&
      ds_3gpp_bearer_cntxt_check_if_any_bear_up_sys_mode(subs_id, SYS_SYS_MODE_LTE) == TRUE) 
  {
      continue_reporting = TRUE;
  }

  ds_eps_check_and_update_ml1_status(subs_id, continue_reporting, FALSE);
  DS_3GPP_MSG1_HIGH("ds_3gpp_pdn_cntx_dl_tput_timer_expired_cmd_hdlr  "
                     "continue_reporting: %d",continue_reporting);
  if (continue_reporting == TRUE) 
  {
     ds_eps_get_current_tput_values (subs_id, &dl_tput_info);

     DS_3GPP_MSG1_HIGH("ds_3gpp_pdn_cntx_dl_tput_timer_expired_cmd_hdlr  "
                     "reporting regular tput for subs: %d",subs_id);

     ds3g_post_downlink_throughput_estimation_ind(dl_tput_info, subs_id);

  }

} /* ds_3gpp_pdn_cntx_dl_tput_timer_expired_cmd_hdlr */



/*===========================================================================
FUNCTION DS_EPS_DL_TPUT_REPORTING_FREQUENCY_MODIFY_HDLR

DESCRIPTION
  This function is called clients modify the reporting frequency

PARAMETERS
  subs_id - Subscription for which report is being generated
  report_frequency - that is minimul of all clients

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void ds_eps_dl_tput_reporting_frequency_modify_hdlr
(
   sys_modem_as_id_e_type subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  DS_3GPP_MSG1_HIGH("ds_3gpp_pdn_cntx_dl_tput_timer_modified_cmd_hdlr subs_id %d",subs_id);

  ds_eps_bearer_cntxt_reset_to_current_dl_byte_count(subs_id);
  ds_3gpp_downlink_throughput_hdlr(subs_id, TRUE);

} /* ds_3gpp_pdn_cntx_dl_tput_timer_modified_cmd_hdlr */


/*===========================================================================
FUNCTION      DS_EPS_GET_CURRENT_DL_TPUT_ESTIMATION

DESCRIPTION
  Handler for PS_SYS_IOCTL_3GPP_LTE_DL_THROUGHPUT_ESTIMATION. Retrieves
  LTE DL throuhput estimation values cached in MH

PARAMETERS 
  arg_val_ptr: Argument to be populated
  subs_id:     Subscription id
  ps_errno:    Err number if IOCTL cant be handled
   
  
 
DEPENDENCIES
  None.

RETURN VALUE
  0  - Success
  -1 - Failure
 
SIDE EFFECTS
  None.
===========================================================================*/ 
int16 ds_eps_get_current_dl_tput_estimation
(
  void                    *arg_val_ptr,
  sys_modem_as_id_e_type   subs_id,
  int16                   *ps_errno
)
{
  ps_sys_dl_throughput_info_type      *tput_val;
  uint32                               report_interval = 0;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(arg_val_ptr == NULL)
  {
    *ps_errno = DS_EFAULT;
    return -1;
  }

  report_interval = ds3gtputmgr_get_estimated_dl_tput_requested_interval(
      (ds3gsubsmgr_subs_id_e_type)subs_id);

  if (report_interval == 0) 
  {
    DS_LTE_MSG0_ERROR("ds_eps_get_current_dl_tput_estimation: reporting is not"
                      "running, frequency is set to zero ");
    *ps_errno = DS_EOPNOTSUPP;
    return -1;
  }
  /*-----------------------------------------------------------------------  
    Reset output buffer
    num_apn = 0 implicit
   -----------------------------------------------------------------------*/
  memset(arg_val_ptr, 0, sizeof(ps_sys_dl_throughput_info_type));
  tput_val = (ps_sys_dl_throughput_info_type *)arg_val_ptr;

  ds_eps_get_current_tput_values(subs_id, tput_val);

  *ps_errno = 0; /* No error */
  return 0; 
} /*ds_eps_get_current_dl_tput_estimation() */

/*===========================================================================
FUNCTION DS_EPS_LTE_CPHY_IND_CONFIGURED_THROUGHPUT_HDLR

DESCRIPTION
  This function is used to handle the message from LTE lowerlayer.This message 
  is received whenever lte updates the configured data rate changes in either
  uplink or downlink direction
  
PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_cphy_ind_configured_throughput_hdlr
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type        subs_id = SYS_MODEM_AS_ID_NONE;
  lte_cphy_configured_tput_info_ind_s *ds_lte_tput_configured_uldl_report_ptr = NULL;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  if((msgr_type != LTE_CPHY_CONFIGURED_TPUT_INFO_IND) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr: Invalid "
                      "message %d or dsmsg_ptr:0x%x",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }

  ds_lte_tput_configured_uldl_report_ptr = (lte_cphy_configured_tput_info_ind_s *) dsmsg_ptr;
  
  if(ds_lte_tput_configured_uldl_report_ptr != NULL)
  {
    subs_id = INST_ID_TO_SYS_AS_ID(ds_lte_tput_configured_uldl_report_ptr->msgr_hdr.inst_id); 
    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      return FALSE;
    }
    DS_LTE_MSG1_HIGH("ds_eps_lte_cphy_ind_configured_throughput_hdlr: received for subs_id "
                      " %d ",subs_id);
    ds_3gpp_hdlr_report_configured_uldl_tput(subs_id,
                ds_lte_tput_configured_uldl_report_ptr->ul_configured_kbps,
                ds_lte_tput_configured_uldl_report_ptr->dl_configured_kbps);
  }
  return TRUE;
}
/*===========================================================================
FUNCTION DS_EPS_LTE_CPHY_ACHIEVABLE_THROUGHPUT_STATUS_HDLR

DESCRIPTION
  This function is used to handle the message from lTE.This message is
  given by ml1 to provide its connected state.

PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_cphy_achievable_throughput_status_hdlr
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type                 subs_id = SYS_MODEM_AS_ID_NONE;
  lte_cphy_achievable_tput_status_ind_s *ds_lte_tput_achievable_status_ptr = NULL;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -*/

  if((msgr_type != LTE_CPHY_ACHIEVABLE_TPUT_STATUS_IND) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_cphy_cnf_achievable_throughput_hdlr: Invalid"
                      "message %d or dsmsg_ptr:0x%x",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }

  ds_lte_tput_achievable_status_ptr = 
    ( lte_cphy_achievable_tput_status_ind_s *)dsmsg_ptr;
  
  if(ds_lte_tput_achievable_status_ptr != NULL)
  {
    subs_id = INST_ID_TO_SYS_AS_ID(ds_lte_tput_achievable_status_ptr->
                                   msgr_hdr.inst_id); 

    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      return FALSE;
    }

    DS_LTE_MSG2_HIGH("LTE_CPHY_ACHIEVABLE_TPUT_STATUS_IND for "
                      "subs_id %d  ml_is_connected %d",subs_id, 
              ds_lte_tput_achievable_status_ptr->ml1_is_connected);
  
    ds_eps_achievable_ml1_is_connected[subs_id] = ds_lte_tput_achievable_status_ptr->ml1_is_connected; 
    ds3gtputmgr_update_tput_uldl_timer(ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id));

    ds_eps_achievable_tput_stats_info[achievable_index_logging].messageid = LTE_CPHY_ACHIEVABLE_TPUT_STATUS_IND;
    ds_eps_achievable_tput_stats_info[achievable_index_logging].subs_id = subs_id;

    achievable_index_logging++;
    if (achievable_index_logging == DS_EPS_ULDL_TPUT_LOGGING_COUNT)
    {
      achievable_index_logging = 0;
    }   

  }
  return TRUE;
}
/*===========================================================================
FUNCTION DS_EPS_LTE_ML1_IS_CONNECTED

DESCRIPTION
  This function is used to query the ml1 connection status

PARAMETERS
  subs_id

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE if ml1 is connected

SIDE EFFECTS
  None.
===========================================================================*/
boolean ds_eps_lte_ml1_is_connected
(
  sys_modem_as_id_e_type  subs_id
)
{

  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
      return FALSE;
  }
  DS_LTE_MSG2_LOW("ML1 connected state %d for subs_id %d",
         ds_eps_achievable_ml1_is_connected[subs_id],
         subs_id);

  return  ds_eps_achievable_ml1_is_connected[subs_id];
}

/*===========================================================================
FUNCTION DS_EPS_LTE_CPHY_CNF_ACHIEVABLE_THROUGHPUT_HDLR

DESCRIPTION
  This function is used to handle the message from lTE . This message is
  received as a response to the query requesting for lte achievable ul/dl
  data rate

PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_cphy_cnf_achievable_throughput_hdlr
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type               subs_id = SYS_MODEM_AS_ID_NONE;
  lte_cphy_achievable_tput_info_cnf_s *ds_lte_tput_achievable_uldl_ptr = NULL;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -*/

  if((msgr_type != LTE_CPHY_ACHIEVABLE_TPUT_INFO_CNF) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_cphy_cnf_achievable_throughput_hdlr: Invalid"
                      "message %d or dsmsg_ptr:0x%x",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }

  ds_lte_tput_achievable_uldl_ptr = 
    (lte_cphy_achievable_tput_info_cnf_s *)dsmsg_ptr;
  
  if(ds_lte_tput_achievable_uldl_ptr != NULL)
  {
    subs_id = INST_ID_TO_SYS_AS_ID(ds_lte_tput_achievable_uldl_ptr->
                                   msgr_hdr.inst_id); 

    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      return FALSE;
    }

    DS_3GPP_MSG1_HIGH("LTE_CPHY_ACHIEVABLE_TPUT_INFO_CNF for cm "
                      "subs_id %d",subs_id);

    ds3gtputmgr_report_uldl_throughput_response(
       ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id),
       ds_lte_tput_achievable_uldl_ptr->ul_achievable_kbps,
       ds_lte_tput_achievable_uldl_ptr->dl_achievable_kbps);
  }
  return TRUE;
}/*ds_eps_lte_cphy_dl_throughput_report_rsp_hdlr*/
#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION DS_EPS_LTE_MAC_ULCNF_TPUT_INFO_START

DESCRIPTION
  This function is used to handle the message from LTE .This message is
  received as a response to the query requesting to start uplink throughput
  reporting
 
 
PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_mac_ulcnf_tput_info_start_cnf
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type                subs_id = SYS_MODEM_AS_ID_NONE;
  lte_mac_ul_tput_info_start_cnf_msg_s *ds_lte_uplink_throughput_cnf = NULL;
  ds3gsubsmgr_subs_id_e_type            subs_indx = DS3GSUBSMGR_SUBS_ID_INVALID;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -*/

  if((msgr_type != LTE_MAC_UL_TPUT_INFO_START_CNF) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG2_ERROR("ds_eps_lte_mac_ulcnf_tput_info_start: Invalid"
                      "message %d or dsmsg_ptr:0x%x",
                      msgr_type,
                      dsmsg_ptr);
    return FALSE;
  }

  ds_lte_uplink_throughput_cnf = 
    (lte_mac_ul_tput_info_start_cnf_msg_s *)dsmsg_ptr;

  if(ds_lte_uplink_throughput_cnf != NULL)
  {
    subs_id = INST_ID_TO_SYS_AS_ID(ds_lte_uplink_throughput_cnf->hdr.inst_id); 

    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      return FALSE;
    }

    subs_indx = ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id);


    ds_eps_ul_tput_est_tbl[subs_indx].l2reporting = 
      ds_lte_uplink_throughput_cnf->l2_reporting;
    if (ds_eps_ul_tput_est_tbl[subs_indx].treporting != 0)
    {
      ds_eps_ul_tput_est_tbl[subs_indx].alpha =  
        (ds_lte_uplink_throughput_cnf->l2_reporting*DS_EPS_MAX_UL_TPUT_ALPHA)/
          (ds_eps_ul_tput_est_tbl[subs_indx].treporting);
    }
    DS_3GPP_MSG3_HIGH("LTE UPLINK CONF for subs_id %d l2 reporting %d "
                      "alpha %d ",
                      subs_id,ds_eps_ul_tput_est_tbl[subs_indx].l2reporting,
                      ds_eps_ul_tput_est_tbl[subs_indx].alpha);

  }
  return TRUE;
}

/*===========================================================================
FUNCTION DS_EPS_LTE_MAC_ULCNF_TPUT_INFO

DESCRIPTION
  This function is used to handle the message from lTE . This message is
  received as a indication providing the uplink throughput value and
  confidence

PARAMETERS
  msgr_type - UMID of the message sent
  dsmsg_ptr - Pointer to the payload

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is received correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_lte_mac_ul_tput_info
(
  msgr_umid_type              msgr_type,
  const msgr_hdr_struct_type *dsmsg_ptr
)
{
  sys_modem_as_id_e_type              subs_id = SYS_MODEM_AS_ID_NONE;
  lte_mac_ul_tput_info_ind_s          *ds_lte_uplink_throughput_info = NULL;
  ds3gsubsmgr_subs_id_e_type          subs_indx = DS3GSUBSMGR_SUBS_ID_INVALID;
  uint64                              old_ultput=0;
  uint64                              new_ultput=0;
  uint32                              old_ulconf=0;
  uint32                              new_ulconf=0;
  uint32                              alpha = 0;
  uint32                              oneminusalpha = 0;
  uint64                              current_ul_wm_count = 0;

  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -*/

  if((msgr_type != LTE_MAC_UL_TPUT_INFO_IND) ||
     (dsmsg_ptr == NULL))
  {
    DS_LTE_MSG1_ERROR("ds_eps_lte_mac_ul_tput_info: Invalid"
                      "message %d or dsmsg_ptr:0x%x",msgr_type);
    return FALSE;
  }

  ds_lte_uplink_throughput_info = (lte_mac_ul_tput_info_ind_s *)dsmsg_ptr;

  if(ds_lte_uplink_throughput_info != NULL)
  {
    subs_id = INST_ID_TO_SYS_AS_ID(
       ds_lte_uplink_throughput_info->hdr.inst_id); 
    if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
    {
      return FALSE;
    }

    subs_indx = ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id);


    DS_3GPP_MSG1_HIGH("ds_eps_lte_mac_ul_tput_info for cm"
                      "subs_id %d",subs_id);

    old_ultput = ds_eps_ul_tput_est_tbl[subs_indx].ul_throughput_est;
    new_ultput = ds_lte_uplink_throughput_info->uplink_datarate_kbps;

    old_ulconf = ds_eps_ul_tput_est_tbl[subs_indx].ul_confidence_level;
    new_ulconf = ds_lte_uplink_throughput_info->confidence;

    alpha =      ds_eps_ul_tput_est_tbl[subs_indx].alpha;
    oneminusalpha = DS_EPS_MAX_UL_TPUT_ALPHA - alpha;

    current_ul_wm_count = ds_eps_bearer_cntxt_get_tx_wm_count(subs_id);

    if (new_ultput > current_ul_wm_count)
    {
      new_ultput = new_ultput - current_ul_wm_count;
    }
    else
    {
      new_ultput = 0;
    }

    if(oneminusalpha > 0)
    {

      new_ultput = ((new_ultput*alpha) 
                    + (old_ultput*oneminusalpha))/DS_EPS_MAX_UL_TPUT_ALPHA;
      new_ulconf = ((new_ulconf*alpha) 
                    + (old_ulconf*oneminusalpha))/DS_EPS_MAX_UL_TPUT_ALPHA;
    }
    
    ds_eps_ul_tput_est_tbl[subs_indx].ul_throughput_est = (uint32)new_ultput;
    ds_eps_ul_tput_est_tbl[subs_indx].ul_confidence_level = new_ulconf;

    ds3gtputmgr_set_uplink_throughput_rate_params(
       ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id),
       ds_eps_ul_tput_est_tbl[subs_indx].ul_throughput_est,
       ds_eps_ul_tput_est_tbl[subs_indx].ul_confidence_level);
    

    DS_3GPP_MSG3_HIGH("throughput indication for cm"
                      "throughput %d confidence %d alpha %d ",
                      ds_eps_ul_tput_est_tbl[subs_indx].ul_throughput_est,
                      ds_eps_ul_tput_est_tbl[subs_indx].ul_confidence_level,
                      ds_eps_ul_tput_est_tbl[subs_indx].alpha);
  }
  return TRUE;
}
#endif
/*===========================================================================
FUNCTION DS_EPS_THROUGHPUT_ULDL_START_IND

DESCRIPTION
  This function is used to sent to lte lower later to start calculating uldl
  achievable data rate.  


DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is sent correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_througput_uldl_start_ind
(
   sys_modem_as_id_e_type subs_id,
   uint32   timer_value,
   uint32   frequency
)
{
  lte_cphy_achievable_tput_info_start_ind_s ds_lte_uldl_tput_start_req;
  boolean                                   ret_val = FALSE;
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  ds3g_subs_id = ds3gsubsmgr_subs_id_cm_to_ds3g( subs_id );
  if(FALSE == ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  if(!ds_eps_achievable_ml1_is_connected[ds3g_subs_id])
  {
    DS_LTE_MSG1_HIGH("ML1 not is  connected state for subs_id %d",subs_id);
    return FALSE;
  }

  ds_lte_uldl_tput_start_req.tmeas_ms = timer_value;
  ds_lte_uldl_tput_start_req.frequency = frequency;
  DS_LTE_MSG3_HIGH("uldl_start_ind for cm report_time: %d for alpha %d subs id %d",
  	                 timer_value,frequency, subs_id);

  if( TRUE == dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_ACHIEVABLE_TPUT_INFO_START_IND,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_uldl_tput_start_req),
       sizeof(lte_cphy_achievable_tput_info_start_ind_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    ))
  {
    ret_val=TRUE;
  }

  ds_eps_achievable_tput_stats_info[achievable_index_logging].messageid = LTE_CPHY_ACHIEVABLE_TPUT_INFO_START_IND;
  ds_eps_achievable_tput_stats_info[achievable_index_logging].subs_id = subs_id;

  achievable_index_logging++;
  if (achievable_index_logging == DS_EPS_ULDL_TPUT_LOGGING_COUNT)
  {
    achievable_index_logging = 0;
  }


  DS_LTE_MSG1_HIGH("Posting of LTE_CPHY_ACHIEVABLE_TPUT_INFO_START_IND , ret_val %d",
                     ret_val);
  return ret_val;
}
/*===========================================================================
FUNCTION DS_EPS_THROUGHPUT_ULDL_STOP_IND

DESCRIPTION
  This function is used to sent to lte lower later to stop calculating uldl
  achievable data rate.  

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is sent correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_througput_uldl_stop_ind
(
   sys_modem_as_id_e_type subs_id
)
{
  lte_cphy_achievable_tput_info_stop_ind_s ds_lte_uldl_tput_stop_req;
  boolean                                  ret_val=FALSE;
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  ds3g_subs_id = ds3gsubsmgr_subs_id_cm_to_ds3g( subs_id );
  if(FALSE == ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  if(!ds_eps_achievable_ml1_is_connected[ds3g_subs_id])
  {
    DS_LTE_MSG1_HIGH("ML1 not is  connected state for subs_id %d",subs_id);
    return FALSE;
  }

  if( TRUE ==dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_ACHIEVABLE_TPUT_INFO_STOP_IND,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_uldl_tput_stop_req),
       sizeof(lte_cphy_achievable_tput_info_stop_ind_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    ))
  { 
    ret_val = TRUE;
  }
  DS_LTE_MSG1_HIGH("Posting LTE_CPHY_ACHIEVABLE_TPUT_INFO_STOP_IND ret_val %d",
                      ret_val);

  ds_eps_achievable_tput_stats_info[achievable_index_logging].messageid = LTE_CPHY_ACHIEVABLE_TPUT_INFO_STOP_IND;
  ds_eps_achievable_tput_stats_info[achievable_index_logging].subs_id = subs_id;

  achievable_index_logging++;
  if (achievable_index_logging == DS_EPS_ULDL_TPUT_LOGGING_COUNT)
  {
    achievable_index_logging = 0;
  }

  return ret_val;
}
#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION DS_EPS_START_STOP_UPLINK_THROUGHPUT_REPORTING

DESCRIPTION
  This function is used to sent to lte lower later to start,stop calculating
  uplink data rate

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is sent correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
uint32 ds_eps_start_stop_uplink_throughput_reporting
(
   sys_modem_as_id_e_type     subs_id,
   boolean                    start_stop,
   uint32                     reporting_timer
)
{
  uint8                                 instance_id = 0;
  lte_mac_ul_tput_info_start_req_msg_s  ds_lte_uplink_tput_start_req;
  lte_mac_ul_tput_info_stop_req_msg_s   ds_lte_uplink_tput_stop_req;
  uint32                                uplink_lte_taccum =0;
  ds3gsubsmgr_subs_id_e_type            subs_indx = DS3GSUBSMGR_SUBS_ID_INVALID;                    
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return FALSE;
  }

  subs_indx = ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id);
  instance_id = SYS_AS_ID_TO_INST_ID(subs_id);
  uplink_lte_taccum = ds_3gpp_nv_manager_get_uplink_tput_tacccum_lte(subs_id);

  memset(&ds_lte_uplink_tput_start_req,0,
         sizeof(lte_mac_ul_tput_info_start_req_msg_s));
  memset(&ds_lte_uplink_tput_stop_req,0,
         sizeof(lte_mac_ul_tput_info_stop_req_msg_s));

  DS_3GPP_MSG3_HIGH("Sending LTE UPLINK START STOP with "
                    "CM sub ID %d",subs_id,0,0);

  uplink_lte_taccum = ds_3gpp_nv_manager_get_uplink_tput_tacccum_lte(
                         subs_id);
  if(uplink_lte_taccum > reporting_timer )
  {
    reporting_timer = uplink_lte_taccum;
  }

  if (start_stop)
  {
   

    ds_lte_uplink_tput_start_req.taccumulate  = uplink_lte_taccum;
    ds_lte_uplink_tput_start_req.treporting  =  reporting_timer;
    if( FALSE ==dsmsgrsnd_per_subs_msg_send_ext( 
         LTE_MAC_REQ_UL_TPUT_INFO_START,
         MSGR_DS_3GPP, 
         (msgr_hdr_struct_type*)(&ds_lte_uplink_tput_start_req),
         sizeof(lte_mac_ul_tput_info_start_req_msg_s),
         instance_id
      ))
    { 
      DS_LTE_MSG0_ERROR("Posting of LTE_MAC_REQ_UL_TPUT_INFO_START "
                        "failed");
    }  
    ds_eps_ul_tput_est_tbl[subs_id].taccumuate = uplink_lte_taccum;
    ds_eps_ul_tput_est_tbl[subs_id].treporting = reporting_timer;
  }
  else
  {
    if( FALSE ==dsmsgrsnd_per_subs_msg_send_ext( 
         LTE_MAC_REQ_UL_TPUT_INFO_STOP,
         MSGR_DS_3GPP, 
         (msgr_hdr_struct_type*)(&ds_lte_uplink_tput_stop_req),
         sizeof(lte_mac_ul_tput_info_stop_req_msg_s),
         instance_id
      ))
    { 
      DS_LTE_MSG0_ERROR("Posting of LTE_MAC_REQ_UL_TPUT_INFO_STOP "
                        "failed");
    }       
    ds_eps_ul_tput_est_tbl[subs_indx].taccumuate = 0;
    ds_eps_ul_tput_est_tbl[subs_indx].treporting = 0;
    ds_eps_ul_tput_est_tbl[subs_indx].l2reporting = 0;
    ds_eps_ul_tput_est_tbl[subs_indx].ul_confidence_level = 0;
    ds_eps_ul_tput_est_tbl[subs_indx].ul_throughput_est = 0;
    ds_eps_ul_tput_est_tbl[subs_indx].alpha =0;
  }
  
  return reporting_timer;
}
#endif
/*===========================================================================
FUNCTION DS_EPS_THROUGHPUT_ULDL_QUERY_REQ

DESCRIPTION
  This function is used to sent to lte lower later to query uldl
  achievable data rate.  

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is message is sent correctly

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds_eps_througput_uldl_query_req
(
   sys_modem_as_id_e_type subs_id
)
{
  lte_cphy_achievable_tput_info_req_s ds_lte_uldl_achievable_tput_query_req;
  boolean                             ret_val = FALSE;
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */

  ds3g_subs_id = ds3gsubsmgr_subs_id_cm_to_ds3g( subs_id );
  if( FALSE == ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  if(!ds_eps_achievable_ml1_is_connected[ds3g_subs_id])
  {
    DS_LTE_MSG1_HIGH("ML1 not is  connected state for subs_id %d",subs_id);
    return FALSE;
  }

  if(FALSE == dsmsgrsnd_per_subs_msg_send_ext( 
       LTE_CPHY_ACHIEVABLE_TPUT_INFO_REQ,
       MSGR_DS_3GPP, 
       (msgr_hdr_struct_type*)(&ds_lte_uldl_achievable_tput_query_req),
       sizeof(lte_cphy_achievable_tput_info_req_s),
       SYS_AS_ID_TO_INST_ID(subs_id)
    ))
  {
    ret_val = TRUE;
  }
  DS_LTE_MSG2_HIGH("Posting LTE_CPHY_ACHIEVABLE_TPUT_INFO_REQ for subs id %d ret_val %d",
                      subs_id,ret_val);
  return ret_val;
}

#ifdef TEST_FRAMEWORK
#error code not present
#endif

/*===========================================================================
FUNCTION DS_EPS_DL_TPUT_LOGGING

DESCRIPTION
  This funciton is used to output log information for the following log
  packet: LOG_DS_LTE_DL_TPUT_C.
  
PARAMETERS 
  subs_id - subscription on which logging is done 
  time_diff - Time difference since last calculation
  ml1_bytes - total ml1 bytes given to DS during time frame
  pdcp_bytes - total pcdp bytes given to DS during time frame

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds_eps_dl_tput_logging
(
  sys_modem_as_id_e_type subs_id,
  uint64                 time_diff,
  uint64                 ml1_bytes,
  uint64                 pdcp_bytes
)
{
  LOG_DS_LTE_DL_TPUT_C_type      *log_ptr = NULL;
  ps_sys_dl_throughput_info_type  dl_tput_info = {0};
/* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - */

  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    return;
  }

  memset(&dl_tput_info,0,sizeof(ps_sys_dl_throughput_info_type));

  /* Allocate memory for the log ptr */
  log_ptr = (LOG_DS_LTE_DL_TPUT_C_type*)log_alloc(LOG_DS_LTE_DL_TPUT_C,
                                             sizeof(LOG_DS_LTE_DL_TPUT_C_type));

  if (log_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("log_alloc failed");
    return;
  }

  ds_eps_get_current_tput_values(subs_id, &dl_tput_info);

  log_ptr->pkt_version = DS_EPS_DL_TPUT_LOG_VERSION;
  log_ptr->time_diff = time_diff;
  log_ptr->ml1_bytes = ml1_bytes;
  log_ptr->pdcp_bytes = pdcp_bytes;
  log_ptr->filtered_confidence_level = dl_tput_info.confidence_level;
  log_ptr->filtered_avail_tput = dl_tput_info.downlink_allowed_rate;

  log_commit(log_ptr);
} /* ds_eps_dl_tput_logging */

#endif /* FEATURE_DATA_LTE */

