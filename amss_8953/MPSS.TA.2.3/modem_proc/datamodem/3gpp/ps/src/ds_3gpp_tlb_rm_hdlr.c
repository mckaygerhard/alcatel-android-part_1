/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

          U M T S  L O O P B A C K  M E C H A N I S M

GENERAL DESCRIPTION
  This file contains functions necessary for the implementation of the loopback 
  test mechanism. 

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2015 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

                                                                   
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/3gpp/ps/src/ds_3gpp_tlb_rm_hdlr.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/09/15   ss     Initial version.
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#include "msg.h"
#include <stringl/stringl.h>

#include "ds_flow_control.h"
#include "ps_iface.h"
#include "ps_logging_defs.h"
#include "ps_svc.h"
#include "ds_3gpp_tlb_rm_hdlr.h"
#include "ds_eps_tlb.h"
#include "ds_umts_tlb_hdlr.h"
#include "ds_3gpp_bearer_context.h"
#include "ds_3gpp_iface_ev_cb_hdlr.h"
#include "ds_3gpp_phys_link_ev_cb_hdlr.h"

int ds_3gpp_tlb_hdlr_tx_data_cb
(
  ps_iface_type     *iface_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *pkt_instance
);

/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_RX_WM_EACH_ENQUE_CB

DESCRIPTION
  Each enque function for rx_wm of Mode B RM iface  

DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
/*ARGSUSED*/
void ds_3gpp_tlb_hdlr_rx_wm_each_enqueue_cb 
(
  dsm_watermark_type *wm,
  void* callback_data
);

/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_IFACE_SETUP_BRIDGE_INTF_EVENT

DESCRIPTION
  This function calls the registration function to respond to rmnet's flow disable/enable events
  
PARAMETERS  
    
DEPENDENCIES 
  None.

RETURN VALUE 
 0 on SUCCESS
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
int ds_3gpp_tlb_hdlr_rmiface_setup_bridge_intf_event 
( 
  ps_iface_type *this_iface_ptr,
  ps_iface_type *target_iface_ptr,
  void          *client_data
);

void ds_3gpp_tlb_init_rm
(
  ds_3gpp_rm_tlb_state_type_s  *rm_tlb_inst_p,
  void                         *mode_spec_hndl
)
{
  uint8          index;
  ps_iface_type  *iface_ptr = NULL;
  uint32         iface_instance;

#ifdef FEATURE_DATA_PS_DATA_LOGGING
  /*-------------------------------------------------------------------------
    DPL iface ID which uniquely identifies each rm_iface
  -------------------------------------------------------------------------*/
  dpl_iid_ifname_enum_type	   ifname[DS_3GPP_MAX_IFACE_PER_TLB *
									  DS_3GPP_TLB_MAX_INSTANCES] =
  {
    DPL_IID_IFNAME_TLB_RM_IFACE_V4,	 
    DPL_IID_IFNAME_TLB_RM_IFACE_V6
  };
#endif /* FEATURE_DATA_PS_DATA_LOGGING */

  ps_sig_enum_type			  ps_sigs[DS_3GPP_MAX_IFACE_PER_TLB*
                                      DS_3GPP_TLB_MAX_INSTANCES] =
  {
    PS_3GPP_LO_B_UM_RX_Q_SIGNAL,
    PS_3GPP_LO_B_UM_RX_Q_SIGNAL_1
  };

  if (rm_tlb_inst_p->init == TRUE)
  {
    DS_3GPP_MSG0_HIGH("RM tlb instance already inited, return");
    return;
  }
  
 for(index=0; index<DS_3GPP_MAX_IFACE_PER_TLB; index++)
 {
    iface_ptr = &(rm_tlb_inst_p->rm_lo_b_iface[index]);
    /* ---------------------------------------------------------------------
     Create LOOP BACK_MODE_B_IFACE. 
   ------------------------------------------------------------------------*/
    iface_instance = ps_iface_create(iface_ptr, 
                                     LO_MODE_B_IFACE, 
                                     NULL,
                                     NULL,
                                     &(rm_tlb_inst_p->\
                                       rm_lo_phys_link[index]),
                                     1);
    DS_3GPP_MSG1_HIGH("LO_MODE_B Iface created :inst:%d",iface_instance);
    ASSERT(iface_instance >= 0);
    
   rm_tlb_inst_p->rx_sig[index] = (ps_sig_enum_type)ps_sigs[index];

   iface_ptr->client_data_ptr = (void *)rm_tlb_inst_p;

    /*-----------------------------------------------------------------------
     Bridge processing function cmd callback.
   -----------------------------------------------------------------------*/
   iface_ptr->bridge_proc_f_ptr = ds_3gpp_tlb_hdlr_rmiface_setup_bridge_intf_event;
    
    /*-----------------------------------------------------------------------
     Set Rm iface tx cmd as well
   -----------------------------------------------------------------------*/

    ps_iface_set_tx_function(iface_ptr, 
                             ds_3gpp_tlb_hdlr_tx_data_cb, 
                             rm_tlb_inst_p);
  
    
    ps_iface_enable_ind(iface_ptr);
  
    /*-----------------------------------------------------------------------
     Enable flow on the interface.
   -----------------------------------------------------------------------*/    
    ps_iface_enable_flow(iface_ptr, DS_FLOW_UMTS_RMSM_MASK);
  
    /*-----------------------------------------------------------------------
     Initialize the watermarks and queues.
   -----------------------------------------------------------------------*/
    dsm_queue_init ( &(rm_tlb_inst_p->rm_lo_rx_wmk[index]),
                       DS_MODE_B_UL_WM_DNE,
                     &(rm_tlb_inst_p->rm_lo_rx_wmk_q[index]));
   
    rm_tlb_inst_p->rm_lo_rx_wmk[index].each_enqueue_func_ptr = 
                      (wm_cb_type)ds_3gpp_tlb_hdlr_rx_wm_each_enqueue_cb;
                       /* RMSM instance */
    rm_tlb_inst_p->rm_lo_rx_wmk[index].each_enqueue_func_data = 
                       (void*)(iface_ptr);
    /*-----------------------------------------------------------------------
     Enable Logging on RM interfaces.        
   -----------------------------------------------------------------------*/
#ifdef FEATURE_DATA_PS_DATA_LOGGING
   (void) ps_iface_dpl_support_network_logging(iface_ptr, 
                                               ifname[index]);
   (void) ps_iface_dpl_set_iface_desc(iface_ptr, NULL);
#endif /* FEATURE_DATA_PS_DATA_LOGGING */
 
  }/* for .. */
  rm_tlb_inst_p->mode_spec_hndl = mode_spec_hndl;
  rm_tlb_inst_p->init = TRUE;
	
}

/*===========================================================================
FUNCTION DS_LOOPBACK_HDLR_TX_DATA_CB

DESCRIPTION
  This function modifies and loops ping and iperf pkts received back to
  RX WM
  This function is registered with ps_iface for a PDP-IP call 
  
PARAMETERS  
  *iface_ptr       - Interface ptr for the call.
  **item_ptr       - Payload
  *meta_info_ptr   - Not used.
  *pkt_instance    - Call instance.
    
DEPENDENCIES 
  None.
  
RETURN VALUE 
  0 - Success
  
SIDE EFFECTS 
  None.

===========================================================================*/
/*ARGSUSED*/
int ds_3gpp_tlb_hdlr_tx_data_cb
(
  ps_iface_type     *iface_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *pkt_instance
)
{
  int                            rval = 0;
  ds_3gpp_rm_tlb_state_type_s    *rm_tlb_state_ptr;
  ps_iface_type                  *bridge_iface_ptr;
  sys_modem_as_id_e_type          subs_id;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!PS_IFACE_IS_VALID(iface_ptr))
  {
    DS_3GPP_MSG0_ERROR("Invalid iface_ptr passed");
    return -1;
  }
  
  rm_tlb_state_ptr = (ds_3gpp_rm_tlb_state_type_s*)(iface_ptr->client_data_ptr);

  if (NULL == rm_tlb_state_ptr)
  {
    DS_3GPP_MSG0_ERROR("Invalid RM TLB ptr");
    return -1;
  }

  bridge_iface_ptr = ps_iface_bridge_iface(iface_ptr);

  if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
  {
    DS_3GPP_MSG0_ERROR("Invalid bridge iface ptr");
    return -1;
  }

  subs_id = ds3gsubsmgr_subs_id_ds_to_cm(PS_IFACE_GET_SUBS_ID(bridge_iface_ptr));

  DS_BEARER_CTXT_VFR_CALL(rval,
  	                      receive_data_from_network,
  	                      subs_id,
  	                      rm_tlb_state_ptr->mode_spec_hndl,
  	                      iface_ptr,
  	                      item_ptr,
  	                      meta_info_ptr,
  	                      subs_id);
  
  return rval;
}/* ds_3gpp_tlb_hdlr_tx_data_cb */

/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_RX_WM_EACH_ENQUE_CB

DESCRIPTION
  Each enque function for rx_wm of Mode B RM iface  

DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
/*ARGSUSED*/
void ds_3gpp_tlb_hdlr_rx_wm_each_enqueue_cb 
(
  dsm_watermark_type *wm,
  void* callback_data
)
{
  ps_iface_type                     *rm_iface_ptr = 
                                       (ps_iface_type *)callback_data;
  ds_3gpp_rm_tlb_state_type_s       *rm_tlb_state_ptr;
  ps_iface_type                     *bridge_iface_ptr;
  sys_modem_as_id_e_type             subs_id;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(rm_iface_ptr == NULL)
  {
    DS_3GPP_MSG0_HIGH("Invalid call back");
    return;
  }

  rm_tlb_state_ptr = (ds_3gpp_rm_tlb_state_type_s *)rm_iface_ptr->client_data_ptr;

  if (NULL == rm_tlb_state_ptr)
  {
    DS_3GPP_MSG0_HIGH("Invalid rm_tlb_state_ptr");
    return;
  }

  bridge_iface_ptr = ps_iface_bridge_iface(rm_iface_ptr);

  if (!PS_IFACE_IS_VALID(bridge_iface_ptr))
  {
    DS_3GPP_MSG0_ERROR("Invalid bridge iface ptr");
    return;
  }

  subs_id = ds3gsubsmgr_subs_id_ds_to_cm(PS_IFACE_GET_SUBS_ID(bridge_iface_ptr));

  DS_BEARER_CTXT_VF_CALL(process_rx_data,
  	                     subs_id,
  	                     rm_iface_ptr);
}/* dsEpsTlb_hdlr_rx_wm_each_enqueue_cb */

/*===========================================================================
FUNCTION DSEPSTLB_HDLR_IFACE_SETUP_BRIDGE_INTF_EVENT

DESCRIPTION
  This function calls the registration function to respond to rmnet's flow disable/enable events
  
PARAMETERS  
    
DEPENDENCIES 
  None.

RETURN VALUE 
 0 on SUCCESS
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
int ds_3gpp_tlb_hdlr_rmiface_setup_bridge_intf_event 
( 
  ps_iface_type *this_iface_ptr,
  ps_iface_type *target_iface_ptr,
  void          *client_data
)
{
  ps_iface_type                  *bridge_iface_ptr = NULL; /* bridged iface pointer */
  ps_phys_link_type              *rm_phys_link_p = NULL;
  sys_modem_as_id_e_type          subs_id;
  ds_3gpp_rm_tlb_state_type_s     *rm_tlb_state_ptr;
  int                             ret_val=0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  DS_LTE_MSG1_HIGH("dsEpsTlb_hdlr_rmiface_setup_bridge_intf_event()0x%x",
                    this_iface_ptr);

  bridge_iface_ptr = ps_iface_bridge_iface(this_iface_ptr); 

  if(bridge_iface_ptr == NULL)
  {
    DS_LTE_MSG0_HIGH("Setting bridge Iface Ptr to NULL");
    return -1;
  }

  rm_tlb_state_ptr = (ds_3gpp_rm_tlb_state_type_s *)this_iface_ptr->client_data_ptr;

  if (NULL == rm_tlb_state_ptr)
  {
    DS_3GPP_MSG0_HIGH("Invalid rm_tlb_state_ptr");
    return -1;
  }

  rm_phys_link_p = PS_IFACE_GET_PHYS_LINK(this_iface_ptr);
  
  if (rm_phys_link_p == NULL) 
  {
    DATA_ERR_FATAL("rm_phys_link_p = NULL");
    return -1;
  }

  subs_id = ds3gsubsmgr_subs_id_ds_to_cm(PS_IFACE_GET_SUBS_ID(bridge_iface_ptr));

  DS_BEARER_CTXT_VFR_CALL(ret_val,
  	                      setup_rm_data_path,
  	                      subs_id,
  	                      this_iface_ptr,
  	                      bridge_iface_ptr,
  	                      subs_id);

  return ret_val;
}/* dsEpsTlb_hdlr_iface_setup_bridge_intf_event */

/*===========================================================================
FUNCTION DS_3GPP_TLB_BRING_UP_V4_IFACE

DESCRIPTION
  This function process the loopback mode 
  bringing up the V4 UM iface.
  
DEPENDENCIES 
  None.

RETURN VALUE 
 TRUE - SUCCESS
 FALSE - FAILURE
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
ps_iface_type *ds_3gpp_tlb_bring_up_iface 
(
  ds_umts_pdp_type_enum_type  pdp_type,
  uint16                      prof_num,
  sys_modem_as_id_e_type      subs_id
)
{
  acl_policy_info_type              *acl_policy_info = NULL;
  ip_pkt_info_type                   ip_info;
  boolean                            result = TRUE;
  ps_iface_type                     *um_iface_ptr = NULL;
  int16                              ps_iface_errno = DS_EWOULDBLOCK;
  uint8                              instance = DS_3GPP_TLB_IPV4_IFACE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  acl_policy_info = (acl_policy_info_type *)modem_mem_alloc(
                                   sizeof(acl_policy_info_type),
                                   MODEM_MEM_CLIENT_DATA_CRIT );

  if (acl_policy_info == NULL ) 
  {
    DS_3GPP_MSG0_ERROR("FAIL modem_mem_alloc for dsEpsTlb_bring_up_iface");
    return  FALSE;
  }

  /* ACL info population */
  acl_init_policy_info(acl_policy_info);
  memset( acl_policy_info, 0, sizeof(acl_policy_info_type) );
  acl_policy_info->iface.kind = DSS_IFACE_NAME; 
  acl_policy_info->iface.info.name = DSS_IFACE_EPC_ANY;
  acl_policy_info->policy_flag = DSS_IFACE_POLICY_SPECIFIED;
  acl_policy_info->pdp_info = (acl_pdp_ctx_cfg_type) ((uint32)(prof_num));
  acl_policy_info->subs_id = ds3gsubsmgr_subs_id_cm_to_ds(subs_id);
  acl_policy_info->tlb_mode = TRUE;
  acl_policy_info->is_routeable = TRUE; /* in UMTS TLB PDN would be already up and iface in_use */
  /* ip_info population */
  memset( &ip_info, 0, sizeof(ip_info) );
  if( DS_UMTS_PDP_IPV4 == pdp_type)
  {
    ip_info.ip_vsn = IP_V4;
    ip_info.ip_hdr.v4.dest.ps_s_addr = ROUTE_DEFAULT_DST;
    acl_policy_info->ip_family = IPV4_ADDR;
    instance = DS_3GPP_TLB_IPV4_IFACE;
  }
  else if( DS_UMTS_PDP_IPV6 == pdp_type)
  {
    ip_info.ip_vsn = IP_V6;
    acl_policy_info->ip_family = IPV6_ADDR;
    instance = DS_3GPP_TLB_IPV6_IFACE;
  }

  /*-------------------------------------------------------------------------
    Bring_up has to be set to TRUE here so that the in_use flag is to TRUE
    when the IFACE gets assigned.
  -------------------------------------------------------------------------*/
  acl_policy_info->bring_up = TRUE;

  acl_policy_info->allow_less_pref_sys = TRUE;

  if ( route_get( &ip_info, 
                  acl_policy_info, 
                  FALSE, 
                  NULL, 
                  &um_iface_ptr ) < 0 )
  {
    DS_3GPP_MSG0_ERROR("route_get: no iface available");
    modem_mem_free((void *)acl_policy_info, MODEM_MEM_CLIENT_DATA_CRIT );
    return  FALSE;
  }

  DS_3GPP_MSG1_HIGH("Activate Um iface 0x%1x",um_iface_ptr);

  if( result != FALSE )
  {
    /*---------------------------------------------------------------------
      Bring up the Um IFACE interface. If there is an error, 
      delete the allocated instance and return FALSE.
    ---------------------------------------------------------------------*/
    if ( ps_iface_bring_up_cmd( um_iface_ptr, 
                                &ps_iface_errno,
                                NULL ) == 0 )
    {
      /* we need to make sure before that the context is already up or not */
      /* if already up , Take action as if we have got IFACE_IP Event */
      DS_3GPP_MSG1_HIGH("UM i/f 0x%lx already up", um_iface_ptr);
    }
    else if ( ps_iface_errno != DS_EWOULDBLOCK )
    {
      DS_LTE_MSG2_ERROR("UM i/f 0x%lx error %d in bring up", 
                (uint32)um_iface_ptr, ps_iface_errno);
    }
    else
    {
      DS_LTE_MSG1_HIGH("um_iface 0x%lx is being brought up ",
          (uint32)um_iface_ptr);
    }
  }
  modem_mem_free((void *)acl_policy_info, MODEM_MEM_CLIENT_DATA_CRIT );
  return um_iface_ptr;
}/* dsEpsTlb_bring_up_v4_iface */

/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_REG_UM_EVENTS

DESCRIPTION
  This function registers callbacks for UM events
  
DEPENDENCIES 
  None.

RETURN VALUE 
  TRUE -- SUCCESS
  FALSE -- FAILURE
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
boolean ds_3gpp_tlb_hdlr_reg_um_events 
(
  ds_3gpp_rm_tlb_state_type_s *rm_tlb_inst,
  sys_modem_as_id_e_type      subs_id,
  ps_iface_event_enum_type    *iface_ev_ptr,
  uint8                       iface_ev_cnt,
  void (*ds_tlb_um_event_handler_cb)(ps_iface_type *, 
                                     ps_iface_event_enum_type,
                                     ps_iface_event_info_u_type,
                                     void *)
)
{
  void* hndl = NULL;
  boolean ret_val = FALSE;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (NULL == rm_tlb_inst ||
  	  NULL == iface_ev_ptr)
  {
    DS_3GPP_MSG0_ERROR("RM tlb instance passed is NULL");
	return FALSE;
  }

  if (rm_tlb_inst->current_bearer_mode == SYS_SYS_MODE_LTE)
  {
    hndl = (ds_eps_tlb_per_subs_info_type *)
		              rm_tlb_inst->mode_spec_hndl;

    if (!ds_eps_tlb_validate_per_subs_info_hndl(hndl))
    {
      DS_3GPP_MSG0_ERROR("Invalid Subs Info handle");
      return FALSE;
    }
  }
  else if (rm_tlb_inst->current_bearer_mode == SYS_SYS_MODE_WCDMA ||
  	       rm_tlb_inst->current_bearer_mode == SYS_SYS_MODE_TDS ||
  	       rm_tlb_inst->current_bearer_mode == SYS_SYS_MODE_GSM)
  {
    hndl = (ds_umts_tlb_per_subs_info_type *)
		              rm_tlb_inst->mode_spec_hndl;

	if (NULL == hndl)
    {
      DS_3GPP_MSG0_ERROR("Invalid Subs Info handle");
      return FALSE;
    }
  }

  ret_val = ds_3gpp_iface_reg_ev(iface_ev_ptr,
                                 iface_ev_cnt,
                                 ds_tlb_um_event_handler_cb,
                                 NULL,
                                 hndl);
  if(!ret_val)
  {
    DS_3GPP_MSG0_ERROR("TLB Registration failed");
    return FALSE;
  }

  return TRUE;

}/* ds_3gpp_tlb_hdlr_reg_um_events */

/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_DEREG_UM_EVENTS

DESCRIPTION
  This function registers callbacks for UM events
  
DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_tlb_hdlr_dereg_um_events 
(
  ds_3gpp_rm_tlb_state_type_s *rmsm_state_ptr,
  ps_iface_event_enum_type    *iface_ev_p,
  uint8                        iface_ev_cnt,
  void (*ds_tlb_um_event_handler_cb)(ps_iface_type *, 
                                     ps_iface_event_enum_type,
                                     ps_iface_event_info_u_type,
                                     void *)
)
{

  DS_3GPP_MSG0_HIGH("dsEpsTlb_hdlr_dereg_um_events()");

  /* De-register for UP and down events 
     if both UM ifaces are down.*/
  if(ds_3gpp_iface_dereg_ev(iface_ev_p,
                            iface_ev_cnt,
                            ds_tlb_um_event_handler_cb,
                            NULL) != TRUE)
  {
    DS_3GPP_MSG0_ERROR("Iface ev deregistration failed");
  }

}/* ds_3gpp_tlb_hdlr_dereg_um_events */

boolean ds_3gpp_tlb_bringup_um_iface
(
  uint16                       prof_num,
  ds_umts_pdp_type_enum_type   pdp_type,
  sys_modem_as_id_e_type       subs_id,
  ds_3gpp_rm_tlb_state_type_s  *rm_tlb_inst,
  void (*ds_tlb_um_iface_up_evt_hdlr)(ps_iface_type *, void *)  
)
{
  boolean        ret_val = FALSE;

  if (NULL == rm_tlb_inst)
  {
    DS_3GPP_MSG0_ERROR("Invalid rm_inst passed");
	return ret_val;
  }
  
  if((pdp_type == DS_UMTS_PDP_IPV4)||
     (pdp_type == DS_UMTS_PDP_IPV4V6))
  {
    DS_3GPP_MSG0_HIGH("Bring up V4 Um iface ");
    rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE] = 
		       ds_3gpp_tlb_bring_up_iface(DS_UMTS_PDP_IPV4,prof_num, subs_id);
  }
  if((pdp_type == DS_UMTS_PDP_IPV6 )||
     (pdp_type == DS_UMTS_PDP_IPV4V6))
  {
    DS_3GPP_MSG0_HIGH("Bring up V6 Um iface ");
    rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE] =
	 	       ds_3gpp_tlb_bring_up_iface(DS_UMTS_PDP_IPV6,prof_num, subs_id);
  }

  if (PS_IFACE_IS_VALID(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE]))
  {
    ret_val = TRUE;
	if(IFACE_UP == 
       ps_iface_state(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE]))
    {
      /* Send and IFACE_UP_EVENT, if the IFACE is already UP */
      if (NULL != ds_tlb_um_iface_up_evt_hdlr)
      {
        ds_tlb_um_iface_up_evt_hdlr(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE], 
			                        rm_tlb_inst->mode_spec_hndl);
      }
    }
  }

  if (PS_IFACE_IS_VALID(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE]))
  {
    ret_val = TRUE;
	if(IFACE_UP == 
       ps_iface_state(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE]))
    {
      /* Send and IFACE_UP_EVENT, if the IFACE is already UP */
      if (NULL != ds_tlb_um_iface_up_evt_hdlr)
      {
        ds_tlb_um_iface_up_evt_hdlr(rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE], 
			                        rm_tlb_inst->mode_spec_hndl);
      }
    }
  }

  return ret_val;
}

void ds_3gpp_tlb_teardown_um_iface
(
  ds_3gpp_rm_tlb_state_type_s  *rm_tlb_inst,
  void (*ds_tlb_um_iface_down_evt_hdlr)(ps_iface_type *, void *)  
)
{
  int16          ps_errno = 0;
  int            v4_ret_val = 0;
  int            v6_ret_val = 0;
  ps_iface_type  *um_v4_iface_ptr;
  ps_iface_type  *um_v6_iface_ptr;
  
  if (NULL == rm_tlb_inst)
  {
    DS_3GPP_MSG0_ERROR("Invalid rm_tlb_inst passed");
	return;
  }

  um_v4_iface_ptr = rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV4_IFACE];
  um_v6_iface_ptr = rm_tlb_inst->um_iface_ptr[DS_3GPP_TLB_IPV6_IFACE];

  if ( PS_IFACE_IS_VALID(um_v4_iface_ptr) &&
  	   (IFACE_UP == um_v4_iface_ptr->iface_private.state ||
  	    IFACE_ROUTEABLE == um_v4_iface_ptr->iface_private.state) )
  {
    v4_ret_val = ps_iface_tear_down_cmd(um_v4_iface_ptr,&ps_errno,NULL);

	if ( (v4_ret_val == 0) && (NULL != ds_tlb_um_iface_down_evt_hdlr))
	{
	  DS_3GPP_MSG0_HIGH("V4 iface down already, handle iface_down");
	  ds_tlb_um_iface_down_evt_hdlr(um_v4_iface_ptr, rm_tlb_inst->mode_spec_hndl);
	}
  }

  if ( PS_IFACE_IS_VALID(um_v6_iface_ptr) &&
  	   (IFACE_UP == um_v6_iface_ptr->iface_private.state ||
  	    IFACE_ROUTEABLE == um_v6_iface_ptr->iface_private.state) )
  {
    v6_ret_val = ps_iface_tear_down_cmd(um_v6_iface_ptr,&ps_errno,NULL);

	if ( (v6_ret_val == 0) && (NULL != ds_tlb_um_iface_down_evt_hdlr))
	{
	  DS_3GPP_MSG0_HIGH("V4 iface down already, handle iface_down");
	  ds_tlb_um_iface_down_evt_hdlr(um_v6_iface_ptr, rm_tlb_inst->mode_spec_hndl);
	}
  }
}


/*===========================================================================
FUNCTION DS_3GPP_TLB_RM_EVENT_HANDLER_CB

DESCRIPTION

PARAMETERS  
    
DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
LOCAL void ds_3gpp_tlb_rm_event_handler_cb
(
  ps_phys_link_type           *rm_phys_link_ptr,
  ps_iface_event_enum_type     event,
  ps_iface_event_info_u_type   event_info,
  void                        *user_data_ptr
)
{
  ds_3gpp_rm_tlb_state_type_s  *rmsm_state_ptr;
  uint8                        instance;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  rmsm_state_ptr = (ds_3gpp_rm_tlb_state_type_s*)user_data_ptr;

  if (NULL == rmsm_state_ptr)
  {
    DS_3GPP_MSG0_ERROR("Invalid RM tlb handle");
    return;
  }

  /*---------------------------------------------------------------------------
    Take action based on the event. Set the Rm_flow_controlled flag in the 
    RMSTATE according to the event received. 
  ---------------------------------------------------------------------------*/
  DS3GPPTLB_GET_INST_FROM_RM_PHYS_LINK_PTR(rm_phys_link_ptr,
                                          instance, 
                                          rmsm_state_ptr);
  
  if( instance >= DS_3GPP_MAX_IFACE_PER_TLB )
  {
    DS_LTE_MSG1_ERROR("Unknown instance: %d. Ignoring",instance);
    return;
  }
  
  switch(event)
  {
    case PHYS_LINK_FLOW_ENABLED_EV:
      rmsm_state_ptr->rm_flow_enabled[instance] = TRUE;
      DS_3GPP_MSG0_HIGH("PHYS_LINK_FLOW_ENABLED_EV for rm iface");
      PS_SET_SIGNAL(rmsm_state_ptr->rx_sig[instance]);
      break;

    case PHYS_LINK_FLOW_DISABLED_EV:
      rmsm_state_ptr->rm_flow_enabled[instance] = FALSE;
      DS_3GPP_MSG0_HIGH("PHYS_LINK_FLOW_DISABLED_EV for Rm iface ");
      break;
      
    default:
      {
        DS_3GPP_MSG1_ERROR("Unknown event: %d. Ignoring",event);
      }
  }
}/* dsEpsTlb_rm_event_handler_cb */


/*===========================================================================
FUNCTION DS_3GPP_TLB_HDLR_REG_RM_EVENTS

DESCRIPTION
  This function registers callbacks for RM phys link flow enable and disable events
  
DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_tlb_hdlr_reg_rm_events 
(
  ps_phys_link_type* rm_phys_link_p,
  ds_3gpp_rm_tlb_state_type_s *rmsm_state_ptr,
  ps_iface_event_enum_type *phys_link_ev_p,
  uint8                     phys_link_ev_cnt
)
{
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(rmsm_state_ptr != NULL);
  ASSERT(rm_phys_link_p != NULL);

  DS_3GPP_MSG0_HIGH("dsEpsTlb_hdlr_reg_rm_events");

  /*-------------------------------------------------------------------------
    Register for PHYS link Flow enabled and Flow disabled events
  -------------------------------------------------------------------------*/
  if(ds_3gpp_phys_link_reg_ev(phys_link_ev_p,phys_link_ev_cnt,
                              ds_3gpp_tlb_rm_event_handler_cb,
                              rm_phys_link_p,rmsm_state_ptr)!= TRUE)
  {
    DS_3GPP_MSG0_ERROR("dsEpsTlb_hdlr_reg_rm_events failure");
  }
}/* dsEpsTlb_hdlr_reg_rm_events */

/*===========================================================================
FUNCTION DSEPSTLB_HDLR_DEREG_RM_EVENTS

DESCRIPTION
  This function de-registers callbacks for RM phys link flow enable and disable events
  
PARAMETERS  
    
DEPENDENCIES 
  None.

RETURN VALUE 
  None
 
SIDE EFFECTS 
  None.
  
===========================================================================*/
void ds_3gpp_tlb_hdlr_dereg_rm_events 
(
  ps_phys_link_type* rm_phys_link_p,
  ds_3gpp_rm_tlb_state_type_s *rmsm_state_ptr,
  ps_iface_event_enum_type *phys_link_ev_p,
  uint8                     phys_link_ev_cnt
)
{  
  ASSERT(rmsm_state_ptr != NULL);

  DS_LTE_MSG0_HIGH("dsEpsTlb_hdlr_dereg_rm_events(),inst(%d)");

  (void)ds_3gpp_phys_link_dereg_ev(phys_link_ev_p,
                                   phys_link_ev_cnt,
                                   ds_3gpp_tlb_rm_event_handler_cb,
                                   rm_phys_link_p);

  return;
}/* dsEpsTlb_hdlr_dereg_rm_events*/


boolean ds_3gpp_tlb_setup_rm
(
  ds_3gpp_rm_tlb_state_type_s  *rm_tlb_inst,
  uint8                         instance,
  ps_iface_event_enum_type     *phys_link_ev_p,
  uint8                         phys_link_ev_cnt,
  boolean                      (*ds_tlb_rm_rx_data_cb)(ps_sig_enum_type,
                                                       void *)
)
{
  ps_phys_link_link_protocol_handle_type protocol_handle;

  if (NULL == rm_tlb_inst)
  {
    DS_3GPP_MSG0_ERROR("RM tlb instance passed is invalid");
	return FALSE;
  }

  memset(&protocol_handle, 0, sizeof(ps_phys_link_link_protocol_handle_type));
    
  /*-------------------------------------------------------------------------
   Set the handler for the downlink signal for the call.
   -------------------------------------------------------------------------*/
  (void)ps_set_sig_handler(
                 rm_tlb_inst->rx_sig[instance],
                 ds_tlb_rm_rx_data_cb,
                 (void*)&(rm_tlb_inst->rm_lo_b_iface[instance]));

  /*-------------------------------------------------------------------------  
     Enable it.
  ------------------------------------------------------------------------*/
  ps_enable_sig(rm_tlb_inst->rx_sig[instance]);

  dsm_empty_queue(&(rm_tlb_inst->rm_lo_rx_wmk[instance]));

  rm_tlb_inst->rm_lo_rx_wmk[instance].total_rcvd_cnt = 0;
  rm_tlb_inst->rm_lo_rx_wmk[instance].lowater_func_ptr = NULL; /* No point in doing FC */

  rm_tlb_inst->rm_lo_rx_wmk[instance].lowater_func_data = 
                          (void *)&(rm_tlb_inst->rm_lo_b_iface[instance]);
  
  rm_tlb_inst->rm_lo_rx_wmk[instance].hiwater_func_ptr = NULL; /* No point in doing FC */
  
  rm_tlb_inst->rm_lo_rx_wmk[instance].hiwater_func_data = 
                          (void *)&(rm_tlb_inst->rm_lo_b_iface[instance]);

  /* Set High Watermark before low */
  dsm_set_hi_wm(&(rm_tlb_inst->rm_lo_rx_wmk[instance]), DS_MODE_B_UL_WM_HI);
  dsm_set_low_wm(&(rm_tlb_inst->rm_lo_rx_wmk[instance]), DS_MODE_B_UL_WM_LO);
  dsm_set_dne(&(rm_tlb_inst->rm_lo_rx_wmk[instance]), DS_MODE_B_UL_WM_DNE);
  if(instance == DS_3GPP_TLB_IPV4_IFACE)
  {
    protocol_handle.none_handle.handle.ip_proto_handle.v4_iface_ptr = 
                      &(rm_tlb_inst->rm_lo_b_iface[instance]);
  }
  else
  {
    protocol_handle.none_handle.handle.ip_proto_handle.v6_iface_ptr = 
                      &(rm_tlb_inst->rm_lo_b_iface[instance]);
  }

  protocol_handle.none_handle.high_protocol = PS_PHYS_LINK_HIGHER_LAYER_PROTOCOL_IP;

  /*---------------------------------------------------------------------------
    Set the link layer protocol to NONE and pass the protocol handle into the
    function that will do this. The protocol handle has just been populated
    with the higher layer protocol and the pointers to the ifaces that might
    communicate over this physlink.
  ---------------------------------------------------------------------------*/
 (void) ps_phys_link_set_link_protocol(
                    &(rm_tlb_inst->rm_lo_phys_link[instance]),
                    PS_PHYS_LINK_LINK_PROTOCOL_NONE,
                    protocol_handle);

  ps_iface_up_ind( &(rm_tlb_inst->rm_lo_b_iface[instance]) );
  ps_iface_phys_link_up_ind(&(rm_tlb_inst->rm_lo_b_iface[instance]));

  ds_3gpp_tlb_hdlr_reg_rm_events(&(rm_tlb_inst->rm_lo_phys_link[instance]),
  	                             rm_tlb_inst,
                                 phys_link_ev_p,
                                 phys_link_ev_cnt);

  ps_phys_link_enable_flow (
               &(rm_tlb_inst->rm_lo_phys_link[instance]), 
               DS_FLOW_UMTS_RMSM_MASK);

  return TRUE;
}

void ds_3gpp_tlb_hdlr_cleanup_rm
(
  ds_3gpp_rm_tlb_state_type_s  *rmsm_state_ptr,
  ps_iface_event_enum_type     *phys_link_ev_p,
  uint8                         phys_link_ev_cnt,
  sys_modem_as_id_e_type        subs_id
)
{
  uint8  index;
  ps_iface_down_mh_event_info_type   down_info;

  memset(&down_info, 0, sizeof(ps_iface_down_mh_event_info_type));

  if (NULL == rmsm_state_ptr)
  {
    DS_3GPP_MSG0_ERROR("Invalid rm tlb handle");
	return;
  }
  
  for(index = DS_3GPP_TLB_IPV4_IFACE; index<DS_3GPP_MAX_IFACE_PER_TLB; index++)
  {
    if (NULL != rmsm_state_ptr->um_iface_ptr[index])
    {
      ps_iface_set_bridge(&(rmsm_state_ptr->rm_lo_b_iface[index]),
                          NULL );
      ds_3gpp_tlb_hdlr_dereg_rm_events(
                             &(rmsm_state_ptr->rm_lo_phys_link[index]),
                             rmsm_state_ptr,
                             phys_link_ev_p,
                             phys_link_ev_cnt);
      dsm_empty_queue(&(rmsm_state_ptr->rm_lo_rx_wmk[index]));
      ps_iface_phys_link_down_ind(&(rmsm_state_ptr->rm_lo_b_iface[index]));
  
      down_info.down_reason = PS_NET_DOWN_REASON_TLB_REGULAR_DEACTIVATION;
      down_info.bearer_tech.technology = DS_SYS_NETWORK_3GPP;
      down_info.bearer_tech.rat_value = ds_3gpp_map_sys_mode_to_ds_sys_rat(
	  	                             ds3gpp_get_current_network_mode(subs_id));
      down_info.bearer_tech.so_mask = ds_3gpp_bearer_cntxt_get_so_mask(subs_id);
  
      ps_iface_down_ind( &(rmsm_state_ptr->rm_lo_b_iface[index]), &down_info);
      rmsm_state_ptr->um_iface_ptr[index]= NULL;
    }
  }/* for .. */
}

