
/*!
  @file
  ds_3gpp_support_iwlan.h

  @brief
  Internal utility functions and routines

  @detail
  OPTIONAL detailed description of this C file.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/data.mpss/3.4.3.1/3gpp/ps/src/ds_3gpp_iwlan_to_lte_ho.c#1 $

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "ds_3gpp_iwlan_to_lte_ho.h"
#include "ds_3gpp_pdn_context.h"

#ifdef FEATURE_IWLAN_HO_SUPPORT

/*===========================================================================

                           HEADER DECLARTIONS

===========================================================================*/

ds_3gpp_iwlan_to_lte_ho_info_type ds_3gpp_iwlan_to_lte_ho_info[DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX];
extern ds_pdn_context_s ds_pdn_context_tbl[DS_3GPP_MAX_PDN_CONTEXT];

/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_CONFIG_V6_PARAMS

DESCRIPTION
  This function trigger configures v6 address from IWLAN side to LTE and fakes RA indication
 
PARAMETERS
  dssnet6_sm_cb_type *instance_ptr - pointer to dssnet6_sm_cb_type
 
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

=============================================================================*/
void ds_3gpp_iwlan_to_lte_ho_config_v6_params
(
   dssnet6_sm_cb_type *instance_ptr
)
{
  uint64                         gateway_iid = 0;
  ds_profile_num_type            3gpp_profile_id = 0;
  uint8                          index = 0;
  ds_3gpp_iface_s               *ds_iface_ptr = NULL;
  ds_pdn_context_s              *pdn_cntx_p = NULL;

  if (instance_ptr == NULL) 
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,
                      "instance_ptr is NULL");
    return;
  }

  do 
  {
    /*---------------------------------------------------------------------
      Set V6 IP Addr to Iface
    ---------------------------------------------------------------------*/
    if (instance_ptr->is_ehrpd_call) 
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,
                      "this is eHRPD call, not setting v6 prefix");
      break;
    }

    /*---------------------------------------------------------------------
      Check to see if ps_iface_ptr is not set to NULL. 
    ---------------------------------------------------------------------*/
    if (NULL == instance_ptr->ps_iface_ptr) 
    {
      DATA_3GMGR_MSG0(MSG_LEGACY_HIGH,
                      "NULL PS Iface, not setting v6 prefix");
      break;
    } 
    else 
    {
      

      /*-------------------------------------------------------------------------
       Get the DS Iface from the PS Iface
      -------------------------------------------------------------------------*/
      DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                      "instance_ptr 0x%x, PS IFace 0x%x",
                      instance_ptr,
                      instance_ptr->ps_iface_ptr);

      if (NULL == instance_ptr->ps_iface_ptr->client_data_ptr) 
      {
        DATA_3GMGR_MSG1(MSG_LEGACY_HIGH,
                        "DS IFACE cannot be obtained from the PS IFace (0x%x), not setting v6 prefix",
                        instance_ptr->ps_iface_ptr);
        break;
      }

      ds_iface_ptr = (ds_3gpp_iface_s *)(instance_ptr->ps_iface_ptr->client_data_ptr);

      /*-------------------------------------------------------------------------
       Get the pdn cntx instance.
      -------------------------------------------------------------------------*/
      pdn_cntx_p = (ds_pdn_context_s *)(ds_iface_ptr->client_data_ptr);

      if (NULL == pdn_cntx_p) 
      {
        DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "PDN context is NULL, not setting v6 prefix");
        break;
      }

      if (NULL == pdn_cntx_p->ds_pdn_context_dyn_p) 
      {
        DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "ds_pdn_context_dyn_p is NULL, not setting v6 prefix");
        break;
      }

      3gpp_profile_id = (ds_profile_num_type)instance_ptr->ps_iface_ptr->iface_private.profile_id_3gpp;

      DATA_3GMGR_MSG2(MSG_LEGACY_HIGH, "profile_id_3gpp:%d, pdn_cntx_p 0x%x",
                      3gpp_profile_id,
                      pdn_cntx_p);

      /* This should always be the same */
      if (pdn_cntx_p->ds_pdn_context_dyn_p->pdp_profile_num != 3gpp_profile_id) 
      {
        DATA_3GMGR_MSG2(MSG_LEGACY_ERROR,
                        "Profile num mismatch, pdn_cntx_p->pdp_profile_num:%d, mav_3gpp_profile_id:%d",
                        pdn_cntx_p->ds_pdn_context_dyn_p->pdp_profile_num,
                        3gpp_profile_id);
        break;
      }

      index = ds_3gpp_iwlan_to_lte_ho_find_pdn_index(3gpp_profile_id);

      if (index == DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX) 
      {
        break;
      }

      /*  Check if the is_valid and HO flags are set */
      DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                      "HO flag:%d HO prefix valid:%d",
                      ds_3gpp_iwlan_to_lte_ho_info[index].handover_flag,
                      ds_3gpp_iwlan_to_lte_ho_info[index].ipv6_addr_valid);

      if (TRUE == ds_3gpp_iwlan_to_lte_ho_info[index].handover_flag &&
          TRUE == ds_3gpp_iwlan_to_lte_ho_info[index].ipv6_addr_valid) 
      {
        DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "HO PDN set and IPv6 addr valid");

        if (-1 == ps_iface_apply_v6_prefix(instance_ptr->ps_iface_ptr,
                                           gateway_iid,
                                           ds_3gpp_iwlan_to_lte_ho_info[index].ipv6_addr.ps_s6_addr64[0],
                                           ds_3gpp_iwlan_to_lte_ho_info[index].valid_lifetime,
                                           ds_3gpp_iwlan_to_lte_ho_info[index].pref_lifetime,
                                           ds_3gpp_iwlan_to_lte_ho_info[index].prefix_length
                                           )
            ) 
        {
          DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Applying V6 Prefix to iface failed ");
          ds_3gpp_iwlan_to_lte_ho_reset_ho_info(3gpp_profile_id);
          break;
        }

        DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Prefix set successfully");

        /*--------------------------------------------------------------------
              Applied prefix successfully Generate a valid RA indication
            --------------------------------------------------------------------*/
        ps_iface_valid_ra_ind(instance_ptr->ps_iface_ptr, ds_3gpp_iwlan_to_lte_ho_info[index].ra_life_time);

        DATA_3GMGR_MSG0(MSG_LEGACY_HIGH, "Sent RA ind");
        ds_3gpp_iwlan_to_lte_ho_reset_ho_info(3gpp_profile_id);
      }
    }
  }
  while (0);
}/*ds_3gpp_configure_v6_iwlan_to_lte_ho*/

/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_IS_PDN_INFO_VALID

DESCRIPTION
  This function checks if the pdn index is valid
 
PARAMETERS
  uint8 index - index of pdn in iwlan_to_lte_ho table
 
DEPENDENCIES
  None.

RETURN VALUE
  boolean.

SIDE EFFECTS
  None

=============================================================================*/
boolean ds_3gpp_iwlan_to_lte_ho_is_pdn_info_valid
(
  uint8 index
)
{
  DS_3GPP_MSG1_HIGH("is_iwlan_to_lte_ho_pdn_info_valid %d",
                    ds_3gpp_iwlan_to_lte_ho_info[index].is_valid);

  return (ds_3gpp_iwlan_to_lte_ho_info[index].is_valid);
}/*ds_3gpp_iwlan_to_lte_ho_is_pdn_info_valid*/


/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_FIND_PDN_INDEX

DESCRIPTION
  This function finds the pdn index from the profile number
 
PARAMETERS
  ds_profile_num_type profile_num
 
DEPENDENCIES
  None.

RETURN VALUE
  uint8 index - index of pdn in iwlan_to_lte_ho table

SIDE EFFECTS
  None

=============================================================================*/
uint8 ds_3gpp_iwlan_to_lte_ho_find_pdn_index
(
  ds_profile_num_type profile_num
)
{
  uint8 index = 0;

  for (index = 0; index < DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX; index++)
  {
    if(ds_3gpp_iwlan_to_lte_ho_info[index].is_valid == FALSE)
    {
      continue;
    }

    if(profile_num == ds_3gpp_iwlan_to_lte_ho_info[index].profile_num)
    {
      DS_3GPP_MSG2_HIGH("match found for iwlan_to_lte_ho_info, profile %d, index %d",
                        profile_num, index);
      return (index);
    }
  }

  return (DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX);

}/*ds_3gpp_iwlan_to_lte_ho_find_pdn_index*/

/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_GET_NEW_PDN_INDEX

DESCRIPTION
  This function create new entry in iwlan_to_lte_ho
  table and returns the pdn index from the profile number
 
PARAMETERS
  ds_profile_num_type profile_num
 
DEPENDENCIES
  None.

RETURN VALUE
  uint8 index - index of pdn in iwlan_to_lte_ho table

SIDE EFFECTS
  None

=============================================================================*/
uint8 ds_3gpp_iwlan_to_lte_ho_get_new_pdn_index
(
  ds_profile_num_type profile_num
)
{
  uint8 index = ds_3gpp_iwlan_to_lte_ho_find_pdn_index(profile_num);

  if(index == DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX)
  {
    for(index = 0; index < DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX; index++)
    {
      if(ds_3gpp_iwlan_to_lte_ho_info[index].is_valid == FALSE)
      {
        DS_3GPP_MSG2_HIGH("using new index %d for profile %d",
                          index, profile_num);
        ds_3gpp_iwlan_to_lte_ho_info[index].is_valid = TRUE;
        ds_3gpp_iwlan_to_lte_ho_info[index].profile_num = profile_num;
        return (index);
      }
    }

    DS_3GPP_MSG0_ERROR("no valid index found for iwlan_to_lte_ho_info");
  }
  else
  {
    DS_3GPP_MSG2_HIGH("found already valid index %d for profile %d",
                      index, profile_num);
    return (index);
  }
}/*ds_3gpp_iwlan_to_lte_ho_get_new_pdn_index*/

/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_IS_HO_ENABLED_FOR_PDN

DESCRIPTION
  This function checks to see if ho_to_lte is enbaled with for pdn
 
PARAMETERS
  ds_profile_num_type profile_num
 
DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE/FALSE

SIDE EFFECTS
  None

=============================================================================*/
boolean ds_3gpp_iwlan_to_lte_ho_is_ho_enabled_for_pdn
(
  ds_profile_num_type profile_num
)
{
  uint8 index = ds_3gpp_iwlan_to_lte_ho_find_pdn_index(profile_num);

  if(index == DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX)
  {
    return (FALSE);
  }

  return (ds_3gpp_iwlan_to_lte_ho_info[index].handover_flag);
}/*ds_3gpp_iwlan_to_lte_ho_is_ho_enabled_for_pdn*/

/*===========================================================================
FUNCTION DS_3GPP_IWLAN_TO_LTE_HO_RESET_HO_INFO

DESCRIPTION
  This function resets iwlan_to_lte_ho table
 
PARAMETERS
  ds_profile_num_type profile_num
 
DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void ds_3gpp_iwlan_to_lte_ho_reset_ho_info
(
  ds_profile_num_type profile_num
)
{
  uint8 index = ds_3gpp_iwlan_to_lte_ho_find_pdn_index(profile_num);

  if(index < DS_3GPP_IWLAN_TO_LTE_HO_PDN_MAX)
  {
    DS_3GPP_MSG2_HIGH("reset index %d for profile %d",
                      index, profile_num);

    memset(&ds_3gpp_iwlan_to_lte_ho_info[index],
           0,
           sizeof(ds_3gpp_iwlan_to_lte_ho_info[index]));

    ds_3gpp_iwlan_to_lte_ho_info[index].is_valid = FALSE;
  }
}
#endif /*FEATURE_IWLAN_HO_SUPPORT*/

