/******************************************************************************
  @file    ds_profile_qmi.c
  @brief   DS PROFILE - QMI related code, which is not technology specific

  DESCRIPTION
  DS PROFILE - QMI related code, which is not technology specific

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

  $Header: //components/rel/data.mpss/3.4.3.1/interface/dsprofile/src/ds_profile_qmi.c#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies Incorporated. 
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
