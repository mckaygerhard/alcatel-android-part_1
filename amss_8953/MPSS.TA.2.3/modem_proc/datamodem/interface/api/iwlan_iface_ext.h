#ifndef IWLAN_IFACE_EXT_H
#define IWLAN_IFACE_EXT_H
/*===========================================================================

              IW L A N _ I F A C E _ E X T . H

DESCRIPTION
   This is IWLAN iface handler external header file. It contains the
   external function declarations.


Copyright (c) 2007-2009 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

  $Header: //components/rel/data.mpss/3.4.3.1/interface/api/iwlan_iface_ext.h#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "customer.h"

/*---------------------------------------------------------------------------
  Used 3GPP+IWLAN Ifaces
---------------------------------------------------------------------------*/
extern uint8  num_used_3gpp2_v4_ifaces;

#endif /* IWLAN_IFACE_EXT_H */
