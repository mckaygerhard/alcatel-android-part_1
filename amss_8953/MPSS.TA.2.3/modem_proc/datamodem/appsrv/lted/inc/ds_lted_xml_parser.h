/*==============================================================================

                        ds_lted_xml_parser.h

GENERAL DESCRIPTION
  Decoder for LTED

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/21/14    ml     Created file/Initial version.
==============================================================================*/
#ifndef DS_LTED_XML_PARSER_H
#define DS_LTED_XML_PARSER_H

#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY

#include "comdef.h"


#define DS_LTED_PLMN_MAX_LEN 50
#define DS_LTED_ANNOUNCING_POLICY_LIST_MAX_SIZE 50
#define DS_LTED_MONITORING_POLICY_LIST_MAX_SIZE 50
#define DS_LTED_DEDICATED_APN_NAME_MAX_SIZE 50
#define DS_LTED_TLS_PSK_PASSWORD_MAX_LEN 50




typedef enum
{
  DS_LTED_RANGE_UNSET,
  DS_LTED_RANGE_SHORT,
  DS_LTED_RANGE_MEDIUM,
  DS_LTED_RANGE_LONG
} ds_lted_announcing_range_e;



typedef struct
{
  char   plmn[DS_LTED_PLMN_MAX_LEN];
  uint32 validity_timer;
} ds_lted_monitoring_policy_info_s;



typedef struct
{
  char                       plmn[DS_LTED_PLMN_MAX_LEN];
  uint32                     validity_timer;
  ds_lted_announcing_range_e range;
} ds_lted_announcing_policy_info_s;



typedef struct
{
  uint32                           announcing_policy_list_size;
  ds_lted_announcing_policy_info_s announcing_policy_list[DS_LTED_ANNOUNCING_POLICY_LIST_MAX_SIZE];

  uint32                           monitoring_policy_list_size;
  ds_lted_monitoring_policy_info_s monitoring_policy_list[DS_LTED_MONITORING_POLICY_LIST_MAX_SIZE];

  char dedicated_apn_name[DS_LTED_DEDICATED_APN_NAME_MAX_SIZE];

  char tls_psk_password[DS_LTED_TLS_PSK_PASSWORD_MAX_LEN];
} ds_lted_provision_info_s;




#ifdef __cplusplus
extern "C"
{
#endif

boolean ds_lted_parse_provision_xml(const char* xml_content, ds_lted_provision_info_s* provision_info);

#ifdef __cplusplus
}
#endif


#endif /* FEATURE_LTE_DISCOVERY */
#endif /* DS_LTED_XML_PARSER_H */
