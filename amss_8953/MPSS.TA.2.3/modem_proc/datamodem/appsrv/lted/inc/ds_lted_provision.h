#ifndef DS_LTED_PROVISION_H
#define DS_LTED_PROVISION_H
/*===========================================================================
 
                         DS_LTED_PROVISION.H

DESCRIPTION
  Header file of DS LTE-D provision module.
 
Copyright (c) 2016 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/lted/inc/ds_lted_provision.h#2 $
  $DateTime: 2016/04/07 15:08:53 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/01/16   hr      Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY

#include "comdef.h"
#include "ds_lted_msg.h"
#include "mmgsdilib_common.h"

/*===========================================================================
                      EXTERNAL FUNCTIONS DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DS_LTED_PROVISION_NV_EFS_INIT
 
DESCRIPTION   Read EFS for LTE-D provision XML, called during bootup and 
              NV refresh EVENT
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_nv_efs_init
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_RESET
 
DESCRIPTION   Reset the LTE-D provision
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_reset
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_INIT
 
DESCRIPTION   Initializes the LTE-D provision module
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_init
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_START
 
DESCRIPTION   Starts LTE-D provision procedure
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_start
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_READ_MSISDN

DESCRIPTION   Reads MSISDN
 
DEPENDENCIES  Subscription needs be ready

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_read_msisdn
(
  mmgsdi_session_id_type        session_id,
  mmgsdi_session_type_enum_type session_type
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_PROCESS_HTTP_CB_EVENT

DESCRIPTION   HTTP callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_process_http_cb_event
(
  ds_lted_http_cb_event_info_type *http_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_PROCESS_REFRESH_TIMER_CB_EVENT

DESCRIPTION   Refresh timer expiry callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_process_refresh_timer_cb_event
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_PROCESS_RETRY_TIMER_CB_EVENT

DESCRIPTION   Retry timer expiry callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_process_retry_timer_cb_event
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_PROCESS_SMS_WAIT_TIMER_CB_EVENT

DESCRIPTION   SMS wait timer expiry callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_process_sms_wait_timer_cb_event
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_PROCESS_SMS_RECEIVED_EVENT

DESCRIPTION   SMS wait timer expiry callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_provision_process_sms_received_event
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_GET_KEY

DESCRIPTION   Returns the PSK retrieved by provision procedure
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_lted_provision_get_key
(
  uint8  *key_buf,
  uint16  buf_len,
  uint16 *key_len
);

/*===========================================================================
FUNCTION      DS_LTED_PROVISION_IS_IN_PROGRESS

DESCRIPTION   Returns the if provision is in progress
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_lted_provision_is_in_progress
(
  void
);

#endif /* FEATURE_LTE_DISCOVERY */
#endif /* DS_LTED_PROVISION_H */
