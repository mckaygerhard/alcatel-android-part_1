/*===========================================================================
 
                          DS_APPSRV_WMS_IF.C

DESCRIPTION
  Interface between appsrv and wms
 
Copyright (c) 2016 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/utils/src/ds_appsrv_wms_if.c#1 $
  $DateTime: 2016/03/24 02:59:44 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/04/16   hr      Initial version
===========================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "datamodem_variation.h"

#include "comdef.h"
#include "data_msg.h"
#include "modem_mem.h"
#include "wms.h"
#include "ds_appsrv_wms_if.h"
#include "ds_appsrv_task.h"

/*===========================================================================
                               GLOBAL VARIABLES
===========================================================================*/
/*--------------------------------------------------------------------------
  To record the APPSRV WMS interface client ID
--------------------------------------------------------------------------*/
static wms_client_id_type ds_appsrv_wms_client_id = 0xFF;

/*--------------------------------------------------------------------------
  Table for the WMS interface clients within APPSRV
--------------------------------------------------------------------------*/
static ds_appsrv_wms_if_client_info_type *ds_appsrv_wms_if_client_tbl[DS_APPSRV_MODULE_MAX] = {0};

/*===========================================================================
                           INTERNAL FUNCTION DECLARE
===========================================================================*/
/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_PARSE_MSG_CALLBACK

DESCRIPTION   Message parsing callback registered with WMS service. Will 
              invoke client callbacks within APPSRV, until one of the client
              decides to consume the message exclusively
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
static void ds_appsrv_wms_if_parse_msg_callback
(
  const wms_client_message_s_type *msg_ptr,
  boolean *shared
)
{
  uint8 i = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0; i < DS_APPSRV_MODULE_MAX; i++)
  {
    if(ds_appsrv_wms_if_client_tbl[i])
    {
      if ((ds_appsrv_wms_if_client_tbl[i]->is_activated) &&
          (ds_appsrv_wms_if_client_tbl[i]->parse_msg_cb))
      {
        ds_appsrv_wms_if_client_tbl[i]->parse_msg_cb(msg_ptr, shared);

        if (*shared == FALSE)
        {
          return;
        }
      }
    }
  }

  return;
}/* ds_appsrv_wms_if_parse_msg_callback */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_MSG_EVENT_CALLBACK

DESCRIPTION   Message event callback registered with WMS service. Will 
              invoke client callbacks within APPSRV, until one of the client
              decides to consume the message exclusively
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
static void ds_appsrv_wms_if_msg_event_callback
(
  wms_msg_event_e_type       event,
  wms_msg_event_info_s_type *info_ptr,
  boolean *shared
)
{
  uint8 i = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0; i < DS_APPSRV_MODULE_MAX; i++)
  {
    if(ds_appsrv_wms_if_client_tbl[i])
    {
      if ((ds_appsrv_wms_if_client_tbl[i]->is_activated) &&
          (ds_appsrv_wms_if_client_tbl[i]->msg_event_cb))
      {
        ds_appsrv_wms_if_client_tbl[i]->msg_event_cb(event, info_ptr, shared);

        if (*shared == FALSE)
        {
          return;
        }
      }
    }
  }

  return;
}/* ds_appsrv_wms_if_msg_event_callback */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_ARE_ALL_CLIENT_INACTIVE

DESCRIPTION   Check and return if none of the APPSRV module has activated 
              WMS service 
 
DEPENDENCIES  None

RETURN VALUE  TRUE  if none of the module has activated WMS service 
              FALSE if at least one of the module has activated WMS service 

SIDE EFFECTS  None
===========================================================================*/
static boolean ds_appsrv_wms_if_are_all_client_inactive
(
  void
)
{
  uint8 i = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0; i < DS_APPSRV_MODULE_MAX; i++)
  {
    if (ds_appsrv_wms_if_client_tbl[i])
    {
      if(ds_appsrv_wms_if_client_tbl[i]->is_activated)
      {
        return FALSE;
      }
    }
  }

  return TRUE;
}/* ds_appsrv_wms_if_are_all_client_inactive */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_ARE_ALL_CLIENT_INVALID

DESCRIPTION   Check and return if none of the APPSRV module has registered 
              WMS service 
 
DEPENDENCIES  None

RETURN VALUE  TRUE  if none of the module has registered WMS service 
              FALSE if at least one of the module has registered WMS service 

SIDE EFFECTS  None
===========================================================================*/
static boolean ds_appsrv_wms_if_are_all_client_invalid
(
  void
)
{
  uint8 i = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0; i < DS_APPSRV_MODULE_MAX; i++)
  {
    if(ds_appsrv_wms_if_client_tbl[i])
    {
      return FALSE;
    }
  }

  return TRUE;
}/* ds_appsrv_wms_if_are_all_client_invalid */

/*===========================================================================
                                FUNCTIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_INIT

DESCRIPTION   Initiates APPSRV WMS interface
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_init
(
  ds_appsrv_module_type module_id
)
{
  wms_client_err_e_type wms_client_err = WMS_CLIENT_ERR_MAX;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (module_id >= DS_APPSRV_MODULE_MAX)
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  if (ds_appsrv_wms_client_id == 0xFF)
  {
    wms_client_err = wms_client_init( WMS_CLIENT_TYPE_WAP_MMS, 
                                      &ds_appsrv_wms_client_id );


    DATA_APPSRV_MSG2(MSG_LEGACY_HIGH, 
                     "APPSRV registering wms services, client ID %d, err %d ",
                     ds_appsrv_wms_client_id, wms_client_err);

    if( wms_client_err != WMS_CLIENT_ERR_NONE )
    {
      ds_appsrv_wms_client_id = 0xFF;

      return wms_client_err;
    }

    wms_client_err = 
      wms_client_reg_parse_msg_cb(ds_appsrv_wms_client_id, 
                                  ds_appsrv_wms_if_parse_msg_callback);

    if (wms_client_err != WMS_CLIENT_ERR_NONE)
    {
      (void)wms_client_release(ds_appsrv_wms_client_id);
      ds_appsrv_wms_client_id = 0xFF;

      DATA_APPSRV_MSG1(MSG_LEGACY_ERROR,
                       "Failed to register WMS parse msg callback err %d", 
                       wms_client_err);

      return wms_client_err;
    }

    wms_client_err = 
      wms_client_reg_msg_cb(ds_appsrv_wms_client_id, 
                            ds_appsrv_wms_if_msg_event_callback);

    if (wms_client_err != WMS_CLIENT_ERR_NONE)
    {
      (void)wms_client_release( ds_appsrv_wms_client_id);
      ds_appsrv_wms_client_id = 0xFF;

      DATA_APPSRV_MSG1(MSG_LEGACY_ERROR,
                       "Failed to register WMS msg event callback err %d", 
                       wms_client_err);

      return wms_client_err;
    }
  }

  ds_appsrv_wms_if_client_tbl[module_id] = 
    (ds_appsrv_wms_if_client_info_type*)modem_mem_alloc(
                                  sizeof(ds_appsrv_wms_if_client_info_type),
                                  MODEM_MEM_CLIENT_DATA);

  if (ds_appsrv_wms_if_client_tbl[module_id] == NULL)
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "Memory allocation failed!");

    if (ds_appsrv_wms_if_are_all_client_invalid())
    {
      (void)wms_client_release(ds_appsrv_wms_client_id);
      ds_appsrv_wms_client_id = 0xFF;
    }

    return WMS_CLIENT_ERR_GENERAL;
  }
  else
  {
    memset((void*)ds_appsrv_wms_if_client_tbl[module_id], 0, 
           sizeof(ds_appsrv_wms_if_client_info_type));
  }

  DATA_APPSRV_MSG1(MSG_LEGACY_HIGH, 
                   "AAPSRV module %d registered wms services",
                   module_id);

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_client_init */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_RELEASE

DESCRIPTION   Deregister WMS service for the module
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_release
(
  ds_appsrv_module_type module_id
)
{
  if (module_id >= DS_APPSRV_MODULE_MAX)
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  if (ds_appsrv_wms_if_client_tbl[module_id])
  {
    modem_mem_free(ds_appsrv_wms_if_client_tbl[module_id], 
                   MODEM_MEM_CLIENT_DATA);

    ds_appsrv_wms_if_client_tbl[module_id] = NULL;

    if (ds_appsrv_wms_if_are_all_client_invalid())
    {
      (void)wms_client_release(ds_appsrv_wms_client_id);
      ds_appsrv_wms_client_id = 0xFF;
    }
  }

  DATA_APPSRV_MSG1(MSG_LEGACY_HIGH, 
                   "AAPSRV module %d deregistered wms services",
                   module_id);

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_client_release */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_ACTIVATE

DESCRIPTION   Activate WMS service for the module so that callbacks can be 
              received. If APPSRV WMS interface is not activated, will go
              ahead and activate itself.
 
DEPENDENCIES  Client already called ds_appsrv_wms_if_client_init

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_activate
(
  ds_appsrv_module_type module_id
)
{
  wms_client_err_e_type wms_client_err = WMS_CLIENT_ERR_MAX;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ((module_id >= DS_APPSRV_MODULE_MAX) ||
      (ds_appsrv_wms_if_client_tbl[module_id] == NULL) )
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  if (ds_appsrv_wms_if_are_all_client_inactive())
  {
    wms_client_err = wms_client_activate(ds_appsrv_wms_client_id);

    if (wms_client_err != WMS_CLIENT_ERR_NONE)
    {
      ds_appsrv_wms_if_client_tbl[module_id]->is_activated = FALSE;

      DATA_APPSRV_MSG1(MSG_LEGACY_ERROR,
                       "Failed to activate APPSRV WMS client error %d", 
                       wms_client_err);

      return wms_client_err;
    }
  }

  ds_appsrv_wms_if_client_tbl[module_id]->is_activated = TRUE;
  DATA_APPSRV_MSG0(MSG_LEGACY_HIGH, "APPSRV module %d activated WMS services");

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_client_activate */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_DEACTIVATE

DESCRIPTION   Deactivate WMS service for the module so that callbacks will 
              not be invoked. Deactivates APPSRV WMS interface if no module
              is active for WMS service anymore
 
DEPENDENCIES  Client already called ds_appsrv_wms_if_client_init

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_deactivate
(
  ds_appsrv_module_type module_id
)
{
  boolean all_client_inactive = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ((module_id >= DS_APPSRV_MODULE_MAX) ||
      (ds_appsrv_wms_if_client_tbl[module_id] == NULL))
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  ds_appsrv_wms_if_client_tbl[module_id]->is_activated = FALSE;

  all_client_inactive = ds_appsrv_wms_if_are_all_client_inactive();

  if (all_client_inactive)
  {
    wms_client_deactivate(ds_appsrv_wms_client_id);
  }

  DATA_APPSRV_MSG2(MSG_LEGACY_HIGH, 
                   "Module %d deactivated WMS service, all client inactive %d",
                   module_id, all_client_inactive);

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_client_deactivate */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_REG_PARSE_MSG_CB

DESCRIPTION   Register message parsing callback with APPSRV WMS interface
 
DEPENDENCIES  Client already called ds_appsrv_wms_if_client_init

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_reg_parse_msg_cb
(
  ds_appsrv_module_type    module_id,
  wms_cl_parse_msg_cb_type msg_parsing_cb
)
{
  if ((module_id >= DS_APPSRV_MODULE_MAX) ||
      (ds_appsrv_wms_if_client_tbl[module_id] == NULL))
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  ds_appsrv_wms_if_client_tbl[module_id]->parse_msg_cb = msg_parsing_cb;

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_reg_parse_msg_cb */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_REG_MSG_CB

DESCRIPTION   Register message event callback with APPSRV WMS interface
 
DEPENDENCIES  Client already called ds_appsrv_wms_if_client_init

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_reg_msg_cb
(
  ds_appsrv_module_type module_id,
  wms_msg_event_cb_type msg_event_cb
)
{
  if ((module_id >= DS_APPSRV_MODULE_MAX) ||
      (ds_appsrv_wms_if_client_tbl[module_id] == NULL))
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "Invalid module ID %d", module_id);

    return WMS_CLIENT_ERR_CLIENT_ID;
  }

  ds_appsrv_wms_if_client_tbl[module_id]->msg_event_cb = msg_event_cb;

  return WMS_CLIENT_ERR_NONE;
}/* ds_appsrv_wms_if_reg_msg_cb */

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_MSG_ACK

DESCRIPTION   Wrapper for wms_msg_ack
 
DEPENDENCIES  APPSRV WMS interface mush have been initialized

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_status_e_type ds_appsrv_wms_if_msg_ack
(
  wms_cmd_cb_type            cmd_cb,
  const void                *user_data,
  const wms_ack_info_s_type *ack_info_ptr
)
{ 
  return wms_msg_ack(ds_appsrv_wms_client_id, cmd_cb, user_data, ack_info_ptr);
}/* ds_appsrv_wms_if_msg_ack */


