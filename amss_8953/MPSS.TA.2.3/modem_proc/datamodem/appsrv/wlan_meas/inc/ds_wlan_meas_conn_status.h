#ifndef DS_WLAN_MEAS_CONN_STATUS_H
#define DS_WLAN_MEAS_CONN_STATUS_H
/*===========================================================================

                 WLAN_MEAS_CONN_STATUS HEADER FILE


DESCRIPTION
  This file contains data declarations and function prototypes 
  
EXTERNALIZED FUNCTIONS 
 
 Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
4/30/15   youjunf      created
===========================================================================*/


/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "ds_wlan_meas_cmd_hdlr.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/


/*===========================================================================
FUNCTION      DS_WLAN_MEAS_CONN_STATUS_INIT

DESCRIPTION   This function initializes wlan connection status module.

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/

void ds_wlan_meas_conn_status_init(void);



/*===========================================================================
FUNCTION DS_WLAN_MEAS_PROCESS_WLAN_SYS_CHG_CMD

DESCRIPTION
  This function process the DS_WLAN_MEAS_CMD_WLAN_SYS_CHG cmd
  
PARAMETERS 
  wlan_status_ptr: wlan status pointer 
    
DEPENDENCIES  None.

RETURN VALUE  None
 
SIDE EFFECTS  None.
===========================================================================*/
void ds_wlan_meas_process_wlan_sys_chg_cmd
(
  ds_wlan_meas_wlan_status_type *wlan_status_ptr
);


#endif /* DS_WLAN_MEAS_CONN_STATUS_H */

