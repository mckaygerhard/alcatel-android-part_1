/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/09/29 21:52:15 $
 * $Header: //components/rel/core.mpss/3.9.1/buses/spmi/bear/src/platform/os/mpss/SpmiOsLogs.c#1 $
 * $Change: 9128810 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOsLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

ULogHandle spmiLogHandle;
