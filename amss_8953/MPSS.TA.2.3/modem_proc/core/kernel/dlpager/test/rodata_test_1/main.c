#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <qurt.h>

#include <dlpager_main.h>
#include <memload_handler.h>

#define PAGE_SIZE (4 * 1024) // 4 KB
#define SWAP_POOL_SIZE (50 * 1024) // 100 KB

#define MAX_FAULTING_THREADS 8
#define TEST_THREAD_STACK_SIZE (4 * 1024)
#define TEST_RANGE  64*1024

extern unsigned int swapped_data [];

extern unsigned int __swapped_segments_start__;
extern unsigned int __swapped_segments_end__;

extern void dump_global_table (void);
extern void dump_swap_table (void);
extern void dump_log_table (void);
extern void sim_load_setup (void);

qurt_thread_t tid[MAX_FAULTING_THREADS];

void unit_test_page_miss (void)
{
    int i, size;
    qurt_thread_t thread_id;
    unsigned int addr, fault_addr;

    addr = (unsigned int)&__swapped_segments_start__;
    size = (unsigned int)&__swapped_segments_end__ - (unsigned int)&__swapped_segments_start__;

    thread_id = qurt_thread_get_id ();
    for (i = 0; i < size/PAGE_SIZE; i++) {
        fault_addr = addr + (i * PAGE_SIZE);
        memload_fault_handler (fault_addr, thread_id);
    }
}

void faultThreadEntry (void *arg)
{
    int i, fault_idx;
    unsigned int start, size, value;

    fault_idx = (int)arg;

    size = TEST_RANGE / MAX_FAULTING_THREADS;
    start = (fault_idx * size);
    printf ("*******thread %d access range from %x -- %x\n\n\n", fault_idx, start, start + size);

    for (i = start; i < start + size; i++) {
        value = swapped_data[i];
        if (value != 0xdeadbeef) {
            printf("deadbeef pattern not found, thread (%d), addr (0x%x), value (0x%x)\n", fault_idx, &swapped_data[i], value);
        }
    }

    printf("\n>>>>>>>successfully tested resuming faulting thread no. %x after demand loading\n\n\n", fault_idx);

    qurt_thread_exit(0);
}

int dlpager_get_attr (dlpager_attr_t *pAttr)
{

    pAttr->swap_pool[0].size = SWAP_POOL_SIZE;
    pAttr->swap_pool[0].page_size = PAGE_SIZE;
    pAttr->num_swap_pools = 1;

    return 0;
}

int main (int arc, char **argv)
{
    int i, r, r1, status;
    volatile int *ptr=&r;
    qurt_thread_attr_t thread_attr;
    char name[QURT_THREAD_ATTR_NAME_MAXLEN];
    void *stack_ptr;
    int prio;

    r = dlpager_init ();
    assert (r == 0);

    printf ("dl_pager is started\n");
    printf (".....\n Unit Testing \n....\n");

    /* Wait till pager is started. Delay without using timer */
    for (i = 0; i < 0xffff; i++) {
        r1 = *ptr;
    }

    /* Setup Simulated load */
    sim_load_setup ();

    qurt_thread_attr_init (&thread_attr);

    for (i = 0; i < MAX_FAULTING_THREADS; i++) {
        stack_ptr = malloc (TEST_THREAD_STACK_SIZE);
        assert (stack_ptr != NULL);
        snprintf (name, QURT_THREAD_ATTR_NAME_MAXLEN, "faultTh_%d", i);
        prio = 100 + i;
        qurt_thread_attr_set_name (&thread_attr, name);
        qurt_thread_attr_set_stack_addr (&thread_attr, stack_ptr);
        qurt_thread_attr_set_stack_size (&thread_attr, TEST_THREAD_STACK_SIZE);
        qurt_thread_attr_set_priority (&thread_attr, prio);
        status = qurt_thread_create (&tid[i], &thread_attr, faultThreadEntry, (void *)i);
        printf("thread %d created id = 0x%x\n", i, tid[i]);
        assert(QURT_EOK==status);
    }

    for (i = 0; i < MAX_FAULTING_THREADS; i++) {
        r = qurt_thread_join (tid[i], &status ); 
        printf( "faulting thread %d exit return %p status %p\n", i, r, status);
    }

    dump_global_table ();
    dump_swap_table ();
    dump_log_table ();

    printf ("Done testing pager\n");
    exit (0);
}
