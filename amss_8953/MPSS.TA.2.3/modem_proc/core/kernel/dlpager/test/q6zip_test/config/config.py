

# main settings
# =============


class GEN_OPTIONS:
        NUM_PAGES      = 10
        ALGORITHM      = "RW"
        USE_FASTISS    = True
        USE_ELF_INPUT  = True
        IS_JENKINS_RUN = False


class ELF_INPUT: 
        ELF          = '..\\..\\..\\..\\..\\build\\ms\\orig_MODEM_PROC_IMG_TAAHANAAQ.elf'
        #SECTION      = '.rw_candidate_compress_section'
        SECTION      = '.candidate_compress_section'
        START        = 0


class CRASHDUMP_INPUT:
        EXTRACTED_RW_DATA = "\\\\cporter\\Dropbox\\for_jenkins\\rw_alg_profiling\\v1.1\\get_rw_data.out"


class RW_OPTIONS:
        PY_COMPRESSOR_PATH            = "..\\..\\compressor\\rw\\v1.1\\bin"
        RUNTIME_DECOMPRESSOR_INC_PATH = "..\\..\\compressor\\rw\\v1.1\\inc"
        RUNTIME_DECOMPRESSOR_SRC      = "..\\..\\compressor\\rw\\v1.1\\src\\rw_compress.c"
        RUNTIME_COMPRESSOR_INC_PATH   = "..\\..\\compressor\\rw\\v1.1\\inc"
        RUNTIME_COMPRESSOR_SRC        = "..\\..\\compressor\\rw\\v1.1\\src\\rw_compress.c"
        NUM_RECOMPRESSIONS            = 1


class RO_OPTIONS:
        PY_COMPRESSOR_PATH            = "..\\..\\compressor\\ro\\v3-d2\\bin"
        RUNTIME_DECOMPRESSOR_INC_PATH = "..\\..\\compressor\\ro\\v3-d2\\inc"
        RUNTIME_DECOMPRESSOR_SRC      = "..\\..\\compressor\\ro\\v3-d2\\src\\q6zip_uncompress.c"


class JENKINS_OPTIONS:
        OUTPUT_FOLDER = "\\\\espresso-pc71\\dropbox\\compression_algorithm_profiling"




# tools
# =====

HEXAGON_CLANG   = 'C:\\Qualcomm\\HEXAGON_Tools\\6.4.02\\qc\\bin\\hexagon-clang.exe'
HEXAGON_OBJCOPY = 'C:\\Qualcomm\\HEXAGON_Tools\\6.4.02\\gnu\\bin\\qdsp6-objcopy'
HEXAGON_SIM     = 'C:\\Qualcomm\\HEXAGON_Tools\\6.4.02\\qc\\bin\\hexagon-sim' 

HEXAGON_GCC     = 'C:\\Qualcomm\\HEXAGON_Tools\\5.0.08\\gnu\\bin\\hexagon-gcc.exe'

HEXAGON_FASTISS = 'C:\\Qualcomm\\HEXAGON_Tools\\5.0.11-002\\bin\\hexagon-fastiss.exe' 






# compiler settings
# =================


HEXAGON_GCC_COMPILE_FLAGS  = ' -O2'
HEXAGON_GCC_COMPILE_FLAGS += ' -c'
HEXAGON_GCC_COMPILE_FLAGS += ' '
HEXAGON_GCC_INCLUDE_FLAGS  = ' -I ..\\..\\inc'
HEXAGON_GCC_INCLUDE_FLAGS += ' -I ..\\..\\..\\..\\..\\core\\api\\kernel\\qurt'
HEXAGON_GCC_INCLUDE_FLAGS += ' -I C:\\Qualcomm\\HEXAGON_Tools\\6.4.02\\qc\\include\\common'
HEXAGON_GCC_INCLUDE_FLAGS += ' '

HEXAGON_GCC_INCLUDE_FLAGS_COMP   = ' '
HEXAGON_GCC_INCLUDE_FLAGS_DECOMP = ' '

if GEN_OPTIONS.ALGORITHM == "RW":
        HEXAGON_GCC_INCLUDE_FLAGS_DECOMP += ' -I '+RW_OPTIONS.RUNTIME_DECOMPRESSOR_INC_PATH
        HEXAGON_GCC_INCLUDE_FLAGS_COMP   += ' -I '+RW_OPTIONS.RUNTIME_COMPRESSOR_INC_PATH
else:
        HEXAGON_GCC_INCLUDE_FLAGS_DECOMP += ' -I '+RO_OPTIONS.RUNTIME_DECOMPRESSOR_INC_PATH

HEXAGON_CLANG_COMPILE_FLAGS  = ' -O2'
HEXAGON_CLANG_COMPILE_FLAGS += ' '

HEXAGON_CLANG_LIBRARY_FLAGS = ' -lhexagon'
HEXAGON_CLANG_LIBRARY_FLAGS += ' '

HEXAGON_CLANG_INCLUDE_FLAGS  = ' -I ..\\..\\..\\..\\..\\core\\api\\kernel\\qurt'
HEXAGON_CLANG_INCLUDE_FLAGS_COMP   = ' '
HEXAGON_CLANG_INCLUDE_FLAGS_DECOMP = ' '
if GEN_OPTIONS.ALGORITHM == "RW":
        HEXAGON_CLANG_INCLUDE_FLAGS_DECOMP += ' -I '+RW_OPTIONS.RUNTIME_DECOMPRESSOR_INC_PATH
        HEXAGON_CLANG_INCLUDE_FLAGS_COMP   += ' -I '+RW_OPTIONS.RUNTIME_COMPRESSOR_INC_PATH
else:
        HEXAGON_CLANG_INCLUDE_FLAGS_DECOMP += ' -I '+RO_OPTIONS.RUNTIME_DECOMPRESSOR_INC_PATH

CPU_FLAG  = ' -mv5 '

RW_DECOMPRESS_DRIVER_SRC_FILES  = ' .\\c_drivers\\q6zip_rw_test.c'
RW_DECOMPRESS_DRIVER_SRC_FILES += ' .\\c_drivers\\test_deltaUncompress.c'
RW_DECOMPRESS_DRIVER_SRC_FILES += ' ..\\..\\src\\q6_dczero.s'
RW_DECOMPRESS_DRIVER_SRC_FILES += ' '
RW_COMPRESS_DRIVER_SRC_FILES  = ' .\\c_drivers\\q6zip_rw_test.c'
RW_COMPRESS_DRIVER_SRC_FILES += ' .\\c_drivers\\test_deltaCompress.c'
RW_COMPRESS_DRIVER_SRC_FILES += ' ..\\..\\src\\q6_dczero.s'
RW_COMPRESS_DRIVER_SRC_FILES += ' '

RW_DECOMPRESS_DRIVER = 'decompress_test'
RW_COMPRESS_DRIVER   = 'compress_test'
RW_DECOMPRESS_DRIVER_OUTPUT_FLAG = ' -o ' + RW_DECOMPRESS_DRIVER + ' '
RW_COMPRESS_DRIVER_OUTPUT_FLAG   = ' -o ' + RW_COMPRESS_DRIVER   + ' '
RW_DECOMPRESS_PREPROC_DEFINE = ' -D DECOMPRESS_TEST '
RW_COMPRESS_PREPROC_DEFINE   = ' -D COMPRESS_TEST '

RO_DECOMPRESS_DRIVER_SRC_FILES  = ' .\\c_drivers\\q6zip_test.c'
RO_DECOMPRESS_DRIVER_SRC_FILES += ' ..\\..\\src\\q6_dczero.s'
RO_DECOMPRESS_DRIVER_SRC_FILES += ' '
RO_DECOMPRESS_DRIVER = 'test'
RO_DECOMPRESS_DRIVER_OUTPUT_FLAG = ' -o ' + RO_DECOMPRESS_DRIVER + ' '


HEXAGON_SIM_DECOMP_FLAGS     = ' --timing -' + CPU_FLAG + ' -p ' 
HEXAGON_FASTISS_DECOMP_FLAGS = ' --nostatsfile --jit --multithread '

HEXAGON_SIM_COMP_FLAGS     = ' --timing -' + CPU_FLAG + ' -p ' 
HEXAGON_FASTISS_COMP_FLAGS = ' --nostatsfile --jit --multithread '


# version is from 3rd backslash to 2nd backslash (going in reverse)
#   expected example input: "..\\compressor\\rw\\v1.1\\src\\rw_compress.c"
def get_version(a):
        to_first_backslash = a [ : a.rfind('\\') ]
        to_second_backslash = to_first_backslash [ : to_first_backslash.rfind('\\') ]
        version = to_second_backslash [ to_second_backslash.rfind('\\') + 1 : ]
        return version
# src is from last 1st backslash to end (going in reverse)
#   expected example input: "..\\compressor\\rw\\v1.1\\src\\rw_compress.c"
def get_src(a):
        return a[ a.rfind('\\') + 1 : ]


if GEN_OPTIONS.IS_JENKINS_RUN:
        d_version = get_version(RW_OPTIONS.RUNTIME_DECOMPRESSOR_SRC)
        d_src     = get_src(RW_OPTIONS.RUNTIME_DECOMPRESSOR_SRC)
        # For RO, blindly set the compression flags anyways
        c_version = get_version(RW_OPTIONS.RUNTIME_COMPRESSOR_SRC)
        c_src     = get_src(RW_OPTIONS.RUNTIME_COMPRESSOR_SRC)

        # add "--sim_out path\src.version.decomp.profile"
        HEXAGON_SIM_DECOMP_FLAGS     += ' --sim_out ' + JENKINS_OPTIONS.OUTPUT_FOLDER + '\\' + d_src + '.' + d_version + '.decomp.profile '
        HEXAGON_FASTISS_DECOMP_FLAGS += ' --sim_out ' + JENKINS_OPTIONS.OUTPUT_FOLDER + '\\' + d_src + '.' + d_version + '.decomp.profile '
        # add "--sim_out path\src.version.comp.profile"
        HEXAGON_SIM_COMP_FLAGS     += ' --sim_out ' + JENKINS_OPTIONS.OUTPUT_FOLDER + '\\' + c_src + '.' + c_version + '.comp.profile '
        HEXAGON_FASTISS_COMP_FLAGS += ' --sim_out ' + JENKINS_OPTIONS.OUTPUT_FOLDER + '\\' + c_src + '.' + c_version + '.comp.profile '

