#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "rw_compress.h"


#ifdef DECOMPRESS_TEST
unsigned long long int test_deltaUncompress(char *data_file, char *output_file);
#endif
#ifdef COMPRESS_TEST
unsigned long long int test_deltaCompress(char *data_file, char *output_file);
#endif


int main(int argc, char* argv[]) 
{
        int test_index;
        int num_blocks;
        int compression_round;
        unsigned long long int total_cycles;
        unsigned long long int avg_cycles;

        if(argc != 3){
                fprintf(stderr, "Error: Incorrect arguments\n");
                fprintf(stderr, "Usage: ./a.out [num_blocks] [compression_round]\n");
                exit(1);
        }

        num_blocks        = atoi(argv[1]);
        compression_round = atoi(argv[2]); 

        printf("num_blocks = %d\n",num_blocks);

        total_cycles = 0;
        for (test_index=0;test_index<num_blocks;test_index++) 
        {
                char compress_file_name[100];
                char uncompress_file_name[100];
                char recompress_file_name[100];
                if(compression_round == 0){   
                        snprintf(compress_file_name, sizeof(compress_file_name),"compressed%d.bin",test_index);
                        snprintf(uncompress_file_name,sizeof(uncompress_file_name),"uncompressed%d.bin",test_index);
                        snprintf(recompress_file_name,sizeof(recompress_file_name),"recompressed%d.bin",test_index);
                }else if(compression_round == 1){
                        snprintf(compress_file_name,sizeof(compress_file_name),"recompressed%d.bin",test_index);
                        snprintf(uncompress_file_name,sizeof(uncompress_file_name),"reuncompressed%d.bin",test_index);
                        snprintf(recompress_file_name,sizeof(recompress_file_name),"rerecompressed%d.bin",test_index);
                }else if (compression_round == 2){
                        snprintf(compress_file_name,sizeof(compress_file_name),"rerecompressed%d.bin",test_index);
                        snprintf(uncompress_file_name,sizeof(uncompress_file_name),"rereuncompressed%d.bin",test_index);
                        snprintf(recompress_file_name,sizeof(recompress_file_name),"rererecompressed%d.bin",test_index);
                }else{
                        fprintf(stderr, "Error: Only 3 rounds of recompression supported\n");
                        exit(1);
                }

#ifdef DECOMPRESS_TEST
                total_cycles += test_deltaUncompress(compress_file_name, uncompress_file_name);
#endif
#ifdef COMPRESS_TEST
                total_cycles += test_deltaCompress(uncompress_file_name, recompress_file_name);
#endif
        }

#if __hexagon__
        avg_cycles = total_cycles / (num_blocks*3);
#ifdef DECOMPRESS_TEST
        printf("avg decompress cycles / 4KB block (tC)=%lld\n", avg_cycles);
#endif
#ifdef COMPRESS_TEST
        printf("avg compress cycles / 4KB block (tC)=%lld\n", avg_cycles);
#endif
#endif

        return 0;
}

