import sys,struct,os
import shutil
from time import sleep
import filecmp


ORIG_BIN = "cand_comp.bin"
num_blocks = 0
input_bytes = 0


def run(cmd):
        print "Running:", cmd
        out=os.system(cmd)
        if out!=0:
                print("FAILED[%d]: %s"%(out,str(cmd)))
                sys.exit()

# strips path and extension
# e.g. returns "helpme" from "path\\morepath\\helpme.vb"
def strip_path_and_ext(a):
        return a[ a.rfind('\\')+1 : a.rfind('.') ]

def strip_ext(a):
        return a[ : a.find('.') ]



def create_compressed_bins_from_crashdump():
        global num_blocks

        print "Preparing compressed bin files from crashdump..."

        sys.path.append(".\\scripts")
        import make_rw_bin_files

        # create the binaries in current directory
        print "Running make_rw_bin_files()"

        if config.GEN_OPTIONS.NUM_PAGES <= 0:
                num_blocks = make_rw_bin_files.make_rw_bin_files(config.CRASHDUMP_INPUT.EXTRACTED_RW_DATA)
        else:
                num_blocks = config.GEN_OPTIONS.NUM_PAGES
                make_rw_bin_files.make_rw_bin_files(config.CRASHDUMP_INPUT.EXTRACTED_RW_DATA, num_blocks)

        print "  num_blocks: %d" % num_blocks


def create_compressed_bins_from_elf():
        global num_blocks
        global input_bytes

        if config.GEN_OPTIONS.ALGORITHM == "RW":
                sys.path.append(config.RW_OPTIONS.PY_COMPRESSOR_PATH)
                import rw_py_compress
        else:
                sys.path.append(config.RO_OPTIONS.PY_COMPRESSOR_PATH)
                import q6zip_compress



        print "Preparing section from elf..."
        run(config.HEXAGON_OBJCOPY + ' -O binary -j ' + config.ELF_INPUT.SECTION + ' ' + config.ELF_INPUT.ELF + ' ' + ORIG_BIN)
        with open(ORIG_BIN, "rb") as f:
                f.seek( (1024*4) * config.ELF_INPUT.START )
                input_bytes = f.read( (1024*4) * config.GEN_OPTIONS.NUM_PAGES )

        f = open(ORIG_BIN, "wb")
        f.write(input_bytes)
        f.close


        print "Starting build-time compression..."
        new_va = 0
        if config.GEN_OPTIONS.ALGORITHM == "RW":
                block_size = 1024
                full_compressed_data = rw_py_compress.rw_py_compress(block_size, new_va, input_bytes)
        else:
                page_size = 4096
                full_compressed_data = q6zip_compress.compress(page_size, new_va, input_bytes)


        print "Dumping compressed data to file...\n"
        with open("orig_compressed.bin", "wb") as f:
                for line in full_compressed_data:
                        f.write(line)
        f.close()


        print "Parsing metadata...\n"
        f = open("orig_compressed.bin", "rb")
        compressed_meta = f.read(2)        
        num_blocks  = struct.unpack('H', compressed_meta)[0]
        compressed_meta = f.read(2)
        #version  = struct.unpack('H', compressed_meta)[0]
        print(" num_blocks: " + str(num_blocks))
        #print(" version: " + str(version))


        if config.GEN_OPTIONS.ALGORITHM == "RO":
                compressed_meta = f.read((1024+4096)*4)
                with open("dict.bin", "wb") as d:
                        d.write(compressed_meta)                


        compressed_page_sizes = []
        compressed_meta = f.read(4)
        prev = struct.unpack('I', compressed_meta)[0]
        for block in range(num_blocks - 1):
                compressed_meta = f.read(4)
                va = struct.unpack('I', compressed_meta)[0]
                delta = va - prev
                print " va[%d]= %s %s" % (block, str(va), str(delta))
                prev = va
                compressed_page_sizes.append(delta)
        compressed_page_sizes.append(4096)        

        print "\nSplitting compressed data into individually compressed pages...\n"
        for idx, block_size in enumerate(compressed_page_sizes):
                compressed_data = f.read(block_size)
                with open("compressed"+str(idx)+'.bin', "wb") as compressed_page:
                        compressed_page.write(compressed_data)
        f.close()





def create_compressed_bins():
        if config.GEN_OPTIONS.USE_ELF_INPUT:
                create_compressed_bins_from_elf()
        else:
                create_compressed_bins_from_crashdump()





def compile_decompressor_ro():

        print "Compiling uncompressor...\n"
        runtime_decompressor_o = ' ' + strip_path_and_ext(config.RO_OPTIONS.RUNTIME_DECOMPRESSOR_SRC) + '.o '
        run(config.HEXAGON_GCC + config.HEXAGON_GCC_COMPILE_FLAGS + config.RO_OPTIONS.RUNTIME_DECOMPRESSOR_SRC + config.HEXAGON_GCC_INCLUDE_FLAGS + config.HEXAGON_GCC_INCLUDE_FLAGS_DECOMP + config.CPU_FLAG)
        run(config.HEXAGON_CLANG + config.HEXAGON_CLANG_COMPILE_FLAGS + config.RO_DECOMPRESS_DRIVER_SRC_FILES + runtime_decompressor_o + config.HEXAGON_CLANG_LIBRARY_FLAGS + config.RO_DECOMPRESS_DRIVER_OUTPUT_FLAG + config.HEXAGON_CLANG_INCLUDE_FLAGS + config.HEXAGON_CLANG_INCLUDE_FLAGS_DECOMP + config.CPU_FLAG)



def compile_decompressor_and_compressor_rw():

        # Note: runtime_decompressor_o and runtime_compressor_o might be identical.
        # Compile and link entire decompressor executable before doing compressor.
        # This avoids overwriting the .o file before linking.
        runtime_decompressor_o = ' ' + strip_path_and_ext(config.RW_OPTIONS.RUNTIME_DECOMPRESSOR_SRC) + '.o '
        runtime_compressor_o   = ' ' + strip_path_and_ext(config.RW_OPTIONS.RUNTIME_COMPRESSOR_SRC)   + '.o '
        
        print "Compiling uncompressor...\n"
        run(config.HEXAGON_GCC + config.HEXAGON_GCC_COMPILE_FLAGS + config.RW_OPTIONS.RUNTIME_DECOMPRESSOR_SRC + config.HEXAGON_GCC_INCLUDE_FLAGS + config.HEXAGON_GCC_INCLUDE_FLAGS_DECOMP + config.CPU_FLAG)
        run(config.HEXAGON_CLANG + config.HEXAGON_CLANG_COMPILE_FLAGS + config.RW_DECOMPRESS_DRIVER_SRC_FILES + runtime_decompressor_o + config.HEXAGON_CLANG_LIBRARY_FLAGS + config.RW_DECOMPRESS_DRIVER_OUTPUT_FLAG + config.RW_DECOMPRESS_PREPROC_DEFINE + config.HEXAGON_CLANG_INCLUDE_FLAGS + config.HEXAGON_CLANG_INCLUDE_FLAGS_DECOMP + config.CPU_FLAG)


        print "Compiling compressor...\n"
        run(config.HEXAGON_GCC + config.HEXAGON_GCC_COMPILE_FLAGS + config.RW_OPTIONS.RUNTIME_COMPRESSOR_SRC + config.HEXAGON_GCC_INCLUDE_FLAGS + config.HEXAGON_GCC_INCLUDE_FLAGS_COMP + config.CPU_FLAG)
        run(config.HEXAGON_CLANG + config.HEXAGON_CLANG_COMPILE_FLAGS + config.RW_COMPRESS_DRIVER_SRC_FILES + runtime_compressor_o + config.HEXAGON_CLANG_LIBRARY_FLAGS + config.RW_COMPRESS_DRIVER_OUTPUT_FLAG + config.RW_COMPRESS_PREPROC_DEFINE + config.HEXAGON_CLANG_INCLUDE_FLAGS + config.HEXAGON_CLANG_INCLUDE_FLAGS_COMP + config.CPU_FLAG)





def run_decompressor_ro():
        if config.GEN_OPTIONS.USE_FASTISS:
                sim   = config.HEXAGON_FASTISS
                flags = config.HEXAGON_FASTISS_DECOMP_FLAGS
        else:
                sim   = config.HEXAGON_SIM
                flags = config.HEXAGON_SIM_DECOMP_FLAGS
        args = ' -- ' + str(num_blocks)
        run(sim + flags + config.RO_DECOMPRESS_DRIVER + args)



def run_decompressor_and_compressor_rw():

        if config.GEN_OPTIONS.USE_FASTISS:
                sim          = config.HEXAGON_FASTISS
                flags_decomp = config.HEXAGON_FASTISS_DECOMP_FLAGS
                flags_comp   = config.HEXAGON_FASTISS_COMP_FLAGS
        else:
                sim          = config.HEXAGON_SIM
                flags_decomp = config.HEXAGON_SIM_DECOMP_FLAGS
                flags_comp   = config.HEXAGON_SIM_COMP_FLAGS

        i = 0
        while i < config.RW_OPTIONS.NUM_RECOMPRESSIONS:
                args = ' -- ' + str(num_blocks) + ' ' + str(i)
                run(sim + flags_decomp + config.RW_DECOMPRESS_DRIVER + args)
                run(sim + flags_comp   + config.RW_COMPRESS_DRIVER   + args)
                i = i + 1

        

def combine_bins(un, com, recom):

        destination = open(un+'.bin', 'wb')
	for block_i in range(num_blocks):
		block = un+'%d.bin'%block_i
		shutil.copyfileobj(open(block, 'rb'), destination)
	destination.close()

	destination = open(com+'.bin', 'wb')
	for block_i in range(num_blocks):
		block = com+'%d.bin'%block_i
		shutil.copyfileobj(open(block, 'rb'), destination)
	destination.close()

	destination = open(recom+'.bin', 'wb')
	for block_i in range(num_blocks):
		block = recom+'%d.bin'%block_i
		shutil.copyfileobj(open(block, 'rb'), destination)
	destination.close()

def compare_bins(un, com, recom, prev_un):

	print "------------------------------------------------------"
        if prev_un:
                match = filecmp.cmp(prev_un+'.bin', un+'.bin') 
                if match:
                        print "PASS comparing " + prev_un + ".bin to "+ un + ".bin"
                else:		
                        print "FAIL comparing " + prev_un + ".bin to "+ un + ".bin"
			
	match=filecmp.cmp(recom+'.bin', com+'.bin') 
	if match:
		print "PASS comparing " + recom + ".bin to " + com + ".bin"
	else:		
		print "FAIL comparing " + recom + ".bin to " + com + ".bin"
	print "------------------------------------------------------"


def test_output_rw():

        print "\nTesting compressed output."

        # Combining bins depends on the number of recompressions we did.
        # The comparisons of interest depend on this, as well.
        
        i = 0
        prev_un = None
        if config.GEN_OPTIONS.USE_ELF_INPUT:
                prev_un = strip_ext(ORIG_BIN)
        un      = 'uncompressed'
        com     = 'compressed'
        recom   = 'recompressed'
        while i < config.RW_OPTIONS.NUM_RECOMPRESSIONS:
                un    = ("re" * i) + un
                com   = ("re" * i) + com
                recom = ("re" * i) + recom

                combine_bins(un, com, recom)
                compare_bins(un, com, recom, prev_un)

                prev_un = un
                i = i + 1



def test_output_ro():

        print "Testing compressed output..."
        destination = open('uncompressed.bin', 'wb')
        for block_i in range(num_blocks):
                block = '%d.out' % block_i
                shutil.copyfileobj(open(block, 'rb'), destination)
        destination.close()

        match = filecmp.cmp(ORIG_BIN, 'uncompressed.bin') 
        if match:
                print "PASS"
        else:                
                with open(ORIG_BIN,"rb") as i, open('uncompressed.bin',"rb") as o:
                        i.seek(config.ELF_INPUT.START)
                        inword = i.read(4)
                        outword = o.read(4)
                        word = 1
                        while (inword == outword):
                                inword = i.read(4)
                                outword = o.read(4)
                                word += 1
                print
                print "Word",word, " ", "Byte",word*4
                print "Expected:", hex(struct.unpack("I",inword.zfill(4))[0])
                print "  Actual:", hex(struct.unpack("I",outword.zfill(4))[0])
                print
                if word > len(input_bytes)/4:
                        print "PASS, but input is longer than what was compressed."
                else:
                        print "FAIL"



def clean_up():
        run('del test compress_test decompress_test *.out *.o gmon* pmu_* stats.txt cov.txt test *.bin *.pyc test fastiss.log /q')



def main_rw():
        if not config.GEN_OPTIONS.USE_ELF_INPUT: # i.e. if using crashdump_input
                if config.RW_OPTIONS.NUM_RECOMPRESSIONS < 2:
                        print "ERROR: Need RW_OPTIONS.NUM_RECOMPRESSIONS to be >= 2 for any crashdump input."
                        print "       Otherwise first recompress will fail, and there's no way to verify correctness."
                        exit(1)
        create_compressed_bins()
        compile_decompressor_and_compressor_rw()
        run_decompressor_and_compressor_rw()
        test_output_rw()
        clean_up()


def main_ro():
        create_compressed_bins()
        compile_decompressor_ro()
        run_decompressor_ro()
        test_output_ro()
        clean_up()
        


def main():

        global config

        # grab config from ./config or cmd-line arg
        if len(sys.argv) > 1:
                sys.path.append(sys.argv[1])
        else:
                sys.path.append(".\\config")
        import config


        if   config.GEN_OPTIONS.ALGORITHM == "RW":
                main_rw()
        elif config.GEN_OPTIONS.ALGORITHM == "RO":
                main_ro()
        else:
                print "ERROR: See config.py. GEN_OPTIONS.ALGORITHM should be RW or RO.  Exiting."
                exit(1)


if __name__ == '__main__':                
        main()
