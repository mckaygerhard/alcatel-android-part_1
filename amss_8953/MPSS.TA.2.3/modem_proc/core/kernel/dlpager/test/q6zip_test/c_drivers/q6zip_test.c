#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "q6zip_uncompress.h"
#include "hexagon_sim_timer.h"

int data_size;
int out_size;
FILE* fp_data;
FILE* fp_dict;
FILE* fp_out;
uint32_t* expected_buf;

char data_buf [0x8000] __attribute__ ((aligned (32)));
char dict_buf [0x8000] __attribute__ ((aligned (32)));
char out_buf [0x8000] __attribute__ ((aligned (32)));


unsigned long long int test(char *data_file,char *dict_file,char *output_file)
{
  fp_data = fopen(data_file, "rb");
  assert(fp_data); 
  fp_dict = fopen(dict_file, "rb");
  assert(fp_dict); 
  fp_out = fopen(output_file, "wb");
  assert(fp_out);
 
  fseek(fp_data, 0L, SEEK_END);
  data_size = ftell(fp_data);
  fseek(fp_data, 0L, SEEK_SET);
  //printf("data size %d\n", data_size);

  fseek(fp_dict, 0L, SEEK_END);
  int dict_size = ftell(fp_dict);
  fseek(fp_dict, 0L, SEEK_SET);
  //printf("dict size %d\n", dict_size);

  //char* data_buf = (char*)malloc (data_size);
  //assert (data_buf);

#ifndef SWAP_ENDIAN
  int data_buf_size = fread(data_buf, 1, data_size , fp_data);
#else
  int data_buf_size = 0;
  int i, j;
  for (i = 0; i < data_size; i += 4)
  {
    data_buf_size += fread(data_buf + i + 3, 1, 1 , fp_data);
    data_buf_size += fread(data_buf + i + 2, 1, 1 , fp_data);
    data_buf_size += fread(data_buf + i + 1, 1, 1 , fp_data);
    data_buf_size += fread(data_buf + i + 0, 1, 1 , fp_data);
//    printf("%02X %02X %02X %02X ", *(data_buf + i + 0), *(data_buf + i + 1), *(data_buf + i + 2), *(data_buf + i + 3));
  }
#endif
  
  //char* dict_buf = (char*)malloc (dict_size);
  //assert (dict_buf);
#if 0
  int dict_buf_size = fread(dict_buf, 1, dict_size , fp_dict);
#else
  int dict_buf_size = 0;
  int i;
  for (i = 0; i < dict_size; i += 4)
  {
    dict_buf_size += fread(dict_buf + i + 0, 1, 1 , fp_dict);
    dict_buf_size += fread(dict_buf + i + 1, 1, 1 , fp_dict);
    dict_buf_size += fread(dict_buf + i + 2, 1, 1 , fp_dict);
    dict_buf_size += fread(dict_buf + i + 3, 1, 1 , fp_dict);
    //printf("%02X %02X %02X %02X ", *(dict_buf + i + 0), *(dict_buf + i + 1), *(dict_buf + i + 2), *(dict_buf + i + 3));
  }
#endif
#if 0
  int j;
  unsigned int *dict_entry=(unsigned int *)dict_buf;
  for (j = 0; j < dict_size; j += 4)
  {
     printf("dict[%d]=0x%x\n",j/4,*dict_entry);
     dict_entry++;
  }
#endif
    
  //out_buf = (char*)(((unsigned int)((char*)malloc(0x4000))));
  //assert (out_buf);
  int total_out = 0;
  int total_count = 0;


#if 0

  printf("hello\n");
  {
     int i,sum;
     for (i=0;i<dict_size;i++) sum+=*(dict_buf+i);
     printf("hello=%d\n",sum);
  }

#endif
  unsigned long long int start_time,end_time,delta;
  start_time=hexagon_sim_read_pcycles();

  out_size=4096;
  q6zip_uncompress(out_buf, &out_size, data_buf, data_size, dict_buf);
  end_time=hexagon_sim_read_pcycles();
  delta=(end_time-start_time);
  //printf("pCycles=%d\n",(int)(end_time-start_time));
  //printf("data_size=%d out_size=%d\n",data_size,out_size);
  fwrite(out_buf, out_size,1,fp_out);
  fclose(fp_out);
  fclose(fp_data);
  fclose(fp_dict);
  return delta;
}

int main(int argc, char* argv[]) 
{
int test_index;
unsigned long long int decompressCycles=0;
  if (argc != 2)
  {
    fprintf (stderr,"Usage: main(start,end)\n" );
    exit(-2);
  }
  int num_blocks=atoi(argv[1]);
printf("num_blocks=%d\n",num_blocks);
  for (test_index=0;test_index<num_blocks;test_index++) 
  {
      //if (test_index!=174)
      {
         char input_file_name[100];
         char output_file_name[100];
       snprintf(input_file_name,sizeof(input_file_name),"compressed%d.bin",test_index);
       snprintf(output_file_name,sizeof(output_file_name),"%d.out",test_index);
       //printf("%s %s\n",input_file_name,output_file_name);
       decompressCycles+=test(input_file_name,"dict.bin",output_file_name);
      }
  }
printf("decompressCycles (tC)=%lld\n",(decompressCycles/(num_blocks*3)));
   return 0;
}

