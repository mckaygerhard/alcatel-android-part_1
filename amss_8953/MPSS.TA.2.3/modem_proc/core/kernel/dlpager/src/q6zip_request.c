#include "q6zip_context.h"
#include "q6zip_request.h"


q6zip_request_t * q6zip_request_alloc (void)
{
    q6zip_context_t * context;

    context = q6zip_context_alloc();
    if ( !context )
    {
        return NULL;
    }

    return &context->request;
}

q6zip_context_t const * q6zip_request_get_context (q6zip_request_t * request)
{
    return ( q6zip_context_t const * ) ( (qurt_addr_t) request - Q6ZIP_OFFSETOF( q6zip_context_t, request ) );
}

void q6zip_request_free (q6zip_request_t ** request)
{
    q6zip_context_t * context;

    /* Dig-out the base address of the context structure */
    context = ( q6zip_context_t * ) ( (qurt_addr_t)*request - Q6ZIP_OFFSETOF( q6zip_context_t, request ) );
    q6zip_context_free( &context );
    *request = NULL;
}

