/* @file q6zip_test_scenario_init.c */
#include "comdef.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test.h"
#include "q6zip_test_scenario_init.h"
#include "q6zip_test_vectors_ro.h"
#include "q6zip_waiter.h"
#include <stdio.h>

static q6zip_waiter_t waiter;
boolean is_adapter_ready = FALSE;

static void adapter_initialized (boolean initialized)
{
    is_adapter_ready = initialized;
    q6zip_waiter_wake( &waiter );
}

static void initialize (q6zip_test_scenario_t * scenario)
{
    q6zip_waiter_init( &waiter );
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
    /* Nothing to configure */
}

static void start (q6zip_test_scenario_t * scenario)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();

    Q6ZIP_ASSERT( scenario );
    q6zip->init( (void *)Q6ZIP_RO_DICT, Q6ZIP_TEST_RO_DICT_SIZE, adapter_initialized, q6zip_test_adapter_response );

    q6zip_waiter_wait( &waiter );

    if ( is_adapter_ready )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "[q6zip/test] dictionary loaded", 0, 0, 0 );
    }
    else
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "[q6zip/test] failed to load dictionary", 0, 0, 0 );
    }
}

static void response (
    q6zip_test_scenario_t *    scenario,
    q6zip_request_t const *  request,
    q6zip_response_t const * response
)
{
    /* Not expecting any response in this scenario */
    Q6ZIP_ASSERT( FALSE );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_init =
{
    .ops =
    {
        initialize,
        configure,
        start,
        response,
        stop
    },
};
